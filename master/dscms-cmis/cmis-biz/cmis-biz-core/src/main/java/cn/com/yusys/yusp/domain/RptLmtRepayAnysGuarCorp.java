/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarCorp
 * @类描述: rpt_lmt_repay_anys_guar_corp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:04:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_corp")
public class RptLmtRepayAnysGuarCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 采集年月 **/
	@Column(name = "ACQUISITION_DATE", unique = false, nullable = true, length = 10)
	private String acquisitionDate;
	
	/** 是否为简易版 **/
	@Column(name = "IS_SIMPLE", unique = false, nullable = true, length = 10)
	private String isSimple;
	
	/** 本次拟担保额度 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 担保测算额度 **/
	@Column(name = "GUAR_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarLmtAmt;
	
	/** 担保人名称 **/
	@Column(name = "GUAR_NAME", unique = false, nullable = true, length = 40)
	private String guarName;
	
	/** 担保人与借款人关系 **/
	@Column(name = "GUAR_DEBIT_RELA", unique = false, nullable = true, length = 5)
	private String guarDebitRela;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 40)
	private String legal;
	
	/** 实际控制人 **/
	@Column(name = "REAL_OPER_CUS_ID", unique = false, nullable = true, length = 40)
	private String realOperCusId;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCapAmt;
	
	/** 企业地址 **/
	@Column(name = "COMPANY_ADDR", unique = false, nullable = true, length = 80)
	private String companyAddr;
	
	/** 是否上市企业 **/
	@Column(name = "IS_STOCK_CORP", unique = false, nullable = true, length = 5)
	private String isStockCorp;
	
	/** 主营业务及主要产品 **/
	@Column(name = "MAIN_BUSI", unique = false, nullable = true, length = 200)
	private String mainBusi;
	
	/** 最近第二年开票销售 **/
	@Column(name = "LAST_TWO_YEAR_INVOICE_SALES_REVENUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearInvoiceSalesRevenue;
	
	/** 最近第二年税务报表净利润 **/
	@Column(name = "LAST_TWO_YEAR_NET_PROFIT_IN_TAX_STATEMENTS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearNetProfitInTaxStatements;
	
	/** 最近第二年自制报表销售 **/
	@Column(name = "LAST_TWO_YEAR_SELF_MADE_REPORT_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearSelfMadeReportSales;
	
	/** 最近第二年自制报表净利润 **/
	@Column(name = "LAST_TWO_YEAR_SELF_MADE_REPORT_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearSelfMadeReportNetProfit;
	
	/** 最近第一年开票销售 **/
	@Column(name = "LAST_YEAR_INVOICE_SALES_REVENUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearInvoiceSalesRevenue;
	
	/** 最近第一年税务报表净利润 **/
	@Column(name = "LAST_YEAR_NET_PROFIT_IN_TAX_STATEMENTS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearNetProfitInTaxStatements;
	
	/** 最近第一年自制报表销售 **/
	@Column(name = "LAST_YEAR_SELF_MADE_REPORT_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearSelfMadeReportSales;
	
	/** 最近第一年自制报表净利润 **/
	@Column(name = "LAST_YEAR_SELF_MADE_REPORT_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearSelfMadeReportNetProfit;
	
	/** 当前年月开票销售 **/
	@Column(name = "CUR_YM_INVOICE_SALES_REVENUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYmInvoiceSalesRevenue;
	
	/** 当前年月税务报表净利润 **/
	@Column(name = "CUR_YM_NET_PROFIT_IN_TAX_STATEMENTS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYmNetProfitInTaxStatements;
	
	/** 当前年月自制报表销售 **/
	@Column(name = "CUR_YM_SELF_MADE_REPORT_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYmSelfMadeReportSales;
	
	/** 当前年月自制报表净利润 **/
	@Column(name = "CUR_YM_SELF_MADE_REPORT_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYmSelfMadeReportNetProfit;
	
	/** 税务报表总资产 **/
	@Column(name = "DEP_TS_TOTAL_RESSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depTsTotalRessetAmt;
	
	/** 税务报表其中：固定资产 **/
	@Column(name = "DEP_TS_FIXED_ASSETS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depTsFixedAssets;
	
	/** 税务报表负债 **/
	@Column(name = "DEP_TS_LIABILITIES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depTsLiabilities;
	
	/** 税务报表净资产 **/
	@Column(name = "DEP_TS_PURE_ASSET", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depTsPureAsset;
	
	/** 自制报表总资产 **/
	@Column(name = "SELF_REP_TOTAL_RESSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfRepTotalRessetAmt;
	
	/** 自制报表其中：固定资产 **/
	@Column(name = "SELF_REP_FIXED_ASSETS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfRepFixedAssets;
	
	/** 自制报表负债 **/
	@Column(name = "SELF_REP_LIABILITIES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfRepLiabilities;
	
	/** 自制报表净资产 **/
	@Column(name = "SELF_REP_PURE_ASSET", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfRepPureAsset;
	
	/** 流动资产金额 **/
	@Column(name = "CURFUND_ASSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundAssetAmt;
	
	/** 货币资金金额 **/
	@Column(name = "CURFUND_PAYMENT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundPaymentAmt;
	
	/** 应收账款金额 **/
	@Column(name = "DEBT_RECEIVABLE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtReceivableAmt;
	
	/** 其他应收款金额 **/
	@Column(name = "OTHER_DEBT_RECEIVABLE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherDebtReceivableAmt;
	
	/** 预付账款金额 **/
	@Column(name = "PREPAYMENTS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prepaymentsAmt;
	
	/** 存货金额 **/
	@Column(name = "STOCK_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stockAmt;
	
	/** 固定资产金额 **/
	@Column(name = "FIXED_ASSETS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fixedAssetsAmt;
	
	/** 无形资产金额 **/
	@Column(name = "INTANGIBLE_ASSETS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intangibleAssetsAmt;
	
	/** 其他资产金额 **/
	@Column(name = "OTHER_ASSETS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherAssetsAmt;
	
	/** 流动负债金额 **/
	@Column(name = "CURRENT_LIABILITIES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentLiabilitiesAmt;
	
	/** 短期借款金额 **/
	@Column(name = "SHORT_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal shortLoanAmt;
	
	/** 应付票据金额 **/
	@Column(name = "BILL_PAYABLE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billPayableAmt;
	
	/** 应付账款金额 **/
	@Column(name = "ACCOUNTS_PAYABLE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal accountsPayableAmt;
	
	/** 预收帐款金额 **/
	@Column(name = "DEPOSIT_RECEIVED_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depositReceivedAmt;
	
	/** 其他应付款金额 **/
	@Column(name = "OTHER_PAYABLE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherPayableAmt;
	
	/** 长期负债金额 **/
	@Column(name = "LONG_LIABILITIES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal longLiabilitiesAmt;
	
	/** 负债总额金额 **/
	@Column(name = "TOTAL_LIABILITIES_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalLiabilitiesAmt;
	
	/** 所有者权益金额 **/
	@Column(name = "OWNER_EQUITY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ownerEquityAmt;
	
	/** 流动资产结构占比 **/
	@Column(name = "CURFUND_ASSET_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundAssetPerc;
	
	/** 货币资金结构占比 **/
	@Column(name = "CURFUND_PAYMENT_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundPaymentPerc;
	
	/** 应收账款结构占比 **/
	@Column(name = "DEBT_RECEIVABLE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtReceivablePerc;
	
	/** 其他应收款结构占比 **/
	@Column(name = "OTHER_DEBT_RECEIVABLE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherDebtReceivablePerc;
	
	/** 预付账款结构占比 **/
	@Column(name = "PREPAYMENTS_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prepaymentsPerc;
	
	/** 存货结构占比 **/
	@Column(name = "STOCK_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stockPerc;
	
	/** 固定资产结构占比 **/
	@Column(name = "FIXED_ASSETS_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fixedAssetsPerc;
	
	/** 无形资产结构占比 **/
	@Column(name = "INTANGIBLE_ASSETS_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal intangibleAssetsPerc;
	
	/** 其他资产结构占比 **/
	@Column(name = "OTHER_ASSETS_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherAssetsPerc;
	
	/** 流动负债结构占比 **/
	@Column(name = "CURRENT_LIABILITIES_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentLiabilitiesPerc;
	
	/** 短期借款结构占比 **/
	@Column(name = "SHORT_LOAN_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal shortLoanPerc;
	
	/** 应付票据结构占比 **/
	@Column(name = "BILL_PAYABLE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billPayablePerc;
	
	/** 应付账款结构占比 **/
	@Column(name = "ACCOUNTS_PAYABLE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal accountsPayablePerc;
	
	/** 预收帐款结构占比 **/
	@Column(name = "DEPOSIT_RECEIVED_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal depositReceivedPerc;
	
	/** 其他应付款结构占比 **/
	@Column(name = "OTHER_PAYABLE_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherPayablePerc;
	
	/** 长期负债结构占比 **/
	@Column(name = "LONG_LIABILITIES_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal longLiabilitiesPerc;
	
	/** 负债总额结构占比 **/
	@Column(name = "TOTAL_LIABILITIES_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalLiabilitiesPerc;
	
	/** 所有者权益结构占比 **/
	@Column(name = "OWNER_EQUITY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ownerEquityPerc;
	
	/** 流动资产简要说明或分析 **/
	@Column(name = "CURFUND_ASSET_DESC", unique = false, nullable = true, length = 65535)
	private String curfundAssetDesc;
	
	/** 货币资金简要说明或分析 **/
	@Column(name = "CURFUND_PAYMENT_DESC", unique = false, nullable = true, length = 65535)
	private String curfundPaymentDesc;
	
	/** 应收账款简要说明或分析 **/
	@Column(name = "DEBT_RECEIVABLE_DESC", unique = false, nullable = true, length = 65535)
	private String debtReceivableDesc;
	
	/** 其他应收款简要说明或分析 **/
	@Column(name = "OTHER_DEBT_RECEIVABLE_DESC", unique = false, nullable = true, length = 65535)
	private String otherDebtReceivableDesc;
	
	/** 预付账款简要说明或分析 **/
	@Column(name = "PREPAYMENTS_DESC", unique = false, nullable = true, length = 65535)
	private String prepaymentsDesc;
	
	/** 存货简要说明或分析 **/
	@Column(name = "STOCK_DESC", unique = false, nullable = true, length = 65535)
	private String stockDesc;
	
	/** 固定资产简要说明或分析 **/
	@Column(name = "FIXED_ASSETS_DESC", unique = false, nullable = true, length = 65535)
	private String fixedAssetsDesc;
	
	/** 无形资产简要说明或分析 **/
	@Column(name = "INTANGIBLE_ASSETS_DESC", unique = false, nullable = true, length = 65535)
	private String intangibleAssetsDesc;
	
	/** 其他资产简要说明或分析 **/
	@Column(name = "OTHER_ASSETS_DESC", unique = false, nullable = true, length = 65535)
	private String otherAssetsDesc;
	
	/** 流动负债简要说明或分析 **/
	@Column(name = "CURRENT_LIABILITIES_DESC", unique = false, nullable = true, length = 65535)
	private String currentLiabilitiesDesc;
	
	/** 短期借款简要说明或分析 **/
	@Column(name = "SHORT_LOAN_DESC", unique = false, nullable = true, length = 65535)
	private String shortLoanDesc;
	
	/** 应付票据简要说明或分析 **/
	@Column(name = "BILL_PAYABLE_DESC", unique = false, nullable = true, length = 65535)
	private String billPayableDesc;
	
	/** 应付账款简要说明或分析 **/
	@Column(name = "ACCOUNTS_PAYABLE_DESC", unique = false, nullable = true, length = 65535)
	private String accountsPayableDesc;
	
	/** 预收帐款简要说明或分析 **/
	@Column(name = "DEPOSIT_RECEIVED_DESC", unique = false, nullable = true, length = 65535)
	private String depositReceivedDesc;
	
	/** 其他应付款简要说明或分析 **/
	@Column(name = "OTHER_PAYABLE_DESC", unique = false, nullable = true, length = 65535)
	private String otherPayableDesc;
	
	/** 长期负债简要说明或分析 **/
	@Column(name = "LONG_LIABILITIES_DESC", unique = false, nullable = true, length = 65535)
	private String longLiabilitiesDesc;
	
	/** 负债总额简要说明或分析 **/
	@Column(name = "TOTAL_LIABILITIES_DESC", unique = false, nullable = true, length = 65535)
	private String totalLiabilitiesDesc;
	
	/** 所有者权益简要说明或分析 **/
	@Column(name = "OWNER_EQUITY_DESC", unique = false, nullable = true, length = 65535)
	private String ownerEquityDesc;
	
	/** 资产负债详情其他需说明事项 **/
	@Column(name = "ASSET_DEBT_RATE_DESC", unique = false, nullable = true, length = 65535)
	private String assetDebtRateDesc;
	
	/** 他行融资情况 **/
	@Column(name = "OTHER_BANK_CPTL_SITU", unique = false, nullable = true, length = 65535)
	private String otherBankCptlSitu;
	
	/** 本行融资情况 **/
	@Column(name = "SELF_BANK_CPTL_SITU", unique = false, nullable = true, length = 65535)
	private String selfBankCptlSitu;
	
	/** 个人经营性贷款融资情况 **/
	@Column(name = "OPERATING_NET_CPTL_SITU", unique = false, nullable = true, length = 65535)
	private String operatingNetCptlSitu;
	
	/** 正常对外担保情况 **/
	@Column(name = "NRML_OUTGUAR_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String nrmlOutguarSituDesc;
	
	/** 关注对外担保情况 **/
	@Column(name = "ATT_OUTGUAR_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String attOutguarSituDesc;
	
	/** 不良对外担保情况 **/
	@Column(name = "BAD_OUTGUAR_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String badOutguarSituDesc;
	
	/** 担保人及实际控制人信用情况 **/
	@Column(name = "GUAR_REAL_OPER__CDT_EXPL", unique = false, nullable = true, length = 65535)
	private String guarRealOperCdtExpl;
	
	/** 租用土地面积 **/
	@Column(name = "RENT_LAND_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentLandSqu;
	
	/** 租用厂房面积 **/
	@Column(name = "RENT_WORKSHOP_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentWorkshopSqu;
	
	/** 自有土地面积 **/
	@Column(name = "SELF_LAND_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfLandSqu;
	
	/** 自有有证土地面积 **/
	@Column(name = "SELF_CERT_LAND_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfCertLandSqu;
	
	/** 自有有证土地性质 **/
	@Column(name = "SELF_CERT_LAND_CHA", unique = false, nullable = true, length = 40)
	private String selfCertLandCha;
	
	/** 房产面积 **/
	@Column(name = "HOUSE_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseSqu;
	
	/** 房产有证面积 **/
	@Column(name = "CERT_HOUSE_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal certHouseSqu;
	
	/** 抵押情况 **/
	@Column(name = "GUAR_SITU", unique = false, nullable = true, length = 65535)
	private String guarSitu;
	
	/** 担保能力总体评价 **/
	@Column(name = "GUAR_ABL_EVAL", unique = false, nullable = true, length = 65535)
	private String guarAblEval;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param acquisitionDate
	 */
	public void setAcquisitionDate(String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}
	
    /**
     * @return acquisitionDate
     */
	public String getAcquisitionDate() {
		return this.acquisitionDate;
	}
	
	/**
	 * @param isSimple
	 */
	public void setIsSimple(String isSimple) {
		this.isSimple = isSimple;
	}
	
    /**
     * @return isSimple
     */
	public String getIsSimple() {
		return this.isSimple;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param guarLmtAmt
	 */
	public void setGuarLmtAmt(java.math.BigDecimal guarLmtAmt) {
		this.guarLmtAmt = guarLmtAmt;
	}
	
    /**
     * @return guarLmtAmt
     */
	public java.math.BigDecimal getGuarLmtAmt() {
		return this.guarLmtAmt;
	}
	
	/**
	 * @param guarName
	 */
	public void setGuarName(String guarName) {
		this.guarName = guarName;
	}
	
    /**
     * @return guarName
     */
	public String getGuarName() {
		return this.guarName;
	}
	
	/**
	 * @param guarDebitRela
	 */
	public void setGuarDebitRela(String guarDebitRela) {
		this.guarDebitRela = guarDebitRela;
	}
	
    /**
     * @return guarDebitRela
     */
	public String getGuarDebitRela() {
		return this.guarDebitRela;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param realOperCusId
	 */
	public void setRealOperCusId(String realOperCusId) {
		this.realOperCusId = realOperCusId;
	}
	
    /**
     * @return realOperCusId
     */
	public String getRealOperCusId() {
		return this.realOperCusId;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}
	
    /**
     * @return paidCapAmt
     */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}
	
	/**
	 * @param companyAddr
	 */
	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}
	
    /**
     * @return companyAddr
     */
	public String getCompanyAddr() {
		return this.companyAddr;
	}
	
	/**
	 * @param isStockCorp
	 */
	public void setIsStockCorp(String isStockCorp) {
		this.isStockCorp = isStockCorp;
	}
	
    /**
     * @return isStockCorp
     */
	public String getIsStockCorp() {
		return this.isStockCorp;
	}
	
	/**
	 * @param mainBusi
	 */
	public void setMainBusi(String mainBusi) {
		this.mainBusi = mainBusi;
	}
	
    /**
     * @return mainBusi
     */
	public String getMainBusi() {
		return this.mainBusi;
	}
	
	/**
	 * @param lastTwoYearInvoiceSalesRevenue
	 */
	public void setLastTwoYearInvoiceSalesRevenue(java.math.BigDecimal lastTwoYearInvoiceSalesRevenue) {
		this.lastTwoYearInvoiceSalesRevenue = lastTwoYearInvoiceSalesRevenue;
	}
	
    /**
     * @return lastTwoYearInvoiceSalesRevenue
     */
	public java.math.BigDecimal getLastTwoYearInvoiceSalesRevenue() {
		return this.lastTwoYearInvoiceSalesRevenue;
	}
	
	/**
	 * @param lastTwoYearNetProfitInTaxStatements
	 */
	public void setLastTwoYearNetProfitInTaxStatements(java.math.BigDecimal lastTwoYearNetProfitInTaxStatements) {
		this.lastTwoYearNetProfitInTaxStatements = lastTwoYearNetProfitInTaxStatements;
	}
	
    /**
     * @return lastTwoYearNetProfitInTaxStatements
     */
	public java.math.BigDecimal getLastTwoYearNetProfitInTaxStatements() {
		return this.lastTwoYearNetProfitInTaxStatements;
	}
	
	/**
	 * @param lastTwoYearSelfMadeReportSales
	 */
	public void setLastTwoYearSelfMadeReportSales(java.math.BigDecimal lastTwoYearSelfMadeReportSales) {
		this.lastTwoYearSelfMadeReportSales = lastTwoYearSelfMadeReportSales;
	}
	
    /**
     * @return lastTwoYearSelfMadeReportSales
     */
	public java.math.BigDecimal getLastTwoYearSelfMadeReportSales() {
		return this.lastTwoYearSelfMadeReportSales;
	}
	
	/**
	 * @param lastTwoYearSelfMadeReportNetProfit
	 */
	public void setLastTwoYearSelfMadeReportNetProfit(java.math.BigDecimal lastTwoYearSelfMadeReportNetProfit) {
		this.lastTwoYearSelfMadeReportNetProfit = lastTwoYearSelfMadeReportNetProfit;
	}
	
    /**
     * @return lastTwoYearSelfMadeReportNetProfit
     */
	public java.math.BigDecimal getLastTwoYearSelfMadeReportNetProfit() {
		return this.lastTwoYearSelfMadeReportNetProfit;
	}
	
	/**
	 * @param lastYearInvoiceSalesRevenue
	 */
	public void setLastYearInvoiceSalesRevenue(java.math.BigDecimal lastYearInvoiceSalesRevenue) {
		this.lastYearInvoiceSalesRevenue = lastYearInvoiceSalesRevenue;
	}
	
    /**
     * @return lastYearInvoiceSalesRevenue
     */
	public java.math.BigDecimal getLastYearInvoiceSalesRevenue() {
		return this.lastYearInvoiceSalesRevenue;
	}
	
	/**
	 * @param lastYearNetProfitInTaxStatements
	 */
	public void setLastYearNetProfitInTaxStatements(java.math.BigDecimal lastYearNetProfitInTaxStatements) {
		this.lastYearNetProfitInTaxStatements = lastYearNetProfitInTaxStatements;
	}
	
    /**
     * @return lastYearNetProfitInTaxStatements
     */
	public java.math.BigDecimal getLastYearNetProfitInTaxStatements() {
		return this.lastYearNetProfitInTaxStatements;
	}
	
	/**
	 * @param lastYearSelfMadeReportSales
	 */
	public void setLastYearSelfMadeReportSales(java.math.BigDecimal lastYearSelfMadeReportSales) {
		this.lastYearSelfMadeReportSales = lastYearSelfMadeReportSales;
	}
	
    /**
     * @return lastYearSelfMadeReportSales
     */
	public java.math.BigDecimal getLastYearSelfMadeReportSales() {
		return this.lastYearSelfMadeReportSales;
	}
	
	/**
	 * @param lastYearSelfMadeReportNetProfit
	 */
	public void setLastYearSelfMadeReportNetProfit(java.math.BigDecimal lastYearSelfMadeReportNetProfit) {
		this.lastYearSelfMadeReportNetProfit = lastYearSelfMadeReportNetProfit;
	}
	
    /**
     * @return lastYearSelfMadeReportNetProfit
     */
	public java.math.BigDecimal getLastYearSelfMadeReportNetProfit() {
		return this.lastYearSelfMadeReportNetProfit;
	}
	
	/**
	 * @param curYmInvoiceSalesRevenue
	 */
	public void setCurYmInvoiceSalesRevenue(java.math.BigDecimal curYmInvoiceSalesRevenue) {
		this.curYmInvoiceSalesRevenue = curYmInvoiceSalesRevenue;
	}
	
    /**
     * @return curYmInvoiceSalesRevenue
     */
	public java.math.BigDecimal getCurYmInvoiceSalesRevenue() {
		return this.curYmInvoiceSalesRevenue;
	}
	
	/**
	 * @param curYmNetProfitInTaxStatements
	 */
	public void setCurYmNetProfitInTaxStatements(java.math.BigDecimal curYmNetProfitInTaxStatements) {
		this.curYmNetProfitInTaxStatements = curYmNetProfitInTaxStatements;
	}
	
    /**
     * @return curYmNetProfitInTaxStatements
     */
	public java.math.BigDecimal getCurYmNetProfitInTaxStatements() {
		return this.curYmNetProfitInTaxStatements;
	}
	
	/**
	 * @param curYmSelfMadeReportSales
	 */
	public void setCurYmSelfMadeReportSales(java.math.BigDecimal curYmSelfMadeReportSales) {
		this.curYmSelfMadeReportSales = curYmSelfMadeReportSales;
	}
	
    /**
     * @return curYmSelfMadeReportSales
     */
	public java.math.BigDecimal getCurYmSelfMadeReportSales() {
		return this.curYmSelfMadeReportSales;
	}
	
	/**
	 * @param curYmSelfMadeReportNetProfit
	 */
	public void setCurYmSelfMadeReportNetProfit(java.math.BigDecimal curYmSelfMadeReportNetProfit) {
		this.curYmSelfMadeReportNetProfit = curYmSelfMadeReportNetProfit;
	}
	
    /**
     * @return curYmSelfMadeReportNetProfit
     */
	public java.math.BigDecimal getCurYmSelfMadeReportNetProfit() {
		return this.curYmSelfMadeReportNetProfit;
	}
	
	/**
	 * @param depTsTotalRessetAmt
	 */
	public void setDepTsTotalRessetAmt(java.math.BigDecimal depTsTotalRessetAmt) {
		this.depTsTotalRessetAmt = depTsTotalRessetAmt;
	}
	
    /**
     * @return depTsTotalRessetAmt
     */
	public java.math.BigDecimal getDepTsTotalRessetAmt() {
		return this.depTsTotalRessetAmt;
	}
	
	/**
	 * @param depTsFixedAssets
	 */
	public void setDepTsFixedAssets(java.math.BigDecimal depTsFixedAssets) {
		this.depTsFixedAssets = depTsFixedAssets;
	}
	
    /**
     * @return depTsFixedAssets
     */
	public java.math.BigDecimal getDepTsFixedAssets() {
		return this.depTsFixedAssets;
	}
	
	/**
	 * @param depTsLiabilities
	 */
	public void setDepTsLiabilities(java.math.BigDecimal depTsLiabilities) {
		this.depTsLiabilities = depTsLiabilities;
	}
	
    /**
     * @return depTsLiabilities
     */
	public java.math.BigDecimal getDepTsLiabilities() {
		return this.depTsLiabilities;
	}
	
	/**
	 * @param depTsPureAsset
	 */
	public void setDepTsPureAsset(java.math.BigDecimal depTsPureAsset) {
		this.depTsPureAsset = depTsPureAsset;
	}
	
    /**
     * @return depTsPureAsset
     */
	public java.math.BigDecimal getDepTsPureAsset() {
		return this.depTsPureAsset;
	}
	
	/**
	 * @param selfRepTotalRessetAmt
	 */
	public void setSelfRepTotalRessetAmt(java.math.BigDecimal selfRepTotalRessetAmt) {
		this.selfRepTotalRessetAmt = selfRepTotalRessetAmt;
	}
	
    /**
     * @return selfRepTotalRessetAmt
     */
	public java.math.BigDecimal getSelfRepTotalRessetAmt() {
		return this.selfRepTotalRessetAmt;
	}
	
	/**
	 * @param selfRepFixedAssets
	 */
	public void setSelfRepFixedAssets(java.math.BigDecimal selfRepFixedAssets) {
		this.selfRepFixedAssets = selfRepFixedAssets;
	}
	
    /**
     * @return selfRepFixedAssets
     */
	public java.math.BigDecimal getSelfRepFixedAssets() {
		return this.selfRepFixedAssets;
	}
	
	/**
	 * @param selfRepLiabilities
	 */
	public void setSelfRepLiabilities(java.math.BigDecimal selfRepLiabilities) {
		this.selfRepLiabilities = selfRepLiabilities;
	}
	
    /**
     * @return selfRepLiabilities
     */
	public java.math.BigDecimal getSelfRepLiabilities() {
		return this.selfRepLiabilities;
	}
	
	/**
	 * @param selfRepPureAsset
	 */
	public void setSelfRepPureAsset(java.math.BigDecimal selfRepPureAsset) {
		this.selfRepPureAsset = selfRepPureAsset;
	}
	
    /**
     * @return selfRepPureAsset
     */
	public java.math.BigDecimal getSelfRepPureAsset() {
		return this.selfRepPureAsset;
	}
	
	/**
	 * @param curfundAssetAmt
	 */
	public void setCurfundAssetAmt(java.math.BigDecimal curfundAssetAmt) {
		this.curfundAssetAmt = curfundAssetAmt;
	}
	
    /**
     * @return curfundAssetAmt
     */
	public java.math.BigDecimal getCurfundAssetAmt() {
		return this.curfundAssetAmt;
	}
	
	/**
	 * @param curfundPaymentAmt
	 */
	public void setCurfundPaymentAmt(java.math.BigDecimal curfundPaymentAmt) {
		this.curfundPaymentAmt = curfundPaymentAmt;
	}
	
    /**
     * @return curfundPaymentAmt
     */
	public java.math.BigDecimal getCurfundPaymentAmt() {
		return this.curfundPaymentAmt;
	}
	
	/**
	 * @param debtReceivableAmt
	 */
	public void setDebtReceivableAmt(java.math.BigDecimal debtReceivableAmt) {
		this.debtReceivableAmt = debtReceivableAmt;
	}
	
    /**
     * @return debtReceivableAmt
     */
	public java.math.BigDecimal getDebtReceivableAmt() {
		return this.debtReceivableAmt;
	}
	
	/**
	 * @param otherDebtReceivableAmt
	 */
	public void setOtherDebtReceivableAmt(java.math.BigDecimal otherDebtReceivableAmt) {
		this.otherDebtReceivableAmt = otherDebtReceivableAmt;
	}
	
    /**
     * @return otherDebtReceivableAmt
     */
	public java.math.BigDecimal getOtherDebtReceivableAmt() {
		return this.otherDebtReceivableAmt;
	}
	
	/**
	 * @param prepaymentsAmt
	 */
	public void setPrepaymentsAmt(java.math.BigDecimal prepaymentsAmt) {
		this.prepaymentsAmt = prepaymentsAmt;
	}
	
    /**
     * @return prepaymentsAmt
     */
	public java.math.BigDecimal getPrepaymentsAmt() {
		return this.prepaymentsAmt;
	}
	
	/**
	 * @param stockAmt
	 */
	public void setStockAmt(java.math.BigDecimal stockAmt) {
		this.stockAmt = stockAmt;
	}
	
    /**
     * @return stockAmt
     */
	public java.math.BigDecimal getStockAmt() {
		return this.stockAmt;
	}
	
	/**
	 * @param fixedAssetsAmt
	 */
	public void setFixedAssetsAmt(java.math.BigDecimal fixedAssetsAmt) {
		this.fixedAssetsAmt = fixedAssetsAmt;
	}
	
    /**
     * @return fixedAssetsAmt
     */
	public java.math.BigDecimal getFixedAssetsAmt() {
		return this.fixedAssetsAmt;
	}
	
	/**
	 * @param intangibleAssetsAmt
	 */
	public void setIntangibleAssetsAmt(java.math.BigDecimal intangibleAssetsAmt) {
		this.intangibleAssetsAmt = intangibleAssetsAmt;
	}
	
    /**
     * @return intangibleAssetsAmt
     */
	public java.math.BigDecimal getIntangibleAssetsAmt() {
		return this.intangibleAssetsAmt;
	}
	
	/**
	 * @param otherAssetsAmt
	 */
	public void setOtherAssetsAmt(java.math.BigDecimal otherAssetsAmt) {
		this.otherAssetsAmt = otherAssetsAmt;
	}
	
    /**
     * @return otherAssetsAmt
     */
	public java.math.BigDecimal getOtherAssetsAmt() {
		return this.otherAssetsAmt;
	}
	
	/**
	 * @param currentLiabilitiesAmt
	 */
	public void setCurrentLiabilitiesAmt(java.math.BigDecimal currentLiabilitiesAmt) {
		this.currentLiabilitiesAmt = currentLiabilitiesAmt;
	}
	
    /**
     * @return currentLiabilitiesAmt
     */
	public java.math.BigDecimal getCurrentLiabilitiesAmt() {
		return this.currentLiabilitiesAmt;
	}
	
	/**
	 * @param shortLoanAmt
	 */
	public void setShortLoanAmt(java.math.BigDecimal shortLoanAmt) {
		this.shortLoanAmt = shortLoanAmt;
	}
	
    /**
     * @return shortLoanAmt
     */
	public java.math.BigDecimal getShortLoanAmt() {
		return this.shortLoanAmt;
	}
	
	/**
	 * @param billPayableAmt
	 */
	public void setBillPayableAmt(java.math.BigDecimal billPayableAmt) {
		this.billPayableAmt = billPayableAmt;
	}
	
    /**
     * @return billPayableAmt
     */
	public java.math.BigDecimal getBillPayableAmt() {
		return this.billPayableAmt;
	}
	
	/**
	 * @param accountsPayableAmt
	 */
	public void setAccountsPayableAmt(java.math.BigDecimal accountsPayableAmt) {
		this.accountsPayableAmt = accountsPayableAmt;
	}
	
    /**
     * @return accountsPayableAmt
     */
	public java.math.BigDecimal getAccountsPayableAmt() {
		return this.accountsPayableAmt;
	}
	
	/**
	 * @param depositReceivedAmt
	 */
	public void setDepositReceivedAmt(java.math.BigDecimal depositReceivedAmt) {
		this.depositReceivedAmt = depositReceivedAmt;
	}
	
    /**
     * @return depositReceivedAmt
     */
	public java.math.BigDecimal getDepositReceivedAmt() {
		return this.depositReceivedAmt;
	}
	
	/**
	 * @param otherPayableAmt
	 */
	public void setOtherPayableAmt(java.math.BigDecimal otherPayableAmt) {
		this.otherPayableAmt = otherPayableAmt;
	}
	
    /**
     * @return otherPayableAmt
     */
	public java.math.BigDecimal getOtherPayableAmt() {
		return this.otherPayableAmt;
	}
	
	/**
	 * @param longLiabilitiesAmt
	 */
	public void setLongLiabilitiesAmt(java.math.BigDecimal longLiabilitiesAmt) {
		this.longLiabilitiesAmt = longLiabilitiesAmt;
	}
	
    /**
     * @return longLiabilitiesAmt
     */
	public java.math.BigDecimal getLongLiabilitiesAmt() {
		return this.longLiabilitiesAmt;
	}
	
	/**
	 * @param totalLiabilitiesAmt
	 */
	public void setTotalLiabilitiesAmt(java.math.BigDecimal totalLiabilitiesAmt) {
		this.totalLiabilitiesAmt = totalLiabilitiesAmt;
	}
	
    /**
     * @return totalLiabilitiesAmt
     */
	public java.math.BigDecimal getTotalLiabilitiesAmt() {
		return this.totalLiabilitiesAmt;
	}
	
	/**
	 * @param ownerEquityAmt
	 */
	public void setOwnerEquityAmt(java.math.BigDecimal ownerEquityAmt) {
		this.ownerEquityAmt = ownerEquityAmt;
	}
	
    /**
     * @return ownerEquityAmt
     */
	public java.math.BigDecimal getOwnerEquityAmt() {
		return this.ownerEquityAmt;
	}
	
	/**
	 * @param curfundAssetPerc
	 */
	public void setCurfundAssetPerc(java.math.BigDecimal curfundAssetPerc) {
		this.curfundAssetPerc = curfundAssetPerc;
	}
	
    /**
     * @return curfundAssetPerc
     */
	public java.math.BigDecimal getCurfundAssetPerc() {
		return this.curfundAssetPerc;
	}
	
	/**
	 * @param curfundPaymentPerc
	 */
	public void setCurfundPaymentPerc(java.math.BigDecimal curfundPaymentPerc) {
		this.curfundPaymentPerc = curfundPaymentPerc;
	}
	
    /**
     * @return curfundPaymentPerc
     */
	public java.math.BigDecimal getCurfundPaymentPerc() {
		return this.curfundPaymentPerc;
	}
	
	/**
	 * @param debtReceivablePerc
	 */
	public void setDebtReceivablePerc(java.math.BigDecimal debtReceivablePerc) {
		this.debtReceivablePerc = debtReceivablePerc;
	}
	
    /**
     * @return debtReceivablePerc
     */
	public java.math.BigDecimal getDebtReceivablePerc() {
		return this.debtReceivablePerc;
	}
	
	/**
	 * @param otherDebtReceivablePerc
	 */
	public void setOtherDebtReceivablePerc(java.math.BigDecimal otherDebtReceivablePerc) {
		this.otherDebtReceivablePerc = otherDebtReceivablePerc;
	}
	
    /**
     * @return otherDebtReceivablePerc
     */
	public java.math.BigDecimal getOtherDebtReceivablePerc() {
		return this.otherDebtReceivablePerc;
	}
	
	/**
	 * @param prepaymentsPerc
	 */
	public void setPrepaymentsPerc(java.math.BigDecimal prepaymentsPerc) {
		this.prepaymentsPerc = prepaymentsPerc;
	}
	
    /**
     * @return prepaymentsPerc
     */
	public java.math.BigDecimal getPrepaymentsPerc() {
		return this.prepaymentsPerc;
	}
	
	/**
	 * @param stockPerc
	 */
	public void setStockPerc(java.math.BigDecimal stockPerc) {
		this.stockPerc = stockPerc;
	}
	
    /**
     * @return stockPerc
     */
	public java.math.BigDecimal getStockPerc() {
		return this.stockPerc;
	}
	
	/**
	 * @param fixedAssetsPerc
	 */
	public void setFixedAssetsPerc(java.math.BigDecimal fixedAssetsPerc) {
		this.fixedAssetsPerc = fixedAssetsPerc;
	}
	
    /**
     * @return fixedAssetsPerc
     */
	public java.math.BigDecimal getFixedAssetsPerc() {
		return this.fixedAssetsPerc;
	}
	
	/**
	 * @param intangibleAssetsPerc
	 */
	public void setIntangibleAssetsPerc(java.math.BigDecimal intangibleAssetsPerc) {
		this.intangibleAssetsPerc = intangibleAssetsPerc;
	}
	
    /**
     * @return intangibleAssetsPerc
     */
	public java.math.BigDecimal getIntangibleAssetsPerc() {
		return this.intangibleAssetsPerc;
	}
	
	/**
	 * @param otherAssetsPerc
	 */
	public void setOtherAssetsPerc(java.math.BigDecimal otherAssetsPerc) {
		this.otherAssetsPerc = otherAssetsPerc;
	}
	
    /**
     * @return otherAssetsPerc
     */
	public java.math.BigDecimal getOtherAssetsPerc() {
		return this.otherAssetsPerc;
	}
	
	/**
	 * @param currentLiabilitiesPerc
	 */
	public void setCurrentLiabilitiesPerc(java.math.BigDecimal currentLiabilitiesPerc) {
		this.currentLiabilitiesPerc = currentLiabilitiesPerc;
	}
	
    /**
     * @return currentLiabilitiesPerc
     */
	public java.math.BigDecimal getCurrentLiabilitiesPerc() {
		return this.currentLiabilitiesPerc;
	}
	
	/**
	 * @param shortLoanPerc
	 */
	public void setShortLoanPerc(java.math.BigDecimal shortLoanPerc) {
		this.shortLoanPerc = shortLoanPerc;
	}
	
    /**
     * @return shortLoanPerc
     */
	public java.math.BigDecimal getShortLoanPerc() {
		return this.shortLoanPerc;
	}
	
	/**
	 * @param billPayablePerc
	 */
	public void setBillPayablePerc(java.math.BigDecimal billPayablePerc) {
		this.billPayablePerc = billPayablePerc;
	}
	
    /**
     * @return billPayablePerc
     */
	public java.math.BigDecimal getBillPayablePerc() {
		return this.billPayablePerc;
	}
	
	/**
	 * @param accountsPayablePerc
	 */
	public void setAccountsPayablePerc(java.math.BigDecimal accountsPayablePerc) {
		this.accountsPayablePerc = accountsPayablePerc;
	}
	
    /**
     * @return accountsPayablePerc
     */
	public java.math.BigDecimal getAccountsPayablePerc() {
		return this.accountsPayablePerc;
	}
	
	/**
	 * @param depositReceivedPerc
	 */
	public void setDepositReceivedPerc(java.math.BigDecimal depositReceivedPerc) {
		this.depositReceivedPerc = depositReceivedPerc;
	}
	
    /**
     * @return depositReceivedPerc
     */
	public java.math.BigDecimal getDepositReceivedPerc() {
		return this.depositReceivedPerc;
	}
	
	/**
	 * @param otherPayablePerc
	 */
	public void setOtherPayablePerc(java.math.BigDecimal otherPayablePerc) {
		this.otherPayablePerc = otherPayablePerc;
	}
	
    /**
     * @return otherPayablePerc
     */
	public java.math.BigDecimal getOtherPayablePerc() {
		return this.otherPayablePerc;
	}
	
	/**
	 * @param longLiabilitiesPerc
	 */
	public void setLongLiabilitiesPerc(java.math.BigDecimal longLiabilitiesPerc) {
		this.longLiabilitiesPerc = longLiabilitiesPerc;
	}
	
    /**
     * @return longLiabilitiesPerc
     */
	public java.math.BigDecimal getLongLiabilitiesPerc() {
		return this.longLiabilitiesPerc;
	}
	
	/**
	 * @param totalLiabilitiesPerc
	 */
	public void setTotalLiabilitiesPerc(java.math.BigDecimal totalLiabilitiesPerc) {
		this.totalLiabilitiesPerc = totalLiabilitiesPerc;
	}
	
    /**
     * @return totalLiabilitiesPerc
     */
	public java.math.BigDecimal getTotalLiabilitiesPerc() {
		return this.totalLiabilitiesPerc;
	}
	
	/**
	 * @param ownerEquityPerc
	 */
	public void setOwnerEquityPerc(java.math.BigDecimal ownerEquityPerc) {
		this.ownerEquityPerc = ownerEquityPerc;
	}
	
    /**
     * @return ownerEquityPerc
     */
	public java.math.BigDecimal getOwnerEquityPerc() {
		return this.ownerEquityPerc;
	}
	
	/**
	 * @param curfundAssetDesc
	 */
	public void setCurfundAssetDesc(String curfundAssetDesc) {
		this.curfundAssetDesc = curfundAssetDesc;
	}
	
    /**
     * @return curfundAssetDesc
     */
	public String getCurfundAssetDesc() {
		return this.curfundAssetDesc;
	}
	
	/**
	 * @param curfundPaymentDesc
	 */
	public void setCurfundPaymentDesc(String curfundPaymentDesc) {
		this.curfundPaymentDesc = curfundPaymentDesc;
	}
	
    /**
     * @return curfundPaymentDesc
     */
	public String getCurfundPaymentDesc() {
		return this.curfundPaymentDesc;
	}
	
	/**
	 * @param debtReceivableDesc
	 */
	public void setDebtReceivableDesc(String debtReceivableDesc) {
		this.debtReceivableDesc = debtReceivableDesc;
	}
	
    /**
     * @return debtReceivableDesc
     */
	public String getDebtReceivableDesc() {
		return this.debtReceivableDesc;
	}
	
	/**
	 * @param otherDebtReceivableDesc
	 */
	public void setOtherDebtReceivableDesc(String otherDebtReceivableDesc) {
		this.otherDebtReceivableDesc = otherDebtReceivableDesc;
	}
	
    /**
     * @return otherDebtReceivableDesc
     */
	public String getOtherDebtReceivableDesc() {
		return this.otherDebtReceivableDesc;
	}
	
	/**
	 * @param prepaymentsDesc
	 */
	public void setPrepaymentsDesc(String prepaymentsDesc) {
		this.prepaymentsDesc = prepaymentsDesc;
	}
	
    /**
     * @return prepaymentsDesc
     */
	public String getPrepaymentsDesc() {
		return this.prepaymentsDesc;
	}
	
	/**
	 * @param stockDesc
	 */
	public void setStockDesc(String stockDesc) {
		this.stockDesc = stockDesc;
	}
	
    /**
     * @return stockDesc
     */
	public String getStockDesc() {
		return this.stockDesc;
	}
	
	/**
	 * @param fixedAssetsDesc
	 */
	public void setFixedAssetsDesc(String fixedAssetsDesc) {
		this.fixedAssetsDesc = fixedAssetsDesc;
	}
	
    /**
     * @return fixedAssetsDesc
     */
	public String getFixedAssetsDesc() {
		return this.fixedAssetsDesc;
	}
	
	/**
	 * @param intangibleAssetsDesc
	 */
	public void setIntangibleAssetsDesc(String intangibleAssetsDesc) {
		this.intangibleAssetsDesc = intangibleAssetsDesc;
	}
	
    /**
     * @return intangibleAssetsDesc
     */
	public String getIntangibleAssetsDesc() {
		return this.intangibleAssetsDesc;
	}
	
	/**
	 * @param otherAssetsDesc
	 */
	public void setOtherAssetsDesc(String otherAssetsDesc) {
		this.otherAssetsDesc = otherAssetsDesc;
	}
	
    /**
     * @return otherAssetsDesc
     */
	public String getOtherAssetsDesc() {
		return this.otherAssetsDesc;
	}
	
	/**
	 * @param currentLiabilitiesDesc
	 */
	public void setCurrentLiabilitiesDesc(String currentLiabilitiesDesc) {
		this.currentLiabilitiesDesc = currentLiabilitiesDesc;
	}
	
    /**
     * @return currentLiabilitiesDesc
     */
	public String getCurrentLiabilitiesDesc() {
		return this.currentLiabilitiesDesc;
	}
	
	/**
	 * @param shortLoanDesc
	 */
	public void setShortLoanDesc(String shortLoanDesc) {
		this.shortLoanDesc = shortLoanDesc;
	}
	
    /**
     * @return shortLoanDesc
     */
	public String getShortLoanDesc() {
		return this.shortLoanDesc;
	}
	
	/**
	 * @param billPayableDesc
	 */
	public void setBillPayableDesc(String billPayableDesc) {
		this.billPayableDesc = billPayableDesc;
	}
	
    /**
     * @return billPayableDesc
     */
	public String getBillPayableDesc() {
		return this.billPayableDesc;
	}
	
	/**
	 * @param accountsPayableDesc
	 */
	public void setAccountsPayableDesc(String accountsPayableDesc) {
		this.accountsPayableDesc = accountsPayableDesc;
	}
	
    /**
     * @return accountsPayableDesc
     */
	public String getAccountsPayableDesc() {
		return this.accountsPayableDesc;
	}
	
	/**
	 * @param depositReceivedDesc
	 */
	public void setDepositReceivedDesc(String depositReceivedDesc) {
		this.depositReceivedDesc = depositReceivedDesc;
	}
	
    /**
     * @return depositReceivedDesc
     */
	public String getDepositReceivedDesc() {
		return this.depositReceivedDesc;
	}
	
	/**
	 * @param otherPayableDesc
	 */
	public void setOtherPayableDesc(String otherPayableDesc) {
		this.otherPayableDesc = otherPayableDesc;
	}
	
    /**
     * @return otherPayableDesc
     */
	public String getOtherPayableDesc() {
		return this.otherPayableDesc;
	}
	
	/**
	 * @param longLiabilitiesDesc
	 */
	public void setLongLiabilitiesDesc(String longLiabilitiesDesc) {
		this.longLiabilitiesDesc = longLiabilitiesDesc;
	}
	
    /**
     * @return longLiabilitiesDesc
     */
	public String getLongLiabilitiesDesc() {
		return this.longLiabilitiesDesc;
	}
	
	/**
	 * @param totalLiabilitiesDesc
	 */
	public void setTotalLiabilitiesDesc(String totalLiabilitiesDesc) {
		this.totalLiabilitiesDesc = totalLiabilitiesDesc;
	}
	
    /**
     * @return totalLiabilitiesDesc
     */
	public String getTotalLiabilitiesDesc() {
		return this.totalLiabilitiesDesc;
	}
	
	/**
	 * @param ownerEquityDesc
	 */
	public void setOwnerEquityDesc(String ownerEquityDesc) {
		this.ownerEquityDesc = ownerEquityDesc;
	}
	
    /**
     * @return ownerEquityDesc
     */
	public String getOwnerEquityDesc() {
		return this.ownerEquityDesc;
	}
	
	/**
	 * @param assetDebtRateDesc
	 */
	public void setAssetDebtRateDesc(String assetDebtRateDesc) {
		this.assetDebtRateDesc = assetDebtRateDesc;
	}
	
    /**
     * @return assetDebtRateDesc
     */
	public String getAssetDebtRateDesc() {
		return this.assetDebtRateDesc;
	}
	
	/**
	 * @param otherBankCptlSitu
	 */
	public void setOtherBankCptlSitu(String otherBankCptlSitu) {
		this.otherBankCptlSitu = otherBankCptlSitu;
	}
	
    /**
     * @return otherBankCptlSitu
     */
	public String getOtherBankCptlSitu() {
		return this.otherBankCptlSitu;
	}
	
	/**
	 * @param selfBankCptlSitu
	 */
	public void setSelfBankCptlSitu(String selfBankCptlSitu) {
		this.selfBankCptlSitu = selfBankCptlSitu;
	}
	
    /**
     * @return selfBankCptlSitu
     */
	public String getSelfBankCptlSitu() {
		return this.selfBankCptlSitu;
	}
	
	/**
	 * @param operatingNetCptlSitu
	 */
	public void setOperatingNetCptlSitu(String operatingNetCptlSitu) {
		this.operatingNetCptlSitu = operatingNetCptlSitu;
	}
	
    /**
     * @return operatingNetCptlSitu
     */
	public String getOperatingNetCptlSitu() {
		return this.operatingNetCptlSitu;
	}
	
	/**
	 * @param nrmlOutguarSituDesc
	 */
	public void setNrmlOutguarSituDesc(String nrmlOutguarSituDesc) {
		this.nrmlOutguarSituDesc = nrmlOutguarSituDesc;
	}
	
    /**
     * @return nrmlOutguarSituDesc
     */
	public String getNrmlOutguarSituDesc() {
		return this.nrmlOutguarSituDesc;
	}
	
	/**
	 * @param attOutguarSituDesc
	 */
	public void setAttOutguarSituDesc(String attOutguarSituDesc) {
		this.attOutguarSituDesc = attOutguarSituDesc;
	}
	
    /**
     * @return attOutguarSituDesc
     */
	public String getAttOutguarSituDesc() {
		return this.attOutguarSituDesc;
	}
	
	/**
	 * @param badOutguarSituDesc
	 */
	public void setBadOutguarSituDesc(String badOutguarSituDesc) {
		this.badOutguarSituDesc = badOutguarSituDesc;
	}
	
    /**
     * @return badOutguarSituDesc
     */
	public String getBadOutguarSituDesc() {
		return this.badOutguarSituDesc;
	}
	
	/**
	 * @param guarRealOperCdtExpl
	 */
	public void setGuarRealOperCdtExpl(String guarRealOperCdtExpl) {
		this.guarRealOperCdtExpl = guarRealOperCdtExpl;
	}
	
    /**
     * @return guarRealOperCdtExpl
     */
	public String getGuarRealOperCdtExpl() {
		return this.guarRealOperCdtExpl;
	}
	
	/**
	 * @param rentLandSqu
	 */
	public void setRentLandSqu(java.math.BigDecimal rentLandSqu) {
		this.rentLandSqu = rentLandSqu;
	}
	
    /**
     * @return rentLandSqu
     */
	public java.math.BigDecimal getRentLandSqu() {
		return this.rentLandSqu;
	}
	
	/**
	 * @param rentWorkshopSqu
	 */
	public void setRentWorkshopSqu(java.math.BigDecimal rentWorkshopSqu) {
		this.rentWorkshopSqu = rentWorkshopSqu;
	}
	
    /**
     * @return rentWorkshopSqu
     */
	public java.math.BigDecimal getRentWorkshopSqu() {
		return this.rentWorkshopSqu;
	}
	
	/**
	 * @param selfLandSqu
	 */
	public void setSelfLandSqu(java.math.BigDecimal selfLandSqu) {
		this.selfLandSqu = selfLandSqu;
	}
	
    /**
     * @return selfLandSqu
     */
	public java.math.BigDecimal getSelfLandSqu() {
		return this.selfLandSqu;
	}
	
	/**
	 * @param selfCertLandSqu
	 */
	public void setSelfCertLandSqu(java.math.BigDecimal selfCertLandSqu) {
		this.selfCertLandSqu = selfCertLandSqu;
	}
	
    /**
     * @return selfCertLandSqu
     */
	public java.math.BigDecimal getSelfCertLandSqu() {
		return this.selfCertLandSqu;
	}
	
	/**
	 * @param selfCertLandCha
	 */
	public void setSelfCertLandCha(String selfCertLandCha) {
		this.selfCertLandCha = selfCertLandCha;
	}
	
    /**
     * @return selfCertLandCha
     */
	public String getSelfCertLandCha() {
		return this.selfCertLandCha;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(java.math.BigDecimal houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return houseSqu
     */
	public java.math.BigDecimal getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param certHouseSqu
	 */
	public void setCertHouseSqu(java.math.BigDecimal certHouseSqu) {
		this.certHouseSqu = certHouseSqu;
	}
	
    /**
     * @return certHouseSqu
     */
	public java.math.BigDecimal getCertHouseSqu() {
		return this.certHouseSqu;
	}
	
	/**
	 * @param guarSitu
	 */
	public void setGuarSitu(String guarSitu) {
		this.guarSitu = guarSitu;
	}
	
    /**
     * @return guarSitu
     */
	public String getGuarSitu() {
		return this.guarSitu;
	}
	
	/**
	 * @param guarAblEval
	 */
	public void setGuarAblEval(String guarAblEval) {
		this.guarAblEval = guarAblEval;
	}
	
    /**
     * @return guarAblEval
     */
	public String getGuarAblEval() {
		return this.guarAblEval;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}


}