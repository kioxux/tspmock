/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtGuareInfo;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.apache.commons.lang.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtInspectInfo;
import cn.com.yusys.yusp.service.LmtInspectInfoService;
import tk.mybatis.mapper.util.StringUtil;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtInspectInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "勘验信息")
@RequestMapping("/api/lmtinspectinfo")
public class LmtInspectInfoResource {
    @Autowired
    private LmtInspectInfoService lmtInspectInfoService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<LmtInspectInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtInspectInfo> list = lmtInspectInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtInspectInfo>> index(QueryModel queryModel) {
        List<LmtInspectInfo> list = lmtInspectInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{inspectSerno}")
    protected ResultDto<LmtInspectInfo> show(@PathVariable("inspectSerno") String inspectSerno) {
        LmtInspectInfo lmtInspectInfo = lmtInspectInfoService.selectByPrimaryKey(inspectSerno);
        return new ResultDto<LmtInspectInfo>(lmtInspectInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtInspectInfo> create(@RequestBody LmtInspectInfo lmtInspectInfo) throws URISyntaxException {
        lmtInspectInfoService.insert(lmtInspectInfo);
        return new ResultDto<LmtInspectInfo>(lmtInspectInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtInspectInfo lmtInspectInfo) throws URISyntaxException {
        int result = lmtInspectInfoService.update(lmtInspectInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{inspectSerno}")
    protected ResultDto<Integer> delete(@PathVariable("inspectSerno") String inspectSerno) {
        int result = lmtInspectInfoService.deleteByPrimaryKey(inspectSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtInspectInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/18 14:45
     * @注释 条件查询
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtInspectInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtInspectInfo> list = lmtInspectInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }


    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.LmtInspectInfo>>
     * @author hubp
     * @date 2021/10/12 16:21
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbymodelxw")
    protected ResultDto<List<LmtInspectInfo>> selectbymodelXw(@RequestBody QueryModel queryModel) {
        List<LmtInspectInfo> list = lmtInspectInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/18 14:47
     * @注释 详情页查询
     */
    @PostMapping("/selectbyid")
    protected ResultDto<LmtInspectInfo> selectbyid(@RequestBody LmtInspectInfo lmtInspectInfo) {
        LmtInspectInfo data = lmtInspectInfoService.selectByPrimaryKey(lmtInspectInfo.getInspectSerno());
        return new ResultDto<LmtInspectInfo>(data);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/18 14:48
     * @注释 保存操作
     */
    @PostMapping("/saveandupdate")
    protected ResultDto saveandupdate(@RequestBody LmtInspectInfo lmtInspectInfo) {
        int i;
        if (StringUtil.isNotEmpty(lmtInspectInfo.getInspectSerno())){
            i  = lmtInspectInfoService.update(lmtInspectInfo);
        }else {
//           String surveyNo = sequenceTemplateClient.getSequenceTemplate("LMT_XD_SERNO", new HashMap<>());
//            lmtInspectInfo.setSurveySerno(surveyNo);
            lmtInspectInfo.setInspectSerno(null);
            i=lmtInspectInfoService.insert(lmtInspectInfo);
        }
        return new ResultDto(i);
    }

 	 /**
     * @author hubp
     * @param lmtGuareInfo
     * @return ResultDto<List<LmtInspectInfo>>
     * @desc 根据流水号查询关联勘验信息
     */
    @ApiOperation("根据流水号查询关联勘验信息")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<List<LmtInspectInfo>> selectBySurveySerno(@RequestBody LmtGuareInfo lmtGuareInfo) {
        String surveySerno = lmtGuareInfo.getSurveySerno();
        List<LmtInspectInfo> list = lmtInspectInfoService.selectBySurveySerno(surveySerno);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }

    /**
     * @author hubp
     * @param queryModel
     * @return ResultDto<List<LmtInspectInfo>>
     * @desc 根据登入人ID查询关联勘验信息
     */
    @ApiOperation("根据登入人ID查询关联勘验信息")
    @PostMapping("/selectbyloginid")
    protected ResultDto<List<LmtInspectInfo>> selectByLoginId(@RequestBody QueryModel queryModel) {
        List<LmtInspectInfo> list = lmtInspectInfoService.selectByLoginId(queryModel);
        return new ResultDto<List<LmtInspectInfo>>(list);
    }
}