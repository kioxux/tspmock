package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutRel
 * @类描述: iqp_certi_inout_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 10:21:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCertiInoutRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 押品出入库申请流水号 **/
	private String serno;
	
	/** 担保合同流水号  **/
	private String guarPkId;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 权证主键 **/
	private String certiPkId;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId == null ? null : guarPkId.trim();
	}
	
    /**
     * @return GuarPkId
     */	
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param certiPkId
	 */
	public void setCertiPkId(String certiPkId) {
		this.certiPkId = certiPkId == null ? null : certiPkId.trim();
	}
	
    /**
     * @return CertiPkId
     */	
	public String getCertiPkId() {
		return this.certiPkId;
	}
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}
}