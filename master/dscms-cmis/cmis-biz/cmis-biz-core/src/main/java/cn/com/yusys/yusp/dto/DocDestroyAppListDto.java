package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyAppList
 * @类描述: doc_destroy_app_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-26 10:11:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocDestroyAppListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 档案销毁流水号 **/
	private String ddalSerno;
	
	/** 销毁户数 **/
	private String destroyCus;
	
	/** 销毁状态 **/
	private String destroyStatus;
	
	/** 销毁日期 **/
	private String destroyDate;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 销毁类型 **/
	private String destroyType;
	
	
	/**
	 * @param ddalSerno
	 */
	public void setDdalSerno(String ddalSerno) {
		this.ddalSerno = ddalSerno == null ? null : ddalSerno.trim();
	}
	
    /**
     * @return DdalSerno
     */	
	public String getDdalSerno() {
		return this.ddalSerno;
	}
	
	/**
	 * @param destroyCus
	 */
	public void setDestroyCus(String destroyCus) {
		this.destroyCus = destroyCus == null ? null : destroyCus.trim();
	}
	
    /**
     * @return DestroyCus
     */	
	public String getDestroyCus() {
		return this.destroyCus;
	}
	
	/**
	 * @param destroyStatus
	 */
	public void setDestroyStatus(String destroyStatus) {
		this.destroyStatus = destroyStatus == null ? null : destroyStatus.trim();
	}
	
    /**
     * @return DestroyStatus
     */	
	public String getDestroyStatus() {
		return this.destroyStatus;
	}
	
	/**
	 * @param destroyDate
	 */
	public void setDestroyDate(String destroyDate) {
		this.destroyDate = destroyDate == null ? null : destroyDate.trim();
	}
	
    /**
     * @return DestroyDate
     */	
	public String getDestroyDate() {
		return this.destroyDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param destroyType
	 */
	public void setDestroyType(String destroyType) {
		this.destroyType = destroyType == null ? null : destroyType.trim();
	}
	
    /**
     * @return DestroyType
     */	
	public String getDestroyType() {
		return this.destroyType;
	}


}