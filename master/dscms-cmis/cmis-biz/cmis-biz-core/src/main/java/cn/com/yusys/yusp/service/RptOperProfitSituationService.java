/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.MapUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperProfitSituation;
import cn.com.yusys.yusp.repository.mapper.RptOperProfitSituationMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperProfitSituationService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:48:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperProfitSituationService {

    @Autowired
    private RptOperProfitSituationMapper rptOperProfitSituationMapper;

    @Autowired
    private ICusClientService iCusClientService;
    private static final Logger logger = LoggerFactory.getLogger(RptOperProfitSituationService.class);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptOperProfitSituation selectByPrimaryKey(String serno) {
        return rptOperProfitSituationMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptOperProfitSituation> selectAll(QueryModel model) {
        List<RptOperProfitSituation> records = (List<RptOperProfitSituation>) rptOperProfitSituationMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptOperProfitSituation> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperProfitSituation> list = rptOperProfitSituationMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptOperProfitSituation record) {
        return rptOperProfitSituationMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptOperProfitSituation record) {
        return rptOperProfitSituationMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptOperProfitSituation record) {
        return rptOperProfitSituationMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptOperProfitSituation record) {
        return rptOperProfitSituationMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptOperProfitSituationMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperProfitSituationMapper.deleteByIds(ids);
    }

    /**
     * 初始化方法
     *
     * @param map
     * @return
     */
    public int initProFitSituation(Map map) {
        RptOperProfitSituation rptOperProfitSituation = new RptOperProfitSituation();
        String serno = map.get("serno").toString();
        String cusId = map.get("cusId").toString();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        rptOperProfitSituation.setSerno(serno);
        ResultDto<List<FinanIndicAnalyDto>> rptFncTotalProfit = iCusClientService.getRptFncTotalProfit(map);
        if (rptFncTotalProfit != null && rptFncTotalProfit.getData() != null) {
            List<FinanIndicAnalyDto> data = rptFncTotalProfit.getData();
            for (FinanIndicAnalyDto finanIndicAnalyDto : data) {
                if ("主营业务利润".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondMainBusiProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstMainBusiProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrMainBusiProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setMainBusiProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setMainBusiProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("毛利率".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrGrossProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setGrossProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setGrossProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("营业费用".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondBusinessExpenses(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstBusinessExpenses(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrBusinessExpenses(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setBusinessExpensesDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setBusinessExpensesLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("管理费用".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondManagementExpenses(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstManagementExpenses(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrManagementExpenses(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setManagementExpensesDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setManagementExpensesLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("财务费用".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondFinancialExpenses(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstFinancialExpenses(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrFinancialExpenses(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setFinancialExpensesDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setFinancialExpensesLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("营业利润".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondOperaExpenses(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstOperaExpenses(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrOperaExpenses(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setOperaExpensesDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setOperaExpensesLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("投资收益".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondInvestIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstInvestIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrInvestIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setInvestIncomeDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setInvestIncomeLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                    if ("利润总额".equals(finanIndicAnalyDto.getItemName())) {
                        rptOperProfitSituation.setNearSecondTotalProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptOperProfitSituation.setNearFirstTotalProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptOperProfitSituation.setCurrTotalProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                        BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                        rptOperProfitSituation.setTotalProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                        rptOperProfitSituation.setTotalProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                        rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                }else if(CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                    if ("年度利润".equals(finanIndicAnalyDto.getItemName())) {
                        rptOperProfitSituation.setNearSecondTotalProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        rptOperProfitSituation.setNearFirstTotalProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        rptOperProfitSituation.setCurrTotalProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                        BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                        BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                        rptOperProfitSituation.setTotalProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                        rptOperProfitSituation.setTotalProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                        rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                        continue;
                    }
                }

                if ("所得税".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondIncomeTax(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstIncomeTax(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrIncomeTax(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setIncomeTaxDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setIncomeTaxLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                if ("实际净利润".equals(finanIndicAnalyDto.getItemName())) {
                    rptOperProfitSituation.setNearSecondRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                    rptOperProfitSituation.setNearFirstRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setCurrRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                    BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearSecondValue());
                    BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue())?BigDecimal.ZERO:finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue())?BigDecimal.ZERO:finanIndicAnalyDto.getNearFirstValue());
                    rptOperProfitSituation.setRealGrossProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                    rptOperProfitSituation.setRealGrossProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                    rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                    continue;
                }
                // if ("净利率".equals(finanIndicAnalyDto.getItemName())) {
                //  rptOperProfitSituation.setNearSecondRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                //  rptOperProfitSituation.setNearFirstRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                //  rptOperProfitSituation.setCurrRealGrossProfit(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                //  BigDecimal lastdiff = finanIndicAnalyDto.getNearFirstValue().subtract(finanIndicAnalyDto.getNearSecondValue());
                //  BigDecimal diff = finanIndicAnalyDto.getCurYmValue().subtract(finanIndicAnalyDto.getNearFirstValue());
                //  rptOperProfitSituation.setRealGrossProfitDiff(Objects.isNull(lastdiff) ? BigDecimal.ZERO : lastdiff);
                //  rptOperProfitSituation.setRealGrossProfitLastYearDiff(Objects.isNull(diff) ? BigDecimal.ZERO : diff);
                //  rptOperProfitSituation.setInputYear(finanIndicAnalyDto.getInputYear());
                //  continue;
                // }
            }
        }
        //三大费用小计
        logger.info("经营利润情况-获取最近第二年三大费用小计:", JSON.toJSONString(rptOperProfitSituation.getNearSecondBusinessExpenses()));
        BigDecimal nearSecondBusinessExpenses = Objects.nonNull(rptOperProfitSituation.getNearSecondBusinessExpenses())?rptOperProfitSituation.getNearSecondBusinessExpenses():BigDecimal.ZERO;
        BigDecimal nearSecondManagementExpenses = Objects.nonNull(rptOperProfitSituation.getNearSecondManagementExpenses())?rptOperProfitSituation.getNearSecondManagementExpenses():BigDecimal.ZERO;
        BigDecimal nearSecondFinancialExpenses = Objects.nonNull(rptOperProfitSituation.getNearSecondFinancialExpenses())?rptOperProfitSituation.getNearSecondFinancialExpenses():BigDecimal.ZERO;
        BigDecimal nearSecondThreeExpensesSum = nearSecondBusinessExpenses.add(nearSecondManagementExpenses).add(nearSecondFinancialExpenses);
        BigDecimal nearFirstBusinessExpenses = Objects.nonNull(rptOperProfitSituation.getNearFirstBusinessExpenses())?rptOperProfitSituation.getNearFirstBusinessExpenses():BigDecimal.ZERO;
        BigDecimal nearFirstManagementExpenses = Objects.nonNull(rptOperProfitSituation.getNearFirstManagementExpenses())?rptOperProfitSituation.getNearFirstManagementExpenses():BigDecimal.ZERO;
        BigDecimal nearFirstFinancialExpenses = Objects.nonNull(rptOperProfitSituation.getNearFirstFinancialExpenses())?rptOperProfitSituation.getNearFirstFinancialExpenses():BigDecimal.ZERO;
        BigDecimal nearFirstThreeExpensesSum = nearFirstBusinessExpenses.add(nearFirstManagementExpenses).add(nearFirstFinancialExpenses);
        BigDecimal currBusinessExpenses = Objects.nonNull(rptOperProfitSituation.getCurrBusinessExpenses())?rptOperProfitSituation.getCurrBusinessExpenses():BigDecimal.ZERO;
        BigDecimal currManagementExpenses = Objects.nonNull(rptOperProfitSituation.getCurrManagementExpenses())?rptOperProfitSituation.getCurrManagementExpenses():BigDecimal.ZERO;
        BigDecimal currFinancialExpenses = Objects.nonNull(rptOperProfitSituation.getCurrFinancialExpenses())?rptOperProfitSituation.getCurrFinancialExpenses():BigDecimal.ZERO;
        BigDecimal currThreeExpensesSum =currBusinessExpenses.add(currManagementExpenses).add(currFinancialExpenses);
        rptOperProfitSituation.setNearSecondThreeExpensesSum(nearSecondThreeExpensesSum);
        rptOperProfitSituation.setNearFirstThreeExpensesSum(nearFirstThreeExpensesSum);
        rptOperProfitSituation.setCurrThreeExpensesSum(currThreeExpensesSum);
        rptOperProfitSituation.setThreeExpensesSumDiff(nearFirstThreeExpensesSum.subtract(nearSecondThreeExpensesSum));
        rptOperProfitSituation.setThreeExpensesSumLastYearDiff(currThreeExpensesSum.subtract(nearFirstThreeExpensesSum));
        int count = rptOperProfitSituationMapper.insert(rptOperProfitSituation);
        return count;
    }

}
