package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import cn.com.yusys.yusp.domain.IqpGuarChgAppRel;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import cn.com.yusys.yusp.service.GuarGuaranteeService;
import cn.com.yusys.yusp.service.IqpGuarChgAppRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author hxl
 * @version 1.0.0
 * @date 2021年8月4日
 * @desc 担保变更，对新主担保人的征信进行校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0074Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0074Service.class);
    @Autowired
    private  GuarGuaranteeService guarGuaranteeService;
//    @Autowired
//    private CusBaseService cusBaseService;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private IqpGuarChgAppRelService iqpGuarChgAppRelService;

    public RiskResultDto riskItem0074 (QueryModel queryModel) {

        RiskResultDto riskResultDto = new RiskResultDto();
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        String serno = queryModel.getCondition().get("bizId").toString();
        queryModel.addCondition("isUnderCont","0");
        queryModel.addCondition("serno",serno);
        List<IqpGuarChgAppRel> iqpGuarChgAppRelList = iqpGuarChgAppRelService.selectByModel(queryModel);
        for (int i = 0; i <iqpGuarChgAppRelList.size() ; i++) {
            String guar_way = iqpGuarChgAppRelList.get(i).getGuarWay();
            if("30".equals(guar_way)){
                String  guarContNo= iqpGuarChgAppRelList.get(i).getGuarContNo();
                log.info("*************担保变更，对新主担保人的征信进行校验***********【{}】", guarContNo);
                queryModel.addCondition("guarContNo",guarContNo);
                List<GuarGuarantee> guarGuarantees = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
                if(guarGuarantees.size()>0){
                    for (int j = 0; j < guarGuarantees.size(); j++) {
                        String  certCode = guarGuarantees.get(i).getAssureCertCode();//保证人证件号码
                        //queryModel.addCondition("certCode",certCode);
                        //List<CreditReportQryLst> creditReportQryLsts = creditReportQryLstService.selectByModel(queryModel);
                        Boolean selectdanbaozhengxin = creditReportQryLstService.selectdanbaozhengxin(certCode);
                        if(!selectdanbaozhengxin){//是否在征信表中有数据
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc("征信数据不存在");
                            return riskResultDto;
                        }
                    }
                }else{
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc("保证人不存在"); //
                    return riskResultDto;
                }

            }


        }
        return riskResultDto;


    }


}
