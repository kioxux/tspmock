package cn.com.yusys.yusp.web.server.xdxt0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxt0009.req.Xdxt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.resp.Xdxt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxt0009.Xdxt0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户经理是否为小微客户经理
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0009:客户经理是否为小微客户经理")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0009Resource.class);

    @Autowired
    private Xdxt0009Service xdxt0009Service;
    /**
     * 交易码：xdxt0009
     * 交易描述：客户经理是否为小微客户经理
     *
     * @param xdxt0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户经理是否为小微客户经理")
    @PostMapping("/xdxt0009")
    protected @ResponseBody
    ResultDto<Xdxt0009DataRespDto> xdxt0009(@Validated @RequestBody Xdxt0009DataReqDto xdxt0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataReqDto));
        Xdxt0009DataRespDto xdxt0009DataRespDto = new Xdxt0009DataRespDto();// 响应Dto:客户经理是否为小微客户经理
        ResultDto<Xdxt0009DataRespDto> xdxt0009DataResultDto = new ResultDto<>();
        String managerId = xdxt0009DataReqDto.getManagerId();//客户经理号
        try {
            // 从xdxt0009DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataReqDto));
            xdxt0009DataRespDto=xdxt0009Service.getXdxt0009(xdxt0009DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataRespDto));

            // 封装xdxt0009DataResultDto中正确的返回码和返回信息
            xdxt0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, e.getMessage());
            // 封装xdxt0009DataResultDto中异常返回码和返回信息
            xdxt0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0009DataRespDto到xdxt0009DataResultDto中
        xdxt0009DataResultDto.setData(xdxt0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataResultDto));
        return xdxt0009DataResultDto;
    }
}
