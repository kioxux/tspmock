package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoAppr
 * @类描述: lmt_sig_invest_basic_info_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 22:15:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestBasicInfoApprDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 审批流水号 **/
	private String approveSerno;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 入池基础资产标准 **/
	private String poolBasicAssetNormal;
	
	/** 初始起算日 **/
	private String firstStartDate;
	
	/** 贷款笔数 **/
	private String loanQnt;
	
	/** 借款人数量 **/
	private String debitQnt;
	
	/** 资产池总本金余额 **/
	private java.math.BigDecimal poolTotalAmt;
	
	/** 资产池合同金额 **/
	private java.math.BigDecimal poolContAmt;
	
	/** 单笔贷款平均本金余额 **/
	private java.math.BigDecimal sigLoanAvgAmt;
	
	/** 单个借款人平均本金余额 **/
	private java.math.BigDecimal sigDebitAvgAmt;
	
	/** 按金额及剩余期限加权的贷款加权平均利率 **/
	private java.math.BigDecimal loanWeightAvgRate;
	
	/** 最晚一笔到期贷款期限 **/
	private String lastSigLoanEndTerm;
	
	/** 前3大借款人未偿还本金余额占比 **/
	private java.math.BigDecimal lastThrUnrepayCapPerc;
	
	/** 前5大借款人未偿还本金余额占比 **/
	private java.math.BigDecimal lastFifUnrepayCapPerc;
	
	/** 是否穿透至底层资产 **/
	private String isPassBasicAsset;
	
	/** 是否全部穿透 **/
	private String isTotalPassBasic;
	
	/** 是否能识别底层资产中单笔资产金额未超过1级资本净额0.15%部分 **/
	private String isBasicAsset;
	
	/** 底层资产中单笔资产金额未超过1级资本净额0.15%部分的合计金额 **/
	private java.math.BigDecimal basicAssetTotalAmt;
	
	/** 底层资产类型 **/
	private String basicAssetType;
	
	/** 资产包最大单项金额 **/
	private java.math.BigDecimal assetPackMaxSigAmt;
	
	/** 资产包户数 **/
	private String assetPackQnt;
	
	/** 基本情况分析 **/
	private String basicCaseAnaly;
	
	/** 资产数量 **/
	private String assetQnt;
	
	/** 发行人数量 **/
	private String issueQnt;
	
	/** 资产池投资金额 **/
	private java.math.BigDecimal poolInvestAmt;
	
	/** 投资期限 **/
	private Integer investTerm;
	
	/** 剩余投资期限 **/
	private Integer surplusInvestTerm;
	
	/** 底层基础资产基本情况分析 **/
	private String basicAssetBasicCaseAnaly;
	
	/** 其他说明 **/
	private String otherDesc;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno == null ? null : approveSerno.trim();
	}
	
    /**
     * @return ApproveSerno
     */	
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param poolBasicAssetNormal
	 */
	public void setPoolBasicAssetNormal(String poolBasicAssetNormal) {
		this.poolBasicAssetNormal = poolBasicAssetNormal == null ? null : poolBasicAssetNormal.trim();
	}
	
    /**
     * @return PoolBasicAssetNormal
     */	
	public String getPoolBasicAssetNormal() {
		return this.poolBasicAssetNormal;
	}
	
	/**
	 * @param firstStartDate
	 */
	public void setFirstStartDate(String firstStartDate) {
		this.firstStartDate = firstStartDate == null ? null : firstStartDate.trim();
	}
	
    /**
     * @return FirstStartDate
     */	
	public String getFirstStartDate() {
		return this.firstStartDate;
	}
	
	/**
	 * @param loanQnt
	 */
	public void setLoanQnt(String loanQnt) {
		this.loanQnt = loanQnt == null ? null : loanQnt.trim();
	}
	
    /**
     * @return LoanQnt
     */	
	public String getLoanQnt() {
		return this.loanQnt;
	}
	
	/**
	 * @param debitQnt
	 */
	public void setDebitQnt(String debitQnt) {
		this.debitQnt = debitQnt == null ? null : debitQnt.trim();
	}
	
    /**
     * @return DebitQnt
     */	
	public String getDebitQnt() {
		return this.debitQnt;
	}
	
	/**
	 * @param poolTotalAmt
	 */
	public void setPoolTotalAmt(java.math.BigDecimal poolTotalAmt) {
		this.poolTotalAmt = poolTotalAmt;
	}
	
    /**
     * @return PoolTotalAmt
     */	
	public java.math.BigDecimal getPoolTotalAmt() {
		return this.poolTotalAmt;
	}
	
	/**
	 * @param poolContAmt
	 */
	public void setPoolContAmt(java.math.BigDecimal poolContAmt) {
		this.poolContAmt = poolContAmt;
	}
	
    /**
     * @return PoolContAmt
     */	
	public java.math.BigDecimal getPoolContAmt() {
		return this.poolContAmt;
	}
	
	/**
	 * @param sigLoanAvgAmt
	 */
	public void setSigLoanAvgAmt(java.math.BigDecimal sigLoanAvgAmt) {
		this.sigLoanAvgAmt = sigLoanAvgAmt;
	}
	
    /**
     * @return SigLoanAvgAmt
     */	
	public java.math.BigDecimal getSigLoanAvgAmt() {
		return this.sigLoanAvgAmt;
	}
	
	/**
	 * @param sigDebitAvgAmt
	 */
	public void setSigDebitAvgAmt(java.math.BigDecimal sigDebitAvgAmt) {
		this.sigDebitAvgAmt = sigDebitAvgAmt;
	}
	
    /**
     * @return SigDebitAvgAmt
     */	
	public java.math.BigDecimal getSigDebitAvgAmt() {
		return this.sigDebitAvgAmt;
	}
	
	/**
	 * @param loanWeightAvgRate
	 */
	public void setLoanWeightAvgRate(java.math.BigDecimal loanWeightAvgRate) {
		this.loanWeightAvgRate = loanWeightAvgRate;
	}
	
    /**
     * @return LoanWeightAvgRate
     */	
	public java.math.BigDecimal getLoanWeightAvgRate() {
		return this.loanWeightAvgRate;
	}
	
	/**
	 * @param lastSigLoanEndTerm
	 */
	public void setLastSigLoanEndTerm(String lastSigLoanEndTerm) {
		this.lastSigLoanEndTerm = lastSigLoanEndTerm == null ? null : lastSigLoanEndTerm.trim();
	}
	
    /**
     * @return LastSigLoanEndTerm
     */	
	public String getLastSigLoanEndTerm() {
		return this.lastSigLoanEndTerm;
	}
	
	/**
	 * @param lastThrUnrepayCapPerc
	 */
	public void setLastThrUnrepayCapPerc(java.math.BigDecimal lastThrUnrepayCapPerc) {
		this.lastThrUnrepayCapPerc = lastThrUnrepayCapPerc;
	}
	
    /**
     * @return LastThrUnrepayCapPerc
     */	
	public java.math.BigDecimal getLastThrUnrepayCapPerc() {
		return this.lastThrUnrepayCapPerc;
	}
	
	/**
	 * @param lastFifUnrepayCapPerc
	 */
	public void setLastFifUnrepayCapPerc(java.math.BigDecimal lastFifUnrepayCapPerc) {
		this.lastFifUnrepayCapPerc = lastFifUnrepayCapPerc;
	}
	
    /**
     * @return LastFifUnrepayCapPerc
     */	
	public java.math.BigDecimal getLastFifUnrepayCapPerc() {
		return this.lastFifUnrepayCapPerc;
	}
	
	/**
	 * @param isPassBasicAsset
	 */
	public void setIsPassBasicAsset(String isPassBasicAsset) {
		this.isPassBasicAsset = isPassBasicAsset == null ? null : isPassBasicAsset.trim();
	}
	
    /**
     * @return IsPassBasicAsset
     */	
	public String getIsPassBasicAsset() {
		return this.isPassBasicAsset;
	}
	
	/**
	 * @param isTotalPassBasic
	 */
	public void setIsTotalPassBasic(String isTotalPassBasic) {
		this.isTotalPassBasic = isTotalPassBasic == null ? null : isTotalPassBasic.trim();
	}
	
    /**
     * @return IsTotalPassBasic
     */	
	public String getIsTotalPassBasic() {
		return this.isTotalPassBasic;
	}
	
	/**
	 * @param isBasicAsset
	 */
	public void setIsBasicAsset(String isBasicAsset) {
		this.isBasicAsset = isBasicAsset == null ? null : isBasicAsset.trim();
	}
	
    /**
     * @return IsBasicAsset
     */	
	public String getIsBasicAsset() {
		return this.isBasicAsset;
	}
	
	/**
	 * @param basicAssetTotalAmt
	 */
	public void setBasicAssetTotalAmt(java.math.BigDecimal basicAssetTotalAmt) {
		this.basicAssetTotalAmt = basicAssetTotalAmt;
	}
	
    /**
     * @return BasicAssetTotalAmt
     */	
	public java.math.BigDecimal getBasicAssetTotalAmt() {
		return this.basicAssetTotalAmt;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType == null ? null : basicAssetType.trim();
	}
	
    /**
     * @return BasicAssetType
     */	
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param assetPackMaxSigAmt
	 */
	public void setAssetPackMaxSigAmt(java.math.BigDecimal assetPackMaxSigAmt) {
		this.assetPackMaxSigAmt = assetPackMaxSigAmt;
	}
	
    /**
     * @return AssetPackMaxSigAmt
     */	
	public java.math.BigDecimal getAssetPackMaxSigAmt() {
		return this.assetPackMaxSigAmt;
	}
	
	/**
	 * @param assetPackQnt
	 */
	public void setAssetPackQnt(String assetPackQnt) {
		this.assetPackQnt = assetPackQnt == null ? null : assetPackQnt.trim();
	}
	
    /**
     * @return AssetPackQnt
     */	
	public String getAssetPackQnt() {
		return this.assetPackQnt;
	}
	
	/**
	 * @param basicCaseAnaly
	 */
	public void setBasicCaseAnaly(String basicCaseAnaly) {
		this.basicCaseAnaly = basicCaseAnaly == null ? null : basicCaseAnaly.trim();
	}
	
    /**
     * @return BasicCaseAnaly
     */	
	public String getBasicCaseAnaly() {
		return this.basicCaseAnaly;
	}
	
	/**
	 * @param assetQnt
	 */
	public void setAssetQnt(String assetQnt) {
		this.assetQnt = assetQnt == null ? null : assetQnt.trim();
	}
	
    /**
     * @return AssetQnt
     */	
	public String getAssetQnt() {
		return this.assetQnt;
	}
	
	/**
	 * @param issueQnt
	 */
	public void setIssueQnt(String issueQnt) {
		this.issueQnt = issueQnt == null ? null : issueQnt.trim();
	}
	
    /**
     * @return IssueQnt
     */	
	public String getIssueQnt() {
		return this.issueQnt;
	}
	
	/**
	 * @param poolInvestAmt
	 */
	public void setPoolInvestAmt(java.math.BigDecimal poolInvestAmt) {
		this.poolInvestAmt = poolInvestAmt;
	}
	
    /**
     * @return PoolInvestAmt
     */	
	public java.math.BigDecimal getPoolInvestAmt() {
		return this.poolInvestAmt;
	}
	
	/**
	 * @param investTerm
	 */
	public void setInvestTerm(Integer investTerm) {
		this.investTerm = investTerm;
	}
	
    /**
     * @return InvestTerm
     */	
	public Integer getInvestTerm() {
		return this.investTerm;
	}
	
	/**
	 * @param surplusInvestTerm
	 */
	public void setSurplusInvestTerm(Integer surplusInvestTerm) {
		this.surplusInvestTerm = surplusInvestTerm;
	}
	
    /**
     * @return SurplusInvestTerm
     */	
	public Integer getSurplusInvestTerm() {
		return this.surplusInvestTerm;
	}
	
	/**
	 * @param basicAssetBasicCaseAnaly
	 */
	public void setBasicAssetBasicCaseAnaly(String basicAssetBasicCaseAnaly) {
		this.basicAssetBasicCaseAnaly = basicAssetBasicCaseAnaly == null ? null : basicAssetBasicCaseAnaly.trim();
	}
	
    /**
     * @return BasicAssetBasicCaseAnaly
     */	
	public String getBasicAssetBasicCaseAnaly() {
		return this.basicAssetBasicCaseAnaly;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc == null ? null : otherDesc.trim();
	}
	
    /**
     * @return OtherDesc
     */	
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}