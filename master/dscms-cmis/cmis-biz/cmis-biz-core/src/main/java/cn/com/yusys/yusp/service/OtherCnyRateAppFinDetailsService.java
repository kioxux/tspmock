/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateApprSubMapper;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateLoanRelOldRateMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateAppFinDetailsMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppFinDetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-05 14:38:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateAppFinDetailsService {
    private static final Logger log = LoggerFactory.getLogger(OtherForRateAppService.class);

    @Autowired
    private OtherCnyRateAppFinDetailsMapper otherCnyRateAppFinDetailsMapper;

    @Autowired
    private OtherCnyRateLoanRelOldRateMapper otherCnyRateLoanRelOldRateMapper;

    @Autowired
    private OtherCnyRateApprSubMapper otherCnyRateApprSubMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateAppFinDetails selectByPrimaryKey(String subSerno) {
        return otherCnyRateAppFinDetailsMapper.selectByPrimaryKey(subSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateAppFinDetails> selectAll(QueryModel model) {
        List<OtherCnyRateAppFinDetails> records = (List<OtherCnyRateAppFinDetails>) otherCnyRateAppFinDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateAppFinDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateAppFinDetails record) {
        return otherCnyRateAppFinDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateAppFinDetails record) {
        return otherCnyRateAppFinDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateAppFinDetails record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppFinDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateAppFinDetails record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppFinDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String subSerno) {
        return otherCnyRateAppFinDetailsMapper.deleteByPrimaryKey(subSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateAppFinDetailsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: addothercnyrateappfindetails
     * @方法描述: 新增本次融资申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addothercnyrateappfindetails(OtherCnyRateAppFinDetails othercnyrateappfindetails) {
        OtherCnyRateAppFinDetails otherCnyRateAppFinDetails = new OtherCnyRateAppFinDetails();

        try {
            //克隆对象
            BeanUtils.copyProperties(othercnyrateappfindetails, otherCnyRateAppFinDetails);

            //补充 克隆缺少数据
            //放入必填参数 操作类型 新增
            otherCnyRateAppFinDetails.setOprType(CmisBizConstants.OPR_TYPE_01);
            //状态 待生效
            otherCnyRateAppFinDetails.setStatus(CmisBizConstants.STD_CUS_LIST_STATUS_00);
            //时间 登记人 等级机构相关
            User user = SessionUtils.getUserInformation();
            otherCnyRateAppFinDetails.setCreateTime(DateUtils.getCurrDate());
            otherCnyRateAppFinDetails.setInputId(user.getLoginCode());
            otherCnyRateAppFinDetails.setInputBrId(user.getOrg().getCode());
            otherCnyRateAppFinDetails.setInputDate(DateUtils.getCurrDateTimeStr());
            otherCnyRateAppFinDetails.setCreateTime(DateUtils.getCurrDate());
            otherCnyRateAppFinDetails.setUpdateTime(DateUtils.getCurrDate());
            otherCnyRateAppFinDetails.setUpdDate(DateUtils.getCurrDateTimeStr());
            otherCnyRateAppFinDetails.setUpdId(user.getLoginCode());
            otherCnyRateAppFinDetails.setUpdBrId(user.getOrg().getCode());

            int i = otherCnyRateAppFinDetailsMapper.insertSelective(otherCnyRateAppFinDetails);
            if (i != 1) {
                throw BizException.error(null, "999999", "新增：本次融资申请异常");
            }
            String serno = otherCnyRateAppFinDetails.getSerno();
            String subSerno = otherCnyRateAppFinDetails.getSubSerno();
            BigDecimal apprLoanRate = otherCnyRateAppFinDetails.getExecRateYear();
            String apprRateType = otherCnyRateAppFinDetails.getPriceMode();
            String rateAdjustCycleApprSub = otherCnyRateAppFinDetails.getRateAdjustCycle();
            BigDecimal lprRate = otherCnyRateAppFinDetails.getCurLpr();
            String fixedDate = otherCnyRateAppFinDetails.getRateIntvalDate();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("subSerno",subSerno);
            queryModel.addCondition("serno",serno);
            List<OtherCnyRateApprSub> otherCnyRateApprSub1 = otherCnyRateApprSubMapper.selectByModel(queryModel);

            if(otherCnyRateApprSub1.size()>0){
                OtherCnyRateApprSub otherCnyRateApprSub2 = otherCnyRateApprSub1.get(0);
                otherCnyRateApprSub2.setApprLoanRate(apprLoanRate);
                otherCnyRateApprSub2.setApprRateType(apprRateType);
                otherCnyRateApprSub2.setRateAdjustCycle(rateAdjustCycleApprSub);
                otherCnyRateApprSub2.setLprRate(lprRate);
                otherCnyRateApprSub2.setFixedDate(fixedDate);
                otherCnyRateApprSubMapper.updateByPrimaryKey(otherCnyRateApprSub2);
            }else if(otherCnyRateApprSub1.size()<=0){
                OtherCnyRateApprSub otherCnyRateApprSub2 = new OtherCnyRateApprSub();
                String apprSubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
                otherCnyRateApprSub2.setApproveSerno(apprSubSerno);
                otherCnyRateApprSub2.setSerno(serno);
                otherCnyRateApprSub2.setSubSerno(subSerno);
                otherCnyRateApprSub2.setSubPrdSerno(otherCnyRateAppFinDetails.getSubPrdSerno());
                otherCnyRateApprSub2.setAccSubPrdNo(otherCnyRateAppFinDetails.getLmtBizType());
                otherCnyRateApprSub2.setAccSubPrdName(otherCnyRateAppFinDetails.getLmtBizTypeName());
                otherCnyRateApprSub2.setApprLoanRate(apprLoanRate);
                otherCnyRateApprSub2.setApprRateType(apprRateType);
                otherCnyRateApprSub2.setRateAdjustCycle(rateAdjustCycleApprSub);
                otherCnyRateApprSub2.setLprRate(lprRate);
                otherCnyRateApprSub2.setFixedDate(fixedDate);
                otherCnyRateApprSub2.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                otherCnyRateApprSubMapper.insertSelective(otherCnyRateApprSub2);
            }

        }catch (Exception e) {
            log.error("本次融资申请新增异常：",e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }

        return new ResultDto(otherCnyRateAppFinDetails);
    }

    /**
     * @方法名称: selectEffectiveList
     * @方法描述: 根据客户编号获取有效申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto selectEffectiveList(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsMapper.selectEffectiveList(queryModel);
        return new ResultDto(list);
    }

    /**
     * @方法名称: lastFinDetails
     * @方法描述: 获取上期融资明细信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateAppFinDetails> lastFinDetails(QueryModel queryModel) {
        /*List<OtherCnyRateAppFinDetails> lastDetails = otherCnyRateAppFinDetailsMapper.selectByModel(queryModel);
        if (CollectionUtils.isEmpty(lastDetails)) {
            String cusId = (String) queryModel.getCondition().get("cusId");
            String subSerno = (String) queryModel.getCondition().get("subSerno");
            QueryModel lastqueryModel = new QueryModel();
            lastqueryModel.setCondition("{\"cusId\": \""+cusId+"\"}");
            lastDetails = otherCnyRateAppFinDetailsMapper.selectEffectiveList(lastqueryModel);
        }*/
        List<OtherCnyRateAppFinDetails> lastDetails=null;
        String subSerno = (String) queryModel.getCondition().get("subSerno");
        String oprType = (String) queryModel.getCondition().get("oprType");
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("subSerno",subSerno);
        queryModel1.addCondition("oprType",oprType);
        List<OtherCnyRateLoanRelOldRate> oldDetails = otherCnyRateLoanRelOldRateMapper.selectByModel(queryModel1);
        if(oldDetails.size()>0){
            String ogrinSubSerno=oldDetails.get(0).getOgrinSubSerno();
            log.info("流水入参" + JSON.toJSONString(ogrinSubSerno));
            QueryModel lastqueryModel = new QueryModel();
            lastqueryModel.addCondition("subSerno",ogrinSubSerno);
            lastDetails = otherCnyRateAppFinDetailsMapper.selectByModel(lastqueryModel);
        }
        return lastDetails;
    }

    /**
     * @方法名称: lastFinDetails
     * @方法描述: 获取本期融资明细信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateAppFinDetails> currFinDetails(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> currDetails = otherCnyRateAppFinDetailsMapper.selectByModel(queryModel);
        return currDetails;
    }

    /**
     * @方法名称: grpFinDetails
     * @方法描述: 集团企业及关联企业在我行上期融资情况
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateAppFinDetails> grpFinDetails(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> grpDetails = otherCnyRateAppFinDetailsMapper.selectByModel(queryModel);
        return grpDetails;
    }

    /**
     * @方法名称: getFinDetailsByCusId
     * @方法描述: 根据查到的集团成员客户号，查询关联企业最新融资情况
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateAppFinDetails> getFinDetailsByCusId(String cusId) {
        List<OtherCnyRateAppFinDetails> otherCnyRateAppFinDetails = otherCnyRateAppFinDetailsMapper.getFinDetailsByCusId(cusId);
        return otherCnyRateAppFinDetails;
    }

    /**
     * @方法名称: checkothercnyrateappfindetails
     * @方法描述: 校验本次融资申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto checkothercnyrateappfindetails(OtherCnyRateAppFinDetails othercnyrateappfindetails) {
        OtherCnyRateAppFinDetails otherCnyRateAppFinDetails = otherCnyRateAppFinDetailsMapper.selectByPrimaryKey(othercnyrateappfindetails.getSubSerno());

        if(!CollectionUtils.nonNull(otherCnyRateAppFinDetails)){
            throw BizException.error(null, "999999", "融资申请不存在");
        }
        return new ResultDto(otherCnyRateAppFinDetails);
    }

    /**
     * @方法名称: updateothercnyrateappfindetails
     * @方法描述: 更新融资信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateothercnyrateappfindetails(OtherCnyRateAppFinDetails othercnyrateappfindetails) {
        {
            OtherCnyRateAppFinDetails otherCnyRateAppFinDetails = new OtherCnyRateAppFinDetails();

            try {
                //克隆对象
                BeanUtils.copyProperties(othercnyrateappfindetails, otherCnyRateAppFinDetails);
                //补充 克隆缺少数据
                OtherCnyRateAppFinDetails oldotherCnyRateAppFinDetails = otherCnyRateAppFinDetailsMapper.selectByPrimaryKey(otherCnyRateAppFinDetails.getSubSerno());
                //放入必填参数 操作类型 新增
                otherCnyRateAppFinDetails.setOprType(oldotherCnyRateAppFinDetails.getOprType());
                //状态 带生效
                otherCnyRateAppFinDetails.setStatus(oldotherCnyRateAppFinDetails.getStatus());
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherCnyRateAppFinDetails.setUpdateTime(DateUtils.getCurrDate());
                otherCnyRateAppFinDetails.setUpdDate(dateFormat.format(new Date()) );
                otherCnyRateAppFinDetails.setUpdId(user.getLoginCode());
                otherCnyRateAppFinDetails.setUpdBrId(user.getOrg().getCode());

                int i = otherCnyRateAppFinDetailsMapper.updateByPrimaryKeySelective(otherCnyRateAppFinDetails);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：融资信息异常");
                }

                String serno = otherCnyRateAppFinDetails.getSerno();
                String subSerno = otherCnyRateAppFinDetails.getSubSerno();
                BigDecimal apprLoanRate = otherCnyRateAppFinDetails.getExecRateYear();
                String apprRateType = otherCnyRateAppFinDetails.getPriceMode();
                String rateAdjustCycleApprSub = otherCnyRateAppFinDetails.getRateAdjustCycle();
                BigDecimal lprRate = otherCnyRateAppFinDetails.getCurLpr();
                String fixedDate = otherCnyRateAppFinDetails.getRateIntvalDate();
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("subSerno",subSerno);
                queryModel.addCondition("serno",serno);
                List<OtherCnyRateApprSub> otherCnyRateApprSub1 = otherCnyRateApprSubMapper.selectByModel(queryModel);

                if(otherCnyRateApprSub1.size()>0){
                    OtherCnyRateApprSub otherCnyRateApprSub2 = otherCnyRateApprSub1.get(0);
                    otherCnyRateApprSub2.setApprLoanRate(apprLoanRate);
                    otherCnyRateApprSub2.setApprRateType(apprRateType);
                    otherCnyRateApprSub2.setRateAdjustCycle(rateAdjustCycleApprSub);
                    otherCnyRateApprSub2.setLprRate(lprRate);
                    otherCnyRateApprSub2.setFixedDate(fixedDate);
                    otherCnyRateApprSubMapper.updateByPrimaryKey(otherCnyRateApprSub2);
                }

            }catch (Exception e) {
                log.error("融资信息修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherCnyRateAppFinDetails);

        }
    }
}
