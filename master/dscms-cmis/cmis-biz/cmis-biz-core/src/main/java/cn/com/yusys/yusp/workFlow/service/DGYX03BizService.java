package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrContImageAuditApp;
import cn.com.yusys.yusp.domain.IqpDiscApp;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.CtrContImageAuditAppMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DGYX03BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX03BizService.class);//定义log

    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;
    @Autowired
    private BGYW01BizService bgyw01Bizservice;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07Bizservice;
    @Autowired
    private BGYW09BizService bgyw09Bizservice;
    @Autowired
    private CentralFileTaskService centralFileTaskService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_YX010.equals(bizType)){
            handleYX010Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG026.equals(bizType)){
            //展期协议签订审核（对公）
            bgyw02BizService.bizOp(resultInstanceDto);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG029.equals(bizType)){
            //担保变更协议签订审核（对公）
            bgyw03Bizservice.bizOp(resultInstanceDto);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG032.equals(bizType)){
            //利率变更协议签订审核（对公）
            bgyw01Bizservice.bizOp(resultInstanceDto);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG035.equals(bizType)){
            //还款计划变更协议签订审核（对公）
            bgyw07Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG038.equals(bizType)){
            //延期还款协议签订审核（对公）
            bgyw09Bizservice.bizOp(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 合同影像审核
    private void handleYX010Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("合同影像审核" + currentOpType);
        try {
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(iqpSerno);
            ctrContImageAuditAppService.put2VarParam(resultInstanceDto,iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                ctrContImageAuditAppService.handleBusinessDataAfterStart(iqpSerno);
                //对公合同影像审核的临时档案任务
                if ("243_5".equals(resultInstanceDto.getCurrentNodeId())){
                    // 资料扫描岗 新增临时档案任务
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(iqpSerno);
                    centralFileTaskdto.setTraceId(ctrContImageAuditApp.getContNo());
                    centralFileTaskdto.setCusId(ctrContImageAuditApp.getCusId());
                    centralFileTaskdto.setCusName(ctrContImageAuditApp.getCusName());
                    centralFileTaskdto.setBizType(CmisFlowConstants.FLOW_TYPE_TYPE_YX010);
                    centralFileTaskdto.setInputId(ctrContImageAuditApp.getInputId());
                    centralFileTaskdto.setInputBrId(ctrContImageAuditApp.getInputBrId());
                    centralFileTaskdto.setOptType("02"); // 非纯指令
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    centralFileTaskdto.setTaskType("02"); // 档案暂存
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    centralFileTaskService.insertSelective(centralFileTaskdto);
                }
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,iqpSerno);
                // ctrContImageAuditAppService.createImageSpplInfo(iqpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                // 推送影像审批信息
                sendImage(resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditAppService.handleBusinessDataAfterEnd(iqpSerno);
                // 最高额授信协议生成档案归档任务
                ctrContImageAuditAppService.createDocArchive(ctrContImageAuditApp);
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,iqpSerno);
                ctrContImageAuditAppService.createImageSpplInfo(iqpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"合同影像审核");
                //微信通知
                String managerId = ctrContImageAuditApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", ctrContImageAuditApp.getCusName());
                        paramMap.put("prdName", "合同影像审核");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                    ctrContImageAuditAppService.put2VarParam(resultInstanceDto,iqpSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("合同影像申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                    ctrContImageAuditAppService.put2VarParam(resultInstanceDto,iqpSerno);
                    //微信通知
                    String managerId = ctrContImageAuditApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", ctrContImageAuditApp.getCusName());
                            paramMap.put("prdName", "合同影像审核");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合同影像申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
            } else {
                log.warn("合同影像申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGYX03.equals(flowCode);
    }


    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();
            if ("243_6".equals(nodeId) || "243_7".equals(nodeId)){//【集中作业放款初审岗、集中作业放款复审岗员工号】 这两个岗位的人审核影像
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }
        if(approveUserId.length() > 0){
            // 一般情况下，直接获取流程实列中的参数。
            Map<String, Object> map = (Map) JSON.parse(resultInstanceDto.getFlowParam());
            String topOutsystemCode = (String) map.get("topOutsystemCode");
            if(StringUtils.isEmpty(topOutsystemCode)){
                throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
            }
            Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
            String docid = imageParams.get("contid");
            if(StringUtils.isEmpty(docid)){
                throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
            }
            approveUserId = approveUserId.substring(1);
            String[] arr = topOutsystemCode.split(";");
            for (int i = 0; i < arr.length; i++) {
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(docid);//任务编号
                imageApprDto.setApproval("同意");//审批意见
                imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                imageApprDto.setOutcode(arr[i]);//文件类型根节点
                imageApprDto.setOpercode(approveUserId);//审批人员
                cmisBizXwCommonService.sendImage(imageApprDto);
            }
        }
    }
}
