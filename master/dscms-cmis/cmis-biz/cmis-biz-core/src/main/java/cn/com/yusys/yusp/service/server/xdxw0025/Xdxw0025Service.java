package cn.com.yusys.yusp.service.server.xdxw0025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0025.req.Xdxw0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0025.resp.Xdxw0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0025Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-5-10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0025Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0025Service.class);

    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;

    /**
     * 借据下抵押物信息列表查询
     *
     * @param xdxw0025DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0025DataRespDto getXdxw0025(Xdxw0025DataReqDto xdxw0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, xdxw0025DataReqDto);
        Xdxw0025DataRespDto xdxw0025DataRespDto = new Xdxw0025DataRespDto();
        try {
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0025.resp.List> list = guarBaseInfoMapper.getGuarBaseListByBillNo(xdxw0025DataReqDto);
            xdxw0025DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, xdxw0025DataRespDto);
        return xdxw0025DataRespDto;
    }

}
