package cn.com.yusys.yusp.web.server.xdht0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0022.req.Xdht0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0022.resp.Xdht0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0022.Xdht0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0022:合同信息列表查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0022Resource.class);

    @Autowired
    private Xdht0022Service xdht0022Service;

    /**
     * 交易码：xdht0022
     * 交易描述：合同信息列表查询
     *
     * @param xdht0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息列表查询")
    @PostMapping("/xdht0022")
    protected @ResponseBody
    ResultDto<Xdht0022DataRespDto> xdht0022(@Validated @RequestBody Xdht0022DataReqDto xdht0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataReqDto));
        Xdht0022DataRespDto xdht0022DataRespDto = new Xdht0022DataRespDto();// 响应Dto:合同信息列表查询
        ResultDto<Xdht0022DataRespDto> xdht0022DataResultDto = new ResultDto<>();

        String cusId = xdht0022DataReqDto.getCusId();//客户号
        String certCode = xdht0022DataReqDto.getCertCode();//证件号码
        Integer startPageNum = xdht0022DataReqDto.getStartPageNum();//开始页数
        Integer pageSize = xdht0022DataReqDto.getPageSize();//条数

        try {
            // 从xdht0022DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataReqDto));
            xdht0022DataRespDto = xdht0022Service.xdht0022(xdht0022DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataRespDto));
            // 封装xdht0022DataResultDto中正确的返回码和返回信息
            xdht0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, e.getMessage());
            // 封装xdht0022DataResultDto中异常返回码和返回信息
            xdht0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0022DataRespDto到xdht0022DataResultDto中
        xdht0022DataResultDto.setData(xdht0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataResultDto));
        return xdht0022DataResultDto;
    }
}
