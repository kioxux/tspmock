package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppr
 * @类描述: lmt_intbank_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 14:53:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtIntbankApprDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 审批流水号 **/
	private String approveSerno;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 授信类型 **/
	private String lmtType;
	
	/** 原授信批复流水号 **/
	private String origiLmtReplySerno;
	
	/** 原授信期限 **/
	private Integer origiLmtTerm;
	
	/** 原授信金额 **/
	private java.math.BigDecimal origiLmtAmt;
	
	/** 授信金额 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 币种 **/
	private String curType;
	
	/** 授信期限 **/
	private Integer lmtTerm;
	
	/** 同业授信准入 **/
	private String intbankLmtAdmit;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 综合分析 **/
	private String inteAnaly;
	
	/** 终审机构 **/
	private String finalApprBrId;
	
	/** 审批模式 **/
	private String apprMode;
	
	/** 评审结论 **/
	private String reviewResult;
	
	/** 结论性描述 **/
	private String restDesc;
	
	/** 出具报告类型 **/
	private String issueReportType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno == null ? null : approveSerno.trim();
	}
	
    /**
     * @return ApproveSerno
     */	
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType == null ? null : lmtType.trim();
	}
	
    /**
     * @return LmtType
     */	
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno == null ? null : origiLmtReplySerno.trim();
	}
	
    /**
     * @return OrigiLmtReplySerno
     */	
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(Integer origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}
	
    /**
     * @return OrigiLmtTerm
     */	
	public Integer getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}
	
	/**
	 * @param origiLmtAmt
	 */
	public void setOrigiLmtAmt(java.math.BigDecimal origiLmtAmt) {
		this.origiLmtAmt = origiLmtAmt;
	}
	
    /**
     * @return OrigiLmtAmt
     */	
	public java.math.BigDecimal getOrigiLmtAmt() {
		return this.origiLmtAmt;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param intbankLmtAdmit
	 */
	public void setIntbankLmtAdmit(String intbankLmtAdmit) {
		this.intbankLmtAdmit = intbankLmtAdmit == null ? null : intbankLmtAdmit.trim();
	}
	
    /**
     * @return IntbankLmtAdmit
     */	
	public String getIntbankLmtAdmit() {
		return this.intbankLmtAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly == null ? null : inteAnaly.trim();
	}
	
    /**
     * @return InteAnaly
     */	
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param finalApprBrId
	 */
	public void setFinalApprBrId(String finalApprBrId) {
		this.finalApprBrId = finalApprBrId == null ? null : finalApprBrId.trim();
	}
	
    /**
     * @return FinalApprBrId
     */	
	public String getFinalApprBrId() {
		return this.finalApprBrId;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode == null ? null : apprMode.trim();
	}
	
    /**
     * @return ApprMode
     */	
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param reviewResult
	 */
	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult == null ? null : reviewResult.trim();
	}
	
    /**
     * @return ReviewResult
     */	
	public String getReviewResult() {
		return this.reviewResult;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc == null ? null : restDesc.trim();
	}
	
    /**
     * @return RestDesc
     */	
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param issueReportType
	 */
	public void setIssueReportType(String issueReportType) {
		this.issueReportType = issueReportType == null ? null : issueReportType.trim();
	}
	
    /**
     * @return IssueReportType
     */	
	public String getIssueReportType() {
		return this.issueReportType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}