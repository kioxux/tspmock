package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpRepayDateChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpRepayDateChgMapper;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayDateChgService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: fangzhen
 * @创建时间: 2020-01-14 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

@Service
public class IqpRepayDateChgService {
    private static final Logger log = LoggerFactory.getLogger(IqpRepayDateChgService.class);
    @Autowired
    private IqpRepayDateChgMapper iqpRepayDateChgMapper;

    @Autowired
    private AccLoanService accLoanService;



    /**
     * 点击下一步 保存数据到还款日变更申请表  申请状态为待发起
     * @param iqpRepayDateChg
     * @return
     */
    public int save(IqpRepayDateChg iqpRepayDateChg) {
        iqpRepayDateChg.setApproveStatus("000");
        iqpRepayDateChg.setOprType("01");
        int result = iqpRepayDateChgMapper.insert(iqpRepayDateChg);
        return result;
    }

    /**
     * 根据  通过借据号  校验日期变更表当中的申请状态是否为"审批中"或者"打回"
     * @param billNo 借据编号
     * @return
     */
    public int checkApproveStatus(String billNo) {
        List<IqpRepayDateChg> iqpRepayDateChgs = this.iqpRepayDateChgMapper.selectByBillNo(billNo);
        return  iqpRepayDateChgs.size();
    }

    /**
     * 根据  通过借据号  校验日期变更表当中的申请状态是否为"审批中"或者"打回"
     * @param iqpSerno 业务流水号
     * @return
     */
    public int checkIsExistIqpRepayDateChgBizByBillNo(String iqpSerno, String billNo) {
        try {
            HashMap<String,String> param = new HashMap<String,String >();
            log.info("校验是否存在在途的还款日期变更申请【{}】", JSONObject.toJSON(iqpSerno));
            // 获取还款日变更信息
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChgNew = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            // 判断是否需要排除当前流水号对应的  还款日变更信息
            if(iqpRepayDateChgNew != null ){
                param.put("iqp_serno",iqpSerno);
            }
            param.put("wfExistsFlag", CmisFlowConstants.REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME);
            // 放入需要的操作类型
            param.put("opr_type", CmisCommonConstants.OPR_TYPE_ADD);
            // 放入借据号
            param.put("bill_no",billNo);
            return this.iqpRepayDateChgMapper.checkIsExistChgBizByBillNo(param);
        } catch (YuspException e) {
            log.error("还款日期变更申请新增失败！", e);
            return -1;

        }
    }

    /**
     * 点击提交按钮  提交申请  修改审批状态为审批中
     * @param iqpRepayDateChg   借据编号
     * @return
     */
    public int commit(IqpRepayDateChg iqpRepayDateChg) {
        return this.iqpRepayDateChgMapper.updateByPrimaryKey(iqpRepayDateChg);
    }

    /**
     * 逻辑删除变更日期操作申请
     * @param iqpRepayDateChg
     * @return
     */
    public int updateOprTypeByPrimaryKey(IqpRepayDateChg iqpRepayDateChg) {
       return this.iqpRepayDateChgMapper.updateOprTypeByPrimaryKey(iqpRepayDateChg);
    }

    /**
     * 主页面点击修改  点击保存按钮保存数据
     * @param iqpRepayDateChg
     * @return
     */
    public int updateCommit(IqpRepayDateChg iqpRepayDateChg) {
        return   this.iqpRepayDateChgMapper.updateCommitByPrimaryKey(iqpRepayDateChg);
    }

    /**
     * 初审  修改状态为审核中
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChg1 = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            if(iqpRepayDateChg1==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【111】-审批中");
            int result= iqpRepayDateChgMapper.updateByPrimaryKey(iqpRepayDateChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }

    }

    /**
     * 审批通过  状态修改为已通过
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChg1 = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            if(iqpRepayDateChg1==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【997】-已通过");
            AccLoan accLoan=new AccLoan();
            SimpleDateFormat  simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String updDate = simpleDateFormat.format(new Date());
            /**
             * zhanyb修改20210427。以下为原内容
             accLoan.setRepayDate(iqpRepayDateChg1.getRepayDate());
             */
            accLoan.setDeductDay(iqpRepayDateChg1.getRepayDate().toString());
            accLoan.setBillNo(iqpRepayDateChg1.getBillNo());
            accLoan.setUpdDate(updDate);
            int i = this.accLoanService.updateSelective(accLoan);
            int result= iqpRepayDateChgMapper.updateYtgByPrimaryKey(iqpRepayDateChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批完成  状态修改为打回
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfteCallBack(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChg1 = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            if(iqpRepayDateChg1==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【992】-打回");
            int result= iqpRepayDateChgMapper.handleBusinessDataAfteCallBack(iqpRepayDateChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批完成   修改状态为追回
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfteTackBack(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChg1 = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            if(iqpRepayDateChg1==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【991】-打回");
            int result= iqpRepayDateChgMapper.handleBusinessDataAfteTackBack(iqpRepayDateChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批通过修改状态为拒绝
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterRefuse(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayDateChg iqpRepayDateChg=new IqpRepayDateChg();
            iqpRepayDateChg.setIqpSerno(iqpSerno);
            IqpRepayDateChg iqpRepayDateChg1 = iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
            if(iqpRepayDateChg1==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【998】-打回");
            int result= iqpRepayDateChgMapper.handleBusinessDataAfterRefuse(iqpRepayDateChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 发起申请校验当前借据是否又其他流程处于审核状态
     * @param iqpRepayDateChg
     * @return
     */
    public int checkIsExistChgBizByBillNo(IqpRepayDateChg iqpRepayDateChg) {
//        return this.iqpRepayWayChgService.checkIsExistChgBizByBillNo(iqpRepayDateChg.getIqpSerno(), iqpRepayDateChg.getBillNo());
        return 0;
    }

    public IqpRepayDateChg selectByPrimaryKey(IqpRepayDateChg iqpRepayDateChg) {
        return this.iqpRepayDateChgMapper.selectByPrimaryKey(iqpRepayDateChg);
    }

    public List<IqpRepayDateChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayDateChg> list = iqpRepayDateChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

}
