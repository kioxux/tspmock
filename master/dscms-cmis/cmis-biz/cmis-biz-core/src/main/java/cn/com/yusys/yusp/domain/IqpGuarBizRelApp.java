/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarBizRelApp
 * @类描述: iqp_guar_biz_rel_app数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:55:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_guar_biz_rel_app")
public class IqpGuarBizRelApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 担保合同流水号  **/
	@Column(name = "GUAR_PK_ID", unique = false, nullable = false, length = 40)
	private String guarPkId;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 本次担保金额 **/
	@Column(name = "THIS_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal thisGuarAmt;
	
	/** 是否追加担保 STD_ZB_YES_NO **/
	@Column(name = "IS_ADD_GUAR", unique = false, nullable = true, length = 5)
	private String isAddGuar;
	
	/** 是否阶段性担保 STD_ZB_YES_NO **/
	@Column(name = "IS_PER_GUR", unique = false, nullable = true, length = 5)
	private String isPerGur;
	
	/** 关联关系 STD_ZB_CORRE_REL **/
	@Column(name = "CORRE_REL", unique = false, nullable = true, length = 5)
	private String correRel;
	
	/** 关联类型 STD_ZB_REL_TYPE **/
	@Column(name = "R_TYPE", unique = false, nullable = true, length = 5)
	private String rType;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId;
	}
	
    /**
     * @return guarPkId
     */
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param thisGuarAmt
	 */
	public void setThisGuarAmt(java.math.BigDecimal thisGuarAmt) {
		this.thisGuarAmt = thisGuarAmt;
	}
	
    /**
     * @return thisGuarAmt
     */
	public java.math.BigDecimal getThisGuarAmt() {
		return this.thisGuarAmt;
	}
	
	/**
	 * @param isAddGuar
	 */
	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar;
	}
	
    /**
     * @return isAddGuar
     */
	public String getIsAddGuar() {
		return this.isAddGuar;
	}
	
	/**
	 * @param isPerGur
	 */
	public void setIsPerGur(String isPerGur) {
		this.isPerGur = isPerGur;
	}
	
    /**
     * @return isPerGur
     */
	public String getIsPerGur() {
		return this.isPerGur;
	}
	
	/**
	 * @param correRel
	 */
	public void setCorreRel(String correRel) {
		this.correRel = correRel;
	}
	
    /**
     * @return correRel
     */
	public String getCorreRel() {
		return this.correRel;
	}
	
	/**
	 * @param rType
	 */
	public void setRType(String rType) {
		this.rType = rType;
	}
	
    /**
     * @return rType
     */
	public String getRType() {
		return this.rType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}