package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import javax.persistence.Column;
import java.math.BigDecimal;

@ExcelCsv(namePrefix = "底层资产明细导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class SigInvestDetailsImportVo {

    /** 项目编号 **/
    @ExcelField(title = "项目编号", viewLength = 20)
    private String proNo;

    /** 项目名称 **/
    @ExcelField(title = "项目名称", viewLength = 20)
    private String proName;

    /** 资产编号 **/
    @ExcelField(title = "资产编号", viewLength = 20)
    private String assetNo;

    /** 项目当前总市值 **/
    @ExcelField(title = "项目当前总市值", viewLength = 30)
    private java.math.BigDecimal proCurtMarketValue;

    /** 我行投资当前市值 **/
    @ExcelField(title = "我行投资当前市值", viewLength = 30)
    private java.math.BigDecimal investCurtMarketValue;

    /** 底层资产编号 **/
    @ExcelField(title = "底层资产编号", viewLength = 20)
    private String basicAssetNo;

    /** 底层资产名称 **/
    @ExcelField(title = "底层资产名称", viewLength = 20)
    private String basicAssetName;

    /** 底层资产类型 **/
    @ExcelField(title = "底层资产类型", viewLength = 20)
    private String basicAssetType;

    /** 底层资产客户编号 **/
    @ExcelField(title = "底层资产客户编号", viewLength = 30)
    private String basicAssetCusId;

    /** 底层资产客户名称 **/
    @ExcelField(title = "底层资产客户名称", viewLength = 30)
    private String basicAssetCusName;

    /** 底层资产持仓占比 **/
    @ExcelField(title = "底层资产持仓占比", viewLength = 30)
    private java.math.BigDecimal basicAssetPostScale;

    /** 底层资产风险暴露金额 **/
    @ExcelField(title = "底层资产风险暴露金额", viewLength = 40)
    private java.math.BigDecimal basicAssetRiskAmt;

    /** 是否为合格担保 **/
    @ExcelField(title = "是否为合格担保", viewLength = 20)
    private String isValidGuar;

    /** 底层增信担保方式 **/
    @ExcelField(title = "底层增信担保方式", viewLength = 20)
    private String basicAssetGuarMode;

    /** 底层资产增信人客户编号 **/
    @ExcelField(title = "底层资产增信人客户编号", viewLength = 40)
    private String basicAssetGuarCusId;

    /** 底层资产增信人客户名称 **/
    @ExcelField(title = "底层资产增信人客户名称", viewLength = 40)
    private String basicAssetGuarCusName;

    /** 底层资产增信人担保金额 **/
    @ExcelField(title = "底层资产增信人担保金额", viewLength = 40)
    private java.math.BigDecimal basicAssetGuarAmt;

    public String getProNo() {
        return proNo;
    }

    public void setProNo(String proNo) {
        this.proNo = proNo;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public BigDecimal getProCurtMarketValue() {
        return proCurtMarketValue;
    }

    public void setProCurtMarketValue(BigDecimal proCurtMarketValue) {
        this.proCurtMarketValue = proCurtMarketValue;
    }

    public BigDecimal getInvestCurtMarketValue() {
        return investCurtMarketValue;
    }

    public void setInvestCurtMarketValue(BigDecimal investCurtMarketValue) {
        this.investCurtMarketValue = investCurtMarketValue;
    }

    public String getBasicAssetNo() {
        return basicAssetNo;
    }

    public void setBasicAssetNo(String basicAssetNo) {
        this.basicAssetNo = basicAssetNo;
    }

    public String getBasicAssetName() {
        return basicAssetName;
    }

    public void setBasicAssetName(String basicAssetName) {
        this.basicAssetName = basicAssetName;
    }

    public String getBasicAssetType() {
        return basicAssetType;
    }

    public void setBasicAssetType(String basicAssetType) {
        this.basicAssetType = basicAssetType;
    }

    public String getBasicAssetCusId() {
        return basicAssetCusId;
    }

    public void setBasicAssetCusId(String basicAssetCusId) {
        this.basicAssetCusId = basicAssetCusId;
    }

    public String getBasicAssetCusName() {
        return basicAssetCusName;
    }

    public void setBasicAssetCusName(String basicAssetCusName) {
        this.basicAssetCusName = basicAssetCusName;
    }

    public BigDecimal getBasicAssetPostScale() {
        return basicAssetPostScale;
    }

    public void setBasicAssetPostScale(BigDecimal basicAssetPostScale) {
        this.basicAssetPostScale = basicAssetPostScale;
    }

    public BigDecimal getBasicAssetRiskAmt() {
        return basicAssetRiskAmt;
    }

    public void setBasicAssetRiskAmt(BigDecimal basicAssetRiskAmt) {
        this.basicAssetRiskAmt = basicAssetRiskAmt;
    }

    public String getIsValidGuar() {
        return isValidGuar;
    }

    public void setIsValidGuar(String isValidGuar) {
        this.isValidGuar = isValidGuar;
    }

    public String getBasicAssetGuarMode() {
        return basicAssetGuarMode;
    }

    public void setBasicAssetGuarMode(String basicAssetGuarMode) {
        this.basicAssetGuarMode = basicAssetGuarMode;
    }

    public String getBasicAssetGuarCusId() {
        return basicAssetGuarCusId;
    }

    public void setBasicAssetGuarCusId(String basicAssetGuarCusId) {
        this.basicAssetGuarCusId = basicAssetGuarCusId;
    }

    public String getBasicAssetGuarCusName() {
        return basicAssetGuarCusName;
    }

    public void setBasicAssetGuarCusName(String basicAssetGuarCusName) {
        this.basicAssetGuarCusName = basicAssetGuarCusName;
    }

    public BigDecimal getBasicAssetGuarAmt() {
        return basicAssetGuarAmt;
    }

    public void setBasicAssetGuarAmt(BigDecimal basicAssetGuarAmt) {
        this.basicAssetGuarAmt = basicAssetGuarAmt;
    }
}
