/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusGrpDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptCustomerOwnerStr;
import cn.com.yusys.yusp.repository.mapper.RptCustomerOwnerStrMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCustomerOwnerStrService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 19:34:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptCustomerOwnerStrService {

    @Autowired
    private RptCustomerOwnerStrMapper rptCustomerOwnerStrMapper;

    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptCustomerOwnerStr selectByPrimaryKey(String pkId) {
        return rptCustomerOwnerStrMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptCustomerOwnerStr> selectAll(QueryModel model) {
        List<RptCustomerOwnerStr> records = (List<RptCustomerOwnerStr>) rptCustomerOwnerStrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptCustomerOwnerStr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptCustomerOwnerStr> list = rptCustomerOwnerStrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptCustomerOwnerStr record) {
        return rptCustomerOwnerStrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptCustomerOwnerStr record) {
        return rptCustomerOwnerStrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptCustomerOwnerStr record) {
        return rptCustomerOwnerStrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptCustomerOwnerStr record) {
        return rptCustomerOwnerStrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptCustomerOwnerStrMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptCustomerOwnerStrMapper.deleteByIds(ids);
    }

    public List<RptCustomerOwnerStr> initOwner(Map map) {
        String grpSerno = map.get("grpSerno").toString();
        String grpNo = map.get("grpNo").toString();
        List<RptCustomerOwnerStr> result = new ArrayList<>();
        List<RptCustomerOwnerStr> rptCustomerOwnerStrs = rptCustomerOwnerStrMapper.selectBySerno(grpSerno);
        if (CollectionUtils.nonEmpty(rptCustomerOwnerStrs)) {
            return rptCustomerOwnerStrs;
        } else {
            ResultDto<CusGrpDto> cusGrpDtoResultDto = iCusClientService.selectCusGrpDtoByGrpNo(grpNo);
            if (cusGrpDtoResultDto != null && cusGrpDtoResultDto.getData() != null) {
                //集团核心客户号
                String coreCusId = cusGrpDtoResultDto.getData().getCoreCusId();
                if (StringUtils.nonBlank(coreCusId)) {
                    ResultDto<List<CusCorpApitalDto>> cusCorpApitalDtoResultDto = iCusClientService.selectByCusIdRel(coreCusId);
                    if (cusCorpApitalDtoResultDto != null && cusCorpApitalDtoResultDto.getData() != null) {
                        List<CusCorpApitalDto> CusCorpApitalList = cusCorpApitalDtoResultDto.getData();
                        if (CollectionUtils.nonEmpty(CusCorpApitalList)) {
                            for (CusCorpApitalDto cusCorpApitalDto : CusCorpApitalList) {
                                RptCustomerOwnerStr rptCustomerOwnerStr = new RptCustomerOwnerStr();
                                rptCustomerOwnerStr.setPkId(StringUtils.getUUID());
                                rptCustomerOwnerStr.setSerno(grpSerno);
                                rptCustomerOwnerStr.setShdCusName(cusCorpApitalDto.getInvtName());
                                rptCustomerOwnerStr.setPerc(cusCorpApitalDto.getInvtPerc());
                                rptCustomerOwnerStr.setPaidCap(cusCorpApitalDto.getInvtAmt());
                                rptCustomerOwnerStr.setInvApp(cusCorpApitalDto.getInvtType());
                                int count = insert(rptCustomerOwnerStr);
                                if (count > 0) {
                                    result.add(rptCustomerOwnerStr);
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    public int save(RptCustomerOwnerStr rptCustomerOwnerStr){
        String pkId = rptCustomerOwnerStr.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptCustomerOwnerStr);
        }else {
            rptCustomerOwnerStr.setPkId(StringUtils.getUUID());
            return  insert(rptCustomerOwnerStr);
        }
    }
}
