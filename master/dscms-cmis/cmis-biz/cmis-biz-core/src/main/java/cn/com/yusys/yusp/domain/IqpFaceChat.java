/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpFaceChat
 * @类描述: iqp_face_chat数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:19:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_face_chat")
public class IqpFaceChat extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 面谈人工号 **/
	@Column(name = "MGR_CODE", unique = false, nullable = true, length = 8)
	private String mgrCode;
	
	/** 面谈地点  STD_ZB_FACETALK_ADDR **/
	@Column(name = "FACE_TALK_ADDR", unique = false, nullable = true, length = 2)
	private String faceTalkAddr;
	
	/** 面谈地点备注 **/
	@Column(name = "ADDR_EXT", unique = false, nullable = true, length = 200)
	private String addrExt;
	
	/** 面谈时间 STD_ZB_FACETALK_TIME **/
	@Column(name = "FACE_TALK_TIME", unique = false, nullable = true, length = 10)
	private String faceTalkTime;
	
	/** 面谈方式 STD_ZB_FACETALK_TYPE **/
	@Column(name = "FACE_TALK_TYPE", unique = false, nullable = true, length = 2)
	private String faceTalkType;
	
	/** 面谈对象 STD_ZB_FACETALK_KIND **/
	@Column(name = "FACE_TALK_KIND", unique = false, nullable = true, length = 20)
	private String faceTalkKind;
	
	/** 借款人身份是否真实 **/
	@Column(name = "IS_REAL_APLYER_INFO", unique = false, nullable = true, length = 1)
	private String isRealAplyerInfo;
	
	/** 借款人的购房行为是否真实 **/
	@Column(name = "IS_REAL_BUY_HOUSE", unique = false, nullable = true, length = 1)
	private String isRealBuyHouse;
	
	/** 拟购房屋价格是否符合市场实际 **/
	@Column(name = "IS_REASONABLE_PRICE", unique = false, nullable = true, length = 1)
	private String isReasonablePrice;
	
	/** 借款人收入情况是否存在疑点 **/
	@Column(name = "IS_DOUBT_INCOME", unique = false, nullable = true, length = 1)
	private String isDoubtIncome;
	
	/** 借款人是否符合贷款条件 **/
	@Column(name = "IS_OK_LOAN_CONDI", unique = false, nullable = true, length = 1)
	private String isOkLoanCondi;
	
	/** 借款人提交资料是否齐全 **/
	@Column(name = "IS_COMPLETE_SOUCE", unique = false, nullable = true, length = 1)
	private String isCompleteSouce;
	
	/** 首付款(元) **/
	@Column(name = "FIRST_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstAmt;
	
	/** 首付款来源 STD_ZB_FIRST_AMT_FROM **/
	@Column(name = "FIRST_AMT_FROM", unique = false, nullable = true, length = 2)
	private String firstAmtFrom;
	
	/** 实际已支付首付款金额（元） **/
	@Column(name = "HAS_PAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasPayAmt;
	
	/** 贷款成数 **/
	@Column(name = "LOAN_PERCENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanPercent;
	
	/** 首付款成数 **/
	@Column(name = "FIRST_AMT_PERCENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstAmtPercent;
	
	/** 备注 **/
	@Column(name = "NOTE", unique = false, nullable = true, length = 200)
	private String note;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param mgrCode
	 */
	public void setMgrCode(String mgrCode) {
		this.mgrCode = mgrCode;
	}
	
    /**
     * @return mgrCode
     */
	public String getMgrCode() {
		return this.mgrCode;
	}
	
	/**
	 * @param faceTalkAddr
	 */
	public void setFaceTalkAddr(String faceTalkAddr) {
		this.faceTalkAddr = faceTalkAddr;
	}
	
    /**
     * @return faceTalkAddr
     */
	public String getFaceTalkAddr() {
		return this.faceTalkAddr;
	}
	
	/**
	 * @param addrExt
	 */
	public void setAddrExt(String addrExt) {
		this.addrExt = addrExt;
	}
	
    /**
     * @return addrExt
     */
	public String getAddrExt() {
		return this.addrExt;
	}
	
	/**
	 * @param faceTalkTime
	 */
	public void setFaceTalkTime(String faceTalkTime) {
		this.faceTalkTime = faceTalkTime;
	}
	
    /**
     * @return faceTalkTime
     */
	public String getFaceTalkTime() {
		return this.faceTalkTime;
	}
	
	/**
	 * @param faceTalkType
	 */
	public void setFaceTalkType(String faceTalkType) {
		this.faceTalkType = faceTalkType;
	}
	
    /**
     * @return faceTalkType
     */
	public String getFaceTalkType() {
		return this.faceTalkType;
	}
	
	/**
	 * @param faceTalkKind
	 */
	public void setFaceTalkKind(String faceTalkKind) {
		this.faceTalkKind = faceTalkKind;
	}
	
    /**
     * @return faceTalkKind
     */
	public String getFaceTalkKind() {
		return this.faceTalkKind;
	}
	
	/**
	 * @param isRealAplyerInfo
	 */
	public void setIsRealAplyerInfo(String isRealAplyerInfo) {
		this.isRealAplyerInfo = isRealAplyerInfo;
	}
	
    /**
     * @return isRealAplyerInfo
     */
	public String getIsRealAplyerInfo() {
		return this.isRealAplyerInfo;
	}
	
	/**
	 * @param isRealBuyHouse
	 */
	public void setIsRealBuyHouse(String isRealBuyHouse) {
		this.isRealBuyHouse = isRealBuyHouse;
	}
	
    /**
     * @return isRealBuyHouse
     */
	public String getIsRealBuyHouse() {
		return this.isRealBuyHouse;
	}
	
	/**
	 * @param isReasonablePrice
	 */
	public void setIsReasonablePrice(String isReasonablePrice) {
		this.isReasonablePrice = isReasonablePrice;
	}
	
    /**
     * @return isReasonablePrice
     */
	public String getIsReasonablePrice() {
		return this.isReasonablePrice;
	}
	
	/**
	 * @param isDoubtIncome
	 */
	public void setIsDoubtIncome(String isDoubtIncome) {
		this.isDoubtIncome = isDoubtIncome;
	}
	
    /**
     * @return isDoubtIncome
     */
	public String getIsDoubtIncome() {
		return this.isDoubtIncome;
	}
	
	/**
	 * @param isOkLoanCondi
	 */
	public void setIsOkLoanCondi(String isOkLoanCondi) {
		this.isOkLoanCondi = isOkLoanCondi;
	}
	
    /**
     * @return isOkLoanCondi
     */
	public String getIsOkLoanCondi() {
		return this.isOkLoanCondi;
	}
	
	/**
	 * @param isCompleteSouce
	 */
	public void setIsCompleteSouce(String isCompleteSouce) {
		this.isCompleteSouce = isCompleteSouce;
	}
	
    /**
     * @return isCompleteSouce
     */
	public String getIsCompleteSouce() {
		return this.isCompleteSouce;
	}
	
	/**
	 * @param firstAmt
	 */
	public void setFirstAmt(java.math.BigDecimal firstAmt) {
		this.firstAmt = firstAmt;
	}
	
    /**
     * @return firstAmt
     */
	public java.math.BigDecimal getFirstAmt() {
		return this.firstAmt;
	}
	
	/**
	 * @param firstAmtFrom
	 */
	public void setFirstAmtFrom(String firstAmtFrom) {
		this.firstAmtFrom = firstAmtFrom;
	}
	
    /**
     * @return firstAmtFrom
     */
	public String getFirstAmtFrom() {
		return this.firstAmtFrom;
	}
	
	/**
	 * @param hasPayAmt
	 */
	public void setHasPayAmt(java.math.BigDecimal hasPayAmt) {
		this.hasPayAmt = hasPayAmt;
	}
	
    /**
     * @return hasPayAmt
     */
	public java.math.BigDecimal getHasPayAmt() {
		return this.hasPayAmt;
	}
	
	/**
	 * @param loanPercent
	 */
	public void setLoanPercent(java.math.BigDecimal loanPercent) {
		this.loanPercent = loanPercent;
	}
	
    /**
     * @return loanPercent
     */
	public java.math.BigDecimal getLoanPercent() {
		return this.loanPercent;
	}
	
	/**
	 * @param firstAmtPercent
	 */
	public void setFirstAmtPercent(java.math.BigDecimal firstAmtPercent) {
		this.firstAmtPercent = firstAmtPercent;
	}
	
    /**
     * @return firstAmtPercent
     */
	public java.math.BigDecimal getFirstAmtPercent() {
		return this.firstAmtPercent;
	}
	
	/**
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
    /**
     * @return note
     */
	public String getNote() {
		return this.note;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}