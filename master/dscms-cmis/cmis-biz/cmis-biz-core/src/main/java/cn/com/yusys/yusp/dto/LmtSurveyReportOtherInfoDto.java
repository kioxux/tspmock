package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportOtherInfo
 * @类描述: lmt_survey_report_other_info数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-25 19:48:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyReportOtherInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 个人征信状态 **/
	private String indivCreditStatus;
	
	/** 个人征信备注 **/
	private String indivCreditRemark;
	
	/** 企业征信状态 **/
	private String corpCreditStatus;
	
	/** 企业征信备注 **/
	private String corpCreditRemark;
	
	/** 营业额检验情况说明一 **/
	private String turnoverChkDesc1;
	
	/** 营业额检验情况说明二 **/
	private String turnoverChkDesc2;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param indivCreditStatus
	 */
	public void setIndivCreditStatus(String indivCreditStatus) {
		this.indivCreditStatus = indivCreditStatus == null ? null : indivCreditStatus.trim();
	}
	
    /**
     * @return IndivCreditStatus
     */	
	public String getIndivCreditStatus() {
		return this.indivCreditStatus;
	}
	
	/**
	 * @param indivCreditRemark
	 */
	public void setIndivCreditRemark(String indivCreditRemark) {
		this.indivCreditRemark = indivCreditRemark == null ? null : indivCreditRemark.trim();
	}
	
    /**
     * @return IndivCreditRemark
     */	
	public String getIndivCreditRemark() {
		return this.indivCreditRemark;
	}
	
	/**
	 * @param corpCreditStatus
	 */
	public void setCorpCreditStatus(String corpCreditStatus) {
		this.corpCreditStatus = corpCreditStatus == null ? null : corpCreditStatus.trim();
	}
	
    /**
     * @return CorpCreditStatus
     */	
	public String getCorpCreditStatus() {
		return this.corpCreditStatus;
	}
	
	/**
	 * @param corpCreditRemark
	 */
	public void setCorpCreditRemark(String corpCreditRemark) {
		this.corpCreditRemark = corpCreditRemark == null ? null : corpCreditRemark.trim();
	}
	
    /**
     * @return CorpCreditRemark
     */	
	public String getCorpCreditRemark() {
		return this.corpCreditRemark;
	}
	
	/**
	 * @param turnoverChkDesc1
	 */
	public void setTurnoverChkDesc1(String turnoverChkDesc1) {
		this.turnoverChkDesc1 = turnoverChkDesc1 == null ? null : turnoverChkDesc1.trim();
	}
	
    /**
     * @return TurnoverChkDesc1
     */	
	public String getTurnoverChkDesc1() {
		return this.turnoverChkDesc1;
	}
	
	/**
	 * @param turnoverChkDesc2
	 */
	public void setTurnoverChkDesc2(String turnoverChkDesc2) {
		this.turnoverChkDesc2 = turnoverChkDesc2 == null ? null : turnoverChkDesc2.trim();
	}
	
    /**
     * @return TurnoverChkDesc2
     */	
	public String getTurnoverChkDesc2() {
		return this.turnoverChkDesc2;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}