package cn.com.yusys.yusp.dto.req;

import java.io.Serializable;

public class DeleteDocDetailParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 档案销毁流水号 **/
    private String ddalSerno;

    /** 档案销毁明细流水号 **/
    private String dddlSerno;

    public String getDdalSerno() {
        return ddalSerno;
    }

    public void setDdalSerno(String ddalSerno) {
        this.ddalSerno = ddalSerno;
    }

    public String getDddlSerno() {
        return dddlSerno;
    }

    public void setDddlSerno(String dddlSerno) {
        this.dddlSerno = dddlSerno;
    }
}
