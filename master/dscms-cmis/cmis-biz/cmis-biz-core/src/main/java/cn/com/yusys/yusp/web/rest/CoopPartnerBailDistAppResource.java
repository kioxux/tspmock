/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPartnerBailDistApp;
import cn.com.yusys.yusp.service.CoopPartnerBailDistAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerBailDistAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 13:14:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cooppartnerbaildistapp")
public class CoopPartnerBailDistAppResource {
    @Autowired
    private CoopPartnerBailDistAppService coopPartnerBailDistAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPartnerBailDistApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPartnerBailDistApp> list = coopPartnerBailDistAppService.selectAll(queryModel);
        return new ResultDto<List<CoopPartnerBailDistApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPartnerBailDistApp>> index(QueryModel queryModel) {
        List<CoopPartnerBailDistApp> list = coopPartnerBailDistAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerBailDistApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPartnerBailDistApp>> query(@RequestBody QueryModel queryModel) {
        List<CoopPartnerBailDistApp> list = coopPartnerBailDistAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerBailDistApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<CoopPartnerBailDistApp> show(@PathVariable("serno") String serno) {
        CoopPartnerBailDistApp coopPartnerBailDistApp = coopPartnerBailDistAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopPartnerBailDistApp>(coopPartnerBailDistApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPartnerBailDistApp> create(@RequestBody CoopPartnerBailDistApp coopPartnerBailDistApp) throws URISyntaxException {
        coopPartnerBailDistAppService.insert(coopPartnerBailDistApp);
        return new ResultDto<CoopPartnerBailDistApp>(coopPartnerBailDistApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPartnerBailDistApp coopPartnerBailDistApp) throws URISyntaxException {
        int result = coopPartnerBailDistAppService.updateSelective(coopPartnerBailDistApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopPartnerBailDistAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPartnerBailDistAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 zhaoxp
     * @创建时间 13:56 2021-04-15
     * @return 测试接口
     **/
//    @PostMapping("/selectBailInfo")
//    protected Map findOne123(@RequestBody Map map) {
////      String newName="查询的企业是"+name+";功能开发中:等待查询企业相关接口研发";
//        Map newMap=new HashMap();
//        newMap.put("bail_acc_no_bal",300);
//        newMap.put("curt_grt_bal",200);
//        newMap.put("allow_dist_bail_amt",100);
//        return newMap;
//    }

    /**
     * @函数名称:queryBailAcctInfo
     * @函数描述:根据账号查询核心账户信息
     * @参数与返回说明:
     * @算法描述: 账户信息查询
     */
    @PostMapping("/queryBailAcctInfo")
    protected ResultDto<Ib1253RespDto> queryBailAcctInfo(@RequestBody Map<String, Object> map) {
        return ResultDto.success(coopPartnerBailDistAppService.queryBailAcctInfo(map));
    }

}
