package cn.com.yusys.yusp.service.server.xdxw0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtEgcInfo;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.xdxw0006.req.List;
import cn.com.yusys.yusp.dto.server.xdxw0006.req.Xdxw0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0006.resp.Xdxw0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtEgcInfoMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Optional;
import java.util.UUID;

@Service
public class Xdxw0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0006Service.class);

    @Resource
    private LmtEgcInfoMapper lmtEgcInfoMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 联系人信息维护
     *
     * @param xdxw0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0006DataRespDto getXdxw0006(Xdxw0006DataReqDto xdxw0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0006DataReqDto));
        Xdxw0006DataRespDto xdxw0006DataRespDto = new Xdxw0006DataRespDto();
        try {
            String surveySerno = xdxw0006DataReqDto.getSurveySerno();//批复号
            java.util.List<List> list = xdxw0006DataReqDto.getList();
            // 非空校验
            if (StringUtils.isBlank(surveySerno) || list.size() == 0) {
                throw new YuspException(EcbEnum.E_GUAR_PARAM_EXPCETION.key, EcbEnum.E_GUAR_PARAM_EXPCETION.value);
            }
            // 删除
            lmtEgcInfoMapper.deleteBySurveySerno(surveySerno);
            for (int i = 0; i < list.size(); i++) {
                String cusName = list.get(i).getCusName();//联系人姓名
                String male = list.get(i).getMale();//性别
                String country = list.get(i).getCountry();//国籍
                String certType = list.get(i).getCertType();//证件类型
                String certNo = list.get(i).getCertNo();//证件号
                String certDate = list.get(i).getCertDate();//证件到期日
                String tel = list.get(i).getTel();//联系电话
                String areaName = list.get(i).getAreaName();//联系人地址
                String relationShip = list.get(i).getRelationShip();//关系
                String job = list.get(i).getJob();//职业

                // 证件号查询客户号
                String cusId = StringUtils.EMPTY;
                if (StringUtil.isNotEmpty(certNo)) {
                    CusBaseClientDto cusInfoDto = Optional.ofNullable(cmisCusClientService.queryCusByCertCode(certNo)).orElse(new CusBaseClientDto());
                    cusId = cusInfoDto.getCusId(); //客户号
                }

                // 准备数据
                LmtEgcInfo lmtEgcInfo = new LmtEgcInfo();
                String pkId = UUID.randomUUID().toString().replace("-", "");
                lmtEgcInfo.setPkId(pkId);
                lmtEgcInfo.setSurveySerno(surveySerno);//调查流水号
                lmtEgcInfo.setCusName(cusName);//客户名称
                lmtEgcInfo.setCusId(cusId);//客户号
                lmtEgcInfo.setLinkPhone(tel);//联系人电话
                lmtEgcInfo.setWorkUnit(job);//工作单位
                lmtEgcInfo.setCommonDebitRela(relationShip);//共借人关系
                // 新增
                logger.info("********XDXW0008*INSERT*插入lmtEgcInfo开始,新增参数为:{}", JSON.toJSONString(lmtEgcInfo));
                lmtEgcInfoMapper.insert(lmtEgcInfo);
            }

            xdxw0006DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xdxw0006DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdxw0006DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0006DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataRespDto));
        return xdxw0006DataRespDto;
    }
}
