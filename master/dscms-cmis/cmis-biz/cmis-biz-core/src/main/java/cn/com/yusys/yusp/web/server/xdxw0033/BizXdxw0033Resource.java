package cn.com.yusys.yusp.web.server.xdxw0033;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0033.req.Xdxw0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0033.resp.Xdxw0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0033.Xdxw0033Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:根据流水号查询征信报告关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0033:根据流水号查询征信报告关联信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0033Resource.class);

    @Resource
    private Xdxw0033Service xdxw0033Service;

    /**
     * 交易码：xdxw0033
     * 交易描述：根据流水号查询征信报告关联信息
     *
     * @param xdxw0033DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询征信报告关联信息")
    @PostMapping("/xdxw0033")
    protected @ResponseBody
    ResultDto<Xdxw0033DataRespDto> xdxw0033(@Validated @RequestBody Xdxw0033DataReqDto xdxw0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataReqDto));
        Xdxw0033DataRespDto xdxw0033DataRespDto = new Xdxw0033DataRespDto();// 响应Dto:根据流水号查询征信报告关联信息
        ResultDto<Xdxw0033DataRespDto> xdxw0033DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataReqDto));
            xdxw0033DataRespDto = xdxw0033Service.getXdxw0033(xdxw0033DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataRespDto));
            // 封装xdxw0033DataResultDto中正确的返回码和返回信息
            xdxw0033DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0033DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, e.getMessage());
            xdxw0033DataResultDto.setCode(e.getErrorCode());
            xdxw0033DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, e.getMessage());
            // 封装xdxw0033DataResultDto中异常返回码和返回信息
            xdxw0033DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0033DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0033DataRespDto到xdxw0033DataResultDto中
        xdxw0033DataResultDto.setData(xdxw0033DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataResultDto));
        return xdxw0033DataResultDto;
    }
}
