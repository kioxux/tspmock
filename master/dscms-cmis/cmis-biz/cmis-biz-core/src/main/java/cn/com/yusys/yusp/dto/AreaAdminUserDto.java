package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaAdminUser
 * @类描述: area_admin_user数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-02 17:26:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AreaAdminUserDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 用户号 **/
	private String userNo;
	
	/** 调查模式 **/
	private String surveyMode;
	
	/** 是否派遣员工 **/
	private String isDispatchEmployee;
	
	/** 是否新员工 **/
	private String isNewEmployee;
	
	/** 直营团队类型 **/
	private String teamType;
	
	/** 小微线上产品权限 **/
	private String onlinePrdManager;
	
	/** 大额信用权限 **/
	private String isLargeCredit;
	
	/** 机构代码 **/
	private String orgCode;
	
	/** 机构名称 **/
	private String orgName;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param userNo
	 */
	public void setUserNo(String userNo) {
		this.userNo = userNo == null ? null : userNo.trim();
	}
	
    /**
     * @return UserNo
     */	
	public String getUserNo() {
		return this.userNo;
	}
	
	/**
	 * @param surveyMode
	 */
	public void setSurveyMode(String surveyMode) {
		this.surveyMode = surveyMode == null ? null : surveyMode.trim();
	}
	
    /**
     * @return SurveyMode
     */	
	public String getSurveyMode() {
		return this.surveyMode;
	}
	
	/**
	 * @param isDispatchEmployee
	 */
	public void setIsDispatchEmployee(String isDispatchEmployee) {
		this.isDispatchEmployee = isDispatchEmployee == null ? null : isDispatchEmployee.trim();
	}
	
    /**
     * @return IsDispatchEmployee
     */	
	public String getIsDispatchEmployee() {
		return this.isDispatchEmployee;
	}
	
	/**
	 * @param isNewEmployee
	 */
	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee == null ? null : isNewEmployee.trim();
	}
	
    /**
     * @return IsNewEmployee
     */	
	public String getIsNewEmployee() {
		return this.isNewEmployee;
	}
	
	/**
	 * @param teamType
	 */
	public void setTeamType(String teamType) {
		this.teamType = teamType == null ? null : teamType.trim();
	}
	
    /**
     * @return TeamType
     */	
	public String getTeamType() {
		return this.teamType;
	}
	
	/**
	 * @param onlinePrdManager
	 */
	public void setOnlinePrdManager(String onlinePrdManager) {
		this.onlinePrdManager = onlinePrdManager == null ? null : onlinePrdManager.trim();
	}
	
    /**
     * @return OnlinePrdManager
     */	
	public String getOnlinePrdManager() {
		return this.onlinePrdManager;
	}
	
	/**
	 * @param isLargeCredit
	 */
	public void setIsLargeCredit(String isLargeCredit) {
		this.isLargeCredit = isLargeCredit == null ? null : isLargeCredit.trim();
	}
	
    /**
     * @return IsLargeCredit
     */	
	public String getIsLargeCredit() {
		return this.isLargeCredit;
	}
	
	/**
	 * @param orgCode
	 */
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode == null ? null : orgCode.trim();
	}
	
    /**
     * @return OrgCode
     */	
	public String getOrgCode() {
		return this.orgCode;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName == null ? null : orgName.trim();
	}
	
    /**
     * @return OrgName
     */	
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}