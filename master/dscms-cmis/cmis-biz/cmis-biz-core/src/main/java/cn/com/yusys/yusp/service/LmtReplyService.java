/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtReplyAccSubDto;
import cn.com.yusys.yusp.dto.LmtReplyAccSubPrdDto;
import cn.com.yusys.yusp.dto.LmtReplySubDto;
import cn.com.yusys.yusp.dto.LmtReplySubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.management.Query;
import java.math.BigDecimal;
import java.util.*;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class    LmtReplyService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyService.class);

    @Resource
    private LmtReplyMapper lmtReplyMapper;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReply selectByPrimaryKey(String pkId) {
        return lmtReplyMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReply> selectAll(QueryModel model) {
        List<LmtReply> records = (List<LmtReply>) lmtReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReply> list = lmtReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReply record) {
        return lmtReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReply record) {
        return lmtReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(LmtReply record) {
        return lmtReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReply record) {
        return lmtReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyMapper.deleteByIds(ids);
    }


    /**
     * @方法名称：qryReplyNo
     * @方法描述：查询可变更的批复号
     * @参数与返回说明：
     * @算法描述：当前客户经理下的所有批复，再经过所有合同申请表过滤，剩下的是可变更的批复号
     * @创建人：zhangming12
     * @创建时间：2021-04-17 下午 4:46
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReply> qryChangeableReply(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReply> list = lmtReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：queryLmtReplyBySerno
     * @方法描述 通过授信流水号查询最新的授信审批数据
     * @参数与返回说明：
     * @算法描述：根据批复生效日期倒序查询最新一笔数据
     * @创建人：zhangming12
     * @创建时间：2021-04-17 下午 4:46
     * @修改记录：修改时间 修改人员  修改原因
     */
    public LmtReply queryLmtReplyBySerno(String serno) throws Exception {
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("serno", serno);
        lmtReplyQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 更新日期倒排
        lmtReplyQueryMap.put("orderBy", " UPDATE_TIME desc");
        List<LmtReply> lmtReplyList = queryLmtReplyDataByParams(lmtReplyQueryMap);
        LmtReply lmtReply = null;
        if (lmtReplyList != null && lmtReplyList.size() == 1) {
            lmtReply = lmtReplyList.get(0);
        } else {
            throw new Exception("授信批复数据获取异常");
        }
        return lmtReply;
    }


    /**
     * @方法名称：queryLmtReplyByReplySerno
     * @方法描述 通过批复号查询批复数据
     * @参数与返回说明：
     * @算法描述：根据批复生效日期倒序查询最新一笔数据
     * @创建人：mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录：修改时间 修改人员  修改原因
     */
    public LmtReply queryLmtReplyByReplySerno(String replySerno) {
        return this.querySingleLmtReplyByReplySerno(replySerno);

    }

    /**
     * @方法名称：querySingleLmtReplyByReplySerno
     * @方法描述：通过参数查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-04-22 下午 4:46
     * @修改记录：修改时间 修改人员  修改原因
     */
    public LmtReply querySingleLmtReplyByReplySerno(String replySerno) {
        return lmtReplyMapper.querySingleLmtReplyByReplySerno(replySerno);
    }

    /**
     * @方法名称：queryLmtReplyDataByParams
     * @方法描述：通过参数查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-04-22 下午 4:46
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReply> queryLmtReplyDataByParams(HashMap<String, String> paramMap) {
        return lmtReplyMapper.queryLmtReplyDataByParams(paramMap);
    }

    /**
     * @方法名称: 生成批复
     * @方法描述: 通过授信审批数据生成授信批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    @Transactional
    public String generateLmtReplyHandleByLmtAppr(LmtAppr lmtAppr, String currentUserId, String currentOrgId) throws Exception {
        if (!CmisCommonConstants.LMT_TYPE_01.equals(lmtAppr.getLmtType()) && !CmisCommonConstants.LMT_TYPE_06.equals(lmtAppr.getLmtType())) {
            log.info("单一授信申请审批通过结束逻辑处理 -> 注销原批复");
            // 其他类型先终止原来批复，生成新的批复。
            invalidatedOldLmtReply(lmtAppr);
        }
        log.info("单一授信申请审批通过结束逻辑处理 -> 生成新的批复");
        // 生成新的批复
        return generateLmtReplyInfoByLmtAppr(lmtAppr, currentUserId, currentOrgId);
    }

    /**
     * @方法名称: 生成批复
     * @方法描述: 通过授信申请数据生成授信批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-16 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    @Transactional
    public void generateLmtReplyHandleByLmtApp(LmtApp lmtApp, String currentUserId, String currentOrgId, String serno) throws Exception {
        // 根据授信申请流水获取授信批复数据
        HashMap<String, String> lmtReplyQueryMap = new HashMap<String, String>();
        lmtReplyQueryMap.put("serno", serno);
        // 更新日期倒排
        lmtReplyQueryMap.put("orderBy", " UPDATE_TIME desc");
        List<LmtReply> lmtReplyList = queryLmtReplyDataByParams(lmtReplyQueryMap);
        LmtReply lmtReply = null;
        if (lmtReplyList != null && lmtReplyList.size() == 1) {
            lmtReply = lmtReplyList.get(0);
        } else {
            throw new Exception("授信批复数据获取异常");
        }
        // 获取授信批复流水
        String replySerno = lmtReply.getReplySerno();
        // 2 获取授信申请分项并生成授信批复分项和分项适用品种
        lmtReplySubService.generateLmtReplySubByLmtAppSub(lmtApp, currentUserId, currentOrgId, replySerno);
    }

    /**
     * @方法名称: generateNewLmtReply
     * @方法描述: 通过审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtReplyInfoByLmtAppr(LmtAppr lmtAppr, String currentUserId, String currentOrgId) throws Exception {
        // 生成批复数据
        String replySerno = generateNewLmtReplyByLmtAppr(lmtAppr, currentUserId, currentOrgId);
        // 2 获取审批授信分项并生成授信批复分项和分项适用品种
        lmtReplySubService.generateNewLmtReplySubByLmtAppr(lmtAppr, currentUserId, currentOrgId, replySerno);
        // 4 生成批复条件
        lmtReplyLoanCondService.generateNewLmtReplyLoanCondByLmtAppr(lmtAppr, replySerno);
        return replySerno;
    }


    /**
     * @方法名称: generateNewLmtReplyByLmtAppr
     * @方法描述: 通过授信审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateNewLmtReplyByLmtAppr(LmtAppr lmtAppr, String currentUserId, String currentOrgId) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 1.根据授信审批的记录生成并组装授信批复数据
        String repaySerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        log.info("单一授信申请审批通过结束逻辑处理 -> 生成新的批复,新批复流水号【{}】", repaySerno);
        LmtReply lmtReply = new LmtReply();
        BeanUtils.copyProperties(lmtAppr, lmtReply);
        lmtReply.setPkId(UUID.randomUUID().toString());
        lmtReply.setCusId(lmtAppr.getCusId());
        lmtReply.setReplySerno(repaySerno);
        // 从审批表中获取
        lmtReply.setLoanApprMode(lmtAppr.getLoanApprMode());

        String consignorCusId = "";
        String consignorCusType = "";
        String consignorCertType = "";
        String consignorCertCode = "";
        List<LmtApprSub> lmtApprSubs = lmtApprSubService.selectAllByApprSerno(lmtAppr.getApproveSerno());
        for (LmtApprSub lmtApprSub : lmtApprSubs) {
            List<LmtApprSubPrd> lmtApprPrdSubs = lmtApprSubPrdService.selectBySubSerno(lmtApprSub.getApproveSubSerno());
            for (LmtApprSubPrd lmtApprSubPrd : lmtApprPrdSubs) {
                if("140203".equals(lmtApprSubPrd.getLmtBizType()) || "14020301".equals(lmtApprSubPrd.getLmtBizType())
                    || "200401".equals(lmtApprSubPrd.getLmtBizType()) || "20040101".equals(lmtApprSubPrd.getLmtBizType())){
                    consignorCusId = lmtApprSubPrd.getConsignorCusId();
                    consignorCusType = lmtApprSubPrd.getConsignorType();
                    consignorCertType = lmtApprSubPrd.getConsignorCertType();
                    consignorCertCode = lmtApprSubPrd.getConsignorCertCode();
                }
            }
        }
        lmtReply.setConsignorCusId(consignorCusId);
        lmtReply.setConsignorType(consignorCusType);
        lmtReply.setConsignorCertType(consignorCertType);
        lmtReply.setConsignorCertCode(consignorCertCode);
        lmtReply.setReplyInureDate(openDay);
        // 批复结论
        LmtApp lmtApp = lmtAppService.selectBySerno(lmtAppr.getSerno());
        lmtReply.setApprResult(lmtApp.getApproveStatus());
        lmtReply.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
        // 设置终审机构 目前都是总行审批
        lmtReply.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        lmtReply.setInputId(lmtApp.getManagerId());
        lmtReply.setInputBrId(lmtApp.getManagerBrId());
        lmtReply.setInputDate(openDay);
        lmtReply.setUpdId(currentUserId);
        lmtReply.setUpdBrId(currentOrgId);
        lmtReply.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtReply.setManagerId(lmtApp.getManagerId());
        lmtReply.setManagerBrId(lmtApp.getManagerBrId());
        lmtReply.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtReply.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(lmtReply);
        log.info("生成新的授信批复:" + JSON.toJSONString(lmtReply));
        return repaySerno;
    }


    /**
     * @方法名称:
     * @方法描述: 终止批复
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void invalidatedOldLmtReply(LmtAppr lmtAppr) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("replySerno", lmtAppr.getOrigiLmtReplySerno());
        List<LmtReply> lmtReplyList = this.queryLmtReplyDataByParams(queryMap);
        if (lmtReplyList != null && lmtReplyList.size() == 1) {
            LmtReply oldLmtReply = lmtReplyList.get(0);
            oldLmtReply.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_02);
            this.update(oldLmtReply);
            log.info("单一授信申请审批通过结束逻辑处理 -> 注销原批复成功,原批复流水号【{}】", lmtAppr.getOrigiLmtReplySerno());
        } else {
            throw new Exception("查询原批复数据异常！");
        }
    }

    /**
     * @方法名称：getOriginReply
     * @方法描述：台账里的批复号取原批复 批复历史沿革
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-19 上午 11:17
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReply> getOriginReply(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        Map<String, Object> condition = queryModel.getCondition();
        String accNo = (String) condition.get("accNo");
        String replySerno = (String) condition.get("replySerno");
        List<LmtReply> lmtReplies = new ArrayList<>();
        if ((StringUtils.isEmpty(accNo) && StringUtils.isEmpty(replySerno))) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        if(StringUtils.isEmpty(replySerno)) {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(accNo);
            replySerno = lmtReplyAcc.getReplySerno();
        }

        //现在台账对应的批复
        LmtReply lmtReply = lmtReplyMapper.getReply(replySerno);
        if(lmtReply == null){
            throw BizException.error(null, null,"未查询到对应的授信批复");
        }
        lmtReplies.add(lmtReply);
        String origiLmtReplySerno = lmtReply.getOrigiLmtReplySerno();
        int count = 0;
        while (!(origiLmtReplySerno == null || "".equals(origiLmtReplySerno) || replySerno.equals(origiLmtReplySerno))) {
            lmtReply = lmtReplyMapper.getOriginReply(origiLmtReplySerno);
            if (lmtReply == null) {
                break;
            }
            if (count > 100){
                break;
            }
            lmtReplies.add(lmtReply);
            origiLmtReplySerno = lmtReply.getOrigiLmtReplySerno();
            count ++;
        }
        PageHelper.clearPage();
        return lmtReplies;
    }


    /**
     * @方法名称：getAllReplyInfo
     * @方法描述：获取批复所有信息包括分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-21 下午 9:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map getAllReplyInfo(String replySerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        LmtReply lmtReply = null;
        List<LmtReplyLoanCond> lmtReplyLoanConds = null;
        List<LmtReplySubDto> lmtReplySubDtoList = null;
        try {
            if (StringUtils.isBlank(replySerno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }

            QueryModel model = new QueryModel();
            model.addCondition("replySerno", replySerno);
            List<LmtReply> lmtReplyList = lmtReplyMapper.selectByModel(model);
            if(CollectionUtils.isEmpty(lmtReplyList)){
                rtnCode = EcbEnum.ECB020012.key;
                rtnMsg = EcbEnum.ECB020012.value;
                return rtnData;
            }
            lmtReply = lmtReplyList.get(0);
            lmtReplyLoanConds = lmtReplyLoanCondService.selectByModel(model);
            List<LmtReplySub> replySubList = lmtReplySubService.selectByModel(model);
            if (CollectionUtils.nonEmpty(replySubList)) {
                lmtReplySubDtoList = (List<LmtReplySubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubList, LmtReplySubDto.class);
                lmtReplySubDtoList.forEach(lmtReplySubDto -> {
                    String replySubSerno = lmtReplySubDto.getReplySubSerno();
                    String subName = lmtReplySubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtReplySubDto.setLmtDrawType(subName);
                    }
                    if (!StringUtils.isBlank(replySubSerno)) {
                        lmtReplySubDto.setLmtDrawNo(replySubSerno);
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("replySubSerno", replySubSerno);
                        List<LmtReplySubPrd> replySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(queryPrdMap);
                        List<LmtReplySubPrdDto> lmtReplySubPrdDtos = (List<LmtReplySubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(replySubPrdList, LmtReplySubPrdDto.class);
                        for (LmtReplySubPrdDto lmtReplySubPrdDto : lmtReplySubPrdDtos) {
                            String bizTypeName = lmtReplySubPrdDto.getLmtBizTypeName();
                            String replySubPrdSerno = lmtReplySubPrdDto.getReplySubPrdSerno();
                            if (StringUtils.nonBlank(bizTypeName)) {
                                lmtReplySubPrdDto.setLmtDrawType(bizTypeName);
                            }
                            if (StringUtils.nonBlank(replySubPrdSerno)) {
                                lmtReplySubPrdDto.setLmtDrawNo(replySubPrdSerno);
                            }
                        }
                        lmtReplySubDto.setChildren(lmtReplySubPrdDtos);
                    }
                });
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtReply", lmtReply);
            rtnData.put("lmtReplySubDtoList", lmtReplySubDtoList);
            rtnData.put("lmtReplyLoanConds", lmtReplyLoanConds);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：获取当前登录人下的批复信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-29 上午 9:04
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReply> queryByManagerId() {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        if (StringUtils.isBlank(managerId)) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("managerId", managerId);
        return lmtReplyMapper.queryLmtReplyDataByParams(paramMap);
    }

    /**
     * @方法名称: updateLmtReplyHandleByLmtReplyChg
     * @方法描述: 通过授信审批数据生成授信批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    @Transactional
    public void updateLmtReplyHandleByLmtReplyChg(LmtReplyChg lmtReplyChg, String currentUserId, String currentOrgId) throws Exception {
        // 通过授信批复变更生成批复
        //this.generateNewLmtReplyByLmtReplyChg(lmtReplyChg, currentUserId, currentOrgId);
        // 获取批复变更授信分项并更新授信批复分项和分项适用品种
        //lmtReplySubService.updateLmtReplySubByLmtReplyChg(lmtReplyChg, currentUserId, currentOrgId);
        // 更新批复条件
        LmtReply lmtReply = lmtReplyMapper.getReply(lmtReplyChg.getReplySerno());
        lmtReply.setPspManaNeed(lmtReplyChg.getPspManaNeed());
        this.update(lmtReply);
        lmtReplyLoanCondService.updateLmtReplyLoanCondByLmtReplyChg(lmtReplyChg);
    }


    /**
     * @方法名称: generateNewLmtReplyByLmtReplyChg
     * @方法描述: 通过授信审批数据生成批复数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplyByLmtReplyChg(LmtReplyChg lmtReplyChg, String currentUserId, String currentOrgId) throws Exception {
        //批复流水号
        String replySerno = lmtReplyChg.getReplySerno();
        log.info("批复流水号【"+replySerno+"】,根据授信批复变更信息更新对应的授信批复信息开始");
        LmtReply lmtReply = this.queryLmtReplyByReplySerno(replySerno);
        // 深拷贝一个对象 使用该对象进行更新
        LmtReply updateLmtReply = (LmtReply) SerializationUtils.clone(lmtReply);
        BeanUtils.copyProperties(lmtReplyChg, updateLmtReply);
        //主键、批复流水号、申请流水号、登记人、登记机构、登记日期、创建时间不变
        updateLmtReply.setPkId(lmtReply.getPkId());
        updateLmtReply.setSerno(lmtReply.getSerno());
        updateLmtReply.setReplySerno(lmtReply.getReplySerno());
        updateLmtReply.setCreateTime(lmtReply.getCreateTime());
        updateLmtReply.setInputId(lmtReply.getInputId());
        updateLmtReply.setInputBrId(lmtReply.getInputBrId());
        updateLmtReply.setInputDate(lmtReply.getInputDate());

        updateLmtReply.setUpdId(currentUserId);
        updateLmtReply.setUpdBrId(currentOrgId);
        updateLmtReply.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        updateLmtReply.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.update(updateLmtReply);
        log.info("批复流水号【"+replySerno+"】,根据授信批复变更信息更新对应的授信批复信息结束");
    }

    /**
     * @方法名称:
     * @方法描述: 集团子成员批复终止
     * @参数与返回说明:
     * @算法描述: 通过原批复编号查询到原批复，更新原批复状态
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void invalidatedOldLmtReplyForLmtGrpReply(String origiLmtReplySerno) throws Exception {
        List<LmtReply> lmtReplyList = lmtReplyMapper.queryLmtReplyDataByLmtGrpSerno(origiLmtReplySerno);
        if (lmtReplyList != null && lmtReplyList.size() > 0) {
            for (LmtReply lmtReply : lmtReplyList) {
                lmtReply.setReplyStatus(CmisLmtConstants.STD_ZB_LMT_STATE_03);
                this.update(lmtReply);
                log.info("续作时，如果新加入的客户存在生效的单一授信台账，则同步失效！");
                if(lmtReply.getLmtType().equals(CmisCommonConstants.LMT_TYPE_03)){
                    log.info("续作判断进入！");
                    LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(lmtReply.getCusId());
                    log.info("查询客户对应的生效的批复台账："+JSON.toJSONString(lmtReplyAcc));
                    if(lmtReplyAcc != null && lmtReplyAcc.getIsGrp().equals(CmisCommonConstants.YES_NO_0)){
                        log.info("存在生效的批复台账判断进入！");
                        lmtReplyAcc.setAccStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_02);
                        lmtReplyAccService.update(lmtReplyAcc);
                    }
                }
            }
        } else {
            throw new Exception("查询成员原批复数据异常！");
        }
    }

    /**
     * @方法名称:
     * @方法描述: 集团子成员批复生成
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateNewLmtReplyByLmtApprForLmtGrpReply(String singleSerno, String currentUserId, String currentOrgId) throws Exception {
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprByApproveSerno(singleSerno);
        return generateLmtReplyHandleByLmtAppr(lmtAppr, currentUserId, currentOrgId);
    }

    /**
     * @方法名称：selectByCusId
     * @方法描述：当前客户下的批复
     * @创建人：zhangming12
     * @创建时间：2021/5/24 9:54
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<LmtReply> selectByCusId(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        if (queryModel.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String cusId = (String) queryModel.getCondition().get("cusId");
        if (StringUtils.isBlank(cusId)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", "01");
        queryMap.put("cusId", cusId);
        PageHelper.clearPage();
        return this.queryLmtReplyDataByParams(queryMap);
    }

    /**
     * @方法名称：selectOpenTotalLmtAmtByCusId
     * @方法描述：查询客户的敞口额度合计
     * @创建人：dumingdi
     * @创建时间：2021/9/23 22:54
     * @修改记录：修改时间 修改人员 修改时间
     */
    public BigDecimal selectOpenTotalLmtAmtByCusId(String cusId) {
        BigDecimal openTotalLmtAmt = BigDecimal.ZERO;

        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", "01");
        queryMap.put("cusId", cusId);
        List<LmtReply> lmtReplyList = queryLmtReplyDataByParams(queryMap);

        if (CollectionUtils.nonEmpty(lmtReplyList)){
            openTotalLmtAmt = lmtReplyList.get(0).getOpenTotalLmtAmt();
        }

        return openTotalLmtAmt;
    }
    /**
     * 根据集团流水号查询本次申报成员授信批复信息
     *
     * @param grpSerno
     * @return
     */
    public List<LmtReply> getLmtReplyByGrpSernoIsDeclare(String grpSerno){
        return lmtReplyMapper.getLmtReplyByGrpSernoIsDeclare(grpSerno);
    }
}
