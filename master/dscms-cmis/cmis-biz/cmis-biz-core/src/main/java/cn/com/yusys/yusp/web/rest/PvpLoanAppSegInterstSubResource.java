/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.PvpLoanApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpLoanAppSegInterstSub;
import cn.com.yusys.yusp.service.PvpLoanAppSegInterstSubService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppSegInterstSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-21 10:25:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "分段计息")
@RequestMapping("/api/pvploanappseginterstsub")
public class PvpLoanAppSegInterstSubResource {
    @Autowired
    private PvpLoanAppSegInterstSubService pvpLoanAppSegInterstSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpLoanAppSegInterstSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubService.selectAll(queryModel);
        return new ResultDto<List<PvpLoanAppSegInterstSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpLoanAppSegInterstSub>> index(QueryModel queryModel) {
        List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppSegInterstSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpLoanAppSegInterstSub> show(@PathVariable("pkId") String pkId) {
        PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub = pvpLoanAppSegInterstSubService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpLoanAppSegInterstSub>(pvpLoanAppSegInterstSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpLoanAppSegInterstSub> create(@RequestBody PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) throws URISyntaxException {
        pvpLoanAppSegInterstSubService.insert(pvpLoanAppSegInterstSub);
        return new ResultDto<PvpLoanAppSegInterstSub>(pvpLoanAppSegInterstSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) throws URISyntaxException {
        int result = pvpLoanAppSegInterstSubService.update(pvpLoanAppSegInterstSub);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpLoanAppSegInterstSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpLoanAppSegInterstSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestParam("pkId") String pkId) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpLoanAppSegInterstSub temp = new PvpLoanAppSegInterstSub();
        temp.setPkId(pkId);
        PvpLoanAppSegInterstSub studyDemo = pvpLoanAppSegInterstSubService.selectByPrimaryKey(pkId);
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * 通用的保存方法
     * @param pvpLoanAppSegInterstSub
     * @return
     */
    @PostMapping("/commonupdateseginterstsubinfo")
    public ResultDto<Map> commonUpdateSegInterstSub(@RequestBody PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub){
        Map rtnData = pvpLoanAppSegInterstSubService.commonUpdateSegInterstSubInfo(pvpLoanAppSegInterstSub);
        return new ResultDto<>(rtnData);
    }

    /**
     * 贷款出账申请分段计息明细新增页面的保存
     *
     * @param pvpLoanAppSegInterstSub
     * @return
     */
    @PostMapping("/savepvpLoanappseginterstsub")
    protected ResultDto<Map> savePvpLoanAppSegInterstSub(@RequestBody PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) throws URISyntaxException {
        Map result = pvpLoanAppSegInterstSubService.savePvpLoanAppSegInterstSub(pvpLoanAppSegInterstSub);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("分段计息分页查询")
    @PostMapping("/querySegInterstSub")
    protected ResultDto<List<PvpLoanAppSegInterstSub>> querySegInterstSub(@RequestBody QueryModel queryModel) {
        List<PvpLoanAppSegInterstSub> list = pvpLoanAppSegInterstSubService.querySegInterstSub(queryModel);
        return new ResultDto<List<PvpLoanAppSegInterstSub>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("分段计息新增")
    @PostMapping("/saveSegInterstSub")
    protected ResultDto<Map> saveSegInterstSub(@RequestBody PvpLoanAppSegInterstSub pvpLoanAppSegInterstSub) throws URISyntaxException {
        ResultDto<PvpLoanAppSegInterstSub> resultDto = new ResultDto<PvpLoanAppSegInterstSub>();
        Map result = pvpLoanAppSegInterstSubService.saveSegInterstSub(pvpLoanAppSegInterstSub);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:params
     * @函数描述:分段计息查看
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("分段计息查看")
    @PostMapping("/showdetialsub")
    protected ResultDto<Object> showDetialSub(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpLoanAppSegInterstSub temp = new PvpLoanAppSegInterstSub();
        PvpLoanAppSegInterstSub studyDemo = pvpLoanAppSegInterstSubService.selectByPrimaryKey((String)params.get(("pkId")));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }
}