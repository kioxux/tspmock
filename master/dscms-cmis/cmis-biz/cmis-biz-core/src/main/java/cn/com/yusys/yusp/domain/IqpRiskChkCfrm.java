/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkCfrm
 * @类描述: iqp_risk_chk_cfrm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 15:04:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_risk_chk_cfrm")
public class IqpRiskChkCfrm extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 32)
	private String iqpSerno;
	
	/** 场景 STD_ZB_BANK_KIND **/
	@Column(name = "SECEN", unique = false, nullable = true, length = 5)
	private String secen;
	
	/** 借款人收入认定意见 **/
	@Column(name = "BORR_INCOME_NOTE", unique = false, nullable = true, length = 400)
	private String borrIncomeNote;
	
	/** 物业管理费认定 **/
	@Column(name = "PMF_AR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pmfAr;
	
	/** 借款人负债认定意见 **/
	@Column(name = "BORR_DEBT_NOTE", unique = false, nullable = true, length = 400)
	private String borrDebtNote;
	
	/** 信用风险评估意见 **/
	@Column(name = "CRDT_RISK_NOTE", unique = false, nullable = true, length = 400)
	private String crdtRiskNote;
	
	/** 首付款支付及来源 **/
	@Column(name = "FIRST_AMT_SRC", unique = false, nullable = true, length = 400)
	private String firstAmtSrc;
	
	/** 首付比例执行依据 **/
	@Column(name = "FIRST_RATE_BASC", unique = false, nullable = true, length = 400)
	private String firstRateBasc;
	
	/** 所购房屋押品本次认定价值 **/
	@Column(name = "GRT_EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grtEvalAmt;
	
	/** 是否足值 STD_ZB_YES_NO **/
	@Column(name = "GRT_IS_ENFV", unique = false, nullable = true, length = 5)
	private String grtIsEnfv;
	
	/** 是否足值认定说明 **/
	@Column(name = "GRT_NOTE", unique = false, nullable = true, length = 400)
	private String grtNote;
	
	/** 价值成数是否合理 STD_ZB_YES_NO **/
	@Column(name = "GRT_AMT_FIT", unique = false, nullable = true, length = 5)
	private String grtAmtFit;
	
	/** 价值成数认定说明 **/
	@Column(name = "GRT_FIT_NOTE", unique = false, nullable = true, length = 400)
	private String grtFitNote;
	
	/** 是否具备处置变现能力 STD_ZB_YES_NO **/
	@Column(name = "GRT_CHAB", unique = false, nullable = true, length = 5)
	private String grtChab;
	
	/** 处置变现能力认定说明 **/
	@Column(name = "GRT_CHAB_NOTE", unique = false, nullable = true, length = 400)
	private String grtChabNote;
	
	/** 购房用途评估意见 **/
	@Column(name = "GRT_HOUSE_POP_NOTE", unique = false, nullable = true, length = 400)
	private String grtHousePopNote;
	
	/** I/I **/
	@Column(name = "M_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mAmt;
	
	/** D/I **/
	@Column(name = "D_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dAmt;
	
	/** D/I（折算后） **/
	@Column(name = "D_AMT_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dAmtCvt;
	
	/** 借款人月收入总额 **/
	@Column(name = "M_INCOME_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mIncomeTot;
	
	/** 借款人已有负债月还款额汇总 **/
	@Column(name = "DEBT_M_REPAY_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtMRepayTot;
	
	/** 借款人已有负债月还款汇总(调整后) **/
	@Column(name = "DEBT_M_REPAY_TOT_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtMRepayTotCvt;
	
	/** 本笔贷款月负债总额 **/
	@Column(name = "M_DEBT_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mDebtTot;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param secen
	 */
	public void setSecen(String secen) {
		this.secen = secen;
	}
	
    /**
     * @return secen
     */
	public String getSecen() {
		return this.secen;
	}
	
	/**
	 * @param borrIncomeNote
	 */
	public void setBorrIncomeNote(String borrIncomeNote) {
		this.borrIncomeNote = borrIncomeNote;
	}
	
    /**
     * @return borrIncomeNote
     */
	public String getBorrIncomeNote() {
		return this.borrIncomeNote;
	}
	
	/**
	 * @param pmfAr
	 */
	public void setPmfAr(java.math.BigDecimal pmfAr) {
		this.pmfAr = pmfAr;
	}
	
    /**
     * @return pmfAr
     */
	public java.math.BigDecimal getPmfAr() {
		return this.pmfAr;
	}
	
	/**
	 * @param borrDebtNote
	 */
	public void setBorrDebtNote(String borrDebtNote) {
		this.borrDebtNote = borrDebtNote;
	}
	
    /**
     * @return borrDebtNote
     */
	public String getBorrDebtNote() {
		return this.borrDebtNote;
	}
	
	/**
	 * @param crdtRiskNote
	 */
	public void setCrdtRiskNote(String crdtRiskNote) {
		this.crdtRiskNote = crdtRiskNote;
	}
	
    /**
     * @return crdtRiskNote
     */
	public String getCrdtRiskNote() {
		return this.crdtRiskNote;
	}
	
	/**
	 * @param firstAmtSrc
	 */
	public void setFirstAmtSrc(String firstAmtSrc) {
		this.firstAmtSrc = firstAmtSrc;
	}
	
    /**
     * @return firstAmtSrc
     */
	public String getFirstAmtSrc() {
		return this.firstAmtSrc;
	}
	
	/**
	 * @param firstRateBasc
	 */
	public void setFirstRateBasc(String firstRateBasc) {
		this.firstRateBasc = firstRateBasc;
	}
	
    /**
     * @return firstRateBasc
     */
	public String getFirstRateBasc() {
		return this.firstRateBasc;
	}
	
	/**
	 * @param grtEvalAmt
	 */
	public void setGrtEvalAmt(java.math.BigDecimal grtEvalAmt) {
		this.grtEvalAmt = grtEvalAmt;
	}
	
    /**
     * @return grtEvalAmt
     */
	public java.math.BigDecimal getGrtEvalAmt() {
		return this.grtEvalAmt;
	}
	
	/**
	 * @param grtIsEnfv
	 */
	public void setGrtIsEnfv(String grtIsEnfv) {
		this.grtIsEnfv = grtIsEnfv;
	}
	
    /**
     * @return grtIsEnfv
     */
	public String getGrtIsEnfv() {
		return this.grtIsEnfv;
	}
	
	/**
	 * @param grtNote
	 */
	public void setGrtNote(String grtNote) {
		this.grtNote = grtNote;
	}
	
    /**
     * @return grtNote
     */
	public String getGrtNote() {
		return this.grtNote;
	}
	
	/**
	 * @param grtAmtFit
	 */
	public void setGrtAmtFit(String grtAmtFit) {
		this.grtAmtFit = grtAmtFit;
	}
	
    /**
     * @return grtAmtFit
     */
	public String getGrtAmtFit() {
		return this.grtAmtFit;
	}
	
	/**
	 * @param grtFitNote
	 */
	public void setGrtFitNote(String grtFitNote) {
		this.grtFitNote = grtFitNote;
	}
	
    /**
     * @return grtFitNote
     */
	public String getGrtFitNote() {
		return this.grtFitNote;
	}
	
	/**
	 * @param grtChab
	 */
	public void setGrtChab(String grtChab) {
		this.grtChab = grtChab;
	}
	
    /**
     * @return grtChab
     */
	public String getGrtChab() {
		return this.grtChab;
	}
	
	/**
	 * @param grtChabNote
	 */
	public void setGrtChabNote(String grtChabNote) {
		this.grtChabNote = grtChabNote;
	}
	
    /**
     * @return grtChabNote
     */
	public String getGrtChabNote() {
		return this.grtChabNote;
	}
	
	/**
	 * @param grtHousePopNote
	 */
	public void setGrtHousePopNote(String grtHousePopNote) {
		this.grtHousePopNote = grtHousePopNote;
	}
	
    /**
     * @return grtHousePopNote
     */
	public String getGrtHousePopNote() {
		return this.grtHousePopNote;
	}
	
	/**
	 * @param mAmt
	 */
	public void setMAmt(java.math.BigDecimal mAmt) {
		this.mAmt = mAmt;
	}
	
    /**
     * @return mAmt
     */
	public java.math.BigDecimal getMAmt() {
		return this.mAmt;
	}
	
	/**
	 * @param dAmt
	 */
	public void setDAmt(java.math.BigDecimal dAmt) {
		this.dAmt = dAmt;
	}
	
    /**
     * @return dAmt
     */
	public java.math.BigDecimal getDAmt() {
		return this.dAmt;
	}
	
	/**
	 * @param dAmtCvt
	 */
	public void setDAmtCvt(java.math.BigDecimal dAmtCvt) {
		this.dAmtCvt = dAmtCvt;
	}
	
    /**
     * @return dAmtCvt
     */
	public java.math.BigDecimal getDAmtCvt() {
		return this.dAmtCvt;
	}
	
	/**
	 * @param mIncomeTot
	 */
	public void setMIncomeTot(java.math.BigDecimal mIncomeTot) {
		this.mIncomeTot = mIncomeTot;
	}
	
    /**
     * @return mIncomeTot
     */
	public java.math.BigDecimal getMIncomeTot() {
		return this.mIncomeTot;
	}
	
	/**
	 * @param debtMRepayTot
	 */
	public void setDebtMRepayTot(java.math.BigDecimal debtMRepayTot) {
		this.debtMRepayTot = debtMRepayTot;
	}
	
    /**
     * @return debtMRepayTot
     */
	public java.math.BigDecimal getDebtMRepayTot() {
		return this.debtMRepayTot;
	}
	
	/**
	 * @param debtMRepayTotCvt
	 */
	public void setDebtMRepayTotCvt(java.math.BigDecimal debtMRepayTotCvt) {
		this.debtMRepayTotCvt = debtMRepayTotCvt;
	}
	
    /**
     * @return debtMRepayTotCvt
     */
	public java.math.BigDecimal getDebtMRepayTotCvt() {
		return this.debtMRepayTotCvt;
	}
	
	/**
	 * @param mDebtTot
	 */
	public void setMDebtTot(java.math.BigDecimal mDebtTot) {
		this.mDebtTot = mDebtTot;
	}
	
    /**
     * @return mDebtTot
     */
	public java.math.BigDecimal getMDebtTot() {
		return this.mDebtTot;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}