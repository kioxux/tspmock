/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySales
 * @类描述: rpt_oper_energy_sales数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:44:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_energy_sales")
public class RptOperEnergySales extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 最近第二年申报销售收入 **/
	@Column(name = "NEAR_SECOND_APP_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondAppSaleIncome;
	
	/** 最近第二不开票收入 **/
	@Column(name = "NEAR_SECOND_NO_INVOICE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondNoInvoiceIncome;
	
	/** 最近第二实际销售收入合计 **/
	@Column(name = "NEAR_SECOND_REAL_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondRealSaleIncome;
	
	/** 最近第二外销收入 **/
	@Column(name = "NEAR_SECOND_OUT_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondOutSaleIncome;
	
	/** 最近第二内销收入 **/
	@Column(name = "NEAR_SECOND_IN_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondInSaleIncome;
	
	/** 最近第一年申报销售收入 **/
	@Column(name = "NEAR_FIRST_APP_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstAppSaleIncome;
	
	/** 最近第一年不开票收入 **/
	@Column(name = "NEAR_FIRST_NO_INVOICE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstNoInvoiceIncome;
	
	/** 最近第一年实际销售收入合计 **/
	@Column(name = "NEAR_FIRST_REAL_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstRealSaleIncome;
	
	/** 最近第一年外销收入 **/
	@Column(name = "NEAR_FIRST_OUT_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstOutSaleIncome;
	
	/** 最近第一年内销收入 **/
	@Column(name = "NEAR_FIRST_IN_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstInSaleIncome;
	
	/** 今年当前申报销售收入 **/
	@Column(name = "CURR_APP_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currAppSaleIncome;
	
	/** 今年当前不开票收入 **/
	@Column(name = "CURR_NO_INVOICE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currNoInvoiceIncome;
	
	/** 今年当前实际销售收入合计 **/
	@Column(name = "CURR_REAL_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currRealSaleIncome;
	
	/** 今年当前外销收入 **/
	@Column(name = "CURR_OUT_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOutSaleIncome;
	
	/** 今年当前内销收入 **/
	@Column(name = "CURR_IN_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currInSaleIncome;
	
	/** 申报销售收入增减值 **/
	@Column(name = "APP_SALE_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appSaleIncomeDiff;
	
	/** 不开票收入增减值 **/
	@Column(name = "NO_INVOICE_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal noInvoiceIncomeDiff;
	
	/** 实际销售收入合计增减值 **/
	@Column(name = "REAL_SALE_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realSaleIncomeDiff;
	
	/** 外销收入增减值 **/
	@Column(name = "OUT_SALE_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outSaleIncomeDiff;
	
	/** 内销收入增减值 **/
	@Column(name = "IN_SALE_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inSaleIncomeDiff;
	
	/** 申报销售收入较上年同期增减 **/
	@Column(name = "APP_SALE_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appSaleIncomeLastYearDiff;
	
	/** 不开票收入较上年同期增减 **/
	@Column(name = "NO_INVOICE_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal noInvoiceIncomeLastYearDiff;
	
	/** 实际销售收入合计较上年同期增减 **/
	@Column(name = "REAL_SALE_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realSaleIncomeLastYearDiff;
	
	/** 外销收入较上年同期增减 **/
	@Column(name = "OUT_SALE_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outSaleIncomeLastYearDiff;
	
	/** 内销收入较上年同期增减 **/
	@Column(name = "IN_SALE_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inSaleIncomeLastYearDiff;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param nearSecondAppSaleIncome
	 */
	public void setNearSecondAppSaleIncome(java.math.BigDecimal nearSecondAppSaleIncome) {
		this.nearSecondAppSaleIncome = nearSecondAppSaleIncome;
	}
	
    /**
     * @return nearSecondAppSaleIncome
     */
	public java.math.BigDecimal getNearSecondAppSaleIncome() {
		return this.nearSecondAppSaleIncome;
	}
	
	/**
	 * @param nearSecondNoInvoiceIncome
	 */
	public void setNearSecondNoInvoiceIncome(java.math.BigDecimal nearSecondNoInvoiceIncome) {
		this.nearSecondNoInvoiceIncome = nearSecondNoInvoiceIncome;
	}
	
    /**
     * @return nearSecondNoInvoiceIncome
     */
	public java.math.BigDecimal getNearSecondNoInvoiceIncome() {
		return this.nearSecondNoInvoiceIncome;
	}
	
	/**
	 * @param nearSecondRealSaleIncome
	 */
	public void setNearSecondRealSaleIncome(java.math.BigDecimal nearSecondRealSaleIncome) {
		this.nearSecondRealSaleIncome = nearSecondRealSaleIncome;
	}
	
    /**
     * @return nearSecondRealSaleIncome
     */
	public java.math.BigDecimal getNearSecondRealSaleIncome() {
		return this.nearSecondRealSaleIncome;
	}
	
	/**
	 * @param nearSecondOutSaleIncome
	 */
	public void setNearSecondOutSaleIncome(java.math.BigDecimal nearSecondOutSaleIncome) {
		this.nearSecondOutSaleIncome = nearSecondOutSaleIncome;
	}
	
    /**
     * @return nearSecondOutSaleIncome
     */
	public java.math.BigDecimal getNearSecondOutSaleIncome() {
		return this.nearSecondOutSaleIncome;
	}
	
	/**
	 * @param nearSecondInSaleIncome
	 */
	public void setNearSecondInSaleIncome(java.math.BigDecimal nearSecondInSaleIncome) {
		this.nearSecondInSaleIncome = nearSecondInSaleIncome;
	}
	
    /**
     * @return nearSecondInSaleIncome
     */
	public java.math.BigDecimal getNearSecondInSaleIncome() {
		return this.nearSecondInSaleIncome;
	}
	
	/**
	 * @param nearFirstAppSaleIncome
	 */
	public void setNearFirstAppSaleIncome(java.math.BigDecimal nearFirstAppSaleIncome) {
		this.nearFirstAppSaleIncome = nearFirstAppSaleIncome;
	}
	
    /**
     * @return nearFirstAppSaleIncome
     */
	public java.math.BigDecimal getNearFirstAppSaleIncome() {
		return this.nearFirstAppSaleIncome;
	}
	
	/**
	 * @param nearFirstNoInvoiceIncome
	 */
	public void setNearFirstNoInvoiceIncome(java.math.BigDecimal nearFirstNoInvoiceIncome) {
		this.nearFirstNoInvoiceIncome = nearFirstNoInvoiceIncome;
	}
	
    /**
     * @return nearFirstNoInvoiceIncome
     */
	public java.math.BigDecimal getNearFirstNoInvoiceIncome() {
		return this.nearFirstNoInvoiceIncome;
	}
	
	/**
	 * @param nearFirstRealSaleIncome
	 */
	public void setNearFirstRealSaleIncome(java.math.BigDecimal nearFirstRealSaleIncome) {
		this.nearFirstRealSaleIncome = nearFirstRealSaleIncome;
	}
	
    /**
     * @return nearFirstRealSaleIncome
     */
	public java.math.BigDecimal getNearFirstRealSaleIncome() {
		return this.nearFirstRealSaleIncome;
	}
	
	/**
	 * @param nearFirstOutSaleIncome
	 */
	public void setNearFirstOutSaleIncome(java.math.BigDecimal nearFirstOutSaleIncome) {
		this.nearFirstOutSaleIncome = nearFirstOutSaleIncome;
	}
	
    /**
     * @return nearFirstOutSaleIncome
     */
	public java.math.BigDecimal getNearFirstOutSaleIncome() {
		return this.nearFirstOutSaleIncome;
	}
	
	/**
	 * @param nearFirstInSaleIncome
	 */
	public void setNearFirstInSaleIncome(java.math.BigDecimal nearFirstInSaleIncome) {
		this.nearFirstInSaleIncome = nearFirstInSaleIncome;
	}
	
    /**
     * @return nearFirstInSaleIncome
     */
	public java.math.BigDecimal getNearFirstInSaleIncome() {
		return this.nearFirstInSaleIncome;
	}
	
	/**
	 * @param currAppSaleIncome
	 */
	public void setCurrAppSaleIncome(java.math.BigDecimal currAppSaleIncome) {
		this.currAppSaleIncome = currAppSaleIncome;
	}
	
    /**
     * @return currAppSaleIncome
     */
	public java.math.BigDecimal getCurrAppSaleIncome() {
		return this.currAppSaleIncome;
	}
	
	/**
	 * @param currNoInvoiceIncome
	 */
	public void setCurrNoInvoiceIncome(java.math.BigDecimal currNoInvoiceIncome) {
		this.currNoInvoiceIncome = currNoInvoiceIncome;
	}
	
    /**
     * @return currNoInvoiceIncome
     */
	public java.math.BigDecimal getCurrNoInvoiceIncome() {
		return this.currNoInvoiceIncome;
	}
	
	/**
	 * @param currRealSaleIncome
	 */
	public void setCurrRealSaleIncome(java.math.BigDecimal currRealSaleIncome) {
		this.currRealSaleIncome = currRealSaleIncome;
	}
	
    /**
     * @return currRealSaleIncome
     */
	public java.math.BigDecimal getCurrRealSaleIncome() {
		return this.currRealSaleIncome;
	}
	
	/**
	 * @param currOutSaleIncome
	 */
	public void setCurrOutSaleIncome(java.math.BigDecimal currOutSaleIncome) {
		this.currOutSaleIncome = currOutSaleIncome;
	}
	
    /**
     * @return currOutSaleIncome
     */
	public java.math.BigDecimal getCurrOutSaleIncome() {
		return this.currOutSaleIncome;
	}
	
	/**
	 * @param currInSaleIncome
	 */
	public void setCurrInSaleIncome(java.math.BigDecimal currInSaleIncome) {
		this.currInSaleIncome = currInSaleIncome;
	}
	
    /**
     * @return currInSaleIncome
     */
	public java.math.BigDecimal getCurrInSaleIncome() {
		return this.currInSaleIncome;
	}
	
	/**
	 * @param appSaleIncomeDiff
	 */
	public void setAppSaleIncomeDiff(java.math.BigDecimal appSaleIncomeDiff) {
		this.appSaleIncomeDiff = appSaleIncomeDiff;
	}
	
    /**
     * @return appSaleIncomeDiff
     */
	public java.math.BigDecimal getAppSaleIncomeDiff() {
		return this.appSaleIncomeDiff;
	}
	
	/**
	 * @param noInvoiceIncomeDiff
	 */
	public void setNoInvoiceIncomeDiff(java.math.BigDecimal noInvoiceIncomeDiff) {
		this.noInvoiceIncomeDiff = noInvoiceIncomeDiff;
	}
	
    /**
     * @return noInvoiceIncomeDiff
     */
	public java.math.BigDecimal getNoInvoiceIncomeDiff() {
		return this.noInvoiceIncomeDiff;
	}
	
	/**
	 * @param realSaleIncomeDiff
	 */
	public void setRealSaleIncomeDiff(java.math.BigDecimal realSaleIncomeDiff) {
		this.realSaleIncomeDiff = realSaleIncomeDiff;
	}
	
    /**
     * @return realSaleIncomeDiff
     */
	public java.math.BigDecimal getRealSaleIncomeDiff() {
		return this.realSaleIncomeDiff;
	}
	
	/**
	 * @param outSaleIncomeDiff
	 */
	public void setOutSaleIncomeDiff(java.math.BigDecimal outSaleIncomeDiff) {
		this.outSaleIncomeDiff = outSaleIncomeDiff;
	}
	
    /**
     * @return outSaleIncomeDiff
     */
	public java.math.BigDecimal getOutSaleIncomeDiff() {
		return this.outSaleIncomeDiff;
	}
	
	/**
	 * @param inSaleIncomeDiff
	 */
	public void setInSaleIncomeDiff(java.math.BigDecimal inSaleIncomeDiff) {
		this.inSaleIncomeDiff = inSaleIncomeDiff;
	}
	
    /**
     * @return inSaleIncomeDiff
     */
	public java.math.BigDecimal getInSaleIncomeDiff() {
		return this.inSaleIncomeDiff;
	}
	
	/**
	 * @param appSaleIncomeLastYearDiff
	 */
	public void setAppSaleIncomeLastYearDiff(java.math.BigDecimal appSaleIncomeLastYearDiff) {
		this.appSaleIncomeLastYearDiff = appSaleIncomeLastYearDiff;
	}
	
    /**
     * @return appSaleIncomeLastYearDiff
     */
	public java.math.BigDecimal getAppSaleIncomeLastYearDiff() {
		return this.appSaleIncomeLastYearDiff;
	}
	
	/**
	 * @param noInvoiceIncomeLastYearDiff
	 */
	public void setNoInvoiceIncomeLastYearDiff(java.math.BigDecimal noInvoiceIncomeLastYearDiff) {
		this.noInvoiceIncomeLastYearDiff = noInvoiceIncomeLastYearDiff;
	}
	
    /**
     * @return noInvoiceIncomeLastYearDiff
     */
	public java.math.BigDecimal getNoInvoiceIncomeLastYearDiff() {
		return this.noInvoiceIncomeLastYearDiff;
	}
	
	/**
	 * @param realSaleIncomeLastYearDiff
	 */
	public void setRealSaleIncomeLastYearDiff(java.math.BigDecimal realSaleIncomeLastYearDiff) {
		this.realSaleIncomeLastYearDiff = realSaleIncomeLastYearDiff;
	}
	
    /**
     * @return realSaleIncomeLastYearDiff
     */
	public java.math.BigDecimal getRealSaleIncomeLastYearDiff() {
		return this.realSaleIncomeLastYearDiff;
	}
	
	/**
	 * @param outSaleIncomeLastYearDiff
	 */
	public void setOutSaleIncomeLastYearDiff(java.math.BigDecimal outSaleIncomeLastYearDiff) {
		this.outSaleIncomeLastYearDiff = outSaleIncomeLastYearDiff;
	}
	
    /**
     * @return outSaleIncomeLastYearDiff
     */
	public java.math.BigDecimal getOutSaleIncomeLastYearDiff() {
		return this.outSaleIncomeLastYearDiff;
	}
	
	/**
	 * @param inSaleIncomeLastYearDiff
	 */
	public void setInSaleIncomeLastYearDiff(java.math.BigDecimal inSaleIncomeLastYearDiff) {
		this.inSaleIncomeLastYearDiff = inSaleIncomeLastYearDiff;
	}
	
    /**
     * @return inSaleIncomeLastYearDiff
     */
	public java.math.BigDecimal getInSaleIncomeLastYearDiff() {
		return this.inSaleIncomeLastYearDiff;
	}


}