/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtFrUfrAccRel;
import cn.com.yusys.yusp.service.LmtFrUfrAccRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAccRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:39:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtfrufraccrel")
public class LmtFrUfrAccRelResource {
    
    private static final Logger log = LoggerFactory.getLogger(LmtFrUfrAccRelResource.class);
    @Autowired
    private LmtFrUfrAccRelService lmtFrUfrAccRelService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtFrUfrAccRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtFrUfrAccRel> list = lmtFrUfrAccRelService.selectAll(queryModel);
        return new ResultDto<List<LmtFrUfrAccRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtFrUfrAccRel>> index(QueryModel queryModel) {
        List<LmtFrUfrAccRel> list = lmtFrUfrAccRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtFrUfrAccRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtFrUfrAccRel> show(@PathVariable("pkId") String pkId) {
        LmtFrUfrAccRel lmtFrUfrAccRel = lmtFrUfrAccRelService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtFrUfrAccRel>(lmtFrUfrAccRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtFrUfrAccRel> create(@RequestBody LmtFrUfrAccRel lmtFrUfrAccRel) throws URISyntaxException {
        lmtFrUfrAccRelService.insert(lmtFrUfrAccRel);
        return new ResultDto<LmtFrUfrAccRel>(lmtFrUfrAccRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtFrUfrAccRel lmtFrUfrAccRel) throws URISyntaxException {
        int result = lmtFrUfrAccRelService.update(lmtFrUfrAccRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtFrUfrAccRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtFrUfrAccRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:querySelectLmtFrUfrAccRel
     * @函数描述:根据入参查询冻结/解冻额度台账关系表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySelectLmtFrUfrAccRel")
    protected ResultDto<Integer> querySelectLmtFrUfrAccRel(@RequestBody Map params)throws Exception{
        log.info("查询冻结/解冻额度台账关系表"+ JSON.toJSONString(params));
        int rtnData =lmtFrUfrAccRelService.querySelectLmtFrUfrAccRel(params);
        return  new ResultDto<>(rtnData);
    }
    
}
