package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

@ExcelCsv(namePrefix = "空白凭证批量导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class BlankCertiInfoVo {

    /*
  凭证编号
   */
    @ExcelField(title = "凭证编号", viewLength = 40)
    private String certiNo;

    public String getCertiNo() {
        return certiNo;
    }

    public void setCertiNo(String certiNo) {
        this.certiNo = certiNo;
    }
}
