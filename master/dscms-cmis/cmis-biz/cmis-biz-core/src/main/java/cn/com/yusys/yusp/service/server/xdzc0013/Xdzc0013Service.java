package cn.com.yusys.yusp.service.server.xdzc0013;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.dto.server.xdzc0013.req.Xdzc0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.resp.Xdzc0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import cn.com.yusys.yusp.service.DocAsplTcontService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

/**
 * 接口处理类:贸易合同新增接口
 *
 * @Author xs
 * @Date 2021/6/15 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0013Service {

    @Autowired
    private DocAsplTcontService docAsplTcontService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0013.Xdzc0013Service.class);

    /**
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0013DataRespDto xdzc0013Service(Xdzc0013DataReqDto xdzc0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value);
        Xdzc0013DataRespDto xdzc0013DataRespDto = new Xdzc0013DataRespDto();
        xdzc0013DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0013DataRespDto.setOpMsg("贸易合同资料上传逻辑报错");// 描述信息

        try {
            String cusId = xdzc0013DataReqDto.getCusId();// 客户编号
            DocAsplTcont docAsplTcont = new DocAsplTcont();
            docAsplTcont.setPkId(UUID.randomUUID().toString().replace("-",""));// 主键
            docAsplTcont.setTcontImgId(xdzc0013DataReqDto.gettContYXSerno());// 贸易合同影像流水号(网银获取)
            docAsplTcont.setTcontNo(xdzc0013DataReqDto.gettContNo());// 贸易合同编号
            docAsplTcont.setTcontCnName(xdzc0013DataReqDto.gettContCnName());// 贸易合同名称

            docAsplTcont.setContAmt(xdzc0013DataReqDto.gettContAmt());// 贸易合同金额
            docAsplTcont.setStartDate(xdzc0013DataReqDto.gettStartDate());// 起始日
            docAsplTcont.setEndDate(xdzc0013DataReqDto.gettEndDate());// 到期日
            docAsplTcont.setSupplierName(xdzc0013DataReqDto.getSupplierName());// 供货方
            docAsplTcont.setConsumeName(xdzc0013DataReqDto.getConsumeName());// (公司名称)消费方
            docAsplTcont.setPrdName(xdzc0013DataReqDto.getPrdName());// 产品名称
            docAsplTcont.setPrdQnt(xdzc0013DataReqDto.getPrdQnt());// 产品数量
            docAsplTcont.setPrdPrice(xdzc0013DataReqDto.getPrdPrice());// 产品单价
            docAsplTcont.setSignDate(xdzc0013DataReqDto.getSignDate());// 签约日期
            docAsplTcont.setDeliverDate(xdzc0013DataReqDto.getDeliverDate());// 交付日期
            docAsplTcont.setDeliverType(xdzc0013DataReqDto.getDeliverType());// 交付方式
            docAsplTcont.setToppName(xdzc0013DataReqDto.getToppName());// 交易对手名称
            docAsplTcont.setToppAcctNo(xdzc0013DataReqDto.getToppAcctNo());// 交易对手账号
            docAsplTcont.setIsBankAcct(xdzc0013DataReqDto.getIsBankAcct());// 交易对手是否本行账户
            docAsplTcont.setOpanOrgNo(xdzc0013DataReqDto.getOpanOrgNo());// 开户行行号
            docAsplTcont.setOpanOrgName(xdzc0013DataReqDto.getOpanOrgName());// 开户行行名
            docAsplTcont.setContHighAvlAmt(xdzc0013DataReqDto.getContHighAvlAmt());// 可用信总金额
            docAsplTcont.setSettlementMethod(xdzc0013DataReqDto.getSettlementMethod());// 结算方式
            docAsplTcont.setCusId(cusId);// 客户编号
            docAsplTcont.setCusName(xdzc0013DataReqDto.getCusName());// 客户名称

            // 判断用户是否是资产池用户
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            if(Objects.isNull(ctrAsplDetails)){
                xdzc0013DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0013DataRespDto.setOpMsg("客户编号："+cusId+"，未签订有效的资产池协议");// 描述信息
                return xdzc0013DataRespDto;
            }
            docAsplTcont.setOprType(CmisCommonConstants.OPR_TYPE_ADD);// 新增
            docAsplTcont.setApproveStatus(CmisCommonConstants.WF_STATUS_000);// 待发起
            docAsplTcont.setContNo(ctrAsplDetails.getContNo());// 资产池协议编号
            docAsplTcont.setInputId(ctrAsplDetails.getInputId());
            docAsplTcont.setInputBrId(ctrAsplDetails.getInputBrId());
            docAsplTcont.setManagerId(ctrAsplDetails.getManagerId());
            docAsplTcont.setManagerBrId(ctrAsplDetails.getManagerBrId());
            docAsplTcont.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));

            if(docAsplTcontService.insert(docAsplTcont)>0){
                xdzc0013DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
                xdzc0013DataRespDto.setOpMsg("购销合同新增成功");// 描述信息
            }else{
                xdzc0013DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0013DataRespDto.setOpMsg("购销合同新增失败");// 描述信息
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, e.getMessage());
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.key);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value);
        return xdzc0013DataRespDto;
    }
}
