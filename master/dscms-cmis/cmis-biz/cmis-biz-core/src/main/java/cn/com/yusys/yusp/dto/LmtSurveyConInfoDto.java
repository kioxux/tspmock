package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyConInfo
 * @类描述: lmt_survey_con_info数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:39:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyConInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 建议金额 **/
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	private java.math.BigDecimal adviceRate;
	
	/** 参考利率 **/
	private java.math.BigDecimal refRate;
	
	/** 建议期限 **/
	private String adviceTerm;
	
	/** 贷款期限 **/
	private String loanTerm;
	
	/** 担保方式 **/
	private String grtMode;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 是否原抵押物 **/
	private String isOldColl;
	
	/** 周转额度 **/
	private java.math.BigDecimal turnovLmt;
	
	/** 新增额度 **/
	private java.math.BigDecimal newAddLmt;
	
	/** 是否存在协办客户经理 **/
	private String isAssManagerId;
	
	/** 协办客户经理工号 **/
	private String assManagerIdJobNo;
	
	/** 客户是否实际经营人 **/
	private String isCusRealOperator;
	
	/** 客户经营是否正常 **/
	private String isCusOperNormal;
	
	/** 拒绝类型 **/
	private String rfuType;
	
	/** 办理建议 **/
	private String prcAdvice;
	
	/** 情况说明 **/
	private String situDesc;
	
	/** 模型金额 **/
	private java.math.BigDecimal modelAmt;
	
	/** 模型利率 **/
	private java.math.BigDecimal modelRate;
	
	/** 模型期限 **/
	private String modelTerm;
	
	/** 客户性质 **/
	private String cusCha;
	
	/** 原借据号 **/
	private String oldBillNo;
	
	/** 原借据金额 **/
	private java.math.BigDecimal oldBillAmt;
	
	/** 原借据余额 **/
	private java.math.BigDecimal oldBillBalance;
	
	/** 原借据利率 **/
	private java.math.BigDecimal oldBillLoanRate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

//	新增字段
	/** 是否提前申贷 **/
	private String isTqsd;

	/** 申贷类型 **/
	private String appLoanWay;

	/** 申贷类型 **/
	private String isCwhb;

	/** 无还本模型利率 **/
	private java.math.BigDecimal whbModelRate;

	/** 无还本模型金额（元） **/
	private java.math.BigDecimal whbModelAmt;

	/** 业务余额（元） **/
	private java.math.BigDecimal loanBalance;

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public String getIsTqsd() {
		return isTqsd;
	}

	public void setIsTqsd(String isTqsd) {
		this.isTqsd = isTqsd;
	}

	public String getAppLoanWay() {
		return appLoanWay;
	}

	public void setAppLoanWay(String appLoanWay) {
		this.appLoanWay = appLoanWay;
	}

	public String getIsCwhb() {
		return isCwhb;
	}

	public void setIsCwhb(String isCwhb) {
		this.isCwhb = isCwhb;
	}

	public BigDecimal getWhbModelRate() {
		return whbModelRate;
	}

	public void setWhbModelRate(BigDecimal whbModelRate) {
		this.whbModelRate = whbModelRate;
	}

	public BigDecimal getWhbModelAmt() {
		return whbModelAmt;
	}

	public void setWhbModelAmt(BigDecimal whbModelAmt) {
		this.whbModelAmt = whbModelAmt;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return AdviceAmt
     */	
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return AdviceRate
     */	
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return RefRate
     */	
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm == null ? null : adviceTerm.trim();
	}
	
    /**
     * @return AdviceTerm
     */	
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm == null ? null : loanTerm.trim();
	}
	
    /**
     * @return LoanTerm
     */	
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param grtMode
	 */
	public void setGrtMode(String grtMode) {
		this.grtMode = grtMode == null ? null : grtMode.trim();
	}
	
    /**
     * @return GrtMode
     */	
	public String getGrtMode() {
		return this.grtMode;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param isOldColl
	 */
	public void setIsOldColl(String isOldColl) {
		this.isOldColl = isOldColl == null ? null : isOldColl.trim();
	}
	
    /**
     * @return IsOldColl
     */	
	public String getIsOldColl() {
		return this.isOldColl;
	}
	
	/**
	 * @param turnovLmt
	 */
	public void setTurnovLmt(java.math.BigDecimal turnovLmt) {
		this.turnovLmt = turnovLmt;
	}
	
    /**
     * @return TurnovLmt
     */	
	public java.math.BigDecimal getTurnovLmt() {
		return this.turnovLmt;
	}
	
	/**
	 * @param newAddLmt
	 */
	public void setNewAddLmt(java.math.BigDecimal newAddLmt) {
		this.newAddLmt = newAddLmt;
	}
	
    /**
     * @return NewAddLmt
     */	
	public java.math.BigDecimal getNewAddLmt() {
		return this.newAddLmt;
	}
	
	/**
	 * @param isAssManagerId
	 */
	public void setIsAssManagerId(String isAssManagerId) {
		this.isAssManagerId = isAssManagerId == null ? null : isAssManagerId.trim();
	}
	
    /**
     * @return IsAssManagerId
     */	
	public String getIsAssManagerId() {
		return this.isAssManagerId;
	}
	
	/**
	 * @param assManagerIdJobNo
	 */
	public void setAssManagerIdJobNo(String assManagerIdJobNo) {
		this.assManagerIdJobNo = assManagerIdJobNo == null ? null : assManagerIdJobNo.trim();
	}
	
    /**
     * @return AssManagerIdJobNo
     */	
	public String getAssManagerIdJobNo() {
		return this.assManagerIdJobNo;
	}
	
	/**
	 * @param isCusRealOperator
	 */
	public void setIsCusRealOperator(String isCusRealOperator) {
		this.isCusRealOperator = isCusRealOperator == null ? null : isCusRealOperator.trim();
	}
	
    /**
     * @return IsCusRealOperator
     */	
	public String getIsCusRealOperator() {
		return this.isCusRealOperator;
	}
	
	/**
	 * @param isCusOperNormal
	 */
	public void setIsCusOperNormal(String isCusOperNormal) {
		this.isCusOperNormal = isCusOperNormal == null ? null : isCusOperNormal.trim();
	}
	
    /**
     * @return IsCusOperNormal
     */	
	public String getIsCusOperNormal() {
		return this.isCusOperNormal;
	}
	
	/**
	 * @param rfuType
	 */
	public void setRfuType(String rfuType) {
		this.rfuType = rfuType == null ? null : rfuType.trim();
	}
	
    /**
     * @return RfuType
     */	
	public String getRfuType() {
		return this.rfuType;
	}
	
	/**
	 * @param prcAdvice
	 */
	public void setPrcAdvice(String prcAdvice) {
		this.prcAdvice = prcAdvice == null ? null : prcAdvice.trim();
	}
	
    /**
     * @return PrcAdvice
     */	
	public String getPrcAdvice() {
		return this.prcAdvice;
	}
	
	/**
	 * @param situDesc
	 */
	public void setSituDesc(String situDesc) {
		this.situDesc = situDesc == null ? null : situDesc.trim();
	}
	
    /**
     * @return SituDesc
     */	
	public String getSituDesc() {
		return this.situDesc;
	}
	
	/**
	 * @param modelAmt
	 */
	public void setModelAmt(java.math.BigDecimal modelAmt) {
		this.modelAmt = modelAmt;
	}
	
    /**
     * @return ModelAmt
     */	
	public java.math.BigDecimal getModelAmt() {
		return this.modelAmt;
	}
	
	/**
	 * @param modelRate
	 */
	public void setModelRate(java.math.BigDecimal modelRate) {
		this.modelRate = modelRate;
	}
	
    /**
     * @return ModelRate
     */	
	public java.math.BigDecimal getModelRate() {
		return this.modelRate;
	}
	
	/**
	 * @param modelTerm
	 */
	public void setModelTerm(String modelTerm) {
		this.modelTerm = modelTerm == null ? null : modelTerm.trim();
	}
	
    /**
     * @return ModelTerm
     */	
	public String getModelTerm() {
		return this.modelTerm;
	}
	
	/**
	 * @param cusCha
	 */
	public void setCusCha(String cusCha) {
		this.cusCha = cusCha == null ? null : cusCha.trim();
	}
	
    /**
     * @return CusCha
     */	
	public String getCusCha() {
		return this.cusCha;
	}
	
	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo == null ? null : oldBillNo.trim();
	}
	
    /**
     * @return OldBillNo
     */	
	public String getOldBillNo() {
		return this.oldBillNo;
	}
	
	/**
	 * @param oldBillAmt
	 */
	public void setOldBillAmt(java.math.BigDecimal oldBillAmt) {
		this.oldBillAmt = oldBillAmt;
	}
	
    /**
     * @return OldBillAmt
     */	
	public java.math.BigDecimal getOldBillAmt() {
		return this.oldBillAmt;
	}
	
	/**
	 * @param oldBillBalance
	 */
	public void setOldBillBalance(java.math.BigDecimal oldBillBalance) {
		this.oldBillBalance = oldBillBalance;
	}
	
    /**
     * @return OldBillBalance
     */	
	public java.math.BigDecimal getOldBillBalance() {
		return this.oldBillBalance;
	}
	
	/**
	 * @param oldBillLoanRate
	 */
	public void setOldBillLoanRate(java.math.BigDecimal oldBillLoanRate) {
		this.oldBillLoanRate = oldBillLoanRate;
	}
	
    /**
     * @return OldBillLoanRate
     */	
	public java.math.BigDecimal getOldBillLoanRate() {
		return this.oldBillLoanRate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}