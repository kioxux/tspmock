/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.IqpLoanApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RepayDayRecordInfo;
import cn.com.yusys.yusp.service.RepayDayRecordInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RepayDayRecordInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-13 20:19:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/repaydayrecordinfo")
public class RepayDayRecordInfoResource {
    @Autowired
    private RepayDayRecordInfoService repayDayRecordInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RepayDayRecordInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<RepayDayRecordInfo> list = repayDayRecordInfoService.selectAll(queryModel);
        return new ResultDto<List<RepayDayRecordInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RepayDayRecordInfo>> index(QueryModel queryModel) {
        List<RepayDayRecordInfo> list = repayDayRecordInfoService.selectByModel(queryModel);
        return new ResultDto<List<RepayDayRecordInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cfgSerno}")
    protected ResultDto<RepayDayRecordInfo> show(@PathVariable("cfgSerno") String cfgSerno) {
        RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoService.selectByPrimaryKey(cfgSerno);
        return new ResultDto<RepayDayRecordInfo>(repayDayRecordInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RepayDayRecordInfo> create(@RequestBody RepayDayRecordInfo repayDayRecordInfo) throws URISyntaxException {
        repayDayRecordInfoService.insert(repayDayRecordInfo);
        return new ResultDto<RepayDayRecordInfo>(repayDayRecordInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RepayDayRecordInfo repayDayRecordInfo) throws URISyntaxException {
        int result = repayDayRecordInfoService.update(repayDayRecordInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cfgSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cfgSerno") String cfgSerno) {
        int result = repayDayRecordInfoService.deleteByPrimaryKey(cfgSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = repayDayRecordInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/14 11:18
     * @注释 分页条件查询
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<RepayDayRecordInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<RepayDayRecordInfo> list = repayDayRecordInfoService.selectByModel(queryModel);
        return new ResultDto<List<RepayDayRecordInfo>>(list);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/5/14 13:49
     * @注释 保存方法-新增
     */
    @PostMapping("/adddata")
    protected ResultDto adddata(@RequestBody RepayDayRecordInfo repayDayRecordInfo) {
        return   repayDayRecordInfoService.addData(repayDayRecordInfo);

    }
    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/29 19:56
     * @version 1.0.0
     * @desc    只为拿到还款日
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectrepayday")
    protected ResultDto selectRepayDay(@RequestBody IqpLoanApp iqpLoanApp) {
        return   repayDayRecordInfoService.selectRepayDay(iqpLoanApp.getIqpSerno());
    }
}
