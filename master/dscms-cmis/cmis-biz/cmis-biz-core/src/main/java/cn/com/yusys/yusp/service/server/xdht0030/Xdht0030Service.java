package cn.com.yusys.yusp.service.server.xdht0030;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询客户我行合作次数
 * @author zhugenrong
 * @version 1.0
 */
@Service
public class Xdht0030Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0030Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 查询客户我行合作次数
     * @param certCode
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int getCoopTimesByCertCode(String certCode) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, certCode);
        int count = ctrLoanContMapper.getCoopTimesByCertCode(certCode);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, count);
        return count;
    }
}
