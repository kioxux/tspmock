/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAttachmentInfo
 * @类描述: credit_card_attachment_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:24:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_card_attachment_info")
public class CreditCardAttachmentInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 公积金缴存状态 **/
	@Column(name = "PUND_STATUS", unique = false, nullable = true, length = 5)
	private String pundStatus;
	
	/** 公积金缴存时间 **/
	@Column(name = "PUND_PAID_DATE", unique = false, nullable = true, length = 20)
	private String pundPaidDate;
	
	/** 公积金缴存基数 **/
	@Column(name = "PUND_DEPOSIT_BASE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pundDepositBase;
	
	/** 公积金缴存总月份 **/
	@Column(name = "PUND_PAID_TOTAL_MONTH", unique = false, nullable = true, length = 10)
	private Integer pundPaidTotalMonth;
	
	/** 主卡签名状况 **/
	@Column(name = "MAIN_CARD_SIGN_STATUS", unique = false, nullable = true, length = 5)
	private String mainCardSignStatus;
	
	/** 身份证明文件状况 **/
	@Column(name = "CERT_FILD_STATUS", unique = false, nullable = true, length = 5)
	private String certFildStatus;
	
	/** 工作证明文件状况 **/
	@Column(name = "WORK_FILE_STATUS", unique = false, nullable = true, length = 5)
	private String workFileStatus;
	
	/** 代发状况 **/
	@Column(name = "REPLACE_STATUS", unique = false, nullable = true, length = 5)
	private String replaceStatus;
	
	/** 代发工资金额 **/
	@Column(name = "REPLACE_PAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replacePayAmt;
	
	/** 个人年收入 **/
	@Column(name = "INDIV_YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal indivYearn;
	
	/** 投保状态 **/
	@Column(name = "INSURE_STATUS", unique = false, nullable = true, length = 5)
	private String insureStatus;
	
	/** 投保时间 **/
	@Column(name = "INDIV_INS_STR_DT", unique = false, nullable = true, length = 20)
	private String indivInsStrDt;
	
	/** 投保基数 **/
	@Column(name = "INSURE_BASE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal insureBase;
	
	/** 投保总月份 **/
	@Column(name = "INSURE_TOTAL_MONTH", unique = false, nullable = true, length = 10)
	private Integer insureTotalMonth;
	
	/** 我行存款状况 **/
	@Column(name = "DEP_STATUS", unique = false, nullable = true, length = 5)
	private String depStatus;
	
	/** 存款类型 **/
	@Column(name = "DEP_TYPE", unique = false, nullable = true, length = 5)
	private String depType;
	
	/** 开户日期 **/
	@Column(name = "OPEN_DATE", unique = false, nullable = true, length = 10)
	private String openDate;
	
	/** 年日均存款 **/
	@Column(name = "DAY_DEP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dayDep;
	
	/** 我行理财状况 **/
	@Column(name = "CHREM_STATUS", unique = false, nullable = true, length = 5)
	private String chremStatus;
	
	/** 已购理财产品期限 **/
	@Column(name = "CHREM_TERM", unique = false, nullable = true, length = 10)
	private Integer chremTerm;
	
	/** 已购我行理财产品金额 **/
	@Column(name = "CHREM_PRD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chremPrdAmt;
	
	/** 我行贷款状况 **/
	@Column(name = "LOAN_STATUS", unique = false, nullable = true, length = 5)
	private String loanStatus;
	
	/** 贷款类型 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private Integer loanTerm;
	
	/** 月还款额 **/
	@Column(name = "MONTH_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal monthRepayAmt;
	
	/** 房产信息状况 **/
	@Column(name = "HOUSE_STATUS", unique = false, nullable = true, length = 5)
	private String houseStatus;
	
	/** 房产类型 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 5)
	private String houseType;
	
	/** 房产总价值 **/
	@Column(name = "HOUSE_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseValue;
	
	/** 房产贷款金额 **/
	@Column(name = "HOUSE_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseLoanAmt;
	
	/** 房贷贷款期限 **/
	@Column(name = "HOUSE_LOAN_TERM", unique = false, nullable = true, length = 10)
	private Integer houseLoanTerm;
	
	/** 房贷月还款额 **/
	@Column(name = "HOUSE_LOAN_MONTH_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseLoanMonthRepayAmt;
	
	/** 是否企业法人 **/
	@Column(name = "IS_REPR", unique = false, nullable = true, length = 5)
	private String isRepr;
	
	/** 公司成立日期 **/
	@Column(name = "COM_START_DATE", unique = false, nullable = true, length = 20)
	private String comStartDate;
	
	/** 公司注册资金 **/
	@Column(name = "COM_REGI_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal comRegiCap;
	
	/** 是否个体工商户 **/
	@Column(name = "IS_INDIV_SHOP", unique = false, nullable = true, length = 5)
	private String isIndivShop;
	
	/** 发照日期 **/
	@Column(name = "LICD_ATE", unique = false, nullable = true, length = 20)
	private String licdAte;
	
	/** 执照有效期 **/
	@Column(name = "BSINS_LIC_IDARE", unique = false, nullable = true, length = 20)
	private String bsinsLicIdare;
	
	/** 居住证明状况 **/
	@Column(name = "INDIV_RSD_ST", unique = false, nullable = true, length = 5)
	private String indivRsdSt;
	
	/** 地址是否一致 **/
	@Column(name = "IS_SAME_ADDR", unique = false, nullable = true, length = 5)
	private String isSameAddr;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param pundStatus
	 */
	public void setPundStatus(String pundStatus) {
		this.pundStatus = pundStatus;
	}
	
    /**
     * @return pundStatus
     */
	public String getPundStatus() {
		return this.pundStatus;
	}
	
	/**
	 * @param pundPaidDate
	 */
	public void setPundPaidDate(String pundPaidDate) {
		this.pundPaidDate = pundPaidDate;
	}
	
    /**
     * @return pundPaidDate
     */
	public String getPundPaidDate() {
		return this.pundPaidDate;
	}
	
	/**
	 * @param pundDepositBase
	 */
	public void setPundDepositBase(java.math.BigDecimal pundDepositBase) {
		this.pundDepositBase = pundDepositBase;
	}
	
    /**
     * @return pundDepositBase
     */
	public java.math.BigDecimal getPundDepositBase() {
		return this.pundDepositBase;
	}
	
	/**
	 * @param pundPaidTotalMonth
	 */
	public void setPundPaidTotalMonth(Integer pundPaidTotalMonth) {
		this.pundPaidTotalMonth = pundPaidTotalMonth;
	}
	
    /**
     * @return pundPaidTotalMonth
     */
	public Integer getPundPaidTotalMonth() {
		return this.pundPaidTotalMonth;
	}
	
	/**
	 * @param mainCardSignStatus
	 */
	public void setMainCardSignStatus(String mainCardSignStatus) {
		this.mainCardSignStatus = mainCardSignStatus;
	}
	
    /**
     * @return mainCardSignStatus
     */
	public String getMainCardSignStatus() {
		return this.mainCardSignStatus;
	}
	
	/**
	 * @param certFildStatus
	 */
	public void setCertFildStatus(String certFildStatus) {
		this.certFildStatus = certFildStatus;
	}
	
    /**
     * @return certFildStatus
     */
	public String getCertFildStatus() {
		return this.certFildStatus;
	}
	
	/**
	 * @param workFileStatus
	 */
	public void setWorkFileStatus(String workFileStatus) {
		this.workFileStatus = workFileStatus;
	}
	
    /**
     * @return workFileStatus
     */
	public String getWorkFileStatus() {
		return this.workFileStatus;
	}
	
	/**
	 * @param replaceStatus
	 */
	public void setReplaceStatus(String replaceStatus) {
		this.replaceStatus = replaceStatus;
	}
	
    /**
     * @return replaceStatus
     */
	public String getReplaceStatus() {
		return this.replaceStatus;
	}
	
	/**
	 * @param replacePayAmt
	 */
	public void setReplacePayAmt(java.math.BigDecimal replacePayAmt) {
		this.replacePayAmt = replacePayAmt;
	}
	
    /**
     * @return replacePayAmt
     */
	public java.math.BigDecimal getReplacePayAmt() {
		return this.replacePayAmt;
	}
	
	/**
	 * @param indivYearn
	 */
	public void setIndivYearn(java.math.BigDecimal indivYearn) {
		this.indivYearn = indivYearn;
	}
	
    /**
     * @return indivYearn
     */
	public java.math.BigDecimal getIndivYearn() {
		return this.indivYearn;
	}
	
	/**
	 * @param insureStatus
	 */
	public void setInsureStatus(String insureStatus) {
		this.insureStatus = insureStatus;
	}
	
    /**
     * @return insureStatus
     */
	public String getInsureStatus() {
		return this.insureStatus;
	}
	
	/**
	 * @param indivInsStrDt
	 */
	public void setIndivInsStrDt(String indivInsStrDt) {
		this.indivInsStrDt = indivInsStrDt;
	}
	
    /**
     * @return indivInsStrDt
     */
	public String getIndivInsStrDt() {
		return this.indivInsStrDt;
	}
	
	/**
	 * @param insureBase
	 */
	public void setInsureBase(java.math.BigDecimal insureBase) {
		this.insureBase = insureBase;
	}
	
    /**
     * @return insureBase
     */
	public java.math.BigDecimal getInsureBase() {
		return this.insureBase;
	}
	
	/**
	 * @param insureTotalMonth
	 */
	public void setInsureTotalMonth(Integer insureTotalMonth) {
		this.insureTotalMonth = insureTotalMonth;
	}
	
    /**
     * @return insureTotalMonth
     */
	public Integer getInsureTotalMonth() {
		return this.insureTotalMonth;
	}
	
	/**
	 * @param depStatus
	 */
	public void setDepStatus(String depStatus) {
		this.depStatus = depStatus;
	}
	
    /**
     * @return depStatus
     */
	public String getDepStatus() {
		return this.depStatus;
	}
	
	/**
	 * @param depType
	 */
	public void setDepType(String depType) {
		this.depType = depType;
	}
	
    /**
     * @return depType
     */
	public String getDepType() {
		return this.depType;
	}
	
	/**
	 * @param openDate
	 */
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	
    /**
     * @return openDate
     */
	public String getOpenDate() {
		return this.openDate;
	}
	
	/**
	 * @param dayDep
	 */
	public void setDayDep(java.math.BigDecimal dayDep) {
		this.dayDep = dayDep;
	}
	
    /**
     * @return dayDep
     */
	public java.math.BigDecimal getDayDep() {
		return this.dayDep;
	}
	
	/**
	 * @param chremStatus
	 */
	public void setChremStatus(String chremStatus) {
		this.chremStatus = chremStatus;
	}
	
    /**
     * @return chremStatus
     */
	public String getChremStatus() {
		return this.chremStatus;
	}
	
	/**
	 * @param chremTerm
	 */
	public void setChremTerm(Integer chremTerm) {
		this.chremTerm = chremTerm;
	}
	
    /**
     * @return chremTerm
     */
	public Integer getChremTerm() {
		return this.chremTerm;
	}
	
	/**
	 * @param chremPrdAmt
	 */
	public void setChremPrdAmt(java.math.BigDecimal chremPrdAmt) {
		this.chremPrdAmt = chremPrdAmt;
	}
	
    /**
     * @return chremPrdAmt
     */
	public java.math.BigDecimal getChremPrdAmt() {
		return this.chremPrdAmt;
	}
	
	/**
	 * @param loanStatus
	 */
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}
	
    /**
     * @return loanStatus
     */
	public String getLoanStatus() {
		return this.loanStatus;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public Integer getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param monthRepayAmt
	 */
	public void setMonthRepayAmt(java.math.BigDecimal monthRepayAmt) {
		this.monthRepayAmt = monthRepayAmt;
	}
	
    /**
     * @return monthRepayAmt
     */
	public java.math.BigDecimal getMonthRepayAmt() {
		return this.monthRepayAmt;
	}
	
	/**
	 * @param houseStatus
	 */
	public void setHouseStatus(String houseStatus) {
		this.houseStatus = houseStatus;
	}
	
    /**
     * @return houseStatus
     */
	public String getHouseStatus() {
		return this.houseStatus;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param houseValue
	 */
	public void setHouseValue(java.math.BigDecimal houseValue) {
		this.houseValue = houseValue;
	}
	
    /**
     * @return houseValue
     */
	public java.math.BigDecimal getHouseValue() {
		return this.houseValue;
	}
	
	/**
	 * @param houseLoanAmt
	 */
	public void setHouseLoanAmt(java.math.BigDecimal houseLoanAmt) {
		this.houseLoanAmt = houseLoanAmt;
	}
	
    /**
     * @return houseLoanAmt
     */
	public java.math.BigDecimal getHouseLoanAmt() {
		return this.houseLoanAmt;
	}
	
	/**
	 * @param houseLoanTerm
	 */
	public void setHouseLoanTerm(Integer houseLoanTerm) {
		this.houseLoanTerm = houseLoanTerm;
	}
	
    /**
     * @return houseLoanTerm
     */
	public Integer getHouseLoanTerm() {
		return this.houseLoanTerm;
	}
	
	/**
	 * @param houseLoanMonthRepayAmt
	 */
	public void setHouseLoanMonthRepayAmt(java.math.BigDecimal houseLoanMonthRepayAmt) {
		this.houseLoanMonthRepayAmt = houseLoanMonthRepayAmt;
	}
	
    /**
     * @return houseLoanMonthRepayAmt
     */
	public java.math.BigDecimal getHouseLoanMonthRepayAmt() {
		return this.houseLoanMonthRepayAmt;
	}
	
	/**
	 * @param isRepr
	 */
	public void setIsRepr(String isRepr) {
		this.isRepr = isRepr;
	}
	
    /**
     * @return isRepr
     */
	public String getIsRepr() {
		return this.isRepr;
	}
	
	/**
	 * @param comStartDate
	 */
	public void setComStartDate(String comStartDate) {
		this.comStartDate = comStartDate;
	}
	
    /**
     * @return comStartDate
     */
	public String getComStartDate() {
		return this.comStartDate;
	}
	
	/**
	 * @param comRegiCap
	 */
	public void setComRegiCap(java.math.BigDecimal comRegiCap) {
		this.comRegiCap = comRegiCap;
	}
	
    /**
     * @return comRegiCap
     */
	public java.math.BigDecimal getComRegiCap() {
		return this.comRegiCap;
	}
	
	/**
	 * @param isIndivShop
	 */
	public void setIsIndivShop(String isIndivShop) {
		this.isIndivShop = isIndivShop;
	}
	
    /**
     * @return isIndivShop
     */
	public String getIsIndivShop() {
		return this.isIndivShop;
	}
	
	/**
	 * @param licdAte
	 */
	public void setLicdAte(String licdAte) {
		this.licdAte = licdAte;
	}
	
    /**
     * @return licdAte
     */
	public String getLicdAte() {
		return this.licdAte;
	}
	
	/**
	 * @param bsinsLicIdare
	 */
	public void setBsinsLicIdare(String bsinsLicIdare) {
		this.bsinsLicIdare = bsinsLicIdare;
	}
	
    /**
     * @return bsinsLicIdare
     */
	public String getBsinsLicIdare() {
		return this.bsinsLicIdare;
	}
	
	/**
	 * @param indivRsdSt
	 */
	public void setIndivRsdSt(String indivRsdSt) {
		this.indivRsdSt = indivRsdSt;
	}
	
    /**
     * @return indivRsdSt
     */
	public String getIndivRsdSt() {
		return this.indivRsdSt;
	}
	
	/**
	 * @param isSameAddr
	 */
	public void setIsSameAddr(String isSameAddr) {
		this.isSameAddr = isSameAddr;
	}
	
    /**
     * @return isSameAddr
     */
	public String getIsSameAddr() {
		return this.isSameAddr;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}