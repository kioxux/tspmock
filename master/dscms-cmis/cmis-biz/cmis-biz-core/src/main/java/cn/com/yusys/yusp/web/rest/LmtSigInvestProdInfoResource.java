/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestProdInfo;
import cn.com.yusys.yusp.service.LmtSigInvestProdInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 11:05:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestprodinfo")
public class LmtSigInvestProdInfoResource {
    @Autowired
    private LmtSigInvestProdInfoService lmtSigInvestProdInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestProdInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestProdInfo> list = lmtSigInvestProdInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestProdInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestProdInfo>> index(QueryModel queryModel) {
        List<LmtSigInvestProdInfo> list = lmtSigInvestProdInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestProdInfo>>(list);
    }

    /**
     * @函数名称:selectall
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectall")
    protected ResultDto<List<LmtSigInvestProdInfo>> selectall(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestProdInfo> list = lmtSigInvestProdInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestProdInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestProdInfo> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestProdInfo lmtSigInvestProdInfo = lmtSigInvestProdInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestProdInfo>(lmtSigInvestProdInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestProdInfo> create(@RequestBody LmtSigInvestProdInfo lmtSigInvestProdInfo) throws URISyntaxException {
        lmtSigInvestProdInfoService.insert(lmtSigInvestProdInfo);
        return new ResultDto<LmtSigInvestProdInfo>(lmtSigInvestProdInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestProdInfo lmtSigInvestProdInfo) throws URISyntaxException {
        int result = lmtSigInvestProdInfoService.update(lmtSigInvestProdInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestProdInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestProdInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateCpfx")
    protected ResultDto<Integer> updateCpfx(@RequestBody LmtSigInvestProdInfo lmtSigInvestProdInfo) throws URISyntaxException {
        int result = lmtSigInvestProdInfoService.updateCpfx(lmtSigInvestProdInfo);
        return new ResultDto<Integer>(result);
    }
}
