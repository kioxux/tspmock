package cn.com.yusys.yusp.constant;

import cn.com.yusys.yusp.commons.session.user.User;

import java.util.Comparator;

/**
 * 用于对项目池下用户集合进行排序的比较器
 */
public class BizTaskPoolUsersComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        return o2.compareTo(o1);
    }
}
