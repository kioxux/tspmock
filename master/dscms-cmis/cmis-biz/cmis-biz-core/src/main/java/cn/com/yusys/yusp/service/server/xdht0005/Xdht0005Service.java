package cn.com.yusys.yusp.service.server.xdht0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.resp.Xdht0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0005Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0005Service.class);

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Resource
    private GrtGuarContMapper grtGuarContMapper;

    /**
     * 合同面签信息列表查询
     *
     * @param xdht0005DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0005DataRespDto getXdht0005(Xdht0005DataReqDto xdht0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataReqDto));
        Xdht0005DataRespDto xdht0005DataRespDto = new Xdht0005DataRespDto();
        try {
            //起始页
            int startPageNum = xdht0005DataReqDto.getStartPageNum();
            //条数
            int pageSize = xdht0005DataReqDto.getPageSize();
            //合同类型
            String contType = xdht0005DataReqDto.getContType();
            PageHelper.startPage(startPageNum, pageSize);
            if (Objects.equals(CommonConstance.CONT_TYPE_01, contType)) {
                //借款合同
                java.util.List<cn.com.yusys.yusp.dto.server.xdht0005.resp.List> list = ctrLoanContMapper.getContInfoByContType(xdht0005DataReqDto);
                xdht0005DataRespDto.setList(list);
            } else if (Objects.equals(CommonConstance.CONT_TYPE_02, contType)) {
                //担保合同
                //合同状态字典转义
                xdht0005DataReqDto.setContStatus(xdht0005DataReqDto.getContStatus() == "200" ? "101" : "100");
                java.util.List<cn.com.yusys.yusp.dto.server.xdht0005.resp.List> list = grtGuarContMapper.getContInfoByContType(xdht0005DataReqDto);
                xdht0005DataRespDto.setList(list);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0005.key, DscmsEnum.TRADE_CODE_XDHT0005.value, JSON.toJSONString(xdht0005DataRespDto));
        return xdht0005DataRespDto;
    }
}
