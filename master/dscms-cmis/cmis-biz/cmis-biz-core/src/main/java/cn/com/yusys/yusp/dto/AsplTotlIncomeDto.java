package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplTotlIncome
 * @类描述: aspl_totl_income数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-04 14:09:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AsplTotlIncomeDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 资产池协议编号 **/
	private String contNo;

	/** 资产池额度合计 **/
	private String contAmt;

	/** 池内资产价值合计 **/
	private String inpAssetValue;

	/** 资产池起始日期 **/
	private String startDate;

	/** 资产池到期日期 **/
	private String endDate;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContAmt() {
		return contAmt;
	}

	public void setContAmt(String contAmt) {
		this.contAmt = contAmt;
	}

	public String getInpAssetValue() {
		return inpAssetValue;
	}

	public void setInpAssetValue(String inpAssetValue) {
		this.inpAssetValue = inpAssetValue;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

}