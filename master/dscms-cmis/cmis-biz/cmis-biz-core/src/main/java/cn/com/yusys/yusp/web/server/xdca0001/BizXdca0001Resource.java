package cn.com.yusys.yusp.web.server.xdca0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0001.req.Xdca0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0001.resp.Xdca0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdca0001.Xdca0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;

/**
 * 接口处理类:信用卡调额申请
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDCA0001:信用卡调额申请")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0001Resource.class);

    @Autowired
    private Xdca0001Service xdca0001Service;

    /**
     * 交易码：xdca0001
     * 交易描述：信用卡调额申请
     *
     * @param xdca0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信用卡调额申请")
    @PostMapping("/xdca0001")
    protected @ResponseBody
    ResultDto<Xdca0001DataRespDto> xdca0001(@Validated @RequestBody Xdca0001DataReqDto xdca0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataReqDto));
        Xdca0001DataRespDto xdca0001DataRespDto = new Xdca0001DataRespDto();// 响应Dto:信用卡调额申请
        ResultDto<Xdca0001DataRespDto> xdca0001DataResultDto = new ResultDto<>();
        String cardNo = xdca0001DataReqDto.getCardNo();//卡号
        String cardPrd = xdca0001DataReqDto.getCardPrd();//卡产品
        String certType = xdca0001DataReqDto.getCertType();//证件类型
        String certNo = xdca0001DataReqDto.getCertNo();//证件号码
        String holdCardName = xdca0001DataReqDto.getHoldCardName();//持卡人姓名
        BigDecimal cdtAmt = xdca0001DataReqDto.getCdtAmt();//信用额度
        BigDecimal newAmt = xdca0001DataReqDto.getNewAmt();//新额度
        String isProvidCdtProve = xdca0001DataReqDto.getIsProvidCdtProve();//是否提供增信证明
        String memo = xdca0001DataReqDto.getMemo();//备注
        try {
            Boolean flag = true;
            if (StringUtil.isEmpty(cardNo)) {
                flag = false;
            } else if (StringUtil.isEmpty(cardPrd)) {
                flag = false;
            } else if (StringUtil.isEmpty(certType)) {
                flag = false;
            } else if (StringUtil.isEmpty(certNo)) {
                flag = false;
            } else if (StringUtil.isEmpty(holdCardName)) {
                flag = false;
            }
            if (flag) {
                // 从xdca0001DataReqDto获取业务值进行业务逻辑处理
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataReqDto));
                xdca0001DataRespDto = xdca0001Service.xdca0001(xdca0001DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataRespDto));
                // 封装xdca0001DataResultDto中正确的返回码和返回信息
                xdca0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdca0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {//请求字段存在空值
                // 封装xdca0001DataResultDto中正确的返回码和返回信息
                xdca0001DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdca0001DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, e.getMessage());
            xdca0001DataResultDto.setCode(e.getErrorCode());
            xdca0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, e.getMessage());
            // 封装xdca0001DataResultDto中异常返回码和返回信息
            xdca0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdca0001DataRespDto到xdca0001DataResultDto中
        xdca0001DataResultDto.setData(xdca0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataResultDto));
        return xdca0001DataResultDto;
    }
}
