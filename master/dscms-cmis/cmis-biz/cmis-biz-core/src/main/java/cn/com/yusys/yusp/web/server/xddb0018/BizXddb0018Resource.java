package cn.com.yusys.yusp.web.server.xddb0018;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0018.req.Xddb0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0018.resp.Xddb0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0018.Xddb0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:押品状态同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDDB0018:押品状态同步")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0018Resource.class);

    @Autowired
    private Xddb0018Service xddb0018Service;

    /**
     * 交易码：xddb0018
     * 交易描述：押品状态同步
     *
     * @param xddb0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0018:押品状态同步")
    @PostMapping("/xddb0018")
    protected @ResponseBody
    ResultDto<Xddb0018DataRespDto> xddb0018(@Validated @RequestBody Xddb0018DataReqDto xddb0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataReqDto));
        Xddb0018DataRespDto xddb0018DataRespDto = new Xddb0018DataRespDto();// 响应Dto:押品状态同步
        ResultDto<Xddb0018DataRespDto> xddb0018DataResultDto = new ResultDto<>();
        // 从xddb0018DataReqDto获取业务值进行业务逻辑处理
        String guarNo = xddb0018DataReqDto.getGuarNo();//押品编号
        String guarStatus = xddb0018DataReqDto.getGuarStatus();//押品状态
        try {
            if (StringUtil.isNotEmpty(guarNo) && StringUtil.isNotEmpty(guarStatus)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataReqDto));
                xddb0018DataRespDto = xddb0018Service.updateWarrantSatteByGuarNo(xddb0018DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataRespDto));
                // 封装xddb0018DataResultDto中正确的返回码和返回信息
                xddb0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                //请求字段存在空值
                logger.info("**************************调用xddb0018Service结束*END*************************");
                xddb0018DataResultDto.setCode(EcbEnum.ECB010001.key);
                xddb0018DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (YuspException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            // 封装xddb0018DataResultDto中异常返回码和返回信息
            xddb0018DataResultDto.setCode(e.getCode());
            xddb0018DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            // 封装xddb0018DataResultDto中异常返回码和返回信息
            xddb0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0018DataRespDto到xddb0018DataResultDto中
        xddb0018DataResultDto.setData(xddb0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataResultDto));
        return xddb0018DataResultDto;
    }

}
