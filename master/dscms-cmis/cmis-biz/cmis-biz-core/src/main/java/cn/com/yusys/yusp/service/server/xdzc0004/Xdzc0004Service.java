package cn.com.yusys.yusp.service.server.xdzc0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.dto.server.xdzc0004.req.Xdzc0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.resp.List;
import cn.com.yusys.yusp.dto.server.xdzc0004.resp.Xdzc0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AsplAssetsListMapper;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 接口处理类:客户资产清单查询接口
 *
 * @Author xs
 * @Date 2021/06/03 16:20
 * @Version 1.0
 */
@Service
public class Xdzc0004Service {

    @Autowired
    private AsplAssetsListService asplAssetsListservice;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0004.Xdzc0004Service.class);
    @Transactional
    public Xdzc0004DataRespDto xdzc0004Service(Xdzc0004DataReqDto xdzc0004DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value);
        Xdzc0004DataRespDto xdzc0004DataRespDto = new Xdzc0004DataRespDto();
        String cusId = xdzc0004DataReqDto.getCusId();//客户编号
        String assetType = xdzc0004DataReqDto.getAssetType();//资产类型
        String assetStartDate = xdzc0004DataReqDto.getAssetStartDate();//资产到期日起
        String assetEndDate = xdzc0004DataReqDto.getAssetEndDate();//资产到期日止
        BigDecimal assetCeiling = xdzc0004DataReqDto.getAssetCeiling();//资产价值上限
        BigDecimal assetFloor = xdzc0004DataReqDto.getAssetFloor();//资产价值下限
        String isAllQuery = xdzc0004DataReqDto.getIsAllQuery();//是否查询全量数据
        String queryType = xdzc0004DataReqDto.getQueryType();// 查询方式 0、所有资产 1、池内资产 2、可出池资产清单
        int page = xdzc0004DataReqDto.getPage();//页数
        int size = xdzc0004DataReqDto.getSize();//单页大小

        long count = 0;
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId",cusId);//客户编号
            queryModel.addCondition("assetType",assetType);//资产类型
            queryModel.addCondition("assetStartDate",assetStartDate);//资产到期日起
            queryModel.addCondition("assetEndDate",assetEndDate);//资产到期日止
            queryModel.addCondition("assetCeiling",assetCeiling);//资产价值上限
            queryModel.addCondition("assetFloor",assetFloor);//资产价值下限
            queryModel.setSort("INP_TIME DESC");// 后进先出法
            if(Objects.equals("1",queryType)){
                // 查询池内资产
                queryModel.addCondition("isPool",CommonConstance.STD_ZB_YES_NO_1);
                queryModel.addCondition("isPledge",CommonConstance.STD_ZB_YES_NO_1);
            }else if (Objects.equals("2",queryType)){
                // 可出池资产清单 (出池日<=到期日前一天) => 营业日期+1 <= 到期日
                queryModel.addCondition("isPool",CommonConstance.STD_ZB_YES_NO_1);
                queryModel.addCondition("isPledge",CommonConstance.STD_ZB_YES_NO_1);
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                queryModel.addCondition("assetStartDate", DateUtils.addDay(DateUtils.parseDate(openday,DateFormatEnum.DEFAULT.getValue()),DateFormatEnum.DEFAULT.getValue(),1));//资产到期日止
            }

            /**
             * 1是（全量查询）
             * 0否（非全量查询）
             * 如果查询全量数据则不需要传页数大小
             */
            java.util.List<AsplAssetsList> asplAssetsLists = null;
            if (CommonConstance.STD_ZB_YES_NO_0.equals(isAllQuery)) {
                //0否（非全量查询）
                PageHelper.startPage(page, size);
                asplAssetsLists = asplAssetsListservice.selectByModelDate(queryModel);
                PageInfo<AsplAssetsList> pageinfo = new PageInfo<>(asplAssetsLists);
                count = pageinfo.getTotal();
                PageHelper.clearPage();
            } else if(CommonConstance.STD_ZB_YES_NO_1.equals(isAllQuery)){
                //1是（全量查询）
                asplAssetsLists = asplAssetsListservice.selectByModelDate(queryModel);
                count = asplAssetsLists.size();
            }
            if(CollectionUtils.isEmpty(asplAssetsLists)){
                xdzc0004DataRespDto.setList(new java.util.ArrayList<List>());
                xdzc0004DataRespDto.setTotal(count);
                return xdzc0004DataRespDto;
            }
            java.util.List<List> lists = null;
            if(null != asplAssetsLists){
                lists = asplAssetsLists.parallelStream().map(e->{
                    List list = new List();
                    list.setAssetNo(e.getAssetNo());// 资产编号
                    list.setGuarNo(e.getGuarNo());// 统一押品编号
                    list.setAssetType(e.getAssetType());// 资产类型
                    list.setAssetValue(e.getAssetValue());// 资产价值
                    list.setAssetEndDate(e.getAssetEndDate());// 资产到期日
                    list.setAssetStatus(e.getAssetStatus());// 资产状态
                    list.setIsPool(e.getIsPool());// 是否入池
                    list.setIsPledge(e.getIsPledge());// 是否入池质押
                    list.setInpTime(e.getInpTime());// 入池时间
                    list.setOutpTime(e.getOutpTime());// 出池时间
                    return list;
                }).collect(Collectors.toList());

                xdzc0004DataRespDto.setList(lists);
                xdzc0004DataRespDto.setTotal(count);
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value);
        return xdzc0004DataRespDto;
    }

}
