/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusDetailsLimit
 * @类描述: lmt_app_rel_cus_details_limit数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 16:34:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_rel_cus_details_limit")
public class LmtAppRelCusDetailsLimit extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
	private String subSerno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 分项额度编号 **/
	@Column(name = "SUB_LMT_NO", unique = false, nullable = true, length = 40)
	private String subLmtNo;
	
	/** 授信品种编号 **/
	@Column(name = "LMT_BIZ_TYPE", unique = false, nullable = true, length = 20)
	private String lmtBizType;
	
	/** 授信品种名称 **/
	@Column(name = "LMT_BIZ_TYPE_NAME", unique = false, nullable = true, length = 200)
	private String lmtBizTypeName;
	
	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 授信余额 **/
	@Column(name = "LMT_BALANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtBalanceAmt;
	
	/** 已用额度 **/
	@Column(name = "OUTSTND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outstndAmt;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 可用额度 **/
	@Column(name = "AVL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal avlLmtAmt;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 额度到期日 **/
	@Column(name = "LMT_END_DATE", unique = false, nullable = true, length = 20)
	private String lmtEndDate;
	
	/** 投资资产名称 **/
	@Column(name = "INVEST_ASSET_NAME", unique = false, nullable = true, length = 200)
	private String investAssetName;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 200)
	private String prdName;
	
	/** 交易日期 **/
	@Column(name = "TRAN_DATE", unique = false, nullable = true, length = 20)
	private String tranDate;
	
	/** 资产到期日 **/
	@Column(name = "ASSET_END_DATE", unique = false, nullable = true, length = 20)
	private String assetEndDate;
	
	/** 额度分类 **/
	@Column(name = "LMT_CATALOG", unique = false, nullable = true, length = 5)
	private String lmtCatalog;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param subLmtNo
	 */
	public void setSubLmtNo(String subLmtNo) {
		this.subLmtNo = subLmtNo;
	}
	
    /**
     * @return subLmtNo
     */
	public String getSubLmtNo() {
		return this.subLmtNo;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}
	
    /**
     * @return lmtBizType
     */
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName;
	}
	
    /**
     * @return lmtBizTypeName
     */
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtBalanceAmt
	 */
	public void setLmtBalanceAmt(java.math.BigDecimal lmtBalanceAmt) {
		this.lmtBalanceAmt = lmtBalanceAmt;
	}
	
    /**
     * @return lmtBalanceAmt
     */
	public java.math.BigDecimal getLmtBalanceAmt() {
		return this.lmtBalanceAmt;
	}
	
	/**
	 * @param outstndAmt
	 */
	public void setOutstndAmt(java.math.BigDecimal outstndAmt) {
		this.outstndAmt = outstndAmt;
	}
	
    /**
     * @return outstndAmt
     */
	public java.math.BigDecimal getOutstndAmt() {
		return this.outstndAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param avlLmtAmt
	 */
	public void setAvlLmtAmt(java.math.BigDecimal avlLmtAmt) {
		this.avlLmtAmt = avlLmtAmt;
	}
	
    /**
     * @return avlLmtAmt
     */
	public java.math.BigDecimal getAvlLmtAmt() {
		return this.avlLmtAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param investAssetName
	 */
	public void setInvestAssetName(String investAssetName) {
		this.investAssetName = investAssetName;
	}
	
    /**
     * @return investAssetName
     */
	public String getInvestAssetName() {
		return this.investAssetName;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	
    /**
     * @return tranDate
     */
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param assetEndDate
	 */
	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}
	
    /**
     * @return assetEndDate
     */
	public String getAssetEndDate() {
		return this.assetEndDate;
	}
	
	/**
	 * @param lmtCatalog
	 */
	public void setLmtCatalog(String lmtCatalog) {
		this.lmtCatalog = lmtCatalog;
	}
	
    /**
     * @return lmtCatalog
     */
	public String getLmtCatalog() {
		return this.lmtCatalog;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}