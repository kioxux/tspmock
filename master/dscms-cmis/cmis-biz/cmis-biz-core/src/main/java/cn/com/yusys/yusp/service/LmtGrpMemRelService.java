/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.SequenceConfig;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;

import cn.com.yusys.yusp.dto.*;

import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppSubPrdMapper;
import cn.com.yusys.yusp.repository.mapper.LmtGrpMemRelMapper;

import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import org.ehcache.core.util.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpMemRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-08 21:01:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpMemRelService {

    @Resource
    private LmtGrpMemRelMapper lmtGrpMemRelMapper;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Resource
    private LmtAppSubPrdMapper lmtAppSubPrdMapper;
    @Resource
    private ICusClientService iCusClientService;

    @Autowired
    private LmtHighCurfundEvalService lmtHighCurfundEvalService;

    @Autowired
    private LmtGrpReplyChgService lmtGrpReplyChgService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private LmtGrpApprService lmtGrpApprService;


    private static final Logger log = LoggerFactory.getLogger(LmtGrpMemRelService.class);


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpMemRel selectByPrimaryKey(String pkId) {
        return lmtGrpMemRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtGrpMemRel> selectAll(QueryModel model) {
        List<LmtGrpMemRel> records = (List<LmtGrpMemRel>) lmtGrpMemRelMapper.selectByModel(model);
        return records;
    }


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpMemRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpMemRel> list = lmtGrpMemRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtGrpMemRel record) {
        return lmtGrpMemRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpMemRel record) {
        return lmtGrpMemRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(LmtGrpMemRel record) {
        return lmtGrpMemRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpMemRel record) {
        return lmtGrpMemRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpMemRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpMemRelMapper.deleteByIds(ids);
    }


    /**
     * @函数名称:queryLmtGrpMemRelByParams
     * @函数描述:通过组合参数查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpMemRel> queryLmtGrpMemRelByParams(HashMap<String, String> params) {
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return lmtGrpMemRelMapper.queryLmtGrpMemRelByParams(params);
    }

    /**
     * @函数名称:queryLmtGrpMemRelByGrpSerno
     * @函数描述:通过集团申请流水号查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpMemRel> queryLmtGrpMemRelByGrpSerno(String grpSerno) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("grpSerno", grpSerno);
        return this.queryLmtGrpMemRelByParams(hashMap);
    }

    /**
     * @函数名称:queryLmtGrpMemRelByGrpSernoAndCusId
     * @函数描述:通过集团申请流水号查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpMemRel queryLmtGrpMemRelByGrpSernoAndCusId(String grpSerno, String cusId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("grpSerno", grpSerno);
        hashMap.put("cusId", cusId);
        hashMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByParams(hashMap);
        if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() == 1) {
            return lmtGrpMemRelList.get(0);
        } else {
            return null;
        }
    }

    /**
     * @函数名称:queryLmtGrpMemRelByGrpSernoAndCusId
     * @函数描述:通过集团申请流水号查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpMemRel queryLmtGrpMemRelBySingleSerno(String singleSerno) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("singleSerno", singleSerno);
        hashMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByParams(hashMap);
        if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() == 1) {
            return lmtGrpMemRelList.get(0);
        } else {
            return null;
        }
    }

    /**
     * @函数名称:queryLmtGrpMemRelByGrpSernoForMgrId
     * @函数描述:通过集团申请流水号和主管客户经理查询集团成员关联信息数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpMemRel> queryLmtGrpMemRelByGrpSernoAndMgr(String grpSerno) {
        HashMap<String, String> hashMap = new HashMap<>();
        User userInfo = SessionUtils.getUserInformation();
        String managerId = userInfo.getLoginCode();
        hashMap.put("grpSerno", grpSerno);
        hashMap.put("managerId", managerId);
        hashMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return this.queryLmtGrpMemRelByParams(hashMap);
    }


    /**
     * @方法名称: saveByQueryGrpMemRel
     * @方法描述: 根据调用cmis_cus服务获取当前集团关联成员关系，插入集团业务申请成员关系表
     * @参数与返回说明:
     * @算法描述: 1.根据调用cmis_cus服务获取当前集团关联成员关系，插入集团业务申请成员关系表
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void saveByQueryGrpMemRel(String grpNo, String serno) {
        CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
        cusGrpMemberRelDto.setGrpNo(grpNo);
        List<CusGrpMemberRelDto> cusGrpMemberRelDtoList = JSONArray.parseArray(JSON.toJSONString(iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData()), CusGrpMemberRelDto.class);
        if (cusGrpMemberRelDtoList != null && cusGrpMemberRelDtoList.size() > 0) {
            User userInfo = SessionUtils.getUserInformation();
            for (Object o : cusGrpMemberRelDtoList) {
                CusGrpMemberRelDto item = ObjectMapperUtils.instance().convertValue(o, CusGrpMemberRelDto.class);
                //中间子表信息
                LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
                lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
                lmtGrpMemRel.setGrpSerno(serno);
                lmtGrpMemRel.setGrpCusId(grpNo);
                lmtGrpMemRel.setGrpCusName(item.getGrpName());
                lmtGrpMemRel.setCusId(item.getCusId());
                lmtGrpMemRel.setCusName(item.getCusName());
                lmtGrpMemRel.setManagerId(item.getManagerId());
                lmtGrpMemRel.setManagerBrId(item.getManagerBrId());
                lmtGrpMemRel.setCusType(item.getCusType());
                lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtGrpMemRel.setInputId(userInfo.getLoginCode());
                lmtGrpMemRel.setInputBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                this.lmtGrpMemRelMapper.insert(lmtGrpMemRel);

                // TODO 添加征信信息
                // 去除  by  xuchi
//                CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
//                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
//                creditReportQryLstAndRealDto.setCusId(lmtGrpMemRel.getCusId());
//                creditReportQryLstAndRealDto.setCusName(lmtGrpMemRel.getCusName());
//                creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
//                creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
//                creditReportQryLstAndRealDto.setBorrowRel("001");
//                String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
//                creditReportQryLstAndRealDto.setQryCls(qryCls);
//                creditReportQryLstAndRealDto.setApproveStatus("000");
//                creditReportQryLstAndRealDto.setIsSuccssInit("0");
//                creditReportQryLstAndRealDto.setQryFlag("02");
//                creditReportQryLstAndRealDto.setQryStatus("001");
//                creditReportQryLstAndRealDto.setManagerId(lmtGrpMemRel.getManagerId());
//                creditReportQryLstAndRealDto.setManagerBrId(lmtGrpMemRel.getManagerBrId());
//                //生成关联征信数据
//                creditReportQryLstAndRealDto.setBizSerno(serno);
//                creditReportQryLstAndRealDto.setScene("011");
//                ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
//                if (!doCreateCreditAuto.getCode().equals("0")) {
//                    log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
//                }
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010054.key, EcbEnum.ECB010054.value);
        }
    }

    /**
     * 集团客户流动资金额度试算
     *
     * @param
     * @return
     */
    public List<LmtGrpMemRel> getGrpHighCurfund(String serno) {
        return lmtGrpMemRelMapper.getGrpHighCurfund(serno);
    }

    /**
     * 集团成员客户限额及债项评级
     *
     * @param
     * @return
     */
    public List<Map> getGrpEvelList(String grpSerno) {
        return lmtGrpMemRelMapper.getGrpEvelList(grpSerno);
    }

    /**
     * 集团客户评级及债项评级
     *
     * @param
     * @return
     */
    public List<LmtGrpMemRel> getGrpLadEvalList(String grpCusId) {
        return lmtGrpMemRelMapper.getGrpLadEvalList(grpCusId);
    }

    /**
     * 集团客户评级及债项评级
     *
     * @param
     * @return
     */
    public List<LmtGrpMemRel> selectLmtGrpReplyByGrpSerno(String grpSerno) {
        return lmtGrpMemRelMapper.selectLmtGrpReplyByGrpSerno(grpSerno);
    }


    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp
     * @方法描述: 通过集团授信申请数据和集团审批信息生成审批关联数据
     * @参数与返回说明: 接受集团授信申请流水号，返回集团授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp(User userInfo, LmtGrpAppr newLmtGrpAppr, LmtGrpApp lmtGrpApp, String approveStatus) throws Exception {
        String lmtType = lmtGrpApp.getLmtType();
        List<LmtGrpMemRel> lmtGrpMemRelList = queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
        if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                // 授信新增 如果不参与填报  不为集团成员客户生成审批数据 add 20210716 mashun
                if (CmisCommonConstants.LMT_TYPE_02.equals(lmtType)) { // 变更
                    log.info("集团授信变更判断");
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtType)) { // 预授信细化
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtType)) { // 额度调剂
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                    if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                        log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                        // 不填报时,不校验当前成员客户
                        continue;
                    }
                }
                String singleSerno = lmtApprService.generateLmtApprInfo(lmtGrpMemRel.getSingleSerno(), approveStatus);
                log.info("集团授信审批通过申请生成审批数据，生成审批流水号为【{}】", singleSerno);
                if(singleSerno != null){
                    log.info("集团授信审批通过申请生成审批数据，生成审批流水号不为空，生成集团审批关联关系");
                    generateLmtGrpMemRelForLmtGrpApprForword(userInfo==null?"":userInfo.getLoginCode(), userInfo==null?"":userInfo.getOrg().getCode(), newLmtGrpAppr.getGrpApproveSerno(), lmtGrpMemRel, singleSerno);
                }
            }
        }
    }

    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprForword
     * @方法描述:
     * @参数与返回说明: item 的关系数据
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpMemRelForLmtGrpApprForword(String currentUserId, String currentOrgId, String grpSerno, LmtGrpMemRel item, String singleSerno) {
        LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
        BeanUtils.copyProperties(item, lmtGrpMemRel);
        lmtGrpMemRel.setSingleSerno(singleSerno);
        lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
        lmtGrpMemRel.setGrpSerno(grpSerno);
        lmtGrpMemRel.setUpdId(currentUserId);
        lmtGrpMemRel.setUpdBrId(currentOrgId);
        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(lmtGrpMemRel);
    }

    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprByBack
     * @方法描述:
     * @参数与返回说明: item 的关系数据
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpMemRelForLmtGrpApprByBack(String currentUserId, String currentOrgId, LmtGrpApp newLmtGrpApp, LmtGrpMemRel item, String singleSerno) {
        LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
        BeanUtils.copyProperties(item, lmtGrpMemRel);
        lmtGrpMemRel.setSingleSerno(singleSerno);
        lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
        // 默认取当前敞口和低风险额度   原取item中数据,但是一直存在敞口额度赋值出现问题且查不出原因,鉴于默认值相同,所以改为以下取数
        log.info("generateLmtGrpMemRelForLmtGrpApprByBack>>>>>保存新增的集团关系表中的原额度信息,原敞口额度[{}],原低风险额度[{}]", item.getOpenLmtAmt(), item.getLowRiskLmtAmt());
        lmtGrpMemRel.setOrigiLowRiskLmtAmt(item.getLowRiskLmtAmt());
        lmtGrpMemRel.setOrigiOpenLmtAmt(item.getOpenLmtAmt());
        if (CmisCommonConstants.LMT_TYPE_08.equals(newLmtGrpApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_02.equals(newLmtGrpApp.getLmtType())
                || CmisCommonConstants.LMT_TYPE_07.equals(newLmtGrpApp.getLmtType())) {
            // 02 变更 07 预授信细化 08 调剂 有单独的页面发起  不需要处理
            // lmtGrpMemRel.setManagerIdSubmitStatus(null);
            // 存在新增向导页面的 业务申请 下一步时不需要初始化客户经理提交状态
        } else {
            // 调用该方法: 续作 复议 再议 复审 ,需要点击提交后才可以初始化客户经理提交状态
            // lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_000);
            lmtGrpMemRel.setManagerIdSubmitStatus(null);
        }
        // 判断预授信细化情况下  上一步业务是否存在预授信额度
        if (CmisCommonConstants.LMT_TYPE_07.equals(newLmtGrpApp.getLmtType())) {
            String isContainPreLmt = this.getOldLmtGrpAppIsContainPreLmt(item.getSingleSerno());
            lmtGrpMemRel.setIsContainPreLmt(isContainPreLmt);
            // 如果不存在预授信细化的分项则自动默认为否
            if(CmisCommonConstants.YES_NO_0.equals(isContainPreLmt)){
                log.info("当前成员[{}]不存在预授信细化的分项!",item.getCusId());
                lmtGrpMemRel.setIsCurtRefine(isContainPreLmt);
            }
        }

        lmtGrpMemRel.setGrpSerno(newLmtGrpApp.getGrpSerno());
        lmtGrpMemRel.setUpdId(currentUserId);
        lmtGrpMemRel.setUpdBrId(currentOrgId);
        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(lmtGrpMemRel);
    }

    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprByBack
     * @方法描述: 预授信细化情况下  上一步业务是否存在预授信额度
     * @参数与返回说明: item 的关系数据
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-06 16:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public String getOldLmtGrpAppIsContainPreLmt(String singleSerno) {
        /*
         * 预授信细化情况下  上一步业务是否存在预授信额度,只要存在一笔业务是预授信 那么跳出循环
         * */
        List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.selectByAccNo(singleSerno);
        long res = 0;
        if (CollectionUtils.nonEmpty(lmtReplyAccSubList)) {
            res = lmtReplyAccSubList.stream().filter(e -> {
                return CmisCommonConstants.STD_ZB_YES_NO_1.equals(e.getIsPreLmt());
            }).count();
        }
        return String.valueOf(res > 0 ? 1 : 0);
    }

    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp
     * @方法描述: 通过集团授信申请数据生成审批数据
     * @参数与返回说明: 接受集团授信申请流水号，返回集团授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpMemRelForLmtGrpApprByLmtGrpAppr(User userInfo, LmtGrpAppr oldlmtGrpAppr, LmtGrpAppr newlmtGrpAppr) throws Exception {
        // 查询审批对应的关联关系
        log.info("根据原审批表数据查询成员客户信息,原审批表审批流水号为:" + oldlmtGrpAppr.getGrpApproveSerno());
        List<LmtGrpMemRel> lmtGrpMemRelList = queryLmtGrpMemRelByGrpSerno(oldlmtGrpAppr.getGrpApproveSerno());
        if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
            for (LmtGrpMemRel item : lmtGrpMemRelList) {
                String singleSerno = lmtApprService.generateLmtApprInfoByLmtAppr(item.getSingleSerno());
                generateLmtGrpMemRelForLmtGrpApprForword(userInfo==null?"":userInfo.getUserId(), userInfo==null?"":userInfo.getOrg().getCode(), newlmtGrpAppr.getGrpApproveSerno(), item, singleSerno);
            }
        }
    }

    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp
     * @方法描述: 通过集团授信申请数据生成审批数据
     * @参数与返回说明: 接受集团授信申请流水号，返回集团授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtGrpMemRelForLmtGrpReplyByLmtGrpAppr(String currentUserId, String currentOrgId, String lmtGrpApprSerno, String lmtGrpReplySerno) throws Exception {
        // 查询审批对应的关联关系
        List<LmtGrpMemRel> lmtGrpMemRelList = queryLmtGrpMemRelByGrpSerno(lmtGrpApprSerno);
        if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
            for (LmtGrpMemRel item : lmtGrpMemRelList) {
                String singleSerno = lmtReplyService.generateNewLmtReplyByLmtApprForLmtGrpReply(item.getSingleSerno(), currentUserId, currentOrgId);
                generateLmtGrpMemRelForLmtGrpApprForword(currentUserId, currentOrgId, lmtGrpReplySerno, item, singleSerno);
            }
        }
    }


    /**
     * @方法名称: generateLmtGrpMemRelForLmtGrpAppByLmtGrpReplyAcc
     * @方法描述: 通过集团授信批复台账数据生成申请数据的关联关系和单一的申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtGrpMemRelForLmtGrpAppByLmtGrpReplyAcc(User userInfo, LmtGrpApp newLmtGrpApp, LmtGrpReplyAcc lmtGrpReplyAcc) throws Exception {
        List<String> cusList = new ArrayList<>();// 集团客户成员数据
        List<String> memList = new ArrayList<>();// 集团成员授信关系数据
        // 集团台账对应的数据关联关系   -- 取集团台账与成员台账之间的关系
        log.info("根据集团授信台账查询关系表数据,授信台账编号:" + lmtGrpReplyAcc.getGrpAccNo());
        List<LmtGrpMemRel> lmtGrpMemRelList = queryLmtGrpMemRelByGrpSerno(lmtGrpReplyAcc.getGrpAccNo());
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            memList.add(lmtGrpMemRel.getCusId());

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
            creditReportQryLstAndRealDto.setCusId(lmtGrpMemRel.getCusId());
            creditReportQryLstAndRealDto.setCusName(lmtGrpMemRel.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            creditReportQryLstAndRealDto.setManagerId(lmtGrpMemRel.getManagerId());
            creditReportQryLstAndRealDto.setManagerBrId(lmtGrpMemRel.getManagerBrId());
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(newLmtGrpApp.getGrpSerno());
            creditReportQryLstAndRealDto.setScene("01");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            log.info("生成征信关联返回数据：" + JSON.toJSONString(doCreateCreditAuto));
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        }
        CusGrpMemberRelDto cusGrpMemberRelDto = new CusGrpMemberRelDto();
        cusGrpMemberRelDto.setGrpNo(newLmtGrpApp.getGrpCusId());
        log.info("查询集团项下成员关系,集团编号:" + newLmtGrpApp.getGrpCusId());
        List<CusGrpMemberRelDto> cusGrpMemberRelDtoList =JSONArray.parseArray(JSON.toJSONString(iCusClientService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto).getData()), CusGrpMemberRelDto.class);
        if (cusGrpMemberRelDtoList != null && cusGrpMemberRelDtoList.size() > 0) {
            for (Object o : cusGrpMemberRelDtoList) {
                CusGrpMemberRelDto item = ObjectMapperUtils.instance().convertValue(o, CusGrpMemberRelDto.class);
                cusList.add(item.getCusId());
                log.info("查询当前集团授信项下成员关系,成员:" + memList.toString());
                // 2.将最新的集团成员与授信成员对比,如果最新的集团成员不在授信成员中,则将最新的集团成员信息插入到授信成员中
                if (memList.toString().contains(item.getCusId())) {
                    log.info("当前最新的集团成员表中成员数据存在于授信成员表中,当前成员:" + item.getCusId());
                    continue;
                } else {
                    log.info("先查询单一授信批复台账表数据!");
                    // LmtApp lmtApp = lmtAppService.selectByCusIdInApping(item.getCusId(), CmisCommonConstants.YES_NO_0);
                    LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(item.getCusId());
                    if (Objects.nonNull(lmtReplyAcc) && StringUtils.nonBlank(lmtReplyAcc.getReplySerno())) {
                        log.info("存在单一授信申报台账,更具当前申报数据获生成一笔新的成员授信相关信息：" + JSON.toJSONString(lmtReplyAcc));
                        // 根据当前数据生成一笔新的成员授信相关信息
                        generateLmtGrpMemRelUnderInsertCusIntoGrp(item, newLmtGrpApp, userInfo);
                    } else {
                        log.info("不存在单一授信,新增授信申请关系表数据,当前成员:" + item.getCusId());
                        LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
                        lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
                        lmtGrpMemRel.setGrpSerno(newLmtGrpApp.getGrpSerno());
                        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
                        lmtGrpMemRel.setSingleSerno(serno);
                        lmtGrpMemRel.setGrpCusId(newLmtGrpApp.getGrpCusId());
                        lmtGrpMemRel.setGrpCusName(newLmtGrpApp.getGrpCusName());
                        lmtGrpMemRel.setCusId(item.getCusId());
                        lmtGrpMemRel.setCusName(item.getCusName());
                        lmtGrpMemRel.setCusType(item.getCusType());
                        lmtGrpMemRel.setManagerId(item.getManagerId());
                        lmtGrpMemRel.setManagerBrId(item.getManagerBrId());
                        lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtGrpMemRel.setInputId(item.getManagerId());
                        lmtGrpMemRel.setInputBrId(item.getManagerBrId());
                        lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtGrpMemRel.setUpdId(item.getManagerId());
                        lmtGrpMemRel.setUpdBrId(item.getManagerBrId());
                        lmtGrpMemRel.setIsPrtcptCurtDeclare(CmisCommonConstants.STD_ZB_YES_NO_1);
                        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        insert(lmtGrpMemRel);

                        LmtApp lmtApp1 = new LmtApp();
                        lmtApp1.setPkId(UUID.randomUUID().toString());
                        lmtApp1.setSerno(serno);
                        lmtApp1.setOgrigiLmtSerno(serno);
                        lmtApp1.setLmtType(CmisCommonConstants.LMT_TYPE_01);
                        lmtApp1.setCusId(item.getCusId());
                        lmtApp1.setCusName(item.getCusName());
                        lmtApp1.setCusType(item.getCusType());
                        lmtApp1.setIsGrp(CmisCommonConstants.YES_NO_1);
                        lmtApp1.setCurType(CmisCommonConstants.CUR_TYPE_CNY);
                        lmtApp1.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        lmtApp1.setManagerId(item.getManagerId());
                        lmtApp1.setManagerBrId(item.getManagerBrId());
                        lmtApp1.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtApp1.setInputId(item.getManagerId());
                        lmtApp1.setInputBrId(item.getManagerBrId());
                        lmtApp1.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtApp1.setUpdId(item.getManagerId());
                        lmtApp1.setUpdBrId(item.getManagerBrId());
                        lmtApp1.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtApp1.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        lmtApp1.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        log.info("不存在单一授信,新增一笔成员授信!");
                        lmtAppService.insert(lmtApp1);
                    }

                }
            }
            log.info("查询最新的集团项下成员关系,成员:" + JSON.toJSONString(cusList));
            // 1.将授信成员与最新的集团成员对比,如果授信成员不在最新的集团成员中,则将当前台账关系表中的成员做逻辑删除操作
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) { // 台账关系
                log.info("判断当前客户是否还存在与关系表中,成员:" + lmtGrpMemRel.getCusId());
                if (cusList.toString().contains(lmtGrpMemRel.getCusId())) {
                    // 存在时 ,进行常规操作
                    log.info("当前客户是否存在于关系表中,成员:" + lmtGrpMemRel.getCusId());
                    Map map = lmtAppService.generateLmtAppByLmtGrpAcc(lmtGrpMemRel.getSingleSerno(), newLmtGrpApp.getLmtType());
                    if (map.get("rtnCode").equals(EcbEnum.ECB010000.key)) {
                        generateLmtGrpMemRelForLmtGrpApprByBack(newLmtGrpApp.getManagerId(), newLmtGrpApp.getManagerBrId(), newLmtGrpApp, lmtGrpMemRel, map.get("serno").toString());
                    } else {
                        return map.get("rtnMsg").toString();
                    }
                } else {
                    // 不存在时 ,进解除申请关系表数据,成员授信申请重置为单一授信申请
                    log.info("当前客户是否不存在于关系表中,成员:" + lmtGrpMemRel.getCusId());
                    LmtApp lmtApp = lmtAppService.selectByCusIdInApping(lmtGrpMemRel.getCusId(), CmisCommonConstants.YES_NO_1);
                    if (lmtApp != null && !StringUtils.isBlank(lmtApp.getSerno())) {
                        log.info("当前客户不存在于关系表中,成员:" + lmtGrpMemRel.getCusId());
                        lmtApp.setIsGrp(CmisCommonConstants.YES_NO_0);
                        lmtAppService.updateSelective(lmtApp);
                        LmtGrpMemRel lmtGrpMemRel1 = this.queryLmtGrpMemRelBySingleSerno(lmtApp.getSerno());
                        lmtGrpMemRel1.setOprType(CmisCommonConstants.OP_TYPE_02);
                        lmtGrpMemRel1.setUpdId(newLmtGrpApp.getManagerId());
                        lmtGrpMemRel1.setUpdBrId(newLmtGrpApp.getManagerBrId());
                        lmtGrpMemRel1.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        updateSelective(lmtGrpMemRel1);
                    } else {
                        throw BizException.error(null, EcbEnum.ECB010092.key, EcbEnum.ECB010092.value);
                    }
                }
            }

        }

//        log.info("根据集团授信台账重新查询关系表数据,授信台账编号:"+lmtGrpReplyAcc.getGrpAccNo());
//        List<LmtGrpMemRel> lmtGrpMemRels = queryLmtGrpMemRelByGrpSerno(lmtGrpReplyAcc.getGrpAccNo());
//        if (lmtGrpMemRels != null && lmtGrpMemRels.size() > 0) {
//            for (LmtGrpMemRel item : lmtGrpMemRels) {
//                Map map = lmtAppService.generateLmtAppByLmtGrpAcc(item.getSingleSerno(), newLmtGrpApp.getLmtType());
//                if (map.get("rtnCode").equals(EcbEnum.ECB010000.key)) {
//                    generateLmtGrpMemRelForLmtGrpApprByBack(userInfo.getLoginCode(), userInfo.getOrg().getCode(), newLmtGrpApp, item, map.get("serno").toString());
//                } else {
//                    return map.get("rtnMsg").toString();
//                }
//            }
//        } else {
//            return EcbEnum.ECB010017.value;
//        }
        return EcbEnum.ECB010000.key;
    }

    /**
     * @函数名称:generateLmtGrpMemRelUnderInsertCusIntoGrp
     * @函数描述:根据当前数据生成一笔新的成员授信相关信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-19 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    private void generateLmtGrpMemRelUnderInsertCusIntoGrp(CusGrpMemberRelDto item, LmtGrpApp newLmtGrpApp, User userInfo) {
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(item.getCusId());
        LmtApp lmtApp = new LmtApp();
        if (Objects.nonNull(lmtReplyAcc)) {
            String replySerno = lmtReplyAcc.getReplySerno();
            LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
            log.info(String.format("校验批复是否存在!"));
            if (Objects.isNull(lmtReply)) {
                throw BizException.error(null, EcbEnum.ECB010071.key, EcbEnum.ECB010071.value);
            }
            String origiSerno = lmtReply.getSerno();
            LmtApp origiLmtApp = lmtAppService.selectBySerno(origiSerno);
            if (Objects.isNull(origiLmtApp)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(serno)) {
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            log.info(String.format("保存新增成员客户单一授信信息,生成流水号%s", serno));
            String pkId = UUID.randomUUID().toString();
            BeanUtils.copyProperties(lmtReply, lmtApp);
            // 申请流水号
            lmtApp.setSerno(serno);
            lmtApp.setOgrigiLmtSerno(lmtReply.getSerno());
            // 数据操作标志为新增
            lmtApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            lmtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            lmtApp.setPkId(pkId);
            lmtApp.setLmtType(newLmtGrpApp.getLmtType());
            lmtApp.setIsGrp(CmisCommonConstants.YES_NO_1);
            lmtApp.setEvalHighCurfundLmtAmt(origiLmtApp.getEvalHighCurfundLmtAmt());
            lmtApp.setOrigiLmtTerm(lmtReply.getLmtTerm());
            lmtApp.setOrigiLmtGraperTerm(lmtReply.getLmtGraperTerm());
            lmtApp.setOrigiLmtReplySerno(replySerno);
            lmtApp.setOrigiOpenTotalLmtAmt(lmtReply.getOpenTotalLmtAmt());
            lmtApp.setOrigiLowRiskTotalLmtAmt(lmtReply.getLowRiskTotalLmtAmt());

            lmtApp.setInputId(userInfo == null ? lmtReplyAcc.getManagerId() : userInfo.getLoginCode());
            lmtApp.setInputBrId(userInfo == null ? lmtReplyAcc.getManagerBrId() : userInfo.getOrg().getCode());
            lmtApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtApp.setUpdId(userInfo == null ? lmtReplyAcc.getManagerId() : userInfo.getLoginCode());
            lmtApp.setUpdBrId(userInfo == null ? lmtReplyAcc.getManagerBrId() : userInfo.getOrg().getCode());
            lmtApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtApp.setManagerId(userInfo == null ? lmtReplyAcc.getManagerId() : userInfo.getLoginCode());
            lmtApp.setManagerBrId(userInfo == null ? lmtReplyAcc.getManagerBrId() : userInfo.getOrg().getCode());
            lmtApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info("校验新纳入成员自有授信期限是否与集团一致,不一致则自动与集团期限保持一致-------start--------");
            if(lmtApp.getLmtTerm().compareTo(newLmtGrpApp.getLmtTerm()) > 0){
                log.info("校验新纳入成员自有授信期限大于集团期限,自动重置成员授信和集团保持一致!");
                lmtApp.setLmtTerm(newLmtGrpApp.getLmtTerm());
            }
            if(CmisCommonConstants.LMT_TYPE_01.equals(newLmtGrpApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_03.equals(newLmtGrpApp.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(newLmtGrpApp.getLmtType())){
                log.info("新增,续作,再议情况下,校验新纳入成员自有授信期限小于集团期限,自动与集团期限保持一致");
                lmtApp.setLmtTerm(newLmtGrpApp.getLmtTerm());
            }
            log.info("校验新纳入成员自有授信期限是否与集团一致,不一致则自动与集团期限保持一致--------end-------");
            lmtApp.setCusName(item.getCusName());
            lmtAppService.insert(lmtApp);
            log.info("保存新增成员客户授信成员关系表数据!");
            LmtGrpMemRel lmtGrpMemRel = new LmtGrpMemRel();
            lmtGrpMemRel.setPkId(UUID.randomUUID().toString());
            lmtGrpMemRel.setGrpSerno(newLmtGrpApp.getGrpSerno());
            lmtGrpMemRel.setSingleSerno(serno);
            lmtGrpMemRel.setGrpCusId(newLmtGrpApp.getGrpCusId());
            lmtGrpMemRel.setGrpCusName(newLmtGrpApp.getGrpCusName());
            lmtGrpMemRel.setCusId(item.getCusId());
            lmtGrpMemRel.setCusName(item.getCusName());
            lmtGrpMemRel.setCusType(item.getCusType());
            // 判断是否存在预授信细化的分项
            List<LmtReplyAccSub> lmtReplyAccSubs = lmtReplyAccSubService.selectByAccNo(lmtReplyAcc.getAccNo());
            for(LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubs){
                if(lmtReplyAccSub.getIsPreLmt().equals(CmisCommonConstants.YES_NO_1)){
                    lmtGrpMemRel.setIsCurtRefine(CmisCommonConstants.YES_NO_1);
                    break;
                }
            }
            // 新增原额度信息
            log.info("保存新增的集团关系表中的原额度信息,原敞口额度[{}],原低风险额度[{}]", lmtReply.getOpenTotalLmtAmt(), lmtReply.getLowRiskTotalLmtAmt());
            lmtGrpMemRel.setOrigiOpenLmtAmt(lmtReply.getOpenTotalLmtAmt());
            lmtGrpMemRel.setOrigiLowRiskLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
            lmtGrpMemRel.setOpenLmtAmt(lmtReply.getOpenTotalLmtAmt());
            lmtGrpMemRel.setOpenLmtAmt(lmtReply.getLowRiskTotalLmtAmt());
            lmtGrpMemRel.setManagerId(userInfo == null ? item.getManagerId() : userInfo.getLoginCode());
            lmtGrpMemRel.setManagerBrId(userInfo == null ? item.getManagerBrId() : userInfo.getOrg().getCode());
            lmtGrpMemRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtGrpMemRel.setInputId(userInfo == null ? item.getManagerId() : userInfo.getLoginCode());
            lmtGrpMemRel.setInputBrId(userInfo == null ? item.getManagerBrId() : userInfo.getOrg().getCode());
            lmtGrpMemRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtGrpMemRel.setUpdId(userInfo == null ? item.getManagerId() : userInfo.getLoginCode());
            lmtGrpMemRel.setUpdBrId(userInfo == null ? item.getManagerBrId() : userInfo.getOrg().getCode());
            lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtGrpMemRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtGrpMemRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtGrpMemRel.setCusName(item.getCusName());
            insert(lmtGrpMemRel);
            log.info("新纳入的成员需要重新计算集团的敞口和低风险合计----start----计算前敞口[{}],低风险[{}]",newLmtGrpApp.getOpenTotalLmtAmt(),newLmtGrpApp.getLowRiskTotalLmtAmt());
            newLmtGrpApp.setOpenTotalLmtAmt(Optional.ofNullable(newLmtGrpApp.getOpenTotalLmtAmt()).orElse(BigDecimal.ZERO).add(Optional.ofNullable(lmtGrpMemRel.getOpenLmtAmt()).orElse(BigDecimal.ZERO)));
            newLmtGrpApp.setLowRiskTotalLmtAmt(Optional.ofNullable(newLmtGrpApp.getLowRiskTotalLmtAmt()).orElse(BigDecimal.ZERO).add(Optional.ofNullable(lmtGrpMemRel.getLowRiskLmtAmt()).orElse(BigDecimal.ZERO)));
            lmtGrpAppService.update(newLmtGrpApp);
            log.info("新纳入的成员需要重新计算集团的敞口和低风险合计----end----计算后敞口[{}],低风险[{}]",newLmtGrpApp.getOpenTotalLmtAmt(),newLmtGrpApp.getLowRiskTotalLmtAmt());
            log.info(String.format("查询员流水对应数据%s", origiSerno));
            LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectBySerno(origiSerno);
            if (Objects.nonNull(lmtHighCurfundEval)) {
                lmtHighCurfundEval.setPkId(UUID.randomUUID().toString());
                lmtHighCurfundEval.setSerno(serno);
                // 登录信息
                lmtHighCurfundEval.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                lmtHighCurfundEval.setInputId(userInfo == null ? item.getManagerId() : userInfo.getLoginCode());
                lmtHighCurfundEval.setInputBrId(userInfo == null ? item.getManagerBrId() : userInfo.getOrg().getCode());
                lmtHighCurfundEval.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtHighCurfundEval.setUpdId(userInfo == null ? item.getManagerId() : userInfo.getLoginCode());
                lmtHighCurfundEval.setUpdBrId(userInfo == null ? item.getManagerBrId() : userInfo.getOrg().getCode());
                lmtHighCurfundEval.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtHighCurfundEval.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtHighCurfundEval.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtHighCurfundEvalService.insertSelective(lmtHighCurfundEval);
            }
            // 单一法人分项信息
            boolean result = lmtReplySubService.copyToLmtAppSub(replySerno, serno);
            if (!result) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成预授信细化申请失败！");
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010072.key, EcbEnum.ECB010072.value);
        }
    }


    /**
     * @函数名称:initLmtGrpMemRelStatusByGrpSerno
     * @函数描述:根据集团流水号初始化集团授信关联成员列表的状态
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer initLmtGrpMemRelStatusByGrpSerno(String grpSerno) {
        Integer result = 0;
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByGrpSerno(grpSerno);
                if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
                    for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                        lmtGrpMemRel.setIsPrtcptCurtDeclare(CmisCommonConstants.YES_NO_1);
                        lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_000);
                        lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                        lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        this.update(lmtGrpMemRel);
                        result++;
                    }
                }
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团流水号初始化集团授信关联成员列表的状态出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return result;
    }

    /**
     * @函数名称:updateIsPrtcptCurtDeclareByPkId
     * @函数描述:根据主键更新是否参与申报的状态
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer updateIsPrtcptCurtDeclareByPkId(String pkId, String isPrtcptCurtDeclare) {
        Integer result = 0;
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                LmtGrpMemRel lmtGrpMemRel = this.selectByPrimaryKey(pkId);
                lmtGrpMemRel.setIsPrtcptCurtDeclare(isPrtcptCurtDeclare);
                lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                result = this.update(lmtGrpMemRel);
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据主键更新是否参与申报的状态出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return result;
    }

    /**
     * @函数名称:finishLmtGrpMemRelStatusByGrpSernoAndMgr
     * @函数描述:根据集团流水号和成员管护人员号更新提交标志为已提交
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer finishLmtGrpMemRelStatusByGrpSernoAndMgr(String grpSerno) {
        Integer result = 0;
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            } else {
                List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByGrpSernoAndMgr(grpSerno);
                if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
                    for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                        lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_997);
                        lmtGrpMemRel.setUpdId(userInfo.getLoginCode());
                        lmtGrpMemRel.setUpdBrId(userInfo.getOrg().getCode());
                        lmtGrpMemRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        this.update(lmtGrpMemRel);
                        result++;
                    }
                }
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团流水号和成员管护人员号更新提交标志为已提交出现异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return result;
    }

    /**
     * @方法名称: judgeIsAllManagerFinish
     * @方法描述: 根据集团授信申请流水号判断是否所有客户经理已完成填报
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> judgeIsAllManagerFinish(String grpSerno) {
        ResultDto rtnData = new ResultDto<String>();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        boolean isExist = true;
        try {
            if (StringUtils.isBlank(grpSerno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            log.info(String.format("根据集团授信申请流水号%s,判断是否所有客户经理已完成填报", grpSerno));

            HashMap paramMap = new HashMap<String, Object>();
            paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramMap.put("grpSerno", grpSerno);
            // 991 111 这种状态不会存在，但不会影响执行结果
            paramMap.put("noFinishStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByParams(paramMap);
            if (lmtGrpMemRelList.size() > 1) {
                isExist = true;
                rtnCode = EcbEnum.ECB010002.key;
                rtnMsg = EcbEnum.ECB010002.value;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("根据集团授信申请流水号判断是否所有客户经理已完成填报异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.setCode(rtnCode);
            rtnData.setMessage(rtnMsg);
            rtnData.setData(String.valueOf(isExist));
        }
        return rtnData;
    }

    /**
     * @方法名称: returnBackLmtGrpAppSingle
     * @方法描述: 退回客户经理集团授信申请填报
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer returnBackLmtGrpAppSingle(String pkId) {
        Integer count = 0;
        try {
            if (StringUtils.isBlank(pkId)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团授信申请主键%s,退回客户经理集团授信申请填报", pkId));
            LmtGrpMemRel lmtGrpMemRel = this.selectByPrimaryKey(pkId);
            if (lmtGrpMemRel != null) {
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                } else {
                    lmtGrpMemRel.setManagerIdSubmitStatus(CmisCommonConstants.WF_STATUS_992);
                    lmtGrpMemRel.setIsPrtcptCurtDeclare(CmisCommonConstants.YES_NO_1);
                    count = this.update(lmtGrpMemRel);
                    // 将单一客户授信申请的数据置为992
                    lmtAppService.backApproveStatus(lmtGrpMemRel.getSingleSerno());
                }
            } else {
                throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团授信申请主键退回客户经理集团授信申请填报异常！", e);
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "," + e.getMessage());
        }
        return count;
    }

    /**
     * @方法名称: judgeIsAfterInitLmtGrpMemRelStatus
     * @方法描述: 根据集团授信申请流水号判断是否已完成初始化成员客户经理填报状态
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String judgeIsAfterInitLmtGrpMemRelStatus(String grpSerno) {
        boolean isExist = false;
        try {
            if (StringUtils.isBlank(grpSerno)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团授信申请流水号%s,判断是否已完成初始化成员客户经理填报状态", grpSerno));

            HashMap paramMap = new HashMap<String, Object>();
            paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramMap.put("grpSerno", grpSerno);
            // 991 111 这种状态不会存在，但不会影响执行结果
            paramMap.put("finishInit", CmisCommonConstants.YES_NO_1);
            List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByParams(paramMap);
            if (lmtGrpMemRelList.size() == 0) {
                isExist = true;
            }
        } catch (YuspException e) {
            throw BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团授信申请流水号判断是否已完成初始化成员客户经理填报异常！", e);
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value + "," + e.getMessage());
        }
        return String.valueOf(isExist);
    }

    /**
     * @方法名称: updateAmtByLmtApp
     * @方法描述: 根据授信申请更新关系表中的金额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateAmtByLmtApp(LmtApp lmtApp) {
        LmtGrpMemRel lmtGrpMemRel = this.queryLmtGrpMemRelBySingleSerno(lmtApp.getSerno());
        lmtGrpMemRel.setLowRiskLmtAmt(lmtApp.getLowRiskTotalLmtAmt());
        lmtGrpMemRel.setOpenLmtAmt(lmtApp.getOpenTotalLmtAmt());
        lmtGrpMemRelMapper.updateLmtGrpMemRelLmtAmt(lmtGrpMemRel);
        log.info(String.format("根据授信申请%s数据更新关系表中的金额", JSON.toJSONString(lmtApp)));
        lmtGrpAppService.updateLmtAmtByGrpSerno(lmtGrpMemRel.getGrpSerno());
    }

    /**
     * 查询上期授信详情
     *
     * @param model
     * @return
     */
    public List<LmtReplySubPrd> selectLastLmt(QueryModel model) {
        List<LmtGrpReply> lmtGrpReplies = lmtGrpReplyService.selectByModel(model);
        List<LmtReplySubPrd> result = new ArrayList<>();
        if (lmtGrpReplies != null && lmtGrpReplies.size() > 0) {
            LmtGrpReply lmtGrpReply = lmtGrpReplies.get(0);
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelMapper.selectByGrpSerno(lmtGrpReply.getGrpSerno());
            String type = model.getCondition().get("type").toString();
            if (lmtGrpMemRelList != null && lmtGrpMemRelList.size() > 0) {
                for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                    String serno = lmtGrpMemRel.getSingleSerno();
                    String cusName = lmtGrpMemRel.getCusName();
                    List<LmtReplySub> lmtReplySubList = lmtReplySubService.selectBySerno(serno);
                    if (lmtReplySubList != null && lmtReplySubList.size() > 0) {
                        for (LmtReplySub lmtReplySub : lmtReplySubList) {
                            String replySubSerno = lmtReplySub.getReplySubSerno();
                            List<LmtReplySubPrd> lmtReplySubPrdList = new ArrayList<>();
                            if ("normal".equals(type)) {
                                lmtReplySubPrdList = lmtReplySubPrdService.selectByReplySubSerno(replySubSerno);
                            } else if ("low".equals(type)) {
                                lmtReplySubPrdList = lmtReplySubPrdService.selectLastByReplySubSerno(replySubSerno);
                            }
                            if (lmtReplySubPrdList != null && lmtReplySubPrdList.size() > 0) {
                                for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                                    lmtReplySubPrd.setCusName(cusName);
                                    result.add(lmtReplySubPrd);
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * 查询本期授信详情
     *
     * @param model
     * @return
     */
    public List<Map> selectThisLmt(QueryModel model) {
        if (model.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        List<Map> list = new ArrayList<>();
        String grpSerno = model.getCondition().get("grpSerno").toString();
        String type = model.getCondition().get("type").toString();
        List<LmtApp> lmtAppByGrpSernoIsDeclare = lmtAppService.getLmtAppByGrpSernoIsDeclare(grpSerno);
        if (CollectionUtils.nonEmpty(lmtAppByGrpSernoIsDeclare)) {
            for (LmtApp lmtApp : lmtAppByGrpSernoIsDeclare) {
                String serno = lmtApp.getSerno();
                Map tempMap = null;
                List<LmtAppSub> lmtAppSubList = null;
                if ("normal".equals(type)) {
                    lmtAppSubList = lmtAppSubService.getNormal(serno);
                } else if ("low".equals(type)) {
                    lmtAppSubList = lmtAppSubService.getLow(serno);
                }
                if (lmtAppSubList != null && lmtAppSubList.size() > 0) {
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        String subSerno = lmtAppSub.getSubSerno();
                        List<LmtAppSubPrd> lmtAppSubPrdList = new ArrayList<>();
                        QueryModel queryModel = new QueryModel();
                        queryModel.getCondition().put("subSerno", subSerno);
                        queryModel.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
                        lmtAppSubPrdList = lmtAppSubPrdMapper.selectByModel(queryModel);
                        tempMap = new HashMap();
                        tempMap.put("pkId", lmtAppSub.getPkId());
                        tempMap.put("serno", serno);
                        tempMap.put("cusName", lmtApp.getCusName());
                        tempMap.put("subPrdSerno", lmtAppSub.getSubSerno());
                        tempMap.put("origiLmtAccSubPrdNo", lmtAppSub.getOrigiLmtAccSubNo());
                        tempMap.put("lmtBizTypeName", lmtAppSub.getSubName());
                        tempMap.put("isPreLmt", lmtAppSub.getIsPreLmt());
                        tempMap.put("guarMode", lmtAppSub.getGuarMode());
                        tempMap.put("origiLmtAccSubPrdAmt", lmtAppSub.getOrigiLmtAccSubAmt());
                        tempMap.put("origiLmtAccSubPrdTerm", lmtAppSub.getOrigiLmtAccSubTerm());
                        tempMap.put("lmtAmt", lmtAppSub.getLmtAmt());
                        tempMap.put("children", lmtAppSubPrdList);
                        list.add(tempMap);
                    }
                }
            }
            return list;
        }
        return null;
    }

    /**
     * 根据集团申请流水号获取单一客户申请流水号
     *
     * @param grpSerno
     * @return
     */
    public List<String> querySernoByGrpSerno(String grpSerno) {
        return lmtGrpMemRelMapper.querySernoByGrpSerno(grpSerno);
    }

    /**
     * @函数名称: selectByGrpReplySerno
     * @函数描述: 根据集团授信申请流水号查询去变更表里面查询批复编号, 再根据批复编号查询授信申报流水号, 根据流水号查询成员批复情况
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-07-28 20:07
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public List<LmtGrpMemRel> selectByGrpReplySerno(String grpSerno) {
        List<LmtGrpMemRel> lmtGrpMemRels = new ArrayList<>();
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("grpSerno", grpSerno);
            log.info("查询授信批复变更申请:" + grpSerno);
            LmtGrpReplyChg lmtGrpReplyChg = lmtGrpReplyChgService.queryLmtGrpReplyChgDataByGrpSerno(map);
            if (lmtGrpReplyChg == null) {
                throw BizException.error(null, EcbEnum.ECB010073.key, EcbEnum.ECB010073.value);
            }
            log.info("查询成员客户批复信息:" + lmtGrpReplyChg.getGrpReplySerno());
            lmtGrpMemRels = this.queryLmtGrpMemRelByGrpSerno(lmtGrpReplyChg.getGrpReplySerno());
        } catch (Exception e) {
            throw BizException.error(null, EcbEnum.ECB010086.key, EcbEnum.ECB010086.value);
        }
        return lmtGrpMemRels;
    }

    /**
     * @函数名称: selectLmtGrpAppIsInFlow
     * @函数描述: 校验当前授信申报是否已进入流程
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-10 20:07
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public int selectLmtGrpAppIsInFlow(String serno) {
        return lmtGrpMemRelMapper.selectLmtGrpAppIsInFlow(serno);
    }

    /**
     * @函数名称: queryLmtGrpMemRelByGrpSerno
     * @函数描述: 查询当前集团名下的客户
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-08-18 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public String queryCusByGrpSerno(String grpSerno) {
        return lmtGrpMemRelMapper.queryCusByGrpSerno(grpSerno);
    }

    /**
     * @函数名称: querySingleSernoByGrpSerno
     * @函数描述: 查询当前集团名下的客户授信申报流水号
     * @创建人: css
     * @创建时间: 2021-10-4 14:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public String querySingleSernoByGrpSerno(String grpSerno) {
        return lmtGrpMemRelMapper.querySingleSernoByGrpSerno(grpSerno);
    }

    /**
     * @函数名称: selectGrpLmtGuarRingDataByGrpSerno
     * @函数描述: 根据集团流水号查询集团授信关联成员列表
     */

    public List<Map> selectGrpLmtGuarRingDataByGrpSerno(String grpSerno) {
        List<Map> list = lmtGrpMemRelMapper.selectGrpLmtGuarRingDataByGrpSerno(grpSerno);
        List<Map> returnList = new ArrayList<>();
        for (Map map : list) {
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus((String) map.get("cusId"));
            if (cusBaseClientDto != null) {
                map.put("certCode", cusBaseClientDto.getCertCode());
            }
            returnList.add(map);
        }
        return returnList;
    }

    /**
     * @函数名称: selectSingleApprLoanCondDataByGrpApproveSerno
     * @函数描述: 根据集团流水号查询集团授信项下成员列表
     */

    public List<Map> selectSingleApprLoanCondDataByGrpApproveSerno(String grpApproveSerno) {
        List<Map> returnList = new ArrayList<>();
        HashMap paramMap = new HashMap<String, Object>();
        paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        paramMap.put("grpSerno", grpApproveSerno);
        List<LmtGrpMemRel> lmtGrpMemRelList = this.queryLmtGrpMemRelByParams(paramMap);
        log.info("最新的一条集团审批项下的成员关系表数据："+JSON.toJSONString(lmtGrpMemRelList));
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList){
            HashMap map = new HashMap();
            map.put("approveSerno",lmtGrpMemRel.getSingleSerno());
            map.put("cusId",lmtGrpMemRel.getCusId());
            map.put("cusName",lmtGrpMemRel.getCusName());
            map.put("openLmtAmt",lmtGrpMemRel.getOpenLmtAmt());
            map.put("lowRiskLmtAmt",lmtGrpMemRel.getLowRiskLmtAmt());
            returnList.add(map);
        }
        return returnList;
    }
}