/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAccList
 * @类描述: doc_acc_list数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 17:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "档案台账导出模板", fileType = ExcelCsv.ExportFileType.XLS)
public class DocAccListVo {

    /**
     * 档案流水号
     **/
    @ExcelField(title = "档案流水号", viewLength = 50)
    private String docSerno;

    /**
     * 档案编号
     **/
    @ExcelField(title = "档案编号", viewLength = 30)
    private String docNo;

    /**
     * 归档模式
     **/
    @ExcelField(title = "归档模式", viewLength = 20, dictCode = "STD_ARCHIVE_MODE")
    private String archiveMode;

    /**
     * 档案分类
     **/
    @ExcelField(title = "档案分类", viewLength = 20, dictCode = "STD_DOC_CLASS")
    private String docClass;

    /**
     * 档案类型
     **/
    @ExcelField(title = "档案类型", viewLength = 20, dictCode = "STD_DOC_TYPE")
    private String docType;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /**
     * 关联业务编号
     **/
    @ExcelField(title = "关联业务编号", viewLength = 20)
    private String bizSerno;

    /**
     * 合同编号
     **/
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /**
     * 借据编号
     **/
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /**
     * 产品名称
     **/
    @ExcelField(title = "产品名称", viewLength = 20)
    private String prdName;

    /**
     * 金额
     **/
    @ExcelField(title = "金额", viewLength = 20, format = "#0.00")
    private BigDecimal loanAmt;

    /**
     * 起始时间
     **/
    @ExcelField(title = "起始时间", viewLength = 20)
    private String startDate;

    /**
     * 到期时间
     **/
    @ExcelField(title = "到期时间", viewLength = 20)
    private String endDate;

    /**
     * 责任人
     **/
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /**
     * 责任机构
     **/
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /**
     * 入库操作人
     **/
    @ExcelField(title = "入库操作人", viewLength = 20)
    private String optUsrName;

    /**
     * 入库操作机构
     **/
    @ExcelField(title = "入库操作机构", viewLength = 20)
    private String optOrgName;

    /**
     * 入库操作时间
     **/
    @ExcelField(title = "入库操作时间", viewLength = 20)
    private String storageOptDate;

    /**
     * 档案入现金库时间
     **/
    @ExcelField(title = "档案入现金库时间", viewLength = 20)
    private String storageCashDate;

    /**
     * 档案状态
     **/
    @ExcelField(title = "档案状态", viewLength = 20, dictCode = "STD_DOC_STAUTS")
    private String docStauts;

    public String getDocSerno() {
        return docSerno;
    }

    public void setDocSerno(String docSerno) {
        this.docSerno = docSerno;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getArchiveMode() {
        return archiveMode;
    }

    public void setArchiveMode(String archiveMode) {
        this.archiveMode = archiveMode;
    }

    public String getDocClass() {
        return docClass;
    }

    public void setDocClass(String docClass) {
        this.docClass = docClass;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getOptUsrName() {
        return optUsrName;
    }

    public void setOptUsrName(String optUsrName) {
        this.optUsrName = optUsrName;
    }

    public String getOptOrgName() {
        return optOrgName;
    }

    public void setOptOrgName(String optOrgName) {
        this.optOrgName = optOrgName;
    }

    public String getStorageOptDate() {
        return storageOptDate;
    }

    public void setStorageOptDate(String storageOptDate) {
        this.storageOptDate = storageOptDate;
    }

    public String getStorageCashDate() {
        return storageCashDate;
    }

    public void setStorageCashDate(String storageCashDate) {
        this.storageCashDate = storageCashDate;
    }

    public String getDocStauts() {
        return docStauts;
    }

    public void setDocStauts(String docStauts) {
        this.docStauts = docStauts;
    }
}