package cn.com.yusys.yusp.web.server.xddb0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0016.req.Xddb0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.Xddb0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0016.Xddb0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:押品信息查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0016:押品信息查询")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXddb0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0016Resource.class);

    @Autowired
    private Xddb0016Service xddb0016Service;

    /**
     * 交易码：xddb0016
     * 交易描述：押品信息查询
     *
     * @param xddb0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品信息查询")
    @PostMapping("/xddb0016")
    protected @ResponseBody
    ResultDto<Xddb0016DataRespDto> xddb0016(@Validated @RequestBody Xddb0016DataReqDto xddb0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016DataReqDto));
        Xddb0016DataRespDto xddb0016DataRespDto = new Xddb0016DataRespDto();// 响应Dto:押品信息查询
        ResultDto<Xddb0016DataRespDto> xddb0016DataResultDto = new ResultDto<>();
        // 从xddb0016DataReqDto获取业务值进行业务逻辑处理
        String cusName = xddb0016DataReqDto.getGuarna();//抵押人名称
        String yppage = xddb0016DataReqDto.getYppage();//页码
        String ypsize = xddb0016DataReqDto.getYpsize();//每页条数
        try {
            if (StringUtil.isNotEmpty(cusName) && StringUtil.isNotEmpty(ypsize) && StringUtil.isNotEmpty(yppage)) {//请求字段非空校验
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016DataReqDto));
                xddb0016DataRespDto = xddb0016Service.selcetGuarBaseInfoByCusName(xddb0016DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016DataRespDto));
                // 封装xddb0016DataResultDto中正确的返回码和返回信息
                xddb0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                //请求必输字段存在空值
                xddb0016DataResultDto.setCode(EcbEnum.ECB010001.key);
                xddb0016DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, e.getMessage());
            // 封装xddb0016DataResultDto中异常返回码和返回信息
            xddb0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0016DataRespDto到xddb0016DataResultDto中
        xddb0016DataResultDto.setData(xddb0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016DataResultDto));
        return xddb0016DataResultDto;
    }
}
