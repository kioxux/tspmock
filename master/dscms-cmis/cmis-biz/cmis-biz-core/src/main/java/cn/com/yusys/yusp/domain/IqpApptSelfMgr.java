/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptSelfMgr
 * @类描述: iqp_appt_self_mgr数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:16:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appt_self_mgr")
public class IqpApptSelfMgr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 贷款申请人主键 **/
	@Column(name = "APPT_CODE", unique = false, nullable = false, length = 32)
	private String apptCode;
	
	/** 企业名称 **/
	@Column(name = "COMPANY_NAME", unique = false, nullable = true, length = 80)
	private String companyName;
	
	/** 企业地址 **/
	@Column(name = "COMPANY_ADDR", unique = false, nullable = true, length = 100)
	private String companyAddr;
	
	/** 企业性质 **/
	@Column(name = "COMPANY_TYPE", unique = false, nullable = true, length = 4)
	private String companyType;
	
	/** 企业规模 **/
	@Column(name = "COMPANY_SCALE", unique = false, nullable = true, length = 2)
	private String companyScale;
	
	/** 统一社会信用代码 **/
	@Column(name = "UNIFY_CREDIT_CODE", unique = false, nullable = true, length = 40)
	private String unifyCreditCode;
	
	/** 成立年份 **/
	@Column(name = "FOUND_YEAR", unique = false, nullable = true, length = 4)
	private String foundYear;
	
	/** 企业-省 **/
	@Column(name = "COMPANY_PROVINCE", unique = false, nullable = true, length = 12)
	private String companyProvince;
	
	/** 企业-市 **/
	@Column(name = "COMPANY_CITY", unique = false, nullable = true, length = 12)
	private String companyCity;
	
	/** 企业-区 **/
	@Column(name = "COMPANY_AREA", unique = false, nullable = true, length = 12)
	private String companyArea;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param companyName
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
    /**
     * @return companyName
     */
	public String getCompanyName() {
		return this.companyName;
	}
	
	/**
	 * @param companyAddr
	 */
	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr;
	}
	
    /**
     * @return companyAddr
     */
	public String getCompanyAddr() {
		return this.companyAddr;
	}
	
	/**
	 * @param companyType
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	
    /**
     * @return companyType
     */
	public String getCompanyType() {
		return this.companyType;
	}
	
	/**
	 * @param companyScale
	 */
	public void setCompanyScale(String companyScale) {
		this.companyScale = companyScale;
	}
	
    /**
     * @return companyScale
     */
	public String getCompanyScale() {
		return this.companyScale;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode;
	}
	
    /**
     * @return unifyCreditCode
     */
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param foundYear
	 */
	public void setFoundYear(String foundYear) {
		this.foundYear = foundYear;
	}
	
    /**
     * @return foundYear
     */
	public String getFoundYear() {
		return this.foundYear;
	}
	
	/**
	 * @param companyProvince
	 */
	public void setCompanyProvince(String companyProvince) {
		this.companyProvince = companyProvince;
	}
	
    /**
     * @return companyProvince
     */
	public String getCompanyProvince() {
		return this.companyProvince;
	}
	
	/**
	 * @param companyCity
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}
	
    /**
     * @return companyCity
     */
	public String getCompanyCity() {
		return this.companyCity;
	}
	
	/**
	 * @param companyArea
	 */
	public void setCompanyArea(String companyArea) {
		this.companyArea = companyArea;
	}
	
    /**
     * @return companyArea
     */
	public String getCompanyArea() {
		return this.companyArea;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}