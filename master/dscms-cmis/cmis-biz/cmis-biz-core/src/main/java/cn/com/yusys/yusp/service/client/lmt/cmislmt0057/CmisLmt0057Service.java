package cn.com.yusys.yusp.service.client.lmt.cmislmt0057;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0057.req.CmisLmt0057ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class cmisLmt0057Service
 * @date 2021/8/25 19:10
 * @desc 线上产品自动生成合同台账
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0057Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0057Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0057ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0057.resp.cmisLmt0057RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 线上产品自动生成合同台账
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0057RespDto cmisLmt0057(CmisLmt0057ReqDto cmisLmt0057ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, JSON.toJSONString(cmisLmt0057ReqDto));
        ResultDto<CmisLmt0057RespDto> cmisLmt0057ResultDto = cmisLmtClientService.cmislmt0057(cmisLmt0057ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value, JSON.toJSONString(cmisLmt0057ResultDto));

        String cmisLmt0057Code = Optional.ofNullable(cmisLmt0057ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0057Meesage = Optional.ofNullable(cmisLmt0057ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0057RespDto cmisLmt0057RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0057ResultDto.getCode()) && cmisLmt0057ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0057ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0057RespDto = cmisLmt0057ResultDto.getData();
        } else {
            if (cmisLmt0057ResultDto.getData() != null) {
                cmisLmt0057Code = cmisLmt0057ResultDto.getData().getErrorCode();
                cmisLmt0057Meesage = cmisLmt0057ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0057Code = EpbEnum.EPB099999.key;
                cmisLmt0057Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0057Code, cmisLmt0057Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0057.value);
        return cmisLmt0057RespDto;
    }
}
