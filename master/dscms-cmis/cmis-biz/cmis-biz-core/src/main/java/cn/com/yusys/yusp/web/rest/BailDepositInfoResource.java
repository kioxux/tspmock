/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BailDepositInfo;
import cn.com.yusys.yusp.service.BailDepositInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailDepositInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-09 21:06:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/baildepositinfo")
public class BailDepositInfoResource {
    @Autowired
    private BailDepositInfoService bailDepositInfoService;




    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<BailDepositInfo> show(@PathVariable("serno") String serno) {
        BailDepositInfo bailDepositInfo = bailDepositInfoService.selectByPrimaryKey(serno);
        return ResultDto.success(bailDepositInfo);
    }



    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BailDepositInfo bailDepositInfo) {
        int result = bailDepositInfoService.update(bailDepositInfo);
        return ResultDto.success(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = bailDepositInfoService.deleteByPrimaryKey(serno);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = bailDepositInfoService.deleteByIds(ids);
        return ResultDto.success(result);
    }
}
