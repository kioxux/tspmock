/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocScanUserInfo
 * @类描述: doc_scan_user_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_scan_user_info")
public class DocScanUserInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 扫描信息流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DSUI_SERNO")
	private String dsuiSerno;
	
	/** 档案流水号 **/
	@Column(name = "DOC_SERNO", unique = false, nullable = true, length = 40)
	private String docSerno;
	
	/** 扫描人编号 **/
	@Column(name = "SCAN_USER_ID", unique = false, nullable = true, length = 40)
	private String scanUserId;
	
	/** 扫描人姓名 **/
	@Column(name = "SCAN_USER_NAME", unique = false, nullable = true, length = 80)
	private String scanUserName;
	
	/** 扫描时间 **/
	@Column(name = "SCAN_TIME", unique = false, nullable = true, length = 20)
	private String scanTime;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param dsuiSerno
	 */
	public void setDsuiSerno(String dsuiSerno) {
		this.dsuiSerno = dsuiSerno;
	}
	
    /**
     * @return dsuiSerno
     */
	public String getDsuiSerno() {
		return this.dsuiSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno;
	}
	
    /**
     * @return docSerno
     */
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param scanUserId
	 */
	public void setScanUserId(String scanUserId) {
		this.scanUserId = scanUserId;
	}
	
    /**
     * @return scanUserId
     */
	public String getScanUserId() {
		return this.scanUserId;
	}
	
	/**
	 * @param scanUserName
	 */
	public void setScanUserName(String scanUserName) {
		this.scanUserName = scanUserName;
	}
	
    /**
     * @return scanUserName
     */
	public String getScanUserName() {
		return this.scanUserName;
	}
	
	/**
	 * @param scanTime
	 */
	public void setScanTime(String scanTime) {
		this.scanTime = scanTime;
	}
	
    /**
     * @return scanTime
     */
	public String getScanTime() {
		return this.scanTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}