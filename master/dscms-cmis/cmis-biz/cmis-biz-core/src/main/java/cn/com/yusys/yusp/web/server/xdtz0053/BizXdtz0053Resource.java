package cn.com.yusys.yusp.web.server.xdtz0053;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0053.req.Xdtz0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.Xdtz0053DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0053.Xdtz0053Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:个人社会关系查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0053:个人社会关系查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0053Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0053Resource.class);

    @Autowired
    private Xdtz0053Service xdtz0053Service;
    /**
     * 交易码：xdtz0053
     * 交易描述：个人社会关系查询
     *
     * @param xdtz0053DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人社会关系查询")
    @PostMapping("/xdtz0053")
    protected @ResponseBody
    ResultDto<Xdtz0053DataRespDto> xdtz0053(@Validated @RequestBody Xdtz0053DataReqDto xdtz0053DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataReqDto));
        Xdtz0053DataRespDto xdtz0053DataRespDto = new Xdtz0053DataRespDto();// 响应Dto:个人社会关系查询
        ResultDto<Xdtz0053DataRespDto> xdtz0053DataResultDto = new ResultDto<>();
        String billNo = xdtz0053DataReqDto.getBillNo();//借据号码
        String mainLoanManName = xdtz0053DataReqDto.getMainLoanManName();//主贷人姓名
        String mainLoanManCertNo = xdtz0053DataReqDto.getMainLoanManCertNo();//主贷人证件号码
        try {
            // 从xdtz0053DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdtz0053DataRespDto = xdtz0053Service.xdtz0053(xdtz0053DataReqDto);
            // TODO 调用XXXXXService层结束
            // 封装xdtz0053DataResultDto中正确的返回码和返回信息
            xdtz0053DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0053DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, e.getMessage());
            // 封装xdtz0053DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdtz0053DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0053DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdtz0053DataRespDto到xdtz0053DataResultDto中
        xdtz0053DataResultDto.setData(xdtz0053DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataResultDto));
        return xdtz0053DataResultDto;
    }
}
