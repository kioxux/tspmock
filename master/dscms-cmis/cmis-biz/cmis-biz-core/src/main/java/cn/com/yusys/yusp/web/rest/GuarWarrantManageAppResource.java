/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.GuarWarrantManageAppDto;
import cn.com.yusys.yusp.repository.mapper.GuarContRelWarrantMapper;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantInfoMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantManageAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:24:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarwarrantmanageapp")
public class GuarWarrantManageAppResource {
    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarContRelWarrantMapper guarContRelWarrantMapper;

    @Autowired
    private GuarWarrantInfoMapper guarWarrantInfoMapper;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarWarrantManageApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarWarrantManageApp> list = guarWarrantManageAppService.selectAll(queryModel);
        return new ResultDto<List<GuarWarrantManageApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<GuarWarrantManageApp>> index(@RequestBody QueryModel queryModel) {
        //审批状态
        String approveStatus = (String) queryModel.getCondition().get("approveStatus");
        //权证出入库申请类型
        String warrantAppType = (String) queryModel.getCondition().get("warrantAppType");

        if (CmisBizConstants.STD_WARRANT_APP_TYPE_01.equals(warrantAppType) || "996,997,998".equals(approveStatus)){
            //权证入库历史按照登记日期由近及远排序
            queryModel.setSort("inputDate desc");
        }
        List<GuarWarrantManageApp> list = guarWarrantManageAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarWarrantManageApp> show(@PathVariable("serno") String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        return new ResultDto<GuarWarrantManageApp>(guarWarrantManageApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarWarrantManageApp> create(@RequestBody GuarWarrantManageApp guarWarrantManageApp) throws URISyntaxException {
        guarWarrantManageAppService.insert(guarWarrantManageApp);
        return new ResultDto<GuarWarrantManageApp>(guarWarrantManageApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarWarrantManageApp guarWarrantManageApp) throws URISyntaxException {
        int result = guarWarrantManageAppService.update(guarWarrantManageApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        int result = guarWarrantManageAppService.deleteByPrimaryKey(serno);

        //权证出入库申请类型
        String warrantAppType = guarWarrantManageApp.getWarrantAppType();

        if (CmisBizConstants.STD_WARRANT_APP_TYPE_01.equals(warrantAppType)){
            //如果是权证入库，则删除对应的担保合同与押品及权证关联信息
            guarContRelWarrantMapper.deleteBySerno(serno);
            //删除权证信息
            guarWarrantInfoMapper.deleteByCoreGuarantyNo(guarWarrantManageApp.getCoreGuarantyNo());
        }

        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarWarrantManageAppService.deleteByIds(ids);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:deleteOnlogic
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号逻辑删除")
    @PostMapping("/deleteOnlogic")
    protected ResultDto<Integer> deleteOnlogic(@RequestBody GuarWarrantManageApp guarWarrantManageApp) {
        //int result = guarWarrantManageAppService.deleteOnlogic(serno);
        String serno = guarWarrantManageApp.getSerno();
        GuarWarrantManageApp oriGuarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        //核心担保编号
        String coreGuarantyNo = oriGuarWarrantManageApp.getCoreGuarantyNo();
        //权证出入库申请类型
        String warrantAppType = guarWarrantManageAppService.selectByPrimaryKey(serno).getWarrantAppType();

        int result = guarWarrantManageAppService.updateSelective(guarWarrantManageApp);

        if (CmisBizConstants.STD_WARRANT_APP_TYPE_01.equals(warrantAppType)){
            //如果是权证入库，则逻辑删除对应的担保合同与押品及权证关联信息
            if (CmisCommonConstants.WF_STATUS_996.equals(guarWarrantManageApp.getApproveStatus())){
                guarContRelWarrantMapper.deleteOnLogicBySerno(serno);
                //逻辑删除对应的权证台账信息
                guarWarrantInfoMapper.deleteOnLogicByCoreGuarantyNo(coreGuarantyNo);
            }else{
                //物理删除对应的担保合同与押品及权证关联信息
                guarContRelWarrantMapper.deleteBySerno(serno);
                //物理删除权证信息
                guarWarrantInfoMapper.deleteByCoreGuarantyNo(coreGuarantyNo);
            }
        }
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:queryGuarWarrantManageAppDtoBySerno
     * @函数描述:根据流水号查询权证(出入库、续借)申请对象
     * @param serno
     * @return
     */
    @ApiOperation("根据流水号查询权证(出入库、续借)申请对象")
    @PostMapping("/queryGuarWBySerno")
    protected ResultDto<GuarWarrantManageAppDto> queryGuarWarrantManageAppDtoBySerno(@RequestBody String serno){
        GuarWarrantManageAppDto guarWarrantManageAppDto = guarWarrantManageAppService.queryGuarWarrantManageAppDtoBySerno(serno);
        return new ResultDto<GuarWarrantManageAppDto>(guarWarrantManageAppDto);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:根据权证(出入库、续借)对象暂存(新增/修改)数据
     * @param guarWarrantManageAppDto
     * @return
     */
    @ApiOperation("根据权证(出入库、续借)对象暂存(新增/修改)数据")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody GuarWarrantManageAppDto guarWarrantManageAppDto){
        int result  = guarWarrantManageAppService.tempSave(guarWarrantManageAppDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:warrantIn
     * @函数描述:
     * @param serno
     * @return
     */
    @ApiOperation("权证入库")
    @PostMapping("/warrantIn")
    protected ResultDto<Integer> warrantIn(@RequestBody String serno){
        guarWarrantManageAppService.handleBusinessAfterEnd(serno);
        return new ResultDto<Integer>(0);
    }

    /**
     * @函数名称:warrantOut
     * @函数描述:
     * @param serno
     * @return
     */
    @ApiOperation("权证出库")
    @PostMapping("/warrantOut")
    protected ResultDto<Integer> warrantOut(@RequestBody String serno){
        guarWarrantManageAppService.handleBusinessAfterEnd(serno);
        return new ResultDto<Integer>(0);
    }

    /**
     * @函数名称:warrantRenew
     * @函数描述:
     * @param serno
     * @return
     */
    @ApiOperation("权证续借")
    @PostMapping("/warrantRenew")
    protected ResultDto<Integer> warrantRenew(@RequestBody String serno){
        int result = guarWarrantManageAppService.warrantRenew(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * 检查押品是否有在途的权证入库申请
     * @函数名称:checkGuarNoIsOnTheWay
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/checkGuarNoIsOnTheWay/{guarNo}")
    protected ResultDto checkGuarNoIsOnTheWay(@PathVariable("guarNo") String guarNo){
        String result = guarWarrantManageAppService.checkGuarNoIsOnTheWay(guarNo);
        return new ResultDto<String>(result);
    }

    /**
     * 检查权证是否有在途的权证出库申请
     * @函数名称:checkcoreguarantynoisontheway
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/checkcoreguarantynoisontheway/{coreGuarantyNo}")
    protected ResultDto<String> checkCoreGuarantyNoIsOnTheWay(@PathVariable("coreGuarantyNo") String coreGuarantyNo){
        String result = guarWarrantManageAppService.checkCoreGuarantyNoIsOnTheWay(coreGuarantyNo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:selectByGuarContNo
     * @函数描述:根据担保编号查询权证出库核心担保编号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectcoreguarantynobyguarcontno")
    @ApiOperation("根据担保编号查询权证出库核心担保编号")
    protected ResultDto<String> selectCoreGuarantyNoByGuarContNo(@RequestBody String guarContNo) {
        String coreGuarantyNo = guarWarrantManageAppService.selectCoreGuarantyNoByGuarContNo(guarContNo);
        return new ResultDto<>(coreGuarantyNo);
    }

    /**
     * 权证入库冲正
     * @函数名称:warrantInRighting
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/warrantinrighting/{serno}")
    protected ResultDto<String> warrantInRighting(@PathVariable("serno") String serno){
        String result = guarWarrantManageAppService.warrantInRighting(serno);
        return new ResultDto<>(result);
    }

    /**
     * 权证出库冲正
     * @函数名称:warrantOutRighting
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/warrantoutrighting/{serno}")
    protected ResultDto<String> warrantOutRighting(@PathVariable("serno") String serno){
        String result = guarWarrantManageAppService.warrantOutRighting(serno);
        return new ResultDto<>(result);
    }

    /**
     * 检查押品是否已入库
     * @函数名称:checkWarrantState
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkwarrantstate")
    protected ResultDto<String> checkWarrantState(@RequestBody QueryModel queryModel){
        String result = guarWarrantManageAppService.checkWarrantState(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * 查询存单的账户号码
     * @函数名称:getGuarAcctNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getguaracctno")
    protected ResultDto<String> getGuarAcctNo(@RequestBody QueryModel queryModel){
        String result = guarWarrantManageAppService.getGuarAcctNo(queryModel);
        return new ResultDto<>(result);
    }
}
