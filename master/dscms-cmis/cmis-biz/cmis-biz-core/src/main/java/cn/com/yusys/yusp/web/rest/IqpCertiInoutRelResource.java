/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.GrtGuarContDto;
import cn.com.yusys.yusp.dto.GuarClientRsDto;
import cn.com.yusys.yusp.dto.IqpCertiInoutAppDto;
import cn.com.yusys.yusp.dto.IqpCertiInoutRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCertiInoutRel;
import cn.com.yusys.yusp.service.IqpCertiInoutRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 10:21:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcertiinoutrel")
public class IqpCertiInoutRelResource {
    @Autowired
    private IqpCertiInoutRelService iqpCertiInoutRelService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCertiInoutRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCertiInoutRel> list = iqpCertiInoutRelService.selectAll(queryModel);
        return new ResultDto<List<IqpCertiInoutRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpCertiInoutRel>> index(QueryModel queryModel) {
        List<IqpCertiInoutRel> list = iqpCertiInoutRelService.selectByModel(queryModel);
        return new ResultDto<List<IqpCertiInoutRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpCertiInoutRel> show(@PathVariable("pkId") String pkId) {
        IqpCertiInoutRel iqpCertiInoutRel = iqpCertiInoutRelService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpCertiInoutRel>(iqpCertiInoutRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCertiInoutRel> create(@RequestBody IqpCertiInoutRel iqpCertiInoutRel) throws URISyntaxException {
        iqpCertiInoutRelService.insert(iqpCertiInoutRel);
        return new ResultDto<IqpCertiInoutRel>(iqpCertiInoutRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCertiInoutRel iqpCertiInoutRel) throws URISyntaxException {
        int result = iqpCertiInoutRelService.update(iqpCertiInoutRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpCertiInoutRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCertiInoutRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取权证与权证出入库申请的关系表中的押品统一编号
     * @param map
     * @return
     */
    @PostMapping("/getIqpCertiInoutRelInGuarNo")
    protected List<String> getIqpCertiInoutRelInGuarNo(@RequestBody Map map) {
        return iqpCertiInoutRelService.getIqpCertiInoutRelInGuarNo(map);
    }

    /**
     * 根据获取权证信息
     * @param
     * @return
     */
    @GetMapping ("/querySelectGuarNo/{optType}")
    public ResultDto<String> querySelectGuarNo(@PathVariable("optType") String optType){
        String rtnData = iqpCertiInoutRelService.querySelectGuarNo(optType);
        return new ResultDto<>(rtnData);
    }
    
}
