package cn.com.yusys.yusp.web.server.xdzc0024;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0024.req.Xdzc0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.Xdzc0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0024.Xdzc0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:票据池资料补全查询
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDZC0024:票据池资料补全查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0024Resource.class);

    @Autowired
    private Xdzc0024Service xdzc0024Service;
    /**
     * 交易码：xdzc0024
     * 交易描述：票据池资料补全查询
     *
     * @param xdzc0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("票据池资料补全查询")
    @PostMapping("/xdzc0024")
    protected @ResponseBody
    ResultDto<Xdzc0024DataRespDto> xdzc0024(@Validated @RequestBody Xdzc0024DataReqDto xdzc0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024DataReqDto));
        Xdzc0024DataRespDto xdzc0024DataRespDto = new Xdzc0024DataRespDto();
        ResultDto<Xdzc0024DataRespDto> xdzc0024DataResultDto = new ResultDto<>();
        try {
            xdzc0024DataRespDto = xdzc0024Service.xdzc0024Service(xdzc0024DataReqDto);
            xdzc0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, e.getMessage());
            xdzc0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0024DataRespDto到xdzc0024DataResultDto中
        xdzc0024DataResultDto.setData(xdzc0024DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024DataResultDto));
        return xdzc0024DataResultDto;
    }
}
