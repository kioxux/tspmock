package cn.com.yusys.yusp.web.server.xdca0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0006.req.Xdca0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0006.resp.Xdca0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdca0005.Xdca0005Service;
import cn.com.yusys.yusp.service.server.xdca0006.Xdca0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:信用卡审批信息核准接口
 *
 * @author wzy
 * @version 1.0
 */
@Api(tags = "XDCA0006:信用卡审批信息核准接口")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0006Resource.class);

    @Autowired
    private Xdca0006Service xdca0006Service;

    /**
     * 交易码：xdca0006
     * 交易描述：信用卡审批信息核准接口
     *
     * @param xdca0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信用卡审批信息核准接口")
    @PostMapping("/xdca0006")
    protected @ResponseBody
    ResultDto<Xdca0006DataRespDto> xdca0006(@Validated @RequestBody Xdca0006DataReqDto xdca0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataReqDto));
        Xdca0006DataRespDto xdca0006DataRespDto = new Xdca0006DataRespDto();// 响应Dto:大额分期合同签订接口
        ResultDto<Xdca0006DataRespDto> xdca0006DataResultDto = new ResultDto<>();
        try {
            //请求报文
            String appNo = xdca0006DataReqDto.getAPPNO_EXTERNAL();//外部流水号
            if (StringUtil.isNotEmpty(appNo)) {
                // 从xdca0005DataReqDto获取业务值进行业务逻辑处理
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataReqDto));
                xdca0006DataRespDto = xdca0006Service.xdzx0006(xdca0006DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataRespDto));
                // 封装xdca0005DataResultDto中正确的返回码和返回信息
                xdca0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdca0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                // 请求字段为空
                xdca0006DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdca0006DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            // 封装xdca0005DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdca0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdca0005DataRespDto到xdca0005DataResultDto中
        xdca0006DataResultDto.setData(xdca0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataRespDto));
        return xdca0006DataResultDto;
    }
}
