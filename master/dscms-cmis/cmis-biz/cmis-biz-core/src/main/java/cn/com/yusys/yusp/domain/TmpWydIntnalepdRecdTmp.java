/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydIntnalepdRecd
 * @类描述: tmp_wyd_intnalepd_recd数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_intnalepd_recd_tmp")
public class TmpWydIntnalepdRecdTmp {
	
	/** 数据日期 **/
	@Column(name = "DATA_DT")
	private String dataDt;
	
	/** 创建时间 **/
	@Column(name = "CREATED_DATETIME")
	private java.util.Date createdDatetime;
	
	/** 客户id **/
	@Column(name = "CUST_ID")
	private String custId;
	
	/** 承诺还款时间 **/
	@Column(name = "PTP_DATE", unique = false, nullable = true)
	private java.util.Date ptpDate;
	
	/** 承诺还款金额 **/
	@Column(name = "PTP_DATE", unique = false, nullable = true, length = 20)
	private String ptpAmount;
	
	/** 催收记录 **/
	@Column(name = "ACTION_NOTE", unique = false, nullable = true, length = 1024)
	private String actionNote;
	
	/** 客户名称 **/
	@Column(name = "NAME", unique = false, nullable = true, length = 32)
	private String name;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param ptpDate
	 */
	public void setPtpDate(java.util.Date ptpDate) {
		this.ptpDate = ptpDate;
	}
	
    /**
     * @return ptpDate
     */
	public java.util.Date getPtpDate() {
		return this.ptpDate;
	}
	
	/**
	 * @param ptpAmount
	 */
	public void setPtpAmount(String ptpAmount) {
		this.ptpAmount = ptpAmount;
	}
	
    /**
     * @return ptpAmount
     */
	public String getPtpAmount() {
		return this.ptpAmount;
	}
	
	/**
	 * @param actionNote
	 */
	public void setActionNote(String actionNote) {
		this.actionNote = actionNote;
	}
	
    /**
     * @return actionNote
     */
	public String getActionNote() {
		return this.actionNote;
	}
	
	/**
	 * @param createdDatetime
	 */
	public void setCreatedDatetime(java.util.Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	
    /**
     * @return createdDatetime
     */
	public java.util.Date getCreatedDatetime() {
		return this.createdDatetime;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}


}