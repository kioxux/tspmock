package cn.com.yusys.yusp.web.server.xdxw0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0009.req.Xdxw0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0009.resp.Xdxw0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0009.Xdxw0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询合同、抵押合同用章编码
 *
 * @author zoubiao
 * @version 1.0
 */
@Api(tags = "XDXW0009:查询合同、抵押合同用章编码")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0009Resource {
	private static final Logger logger = LoggerFactory.getLogger(BizXdxw0009Resource.class);

	@Autowired
	private Xdxw0009Service xdxw0009Service;
	/**
	 * 交易码：xdxw0009
	 * 交易描述：查询合同、抵押合同用章编码
	 *
	 * @param xdxw0009DataReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("查询合同、抵押合同用章编码")
	@PostMapping("/xdxw0009")
	protected @ResponseBody
	ResultDto<Xdxw0009DataRespDto> xdxw0009(@Validated @RequestBody Xdxw0009DataReqDto xdxw0009DataReqDto) throws Exception {
		logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataReqDto));
		Xdxw0009DataRespDto xdxw0009DataRespDto = new Xdxw0009DataRespDto();// 响应Dto:担保人人数查询
		ResultDto<Xdxw0009DataRespDto> xdxw0009DataResultDto = new ResultDto<>();
		String loanContNo = xdxw0009DataReqDto.getLoanContNo();//借款合同号
		try {
			// 从xdxw0009DataReqDto获取业务值进行业务逻辑处理
			// 调用xdxw0009Service层开始
			logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataReqDto));
			xdxw0009DataRespDto = xdxw0009Service.getXdxw0009(loanContNo);
			logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataRespDto));
			// 封装xdxw0009DataResultDto中正确的返回码和返回信息
			xdxw0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
			xdxw0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
		} catch (Exception e) {
			logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, e.getMessage());
			// 封装xdxw0009DataResultDto中异常返回码和返回信息
			xdxw0009DataResultDto.setCode(EpbEnum.EPB099999.key);
			xdxw0009DataResultDto.setCode(EpbEnum.EPB099999.value);
		}
		// 封装xdxw0009DataRespDto到xdxw0009DataResultDto中
		xdxw0009DataResultDto.setData(xdxw0009DataRespDto);
		logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataResultDto));
		return xdxw0009DataResultDto;
	}
}
