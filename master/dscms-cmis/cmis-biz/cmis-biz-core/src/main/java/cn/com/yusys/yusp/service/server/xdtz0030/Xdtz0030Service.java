package cn.com.yusys.yusp.service.server.xdtz0030;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0030.resp.Xdtz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 响应Data：查看信贷贴现台账中票据是否已经存在
 *
 * @author lihh
 * @version 1.0
 */
@Service
@Transactional(rollbackFor = {BizException.class, Exception.class})
public class Xdtz0030Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0030Service.class);

    @Autowired
    private AccDiscMapper accDiscMapper;


    /**
     * 查看信贷贴现台账中票据是否已经存在
     * @param porderNo
     * @return
     */
    public Xdtz0030DataRespDto getPorderNumByPorderNo(String porderNo) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, porderNo);
        Xdtz0030DataRespDto xdtz0030DataRespDto = accDiscMapper.getPorderNumByPorderNo(porderNo);
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataRespDto));
        return xdtz0030DataRespDto;
    }

}
