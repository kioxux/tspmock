/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCptlSitu;
import cn.com.yusys.yusp.service.RptCptlSituService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:34:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcptlsitu")
public class RptCptlSituResource {
    @Autowired
    private RptCptlSituService rptCptlSituService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCptlSitu>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCptlSitu> list = rptCptlSituService.selectAll(queryModel);
        return new ResultDto<List<RptCptlSitu>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCptlSitu>> index(QueryModel queryModel) {
        List<RptCptlSitu> list = rptCptlSituService.selectByModel(queryModel);
        return new ResultDto<List<RptCptlSitu>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptCptlSitu> show(@PathVariable("pkId") String pkId) {
        RptCptlSitu rptCptlSitu = rptCptlSituService.selectByPrimaryKey(pkId);
        return new ResultDto<RptCptlSitu>(rptCptlSitu);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCptlSitu> create(@RequestBody RptCptlSitu rptCptlSitu) throws URISyntaxException {
        rptCptlSituService.insert(rptCptlSitu);
        return new ResultDto<RptCptlSitu>(rptCptlSitu);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCptlSitu rptCptlSitu) throws URISyntaxException {
        int result = rptCptlSituService.update(rptCptlSitu);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptCptlSituService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptCptlSituService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptCptlSitu>> selectBySerno(@RequestBody QueryModel model) {
        return new ResultDto<List<RptCptlSitu>>(rptCptlSituService.selectByModel(model));
    }

    @PostMapping("/updateSitu")
    protected ResultDto<Integer> updateSitu(@RequestBody RptCptlSitu rptCptlSitu){
        return new ResultDto<Integer>(rptCptlSituService.updateSelective(rptCptlSitu));
    }

    /**
     * 查询集团成员融资信息
     * @param model
     * @return
     */
    @PostMapping("/selectGrpSitu")
    protected ResultDto<List<Map<String,Object>>> selectGrpSitu(@RequestBody QueryModel model){
        return new ResultDto<List<Map<String, Object>>>(rptCptlSituService.selectGrpSitu(model));
    }

    /**
     * 初始化信息
     * @param rptCptlSitu
     * @return
     */
    @PostMapping("/initCptlSitu")
    protected ResultDto<RptCptlSitu> initCptlSitu(@RequestBody RptCptlSitu rptCptlSitu){
        return new ResultDto<RptCptlSitu>(rptCptlSituService.initCptlSitu(rptCptlSitu));
    }

    @PostMapping("/selectByGrpSerno")
    protected ResultDto<RptCptlSitu> selectByGrpSerno(@RequestBody String serno){
        return new ResultDto<RptCptlSitu>(rptCptlSituService.selectBySerno(serno));
    }

    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptCptlSitu rptCptlSitu){
        return  new ResultDto<Integer>(rptCptlSituService.save(rptCptlSitu));
    }
}
