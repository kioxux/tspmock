package cn.com.yusys.yusp.service.server.xdsx0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.ZxdPreLmtApply;
import cn.com.yusys.yusp.dto.UserAndDutyReqDto;
import cn.com.yusys.yusp.dto.UserAndDutyRespDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.req.Xdsx0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.resp.Xdsx0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.ZxdPreLmtApplyMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2FxyjxtClientService;
import cn.com.yusys.yusp.service.Dscms2WxClientService;
import cn.com.yusys.yusp.service.DscmsPspQtClientService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 接口处理类:推送苏州地方征信给信贷
 *
 * @author admin
 * @version 1.0
 */
@Service
public class Xdsx0022Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0022Service.class);

    @Autowired
    private Dscms2FxyjxtClientService dscms2FxyjxtClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ZxdPreLmtApplyMapper zxdPreLmtApplyMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private Dscms2WxClientService dscms2WxClientService;
    @Autowired
    private DscmsPspQtClientService dscmsPspQtClientService;
    @Autowired
    private SenddxService senddxService;

    /**
     * 推送苏州地方征信给信贷
     *
     * @param xdsx0022DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0022DataRespDto xdsx0022(Xdsx0022DataReqDto xdsx0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022DataReqDto));
        //返回对象
        Xdsx0022DataRespDto xdsx0022DataRespDto = new Xdsx0022DataRespDto();
        String erorcd = "9999";
        String erortx = "交易异常，请联系管理员！";
        try {
            BigDecimal szCreditScoreScore = new BigDecimal(0);
            BigDecimal bsRateScore = new BigDecimal(0);
            BigDecimal depositSaleRateScore = new BigDecimal(0);
            BigDecimal patentInfoScore = new BigDecimal(0);
            BigDecimal profitGrowInfoScore = new BigDecimal(0);
            BigDecimal providSsPayInfoScore = new BigDecimal(0);
            BigDecimal utilitiesPayInfoScore = new BigDecimal(0);
            BigDecimal scotolScore = new BigDecimal(0);
            BigDecimal szObankLoanBalSum = new BigDecimal(0);
            BigDecimal maxlimSum = new BigDecimal(0);
            String ifadmi = "";
            String status = "";

            String msg = "success";
            String connRecordId = xdsx0022DataReqDto.getConnRecordId();//对接记录ID
            String conName = xdsx0022DataReqDto.getConName();//企业名称
            String conCode = xdsx0022DataReqDto.getConCode();//企业代码
            String initBrId = xdsx0022DataReqDto.getInitBrId();//具体发起机构
            BigDecimal expectCrdLmt = xdsx0022DataReqDto.getExpectCrdLmt();//借款人预期借款额度
            String bizType = xdsx0022DataReqDto.getBizType();//具体品种
            String crdTerm = xdsx0022DataReqDto.getCrdTerm();//授信期限
            String taxEval = xdsx0022DataReqDto.getTaxEval();//纳税评级
            String creditScroe = xdsx0022DataReqDto.getCreditScroe();//征信报告信用评分
            BigDecimal szObankLoanBal = xdsx0022DataReqDto.getSzObankLoanBal();//信用贷款用信余额
            String isBlklsOrGreyls = xdsx0022DataReqDto.getIsBlklsOrGreyls();//是否黑灰名单客户
            String isSzTaxDisplay = xdsx0022DataReqDto.getIsSzTaxDisplay();//苏州地方征信是否显示纳税信息
            BigDecimal poiorTaxAmt = xdsx0022DataReqDto.getPoiorTaxAmt();//上年度纳税金额
            String isRent = xdsx0022DataReqDto.getIsRent();//是否租用
            String isOwn = xdsx0022DataReqDto.getIsOwn();//是否自有
            String isHavingProp = xdsx0022DataReqDto.getIsHavingProp();//是否具备自有产权证
            String szCreditScore = xdsx0022DataReqDto.getSzCreditScore();//企业苏州地方征信信用情况
            BigDecimal bsRate = xdsx0022DataReqDto.getBsRate();//资产负债率
            BigDecimal depositSaleRate = xdsx0022DataReqDto.getDepositSaleRate();//企业存量贷款与销售收入比率
            String patentInfo = xdsx0022DataReqDto.getPatentInfo();//专利信息情况
            String profitGrowInfo = xdsx0022DataReqDto.getProfitGrowInfo();//利润增长情况
            String controlerOpYear = xdsx0022DataReqDto.getControlerOpYear();//实际控制人从事本行业年限
            String conTrade = xdsx0022DataReqDto.getConTrade();//企业所属行业
            String providSsPayInfo = xdsx0022DataReqDto.getProvidSsPayInfo();//公积金、社保缴纳情况
            String utilitiesPayInfo = xdsx0022DataReqDto.getUtilitiesPayInfo();//水电气费缴纳情况
            String reprSettleInfo = xdsx0022DataReqDto.getReprSettleInfo();//法人或实际控制人落户情况
            BigDecimal totalScore = xdsx0022DataReqDto.getTotalScore();//总分值
            String isAdmit = xdsx0022DataReqDto.getIsAdmit();//是否准入（自动测算）
            BigDecimal maxPrecrdAmt = xdsx0022DataReqDto.getMaxPrecrdAmt();//最大预授信额度
            String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
            if (creditScroe == null || "null".equals(creditScroe) || "".equals(creditScroe)) {
                creditScroe = "0";
            }
            if (poiorTaxAmt == null) {
                poiorTaxAmt = new BigDecimal("0");
            }
            if (szObankLoanBal == null) {
                szObankLoanBal = new BigDecimal("0");
            }

            //获取当日跑批情况，若在跑批中，则返回报错“当前非营业时间”
            ResultDto<Xdqt0006DataRespDto> xdqt0006DataRespDto = dscmsPspQtClientService.xdqt0006();
            String progress = xdqt0006DataRespDto.getData().getProgress();
            if ("1".equals(progress)) {
                msg = "当前非营业时间!";
            }
            if ("".equals(connRecordId)) {
                msg = "对接记录ID不能为空!";
            }
            if ("success".equals(msg)) {
                HhmdkhReqDto reqDto = new HhmdkhReqDto();
                reqDto.setCustnm(conName);
                ResultDto<HhmdkhRespDto> hhmdkhRespDto = dscms2FxyjxtClientService.hhmdkh(reqDto);
                isBlklsOrGreyls = hhmdkhRespDto.getData().getIfBlack();
                if ("A".equals(szCreditScore)) {//企业苏州地方征信信用情况
                    szCreditScoreScore = new BigDecimal(4);
                }
//                if ("A".equals(bsRate)) {//资产负债率
//                    bsRateScore = new BigDecimal(20);
//                } else if ("B".equals(bsRate)) {
//                    bsRateScore = new BigDecimal(16);
//                } else if ("C".equals(bsRate)) {
//                    bsRateScore = new BigDecimal(10);
//                }
//                if ("A".equals(depositSaleRate)) {//企业存量贷款与销售收入比率
//                    depositSaleRateScore = new BigDecimal(18);
//                } else if ("B".equals(depositSaleRate)) {
//                    depositSaleRateScore = new BigDecimal(16);
//                } else if ("C".equals(depositSaleRate)) {
//                    depositSaleRateScore = new BigDecimal(12);
//                }
                if ("A".equals(patentInfo)) {//专利信息情况
                    patentInfoScore = new BigDecimal(2);
                }
                if ("A".equals(profitGrowInfo)) {//利润增长情况
                    profitGrowInfoScore = new BigDecimal(18);
                } else if ("B".equals(profitGrowInfo)) {
                    profitGrowInfoScore = new BigDecimal(15);
                } else if ("C".equals(profitGrowInfo)) {
                    profitGrowInfoScore = new BigDecimal(13);
                } else if ("D".equals(profitGrowInfo)) {
                    profitGrowInfoScore = new BigDecimal(10);
                } else if ("E".equals(profitGrowInfo)) {
                    profitGrowInfoScore = new BigDecimal(8);
                }
                if ("A".equals(providSsPayInfo)) {//公积金、社保缴纳情况
                    providSsPayInfoScore = new BigDecimal(2);
                }
                if ("A".equals(utilitiesPayInfo)) {//水电气费缴纳情况
                    utilitiesPayInfoScore = new BigDecimal(10);
                } else if ("B".equals(utilitiesPayInfo)) {
                    utilitiesPayInfoScore = new BigDecimal(7);
                } else if ("C".equals(utilitiesPayInfo)) {
                    utilitiesPayInfoScore = new BigDecimal(5);
                } else {
                    utilitiesPayInfoScore = new BigDecimal(2);
                }
                //地方征信报告信用评分低于600分、苏州地方征信显示的他行（含小贷公司）信用贷款用信余额超500万、我行风险预警系统内认定为黑灰名单客户、征信系统未显示纳税信息，满足上述任一个条件则不予准入。
                if (Integer.valueOf(creditScroe) < 600 || szObankLoanBal.compareTo(new BigDecimal(5000000)) >= 0 || "1".equals(isBlklsOrGreyls) || "0".equals(isSzTaxDisplay)) {
                    ifadmi = "DISAGREED";
                } else {
                    ifadmi = "AGREED";
                    BigDecimal scoreRate = new BigDecimal(0);//信用信评分卡调节系数
                    BigDecimal lastYearTaxSum = new BigDecimal(0);//上年度纳税总额
                    /*(1) 企业信用评分>=680分，信用评分调节系数为6；
                    (2) 640分<=企业信用评分<680分，企业信用调节系数为5；
                    (3) 600分<=企业信用评分<640分，信用评分系数为4；
                    (4) 企业信用评分<600分，不予准入*/
                    BigDecimal creditScroepf = new BigDecimal(creditScroe);
                    if (creditScroepf.compareTo(new BigDecimal(680)) >= 0) {
                        scoreRate = new BigDecimal(6);
                    } else if (creditScroepf.compareTo(new BigDecimal(640)) >= 0 && creditScroepf.compareTo(new BigDecimal(680)) < 0) {
                        scoreRate = new BigDecimal(5);
                    } else if (creditScroepf.compareTo(new BigDecimal(600)) >= 0 && creditScroepf.compareTo(new BigDecimal(640)) < 0) {
                        scoreRate = new BigDecimal(4);
                    } else {
                        scoreRate = new BigDecimal(0);
                    }
                    lastYearTaxSum = poiorTaxAmt;//上年度纳税总额
                    maxlimSum = lastYearTaxSum.multiply(scoreRate);//最大贷款额度=上年度纳税总额*评分卡调节系数
                    if (szObankLoanBal != null && !"".equals(szObankLoanBal)) {
                        szObankLoanBalSum = szObankLoanBal;//苏州地方征信显示的他行（含小贷公司）信用贷款用信余额
                        if (szObankLoanBalSum.add(maxlimSum).compareTo(new BigDecimal(5000000)) >= 0) {//若企业在其他金融机构有信用贷款的，企业他行信用贷款+企业本行信用贷款+企业主在各家银行的经营性信用贷款总额+征信贷总额最高控制在500万元。
                            maxlimSum = new BigDecimal(5000000).subtract(szObankLoanBalSum);
                        }
                    }
                    if (maxlimSum.compareTo(new BigDecimal(3000000)) >= 0) {//“征信贷”预授信贷款额度，最高不超过300万元。
                        maxlimSum = new BigDecimal(3000000);
                    }
                    if ("1".equals(isRent)) {//如借款企业经营场所系租用的，最高金额为110万元
                        if (maxlimSum.compareTo(new BigDecimal(1100000)) >= 0) {
                            maxlimSum = new BigDecimal(1100000);
                        }
                    }
                    if ("1".equals(isOwn) && "0".equals(isHavingProp)) {//经营场所自有但无产权证的，最高金额200万元。
                        if (maxlimSum.compareTo(new BigDecimal(2000000)) >= 0) {
                            maxlimSum = new BigDecimal(2000000);
                        }
                    }
                    maxlimSum = maxlimSum.divide(new BigDecimal(10000));//因预授信额度单位为万元，所以除以10000

                }
            }
            String ifAdmitSys = CmisBizConstants.NO;
            if ("success".equals(msg)) {//如果准入通过，这自动发送至微信端并保存申请记录，准入不通过，则只保存申请记录
                if ("AGREED".equals(ifadmi)) {//准入通过
                    Wxp001ReqDto reqDto = new Wxp001ReqDto();
                    reqDto.setAdmres(ifadmi);
                    reqDto.setApplid(connRecordId);
                    reqDto.setEntpid(conCode);
                    reqDto.setEntnam(conName);
                    reqDto.setReason("AGREED".equals(ifadmi) ? "同意" : "不同意");
                    reqDto.setPreamt(maxlimSum);
                    dscms2WxClientService.wxp001(reqDto);
                }

                if ("AGREED".equals(ifadmi)) {//准入通过，则将状态变更为已发送至移动端
                    status = "2";
                    ifAdmitSys = CmisBizConstants.YES;
                } else {
                    status = "1";
                }
                ZxdPreLmtApply record = new ZxdPreLmtApply();
                record.setApplyRecordId(connRecordId);
                record.setCorpId(conCode);
                record.setCorpName(conName);
                record.setLaunchOrg(initBrId);
                record.setBorrowerLossLmt(expectCrdLmt);
                record.setPrdName(bizType);
                record.setReportCreditGrade(creditScroe);
                record.setCreditBalance(szObankLoanBal);
                record.setIfBlack(isBlklsOrGreyls);
                record.setIfShowTax(isSzTaxDisplay);
                record.setLtYearTaxAmt(poiorTaxAmt);
                record.setIfRent(isRent);
                record.setIfSelf(isOwn);
                record.setIfHaveCert(isHavingProp);
                record.setCorpCreditInfo(Integer.valueOf(szCreditScoreScore + ""));
                record.setAssetRate(bsRateScore);
                record.setCorpStockRateSys(depositSaleRateScore);
                record.setPatentInfoSys(Integer.valueOf(patentInfoScore + ""));
                record.setProfitIncreaseSys(Integer.valueOf(profitGrowInfoScore + ""));
                record.setSocialPaymentSys(Integer.valueOf(providSsPayInfoScore + ""));
                record.setWaterElecSys(Integer.valueOf(utilitiesPayInfoScore + ""));
                record.setScoreTotalSys(Integer.valueOf(scotolScore + ""));
                record.setIfAdmitSys(ifadmi);
                record.setMaxLmtAmt(maxlimSum);
                record.setStatus(status);
                record.setCreateTime(new Date());
                zxdPreLmtApplyMapper.insertSelective(record);


                //查询所有岗位为征信贷评审经理岗的客户经理手机号码
                //TODO 内网放开以下注释 徐超 公共jar未同步外网 外网放开会报错
//                UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
//                userAndDutyReqDto.setDutyNo("3032");
                //List<UserAndDutyRespDto> userAndDutyRespDtoList = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
//                ResultDto<List<UserAndDutyRespDto>> resultDto = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
//                String telnum = "";
//                String sendMessage = "客户" + conName + "申请征信贷，申请日期" + openday + ",请及时关注和审核!!";//推送消息内容
//                SenddxReqDto senddxReqDto = new SenddxReqDto();
//                senddxReqDto.setInfopt("dx");

//                List<UserAndDutyRespDto> userAndDutyRespDtoList = resultDto.getData();
//
//                List<SenddxReqList> senddxReqListList = new ArrayList<>();
//                for (int i = 0; i < userAndDutyRespDtoList.size(); i++) {
//                    telnum = userAndDutyRespDtoList.get(i).getPhone();
//                    if (!"".equals(telnum)) {
//                        SenddxReqList senddxReqList = new SenddxReqList();
//                        senddxReqList.setMobile(telnum);
//                        senddxReqList.setSmstxt(sendMessage);
//                        senddxReqListList.add(senddxReqList);
//                    }
//                }
//                //发送短信
//                senddxReqDto.setSenddxReqList(senddxReqListList);
//                senddxService.senddx(senddxReqDto);
                erorcd = "0000";
                erortx = "交易成功！";
            } else {
                erorcd = "9999";
                erortx = msg;
            }
            xdsx0022DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
            xdsx0022DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, e.getMessage());
            xdsx0022DataRespDto.setOpFlag("F");
            xdsx0022DataRespDto.setOpMsg(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, e.getMessage());
            xdsx0022DataRespDto.setOpFlag("F");
            xdsx0022DataRespDto.setOpMsg(e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value);
        return xdsx0022DataRespDto;
    }

}
