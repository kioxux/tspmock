package cn.com.yusys.yusp.web.server.xdxw0064;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0064.req.Xdxw0064DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0064.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0064.resp.Xdxw0064DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0064.Xdxw0064Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷授信调查信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0064:优企贷、优农贷授信调查信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0064Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0064Resource.class);

    @Autowired
    private Xdxw0064Service xdxw0064Service;

    /**
     * 交易码：xdxw0064
     * 交易描述：优企贷、优农贷授信调查信息查询
     *
     * @param xdxw0064DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷授信调查信息查询")
    @PostMapping("/xdxw0064")
    protected @ResponseBody
    ResultDto<Xdxw0064DataRespDto> xdxw0064(@Validated @RequestBody Xdxw0064DataReqDto xdxw0064DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataReqDto));
        Xdxw0064DataRespDto xdxw0064DataRespDto = new Xdxw0064DataRespDto();// 响应Dto:优企贷、优农贷授信调查信息查询
        ResultDto<Xdxw0064DataRespDto> xdxw0064DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0064DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0064Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataReqDto));
            java.util.List<List> result = Optional.ofNullable(xdxw0064Service.getSurveyList(xdxw0064DataReqDto)).orElseGet(() -> {
                List list = new List();
                list.setSurvey_serno(StringUtils.EMPTY);// 主键
                list.setLoan_type(StringUtils.EMPTY);// 1综合消费贷2汽车消费贷3经营性小贷4经营性微贷
                list.setZb_manager_id(StringUtils.EMPTY);// 主办客户经理号
                list.setXb_manager_id(StringUtils.EMPTY);// 现在存的是协办客户经理中文名
                list.setInput_date(StringUtils.EMPTY);// 检查日期
                list.setApprove_status(StringUtils.EMPTY);// 审批状态
                list.setBiz_from(StringUtils.EMPTY);// 业务来源XD_BIZ_FROM
                list.setTjr(StringUtils.EMPTY);// 推荐人
                list.setBiz_type(StringUtils.EMPTY);// 业务类型
                list.setCopy_from(StringUtils.EMPTY);// 从哪复制
                list.setLast_update_time(StringUtils.EMPTY);// 最后一次审批时间
                list.setZffs(StringUtils.EMPTY);// 支付方式
                list.setCont_type(StringUtils.EMPTY);// 合同类型S
                list.setBcsx_amt(StringUtils.EMPTY);// 本次授信金额
                list.setBcyx_amt(StringUtils.EMPTY);// 本次用信金额
                list.setIs_sjsh(StringUtils.EMPTY);// 是否随借随还
                list.setIflag(StringUtils.EMPTY);// 撤销标志
                list.setXf_org(StringUtils.EMPTY);// 取的s_user表中的xf_orgid
                return Arrays.asList(list);
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(result));
            xdxw0064DataRespDto.setList(result);
            // 封装xdxw0064DataResultDto中正确的返回码和返回信息
            xdxw0064DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0064DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, e.getMessage());
            // 封装xdxw0064DataResultDto中异常返回码和返回信息
            xdxw0064DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0064DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0064DataRespDto到xdxw0064DataResultDto中
        xdxw0064DataResultDto.setData(xdxw0064DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataResultDto));
        return xdxw0064DataResultDto;
    }
}
