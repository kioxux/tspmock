/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.OtherAppDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueBranchIdentySubMapper;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueCadaIdentySubMapper;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueIdentyRelMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.OtherGrtValueIdentyAppMapper;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueIdentyAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 20:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherGrtValueIdentyAppService {
    private static final Logger log = LoggerFactory.getLogger(OtherGrtValueIdentyAppService.class);

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private OtherGrtValueIdentyAppMapper otherGrtValueIdentyAppMapper;

    @Autowired
    private OtherGrtValueIdentyRelMapper otherGrtValueIdentyRelMapper;

    @Autowired
    private OtherGrtValueCadaIdentySubMapper otherGrtValueCadaIdentySubMapper;

    @Autowired
    private OtherGrtValueBranchIdentySubMapper otherGrtValueBranchIdentySubMapper;

    @Resource
    private CmisCusClientService cmisCusClientService;

    @Resource
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public OtherGrtValueIdentyApp selectByPrimaryKey(String serno) {
        return otherGrtValueIdentyAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherGrtValueIdentyApp> selectAll(QueryModel model) {
        List<OtherGrtValueIdentyApp> records = (List<OtherGrtValueIdentyApp>) otherGrtValueIdentyAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<OtherGrtValueIdentyApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(OtherGrtValueIdentyApp record) {
        return otherGrtValueIdentyAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(OtherGrtValueIdentyApp record) {
        return otherGrtValueIdentyAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(OtherGrtValueIdentyApp record) {
        return otherGrtValueIdentyAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(OtherGrtValueIdentyApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherGrtValueIdentyAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherGrtValueIdentyAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherGrtValueIdentyAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int deleteInfo(String serno) {
        OtherGrtValueIdentyApp otherGrtValueIdentyApp = otherGrtValueIdentyAppMapper.selectByPrimaryKey(serno);
        if (otherGrtValueIdentyApp == null){
            throw BizException.error(null, "999999", "删除：授信抵押物价值认定申请异常");
        }
        //打回
        if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherGrtValueIdentyApp.getApproveStatus())) {
            otherGrtValueIdentyApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherGrtValueIdentyApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}", otherGrtValueIdentyApp.getSerno());
            workflowCoreClient.deleteByBizId(otherGrtValueIdentyApp.getSerno());
            int i = otherGrtValueIdentyAppMapper.updateByPrimaryKeySelective(otherGrtValueIdentyApp);

            if (i != 1) {
                throw BizException.error(null, "999999", "删除：外币利率申请异常");
            }
            return i;
        }else{
            //1、授信抵质押物价值认定与抵质押物关联表
            int i = otherGrtValueIdentyRelMapper.updateBySerno(serno);
            //2、授信抵质押物价值云评估明细
            int j = otherGrtValueCadaIdentySubMapper.updateBySerno(serno);
            //3、授信抵质押物价值支行明细
            int k = otherGrtValueBranchIdentySubMapper.updateBySerno(serno);

            //转换日期 ASSURE_CERT_CODE
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
            User user = SessionUtils.getUserInformation();
            otherGrtValueIdentyApp.setUpdateTime(DateUtils.getCurrDate());
            otherGrtValueIdentyApp.setUpdDate(dateFormat.format(new Date()) );
            otherGrtValueIdentyApp.setUpdId(user.getLoginCode());
            otherGrtValueIdentyApp.setUpdBrId(user.getOrg().getCode());

            //操作类型  删除
            otherGrtValueIdentyApp.setOprType(CmisBizConstants.OPR_TYPE_02);
            int l = otherGrtValueIdentyAppMapper.updateByPrimaryKeySelective(otherGrtValueIdentyApp);
            if (l != 1) {
                throw BizException.error(null, "999999", "删除：授信抵押物价值认定申请异常");
            }

            return i;
        }
    }

    /**
     * @方法名称: getOtherGrtValueIdentyAppByModel
     * @方法描述: 根据querymodel获取当前客户经理名下所有授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherGrtValueIdentyApp> getOtherGrtValueIdentyAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_000+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_111
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_992);
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getOtherGrtValueIdentyAppHis
     * @方法描述: 获取当前客户经理名下所有授信抵押物价值认定申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherGrtValueIdentyApp> getOtherGrtValueIdentyAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_990+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_991
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_996
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_997+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_998
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_993
        );
        List<OtherGrtValueIdentyApp> list = otherGrtValueIdentyAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: addothergrtvalueidentyapp
     * @方法描述: 新增授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addothergrtvalueidentyapp(OtherGrtValueIdentyApp othergrtvalueidentyapp) {
        {
            OtherGrtValueIdentyApp otherGrtValueIdentyApp = new OtherGrtValueIdentyApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(othergrtvalueidentyapp, otherGrtValueIdentyApp);

                CusBaseDto cusBaseDto1 = commonService.getCusBaseByCusId(otherGrtValueIdentyApp.getCusId());
                //信用等级
                otherGrtValueIdentyApp.setFinalRank(cusBaseDto1.getCusCrdGrade());

                //对公
                //查询根据客户编号查询法人名称,名称，证件类型，证件号，电话 cus_base,cus_indiv,cus_corp_mgr
                CmisCus0008ReqDto cmisCus0008ReqDto = new CmisCus0008ReqDto();
                cmisCus0008ReqDto.setCusId(otherGrtValueIdentyApp.getCusId());
                ResultDto<CmisCus0008RespDto> cmisCusResultRespDto = cmisCusClientService.cmiscus0008(cmisCus0008ReqDto);
                if(ResultDto.success().getCode().equals(cmisCusResultRespDto.getCode())){
                    CmisCus0008RespDto cmisCus0008RespDto = cmisCusResultRespDto.getData();
                    if(!Objects.isNull(cmisCus0008RespDto)){
                        //法定代表人
                        otherGrtValueIdentyApp.setCusIdMgr(cmisCus0008RespDto.getMrgName());
                    }
                }

                //个人
                //查询根据客户编号查询个人基本信息
                CmisCus0007ReqDto cmisCus0007ReqDto = new CmisCus0007ReqDto();
                cmisCus0007ReqDto.setCusId(otherGrtValueIdentyApp.getCusId());
                ResultDto<CmisCus0007RespDto> cmisCusIndivResultRespDto = cmisCusClientService.cmiscus0007(cmisCus0007ReqDto);
                if(ResultDto.success().getCode().equals(cmisCusIndivResultRespDto.getCode())){
                    CmisCus0007RespDto cmisCus0007RespDto = cmisCusIndivResultRespDto.getData();
                    List<CusIndivDto> cusIndivList = cmisCus0007RespDto.getCusIndivList();
                    List<CusIndivDto> collect = cusIndivList.stream().filter(cusIndivDto -> Objects.equals(otherGrtValueIdentyApp.getCusId(), cusIndivDto.getCusId())).collect(Collectors.toList());
                    if(!Objects.isNull(collect) && collect.size()>0){
                        //住所地
                        otherGrtValueIdentyApp.setHouhRegAdd(collect.get(0).getIndivHouhRegAdd());
                    }
                }

                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherGrtValueIdentyApp.setOprType(CmisBizConstants.OPR_TYPE_01);
                //审批状态
                otherGrtValueIdentyApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherGrtValueIdentyApp.setManagerBrId(user.getOrg().getCode());
                otherGrtValueIdentyApp.setManagerId(user.getLoginCode());
                otherGrtValueIdentyApp.setInputId(user.getLoginCode());
                otherGrtValueIdentyApp.setInputBrId(user.getOrg().getCode());
                otherGrtValueIdentyApp.setInputDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyApp.setCreateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyApp.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyApp.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyApp.setUpdId(user.getLoginCode());
                otherGrtValueIdentyApp.setUpdBrId(user.getOrg().getCode());

                int i = otherGrtValueIdentyAppMapper.insertSelective(otherGrtValueIdentyApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：授信抵押物价值认定申请异常");
                }

            }catch (Exception e) {
                log.error("授信抵押物价值认定新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherGrtValueIdentyApp);

        }
    }

    /**
     * @方法名称: updateothergrtvalueidentyapp
     * @方法描述: 更新授信抵押物价值认定申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateothergrtvalueidentyapp(OtherGrtValueIdentyApp othergrtvalueidentyapp) {
        {
            OtherGrtValueIdentyApp otherGrtValueIdentyApp = new OtherGrtValueIdentyApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(othergrtvalueidentyapp, otherGrtValueIdentyApp);

                CusBaseDto cusBaseDto1 = commonService.getCusBaseByCusId(otherGrtValueIdentyApp.getCusId());
                //信用等级
                otherGrtValueIdentyApp.setFinalRank(cusBaseDto1.getCusCrdGrade());

                //对公
                //查询根据客户编号查询法人名称,名称，证件类型，证件号，电话 cus_base,cus_indiv,cus_corp_mgr
                CmisCus0008ReqDto cmisCus0008ReqDto = new CmisCus0008ReqDto();
                cmisCus0008ReqDto.setCusId(otherGrtValueIdentyApp.getCusId());
                ResultDto<CmisCus0008RespDto> cmisCusResultRespDto = cmisCusClientService.cmiscus0008(cmisCus0008ReqDto);
                if(ResultDto.success().getCode().equals(cmisCusResultRespDto.getCode())){
                    CmisCus0008RespDto cmisCus0008RespDto = cmisCusResultRespDto.getData();
                    if(!Objects.isNull(cmisCus0008RespDto)){
                        //法定代表人
                        otherGrtValueIdentyApp.setCusIdMgr(cmisCus0008RespDto.getMrgName());
                    }
                }

                //个人
                //查询根据客户编号查询个人基本信息
                CmisCus0007ReqDto cmisCus0007ReqDto = new CmisCus0007ReqDto();
                cmisCus0007ReqDto.setCusId(otherGrtValueIdentyApp.getCusId());
                ResultDto<CmisCus0007RespDto> cmisCusIndivResultRespDto = cmisCusClientService.cmiscus0007(cmisCus0007ReqDto);
                if(ResultDto.success().getCode().equals(cmisCusIndivResultRespDto.getCode())){
                    CmisCus0007RespDto cmisCus0007RespDto = cmisCusIndivResultRespDto.getData();
                    List<CusIndivDto> cusIndivList = cmisCus0007RespDto.getCusIndivList();
                    List<CusIndivDto> collect = cusIndivList.stream().filter(cusIndivDto -> Objects.equals(otherGrtValueIdentyApp.getCusId(), cusIndivDto.getCusId())).collect(Collectors.toList());
                    if(!Objects.isNull(collect) && collect.size()>0){
                        //住所地
                        otherGrtValueIdentyApp.setHouhRegAdd(collect.get(0).getIndivHouhRegAdd());
                    }
                }

                //补充 克隆缺少数据
                OtherGrtValueIdentyApp oldotherGrtValueIdentyApp = otherGrtValueIdentyAppMapper.selectByPrimaryKey(otherGrtValueIdentyApp.getSerno());
                //放入必填参数 操作类型 新增
                otherGrtValueIdentyApp.setOprType(oldotherGrtValueIdentyApp.getOprType());
                //审批状态
                otherGrtValueIdentyApp.setApproveStatus(oldotherGrtValueIdentyApp.getApproveStatus());
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherGrtValueIdentyApp.setUpdateTime(DateUtils.getCurrDate());
                otherGrtValueIdentyApp.setUpdDate(dateFormat.format(new Date()) );
                otherGrtValueIdentyApp.setUpdId(user.getLoginCode());
                otherGrtValueIdentyApp.setUpdBrId(user.getOrg().getCode());

                int i = otherGrtValueIdentyAppMapper.updateByPrimaryKeySelective(otherGrtValueIdentyApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：授信抵押物价值认定申请异常");
                }

            }catch (Exception e) {
                log.error("授信抵押物价值认定修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherGrtValueIdentyApp);

        }
    }

    /**
     * @方法名称: checkothergrtvalueidentyapp
     * @方法描述: 校验抵押物价值认定信息是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto checkothergrtvalueidentyapp(OtherGrtValueIdentyApp othergrtvalueidentyapp) {
        OtherGrtValueIdentyApp otherGrtValueIdentyApp = otherGrtValueIdentyAppMapper.selectByPrimaryKey(othergrtvalueidentyapp.getSerno());

        if(!CollectionUtils.nonNull(otherGrtValueIdentyApp)){
            throw BizException.error(null, "999999", "抵押物价值认定信息不存在");
        }
        return new ResultDto(otherGrtValueIdentyApp);
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherGrtValueIdentyApp otherGrtValueIdentyApp = new OtherGrtValueIdentyApp();
        otherGrtValueIdentyApp.setSerno(serno);
        otherGrtValueIdentyApp.setApproveStatus(approveStatus);
        return updateSelective(otherGrtValueIdentyApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author css
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherGrtValueIdentyAppMapper.selectOtherAppDtoDataByParam(map);
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据授信流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherGrtValueIdentyApp> selectByLmtSerno(Map params) {
        String  lmtSerno = (String) params.get("lmtSerno");
        if(null!=lmtSerno || !"".equals(lmtSerno)) {
            return otherGrtValueIdentyAppMapper.selectByLmtSerno(params);
        }else {
            throw new RuntimeException("Oops: lmtSerno can't be null!" );
        }
    }
}
