/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperEnergySalesDetail;
import cn.com.yusys.yusp.service.RptOperEnergySalesDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySalesDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:49:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperenergysalesdetail")
public class RptOperEnergySalesDetailResource {
    @Autowired
    private RptOperEnergySalesDetailService rptOperEnergySalesDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperEnergySalesDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperEnergySalesDetail> list = rptOperEnergySalesDetailService.selectAll(queryModel);
        return new ResultDto<List<RptOperEnergySalesDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperEnergySalesDetail>> index(QueryModel queryModel) {
        List<RptOperEnergySalesDetail> list = rptOperEnergySalesDetailService.selectByModel(queryModel);
        return new ResultDto<List<RptOperEnergySalesDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperEnergySalesDetail> show(@PathVariable("pkId") String pkId) {
        RptOperEnergySalesDetail rptOperEnergySalesDetail = rptOperEnergySalesDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperEnergySalesDetail>(rptOperEnergySalesDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperEnergySalesDetail> create(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail) throws URISyntaxException {
        rptOperEnergySalesDetailService.insert(rptOperEnergySalesDetail);
        return new ResultDto<RptOperEnergySalesDetail>(rptOperEnergySalesDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail) throws URISyntaxException {
        int result = rptOperEnergySalesDetailService.update(rptOperEnergySalesDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperEnergySalesDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperEnergySalesDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperEnergySalesDetail>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptOperEnergySalesDetail>>(rptOperEnergySalesDetailService.selectByModel(model));
    }

    @PostMapping("/insertSalesDetail")
    protected ResultDto<Integer> insertSalesDetail(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail){
        rptOperEnergySalesDetail.setPkId(UUID.randomUUID().toString());
        return  new ResultDto<Integer>(rptOperEnergySalesDetailService.insertSalesDetail(rptOperEnergySalesDetail));
    }

    @PostMapping("/updateSalesDetail")
    protected ResultDto<Integer> updateSalesDetail(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail){
        return new ResultDto<Integer>(rptOperEnergySalesDetailService.updateSelective(rptOperEnergySalesDetail));
    }
    @PostMapping("/deleteSalesDetail")
    protected ResultDto<Integer> deleteSalesDetail(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail){
        String pkId = rptOperEnergySalesDetail.getPkId();
        return new ResultDto<Integer>(rptOperEnergySalesDetailService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody RptOperEnergySalesDetail rptOperEnergySalesDetail){
        rptOperEnergySalesDetail.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOperEnergySalesDetailService.insertSelective(rptOperEnergySalesDetail));
    }
}
