/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSurveyReportOtherInfo;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportOtherInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportOtherInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-25 19:48:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSurveyReportOtherInfoService {

    @Autowired
    private LmtSurveyReportOtherInfoMapper lmtSurveyReportOtherInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSurveyReportOtherInfo selectByPrimaryKey(String surveySerno) {
        return lmtSurveyReportOtherInfoMapper.selectByPrimaryKey(surveySerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSurveyReportOtherInfo> selectAll(QueryModel model) {
        List<LmtSurveyReportOtherInfo> records = (List<LmtSurveyReportOtherInfo>) lmtSurveyReportOtherInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSurveyReportOtherInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyReportOtherInfo> list = lmtSurveyReportOtherInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSurveyReportOtherInfo record) {
        return lmtSurveyReportOtherInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSurveyReportOtherInfo record) {
        return lmtSurveyReportOtherInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSurveyReportOtherInfo record) {
        return lmtSurveyReportOtherInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSurveyReportOtherInfo record) {
        return lmtSurveyReportOtherInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String surveySerno) {
        return lmtSurveyReportOtherInfoMapper.deleteByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSurveyReportOtherInfoMapper.deleteByIds(ids);
    }
}
