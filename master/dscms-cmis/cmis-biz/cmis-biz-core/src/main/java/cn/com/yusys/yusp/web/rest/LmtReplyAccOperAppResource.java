/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.domain.LmtGrpReplyAccOperApp;
import cn.com.yusys.yusp.domain.PvpLoanAppSegInterstSub;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAccOperApp;
import cn.com.yusys.yusp.service.LmtReplyAccOperAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:23:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyaccoperapp")
public class LmtReplyAccOperAppResource {
    @Autowired
    private LmtReplyAccOperAppService lmtReplyAccOperAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAccOperApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAccOperApp> list = lmtReplyAccOperAppService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAccOperApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAccOperApp>> index(QueryModel queryModel) {
        List<LmtReplyAccOperApp> list = lmtReplyAccOperAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAccOperApp> show(@PathVariable("pkId") String pkId) {
        LmtReplyAccOperApp lmtReplyAccOperApp = lmtReplyAccOperAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAccOperApp>(lmtReplyAccOperApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAccOperApp> create(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) throws URISyntaxException {
        lmtReplyAccOperAppService.insert(lmtReplyAccOperApp);
        return new ResultDto<LmtReplyAccOperApp>(lmtReplyAccOperApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) throws URISyntaxException {
        int result = lmtReplyAccOperAppService.update(lmtReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) throws URISyntaxException {
        int result = lmtReplyAccOperAppService.updateSelective(lmtReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccOperAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccOperAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:guideSave
     * @函数描述:额度冻结解冻向导下一步保存
     * @创建人：ywl
     * @创建时间：2021/5/18
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/guideSave")
    protected ResultDto<String> guideSave(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) throws Exception {
        return new ResultDto<String>(lmtReplyAccOperAppService.guideSave(lmtReplyAccOperApp));
    }

    /**
     * @函数名称:queryAll
     * @函数描述:单一客户
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<LmtReplyAccOperApp>> queryAll(@RequestBody QueryModel queryModel) {
        List<LmtReplyAccOperApp> list = lmtReplyAccOperAppService.queryAll(queryModel);
        return new ResultDto<List<LmtReplyAccOperApp>>(list);
    }
    /**
     * @函数名称:queryHis
     * @函数描述:单一客户
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryHis")
    protected ResultDto<List<LmtReplyAccOperApp>> queryHis(@RequestBody QueryModel queryModel) {
        List<LmtReplyAccOperApp> list = lmtReplyAccOperAppService.queryHis(queryModel);
        return new ResultDto<List<LmtReplyAccOperApp>>(list);
    }

    /**
     * @函数名称:updateoperapp
     * @函数描述:客户额度冻结/解冻/终止申请暂存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoperapp")
    protected ResultDto<Integer> updateoperapp(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) throws URISyntaxException {
        int result = lmtReplyAccOperAppService.updateoperapp(lmtReplyAccOperApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectlmtreplyaccoperappbyserno
     * @函数描述:客户额度冻结/解冻/终止申请根据流水号查询
     * @创建者: yangwl
     * @创建时间: 2021/5/21
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectlmtreplyaccoperappbyserno")
    protected  ResultDto<LmtReplyAccOperApp> selectlmtreplyaccoperappbyserno(@RequestBody String serno) {
        LmtReplyAccOperApp lmtReplyAccOperApp = lmtReplyAccOperAppService.selectlmtreplyaccoperappbyserno(serno);
        return new ResultDto<LmtReplyAccOperApp>(lmtReplyAccOperApp);
    }

    /**
     * @函数名称:selectlmtreplyaccoperapp
     * @函数描述:查询客户额度冻结/解冻/终止申请是否已存在
     * @创建者: yangwl
     * @创建时间: 2021/6/24
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectlmtreplyaccoperapp")
    protected  ResultDto<Map> selectLmtReplyAccOperApp(@RequestBody LmtReplyAccOperApp lmtReplyAccOperApp) {
        Map rtnData = lmtReplyAccOperAppService.selectLmtReplyAccOperApp(lmtReplyAccOperApp);
        return new ResultDto<Map>(rtnData);
    }
}
