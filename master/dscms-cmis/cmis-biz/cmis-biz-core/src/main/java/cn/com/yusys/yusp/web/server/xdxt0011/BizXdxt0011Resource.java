package cn.com.yusys.yusp.web.server.xdxt0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdxt0011.Xdxt0011Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.dto.server.xdxt0011.req.Xdxt0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.resp.Xdxt0011DataRespDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户经理信息通用查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0011:客户经理信息通用查")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0011Resource.class);

    @Autowired
    private Xdxt0011Service xdxt0011Service;
    /**
     * 交易码：xdxt0011
     * 交易描述：客户经理信息通用查
     *
     * @param xdxt0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户经理信息通用查")
    @PostMapping("/xdxt0011")
    protected @ResponseBody
    ResultDto<Xdxt0011DataRespDto> xdxt0011(@Validated @RequestBody Xdxt0011DataReqDto xdxt0011DataReqDto) throws BizException,Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataReqDto));
        Xdxt0011DataRespDto xdxt0011DataRespDto = new Xdxt0011DataRespDto();// 响应Dto:客户经理信息通用查
        ResultDto<Xdxt0011DataRespDto> xdxt0011DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataReqDto));
            xdxt0011DataRespDto=xdxt0011Service.getXdxt0011(xdxt0011DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataReqDto));
            // 封装xdxt0011DataResultDto中正确的返回码和返回信息
            xdxt0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }  catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdxt0011DataResultDto.setCode(e.getErrorCode());
            xdxt0011DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, e.getMessage());
            // 封装xdxt0011DataResultDto中异常返回码和返回信息
            xdxt0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0011DataRespDto到xdxt0011DataResultDto中
        xdxt0011DataResultDto.setData(xdxt0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataResultDto));
        return xdxt0011DataResultDto;
    }
}
