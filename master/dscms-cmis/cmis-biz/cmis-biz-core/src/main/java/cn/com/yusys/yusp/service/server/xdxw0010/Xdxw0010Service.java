package cn.com.yusys.yusp.service.server.xdxw0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0010.req.Xdxw0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0010.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtInspectInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0010Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0010Service.class);

    @Autowired
    private LmtInspectInfoMapper lmtInspectInfoMapper;

    /**
     * 勘验列表信息查询
     * @param xdxw0010DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public java.util.List<List> getInspectInfoList(Xdxw0010DataReqDto xdxw0010DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010DataReqDto));
        PageInfo<List> pageInfo = new PageInfo<>();
        try {
            PageHelper.startPage(Optional.ofNullable(xdxw0010DataReqDto.getStartPageNum()).orElse(CmisBizConstants.INT_ONE), Optional.ofNullable(xdxw0010DataReqDto.getPageSize()).orElse(CmisBizConstants.INT_ONE));
            java.util.List<List> result = lmtInspectInfoMapper.getInspectInfoList(xdxw0010DataReqDto);
            pageInfo = new PageInfo<>(result);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(pageInfo));
        return pageInfo.getList();
    }


}
