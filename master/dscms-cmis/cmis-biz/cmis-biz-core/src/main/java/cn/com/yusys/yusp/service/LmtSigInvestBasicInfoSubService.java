/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicFncAnly;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicFncAnlyMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoSubMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:18:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoSubService {

    @Autowired
    private LmtSigInvestBasicInfoSubMapper lmtSigInvestBasicInfoSubMapper;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService;

    @Autowired
    private LmtSigInvestBasicFncAnlyMapper lmtSigInvestBasicFncAnlyMapper;

    @Autowired
    private LmtAppRelGrpLimitService lmtAppRelGrpLimitService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSub selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoSub> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoSub> records = (List<LmtSigInvestBasicInfoSub>) lmtSigInvestBasicInfoSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoSub> list = lmtSigInvestBasicInfoSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoSub record) {
        return lmtSigInvestBasicInfoSubMapper.insert(record);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertBasicInfoSub(LmtSigInvestBasicInfoSub record) {
        //添加企业相关信息
        lmtAppRelCusInfoService.insertCusInfoApp(record.getBasicSerno(),record.getBasicCusId(),null,record.getBasicCusCatalog());

        //添加集团额度相关信息
        lmtAppRelGrpLimitService.insertCusLmtInfo(record.getBasicSerno(), record.getBasicCusId(), null, record.getBasicCusCatalog());

        LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly = new LmtSigInvestBasicFncAnly();
        lmtSigInvestBasicFncAnly.setSerno(record.getSerno());
        lmtSigInvestBasicFncAnly.setBasicSerno(record.getBasicSerno());
        lmtSigInvestBasicFncAnly.setCusId(record.getBasicCusId());
        lmtSigInvestBasicFncAnlyMapper.insert(lmtSigInvestBasicFncAnly);
        if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(record.getIsAppBasicLmt())){
            lmtSigInvestBasicLimitAppService.insert(record);
        }
        return lmtSigInvestBasicInfoSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoSub record) {
        return lmtSigInvestBasicInfoSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoSub record) {
        return lmtSigInvestBasicInfoSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoSub record) {
        return lmtSigInvestBasicInfoSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoSubMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtSigInvestBasicInfoSubMapper.updateByParams(delMap);
    }




    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public List<LmtSigInvestBasicInfoSub> selectListBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubList = selectByModel(queryModel);
        if (lmtSigInvestBasicInfoSubList!=null && lmtSigInvestBasicInfoSubList.size()>0){
            return lmtSigInvestBasicInfoSubList;
        }
        return null;
    }

    public List<LmtSigInvestBasicInfoSub> selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubs = selectAll(queryModel);
        if (lmtSigInvestBasicInfoSubs!=null && lmtSigInvestBasicInfoSubs.size()>0){
            return lmtSigInvestBasicInfoSubs;
        }
        return null;
    }
}
