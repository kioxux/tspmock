/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalApply
 * @类描述: guar_eval_apply数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-26 15:21:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_eval_apply")
public class GuarEvalApply extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "EVAL_APPLY_SERNO")
	private String evalApplySerno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 32)
	private String guarNo;
	
	/** 评估方式 **/
	@Column(name = "EVAL_WAY", unique = false, nullable = true, length = 5)
	private String evalWay;
	
	/** 认定价值币种 **/
	@Column(name = "IDENTY_VALUE_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String identyValueCurType;
	
	/** 认定价值 **/
	@Column(name = "IDENTY_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal identyValue;
	
	/** 折算人民币价值 **/
	@Column(name = "IDENTY_VALUE_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal identyValueCny;
	
	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 原申请流水号 **/
	@Column(name = "OLD_EVAL_APPLY_SERNO", unique = false, nullable = true, length = 40)
	private String oldEvalApplySerno;

	/** 估值申请生效状态 **/
	@Column(name = "EVAL_APPLY_STATUS", unique = false, nullable = true, length = 40)
	private String evalApplyStatus;
	
	
	/**
	 * @param evalApplySerno
	 */
	public void setEvalApplySerno(String evalApplySerno) {
		this.evalApplySerno = evalApplySerno;
	}
	
    /**
     * @return evalApplySerno
     */
	public String getEvalApplySerno() {
		return this.evalApplySerno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param evalWay
	 */
	public void setEvalWay(String evalWay) {
		this.evalWay = evalWay;
	}
	
    /**
     * @return evalWay
     */
	public String getEvalWay() {
		return this.evalWay;
	}
	
	/**
	 * @param identyValueCurType
	 */
	public void setIdentyValueCurType(String identyValueCurType) {
		this.identyValueCurType = identyValueCurType;
	}
	
    /**
     * @return identyValueCurType
     */
	public String getIdentyValueCurType() {
		return this.identyValueCurType;
	}
	
	/**
	 * @param identyValue
	 */
	public void setIdentyValue(java.math.BigDecimal identyValue) {
		this.identyValue = identyValue;
	}
	
    /**
     * @return identyValue
     */
	public java.math.BigDecimal getIdentyValue() {
		return this.identyValue;
	}
	
	/**
	 * @param identyValueCny
	 */
	public void setIdentyValueCny(java.math.BigDecimal identyValueCny) {
		this.identyValueCny = identyValueCny;
	}
	
    /**
     * @return identyValueCny
     */
	public java.math.BigDecimal getIdentyValueCny() {
		return this.identyValueCny;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oldEvalApplySerno
	 */
	public void setOldEvalApplySerno(String oldEvalApplySerno) {
		this.oldEvalApplySerno = oldEvalApplySerno;
	}

	/**
	 * @return oldEvalApplySerno
	 */
	public String getOldEvalApplySerno() {
		return this.oldEvalApplySerno;
	}

	/**
	 * @param evalApplyStatus
	 */
	public void setEvalApplyStatus(String evalApplyStatus) {
		this.evalApplyStatus = evalApplyStatus;
	}

	/**
	 * @return evalApplyStatus
	 */
	public String getEvalApplyStatus() {
		return this.evalApplyStatus;
	}


}