/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.domain.CreditCardAppInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardFinalJudgInifo;
import cn.com.yusys.yusp.repository.mapper.CreditCardFinalJudgInifoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFinalJudgInifoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-27 14:33:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardFinalJudgInifoService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private CreditCardFinalJudgInifoMapper creditCardFinalJudgInifoMapper;
    @Autowired
    private  CreditCardAppInfoService creditCardAppInfoService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardFinalJudgInifo selectByPrimaryKey(String pkId) {
        return creditCardFinalJudgInifoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardFinalJudgInifo> selectAll(QueryModel model) {
        List<CreditCardFinalJudgInifo> records = (List<CreditCardFinalJudgInifo>) creditCardFinalJudgInifoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardFinalJudgInifo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardFinalJudgInifo> list = creditCardFinalJudgInifoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardFinalJudgInifo record) {
        return creditCardFinalJudgInifoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardFinalJudgInifo record) {
        return creditCardFinalJudgInifoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardFinalJudgInifo record) {
        return creditCardFinalJudgInifoMapper.updateByPrimaryKey(record);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/3 11:18
     * @version 1.0.0
     * @desc  终审一审意见修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int updateFirst(CreditCardFinalJudgInifo record) {
        int result = 0;
        //申请白金卡且接受降级卡种的，终审额度小于10w的自动降为金卡
        String applyCardPrd= record.getApplyCardPrd();
        String commonApplyPrd = record.getCommonCardPrd();
        BigDecimal commonAmt = record.getCommonCardApproveAmt();
        String serno = record.getSerno();
        log.info("终审一审意见修改:{}",serno);
        BigDecimal approveAmt = record.getApproveAmt();
        CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(serno);
        String applyType = creditCardAppInfo.getApplyType();
        String isagreed = creditCardAppInfo.getIsAgreeCardDown();
        //主副同申或者独立主卡，验证
        if(("A".equals(applyType) || "B".equals(applyType)) && "00".equals(record.getApproveResult())){
            if("1".equals(isagreed) && "000003".equals(applyCardPrd)&&approveAmt.compareTo(new BigDecimal(100000))<0){
                record.setApplyCardPrd("000002");
                creditCardAppInfo.setApplyCardPrd("000002");
            }
        }
        //主副同申或者独立副卡，验证副卡金额
        if(("A".equals(applyType) || "S".equals(applyType)) && "00".equals(record.getCommonCardApproveResult())){
            if("1".equals(isagreed) && "000003".equals(commonApplyPrd)&&commonAmt.compareTo(new BigDecimal(100000))<0){
                record.setCommonCardPrd("000002");
                creditCardAppInfo.setApplyCommonCardPrd("000002");
            }
        }
        String postFlag = record.getFinalPostFlag();
        //判断终审一岗还是二岗
        if("00".equals(postFlag)){
            result = creditCardFinalJudgInifoMapper.updateByPrimaryKey(record);
            if(result !=1){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡终审意见信息修改异常！");
            }
            //如果一岗降级，修改二岗数据以展示
            //查询终审二岗数据，更改二岗产品
            record.setFinalPostFlag("01");
            CreditCardFinalJudgInifo creditCardFinalJudgInifo2  = this.selectBySerno(record);
            record.setPkId(creditCardFinalJudgInifo2.getPkId());
            result = creditCardFinalJudgInifoMapper.updateByPrimaryKey(record);
            if(result !=1){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡终审意见信息修改异常！");
            }
        }else{
            result = creditCardFinalJudgInifoMapper.updateByPrimaryKey(record);
            if(result !=1){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡终审意见信息修改异常！");
            }
        }
        result = creditCardAppInfoService.updateSelective(creditCardAppInfo);
        if(result !=1){
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "信用卡信息修改异常！");
        }
        return result;
    }
    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardFinalJudgInifo record) {
        return creditCardFinalJudgInifoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardFinalJudgInifoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardFinalJudgInifoMapper.deleteByIds(ids);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/29 11:09
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public CreditCardFinalJudgInifo selectBySerno(CreditCardFinalJudgInifo creditCardFinalJudgInifo) {
        Map param = new HashMap();
        param.put("serno",creditCardFinalJudgInifo.getSerno());
        param.put("finalPostFlag",creditCardFinalJudgInifo.getFinalPostFlag());
        return creditCardFinalJudgInifoMapper.selectBySerno(param);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/29 11:09
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public List<CreditCardFinalJudgInifo> selectByIqpSerno(String serno) {
        Map param = new HashMap();
        param.put("serno",serno);
        return creditCardFinalJudgInifoMapper.selectByIqpSerno(param);
    }


}
