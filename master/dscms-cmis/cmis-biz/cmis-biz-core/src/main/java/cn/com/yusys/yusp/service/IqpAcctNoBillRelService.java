/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpAcctNoBillRel;
import cn.com.yusys.yusp.repository.mapper.IqpAcctNoBillRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctNoBillRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-25 17:45:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpAcctNoBillRelService {

    @Autowired
    private IqpAcctNoBillRelMapper iqpAcctNoBillRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpAcctNoBillRel selectByPrimaryKey(String pkId) {
        return iqpAcctNoBillRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAcctNoBillRel> selectAll(QueryModel model) {
        List<IqpAcctNoBillRel> records = (List<IqpAcctNoBillRel>) iqpAcctNoBillRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpAcctNoBillRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAcctNoBillRel> list = iqpAcctNoBillRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpAcctNoBillRel record) {
        return iqpAcctNoBillRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpAcctNoBillRel record) {
        return iqpAcctNoBillRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpAcctNoBillRel record) {
        return iqpAcctNoBillRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpAcctNoBillRel record) {
        return iqpAcctNoBillRelMapper.updateByPrimaryKeySelective(record);
    }
    /**
     * @方法名称: updateAcctStatusByBillNoSelective
     * @方法描述: 根据借据号更新 - 只更新 状态 字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateAcctStatusByBillNoSelective(IqpAcctNoBillRel record) {
        return iqpAcctNoBillRelMapper.updateAcctStatusByBillNoSelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAcctNoBillRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAcctNoBillRelMapper.deleteByIds(ids);
    }

    /**
     * 批量添加数据
     * @param iqpAcctNoBillRelList
     * @return
     */
    public int insertForeach(List<IqpAcctNoBillRel> iqpAcctNoBillRelList){
        return iqpAcctNoBillRelMapper.insertForeach(iqpAcctNoBillRelList);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpAcctNoBillRelMapper.updateByParams(delMap);
    }
}
