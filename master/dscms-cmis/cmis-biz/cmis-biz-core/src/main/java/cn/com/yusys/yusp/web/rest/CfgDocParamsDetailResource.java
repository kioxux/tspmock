/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CfgDocParamsList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgDocParamsDetail;
import cn.com.yusys.yusp.service.CfgDocParamsDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgdocparamsdetail")
public class CfgDocParamsDetailResource {
    @Autowired
    private CfgDocParamsDetailService cfgDocParamsDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgDocParamsDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgDocParamsDetail> list = cfgDocParamsDetailService.selectAll(queryModel);
        return new ResultDto<List<CfgDocParamsDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgDocParamsDetail>> index(QueryModel queryModel) {
        List<CfgDocParamsDetail> list = cfgDocParamsDetailService.selectByModel(queryModel);
        return new ResultDto<List<CfgDocParamsDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cdpdSerno}")
    protected ResultDto<CfgDocParamsDetail> show(@PathVariable("cdpdSerno") String cdpdSerno) {
        CfgDocParamsDetail cfgDocParamsDetail = cfgDocParamsDetailService.selectByPrimaryKey(cdpdSerno);
        return new ResultDto<CfgDocParamsDetail>(cfgDocParamsDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgDocParamsDetail> create(@RequestBody CfgDocParamsDetail cfgDocParamsDetail) throws URISyntaxException {
        cfgDocParamsDetailService.insert(cfgDocParamsDetail);
        return new ResultDto<CfgDocParamsDetail>(cfgDocParamsDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgDocParamsDetail cfgDocParamsDetail) throws URISyntaxException {
        int result = cfgDocParamsDetailService.update(cfgDocParamsDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cdpdSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cdpdSerno") String cdpdSerno) {
        int result = cfgDocParamsDetailService.deleteByPrimaryKey(cdpdSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgDocParamsDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectConditonByCdplSerno
     * @函数描述:根据参数配置流水查询资料清单信息
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectConditonByCdplSerno")
    protected ResultDto<List<CfgDocParamsDetail>> selectConditonByCdplSerno(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort("sort+0 asc,doc_type_data desc");
        }
        List<CfgDocParamsDetail> list = cfgDocParamsDetailService.selectByModel(queryModel);
        return new ResultDto<List<CfgDocParamsDetail>>(list);
    }

    /**
     * @函数名称:deleteByIds
     * @函数描述:批量对象删除
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    @PostMapping("/deleteByIds")
    protected ResultDto<Integer> deleteByIds(@RequestBody String ids) {
        int result = cfgDocParamsDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:selectConditonByCdplSernoSecond
     * @函数描述:根据参数配置流水查询资料清单信息(查看页面)
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectConditonByCdplSernoSecond")
    protected ResultDto<List<CfgDocParamsDetail>> selectConditonByCdplSernoSecond(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort("sort+0 asc,doc_type_data desc");
        }
        List<CfgDocParamsDetail> list = cfgDocParamsDetailService.selectByModelSecond(queryModel);
        return new ResultDto<List<CfgDocParamsDetail>>(list);
    }
}
