/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAcctChgCont;
import cn.com.yusys.yusp.service.IqpAcctChgContService;
import cn.com.yusys.yusp.service.IqpRepayWayChgService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctChgContResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-14 09:16:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpacctchgcont")
public class IqpAcctChgContResource {
    @Autowired
    private IqpAcctChgContService iqpAcctChgContService;
    private static final Logger log = LoggerFactory.getLogger(IqpRepayWayChgService.class);

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAcctChgCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAcctChgCont> list = iqpAcctChgContService.selectAll(queryModel);
        return new ResultDto<List<IqpAcctChgCont>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAcctChgCont>> index(QueryModel queryModel) {
        List<IqpAcctChgCont> list = iqpAcctChgContService.selectByModel(queryModel);
        return new ResultDto<List<IqpAcctChgCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpAcctChgCont> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpAcctChgCont iqpAcctChgCont = iqpAcctChgContService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpAcctChgCont>(iqpAcctChgCont);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAcctChgCont> create(@RequestBody IqpAcctChgCont iqpAcctChgCont) throws URISyntaxException {
        iqpAcctChgContService.insert(iqpAcctChgCont);
        return new ResultDto<IqpAcctChgCont>(iqpAcctChgCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAcctChgCont iqpAcctChgCont) throws URISyntaxException {
        int result = iqpAcctChgContService.update(iqpAcctChgCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpAcctChgContService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAcctChgContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:新增合同帐号变更申请数据
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addIqpAcctChgCont")
    protected ResultDto<IqpAcctChgCont> addIqpAcctChgCont(@RequestBody IqpAcctChgCont iqpAcctChgCont) throws URISyntaxException {
        log.info("新增合同帐号变更申请表数据【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(iqpAcctChgCont));
        iqpAcctChgContService.insert(iqpAcctChgCont);
        return new ResultDto<IqpAcctChgCont>(iqpAcctChgCont);
    }

    /**
     * @函数名称:修改合同帐号变更申请数据
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateIqpAcctChgCont")
    protected ResultDto<Map> updateIqpAcctChgCont(@RequestBody IqpAcctChgCont iqpAcctChgCont) throws URISyntaxException {
        log.info("修改合同帐号变更申请表数据【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(iqpAcctChgCont));
        return new ResultDto<Map>(iqpAcctChgContService.updateIqpAcctChgCont(iqpAcctChgCont));
    }

    /**
     * @函数名称:检查是否存在在途的合同帐号变更申请
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkIsExisitsIqpAcctChgCont")
    protected boolean checkIsExisitsOrderIntransit(@RequestBody IqpAcctChgCont iqpAcctChgCont ) throws URISyntaxException{
        boolean flag = false;
        log.info("新增合同帐号变更申请表数据【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(iqpAcctChgCont));
        String cont_no ="";
        return iqpAcctChgContService.queryIsExisitsOrderIntransit(cont_no);
    }

    @PostMapping("/checkExistIqpAcctChg")
    protected  ResultDto checkExistIqpAcctChg(@RequestBody IqpAcctChgCont iqpAcctChgCont)throws URISyntaxException{
        log.info("是否存在已进入流程的账号变更信息【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(iqpAcctChgCont.getContNo()));
        return new ResultDto<Boolean>(iqpAcctChgContService.checkExistIqpAcctChg(iqpAcctChgCont));
    }

    /**
     * 检查是否存在 合同账号变更 账号
     * @param contNo
     * @param acctNo
     * @return
     */
    @GetMapping("/{contNo}/{acctNo}")
    protected ResultDto<Integer> checkAcctUpdApp(@PathVariable("contNo") String contNo,
                                                 @PathVariable("acctNo") String acctNo){
        log.info("检查是否存在 合同账号变更 账号  的在途记录【{"+this.getClass().getName().toString()+"}】", contNo);
        return new ResultDto<Integer>(iqpAcctChgContService.checkAcctUpdApp(contNo,acctNo));



    }

    /*@PostMapping("/updateApproveStatus/{iqpSerno}")
    protected  ResultDto<Integer> updateApproveStatus(@PathVariable("iqpSerno") String iqp_serno){
        Map map = new HashMap();
        map.put("iqp_serno",iqp_serno);
        map.put("approve_status","992");
        return new ResultDto<Integer>(iqpAcctChgContService.updateApproveStatus(map));
    }*/

    @PostMapping("/insertIqpAcctChgCont")
    protected  ResultDto insertIqpAcctChgCont(@RequestBody IqpAcctChgCont iqpAcctChgCont)throws URISyntaxException{
        log.info("新增合同帐号变更申请表数据【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(iqpAcctChgCont.getContNo()));
        return new ResultDto<Boolean>(iqpAcctChgContService.insertIqpAcctChgCont(iqpAcctChgCont));
    }

}
