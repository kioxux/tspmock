package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.ypxt.buscon.BusconService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param
 * @return
 * @author shenli
 * @date 2021-6-7 20:45:14
 * @version 1.0.0
 * @desc 零售空白合同签订审核流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class LSYW02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(LSYW02BizService.class);
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private GuarBusinessRelService guarBusinessRelService;
    @Autowired
    private GuarBizRelService guarBizRelService;
    @Autowired
    private BusconService busconService;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private  CentralFileTaskService centralFileTaskService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private BizCommonService bizCommonService;
    @Autowired
    private GrtGuarContService grtGuarContService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if("LS004".equals(bizType)){

            handlerLS004(resultInstanceDto);

        }
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }


    //零售空白合同签订
    private void handlerLS004(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                createCentralFileTask(resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno);
                sendImage(resultInstanceDto);
                sendMessage(resultInstanceDto,"通过");
                ctrLoanContService.changelmt(extSerno);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.LSYW02.equals(flowCode);
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-10 15:07:49
     * @注释 审批状态更新
     */
    public void updateStatus(String serno,String state){
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
        ctrLoanCont.setApproveStatus(state);
        ctrLoanContService.updateSelective(ctrLoanCont);
    }
    /**
     * @创建人 shenli
     * @创建时间 2021-5-10 15:08:32
     * @注释 审批通过后，更新合同状态为“未生效”
     */
    public void end(String extSerno) {
        //审批通过的操作
        try {

            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(extSerno);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(ctrLoanCont.getIqpSerno());

            //1. 向额度系统发送接口：额度校验
            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0009ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0009ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
            cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
            cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
            cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
            cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
            cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
            cmisLmt0009ReqDto.setInputId(iqpLoanApp.getInputId());
            cmisLmt0009ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
            cmisLmt0009ReqDto.setInputDate(iqpLoanApp.getInputDate());
            //cmisLmt0009ReqDto.setOrigiDealBizNo(iqpLoanApp.getIqpSerno());//原交易业务编号
            //cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            //cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            //cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
            cmisLmt0009ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
            cmisLmt0009ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0009ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDto.setLmtType("01");//额度类型
            cmisLmt0009OccRelListReqDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
            cmisLmt0009ReqDto.setBussStageType("02");//合同阶段


            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
            ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());
            String code = cmisLmt0009RespDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-额度校验失败！", ctrLoanCont.getIqpSerno());
            }

            //1. 向额度系统发送接口：占用额度
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
            //cmisLmt0011ReqDto.setOrigiDealBizNo(ctrLoanCont.getIqpSerno());//原交易业务编号
            //cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            //cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            //cmisLmt0011ReqDto.setOrigiRecoverType("1");//原交易属性
            cmisLmt0011ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0011ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
            cmisLmt0011ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
            cmisLmt0011ReqDto.setBelgLine("04");//零售条线
            cmisLmt0011ReqDto.setBussStageType("02");//合同阶段


            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType("01");//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
            cmisLmt0011OccRelListDto.setBizTotalAmt(ctrLoanCont.getContAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmt(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(ctrLoanCont.getContAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011ReqDto.toString());
            ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011RespDto.toString());

            code = cmisLmt0011RespDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-额度占用失败！", ctrLoanCont.getIqpSerno());
            }

            //2. 额度占用成功后。更新合同状态为“已生效”
            ctrLoanCont.setSignDate(stringRedisTemplate.opsForValue().get("openDay"));
            ctrLoanCont.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            ctrLoanCont.setContStatus("200");//更新合同状为“已生效”
            ctrLoanCont.setPaperContSignDate(stringRedisTemplate.opsForValue().get("openDay"));
            ctrLoanContService.updateSelective(ctrLoanCont);

            //将关联的担保合同的合同状态更新为“已生效”
            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(ctrLoanCont.getIqpSerno());
            if(grtGuarContList == null || grtGuarContList.size() == 0){
                grtGuarContList = grtGuarContMapper.selectDataByContNo(ctrLoanCont.getContNo());
            }
            if (grtGuarContList != null && grtGuarContList.size() > 0) {
                for (int i = 0; i < grtGuarContList.size(); i++) {
                    GrtGuarCont grtGuarCont = grtGuarContList.get(i);
                    grtGuarCont.setGuarContState("101");//生效,码值没有确定最终版
                    grtGuarCont.setSignDate(stringRedisTemplate.opsForValue().get("openDay"));
                    if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                        log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtGuarCont);
                    Map<String,String> map = new HashMap<>();
                    map.put("bizType","02");
                    map.put("isFlag","1");
                    map.put("bizSerno",ctrLoanCont.getIqpSerno());
                    map.put("guarContNo",grtGuarCont.getGuarContNo());
                    guarBaseInfoService.buscon(map);
                }
            }

            if(!"00".equals(iqpLoanApp.getGuarWay())){

                try {
                    log.info("调用信贷业务合同信息同步开始");
                    guarBusinessRelService.sendBusinf("02","02",ctrLoanCont.getContNo());
                    log.info("调用信贷业务合同信息同步结束");
                }catch (Exception e){
                    log.info("调用信贷业务合同信息同步失败"+ e);
                }

                try {
                    log.info("调用根据业务流水号同步押品与业务关联信息开始");
                    // 请求对象信息
                    BusconReqDto busconReqDto = new BusconReqDto();
                    // 循环体
                    List list = new ArrayList();
                    // 根据业务流水号查询押品列表信息
                    QueryModel queryModel = new QueryModel();
                    queryModel.getCondition().put("serno", ctrLoanCont.getIqpSerno());
                    List<GuarBizRel> guarBizRelList = guarBizRelService.selectAll(queryModel);

                    if (CollectionUtils.isEmpty(guarBizRelList)) {
                        log.info("根据业务流水号【{}】未查询到押品信息，所属业务类型【{}】", ctrLoanCont.getIqpSerno(), "02");
                    }else{
                        // 拼装请求报文
                        guarBizRelList.stream().forEach(s -> {
                            // 单循环体
                            cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List busconList = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                            // 业务编号
                            busconList.setYwbhyp(ctrLoanCont.getContNo());
                            // 业务类型
                            busconList.setYwlxyp("02");
                            // 押品统一编号
                            busconList.setYptybh(s.getGuarNo());
                            // 是否有效 1-有效；0-无效
                            busconList.setIsflag("1");
                            list.add(busconList);
                        });
                        busconReqDto.setList(list);
                        // 请求押品系统
                        busconService.buscon(busconReqDto);
                    }
                    log.info("调用根据业务流水号同步押品与业务关联信息结束");
                }catch (Exception e){
                    log.info("调用根据业务流水号同步押品与业务关联信息失败"+ e);
                }


                if (grtGuarContList != null && grtGuarContList.size() > 0) {
                    for (int i = 0; i < grtGuarContList.size(); i++) {
                        GrtGuarCont grtGuarCont = grtGuarContList.get(i);

                        try {
                            log.info("调用信贷担保合同信息同步开始");
                            guarBusinessRelService.sendConinf("02",grtGuarCont.getGuarContNo());
                            log.info("调用信贷担保合同信息同步结束");
                        }catch (Exception e){
                            log.info("调用信贷担保合同信息同步失败"+ e);
                        }


                        try {
                            log.info("调用根据担保合同编号同步担保合同与押品关系开始");
                            guarBusinessRelService.sendContra(grtGuarCont.getGuarContNo());
                            log.info("调用根据担保合同编号同步担保合同与押品关系结束");
                        }catch (Exception e){
                            log.info("调用根据担保合同编号同步担保合同与押品关系失败"+ e);
                        }

                    }
                }
            }

        } catch (Exception e) {
            log.error("零售-合同申请签订异常：", e);
        }
    }



/* 空白合同模式；
    1、任务生成机构：本地支行 ；
    2、任务产生节点：申请流程提交后，当审批状态变成审批中；
    3、任务类型=派发、操作类型=非纯指令
    4、退回时：
    a.退回时，若打回人选择逐级提交，则在客户经理再次提交时生成“任务类型=暂存及派发，操作类型=非纯指令”任务，任务生成节点：流程提交后；
    b.若打回人选择点对点提交，则在客户经理再次提交时不生成新的档案池任务；
 */
    public void createCentralFileTask (ResultInstanceDto resultInstanceDto){
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(resultInstanceDto.getBizId());
        if("231_5".equals(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId())){
            try {
                log.info("零售空白合同签订审核流程："+"合同编号："+resultInstanceDto.getBizId()+"-发起临时档案开始");
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(ctrLoanCont.getInputBrId());
                String orgType = resultDto.getData().getOrgType();
                //        0-总行部室
                //        1-异地支行（有分行）
                //        2-异地支行（无分行）
                //        3-异地分行
                //        4-中心支行
                //        5-综合支行
                //        6-对公支行
                //        7-零售支行
                if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) &&!"8".equals(orgType) && !"9".equals(orgType) && !"A".equals(orgType)){

                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("instanceId",resultInstanceDto.getInstanceId());
                    queryModel.addCondition("bizType","LS004");
                    List<CentralFileTask> CentralFileTaskList =  centralFileTaskService.selectAll(queryModel);
                    String taskType = "";
                    if(CentralFileTaskList!=null && CentralFileTaskList.size() >0){//大于0,代表存打回
                        taskType = "04"; // 暂存及派发
                    }else{
                        taskType = "03"; // 档案派发
                    }
                    //新增临时档案任务
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(ctrLoanCont.getIqpSerno());
                    centralFileTaskdto.setTraceId(ctrLoanCont.getContNo());
                    centralFileTaskdto.setCusId(ctrLoanCont.getCusId());
                    centralFileTaskdto.setCusName(ctrLoanCont.getCusName());
                    centralFileTaskdto.setBizType("LS004"); // LS004 零售空白合同签订
                    centralFileTaskdto.setInputId(ctrLoanCont.getInputId());
                    centralFileTaskdto.setInputBrId(ctrLoanCont.getInputBrId());
                    centralFileTaskdto.setOptType("02"); // 非纯指令
                    centralFileTaskdto.setTaskType(taskType); // 任务类型
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                    log.info("零售空白合同签订审核流程："+"合同编号："+resultInstanceDto.getBizId()+"-发起临时档案成功");
                }
                log.info("零售空白合同签订审核流程："+"合同编号："+resultInstanceDto.getBizId()+"-发起临时档案结束");

            }catch (Exception e){
                log.info("零售空白合同签订审核流程："+"合同编号："+resultInstanceDto.getBizId()+"-归档异常------------------"+e);
            }
        }
    }

    /**
     * 发送短信
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void sendMessage(ResultInstanceDto resultInstanceDto,String result) {
        String contNo = resultInstanceDto.getBizId();
        log.info("零售空白合同签订审核流程："+"流水号："+contNo+"-发送短信开始------------------");
        CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
        String managerId = ctrLoanCont.getManagerId();
        try {
            String mgrTel = "";
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }

            if (!StringUtils.isBlank(mgrTel)) {
                String messageType = "MSG_LS_M_0001";// 短信编号
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", resultInstanceDto.getBizUserName());
                paramMap.put("flowName", "零售空白合同签订");
                paramMap.put("result", result);

                //执行发送借款人操作
                log.info("零售空白合同签订审核流程："+"流水号："+resultInstanceDto.getBizId()+"-发送短信------------------");
                ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);

            }
        } catch (Exception e) {
            log.info("零售空白合同签订审核流程："+"流水号："+contNo+"-发送短信失败------------------"+e);
        }
        log.info("零售空白合同签订审核流程："+"流水号："+contNo+"-发送短信结束------------------");
    }

    /**
     * 推送影像审批信息
     *
     * @author shenli
     * @date 2021-9-29 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();

            if ("231_7".equals(nodeId)){//集中作业零售合同审批岗
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }

        if(approveUserId.length() > 0){
            approveUserId = approveUserId.substring(1);
            String topOutSystemCodes = "GRXFDKCZJB;GRXFDKCZDY;GRXFDKCZZY;GRXFDKCZBZDB";
            bizCommonService.sendImage(resultInstanceDto.getBizId(),topOutSystemCodes,approveUserId);
        }
    }


}
