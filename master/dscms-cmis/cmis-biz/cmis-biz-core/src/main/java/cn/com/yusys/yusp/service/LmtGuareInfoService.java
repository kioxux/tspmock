/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtGuareCloestInfo;
import cn.com.yusys.yusp.domain.LmtGuareInfo;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-25 17:16:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGuareInfoService {

    @Autowired
    private LmtGuareInfoMapper lmtGuareInfoMapper;
    @Autowired
    private LmtGuareCloestInfoService lmtGuareCloestInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGuareInfo selectByPrimaryKey(String serno) {
        return lmtGuareInfoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtGuareInfo> selectAll(QueryModel model) {
        List<LmtGuareInfo> records = (List<LmtGuareInfo>) lmtGuareInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGuareInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGuareInfo> list = lmtGuareInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtGuareInfo record) {
        return lmtGuareInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtGuareInfo record) {
        return lmtGuareInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtGuareInfo record) {
        return lmtGuareInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtGuareInfo record) {
        return lmtGuareInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtGuareInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGuareInfoMapper.deleteByIds(ids);
    }

    /**
     * @return 新增 修改  抵押品信息逻辑
     * @创建人 wzy
     * @创建时间 15:57 2021-04-12
     **/
    @Transactional(rollbackFor = Exception.class)
    public int saveGuareInfo(LmtGuareInfo guareInfo) {
        //基本信息有无创建
        int result;
        String pkId = guareInfo.getPkId();
        if (StringUtils.isBlank(pkId)) {
            //无基本信息
            result = this.insertSelective(guareInfo);
        } else {
            result = this.updateSelective(guareInfo);
        }
        //企业信息有无创建
        return result;
    }

    /**
     * @param guareInfo
     * @return int
     * @author hubp
     * @date 2021/7/3 14:52
     * @version 1.0.0
     * @desc  PC端专用
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveGuareInfogu(LmtGuareInfo guareInfo) {
        //基本信息有无创建
        int result;
        String pkId = guareInfo.getPkId();
        if (!StringUtils.isBlank(pkId) && this.selectByPrimaryKey(pkId) == null) {
            //无基本信息
            result = this.insertSelective(guareInfo);
        } else {
            result = this.updateSelective(guareInfo);
        }
        //企业信息有无创建
        return result;
    }

    /**
     * @param pkId
     * @return int
     * @author hubp
     * @date 2021/6/15 19:36
     * @version 1.0.0
     * @desc 查找抵押物评估信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto selectGuar(String pkId) {
        // 通过pkI查找云评估编号
        LmtGuareInfo lmtGuareInfo = lmtGuareInfoMapper.selectByPrimaryKey(pkId);
        if (null == lmtGuareInfo || StringUtils.isBlank(lmtGuareInfo.getCloudEvalNo())) {
            return new ResultDto(null).code(9999).message("抵押物关键信息为空！请核查数据");
        }
        LmtGuareCloestInfo lmtGuareCloestInfo = lmtGuareCloestInfoService.selectByPrimaryKey(lmtGuareInfo.getCloudEvalNo());
        return new ResultDto(lmtGuareCloestInfo).code(0);
    }
}
