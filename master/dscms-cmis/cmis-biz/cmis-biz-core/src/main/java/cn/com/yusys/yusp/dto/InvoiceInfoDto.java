package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InvoiceInfo
 * @类描述: invoice_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 22:43:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class InvoiceInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 补录流水号 **/
	private String serno;
	
	/** 发票类型 **/
	private String billType;
	
	/** 购销合同编号 **/
	private String tContNo;
	
	/** 发票影像流水号 **/
	private String billImgId;
	
	/** 发票是否补录 **/
	private String isAddpReco;
	
	/** 开票日期 **/
	private String openDate;
	
	/** 票号 **/
	private String billNo;
	
	/** 购买方名称 **/
	private String buyBrId;
	
	/** 购买方纳税人识别号 **/
	private String buyTaxNo;
	
	/** 购买方地址 **/
	private String buyAddr;
	
	/** 购买方电话 **/
	private String buyPhone;
	
	/** 购买方开户行 **/
	private String buyBank;
	
	/** 购买方账号 **/
	private String buyAcc;
	
	/** 产品的名称 **/
	private String prdName;
	
	/** 规格型号 **/
	private String norms;
	
	/** 数量 **/
	private java.math.BigDecimal quant;
	
	/** 单价 **/
	private java.math.BigDecimal price;
	
	/** 金额 **/
	private java.math.BigDecimal amt;
	
	/** 税率 **/
	private java.math.BigDecimal taxRate;
	
	/** 税额 **/
	private java.math.BigDecimal taxAmt;
	
	/** 销售方名称 **/
	private String sellBrId;
	
	/** 销售纳税人识别号 **/
	private String sellTaxNo;
	
	/** 销售地址 **/
	private String sellAddr;
	
	/** 销售电话 **/
	private String sellPhone;
	
	/** 销售开户行 **/
	private String sellBank;
	
	/** 销售账号 **/
	private String sellAcc;
	
	/** 收款人 **/
	private String payee;
	
	/** 复核人 **/
	private String checkName;
	
	/** 开票人 **/
	private String openName;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billType
	 */
	public void setBillType(String billType) {
		this.billType = billType == null ? null : billType.trim();
	}
	
    /**
     * @return BillType
     */	
	public String getBillType() {
		return this.billType;
	}
	
	/**
	 * @param tContNo
	 */
	public void setTContNo(String tContNo) {
		this.tContNo = tContNo == null ? null : tContNo.trim();
	}
	
    /**
     * @return TContNo
     */	
	public String getTContNo() {
		return this.tContNo;
	}
	
	/**
	 * @param billImgId
	 */
	public void setBillImgId(String billImgId) {
		this.billImgId = billImgId == null ? null : billImgId.trim();
	}
	
    /**
     * @return BillImgId
     */	
	public String getBillImgId() {
		return this.billImgId;
	}
	
	/**
	 * @param isAddpReco
	 */
	public void setIsAddpReco(String isAddpReco) {
		this.isAddpReco = isAddpReco == null ? null : isAddpReco.trim();
	}
	
    /**
     * @return IsAddpReco
     */	
	public String getIsAddpReco() {
		return this.isAddpReco;
	}
	
	/**
	 * @param openDate
	 */
	public void setOpenDate(String openDate) {
		this.openDate = openDate == null ? null : openDate.trim();
	}
	
    /**
     * @return OpenDate
     */	
	public String getOpenDate() {
		return this.openDate;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param buyBrId
	 */
	public void setBuyBrId(String buyBrId) {
		this.buyBrId = buyBrId == null ? null : buyBrId.trim();
	}
	
    /**
     * @return BuyBrId
     */	
	public String getBuyBrId() {
		return this.buyBrId;
	}
	
	/**
	 * @param buyTaxNo
	 */
	public void setBuyTaxNo(String buyTaxNo) {
		this.buyTaxNo = buyTaxNo == null ? null : buyTaxNo.trim();
	}
	
    /**
     * @return BuyTaxNo
     */	
	public String getBuyTaxNo() {
		return this.buyTaxNo;
	}
	
	/**
	 * @param buyAddr
	 */
	public void setBuyAddr(String buyAddr) {
		this.buyAddr = buyAddr == null ? null : buyAddr.trim();
	}
	
    /**
     * @return BuyAddr
     */	
	public String getBuyAddr() {
		return this.buyAddr;
	}
	
	/**
	 * @param buyPhone
	 */
	public void setBuyPhone(String buyPhone) {
		this.buyPhone = buyPhone == null ? null : buyPhone.trim();
	}
	
    /**
     * @return BuyPhone
     */	
	public String getBuyPhone() {
		return this.buyPhone;
	}
	
	/**
	 * @param buyBank
	 */
	public void setBuyBank(String buyBank) {
		this.buyBank = buyBank == null ? null : buyBank.trim();
	}
	
    /**
     * @return BuyBank
     */	
	public String getBuyBank() {
		return this.buyBank;
	}
	
	/**
	 * @param buyAcc
	 */
	public void setBuyAcc(String buyAcc) {
		this.buyAcc = buyAcc == null ? null : buyAcc.trim();
	}
	
    /**
     * @return BuyAcc
     */	
	public String getBuyAcc() {
		return this.buyAcc;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param norms
	 */
	public void setNorms(String norms) {
		this.norms = norms == null ? null : norms.trim();
	}
	
    /**
     * @return Norms
     */	
	public String getNorms() {
		return this.norms;
	}
	
	/**
	 * @param quant
	 */
	public void setQuant(java.math.BigDecimal quant) {
		this.quant = quant;
	}
	
    /**
     * @return Quant
     */	
	public java.math.BigDecimal getQuant() {
		return this.quant;
	}
	
	/**
	 * @param price
	 */
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}
	
    /**
     * @return Price
     */	
	public java.math.BigDecimal getPrice() {
		return this.price;
	}
	
	/**
	 * @param amt
	 */
	public void setAmt(java.math.BigDecimal amt) {
		this.amt = amt;
	}
	
    /**
     * @return Amt
     */	
	public java.math.BigDecimal getAmt() {
		return this.amt;
	}
	
	/**
	 * @param taxRate
	 */
	public void setTaxRate(java.math.BigDecimal taxRate) {
		this.taxRate = taxRate;
	}
	
    /**
     * @return TaxRate
     */	
	public java.math.BigDecimal getTaxRate() {
		return this.taxRate;
	}
	
	/**
	 * @param taxAmt
	 */
	public void setTaxAmt(java.math.BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	
    /**
     * @return TaxAmt
     */	
	public java.math.BigDecimal getTaxAmt() {
		return this.taxAmt;
	}
	
	/**
	 * @param sellBrId
	 */
	public void setSellBrId(String sellBrId) {
		this.sellBrId = sellBrId == null ? null : sellBrId.trim();
	}
	
    /**
     * @return SellBrId
     */	
	public String getSellBrId() {
		return this.sellBrId;
	}
	
	/**
	 * @param sellTaxNo
	 */
	public void setSellTaxNo(String sellTaxNo) {
		this.sellTaxNo = sellTaxNo == null ? null : sellTaxNo.trim();
	}
	
    /**
     * @return SellTaxNo
     */	
	public String getSellTaxNo() {
		return this.sellTaxNo;
	}
	
	/**
	 * @param sellAddr
	 */
	public void setSellAddr(String sellAddr) {
		this.sellAddr = sellAddr == null ? null : sellAddr.trim();
	}
	
    /**
     * @return SellAddr
     */	
	public String getSellAddr() {
		return this.sellAddr;
	}
	
	/**
	 * @param sellPhone
	 */
	public void setSellPhone(String sellPhone) {
		this.sellPhone = sellPhone == null ? null : sellPhone.trim();
	}
	
    /**
     * @return SellPhone
     */	
	public String getSellPhone() {
		return this.sellPhone;
	}
	
	/**
	 * @param sellBank
	 */
	public void setSellBank(String sellBank) {
		this.sellBank = sellBank == null ? null : sellBank.trim();
	}
	
    /**
     * @return SellBank
     */	
	public String getSellBank() {
		return this.sellBank;
	}
	
	/**
	 * @param sellAcc
	 */
	public void setSellAcc(String sellAcc) {
		this.sellAcc = sellAcc == null ? null : sellAcc.trim();
	}
	
    /**
     * @return SellAcc
     */	
	public String getSellAcc() {
		return this.sellAcc;
	}
	
	/**
	 * @param payee
	 */
	public void setPayee(String payee) {
		this.payee = payee == null ? null : payee.trim();
	}
	
    /**
     * @return Payee
     */	
	public String getPayee() {
		return this.payee;
	}
	
	/**
	 * @param checkName
	 */
	public void setCheckName(String checkName) {
		this.checkName = checkName == null ? null : checkName.trim();
	}
	
    /**
     * @return CheckName
     */	
	public String getCheckName() {
		return this.checkName;
	}
	
	/**
	 * @param openName
	 */
	public void setOpenName(String openName) {
		this.openName = openName == null ? null : openName.trim();
	}
	
    /**
     * @return OpenName
     */	
	public String getOpenName() {
		return this.openName;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}