package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据修改统计分类或科目投向审批流程（东海）
 *
 * @author macm
 * @version 1.0
 */
@Service
public class DHCZ42BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ42BizService.class);

    @Autowired
    private XTGL03BizService xTGL03BizService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        // DHH19 数据修改-统计分类（东海） DHH20数据修改-科目投向（东海）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_DHH19.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_DHH20.equals(bizType)) {
            xTGL03BizService.iqpDataModify(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.DHCZ42.equals(flowCode);
    }
}
