/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtAppSub;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-07 19:49:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtAppSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtAppSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtAppSub> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtAppSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtAppSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtAppSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtAppSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPkId(LmtAppSub lmtAppSub);

    /**
     * 通过分项流水号查询授信分项信息
     *
     * @param
     * @return
     */
    LmtAppSub selectBySubSerno(String subSerno);

    /**
     * 通过分项流水号查询省心快贷授信分项信息
     *
     * @param
     * @return
     */
    List<LmtAppSub> selectSxkdLmtAppSubBySubSerno(String subSerno);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据授信申请流水号逻辑删除授信分项
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据授信申请流水号查询对应所有授信分项
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtAppSub> selectByParams(Map params);

    /**
     * @方法名称: insertList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertLmtAppSubList(@Param("list")List<LmtAppSub>  list);

    /**
     * @函数名称:deleteBySubSerno
     * @函数描述:根据授信分项流水号关联删除授信分项
     * @参数与返回说明:subSerno 授信分项流水号
     * @算法描述:
     */
    int deleteBySubSerno(String subSerno);

    /**
     * 获取一般额度分项信息
     * @param serno
     * @return
     */
    List<LmtAppSub> getNormal(String serno);

    /**
     * 获取低风险额度分项信息
     * @param serno
     * @return
     */
    List<LmtAppSub> getLow(String serno);

    /**
     * @函数名称:queryLmtAppSubByGrpSerno
     * @函数描述:根据集团授信申请流水号查询项下的分项信息
     * @参数与返回说明:
     * @算法描述:
     */

    List<LmtAppSub> queryLmtAppSubByGrpSerno(String grpSerno);

    /**
     * @函数名称:selectSumLmtAmtFromLmtAppSub
     * @函数描述:根据申请流水获取分项金额的总额
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    BigDecimal selectSumLmtAmtFromLmtAppSub(String serno);

    /**
     * 根据授信流水查询分项流水号
     * @param serno
     * @return
     */
    List<String> getSubSernoBySerno(String serno);

}