/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDelayAgreement;
import cn.com.yusys.yusp.repository.mapper.IqpDelayAgreementMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDelayAgreementService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 15:07:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpDelayAgreementService {

    @Autowired
    private IqpDelayAgreementMapper iqpDelayAgreementMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpDelayAgreement selectByPrimaryKey(String agrSerno) {
        return iqpDelayAgreementMapper.selectByPrimaryKey(agrSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDelayAgreement> selectAll(QueryModel model) {
        List<IqpDelayAgreement> records = (List<IqpDelayAgreement>) iqpDelayAgreementMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpDelayAgreement> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDelayAgreement> list = iqpDelayAgreementMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpDelayAgreement record) {
        return iqpDelayAgreementMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpDelayAgreement record) {
        record.setInputDate(DateUtils.getCurrDateStr());
        record.setCreateTime(DateUtils.getCurrTimestamp());
        record.setUpdateTime(record.getCreateTime());
        record.setUpdDate(record.getInputDate());
        return iqpDelayAgreementMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpDelayAgreement record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpDelayAgreementMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpDelayAgreement record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpDelayAgreementMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String agrSerno) {
        return iqpDelayAgreementMapper.deleteByPrimaryKey(agrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDelayAgreementMapper.deleteByIds(ids);
    }
}
