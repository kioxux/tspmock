package cn.com.yusys.yusp.web.server.xddh0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0001.Xddh0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 * 接口处理类:查询贷后任务是否完成现场检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0001:查询贷后任务是否完成现场检查")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0001Resource.class);

    @Autowired
    private Xddh0001Service xddh0001Service;
    /**
     * 交易码：xddh0001
     * 交易描述：查询贷后任务是否完成现场检查
     *
     * @param xddh0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询贷后任务是否完成现场检查")
    @PostMapping("/xddh0001")
    protected @ResponseBody
    ResultDto<Xddh0001DataRespDto> xddh0001(@Validated @RequestBody Xddh0001DataReqDto xddh0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001DataReqDto));
        // 响应Dto:查询贷后任务是否完成现场检查
        Xddh0001DataRespDto xddh0001DataRespDto = new Xddh0001DataRespDto();
        ResultDto<Xddh0001DataRespDto> xddh0001DataResultDto = new ResultDto<>();
        //任务编号
        String serno = xddh0001DataReqDto.getSerno();
        //现场检查时间 2020-06-22
        String iotspotTime =xddh0001DataReqDto.getIotspotTime();
        //状态
        String status = xddh0001DataReqDto.getStatus();
        try {
            //1.必输参数校验
            if (StringUtils.isAnyBlank(serno,iotspotTime,status)) {
                xddh0001DataResultDto.setData(xddh0001DataRespDto);
                xddh0001DataResultDto.setCode(EpbEnum.EPB090009.key);
                xddh0001DataResultDto.setMessage(EpbEnum.EPB090009.value);
                return xddh0001DataResultDto;
            }
            // 从xddh0001DataReqDto获取业务值进行业务逻辑处理
            xddh0001DataRespDto = xddh0001Service.xddh0001(xddh0001DataReqDto);
            //如果查询结果为空，说明传入的参数有问题
            if (StringUtils.isBlank(xddh0001DataReqDto.getSerno())) {
                return returnError(xddh0001DataResultDto,xddh0001DataRespDto);
            }
            // 封装xddh0001DataResultDto中正确的返回码和返回信息
            xddh0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, e.getMessage());
            // 封装xddh0001DataResultDto中异常返回码和返回信息
            xddh0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0001DataRespDto到xddh0001DataResultDto中
        xddh0001DataResultDto.setData(xddh0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001DataResultDto));
        return xddh0001DataResultDto;
    }

    /**
     * @Description:返回参数异常
     * @Author: YX-WJ
     * @Date: 2021/6/8 20:32

     * @return: void
     **/
    protected ResultDto<Xddh0001DataRespDto> returnError(ResultDto<Xddh0001DataRespDto> xddh0001DataResultDto,Xddh0001DataRespDto xddh0001DataRespDto){
        xddh0001DataResultDto.setCode(EpbEnum.EPB090004.key);
        xddh0001DataResultDto.setMessage("传入参数异常，业务执行失败");
        xddh0001DataResultDto.setData(xddh0001DataRespDto);
        return xddh0001DataResultDto;
    }
}
