package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.LmtSigInvestAcc;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.domain.LmtSigInvestAppr;
import cn.com.yusys.yusp.domain.LmtSigInvestRst;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005LmtDetailsListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.req.CmisLmt0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0005.resp.CmisLmt0005RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestAccMapper;
import cn.com.yusys.yusp.util.Comm4CalFormulaUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestAccService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-26 18:27:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestAccService extends BizInvestCommonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LmtSigInvestApprService.class);

    @Autowired
    private LmtSigInvestAccMapper lmtSigInvestAccMapper;

    @Autowired
    private LmtSigInvestBasicInfoSubAccService lmtSigInvestBasicInfoSubAccService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Comm4CalFormulaUtils comm4CalFormulaUtils ;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private LmtSigInvestRstService lmtSigInvestRstService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestAcc selectByPrimaryKey(String pkId) {
        return lmtSigInvestAccMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSigInvestAcc> selectAll(QueryModel model) {
        List<LmtSigInvestAcc> records = (List<LmtSigInvestAcc>) lmtSigInvestAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSigInvestAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestAcc> list = lmtSigInvestAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSigInvestAcc record) {
        return lmtSigInvestAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSigInvestAcc record) {
        return lmtSigInvestAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSigInvestAcc record) {
        return lmtSigInvestAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSigInvestAcc record) {
        return lmtSigInvestAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSigInvestAccMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号获取台账记录(有效)
     *
     * @param serno
     * @return
     */
    public LmtSigInvestAcc selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtSigInvestAcc> lmtSigInvestAccs = selectByModel(queryModel);
        if (lmtSigInvestAccs != null && lmtSigInvestAccs.size() > 0) {
            return lmtSigInvestAccs.get(0);
        }
        return null;
    }

    /**
     * 根据批复流水号获取台账记录(有效)
     *
     * @param replySerno
     * @return
     */
    public LmtSigInvestAcc selectByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno", replySerno);
        List<LmtSigInvestAcc> lmtSigInvestAccs = selectByModel(queryModel);
        if (lmtSigInvestAccs != null && lmtSigInvestAccs.size() > 0) {
            return lmtSigInvestAccs.get(0);
        }
        return null;
    }

    /**
     * 根据批复台账编号获取台账记录(有效)
     * @param accNo
     * @return
     */
    public LmtSigInvestAcc selectByAccNo(String accNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("accNo", accNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

        if(StringUtils.isBlank(accNo)){
            throw new BizException(null,"",null,"台账编号不允许为空");
        }

        List<LmtSigInvestAcc> lmtSigInvestAccs = selectByModel(queryModel);
        if (lmtSigInvestAccs != null && lmtSigInvestAccs.size() > 0) {
            return lmtSigInvestAccs.get(0);
        }
        return null;
    }

    /**
     * @方法名称: initLmtSigInvestAccInfo
     * @方法描述: 初始化台账主表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestAcc initLmtSigInvestAccInfo(LmtSigInvestAppr entityBean) {
        LmtSigInvestAcc lmtSigInvestAcc = new LmtSigInvestAcc();
        //如果是授信变更
        //拷贝数据
        BeanUtils.copyProperties(entityBean, lmtSigInvestAcc);
        //到日期处理
        int term =  entityBean.getLmtTerm();

        // //起始日期 采用OpenDay
        String startDate = getCurrrentDateStr();

        lmtSigInvestAcc.setEndDate(DateUtils.addMonth(startDate, DateFormatEnum.DEFAULT.getValue(), term));
        //主键
        lmtSigInvestAcc.setPkId(generatePkId());
        //批复编号
        lmtSigInvestAcc.setAccNo(generateSerno(SeqConstant.INVEST_LMT_ACC_SEQ));
        //终身机构
        lmtSigInvestAcc.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //批复状态
        lmtSigInvestAcc.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);

        String apprResult = Optional.ofNullable(entityBean.getReviewResult()).orElse(entityBean.getReviewResultZh());
        //评审结论
        lmtSigInvestAcc.setApprResult(apprResult);
        //最新更新日期
        lmtSigInvestAcc.setUpdDate(getCurrrentDateStr());
        //创建日期
        lmtSigInvestAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //到期日
        String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd", lmtSigInvestAcc.getLmtTerm());
        //起始日期
        lmtSigInvestAcc.setStartDate(startDate);
        //到期日
        lmtSigInvestAcc.setEndDate(endDate);
        return lmtSigInvestAcc;
    }

    /**
     * @方法名称: initLmtSigInvestAccInfo
     * @方法描述: 初始化台账主表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestAcc updaetLmtSigInvestAccInfo(LmtSigInvestAppr entityBean, String replySerno, String appType) {
        //根据原批复编号，查询批复信息
        //根据 客户号、授信品种、客户大类 获取唯一有效台账记录  --- add by lixy 2021年9月8日
        LmtSigInvestAcc lmtSigInvestAcc = selectByReplySerno(replySerno);
        //获取原批复编号主键信息
        String pkId = lmtSigInvestAcc.getPkId();
        //原批复编号台账编号
        String accNo = lmtSigInvestAcc.getAccNo();
        //原批复生效日期(变更 startDate是从原授信台账、批复中获取)
        String origiStartDate = lmtSigInvestAcc.getStartDate();
        //如果是授信变更
        User user = SessionUtils.getUserInformation();
        //拷贝数据
        BeanUtils.copyProperties(entityBean, lmtSigInvestAcc);

        //到日期处理
        int term = entityBean.getLmtTerm();
        //授信变更 结束时间 使用 原批复生效日期+授信期限 计算
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType)){
            lmtSigInvestAcc.setEndDate(DateUtils.addMonth(origiStartDate, DateFormatEnum.DEFAULT.getValue(), term));
        }else{
            lmtSigInvestAcc.setEndDate(DateUtils.addMonth(getCurrrentDateStr(), DateFormatEnum.DEFAULT.getValue(), term));
        }
        //主键
        lmtSigInvestAcc.setPkId(pkId);
        //批复编号
        lmtSigInvestAcc.setAccNo(accNo);
        //终身机构
        lmtSigInvestAcc.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
/*        //审批模式 TODO：不知道怎么取
        lmtSigInvestAcc.setApprMode("");
        //审批结论 TODO：不知道怎么取
        lmtSigInvestAcc.setApprResult("");*/

        String apprResult = Optional.ofNullable(entityBean.getReviewResult()).orElse(entityBean.getReviewResultZh());
        //审批结论
        lmtSigInvestAcc.setApprResult(apprResult);

        //批复状态
        lmtSigInvestAcc.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //最新更新日期
        lmtSigInvestAcc.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
       /* //创建日期
        lmtSigInvestAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));*/
        //更新日期
        lmtSigInvestAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //起始日期 授信变更采用原申请的开始日期
        if (CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType)){
            //起始日期
            lmtSigInvestAcc.setStartDate(origiStartDate);
        }else{
            //起始日期
            lmtSigInvestAcc.setStartDate(getCurrrentDateStr());
        }
        return lmtSigInvestAcc;
    }

    /**
     * 单笔投资额度审批通过后通知额度系统
     *
     * @param serno
     */
    public void lmtSigInvestSendCmisLmt0005(String serno, String lmtType, String origiAccNo) throws Exception {

        //发送额度系统，接口额度同步操作
        CmisLmt0005ReqDto cmisLmt0005ReqDto = new CmisLmt0005ReqDto();
        cmisLmt0005ReqDto.setSysId(CmisCommonConstants.STD_PERIPHERAL_SYS_XDG);

        LmtSigInvestAcc lmtSigInvestAcc = selectBySerno(serno);

        cmisLmt0005ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
        cmisLmt0005ReqDto.setCusId(lmtSigInvestAcc.getCusId());
        cmisLmt0005ReqDto.setCusName(lmtSigInvestAcc.getCusName());
        //额度类型 如果客户大类是2--对公，则额度类型是05--主体授信，否则是04--产品授信
        String lmtBizType = lmtSigInvestAcc.getLmtBizType();

        if(CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_16010101.equals(lmtBizType) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_16020101.equals(lmtBizType)){
            cmisLmt0005ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_08);//承销额度
        }else{
            if (CmisLmtConstants.STD_ZB_CUS_CATALOG2.equals(lmtSigInvestAcc.getCusCatalog())) {
                cmisLmt0005ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_05);
            } else {
                cmisLmt0005ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_04);
            }
        }

        if (CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(lmtType) && StringUtils.nonBlank(origiAccNo)) {
            //授信复议，授信续作需要传isCreateAcc为1--是，以及原批复台账编号
            cmisLmt0005ReqDto.setOrigiAccNo(origiAccNo);
            cmisLmt0005ReqDto.setIsCreateAcc(CmisLmtConstants.YES_NO_Y);
        }

        //投资类型 债券投资 （必输）
        cmisLmt0005ReqDto.setInvestType(lmtSigInvestAcc.getInvestType());
        cmisLmt0005ReqDto.setAccNo(lmtSigInvestAcc.getAccNo());
        cmisLmt0005ReqDto.setProNo(lmtSigInvestAcc.getProNo());
        cmisLmt0005ReqDto.setLmtAmt(lmtSigInvestAcc.getLmtAmt());
        cmisLmt0005ReqDto.setSobsAmt(lmtSigInvestAcc.getSobsAmt());
        cmisLmt0005ReqDto.setAssetManaAmt(lmtSigInvestAcc.getAssetManaAmt());
        cmisLmt0005ReqDto.setProName(lmtSigInvestAcc.getProName());
        cmisLmt0005ReqDto.setAssetNo(lmtSigInvestAcc.getAssetNo());
        cmisLmt0005ReqDto.setLimitSubNo(lmtSigInvestAcc.getLmtBizType());
        cmisLmt0005ReqDto.setLimitSubName(lmtSigInvestAcc.getLmtBizTypeName());
        cmisLmt0005ReqDto.setTerm(lmtSigInvestAcc.getLmtTerm());
        cmisLmt0005ReqDto.setStartDate(lmtSigInvestAcc.getStartDate());
        cmisLmt0005ReqDto.setEndDate(lmtSigInvestAcc.getEndDate());
        cmisLmt0005ReqDto.setIsRevolv(lmtSigInvestAcc.getIsRevolv());
        cmisLmt0005ReqDto.setCurType(lmtSigInvestAcc.getCurType());
        //额度状态 默认 01--生效
        cmisLmt0005ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        cmisLmt0005ReqDto.setManagerId(lmtSigInvestAcc.getManagerId());
        cmisLmt0005ReqDto.setManagerBrId(lmtSigInvestAcc.getManagerBrId());
        cmisLmt0005ReqDto.setInputId(lmtSigInvestAcc.getInputId());
        cmisLmt0005ReqDto.setInputBrId(lmtSigInvestAcc.getInputBrId());
        cmisLmt0005ReqDto.setInputDate(lmtSigInvestAcc.getInputDate());

        List<CmisLmt0005LmtDetailsListReqDto> lmtDetailsList = new ArrayList<>();

        List<Map<String, Object>> lmtSigInvestBasicInfoSubAccList = lmtSigInvestBasicInfoSubAccService.selectBySerno(serno);

        if (lmtSigInvestBasicInfoSubAccList != null && lmtSigInvestBasicInfoSubAccList.size() > 0) {
            for (Map<String, Object> subAcc : lmtSigInvestBasicInfoSubAccList) {
                CmisLmt0005LmtDetailsListReqDto lmtDetail = new CmisLmt0005LmtDetailsListReqDto();
                //底层融资人客户编号
                lmtDetail.setBasicCusId((String) subAcc.get("basicCusId"));
                //底层融资人客户名称
                lmtDetail.setBasicCusName((String) subAcc.get("basicCusName"));
                //是否生成穿透化额度取是否申报底层授信
                String isAppBasicLmt = (String) subAcc.get("isAppBasicLmt");
                lmtDetail.setIsCreateLmt(isAppBasicLmt);
                //底层融资人客户类型
                lmtDetail.setbasicCusCatalog((String) subAcc.get("basicCusCatalog"));

                /**add by zhangjw 20210708
                 * 若申报底层授信，则底层融资人批复台账编号 为 生成的底层授信台账号，   底层融资人批复台账编号 为 生成的底层授信台账号
                 * 若不申报底层授信，则底层融资人批复台账编号 为空， 底层融资人批复分项编号 为 占用底层授信分项编号USE_BASIC_LMT_SUB_SERNO
                 */
                if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(isAppBasicLmt)) {
                    //底层融资人批复台账编号  生成穿透化额度时有值
                    lmtDetail.setBasicAccNo((String) subAcc.get("basicAccNo"));
                    //底层融资人批复分项编号  取底层融资人批复台账编号
                    lmtDetail.setBasicAccSubNo((String) subAcc.get("basicAccNo"));
                    //额度品种编号
                    lmtDetail.setLimitSubNo(CmisLmtConstants.LIMIT_SUB_NO_15030101);
                    //额度品种名称
                    lmtDetail.setLimitSubName(CmisLmtConstants.LIMIT_SUB_NAME_15030101);
                    //担保方式
                    lmtDetail.setSuitGuarWay(null);
                    //额度状态 默认 01--生效
                    lmtDetail.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                    //操作类型
                    lmtDetail.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                } else {
                    lmtDetail.setBasicAccNo("");
                    lmtDetail.setBasicAccSubNo((String) subAcc.get("useBasicLmtSubSerno"));
                }
                //本次占用金额
                /*String lmtAmt = (String)subAcc.get("lmtAmt") == null ?"0" : (String)subAcc.get("lmtAmt");//产品授信金额
                String basicAssetBalanceAmt =  (String)subAcc.get("basicAssetBalanceAmt") == null ? "0" : (String)subAcc.get("basicAssetBalanceAmt");//底层资产余额
                String intendActualIssuedScale =  (String)subAcc.get("intendActualIssuedScale") == null ?"0" :(String)subAcc.get("intendActualIssuedScale");//拟/实际发行规模（原币）*/

                //产品授信金额
                BigDecimal lmtAmt = NumberUtils.nullDefaultZero(subAcc.get("lmtAmt"));
                //底层资产余额
                BigDecimal basicAssetBalanceAmt = NumberUtils.nullDefaultZero(subAcc.get("basicAssetBalanceAmt"));
                //拟/实际发行规模（原币）
                BigDecimal prdTotalAmt = NumberUtils.nullDefaultZero(subAcc.get("prdTotalAmt"));

                if (prdTotalAmt == null || prdTotalAmt.compareTo(BigDecimal.ZERO)==0) {
                    throw new BizException(null, "", null, "项目总金额为空，请核查");
                } else {
                    //产品授信金额*底层资产余额/(拟/实际发行规模（原币）)
                    BigDecimal useAmt = comm4CalFormulaUtils.getPrdLmtUseAmt(basicAssetBalanceAmt, lmtAmt, prdTotalAmt) ;
                    lmtDetail.setUseamt(useAmt);
                }
                //责任人
                lmtDetail.setManagerId((String) subAcc.get("inputId"));
                //责任机构
                lmtDetail.setManagerBrId((String) subAcc.get("inputBrId"));
                //登记人
                lmtDetail.setInputId((String) subAcc.get("inputId"));
                //登记机构
                lmtDetail.setInputBrId((String) subAcc.get("inputBrId"));
                //登记日期
                lmtDetail.setInputDate((String) subAcc.get("inputDate"));

                lmtDetailsList.add(lmtDetail);
            }
        }
        cmisLmt0005ReqDto.setLmtDetailsList(lmtDetailsList);

        // 调用额度接口
        LOGGER.info("根据业务申请编号【{}】,前往额度系统进行资金业务额度同步开始！", serno);
        LOGGER.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0005.key,
                DscmsLmtEnum.TRADE_CODE_CMISLMT0005.value, JSON.toJSONString(cmisLmt0005ReqDto));
        ResultDto<CmisLmt0005RespDto> cmisLmt0005RespDto = cmisLmtClientService.cmisLmt0005(cmisLmt0005ReqDto);
        if (cmisLmt0005RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0005RespDto.getData().getErrorCode())) {
            LOGGER.info("根据业务申请编号【{}】,前往额度系统进行资金业务额度同步成功！", serno);
        } else if (cmisLmt0005RespDto != null && !SuccessEnum.SUCCESS.key.equals(cmisLmt0005RespDto.getData().getErrorCode())) {
            LOGGER.info("根据业务申请编号【{}】,前往额度系统进行资金业务额度同步失败！", serno);
            String errorCode = cmisLmt0005RespDto.getData().getErrorCode() ;
            String errorMessage = cmisLmt0005RespDto.getData().getErrorMsg() ;
            throw new YuspException(errorCode, errorMessage);
        }else{
            LOGGER.info("根据业务申请编号【{}】,前往额度系统进行资金业务额度同步失败！", serno);
            String errorCode = "9999" ;
            String errorMessage = "交易异常，返回为空";
            throw new YuspException(errorCode, errorMessage);
        }
    }

    /**
     * 更新资产编号 并重新推送到额度系统
     *
     * @param lmtSigInvestAcc
     * @return
     */
    @Transactional
    public int updateAssetNo(LmtSigInvestAcc lmtSigInvestAcc) {
        Logger log = LoggerFactory.getLogger(LmtSigInvestApprService.class);
        int result = 0;
        try {
            //查询台账表中是否已存在当前资产编号 存在则报错
            check_isExistAssetNo(lmtSigInvestAcc.getAssetNo(),lmtSigInvestAcc.getProNo());

            //更新台账表
            result = updateSelective(lmtSigInvestAcc);

            //更新批复表
            lmtSigInvestRstService.updateAssetNo(lmtSigInvestAcc.getReplySerno(),lmtSigInvestAcc.getAssetNo());
            //更新申请表
            lmtSigInvestAppService.updateAssetNo(lmtSigInvestAcc.getSerno(),lmtSigInvestAcc.getAssetNo());

            try{
                if (result == 1) {
                    lmtSigInvestSendCmisLmt0005(lmtSigInvestAcc.getSerno(), null, null);
                }
            }catch (Exception e){
                throw new BizException(null,"9999",null,e.getMessage());
            }
            result = 1;
        } catch (Exception e) {
            if (e instanceof BizException) {
                throw e;
            }
            log.error(e.getMessage(), e);
            result = 0;
        }
        if (result == 0) {
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_ASSETNO_SAVE_FAILED.key, EclEnum.LMT_SIG_INVESTAPP_ASSETNO_SAVE_FAILED.value);
        }
        return result;
    }

    /**
     * @return
     */
    public List<LmtSigInvestAcc> selectSigInvestInvestType(QueryModel queryModel) {
        return lmtSigInvestAccMapper.selectSigInvestInvestType(queryModel);
    }


    /**
     * 查询台账表中是否已存在当前资产编号
     * @param assetNo
     * @param proNo
     * @return
     */
    public void check_isExistAssetNo(String assetNo, String proNo) {
        //授信新增 资产编号校验 //TODO 资产编号校验
        if (StringUtils.nonBlank(assetNo)) {
            QueryModel queryModel = new QueryModel();
//            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("accStatus", CmisLmtConstants.STD_ZB_APPR_ST_01);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
            queryModel.addCondition("assetNo", assetNo);
            List<LmtSigInvestAcc> lmtSigInvestAccs = lmtSigInvestAccMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(lmtSigInvestAccs)) {
                lmtSigInvestAccs.forEach(a -> {
                    if (!proNo.equals(a.getProNo())) {
                        //该资产编号【XXX1】已存在对应项目编号【XXX2】、项目名称为【XXX3】的授信，请核实！
                        String val = EclEnum.LMT_SIG_INVESTAPP_EORROR000030.value
                                .replace("XXX1", assetNo)
                                .replace("XXX2", a.getProNo())
                                .replace("XXX3", a.getProName());
                        throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000030.key, val);
                    }
                });
            }
            //判断当前资产编号是否存在正在流转中的记录(项目编号相同除外)
            queryModel = new QueryModel();
            queryModel.addCondition("assetNo",assetNo);
            queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);

            List status = new ArrayList();
            status.add(CmisBizConstants.APPLY_STATE_APP);//111
            status.add(CmisBizConstants.APPLY_STATE_TODO);//000
            status.add(CmisBizConstants.APPLY_STATE_CALL_BACK);//992
            queryModel.addCondition("noApproveStatus", status);

            List<LmtSigInvestApp> lmtSigInvestApps = lmtSigInvestAppService.selectAll(queryModel);
            if (CollectionUtils.nonEmpty(lmtSigInvestApps)){
                lmtSigInvestApps.forEach(a->{
                    if (!proNo.equals(a.getProNo())){
                        //该资产编号【XXX1】已存在对应项目编号【XXX2】、项目名称为【XXX3】的授信，请核实！
                        String val = EclEnum.LMT_SIG_INVESTAPP_EORROR000031.value
                                .replace("XXX1", assetNo)
                                .replace("XXX2", a.getProNo())
                                .replace("XXX3", a.getProName());
                        throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_EORROR000031.key, val);
                    }
                });
            }
        }
    }
}
