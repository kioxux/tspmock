/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCptlSituExtGuar;
import cn.com.yusys.yusp.service.RptCptlSituExtGuarService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituExtGuarResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:53:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcptlsituextguar")
public class RptCptlSituExtGuarResource {
    @Autowired
    private RptCptlSituExtGuarService rptCptlSituExtGuarService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCptlSituExtGuar>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCptlSituExtGuar> list = rptCptlSituExtGuarService.selectAll(queryModel);
        return new ResultDto<List<RptCptlSituExtGuar>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCptlSituExtGuar>> index(QueryModel queryModel) {
        List<RptCptlSituExtGuar> list = rptCptlSituExtGuarService.selectByModel(queryModel);
        return new ResultDto<List<RptCptlSituExtGuar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptCptlSituExtGuar> show(@PathVariable("pkId") String pkId) {
        RptCptlSituExtGuar rptCptlSituExtGuar = rptCptlSituExtGuarService.selectByPrimaryKey(pkId);
        return new ResultDto<RptCptlSituExtGuar>(rptCptlSituExtGuar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCptlSituExtGuar> create(@RequestBody RptCptlSituExtGuar rptCptlSituExtGuar) throws URISyntaxException {
        rptCptlSituExtGuarService.insert(rptCptlSituExtGuar);
        return new ResultDto<RptCptlSituExtGuar>(rptCptlSituExtGuar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCptlSituExtGuar rptCptlSituExtGuar) throws URISyntaxException {
        int result = rptCptlSituExtGuarService.update(rptCptlSituExtGuar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptCptlSituExtGuarService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptCptlSituExtGuarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptCptlSituExtGuar>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptCptlSituExtGuar>>(rptCptlSituExtGuarService.selectByModel(model));
    }

    @PostMapping("/insertExtGuar")
    protected ResultDto<Integer> insertExtGuar(@RequestBody RptCptlSituExtGuar rptCptlSituExtGuar){
        rptCptlSituExtGuar.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptCptlSituExtGuarService.insertSelective(rptCptlSituExtGuar));
    }

    @PostMapping("/updateExtGuar")
    protected ResultDto<Integer> updateExtGuar(@RequestBody RptCptlSituExtGuar rptCptlSituExtGuar){
        return new ResultDto<Integer>(rptCptlSituExtGuarService.updateSelective(rptCptlSituExtGuar));
    }

    @PostMapping("/deleteExtGuar")
    protected ResultDto<Integer> deleteExtGuar(@RequestBody RptCptlSituExtGuar rptCptlSituExtGuar){
        String pkId = rptCptlSituExtGuar.getPkId();
        return new ResultDto<Integer>(rptCptlSituExtGuarService.deleteByPrimaryKey(pkId));
    }

    /**
     * 查询集团融资对外担保情况
     * @param model
     * @return
     */
    @PostMapping("/selectGrpExtGuar")
    protected ResultDto<List<Map<String,Object>>> selectGrpExtGuar(@RequestBody QueryModel model){
        return new ResultDto<List<Map<String, Object>>>(rptCptlSituExtGuarService.selectGrpExtGuar(model));
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptCptlSituExtGuar>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptCptlSituExtGuar>>(rptCptlSituExtGuarService.selectBySerno(serno));
    }
}
