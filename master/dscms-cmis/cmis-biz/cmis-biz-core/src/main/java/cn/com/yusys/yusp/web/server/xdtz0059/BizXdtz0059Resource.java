package cn.com.yusys.yusp.web.server.xdtz0059;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0059.req.Xdtz0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.resp.Xdtz0059DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0059.Xdtz0059Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:更新信贷台账信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0059:更新信贷台账信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0059Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0059Resource.class);
    @Autowired
    private Xdtz0059Service xdtz0059Service;

    /**
     * 交易码：xdtz0059
     * 交易描述：更新信贷台账信息
     *
     * @param xdtz0059DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新信贷台账信息")
    @PostMapping("/xdtz0059")
    protected @ResponseBody
    ResultDto<Xdtz0059DataRespDto> xdtz0059(@Validated @RequestBody Xdtz0059DataReqDto xdtz0059DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataReqDto));
        Xdtz0059DataRespDto xdtz0059DataRespDto = new Xdtz0059DataRespDto();// 响应Dto:更新信贷台账信息
        ResultDto<Xdtz0059DataRespDto> xdtz0059DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0059DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataReqDto));
            xdtz0059DataRespDto = xdtz0059Service.xdtz0059(xdtz0059DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataRespDto));
            // 封装xdtz0059DataResultDto中正确的返回码和返回信息
            if("F".equals(xdtz0059DataRespDto.getOpFlag())){
                xdtz0059DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0059DataResultDto.setMessage(xdtz0059DataRespDto.getOpMsg());
            }else{
                xdtz0059DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdtz0059DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, e.getMessage());
            // 封装xdtz0059DataResultDto中异常返回码和返回信息
            xdtz0059DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0059DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0059DataRespDto到xdtz0059DataResultDto中
        xdtz0059DataResultDto.setData(xdtz0059DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataResultDto));
        return xdtz0059DataResultDto;
    }
}
