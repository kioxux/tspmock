package cn.com.yusys.yusp.web.server.xdtz0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0036.req.Xdtz0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.resp.Xdtz0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0036.Xdtz0036Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0036:根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0036Resource.class);

    @Autowired
    private Xdtz0036Service xdtz0036Service;

    /**
     * 交易码：xdtz0036
     * 交易描述：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
     *
     * @param xdtz0036DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数")
    @PostMapping("/xdtz0036")
    protected @ResponseBody
    ResultDto<Xdtz0036DataRespDto> xdtz0036(@Validated @RequestBody Xdtz0036DataReqDto xdtz0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataReqDto));
        Xdtz0036DataRespDto xdtz0036DataRespDto = new Xdtz0036DataRespDto();// 响应Dto:根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
        ResultDto<Xdtz0036DataRespDto> xdtz0036DataResultDto = new ResultDto<>();
        String cusId = xdtz0036DataReqDto.getCusId();//客户号
        String maxOverdueDays = xdtz0036DataReqDto.getMaxOverdueDays();//最大逾期天数
        try {
            // 从xdtz0036DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataReqDto));
            xdtz0036DataRespDto = xdtz0036Service.xdtz0036(xdtz0036DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataRespDto));
            // 封装xdtz0036DataResultDto中正确的返回码和返回信息
            xdtz0036DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0036DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, e.getMessage());
            // 封装xdtz0036DataResultDto中异常返回码和返回信息
            xdtz0036DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0036DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0036DataRespDto到xdtz0036DataResultDto中
        xdtz0036DataResultDto.setData(xdtz0036DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataResultDto));
        return xdtz0036DataResultDto;
    }
}
