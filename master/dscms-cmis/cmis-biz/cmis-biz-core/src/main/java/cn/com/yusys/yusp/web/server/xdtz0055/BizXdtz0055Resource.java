package cn.com.yusys.yusp.web.server.xdtz0055;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0055.req.Xdtz0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0055.Xdtz0055Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;

/**
 * 接口处理类:查询退回受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0055:查询退回受托信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0055Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0055Resource.class);

    @Autowired
    private Xdtz0055Service xdtz0055Service;

    /**
     * 交易码：xdtz0055
     * 交易描述：查询退回受托信息
     *
     * @param xdtz0055DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询退回受托信息")
    @PostMapping("/xdtz0055")
    protected @ResponseBody
    ResultDto<Xdtz0055DataRespDto> xdtz0055(@Validated @RequestBody Xdtz0055DataReqDto xdtz0055DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055DataReqDto));
        // 响应Dto:查询退回受托信息
        Xdtz0055DataRespDto xdtz0055DataRespDto = new Xdtz0055DataRespDto();
        ResultDto<Xdtz0055DataRespDto> xdtz0055DataResultDto = new ResultDto<>();
        //客户号
        String cusId = xdtz0055DataReqDto.getCusId();
        //客户账号
        String cusAcctNo = xdtz0055DataReqDto.getCusAcctNo();
        try {
            //校验返回参数，两者不能同时为空
            if (StringUtils.isAllEmpty(cusId, cusAcctNo)) {
                xdtz0055DataResultDto.setCode(EpbEnum.EPB090009.key);
                xdtz0055DataResultDto.setMessage(EpbEnum.EPB090009.value);
                xdtz0055DataResultDto.setData(xdtz0055DataRespDto);
                return xdtz0055DataResultDto;
            }
            // 从xdtz0055DataReqDto获取业务值进行业务逻辑处理
            xdtz0055DataRespDto = xdtz0055Service.xdtz0055(cusId,cusAcctNo);
            //如果查询结果为空，说明传入的参数有问题
            if (xdtz0055DataRespDto == null) {
                xdtz0055DataResultDto.setCode(EpbEnum.EPB090004.key);
                xdtz0055DataResultDto.setMessage(EpbEnum.EPB090004.value);
                xdtz0055DataResultDto.setData(xdtz0055DataRespDto);
                return xdtz0055DataResultDto;
            }
            // 封装xdtz0055DataResultDto中正确的返回码和返回信息
            xdtz0055DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0055DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, e.getMessage());
            // 封装xdtz0055DataResultDto中异常返回码和返回信息
            xdtz0055DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0055DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0055DataRespDto到xdtz0055DataResultDto中
        xdtz0055DataResultDto.setData(xdtz0055DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055DataResultDto));
        return xdtz0055DataResultDto;
    }
}
