/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicFncAnly
 * @类描述: lmt_sig_invest_basic_fnc_anly数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 20:54:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_basic_fnc_anly")
public class LmtSigInvestBasicFncAnly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 底层申请流水号 **/
	@Column(name = "BASIC_SERNO", unique = false, nullable = true, length = 40)
	private String basicSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 总资产 **/
	@Column(name = "TOTAL_RESSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalRessetAmt;
	
	/** 总负债 **/
	@Column(name = "TOTAL_DEBT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalDebtAmt;
	
	/** 营业收入 **/
	@Column(name = "BSINS_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bsinsIncome;
	
	/** 净利润 **/
	@Column(name = "PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal profit;
	
	/** 资产负债率 **/
	@Column(name = "ASSET_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetDebtRate;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 2000)
	private String otherDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param basicSerno
	 */
	public void setBasicSerno(String basicSerno) {
		this.basicSerno = basicSerno;
	}
	
    /**
     * @return basicSerno
     */
	public String getBasicSerno() {
		return this.basicSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param totalRessetAmt
	 */
	public void setTotalRessetAmt(java.math.BigDecimal totalRessetAmt) {
		this.totalRessetAmt = totalRessetAmt;
	}
	
    /**
     * @return totalRessetAmt
     */
	public java.math.BigDecimal getTotalRessetAmt() {
		return this.totalRessetAmt;
	}
	
	/**
	 * @param totalDebtAmt
	 */
	public void setTotalDebtAmt(java.math.BigDecimal totalDebtAmt) {
		this.totalDebtAmt = totalDebtAmt;
	}
	
    /**
     * @return totalDebtAmt
     */
	public java.math.BigDecimal getTotalDebtAmt() {
		return this.totalDebtAmt;
	}
	
	/**
	 * @param bsinsIncome
	 */
	public void setBsinsIncome(java.math.BigDecimal bsinsIncome) {
		this.bsinsIncome = bsinsIncome;
	}
	
    /**
     * @return bsinsIncome
     */
	public java.math.BigDecimal getBsinsIncome() {
		return this.bsinsIncome;
	}
	
	/**
	 * @param profit
	 */
	public void setProfit(java.math.BigDecimal profit) {
		this.profit = profit;
	}
	
    /**
     * @return profit
     */
	public java.math.BigDecimal getProfit() {
		return this.profit;
	}
	
	/**
	 * @param assetDebtRate
	 */
	public void setAssetDebtRate(java.math.BigDecimal assetDebtRate) {
		this.assetDebtRate = assetDebtRate;
	}
	
    /**
     * @return assetDebtRate
     */
	public java.math.BigDecimal getAssetDebtRate() {
		return this.assetDebtRate;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}