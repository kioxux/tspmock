package cn.com.yusys.yusp.web.server.xdzc0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0009.req.Xdzc0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.resp.Xdzc0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0009.Xdzc0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池出票交易接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0009:资产池出票交易接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0009Resource.class);

    @Autowired
    private Xdzc0009Service xdzc0009Service;
    /**
     * 交易码：xdzc0009
     * 交易描述：资产池出票交易接口
     *
     * @param xdzc0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出票交易接口")
    @PostMapping("/xdzc0009")
    protected @ResponseBody
    ResultDto<Xdzc0009DataRespDto> xdzc0009(@Validated @RequestBody Xdzc0009DataReqDto xdzc0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009DataReqDto));
        Xdzc0009DataRespDto xdzc0009DataRespDto = new Xdzc0009DataRespDto();// 响应Dto:资产池出票交易接口
        ResultDto<Xdzc0009DataRespDto> xdzc0009DataResultDto = new ResultDto<>();
        try {
            xdzc0009DataRespDto = xdzc0009Service.xdzc0009Service(xdzc0009DataReqDto);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, e.getMessage());
            // 封装xdzc0009DataResultDto中异常返回码和返回信息
            xdzc0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0009DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0009DataRespDto到xdzc0009DataResultDto中
        xdzc0009DataResultDto.setData(xdzc0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009DataResultDto));
        return xdzc0009DataResultDto;
    }
}
