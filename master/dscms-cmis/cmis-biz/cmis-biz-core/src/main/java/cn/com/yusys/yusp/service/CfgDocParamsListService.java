/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CfgDocParamsDetail;
import cn.com.yusys.yusp.domain.CfgDocParamsList;
import cn.com.yusys.yusp.dto.CfgDocParamsListDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.repository.mapper.CfgDocParamsListMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsListService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgDocParamsListService {

    @Autowired
    private CfgDocParamsListMapper cfgDocParamsListMapper;

    @Autowired
    private CfgDocParamsDetailService cfgDocParamsDetailService;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CfgDocParamsList selectByPrimaryKey(String cdplSerno) {
        return cfgDocParamsListMapper.selectByPrimaryKey(cdplSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgDocParamsList> selectAll(QueryModel model) {
        List<CfgDocParamsList> records = (List<CfgDocParamsList>) cfgDocParamsListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CfgDocParamsList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgDocParamsList> list = cfgDocParamsListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CfgDocParamsList record) {
        return cfgDocParamsListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CfgDocParamsList record) {
        return cfgDocParamsListMapper.insertSelective(record);
    }



    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CfgDocParamsList record) {
        return cfgDocParamsListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CfgDocParamsList record) {
        return cfgDocParamsListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cdplSerno) {
        return cfgDocParamsListMapper.deleteByPrimaryKey(cdplSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgDocParamsListMapper.deleteByIds(ids);
    }
    /**
     * @函数名称:saveCfgDocParamsList
     * @函数描述:档案参数新增
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    public Map saveDoImageSpplInfo(CfgDocParamsList record) {
        Map result = new HashMap();
        String cdplSerno = "";
        String rtnCode = EcbEnum.CDPL_SUCCESS_DEF.key;
        String rtnMsg ="";
        String updId ="";//登记人
        String updBrId ="";//登记人机构
        if (null == record.getCdplSerno()) {
            cdplSerno = StringUtils.getUUID();
            record.setCdplSerno(cdplSerno);
        }
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
        }
        Date createTime = DateUtils.getCurrTimestamp();// 创建时间
        String inputDate = DateUtils.getCurrDateStr();// 申请日期
        record.setInputId(updId);//登记人ID
        record.setInputBrId(updBrId);//登记人机构
        record.setInputDate(inputDate);
        record.setCreateTime(createTime);
        int count = cfgDocParamsListMapper.insertSelective(record);
        if (count >0){
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg", rtnMsg);
        }else {
            result.put("rtnCode",EcbEnum.E_CFG_DOC_PARAMS_LIST_INSERTEXCEPTION_01.key);
            result.put("rtnMsg", EcbEnum.E_CFG_DOC_PARAMS_LIST_INSERTEXCEPTION_01.value);
        }
        return result;
    }
    /**
     * @函数名称:saveDoImageSpplInfos
     * @函数描述:档案参数新增+档案参数清单保存
     * @参数与返回说明:
     * @算法描述:
     * @author:cainingbo_yx
     */
    public int saveDoImageSpplInfos(CfgDocParamsListDto record) {
        String cdplSerno = "";
        int count =0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            CfgDocParamsList cfgDocParamsList = new CfgDocParamsList();
            //档案参数配置流水号
            if (null == record.getCdplSerno()) {
                cdplSerno = StringUtils.getUUID();
            }else {
                cdplSerno=record.getCdplSerno();
            }
            //业务流水号
            cfgDocParamsList.setCdplSerno(cdplSerno);
            // 登记人
            cfgDocParamsList.setInputId(inputId);
            // 登记机构
            cfgDocParamsList.setInputBrId(inputBrId);
            // 登记日期
            cfgDocParamsList.setInputDate(inputDate);
            // 登记时间
            cfgDocParamsList.setCreateTime(createTime);
            // 修改人
            cfgDocParamsList.setUpdId(inputId);
            // 修改机构
            cfgDocParamsList.setUpdBrId(inputBrId);
            // 修改日期
            cfgDocParamsList.setUpdDate(inputDate);
            // 修改时间
            cfgDocParamsList.setUpdateTime(createTime);
            //档案分类
            cfgDocParamsList.setDocClass(record.getDocClass());
            //档案类型
            cfgDocParamsList.setDocType(record.getDocType());
            //是否启用
            cfgDocParamsList.setIsBegin(record.getIsBegin());
            count = insertSelective(cfgDocParamsList);
            if (count > 0) {
                //费用明细表
                List<CfgDocParamsDetail> cfgDocParamsListDtoList = record.getCfgDocParamsDetail();
                if(cfgDocParamsListDtoList != null){
                    for (CfgDocParamsDetail cfgDocParamsDetails : cfgDocParamsListDtoList) {
                        if(cfgDocParamsDetails.getCdpdSerno() ==null || "".equals(cfgDocParamsDetails.getCdpdSerno()) ){
                            cfgDocParamsDetails.setCdpdSerno(StringUtils.getUUID());
                        }
                        cfgDocParamsDetails.setCdplSerno(cdplSerno);
                        cfgDocParamsDetails.setInputDate(inputDate);
                        cfgDocParamsDetails.setInputBrId(inputBrId);
                        cfgDocParamsDetails.setInputId(inputId);
                        cfgDocParamsDetails.setCreateTime(createTime);
                        cfgDocParamsDetails.setUpdDate(inputDate);
                        cfgDocParamsDetails.setUpdBrId(inputBrId);
                        cfgDocParamsDetails.setUpdId(inputId);
                        cfgDocParamsDetails.setUpdateTime(createTime);
                        count = cfgDocParamsDetailService.insert(cfgDocParamsDetails);
                    }
                }
            } else {
                throw new YuspException(EcnEnum.ECN069999.key, EcnEnum.ECN060001.value);
            }
        }
        return count;
    }

    /**
     * @函数名称:updateCfgDocParamsLists
     * @函数描述:对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    public int updateCfgDocParamsLists(CfgDocParamsListDto record) {
        String cdplSerno = "";
        int count = 0;
        if (record != null) {
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            CfgDocParamsList cfgDocParamsList = new CfgDocParamsList();
            //档案参数配置流水号
            if (null == record.getCdplSerno()) {
                cdplSerno = StringUtils.getUUID();
                cfgDocParamsList.setCdplSerno(cdplSerno);
            }
            cfgDocParamsList.setCdplSerno(record.getCdplSerno());
            // 登记人
            cfgDocParamsList.setInputId(inputId);
            // 登记机构
            cfgDocParamsList.setInputBrId(inputBrId);
            // 登记日期
            cfgDocParamsList.setInputDate(inputDate);
            // 登记时间
            cfgDocParamsList.setCreateTime(createTime);
            // 修改人
            cfgDocParamsList.setUpdId(inputId);
            // 修改机构
            cfgDocParamsList.setUpdBrId(inputBrId);
            // 修改日期
            cfgDocParamsList.setUpdDate(inputDate);
            // 修改时间
            cfgDocParamsList.setUpdateTime(createTime);
            //档案分类
            cfgDocParamsList.setDocClass(record.getDocClass());
            //档案类型
            cfgDocParamsList.setDocType(record.getDocType());
            //是否启用
            cfgDocParamsList.setIsBegin(record.getIsBegin());
            count = updateSelective(cfgDocParamsList);
            //费用明细表
            List<CfgDocParamsDetail> cfgDocParamsListDtoList = record.getCfgDocParamsDetail();
            for (CfgDocParamsDetail cfgDocParamsDetails : cfgDocParamsListDtoList) {
                CfgDocParamsDetail  cfgDocParamsDetail = cfgDocParamsDetailService.selectByPrimaryKey(cfgDocParamsDetails.getCdpdSerno());
                if(cfgDocParamsDetail != null){
                    cfgDocParamsDetails.setUpdDate(inputDate);
                    cfgDocParamsDetails.setUpdBrId(inputBrId);
                    cfgDocParamsDetails.setUpdId(inputId);
                    cfgDocParamsDetails.setUpdateTime(createTime);
                    count = cfgDocParamsDetailService.updateSelective(cfgDocParamsDetails);
                }else{
                    cfgDocParamsDetails.setInputId(inputId);
                    cfgDocParamsDetails.setInputBrId(inputBrId);
                    cfgDocParamsDetails.setInputDate(inputDate);
                    cfgDocParamsDetails.setCreateTime(createTime);
                    cfgDocParamsDetails.setUpdDate(inputDate);
                    cfgDocParamsDetails.setUpdBrId(inputBrId);
                    cfgDocParamsDetails.setUpdId(inputId);
                    cfgDocParamsDetails.setUpdateTime(createTime);
                    cfgDocParamsDetails.setCdpdSerno(StringUtils.getUUID());
                    cfgDocParamsDetails.setCdplSerno(record.getCdplSerno());
                    count = cfgDocParamsDetailService.insert(cfgDocParamsDetails);
                }
            }
        }
        return count;
    }
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     * @author:cainingbo_yx
     */

    public Map deleteByCdplSerno(CfgDocParamsList cfgDocParamsList) {
        String rtnCode = EcbEnum.CDPL_SUCCESS_DEF.key;
        String rtnMsg ="";
        Map result = new HashMap();
        String isBeging = cfgDocParamsList.getIsBegin();
        if("1".equals(isBeging)){//是否启用
            throw BizException.error(null, EcbEnum.CDPL_EXCEPTION_DEF.key, "档案参数配置在启用状态下不可删除！");
        }
        String cdplSerno = cfgDocParamsList.getCdplSerno();
        int count = cfgDocParamsListMapper.deleteByPrimaryKey(cdplSerno);
        if(count>0){
           int counts = cfgDocParamsDetailService.deleteByCdplSerno(cdplSerno);
            result.put("rthCode",rtnCode);
            result.put("rthMsg",rtnMsg);
            result.put("count",counts);//删除条数
        }else{
            result.put("rthCode",EcbEnum.CDPL_EXCEPTION_DEF.key);
            result.put("rtnMsg","数据更新异常！");
        }
        return result;
    }

    /**
     * 获取启用状态的档案文件清单配置
     * @author jijian_yx
     * @date 2021/6/22 21:10
     **/
    public CfgDocParamsList getEnableCfgDoc(String docType) {
        return cfgDocParamsListMapper.selectEnableByDocType(docType);
    }
}
