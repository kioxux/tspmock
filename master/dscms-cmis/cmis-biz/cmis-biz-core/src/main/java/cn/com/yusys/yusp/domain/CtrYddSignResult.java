/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrYddSignResult
 * @类描述: ctr_ydd_sign_result数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-10 20:42:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_ydd_sign_result")
public class CtrYddSignResult extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 证件号码 **/
	@Id
	@Column(name = "CERT_CODE")
	private String certCode;
	
	/** 合同编号 **/
	@Id
	@Column(name = "CONT_NO")
	private String contNo;
	
	/** 担保合同编号 **/
	@Id
	@Column(name = "GUAR_CONT_NO")
	private String guarContNo;
	
	/** 借款合同签约状态 **/
	@Column(name = "LOAN_CONT_SIGN_STATUS", unique = false, nullable = true, length = 5)
	private String loanContSignStatus;
	
	/** 担保合同签约状态 **/
	@Column(name = "GUAR_CONT_SIGN_STATUS", unique = false, nullable = true, length = 5)
	private String guarContSignStatus;
	
	/** 主债权合同及抵押合同表签约状态 **/
	@Column(name = "CLAIM_CONT_SIGN_STATUS", unique = false, nullable = true, length = 5)
	private String claimContSignStatus;
	
	/** 不动产登记申请书签约状态 **/
	@Column(name = "REGIST_APP_SIGN_STATUS", unique = false, nullable = true, length = 5)
	private String registAppSignStatus;
	
	/** 影像目录 **/
	@Column(name = "IMAGE_DIR", unique = false, nullable = true, length = 20)
	private String imageDir;
	
	/** 主债权合同及抵押合同表编号 **/
	@Column(name = "CLAIM_CONT_NO", unique = false, nullable = true, length = 40)
	private String claimContNo;
	
	/** 不动产登记申请书编号 **/
	@Column(name = "REGIST_APP_NO", unique = false, nullable = true, length = 40)
	private String registAppNo;
	
	/** 签约时间 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 40)
	private String signDate;
	
	/** 借款合同影像流水号 **/
	@Column(name = "LOAN_CONT_IMAGE_NO", unique = false, nullable = true, length = 40)
	private String loanContImageNo;
	
	/** 担保合同影像流水号 **/
	@Column(name = "GUAR_CONT_IMAGE_NO", unique = false, nullable = true, length = 40)
	private String guarContImageNo;
	
	/** 主债权合同影像流水号 **/
	@Column(name = "CLAIM_CONT_IMAGE_NO", unique = false, nullable = true, length = 40)
	private String claimContImageNo;
	
	/** 登记申请书影像流水号 **/
	@Column(name = "REGIST_APP_IMAGE_NO", unique = false, nullable = true, length = 40)
	private String registAppImageNo;
	
	/** 是否主借款人 **/
	@Column(name = "IS_MAIN_BORROWER", unique = false, nullable = true, length = 10)
	private String isMainBorrower;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param loanContSignStatus
	 */
	public void setLoanContSignStatus(String loanContSignStatus) {
		this.loanContSignStatus = loanContSignStatus;
	}
	
    /**
     * @return loanContSignStatus
     */
	public String getLoanContSignStatus() {
		return this.loanContSignStatus;
	}
	
	/**
	 * @param guarContSignStatus
	 */
	public void setGuarContSignStatus(String guarContSignStatus) {
		this.guarContSignStatus = guarContSignStatus;
	}
	
    /**
     * @return guarContSignStatus
     */
	public String getGuarContSignStatus() {
		return this.guarContSignStatus;
	}
	
	/**
	 * @param claimContSignStatus
	 */
	public void setClaimContSignStatus(String claimContSignStatus) {
		this.claimContSignStatus = claimContSignStatus;
	}
	
    /**
     * @return claimContSignStatus
     */
	public String getClaimContSignStatus() {
		return this.claimContSignStatus;
	}
	
	/**
	 * @param registAppSignStatus
	 */
	public void setRegistAppSignStatus(String registAppSignStatus) {
		this.registAppSignStatus = registAppSignStatus;
	}
	
    /**
     * @return registAppSignStatus
     */
	public String getRegistAppSignStatus() {
		return this.registAppSignStatus;
	}
	
	/**
	 * @param imageDir
	 */
	public void setImageDir(String imageDir) {
		this.imageDir = imageDir;
	}
	
    /**
     * @return imageDir
     */
	public String getImageDir() {
		return this.imageDir;
	}
	
	/**
	 * @param claimContNo
	 */
	public void setClaimContNo(String claimContNo) {
		this.claimContNo = claimContNo;
	}
	
    /**
     * @return claimContNo
     */
	public String getClaimContNo() {
		return this.claimContNo;
	}
	
	/**
	 * @param registAppNo
	 */
	public void setRegistAppNo(String registAppNo) {
		this.registAppNo = registAppNo;
	}
	
    /**
     * @return registAppNo
     */
	public String getRegistAppNo() {
		return this.registAppNo;
	}
	
	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}
	
    /**
     * @return signDate
     */
	public String getSignDate() {
		return this.signDate;
	}
	
	/**
	 * @param loanContImageNo
	 */
	public void setLoanContImageNo(String loanContImageNo) {
		this.loanContImageNo = loanContImageNo;
	}
	
    /**
     * @return loanContImageNo
     */
	public String getLoanContImageNo() {
		return this.loanContImageNo;
	}
	
	/**
	 * @param guarContImageNo
	 */
	public void setGuarContImageNo(String guarContImageNo) {
		this.guarContImageNo = guarContImageNo;
	}
	
    /**
     * @return guarContImageNo
     */
	public String getGuarContImageNo() {
		return this.guarContImageNo;
	}
	
	/**
	 * @param claimContImageNo
	 */
	public void setClaimContImageNo(String claimContImageNo) {
		this.claimContImageNo = claimContImageNo;
	}
	
    /**
     * @return claimContImageNo
     */
	public String getClaimContImageNo() {
		return this.claimContImageNo;
	}
	
	/**
	 * @param registAppImageNo
	 */
	public void setRegistAppImageNo(String registAppImageNo) {
		this.registAppImageNo = registAppImageNo;
	}
	
    /**
     * @return registAppImageNo
     */
	public String getRegistAppImageNo() {
		return this.registAppImageNo;
	}
	
	/**
	 * @param isMainBorrower
	 */
	public void setIsMainBorrower(String isMainBorrower) {
		this.isMainBorrower = isMainBorrower;
	}
	
    /**
     * @return isMainBorrower
     */
	public String getIsMainBorrower() {
		return this.isMainBorrower;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}