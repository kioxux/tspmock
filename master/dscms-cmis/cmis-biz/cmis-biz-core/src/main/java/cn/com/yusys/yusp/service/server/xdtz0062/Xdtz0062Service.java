package cn.com.yusys.yusp.service.server.xdtz0062;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.req.Xdtz0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.resp.Xdtz0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.ComLoanInfoServices;
import cn.com.yusys.yusp.service.DoubleViewServices;
import cn.com.yusys.yusp.service.HxdListQueryServices;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务逻辑类:查询客户的个人消费贷款
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0062Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0062Service.class);

    @Resource
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：xdtz0062
     * 交易描述：查询客户的个人消费贷款
     *
     * @param xdtz0062DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0062DataRespDto xdtz0062(Xdtz0062DataReqDto xdtz0062DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataReqDto));
        Xdtz0062DataRespDto xdtz0062DataRespDto = new Xdtz0062DataRespDto();
        try {

            String cusId = xdtz0062DataReqDto.getCusId();
            int count = 0;
            //查詢貸款信息
            Map queryMap = new HashMap();
            queryMap.put("cusId", cusId);
            logger.info("***********查询台账信息表开始*START**************查询参数" + cusId);
            List<AccLoan> accLoanList = accLoanMapper.selectxdtzAccLoanForxdtz(queryMap);
            logger.info("***********查询台账信息表结束*END***************");

            for (int i = 0; i < accLoanList.size(); i++) {
                String prdId = accLoanList.get(i).getPrdId();
                logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                String prdCode = prdresultDto.getCode();//返回结果
                String prdType = StringUtils.EMPTY;
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                    if (CfgPrdBasicinfoDto != null) {
                        prdType = CfgPrdBasicinfoDto.getPrdType();
                    }
                }
                if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType)) {//经营
                    prdType = DscmsBizTzEnum.PRDTYPE_01.key;
                } else if (DscmsBizTzEnum.PRDTYPE_09.key.equals(prdType)) {//消费
                    count++;
                }
                logger.info("***********调用iCmisCfgClientService查询产品类别*END**************");
            }
            xdtz0062DataRespDto.setCount(String.valueOf(count));
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataRespDto));
        return xdtz0062DataRespDto;
    }
}
