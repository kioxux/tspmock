/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AsplIoPoolDetails;
import cn.com.yusys.yusp.domain.IqpAppAspl;
import cn.com.yusys.yusp.dto.InPoolAssetListDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolDetailsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:15:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AsplIoPoolDetailsMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AsplIoPoolDetails selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AsplIoPoolDetails> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AsplIoPoolDetails record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AsplIoPoolDetails record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AsplIoPoolDetails record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AsplIoPoolDetails record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: insertAsplIoPoolDetailsList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertAsplIoPoolDetailsList(@Param("list") List<AsplIoPoolDetails> asplIoPoolDetailsList);

    /**
     * @方法名称: inPoolAccList
     * @方法描述: 池内业务台账列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<InPoolAssetListDto> inPoolAssetList(QueryModel model);

    /**
     * @方法名称: selectBySelfModel
     * @方法描述: 灵活查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AsplIoPoolDetails> selectBySelfModel(QueryModel queryModel);

    /**
     * @方法名称: selectBySelfModel
     * @方法描述: 灵活查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.xdzc0020.resp.List> selectAssetIoInfoBySerno(@Param("serno") String serno);

    /**
     * 根据入参查询入池资产信息
     *
     * @param queryMap
     * @return
     */
    List<AsplIoPoolDetails> queryAsplIoPoolDetailsDataByParams(Map queryMap);

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    List<AsplIoPoolDetails> selectBySerno(@Param("serno") String serno);

    /**
     * 根据流水号删除
     * @param serno
     * @return
     */
    int deleteOutpoolDetailsBySerno(@Param("serno")String serno);

    /**
     * 根据流水和资产编号删除
     * @param serno
     * @param assetNo
     */
    int delOutPoolDetails(@Param("serno")String serno, @Param("assetNo")String assetNo);

    /**
     * 计算批次总价值
     * @param serno
     * @return
     */
    BigDecimal computeSumAmt(@Param("serno")String serno);
}