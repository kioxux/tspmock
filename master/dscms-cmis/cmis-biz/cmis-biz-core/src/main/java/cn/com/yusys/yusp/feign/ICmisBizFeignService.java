package cn.com.yusys.yusp.feign;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 调用yuxpservice提供的API接口
 */
@FeignClient("yuxpservice")
public interface ICmisBizFeignService {
    /**
     * 根据岗位编码查询对应的用户列表
     * @param dutyId
     * @return
     */
    @GetMapping(value = "/api/authoritysearch/getDutyUsers/{dutyId}")
    public String getDutyUsers(@PathVariable("dutyId") String dutyId);
}
