/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBuilProject;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;
import cn.com.yusys.yusp.service.GuarInfBusinessIndustryHousrService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBusinessIndustryHousrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfbusinessindustryhousr")
public class GuarInfBusinessIndustryHousrResource {
    @Autowired
    private GuarInfBusinessIndustryHousrService guarInfBusinessIndustryHousrService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfBusinessIndustryHousr>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfBusinessIndustryHousr> list = guarInfBusinessIndustryHousrService.selectAll(queryModel);
        return new ResultDto<List<GuarInfBusinessIndustryHousr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfBusinessIndustryHousr>> index(QueryModel queryModel) {
        List<GuarInfBusinessIndustryHousr> list = guarInfBusinessIndustryHousrService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfBusinessIndustryHousr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfBusinessIndustryHousr> show(@PathVariable("serno") String serno) {
        GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr = guarInfBusinessIndustryHousrService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfBusinessIndustryHousr>(guarInfBusinessIndustryHousr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfBusinessIndustryHousr> create(@RequestBody GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr) throws URISyntaxException {
        guarInfBusinessIndustryHousrService.insert(guarInfBusinessIndustryHousr);
        return new ResultDto<GuarInfBusinessIndustryHousr>(guarInfBusinessIndustryHousr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr) throws URISyntaxException {
        int result = guarInfBusinessIndustryHousrService.update(guarInfBusinessIndustryHousr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfBusinessIndustryHousrService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfBusinessIndustryHousrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfBusinessIndustryHousr
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfBusinessIndustryHousr")
    protected ResultDto<Integer> preserveGuarCargoPledge(@RequestBody GuarInfBusinessIndustryHousr guarInfBusinessIndustryHousr) {
        int result = guarInfBusinessIndustryHousrService.preserveGuarInfBusinessIndustryHousr(guarInfBusinessIndustryHousr);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfBusinessIndustryHousr> queryBySerno(@RequestBody GuarInfBusinessIndustryHousr record) {
        GuarInfBusinessIndustryHousr guarInfBusinessHousr = guarInfBusinessIndustryHousrService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfBusinessIndustryHousr>(guarInfBusinessHousr);
    }
}
