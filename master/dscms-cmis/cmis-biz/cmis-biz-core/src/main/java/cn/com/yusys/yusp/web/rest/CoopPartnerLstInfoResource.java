/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPartnerLstInfo;
import cn.com.yusys.yusp.service.CoopPartnerLstInfoService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerLstInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-20 09:38:30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cooppartnerlstinfo")
public class CoopPartnerLstInfoResource {
    @Autowired
    private CoopPartnerLstInfoService coopPartnerLstInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPartnerLstInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopPartnerLstInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPartnerLstInfo>> index(QueryModel queryModel) {
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerLstInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPartnerLstInfo>> query(@RequestBody QueryModel queryModel) {
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerLstInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，不适用管护条件
     * @创建者: zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCoopPartnerInfo")
    protected ResultDto<List<CoopPartnerLstInfo>> queryCoopPartnerInfo(@RequestBody QueryModel queryModel) {
        List<CoopPartnerLstInfo> list = coopPartnerLstInfoService.queryCoopPartnerInfo(queryModel);
        return new ResultDto<List<CoopPartnerLstInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{partnerNo}")
    protected ResultDto<CoopPartnerLstInfo> show(@PathVariable("partnerNo") String partnerNo) {
        CoopPartnerLstInfo coopPartnerLstInfo = coopPartnerLstInfoService.selectByPrimaryKey(partnerNo);
        return new ResultDto<CoopPartnerLstInfo>(coopPartnerLstInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPartnerLstInfo> create(@RequestBody CoopPartnerLstInfo coopPartnerLstInfo) throws URISyntaxException {
        coopPartnerLstInfoService.insert(coopPartnerLstInfo);
        return new ResultDto<CoopPartnerLstInfo>(coopPartnerLstInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPartnerLstInfo coopPartnerLstInfo) throws URISyntaxException {
        int result = coopPartnerLstInfoService.update(coopPartnerLstInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{partnerNo}")
    protected ResultDto<Integer> delete(@PathVariable("partnerNo") String partnerNo) {
        int result = coopPartnerLstInfoService.deleteByPrimaryKey(partnerNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPartnerLstInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
