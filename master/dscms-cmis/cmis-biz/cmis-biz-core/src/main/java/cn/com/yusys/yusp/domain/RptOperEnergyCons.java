/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergyCons
 * @类描述: rpt_oper_energy_cons数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:41:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_energy_cons")
public class RptOperEnergyCons extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 最近第二年用电度数 **/
	@Column(name = "NEAR_SECOND_POWER_NUM", unique = false, nullable = true, length = 20)
	private String nearSecondPowerNum;
	
	/** 最近第二年用气量 **/
	@Column(name = "NEAR_SECOND_GAS_AMOUNT", unique = false, nullable = true, length = 20)
	private String nearSecondGasAmount;
	
	/** 最近第二年用水量 **/
	@Column(name = "NEAR_SECOND_WATER_AMOUNT", unique = false, nullable = true, length = 20)
	private String nearSecondWaterAmount;
	
	/** 最近第二年工人人数 **/
	@Column(name = "NEAR_SECOND_WORKER_NUM", unique = false, nullable = true, length = 10)
	private Integer nearSecondWorkerNum;
	
	/** 最近第一年用电度数 **/
	@Column(name = "NEAR_FIRST_POWER_NUM", unique = false, nullable = true, length = 20)
	private String nearFirstPowerNum;
	
	/** 最近第一年用气量 **/
	@Column(name = "NEAR_FIRST_GAS_AMOUNT", unique = false, nullable = true, length = 20)
	private String nearFirstGasAmount;
	
	/** 最近第一年用水量 **/
	@Column(name = "NEAR_FIRST_WATER_AMOUNT", unique = false, nullable = true, length = 20)
	private String nearFirstWaterAmount;
	
	/** 最近第一年工人人数 **/
	@Column(name = "NEAR_FIRST_WORKER_NUM", unique = false, nullable = true, length = 10)
	private Integer nearFirstWorkerNum;
	
	/** 今年当前用电度数 **/
	@Column(name = "CURR_POWER_NUM", unique = false, nullable = true, length = 20)
	private String currPowerNum;
	
	/** 今年当前用气量 **/
	@Column(name = "CURR_GAS_AMOUNT", unique = false, nullable = true, length = 20)
	private String currGasAmount;
	
	/** 今年当前用水量 **/
	@Column(name = "CURR_WATER_AMOUNT", unique = false, nullable = true, length = 20)
	private String currWaterAmount;
	
	/** 今年当前工人人数 **/
	@Column(name = "CURR_WORKER_NUM", unique = false, nullable = true, length = 10)
	private Integer currWorkerNum;
	
	/** 去年同期用电度数 **/
	@Column(name = "LAST_YEAR_POWER_NUM", unique = false, nullable = true, length = 20)
	private String lastYearPowerNum;
	
	/** 去年同期用气量 **/
	@Column(name = "LAST_YEAR_GAS_AMOUNT", unique = false, nullable = true, length = 20)
	private String lastYearGasAmount;
	
	/** 去年同期用水量 **/
	@Column(name = "LAST_YEAR_WATER_AMOUNT", unique = false, nullable = true, length = 20)
	private String lastYearWaterAmount;
	
	/** 去年同期工人人数 **/
	@Column(name = "LAST_YEAR_WORKER_NUM", unique = false, nullable = true, length = 10)
	private Integer lastYearWorkerNum;
	
	/** 用电度数备注 **/
	@Column(name = "POWER_NUM_REMARK", unique = false, nullable = true, length = 65535)
	private String powerNumRemark;
	
	/** 用气量备注 **/
	@Column(name = "GAS_AMOUNTRE_MARK", unique = false, nullable = true, length = 65535)
	private String gasAmountreMark;
	
	/** 用水量备注 **/
	@Column(name = "WATER_AMOUNT_REMARK", unique = false, nullable = true, length = 65535)
	private String waterAmountRemark;
	
	/** 工人人数备注 **/
	@Column(name = "WORKER_NUM_REMARK", unique = false, nullable = true, length = 65535)
	private String workerNumRemark;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param nearSecondPowerNum
	 */
	public void setNearSecondPowerNum(String nearSecondPowerNum) {
		this.nearSecondPowerNum = nearSecondPowerNum;
	}
	
    /**
     * @return nearSecondPowerNum
     */
	public String getNearSecondPowerNum() {
		return this.nearSecondPowerNum;
	}
	
	/**
	 * @param nearSecondGasAmount
	 */
	public void setNearSecondGasAmount(String nearSecondGasAmount) {
		this.nearSecondGasAmount = nearSecondGasAmount;
	}
	
    /**
     * @return nearSecondGasAmount
     */
	public String getNearSecondGasAmount() {
		return this.nearSecondGasAmount;
	}
	
	/**
	 * @param nearSecondWaterAmount
	 */
	public void setNearSecondWaterAmount(String nearSecondWaterAmount) {
		this.nearSecondWaterAmount = nearSecondWaterAmount;
	}
	
    /**
     * @return nearSecondWaterAmount
     */
	public String getNearSecondWaterAmount() {
		return this.nearSecondWaterAmount;
	}
	
	/**
	 * @param nearSecondWorkerNum
	 */
	public void setNearSecondWorkerNum(Integer nearSecondWorkerNum) {
		this.nearSecondWorkerNum = nearSecondWorkerNum;
	}
	
    /**
     * @return nearSecondWorkerNum
     */
	public Integer getNearSecondWorkerNum() {
		return this.nearSecondWorkerNum;
	}
	
	/**
	 * @param nearFirstPowerNum
	 */
	public void setNearFirstPowerNum(String nearFirstPowerNum) {
		this.nearFirstPowerNum = nearFirstPowerNum;
	}
	
    /**
     * @return nearFirstPowerNum
     */
	public String getNearFirstPowerNum() {
		return this.nearFirstPowerNum;
	}
	
	/**
	 * @param nearFirstGasAmount
	 */
	public void setNearFirstGasAmount(String nearFirstGasAmount) {
		this.nearFirstGasAmount = nearFirstGasAmount;
	}
	
    /**
     * @return nearFirstGasAmount
     */
	public String getNearFirstGasAmount() {
		return this.nearFirstGasAmount;
	}
	
	/**
	 * @param nearFirstWaterAmount
	 */
	public void setNearFirstWaterAmount(String nearFirstWaterAmount) {
		this.nearFirstWaterAmount = nearFirstWaterAmount;
	}
	
    /**
     * @return nearFirstWaterAmount
     */
	public String getNearFirstWaterAmount() {
		return this.nearFirstWaterAmount;
	}
	
	/**
	 * @param nearFirstWorkerNum
	 */
	public void setNearFirstWorkerNum(Integer nearFirstWorkerNum) {
		this.nearFirstWorkerNum = nearFirstWorkerNum;
	}
	
    /**
     * @return nearFirstWorkerNum
     */
	public Integer getNearFirstWorkerNum() {
		return this.nearFirstWorkerNum;
	}
	
	/**
	 * @param currPowerNum
	 */
	public void setCurrPowerNum(String currPowerNum) {
		this.currPowerNum = currPowerNum;
	}
	
    /**
     * @return currPowerNum
     */
	public String getCurrPowerNum() {
		return this.currPowerNum;
	}
	
	/**
	 * @param currGasAmount
	 */
	public void setCurrGasAmount(String currGasAmount) {
		this.currGasAmount = currGasAmount;
	}
	
    /**
     * @return currGasAmount
     */
	public String getCurrGasAmount() {
		return this.currGasAmount;
	}
	
	/**
	 * @param currWaterAmount
	 */
	public void setCurrWaterAmount(String currWaterAmount) {
		this.currWaterAmount = currWaterAmount;
	}
	
    /**
     * @return currWaterAmount
     */
	public String getCurrWaterAmount() {
		return this.currWaterAmount;
	}
	
	/**
	 * @param currWorkerNum
	 */
	public void setCurrWorkerNum(Integer currWorkerNum) {
		this.currWorkerNum = currWorkerNum;
	}
	
    /**
     * @return currWorkerNum
     */
	public Integer getCurrWorkerNum() {
		return this.currWorkerNum;
	}
	
	/**
	 * @param lastYearPowerNum
	 */
	public void setLastYearPowerNum(String lastYearPowerNum) {
		this.lastYearPowerNum = lastYearPowerNum;
	}
	
    /**
     * @return lastYearPowerNum
     */
	public String getLastYearPowerNum() {
		return this.lastYearPowerNum;
	}
	
	/**
	 * @param lastYearGasAmount
	 */
	public void setLastYearGasAmount(String lastYearGasAmount) {
		this.lastYearGasAmount = lastYearGasAmount;
	}
	
    /**
     * @return lastYearGasAmount
     */
	public String getLastYearGasAmount() {
		return this.lastYearGasAmount;
	}
	
	/**
	 * @param lastYearWaterAmount
	 */
	public void setLastYearWaterAmount(String lastYearWaterAmount) {
		this.lastYearWaterAmount = lastYearWaterAmount;
	}
	
    /**
     * @return lastYearWaterAmount
     */
	public String getLastYearWaterAmount() {
		return this.lastYearWaterAmount;
	}
	
	/**
	 * @param lastYearWorkerNum
	 */
	public void setLastYearWorkerNum(Integer lastYearWorkerNum) {
		this.lastYearWorkerNum = lastYearWorkerNum;
	}
	
    /**
     * @return lastYearWorkerNum
     */
	public Integer getLastYearWorkerNum() {
		return this.lastYearWorkerNum;
	}
	
	/**
	 * @param powerNumRemark
	 */
	public void setPowerNumRemark(String powerNumRemark) {
		this.powerNumRemark = powerNumRemark;
	}
	
    /**
     * @return powerNumRemark
     */
	public String getPowerNumRemark() {
		return this.powerNumRemark;
	}
	
	/**
	 * @param gasAmountreMark
	 */
	public void setGasAmountreMark(String gasAmountreMark) {
		this.gasAmountreMark = gasAmountreMark;
	}
	
    /**
     * @return gasAmountreMark
     */
	public String getGasAmountreMark() {
		return this.gasAmountreMark;
	}
	
	/**
	 * @param waterAmountRemark
	 */
	public void setWaterAmountRemark(String waterAmountRemark) {
		this.waterAmountRemark = waterAmountRemark;
	}
	
    /**
     * @return waterAmountRemark
     */
	public String getWaterAmountRemark() {
		return this.waterAmountRemark;
	}
	
	/**
	 * @param workerNumRemark
	 */
	public void setWorkerNumRemark(String workerNumRemark) {
		this.workerNumRemark = workerNumRemark;
	}
	
    /**
     * @return workerNumRemark
     */
	public String getWorkerNumRemark() {
		return this.workerNumRemark;
	}


}