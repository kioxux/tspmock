package cn.com.yusys.yusp.service.server.xdca0003;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0003.req.Xdca0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0003.resp.Xdca0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditCtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0003Service
 * @类描述: #服务类 大额分期合同列表查询接口
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdca0003Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0003Service.class);

    @Autowired
    private CreditCtrLoanContMapper creditCtrLoanContMapper;

    /**
     * 大额分期合同列表查询接口
     *
     * @param xdca0003DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0003DataRespDto xdca0003(Xdca0003DataReqDto xdca0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value);
        Xdca0003DataRespDto xdca0003DataRespDto = new Xdca0003DataRespDto();
        try {
            String cusName = xdca0003DataReqDto.getCusName();//客户姓名
            String certType = xdca0003DataReqDto.getCertType();//证件类型
            String certCode = xdca0003DataReqDto.getCertCode();//证件号码
            String startNo = xdca0003DataReqDto.getStartNo();//起始记录号
            String queryNum = xdca0003DataReqDto.getQueryNum();//查询记录数

            //分页查询
            int startPageNum = 1;//页码
            int pageSize = 10;//每页条数
            if (StringUtil.isNotEmpty(startNo) && StringUtil.isNotEmpty(queryNum)) {
                startPageNum = Integer.parseInt(startNo);
                pageSize = Integer.parseInt(queryNum);
            }

            //查询合同记录
            Map QueryMap = new HashMap();
            QueryMap.put("cusName", cusName);//客户姓名
            QueryMap.put("certType", certType);//证件类型
            QueryMap.put("certCode", certCode);//证件号码
            QueryMap.put("startPageNum", startPageNum);//起始记录号
            QueryMap.put("pageSize", pageSize);//查询记录数
            //查询总记录数
            logger.info("*********XDCA0003*大额分期合同列表查询接口开始,查询参数为:{}", JSON.toJSONString(QueryMap));
            PageHelper.startPage(startPageNum, pageSize);
            java.util.List<cn.com.yusys.yusp.dto.server.xdca0003.resp.List> list = creditCtrLoanContMapper.selectCreditCtrLoanContByCusName(QueryMap);
            PageHelper.clearPage();
            logger.info("*********XDCA0003*大额分期合同列表查询接口结束,返回参数为:{}", JSON.toJSONString(list));

            //返回信息
            if(list.size()>0){
                xdca0003DataRespDto.setList(list);
                Integer count = list.size();
                xdca0003DataRespDto.setTotalNum(count.toString());
            }else{
                xdca0003DataRespDto.setTotalNum(StringUtils.STRING_ZERROR);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value);
        return xdca0003DataRespDto;
    }

}
