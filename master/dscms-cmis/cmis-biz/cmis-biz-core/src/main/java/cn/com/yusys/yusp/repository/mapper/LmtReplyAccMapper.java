/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyAcc;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplyAccMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtReplyAcc selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtReplyAcc> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtReplyAcc record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtReplyAcc record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtReplyAcc record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtReplyAcc record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 通过入参查询授信台账信息
     *
     * @param queryMap
     * @return
     */
    List<LmtReplyAcc> selectByParams(Map queryMap);

    /**
     * 通过台帐号查询授信台账信息
     *
     * @param accNo
     * @return
     */
    LmtReplyAcc selectByAccNo(String accNo);

    /**
     * 获取当前登陆人名下可进行预授信细化的授信批复台账
     *
     * @param params
     * @return
     */
    List<LmtReplyAcc> getReplyAccForChg(QueryModel params);

    /**
     * @方法名称: selectLmtReplyAccByGrpAccNo
     * @方法描述: 根据集团授信批复台账编号查询所有单一客户的集团成员台账编号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtReplyAcc> selectLmtReplyAccByGrpAccNo(@Param("grpAccNo")String grpAccNo);

    /**
     * 获取余额
     * @param queryMap
     * @return
     */
    BigDecimal getBalance(Map queryMap);

     /**
     * @方法名称：selectBySerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:48
     * @修改记录：修改时间 修改人员 修改时间
    */
    LmtReplyAcc selectBySerno(@Param("serno")String serno);

    /**
     * @方法名称：updateBySerno
     * @方法描述： 根据授信流水号逻辑删除
     * @创建人：xs
     * @创建时间：2021/5/27 19:48
     * @修改记录：修改时间 修改人员 修改时间
     */
    int updateBySerno(@Param("serno")String serno);

    /**
     * 根据批复编号获取批复台账详情信息，
     *
     * @param queryMap
     * @return
     */
    LmtReplyAcc selectAccNoDataByParams(Map queryMap);
    /**
     * 根据客户号查询是否存在授信批复台账
     * @param cusId
     * @return
     */
    LmtReplyAcc queryLmtAppSernoByCusId(String cusId);
    /**
     * 根据批复台账编号更新授信额度
     * @param map
     * @return
     */
    int updateLmtReplyAccAmtByAccNo(Map map);
    /**
     * @函数名称:deleteByAccNoAndNoExitsSub
     * @函数描述:根据授信协议编号关联删除不存在授信分项的授信协议
     * @参数与返回说明:accNo 授信协议编号
     * @算法描述:
     */
    int deleteByAccNoAndNoExitsSub(String accNo);

    /**
     * @方法名称: getLmtReplyAccBySingleSerno
     * @方法描述: 根据申请流水号查询授信批复台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtReplyAcc> getLmtReplyAccBySingleSerno(String grpReplySerno);

    /**
     * @方法名称: selectCountLmtReplayBycusId
     * @方法描述: 查找出该客户是否存在有效的授信协议
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectCountLmtReplayAccBycusId(HashMap<String, String> paramMap);

    /**
     * 获取对公批复
     * @param cusId
     * @return
     */

    List findlmtReplyAccListByCusId(@Param("cusId") String cusId);


    /**
     * 查找出该客户是否存在有效的授信协议（不考虑是否逾期情况）
     * @param cusId
     * @return
     */
    int getCountLmtReplayAccNoDateBycusId(@Param("cusId") String cusId);

}