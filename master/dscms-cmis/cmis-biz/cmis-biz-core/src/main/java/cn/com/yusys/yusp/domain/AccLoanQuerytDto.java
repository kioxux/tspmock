/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoan
 * @类描述: acc_loan数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 15:34:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AccLoanQuerytDto extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 放款流水号 **/
	private String pvpSerno;

	/** 逾期余额 **/
	private java.math.BigDecimal overdueBalance;

	/** 基准利率 **/
	private java.math.BigDecimal rulingIr;

	/** 借据编号 **/
	private String billNo;

	/** 合同编号 **/
	private String contNo;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 产品类型属性 **/
	private String prdTypeProp;

	/** 担保方式 **/
	private String guarMode;

	/** 贷款形式 **/
	private String loanModal;

	/** 贷款发放币种 **/
	private String contCurType;

	/** 汇率 **/
	private java.math.BigDecimal exchangeRate;

	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;

	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;

	/** 折合人民币金额 **/
	private java.math.BigDecimal exchangeRmbAmt;

	/** 折合人民币余额 **/
	private java.math.BigDecimal exchangeRmbBal;

	/** 贷款起始日 **/
	private String loanStartDate;

	/** 贷款到期日 **/
	private String loanEndDate;

	/** 贷款期限 **/
	private String loanTerm;

	/** 贷款期限单位 **/
	private String loanTermUnit;

	/** 展期次数 **/
	private String extTimes;

	/** 逾期天数 **/
	private String overdueDay;

	/** 逾期期数 **/
	private String overdueTimes;

	/** 结清日期 **/
	private String settlDate;

	/** 利率调整方式 **/
	private String rateAdjMode;

	/** 是否分段计息 **/
	private String isSegInterest;

	/** LPR授信利率区间 **/
	private String lprRateIntval;

	/** 当前LPR利率 **/
	private java.math.BigDecimal curtLprRate;

	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;

	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;

	/** 逾期利率浮动比 **/
	private java.math.BigDecimal overdueRatePefloat;

	/** 逾期执行利率(年利率) **/
	private java.math.BigDecimal overdueExecRate;

	/** 复息利率浮动比 **/
	private java.math.BigDecimal ciRatePefloat;

	/** 复息执行利率(年利率) **/
	private java.math.BigDecimal ciExecRate;

	/** 利率调整选项 **/
	private String rateAdjType;

	/** 下一次利率调整间隔 **/
	private String nextRateAdjInterval;

	/** 下一次利率调整间隔单位 **/
	private String nextRateAdjUnit;

	/** 第一次调整日 **/
	private String firstAdjDate;

	/** 还款方式 **/
	private String repayMode;

	/** 结息间隔周期 **/
	private String eiIntervalCycle;

	/** 结息间隔周期单位 **/
	private String eiIntervalUnit;

	/** 扣款方式 **/
	private String deductType;

	/** 扣款日 **/
	private String deductDay;

	/** 贷款发放账号 **/
	private String loanPayoutAccno;

	/** 贷款发放账号子序号 **/
	private String loanPayoutSubNo;

	/** 发放账号名称 **/
	private String payoutAcctName;

	/** 是否受托支付 **/
	private String isBeEntrustedPay;

	/** 贷款还款账号 **/
	private String repayAccno;

	/** 贷款还款账户子序号 **/
	private String repaySubAccno;

	/** 还款账户名称 **/
	private String repayAcctName;

	/** 贷款投向 **/
	private String loanTer;

	/** 借款用途类型 **/
	private String loanUseType;

	/** 科目号 **/
	private String subjectNo;

	/** 农户类型 **/
	private String agriType;

	/** 涉农贷款投向 **/
	private String agriLoanTer;

	/** 贷款承诺标志 **/
	private String loanPromiseFlag;

	/** 贷款承诺类型 **/
	private String loanPromiseType;

	/** 是否贴息 **/
	private String isSbsy;

	/** 贴息人存款账号 **/
	private String sbsyDepAccno;

	/** 贴息比例 **/
	private java.math.BigDecimal sbsyPerc;

	/** 贴息到期日 **/
	private String sbysEnddate;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 批复编号 **/
	private String replyNo;

	/** 贷款类别 **/
	private String loanTypeDetail;

	/** 是否落实贷款 **/
	private String isPactLoan;

	/** 是否绿色产业 **/
	private String isGreenIndustry;

	/** 是否经营性物业贷款 **/
	private String isOperPropertyLoan;

	/** 是否钢贸行业贷款 **/
	private String isSteelLoan;

	/** 是否不锈钢行业贷款 **/
	private String isStainlessLoan;

	/** 是否扶贫贴息贷款 **/
	private String isPovertyReliefLoan;

	/** 是否劳动密集型小企业贴息贷款 **/
	private String isLaborIntenSbsyLoan;

	/** 保障性安居工程贷款 **/
	private String goverSubszHouseLoan;

	/** 项目贷款节能环保 **/
	private String engyEnviProteLoan;

	/** 是否农村综合开发贷款标志 **/
	private String isCphsRurDelpLoan;

	/** 房地产贷款 **/
	private String realproLoan;

	/** 房产开发贷款资本金比例 **/
	private String realproLoanRate;

	/** 担保方式细分 **/
	private String guarDetailMode;

	/** 账务机构编号 **/
	private String finaBrId;

	/** 账务机构名称 **/
	private String finaBrIdName;

	/** 放款机构编号 **/
	private String disbOrgNo;

	/** 放款机构名称 **/
	private String disbOrgName;

	/** 五级分类 **/
	private String fiveClass;

	/** 十级分类 **/
	private String tenClass;

	/** 分类日期 **/
	private String classDate;

	/** 台账状态 **/
	private String accStatus;

	/** 操作类型 **/
	private String oprType;

	/** 所属条线 **/
	private String belgLine;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 主管客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String managerBrId;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 正常本金 **/
	private java.math.BigDecimal zcbjAmt;

	/** 逾期本金 **/
	private java.math.BigDecimal overdueCapAmt;

	/** 欠息 **/
	private java.math.BigDecimal debitInt;

	/** 罚息 **/
	private java.math.BigDecimal penalInt;

	/** 复息 **/
	private java.math.BigDecimal compoundInt;

	/** 核销本金 **/
	private java.math.BigDecimal totalHxbjAmt;

	/** 核销利息 **/
	private java.math.BigDecimal totalHxlxAmt;

	/** 正常利率浮动方式 **/
	private String irFloatType;

	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;

	/** 是否省心E付(STD_ZB_YES_NO) **/
	private String isSxef;

	/** 客户经理姓名 **/
	private String managerIdName;

	/** 责任机构名称 **/
	private String managerBrIdName;



	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	public String getManagerIdName() {
		return managerIdName;
	}

	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return this.pvpSerno;
	}

	/**
	 * @param overdueBalance
	 */
	public void setOverdueBalance(java.math.BigDecimal overdueBalance) {
		this.overdueBalance = overdueBalance;
	}

	/**
	 * @return overdueBalance
	 */
	public java.math.BigDecimal getOverdueBalance() {
		return this.overdueBalance;
	}

	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}

	/**
	 * @return rulingIr
	 */
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	/**
	 * @return billNo
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	/**
	 * @return loanModal
	 */
	public String getLoanModal() {
		return this.loanModal;
	}

	/**
	 * @param contCurType
	 */
	public void setContCurType(String contCurType) {
		this.contCurType = contCurType;
	}

	/**
	 * @return contCurType
	 */
	public String getContCurType() {
		return this.contCurType;
	}

	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * @return exchangeRate
	 */
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	/**
	 * @return loanAmt
	 */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}

	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	/**
	 * @return loanBalance
	 */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}

	/**
	 * @param exchangeRmbAmt
	 */
	public void setExchangeRmbAmt(java.math.BigDecimal exchangeRmbAmt) {
		this.exchangeRmbAmt = exchangeRmbAmt;
	}

	/**
	 * @return exchangeRmbAmt
	 */
	public java.math.BigDecimal getExchangeRmbAmt() {
		return this.exchangeRmbAmt;
	}

	/**
	 * @param exchangeRmbBal
	 */
	public void setExchangeRmbBal(java.math.BigDecimal exchangeRmbBal) {
		this.exchangeRmbBal = exchangeRmbBal;
	}

	/**
	 * @return exchangeRmbBal
	 */
	public java.math.BigDecimal getExchangeRmbBal() {
		return this.exchangeRmbBal;
	}

	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	/**
	 * @return loanStartDate
	 */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}

	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}

	/**
	 * @return loanEndDate
	 */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}

	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	/**
	 * @return loanTerm
	 */
	public String getLoanTerm() {
		return this.loanTerm;
	}

	/**
	 * @param loanTermUnit
	 */
	public void setLoanTermUnit(String loanTermUnit) {
		this.loanTermUnit = loanTermUnit;
	}

	/**
	 * @return loanTermUnit
	 */
	public String getLoanTermUnit() {
		return this.loanTermUnit;
	}

	/**
	 * @param extTimes
	 */
	public void setExtTimes(String extTimes) {
		this.extTimes = extTimes;
	}

	/**
	 * @return extTimes
	 */
	public String getExtTimes() {
		return this.extTimes;
	}

	/**
	 * @param overdueDay
	 */
	public void setOverdueDay(String overdueDay) {
		this.overdueDay = overdueDay;
	}

	/**
	 * @return overdueDay
	 */
	public String getOverdueDay() {
		return this.overdueDay;
	}

	/**
	 * @param overdueTimes
	 */
	public void setOverdueTimes(String overdueTimes) {
		this.overdueTimes = overdueTimes;
	}

	/**
	 * @return overdueTimes
	 */
	public String getOverdueTimes() {
		return this.overdueTimes;
	}

	/**
	 * @param settlDate
	 */
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	/**
	 * @return settlDate
	 */
	public String getSettlDate() {
		return this.settlDate;
	}

	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}

	/**
	 * @return rateAdjMode
	 */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}

	/**
	 * @param isSegInterest
	 */
	public void setIsSegInterest(String isSegInterest) {
		this.isSegInterest = isSegInterest;
	}

	/**
	 * @return isSegInterest
	 */
	public String getIsSegInterest() {
		return this.isSegInterest;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}

	/**
	 * @return lprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	/**
	 * @return curtLprRate
	 */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return rateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return execRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}

	/**
	 * @return overdueRatePefloat
	 */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}

	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}

	/**
	 * @return overdueExecRate
	 */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}

	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}

	/**
	 * @return ciRatePefloat
	 */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}

	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}

	/**
	 * @return ciExecRate
	 */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}

	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}

	/**
	 * @return rateAdjType
	 */
	public String getRateAdjType() {
		return this.rateAdjType;
	}

	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}

	/**
	 * @return nextRateAdjInterval
	 */
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}

	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit;
	}

	/**
	 * @return nextRateAdjUnit
	 */
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}

	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}

	/**
	 * @return firstAdjDate
	 */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}

	/**
	 * @return repayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle;
	}

	/**
	 * @return eiIntervalCycle
	 */
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}

	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit;
	}

	/**
	 * @return eiIntervalUnit
	 */
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}

	/**
	 * @param deductType
	 */
	public void setDeductType(String deductType) {
		this.deductType = deductType;
	}

	/**
	 * @return deductType
	 */
	public String getDeductType() {
		return this.deductType;
	}

	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay;
	}

	/**
	 * @return deductDay
	 */
	public String getDeductDay() {
		return this.deductDay;
	}

	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}

	/**
	 * @return loanPayoutAccno
	 */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}

	/**
	 * @param loanPayoutSubNo
	 */
	public void setLoanPayoutSubNo(String loanPayoutSubNo) {
		this.loanPayoutSubNo = loanPayoutSubNo;
	}

	/**
	 * @return loanPayoutSubNo
	 */
	public String getLoanPayoutSubNo() {
		return this.loanPayoutSubNo;
	}

	/**
	 * @param payoutAcctName
	 */
	public void setPayoutAcctName(String payoutAcctName) {
		this.payoutAcctName = payoutAcctName;
	}

	/**
	 * @return payoutAcctName
	 */
	public String getPayoutAcctName() {
		return this.payoutAcctName;
	}

	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay;
	}

	/**
	 * @return isBeEntrustedPay
	 */
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}

	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno;
	}

	/**
	 * @return repayAccno
	 */
	public String getRepayAccno() {
		return this.repayAccno;
	}

	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno;
	}

	/**
	 * @return repaySubAccno
	 */
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}

	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName;
	}

	/**
	 * @return repayAcctName
	 */
	public String getRepayAcctName() {
		return this.repayAcctName;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}

	/**
	 * @return loanTer
	 */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param loanUseType
	 */
	public void setLoanUseType(String loanUseType) {
		this.loanUseType = loanUseType;
	}

	/**
	 * @return loanUseType
	 */
	public String getLoanUseType() {
		return this.loanUseType;
	}

	/**
	 * @param subjectNo
	 */
	public void setSubjectNo(String subjectNo) {
		this.subjectNo = subjectNo;
	}

	/**
	 * @return subjectNo
	 */
	public String getSubjectNo() {
		return this.subjectNo;
	}

	/**
	 * @param agriType
	 */
	public void setAgriType(String agriType) {
		this.agriType = agriType;
	}

	/**
	 * @return agriType
	 */
	public String getAgriType() {
		return this.agriType;
	}

	/**
	 * @param agriLoanTer
	 */
	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer;
	}

	/**
	 * @return agriLoanTer
	 */
	public String getAgriLoanTer() {
		return this.agriLoanTer;
	}

	/**
	 * @param loanPromiseFlag
	 */
	public void setLoanPromiseFlag(String loanPromiseFlag) {
		this.loanPromiseFlag = loanPromiseFlag;
	}

	/**
	 * @return loanPromiseFlag
	 */
	public String getLoanPromiseFlag() {
		return this.loanPromiseFlag;
	}

	/**
	 * @param loanPromiseType
	 */
	public void setLoanPromiseType(String loanPromiseType) {
		this.loanPromiseType = loanPromiseType;
	}

	/**
	 * @return loanPromiseType
	 */
	public String getLoanPromiseType() {
		return this.loanPromiseType;
	}

	/**
	 * @param isSbsy
	 */
	public void setIsSbsy(String isSbsy) {
		this.isSbsy = isSbsy;
	}

	/**
	 * @return isSbsy
	 */
	public String getIsSbsy() {
		return this.isSbsy;
	}

	/**
	 * @param sbsyDepAccno
	 */
	public void setSbsyDepAccno(String sbsyDepAccno) {
		this.sbsyDepAccno = sbsyDepAccno;
	}

	/**
	 * @return sbsyDepAccno
	 */
	public String getSbsyDepAccno() {
		return this.sbsyDepAccno;
	}

	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

	/**
	 * @return sbsyPerc
	 */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}

	/**
	 * @param sbysEnddate
	 */
	public void setSbysEnddate(String sbysEnddate) {
		this.sbysEnddate = sbysEnddate;
	}

	/**
	 * @return sbysEnddate
	 */
	public String getSbysEnddate() {
		return this.sbysEnddate;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail;
	}

	/**
	 * @return loanTypeDetail
	 */
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}

	/**
	 * @param isPactLoan
	 */
	public void setIsPactLoan(String isPactLoan) {
		this.isPactLoan = isPactLoan;
	}

	/**
	 * @return isPactLoan
	 */
	public String getIsPactLoan() {
		return this.isPactLoan;
	}

	/**
	 * @param isGreenIndustry
	 */
	public void setIsGreenIndustry(String isGreenIndustry) {
		this.isGreenIndustry = isGreenIndustry;
	}

	/**
	 * @return isGreenIndustry
	 */
	public String getIsGreenIndustry() {
		return this.isGreenIndustry;
	}

	/**
	 * @param isOperPropertyLoan
	 */
	public void setIsOperPropertyLoan(String isOperPropertyLoan) {
		this.isOperPropertyLoan = isOperPropertyLoan;
	}

	/**
	 * @return isOperPropertyLoan
	 */
	public String getIsOperPropertyLoan() {
		return this.isOperPropertyLoan;
	}

	/**
	 * @param isSteelLoan
	 */
	public void setIsSteelLoan(String isSteelLoan) {
		this.isSteelLoan = isSteelLoan;
	}

	/**
	 * @return isSteelLoan
	 */
	public String getIsSteelLoan() {
		return this.isSteelLoan;
	}

	/**
	 * @param isStainlessLoan
	 */
	public void setIsStainlessLoan(String isStainlessLoan) {
		this.isStainlessLoan = isStainlessLoan;
	}

	/**
	 * @return isStainlessLoan
	 */
	public String getIsStainlessLoan() {
		return this.isStainlessLoan;
	}

	/**
	 * @param isPovertyReliefLoan
	 */
	public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
		this.isPovertyReliefLoan = isPovertyReliefLoan;
	}

	/**
	 * @return isPovertyReliefLoan
	 */
	public String getIsPovertyReliefLoan() {
		return this.isPovertyReliefLoan;
	}

	/**
	 * @param isLaborIntenSbsyLoan
	 */
	public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
		this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
	}

	/**
	 * @return isLaborIntenSbsyLoan
	 */
	public String getIsLaborIntenSbsyLoan() {
		return this.isLaborIntenSbsyLoan;
	}

	/**
	 * @param goverSubszHouseLoan
	 */
	public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
		this.goverSubszHouseLoan = goverSubszHouseLoan;
	}

	/**
	 * @return goverSubszHouseLoan
	 */
	public String getGoverSubszHouseLoan() {
		return this.goverSubszHouseLoan;
	}

	/**
	 * @param engyEnviProteLoan
	 */
	public void setEngyEnviProteLoan(String engyEnviProteLoan) {
		this.engyEnviProteLoan = engyEnviProteLoan;
	}

	/**
	 * @return engyEnviProteLoan
	 */
	public String getEngyEnviProteLoan() {
		return this.engyEnviProteLoan;
	}

	/**
	 * @param isCphsRurDelpLoan
	 */
	public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
		this.isCphsRurDelpLoan = isCphsRurDelpLoan;
	}

	/**
	 * @return isCphsRurDelpLoan
	 */
	public String getIsCphsRurDelpLoan() {
		return this.isCphsRurDelpLoan;
	}

	/**
	 * @param realproLoan
	 */
	public void setRealproLoan(String realproLoan) {
		this.realproLoan = realproLoan;
	}

	/**
	 * @return realproLoan
	 */
	public String getRealproLoan() {
		return this.realproLoan;
	}

	/**
	 * @param realproLoanRate
	 */
	public void setRealproLoanRate(String realproLoanRate) {
		this.realproLoanRate = realproLoanRate;
	}

	/**
	 * @return realproLoanRate
	 */
	public String getRealproLoanRate() {
		return this.realproLoanRate;
	}

	/**
	 * @param guarDetailMode
	 */
	public void setGuarDetailMode(String guarDetailMode) {
		this.guarDetailMode = guarDetailMode;
	}

	/**
	 * @return guarDetailMode
	 */
	public String getGuarDetailMode() {
		return this.guarDetailMode;
	}

	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	/**
	 * @return finaBrId
	 */
	public String getFinaBrId() {
		return this.finaBrId;
	}

	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}

	/**
	 * @return finaBrIdName
	 */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}

	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo;
	}

	/**
	 * @return disbOrgNo
	 */
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}

	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName;
	}

	/**
	 * @return disbOrgName
	 */
	public String getDisbOrgName() {
		return this.disbOrgName;
	}

	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	/**
	 * @return fiveClass
	 */
	public String getFiveClass() {
		return this.fiveClass;
	}

	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}

	/**
	 * @return tenClass
	 */
	public String getTenClass() {
		return this.tenClass;
	}

	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}

	/**
	 * @return classDate
	 */
	public String getClassDate() {
		return this.classDate;
	}

	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

	/**
	 * @return accStatus
	 */
	public String getAccStatus() {
		return this.accStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param zcbjAmt
	 */
	public void setZcbjAmt(java.math.BigDecimal zcbjAmt) {
		this.zcbjAmt = zcbjAmt;
	}

	/**
	 * @return zcbjAmt
	 */
	public java.math.BigDecimal getZcbjAmt() {
		return this.zcbjAmt;
	}

	/**
	 * @param overdueCapAmt
	 */
	public void setOverdueCapAmt(java.math.BigDecimal overdueCapAmt) {
		this.overdueCapAmt = overdueCapAmt;
	}

	/**
	 * @return overdueCapAmt
	 */
	public java.math.BigDecimal getOverdueCapAmt() {
		return this.overdueCapAmt;
	}

	/**
	 * @param debitInt
	 */
	public void setDebitInt(java.math.BigDecimal debitInt) {
		this.debitInt = debitInt;
	}

	/**
	 * @return debitInt
	 */
	public java.math.BigDecimal getDebitInt() {
		return this.debitInt;
	}

	/**
	 * @param penalInt
	 */
	public void setPenalInt(java.math.BigDecimal penalInt) {
		this.penalInt = penalInt;
	}

	/**
	 * @return penalInt
	 */
	public java.math.BigDecimal getPenalInt() {
		return this.penalInt;
	}

	/**
	 * @param compoundInt
	 */
	public void setCompoundInt(java.math.BigDecimal compoundInt) {
		this.compoundInt = compoundInt;
	}

	/**
	 * @return compoundInt
	 */
	public java.math.BigDecimal getCompoundInt() {
		return this.compoundInt;
	}

	/**
	 * @param totalHxbjAmt
	 */
	public void setTotalHxbjAmt(java.math.BigDecimal totalHxbjAmt) {
		this.totalHxbjAmt = totalHxbjAmt;
	}

	/**
	 * @return totalHxbjAmt
	 */
	public java.math.BigDecimal getTotalHxbjAmt() {
		return this.totalHxbjAmt;
	}

	/**
	 * @param totalHxlxAmt
	 */
	public void setTotalHxlxAmt(java.math.BigDecimal totalHxlxAmt) {
		this.totalHxlxAmt = totalHxlxAmt;
	}

	/**
	 * @return totalHxlxAmt
	 */
	public java.math.BigDecimal getTotalHxlxAmt() {
		return this.totalHxlxAmt;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}

	/**
	 * @return irFloatType
	 */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	/**
	 * @return irFloatRate
	 */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param isSxef
	 */
	public void setIsSxef(String isSxef) {
		this.isSxef = isSxef;
	}

	/**
	 * @return isSxef
	 */
	public String getIsSxef() {
		return this.isSxef;
	}


}