/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.BizCorreManagerInfo;
import cn.com.yusys.yusp.domain.CtrLoanExt;
import cn.com.yusys.yusp.domain.IqpLoanExtApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.IqpLoanExtAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanExtAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-11 16:25:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpLoanExtAppService {

    private final Logger log = LoggerFactory.getLogger(IqpLoanExtAppService.class);
    @Autowired
    private IqpLoanExtAppMapper iqpLoanExtAppMapper;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private CtrLoanExtService ctrLoanExtService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private BizCorreManagerInfoService bizCorreManagerInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLoanExtApp selectByPrimaryKey(String extSerno) {
        return iqpLoanExtAppMapper.selectByPrimaryKey(extSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanExtApp> selectAll(QueryModel model) {
        List<IqpLoanExtApp> records = (List<IqpLoanExtApp>) iqpLoanExtAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpLoanExtApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanExtApp> list = iqpLoanExtAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpLoanExtApp record) {
        return iqpLoanExtAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanExtApp record) {
        return iqpLoanExtAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpLoanExtApp record) {
        return iqpLoanExtAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanExtApp record) {
        return iqpLoanExtAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String extSerno) {
        return iqpLoanExtAppMapper.deleteByPrimaryKey(extSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanExtAppMapper.deleteByIds(ids);
    }

    /**
     * 新增业务展期信息{}
     * @param iqpLoanExtApp
     */
    public void addIqpLoanExtApp(IqpLoanExtApp iqpLoanExtApp) {
        //新增业务展期信息 -> 更新原借据展期信息
        //没有save方法(- -!)  先删除后新增
        String extSerno = iqpLoanExtApp.getExtSerno();
        if(StringUtils.isBlank(extSerno)){
            throw new YuspException(EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value + "extSerno:" + extSerno);
        }
        //如果为空,就新增,不为空就update;
        IqpLoanExtApp tempIqpLoanExtApp = iqpLoanExtAppMapper.selectByPrimaryKey(extSerno);

        if(tempIqpLoanExtApp != null){
            iqpLoanExtAppMapper.deleteByPrimaryKey(extSerno);
        }
        //同步修改原来借据的展期次数
        int count = iqpLoanExtAppMapper.insertSelective(iqpLoanExtApp);
        if(count>0){
            String billNo = iqpLoanExtApp.getOldBillNo();
            if(StringUtils.nonBlank(billNo)){
                AccLoan accLoan = accLoanService.selectByPrimaryKey(billNo);
                /**
                 * zhanyb修改20210427。以下为原内容
                 accLoan.setPostCount(new BigDecimal("1"));
                 */
                accLoan.setExtTimes("1");
                accLoanService.update(accLoan);
            } else {
                throw new YuspException(EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value + "billNo:" + billNo);
            }
        }

        if(tempIqpLoanExtApp == null){
            //展期办理人员 登记
            log.info("同步更新办理人员表"+iqpLoanExtApp.getExtSerno()+"-获取当前登录用户数据");
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo==null){
                throw new YuspException(EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key, EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value);
            }
            BizCorreManagerInfo bizCorreManagerInfo = new BizCorreManagerInfo();
            bizCorreManagerInfo.setCorreMgrType(CmisBizConstants.CORRE_REL_TYPE_MAIN);
            bizCorreManagerInfo.setBizSerno(iqpLoanExtApp.getExtSerno());
            bizCorreManagerInfo.setCorreManagerId(userInfo.getLoginCode());
            bizCorreManagerInfo.setCorreManagerBrId(userInfo.getOrg().getCode());
            bizCorreManagerInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            bizCorreManagerInfo.setPkId(StringUtils.uuid(true));
            int insertCount = bizCorreManagerInfoService.insertSelective(bizCorreManagerInfo);
            if(insertCount<0){
                throw new YuspException(EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.key, EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.value);
            }
        }
    }

    /**
     * 删除业务展期信息
     * @param iqpLoanExtApp
     */
    public void deleteIqpLoanExtAppInfo(IqpLoanExtApp iqpLoanExtApp) {
        String extSerno = iqpLoanExtApp.getExtSerno();
        String billNo = iqpLoanExtApp.getOldBillNo();
        if(StringUtils.isBlank(extSerno)){
            throw new YuspException(EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value + "extSerno:" + extSerno);
        }
        if(StringUtils.isBlank(billNo)){
            throw new YuspException(EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value + "billNo:" + billNo);
        }
        iqpLoanExtApp.setOprType("02");
        int count = iqpLoanExtAppMapper.updateByPrimaryKeySelective(iqpLoanExtApp);
        if(count>0){
            AccLoan accLoan = accLoanService.selectByPrimaryKey(billNo);

            /**
             * zhanyb修改20210427。以下为原内容
             accLoan.setPostCount(new BigDecimal("0"));
             */
            accLoan.setExtTimes("0");
            accLoanService.update(accLoan);
        }
    }

    /**
     * 1.更新业务申请状态为通过  997
     * 2.插入协议表
     * @param extSerno
     */
    public void handleBusinessDataAfterEnd(String extSerno) {

        //更新业务信息为通过
        IqpLoanExtApp iqpLoanExtApp = iqpLoanExtAppMapper.selectByPrimaryKey(extSerno);
        iqpLoanExtApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        iqpLoanExtAppMapper.updateByPrimaryKeySelective(iqpLoanExtApp);
        log.info("业务展期通过完成{} "+ JSON.toJSONString(iqpLoanExtApp));
        //什么都不管先copy
        CtrLoanExt ctrLoanExt = new CtrLoanExt();
        BeanUtils.copyProperties(iqpLoanExtApp,ctrLoanExt);

        //生成协议编号    设置 合同状态为待生效  未生效
        String oldContNo = iqpLoanExtApp.getOldContNo();
        String bizType = oldContNo.substring(2,4);
        Map<String,String> param = new HashMap<>();
        param.put("bizType", bizType);
        String extCtrNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, param);
        ctrLoanExt.setExtCtrNo(extCtrNo);
        //合同状态为待生效
        ctrLoanExt.setExtCtrStatus(CmisBizConstants.EXT_CTR_STATUS_001);
        ctrLoanExtService.insertSelective(ctrLoanExt);
        log.info("业务展期生成展期协议完成{} " + JSON.toJSONString(ctrLoanExt));

        //展期协议办理人员 登记
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("bizSerno", iqpLoanExtApp.getExtSerno());
        paramMap.put("correMgrType", CmisBizConstants.CORRE_REL_TYPE_MAIN);


        List<BizCorreManagerInfo> bizCorreManagerInfos = bizCorreManagerInfoService.selectBizCorreManagerInfoByBizSerno(paramMap);
        if (CollectionUtils.isEmpty(bizCorreManagerInfos)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.key, EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.value);
        }
        BizCorreManagerInfo bizCorreManagerInfo = bizCorreManagerInfos.get(0);

        log.info("同步更新办理人员表" + iqpLoanExtApp.getExtSerno() + "-获取当前登录用户数据");

        bizCorreManagerInfo.setBizSerno(ctrLoanExt.getExtCtrNo());
        bizCorreManagerInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        bizCorreManagerInfo.setPkId(StringUtils.uuid(true));
        int insertCount = bizCorreManagerInfoService.insertSelective(bizCorreManagerInfo);
        if(insertCount<0){
            throw new YuspException(EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.key, EcbEnum.E_IQP_INSERT_BIZMANA_FAILED.value);
        }
    }
    /**
     * 删除业务展期信息
     * @param contSerno
     * @创建人：周茂伟
     */
    public int deleteIqpLoanExtByContSerno(String contSerno) {
        int result=0;
        result=iqpLoanExtAppMapper.deleteByContSerno(contSerno);
        return result;
    }
    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLoanExtApp selectByBillNo(IqpLoanExtApp iqpLoanExtApp) {
        return iqpLoanExtAppMapper.selectByBillNo(iqpLoanExtApp);
    }

    /**
     * @方法名称: saveIqpLoanExtApp
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int saveIqpLoanExtApp(IqpLoanExtApp record) {
        int count=0;
        if(record !=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
                IqpLoanExtApp iqpLoanExtApp=selectByBillNo(record);
                String serno = StringUtils.getUUID();
                if(iqpLoanExtApp != null){
                    record.setUpdId(inputId);
                    record.setUpdBrId(inputBrId);
                    record.setUpdDate(inputDate);
                    count=iqpLoanExtAppMapper.updateByPrimaryKey(record);
                }else {
                    record.setSerno(serno);
                    record.setInputId(inputId);
                    record.setInputBrId(inputBrId);
                    record.setInputDate(inputDate);
                    record.setUpdId(inputId);
                    record.setUpdBrId(inputBrId);
                    record.setUpdDate(inputDate);
                    count =iqpLoanExtAppMapper.insert(record);
                }

        }
        return count;
    }
}
