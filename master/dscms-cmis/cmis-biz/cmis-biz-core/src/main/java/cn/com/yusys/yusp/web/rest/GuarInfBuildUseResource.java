/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBuildUse;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfBuildUse;
import cn.com.yusys.yusp.service.GuarInfBuildUseService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuildUseResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfbuilduse")
public class GuarInfBuildUseResource {
    @Autowired
    private GuarInfBuildUseService guarInfBuildUseService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfBuildUse>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfBuildUse> list = guarInfBuildUseService.selectAll(queryModel);
        return new ResultDto<List<GuarInfBuildUse>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfBuildUse>> index(QueryModel queryModel) {
        List<GuarInfBuildUse> list = guarInfBuildUseService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfBuildUse>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfBuildUse> show(@PathVariable("serno") String serno) {
        GuarInfBuildUse guarInfBuildUse = guarInfBuildUseService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfBuildUse>(guarInfBuildUse);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfBuildUse> create(@RequestBody GuarInfBuildUse guarInfBuildUse) throws URISyntaxException {
        guarInfBuildUseService.insert(guarInfBuildUse);
        return new ResultDto<GuarInfBuildUse>(guarInfBuildUse);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfBuildUse guarInfBuildUse) throws URISyntaxException {
        int result = guarInfBuildUseService.update(guarInfBuildUse);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfBuildUseService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfBuildUseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfBuildUse
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfBuildUse")
    protected ResultDto<Integer> preserveGuarInfBuildUse(@RequestBody GuarInfBuildUse guarInfBuildUse) {
        int result = guarInfBuildUseService.preserveGuarInfBuildUse(guarInfBuildUse);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfBuildUse> queryBySerno(@RequestBody GuarInfBuildUse record) {
        GuarInfBuildUse guarInfBuildUse = guarInfBuildUseService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfBuildUse>(guarInfBuildUse);
    }
}
