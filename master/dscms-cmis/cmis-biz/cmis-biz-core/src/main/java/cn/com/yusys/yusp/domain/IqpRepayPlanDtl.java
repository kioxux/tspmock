/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpRepayPlanDtl
 * @类描述: iqp_repay_plan_dtl数据实体类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-03 15:10:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_plan_dtl")
public class IqpRepayPlanDtl extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 期数 **/
	@Column(name = "TIMES", unique = false, nullable = true, length = 10)
	private Integer times;
	
	/** 计划还款日期 **/
	@Column(name = "PLAN_REPAY_DATE", unique = false, nullable = true, length = 10)
	private String planRepayDate;
	
	/** 计划应还款本金 **/
	@Column(name = "PLAN_AMT", unique = false, nullable = true, length = 10)
	private BigDecimal planAmt;

	/** 状态 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param times
	 */
	public void setTimes(Integer times) {
		this.times = times;
	}
	
    /**
     * @return times
     */
	public Integer getTimes() {
		return this.times;
	}
	
	/**
	 * @param planRepayDate
	 */
	public void setPlanRepayDate(String planRepayDate) {
		this.planRepayDate = planRepayDate;
	}
	
    /**
     * @return planRepayDate
     */
	public String getPlanRepayDate() {
		return this.planRepayDate;
	}
	
	/**
	 * @param planAmt
	 */
	public void setPlanAmt(BigDecimal planAmt) {
		this.planAmt = planAmt;
	}
	
    /**
     * @return planAmt
     */
	public BigDecimal getPlanAmt() {
		return this.planAmt;
	}


	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
}