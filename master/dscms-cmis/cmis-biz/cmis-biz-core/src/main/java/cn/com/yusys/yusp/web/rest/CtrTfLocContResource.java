/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrDiscCont;
import cn.com.yusys.yusp.domain.CtrTfLocCont;
import cn.com.yusys.yusp.service.CtrTfLocContService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrTfLocContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-14 11:16:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "开证合同")
@RequestMapping("/api/ctrtfloccont")
public class CtrTfLocContResource {
    @Autowired
    private CtrTfLocContService ctrTfLocContService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrTfLocCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrTfLocCont> list = ctrTfLocContService.selectAll(queryModel);
        return new ResultDto<List<CtrTfLocCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrTfLocCont>> index(QueryModel queryModel) {
        List<CtrTfLocCont> list = ctrTfLocContService.selectByModel(queryModel);
        return new ResultDto<List<CtrTfLocCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrTfLocCont> show(@PathVariable("pkId") String pkId) {
        CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrTfLocCont>(ctrTfLocCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrTfLocCont> create(@RequestBody CtrTfLocCont ctrTfLocCont) throws URISyntaxException {
        ctrTfLocContService.insert(ctrTfLocCont);
        return new ResultDto<CtrTfLocCont>(ctrTfLocCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrTfLocCont ctrTfLocCont) throws URISyntaxException {
        int result = ctrTfLocContService.update(ctrTfLocCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrTfLocContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrTfLocContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:待签定列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrTfLocCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrTfLocCont> list = ctrTfLocContService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrTfLocCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrTfLocCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrTfLocCont> list = ctrTfLocContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrTfLocCont>>(list);
    }

    /**
     * 合同签订保存方法
     * @param ctrTfLocCont
     * @return
     */
    @ApiOperation("合同签订保存方法")
    @PostMapping("/onSign")
    protected ResultDto<Integer> onSign(@RequestBody CtrTfLocCont ctrTfLocCont) {
        Integer result   = ctrTfLocContService.onSign(ctrTfLocCont);
        return new ResultDto<Integer>(result);
    }

    /**
     * 合同注销保存方法
     * @param params
     * @return
     */
    @ApiOperation("合同注销保存方法")
    @PostMapping("/updateLogout")
    protected ResultDto<Map> updateLogout(@RequestBody Map params) throws ParseException {
        Map rtnData   = ctrTfLocContService.updateLogout(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrTfLocCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrTfLocCont> list = ctrTfLocContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrTfLocCont>>(list);
    }

    /**
     * @函数名称:queryCtrTfLocContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrtfloccontdatabyserno")
    protected ResultDto<CtrTfLocCont> queryCtrTfLocContDataBySerno(@RequestBody String serno) {
        CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByIqpSerno(serno);
        return new ResultDto<CtrTfLocCont>(ctrTfLocCont);
    }
}
