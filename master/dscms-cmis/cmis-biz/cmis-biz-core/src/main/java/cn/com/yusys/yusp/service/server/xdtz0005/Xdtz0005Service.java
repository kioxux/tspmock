package cn.com.yusys.yusp.service.server.xdtz0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0005.req.Xdtz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.resp.Xdtz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:根据流水号查询是否放款标记
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0005Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0005DataRespDto xdtz0005(Xdtz0005DataReqDto xdtz0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataReqDto));
        Xdtz0005DataRespDto xdtz0005DataRespDto = new Xdtz0005DataRespDto();
        int record = 0;//查询结果
        try {
            String SurveySerno = xdtz0005DataReqDto.getSerno();//业务流水
            //查询参数
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("SurveySerno",SurveySerno);
            logger.info("根据流水号查询是否放款标记开始,查询参数为:{}", JSON.toJSONString(queryModel));
            record = ctrLoanContMapper.queryishaveLoanRecordBySerno(queryModel);
            logger.info("根据流水号查询是否放款标记结束,反回参数为:{}", JSON.toJSONString(record));
            if (record > 0) {//存在放款
                xdtz0005DataRespDto.setIsDisb(DscmsBizTzEnum.YESNO_YES.key);
            } else {
                xdtz0005DataRespDto.setIsDisb(DscmsBizTzEnum.YESNO_NO.key);
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataRespDto));
        return xdtz0005DataRespDto;
    }
}
