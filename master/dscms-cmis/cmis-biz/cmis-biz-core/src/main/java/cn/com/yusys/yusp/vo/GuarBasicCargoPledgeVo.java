package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfBuilProject;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;

public class GuarBasicCargoPledgeVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfCargoPledge guarInfCargoPledge;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfCargoPledge getGuarInfCargoPledge() {
        return guarInfCargoPledge;
    }

    public void setGuarInfCargoPledge(GuarInfCargoPledge guarInfCargoPledge) {
        this.guarInfCargoPledge = guarInfCargoPledge;
    }
}
