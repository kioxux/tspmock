package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPerferRateApplyInfo
 * @类描述: lmt_perfer_rate_apply_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-13 22:56:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtPerferRateApplyInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查流水号 **/
	private String surveySerno;

	/** 第三方业务流水号 **/
	private String bizSerno;
	
	/** 申请利率 **/
	private java.math.BigDecimal appRate;
	
	/** 上期贷款方式 **/
	private String preLoanMode;
	
	/** 上期贷款金额 **/
	private java.math.BigDecimal preLoanAmt;
	
	/** 上期执行年利率 **/
	private java.math.BigDecimal preExeYearRate;
	
	/** 上期贷款期限 **/
	private String preLoanTerm;
	
	/** 客户经理意见 **/
	private String managerIdAdvice;
	
	/** 客户经理其他意见 **/
	private String managerIdOtherAdvice;
	
	/** 其他原因 **/
	private String otherResn;
	
	/** 批复利率 **/
	private java.math.BigDecimal replyRate;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 拒绝原因 **/
	private String refuseReasons;
	
	/** 审批节点类型 **/
	private String approveNodeType;
	
	/** 当前审批节点 **/
	private String curtApproveNode;
	
	/** 当前审批人工号 **/
	private String curtApprIdJobNo;
	
	/** 申请时间 **/
	private String appTime;
	
	/** 是否存量用户  **/
	private String whetherStockUser;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
	/**
	 * @param appRate
	 */
	public void setAppRate(java.math.BigDecimal appRate) {
		this.appRate = appRate;
	}
	
    /**
     * @return AppRate
     */	
	public java.math.BigDecimal getAppRate() {
		return this.appRate;
	}
	
	/**
	 * @param preLoanMode
	 */
	public void setPreLoanMode(String preLoanMode) {
		this.preLoanMode = preLoanMode == null ? null : preLoanMode.trim();
	}
	
    /**
     * @return PreLoanMode
     */	
	public String getPreLoanMode() {
		return this.preLoanMode;
	}
	
	/**
	 * @param preLoanAmt
	 */
	public void setPreLoanAmt(java.math.BigDecimal preLoanAmt) {
		this.preLoanAmt = preLoanAmt;
	}
	
    /**
     * @return PreLoanAmt
     */	
	public java.math.BigDecimal getPreLoanAmt() {
		return this.preLoanAmt;
	}
	
	/**
	 * @param preExeYearRate
	 */
	public void setPreExeYearRate(java.math.BigDecimal preExeYearRate) {
		this.preExeYearRate = preExeYearRate;
	}
	
    /**
     * @return PreExeYearRate
     */	
	public java.math.BigDecimal getPreExeYearRate() {
		return this.preExeYearRate;
	}
	
	/**
	 * @param preLoanTerm
	 */
	public void setPreLoanTerm(String preLoanTerm) {
		this.preLoanTerm = preLoanTerm == null ? null : preLoanTerm.trim();
	}
	
    /**
     * @return PreLoanTerm
     */	
	public String getPreLoanTerm() {
		return this.preLoanTerm;
	}
	
	/**
	 * @param managerIdAdvice
	 */
	public void setManagerIdAdvice(String managerIdAdvice) {
		this.managerIdAdvice = managerIdAdvice == null ? null : managerIdAdvice.trim();
	}
	
    /**
     * @return ManagerIdAdvice
     */	
	public String getManagerIdAdvice() {
		return this.managerIdAdvice;
	}
	
	/**
	 * @param managerIdOtherAdvice
	 */
	public void setManagerIdOtherAdvice(String managerIdOtherAdvice) {
		this.managerIdOtherAdvice = managerIdOtherAdvice == null ? null : managerIdOtherAdvice.trim();
	}
	
    /**
     * @return ManagerIdOtherAdvice
     */	
	public String getManagerIdOtherAdvice() {
		return this.managerIdOtherAdvice;
	}
	
	/**
	 * @param otherResn
	 */
	public void setOtherResn(String otherResn) {
		this.otherResn = otherResn == null ? null : otherResn.trim();
	}
	
    /**
     * @return OtherResn
     */	
	public String getOtherResn() {
		return this.otherResn;
	}
	
	/**
	 * @param replyRate
	 */
	public void setReplyRate(java.math.BigDecimal replyRate) {
		this.replyRate = replyRate;
	}
	
    /**
     * @return ReplyRate
     */	
	public java.math.BigDecimal getReplyRate() {
		return this.replyRate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param refuseReasons
	 */
	public void setRefuseReasons(String refuseReasons) {
		this.refuseReasons = refuseReasons == null ? null : refuseReasons.trim();
	}
	
    /**
     * @return RefuseReasons
     */	
	public String getRefuseReasons() {
		return this.refuseReasons;
	}
	
	/**
	 * @param approveNodeType
	 */
	public void setApproveNodeType(String approveNodeType) {
		this.approveNodeType = approveNodeType == null ? null : approveNodeType.trim();
	}
	
    /**
     * @return ApproveNodeType
     */	
	public String getApproveNodeType() {
		return this.approveNodeType;
	}
	
	/**
	 * @param curtApproveNode
	 */
	public void setCurtApproveNode(String curtApproveNode) {
		this.curtApproveNode = curtApproveNode == null ? null : curtApproveNode.trim();
	}
	
    /**
     * @return CurtApproveNode
     */	
	public String getCurtApproveNode() {
		return this.curtApproveNode;
	}
	
	/**
	 * @param curtApprIdJobNo
	 */
	public void setCurtApprIdJobNo(String curtApprIdJobNo) {
		this.curtApprIdJobNo = curtApprIdJobNo == null ? null : curtApprIdJobNo.trim();
	}
	
    /**
     * @return CurtApprIdJobNo
     */	
	public String getCurtApprIdJobNo() {
		return this.curtApprIdJobNo;
	}
	
	/**
	 * @param appTime
	 */
	public void setAppTime(String appTime) {
		this.appTime = appTime == null ? null : appTime.trim();
	}
	
    /**
     * @return AppTime
     */	
	public String getAppTime() {
		return this.appTime;
	}
	
	/**
	 * @param whetherStockUser
	 */
	public void setWhetherStockUser(String whetherStockUser) {
		this.whetherStockUser = whetherStockUser == null ? null : whetherStockUser.trim();
	}
	
    /**
     * @return WhetherStockUser
     */	
	public String getWhetherStockUser() {
		return this.whetherStockUser;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}
