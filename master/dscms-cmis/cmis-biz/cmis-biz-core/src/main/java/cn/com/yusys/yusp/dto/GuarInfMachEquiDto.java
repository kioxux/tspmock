package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMachEqui
 * @类描述: guar_inf_mach_equi数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:09:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfMachEquiDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 设备铭牌编号 **/
	private String equipNo;
	
	/** 一手/二手标识 STD_ZB_YESBS **/
	private String isUsed;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 设备类型 STD_ZB_SBLX **/
	private String machineType;
	
	/** 设备分类 STD_ZB_SBFL **/
	private String machineCode;
	
	/** 型号/规格 **/
	private String specModel;
	
	/** 品牌/生产厂商 **/
	private String vehicleBrand;
	
	/** 出厂日期或报关日期 **/
	private String factoryDate;
	
	/** 设计使用到期日期 **/
	private String matureDate;
	
	/** 是否有产品合格证 **/
	private String indEligibleCerti;
	
	/** 发票编号 **/
	private String invoiceNo;
	
	/** 发票日期 **/
	private String invoiceDate;
	
	/** 发票金额（元） **/
	private java.math.BigDecimal buyPrice;
	
	/** 设备名称 **/
	private String equipName;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 押品使用情况 **/
	private String guarUtilCase;
	
	/** 设备数量 **/
	private String equipQnt;
	
	/** 设备存放地址 **/
	private String equipDepo;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param equipNo
	 */
	public void setEquipNo(String equipNo) {
		this.equipNo = equipNo == null ? null : equipNo.trim();
	}
	
    /**
     * @return EquipNo
     */	
	public String getEquipNo() {
		return this.equipNo;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed == null ? null : isUsed.trim();
	}
	
    /**
     * @return IsUsed
     */	
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param machineType
	 */
	public void setMachineType(String machineType) {
		this.machineType = machineType == null ? null : machineType.trim();
	}
	
    /**
     * @return MachineType
     */	
	public String getMachineType() {
		return this.machineType;
	}
	
	/**
	 * @param machineCode
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode == null ? null : machineCode.trim();
	}
	
    /**
     * @return MachineCode
     */	
	public String getMachineCode() {
		return this.machineCode;
	}
	
	/**
	 * @param specModel
	 */
	public void setSpecModel(String specModel) {
		this.specModel = specModel == null ? null : specModel.trim();
	}
	
    /**
     * @return SpecModel
     */	
	public String getSpecModel() {
		return this.specModel;
	}
	
	/**
	 * @param vehicleBrand
	 */
	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand == null ? null : vehicleBrand.trim();
	}
	
    /**
     * @return VehicleBrand
     */	
	public String getVehicleBrand() {
		return this.vehicleBrand;
	}
	
	/**
	 * @param factoryDate
	 */
	public void setFactoryDate(String factoryDate) {
		this.factoryDate = factoryDate == null ? null : factoryDate.trim();
	}
	
    /**
     * @return FactoryDate
     */	
	public String getFactoryDate() {
		return this.factoryDate;
	}
	
	/**
	 * @param matureDate
	 */
	public void setMatureDate(String matureDate) {
		this.matureDate = matureDate == null ? null : matureDate.trim();
	}
	
    /**
     * @return MatureDate
     */	
	public String getMatureDate() {
		return this.matureDate;
	}
	
	/**
	 * @param indEligibleCerti
	 */
	public void setIndEligibleCerti(String indEligibleCerti) {
		this.indEligibleCerti = indEligibleCerti == null ? null : indEligibleCerti.trim();
	}
	
    /**
     * @return IndEligibleCerti
     */	
	public String getIndEligibleCerti() {
		return this.indEligibleCerti;
	}
	
	/**
	 * @param invoiceNo
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo == null ? null : invoiceNo.trim();
	}
	
    /**
     * @return InvoiceNo
     */	
	public String getInvoiceNo() {
		return this.invoiceNo;
	}
	
	/**
	 * @param invoiceDate
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate == null ? null : invoiceDate.trim();
	}
	
    /**
     * @return InvoiceDate
     */	
	public String getInvoiceDate() {
		return this.invoiceDate;
	}
	
	/**
	 * @param buyPrice
	 */
	public void setBuyPrice(java.math.BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	
    /**
     * @return BuyPrice
     */	
	public java.math.BigDecimal getBuyPrice() {
		return this.buyPrice;
	}
	
	/**
	 * @param equipName
	 */
	public void setEquipName(String equipName) {
		this.equipName = equipName == null ? null : equipName.trim();
	}
	
    /**
     * @return EquipName
     */	
	public String getEquipName() {
		return this.equipName;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase == null ? null : guarUtilCase.trim();
	}
	
    /**
     * @return GuarUtilCase
     */	
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param equipQnt
	 */
	public void setEquipQnt(String equipQnt) {
		this.equipQnt = equipQnt == null ? null : equipQnt.trim();
	}
	
    /**
     * @return EquipQnt
     */	
	public String getEquipQnt() {
		return this.equipQnt;
	}
	
	/**
	 * @param equipDepo
	 */
	public void setEquipDepo(String equipDepo) {
		this.equipDepo = equipDepo == null ? null : equipDepo.trim();
	}
	
    /**
     * @return EquipDepo
     */	
	public String getEquipDepo() {
		return this.equipDepo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}