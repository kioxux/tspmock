package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.IqpAppDiscContDetails;
import cn.com.yusys.yusp.domain.IqpAppMain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IqpMainDiscDto implements Serializable {
    private static final long serialVersionUID = 1L;



    //    /** 主键 **/
//    private String pkId;
//
//    /** 流水号 **/
//    private String serno;
//
    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

//    /** 业务类型 **/
//    private String busiType;
//
//    /** 担保方式 STD_ZB_ASSURE_MEANS **/
//    private String guarMode;
//
//    /** 合同币种 STD_ZX_CUR_TYPE **/
//    private String curType;
//
//    /** 合同金额 **/
//    private java.math.BigDecimal contAmt;
//
//    /** 合同期限 **/
//    private String contTerm;
//
//    /** 合同起始日 **/
//    private String startDate;
//
//    /** 合同到期日 **/
//    private String endDate;
//
    /** 是否续签  STD_ZB_YES_NO **/
    private String isRenew;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(String isRenew) {
        this.isRenew = isRenew;
    }

    public String getOrigiContNo() {
        return origiContNo;
    }

    public void setOrigiContNo(String origiContNo) {
        this.origiContNo = origiContNo;
    }

    public String getIsUseLmtAmt() {
        return isUseLmtAmt;
    }

    public void setIsUseLmtAmt(String isUseLmtAmt) {
        this.isUseLmtAmt = isUseLmtAmt;
    }

    public String getDiscContType() {
        return discContType;
    }

    public void setDiscContType(String discContType) {
        this.discContType = discContType;
    }

    public String getRqstrLmtLimitNo() {
        return rqstrLmtLimitNo;
    }

    public void setRqstrLmtLimitNo(String rqstrLmtLimitNo) {
        this.rqstrLmtLimitNo = rqstrLmtLimitNo;
    }

    public String getAcptCrpLmtNo() {
        return acptCrpLmtNo;
    }

    public void setAcptCrpLmtNo(String acptCrpLmtNo) {
        this.acptCrpLmtNo = acptCrpLmtNo;
    }

    public String getAcptCrpCusId() {
        return acptCrpCusId;
    }

    public void setAcptCrpCusId(String acptCrpCusId) {
        this.acptCrpCusId = acptCrpCusId;
    }

    public String getAcptCrpCusName() {
        return acptCrpCusName;
    }

    public void setAcptCrpCusName(String acptCrpCusName) {
        this.acptCrpCusName = acptCrpCusName;
    }

    /** 原合同编号 **/
    private String origiContNo;

    /** 是否使用授信额度 STD_ZB_YES_NO **/
    private String isUseLmtAmt;

//    /** 授信额度编号 **/
//    private String lmtAccNo;
//
//    /** 批复编号 **/
//    private String replyNo;
//
//    /** 是否电子用印 STD_ZB_YES_NO **/
//    private String isESeal;
//
//    /** 是否在线抵押 STD_ZB_YES_NO **/
//    private String isOlPld;
//
//    /** 所属条线 **/
//    private String belgLine;
//
//    /** 债项等级 **/
//    private String debtLevel;
//
//    /** 违约风险暴露EAD **/
//    private java.math.BigDecimal ead;
//
//    /** 违约损失率LGD **/
//    private java.math.BigDecimal lgd;
//
//    /** 联系人 **/
//    private String linkman;
//
//    /** 电话 **/
//    private String phone;
//
//    /** 传真 **/
//    private String fax;
//
//    /** 邮箱 **/
//    private String email;
//
//    /** QQ **/
//    private String qq;
//
//    /** 微信 **/
//    private String wechat;
//
//    /** 送达地址 **/
//    private String servedAddr;
//
//    /** 审批状态 WF_STATUS **/
//    private String apprStatus;
//
//    /** 操作类型 STD_ZB_OPER_TYPE **/
//    private String oprType;
//
//    /** 登记人 **/
//    private String inputId;
//
//    /** 登记机构 **/
//    private String inputBrId;
//
//    /** 登记日期 **/
//    private String inputDate;
//
//    /** 最后修改人 **/
//    private String udpId;
//
//    /** 最后修改机构 **/
//    private String udpBrId;
//
//    /** 最后修改日期 **/
//    private String udpDate;
//
//    /** 创建时间 **/
//    private Date createTime;
//
//    /** 修改时间 **/
//    private Date updateTime;
//
//
//
//
//
//
//
    /** 贴现协议类型   STD_ZB_DISC_CONT_TYPE **/
    private String discContType;

    /** 票据种类   STD_ZB_DRFT_TYPE **/
    private String drftType;

    public String getDrftType() {
        return drftType;
    }

    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }
//    /** 是否电子票据  STD_ZB_YES_NO **/
//    private String isEDrft;
//
//    public static long getSerialVersionUID() {
//        return serialVersionUID;
//    }
//
//    public String getPkId() {
//        return pkId;
//    }
//
//    public void setPkId(String pkId) {
//        this.pkId = pkId;
//    }
//
//    public String getSerno() {
//        return serno;
//    }
//
//    public void setSerno(String serno) {
//        this.serno = serno;
//    }
//
//    public String getCusId() {
//        return cusId;
//    }
//
//    public void setCusId(String cusId) {
//        this.cusId = cusId;
//    }
//
//    public String getCusName() {
//        return cusName;
//    }
//
//    public void setCusName(String cusName) {
//        this.cusName = cusName;
//    }
//
//    public String getPrdId() {
//        return prdId;
//    }
//
//    public void setPrdId(String prdId) {
//        this.prdId = prdId;
//    }
//
//    public String getPrdName() {
//        return prdName;
//    }
//
//    public void setPrdName(String prdName) {
//        this.prdName = prdName;
//    }
//
//    public String getBusiType() {
//        return busiType;
//    }
//
//    public void setBusiType(String busiType) {
//        this.busiType = busiType;
//    }
//
//    public String getGuarMode() {
//        return guarMode;
//    }
//
//    public void setGuarMode(String guarMode) {
//        this.guarMode = guarMode;
//    }
//
//    public String getCurType() {
//        return curType;
//    }
//
//    public void setCurType(String curType) {
//        this.curType = curType;
//    }
//
//    public BigDecimal getContAmt() {
//        return contAmt;
//    }
//
//    public void setContAmt(BigDecimal contAmt) {
//        this.contAmt = contAmt;
//    }
//
//    public String getContTerm() {
//        return contTerm;
//    }
//
//    public void setContTerm(String contTerm) {
//        this.contTerm = contTerm;
//    }
//
//    public String getStartDate() {
//        return startDate;
//    }
//
//    public void setStartDate(String startDate) {
//        this.startDate = startDate;
//    }
//
//    public String getEndDate() {
//        return endDate;
//    }
//
//    public void setEndDate(String endDate) {
//        this.endDate = endDate;
//    }
//
//    public String getIsRenew() {
//        return isRenew;
//    }
//
//    public void setIsRenew(String isRenew) {
//        this.isRenew = isRenew;
//    }
//
//    public String getOrigiContNo() {
//        return origiContNo;
//    }
//
//    public void setOrigiContNo(String origiContNo) {
//        this.origiContNo = origiContNo;
//    }
//
//    public String getIsUseLmtAmt() {
//        return isUseLmtAmt;
//    }
//
//    public void setIsUseLmtAmt(String isUseLmtAmt) {
//        this.isUseLmtAmt = isUseLmtAmt;
//    }
//
//    public String getLmtAccNo() {
//        return lmtAccNo;
//    }
//
//    public void setLmtAccNo(String lmtAccNo) {
//        this.lmtAccNo = lmtAccNo;
//    }
//
//    public String getReplyNo() {
//        return replyNo;
//    }
//
//    public void setReplyNo(String replyNo) {
//        this.replyNo = replyNo;
//    }
//
//    public String getIsESeal() {
//        return isESeal;
//    }
//
//    public void setIsESeal(String isESeal) {
//        this.isESeal = isESeal;
//    }
//
//    public String getIsOlPld() {
//        return isOlPld;
//    }
//
//    public void setIsOlPld(String isOlPld) {
//        this.isOlPld = isOlPld;
//    }
//
//    public String getBelgLine() {
//        return belgLine;
//    }
//
//    public void setBelgLine(String belgLine) {
//        this.belgLine = belgLine;
//    }
//
//    public String getDebtLevel() {
//        return debtLevel;
//    }
//
//    public void setDebtLevel(String debtLevel) {
//        this.debtLevel = debtLevel;
//    }
//
//    public BigDecimal getEad() {
//        return ead;
//    }
//
//    public void setEad(BigDecimal ead) {
//        this.ead = ead;
//    }
//
//    public BigDecimal getLgd() {
//        return lgd;
//    }
//
//    public void setLgd(BigDecimal lgd) {
//        this.lgd = lgd;
//    }
//
//    public String getLinkman() {
//        return linkman;
//    }
//
//    public void setLinkman(String linkman) {
//        this.linkman = linkman;
//    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public String getFax() {
//        return fax;
//    }
//
//    public void setFax(String fax) {
//        this.fax = fax;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getQq() {
//        return qq;
//    }
//
//    public void setQq(String qq) {
//        this.qq = qq;
//    }
//
//    public String getWechat() {
//        return wechat;
//    }
//
//    public void setWechat(String wechat) {
//        this.wechat = wechat;
//    }
//
//    public String getServedAddr() {
//        return servedAddr;
//    }
//
//    public void setServedAddr(String servedAddr) {
//        this.servedAddr = servedAddr;
//    }
//
//    public String getApprStatus() {
//        return apprStatus;
//    }
//
//    public void setApprStatus(String apprStatus) {
//        this.apprStatus = apprStatus;
//    }
//
//    public String getOprType() {
//        return oprType;
//    }
//
//    public void setOprType(String oprType) {
//        this.oprType = oprType;
//    }
//
//    public String getInputId() {
//        return inputId;
//    }
//
//    public void setInputId(String inputId) {
//        this.inputId = inputId;
//    }
//
//    public String getInputBrId() {
//        return inputBrId;
//    }
//
//    public void setInputBrId(String inputBrId) {
//        this.inputBrId = inputBrId;
//    }
//
//    public String getInputDate() {
//        return inputDate;
//    }
//
//    public void setInputDate(String inputDate) {
//        this.inputDate = inputDate;
//    }
//
//    public String getUdpId() {
//        return udpId;
//    }
//
//    public void setUdpId(String udpId) {
//        this.udpId = udpId;
//    }
//
//    public String getUdpBrId() {
//        return udpBrId;
//    }
//
//    public void setUdpBrId(String udpBrId) {
//        this.udpBrId = udpBrId;
//    }
//
//    public String getUdpDate() {
//        return udpDate;
//    }
//
//    public void setUdpDate(String udpDate) {
//        this.udpDate = udpDate;
//    }
//
//    public Date getCreateTime() {
//        return createTime;
//    }
//
//    public void setCreateTime(Date createTime) {
//        this.createTime = createTime;
//    }
//
//    public Date getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(Date updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public String getDiscContType() {
//        return discContType;
//    }
//
//    public void setDiscContType(String discContType) {
//        this.discContType = discContType;
//    }
//
//    public String getDrftType() {
//        return drftType;
//    }
//
//    public void setDrftType(String drftType) {
//        this.drftType = drftType;
//    }
//
//    public String getIsEDrft() {
//        return isEDrft;
//    }
//
//    public void setIsEDrft(String isEDrft) {
//        this.isEDrft = isEDrft;
//    }
//
//    public String getRqstrAccNo() {
//        return rqstrAccNo;
//    }
//
//    public void setRqstrAccNo(String rqstrAccNo) {
//        this.rqstrAccNo = rqstrAccNo;
//    }
//
//    public String getRqstrAccName() {
//        return rqstrAccName;
//    }
//
//    public void setRqstrAccName(String rqstrAccName) {
//        this.rqstrAccName = rqstrAccName;
//    }
//
//    public String getPurType() {
//        return purType;
//    }
//
//    public void setPurType(String purType) {
//        this.purType = purType;
//    }
//
//    public String getDiscCurType() {
//        return discCurType;
//    }
//
//    public void setDiscCurType(String discCurType) {
//        this.discCurType = discCurType;
//    }
//
//    public BigDecimal getDrftTotalAmt() {
//        return drftTotalAmt;
//    }
//
//    public void setDrftTotalAmt(BigDecimal drftTotalAmt) {
//        this.drftTotalAmt = drftTotalAmt;
//    }
//
//    public String getIsAtcf() {
//        return isAtcf;
//    }
//
//    public void setIsAtcf(String isAtcf) {
//        this.isAtcf = isAtcf;
//    }
//
//    public String getPintMode() {
//        return pintMode;
//    }
//
//    public void setPintMode(String pintMode) {
//        this.pintMode = pintMode;
//    }
//
//    public String getRqstrLmtLimitNo() {
//        return rqstrLmtLimitNo;
//    }
//
//    public void setRqstrLmtLimitNo(String rqstrLmtLimitNo) {
//        this.rqstrLmtLimitNo = rqstrLmtLimitNo;
//    }
//
//    public String getRqstrReplyNo() {
//        return rqstrReplyNo;
//    }
//
//    public void setRqstrReplyNo(String rqstrReplyNo) {
//        this.rqstrReplyNo = rqstrReplyNo;
//    }
//
//    public String getAcptCrpLmtNo() {
//        return acptCrpLmtNo;
//    }
//
//    public void setAcptCrpLmtNo(String acptCrpLmtNo) {
//        this.acptCrpLmtNo = acptCrpLmtNo;
//    }
//
//    public String getAcptCrpReplyNo() {
//        return acptCrpReplyNo;
//    }
//
//    public void setAcptCrpReplyNo(String acptCrpReplyNo) {
//        this.acptCrpReplyNo = acptCrpReplyNo;
//    }
//
//    public String getAcptCrpCusId() {
//        return acptCrpCusId;
//    }
//
//    public void setAcptCrpCusId(String acptCrpCusId) {
//        this.acptCrpCusId = acptCrpCusId;
//    }
//
//    public String getAcptCrpCusName() {
//        return acptCrpCusName;
//    }
//
//    public void setAcptCrpCusName(String acptCrpCusName) {
//        this.acptCrpCusName = acptCrpCusName;
//    }
//
//    public String getUpdDate() {
//        return updDate;
//    }
//
//    public void setUpdDate(String updDate) {
//        this.updDate = updDate;
//    }
//
//    /** 申请人账号 **/
//    private String rqstrAccNo;
//
//    /** 申请人账户名称 **/
//    private String rqstrAccName;
//
//    /** 买入类型  STD_ZB_PUR_TYPE **/
//    private String purType;
//
//    /** 贴现币种  STD_ZX_CUR_TYPE **/
//    private String discCurType;
//
//    /** 票面总金额 **/
//    private java.math.BigDecimal drftTotalAmt;
//
//    /** 是否先贴后查  STD_ZB_YES_NO **/
//    private String isAtcf;
//
//    /** 付息方式  STD_ZB_PINT_MODE **/
//    private String pintMode;
//
    /** 申请人授信额度编号 **/
    private String rqstrLmtLimitNo;

//    /** 申请人批复编号 **/
//    private String rqstrReplyNo;
//
    /** 承兑企业授信额度编号 **/
    private String acptCrpLmtNo;

//    /** 承兑企业批复编号 **/
//    private String acptCrpReplyNo;
//
    /** 承兑企业客户编号 **/
    private String acptCrpCusId;

    /** 承兑企业客户名称 **/
    private String acptCrpCusName;
//
//    /** 最近修改日期 **/
//    private String updDate;


}
