package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0084Service;
import com.alibaba.fastjson.JSON;
import com.mysql.cj.util.StringUtils;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
public class RiskItem0016Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0016Service.class);

    @Resource
    private ICusClientService iCusClientService;//注入客户服务接口

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * @方法名称: riskItem0016
     * @方法描述: 借款人为“镇、村经济组织”相关校验
     * @参数与返回说明:
     * @算法描述:
     * 投资主体（即主借款人）为村级组织投资或其客户类型“镇、村经济组织”时
     * 1）财报科目净利润连续2年小于等于0
     * 2）上一年度或者当期资产负债表的资产负债率大于85%
     * 2）借款人在我行的内部评级（即期信用等级）低于或等于CCC
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0016(String cusId) {

        RiskResultDto riskResultDto = new RiskResultDto();
        if (cn.com.yusys.yusp.commons.util.StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        CusCorpDto cusCorpDto =  null;
        CusIndivDto CusIndivDto = null;
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 如果是对公客户
            cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
        }else {
            CusIndivDto = cmisCusClientService.queryCusindivByCusid(cusId).getData();
        }
        if(Objects.isNull(cusCorpDto) && Objects.isNull(CusIndivDto)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0100);
            return riskResultDto;
        }
        // 判断投资主体（即主借款人）为村级组织投资或其客户类型“镇、村经济组织”时
        // 村级组织投资：8
        // 镇、村经济组织:250
        if ((null !=cusCorpDto && (Objects.equals("8",cusCorpDto.getInvestMbody()) || Objects.equals("250",cusCorpDto.getCusType())))
         || (null != CusIndivDto && Objects.equals("250",CusIndivDto.getCusType()))) {
            // 获取财报科目净利润与上一年度或者当期资产负债表的资产负债率信息
            ResultDto<Map<String, FinanIndicAnalyDto>> resultDto =  iCusClientService.getFinRepRetProAndRatOfLia(cusId);
            log.info("当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(resultDto)+"】}");
            if(Objects.nonNull(resultDto) && Objects.nonNull(resultDto.getData())) {
                Map<String, FinanIndicAnalyDto> resultMap = resultDto.getData();
                // 报科目净利润连续2年小于等于0
                if ((NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("retainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)
                        && (NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearRetainedProfits")).get().getCurYmValue()).compareTo(new BigDecimal(0)) <= 0)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001603);
                    return riskResultDto;
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001605);
                return riskResultDto;
            }
            // 查询法人客户的信用评级信息
            ResultDto<Map<String, String>> result = iCusClientService.selectGradeInfoByCusId(cusId);
            Map<String, String> gradeInfo = result.getData();
            if (Objects.nonNull(gradeInfo)) {
                String finalRank = gradeInfo.get("finalRank");
                if (!StringUtils.isNullOrEmpty(finalRank) && !"130".equals(finalRank) && !"140".equals(finalRank) && !"150".equals(finalRank) && !"160".equals(finalRank) && !"170".equals(finalRank)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001601);
                    return riskResultDto;
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001602);
                return riskResultDto;
            }
        }else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
    }

}
