package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.MapUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.HouseEvalModelDto;
import cn.com.yusys.yusp.domain.IndexMethodEvalRequestDTO;
import cn.com.yusys.yusp.domain.MarketPriceEvalRequestDTO;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-eval-core模块
 * @类名称: EvaluateResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: XuSheng
 * @创建时间: 2021-03-24 20:27:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/evaluate")

public class EvaluateResource {

    private final static Logger log = LoggerFactory.getLogger(EvaluateResource.class);

    /**
     * @方法名称:incomePriceEval
     * @方法描述:收益法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "收益法估值计算")
    @PostMapping(value = "/incomeprice")
    protected ResultDto<HashMap<String, String>> incomePriceEval(@RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            // 当前评估时间
            // String curtEvalTime = hashMap.get("curt_eval_time");
            //年租金 (元)
            BigDecimal yearRent = new BigDecimal(hashMap.get("year_rent"));
            //平均空置率(%)
            BigDecimal avgVacancyRate = new BigDecimal(hashMap.get("avg_vacancy_rate"));
            //营业税金及附加税率(%)
            BigDecimal businessSurtaxRate = new BigDecimal(hashMap.get("business_surtax_rate"));
            //折现率(%)
            BigDecimal discountRate = new BigDecimal(hashMap.get("discount_rate"));
            //预期年化净收入(元)
            BigDecimal expectYearIncome = null;
            //房产税率(%)
            BigDecimal houseTax = new BigDecimal(hashMap.get("house_tax"));
            //所得税率(%)
            BigDecimal incomeRate = new BigDecimal(hashMap.get("income_rate"));
            //管理费率(%)
            BigDecimal manageRate = new BigDecimal(hashMap.get("manage_rate"));
            //其他费率(%)
            BigDecimal otherRate = new BigDecimal(hashMap.get("other_rate"));
            //剩余收益期限(年)
            int surplusIncomeTerm = Integer.parseInt(hashMap.get("surplus_income_term"));
            /**
             * 计算公式：
             * 预期年化净收入 = 实际年租金 × ( 1 - 待估押品类似物业平均空置率 ) × ( 1 - 房产税率-营业税及附加税率 -管理费率 - 其他费率 ) × ( 1 - 所得税率 )
             * 内部评估价值 = 预期年华净收入× [ 1 - 1 / ( 1 + 折现率 ) ^ 剩余收益期限 ] / 折现率
             */
            // 计算结果:预期年化净收入
            expectYearIncome = yearRent.multiply(new BigDecimal(1).subtract(avgVacancyRate));
            expectYearIncome = expectYearIncome.multiply(new BigDecimal(1).subtract(houseTax).subtract(businessSurtaxRate).subtract(manageRate)
                    .subtract(otherRate));
            expectYearIncome = expectYearIncome.multiply(new BigDecimal(1).subtract(incomeRate));
            // 计算结果：内部评估价值
            BigDecimal tempOne = new BigDecimal(1).add(discountRate);
            BigDecimal temptwo = tempOne.pow(surplusIncomeTerm);
            BigDecimal tempthr = new BigDecimal(1).divide(temptwo, 6, BigDecimal.ROUND_HALF_UP);
            BigDecimal evalResult = expectYearIncome.multiply(new BigDecimal(1).subtract(tempthr));
            evalResult = evalResult.divide(discountRate, 2, BigDecimal.ROUND_HALF_UP);
            resultMap.put("expectYearIncome", expectYearIncome.toString());
            resultMap.put("evalResult", evalResult.toString());
        } catch (Exception e) {
            log.error("收益法估值计算异常：", e);
            return null;
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:repeatPriceEval
     * @方法描述:重置成本法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "重置成本法估值计算")
    @PostMapping("/repeatprice")
    protected ResultDto<HashMap<String, String>> repeatPriceEval(@RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            // 经济性贬值(元)
            //BigDecimal eclDvl = new BigDecimal(hashMap.get("ecl_dvl"));
            // 设备安装费率(%)
            BigDecimal equipmentInstallationRate = new BigDecimal(hashMap.get("equipment_installation_rate"));
            // 预计可被利用的产能比率(%)
            BigDecimal foreUsedCapacityRate = new BigDecimal(hashMap.get("fore_used_capacity_rate"));
            // 运杂费率(%)
            BigDecimal freightRate = new BigDecimal(hashMap.get("freight_rate"));
            // 功能性贬值(元)
            BigDecimal ftnDvl = new BigDecimal(hashMap.get("ftn_dvl"));
            // 押品分类
            BigDecimal guarType = new BigDecimal(hashMap.get("guar_type"));
            // 观察法成新率(%)
            BigDecimal observationNewRate = new BigDecimal(hashMap.get("observation_new_rate"));
            // 观察法成新率权重(%)
            BigDecimal observationNewWeight = new BigDecimal(hashMap.get("observation_new_weight"));
            // 实际性贬值(元)
            //BigDecimal prtDvl = new BigDecimal(hashMap.get("prt_dvl"));
            // 购置价格 (元)
            BigDecimal purchasePrice = new BigDecimal(hashMap.get("purchase_price"));
            // 年限法成新率(%)
            BigDecimal yearNewRate = new BigDecimal(hashMap.get("year_new_rate"));
            // 年限法成新率权重(%)
            BigDecimal yearNewWeight = new BigDecimal(hashMap.get("year_new_weight"));
            // 其他费率(%)
            BigDecimal otherRate = new BigDecimal(hashMap.get("other_rate"));
            /**
             * 计算公式：
             * 评估价值 = 重置总成本 - 实体性贬值 - 功能性贬值 - 经济性贬值
             * 重置总成本 = 购置价格 × （1 + 运杂费率 + 设备安装费率 + 其它费率）
             * 实体性贬值 = 重置总成本 × [1 - （ 年限法成新率 × 年限法成新率权重 + 观察法成新率 × 观察法成新率权重） ]
             * 经济性贬值 =重置总成本 ×（ 1 – 预计可被利用的产能比率 ^ 0.7 ）
             */

            // 计算：重置总成本
            BigDecimal resetCost = BigDecimal.ZERO;
            resetCost = purchasePrice
                    .multiply(new BigDecimal(1).add(freightRate).add(equipmentInstallationRate).add(otherRate));
            // 计算：实体性贬值
            BigDecimal prtDvl = BigDecimal.ZERO;
            BigDecimal tempTwo = new BigDecimal(1).subtract(
                    (yearNewRate.multiply(yearNewWeight)).add(observationNewRate.multiply(observationNewWeight)));
            prtDvl = resetCost.multiply(tempTwo);
            // 计算：经济性贬值
            BigDecimal eclDvl = resetCost
                    .multiply(new BigDecimal(1).subtract(BigDecimal.valueOf(Math.pow(foreUsedCapacityRate.doubleValue(), 0.7))));
            // 计算：评估价值
            BigDecimal evalResult = resetCost.subtract(prtDvl).subtract(ftnDvl)
                    .subtract(eclDvl);

            // 组装返回结果
            resultMap.put("evalResult", evalResult.toString());
            resultMap.put("resetCost", resetCost.toString());
            resultMap.put("eclDvl", eclDvl.toString());
            resultMap.put("prtDvl", prtDvl.toString());
        } catch (Exception e) {
            log.error("重置成本法估值计算异常：", e);
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:comPerPriceEval
     * @方法描述:净资产调整法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "净资产调整法估值计算")
    @PostMapping(value = "/comperprice")
    protected ResultDto<HashMap<String, String>> comPerPriceEval(@RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            //账龄≥2年的预付费用已计提的减值准备 (元)
            BigDecimal alreadyPlan = new BigDecimal(hashMap.get("already_plan"));
            //账龄≥2年的应收账款已计提减值准备 (元)
            BigDecimal decValue = new BigDecimal(hashMap.get("dec_value"));
            //出质股权比例(%)
            BigDecimal mortStkPerc = new BigDecimal(hashMap.get("mort_stk_perc"));
            //净资产 (元)
            BigDecimal nas = new BigDecimal(hashMap.get("nas"));
            //经审计后的账龄≥ 2年的预付费用 (元)
            BigDecimal preExpense = new BigDecimal(hashMap.get("pre_expense"));
            //经审计后的账龄≥ 2年的应收账款 (元)
            BigDecimal receiveAccount = new BigDecimal(hashMap.get("receive_account"));
            // 重估价值=[（净资产–(经审计后的账龄≥2年的应收账款-2年的应收账款对应的已计提的减值准备)-（经审计后的账龄≥2年的预付费用-2年的预付费用对应的已计提的减值准备）]× 出质股权比例
            BigDecimal evalResult = BigDecimal.ZERO;
            evalResult = nas.subtract(receiveAccount.subtract(decValue))
                    .subtract(preExpense.subtract(alreadyPlan));
            evalResult = evalResult.multiply(mortStkPerc);
            // 组装返回结果
            resultMap.put("evalResult", evalResult.toString());
        } catch (Exception e) {
            log.error("净资产调整法估值计算异常：", e);
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:projectProcessEval
     * @方法描述:工程进度法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "工程进度法估值计算")
    @PostMapping(value = "/projectprogress")
    protected ResultDto<HashMap<String, String>> projectProcessEval(@RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            // 营业税及附加税税率(%)
            BigDecimal busRate = new BigDecimal(hashMap.get("bus_rate"));
            // 价值折扣率(%)
            BigDecimal discountRate = new BigDecimal(hashMap.get("discount_rate"));
            // 所得税率(%)
            BigDecimal incomeRate = new BigDecimal(hashMap.get("income_rate"));
            // 土地增值税税率(%)
            BigDecimal landRate = new BigDecimal(hashMap.get("land_rate"));
            // 管理费率(%)
            BigDecimal manageRate = new BigDecimal(hashMap.get("manage_rate"));
            // 其他费率(%)
            BigDecimal otherRate = new BigDecimal(hashMap.get("other_rate"));
            // 工程完成程度(%)
            BigDecimal projectProgress = new BigDecimal(hashMap.get("project_progress"));
            // 预计完工后工程总价值 (元)
            BigDecimal projectValue = new BigDecimal(hashMap.get("project_value"));
            // 销售费率(%)
            BigDecimal saleRate = new BigDecimal(hashMap.get("sale_rate"));
            /*  重估价值=预计完工后工程价值 * 工程完成进度
             *   x（1-价值折扣率）x（1-营业税金及附加税率 - 土地增值税税率 - 销售费率  - 管理费率 - 其他费率）
             *   x（1-所得税率）
             */
            BigDecimal evalResult = BigDecimal.ZERO;
            evalResult = projectValue.multiply(projectProgress).multiply(new BigDecimal(1).subtract(discountRate));
            evalResult = evalResult.multiply(new BigDecimal(1).subtract(busRate)
                    .subtract(landRate).subtract(saleRate).subtract(manageRate).subtract(otherRate));
            evalResult = evalResult.multiply(new BigDecimal(1).subtract(incomeRate));

            // 组装返回结果
            resultMap.put("evalResult", evalResult.toString());
        } catch (Exception e) {
            log.error("工程进度法估值计算异常：", e);
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:autoInquiryEval
     * @方法描述:自主询价法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自主询价法估值计算")
    @RequestMapping(value = "/autoInquiryEval")
    protected ResultDto<HashMap<String, String>> autoInquiryEval(@RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            // 当前评估押品价值询价渠道
            BigDecimal mortPrice = new BigDecimal(hashMap.get("mort_price"));
            // 评估单价
            BigDecimal mortQnt = new BigDecimal(hashMap.get("mort_qnt"));
            // 待估押品数量
            BigDecimal unitPrice = new BigDecimal(hashMap.get("unit_price"));
            /*
             * 重估价值 = 评估单价 x 待估押品数量
             * */
            BigDecimal evalResult = mortPrice.multiply(unitPrice);
            resultMap.put("evalResult", evalResult.toString());
        } catch (Exception e) {
            log.error("自主询价法估值计算异常：", e);
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:landPrice
     * @方法描述:基准地价系数修正法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("基准地价系数修正法估值计算")
    @PostMapping("/landPrice")
    protected ResultDto<HashMap<String, String>> datumLandPriceEval(
            @RequestBody String jsonPar) {
        //返回结果
        HashMap<String, String> resultMap = new HashMap<String, String>();
        HashMap<String, String> hashMap = JSON.parseObject(jsonPar, HashMap.class);
        try {
            // 坐落位置
            //BigDecimal  addr = new BigDecimal(hashMap.get("addr")) ;
            // 土地面积（平方米）
            BigDecimal area = new BigDecimal(hashMap.get("area"));
            // 估价日期修正系数
            BigDecimal dateUpdateRadio = new BigDecimal(hashMap.get("date_update_radio"));
            // 估值日期
            String evalDate = hashMap.get("eval_date");
            // 土地使用权到期日
            String landDate = hashMap.get("land_date");
            // land_grade
            //BigDecimal  landGrade = new BigDecimal(hashMap.get("land_grade")) ;
            // 基准地价 (元)
            BigDecimal landPrice = new BigDecimal(hashMap.get("land_price"));
            // 土地还原利率(%)
            BigDecimal landReturnRate = new BigDecimal(hashMap.get("land_return_rate"));
            // 土地用途
            //BigDecimal  landWay = new BigDecimal(hashMap.get("land_way")) ;
            // 押品分类标识
            //BigDecimal  mortType = new BigDecimal(hashMap.get("mort_type")) ;
            // 其他因素修正系数
            BigDecimal otherUpdateRadio = new BigDecimal(hashMap.get("other_update_radio"));
            // 土地使用权法定最高出让年限（年
            BigDecimal sellYear = new BigDecimal(hashMap.get("sell_year"));
            // 容积率修正系数
            BigDecimal sizeUpdateRadio = new BigDecimal(hashMap.get("size_update_radio"));
            // 总累积调整系数
            //BigDecimal  sumUpdateRadio = new BigDecimal(hashMap.get("sum_update_radio")) ;
            // 使用年限修正系数
            //BigDecimal  useYearRadio = new BigDecimal(hashMap.get("use_year_radio")) ;
            /**
             * 待估押品的评估价值=待估押品评估单价×土地面积
             * 待估押品评估单价=估价对象所在区域基准地价 ×总累积调整系数
             * 总累积调整系数=容积率修正系数×使用年期修正系数×估价日期修正系数×（1+其他因素修正系数）
             * 使用年期修正系数= [ 1-1 / (1+土地还原利率) ^ 待估宗地剩余使用年限 ] / [ 1 – 1 / ( 1 +土地还原利率 ) ^ 法定最高出让年限 ]
             */
            // 待估宗地剩余使用年限 between_year
            int days = DateUtils.getDaysByTwoDates(evalDate, landDate, DateFormatEnum.DEFAULT.getValue());
            //按365天一年计算，保留一位小数，四舍五入
            double betweenYear = (double)Math.round(days / 365L);
            // 使用年期修正系数 = [ 1-1 / (1+土地还原利率) ^ 待估宗地剩余使用年限 ] / [ 1 – 1 / ( 1 +土地还原利率 ) ^ 法定最高出让年限 ]
            double useYearRadio = (1 - 1 / Math.pow(1 + landReturnRate.doubleValue(), betweenYear))
                    / (1 - 1 / Math.pow(1 + landReturnRate.doubleValue(), sellYear.doubleValue()));
            // 总累积调整系数 = 容积率修正系数×使用年期修正系数×估价日期修正系数×（1+其他因素修正系数）
            BigDecimal sumUpdateRadio = sizeUpdateRadio.multiply(BigDecimal.valueOf(useYearRadio))
                    .multiply(dateUpdateRadio).multiply(new BigDecimal(1).add(otherUpdateRadio));
            // 待估押品评估单价 = 估价对象所在区域基准地价 ×总累积调整系数
            BigDecimal evalPrice = landPrice.multiply(sumUpdateRadio);
            // 待估押品的评估价值 = 待估押品评估单价×土地面积
            BigDecimal evalCost = evalPrice.multiply(area);
            // 组装返回结果
            resultMap.put("sumUpdateRadio", sumUpdateRadio.setScale(2, BigDecimal.ROUND_UP).toString());
            resultMap.put("useYearRadio", Double.toString(useYearRadio));
            resultMap.put("evalPrice", evalPrice.setScale(2, BigDecimal.ROUND_UP).toString());
            resultMap.put("evalCost", evalCost.setScale(2, BigDecimal.ROUND_UP).toString());
        } catch (Exception e) {
            log.error("基准地价系数修正法估值计算异常：", e);
        }
        return new ResultDto<HashMap<String, String>>(resultMap);
    }

    /**
     * @方法名称:marketPricePropEval
     * @方法描述:市场价格法估值计算--不动产
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/marketpriceprop")
    protected ResultDto<Map<String, Object>> marketPricePropEval(@RequestBody MarketPriceEvalRequestDTO evalRequestDTO) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 数量
            BigDecimal number = evalRequestDTO.getNumber();
            // 价格
            BigDecimal price = evalRequestDTO.getPrice();
            // 估值结果
            BigDecimal reevalAmtProp = BigDecimal.ZERO;
            // 重估价值=押品数量×当天市场价格
            reevalAmtProp = number.multiply(price);

            // 组装返回结果
            resultMap.put("evalAmtProp", reevalAmtProp);
        } catch (Exception e) {
            log.error("市场价格法估值（动产）计算异常：", e);
        }

        return new ResultDto<Map<String, Object>>(resultMap);
    }

    /**
     * @方法名称:marketPricePropFin
     * @方法描述:市场价格法估值计算--金融质押品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/marketpricefin")
    protected ResultDto<Map<String, Object>> marketPricePropFin(@RequestBody MarketPriceEvalRequestDTO evalRequestDTO) {

        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 数量
            BigDecimal number = evalRequestDTO.getNumber();
            // 价格
            BigDecimal price = evalRequestDTO.getPrice();
            // 估值结果
            BigDecimal reevalAmtFin = BigDecimal.ZERO;
            // 重估价值=押品数量×当天市场价格
            reevalAmtFin = number.multiply(price);

            // 组装返回结果
            resultMap.put("evalAmtFin", reevalAmtFin);
        } catch (Exception e) {
            log.error("市场价格法估值（金融质押品）计算异常：", e);
        }
        return new ResultDto<Map<String, Object>>(resultMap);
    }

    /**
     * @方法名称:indexMethodEval
     * @方法描述:指数法估值计算
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/indexmethod")
    protected ResultDto<Map<String, Object>> indexMethodEval(@RequestBody IndexMethodEvalRequestDTO evalRequestDTO) {

        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 初估价值
            BigDecimal evalOutAmt = evalRequestDTO.getEvalOutAmt();
            // 当前价格指数值 (元)
            BigDecimal nowEvaluatIndexValue = evalRequestDTO.getNowEvaluatIndexValue();
            // 比较期价格指数值 (元)
            BigDecimal compareEvaluatIndexValue = evalRequestDTO.getCompareEvaluatIndexValue();
            // 房屋物理折损率(%)
            BigDecimal housingRate = evalRequestDTO.getHousingRate();
            // 初估认定日期
            String evalOutDate = evalRequestDTO.getEvalOutDate();
            // 当前评估日期
            String nowDate = evalRequestDTO.getNowEvaluatDate();
            // 以'-'分组
            String[] dateNow = nowDate.split("-");
            String[] dateFst = evalOutDate.split("-");
            // 当前评估-年
            int nowYear = Integer.parseInt(dateNow[0]);
            // 初估认定-年
            int fstYear = Integer.parseInt(dateFst[0]);
            // 当前评估-月
            int nowMonth = Integer.parseInt(dateNow[1]);
            // 初估认定-月
            int fstMonth = Integer.parseInt(dateFst[1]);
            // 内部评估价值=初估评估价值 ×（当前评估时点对应指数 / 初估评估时点对应指数） × [ 1–（当前评估年月 - 初估评估年月）/12 × 年度房屋物理折损率 ]
            int month = (nowYear - fstYear) * 12 + (nowMonth - fstMonth);
            Double value = evalOutAmt.doubleValue()
                    * (nowEvaluatIndexValue.doubleValue() / compareEvaluatIndexValue.doubleValue())
                    * (1 - (double) month / 12 * housingRate.doubleValue());
            Double valueRate = (value - evalOutAmt.doubleValue()) / evalOutAmt.doubleValue();

            // 组装返回结果
            resultMap.put("evalAmt", value);
            resultMap.put("volatilityValue", valueRate);
        } catch (Exception e) {
            log.error("指数法估值计算异常：", e);
        }

        return new ResultDto<Map<String, Object>>(resultMap);
    }

    /**
     * @方法名称:houseEvalModelResult
     * @方法描述:市场比较法估值计算--房产
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/houseEvalModelResult")
    protected ResultDto<Map<String, Object>> houseEvalModelResult(@RequestBody HouseEvalModelDto houseEvalModelDto) {

        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 待估样品面积
            double area = houseEvalModelDto.getArea();
            // 对比样品单价
            double a3 = houseEvalModelDto.getA3();
            double b3 = houseEvalModelDto.getB3();
            double c3 = houseEvalModelDto.getC3();

            // 权重
            double area_busy = houseEvalModelDto.getArea_busy();
            double traffic = houseEvalModelDto.getTraffic();
            double environ = houseEvalModelDto.getEnviron();
            double publics = houseEvalModelDto.getPublics();
            double building = houseEvalModelDto.getBuilding();
            double orient = houseEvalModelDto.getOrient();
            double fitment = houseEvalModelDto.getFitment();
            double equip = houseEvalModelDto.getEquip();
            double property = houseEvalModelDto.getProperty();
            double projects = houseEvalModelDto.getProjects();
            double remainyear = houseEvalModelDto.getRemainyear();
            double poltratio = houseEvalModelDto.getPoltratio();

            // 对比实例一
            double a4 = houseEvalModelDto.getA4();
            double a5 = houseEvalModelDto.getA5();
            double a6 = houseEvalModelDto.getA6();
            double a7 = houseEvalModelDto.getA7();
            double a8 = houseEvalModelDto.getA8();
            double a9 = houseEvalModelDto.getA9();
            double a10 = houseEvalModelDto.getA10();
            double a11 = houseEvalModelDto.getA11();
            double a12 = houseEvalModelDto.getA12();
            double a13 = houseEvalModelDto.getA13();
            double a14 = houseEvalModelDto.getA14();
            double a15 = houseEvalModelDto.getA15();
            double a16 = 0;
            double a17 = 0;

            // 对比实例二
            double b4 = houseEvalModelDto.getB4();
            double b5 = houseEvalModelDto.getB5();
            double b6 = houseEvalModelDto.getB6();
            double b7 = houseEvalModelDto.getB7();
            double b8 = houseEvalModelDto.getB8();
            double b9 = houseEvalModelDto.getB9();
            double b10 = houseEvalModelDto.getB10();
            double b11 = houseEvalModelDto.getB11();
            double b12 = houseEvalModelDto.getB12();
            double b13 = houseEvalModelDto.getB13();
            double b14 = houseEvalModelDto.getB14();
            double b15 = houseEvalModelDto.getB15();
            double b16 = 0;
            double b17 = 0;

            // 对比实例三
            double c4 = houseEvalModelDto.getC4();
            double c5 = houseEvalModelDto.getC5();
            double c6 = houseEvalModelDto.getC6();
            double c7 = houseEvalModelDto.getC7();
            double c8 = houseEvalModelDto.getC8();
            double c9 = houseEvalModelDto.getC9();
            double c10 = houseEvalModelDto.getC10();
            double c11 = houseEvalModelDto.getC11();
            double c12 = houseEvalModelDto.getC12();
            double c13 = houseEvalModelDto.getC13();
            double c14 = houseEvalModelDto.getC14();
            double c15 = houseEvalModelDto.getC15();
            double c16 = 0;
            double c17 = 0;

            // 对比实例总累积调整系数
            double f1 = 0;
            double f2 = 0;
            double f3 = 0;

            // 对比实例调整后的市场单价
            double p1 = 0;
            double p2 = 0;
            double p3 = 0;

            // 估值结果
            double result = 0;

            // 总累积调整系数
            f1 = 100 / a4 * area_busy + 100 / a5 * traffic
                    + 100 / a6 * environ + 100 / a7 * publics
                    + 100 / a8 * building + 100 / a9 * orient
                    + 100 / a10 * fitment + 100 / a11 * equip
                    + 100 / a12 * property + 100 / a13 * projects
                    + 100 / a14 * remainyear + 100 / a15 * poltratio;

            f2 = 100 / b4 * area_busy + 100 / b5 * traffic
                    + 100 / b6 * environ + 100 / b7 * publics
                    + 100 / b8 * building + 100 / b9 * orient
                    + 100 / b10 * fitment + 100 / b11 * equip
                    + 100 / b12 * property + 100 / b13 * projects
                    + 100 / b14 * remainyear + 100 / b15 * poltratio;

            f3 = 100 / c4 * area_busy + 100 / c5 * traffic
                    + 100 / c6 * environ + 100 / c7 * publics
                    + 100 / c8 * building + 100 / c9 * orient
                    + 100 / c10 * fitment + 100 / c11 * equip
                    + 100 / c12 * property + 100 / c13 * projects
                    + 100 / c14 * remainyear + 100 / c15 * poltratio;

            // 判断是否double无穷
            // p:对比实例调整后的市场单价＝对比实例调整前的市场单价×总累积调整系数
            if (!Double.isInfinite(f1)) {
                a16 = f1;
                p1 = a3 * f1;
                a17 = p1;
            }
            if (!Double.isInfinite(f2)) {
                b16 = f2;
                p2 = b3 * f2;
                b17 = p2;
            }
            if (!Double.isInfinite(f3)) {
                c16 = f3;
                p3 = c3 * f3;
                c17 = p3;
            }

            // 待估对象评估价值＝对比实例平均的调整后单价×待估对象建筑面积
            result = ((p1 + p2 + p3) / 3) * area;

            // 组装返回结果
            resultMap.put("a16", a16);
            resultMap.put("a17", a17);
            resultMap.put("b16", b16);
            resultMap.put("b17", b17);
            resultMap.put("c16", c16);
            resultMap.put("c17", c17);
            resultMap.put("result", result);
        } catch (Exception e) {
            log.error("市场价格法估值（房产）计算异常：", e);
        }
        return new ResultDto<Map<String, Object>>(resultMap);
    }

    /**
     * @方法名称:houseEvalModelResult
     * @方法描述:根据土地还原率计算年限调整系数
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/calCompareToMarket4LandTimeFactor")
    public Map<String, Object> calCompareToMarket4LandTimeFactor(@RequestBody Map<String, Object> request) {

        Map<String, Object> resultMap = new HashMap<String, Object>();

        // 对比实力年限调整系数
        BigDecimal f1 = BigDecimal.ZERO;
        BigDecimal f2 = BigDecimal.ZERO;
        BigDecimal f3 = BigDecimal.ZERO;
        try {
            if (request.containsKey("ddMess")) {
                // 获取页面的值
                BigDecimal r = new BigDecimal(String.valueOf(request.get("ddMess").toString().split(",")[0]));

                String guarEvalDate = String.valueOf(request.get("ddMess").toString().split(",")[1]);
                String sam1EvalDate = String.valueOf(request.get("ddMess").toString().split(",")[2]);
                String sam2EvalDate = String.valueOf(request.get("ddMess").toString().split(",")[3]);
                String sam3EvalDate = String.valueOf(request.get("ddMess").toString().split(",")[4]);

                String guarLandUseEndDate = String.valueOf(request.get("ddMess").toString().split(",")[5]);
                String sam1LandUseEndDate = String.valueOf(request.get("ddMess").toString().split(",")[6]);
                String sam2LandUseEndDate = String.valueOf(request.get("ddMess").toString().split(",")[7]);
                String sam3LandUseEndDate = String.valueOf(request.get("ddMess").toString().split(",")[8]);

                // 年限调整系数：公式
                // f1=[1-1/(1+r)^(B0_6-B0_5)]/[1-1/(1+r)^(B1_6-B1_5)]
                // f2=[1-1/(1+r)^(B0_6-B0_5)]/[1-1/(1+r)^(B2_6-B2_5)]
                // f3=[1-1/(1+r)^(B0_6-B0_5)]/[1-1/(1+r)^(B3_6-B3_5)]
                BigDecimal temp = new BigDecimal(1).subtract(new BigDecimal(1).divide(this.calculateFactorTool(r,
                        guarLandUseEndDate, guarEvalDate), 10, BigDecimal.ROUND_HALF_UP));
                BigDecimal tempOne = new BigDecimal(1).subtract(new BigDecimal(1).divide(this.calculateFactorTool(r,
                        sam1LandUseEndDate, sam1EvalDate), 10, BigDecimal.ROUND_HALF_UP));
                BigDecimal tempTwo = new BigDecimal(1).subtract(new BigDecimal(1).divide(this.calculateFactorTool(r,
                        sam2LandUseEndDate, sam2EvalDate), 10, BigDecimal.ROUND_HALF_UP));
                BigDecimal tempThr = new BigDecimal(1).subtract(new BigDecimal(1).divide(this.calculateFactorTool(r,
                        sam3LandUseEndDate, sam3EvalDate), 10, BigDecimal.ROUND_HALF_UP));

                f1 = temp.divide(tempOne, 6, BigDecimal.ROUND_HALF_UP);
                f2 = temp.divide(tempTwo, 6, BigDecimal.ROUND_HALF_UP);
                f3 = temp.divide(tempThr, 6, BigDecimal.ROUND_HALF_UP);

                // 设置标记值
                resultMap.put("msg", "success");
                resultMap.put("f1", f1);
                resultMap.put("f2", f2);
                resultMap.put("f3", f3);
            } else {
                resultMap.put("msg", "error");
            }
        } catch (Exception e) {
            log.error("土地还原率求年限调整系数计算异常：", e);
        }
        return resultMap;
    }

    /**
     * @方法名称:calculateFactorTool
     * @方法描述:市场比较法估值计算工具
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("calculateFactorTool")
    public BigDecimal calculateFactorTool(BigDecimal r, String guarLandUseEndDate, String guarEvalDate) {
        BigDecimal rs = BigDecimal.ZERO;
        Double tempOne = new BigDecimal(1).add(r).doubleValue();
        Double tempTwo = this.compareToDate(guarLandUseEndDate, guarEvalDate).doubleValue();
        rs = new BigDecimal(Double.toString(Math.pow(tempOne, tempTwo)));
        return rs;
    }

    /**
     * @方法名称:compareToDate
     * @方法描述:市场比较法估值年月计算工具
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("compareToDate")
    public static BigDecimal compareToDate(String curDate, String date) {
        //日期格式YYYY-MM-DD
        int months = 0;
        int year1 = Integer.parseInt(curDate.substring(0, 4)); //年YYYY
        int year2 = Integer.parseInt(date.substring(0, 4)); //年YYYY
        int month1 = Integer.parseInt(curDate.substring(5, 7));//月MM
        int month2 = Integer.parseInt(date.substring(5, 7));//月MM
        int day1 = Integer.parseInt(curDate.substring(8, 10));//日DD
        int day2 = Integer.parseInt(date.substring(8, 10));//日DD
        if (month1 >= month2) {
            months = (year1 - year2) * 12 + (month1 - month2);
        } else {
            months = (year1 - 1 - year2) * 12 + (month1 + 12 - month2);
        }
        BigDecimal daysBetween = new BigDecimal(day1).subtract(new BigDecimal(day2)); //计算两者相隔的DD
        daysBetween = daysBetween.divide(new BigDecimal(30), 10, BigDecimal.ROUND_HALF_UP);//将DD转换为MM
        return new BigDecimal(months).add(daysBetween);
    }

    /**
     * @方法名称:calCompareToMarket4House
     * @方法描述:土地类市场比较法估值
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/calCompareToMarket4House")
    public Map<String, Object> calCompareToMarket4House(@RequestBody Map<String, Object> request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            // 土地使用权面积
            double d0 = Double.valueOf((String) request.get("d0"));
            // 比较样本调整后单价
            double P1 = Double.valueOf((String) request.get("P1"));
            double P2 = Double.valueOf((String) request.get("P2"));
            double P3 = Double.valueOf((String) request.get("P3"));
            // 待估押品评估单价＝average(比较样本调整后单价)
            double evalArea = (P1 + P2 + P3) / 3;
            // 待估押品评估价值＝待估押品评估单价×土地使用权面积
            double evalResult = ((P1 + P2 + P3) / 3) * d0;

            resultMap.put("evalArea", evalArea);
            resultMap.put("evalResult", evalResult);

        } catch (Exception e) {
            log.error("市场价格法估值（土地）计算异常：", e);
        }
        return resultMap;
    }
}
