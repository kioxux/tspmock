/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.LmtIntbankAccAndLmtSigDto;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankAcc;
import cn.com.yusys.yusp.service.LmtIntbankAccService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 16:54:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankacc")
public class LmtIntbankAccResource {
    @Autowired
    private LmtIntbankAccService lmtIntbankAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankAcc> list = lmtIntbankAccService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankAcc>> index(QueryModel queryModel) {
        List<LmtIntbankAcc> list = lmtIntbankAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankAcc> show(@PathVariable("pkId") String pkId) {
        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankAcc>(lmtIntbankAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankAcc> create(@RequestBody LmtIntbankAcc lmtIntbankAcc) throws URISyntaxException {
        lmtIntbankAccService.insert(lmtIntbankAcc);
        return new ResultDto<LmtIntbankAcc>(lmtIntbankAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankAcc lmtIntbankAcc) throws URISyntaxException {
        int result = lmtIntbankAccService.update(lmtIntbankAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 条件查询
     * @param queryModel
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtIntbankAcc>> selectByModel(@RequestBody QueryModel queryModel){
        List<LmtIntbankAcc> list = lmtIntbankAccService.selectByModelList(queryModel);
        return new ResultDto<List<LmtIntbankAcc>>(list);
    }
    /**
     * 根据台账编号查询
     */
    @PostMapping("selectByAccNo")
    protected ResultDto<LmtIntbankAcc> selectByAccNo(@RequestBody Map condition ){
        String accNo = condition.get("accNo").toString();
        return new ResultDto<LmtIntbankAcc>(lmtIntbankAccService.selectByAccNo(accNo));
    }
    /**
     * 获取有效批复编号
     */
    @PostMapping("getReplySerno")
    protected ResultDto<List<LmtIntbankAcc>> getReplySerno(@RequestBody QueryModel model){
        return new ResultDto<List<LmtIntbankAcc>>(lmtIntbankAccService.getReplySerno(model));
    }

    @PostMapping("/selectByReplySerno")
    protected ResultDto<LmtIntbankAcc> selectByReplySerno(@RequestBody Map condition){
        String replySerno = condition.get("replySerno").toString();
        return new ResultDto<LmtIntbankAcc>(lmtIntbankAccService.selectByReplyNo(replySerno));
    }
    /**
     * 投后业务查询（贷后）
     * @param queryModel
     * @return
     * @创建人：周茂伟
     */
    @PostMapping("/selectForPsp")
    protected ResultDto<List<LmtIntbankAccAndLmtSigDto>> selectForPsp(@RequestBody QueryModel queryModel){
        List<LmtIntbankAccAndLmtSigDto> list = lmtIntbankAccService.selectForPsp(queryModel);
        return new ResultDto<List<LmtIntbankAccAndLmtSigDto>>(list);
    }
}
