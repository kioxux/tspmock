package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRruUpdApp
 * @类描述: pvp_rru_upd_app数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-28 14:37:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpRruUpdAppDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 退汇修改申请流水号 **/
	private String rruSerno;

	/** 业务流水号 **/
	private String oldSerno;

	/** 受托支付编号 **/
	private String accptBookNo;

	/** 新支付交易流水 **/
	private String newAuthNo;

	/** 借据编号 **/
	private String billNo;

	/** 授权编号 **/
	private String authNo;

	/** 合同编号 **/
	private String contNo;

	/** 客户编号 **/
	private String cusId;

	/** 放款时间 **/
	private String pvpDate;

	/** 账户属性 **/
	private String acctAttr;

	/** 原账号归属 **/
	private String oldAcctBelong;

	/** 原账号分类 **/
	private String oldAcctClass;

	/** 原账号 **/
	private String oldAcctNo;

	/** 原账户名称 **/
	private String oldAcctName;

	/** 原开户行行号/机构编号 **/
	private String oldOpanOrgNo;

	/** 原开户行行名/机构名称 **/
	private String oldOpanOrgName;

	/** 原币种 **/
	private String oldCurType;

	/** 原支付金额 **/
	private java.math.BigDecimal oldDefrayAmt;

	/** 账号归属 **/
	private String acctBelong;

	/** 账号分类 **/
	private String acctClass;

	/** 账号 **/
	private String acctNo;

	/** 账号名称 **/
	private String acctName;

	/** 开户行行号 **/
	private String opanOrgNo;

	/** 开户行行名 **/
	private String opanOrgName;

	/** 机构编号 **/
	private String orgNo;

	/** 机构名称 **/
	private String orgName;

	/** 币种 **/
	private String curType;

	/** 支付金额 **/
	private java.math.BigDecimal defrayAmt;

	/** 退汇原因 **/
	private String rruResn;

	/** 授权状态 **/
	private String authStatus;

	/** 退汇修改说明 **/
	private String rruExpl;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 申请状态 **/
	private String approveStatus;

	/** 操作类型 **/
	private String oprType;


	/**
	 * @param rruSerno
	 */
	public void setRruSerno(String rruSerno) {
		this.rruSerno = rruSerno == null ? null : rruSerno.trim();
	}

    /**
     * @return RruSerno
     */
	public String getRruSerno() {
		return this.rruSerno;
	}

	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno == null ? null : oldSerno.trim();
	}

    /**
     * @return OldSerno
     */
	public String getOldSerno() {
		return this.oldSerno;
	}

	/**
	 * @param accptBookNo
	 */
	public void setAccptBookNo(String accptBookNo) {
		this.accptBookNo = accptBookNo == null ? null : accptBookNo.trim();
	}

    /**
     * @return AccptBookNo
     */
	public String getAccptBookNo() {
		return this.accptBookNo;
	}

	/**
	 * @param newAuthNo
	 */
	public void setNewAuthNo(String newAuthNo) {
		this.newAuthNo = newAuthNo == null ? null : newAuthNo.trim();
	}

    /**
     * @return NewAuthNo
     */
	public String getNewAuthNo() {
		return this.newAuthNo;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}

    /**
     * @return BillNo
     */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param authNo
	 */
	public void setAuthNo(String authNo) {
		this.authNo = authNo == null ? null : authNo.trim();
	}

    /**
     * @return AuthNo
     */
	public String getAuthNo() {
		return this.authNo;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}

    /**
     * @return ContNo
     */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

    /**
     * @return CusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param pvpDate
	 */
	public void setPvpDate(String pvpDate) {
		this.pvpDate = pvpDate == null ? null : pvpDate.trim();
	}

    /**
     * @return PvpDate
     */
	public String getPvpDate() {
		return this.pvpDate;
	}

	/**
	 * @param acctAttr
	 */
	public void setAcctAttr(String acctAttr) {
		this.acctAttr = acctAttr == null ? null : acctAttr.trim();
	}

    /**
     * @return AcctAttr
     */
	public String getAcctAttr() {
		return this.acctAttr;
	}

	/**
	 * @param oldAcctBelong
	 */
	public void setOldAcctBelong(String oldAcctBelong) {
		this.oldAcctBelong = oldAcctBelong == null ? null : oldAcctBelong.trim();
	}

    /**
     * @return OldAcctBelong
     */
	public String getOldAcctBelong() {
		return this.oldAcctBelong;
	}

	/**
	 * @param oldAcctClass
	 */
	public void setOldAcctClass(String oldAcctClass) {
		this.oldAcctClass = oldAcctClass == null ? null : oldAcctClass.trim();
	}

    /**
     * @return OldAcctClass
     */
	public String getOldAcctClass() {
		return this.oldAcctClass;
	}

	/**
	 * @param oldAcctNo
	 */
	public void setOldAcctNo(String oldAcctNo) {
		this.oldAcctNo = oldAcctNo == null ? null : oldAcctNo.trim();
	}

    /**
     * @return OldAcctNo
     */
	public String getOldAcctNo() {
		return this.oldAcctNo;
	}

	/**
	 * @param oldAcctName
	 */
	public void setOldAcctName(String oldAcctName) {
		this.oldAcctName = oldAcctName == null ? null : oldAcctName.trim();
	}

    /**
     * @return OldAcctName
     */
	public String getOldAcctName() {
		return this.oldAcctName;
	}

	/**
	 * @param oldOpanOrgNo
	 */
	public void setOldOpanOrgNo(String oldOpanOrgNo) {
		this.oldOpanOrgNo = oldOpanOrgNo == null ? null : oldOpanOrgNo.trim();
	}

    /**
     * @return OldOpanOrgNo
     */
	public String getOldOpanOrgNo() {
		return this.oldOpanOrgNo;
	}

	/**
	 * @param oldOpanOrgName
	 */
	public void setOldOpanOrgName(String oldOpanOrgName) {
		this.oldOpanOrgName = oldOpanOrgName == null ? null : oldOpanOrgName.trim();
	}

    /**
     * @return OldOpanOrgName
     */
	public String getOldOpanOrgName() {
		return this.oldOpanOrgName;
	}

	/**
	 * @param oldCurType
	 */
	public void setOldCurType(String oldCurType) {
		this.oldCurType = oldCurType == null ? null : oldCurType.trim();
	}

    /**
     * @return OldCurType
     */
	public String getOldCurType() {
		return this.oldCurType;
	}

	/**
	 * @param oldDefrayAmt
	 */
	public void setOldDefrayAmt(java.math.BigDecimal oldDefrayAmt) {
		this.oldDefrayAmt = oldDefrayAmt;
	}

    /**
     * @return OldDefrayAmt
     */
	public java.math.BigDecimal getOldDefrayAmt() {
		return this.oldDefrayAmt;
	}

	/**
	 * @param acctBelong
	 */
	public void setAcctBelong(String acctBelong) {
		this.acctBelong = acctBelong == null ? null : acctBelong.trim();
	}

    /**
     * @return AcctBelong
     */
	public String getAcctBelong() {
		return this.acctBelong;
	}

	/**
	 * @param acctClass
	 */
	public void setAcctClass(String acctClass) {
		this.acctClass = acctClass == null ? null : acctClass.trim();
	}

    /**
     * @return AcctClass
     */
	public String getAcctClass() {
		return this.acctClass;
	}

	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}

    /**
     * @return AcctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}

	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}

    /**
     * @return AcctName
     */
	public String getAcctName() {
		return this.acctName;
	}

	/**
	 * @param opanOrgNo
	 */
	public void setOpanOrgNo(String opanOrgNo) {
		this.opanOrgNo = opanOrgNo == null ? null : opanOrgNo.trim();
	}

    /**
     * @return OpanOrgNo
     */
	public String getOpanOrgNo() {
		return this.opanOrgNo;
	}

	/**
	 * @param opanOrgName
	 */
	public void setOpanOrgName(String opanOrgName) {
		this.opanOrgName = opanOrgName == null ? null : opanOrgName.trim();
	}

    /**
     * @return OpanOrgName
     */
	public String getOpanOrgName() {
		return this.opanOrgName;
	}

	/**
	 * @param orgNo
	 */
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo == null ? null : orgNo.trim();
	}

    /**
     * @return OrgNo
     */
	public String getOrgNo() {
		return this.orgNo;
	}

	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName == null ? null : orgName.trim();
	}

    /**
     * @return OrgName
     */
	public String getOrgName() {
		return this.orgName;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}

    /**
     * @return CurType
     */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param defrayAmt
	 */
	public void setDefrayAmt(java.math.BigDecimal defrayAmt) {
		this.defrayAmt = defrayAmt;
	}

    /**
     * @return DefrayAmt
     */
	public java.math.BigDecimal getDefrayAmt() {
		return this.defrayAmt;
	}

	/**
	 * @param rruResn
	 */
	public void setRruResn(String rruResn) {
		this.rruResn = rruResn == null ? null : rruResn.trim();
	}

    /**
     * @return RruResn
     */
	public String getRruResn() {
		return this.rruResn;
	}

	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus == null ? null : authStatus.trim();
	}

    /**
     * @return AuthStatus
     */
	public String getAuthStatus() {
		return this.authStatus;
	}

	/**
	 * @param rruExpl
	 */
	public void setRruExpl(String rruExpl) {
		this.rruExpl = rruExpl == null ? null : rruExpl.trim();
	}

    /**
     * @return RruExpl
     */
	public String getRruExpl() {
		return this.rruExpl;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}

    /**
     * @return ApproveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}


}
