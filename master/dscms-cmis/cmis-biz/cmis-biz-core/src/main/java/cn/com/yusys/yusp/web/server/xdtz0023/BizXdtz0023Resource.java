package cn.com.yusys.yusp.web.server.xdtz0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0023.req.Xdtz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.resp.Xdtz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0023.Xdtz0023Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;

/**
 * 接口处理类:保证金等级入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0023:保证金等级入账")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0023Resource.class);


    @Autowired
    private Xdtz0023Service xdtz0023Service;
    /**
     * 交易码：xdtz0023
     * 交易描述：保证金等级入账
     *
     * @param xdtz0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金等级入账")
    @PostMapping("/xdtz0023")
    protected @ResponseBody
    ResultDto<Xdtz0023DataRespDto> xdtz0023(@Validated @RequestBody Xdtz0023DataReqDto xdtz0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataReqDto));
        Xdtz0023DataRespDto xdtz0023DataRespDto = new Xdtz0023DataRespDto();// 响应Dto:保证金等级入账
        ResultDto<Xdtz0023DataRespDto> xdtz0023DataResultDto = new ResultDto<>();

        String bizType = xdtz0023DataReqDto.getBizType();//业务类型
        String oprtype = xdtz0023DataReqDto.getOprtype();//操作类型
        String cusId = xdtz0023DataReqDto.getCusId();//客户编号
        String cusName = xdtz0023DataReqDto.getCusName();//客户姓名
        String finaDept = xdtz0023DataReqDto.getFinaDept();//财务部门
        String endDate = xdtz0023DataReqDto.getEndDate();//到期日期
        String billNo = xdtz0023DataReqDto.getBillNo();//借据号
        String bailAcct = xdtz0023DataReqDto.getList().get(0).getBailAcct();//保证金账户
        BigDecimal securityAmt = xdtz0023DataReqDto.getList().get(0).getSecurityAmt();//保证金金额
        String intMode = xdtz0023DataReqDto.getList().get(0).getIntMode();//计息方式
        String bailIntDepAcctNo = xdtz0023DataReqDto.getList().get(0).getBailIntDepAcctNo();//保证金利息存入账号
        BigDecimal bailExchgRate = xdtz0023DataReqDto.getList().get(0).getBailExchgRate();//保证金汇率
        String cusSettlAcct = xdtz0023DataReqDto.getList().get(0).getCusSettlAcct();//客户结算账户
        BigDecimal cretQuotationExchgRate = xdtz0023DataReqDto.getList().get(0).getCretQuotationExchgRate();//信贷牌价汇率
        try {
            // 从xdtz0023DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdtz0023DataRespDto =xdtz0023Service.xdtz0023(xdtz0023DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdtz0023DataRespDto对象开始
//            xdtz0023DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
//            xdtz0023DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdtz0023DataRespDto对象结束
            // 封装xdtz0023DataResultDto中正确的返回码和返回信息
            xdtz0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, e.getMessage());
            // 封装xdtz0023DataResultDto中异常返回码和返回信息
            xdtz0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0023DataRespDto到xdtz0023DataResultDto中
        xdtz0023DataResultDto.setData(xdtz0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataResultDto));
        return xdtz0023DataResultDto;
    }
}
