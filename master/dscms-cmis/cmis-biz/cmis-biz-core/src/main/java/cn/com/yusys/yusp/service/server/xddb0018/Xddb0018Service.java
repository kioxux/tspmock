package cn.com.yusys.yusp.service.server.xddb0018;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.xddb0018.req.Xddb0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0018.resp.Xddb0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:押品状态同步
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xddb0018Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0018Service.class);

    @Resource
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;

    @Resource
    GuarContRelWarrantMapper guarContRelWarrantMapper;

    @Resource
    GuarMortgageManageAppMapper guarMortgageManageAppMapper;

    @Resource
    CtrLoanContMapper ctrLoanContMapper;
    @Resource
    CtrContImageAuditAppMapper ctrContImageAuditAppMapper;
    @Resource
    SequenceTemplateClient sequenceTemplateClient;

    @Resource
    GuarWarrantInfoService guarWarrantInfoService;
    /**
     * 押品状态同步
     *
     * @param xdkh0018DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0018DataRespDto updateWarrantSatteByGuarNo(Xddb0018DataReqDto xdkh0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value);
        Xddb0018DataRespDto xddb0018DataRespDto = new Xddb0018DataRespDto();

        try {
            String coreGrtNo = xdkh0018DataReqDto.getGuarNo();//押品编号
            String warrantState = xdkh0018DataReqDto.getGuarStatus();//押品状态
            int result = 0;//修改结果记录数
            String opFlag = "";//执行结果
            if (StringUtil.isNotEmpty(coreGrtNo)) {//押品编号非空校验
                logger.info("**************************首先根据要编号查询是否存在指定的记录**************************");
                result = guarWarrantInfoService.selectIsExistByCoreGuarantyNo(coreGrtNo);
                if (result > 0) {//存在指定押品记录，执行修改押品出入库状态操作
                    Map queryMap = new HashMap();
                    queryMap.put("coreGrtNo", coreGrtNo);//押品状态

                    if (CmisBizConstants.guarStatusMap.containsKey(warrantState)){
                        warrantState = CmisBizConstants.guarStatusMap.get(warrantState);
                    }
                    queryMap.put("warrantState", warrantState);//押品状态
                    result = guarWarrantInfoService.updateWarrantStateByCoreGrtNo(queryMap);
                    if (result > 0) {//修改操作成功
                        xddb0018DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                        xddb0018DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                        try{
                            List<GuarMortgageManageApp> mainContNo = new ArrayList<>();
                            //通过主押品编号从guarWarrantManageApp查流水号
                            String guarSerno = guarWarrantManageAppMapper.selectSernoByCoreGuarantyNo(xdkh0018DataReqDto.getGuarNo());
                            //查一条流水号为当前流水号的记录
                            List<GuarContRelWarrant> list = guarContRelWarrantMapper.selectGuarNoByGuarNoAndGuarContNo(guarSerno);
                            if(CollectionUtils.nonEmpty(list)){
                                //通过这条记录联表查询获取主合同号
                                Map queryMap2 = new HashMap();
                                queryMap2.put("guarNo", list.get(0).getGuarNo());
                                queryMap2.put("guarContNo", list.get(0).getGuarContNo());
                                mainContNo = guarMortgageManageAppMapper.selectMainContNoByGuarContNoAndGuarNo(queryMap2);
                            }
                            if(CollectionUtils.nonEmpty(mainContNo)){
                                //更新合同表,新插一条线上提款启用的数据
                                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(mainContNo.get(0).getMainContNo());
                                if(ctrLoanCont.getContStatus().equals("200")&&ctrLoanCont.getPrdId().equals("022028")){
                                    if(StringUtils.isEmpty(ctrLoanCont.getCtrBeginFlag())){
                                        ctrLoanCont.setCtrBeginFlag("1");
                                    }else if(ctrLoanCont.getCtrBeginFlag().equals("2")){
                                        ctrLoanCont.setCtrBeginFlag("1");
                                    }
                                    CtrContImageAuditApp ctrContImageAuditApp = new CtrContImageAuditApp();
                                    ctrContImageAuditApp.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>()));
                                    ctrContImageAuditApp.setContNo(ctrLoanCont.getContNo());
                                    ctrContImageAuditApp.setContType(ctrLoanCont.getContType());
                                    ctrContImageAuditApp.setCusId(ctrLoanCont.getCusId());
                                    ctrContImageAuditApp.setCusName(ctrLoanCont.getCusName());
                                    ctrContImageAuditApp.setPrdId(ctrLoanCont.getPrdId());
                                    ctrContImageAuditApp.setPrdName(ctrLoanCont.getPrdName());
                                    ctrContImageAuditApp.setCurType(ctrLoanCont.getCurType());
                                    ctrContImageAuditApp.setContAmt(ctrLoanCont.getContAmt());
                                    ctrContImageAuditApp.setManagerId(ctrLoanCont.getManagerId());
                                    ctrContImageAuditApp.setManagerBrId(ctrLoanCont.getManagerBrId());
                                    ctrContImageAuditApp.setGuarMode(mainContNo.get(0).getGuarWay());
                                    ctrContImageAuditApp.setOprType("01");
                                    ctrContImageAuditApp.setApproveStatus("997");
                                    ctrContImageAuditAppMapper.insertSelective(ctrContImageAuditApp);
                                    ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                                }
                            }
                        }catch (Exception e){
                            logger.info("**************************押品权证入库自动启动合同失败**************************");
                        }


                    } else {//修改记录失败
                        logger.info("**************************押品权证出入库状态修改失败**************************");
                        xddb0018DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0018DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
                    }
                } else {//不存在指定押品记录
                    logger.info("**************************不存在对应押品记录,押品状态同步结束*END*************************");
                    xddb0018DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xddb0018DataRespDto.setOpMsg(DscmsBizDbEnum.NO_RECORD.value);
                }
            } else {
                //请求参数不存在,返回空值
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value);
                xddb0018DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
                xddb0018DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value);
        return xddb0018DataRespDto;
    }
}
