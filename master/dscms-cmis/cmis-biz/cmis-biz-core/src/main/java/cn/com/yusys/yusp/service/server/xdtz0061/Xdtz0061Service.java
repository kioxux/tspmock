package cn.com.yusys.yusp.service.server.xdtz0061;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0061.req.Xdtz0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.resp.Xdtz0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 业务逻辑类:小微电子签约借据号生成
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0061Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0061Service.class);

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    /**
     * 交易码：xdtz0061
     * 交易描述：小微电子签约借据号生成
     *
     * @param xdtz0061DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0061DataRespDto xdtz0061(Xdtz0061DataReqDto xdtz0061DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataReqDto));
        Xdtz0061DataRespDto xdtz0061DataRespDto = new Xdtz0061DataRespDto();
        try {

            Map seqMap = new HashMap();
            seqMap.put("contNo", xdtz0061DataReqDto.getContNo());
            String billNo = cmisBizXwCommonService.getBillNo(xdtz0061DataReqDto.getContNo());

            xdtz0061DataRespDto.setBillNo(billNo);
            xdtz0061DataRespDto.setOpFlag("S");
            xdtz0061DataRespDto.setOpMsg("成功");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataRespDto));
        return xdtz0061DataRespDto;
    }
}
