package cn.com.yusys.yusp.service.client.bsp.outerdata.qyssxx;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/24 9:51
 * @desc    查询涉诉信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class QyssxxService {
    private static final Logger logger = LoggerFactory.getLogger(QyssxxService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * @param qyssxxReqDto
     * @return cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto
     * @author hubp
     * @date 2021/5/24 16:42
     * @version 1.0.0
     * @desc 查询涉诉信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public QyssxxRespDto qyssxx(QyssxxReqDto qyssxxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(qyssxxReqDto));
        ResultDto<QyssxxRespDto> qyssxxResultDto = dscms2OuterdataClientService.qyssxx(qyssxxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(qyssxxResultDto));
        String qyssxxCode = Optional.ofNullable(qyssxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String qyssxxMeesage = Optional.ofNullable(qyssxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        QyssxxRespDto qyssxxRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, qyssxxResultDto.getCode())) {
            //  获取相关的值并解析
            qyssxxRespDto = qyssxxResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(qyssxxCode, qyssxxMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value);
        return qyssxxRespDto;
    }
}
