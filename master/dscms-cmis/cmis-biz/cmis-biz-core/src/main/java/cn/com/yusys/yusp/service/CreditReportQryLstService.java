package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.CommonUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.*;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006RespDto;
import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.req.Xwd001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.resp.Xwd001RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.CreditAuthbookInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credxx.CredxxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.util.CmisBizCiis2ndUtils;
import cn.com.yusys.yusp.vo.CreditReportQryLstDZVo;
import cn.com.yusys.yusp.vo.CreditReportQryLstVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditReportQryLstService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditReportQryLstService {

    private static final Logger log = LoggerFactory.getLogger(CreditReportQryLstService.class);

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    @Autowired
    private CredxxService credxxService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;

    @Autowired
    private Dscms2SzzxClientService dscms2SzzxClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;

    @Value("${application.creditUrl.url}")
    private String creditUrl;

    @Autowired
    private CreditQryBizRealService creditQryBizRealService;

    @Autowired
    private CreditAuthbookInfoMapper creditAuthbookInfoMapper;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private Dscms2XwdClientService dscms2XwdClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private IqpContExtService iqpContExtService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private CmisPspClientService cmisPspClientService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CreditReportQryLst selectByPrimaryKey(String crqlSerno) {
        return creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CreditReportQryLst> selectAll(QueryModel model) {
        List<CreditReportQryLst> records = (List<CreditReportQryLst>) creditReportQryLstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditReportQryLst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //区分移动端与PC端排序规则
        if ("OAM".equals(Optional.ofNullable(model.getCondition().get("mobie")).orElse(StringUtils.EMPTY))) {
            model.setSort("updateTime desc");
        }
        //if (getDutyInfo()) {
        //    model.getCondition().put("managerId", SessionUtils.getUserInformation().getLoginCode());
        //}
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(CreditReportQryLst record) {
        if (record != null) {
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setIsSuccssInit("0");
            record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
            record.setQryStatus("001");
            User userInfo = SessionUtils.getUserInformation();
            // 获取用户信息
            if (userInfo != null && StringUtils.isEmpty(record.getManagerId())) {
                record.setManagerId(userInfo.getLoginCode());
                record.setManagerBrId(userInfo.getOrg().getCode());
            }
            record.setAuthWay(Optional.ofNullable(record.getAuthWay()).orElse("01"));
            //证件类型为身份证的时候进行身份校验
            String certType = record.getCertType();
            // A 身份证
            if ("A".equals(certType)) {
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
                // 证件类型 0101身份证
                idCheckReqDto.setCert_type("0101");//(cusIndivDto.getCertType()
                // 用户名称
                idCheckReqDto.setUser_name(record.getCusName());
                // 证件号码
                idCheckReqDto.setId_number(record.getCertCode());
                // 机构类型 2：银行营业厅
                idCheckReqDto.setSource_type("2");
                // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
                idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
                ResultDto<IdCheckRespDto> idCheckResultDto = dscms2OuterdataClientService.idCheck(idCheckReqDto);
                String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                IdCheckRespDto idCheckRespDto = null;
                if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    idCheckRespDto = idCheckResultDto.getData();
                    if (!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())) {
                        throw BizException.error(null, idCheckCode, idCheckMeesage);
                    } else {
                        creditReportQryLstMapper.insert(record);
                    }
                } else {
                    //抛出错误异常
                    throw BizException.error(null, idCheckCode, EcsEnum.CUS_INDIV_CHECK_RESULT01.value + idCheckMeesage);
                }
            } else {
                creditReportQryLstMapper.insert(record);
            }
        }
        return 0;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public CreditReportQryLst insertXw(CreditReportQryLstAndRealDto creditReportQryLstAndRealDto) {
        CreditReportQryLst record = new CreditReportQryLst();
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal() ;
        BeanUtils.copyProperties(creditReportQryLstAndRealDto, record);
        if (record != null) {
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setIsSuccssInit("0");
            record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
            record.setQryStatus("001");
            User userInfo = SessionUtils.getUserInformation();
            // 获取用户信息
            if (userInfo != null && StringUtils.isEmpty(record.getManagerId())) {
                record.setManagerId(userInfo.getLoginCode());
                record.setManagerBrId(userInfo.getOrg().getCode());
            }
            record.setAuthWay(Optional.ofNullable(record.getAuthWay()).orElse("01"));
            //证件类型为身份证的时候进行身份校验
            String certType = record.getCertType();
            // A 身份证
            if ("A".equals(certType)) {
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
                // 证件类型 0101身份证
                idCheckReqDto.setCert_type("0101");//(cusIndivDto.getCertType()
                // 用户名称
                idCheckReqDto.setUser_name(record.getCusName());
                // 证件号码
                idCheckReqDto.setId_number(record.getCertCode());
                // 机构类型 2：银行营业厅
                idCheckReqDto.setSource_type("2");
                // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
                idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
                ResultDto<IdCheckRespDto> idCheckResultDto = dscms2OuterdataClientService.idCheck(idCheckReqDto);
                String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                IdCheckRespDto idCheckRespDto = null;
                if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    idCheckRespDto = idCheckResultDto.getData();
                    if (!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())) {
                        throw BizException.error(null, idCheckCode, idCheckMeesage);
                    } else {
                        creditReportQryLstMapper.insert(record);
                    }
                } else {
                    //抛出错误异常
                    throw BizException.error(null, idCheckCode, EcsEnum.CUS_INDIV_CHECK_RESULT01.value + idCheckMeesage);
                }
            } else {
                creditReportQryLstMapper.insert(record);
            }
        }
        // 下一步插入关联表
        creditQryBizReal.setBizSerno(creditReportQryLstAndRealDto.getBizSerno());
        creditQryBizReal.setCrqlSerno(record.getCrqlSerno());
        creditQryBizReal.setCusId(record.getBorrowerCusId());
        creditQryBizReal.setCusName(record.getBorrowerCusName());
        creditQryBizReal.setCertType("A");
        creditQryBizReal.setCertCode(record.getBorrowerCertCode());
        creditQryBizReal.setBorrowRel(record.getBorrowRel());
        creditQryBizReal.setScene("02");
        int i = creditQryBizRealMapper.insert(creditQryBizReal);
        if (i != 1) {
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "新增征信异常！");
        }
        return record;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CreditReportQryLst record) {
        return creditReportQryLstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CreditReportQryLst record) {
        return creditReportQryLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CreditReportQryLst record) {
        return creditReportQryLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String crqlSerno) {
        return creditReportQryLstMapper.deleteByPrimaryKey(crqlSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return creditReportQryLstMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selectByApplySerno
     * @方法描述：获取授信申请流水号下的客户征信
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-28 下午 6:34
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<CreditReportQryLst> selectByApplySerno(String applySerno) {
        return creditReportQryLstMapper.selectByAppLySerno(applySerno);
    }

    /**
     * @方法名称：getGrpCusRelation
     * @方法描述：获取集团授信申请流水号下的客户征信
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-28 下午 6:34
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<CreditReportQryLst> getGrpCusRelation(String grpSerno) {
        return creditReportQryLstMapper.getGrpCusRelation(grpSerno);
    }

    public int updateApproveStatus(String crqlSerno, String approveStatus) {
        return creditReportQryLstMapper.updateApproveStatus(crqlSerno, approveStatus);
    }

    /**
     * @方法名称：selectCreditReportQryLstByCrqlSerno
     * @方法描述：根据业务流水号获取征信信息
     * @参数与返回说明：
     * @算法描述：
     */
    public List<CreditReportQryLstDto> selectCreditReportQryLstByCrqlSerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //区分移动端与PC端排序规则
        if ("OAM".equals(Optional.ofNullable(model.getCondition().get("mobie")).orElse(StringUtils.EMPTY))) {
            model.setSort("updateTime desc");
        }
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(model);
        PageHelper.clearPage();
        return list;
    }

    public List<CreditReportQryLstDto> selectAllCreditReportQryLstByCrqlSerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //区分移动端与PC端排序规则
        if ("OAM".equals(Optional.ofNullable(model.getCondition().get("mobie")).orElse(StringUtils.EMPTY))) {
            model.setSort("updateTime desc");
        }
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.selectAllCreditReportQryLstByCrqlSerno(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称：getCreditInfo
     * @方法描述：根据流水号获取征信台账信息
     * @参数与返回说明：
     * @算法描述：
     */
    public List<CreditReportQryLstDto> getCreditInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("reportCreateTime desc");
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.getCreditInfo(model);
        PageHelper.clearPage();
        return list;
    }

    public List<CreditReportQryLstVo> getCreditInfoVo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("reportCreateTime desc");
        List<CreditReportQryLstVo> list = creditReportQryLstMapper.getCreditInfoVo(model);
        PageHelper.clearPage();
        list.forEach((CreditReportQryLstVo item) -> {
            item.setManagerId(OcaTranslatorUtils.getUserName(item.getManagerId()));
            item.setManagerBrId(OcaTranslatorUtils.getOrgName(item.getManagerBrId()));
            item.setAuthbookContent(handleAuthbookContent(item.getAuthbookContent()));
        });
        return list;
    }

    public List<CreditReportQryLstDZVo> getCreditInfoDZVo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("reportCreateTime desc");
        List<CreditReportQryLstDZVo> list = creditReportQryLstMapper.getCreditInfoDZVo(model);
        PageHelper.clearPage();
        list.forEach((CreditReportQryLstDZVo item) -> {
            item.setManagerId(OcaTranslatorUtils.getUserName(item.getManagerId()));
            item.setManagerBrId(OcaTranslatorUtils.getOrgName(item.getManagerBrId()));
        });
        return list;
    }

    /**
     * @方法名称：handleAuthbookContent
     * @方法描述：手动处理授权书内容导出翻译
     * @参数与返回说明：
     * @算法描述：
     */
    public String handleAuthbookContent(String authbookContent) {
        /**
         * 001: 审核本人贷款申请, 002: 审核本人贷记卡、准贷记卡申请, 003: 审核本人作为担保人,
         * 004: 受理法人或其他组织的贷款申请或其作为担保人，需要查询本人作为法定代表人、出资人及关联人信用状况, 005: 资信审查,
         * 006: 特约商户实名审查, 007: 对已发放的信贷业务进行贷后风险管理, 008: 审核本单位信贷业务申请, 009: 审核本单位作为担保人,
         * 010: 审核本单位作为关联人, 011: 受理企业信用等级评定
         * */
        if (StringUtils.nonBlank(authbookContent)) {
            String[] tmp = StringUtils.split(authbookContent, ";");
            for (String item : tmp) {
                authbookContent = StringUtils.replaceAll(authbookContent, item, getAuthbookCus(item));
            }
        }
        return authbookContent;
    }

    public String getAuthbookCus(String key) {
        String value = key;
        switch (key) {
            case "001":
                value = "审核本人贷款申请";
                break;
            case "002":
                value = "审核本人贷记卡、准贷记卡申请";
                break;
            case "003":
                value = "审核本人作为担保人";
                break;
            case "004":
                value = "受理法人或其他组织的贷款申请或其作为担保人，需要查询本人作为法定代表人、出资人及关联人信用状况";
                break;
            case "005":
                value = "资信审查";
                break;
            case "006":
                value = "特约商户实名审查";
                break;
            case "007":
                value = "对已发放的信贷业务进行贷后风险管理";
                break;
            case "008":
                value = "审核本单位信贷业务申请";
                break;
            case "009":
                value = "审核本单位作为担保人";
                break;
            case "010":
                value = "审核本单位作为关联人";
                break;
            case "011":
                value = "受理企业信用等级评定";
                break;
            default:
                break;
        }
        return value;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param crqlSerno
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDataAfterStart(String crqlSerno) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取征信申请{}申请主表信息", crqlSerno);
            int updateCount = 0;
            log.info("流程发起-更新征信合同申请{}流程审批状态为【111】-审批中", crqlSerno);
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_111);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if(Objects.isNull(creditReportQryLst.getSendTime())) {
                creditReportQryLstMapper.updateApproveDate(crqlSerno);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程发起业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    public void checkAuthWay(CreditReportQryLst creditReportQryLst) {
        //贷后管理需满足校验规则：授权书内容勾选过贷后条款，主借款人该笔业务的借据未结清
        if ("27".equals(creditReportQryLst.getQryResn()) && "02".equals(creditReportQryLst.getAuthWay())) {
            //TODO:2.主借款人该笔业务的借据未结清
            if (StringUtils.nonBlank(creditReportQryLst.getBorrowerCertCode())) {
                //根据借款人证件号 获取借款人客户号 调用cus服务
                CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(creditReportQryLst.getBorrowerCertCode());
                if (cusBaseClientDto != null) {
                    BigDecimal existAcc = accLoanService.checkCusNotClosedAccZXByCusId(cusBaseClientDto.getCusId());
                    BigDecimal existAccp = accLoanService.checkCusNotClosedAccpByCusId(cusBaseClientDto.getCusId());
                    BigDecimal existAccd = accLoanService.checkCusNotClosedAccdByCusId(cusBaseClientDto.getCusId());
                    BigDecimal existAccc = accLoanService.checkCusNotClosedAcccByCusId(cusBaseClientDto.getCusId());
                    BigDecimal existAcct = accLoanService.checkCusNotClosedAcctByCusId(cusBaseClientDto.getCusId());
                    BigDecimal existAcce = accLoanService.checkCusNotClosedAcceByCusId(cusBaseClientDto.getCusId());
                    if (existAcc.compareTo(BigDecimal.ZERO) <= 0 && existAccp.compareTo(BigDecimal.ZERO) <= 0 && existAccd.compareTo(BigDecimal.ZERO) <= 0 && existAccc.compareTo(BigDecimal.ZERO) <= 0 && existAcct.compareTo(BigDecimal.ZERO ) <= 0 && existAcce.compareTo(BigDecimal.ZERO ) <= 0) {
                        throw BizException.error(null, "9999", "客户没有未结清借据信息，无法申请！");
                    }
                } else {
                    throw BizException.error(null, "9999", "客户没有未结清借据信息，无法申请！");
                }
            }
        }
    }

    public void commitStart(String crqlSerno, String qryCls, String outSystemCode, String imageId) {
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (Objects.isNull(userInfo)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            //判断是否是存在在途任务
            CreditReportQryLst creditReportQryLstOrgin = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLstOrgin == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            QueryModel model = new QueryModel();
            model.getCondition().put("certCode", creditReportQryLstOrgin.getCertCode());
            model.getCondition().put("certType", creditReportQryLstOrgin.getCertType());
            model.addCondition("qryResn",creditReportQryLstOrgin.getQryResn());
            model.addCondition("approveStatus","111");
            checkApplyExistsStatus(model);// 风险拦截实现

            if ("3".equals(qryCls)) {
                //判断是否是对苏州地区企业客户发起的苏州地方征信查询
                CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
                if (creditReportQryLst == null) {
                    throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                }
                ResultDto<CusCorpDto> cusCorpDtoResultDto = icusClientService.queryCusCropDtoByCusId(creditReportQryLst.getCusId());
                if (cusCorpDtoResultDto != null && cusCorpDtoResultDto.getData() != null) {
                    CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
                    if (!StringUtils.startsWith(cusCorpDto.getRegiAreaCode(), "3205") && !StringUtils.startsWith(cusCorpDto.getRegiAddr(), "3205")) {
                        log.info("客户号：{},不是苏州地区企业，请检查该客户的注册登记地址!", cusCorpDto.getCusId());
                        throw BizException.error(null, "9999", "只能对苏州地区企业发起苏州地方征信申请");
                    }
                }
                //苏州地方征信每日查询笔数上限（笔）
                int szCount = 0;
                String querySzEverydayCount = getSysParameterByName("QUERY_SZ_EVERYDAY_COUNT");
                if (StringUtils.isBlank(querySzEverydayCount)) {
                    throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
                }
                szCount = Integer.parseInt(querySzEverydayCount);
                int SZCount = creditReportQryLstMapper.selectZXQueryCount(userInfo.getLoginCode(), "3", DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                if (SZCount >= szCount) {
                    throw BizException.error(null, "9999", "客户经理每日发起查询的笔数上限为" + szCount + "笔");
                }
            } else if ("0".equals(qryCls) || "1".equals(qryCls)) {
                //校验是否完成影像
                ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
                imageDataReqDto.setDocId(imageId);
                imageDataReqDto.setOutSystemCode(outSystemCode);
                Map<String, String> indexMap = new HashMap<>();
                indexMap.put("index", imageId);
                imageDataReqDto.setIndexMap(indexMap);
                ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
                if (map.getData() == null || map.getData().size() == 0) {
                    log.info("根据调查流水号{}未获取到影像信息，不允许提交！", crqlSerno);
                    throw BizException.error(null, "9999", "请上传影像资料");
                } else {
                    CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
                    //for (Map.Entry<String, Integer> vo : map.getData().entrySet()) {
                    //    creditReportQryLst.setImageNo(vo.getKey());
                    //}
                    creditReportQryLst.setImageNo(imageId);
                    creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);
                }

                if (StringUtils.isBlank(crqlSerno)) {
                    throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
                }
                CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
                if (creditReportQryLst == null) {
                    throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
                }
                //贷后管理需满足校验规则：授权书内容勾选过贷后条款，主借款人该笔业务的借据未结清
                if ("27".equals(creditReportQryLst.getQryResn())) {
                    //TODO:1.授权书内容勾选过贷后条款
                    if (!StringUtils.containsAny(creditReportQryLst.getAuthbookContent(), "007")) {
                        throw BizException.error(null, "9999", "授权书内容未勾选贷后条款");
                    }
                    //TODO:2.主借款人该笔业务的借据未结清
                    if (StringUtils.nonBlank(creditReportQryLst.getBorrowerCertCode())) {
                        //根据借款人证件号 获取借款人客户号 调用cus服务
                        CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(creditReportQryLst.getBorrowerCertCode());
                        if (cusBaseClientDto != null) {
                            BigDecimal existAcc = accLoanService.checkCusNotClosedAccZXByCusId(cusBaseClientDto.getCusId());
                            BigDecimal existAccp = accLoanService.checkCusNotClosedAccpByCusId(cusBaseClientDto.getCusId());
                            BigDecimal existAccd = accLoanService.checkCusNotClosedAccdByCusId(cusBaseClientDto.getCusId());
                            BigDecimal existAccc = accLoanService.checkCusNotClosedAcccByCusId(cusBaseClientDto.getCusId());
                            BigDecimal existAcct = accLoanService.checkCusNotClosedAcctByCusId(cusBaseClientDto.getCusId());
                            BigDecimal existAcce = accLoanService.checkCusNotClosedAcceByCusId(cusBaseClientDto.getCusId());
                            if (existAcc.compareTo(BigDecimal.ZERO) <= 0 && existAccp.compareTo(BigDecimal.ZERO) <= 0 && existAccd.compareTo(BigDecimal.ZERO) <= 0 && existAccc.compareTo(BigDecimal.ZERO) <= 0 && existAcct.compareTo(BigDecimal.ZERO ) <= 0 && existAcce.compareTo(BigDecimal.ZERO ) <= 0) {
                                throw BizException.error(null, "9999", "客户没有未结清借据信息，无法申请！");
                            }
                        } else {
                            throw BizException.error(null, "9999", "客户没有未结清借据信息，无法申请！");
                        }
                    }
                }
                boolean result = false;
                if (userInfo != null) {
                    if (!userInfo.getLoginCode().equals(creditReportQryLst.getManagerId())) {
                        result = true;
                    }
                }
                if (!"27".equals(creditReportQryLst.getQryResn()) && !result) {
                    //判断授权书日期
                    //不取电脑时间，取系统时间进行判断
                    //int dateTimes = calc(creditReportQryLst.getAuthbookDate(), DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    String openday = stringRedisTemplate.opsForValue().get("openDay");
                    int dateTimes = calc(creditReportQryLst.getAuthbookDate(), openday);
                    int authTimeDays = 0;
                    //获取征信授权书有效期天数
                    String authBookDays = getSysParameterByName("AUTH_BOOK_DAYS");
                    if (StringUtils.isBlank(authBookDays)) {
                        throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
                    }
                    authTimeDays = Integer.parseInt(authBookDays);
                    if (dateTimes >= authTimeDays) {
                        throw BizException.error(null, "9999", "授权书日期超" + authTimeDays + "个工作日，查询征信报告失败");
                    }
                }

                //人行征信每日查询笔数上限（笔）
                int rhCount = 0;
                String queryRhEverydayCount = getSysParameterByName("QUERY_RH_EVERYDAY_COUNT");
                if (StringUtils.isBlank(queryRhEverydayCount)) {
                    throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
                }
                rhCount = Integer.parseInt(queryRhEverydayCount);
                int RHCount = creditReportQryLstMapper.selectZXQueryCount(userInfo.getLoginCode(), "0,1",
                        DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                if (RHCount >= rhCount) {
                    throw BizException.error(null, "9999", "客户经理每日发起查询的笔数上限为" + rhCount + "笔");
                }
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public Map<String, String> handleCreitData(String crqlSerno) {
        try {
            /**
             * 1、查询时，发起申请先调用苏州地方征信查询接口
             * 2、如果接口返回不是成功，则判断是否异地分支行发起的查询
             *         是异地分行时，直接拒绝，需线下完成征信授权
             *         否则提交查询流程
             * 3、如果接口返回成功，则直接生成查询申请记录，可查阅地方征信报告
             * */
            try {
                Map<String, String> result = new HashMap<>();
                handleBusinessDZData(crqlSerno);
                return result;
            } catch (BizException e) {
                log.error("业务流水号：{}，地方征信查询接口查询失败，失败原因：{}", crqlSerno, e.getMessage());
                CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);
                Map<String, String> result = new HashMap<>();
                result.put("code", e.getErrorCode());
                result.put("erortx", e.getMessage());
                return result;
                // throw BizException.error(null, e.getErrorCode(), e.getMessage());
                //地方征信判断是否是异步机构
                // User userInfo = SessionUtils.getUserInformation();
                // 获取用户信息
                // if (userInfo == null) {
                //    throw BizException.error(null, "9999", "获取用户信息异常");
                // }
                // ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(userInfo.getOrg().getCode());
                // AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                // if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())) {
                //    throw BizException.error(null, "ZX9999", "异地机构,需线下完成征信授权");
                // } else {
                //    throw e;
                // }
            }
        } catch (BizException e) {
            log.error("处理地方征信业务发生异常，异常信息：{}", e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error("处理地方征信业务发生异常，异常信息：{}", e.getMessage());
            throw e;
        }
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param crqlSerno
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDataAfterEnd(String crqlSerno, ResultInstanceDto resultInstanceDto) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取征信申请{}申请主表信息", crqlSerno);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            int updateCount = 0;
            log.info("审批通过生成获征信信息-发送征信系统{}信息", crqlSerno);

            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            CredxxReqDto credxxReqDto = new CredxxReqDto();
            credxxReqDto.setCreditType(creditReportQryLst.getQryCls());//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(creditReportQryLst.getCusName());//客户名称
            credxxReqDto.setCertificateType(toConverCurrency(creditReportQryLst.getCertType()));//证件类型
            credxxReqDto.setCertificateNum(creditReportQryLst.getCertCode());//证件号
            credxxReqDto.setQueryReason(convertQueryReason(creditReportQryLst.getQryResn(), creditReportQryLst.getQryCls()));//查询原因
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserId(creditReportQryLst.getManagerId());//客户经理工号
            credxxReqDto.setBrchno(creditReportQryLst.getManagerBrId());
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("1");//信用报告复用策略 0-强制查询征信征信 1-查询1天内的本地报告
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            User userInfo = SessionUtils.getUserInformation();
            ResultDto<AdminSmUserDto> resultDtoSp = adminSmUserService.getByLoginCode(resultInstanceDto.getCurrentUserId());
            AdminSmUserDto adminSmUserDtoSp = resultDtoSp.getData();
            if (adminSmUserDtoSp != null) {
                credxxReqDto.setApprovalName(adminSmUserDtoSp.getUserName());//审批人姓名
                credxxReqDto.setApprovalIdCard(adminSmUserDtoSp.getCertNo());//审批人身份证号
            } else {
                //总行：朱真尧 东海：邵雯
                if (creditReportQryLst.getManagerBrId().startsWith("81")) {
                    credxxReqDto.setApprovalName("邵雯");//审批人姓名
                    credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
                } else {
                    credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                    credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
                }

            }

            credxxReqDto.setArchiveCreateDate(creditReportQryLst.getAuthbookDate());//授权书签订日期
            // 授权结束日期，如果不是贷后，应该是授权日期+有效期。如果是贷后+50年
            if (Objects.equals("27", creditReportQryLst.getQryResn())) {
                credxxReqDto.setArchiveExpireDate(DateUtils.addYear(creditReportQryLst.getAuthbookDate(), DateFormatEnum.DEFAULT.getValue(), 50));//授权结束日期
            } else {
                //获取征信授权书有效期天数
                String authBookDays = getSysParameterByName("AUTH_BOOK_DAYS");
                credxxReqDto.setArchiveExpireDate(DateUtils.addDay(creditReportQryLst.getAuthbookDate(), DateFormatEnum.DEFAULT.getValue(), Integer.parseInt(Optional.ofNullable(authBookDays).orElse("0"))));//授权结束日期
            }
            credxxReqDto.setArchiveExpireDate(DateUtils.addYear(creditReportQryLst.getAuthbookDate(), DateFormatEnum.DEFAULT.getValue(), 50));//授权结束日期
            credxxReqDto.setCreditDocId(creditReportQryLst.getImageNo());//授权影像编号
            credxxReqDto.setBorrowPersonRelation(creditReportQryLst.getBorrowRel());//与主借款人关系
            credxxReqDto.setBorrowPersonRelationName(creditReportQryLst.getBorrowerCusName());//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber(creditReportQryLst.getBorrowerCertCode());//主借款人证件号
            credxxReqDto.setAuditReason(convertAuthCont(creditReportQryLst.getAuthbookContent(), creditReportQryLst.getQryCls()));//授权书内容

            ResultDto<CredxxRespDto> credxxResultDto = dscms2Ciis2ndClientService.credxx(credxxReqDto);
            String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            CredxxRespDto credxxRespDto = null;
            // 征信白户处理
            if ("6666".equals(credxxResultDto.getCode())) {
                log.info("征信中心无信用信息---此异常特殊处理");
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setQryStatus("003");

                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                if ("05".equals(creditReportQryLst.getQryFlag())) {
                    creditAuthbookInfo.setAuthMode("01");
                } else {
                    creditAuthbookInfo.setAuthMode("02");
                }
                creditAuthbookInfo.setImageNo(crqlSerno);
                // 授权书台账，根据授权编号，有则更新，无则新增
                CreditAuthbookInfo creditAuthbookInfoOld = creditAuthbookInfoMapper.selectByAuthBookNo(creditAuthbookInfo);
                if (creditAuthbookInfoOld != null) {
                    BeanUtils.copyProperties(creditAuthbookInfo, creditAuthbookInfoOld);
                    creditAuthbookInfoService.updateSelective(creditAuthbookInfoOld);
                } else {
                    creditAuthbookInfoService.insert(creditAuthbookInfo);
                }
            } else {
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
                    //  获取相关的值并解析
                    credxxRespDto = credxxResultDto.getData();
                    String zxSerno = credxxRespDto.getReportId();
                    creditReportQryLst.setReportNo(zxSerno);
                    if (!StringUtils.isEmpty(zxSerno)) {
                        creditReportQryLst.setIsSuccssInit("1");
                        creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                        creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                        creditReportQryLst.setQryStatus("003");

                        CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
                        callCiis2ndReqDto.setPrefixUrl(creditUrl);
                        callCiis2ndReqDto.setReqId(credxxRespDto.getReqId());
                        callCiis2ndReqDto.setOrgName("信贷");
                        callCiis2ndReqDto.setUserCode(creditReportQryLst.getManagerId());
                        callCiis2ndReqDto.setUserName(adminSmUserDto.getUserName());
                        if ("0".equals(creditReportQryLst.getQryCls())) {
                            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
                        } else {
                            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_ENT.key);
                        }
                        String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
                        creditReportQryLst.setCreditUrl(url);
                    } else {
                        creditReportQryLst.setQryStatus("002");
                    }
                } else {
                    //  抛出错误异常
                    throw BizException.error(null, credxxCode,credxxMeesage);
                    // 不抛出错误异常，记录错误异常处理
             /*     log.error("二代征信交易失败，流水号：{}，错误码值：{}，错误信息：{}", crqlSerno, credxxCode, credxxMeesage);
                    BizCommonUtils BizCommonUtils = new BizCommonUtils();
                    BizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, credxxCode, credxxMeesage), resultInstanceDto);
                */
                }
                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                if ("05".equals(creditReportQryLst.getQryFlag())) {
                    creditAuthbookInfo.setAuthMode("01");
                } else {
                    creditAuthbookInfo.setAuthMode("02");
                }
                creditAuthbookInfo.setImageNo(crqlSerno);
                // 授权书台账，根据授权编号，有则更新，无则新增
                CreditAuthbookInfo creditAuthbookInfoOld = creditAuthbookInfoMapper.selectByAuthBookNo(creditAuthbookInfo);
                if (creditAuthbookInfoOld != null) {
                    BeanUtils.copyProperties(creditAuthbookInfo, creditAuthbookInfoOld);
                    creditAuthbookInfoService.updateSelective(creditAuthbookInfoOld);
                } else {
                    creditAuthbookInfoService.insert(creditAuthbookInfo);
                }
            }
//            Credi12ReqDto credi12ReqDto = new Credi12ReqDto();
//            credi12ReqDto.setCreditType("0");//报告类型 0 个人 1 企业
//            credi12ReqDto.setCustomName(creditReportQryLst.getCusName());//客户名称
//            credi12ReqDto.setCertificateType(creditReportQryLst.getCertType());//证件类型
//            credi12ReqDto.setCertificateNum(creditReportQryLst.getCertCode());//证件号
//            credi12ReqDto.setQueryReason(creditReportQryLst.getQryResn());//查询原因
//            credi12ReqDto.setCreateUserCode(adminSmUserDto.getCertNo());//客户经理身份证号
//            credi12ReqDto.setApplySrc("XDG");//申请来源
//            credi12ReqDto.setCreateUser(adminSmUserDto.getUserName());//客户经理名称
//            credi12ReqDto.setSystemCode("XDG");//系统编号
//            credi12ReqDto.setBusiness("XDG");//产品业务线
//            credi12ReqDto.setBorrowPersonRelation(creditReportQryLst.getBorrowRel());//与主借款人关系
//            credi12ReqDto.setAreaName("");//片区名称
//            credi12ReqDto.setAreaId("");//片区编号
//            credi12ReqDto.setSyncFlag("0");//是否需要审批 0 无需审批 1 需要审批
//            credi12ReqDto.setIsAutoCheckPass("true");//是否自动审批通过
//            credi12ReqDto.setCreditDocId(creditReportQryLst.getImageNo());//授权影像编号
//            credi12ReqDto.setCreateUserPhone(adminSmUserDto.getUserMobilephone());//客户经理手机号
//            credi12ReqDto.setBorrowPersonRelationName(creditReportQryLst.getBorrowerCusName());//主借款人名称
//            credi12ReqDto.setBorrowPersonRelationNumber(creditReportQryLst.getBorrowerCertCode());//主借款人证件号
//            credi12ReqDto.setAuditReason(creditReportQryLst.getAuthbookContent());//授权书内容
//            ResultDto<Credi12RespDto>  resultDtoResp12 =dscms2Ciis2ndClientService.credi12(credi12ReqDto);
//            Credi12RespDto  credi12RespDto= resultDtoResp12.getData();
//            String zxSerno12 = credi12RespDto.getReportId();
//            creditReportQryLst.setReportNo(zxSerno);

            creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
            creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);

            log.info("审批通过-更新征信业务申请{}流程审批状态为【997】-通过", crqlSerno);
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_997);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            if (!"27".equals(creditReportQryLst.getQryResn()) && "1".equals(creditReportQryLst.getIsSuccssInit()) && !"04".equals(creditReportQryLst.getQryFlag())) {
                DocCreditArchiveClientDto docCreditArchiveClientDto = new DocCreditArchiveClientDto();
                BeanUtils.copyProperties(creditReportQryLst, docCreditArchiveClientDto);
                docCreditArchiveClientDto.setBizSerno(creditReportQryLst.getCrqlSerno());
                docCreditArchiveClientDto.setQryUser(creditReportQryLst.getManagerId());
                docCreditArchiveClientDto.setQryOrg(creditReportQryLst.getManagerBrId());
                docCreditArchiveClientDto.setInputId(creditReportQryLst.getManagerId());
                docCreditArchiveClientDto.setInputBrId(creditReportQryLst.getManagerBrId());
                cmisBizClientService.createDocCreateArchiveBySys(docCreditArchiveClientDto);
            }

        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     * 2、新增档案任务
     *
     * @param resultInstanceDto
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDZDataAfterStart(ResultInstanceDto resultInstanceDto) {
        try {
            String crqlSerno = resultInstanceDto.getBizId();
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取征信申请{}申请主表信息", crqlSerno);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;

            log.info("流程发起-更新征信合同申请{}流程审批状态为【111】-审批中", crqlSerno);
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_111);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            // 如果是客户经理提交 新增一条档案任务
            if ("KHJL".equals(resultInstanceDto.getNodeSign())) {
                //3、新增档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(creditReportQryLst.getCrqlSerno());
                centralFileTaskdto.setCusId(creditReportQryLst.getCusId());
                centralFileTaskdto.setCusName(creditReportQryLst.getCusName());
                centralFileTaskdto.setBizType(resultInstanceDto.getBizType()); // 征信资料
                centralFileTaskdto.setInputId(creditReportQryLst.getManagerId());
                centralFileTaskdto.setInputBrId(creditReportQryLst.getManagerBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setTaskType("04"); // 档案暂存及派发
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                int i = centralFileTaskService.insertSelective(centralFileTaskdto);
                if (i < 1) {
                    log.error("征信业务申请流程发起业务处理发生异常！新增档案任务失败，征信业务申请流水号【{}】", crqlSerno);
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
                }
            } else {
                log.info("非客户经理角色，无法新增档案池任务，征信业务申请流水号【{}】，当前角色为：{}", crqlSerno, resultInstanceDto.getNodeSign());
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程发起业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param crqlSerno
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDZDataAfterEnd(String crqlSerno, ResultInstanceDto resultInstanceDto) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取征信申请" + crqlSerno + "申请主表信息");
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;

            log.info("审批通过生成获征信信息-发送征信系统" + crqlSerno + "信息");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();

            ResultDto<AdminSmOrgDto> resultDto1 = adminSmOrgService.getByOrgCode(creditReportQryLst.getManagerBrId());
            AdminSmOrgDto adminSmOrgDto = resultDto1.getData();

            Zxcx006ReqDto zxcx006ReqDto = new Zxcx006ReqDto();
            // 查询苏州地方征信信息时，先获取taskId，再获取征信url
            zxcx006ReqDto.setState("new");    //请求类型
            zxcx006ReqDto.setReportReason(creditReportQryLst.getQryResn());    //查询原因
            zxcx006ReqDto.setQuerystaffNo(creditReportQryLst.getManagerId());    //客户经理工号
            zxcx006ReqDto.setQuerystaff(adminSmUserDto.getUserName());    //客户经理名称
            zxcx006ReqDto.setInqueryorgNo(adminSmOrgDto.getOrgCode());    //查询机构号
            zxcx006ReqDto.setInqueryorg(adminSmOrgDto.getOrgName());    //查询机构名称
            zxcx006ReqDto.setCardCode(creditReportQryLst.getCertCode());    //证件号码
            //测试环境：ZJGNS-ZXD；生产环境：zjgns-ssjk
            String zxWsUser = getSysParameterByName("ZX_WS_USER");
            zxcx006ReqDto.setWsUser(Optional.ofNullable(zxWsUser).orElse("ZJGNS-ZXD"));    //征信提供的用户名

            ResultDto<Zxcx006RespDto> zxcx006ResultDto = dscms2SzzxClientService.zxcx006(zxcx006ReqDto);
            String zxcx006Code = Optional.ofNullable(zxcx006ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zxcx006Meesage = Optional.ofNullable(zxcx006ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Zxcx006RespDto zxcx006RespDto = null;
            if (Objects.equals(zxcx006Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取相关的值并解析
                zxcx006RespDto = zxcx006ResultDto.getData();
                String taskId = zxcx006RespDto.getTaskId(); //任务id,否,state为new时，必输，作为查询时的taskId传入；state为old时，非必输

                if (!StringUtils.isEmpty(taskId)) {
                    //苏州地方征信，由于需要数据，暂时使用给定可用的值就行处理
                    creditReportQryLst.setIsSuccssInit("1");
                    creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setQryStatus("003");
                    creditReportQryLst.setReportNo(taskId);

                    if (StringUtils.startsWith(zxcx006RespDto.getErortx(), "http")) {
                        String url = zxcx006RespDto.getErortx(); //url
                        creditReportQryLst.setCreditUrl(url);
                    } else {
                        throw BizException.error(null, "9999", zxcx006RespDto.getErortx());
                    }
                } else {
                    creditReportQryLst.setQryStatus("002");
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null,zxcx006Code,zxcx006Meesage);
                // 不抛出错误异常，记录错误异常处理
          /*      log.error("苏州地方征信交易失败，流水号：{}，错误码值：{}，错误信息：{}", crqlSerno, zxcx006Code, zxcx006Meesage);
                BizCommonUtils BizCommonUtils = new BizCommonUtils();
                BizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, zxcx006Code, zxcx006Meesage), resultInstanceDto);
            */}
            creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);

            log.info("审批通过-更新征信业务申请" + crqlSerno + "流程审批状态为【997】-通过");
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_997);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    public void handleBusinessDZData(String crqlSerno) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取征信申请" + crqlSerno + "申请主表信息");
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;

            log.info("审批通过生成获征信信息-发送征信系统" + crqlSerno + "信息");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();

            ResultDto<AdminSmOrgDto> resultDto1 = adminSmOrgService.getByOrgCode(creditReportQryLst.getManagerBrId());
            AdminSmOrgDto adminSmOrgDto = resultDto1.getData();

            Zxcx006ReqDto zxcx006ReqDto = new Zxcx006ReqDto();
            // 查询苏州地方征信信息时，先获取taskId，再获取征信url
            zxcx006ReqDto.setState("new");    //请求类型
            zxcx006ReqDto.setReportReason(creditReportQryLst.getQryResn());    //查询原因
            zxcx006ReqDto.setQuerystaffNo(creditReportQryLst.getManagerId());    //客户经理工号
            zxcx006ReqDto.setQuerystaff(adminSmUserDto.getUserName());    //客户经理名称
            zxcx006ReqDto.setInqueryorgNo(adminSmOrgDto.getOrgCode());    //查询机构号
            zxcx006ReqDto.setInqueryorg(adminSmOrgDto.getOrgName());    //查询机构名称
            zxcx006ReqDto.setCardCode(creditReportQryLst.getCertCode());    //证件号码
            //测试环境：ZJGNS-ZXD；生产环境：zjgns-ssjk
            String zxWsUser = getSysParameterByName("ZX_WS_USER");
            zxcx006ReqDto.setWsUser(Optional.ofNullable(zxWsUser).orElse("ZJGNS-ZXD"));    //征信提供的用户名

            ResultDto<Zxcx006RespDto> zxcx006ResultDto = dscms2SzzxClientService.zxcx006(zxcx006ReqDto);
            String zxcx006Code = Optional.ofNullable(zxcx006ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zxcx006Meesage = Optional.ofNullable(zxcx006ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Zxcx006RespDto zxcx006RespDto = null;
            if (Objects.equals(zxcx006Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取相关的值并解析
                zxcx006RespDto = zxcx006ResultDto.getData();
                String taskId = zxcx006RespDto.getTaskId(); //任务id,否,state为new时，必输，作为查询时的taskId传入；state为old时，非必输

                if (!StringUtils.isEmpty(taskId)) {
                    //苏州地方征信，由于需要数据，暂时使用给定可用的值就行处理
                    creditReportQryLst.setIsSuccssInit("1");
                    creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setQryStatus("003");
                    creditReportQryLst.setReportNo(taskId);

                    if (StringUtils.startsWith(zxcx006RespDto.getErortx(), "http")) {
                        String url = zxcx006RespDto.getErortx(); //url
                        creditReportQryLst.setCreditUrl(url);
                    } else {
                        throw BizException.error(null, "9999", zxcx006RespDto.getErortx());
                    }
                } else {
                    creditReportQryLst.setQryStatus("002");
                }

                creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);

                log.info("审批通过-更新征信业务申请" + crqlSerno + "流程审批状态为【997】-通过");
                updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_997);
                if (updateCount < 0) {
                    throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null, zxcx006Code, zxcx006Meesage);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批否决后进行的业务操作
     *
     * @param crqlSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String crqlSerno) {
        // 否决改变标志 审批中 111-> 审批不通过 998

        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批否决-获取征信申请" + crqlSerno + "申请主表信息");
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            // 更新申请状态
            creditReportQryLst.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_998);

//            centralFileTaskService.taskRefuse(); TODO
        } catch (YuspException e) {
            throw e;
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批否决业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /*
     * 证件映射 信贷--->征信
     * @param xdbz 信贷证件码值
     * @return hxbz 征信证件码值
     */
    public static String toConverCurrency(String xdbz) {
        String hxbz = "";
        if ("C".equals(xdbz)) { //户口簿
            hxbz = "1";
        } else if ("B".equals(xdbz)) {//护照
            hxbz = "2";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            hxbz = "5";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            hxbz = "6";
        } else if ("12".equals(xdbz)) { // 外国人居留证
            hxbz = "8";
        } else if ("Y".equals(xdbz)) {//警官证
            hxbz = "9";
        } else if ("13".equals(xdbz)) {//香港身份证
            hxbz = "A";
        } else if ("14".equals(xdbz)) {//澳门身份证
            hxbz = "B";
        } else if ("15".equals(xdbz)) {//台湾身份证
            hxbz = "C";
        } else if ("16".equals(xdbz)) {//其他证件
            hxbz = "X";
        } else if ("A".equals(xdbz)) {//居民身份证及其他以公民身份证号为标识的证件
            hxbz = "10";
        } else if ("11".equals(xdbz)) {//军人身份证件
            hxbz = "20";
        } else if ("06".equals(xdbz)) {//工商注册号
            hxbz = "01";
        } else if ("01".equals(xdbz)) {//机关和事业单位登记号
            hxbz = "02";
        } else if ("02".equals(xdbz)) {//社会团体登记号
            hxbz = "03";
        } else if ("03".equals(xdbz)) {//民办非企业登记号
            hxbz = "04";
        } else if ("04".equals(xdbz)) {//基金会登记号
            hxbz = "05";
        } else if ("05".equals(xdbz)) {//宗教证书登记号
            hxbz = "06";
        } else if ("P2".equals(xdbz)) {//中征码
            hxbz = "10";
        } else if ("R".equals(xdbz)) {//统一社会信用代码
            hxbz = "20";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            hxbz = "30";
        } else if ("07".equals(xdbz)) {//纳税人识别号（国税）
            hxbz = "41";
        } else if ("08".equals(xdbz)) {//纳税人识别号（地税）
            hxbz = "42";
        } else {
            hxbz = xdbz;//未匹配到的证件类型
        }
        return hxbz;
    }

    /**
     * @方法名称: asyncExportCreditList
     * @方法描述: 异步导出人行征信台账信息明细
     * @参数与返回说明: 导出进度信息
     * @算法描述: 无
     */

    public ProgressDto asyncExportCreditList(QueryModel model) {
        model.getCondition().put("qryStatus", "003");
        DataAcquisition dataAcquisition = (size, page, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(size);
            queryModeTemp.setSize(page);
            String apiUrl = "/api/creditreportqrylst/asyncExportCreditList";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if(StringUtils.nonBlank(dataAuth)){
                queryModeTemp.setDataAuth(dataAuth);
            }
            return getCreditInfoVo(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(CreditReportQryLstVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: asyncExportCreditListDZ
     * @方法描述: 异步导出苏州地方征信台账信息明细
     * @参数与返回说明: 导出进度信息
     * @算法描述: 无
     */

    public ProgressDto asyncExportCreditListDZ(QueryModel model) {
        model.getCondition().put("qryStatus", "003");
        model.getCondition().put("qryCls", "3");
        DataAcquisition dataAcquisition = (size, page, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(size);
            queryModeTemp.setSize(page);
            String apiUrl = "/api/creditreportqrylst/asyncExportCreditListDZ";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if(StringUtils.nonBlank(dataAuth)){
                queryModeTemp.setDataAuth(dataAuth);
            }
            return getCreditInfoDZVo(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(CreditReportQryLstDZVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: initGrpcCreditByScene
     * @方法描述: 初始化集团客户征信查询对象
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public int initGrpcCreditByScene(String serno, String period, String bizScene) {
        // 如果首次查询到的数据为空，则调用初始数据方法，初始化方法只执行一次
        QueryModel isExists = new QueryModel();
        isExists.addCondition("bizSerno", serno);
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(isExists);

        List<LmtGrpMemRel> listMemRel = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        List<CreditReportQryLst> credList = new ArrayList<>();
        List<CreditQryBizReal> relList = new ArrayList<>();
        if (list.size() <= 0) {
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
            if (lmtGrpApp == null) {
                return 0;
            }
            ResultDto<CusGrpDto> grpResultDto = iCusClientService.selectCusGrpDtoByGrpNo(lmtGrpApp.getGrpCusId());
            if (grpResultDto == null || grpResultDto.getData() == null) {
                return 0;
            }
            CusGrpDto cusGrpDto = grpResultDto.getData();
            CusBaseDto cusCoreBaseDto = cmisCusClientService.cusBaseInfo(cusGrpDto.getCoreCusId()).getData();
            String qryCoreCls = "1".equals(cusCoreBaseDto.getCusCatalog()) ? "0" : "1";

            for (LmtGrpMemRel grpMemRel : listMemRel) {
                if (ObjectUtils.isEmpty(grpMemRel)) {
                    continue;
                }
                if (StringUtils.isEmpty(grpMemRel.getCusName()) || StringUtils.isEmpty(grpMemRel.getCusId()) || StringUtils.isEmpty(grpMemRel.getGrpCusId())) {
                    continue;
                }
                CreditReportAndRelDto recordBorrower = new CreditReportAndRelDto();
                recordBorrower.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                recordBorrower.setIsSuccssInit("0");
                recordBorrower.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                recordBorrower.setQryStatus("001");
                String crqlSernoDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                String authbookNoSeqDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                recordBorrower.setCrqlSerno(crqlSernoDo);
                recordBorrower.setAuthbookNo(authbookNoSeqDo);
                recordBorrower.setManagerId(grpMemRel.getManagerId());
                recordBorrower.setManagerBrId(grpMemRel.getManagerBrId());
                recordBorrower.setBizSerno(serno);
                recordBorrower.setBorrowerCusId(grpMemRel.getGrpCusId());
                recordBorrower.setBorrowerCusName(grpMemRel.getGrpCusName());
                CusBaseDto cusBaseDtoDo = cmisCusClientService.cusBaseInfo(grpMemRel.getCusId()).getData();
                recordBorrower.setBorrowerCertCode(cusBaseDtoDo.getCertCode());
                //与主借款人是主借款人关系
                recordBorrower.setBorrowRel("001");
                recordBorrower.setCusId(grpMemRel.getCusId());
                recordBorrower.setCusName(grpMemRel.getCusName());
                recordBorrower.setCertType(cusBaseDtoDo.getCertType());
                recordBorrower.setCertCode(cusBaseDtoDo.getCertCode());
                recordBorrower.setCqbrSerno(StringUtils.uuid(true));
                recordBorrower.setScene(period);

                //判断主借款人类型
                if ("1".equals(cusBaseDtoDo.getCusCatalog())) {
                    recordBorrower.setQryCls("0");
                } else {
                    recordBorrower.setQryCls("1");
                }
                recordBorrower.setQryResn(getQryResnByPerIodAndReal(recordBorrower.getBorrowRel(), period, recordBorrower.getQryCls()));
                CreditReportQryLst creditReportQryLstDo = new CreditReportQryLst();
                CreditQryBizReal creditQryBizRealDo = new CreditQryBizReal();
                BeanUtils.copyProperties(recordBorrower, creditReportQryLstDo);
                BeanUtils.copyProperties(recordBorrower, creditQryBizRealDo);
                boolean result = selectByPeriod(period, creditReportQryLstDo);
                if (result) {
                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLstDo);
                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizRealDo);
                } else {
                    credList.add(creditReportQryLstDo);
                }
                relList.add(creditQryBizRealDo);
            }

            //通过流水号，获取主担保人编号
            /*
             * 1.通过流水号，获取所有分项编号
             * 2.通过分项编号，查询业务与押品关系获取押品统一编号
             * 3.通过押品统一编号，查询保证人获取保证人信息
             * */
            ResultDto<List<LmtGrpAppSubDto>> resultGrpAppSubDto = lmtAppSubPrdService.queryLmtGrpAppSubSumByGrpSerno(serno);
            if (resultGrpAppSubDto != null) {
                List<LmtGrpAppSubDto> lmtGrpAppSubDtoList = resultGrpAppSubDto.getData();
                if (CollectionUtils.nonEmpty(lmtGrpAppSubDtoList)) {
                    for (LmtGrpAppSubDto lmtGrpAppSubDto : lmtGrpAppSubDtoList) {
                        if (ObjectUtils.isEmpty(lmtGrpAppSubDto)) {
                            continue;
                        }
                        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.queryGuarGuaranteeBySerno(lmtGrpAppSubDto.getSubPrdSerno());
                        for (GuarGuarantee guarGuarantee : guarGuaranteeList) {
                            if (ObjectUtils.isEmpty(guarGuarantee)) {
                                continue;
                            }
                            if (StringUtils.isEmpty(guarGuarantee.getAssureName()) || StringUtils.isEmpty(guarGuarantee.getCerType()) || StringUtils.isEmpty(guarGuarantee.getAssureCertCode())) {
                                continue;
                            }
                            //CusBaseDto cusBaseDtoPrd = cmisCusClientService.cusBaseInfo(guarGuarantee.getCusId()).getData();

                            CreditReportAndRelDto record = new CreditReportAndRelDto();
                            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                            record.setIsSuccssInit("0");
                            record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                            record.setQryStatus("001");
                            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                            String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                            record.setCrqlSerno(crqlSerno);
                            record.setAuthbookNo(authbookNoSeq);
                            record.setManagerId(lmtGrpApp.getManagerId());
                            record.setManagerBrId(lmtGrpApp.getManagerBrId());

                            record.setBizSerno(serno);
                            record.setBorrowerCusId(cusGrpDto.getCoreCusId());
                            record.setBorrowerCusName(cusGrpDto.getCoreCusName());
                            record.setBorrowerCertCode(cusGrpDto.getCoreCusCertNo());

                            //与主借款人是担保关系
                            record.setBorrowRel("007");
                            record.setCusId(guarGuarantee.getCusId());
                            record.setCusName(guarGuarantee.getAssureName());
                            record.setCertType(guarGuarantee.getCerType());
                            record.setCertCode(guarGuarantee.getAssureCertCode());
                            record.setCqbrSerno(StringUtils.uuid(true));
                            record.setScene(period);

                            //判断担保人类型
                            //10001 担保公司
                            //10002 个人
                            //10003 企业
                            if ("10002".equals(guarGuarantee.getCusTyp())) {
                                record.setQryCls("0");
                            } else {
                                record.setQryCls("1");
                            }
                            record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                            BeanUtils.copyProperties(record, creditReportQryLst);
                            BeanUtils.copyProperties(record, creditQryBizReal);
                            boolean resultUp = selectByPeriod(period, creditReportQryLst);
                            if (resultUp) {
                                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                            } else {
                                credList.add(creditReportQryLst);
                            }
                            relList.add(creditQryBizReal);
                        }

                        List<GuarBaseInfo> guarBaseInfoList = guarGuaranteeMapper.queryGuarBaseInfoBySerno(lmtGrpAppSubDto.getSubPrdSerno());
                        for (GuarBaseInfo guarBaseInfo : guarBaseInfoList) {
                            if (ObjectUtils.isEmpty(guarBaseInfo)) {
                                continue;
                            }
                            if (StringUtils.isEmpty(guarBaseInfo.getGuarCusName()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertType()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertCode())) {
                                continue;
                            }

                            CreditReportAndRelDto record = new CreditReportAndRelDto();
                            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                            record.setIsSuccssInit("0");
                            record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                            record.setQryStatus("001");
                            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                            String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                            record.setCrqlSerno(crqlSerno);
                            record.setAuthbookNo(authbookNoSeq);
                            record.setManagerId(lmtGrpApp.getManagerId());
                            record.setManagerBrId(lmtGrpApp.getManagerBrId());

                            record.setBizSerno(serno);
                            record.setBorrowerCusId(cusGrpDto.getCoreCusId());
                            record.setBorrowerCusName(cusGrpDto.getCoreCusName());
                            record.setBorrowerCertCode(cusGrpDto.getCoreCusCertNo());

                            //与主借款人是担保关系
                            record.setBorrowRel("007");
                            record.setCusId(guarBaseInfo.getGuarCusId());
                            record.setCusName(guarBaseInfo.getGuarCusName());
                            record.setCertType(guarBaseInfo.getGuarCertType());
                            record.setCertCode(guarBaseInfo.getGuarCertCode());
                            record.setCqbrSerno(StringUtils.uuid(true));
                            record.setScene(period);
                            //判断主借款人类型
                            if ("1".equals(qryCoreCls)) {
                                record.setQryCls("0");
                            } else {
                                record.setQryCls("1");
                            }
                            record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                            BeanUtils.copyProperties(record, creditReportQryLst);
                            BeanUtils.copyProperties(record, creditQryBizReal);
                            boolean resultUp = selectByPeriod(period, creditReportQryLst);
                            if (resultUp) {
                                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                            } else {
                                credList.add(creditReportQryLst);
                            }
                            relList.add(creditQryBizReal);
                        }
                    }
                }
            }


            // 高管或配偶信息,使用核心客户ID来进行查询
            if ("1".equals(qryCoreCls)) {
                QueryModel model = new QueryModel();
                model.getCondition().put("mrgTypes", "200400,201200,01,02");
                model.getCondition().put("cusIdRel", cusGrpDto.getCoreCusId());
                model.getCondition().put("oprType", "01");
                ResultDto<List<CusCorpMgrDto>> resultDto = cmisCusClientService.queryCusCorpMgr(model);
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusCorpMgrDto> mgrDtoList = resultDto.getData();
                        if (mgrDtoList.size() > 0) {
                            for (int i = 0; i < mgrDtoList.size(); i++) {
                                String s = JSON.toJSONString(mgrDtoList.get(i));
                                CusCorpMgrDto cusCorpMgrDto = JSON.parseObject(s, CusCorpMgrDto.class);

                                if (ObjectUtils.isEmpty(cusCorpMgrDto)) {
                                    continue;
                                }
                                if (StringUtils.isEmpty(cusCorpMgrDto.getMrgName()) || StringUtils.isEmpty(cusCorpMgrDto.getMrgType()) || StringUtils.isEmpty(cusCorpMgrDto.getMrgCertCode())) {
                                    continue;
                                }

                                CreditReportAndRelDto record = new CreditReportAndRelDto();
                                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                                record.setIsSuccssInit("0");
                                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                                record.setQryStatus("001");
                                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                                record.setCrqlSerno(crqlSerno);
                                record.setAuthbookNo(authbookNoSeq);
                                record.setManagerId(lmtGrpApp.getManagerId());
                                record.setManagerBrId(lmtGrpApp.getManagerBrId());

                                record.setBizSerno(serno);
                                record.setBorrowerCusId(cusGrpDto.getCoreCusId());
                                record.setBorrowerCusName(cusGrpDto.getCoreCusName());
                                record.setBorrowerCertCode(cusGrpDto.getCoreCusCertNo());

                                CusBaseDto cusBaseCorpDo = cmisCusClientService.cusBaseInfo(cusCorpMgrDto.getCusId()).getData();
                                String qryCls = null;
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCusCatalog())) {
                                    log.info("高管信息，客户编号有误");
                                    qryCls = "0";
                                } else {
                                    qryCls = "1".equals(cusBaseCorpDo.getCusCatalog()) ? "0" : "1";
                                }
                                record.setQryCls(qryCls);
                                if ("0".equals(qryCls)) {
                                    record.setBorrowRel("008");
                                } else {
                                    record.setBorrowRel("003");
                                }
                                record.setCusId(cusCorpMgrDto.getCusId());
                                record.setCusName(cusCorpMgrDto.getMrgName());
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCertType())) {
                                    record.setCertType("A");
                                } else {
                                    record.setCertType(cusBaseCorpDo.getCertType());
                                }
                                record.setCertCode(cusCorpMgrDto.getMrgCertCode());
                                record.setCqbrSerno(StringUtils.uuid(true));
                                record.setScene(period);
                                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                                BeanUtils.copyProperties(record, creditReportQryLst);
                                BeanUtils.copyProperties(record, creditQryBizReal);
                                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                                if (resultUp) {
                                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                                } else {
                                    credList.add(creditReportQryLst);
                                }
                                relList.add(creditQryBizReal);
                            }
                        }
                    }
                }
            } else if ("0".equals(qryCoreCls)) {
                QueryModel model = new QueryModel();
                model.getCondition().put("indivCusRel", "102000");
                model.getCondition().put("cusIdRel", cusGrpDto.getCoreCusId());
                // model.getCondition().put("oprType", "01");
                ResultDto<List<CusIndivSocialResp>> resultDto = cmisCusClientService.queryCusSocial(model);
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusIndivSocialResp> indivSocList = resultDto.getData();
                        if (indivSocList.size() > 0) {
                            for (int i = 0; i < indivSocList.size(); i++) {
                                String s = JSON.toJSONString(indivSocList.get(i));
                                CusIndivSocialResp cusIndivSocialResp = JSON.parseObject(s, CusIndivSocialResp.class);

                                if (ObjectUtils.isEmpty(cusIndivSocialResp)) {
                                    continue;
                                }
                                if (StringUtils.isEmpty(cusIndivSocialResp.getName()) || StringUtils.isEmpty(cusIndivSocialResp.getCertType()) || StringUtils.isEmpty(cusIndivSocialResp.getCertCode())) {
                                    continue;
                                }

                                CreditReportAndRelDto record = new CreditReportAndRelDto();
                                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                                record.setIsSuccssInit("0");
                                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                                record.setQryStatus("001");
                                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                                record.setCrqlSerno(crqlSerno);
                                record.setAuthbookNo(authbookNoSeq);
                                record.setManagerId(lmtGrpApp.getManagerId());
                                record.setManagerBrId(lmtGrpApp.getManagerBrId());

                                record.setBizSerno(serno);
                                record.setBorrowerCusId(cusGrpDto.getCoreCusId());
                                record.setBorrowerCusName(cusGrpDto.getCoreCusName());
                                record.setBorrowerCertCode(cusGrpDto.getCoreCusCertNo());

                                CusBaseDto cusBaseCorpDo = cmisCusClientService.cusBaseInfo(cusIndivSocialResp.getCusId()).getData();
                                String qryCls = null;
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCusCatalog())) {
                                    log.info("配偶信息，客户编号有误");
                                    qryCls = "0";
                                } else {
                                    qryCls = "1".equals(cusBaseCorpDo.getCusCatalog()) ? "0" : "1";
                                }
                                record.setQryCls(qryCls);
                                if ("0".equals(qryCls)) {
                                    record.setBorrowRel("009");
                                } else {
                                    record.setBorrowRel("003");
                                }
                                record.setCusId(cusIndivSocialResp.getCusId());
                                record.setCusName(cusIndivSocialResp.getName());
                                record.setCertType(cusIndivSocialResp.getCertType());
                                record.setCertCode(cusIndivSocialResp.getCertCode());
                                record.setCqbrSerno(StringUtils.uuid(true));
                                record.setScene(period);
                                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                                BeanUtils.copyProperties(record, creditReportQryLst);
                                BeanUtils.copyProperties(record, creditQryBizReal);
                                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                                if (resultUp) {
                                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                                } else {
                                    credList.add(creditReportQryLst);
                                }
                                relList.add(creditQryBizReal);
                            }
                        }
                    }
                }
            }
        }
        if (listMemRel.size() > 0) {
            for (LmtGrpMemRel lmtGrpMemRel : listMemRel) {
                if (lmtGrpMemRel == null || StringUtils.isEmpty(lmtGrpMemRel.getCusId())) {
                    continue;
                }
                CreditReportQryLst reportQryLst = new CreditReportQryLst();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
                if (cusBaseClientDto == null || StringUtils.isEmpty(cusBaseClientDto.getCertCode())) {
                    log.info("不存在此用户的证件号");
                    continue;
                }
                reportQryLst.setCusId(lmtGrpMemRel.getCusId());
                reportQryLst.setCusName(lmtGrpMemRel.getCusName());
                reportQryLst.setCertCode(cusBaseClientDto.getCertCode());
                CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(reportQryLst);
                if (creditReportQryLst != null && !Objects.equals("27", creditReportQryLst.getQryResn())) {
                    CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                    BeanUtils.copyProperties(creditReportQryLst, creditQryBizReal);
                    creditQryBizReal.setBizSerno(serno);
                    creditQryBizReal.setScene("01");
                    if (creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode()) != null) {
                        CreditQryBizReal creditQryBizRealnew = creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode());
                        creditQryBizReal.setCqbrSerno(null);
                        creditQryBizRealService.deleteByPrimaryKey(creditQryBizRealnew.getCqbrSerno());
                        creditQryBizRealService.insertSelective(creditQryBizReal);
                        //creditQryBizRealService.updateSelective(creditQryBizReal);
                    }
                }

                if (StringUtils.isEmpty(lmtGrpMemRel.getSingleSerno())) {
                    continue;
                }

                QueryModel model = new QueryModel();
                model.getCondition().put("bizSerno", lmtGrpMemRel.getSingleSerno());
                List<CreditReportQryLstDto> listCreditReport = selectAllCreditReportQryLstByCrqlSerno(model);
                if (listCreditReport.size() > 0) {
                    for (CreditReportQryLstDto creditReportQryLstDto : listCreditReport) {
                        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                        BeanUtils.copyProperties(creditReportQryLstDto, creditQryBizReal);
                        creditQryBizReal.setBizSerno(serno);
                        creditQryBizReal.setScene("01");
                        if (creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode()) != null && selectByPrimaryKey(creditQryBizReal.getCrqlSerno()) != null && "997".equals(selectByPrimaryKey(creditQryBizReal.getCrqlSerno()).getApproveStatus())) {
                            CreditQryBizReal creditQryBizRealnew = creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode());
                            creditQryBizReal.setCqbrSerno(null);
                            creditQryBizRealService.deleteByPrimaryKey(creditQryBizRealnew.getCqbrSerno());
                            creditQryBizRealService.insertSelective(creditQryBizReal);
                            //creditQryBizRealService.updateSelective(creditQryBizReal);
                        } else {
                            if (selectByPrimaryKey(creditQryBizReal.getCrqlSerno()) != null && "997".equals(selectByPrimaryKey(creditQryBizReal.getCrqlSerno()).getApproveStatus())) {
                                creditQryBizReal.setCqbrSerno(null);
                                creditQryBizRealService.insertSelective(creditQryBizReal);
                            }
                        }
                    }
                }
            }
        }

        if (list.size() > 1) {
            return list.size();
        }

        credList = credList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                new TreeSet<>(Comparator.comparing(CreditReportQryLst::getCusId))), ArrayList::new));

        relList = relList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                new TreeSet<>(Comparator.comparing(CreditQryBizReal::getCusId))), ArrayList::new));

        if (credList.size() > 0) {
            int result = creditReportQryLstMapper.insertCreditReportQryList(credList);
            if (result > 0) {
                int resultRel = creditQryBizRealMapper.insertCreditBizRelList(relList);
                if (resultRel > 0) {
                } else {
                    throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存业务征信关系表失败\"" + EcsEnum.E_SAVE_FAIL.value);
                }
            } else {
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存征信信息表失败\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        } else if (relList.size() > 0) {
            int resultRel = creditQryBizRealMapper.insertCreditBizRelList(relList);
            if (resultRel > 0) {
            } else {
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存业务征信关系表失败\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        }

        return 0;
    }

    /**
     * @方法名称: initCreditByScene
     * @方法描述: 初始化关联业务的征信查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public int initCreditByScene(String serno, String period, String bizScene) {
        // 如果首次查询到的数据为空，则调用初始数据方法，初始化方法只执行一次
        QueryModel isExists = new QueryModel();
        isExists.addCondition("bizSerno", serno);
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(isExists);
        if (list.size() > 1) {
            return list.size();
        }

        List<CreditReportQryLst> credList = new ArrayList<>();
        List<CreditQryBizReal> relList = new ArrayList<>();
        User userInfo = SessionUtils.getUserInformation();

        //小微业务
        if ("CRE18".equals(bizScene)) {
            //通过流水号，获取主借款人编号
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(serno);
            if (lmtSurveyReportMainInfo != null) {
                CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtSurveyReportMainInfo.getCusId());
                creditReportQryLstAndRealDto.setCusId(lmtSurveyReportMainInfo.getCusId());
                creditReportQryLstAndRealDto.setCusName(lmtSurveyReportMainInfo.getCusName());
                creditReportQryLstAndRealDto.setCertCode(lmtSurveyReportMainInfo.getCertCode());
                creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
                // 2021年11月13日23:00:37 hubp 插入主借款人信息
                creditReportQryLstAndRealDto.setBorrowerCertCode(lmtSurveyReportMainInfo.getCertCode());
                creditReportQryLstAndRealDto.setBorrowerCusId(lmtSurveyReportMainInfo.getCusId());
                creditReportQryLstAndRealDto.setBorrowerCusName(lmtSurveyReportMainInfo.getCusName());
                creditReportQryLstAndRealDto.setBorrowRel("001");
                String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
                creditReportQryLstAndRealDto.setQryCls(qryCls);
                // 创建之后直接置为通过状态
                creditReportQryLstAndRealDto.setApproveStatus("000");
                creditReportQryLstAndRealDto.setIsSuccssInit("0");
                creditReportQryLstAndRealDto.setManagerId(lmtSurveyReportMainInfo.getManagerId());
                creditReportQryLstAndRealDto.setManagerBrId(lmtSurveyReportMainInfo.getManagerBrId());
                creditReportQryLstAndRealDto.setQryFlag("02");
                creditReportQryLstAndRealDto.setQryStatus("001");
                //生成关联征信数据
                creditReportQryLstAndRealDto.setBizSerno(serno);
                creditReportQryLstAndRealDto.setScene("01");
                createCreditAuto(creditReportQryLstAndRealDto);
            }
        } else if ("CRE03".equals(bizScene)) {
           initCRE03(serno);
        } else if ("CRE01".equals(bizScene)) {
            //通过流水号，获取主借款人编号
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            if (lmtApp == null) {
                return 0;
            }
            CreditReportAndRelDto recordBorrower = new CreditReportAndRelDto();
            recordBorrower.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            recordBorrower.setIsSuccssInit("0");
            recordBorrower.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
            recordBorrower.setQryStatus("001");
            String crqlSernoDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
            String authbookNoSeqDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            recordBorrower.setCrqlSerno(crqlSernoDo);
            recordBorrower.setAuthbookNo(authbookNoSeqDo);
            if (userInfo != null) {
                recordBorrower.setManagerId(userInfo.getLoginCode());
                recordBorrower.setManagerBrId(userInfo.getOrg().getCode());
            } else {
                recordBorrower.setManagerId(lmtApp.getManagerId());
                recordBorrower.setManagerBrId(lmtApp.getManagerBrId());
            }
            recordBorrower.setBizSerno(serno);
            recordBorrower.setBorrowerCusId(lmtApp.getCusId());
            recordBorrower.setBorrowerCusName(lmtApp.getCusName());
            CusBaseDto cusBaseDtoDo = cmisCusClientService.cusBaseInfo(lmtApp.getCusId()).getData();
            recordBorrower.setBorrowerCertCode(cusBaseDtoDo.getCertCode());
            //与主借款人是主借款人关系
            recordBorrower.setBorrowRel("001");
            recordBorrower.setCusId(cusBaseDtoDo.getCusId());
            recordBorrower.setCusName(cusBaseDtoDo.getCusName());
            recordBorrower.setCertType(cusBaseDtoDo.getCertType());
            recordBorrower.setCertCode(cusBaseDtoDo.getCertCode());
            recordBorrower.setCqbrSerno(StringUtils.uuid(true));
            recordBorrower.setScene(period);

            //判断主借款人类型
            if ("1".equals(cusBaseDtoDo.getCusCatalog())) {
                recordBorrower.setQryCls("0");
            } else {
                recordBorrower.setQryCls("1");
            }
            recordBorrower.setQryResn(getQryResnByPerIodAndReal(recordBorrower.getBorrowRel(), period, recordBorrower.getQryCls()));
            CreditReportQryLst creditReportQryLstDo = new CreditReportQryLst();
            CreditQryBizReal creditQryBizRealDo = new CreditQryBizReal();
            BeanUtils.copyProperties(recordBorrower, creditReportQryLstDo);
            BeanUtils.copyProperties(recordBorrower, creditQryBizRealDo);
            boolean result = selectByPeriod(period, creditReportQryLstDo);
            if (result) {
                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLstDo);
                BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizRealDo);
            } else {
                credList.add(creditReportQryLstDo);
            }
            relList.add(creditQryBizRealDo);

            //通过流水号，获取主担保人编号
            /*
             * 1.通过流水号，获取所有分项编号
             * 2.通过分项编号，查询业务与押品关系获取押品统一编号
             * 3.通过押品统一编号，查询保证人获取保证人信息
             * */
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
            if (CollectionUtils.nonEmpty(lmtAppSubList)) {
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    if (ObjectUtils.isEmpty(lmtAppSub)) {
                        continue;
                    }
                    List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.queryGuarGuaranteeBySerno(lmtAppSub.getSubSerno());
                    for (GuarGuarantee guarGuarantee : guarGuaranteeList) {
                        if (ObjectUtils.isEmpty(guarGuarantee)) {
                            continue;
                        }
                        if (StringUtils.isEmpty(guarGuarantee.getAssureName()) || StringUtils.isEmpty(guarGuarantee.getCerType()) || StringUtils.isEmpty(guarGuarantee.getAssureCertCode())) {
                            continue;
                        }
                        //CusBaseDto cusBaseDtoPrd = cmisCusClientService.cusBaseInfo(guarGuarantee.getCusId()).getData();

                        CreditReportAndRelDto record = new CreditReportAndRelDto();
                        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        record.setIsSuccssInit("0");
                        record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                        record.setQryStatus("001");
                        String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                        String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                        record.setCrqlSerno(crqlSerno);
                        record.setAuthbookNo(authbookNoSeq);
                        if (userInfo != null) {
                            record.setManagerId(userInfo.getLoginCode());
                            record.setManagerBrId(userInfo.getOrg().getCode());
                        } else {
                            record.setManagerId(lmtApp.getManagerId());
                            record.setManagerBrId(lmtApp.getManagerBrId());
                        }

                        record.setBizSerno(serno);
                        record.setBorrowerCusId(lmtApp.getCusId());
                        record.setBorrowerCusName(lmtApp.getCusName());
                        CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId()).getData();
                        record.setBorrowerCertCode(cusBaseDto.getCertCode());

                        //与主借款人是担保关系
                        record.setBorrowRel("007");
                        record.setCusId(guarGuarantee.getCusId());
                        record.setCusName(guarGuarantee.getAssureName());
                        record.setCertType(guarGuarantee.getCerType());
                        record.setCertCode(guarGuarantee.getAssureCertCode());
                        record.setCqbrSerno(StringUtils.uuid(true));
                        record.setScene(period);

                        //判断担保人类型
                        //10001 担保公司
                        //10002 个人
                        //10003 企业
                        if ("10002".equals(guarGuarantee.getCusTyp())) {
                            record.setQryCls("0");
                        } else {
                            record.setQryCls("1");
                        }
                        record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                        BeanUtils.copyProperties(record, creditReportQryLst);
                        BeanUtils.copyProperties(record, creditQryBizReal);
                        boolean resultUp = selectByPeriod(period, creditReportQryLst);
                        if (resultUp) {
                            CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                            BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                        } else {
                            credList.add(creditReportQryLst);
                        }
                        relList.add(creditQryBizReal);
                    }

                    List<GuarBaseInfo> guarBaseInfoList = guarGuaranteeMapper.queryGuarBaseInfoBySerno(lmtAppSub.getSubSerno());
                    for (GuarBaseInfo guarBaseInfo : guarBaseInfoList) {
                        if (ObjectUtils.isEmpty(guarBaseInfo)) {
                            continue;
                        }
                        if (StringUtils.isEmpty(guarBaseInfo.getGuarCusName()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertType()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertCode())) {
                            continue;
                        }

                        CreditReportAndRelDto record = new CreditReportAndRelDto();
                        record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        record.setIsSuccssInit("0");
                        record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                        record.setQryStatus("001");
                        String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                        String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                        record.setCrqlSerno(crqlSerno);
                        record.setAuthbookNo(authbookNoSeq);
                        if (userInfo != null) {
                            record.setManagerId(userInfo.getLoginCode());
                            record.setManagerBrId(userInfo.getOrg().getCode());
                        } else {
                            record.setManagerId(lmtApp.getManagerId());
                            record.setManagerBrId(lmtApp.getManagerBrId());
                        }

                        record.setBizSerno(serno);
                        record.setBorrowerCusId(lmtApp.getCusId());
                        record.setBorrowerCusName(lmtApp.getCusName());
                        CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId()).getData();
                        record.setBorrowerCertCode(cusBaseDto.getCertCode());

                        //与主借款人是担保关系
                        record.setBorrowRel("007");
                        record.setCusId(guarBaseInfo.getGuarCusId());
                        record.setCusName(guarBaseInfo.getGuarCusName());
                        record.setCertType(guarBaseInfo.getGuarCertType());
                        record.setCertCode(guarBaseInfo.getGuarCertCode());
                        record.setCqbrSerno(StringUtils.uuid(true));
                        record.setScene(period);
                        //判断主借款人类型
                        if ("1".equals(cusBaseDtoDo.getCusCatalog())) {
                            recordBorrower.setQryCls("0");
                        } else {
                            recordBorrower.setQryCls("1");
                        }
                        record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                        BeanUtils.copyProperties(record, creditReportQryLst);
                        BeanUtils.copyProperties(record, creditQryBizReal);
                        boolean resultUp = selectByPeriod(period, creditReportQryLst);
                        if (resultUp) {
                            CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                            BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                        } else {
                            credList.add(creditReportQryLst);
                        }
                        relList.add(creditQryBizReal);
                    }
                }
            }

            // 高管或配偶信息
            if ("1".equals(creditReportQryLstDo.getQryCls())) {
                QueryModel model = new QueryModel();
                model.getCondition().put("mrgTypes", "200400,201200,01,02");
                model.getCondition().put("cusIdRel", creditReportQryLstDo.getCusId());
                model.getCondition().put("oprType", "01");
                ResultDto<List<CusCorpMgrDto>> resultDto = cmisCusClientService.queryCusCorpMgr(model);
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusCorpMgrDto> mgrDtoList = resultDto.getData();
                        if (mgrDtoList.size() > 0) {
                            for (int i = 0; i < mgrDtoList.size(); i++) {
                                String s = JSON.toJSONString(mgrDtoList.get(i));
                                CusCorpMgrDto cusCorpMgrDto = JSON.parseObject(s, CusCorpMgrDto.class);

                                if (ObjectUtils.isEmpty(cusCorpMgrDto)) {
                                    continue;
                                }
                                if (StringUtils.isEmpty(cusCorpMgrDto.getMrgName()) || StringUtils.isEmpty(cusCorpMgrDto.getMrgType()) || StringUtils.isEmpty(cusCorpMgrDto.getMrgCertCode())) {
                                    continue;
                                }

                                CreditReportAndRelDto record = new CreditReportAndRelDto();
                                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                                record.setIsSuccssInit("0");
                                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                                record.setQryStatus("001");
                                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                                record.setCrqlSerno(crqlSerno);
                                record.setAuthbookNo(authbookNoSeq);
                                if (userInfo != null) {
                                    record.setManagerId(userInfo.getLoginCode());
                                    record.setManagerBrId(userInfo.getOrg().getCode());
                                } else {
                                    record.setManagerId(lmtApp.getManagerId());
                                    record.setManagerBrId(lmtApp.getManagerBrId());
                                }

                                record.setBizSerno(serno);
                                record.setBorrowerCusId(lmtApp.getCusId());
                                record.setBorrowerCusName(lmtApp.getCusName());
                                CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId()).getData();
                                record.setBorrowerCertCode(cusBaseDto.getCertCode());

                                CusBaseDto cusBaseCorpDo = cmisCusClientService.cusBaseInfo(cusCorpMgrDto.getCusId()).getData();
                                String qryCls = null;
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCusCatalog())) {
                                    log.info("高管信息，客户编号有误");
                                    qryCls = "0";
                                } else {
                                    qryCls = "1".equals(cusBaseCorpDo.getCusCatalog()) ? "0" : "1";
                                }
                                record.setQryCls(qryCls);
                                if ("0".equals(qryCls)) {
                                    record.setBorrowRel("008");
                                } else {
                                    record.setBorrowRel("003");
                                }
                                record.setCusId(cusCorpMgrDto.getCusId());
                                record.setCusName(cusCorpMgrDto.getMrgName());
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCertType())) {
                                    record.setCertType("A");
                                } else {
                                    record.setCertType(cusBaseCorpDo.getCertType());
                                }
                                record.setCertCode(cusCorpMgrDto.getMrgCertCode());
                                record.setCqbrSerno(StringUtils.uuid(true));
                                record.setScene(period);
                                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                                BeanUtils.copyProperties(record, creditReportQryLst);
                                BeanUtils.copyProperties(record, creditQryBizReal);
                                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                                if (resultUp) {
                                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                                } else {
                                    credList.add(creditReportQryLst);
                                }
                                relList.add(creditQryBizReal);
                            }
                        }
                    }
                }
            } else if ("0".equals(creditReportQryLstDo.getQryCls())) {
                QueryModel model = new QueryModel();
                model.getCondition().put("indivCusRel", "102000");
                model.getCondition().put("cusIdRel", creditReportQryLstDo.getCusId());
                // model.getCondition().put("oprType", "01");
                ResultDto<List<CusIndivSocialResp>> resultDto = cmisCusClientService.queryCusSocial(model);
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusIndivSocialResp> indivSocList = resultDto.getData();
                        if (indivSocList.size() > 0) {
                            for (int i = 0; i < indivSocList.size(); i++) {
                                String s = JSON.toJSONString(indivSocList.get(i));
                                CusIndivSocialResp cusIndivSocialResp = JSON.parseObject(s, CusIndivSocialResp.class);

                                if (ObjectUtils.isEmpty(cusIndivSocialResp)) {
                                    continue;
                                }
                                if (StringUtils.isEmpty(cusIndivSocialResp.getName()) || StringUtils.isEmpty(cusIndivSocialResp.getCertType()) || StringUtils.isEmpty(cusIndivSocialResp.getCertCode())) {
                                    continue;
                                }

                                CreditReportAndRelDto record = new CreditReportAndRelDto();
                                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                                record.setIsSuccssInit("0");
                                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                                record.setQryStatus("001");
                                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                                record.setCrqlSerno(crqlSerno);
                                record.setAuthbookNo(authbookNoSeq);
                                if (userInfo != null) {
                                    record.setManagerId(userInfo.getLoginCode());
                                    record.setManagerBrId(userInfo.getOrg().getCode());
                                } else {
                                    record.setManagerId(lmtApp.getManagerId());
                                    record.setManagerBrId(lmtApp.getManagerBrId());
                                }

                                record.setBizSerno(serno);
                                record.setBorrowerCusId(lmtApp.getCusId());
                                record.setBorrowerCusName(lmtApp.getCusName());
                                CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(lmtApp.getCusId()).getData();
                                record.setBorrowerCertCode(cusBaseDto.getCertCode());

                                CusBaseDto cusBaseCorpDo = cmisCusClientService.cusBaseInfo(cusIndivSocialResp.getCusId()).getData();
                                String qryCls = null;
                                if (cusBaseCorpDo == null || StringUtils.isEmpty(cusBaseCorpDo.getCusCatalog())) {
                                    log.info("配偶信息，客户编号有误");
                                    qryCls = "0";
                                } else {
                                    qryCls = "1".equals(cusBaseCorpDo.getCusCatalog()) ? "0" : "1";
                                }
                                record.setQryCls(qryCls);
                                if ("0".equals(qryCls)) {
                                    record.setBorrowRel("009");
                                } else {
                                    record.setBorrowRel("003");
                                }
                                record.setCusId(cusIndivSocialResp.getCusId());
                                record.setCusName(cusIndivSocialResp.getName());
                                record.setCertType(cusIndivSocialResp.getCertType());
                                record.setCertCode(cusIndivSocialResp.getCertCode());
                                record.setCqbrSerno(StringUtils.uuid(true));
                                record.setScene(period);
                                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                                BeanUtils.copyProperties(record, creditReportQryLst);
                                BeanUtils.copyProperties(record, creditQryBizReal);
                                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                                if (resultUp) {
                                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                                } else {
                                    credList.add(creditReportQryLst);
                                }
                                relList.add(creditQryBizReal);
                            }
                        }
                    }
                }
            }
        } else if ("CRE06".equals(bizScene)) { //展期业务申请
            //通过流水号，获取主借款人编号
            IqpContExt iqpContExt = iqpContExtService.selectByPrimaryKey(serno);
            if (iqpContExt == null) {
                return 0;
            }
            CreditReportAndRelDto recordBorrower = new CreditReportAndRelDto();
            recordBorrower.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            recordBorrower.setIsSuccssInit("0");
            recordBorrower.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
            recordBorrower.setQryStatus("001");
            String crqlSernoDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
            String authbookNoSeqDo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            recordBorrower.setCrqlSerno(crqlSernoDo);
            recordBorrower.setAuthbookNo(authbookNoSeqDo);
            recordBorrower.setManagerId(iqpContExt.getManagerId());
            recordBorrower.setManagerBrId(iqpContExt.getManagerBrId());
            recordBorrower.setBizSerno(serno);
            recordBorrower.setBorrowerCusId(iqpContExt.getCusId());
            recordBorrower.setBorrowerCusName(iqpContExt.getCusName());
            CusBaseDto cusBaseDtoDo = cmisCusClientService.cusBaseInfo(iqpContExt.getCusId()).getData();
            recordBorrower.setBorrowerCertCode(cusBaseDtoDo.getCertCode());
            //与主借款人是主借款人关系
            recordBorrower.setBorrowRel("001");
            recordBorrower.setCusId(cusBaseDtoDo.getCusId());
            recordBorrower.setCusName(cusBaseDtoDo.getCusName());
            recordBorrower.setCertType(cusBaseDtoDo.getCertType());
            recordBorrower.setCertCode(cusBaseDtoDo.getCertCode());
            recordBorrower.setCqbrSerno(StringUtils.uuid(true));
            recordBorrower.setScene(period);

            //判断主借款人类型
            if ("1".equals(cusBaseDtoDo.getCusCatalog())) {
                recordBorrower.setQryCls("0");
            } else {
                recordBorrower.setQryCls("1");
            }
            recordBorrower.setQryResn(getQryResnByPerIodAndReal(recordBorrower.getBorrowRel(), period, recordBorrower.getQryCls()));
            CreditReportQryLst creditReportQryLstDo = new CreditReportQryLst();
            CreditQryBizReal creditQryBizRealDo = new CreditQryBizReal();
            BeanUtils.copyProperties(recordBorrower, creditReportQryLstDo);
            BeanUtils.copyProperties(recordBorrower, creditQryBizRealDo);
            boolean result = selectByPeriod(period, creditReportQryLstDo);
            if (result) {
                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLstDo);
                BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizRealDo);
            } else {
                credList.add(creditReportQryLstDo);
            }
            relList.add(creditQryBizRealDo);

            /*
             * 1.通过展期业务流水号，获取展期业务追加的担保合同
             * 2.通过原合同编号，查询原合同编号关联的担保合同
             * 3.获取原担保合同与新增担保合同项下的押品、保证人信息
             * 3.通过押品统一编号，查询保证人获取保证人信息
             * */
            //查询展期业务原合同与该笔业务的所有保证人
            List<GuarGuarantee> guarGuaranteeListOld = queryGuarGuaranteeByContNo(iqpContExt.getContNo());//原合同
            List<GuarGuarantee> guarGuaranteeListNew = queryGuarGuaranteeByContNo(iqpContExt.getIqpSerno());//该笔业务
            guarGuaranteeListOld.addAll(guarGuaranteeListNew);
            for (GuarGuarantee guarGuarantee : guarGuaranteeListOld) {
                if (ObjectUtils.isEmpty(guarGuarantee)) {
                    continue;
                }
                if (StringUtils.isEmpty(guarGuarantee.getAssureName()) || StringUtils.isEmpty(guarGuarantee.getCerType()) || StringUtils.isEmpty(guarGuarantee.getAssureCertCode())) {
                    continue;
                }

                CreditReportAndRelDto record = new CreditReportAndRelDto();
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                record.setIsSuccssInit("0");
                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                record.setQryStatus("001");
                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                record.setCrqlSerno(crqlSerno);
                record.setAuthbookNo(authbookNoSeq);
                record.setManagerId(iqpContExt.getManagerId());
                record.setManagerBrId(iqpContExt.getManagerBrId());

                record.setBizSerno(serno);
                record.setBorrowerCusId(iqpContExt.getCusId());
                record.setBorrowerCusName(iqpContExt.getCusName());
                CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(iqpContExt.getCusId()).getData();
                record.setBorrowerCertCode(cusBaseDto.getCertCode());

                //与主借款人是担保关系
                record.setBorrowRel("007");
                record.setCusId(guarGuarantee.getCusId());
                record.setCusName(guarGuarantee.getAssureName());
                record.setCertType(guarGuarantee.getCerType());
                record.setCertCode(guarGuarantee.getAssureCertCode());
                record.setCqbrSerno(StringUtils.uuid(true));
                record.setScene(period);

                //判断担保人类型
                //10001 担保公司
                //10002 个人
                //10003 企业
                if ("10002".equals(guarGuarantee.getCusTyp())) {
                    record.setQryCls("0");
                } else {
                    record.setQryCls("1");
                }
                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                BeanUtils.copyProperties(record, creditReportQryLst);
                BeanUtils.copyProperties(record, creditQryBizReal);
                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                if (resultUp) {
                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                } else {
                    credList.add(creditReportQryLst);
                }
                relList.add(creditQryBizReal);
            }
            //查询抵质押物所有权人
            List<GuarBaseInfo> guarBaseInfoListOld = queryGuarBaseInfoByContNo(iqpContExt.getContNo());//原合同
            List<GuarBaseInfo> guarBaseInfoListNew = queryGuarBaseInfoByContNo(iqpContExt.getIqpSerno());//该笔业务
            guarBaseInfoListOld.addAll(guarBaseInfoListNew);
            for (GuarBaseInfo guarBaseInfo : guarBaseInfoListOld) {
                if (ObjectUtils.isEmpty(guarBaseInfo)) {
                    continue;
                }
                if (StringUtils.isEmpty(guarBaseInfo.getGuarCusName()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertType()) || StringUtils.isEmpty(guarBaseInfo.getGuarCertCode())) {
                    continue;
                }
                //CusBaseDto cusBaseDtoPrd = cmisCusClientService.cusBaseInfo(guarGuarantee.getCusId()).getData();

                CreditReportAndRelDto record = new CreditReportAndRelDto();
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                record.setIsSuccssInit("0");
                record.setCreateTime(DateUtils.parseDateTimeByDef(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)));
                record.setQryStatus("001");
                String crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
                String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
                record.setCrqlSerno(crqlSerno);
                record.setAuthbookNo(authbookNoSeq);
                record.setManagerId(iqpContExt.getManagerId());
                record.setManagerBrId(iqpContExt.getManagerBrId());

                record.setBizSerno(serno);
                record.setBorrowerCusId(iqpContExt.getCusId());
                record.setBorrowerCusName(iqpContExt.getCusName());
                CusBaseDto cusBaseDto = cmisCusClientService.cusBaseInfo(iqpContExt.getCusId()).getData();
                record.setBorrowerCertCode(cusBaseDto.getCertCode());

                //与主借款人是担保关系
                record.setBorrowRel("007");
                record.setCusId(guarBaseInfo.getGuarCusId());
                record.setCusName(guarBaseInfo.getGuarCusName());
                record.setCertType(guarBaseInfo.getGuarCertType());
                record.setCertCode(guarBaseInfo.getGuarCertCode());
                record.setCqbrSerno(StringUtils.uuid(true));
                record.setScene(period);

                //判断担保人类型
                /**
                 * 110 一般自然人
                 * 120 个体工商户(无字号)
                 * 130 一般农户
                 * 150 小微企业主
                 * 211 企业法人
                 * 212 企业非法人
                 * 221 事业单位
                 * 222 事业非法人
                 * 231 社会团体
                 * 232 社团非法人
                 * 241 党政机关
                 * 242 机关非法人
                 * 243 政府平台
                 * 250 村镇经济组织
                 * 260 个体工商户
                 * 270 工会法人
                 * 280 农民专业合作社
                 * 290 担保公司
                 * 299 其它
                 * 311 集团客户
                 * 315 微信客户
                 * */
                if ("221".equals(guarBaseInfo.getGuarCusType()) || "231".equals(guarBaseInfo.getGuarCusType()) || "241".equals(guarBaseInfo.getGuarCusType()) || "243".equals(guarBaseInfo.getGuarCusType()) || "250".equals(guarBaseInfo.getGuarCusType()) || "280".equals(guarBaseInfo.getGuarCusType()) || "290".equals(guarBaseInfo.getGuarCusType())) {
                    record.setQryCls("1");
                } else {
                    record.setQryCls("0");
                }
                record.setQryResn(getQryResnByPerIodAndReal(record.getBorrowRel(), period, record.getQryCls()));

                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                BeanUtils.copyProperties(record, creditReportQryLst);
                BeanUtils.copyProperties(record, creditQryBizReal);
                boolean resultUp = selectByPeriod(period, creditReportQryLst);
                if (resultUp) {
                    CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                    BeanUtils.copyProperties(creditReportQryLstUpdate, creditQryBizReal);
                } else {
                    credList.add(creditReportQryLst);
                }
                relList.add(creditQryBizReal);
            }
        }else if ("CRE10".equals(bizScene)) { //贷后检查-对公定期检查
             cmisPspClientService.createCreditCus(serno);
        }
        credList = credList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                new TreeSet<>(Comparator.comparing(CreditReportQryLst::getCusId))), ArrayList::new));

        relList = relList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() ->
                new TreeSet<>(Comparator.comparing(CreditQryBizReal::getCusId))), ArrayList::new));

        if (credList.size() > 0) {
            int result = creditReportQryLstMapper.insertCreditReportQryList(credList);
            if (result > 0) {
                int resultRel = creditQryBizRealMapper.insertCreditBizRelList(relList);
                if (resultRel > 0) {
                } else {
                    throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存业务征信关系表失败\"" + EcsEnum.E_SAVE_FAIL.value);
                }
            } else {
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存征信信息表失败\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        } else if (relList.size() > 0) {
            int resultRel = creditQryBizRealMapper.insertCreditBizRelList(relList);
            if (resultRel > 0) {
            } else {
                throw BizException.error(null, EcsEnum.E_SAVE_FAIL.key, "\"保存业务征信关系表失败\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        }
        return 0;
}

    /**
     * 集团授信征信查询初始化
     * @param serno
     */
    public void initCRE03(String serno){
        List<LmtGrpMemRel> listMemRel = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        if (listMemRel.size() > 0) {
            for (LmtGrpMemRel lmtGrpMemRel : listMemRel) {
                CreditReportQryLst reportQryLst = new CreditReportQryLst();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtGrpMemRel.getCusId());
                reportQryLst.setCusId(lmtGrpMemRel.getCusId());
                reportQryLst.setCusName(lmtGrpMemRel.getCusName());
                reportQryLst.setCertCode(cusBaseClientDto.getCertCode());
                CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(reportQryLst);
                if (creditReportQryLst != null && !Objects.equals("27", creditReportQryLst.getQryResn())) {
                    CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                    BeanUtils.copyProperties(creditReportQryLst, creditQryBizReal);
                    creditQryBizReal.setBizSerno(serno);
                    creditQryBizReal.setScene("01");
                    if (creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode()) != null) {
                        CreditQryBizReal creditQryBizRealnew = creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode());
                        creditQryBizReal.setCqbrSerno(null);
                        creditQryBizRealService.deleteByPrimaryKey(creditQryBizRealnew.getCqbrSerno());
                        creditQryBizRealService.insertSelective(creditQryBizReal);
                        //creditQryBizRealService.updateSelective(creditQryBizReal);
                    }
                }

                QueryModel model = new QueryModel();
                model.getCondition().put("bizSerno", lmtGrpMemRel.getSingleSerno());
                List<CreditReportQryLstDto> listCreditReport = selectCreditReportQryLstByCrqlSerno(model);
                if (listCreditReport.size() > 0) {
                    for (CreditReportQryLstDto creditReportQryLstDto : listCreditReport) {
                        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                        BeanUtils.copyProperties(creditReportQryLstDto, creditQryBizReal);
                        creditQryBizReal.setBizSerno(serno);
                        creditQryBizReal.setScene("01");
                        if (creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode()) != null && selectByPrimaryKey(creditQryBizReal.getCrqlSerno()) != null && "997".equals(selectByPrimaryKey(creditQryBizReal.getCrqlSerno()).getApproveStatus())) {
                            CreditQryBizReal creditQryBizRealnew = creditQryBizRealMapper.selectByBizSernoAndCertCode(creditQryBizReal.getBizSerno(), creditQryBizReal.getCertCode());
                            creditQryBizReal.setCqbrSerno(null);
                            creditQryBizRealService.deleteByPrimaryKey(creditQryBizRealnew.getCqbrSerno());
                            creditQryBizRealService.insertSelective(creditQryBizReal);
                            //creditQryBizRealService.updateSelective(creditQryBizReal);
                        } else {
                            if (selectByPrimaryKey(creditQryBizReal.getCrqlSerno()) != null && "997".equals(selectByPrimaryKey(creditQryBizReal.getCrqlSerno()).getApproveStatus())) {
                                creditQryBizReal.setCqbrSerno(null);
                                creditQryBizRealService.insertSelective(creditQryBizReal);
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * 根据借款合同号查询项下的担保合同所有的抵质押物信息
     * @param contNo
     * @return
     */
    public List<GuarBaseInfo>  queryGuarBaseInfoByContNo(String contNo){
        String result = "success";
        //1.查询借款合同名下的担保合同列表
        String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNoForCreditQuery(contNo);
        List<GuarBaseInfo>  returnList = new ArrayList<>();
        if (!StringUtils.isEmpty(guarContNos)) {
            String[] guarContNoArr = guarContNos.split(",");

            for (String guarContNo : guarContNoArr) {
                //2.遍历担保合同列表，查询担保合同关联的押品列表
                List<GuarBaseInfo>  list = guarBaseInfoService.selectByGuarContNoModel(guarContNo,null);
                if(list!=null && list.size()>0){
                    returnList.addAll(list);
                }
            }
        }
        return returnList;
    }

    /**
     * 根据借款合同号查询项下的担保合同所有的保证人信息
     * @param contNo
     * @return
     */
    public List<GuarGuarantee>  queryGuarGuaranteeByContNo(String contNo){
        String result = "success";
        //1.查询借款合同名下的担保合同列表
        String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNoForCreditQuery(contNo);
        List<GuarGuarantee>  returnList = new ArrayList<>();
        if (!StringUtils.isEmpty(guarContNos)) {
            String[] guarContNoArr = guarContNos.split(",");

            for (String guarContNo : guarContNoArr) {
                //2.遍历担保合同列表，查询担保合同关联的押品列表
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("guarContNo",guarContNo);
                List<GuarGuarantee>  list = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
                if(list!=null && list.size()>0){
                    returnList.addAll(list);
                }
            }
        }
        return returnList;
    }
    /**
     * @方法名称:
     * @方法描述: 根据业务阶段以及与主借款人关系，确定征信原因
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String getQryResnByPerIodAndReal(String borrowRel, String period, String qryCls) {
        //授信管理-01 合同签订-02 出账放款-03 贷后检查-04
        String qryResn = StringUtils.EMPTY;
        if ("011".equals(period)) {
            period = "01";
        }
        //个人
        if ("0".equals(qryCls)) {
            //主借款人(共同借款人) 授信管理 -> 贷款审批
            //主借款人(共同借款人) 合同签订 -> 贷款审批
            //主借款人(共同借款人) 出账放款 -> 贷款审批

            //主借款人(共同借款人) 贷后检查 -> 贷后管理
            //担保人 贷后检查 -> 贷后管理
            //法定代表、出资人及关联人等 贷后检查 -> 贷后管理
            //其他关系人 贷后检查 -> 贷后管理

            if (("001".equals(borrowRel) || "005".equals(borrowRel)) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "02";
            } else if (("001".equals(borrowRel) || "005".equals(borrowRel) || "007".equals(borrowRel) || "008".equals(borrowRel) || "009".equals(borrowRel)) && "04".equals(period)) {
                qryResn = "27";

                //担保人 授信管理 -> 担保资格审查
                //担保人 合同签订 -> 担保资格审查
                //担保人 出账放款 -> 担保资格审查

            } else if ("007".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "08";

                //法定代表、出资人及关联人等 授信管理 -> 法人代表、出资人及关联人等资信审查
                //法定代表、出资人及关联人等 合同签订 -> 法人代表、出资人及关联人等资信审查
                //法定代表、出资人及关联人等 出账放款 -> 法人代表、出资人及关联人等资信审查
            } else if ("008".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "22";

                //其他关系人 授信管理 -> 资信审查
                //其他关系人 合同签订 -> 资信审查
                //其他关系人 出账放款 -> 资信审查
            } else if ("009".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "25";
            }
        } else {
            //企业

            //主借款人 授信管理 -> 贷前审查/额度审批
            //主借款人 合同签订 -> 贷前审查/额度审批
            //主借款人 出账放款 -> 贷前审查/额度审批

            //主借款人 贷后检查 -> 贷后管理
            //共同借款人 贷后检查 -> 贷后管理
            //担保人 贷后检查 -> 贷后管理
            //关联人 贷后检查 -> 贷后管理
            if ("001".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "01";
            } else if (("001".equals(borrowRel) || "005".equals(borrowRel) || "007".equals(borrowRel) || "008".equals(borrowRel)) && "04".equals(period)) {
                qryResn = "27";

                //共同借款人 授信管理 -> 贷前审查
                //共同借款人 合同签订 -> 贷前审查
                //共同借款人 出账放款 -> 贷前审查
            } else if ("005".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "01";

                //担保人 授信管理 -> 担保审查
                //担保人 合同签订 -> 担保审查
                //担保人 出账放款 -> 担保审查
            } else if ("007".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "18";

                //关联人 授信管理 -> 关联查询
                //关联人 合同签订 -> 关联查询
                //关联人 出账放款 -> 关联查询
            } else if ("003".equals(borrowRel) && ("01".equals(period) || "02".equals(period) || "03".equals(period))) {
                qryResn = "05";
            }
        }
        return qryResn;
    }


    /**
     * @方法名称:
     * @方法描述: 获取对应的岗位信息
     * @参数与返回值说明:
     * @算法描述: 无
     */
    public boolean getDutyInfo() {
        log.info("开始获取用户岗位信息");
        try {
            User user = SessionUtils.getUserInformation();
            String orgName = user.getOrg().getName();
            if (StringUtils.containsAny(orgName, "支行")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @方法名称: selectCreditByCertCode
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditReportQryLst> selectCreditByCertCode(QueryModel model) {
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectByModel(model);
        return list;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/23 11:13
     * @注释 查询三十天内是否进行征询登记
     */
    public Boolean selectbyxw(String certCode) {
        Boolean b = false;
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectbyxw(certCode);
        if (list.size() > 0) {
            //获取最新的一条
            CreditReportQryLst creditReportQryLst = list.get(0);
            //判断是否三十天内
            String authbookDate = creditReportQryLst.getReportCreateTime();
            if (StringUtils.isBlank(authbookDate)) {
                return false;
            }
            String[] reportSplit = DateUtils.formatDate8To10(DateUtils.formatDate10To8(authbookDate)).split("-");
            LocalDate reportOf = LocalDate.of(Integer.valueOf(reportSplit[0]), Integer.valueOf(reportSplit[1]), Integer.valueOf(reportSplit[2]));
            //三十天之前的日期
            LocalDate localDateReport = LocalDate.now().minusDays(30);
            //比大小
            b = localDateReport.isBefore(reportOf);

        }
        return b;
    }


    public Boolean selectdanbaozhengxin(String certCode) {
        Boolean b = false;
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectdanbaozhengxin(certCode);
        if (list.size() > 0) {
            b=true;
        }
        return b;
    }

    /**
     * @方法名称: createCreditAndRel
     * @方法描述: 业务方创建征信信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void createCreditAndRel(CreditReportQryLstDto record) {
        try {
            if (StringUtils.isBlank(record.getCrqlSerno())) {
                record.setCrqlSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>()));
            }
            if (StringUtils.isBlank(record.getAuthbookNo()) && !"27".equals(record.getQryResn())) {
                record.setAuthbookNo(sequenceTemplateClient.getSequenceTemplate(SeqConstant.AUTHBOOK_NO_SEQ, new HashMap<>()));
            }
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            BeanUtils.copyProperties(record, creditReportQryLst);
            //boolean result = selectByPeriod(record.getScene(), creditReportQryLst);
            boolean result = false; //新增就是单纯的新增
            if (result) {
                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                BeanUtils.copyProperties(creditReportQryLstUpdate, record);
            } else {
                insert(creditReportQryLst);
            }
            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
            BeanUtils.copyProperties(record, creditQryBizReal);
            try {
                if (creditQryBizRealMapper.selectByBizSernoAndCreSerno(creditQryBizReal.getBizSerno(), creditQryBizReal.getCrqlSerno()) != null) {
                    creditQryBizRealService.updateSelective(creditQryBizReal);
                } else {
                    creditQryBizRealMapper.insert(creditQryBizReal);
                }
            } catch (Exception e) {
                log.info("失败日志打印:", e);
                throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @方法名称: updateCreditAndRel
     * @方法描述: 业务方更新征信信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void updateCreditAndRel(CreditReportQryLstDto record) {
        try {
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            BeanUtils.copyProperties(record, creditReportQryLst);
            update(creditReportQryLst);
            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
            BeanUtils.copyProperties(record, creditQryBizReal);
            try {
                creditQryBizRealMapper.updateByPrimaryKey(creditQryBizReal);
            } catch (Exception e) {
                log.info("失败日志打印:", e);
                throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @方法名称: deleteCreditAndRel
     * @方法描述: 业务方删除征信信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void deleteCreditAndRel(Map map) {
        try {
            String crqlSerno = map.get("crqlSerno").toString();
            String cqbrSerno = map.get("cqbrSerno").toString();
            try {
                creditReportQryLstMapper.deleteByPrimaryKey(crqlSerno);
                creditQryBizRealMapper.deleteByPrimaryKey(cqbrSerno);
            } catch (Exception e) {
                log.info("失败日志打印:", e);
                throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @方法名称: selectByPeriod
     * @方法描述: 业务方发起申请规则
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean selectByPeriod(String period, CreditReportQryLst record) {
        //授信阶段
        //合同签订阶段、出账放款阶段规则同授信阶段（引入功能，取消原同一查询原因、同一与主借款人关系5个工作日内直接带入报告功能）
        if ("01234".equals(period) || "02123".equals(period) || "03123".equals(period) && !"3".equals(record.getQryCls())) {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null && StringUtils.isBlank(record.getManagerId())) {
                record.setManagerId(userInfo.getLoginCode());
            }
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(record);
            //存在授权日期为空的历史数据
            if (creditReportQryLst != null) {
                //判断是否五个工作日内
                //贷前阶段（授信）本地报告有效期（天）
                int dqQueryTime = 0;
                String dqQueryDays = getSysParameterByName("DQ_QUERY_DAYS");
                if (StringUtils.isBlank(dqQueryDays)) {
                    throw BizException.error(null, "9999", "获取配置参数异常");
                }
                dqQueryTime = Integer.parseInt(dqQueryDays);
                String authbookDate = creditReportQryLst.getAuthbookDate();
                // int dateTimes = calc(authbookDate, DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                int dateTimes = calc(creditReportQryLst.getAuthbookDate(), stringRedisTemplate.opsForValue().get("openDay"));
                //判断是否一个月内
                String reportCreateTime = creditReportQryLst.getReportCreateTime();
                if (StringUtils.isBlank(reportCreateTime)) {
                    return false;
                }
                String[] reportSplit = DateUtils.formatDate8To10(DateUtils.formatDate10To8(reportCreateTime)).split("-");
                LocalDate reportOf = LocalDate.of(Integer.valueOf(reportSplit[0]), Integer.valueOf(reportSplit[1]), Integer.valueOf(reportSplit[2]));
                LocalDate localDateReport = LocalDate.now().minusDays(30);
                if (dateTimes < dqQueryTime && localDateReport.isBefore(reportOf)) {
                    return true;
                }
            }
        }
        //集团成员授权，不需要符合5个工作日、相同查询原因的规则
        if ("0111".equals(period) && !"3".equals(record.getQryCls())) {
            CreditReportQryLst creditReportQryLstnew = new CreditReportQryLst();
            BeanUtils.copyProperties(record, creditReportQryLstnew);
            creditReportQryLstnew.setQryResn(null);
            creditReportQryLstnew.setBorrowRel(null);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(creditReportQryLstnew);
            if (creditReportQryLst != null) {
                return true;
            }
            return false;
        }
        //合同签订阶段、出账放款阶段先保留
        if ("0211".equals(period) || "0311".equals(period) && !"3".equals(record.getQryCls())) {
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(record);
            if (creditReportQryLst != null) {
                //判断是否三个月内
                //贷中阶段（合同、放款）本地报告有效期（天）
                int dzQueryTime = 0;
                String dzQueryDays = getSysParameterByName("DZ_QUERY_DAYS");
                if (StringUtils.isBlank(dzQueryDays)) {
                    throw BizException.error(null, "9999", "获取配置参数异常");
                }
                dzQueryTime = Integer.parseInt(dzQueryDays);
                String reportCreateTime = creditReportQryLst.getReportCreateTime();
                if (StringUtils.isBlank(reportCreateTime)) {
                    return false;
                }
                String[] reportSplit = DateUtils.formatDate8To10(DateUtils.formatDate10To8(reportCreateTime)).split("-");
                LocalDate reportOf = LocalDate.of(Integer.valueOf(reportSplit[0]), Integer.valueOf(reportSplit[1]), Integer.valueOf(reportSplit[2]));
                LocalDate localDateReport = LocalDate.now().minusDays(dzQueryTime);
                if (localDateReport.isBefore(reportOf)) {
                    return true;
                }
            }
        }
        //贷后检查阶段
        if ("04123".equals(period) && "27".equals(record.getQryResn()) && !"3".equals(record.getQryCls())) {
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPeriod(record);
            if (creditReportQryLst != null) {
                //判断是否三十个工作日内
                //贷后批量重查周期（天）
                int dhQueryTime = 0;
                String dhQueryDays = getSysParameterByName("DH_QUERY_DAYS");
                if (StringUtils.isBlank(dhQueryDays)) {
                    throw BizException.error(null, "9999", "获取配置参数异常");
                }
                dhQueryTime = Integer.parseInt(dhQueryDays);
                String authbookDate = creditReportQryLst.getAuthbookDate();
                String[] split = authbookDate.split("-");
                LocalDate of = LocalDate.of(Integer.valueOf(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]));
                LocalDate localDate = LocalDate.now().minusDays(dhQueryTime);
                if (localDate.isBefore(of)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取系统参数
     *
     * @param configName
     * @return
     */
    public String getSysParameterByName(String configName) {
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(configName);
            return adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        } catch (Exception e) {
            log.error("获取系统参数{}失败{}", configName, e.getMessage());
            return "";
        }
    }

    /**
     * 法定节假日列表
     *
     * @param
     * @return
     */
    public List<String> getHolidayList() {
        List<String> holidays = new ArrayList<>();
        //元旦
        holidays.add("2021-01-01");
        holidays.add("2021-01-02");
        holidays.add("2021-01-03");
        //春节
        holidays.add("2021-02-11");
        holidays.add("2021-02-12");
        holidays.add("2021-02-13");
        holidays.add("2021-02-14");
        holidays.add("2021-02-15");
        holidays.add("2021-02-16");
        holidays.add("2021-02-17");
        //清明节
        holidays.add("2021-04-03");
        holidays.add("2021-04-04");
        holidays.add("2021-04-05");
        //劳动节
        holidays.add("2021-05-01");
        holidays.add("2021-05-02");
        holidays.add("2021-05-03");
        holidays.add("2021-05-04");
        holidays.add("2021-05-05");
        //端午节
        holidays.add("2021-06-12");
        holidays.add("2021-06-13");
        holidays.add("2021-06-14");
        //中秋节
        holidays.add("2021-09-19");
        holidays.add("2021-09-20");
        holidays.add("2021-09-21");
        //国庆节
        holidays.add("2021-10-01");
        holidays.add("2021-10-02");
        holidays.add("2021-10-03");
        holidays.add("2021-10-04");
        holidays.add("2021-10-05");
        holidays.add("2021-10-06");
        holidays.add("2021-10-07");
        return holidays;
    }

    /**
     * 需要上班的周末列表
     *
     * @param
     * @return
     */
    public List<String> getNeedWorkWeekends() {
        List<String> workWeekends = new ArrayList<>();
        workWeekends.add("2021-02-07");
        workWeekends.add("2021-02-20");
        workWeekends.add("2021-04-25");
        workWeekends.add("2021-05-08");
        workWeekends.add("2021-09-18");
        workWeekends.add("2021-09-26");
        workWeekends.add("2021-10-09");
        return workWeekends;
    }

    /**
     * 计算两个日期之间的工作日天数，包含起始日期，不包含终止日期
     *
     * @param start
     * @param stop
     * @return
     */
    public int calc(String start, String stop) {
        List<String> workWeekends = getNeedWorkWeekends();
        List<String> holidays = getHolidayList();
        String pattern = "yyyy-MM-dd";
        Date begin = DateUtils.parseDate(start, pattern);
        Date end = DateUtils.parseDate(stop, pattern);
        Calendar c = Calendar.getInstance();
        c.setTime(begin);
        int count = 0;
        String ymd = null;
        while (c.getTime().before(end)) {
            ymd = DateFormatUtils.format(c.getTime(), pattern);
            //不是法定节假日
            if (!holidays.contains(ymd)) {
                //不是休息日
                if (workWeekends.contains(ymd)) {
                    count++;
                    log.info("工作日日期：{}", ymd);
                } else {
                    //非周末
                    if (c.get(Calendar.DAY_OF_WEEK) != 1 && c.get(Calendar.DAY_OF_WEEK) != 7) {
                        count++;
                        log.info("工作日日期：{}", ymd);
                    }
                }
            }
            c.add(Calendar.DATE, 1);
        }
        return count;
    }

    /**
     * @方法名称: riskItem0068
     * @方法描述: 征信查询重复发起校验
     * @参数与返回说明:
     * @算法描述: 在人行征信查询发起提交时，对同一个客户，在同一天中，同一证件类型、同一查询原因的征信查询，只允许发起一次；
     * @创建人: yfs
     * @创建时间: 2021-7-21 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0068(String serno) {
        log.info("征信查询重复发起校验*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        CreditReportQryLst creditReportQryLst = selectByPrimaryKey(serno);
        if (Objects.isNull(creditReportQryLst)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0020);
            return riskResultDto;
        }
        // 初始化查询条件
        QueryModel model = new QueryModel();
        if(StringUtils.isEmpty(creditReportQryLst.getCertCode()) || StringUtils.isEmpty(creditReportQryLst.getCertType())
                || StringUtils.isEmpty(creditReportQryLst.getQryResn())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06801);
            return riskResultDto;
        }else {
            model.addCondition("certCode",creditReportQryLst.getCertCode());
            model.addCondition("certType",creditReportQryLst.getCertType());
            model.addCondition("qryResn",creditReportQryLst.getQryResn());
            model.addCondition("approveStatus","111");
        }
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectByModel(model);
        if(CollectionUtils.nonEmpty(list)) {
            for(CreditReportQryLst report : list) {
                if(Objects.equals(serno,report.getCrqlSerno())) {
                    continue;
                }
                // 创建时间与当前时间一致，则表示发起流程的时间
                if (!Objects.equals(CmisFlowConstants.WF_STATUS_000,report.getApproveStatus()) && Objects.equals(DateUtils.formatDate(report.getCreateTime(),"yyyy-MM-dd"),DateUtils.formatDate("yyyy-MM-dd"))) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06802);
                    return riskResultDto;
                }
            }
        }
        log.info("征信查询重复发起校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: RiskItem0092
     * @方法描述: 征信报告有效期校验
     * @参数与返回说明:
     * @算法描述: 报告生成日期与授信业务提交日期相差超过1个月时，则拦截
     * @创建人: yfs
     * @创建时间: 2021-8-17 15:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0092(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("征信报告有效期校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 初始化查询条件
        QueryModel model = new QueryModel();
        model.addCondition("bizSerno",serno);
        List<CreditReportQryLstDto> list = creditReportQryLstMapper.selectCreditReportQryLstByCrqlSerno(model);
        if(CollectionUtils.isEmpty(list)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09201);
            return riskResultDto;
        }
        boolean flag = false;
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date now = DateUtils.parseDate(openDay,"yyyy-MM-dd");
        for(CreditReportQryLstDto creditReportQryLstDto : list) {
            if(StringUtils.nonEmpty(creditReportQryLstDto.getReportCreateTime())) {
                Date reportTime = DateUtils.parseDate(creditReportQryLstDto.getReportCreateTime(),"yyyy-MM-dd");
                if(DateUtils.compare(DateUtils.addMonth(reportTime, 1), now) >= 0) {
                    flag = true;
                }
            }
        }
        if(!flag) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09202);
            return riskResultDto;
        }
        log.info("征信报告有效期校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    @Transactional(rollbackFor = Exception.class)
    public int createCreditAuto(CreditReportQryLstAndRealDto creditReportQryLstAndRealDto) {
        //添加征信信息
        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
        //征信关联表
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        String crqlSerno = null;
        int result = 0;
        if (creditReportQryLstAndRealDto != null && "001".equals(creditReportQryLstAndRealDto.getBorrowRel())) {
            creditReportQryLstAndRealDto.setBorrowerCusId(creditReportQryLstAndRealDto.getCusId());
            creditReportQryLstAndRealDto.setBorrowerCusName(creditReportQryLstAndRealDto.getCusName());
            creditReportQryLstAndRealDto.setBorrowerCertCode(creditReportQryLstAndRealDto.getCertCode());
        }
        BeanUtils.copyProperties(creditReportQryLstAndRealDto, creditReportQryLst);
        if (selectByPeriod(creditReportQryLstAndRealDto.getScene(),creditReportQryLst)) {
            // 如果存在，直接关联
            // 查出该信息
            CreditReportQryLst creditReportQryLstNew = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
            crqlSerno = creditReportQryLstNew.getCrqlSerno();
        } else {
            //生成征信流水号
            crqlSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.CRQL_SERNO, new HashMap<>());
            //生成征信详情数据
            creditReportQryLst.setCrqlSerno(crqlSerno);
            if (StringUtils.isEmpty(creditReportQryLst.getQryResn())) {
                creditReportQryLst.setQryResn(getQryResnByPerIodAndReal(creditReportQryLstAndRealDto.getBorrowRel(), creditReportQryLstAndRealDto.getScene(), creditReportQryLstAndRealDto.getQryCls()));
            }
            result = insertSelective(creditReportQryLst);
            if (result != 1) {
                log.info("业务流水号：{}，生成征信详情数据异常", creditReportQryLstAndRealDto.getBizSerno());
                return result;
            }
        }
        //生成关联征信数据
        BeanUtils.copyProperties(creditReportQryLstAndRealDto, creditQryBizReal);
        creditQryBizReal.setCrqlSerno(crqlSerno);
        result = creditQryBizRealService.insertSelective(creditQryBizReal);
        if (result != 1) {
            log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            return result;
        }
        if ("012".equals(creditReportQryLstAndRealDto.getBizType())) {
            createCorpMrg(creditReportQryLstAndRealDto);
        }
        return result;
    }

    @Transactional
    public int createAndUpdate(CreditReportQryLst record) {
        // 判断是否已存在征信记录，有则更新，无则新增
        int result = 0;
        if (creditReportQryLstMapper.selectByPrimaryKey(record.getCrqlSerno()) != null) {
            result = update(record);
        } else {
            result = insert(record);
        }
        return result;
    }

    /**
     * 在途任务判断
     * @param model
     * @return
     */
    public void checkApplyExistsStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        //model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
        model.getCondition().put("applyExistsStatus", "111");
        List<CreditReportQryLst> list = creditReportQryLstMapper.selectByModel(model);
        if (list.size() > 0) {
            throw BizException.error(null, "9999", "该用户已存在在途的征信查询申请流程！");
        }
        PageHelper.clearPage();
    }

    public void createCorpMrg(CreditReportQryLstAndRealDto record) {
        if (record != null) {
            if ("1".equals(record.getQryCls())) {
                QueryModel model = new QueryModel();
                model.getCondition().put("mrgTypes", "200400,201200,01,02");
                model.getCondition().put("cusIdRel", record.getCusId());
                model.getCondition().put("oprType", "01");
                ResultDto<List<CusCorpMgrDto>> resultDto = cmisCusClientService.queryCusCorpMgr(model);
                Set<String> set = new HashSet<>();
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusCorpMgrDto> mgrDtoList = resultDto.getData();
                        if (mgrDtoList.size() > 0) {
                            for (int i = 0; i < mgrDtoList.size(); i++) {
                                String s = JSON.toJSONString(mgrDtoList.get(i));
                                CusCorpMgrDto cusCorpMgrDto = JSON.parseObject(s, CusCorpMgrDto.class);
                                set.add(cusCorpMgrDto.getCusId());
                            }

                            if (set.size() > 0) {
                                for (String cusId : set) {
                                    CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
                                    CusBaseClientDto cusBaseMrgClientDto = iCusClientService.queryCus(cusId);
                                    creditReportQryLstAndRealDto.setCusId(cusId);
                                    creditReportQryLstAndRealDto.setCusName(cusBaseMrgClientDto.getCusName());
                                    creditReportQryLstAndRealDto.setCertCode(cusBaseMrgClientDto.getCertCode());
                                    creditReportQryLstAndRealDto.setCertType(cusBaseMrgClientDto.getCertType());
                                    creditReportQryLstAndRealDto.setBorrowerCusId(record.getCusId());
                                    creditReportQryLstAndRealDto.setBorrowerCusName(record.getCusName());
                                    creditReportQryLstAndRealDto.setBorrowerCertCode(record.getCertCode());
                                    String qryCls = "1".equals(cusBaseMrgClientDto.getCusCatalog()) ? "0" : "1";
                                    if ("0".equals(qryCls)) {
                                        creditReportQryLstAndRealDto.setBorrowRel("008");
                                    } else {
                                        creditReportQryLstAndRealDto.setBorrowRel("003");
                                    }
                                    creditReportQryLstAndRealDto.setQryCls(qryCls);
                                    creditReportQryLstAndRealDto.setApproveStatus("000");
                                    creditReportQryLstAndRealDto.setIsSuccssInit("0");
                                    creditReportQryLstAndRealDto.setQryFlag("02");
                                    creditReportQryLstAndRealDto.setQryStatus("001");
                                    //生成关联征信数据
                                    creditReportQryLstAndRealDto.setBizSerno(record.getBizSerno());
                                    creditReportQryLstAndRealDto.setScene(record.getScene());
                                    createCreditAuto(creditReportQryLstAndRealDto);
                                }
                            }
                        }
                    }
                }
            } else if ("0".equals(record.getQryCls())) {
                QueryModel model = new QueryModel();
                model.getCondition().put("indivCusRel", "102000");
                model.getCondition().put("cusIdRel", record.getCusId());
                model.getCondition().put("oprType", "01");
                ResultDto<List<CusIndivSocialResp>> resultDto = cmisCusClientService.queryCusSocial(model);
                Set<String> set = new HashSet<>();
                if (resultDto != null) {
                    if(CollectionUtils.nonEmpty(resultDto.getData())) {
                        List<CusIndivSocialResp> indivSocList = resultDto.getData();
                        if (indivSocList.size() > 0) {
                            for (int i = 0; i < indivSocList.size(); i++) {
                                String s = JSON.toJSONString(indivSocList.get(i));
                                CusIndivSocialResp cusIndivSocialResp = JSON.parseObject(s, CusIndivSocialResp.class);
                                set.add(cusIndivSocialResp.getCusId());
                            }

                            if (set.size() > 0) {
                                for (String cusId : set) {
                                    CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
                                    CusBaseClientDto cusBaseMrgClientDto = iCusClientService.queryCus(cusId);
                                    creditReportQryLstAndRealDto.setCusId(cusId);
                                    creditReportQryLstAndRealDto.setCusName(cusBaseMrgClientDto.getCusName());
                                    creditReportQryLstAndRealDto.setCertCode(cusBaseMrgClientDto.getCertCode());
                                    creditReportQryLstAndRealDto.setCertType(cusBaseMrgClientDto.getCertType());
                                    creditReportQryLstAndRealDto.setBorrowerCusId(record.getCusId());
                                    creditReportQryLstAndRealDto.setBorrowerCusName(record.getCusName());
                                    creditReportQryLstAndRealDto.setBorrowerCertCode(record.getCertCode());
                                    String qryCls = "1".equals(cusBaseMrgClientDto.getCusCatalog()) ? "0" : "1";
                                    if ("0".equals(qryCls)) {
                                        creditReportQryLstAndRealDto.setBorrowRel("009");
                                    } else {
                                        creditReportQryLstAndRealDto.setBorrowRel("003");
                                    }
                                    creditReportQryLstAndRealDto.setQryCls(qryCls);
                                    creditReportQryLstAndRealDto.setApproveStatus("000");
                                    creditReportQryLstAndRealDto.setIsSuccssInit("0");
                                    creditReportQryLstAndRealDto.setQryFlag("02");
                                    creditReportQryLstAndRealDto.setQryStatus("001");
                                    //生成关联征信数据
                                    creditReportQryLstAndRealDto.setBizSerno(record.getBizSerno());
                                    creditReportQryLstAndRealDto.setScene(record.getScene());
                                    createCreditAuto(creditReportQryLstAndRealDto);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void createGuar(CreditReportQryLstAndRealDto record) {
        if (record != null) {
            Map map = lmtAppSubService.getSubAndPrd(record.getBizSerno());
            if (CollectionUtils.nonEmpty(map)) {
                List<Map> list = (List<Map>) map.get("subList");
                Set<String> set = new HashSet<>();
                if (list.size() > 0) {
                    for (Map mapItem : list) {
                        set.add((String) mapItem.get("subPrdSerno"));
                    }
                }
                if (set.size() > 0) {
                    for (String subPrdSerno : set) {
                        QueryModel model = new QueryModel();
                        model.getCondition().put("serno", subPrdSerno);
                        List<GuarBizRelGuaranteeDto> listGuar = (List<GuarBizRelGuaranteeDto>) guarGuaranteeService.queryGuarInfoSell(model);
                        if (listGuar.size() > 0) {
                            for (GuarBizRelGuaranteeDto guaranteeDto : listGuar) {
                                CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
                                CusBaseClientDto cusBaseMrgClientDto = iCusClientService.queryCus(guaranteeDto.getCusId());
                                creditReportQryLstAndRealDto.setCusId(guaranteeDto.getCusId());
                                creditReportQryLstAndRealDto.setCusName(guaranteeDto.getAssureName());
                                creditReportQryLstAndRealDto.setCertCode(guaranteeDto.getAssureCertCode());
                                creditReportQryLstAndRealDto.setCertType(guaranteeDto.getCerType());
                                creditReportQryLstAndRealDto.setBorrowerCusId(record.getCusId());
                                creditReportQryLstAndRealDto.setBorrowerCusName(record.getCusName());
                                creditReportQryLstAndRealDto.setBorrowerCertCode(record.getCertCode());
                                String qryCls = "1".equals(cusBaseMrgClientDto.getCusCatalog()) ? "0" : "1";
                                creditReportQryLstAndRealDto.setBorrowRel("007");
                                creditReportQryLstAndRealDto.setQryCls(qryCls);
                                creditReportQryLstAndRealDto.setApproveStatus("000");
                                creditReportQryLstAndRealDto.setIsSuccssInit("0");
                                creditReportQryLstAndRealDto.setQryFlag("02");
                                creditReportQryLstAndRealDto.setQryStatus("001");
                                //生成关联征信数据
                                creditReportQryLstAndRealDto.setBizSerno(record.getBizSerno());
                                creditReportQryLstAndRealDto.setScene(record.getScene());
                                createCreditAuto(creditReportQryLstAndRealDto);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 校验是否可查看征信报告
     *
     * @param record
     * @return
     */
    public Boolean checkOutReport(CreditReportQryLst record) {
        if (record != null) {
            //判断是否三个月内
            //贷中阶段（合同、放款）本地报告有效期（天）
            int dzQueryTime = 0;
            String dzQueryDays = getSysParameterByName("DZ_QUERY_DAYS");
            if (StringUtils.isBlank(dzQueryDays)) {
                throw BizException.error(null, "9999", "获取配置参数异常");
            }
            dzQueryTime = Integer.parseInt(dzQueryDays);
            String reportCreateTime = record.getReportCreateTime();
            if (StringUtils.isBlank(reportCreateTime)) {
                return false;
            }
            String[] reportSplit = DateUtils.formatDate8To10(DateUtils.formatDate10To8(reportCreateTime)).split("-");
            LocalDate reportOf = LocalDate.of(Integer.valueOf(reportSplit[0]), Integer.valueOf(reportSplit[1]), Integer.valueOf(reportSplit[2]));
            LocalDate localDateReport = LocalDate.now().minusDays(dzQueryTime);
            if (localDateReport.isBefore(reportOf)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param queryModel
     * @return CreditReportDto
     * @author 尚智勇
     * @date 2021/9/27 23:22
     * @version 1.0.0
     * @desc 查询有无征信报告
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CreditReportDto> checkCredReport(QueryModel queryModel) {
        List<CreditReportDto> result = new ArrayList<>();
        String qryCls = queryModel.getCondition().get("qryCls").toString(); // 查询标识，以这个确认查询企业还是个人
        CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey((String) queryModel.getCondition().get("crqlSerno"));
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date nowDay = DateUtils.parseDate(openDay,"yyyy-MM-dd");
        // 30天前
        Date oldDay = DateUtils.addDay(nowDay, -30);
        //查询对应的业务
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey((String) queryModel.getCondition().get("bizSerno"));
        // 调用通用接口
        CredzbReqDto credzbReqDto = new CredzbReqDto();
        if(StringUtils.isBlank(qryCls) || "0".equals(qryCls)){ // 个人
            credzbReqDto.setRuleCode("R009");
            credzbReqDto.setCertificateNum(creditReportQryLst.getCertCode()); // 个人查询专用
        } else if ("1".equals(qryCls)){ // 企业
            credzbReqDto.setRuleCode("R010");
        } else {
            credzbReqDto.setRuleCode("R009");
            credzbReqDto.setCertificateNum(creditReportQryLst.getCertCode()); // 个人查询专用
        }
        credzbReqDto.setReqId(lmtSurveyReportMainInfo.getSurveySerno());
        // 2021年11月15日17:08:41 hubp 问题编号：20211115-00128 修改机构号取值
        if(StringUtils.nonBlank(lmtSurveyReportMainInfo.getManagerBrId())){
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getManagerBrId());
        } else {
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getInputBrId());
        }
        credzbReqDto.setCustomName(creditReportQryLst.getCusName());
        credzbReqDto.setStartDate(DateUtils.formatDate(oldDay,"yyyy-MM-dd"));
        credzbReqDto.setEndDate(openDay);
        // 调查征信报告
        ResultDto<CredzbRespDto> credzbRespDto = dscms2Ciis2ndClientService.credzb(credzbReqDto);
        if (credzbRespDto != null && SuccessEnum.CMIS_SUCCSESS.key.equals(credzbRespDto.getCode())) {
            log.info("调用征信查询接口成功，业务流水号【{}】", lmtSurveyReportMainInfo.getSurveySerno());
            log.info("调用征信查询接口成功，返回数据【{}】", JSON.toJSONString(credzbRespDto));
            if (Objects.nonNull(credzbRespDto.getData()) && ((CommonUtils.nonNull(credzbRespDto.getData().getR009()) && StringUtils.nonBlank(credzbRespDto.getData().getR009().getREPORTID()))||
                    (CommonUtils.nonNull(credzbRespDto.getData().getR010()) && StringUtils.nonBlank(credzbRespDto.getData().getR010().getREPORTID())))) {
                CreditReportDto creditReportDto = new CreditReportDto();
                if ("1".equals(qryCls)){
                    // 企业
                    R010 r010 = credzbRespDto.getData().getR010();
                    creditReportDto.setCusName(creditReportQryLst.getCusName());
                    creditReportDto.setCertCode(creditReportQryLst.getCertCode());
                    creditReportDto.setSource(r010.getSOURCE());
                    creditReportDto.setReqId(r010.getREQID());
                    creditReportDto.setReportId(r010.getREPORTID());
                    creditReportDto.setQueryTime(r010.getQUERYTIME());
                    creditReportDto.setAuthbookDate(r010.getARCHIVECREATEDATE());
                    log.info("30天内征信报告编号【{}】", r010.getREPORTID());
                } else {
                    // 个人
                    R009 r009 = credzbRespDto.getData().getR009();
                    creditReportDto.setCusName(creditReportQryLst.getCusName());
                    creditReportDto.setCertCode(creditReportQryLst.getCertCode());
                    creditReportDto.setSource(r009.getSOURCE());
                    creditReportDto.setReqId(r009.getREQID());
                    creditReportDto.setReportId(r009.getREPORTID());
                    creditReportDto.setQueryTime(r009.getQUERYTIME());
                    creditReportDto.setAuthbookDate(r009.getARCHIVECREATEDATE());
                    log.info("30天内征信报告编号【{}】", r009.getREPORTID());
                }
                result.add(creditReportDto);
            } else {
                log.info("不存在30天内有效的征信报告");
            }
        } else {
            String errorMsg = "";
            if(credzbRespDto!=null){
                errorMsg = credzbRespDto.getMessage();
            }
            log.info("征信查询接口调用失败:【{}】", errorMsg);
        }
        return result;
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.CreditReportDto>
     * @author hubp
     * @date 2021/11/8 14:45
     * @version 1.0.0
     * @desc  引入查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CreditReportDto> checkCredReportXw(QueryModel queryModel) {
        log.info("引入查询征信信息queryModel传入参数:【{}】", JSON.toJSONString(queryModel));
        User user = SessionUtils.getUserInformation();
        String crqlSerno = queryModel.getCondition().get("crqlSerno").toString();
        List<CreditReportDto> resultList = new ArrayList<>();
        QueryModel model = new QueryModel();
        model.addCondition("certCode",queryModel.getCondition().get("certCode").toString());
        model.addCondition("managerId",user.getLoginCode());
        log.info("引入查询征信信息传入参数:【{}】", JSON.toJSONString(model));
        List<CreditReportQryLst> qyLst = creditReportQryLstMapper.importCreditReportList(model);
        log.info("引入查询征信信息返回参数:【{}】", JSON.toJSONString(qyLst));
        if(qyLst.size() > 0){
            for(CreditReportQryLst creditReportQryLst : qyLst) {
                CreditReportDto creditReportDto = new CreditReportDto();
                creditReportDto.setReportId(creditReportQryLst.getReportNo());
                creditReportDto.setCertCode(creditReportQryLst.getCertCode());
                creditReportDto.setCusName(creditReportQryLst.getCusName());
                creditReportDto.setReqId(creditReportQryLst.getCrqlSerno()); // 征信查询流水号
                creditReportDto.setQueryTime(creditReportQryLst.getSendTime());
                creditReportDto.setApproveStatus(creditReportQryLst.getApproveStatus());
                creditReportDto.setCrqlSerno(crqlSerno);
                resultList.add(creditReportDto);
            }
        }
        return resultList;
    }
    /**
     * @param  creditReportDto
     * @return int 更新条数
     * @author 尚智勇
     * @date 2021/9/27 23:22
     * @version 1.0.0
     * @desc 选取返回后更新征信信息
     * @修改历史: 修改时间:2021年11月8日15:25:53    修改人员 ： hubp   修改原因:修改引入逻辑
     */
    public int updatecreditReportQryLst(String crqlserno, CreditReportDto creditReportDto) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlserno);
        creditReportQryLst.setSendTime(openDay); // 成功发起时间
        creditReportQryLst.setAuthbookDate(creditReportDto.getAuthbookDate()); // 授权书日期
        creditReportQryLst.setReportCreateTime(openDay); // 报告生成时间
        creditReportQryLst.setReportNo(creditReportDto.getReportId()); // 征信报告编号
        creditReportQryLst.setApproveStatus("997"); // 审批状态
        creditReportQryLst.setQryStatus("003"); // 征信查询状态
        creditReportQryLst.setIsSuccssInit(CmisCommonConstants.YES_NO_1); // 是否成功发起
        creditReportQryLst.setQryFlag("02"); // 征信查询标识
        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
        AdminSmUserDto adminSmUserDto = resultDto.getData();
        CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
        callCiis2ndReqDto.setPrefixUrl(creditUrl);
        callCiis2ndReqDto.setReqId(creditReportDto.getReqId());
        callCiis2ndReqDto.setOrgName("信贷");
        callCiis2ndReqDto.setUserCode(creditReportQryLst.getManagerId());
        callCiis2ndReqDto.setUserName(resultDto.getData().getUserName());
        if ("0".equals(creditReportQryLst.getQryCls())) {
            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
        } else {
            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_ENT.key);
        }
        String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
        creditReportQryLst.setCreditUrl(url);
        log.info("更新征信信息开始:【{}】", creditReportQryLst.getCrqlSerno());
        int i = creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);
        log.info("更新征信信息结束，更新条数:【{}】", i);
        return i;
    }

    public String convertAuthCont(String authbookContent, String qryCls) {
        ArrayList<String> newAuthbookContent = new ArrayList<>();
        if (StringUtils.nonEmpty(authbookContent)) {
            String[] authbookContentArry = authbookContent.split(";");
            for (String item : authbookContentArry) {
                if (Objects.equals("1", qryCls)) {
                    if ("008".equals(item)) {
                        newAuthbookContent.add("001");
                    }
                    if ("009".equals(item)) {
                        newAuthbookContent.add("003");
                    }
                    if ("010".equals(item)) {
                        newAuthbookContent.add("005");
                    }
                    if ("011".equals(item)) {
                        newAuthbookContent.add("008");
                    }
                    if ("007".equals(item)) {
                        newAuthbookContent.add("004");
                    }
                } else {
                    if ("001".equals(item)) {
                        newAuthbookContent.add("001");
                    }
                    if ("002".equals(item)) {
                        newAuthbookContent.add("002");
                    }
                    if ("003".equals(item)) {
                        newAuthbookContent.add("003");
                    }
                    if ("004".equals(item)) {
                        newAuthbookContent.add("005");
                    }
                    if ("005".equals(item)) {
                        newAuthbookContent.add("010");
                    }
                    if ("006".equals(item)) {
                        newAuthbookContent.add("007");
                    }
                    if ("007".equals(item)) {
                        newAuthbookContent.add("004");
                    }
                }
            }
        }
        String handleAuthbookContent = StringUtils.concat(newAuthbookContent, ";");
        return handleAuthbookContent;
    }

    public String convertQueryReason(String qryResn, String qryCls) {
        if (Objects.equals("0", qryCls)) {
            if ("02".equals(qryResn)) {
                qryResn = "02";
            } else if ("08".equals(qryResn)) {
                qryResn = "08";
            } else if ("22".equals(qryResn)) {
                qryResn = "22";
            } else if ("25".equals(qryResn)) {
                qryResn = "25";
            } else if ("27".equals(qryResn)) {
                qryResn = "01";
            }
        } else {
            if ("17".equals(qryResn)) {
                qryResn = "17";
            } else if ("01".equals(qryResn)) {
                qryResn = "01";
            } else if ("27".equals(qryResn)) {
                qryResn = "03";
            } else if ("04".equals(qryResn)) {
                qryResn = "04";
            } else if ("05".equals(qryResn)) {
                qryResn = "05";
            } else if ("18".equals(qryResn)) {
                qryResn = "18";
            }
        }
        return qryResn;
    }

    /**
     * @方法名称: importCreditReportList
     * @方法描述: 引入30天内已查询的征信报告
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditReportQryLst> importCreditReportList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("updateTime desc");
        List<CreditReportQryLst> list = creditReportQryLstMapper.importCreditReportList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: importCreditAndRel
     * @方法描述: 导入征信信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public String importCreditAndRel(CreditReportQryLstDto record) {
        String crqlSerno = StringUtils.EMPTY;
        try {
            if (StringUtils.isBlank(record.getCrqlSerno())) {
                record.setCrqlSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>()));
            }
            if (StringUtils.isBlank(record.getAuthbookNo()) && !"27".equals(record.getQryResn())) {
                record.setAuthbookNo(sequenceTemplateClient.getSequenceTemplate(SeqConstant.AUTHBOOK_NO_SEQ, new HashMap<>()));
            }
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            BeanUtils.copyProperties(record, creditReportQryLst);
            boolean result = false; //新增就是单纯的新增
            /*
            *  1.引入本人查询报告，无需审批，直接引用
            *  2.引入非本人查询报告，需审批，审批节点为该报告所属的客户经理-所属机构负责人
            * */
            User userInfo = SessionUtils.getUserInformation();
            // 获取用户信息
            if (userInfo != null) {
                if (userInfo.getLoginCode().equals(record.getManagerId())) {
                    result = true;
                }
            }
            if (result) {
                CreditReportQryLst creditReportQryLstUpdate = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                BeanUtils.copyProperties(creditReportQryLstUpdate, record);
            } else {
                record.setCrqlSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>()));
                record.setApproveStatus("000");
                record.setIsSuccssInit("0");
                record.setCreditUrl(null);
                record.setReportCreateTime(null);
                record.setSendTime(null);
                BeanUtils.copyProperties(record, creditReportQryLst);
                insert(creditReportQryLst);
                crqlSerno = record.getCrqlSerno();
            }
            CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
            BeanUtils.copyProperties(record, creditQryBizReal);
            try {
                if (creditQryBizRealMapper.selectByBizSernoAndCreSerno(creditQryBizReal.getBizSerno(), creditQryBizReal.getCrqlSerno()) != null) {
                    creditQryBizRealService.updateSelective(creditQryBizReal);
                } else {
                    creditQryBizRealMapper.insert(creditQryBizReal);
                }
            } catch (Exception e) {
                log.info("失败日志打印:", e);
                throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return crqlSerno;
    }

	public void xwd001(String crqlSerno) {
        // 根据流水号查询征信查询申请信息
        CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);

        // 当征信查询成功发起后 && 征信查询原因为为贷款审批 && 与主借款人关系为主借款人时 才推送新微贷
        if (creditReportQryLst != null && "1".equals(creditReportQryLst.getIsSuccssInit())
                && "02".equals(creditReportQryLst.getQryResn())
                && "001".equals(creditReportQryLst.getBorrowRel())) {
            // 是否小微员工发起，若是则调用新微贷贷款申请接口
            ResultDto<GetIsXwUserDto> getIsXwUserDto = adminSmUserService.getIsXWUserByLoginCode(creditReportQryLst.getManagerId());
            if ("Y".equals(getIsXwUserDto.getData().getIsXWUser())) {
                log.info("调用新微贷贷款申请接口开始");
                // 获取客户经理账号信息
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
                AdminSmUserDto adminSmUserDto = resultDto.getData();

                // 获取客户的联系信息
                CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(creditReportQryLst.getCusId());
                if (cusIndivContactDto != null) {
                    Xwd001ReqDto xwd001ReqDto = new Xwd001ReqDto();
                    // 行内业务流水号
                    xwd001ReqDto.setBusinessId(creditReportQryLst.getCrqlSerno());
                    // 申请人名称
                    xwd001ReqDto.setApplName(creditReportQryLst.getCusName());
                    // 申请人证件类型
                    xwd001ReqDto.setApplCertificateType(creditReportQryLst.getCertType());
                    // 申请人证件号码
                    xwd001ReqDto.setApplCertificateNumber(creditReportQryLst.getCertCode());
                    // 联系方式
                    xwd001ReqDto.setTel(cusIndivContactDto.getMobileNo());
                    // 资金用途
                    xwd001ReqDto.setLoanPurposeCode("02");
                    // 营销人员编号
                    xwd001ReqDto.setMarketingNumber(adminSmUserDto.getLoginCode());
                    // 营销人员姓名
                    xwd001ReqDto.setMarketingName(adminSmUserDto.getUserName());

                    Map creditAuthCodeListMap = new HashMap();
                    creditAuthCodeListMap.put("docCode", crqlSerno);
                    creditAuthCodeListMap.put("docType", "2");
                    creditAuthCodeListMap.put("docName", "个人征信查询使用授权书（合规线上版）");
                    List creditAuthCodeList = new ArrayList();
                    creditAuthCodeList.add(creditAuthCodeListMap);
                    // 贷款授权和声明信息
                    xwd001ReqDto.setCreditAuthCodeList(JSONArray.toJSONString(creditAuthCodeList));
                    // 渠道标记
                    xwd001ReqDto.setChannelCode("XXDZX");
                    // 营销路径
                    xwd001ReqDto.setMarketPathCode("99");
                    log.info(xwd001ReqDto.toString());

                    // 调用新微贷贷款申请接口
                    ResultDto<Xwd001RespDto> xwd001ResultDto = dscms2XwdClientService.xwd001(xwd001ReqDto);
                    Xwd001RespDto xwd001RespDto = xwd001ResultDto.getData();

                    if (xwd001ResultDto != null && SuccessEnum.SUCCESS.key.equals(xwd001ResultDto.getCode())) {
                        String serNo = xwd001RespDto.getData();//新微贷流水号
                        log.info("贷款申请接口调用成功,新微贷业务流水号【{}】", serNo);
                    } else {
                        log.info("贷款申请接口调用失败");
                    }
                }
                else {
                    log.info("客户【{}】的联系信息不存在", creditReportQryLst.getCusId());
                }

            }
        }

    }
}
/**征信字典项
 [{"key":"0","value":"个人征信"},{"key":"1","value":"企业征信"},{"key":"3","value":"苏州地方征信"}]
 [{"key":"02","value":"贷款审批（个人）"},{"key":"08","value":"担保资格审查（个人）"},{"key":"22","value":"法人代表，出资人及关联人等资信查询（个人）"},{"key":"25","value":"资信审查（个人）"},{"key":"27","value":"贷后管理"},{"key":"17","value":"额度审批（企业）"},{"key":"01","value":"贷前审查（企业）"},{"key":"04","value":"其他原因（企业）"},{"key":"05","value":"关联查询（企业）"},{"key":"18","value":"担保审查（企业）"},{"key":"1","value":"贷前（苏州地方）"},{"key":"2","value":"贷中（苏州地方）"},{"key":"3","value":"贷后（苏州地方）"},{"key":"4","value":"关联查询（苏州地方）"}]
 [{"key":"001","value":"主借款人"},{"key":"005","value":"共同借款人"},{"key":"007","value":"担保人"},{"key":"008","value":"法定代表，出资人及关联人等"},{"key":"009","value":"其他关系人"},{"key":"003","value":"关联人"}]
 [{"key":"001","value":"审核本人贷款申请"},{"key":"002","value":"审批本人贷记卡/准贷记卡申请"},{"key":"003","value":"审核本人作为担保人"},{"key":"004","value":"对已发放的信贷业务进行贷后风险管理"},{"key":"005","value":"受理法人或其他组织的贷款申请或其作为担保人，需要查询本人作为法定代表人、出资人及关联人信用情况"},{"key":"006","value":"对公业务贷后管理需要查询本人作为法定代表人、出资人及关联人信用状况"},{"key":"007","value":"特约商户实名审查"},{"key":"008","value":"处理本人的征信异议"},{"key":"009","value":"其他支行认为需要查询本人的信用状况"},{"key":"010","value":"资信审查"},{"key":"101","value":"审核本单位信贷业务申请"},{"key":"102","value":"审批本单位信贷业务"},{"key":"103","value":"审核本单位作为担保人"},{"key":"104","value":"对授权人已发放的信贷业务进行贷后风险管理"},{"key":"105","value":"涉及本单位的关联人的信贷业务或担保业务，需要查询本单位信用状况"},{"key":"106","value":"处理本单位的征信异议"},{"key":"107","value":"其他支行认为需要查询本单位的信用状况"},{"key":"108","value":"受理企业信用等级评定"}]
 [{"key":"01","value":"人工"},{"key":"02","value":"业务发起"},{"key":"03","value":"自动化贷后征信查询"},{"key":"04","value":"信用卡发起"},{"key":"05","value":"移动OA"}]
 [{"key":"01","value":"纸质授权"},{"key":"02","value":"电子授权"},{"key":"03","value":"远程授权"}]
 [{"key":"01","value":"授信管理"},{"key":"02","value":"合同签订"},{"key":"03","value":"出账放款"},{"key":"04","value":"贷后检查"}]
 **/