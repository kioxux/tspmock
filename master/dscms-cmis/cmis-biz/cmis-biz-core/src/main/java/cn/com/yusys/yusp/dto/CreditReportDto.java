package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportDto
 * @类描述: CreditReportDto数据实体类
 * @功能描述:
 * @创建人: zsm
 * @创建时间: 2021-09-22 11:00:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CreditReportDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String cusName; // 客户姓名

    private String certCode; // 证件号

    private String approveStatus; // 审批状态

    private String reportId;// 报告编号

    private String reqId;// 请求业务号

    private String queryTime;// 查询时间

    private String source;// 报告来源

    private String crqlSerno; // 征信流水号

    private String authbookDate; // 授权书日期

    public String getAuthbookDate() {
        return authbookDate;
    }

    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getCrqlSerno() {
        return crqlSerno;
    }

    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(String queryTime) {
        this.queryTime = queryTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
