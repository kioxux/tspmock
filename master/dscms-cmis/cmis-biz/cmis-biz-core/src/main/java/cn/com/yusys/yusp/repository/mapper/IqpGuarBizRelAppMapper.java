/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpGuarBizRelApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarBizRelAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:55:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpGuarBizRelAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpGuarBizRelApp selectByPrimaryKey(@Param("pkId") String pkId);


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpGuarBizRelApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpGuarBizRelApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpGuarBizRelApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpGuarBizRelApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpGuarBizRelApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 通过业务申请主键进行逻辑删除，即修改opr_type
     * @param delMap
     * @return
     */
    int updateByParamsBySerno(Map delMap);

    /**
     * 通过入参批量更新数据
     * @param delMap
     * @return
     */
    int batchUpdateByParams(Map delMap);

    /**
     * 通过入参信息查询数据是否存在
     * @param params
     * @return
     */
    List<IqpGuarBizRelApp> selectDataByParams(Map params);

    /**
     * @方法名称: selectByGuarContNoKey
     * @方法描述: 根据担保合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpGuarBizRelApp> selectByGuarContNoKey(Map params);

    /**
     * 通过担保合同查询关系数据
     * @param params
     * @return
     */
    List<IqpGuarBizRelApp> selectAmtDataByParams(Map params);

    /**
     * 通过主键进行逻辑删除，即修改opr_type
     * @param delMap
     * @return
     */
    int updateByParams(Map delMap);

    /**
     * 通过入参信息查询数据是否存在
     * @param params
     * @return
     */
    List<IqpGuarBizRelApp> selectByParams(Map params);

    /**
     * 通过入参信息更新信息
     * @param pkId
     * @return
     */

    int updateByPkIdKey(@Param("pkId")String pkId,@Param("oprType")String oprType);

    /**
     * @方法名称: updateByContNoKey
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByContNoKey(@Param("pkId")String pkId, @Param("contNo")String contNo,@Param("correRel")String correRel);
    /**
     * @方法名称: queryContNoKey
     * @方法描述: 根据合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
	  List<IqpGuarBizRelApp> queryContNoKey(@Param("grtSerno")String contNo,@Param("oprType")String oprType);

    /**
     * 通过入参查询是否存在其他有效的非解除的关系数据
     * @param param
     * @return
     */
	  List<IqpGuarBizRelApp> selectOtherDataByParams(Map param);

    /**
     * 批量添加业务与担保合同关系数据
     * @param iqpGuarBizRelAppList
     * @return
     */
    int insertForeach(List<IqpGuarBizRelApp> iqpGuarBizRelAppList);
    
    /**
     * 根据业务申请流水号获取担保信息
     * @param guarContNo
     * @return
     */
    List<Map> selectIqpGuarBizRelApp(@Param("guarContNo")String guarContNo);
}