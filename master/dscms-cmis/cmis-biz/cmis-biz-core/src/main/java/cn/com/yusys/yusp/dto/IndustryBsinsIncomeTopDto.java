package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IndustryBsinsIncomeTop
 * @类描述: industry_bsins_income_top数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 19:31:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IndustryBsinsIncomeTopDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 行业分类 **/
	private String tradeClass;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 总资产 **/
	private java.math.BigDecimal totalRessetAmt;
	
	/** 净资产 **/
	private java.math.BigDecimal totalPureAmt;
	
	/** 营业收入 **/
	private java.math.BigDecimal bsinsIncome;
	
	/** 净利润 **/
	private java.math.BigDecimal profit;
	
	/** 资产负债率 **/
	private java.math.BigDecimal assetDebtRate;
	
	/** 营业融资总额 **/
	private java.math.BigDecimal totalOperFinance;
	
	/** 销贷比 **/
	private java.math.BigDecimal loanSoldRate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass == null ? null : tradeClass.trim();
	}
	
    /**
     * @return TradeClass
     */	
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param totalRessetAmt
	 */
	public void setTotalRessetAmt(java.math.BigDecimal totalRessetAmt) {
		this.totalRessetAmt = totalRessetAmt;
	}
	
    /**
     * @return TotalRessetAmt
     */	
	public java.math.BigDecimal getTotalRessetAmt() {
		return this.totalRessetAmt;
	}
	
	/**
	 * @param totalPureAmt
	 */
	public void setTotalPureAmt(java.math.BigDecimal totalPureAmt) {
		this.totalPureAmt = totalPureAmt;
	}
	
    /**
     * @return TotalPureAmt
     */	
	public java.math.BigDecimal getTotalPureAmt() {
		return this.totalPureAmt;
	}
	
	/**
	 * @param bsinsIncome
	 */
	public void setBsinsIncome(java.math.BigDecimal bsinsIncome) {
		this.bsinsIncome = bsinsIncome;
	}
	
    /**
     * @return BsinsIncome
     */	
	public java.math.BigDecimal getBsinsIncome() {
		return this.bsinsIncome;
	}
	
	/**
	 * @param profit
	 */
	public void setProfit(java.math.BigDecimal profit) {
		this.profit = profit;
	}
	
    /**
     * @return Profit
     */	
	public java.math.BigDecimal getProfit() {
		return this.profit;
	}
	
	/**
	 * @param assetDebtRate
	 */
	public void setAssetDebtRate(java.math.BigDecimal assetDebtRate) {
		this.assetDebtRate = assetDebtRate;
	}
	
    /**
     * @return AssetDebtRate
     */	
	public java.math.BigDecimal getAssetDebtRate() {
		return this.assetDebtRate;
	}
	
	/**
	 * @param totalOperFinance
	 */
	public void setTotalOperFinance(java.math.BigDecimal totalOperFinance) {
		this.totalOperFinance = totalOperFinance;
	}
	
    /**
     * @return TotalOperFinance
     */	
	public java.math.BigDecimal getTotalOperFinance() {
		return this.totalOperFinance;
	}
	
	/**
	 * @param loanSoldRate
	 */
	public void setLoanSoldRate(java.math.BigDecimal loanSoldRate) {
		this.loanSoldRate = loanSoldRate;
	}
	
    /**
     * @return LoanSoldRate
     */	
	public java.math.BigDecimal getLoanSoldRate() {
		return this.loanSoldRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}