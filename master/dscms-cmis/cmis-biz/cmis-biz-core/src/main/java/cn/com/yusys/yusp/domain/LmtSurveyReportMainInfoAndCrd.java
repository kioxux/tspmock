/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * @创建人 WH
 * @创建时间 2021/5/31 19:22
 * @注释 增加了一些多余的字段 批复状态
 */
public class LmtSurveyReportMainInfoAndCrd {
    private static final long serialVersionUID = 1L;

	//@Column(name = "SURVEY_SERNO")
	private String surveySerno;

	/** 第三方业务流水号 **/
	//@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 80)
	private String bizSerno;

	/** 产品名称 **/
	//@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型 **/
	//@Column(name = "PRD_TYPE", unique = false, nullable = true, length = 5)
	private String prdType;

	/** 产品编号 **/
	//@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 小微调查报告类型 **/
	//@Column(name = "SURVEY_TYPE", unique = false, nullable = true, length = 5)
	private String surveyType;

	/** 客户编号 **/
	//@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;

	/** 客户名称 **/
	//@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 证件类型 **/
	//@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;

	/** 证件号码 **/
	//@Column(name = "CERT_CODE", unique = false, nullable = true, length = 100)
	private String certCode;

	/** 申请金额 **/
	//@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;

	/** 审批状态 **/
	//@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 数据来源 **/
	//@Column(name = "DATA_SOURCE", unique = false, nullable = true, length = 20)
	private String dataSource;

	/** 数据来源 **/
	//@Column(name = "CUS_CHANNEL", unique = false, nullable = true, length = 20)
	private String cusChannel;

	/** 进件时间 **/
	//@Column(name = "INTO_TIME", unique = false, nullable = true, length = 10)
	private String intoTime;

	/** 是否自动分配 **/
	//@Column(name = "IS_AUTODIVIS", unique = false, nullable = true, length = 1)
	private String isAutodivis;

	/** 是否线下调查 **/
	//@Column(name = "IS_STOP_OFFLINE", unique = false, nullable = true, length = 1)
	private String isStopOffline;

	/** 操作类型 **/
	//@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 营销人工号 **/
	//@Column(name = "MAR_ID", unique = false, nullable = true, length = 32)
	private String marId;

	/** 主管机构 **/
	//@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 主管客户经理 **/
	//@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 登记人 **/
	//@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	//@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	//@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	//@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	//@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	//@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	//@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	//@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	//批复状态
    private String replyStatus;


	private String teamType;
	//模型结果
	private String modelRstStatus;

	public String getModelRstStatus() {
		return modelRstStatus;
	}

	public void setModelRstStatus(String modelRstStatus) {
		this.modelRstStatus = modelRstStatus;
	}

	public String getTeamType() {
		return teamType;
	}

	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}

	@Override
	public String toString() {
		return "LmtSurveyReportMainInfoAndCrd{" +
				"surveySerno='" + surveySerno + '\'' +
				", bizSerno='" + bizSerno + '\'' +
				", prdName='" + prdName + '\'' +
				", prdType='" + prdType + '\'' +
				", prdId='" + prdId + '\'' +
				", surveyType='" + surveyType + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", certType='" + certType + '\'' +
				", certCode='" + certCode + '\'' +
				", appAmt=" + appAmt +
				", approveStatus='" + approveStatus + '\'' +
				", dataSource='" + dataSource + '\'' +
				", cusChannel='" + cusChannel + '\'' +
				", intoTime='" + intoTime + '\'' +
				", isAutodivis='" + isAutodivis + '\'' +
				", isStopOffline='" + isStopOffline + '\'' +
				", oprType='" + oprType + '\'' +
				", marId='" + marId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				", managerId='" + managerId + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate='" + inputDate + '\'' +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate='" + updDate + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", replyStatus='" + replyStatus + '\'' +
				", teamType='" + teamType + '\'' +
				", modelRstStatus='" + modelRstStatus + '\'' +
				'}';
	}

	public String getSurveySerno() {
		return surveySerno;
	}

	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPrdType() {
		return prdType;
	}

	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getSurveyType() {
		return surveyType;
	}

	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public BigDecimal getAppAmt() {
		return appAmt;
	}

	public void setAppAmt(BigDecimal appAmt) {
		this.appAmt = appAmt;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getIntoTime() {
		return intoTime;
	}

	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime;
	}

	public String getIsAutodivis() {
		return isAutodivis;
	}

	public void setIsAutodivis(String isAutodivis) {
		this.isAutodivis = isAutodivis;
	}

	public String getIsStopOffline() {
		return isStopOffline;
	}

	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	public String getMarId() {
		return marId;
	}

	public void setMarId(String marId) {
		this.marId = marId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getReplyStatus() {
		return replyStatus;
	}

	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}

	public String getCusChannel() {
		return cusChannel;
	}

	public void setCusChannel(String cusChannel) {
		this.cusChannel = cusChannel;
	}
}