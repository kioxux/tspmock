package cn.com.yusys.yusp.service.server.xdtz0035;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0003.Cmisbatch0003RespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0035.req.Xdtz0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.resp.Xdtz0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0003.CmisBatch0003Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Objects;

/**
 * 接口处理类:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
 *
 * @author YX-YD
 * @version 1.0
 */
@Service
public class Xdtz0035Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0035Service.class);

    @Autowired
    private CmisBatch0003Service cmisBatch0003Service;

    /**
     * @param xdtz0035DataReqDto
     * @Description:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
     * @Author: YX-YD
     * @Date: 2021/6/4 20:26
     * @return: xdtz0035DataRespDto
     **/
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0035DataRespDto xdtz0035(Xdtz0035DataReqDto xdtz0035DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataReqDto));
        //定义返回信息
        Xdtz0035DataRespDto xdtz0035DataRespDto = new Xdtz0035DataRespDto();
        try {
            //请求字段
            String billNo = xdtz0035DataReqDto.getBillNo();
            int count = 0;
            // 必输字段校验
            if (StringUtil.isNotEmpty(billNo)) {
                // 根据借据编号前往信贷查询当房贷未结清时，近24个月内逾期记录条数
                logger.info("根据借据编号前往信贷查询当房贷未结清时，近24个月内逾期记录条数开始,查询参数为:{}", JSON.toJSONString(billNo));
                Cmisbatch0003ReqDto cmisbatch0003ReqDto = new Cmisbatch0003ReqDto();
                cmisbatch0003ReqDto.setDkjiejuh(billNo);
                Cmisbatch0003RespDto cmisbatch0003RespDto = cmisBatch0003Service.cmisbatch0003(cmisbatch0003ReqDto);
                if (Objects.nonNull(cmisbatch0003RespDto)) {
                    count = 1;
                }
                logger.info("根据借据编号前往信贷查询当房贷未结清时，近24个月内逾期记录条数开始,查询参数为:{}", JSON.toJSONString(count));
                if (count > 0) {
                    xdtz0035DataRespDto.setIsHavingOverdueRecord24(String.valueOf(count));
                } else {
                    xdtz0035DataRespDto.setIsHavingOverdueRecord24(SuccessEnum.CMIS_SUCCSESS.key);
                }
            } else {
                // 请求参数不存在
                xdtz0035DataRespDto.setIsHavingOverdueRecord24(StringUtils.EMPTY);
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataRespDto));
        return xdtz0035DataRespDto;
    }
}
