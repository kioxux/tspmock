package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.LmtGrpSubPrdAdjustRel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustRelAllListDto
 * @类描述: lmt_grp_sub_prd_adjust_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:25:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGrpSubPrdAdjustRelAllListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** domain **/
	private List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRel;
	
	/**
	 * @param lmtGrpSubPrdAdjustRel
	 */
	public void setLmtGrpSubPrdAdjustRel(List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRel) {
		this.lmtGrpSubPrdAdjustRel = lmtGrpSubPrdAdjustRel;
	}
	
    /**
     * @return lmtGrpSubPrdAdjustRel
     */	
	public List<LmtGrpSubPrdAdjustRel> getLmtGrpSubPrdAdjustRel() {
		return this.lmtGrpSubPrdAdjustRel;
	}


}