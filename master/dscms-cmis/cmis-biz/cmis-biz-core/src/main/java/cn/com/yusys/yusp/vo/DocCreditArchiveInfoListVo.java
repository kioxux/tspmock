/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocCreditArchiveInfo
 * @类描述: doc_credit_archive_info数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-15 17:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "人行征信档案归档", fileType = ExcelCsv.ExportFileType.XLS)
public class DocCreditArchiveInfoListVo {

    /**
     * 档案流水号
     **/
    @ExcelField(title = "档案流水号", viewLength = 20)
    private String dcaiSerno;

    /**
     * 关联业务流水号
     **/
    @ExcelField(title = "关联业务流水号", viewLength = 20)
    private String bizSerno;

    /**
     * 主借款人名称
     **/
    @ExcelField(title = "主借款人名称", viewLength = 20)
    private String borrowerCusName;

    /**
     * 主借款人证件号码
     **/
    @ExcelField(title = "主借款人证件号码", viewLength = 20)
    private String borrowerCertCode;

    /**
     * 征信查询对象名称
     **/
    @ExcelField(title = "征信查询对象名称", viewLength = 20)
    private String cusName;

    /**
     * 查询对象证件号码
     **/
    @ExcelField(title = "查询对象证件号码", viewLength = 20)
    private String certCode;

    /**
     * 授权书日期
     **/
    @ExcelField(title = "授权书日期", viewLength = 20)
    private String authbookDate;

    /**
     * 查询人
     **/
    @ExcelField(title = "查询人", viewLength = 20)
    private String qryUserName;

    /**
     * 查询机构
     **/
    @ExcelField(title = "查询机构", viewLength = 20)
    private String qryOrgName;

    /**
     * 生成日期
     **/
    @ExcelField(title = "生成日期", viewLength = 20)
    private String createDate;

    /**
     * 接收人
     **/
    @ExcelField(title = "接收人", viewLength = 20)
    private String receiverIdName;

    /**
     * 接收日期
     **/
    @ExcelField(title = "接收日期", viewLength = 20)
    private String receiverDate;

    /**
     * 核对人
     **/
    @ExcelField(title = "核对人", viewLength = 20)
    private String checkIdName;

    /**
     * 核对日期
     **/
    @ExcelField(title = "核对日期", viewLength = 20)
    private String checkDate;

    /**
     * 征信档案状态
     **/
    @ExcelField(title = "征信档案状态", viewLength = 20, dictCode = "STD_CREDIT_DOC_STAUTS")
    private String docStauts;

    public String getDcaiSerno() {
        return dcaiSerno;
    }

    public void setDcaiSerno(String dcaiSerno) {
        this.dcaiSerno = dcaiSerno;
    }

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getBorrowerCusName() {
        return borrowerCusName;
    }

    public void setBorrowerCusName(String borrowerCusName) {
        this.borrowerCusName = borrowerCusName;
    }

    public String getBorrowerCertCode() {
        return borrowerCertCode;
    }

    public void setBorrowerCertCode(String borrowerCertCode) {
        this.borrowerCertCode = borrowerCertCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getAuthbookDate() {
        return authbookDate;
    }

    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    public String getQryUserName() {
        return qryUserName;
    }

    public void setQryUserName(String qryUserName) {
        this.qryUserName = qryUserName;
    }

    public String getQryOrgName() {
        return qryOrgName;
    }

    public void setQryOrgName(String qryOrgName) {
        this.qryOrgName = qryOrgName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getReceiverIdName() {
        return receiverIdName;
    }

    public void setReceiverIdName(String receiverIdName) {
        this.receiverIdName = receiverIdName;
    }

    public String getReceiverDate() {
        return receiverDate;
    }

    public void setReceiverDate(String receiverDate) {
        this.receiverDate = receiverDate;
    }

    public String getCheckIdName() {
        return checkIdName;
    }

    public void setCheckIdName(String checkIdName) {
        this.checkIdName = checkIdName;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getDocStauts() {
        return docStauts;
    }

    public void setDocStauts(String docStauts) {
        this.docStauts = docStauts;
    }
}