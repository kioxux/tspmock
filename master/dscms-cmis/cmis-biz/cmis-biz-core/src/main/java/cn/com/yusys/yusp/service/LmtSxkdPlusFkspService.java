/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSxkdPlusFksp;
import cn.com.yusys.yusp.repository.mapper.LmtSxkdPlusFkspMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSxkdPlusFkspService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-16 10:36:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSxkdPlusFkspService {

    @Autowired
    private LmtSxkdPlusFkspMapper lmtSxkdPlusFkspMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSxkdPlusFksp selectByPrimaryKey(String pkId) {
        return lmtSxkdPlusFkspMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSxkdPlusFksp> selectAll(QueryModel model) {
        List<LmtSxkdPlusFksp> records = (List<LmtSxkdPlusFksp>) lmtSxkdPlusFkspMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSxkdPlusFksp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSxkdPlusFksp> list = lmtSxkdPlusFkspMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSxkdPlusFksp record) {
        return lmtSxkdPlusFkspMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSxkdPlusFksp record) {
        return lmtSxkdPlusFkspMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSxkdPlusFksp record) {
        return lmtSxkdPlusFkspMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSxkdPlusFksp record) {
        return lmtSxkdPlusFkspMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSxkdPlusFkspMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSxkdPlusFkspMapper.deleteByIds(ids);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public LmtSxkdPlusFksp selectBySerno(String serno) {
        return lmtSxkdPlusFkspMapper.selectBySerno(serno);
    }
}
