/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtIntbankAcc;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.LmtIntbankAccSub;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAccSubMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 14:22:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtIntbankAccSubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtIntbankAccSub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtIntbankAccSub> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtIntbankAccSub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtIntbankAccSub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtIntbankAccSub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtIntbankAccSub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据台账编号查询分项台账信息
     * @param accNo
     * @return
     */
    List<LmtIntbankAccSub> selectByAccNo(@Param("accNo") String accNo);

    /**
     * 根据申请流水号查询分项台账信息
     * @param serno
     * @return
     */
    List<LmtIntbankAccSub> selectBySerno(@Param("serno") String serno);

    /**
     * 根据分项额度编号查询
     * @param accSubNo
     * @return
     */
    LmtIntbankAccSub selectByAccSubNo(@Param("accSubNo") String accSubNo);

    /**
     * 逻辑删除台账原批复分项信息
     * @param accNo
     */
    void deleteLogicByAccNo(@Param("accNo") String accNo);

    /**
     * 根据批复分项流水号查询分项台账信息
     * @param replySubSerno
     * @return
     */
    LmtIntbankAccSub selectByReplySubSerno(@Param("replySubSerno") String replySubSerno);
}