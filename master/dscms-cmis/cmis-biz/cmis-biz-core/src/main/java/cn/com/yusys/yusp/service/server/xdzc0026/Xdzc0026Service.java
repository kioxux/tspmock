package cn.com.yusys.yusp.service.server.xdzc0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0026.req.Xdzc0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.resp.Xdzc0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreCoClientService;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 接口处理类:资产池主动还款
 *
 * @Author xs
 * @Date 2021/6/21 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0026Service {

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0026Service.class);

    /**
     * 交易码：xdzc0026
     * 交易描述: 资产池主动还款
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0026DataRespDto xdzc0026Service(Xdzc0026DataReqDto xdzc0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value);
        Xdzc0026DataRespDto xdzc0026DataRespDto = new Xdzc0026DataRespDto();

        String tradeCode = xdzc0026DataReqDto.getTradeCode();//交易流水号
        String billNo = xdzc0026DataReqDto.getBillNo();//借据编号
        String repayMode1 = xdzc0026DataReqDto.getRepayMode1();//一级还款模式
        String repayMode2 = xdzc0026DataReqDto.getRepayMode2();//二级还款模式
        BigDecimal repayAmt = xdzc0026DataReqDto.getRepayAmt();//还款金额
        String loanRecoverType = xdzc0026DataReqDto.getLoanRecoverType();//贷款回收方式
        String repayAccNO = xdzc0026DataReqDto.getRepayAccNO();//还款账户
        String repayAccName = xdzc0026DataReqDto.getRepayAccName();//还款账户名称
        String repayAccSubNo = xdzc0026DataReqDto.getRepayAccSubNo();//还款账户子序号
        String Magerid = xdzc0026DataReqDto.getMagerid();//主管客户经理
        // 默认出库失败
        xdzc0026DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
        xdzc0026DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
        try {

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value);
        return xdzc0026DataRespDto;
    }
}
