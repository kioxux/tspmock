package cn.com.yusys.yusp.service.server.xdsx0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import cn.com.yusys.yusp.domain.LoanEdCheck;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdsx0019.req.Xdsx0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.resp.Xdsx0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LoanEdCheckMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.ToppAcctSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.MessageSendService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0026Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-19 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0019Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0019Service.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private LoanEdCheckMapper loanEdCheckMapper;

    /**
     * 风控发送信贷审核受托信息
     *
     * @param xdsx0019DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0019DataRespDto getXdsx0019(Xdsx0019DataReqDto xdsx0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataReqDto));
        Xdsx0019DataRespDto xdsx0019DataRespDto = new Xdsx0019DataRespDto();
        try {
            LoanEdCheck loanEdCheck = new LoanEdCheck();
            //业务流水号
            String serno = xdsx0019DataReqDto.getSerno();
            if (StringUtils.isEmpty(serno)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //客户编号
            String cus_id = xdsx0019DataReqDto.getCus_id();
            if (StringUtils.isEmpty(cus_id)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //客户名称
            String cus_name = xdsx0019DataReqDto.getCus_name();
            if (StringUtils.isEmpty(cus_name)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //合同编号
            String contNo = xdsx0019DataReqDto.getCont_no();
            if (StringUtils.isEmpty(contNo)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //客户经理编号
            String managerId = xdsx0019DataReqDto.getManager_id();
            if (StringUtils.isEmpty(contNo)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //受托支付账户户名
            String trade_partner_acc = xdsx0019DataReqDto.getTrade_partner_acc();
            if (StringUtils.isEmpty(trade_partner_acc)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //受托支付账户户名
            String trade_partner_name = xdsx0019DataReqDto.getTrade_partner_name();
            if (StringUtils.isEmpty(trade_partner_name)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //受托支付开户行号
            String khh_no = xdsx0019DataReqDto.getKhh_no();
            if (StringUtils.isEmpty(khh_no)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //受托支付开户行名
            String khh_name = xdsx0019DataReqDto.getKhh_name();
            if (StringUtils.isEmpty(khh_name)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //借据期限
            String bill_date = xdsx0019DataReqDto.getBill_date();
            if (StringUtils.isEmpty(bill_date)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //审批利率
            BigDecimal chack_rate = xdsx0019DataReqDto.getChack_rate();
            if (Objects.isNull(chack_rate)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //放款/还款卡号
            String refund_card = xdsx0019DataReqDto.getRefund_card();
            if (Objects.isNull(refund_card)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //中文合同号
            String cont_cn_no = xdsx0019DataReqDto.getCont_cn_no();
            if (Objects.isNull(cont_cn_no)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            //中文合同号
            BigDecimal bill_amount = xdsx0019DataReqDto.getBill_amount();
            if (Objects.isNull(bill_amount)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            loanEdCheck.setSerno(serno);//流水号
            loanEdCheck.setUpdateTime(DateUtils.getCurrTimestamp());//更新时间
            loanEdCheck.setAcctbNm(khh_name);//开户行行名
            loanEdCheck.setAcctbNo(khh_no);//开户行行号
            loanEdCheck.setApprRate(chack_rate);//审批利率
            loanEdCheck.setBillAmt(bill_amount);//借据金额
            loanEdCheck.setBillNo(xdsx0019DataReqDto.getBill_no());//借据号
            loanEdCheck.setContCnNo(cont_cn_no);//中文合同编号
            loanEdCheck.setContNo(contNo);//合同编号
            loanEdCheck.setCreateTime(DateUtils.getCurrTimestamp());
            loanEdCheck.setCusId(cus_id);//客户号
            loanEdCheck.setCusName(cus_name);//客户名称
            loanEdCheck.setDisbRepayCard(refund_card);//放款/还款卡号
            loanEdCheck.setManagerId(managerId);//客户经理
            AdminSmUserDto byLoginCode = commonService.getByLoginCode(managerId);
            if (Objects.nonNull(byLoginCode)) {
                loanEdCheck.setManagerName(byLoginCode.getUserName());//客户经理名称
            }
            loanEdCheck.setLoanTerm(bill_date);//贷款期限
            loanEdCheck.setTradePartnerAcc(trade_partner_acc);//受托支付账户账号
            loanEdCheck.setTradePartnerName(trade_partner_name);//受托支付账户户名
            loanEdCheckMapper.insertSelective(loanEdCheck);
            xdsx0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xdsx0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
        } catch (BizException e) {
            xdsx0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdsx0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdsx0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdsx0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, e.getMessage());
            throw new Exception(EcbEnum.ECB019999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataRespDto));
        return xdsx0019DataRespDto;
    }

}
