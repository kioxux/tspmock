package cn.com.yusys.yusp.service.server.xdcz0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.req.Xdcz0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.resp.Xdcz0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AccCvrsService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0014.CmisLmt0014Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:电子保函注销
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdcz0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0002Service.class);
    @Autowired
    private AccCvrsService accCvrsService;// 保函台账

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 电子保函注销
     *
     * @param xdcz0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0002DataRespDto xdcz0002(Xdcz0002DataReqDto xdcz0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value);
        Xdcz0002DataRespDto xdcz0002DataRespDto = new Xdcz0002DataRespDto();
        try {
            String zxflag = xdcz0002DataReqDto.getZxflag();//注销标识 1.退保 2.注销 退保、注销均注销信贷保函台账
            String billNo = xdcz0002DataReqDto.getBillNO();//借据号
            if (StringUtils.isBlank(billNo)) {
                xdcz0002DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdcz0002DataRespDto.setOpMsg("借据号billNo不能为空");
                return xdcz0002DataRespDto;
            }
            String opFlag = null;// 操作成功标志位
            String opMsg = null;// 描述信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("billNo", billNo);
            logger.info("查询保函台账表开始,请求参数为{}", JSON.toJSONString(queryModel));
            List<AccCvrs> accCvrsList = accCvrsService.selectByModel(queryModel);
            logger.info("查询保函台账表结束,返回参数为{}", JSON.toJSONString(accCvrsList));
            if (CollectionUtils.nonEmpty(accCvrsList)) {
                Map<String, String> queryMap = new HashMap<>();
                queryMap.put("accStatus", "0");
                queryMap.put("billNo", billNo);
                logger.info("根据借据号更新保函台账表中台账状态开始,请求参数为{}", JSON.toJSONString(queryMap));
                int result = accCvrsService.updateAccStatusByBillNo(queryMap);
                logger.info("根据借据号更新保函台账表中台账状态结束,响应参数为{}", JSON.toJSONString(result));

                AccCvrs accCvrs = accCvrsList.get(0);
                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accCvrs.getManagerBrId()));//金融机构代码
                cmisLmt0014ReqDto.setSerno(accCvrs.getPvpSerno());//交易流水号
                cmisLmt0014ReqDto.setInputId(accCvrs.getInputId());//登记人
                cmisLmt0014ReqDto.setInputBrId(accCvrs.getInputBrId());//登记机构
                cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期

                CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
                cmisLmt0014ReqdealBizListDto.setDealBizNo(billNo);//台账编号
                cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
                logger.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", accCvrs.getPvpSerno(), JSON.toJSONString(cmisLmt0014ReqDto));
                ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
                logger.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", accCvrs.getPvpSerno(), JSON.toJSONString(cmisLmt0014RespDtoResultDto));
                if (!Objects.equals(cmisLmt0014RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)
                        || !Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                    logger.info("台账恢复，前往额度系统进行额度恢复异常");
                    throw BizException.error(null, EcbEnum.ECB019999.key, cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                }
                opFlag = DscmsBizCzEnum.OP_FLAG_S.key;
                opMsg = DscmsBizCzEnum.OP_FLAG_S.value;
            } else {
                opFlag = DscmsBizCzEnum.OP_FLAG_F.key;
                opMsg = EcbEnum.ECB010032.value + billNo;
            }
            xdcz0002DataRespDto.setOpFlag(opFlag);// 操作成功标志位
            xdcz0002DataRespDto.setOpMsg(opMsg);// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value);
        return xdcz0002DataRespDto;
    }

}
