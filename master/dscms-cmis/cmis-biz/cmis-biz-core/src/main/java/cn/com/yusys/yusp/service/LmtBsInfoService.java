package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtBsInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.dto.LmtBsInfoDto;
import cn.com.yusys.yusp.repository.mapper.LmtBsInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtBsInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-05-10 15:10:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtBsInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private LmtBsInfoMapper lmtBsInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtBsInfo selectByPrimaryKey(String pkId) {
        return lmtBsInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtBsInfo> selectAll(QueryModel model) {
        List<LmtBsInfo> records = (List<LmtBsInfo>) lmtBsInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtBsInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtBsInfo> list = lmtBsInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtBsInfo record) {
        return lmtBsInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtBsInfo record) {
        return lmtBsInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtBsInfo record) {
        return lmtBsInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtBsInfo record) {
        return lmtBsInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtBsInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtBsInfoMapper.deleteByIds(ids);
    }

    /*
     * @param [surveySerno]
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtBsInfo>
     * @author hubp
     * @date 2021/5/18 15:15
     * @version 1.0.0
     * @desc 通过调查流水号查询信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly = true)
    public List<LmtBsInfo> selectBySurveySerno(String surveySerno) {
        List<LmtBsInfo> records = lmtBsInfoMapper.selectBySurveySerno(surveySerno);
        return records;
    }

    /*
     * @param [saveData]
     * @return int
     * @author hubp
     * @date 2021/5/18 16:59
     * @version 1.0.0
     * @desc 循环更新非空字段
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateSelective(LmtBsInfoDto lmtBsInfoDto) {
        int result = -1;
        try {
            List<LmtBsInfo> list = lmtBsInfoDto.getList();
            if (list.size() <= 0) {
                return result;
            }
            list.forEach(lmtBsInfo -> {
                logger.info("开始更新信息，流水号为：.............." + lmtBsInfo.getSurveySerno());
                lmtBsInfoMapper.updateByPrimaryKeySelective(lmtBsInfo);
            });
            result = 1;
        } catch (Exception e) {
            throw e;
        } finally {
            logger.info("更新结束..............");
        }
        return result;
    }

    @Transactional(readOnly = true)
    public List<LmtBsInfo> selectByLmtBsInfoMap(Map lmtBsInfoMap) {
        List<LmtBsInfo> records = lmtBsInfoMapper.selectByLmtBsInfoMap(lmtBsInfoMap);
        return records;
    }
}
