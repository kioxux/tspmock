package cn.com.yusys.yusp.web.server.xdxw0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0038.Xdxw0038Service;
import cn.com.yusys.yusp.service.server.xdxw0045.Xdxw0045Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0045.req.Xdxw0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0045.resp.Xdxw0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优抵贷客户调查撤销
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0045:优抵贷客户调查撤销")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0045Resource.class);

    @Autowired
    private Xdxw0045Service xdxw0045Service;

    /**
     * 交易码：xdxw0045
     * 交易描述：优抵贷客户调查撤销
     *
     * @param xdxw0045DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优抵贷客户调查撤销")
    @PostMapping("/xdxw0045")
    protected @ResponseBody
    ResultDto<Xdxw0045DataRespDto> xdxw0045(@Validated @RequestBody Xdxw0045DataReqDto xdxw0045DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataReqDto));
        Xdxw0045DataRespDto xdxw0045DataRespDto = new Xdxw0045DataRespDto();// 响应Dto:优抵贷客户调查撤销
        ResultDto<Xdxw0045DataRespDto> xdxw0045DataResultDto = new ResultDto<>();
        String op_flag = xdxw0045DataReqDto.getOp_flag();//操作标识
        String survey_serno = xdxw0045DataReqDto.getSurvey_serno();//流水号
        String tax_grade_result = xdxw0045DataReqDto.getTax_grade_result();//模型评级结果
        String tax_score = xdxw0045DataReqDto.getTax_score();//模型评分
        try {
            // 从xdxw0045DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataReqDto));
            xdxw0045DataRespDto = xdxw0045Service.xdxw0045(xdxw0045DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataRespDto));
            // 封装xdxw0045DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0045DataRespDto.getOpFlag();
            String opMessage = xdxw0045DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0045DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0045DataResultDto.setMessage(opMessage);
            } else {
                xdxw0045DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0045DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0045DataResultDto.setMessage(e.getMessage());
            xdxw0045DataResultDto.setCode(e.getErrorCode());
            xdxw0045DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, e.getMessage());
            // 封装xdxw0045DataResultDto中异常返回码和返回信息
            xdxw0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0045DataResultDto.setMessage(e.getMessage());
            xdxw0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0045DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0045DataRespDto到xdxw0045DataResultDto中
        xdxw0045DataResultDto.setData(xdxw0045DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataResultDto));
        return xdxw0045DataResultDto;
    }
}
