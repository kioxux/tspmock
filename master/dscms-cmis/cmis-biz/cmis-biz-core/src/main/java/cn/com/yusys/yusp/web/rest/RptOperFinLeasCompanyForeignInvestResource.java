/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperFinLeasCompanyForeignInvest;
import cn.com.yusys.yusp.service.RptOperFinLeasCompanyForeignInvestService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperFinLeasCompanyForeignInvestResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 16:45:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperfinleascompanyforeigninvest")
public class RptOperFinLeasCompanyForeignInvestResource {
    @Autowired
    private RptOperFinLeasCompanyForeignInvestService rptOperFinLeasCompanyForeignInvestService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperFinLeasCompanyForeignInvest>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperFinLeasCompanyForeignInvest> list = rptOperFinLeasCompanyForeignInvestService.selectAll(queryModel);
        return new ResultDto<List<RptOperFinLeasCompanyForeignInvest>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperFinLeasCompanyForeignInvest>> index(QueryModel queryModel) {
        List<RptOperFinLeasCompanyForeignInvest> list = rptOperFinLeasCompanyForeignInvestService.selectByModel(queryModel);
        return new ResultDto<List<RptOperFinLeasCompanyForeignInvest>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperFinLeasCompanyForeignInvest> show(@PathVariable("pkId") String pkId) {
        RptOperFinLeasCompanyForeignInvest rptOperFinLeasCompanyForeignInvest = rptOperFinLeasCompanyForeignInvestService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperFinLeasCompanyForeignInvest>(rptOperFinLeasCompanyForeignInvest);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperFinLeasCompanyForeignInvest> create(@RequestBody RptOperFinLeasCompanyForeignInvest rptOperFinLeasCompanyForeignInvest) throws URISyntaxException {
        rptOperFinLeasCompanyForeignInvestService.insert(rptOperFinLeasCompanyForeignInvest);
        return new ResultDto<RptOperFinLeasCompanyForeignInvest>(rptOperFinLeasCompanyForeignInvest);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperFinLeasCompanyForeignInvest rptOperFinLeasCompanyForeignInvest) throws URISyntaxException {
        int result = rptOperFinLeasCompanyForeignInvestService.update(rptOperFinLeasCompanyForeignInvest);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperFinLeasCompanyForeignInvestService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperFinLeasCompanyForeignInvestService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperFinLeasCompanyForeignInvest>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptOperFinLeasCompanyForeignInvest>>(rptOperFinLeasCompanyForeignInvestService.selectByModel(model));
    }
    @PostMapping("/deleteForeign")
    protected ResultDto<Integer> deleteForeign(@RequestBody RptOperFinLeasCompanyForeignInvest rptOperFinLeasCompanyForeignInvest){
        return new ResultDto<Integer>(rptOperFinLeasCompanyForeignInvestService.deleteByPrimaryKey(rptOperFinLeasCompanyForeignInvest.getPkId()));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptOperFinLeasCompanyForeignInvest rptOperFinLeasCompanyForeignInvest){
        return new ResultDto<Integer>(rptOperFinLeasCompanyForeignInvestService.save(rptOperFinLeasCompanyForeignInvest));
    }
}
