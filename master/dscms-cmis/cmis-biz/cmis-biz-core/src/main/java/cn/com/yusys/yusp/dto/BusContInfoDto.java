package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class BusContInfoDto {
    //流水号
    private String serno;
    //合同号
    private String contNo;
    //额度编号
    private String lmtAccNo;
    //批复编号
    private String replyNo;
    //客户编号
    private String cusId;
    //客户姓名
    private String cusName;
    //贷款形式
    private String loanModal;
    //产品编号
    private String prdId;
    //产品名称
    private String prdName;
    //贷款投向
    private String loanTer;
    //合同类型
    private String contType;
    //担保方式
    private String guarMode;
    //币种
    private String curType;
    //合同金额
    private BigDecimal contAmt;
    //起始日
    private String startDate;
    //到期日
    private String endDate;
    //合同状态
    private String contStatus;
    //登记人编号
    private String inputId;
    //主管客户经理号
    private String managerId;
    //主管机构号
    private String managerBrId;
    //账务机构
    private String finaBrId;
    //保函类型
    private String guaranteeType;
    //保函名称
    private String guaranteeName;
    //信用证付款期限
    private BigDecimal iocTerm;
    //远期天数
    private Integer fastDay;
    //具体产品
    private String proDetails;
    //是否可无条件撤销
    private String isinRevocation;
    //合同期限
    private Integer contTerm;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getGuaranteeName() {
        return guaranteeName;
    }

    public void setGuaranteeName(String guaranteeName) {
        this.guaranteeName = guaranteeName;
    }

    public BigDecimal getIocTerm() {
        return iocTerm;
    }

    public void setIocTerm(BigDecimal iocTerm) {
        this.iocTerm = iocTerm;
    }

    public Integer getFastDay() {
        return fastDay;
    }

    public void setFastDay(Integer fastDay) {
        this.fastDay = fastDay;
    }

    public String getProDetails() {
        return proDetails;
    }

    public void setProDetails(String proDetails) {
        this.proDetails = proDetails;
    }

    public String getIsinRevocation() {
        return isinRevocation;
    }

    public void setIsinRevocation(String isinRevocation) {
        this.isinRevocation = isinRevocation;
    }

    public Integer getContTerm() {
        return contTerm;
    }

    public void setContTerm(Integer contTerm) {
        this.contTerm = contTerm;
    }
}
