package cn.com.yusys.yusp.service.server.xdtz0013;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.req.Xdtz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.resp.BillList;
import cn.com.yusys.yusp.dto.server.xdtz0013.resp.Xdtz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AreaAdminUserMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 接口处理类:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0013Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0013Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 交易码：Xdtz0013
     * 交易描述：查询小微借据余额、结清日期、贷款类型
     *
     * @param xdtz0013DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0013DataRespDto xdtz0013(Xdtz0013DataReqDto xdtz0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataReqDto));
        //定义返回信息
        Xdtz0013DataRespDto xdtz0013DataRespDto = new Xdtz0013DataRespDto();
        try {
            //请求字段
            String certType = xdtz0013DataReqDto.getCertType();
            String certNo = xdtz0013DataReqDto.getCertNo();
            String cusId = StringUtils.EMPTY;

            //根据客户证件号查询客户的客户号
            List<String> cusIds = commonService.getCusBaseByCertCode(certNo);
            if (CollectionUtils.nonEmpty(cusIds)) {
                cusId = cusIds.get(0);
            }
            //如果客户号查询为空,直接返回
            if (StringUtil.isEmpty(cusId)) {//无此客户
                return xdtz0013DataRespDto;
            }
            //查询借据余额
            BigDecimal billBal = BigDecimal.ZERO;
            //查询最近一笔借据的结清日期
            String lastSettlDate = StringUtils.EMPTY;
            //产品编号
            String prdId = StringUtils.EMPTY;
            //借据编号
            String billNo = StringUtils.EMPTY;
            //查询团队类型
            String managerId = StringUtils.EMPTY;
            String orgNo = StringUtils.EMPTY;
            String managerName = StringUtils.EMPTY;
            String OrgName = StringUtils.EMPTY;
            String teamType = StringUtils.EMPTY;
            String surveyType = StringUtils.EMPTY;

            //查詢貸款信息
            Map queryMap = new HashMap();
            queryMap.put("cusId", cusId);
            logger.info("***********查询台账信息表开始*START**************查询参数" + cusId);
            List<AccLoan> accLoanList = accLoanMapper.selectAccLoanForxdtz(queryMap);
            logger.info("***********查询台账信息表结束*END***************");
            java.util.List<BillList> list = new ArrayList<>();
            for (int i = 0; i < accLoanList.size(); i++) {
                managerId = accLoanList.get(i).getManagerId();
                orgNo = accLoanList.get(i).getManagerBrId();
                prdId = accLoanList.get(i).getPrdId();
                billNo = accLoanList.get(i).getBillNo();
                billBal = accLoanList.get(i).getLoanBalance();
                lastSettlDate = accLoanList.get(i).getSettlDate();
                logger.info("***********调用AdminSmUserService;adminSmOrgService用户信息查询服务开始*START**************");
                if (StringUtil.isNotEmpty(managerId)) {//查询经理号不为空
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    String code = resultDto.getCode();//返回结果
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        managerName = adminSmUserDto.getUserName();
                    }
                }
                if (StringUtil.isNotEmpty(orgNo)) {//查询机构号不为空
                    ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(orgNo);
                    String codeNum = resultOrgDto.getCode();//返回结果
                    if (StringUtil.isNotEmpty(codeNum) && CmisBizConstants.NUM_ZERO.equals(codeNum)) {
                        AdminSmOrgDto adminorgDto = resultOrgDto.getData();
                        OrgName = adminorgDto.getOrgName();
                    }
                }
                logger.info("***********调用AdminSmUserService;adminSmOrgService用户信息查询服务结束*END*****************");
                //查询团队机构
                QueryModel model = new QueryModel();
                model.addCondition("userNo", managerId);
                List<AreaAdminUser> adminUserList = areaAdminUserMapper.selectByModel(model);
                if (CollectionUtils.nonEmpty(adminUserList) && adminUserList.size() > 0) {
                    teamType = adminUserList.get(0).getTeamType();
                    surveyType = adminUserList.get(0).getSurveyMode();
                }
                logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                String prdCode = prdresultDto.getCode();//返回结果
                String prdType = StringUtils.EMPTY;
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                    if (CfgPrdBasicinfoDto != null) {
                        prdType = CfgPrdBasicinfoDto.getPrdType();
                    }
                }
                if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType)) {//经营
                    prdType = DscmsBizTzEnum.PRDTYPE_01.key;
                } else if (DscmsBizTzEnum.PRDTYPE_09.key.equals(prdType)) {//消费
                    prdType = DscmsBizTzEnum.PRDTYPE_02.key;
                }
                logger.info("***********调用iCmisCfgClientService查询产品类别*END**************");

                if (billBal == null) {
                    billBal = BigDecimal.ZERO;
                }
                //返回信息
                BillList billList = new BillList();
                billList.setBillNo(billNo);// 借据编号
                billList.setBillBal(billBal);// 借据余额
                billList.setLastSettlDate(lastSettlDate);// 最近一笔借据结清日期
                billList.setLoanType(prdType);// 贷款类型
                billList.setManagerId(managerId);// 归属客户经理工号
                billList.setManagerName(managerName);// 归属客户经理姓名
                billList.setOrgNo(orgNo);// 所在机构编号
                billList.setOrgName(OrgName);// 所在机构名称
                billList.setTeamType(teamType);// 直营团队类型
                list.add(billList);
            }
            xdtz0013DataRespDto.setBillList(list);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataRespDto));
        return xdtz0013DataRespDto;
    }


}
