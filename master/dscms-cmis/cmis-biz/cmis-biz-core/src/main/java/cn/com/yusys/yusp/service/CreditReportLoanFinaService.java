/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CreditAuthbookInfo;
import cn.com.yusys.yusp.domain.CreditQryBizReal;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006RespDto;
import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CreditAuthbookInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.CmisBizCiis2ndUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditReportLoanFina;
import cn.com.yusys.yusp.repository.mapper.CreditReportLoanFinaMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportLoanFinaService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-07 16:49:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditReportLoanFinaService {

    private static final Logger log = LoggerFactory.getLogger(CreditReportLoanFinaService.class);

    @Autowired
    private CreditReportLoanFinaMapper creditReportLoanFinaMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    @Autowired
    private CreditQryBizRealService creditQryBizRealService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;

    @Value("${application.creditUrl.url}")
    private String creditUrl;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private Dscms2SzzxClientService dscms2SzzxClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CreditAuthbookInfoMapper creditAuthbookInfoMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditReportLoanFina selectByPrimaryKey(String crlfSerno) {
        return creditReportLoanFinaMapper.selectByPrimaryKey(crlfSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditReportLoanFina> selectAll(QueryModel model) {
        List<CreditReportLoanFina> records = (List<CreditReportLoanFina>) creditReportLoanFinaMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditReportLoanFina> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditReportLoanFina> list = creditReportLoanFinaMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditReportLoanFina record) {
        return creditReportLoanFinaMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditReportLoanFina record) {
        User userInfo = SessionUtils.getUserInformation();
        // 获取用户信息
        if (userInfo != null) {
            record.setInputId(userInfo.getLoginCode());
            record.setInputBrId(userInfo.getOrg().getCode());
            record.setInputDate(DateUtils.getCurrDateTimeStr());
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        record.setUpdDate(DateUtils.getCurrDateTimeStr());
        return creditReportLoanFinaMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditReportLoanFina record) {
        return creditReportLoanFinaMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditReportLoanFina record) {
        User userInfo = SessionUtils.getUserInformation();
        // 获取用户信息
        if (userInfo != null) {
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        record.setUpdDate(DateUtils.getCurrDateTimeStr());
        return creditReportLoanFinaMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String crlfSerno) {
        return creditReportLoanFinaMapper.deleteByPrimaryKey(crlfSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditReportLoanFinaMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: createCreditAndLoan
     * @方法描述: 移动OAM征信查询信息保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int createCreditAndLoan(CreditReportLoanFinaDto record) {
        log.info("移动OA征信查询请求报文："+record.toString());
        if (StringUtils.isBlank(record.getAuthbookNo())) {
            record.setAuthbookNo(sequenceTemplateClient.getSequenceTemplate(SeqConstant.AUTHBOOK_NO_SEQ, new HashMap<>()));
        }
        if (StringUtils.isBlank(record.getCrlfSerno())) {
            record.setCrlfSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRLF_SERNO, new HashMap<>()));
        }
        //在人行征信查询发起提交时，对同一个客户，在同一天中，同一证件类型、同一查询原因的征信查询，只允许发起一次
        if ("1".equals(record.getIsAutoCheckPass()) && !"3".equals(record.getQryCls())) {
            //初始化查询条件
            QueryModel model = new QueryModel();
            model.addCondition("certCode",record.getCertCode());
            model.addCondition("certType",record.getCertType());
            model.addCondition("qryResn",record.getQryResn());
            List<CreditReportQryLst> list = creditReportQryLstMapper.selectByModel(model);
            if(CollectionUtils.nonEmpty(list)) {
                for(CreditReportQryLst report : list) {
                    // 创建时间与当前时间一致，则表示发起流程的时间
                    if (!Objects.equals(CmisFlowConstants.WF_STATUS_000,report.getApproveStatus()) && Objects.equals(DateUtils.formatDate(report.getCreateTime(),"yyyy-MM-dd"),DateUtils.formatDate("yyyy-MM-dd"))) {
                        throw BizException.error(null, "9999", "同一天不能再次对同一客户发起人行征信申请");
                    }
                }
            }
        }
        // 判断是否已存在征信记录，有则更新，无则新增
        int result = 0;
        CreditReportLoanFina creditReportLoanFina = new CreditReportLoanFina();
        CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
        BeanUtils.copyProperties(record, creditReportLoanFina);
        BeanUtils.copyProperties(record, creditReportQryLstAndRealDto);

        creditReportQryLstAndRealDto.setApproveStatus(Optional.ofNullable(record.getApproveStatus()).orElse("000"));
        creditReportQryLstAndRealDto.setIsSuccssInit(Optional.ofNullable(record.getIsSuccssInit()).orElse("0"));
        creditReportQryLstAndRealDto.setQryFlag(Optional.ofNullable(record.getQryFlag()).orElse("05"));
        creditReportQryLstAndRealDto.setQryStatus(Optional.ofNullable(record.getQryStatus()).orElse("001"));
        //生成关联征信数据
        if (StringUtils.isBlank(record.getBizSerno()) && !"3".equals(record.getQryCls())) {
            creditReportQryLstAndRealDto.setBizSerno(record.getCrlfSerno());
        }
        creditReportQryLstAndRealDto.setScene(Optional.ofNullable(record.getScene()).orElse("01"));
        int doCreateCreditAuto = createCreditQryAndRel(creditReportQryLstAndRealDto, record.getIsAutoCheckPass());
        if (doCreateCreditAuto == 0) {
            log.info("贷款融资业务流水号：{}，生成征信数据异常", record.getCrlfSerno());
            result = doCreateCreditAuto;
            return result;
        }

        // 如果是苏州地方征信，则不需添加融资
        if ("3".equals(record.getQryCls())) {
            return result;
        }
        if (creditReportLoanFinaMapper.selectByPrimaryKey(record.getCrlfSerno()) != null) {
            result = updateSelective(creditReportLoanFina);
            if (result != 1) {
                log.info("更新贷款融资业务数据异常");
                return result;
            }
        } else {
            result = insertSelective(creditReportLoanFina);
            if (result != 1) {
                log.info("生成贷款融资业务数据异常");
                return result;
            }
        }
        return result;
    }

    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public int createCreditQryAndRel(CreditReportQryLstAndRealDto creditReportQryLstAndRealDto, String isAutoCheckPass) {
        //添加征信信息
        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
        //征信关联表
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        String crqlSerno = null;
        int result = 0;
        BeanUtils.copyProperties(creditReportQryLstAndRealDto, creditReportQryLst);
        if (creditReportQryLstMapper.selectByPrimaryKey(creditReportQryLstAndRealDto.getCrqlSerno()) != null) {
            // 如果存在，直接关联
            // 查出该信息,并更新数据
            result = creditReportQryLstService.update(creditReportQryLst);
            if (result != 1) {
                log.info("业务流水号：{}，更新征信详情数据异常", creditReportQryLstAndRealDto.getBizSerno());
                return result;
            }
            crqlSerno = creditReportQryLstAndRealDto.getCrqlSerno();
        } else {
            //生成征信流水号
            if (StringUtils.nonBlank(creditReportQryLstAndRealDto.getCrqlSerno())) {
                crqlSerno = creditReportQryLstAndRealDto.getCrqlSerno();
            } else {
                crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
            }
            //生成征信详情数据
            creditReportQryLst.setCrqlSerno(crqlSerno);
            result = creditReportQryLstService.insertSelective(creditReportQryLst);
            if (result != 1) {
                log.info("业务流水号：{}，生成征信详情数据异常", creditReportQryLstAndRealDto.getBizSerno());
                return result;
            }
        }
        if ("1".equals(isAutoCheckPass) && "3".equals(creditReportQryLstAndRealDto.getQryCls())) {
            handleBusinessDZDataAfterEnd(crqlSerno);
        }
        if ("1".equals(isAutoCheckPass) && !"3".equals(creditReportQryLstAndRealDto.getQryCls())) {
            handleBusinessDataAfterEnd(crqlSerno);
        }

        if (StringUtils.isBlank(creditReportQryLstAndRealDto.getBizSerno())) {
            return result;
        }
        //生成关联征信数据
        BeanUtils.copyProperties(creditReportQryLstAndRealDto, creditQryBizReal);
        creditQryBizReal.setCrqlSerno(crqlSerno);
        if (creditQryBizRealMapper.selectByBizSernoAndCreSerno(creditReportQryLstAndRealDto.getBizSerno(), crqlSerno) != null) {
            creditQryBizReal.setCqbrSerno(creditQryBizRealMapper.selectByBizSernoAndCreSerno(creditReportQryLstAndRealDto.getBizSerno(), crqlSerno).getCqbrSerno());
            result = creditQryBizRealService.updateSelective(creditQryBizReal);
            if (result != 1) {
                log.info("业务流水号：{}，更新征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
                return result;
            }
        } else {
            result = creditQryBizRealService.insertSelective(creditQryBizReal);
            if (result != 1) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
                return result;
            }
        }
        return result;
    }

    /**
     * @方法名称: queryByCrqlSerno
     * @方法描述: 通过征信流水号查询贷款融资信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CreditReportLoanFina queryByCrqlSerno(String crqlSerno) {
        CreditQryBizReal creditQryBizReal = creditQryBizRealMapper.selectByBizSernoAndCreSerno(null, crqlSerno);
        CreditReportLoanFina creditReportLoanFina = new CreditReportLoanFina();
        if (creditQryBizReal != null && StringUtils.nonBlank(creditQryBizReal.getBizSerno())) {
            creditReportLoanFina = selectByPrimaryKey(creditQryBizReal.getBizSerno());
        }
        return creditReportLoanFina;
    }

    /**
     * @方法名称: selectByCrqlSerno
     * @方法描述: 通过征信流水号查询完整信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CreditReportLoanFinaDto selectByCrqlSerno(String crqlSerno) {
        CreditQryBizReal creditQryBizReal = creditQryBizRealMapper.selectByBizSernoAndCreSerno(null, crqlSerno);
        CreditReportLoanFinaDto creditReportLoanFinaDto = new CreditReportLoanFinaDto();
        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
        CreditReportLoanFina creditReportLoanFina = new CreditReportLoanFina();
        if (creditReportQryLstMapper.selectByPrimaryKey(crqlSerno) != null) {
            creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
        }
        if (creditQryBizReal != null && StringUtils.nonBlank(creditQryBizReal.getBizSerno()) && selectByPrimaryKey(creditQryBizReal.getBizSerno()) != null) {
            creditReportLoanFina = selectByPrimaryKey(creditQryBizReal.getBizSerno());
        }
        BeanUtils.copyProperties(creditReportLoanFina, creditReportLoanFinaDto);
        BeanUtils.copyProperties(creditReportQryLst, creditReportLoanFinaDto);
        return creditReportLoanFinaDto;
    }


    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDataAfterEnd(String crqlSerno) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取征信申请{}申请主表信息", crqlSerno);
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            int updateCount = 0;
            log.info("审批通过生成获征信信息-发送征信系统{}信息", crqlSerno);

            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            CredxxReqDto credxxReqDto = new CredxxReqDto();
            credxxReqDto.setCreditType(creditReportQryLst.getQryCls());//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(creditReportQryLst.getCusName());//客户名称
            credxxReqDto.setCertificateType(creditReportQryLstService.toConverCurrency(creditReportQryLst.getCertType()));//证件类型
            credxxReqDto.setCertificateNum(creditReportQryLst.getCertCode());//证件号
            credxxReqDto.setQueryReason(creditReportQryLstService.convertQueryReason(creditReportQryLst.getQryResn(), creditReportQryLst.getQryCls()));//查询原因
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserId(creditReportQryLst.getManagerId());//客户经理工号
            credxxReqDto.setBrchno(creditReportQryLst.getManagerBrId());
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("1");//信用报告复用策略
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            //总行：朱真尧 东海：邵雯
            if (creditReportQryLst.getManagerBrId().startsWith("81")) {
                credxxReqDto.setApprovalName("邵雯");//审批人姓名
                credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
            } else {
                credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
            }
            // credxxReqDto.setApprovalName(adminSmUserDto.getUserName());//审批人姓名
            // credxxReqDto.setApprovalIdCard(adminSmUserDto.getCertNo());//审批人身份证号
            credxxReqDto.setArchiveCreateDate(creditReportQryLst.getAuthbookDate());//授权书签订日期
            credxxReqDto.setArchiveExpireDate(DateUtils.addYear(creditReportQryLst.getAuthbookDate(), DateFormatEnum.DEFAULT.getValue(), 50));//授权结束日期
            credxxReqDto.setCreditDocId(Optional.ofNullable(creditReportQryLst.getImageNo()).orElse(crqlSerno));//授权影像编号
            credxxReqDto.setBorrowPersonRelation(creditReportQryLst.getBorrowRel());//与主借款人关系
            credxxReqDto.setBorrowPersonRelationName(creditReportQryLst.getBorrowerCusName());//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber(creditReportQryLst.getBorrowerCertCode());//主借款人证件号
            credxxReqDto.setAuditReason(creditReportQryLstService.convertAuthCont(creditReportQryLst.getAuthbookContent(), creditReportQryLst.getQryCls()));//授权书内容

            ResultDto<CredxxRespDto> credxxResultDto = dscms2Ciis2ndClientService.credxx(credxxReqDto);
            String credxxCode = Optional.ofNullable(credxxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String credxxMeesage = Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            CredxxRespDto credxxRespDto = null;
            // 征信白户处理
            if ("6666".equals(credxxResultDto.getCode())) {
                log.info("征信中心无信用信息---此异常特殊处理");
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                creditReportQryLst.setQryStatus("003");

                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                creditAuthbookInfo.setAuthMode("01");
                creditAuthbookInfo.setImageNo(crqlSerno);

                // 授权书台账，根据授权编号，有则更新，无则新增
                CreditAuthbookInfo creditAuthbookInfoOld = creditAuthbookInfoMapper.selectByAuthBookNo(creditAuthbookInfo);
                if (creditAuthbookInfoOld != null) {
                    BeanUtils.copyProperties(creditAuthbookInfo, creditAuthbookInfoOld);
                    creditAuthbookInfoService.updateSelective(creditAuthbookInfoOld);
                } else {
                    creditAuthbookInfoService.insert(creditAuthbookInfo);
                }
            } else {
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credxxResultDto.getCode())) {
                    //  获取相关的值并解析
                    credxxRespDto = credxxResultDto.getData();
                    String zxSerno = credxxRespDto.getReportId();
                    creditReportQryLst.setReportNo(zxSerno);
                    if (!StringUtils.isEmpty(zxSerno)) {
                        creditReportQryLst.setIsSuccssInit("1");
                        creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                        creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                        creditReportQryLst.setQryStatus("003");

                        CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
                        callCiis2ndReqDto.setPrefixUrl(creditUrl);
                        callCiis2ndReqDto.setReqId(credxxRespDto.getReqId());
                        callCiis2ndReqDto.setOrgName("信贷");
                        callCiis2ndReqDto.setUserCode(creditReportQryLst.getManagerId());
                        callCiis2ndReqDto.setUserName(adminSmUserDto.getUserName());
                        if ("0".equals(creditReportQryLst.getQryCls())) {
                            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
                        } else {
                            callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_ENT.key);
                        }
                        String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
                        creditReportQryLst.setCreditUrl(url);
                    } else {
                        creditReportQryLst.setQryStatus("002");
                    }
                } else {
                    // 不抛出错误异常，记录错误异常处理
                    log.error("二代征信交易失败，流水号：{}，错误码值：{}，错误信息：{}", crqlSerno, credxxCode, credxxMeesage);
                    //  抛出错误异常
                    throw BizException.error(null, credxxCode,credxxMeesage);
                }
                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                creditAuthbookInfo.setAuthMode("01");
                creditAuthbookInfo.setImageNo(crqlSerno);

                // 授权书台账，根据授权编号，有则更新，无则新增
                CreditAuthbookInfo creditAuthbookInfoOld = creditAuthbookInfoMapper.selectByAuthBookNo(creditAuthbookInfo);
                if (creditAuthbookInfoOld != null) {
                    BeanUtils.copyProperties(creditAuthbookInfo, creditAuthbookInfoOld);
                    creditAuthbookInfoService.updateSelective(creditAuthbookInfoOld);
                } else {
                    creditAuthbookInfoService.insert(creditAuthbookInfo);
                }
            }

            creditReportQryLst.setImageNo(crqlSerno);
            creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
            creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);

            log.info("审批通过-更新征信业务申请{}流程审批状态为【997】-通过", crqlSerno);
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_997);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            if (!"27".equals(creditReportQryLst.getQryResn()) && "1".equals(creditReportQryLst.getIsSuccssInit()) && !"04".equals(creditReportQryLst.getQryFlag())) {
                DocCreditArchiveClientDto docCreditArchiveClientDto = new DocCreditArchiveClientDto();
                BeanUtils.copyProperties(creditReportQryLst, docCreditArchiveClientDto);
                docCreditArchiveClientDto.setBizSerno(creditReportQryLst.getCrqlSerno());
                docCreditArchiveClientDto.setQryUser(creditReportQryLst.getManagerId());
                docCreditArchiveClientDto.setQryOrg(creditReportQryLst.getManagerBrId());
                docCreditArchiveClientDto.setInputId(creditReportQryLst.getManagerId());
                docCreditArchiveClientDto.setInputBrId(creditReportQryLst.getManagerBrId());
                cmisBizClientService.createDocCreateArchiveBySys(docCreditArchiveClientDto);
            }

        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    @Transactional(rollbackFor = {Exception.class, BizException.class})
    public void handleBusinessDZDataAfterEnd(String crqlSerno) {
        try {
            if (StringUtils.isBlank(crqlSerno)) {
                throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-获取征信申请" + crqlSerno + "申请主表信息");
            CreditReportQryLst creditReportQryLst = creditReportQryLstMapper.selectByPrimaryKey(crqlSerno);
            if (creditReportQryLst == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;

            log.info("审批通过生成获征信信息-发送征信系统" + crqlSerno + "信息");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(creditReportQryLst.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();

            ResultDto<AdminSmOrgDto> resultDto1 = adminSmOrgService.getByOrgCode(creditReportQryLst.getManagerBrId());
            AdminSmOrgDto adminSmOrgDto = resultDto1.getData();

            Zxcx006ReqDto zxcx006ReqDto = new Zxcx006ReqDto();
            // 查询苏州地方征信信息时，先获取taskId，再获取征信url
            zxcx006ReqDto.setState("new");    //请求类型
            zxcx006ReqDto.setReportReason(creditReportQryLst.getQryResn());    //查询原因
            zxcx006ReqDto.setQuerystaffNo(creditReportQryLst.getManagerId());    //客户经理工号
            zxcx006ReqDto.setQuerystaff(adminSmUserDto.getUserName());    //客户经理名称
            zxcx006ReqDto.setInqueryorgNo(adminSmOrgDto.getOrgCode());    //查询机构号
            zxcx006ReqDto.setInqueryorg(adminSmOrgDto.getOrgName());    //查询机构名称
            zxcx006ReqDto.setCardCode(creditReportQryLst.getCertCode());    //证件号码
            //测试环境：ZJGNS-ZXD；生产环境：zjgns-ssjk
            String zxWsUser = creditReportQryLstService.getSysParameterByName("ZX_WS_USER");
            zxcx006ReqDto.setWsUser(Optional.ofNullable(zxWsUser).orElse("ZJGNS-ZXD"));    //征信提供的用户名

            ResultDto<Zxcx006RespDto> zxcx006ResultDto = dscms2SzzxClientService.zxcx006(zxcx006ReqDto);
            String zxcx006Code = Optional.ofNullable(zxcx006ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zxcx006Meesage = Optional.ofNullable(zxcx006ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Zxcx006RespDto zxcx006RespDto = null;
            if (Objects.equals(zxcx006Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取相关的值并解析
                zxcx006RespDto = zxcx006ResultDto.getData();
                String taskId = zxcx006RespDto.getTaskId(); //任务id,否,state为new时，必输，作为查询时的taskId传入；state为old时，非必输

                if (!StringUtils.isEmpty(taskId)) {
                    //苏州地方征信，由于需要数据，暂时使用给定可用的值就行处理
                    creditReportQryLst.setIsSuccssInit("1");
                    creditReportQryLst.setReportCreateTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setSendTime(stringRedisTemplate.opsForValue().get("openDay"));
                    creditReportQryLst.setQryStatus("003");
                    creditReportQryLst.setReportNo(taskId);

                    if (StringUtils.startsWith(zxcx006RespDto.getErortx(), "http")) {
                        String url = zxcx006RespDto.getErortx(); //url
                        creditReportQryLst.setCreditUrl(url);
                    } else {
                        throw BizException.error(null, "9999", zxcx006RespDto.getErortx());
                    }
                } else {
                    creditReportQryLst.setQryStatus("002");
                }
            } else {
                // 不抛出错误异常，记录错误异常处理
                log.error("苏州地方征信交易失败，流水号：{}，错误码值：{}，错误信息：{}", crqlSerno, zxcx006Code, zxcx006Meesage);
                //  抛出错误异常
                 throw BizException.error(null,zxcx006Code,zxcx006Meesage);
            }
            creditReportQryLstMapper.updateByPrimaryKeySelective(creditReportQryLst);

            log.info("审批通过-更新征信业务申请" + crqlSerno + "流程审批状态为【997】-通过");
            updateCount = creditReportQryLstMapper.updateApproveStatus(crqlSerno, CmisCommonConstants.WF_STATUS_997);
            if (updateCount < 0) {
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 获取sequence流水号
     **/
    public String querySequences() {
        //TODO 后续改为getSequenceTemplate获取
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
        return serno;
    }
}
