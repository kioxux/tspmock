/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydAcctSubDetail
 * @类描述: tmp_wyd_acct_sub_detail数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name ="tmp_wyd_acct_sub_detail_tmp")
public class TmpWydAcctSubDetailTmp {
	
	/** 流水号 **/
	@Id
	@Column(name = "SERIALNO")
	private String serialno;
	
	/** 交易流水号 **/
	@Column(name = "TRANSSERIALNO", unique = false, nullable = true, length = 32)
	private String transserialno;
	
	/** 交易码 **/
	@Column(name = "TRANSCODE", unique = false, nullable = true, length = 10)
	private String transcode;
	
	/** 分账流水号 **/
	@Column(name = "SUBLEDGERSERIALNO", unique = false, nullable = true, length = 32)
	private String subledgerserialno;
	
	/** 借据编号 **/
	@Column(name = "RELATIVEOBJECTNO", unique = false, nullable = true, length = 32)
	private String relativeobjectno;
	
	/** 记账机构 **/
	@Column(name = "ACCOUNTINGORGID", unique = false, nullable = true, length = 10)
	private String accountingorgid;
	
	/** 币种 **/
	@Column(name = "CURRENCY", unique = false, nullable = true, length = 10)
	private String currency;
	
	/** 发生日期 **/
	@Column(name = "BOOKDATE", unique = false, nullable = true, length = 10)
	private String bookdate;
	
	/** 系统科目号 **/
	@Column(name = "ACCOUNTCODENO", unique = false, nullable = true, length = 20)
	private String accountcodeno;
	
	/** 银行科目号 **/
	@Column(name = "EXACCOUNTCODENO", unique = false, nullable = true, length = 20)
	private String exaccountcodeno;
	
	/** 发生额 **/
	@Column(name = "AMOUNT", unique = false, nullable = true, length = 20)
	private String amount;
	
	/** 余额方向 **/
	@Column(name = "DIRECTION", unique = false, nullable = true, length = 5)
	private String direction;
	
	/** 描述 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 变化后借方余额 **/
	@Column(name = "DEBITBALANCE", unique = false, nullable = true, length = 20)
	private String debitbalance;
	
	/** 变化后贷方余额 **/
	@Column(name = "CREDITBALANCE", unique = false, nullable = true, length = 20)
	private String creditbalance;
	
	/** 变化前借方余额 **/
	@Column(name = "ODEBITBALANCE", unique = false, nullable = true, length = 20)
	private String odebitbalance;
	
	/** 变化前贷方余额 **/
	@Column(name = "OCREDITBALANCE", unique = false, nullable = true, length = 20)
	private String ocreditbalance;
	
	/** 交易明细码 **/
	@Column(name = "SUBTRANSCODE", unique = false, nullable = true, length = 10)
	private String subtranscode;
	
	/** 描述代码 **/
	@Column(name = "REMARKCODE", unique = false, nullable = true, length = 200)
	private String remarkcode;
	
	/** 产品id **/
	@Column(name = "PRODUCTID", unique = false, nullable = true, length = 20)
	private String productid;
	
	/** 分录类型 **/
	@Column(name = "SUBDETAILTYPE", unique = false, nullable = true, length = 30)
	private String subdetailtype;
	
	/** 支付系统流水号 **/
	@Column(name = "CONSUMERSEQNO", unique = false, nullable = true, length = 32)
	private String consumerseqno;
	
	/** 支付业务流水号 **/
	@Column(name = "BIZSEQNO", unique = false, nullable = true, length = 32)
	private String bizseqno;
	
	
	/**
	 * @param serialno
	 */
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	
    /**
     * @return serialno
     */
	public String getSerialno() {
		return this.serialno;
	}
	
	/**
	 * @param transserialno
	 */
	public void setTransserialno(String transserialno) {
		this.transserialno = transserialno;
	}
	
    /**
     * @return transserialno
     */
	public String getTransserialno() {
		return this.transserialno;
	}
	
	/**
	 * @param transcode
	 */
	public void setTranscode(String transcode) {
		this.transcode = transcode;
	}
	
    /**
     * @return transcode
     */
	public String getTranscode() {
		return this.transcode;
	}
	
	/**
	 * @param subledgerserialno
	 */
	public void setSubledgerserialno(String subledgerserialno) {
		this.subledgerserialno = subledgerserialno;
	}
	
    /**
     * @return subledgerserialno
     */
	public String getSubledgerserialno() {
		return this.subledgerserialno;
	}
	
	/**
	 * @param relativeobjectno
	 */
	public void setRelativeobjectno(String relativeobjectno) {
		this.relativeobjectno = relativeobjectno;
	}
	
    /**
     * @return relativeobjectno
     */
	public String getRelativeobjectno() {
		return this.relativeobjectno;
	}
	
	/**
	 * @param accountingorgid
	 */
	public void setAccountingorgid(String accountingorgid) {
		this.accountingorgid = accountingorgid;
	}
	
    /**
     * @return accountingorgid
     */
	public String getAccountingorgid() {
		return this.accountingorgid;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
    /**
     * @return currency
     */
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param bookdate
	 */
	public void setBookdate(String bookdate) {
		this.bookdate = bookdate;
	}
	
    /**
     * @return bookdate
     */
	public String getBookdate() {
		return this.bookdate;
	}
	
	/**
	 * @param accountcodeno
	 */
	public void setAccountcodeno(String accountcodeno) {
		this.accountcodeno = accountcodeno;
	}
	
    /**
     * @return accountcodeno
     */
	public String getAccountcodeno() {
		return this.accountcodeno;
	}
	
	/**
	 * @param exaccountcodeno
	 */
	public void setExaccountcodeno(String exaccountcodeno) {
		this.exaccountcodeno = exaccountcodeno;
	}
	
    /**
     * @return exaccountcodeno
     */
	public String getExaccountcodeno() {
		return this.exaccountcodeno;
	}
	
	/**
	 * @param amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
    /**
     * @return amount
     */
	public String getAmount() {
		return this.amount;
	}
	
	/**
	 * @param direction
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
    /**
     * @return direction
     */
	public String getDirection() {
		return this.direction;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param debitbalance
	 */
	public void setDebitbalance(String debitbalance) {
		this.debitbalance = debitbalance;
	}
	
    /**
     * @return debitbalance
     */
	public String getDebitbalance() {
		return this.debitbalance;
	}
	
	/**
	 * @param creditbalance
	 */
	public void setCreditbalance(String creditbalance) {
		this.creditbalance = creditbalance;
	}
	
    /**
     * @return creditbalance
     */
	public String getCreditbalance() {
		return this.creditbalance;
	}
	
	/**
	 * @param odebitbalance
	 */
	public void setOdebitbalance(String odebitbalance) {
		this.odebitbalance = odebitbalance;
	}
	
    /**
     * @return odebitbalance
     */
	public String getOdebitbalance() {
		return this.odebitbalance;
	}
	
	/**
	 * @param ocreditbalance
	 */
	public void setOcreditbalance(String ocreditbalance) {
		this.ocreditbalance = ocreditbalance;
	}
	
    /**
     * @return ocreditbalance
     */
	public String getOcreditbalance() {
		return this.ocreditbalance;
	}
	
	/**
	 * @param subtranscode
	 */
	public void setSubtranscode(String subtranscode) {
		this.subtranscode = subtranscode;
	}
	
    /**
     * @return subtranscode
     */
	public String getSubtranscode() {
		return this.subtranscode;
	}
	
	/**
	 * @param remarkcode
	 */
	public void setRemarkcode(String remarkcode) {
		this.remarkcode = remarkcode;
	}
	
    /**
     * @return remarkcode
     */
	public String getRemarkcode() {
		return this.remarkcode;
	}
	
	/**
	 * @param productid
	 */
	public void setProductid(String productid) {
		this.productid = productid;
	}
	
    /**
     * @return productid
     */
	public String getProductid() {
		return this.productid;
	}
	
	/**
	 * @param subdetailtype
	 */
	public void setSubdetailtype(String subdetailtype) {
		this.subdetailtype = subdetailtype;
	}
	
    /**
     * @return subdetailtype
     */
	public String getSubdetailtype() {
		return this.subdetailtype;
	}
	
	/**
	 * @param consumerseqno
	 */
	public void setConsumerseqno(String consumerseqno) {
		this.consumerseqno = consumerseqno;
	}
	
    /**
     * @return consumerseqno
     */
	public String getConsumerseqno() {
		return this.consumerseqno;
	}
	
	/**
	 * @param bizseqno
	 */
	public void setBizseqno(String bizseqno) {
		this.bizseqno = bizseqno;
	}
	
    /**
     * @return bizseqno
     */
	public String getBizseqno() {
		return this.bizseqno;
	}


}