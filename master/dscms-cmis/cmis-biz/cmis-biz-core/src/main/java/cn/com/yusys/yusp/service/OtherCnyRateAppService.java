/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 10:14:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateAppService {


    private static final Logger log = LoggerFactory.getLogger(OtherCnyRateAppService.class);

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private OtherCnyRateAppMapper otherCnyRateAppMapper;
    
    @Autowired
    private OtherCnyRateAppTaxMapper otherCnyRateAppTaxMapper;
    
    @Autowired
    private OtherCnyRateAppFinDetailsMapper otherCnyRateAppFinDetailsMapper;
    
    @Autowired
    private OtherCnyRateAppSubMapper otherCnyRateAppSubMapper;
    
    @Autowired
    private OtherCnyRateAppSubService otherCnyRateAppSubService;
    
    @Autowired
    private OtherCnyRateApprSubMapper otherCnyRateApprSubMapper;

    @Autowired
    private LmtOtherAppRelMapper lmtOtherAppRelMapper;

    @Autowired
    private OtherCnyRateAppService otherCnyRateAppService;
    @Autowired
    private OtherForRateAppService otherForRateAppService;
    @Autowired
    private OtherBailDepPreferRateAppCusListService otherBailDepPreferRateAppCusListService;
    @Autowired
    private OtherAccpPerferFeeAppCusListService otherAccpPerferFeeAppCusListService;
    @Autowired
    private OtherDiscPerferRateAppService otherDiscPerferRateAppService;
    @Autowired
    private OtherRecordAccpSignPlanAppSubService otherRecordAccpSignPlanAppSubService;
    @Autowired
    private OtherRecordAccpSignOfBocAppService otherRecordAccpSignOfBocAppService;
    @Autowired
    private OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;
    @Autowired
    private OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;
    @Autowired
    private OtherItemAppService otherItemAppService;
    @Autowired
    private OtherBailDepPreferRateAppMapper otherBailDepPreferRateAppMapper;
    @Autowired
    private OtherAccpPerferFeeAppMapper otherAccpPerferFeeAppMapper;
    @Autowired
    private OtherRecordAccpSignPlanAppMapper otherRecordAccpSignPlanAppMapper;
    @Autowired
    private OtherRecordAccpSignOrAllPldAppService otherRecordAccpSignOrAllPldAppService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;
    @Autowired
    private OtherCnyRateAppFinDetailsService otherCnyRateAppFinDetailsService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateApp selectByPrimaryKey(String serno) {
        return otherCnyRateAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateApp> selectAll(QueryModel model) {
        List<OtherCnyRateApp> records = (List<OtherCnyRateApp>) otherCnyRateAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateApp> list = otherCnyRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateApp record) {
    	record.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
    	record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        return otherCnyRateAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateApp record) {
        return otherCnyRateAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateApp record) {
        return otherCnyRateAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateApp record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherCnyRateAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: getOtherCnyRateAppByModel
     * @方法描述: 根据querymodel获取当前客户经理名下所有人民币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateApp> getOtherCnyRateAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_000+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_111
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_990+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_991
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_992+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_993);
        List<OtherCnyRateApp> list = otherCnyRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getOtherCnyRateAppHis
     * @方法描述: 获取当前客户经理名下所有人民币利率定价申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherCnyRateApp> getOtherCnyRateAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_996+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_997
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_998
        );
        List<OtherCnyRateApp> list = otherCnyRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
    
    /**
     * 插入人民币定价利率数据
     * @param dto
     */
    public void insertOtherCnyRateAppDto(OtherCnyRateAppDto dto) {
    	OtherCnyRateApp rateApp = dto.getOtherCnyRateApp();
    	rateApp.setUpdateTime(new Date());
    	rateApp.setUpdDate(DateUtils.getCurrDateStr());
    	OtherCnyRateApp appSaved = otherCnyRateAppMapper.selectByPrimaryKey(dto.getOtherCnyRateApp().getSerno());
    	if (appSaved != null) {
    		otherCnyRateAppMapper.updateByPrimaryKeySelective(rateApp);
    	} else {
    		rateApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        	rateApp.setCreateTime(new Date());
    		otherCnyRateAppMapper.insertSelective(rateApp);
    	}
    	
    	//税费信息
    	List<OtherCnyRateAppTax> taxs = dto.getOtherCnyRateAppTaxList();
     	if (!CollectionUtils.isEmpty(taxs)) {
     		taxs.stream().forEach(tax -> {
        		BeanUtils.copyProperties(rateApp, tax);
        		if (StringUtils.isEmpty(tax.getPkId())) {
        			tax.setPkId(UUID.randomUUID().toString().replace("-",""));
        		}
        		OtherCnyRateAppTax taxSaved = otherCnyRateAppTaxMapper.selectByPrimaryKey(tax.getPkId());
                tax.setOprType("01");
        		if (taxSaved != null) {
        			otherCnyRateAppTaxMapper.updateByPrimaryKeySelective(tax);
        		} else {
        			otherCnyRateAppTaxMapper.insertSelective(tax);
        		}
        	});
     	}
     	
     	//融资信息
     	List<OtherCnyRateAppFinDetails> finDetailsList = dto.getFinDetailsList();
     	if (!CollectionUtils.isEmpty(finDetailsList)) {
     		finDetailsList.stream().forEach(finDetails -> {
     			if (StringUtils.isEmpty(finDetails.getSubSerno())) {
     				finDetails.setSubSerno(UUID.randomUUID().toString().replace("-",""));
     			}
     			OtherCnyRateAppFinDetails finDetailsSaved = 
     					otherCnyRateAppFinDetailsMapper.selectByPrimaryKey(finDetails.getSubSerno());
     			if (finDetailsSaved == null) {
     				otherCnyRateAppFinDetailsMapper.insertSelective(finDetails);
     			} else {
     				otherCnyRateAppFinDetailsMapper.updateByPrimaryKeySelective(finDetails);
     			}
     		});
     		
     	}
     	
     	//审批表
     	List<OtherCnyRateApprSub> apprSubList = dto.getApprSubList();
     	if (!CollectionUtils.isEmpty(apprSubList)) {
     		apprSubList.stream().forEach((apprSub) -> {
     			if (StringUtils.isEmpty(apprSub.getApproveSerno())) {
     				apprSub.setApproveSerno(UUID.randomUUID().toString().replace("-",""));
     			}
     			OtherCnyRateApprSub otherCnyRateApprSubSaved = 
     					otherCnyRateApprSubMapper.selectByPrimaryKey(apprSub.getApproveSerno());
     			if (otherCnyRateApprSubSaved == null) {
     				otherCnyRateApprSubMapper.insertSelective(apprSub);
     			} else {
     				otherCnyRateApprSubMapper.updateByPrimaryKeySelective(apprSub);
     			}
     		});
		}
    	
    }
    
    /**
     *  逻辑删除人民币定价利率及所有关联表
     * @param serno
     */
    public int updateAll(String serno) {
        int result = 0;
        OtherCnyRateApp otherCnyRateApp = otherCnyRateAppMapper.selectByPrimaryKey(serno);
        if (otherCnyRateApp ==null){
            return result;
        }
        //否决、打回
        if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherCnyRateApp.getApproveStatus())){
            otherCnyRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherCnyRateApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherCnyRateApp.getSerno());
            workflowCoreClient.deleteByBizId(otherCnyRateApp.getSerno());
            result = otherCnyRateAppMapper.updateByPrimaryKeySelective(otherCnyRateApp);
        }else{
            otherCnyRateApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            result = otherCnyRateAppMapper.updateByPrimaryKeySelective(otherCnyRateApp);

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", serno);
            //税费
            List<OtherCnyRateAppTax> taxs = otherCnyRateAppTaxMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(taxs)) {
                taxs.stream().forEach(tax -> {
                    tax.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                    otherCnyRateAppTaxMapper.updateByPrimaryKeySelective(tax);
                });
            }
            //融资信息
            List<OtherCnyRateAppFinDetails> finDetailsList = otherCnyRateAppFinDetailsMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(finDetailsList)) {
                finDetailsList.stream().forEach(finDetails -> {
                    finDetails.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                    otherCnyRateAppFinDetailsMapper.updateByPrimaryKeySelective(finDetails);
                });
            }
            //定价权限及关联表
            List<OtherCnyRateAppSub> appSubList = otherCnyRateAppSubMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(appSubList)) {
                appSubList.stream().forEach(appSub -> {
                    otherCnyRateAppSubService.updateAll(appSub.getSubSerno());
                });
            }
        }
    	return result;
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherCnyRateApp otherCnyRateApp = new OtherCnyRateApp();
        otherCnyRateApp.setSerno(serno);
        otherCnyRateApp.setApproveStatus(approveStatus);
        return updateSelective(otherCnyRateApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author css
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherCnyRateAppMapper.selectOtherAppDtoDataByParam(map);
    }

    /**
     * 查询所有类型的其他事项申报数据
     *
     * @author cjd
     */
    public ResultDto<List<Map>> getAppInfo(Map map) {
        List resultList = new ArrayList();
        log.info(String.format("查询其他申报事项开始"));
        String cusId = "";
        if(map.containsKey("cusId")) {
            cusId = (String) map.get("cusId");//客户编号
        }
        if(null != cusId && !"".equals(cusId)) {

            // 人民币利率定价
            QueryModel modelR = new QueryModel ();
            modelR.addCondition("cusId",cusId);
            modelR.addCondition("oprType","01");
            modelR.setSort("update_time desc");
            List<OtherCnyRateApp> OtherCnyRateAppList = otherCnyRateAppMapper.selectByModel(modelR);
            if(OtherCnyRateAppList.size()>0) {
                Map result = new HashMap();
                result.put("serno",OtherCnyRateAppList.get(0).getSerno());
                result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_01);
                result.put("inputId",OtherCnyRateAppList.get(0).getInputId());
                result.put("inputBrId",OtherCnyRateAppList.get(0).getInputBrId());
                result.put("inputDate",OtherCnyRateAppList.get(0).getInputDate());
                result.put("approveStatus",OtherCnyRateAppList.get(0).getApproveStatus());
                resultList.add(result);
            }
            QueryModel model = new QueryModel ();
            model.addCondition("cusId",cusId);
            model.addCondition("oprType","01");
            model.setSort("input_date desc");
            //外币利率定价申请
            List<OtherForRateApp> OtherForRateAppList = otherForRateAppService.selectAll(model);
            if(OtherForRateAppList.size()>0) {
                for(int i=0;i<OtherForRateAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherForRateAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_02);
                    result.put("inputId",OtherForRateAppList.get(i).getInputId());
                    result.put("inputBrId",OtherForRateAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherForRateAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherForRateAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }
            //保证金存款特惠利率申请
            List<OtherBailDepPreferRateAppCusList> OtherBailDepPreferRateAppCusList = otherBailDepPreferRateAppCusListService.selectAll(model);
            if(OtherBailDepPreferRateAppCusList.size()>0) {
                // 获取流水号
                String serno = OtherBailDepPreferRateAppCusList.get(0).getSerno();
                if(null != serno){
                    // 获取 other_bail_dep_prefer_rate_app  保证金存款特惠利率申请
                    OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppMapper.selectByPrimaryKey(serno);
                    if(null != otherBailDepPreferRateApp){
                        Map result = new HashMap();
                        result.put("serno",otherBailDepPreferRateApp.getSerno());
                        result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_03);
                        result.put("inputId",otherBailDepPreferRateApp.getInputId());
                        result.put("inputBrId",otherBailDepPreferRateApp.getInputBrId());
                        result.put("inputDate",otherBailDepPreferRateApp.getInputDate());
                        result.put("approveStatus",otherBailDepPreferRateApp.getApproveStatus());
                        resultList.add(result);
                    }
                }
            }
            //银票手续费优惠申请表
            List<OtherAccpPerferFeeAppCusList> OtherAccpPerferFeeAppCusList = otherAccpPerferFeeAppCusListService.selectAll(model);
            if(OtherAccpPerferFeeAppCusList.size()>0) {
                // 获取流水号
                String sernoYP = OtherAccpPerferFeeAppCusList.get(0).getSerno();
                if(null != sernoYP){
                    // 获取 other_accp_perfer_fee_app  保证金存款特惠利率申请
                    OtherAccpPerferFeeApp otherAccpPerferFeeApp = otherAccpPerferFeeAppMapper.selectByPrimaryKey(sernoYP);
                    if(null != otherAccpPerferFeeApp){
                        Map result = new HashMap();
                        result.put("serno",otherAccpPerferFeeApp.getSerno());
                        result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_04);
                        result.put("inputId",otherAccpPerferFeeApp.getInputId());
                        result.put("inputBrId",otherAccpPerferFeeApp.getInputBrId());
                        result.put("inputDate",otherAccpPerferFeeApp.getInputDate());
                        result.put("approveStatus",otherAccpPerferFeeApp.getApproveStatus());
                        resultList.add(result);
                    }
                }
            }
            //贴现优惠利率申请
            List<OtherDiscPerferRateApp> OtherDiscPerferRateAppList = otherDiscPerferRateAppService.selectAll(model);
            if(OtherDiscPerferRateAppList.size()>0) {
                for(int i=0;i<OtherDiscPerferRateAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherDiscPerferRateAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_05);
                    result.put("inputId",OtherDiscPerferRateAppList.get(i).getInputId());
                    result.put("inputBrId",OtherDiscPerferRateAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherDiscPerferRateAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherDiscPerferRateAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }
            // 银票签发及全资质押类业务 other_record_accp_sign_or_all_pld_app
            List<OtherRecordAccpSignOrAllPldApp> otherRecordAccpSignOrAllPldAppList = otherRecordAccpSignOrAllPldAppService.selectAll(model);
            if(otherRecordAccpSignOrAllPldAppList.size()>0) {
                for(int i=0;i<otherRecordAccpSignOrAllPldAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",otherRecordAccpSignOrAllPldAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_12);
                    result.put("inputId",otherRecordAccpSignOrAllPldAppList.get(i).getInputId());
                    result.put("inputBrId",otherRecordAccpSignOrAllPldAppList.get(i).getInputBrId());
                    result.put("inputDate",otherRecordAccpSignOrAllPldAppList.get(i).getInputDate());
                    result.put("approveStatus",otherRecordAccpSignOrAllPldAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }

            //银票签发业务计划申请
            List<OtherRecordAccpSignPlanAppSub> OtherRecordAccpSignPlanAppSubList = otherRecordAccpSignPlanAppSubService.selectAll(model);
            if(OtherRecordAccpSignPlanAppSubList.size()>0) {
                OtherRecordAccpSignPlanApp otherRecordAccpSignPlanApp  = otherRecordAccpSignPlanAppMapper.selectByPrimaryKey(OtherRecordAccpSignPlanAppSubList.get(0).getSerno());
               if(null != otherRecordAccpSignPlanApp){
                    Map result = new HashMap();
                    result.put("serno",otherRecordAccpSignPlanApp.getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_06);
                    result.put("inputId",otherRecordAccpSignPlanApp.getInputId());
                    result.put("inputBrId",otherRecordAccpSignPlanApp.getInputBrId());
                    result.put("inputDate",otherRecordAccpSignPlanApp.getInputDate());
                    result.put("approveStatus",otherRecordAccpSignPlanApp.getApproveStatus());
                    resultList.add(result);
                }
            }
            //中行代签电票申请
            List<OtherRecordAccpSignOfBocApp> OtherRecordAccpSignOfBocAppList = otherRecordAccpSignOfBocAppService.selectAll(model);
            if(OtherRecordAccpSignOfBocAppList.size()>0) {
                for(int i=0;i<OtherRecordAccpSignOfBocAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherRecordAccpSignOfBocAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_07);
                    result.put("inputId",OtherRecordAccpSignOfBocAppList.get(i).getInputId());
                    result.put("inputBrId",OtherRecordAccpSignOfBocAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherRecordAccpSignOfBocAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherRecordAccpSignOfBocAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }
            // 用信备案申请  OTHER_RECORD_SPECIAL_LOAN_APP
            List<OtherRecordSpecialLoanApp> OtherRecordSpecialLoanAppList = otherRecordSpecialLoanAppService.selectAll(model);
            if(OtherRecordSpecialLoanAppList.size()>0) {
                for(int i=0;i<OtherRecordSpecialLoanAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherRecordSpecialLoanAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_08);
                    result.put("inputId",OtherRecordSpecialLoanAppList.get(i).getInputId());
                    result.put("inputBrId",OtherRecordSpecialLoanAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherRecordSpecialLoanAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherRecordSpecialLoanAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }

            //授信抵质押物价值认定申请
            List<OtherGrtValueIdentyApp> OtherGrtValueIdentyAppList = otherGrtValueIdentyAppService.selectAll(model);
            if(OtherGrtValueIdentyAppList.size()>0) {
                for(int i=0;i<OtherGrtValueIdentyAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherGrtValueIdentyAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_10);
                    result.put("inputId",OtherGrtValueIdentyAppList.get(i).getInputId());
                    result.put("inputBrId",OtherGrtValueIdentyAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherGrtValueIdentyAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherGrtValueIdentyAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }
            //其他事项申请
            List<OtherItemApp> OtherItemAppList = otherItemAppService.selectAll(model);
            if(OtherItemAppList.size()>0) {
                for(int i=0;i<OtherItemAppList.size();i++){
                    Map result = new HashMap();
                    result.put("serno",OtherItemAppList.get(i).getSerno());
                    result.put("bizType",CmisBizConstants.STD_LMT_OTHER_APP_TYPE_11);
                    result.put("inputId",OtherItemAppList.get(i).getInputId());
                    result.put("inputBrId",OtherItemAppList.get(i).getInputBrId());
                    result.put("inputDate",OtherItemAppList.get(i).getInputDate());
                    result.put("approveStatus",OtherItemAppList.get(i).getApproveStatus());
                    resultList.add(result);
                }
            }
        }
        return new ResultDto<List<Map>>(resultList);
    }
}
