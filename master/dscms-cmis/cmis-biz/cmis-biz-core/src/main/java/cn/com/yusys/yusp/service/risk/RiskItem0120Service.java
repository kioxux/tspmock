package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtSxkdPlusFksp;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.repository.mapper.LmtSxkdPlusFkspMapper;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.LmtAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RiskItem0120Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0120Service.class);

    @Autowired
    private LmtSxkdPlusFkspMapper lmtSxkdPlusFkspMapper;

    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @方法名称: riskItem0120
     * @方法描述: 企业国控类校验
     * @参数与返回说明:
     * @算法描述:
     * 若企业的国控类标识为是，则在出账环节需增加风险拦截提醒项：该客户用信需经信贷管理部审核
     * @创建人: zhangliang15
     * @创建时间: 2021-09-23 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0120(QueryModel queryModel) {
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("企业国控类校验开始*******************客户号：【{}】",cusId);
        RiskResultDto riskResultDto = new RiskResultDto();

        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021);
            return riskResultDto;
        }
        // 获取客户基本信息
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        if(Objects.isNull(cusBaseClientDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0010);
            return riskResultDto;
        }
        // 如果是对公客户
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            // 根据客户号获取对公客户基本信息
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
            if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                // 若企业的国控类标识为是,返回该客户用信需经信贷管理部审核
                if ("1".equals(cusCorpDto.getIsNatctl())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_01194);
                    return riskResultDto;
                }
            }
        }

        log.info("企业国控类校验开始*******************客户号：【{}】",cusId);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}