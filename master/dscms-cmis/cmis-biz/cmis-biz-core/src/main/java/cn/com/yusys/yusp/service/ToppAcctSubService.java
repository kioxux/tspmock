/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.ToppAcctSubMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAcctSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:12:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ToppAcctSubService {

    @Resource
    private ToppAcctSubMapper toppAcctSubMapper;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    private static final Logger log = LoggerFactory.getLogger(ToppAcctSubService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ToppAcctSub selectByPrimaryKey(String pkId) {
        return toppAcctSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ToppAcctSub> selectAll(QueryModel model) {
        List<ToppAcctSub> records = (List<ToppAcctSub>) toppAcctSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ToppAcctSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ToppAcctSub> list = toppAcctSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ToppAcctSub record) {
        return toppAcctSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ToppAcctSub record) {
        return toppAcctSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ToppAcctSub record) {
        return toppAcctSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ToppAcctSub record) {
        return toppAcctSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return toppAcctSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return toppAcctSubMapper.deleteByIds(ids);
    }

    /**
     * 获取基本信息
     *
     * @param queryMap
     * @return
     */
    public ToppAcctSub queryToppAcctSubByDataParams(HashMap<String, String> queryMap) {
        return toppAcctSubMapper.queryToppAcctSubByDataParams(queryMap);
    }


    /**
     * 贷款出账申请----交易对手账户明细，新增
     * @param toppAcctSub
     * @author  zhanyb
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveToppAcctSub(ToppAcctSub toppAcctSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try{
            if(toppAcctSub ==null){
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo==null){
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            }else{
                toppAcctSub.setInputId(userInfo.getLoginCode());
                toppAcctSub.setInputBrId(userInfo.getOrg().getCode());
                toppAcctSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                toppAcctSub.setOprType("01");
            }
            Map seqMap = new HashMap();
            String  pkId = StringUtils.uuid(true);
            toppAcctSub.setPkId(pkId);//主键为PK_ID
            String bizSerno = toppAcctSub.getBizSerno();
            int insertCount = toppAcctSubMapper.insertSelective(toppAcctSub);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
            result.put("bizSerno",bizSerno);
            log.info("贷款出账申请交易对手账户明细"+bizSerno+"-保存成功！");
        }catch(YuspException e){
            log.error("贷款出账申请交易对手账户明细新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("贷款出账申请交易对手账户明细异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @创建人 zhanyb
     * @创建时间 2021-05-05 15:35
     * @注释 交易对手账户，分页查询
     */
    public List<ToppAcctSub> queryToppAcctSub(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ToppAcctSub> list = toppAcctSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 zhanyb
     *交易对手账户,修改
     * @param toppAcctSub
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonUpdateToppAcctSub(ToppAcctSub toppAcctSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (toppAcctSub == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                toppAcctSub.setUpdId(userInfo.getLoginCode());
                toppAcctSub.setUpdBrId(userInfo.getOrg().getCode());
                toppAcctSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            Map seqMap = new HashMap();
            int updCount = toppAcctSubMapper.updateByPrimaryKeySelective(toppAcctSub);
            if(updCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }
        }catch(YuspException e){
            log.error("交易对手账户修改异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("交易对手账户异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @函数名称:deleteByPkId
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */

    public int deleteByPkId(ToppAcctSub top) {
        int result=0;
        if (top.getPkId()==null){
            return 0;
        }
        ToppAcctSub toppAcctSub1 = toppAcctSubMapper.selectByPrimaryKey(top.getPkId());
        // 逻辑删除
        toppAcctSub1.setOprType(CommonConstance.OPR_TYPE_DELETE);
        //获取当前登录信息
        // User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        // 判空
//        if (!StringUtils.isBlank(userInfo.getLoginCode()) && null != userInfo.getOrg()) {
//            toppAcctSub1.setUpdId(userInfo.getLoginCode());
//            toppAcctSub1.setUpdBrId(userInfo.getOrg().getCode());
//        }
        toppAcctSub1.setUpdDate(nowDate);
         result = updateSelective(toppAcctSub1);
        return result;
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */

    public List<ToppAcctSub> selectBySerno(QueryModel model) {
        return toppAcctSubMapper.selectBySerno(model);
    }

    /**
     * @函数名称:insertBySerno
     * @函数描述:通过流水号新增参与行信息
     * @参数与返回说明:
     * @算法描述:
     */

    public int insertBySerno(ToppAcctSub toppAcctSub) {
        // 获取当前登录信息
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        // 赋值
        toppAcctSub.setOprType(CommonConstance.OPR_TYPE_ADD);
        toppAcctSub.setInputId(userInfo.getLoginCode());
        toppAcctSub.setInputBrId(userInfo.getOrg().getCode());
        toppAcctSub.setInputDate(nowDate);
        toppAcctSub.setUpdId(userInfo.getLoginCode());
        toppAcctSub.setUpdBrId(userInfo.getOrg().getCode());
        toppAcctSub.setUpdDate(nowDate);
        toppAcctSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return insertSelective(toppAcctSub);
    }

    /**
     * 获取基本信息
     *
     * @param bizSerno
     * @return
     */
    public ToppAcctSub queryToppAcctSubDataByKey(String bizSerno) {
        return toppAcctSubMapper.queryToppAcctSubDataByKey(bizSerno);
    }


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ToppAcctSub> selectDistinctAcctByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ToppAcctSub> list = toppAcctSubMapper.selectDistinctAcctByModel(model);
        PageHelper.clearPage();
        return list;
    }
}