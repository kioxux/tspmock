/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMachEqui
 * @类描述: guar_inf_mach_equi数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:09:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_mach_equi")
public class GuarInfMachEqui extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 设备铭牌编号 **/
	@Column(name = "EQUIP_NO", unique = false, nullable = true, length = 40)
	private String equipNo;
	
	/** 一手/二手标识 STD_ZB_YESBS **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 设备类型 STD_ZB_SBLX **/
	@Column(name = "MACHINE_TYPE", unique = false, nullable = true, length = 10)
	private String machineType;
	
	/** 设备分类 STD_ZB_SBFL **/
	@Column(name = "MACHINE_CODE", unique = false, nullable = true, length = 10)
	private String machineCode;
	
	/** 型号/规格 **/
	@Column(name = "SPEC_MODEL", unique = false, nullable = true, length = 100)
	private String specModel;
	
	/** 品牌/生产厂商 **/
	@Column(name = "VEHICLE_BRAND", unique = false, nullable = true, length = 100)
	private String vehicleBrand;
	
	/** 出厂日期或报关日期 **/
	@Column(name = "FACTORY_DATE", unique = false, nullable = true, length = 10)
	private String factoryDate;
	
	/** 设计使用到期日期 **/
	@Column(name = "MATURE_DATE", unique = false, nullable = true, length = 10)
	private String matureDate;
	
	/** 是否有产品合格证 **/
	@Column(name = "IND_ELIGIBLE_CERTI", unique = false, nullable = true, length = 10)
	private String indEligibleCerti;
	
	/** 发票编号 **/
	@Column(name = "INVOICE_NO", unique = false, nullable = true, length = 40)
	private String invoiceNo;
	
	/** 发票日期 **/
	@Column(name = "INVOICE_DATE", unique = false, nullable = true, length = 10)
	private String invoiceDate;
	
	/** 发票金额（元） **/
	@Column(name = "BUY_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal buyPrice;
	
	/** 设备名称 **/
	@Column(name = "EQUIP_NAME", unique = false, nullable = true, length = 100)
	private String equipName;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 押品使用情况 **/
	@Column(name = "GUAR_UTIL_CASE", unique = false, nullable = true, length = 10)
	private String guarUtilCase;
	
	/** 设备数量 **/
	@Column(name = "EQUIP_QNT", unique = false, nullable = true, length = 10)
	private String equipQnt;
	
	/** 设备存放地址 **/
	@Column(name = "EQUIP_DEPO", unique = false, nullable = true, length = 100)
	private String equipDepo;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param equipNo
	 */
	public void setEquipNo(String equipNo) {
		this.equipNo = equipNo;
	}
	
    /**
     * @return equipNo
     */
	public String getEquipNo() {
		return this.equipNo;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param machineType
	 */
	public void setMachineType(String machineType) {
		this.machineType = machineType;
	}
	
    /**
     * @return machineType
     */
	public String getMachineType() {
		return this.machineType;
	}
	
	/**
	 * @param machineCode
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	
    /**
     * @return machineCode
     */
	public String getMachineCode() {
		return this.machineCode;
	}
	
	/**
	 * @param specModel
	 */
	public void setSpecModel(String specModel) {
		this.specModel = specModel;
	}
	
    /**
     * @return specModel
     */
	public String getSpecModel() {
		return this.specModel;
	}
	
	/**
	 * @param vehicleBrand
	 */
	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}
	
    /**
     * @return vehicleBrand
     */
	public String getVehicleBrand() {
		return this.vehicleBrand;
	}
	
	/**
	 * @param factoryDate
	 */
	public void setFactoryDate(String factoryDate) {
		this.factoryDate = factoryDate;
	}
	
    /**
     * @return factoryDate
     */
	public String getFactoryDate() {
		return this.factoryDate;
	}
	
	/**
	 * @param matureDate
	 */
	public void setMatureDate(String matureDate) {
		this.matureDate = matureDate;
	}
	
    /**
     * @return matureDate
     */
	public String getMatureDate() {
		return this.matureDate;
	}
	
	/**
	 * @param indEligibleCerti
	 */
	public void setIndEligibleCerti(String indEligibleCerti) {
		this.indEligibleCerti = indEligibleCerti;
	}
	
    /**
     * @return indEligibleCerti
     */
	public String getIndEligibleCerti() {
		return this.indEligibleCerti;
	}
	
	/**
	 * @param invoiceNo
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	
    /**
     * @return invoiceNo
     */
	public String getInvoiceNo() {
		return this.invoiceNo;
	}
	
	/**
	 * @param invoiceDate
	 */
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
    /**
     * @return invoiceDate
     */
	public String getInvoiceDate() {
		return this.invoiceDate;
	}
	
	/**
	 * @param buyPrice
	 */
	public void setBuyPrice(java.math.BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	
    /**
     * @return buyPrice
     */
	public java.math.BigDecimal getBuyPrice() {
		return this.buyPrice;
	}
	
	/**
	 * @param equipName
	 */
	public void setEquipName(String equipName) {
		this.equipName = equipName;
	}
	
    /**
     * @return equipName
     */
	public String getEquipName() {
		return this.equipName;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase;
	}
	
    /**
     * @return guarUtilCase
     */
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param equipQnt
	 */
	public void setEquipQnt(String equipQnt) {
		this.equipQnt = equipQnt;
	}
	
    /**
     * @return equipQnt
     */
	public String getEquipQnt() {
		return this.equipQnt;
	}
	
	/**
	 * @param equipDepo
	 */
	public void setEquipDepo(String equipDepo) {
		this.equipDepo = equipDepo;
	}
	
    /**
     * @return equipDepo
     */
	public String getEquipDepo() {
		return this.equipDepo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}