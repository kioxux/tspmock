/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.LmtApprSubPrd;
import cn.com.yusys.yusp.domain.LmtReplySubPrd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyChgSubPrd;
import cn.com.yusys.yusp.repository.mapper.LmtReplyChgSubPrdMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgSubPrdService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-24 14:33:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyChgSubPrdService {

    @Autowired
    private LmtReplyChgSubPrdMapper lmtReplyChgSubPrdMapper;
    @Autowired
    private LmtReplySubService lmtReplySubService;
    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyChgSubPrd selectByPrimaryKey(String pkId) {
        return lmtReplyChgSubPrdMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyChgSubPrd> selectAll(QueryModel model) {
        List<LmtReplyChgSubPrd> records = (List<LmtReplyChgSubPrd>) lmtReplyChgSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyChgSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyChgSubPrd> list = lmtReplyChgSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyChgSubPrd record) {
        return lmtReplyChgSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyChgSubPrd record) {
        return lmtReplyChgSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReplyChgSubPrd record) {
        return lmtReplyChgSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyChgSubPrd record) {
        return lmtReplyChgSubPrdMapper.updateSelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyChgSubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyChgSubPrdMapper.deleteByIds(ids);
    }

/**
   * @方法名称：insertList
   * @方法描述：批复分项产品变更插入集合
   * @参数与返回说明：
   * @算法描述：
   * @创建人：zhangming12
   * @创建时间：2021-04-26 下午 10:05
   * @修改记录：修改时间   修改人员  修改原因
   */
    public int insertList(List<LmtReplyChgSubPrd> lmtReplyChgSubPrdList){
        return lmtReplyChgSubPrdMapper.insertList(lmtReplyChgSubPrdList);
    }

    /**
     * @方法名称: selectLmtReplyChgSubPrdByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyChgSubPrd> queryLmtReplyChgSubPrdByParams(HashMap<String, String> subPrdQueryMap) {
        return lmtReplyChgSubPrdMapper.queryLmtReplyChgSubPrdByParams(subPrdQueryMap);
    }


    public void updateLmtReplyChgSubPrdOprDeleteByChgSub(String subSerno) {
        HashMap queryMap = new HashMap();
        queryMap.put("subSerno",subSerno);
        List<LmtReplyChgSubPrd> listSubPrd = lmtReplyChgSubPrdMapper.queryLmtReplyChgSubPrdByParams(queryMap);
    }
}
