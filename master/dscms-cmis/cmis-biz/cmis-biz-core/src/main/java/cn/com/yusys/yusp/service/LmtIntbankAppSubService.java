/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtIntbankAccSub;
import cn.com.yusys.yusp.domain.LmtIntbankAppSub;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankAppSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Supplier;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 16:09:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankAppSubService {

    @Autowired
    private LmtIntbankAppSubMapper lmtIntbankAppSubMapper;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankAppSub selectByPrimaryKey(String pkId) {
        return lmtIntbankAppSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankAppSub> selectAll(QueryModel model) {
        List<LmtIntbankAppSub> records = (List<LmtIntbankAppSub>) lmtIntbankAppSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankAppSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAppSub> list = lmtIntbankAppSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankAppSub record) {
        int count = lmtIntbankAppSubMapper.insert(record);
        String serno = record.getSerno();
        lmtIntbankAppService.updateLmtAmt(serno);
        return count;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankAppSub record) {
        return lmtIntbankAppSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankAppSub record) {
        int count = lmtIntbankAppSubMapper.updateByPrimaryKey(record);
        String serno = record.getSerno();
        lmtIntbankAppService.updateLmtAmt(serno);
        return count;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankAppSub record) {
        return lmtIntbankAppSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankAppSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankAppSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtIntbankAppSubMapper.updateByParams(delMap);
    }
    /**
     * @方法名称: createAppLmtSubChaApp
     * @方法描述: 创建额度调整分项申请数据
     * @参数与返回说明:【apprSerno】批复台账编号
     * @算法描述: 无
     */
    public List<LmtIntbankAppSub> createLmtIntbankAppSub(String replySerno) {
        List<LmtIntbankAppSub> lmtIntbankAppSubs = new ArrayList<>() ;
        //根据批复编号和操作类型查找批复编号下所有的批复信息
        QueryModel model = new QueryModel();
        model.addCondition("replySerno",replySerno);
        model.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtIntbankAccSub> LmtIntbankAccSubList= lmtIntbankAccSubService.selectAll(model);
        //生成主键，获取流水号
        HashMap<String, String> param = new HashMap<>();
        Supplier<LmtIntbankAppSub> supplier = LmtIntbankAppSub::new ;
        //判断获取的分项记录不为空，遍历处理
        if(LmtIntbankAccSubList.size()>0){
            for(LmtIntbankAccSub lmtIntbankAccSub:LmtIntbankAccSubList){
                //获取初始化对象
                LmtIntbankAppSub lmtIntbankAppSub = supplier.get() ;
                org.springframework.beans.BeanUtils.copyProperties(lmtIntbankAccSub, lmtIntbankAppSub);
                //生成主键，获取流水号
                String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, param);
                //生成流水号
                String subSerno = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SUB_SERNO, param);
                //分项申请编号
                lmtIntbankAppSub.setSubSerno(subSerno);
                //主键
                lmtIntbankAppSub.setPkId(pkValue);
                lmtIntbankAppSub.setTerm(lmtIntbankAccSub.getLmtTerm());
                lmtIntbankAppSub.setOrigiLmtAccSubNo(lmtIntbankAccSub.getAccSubNo());
                lmtIntbankAppSub.setOrigiLmtAccSubAmt(lmtIntbankAccSub.getLmtAmt());
                lmtIntbankAppSub.setOrigiLmtAccSubTerm(lmtIntbankAccSub.getLmtTerm());
                //TODO 待补充

                lmtIntbankAppSubs.add(lmtIntbankAppSub) ;
            }
        }
        return lmtIntbankAppSubs ;
    }

    /**
     * 添加授信从表数据
     * @param serno
     * @param info
     * @param isUpdate
     */
    public int addOrUpdateLmtIntbankAppSub(String serno, Map info, boolean isUpdate) {
        LmtIntbankAppSub subApp = null;
        //更新Bean临时存储原PkId(info中可能会覆盖pkid)
        String origiPkId = null;
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashMap<>());
        String subSernoValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.LMT_SERNO, new HashMap<>());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        if (isUpdate){
            subApp = selectBySerno(serno);
            if (subApp == null){
                return 0;
            }else{
                origiPkId = subApp.getPkId();
            }
        }else{
            subApp = new LmtIntbankAppSub();
        }
        BeanUtils.mapToBean(info,subApp);
        subApp.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
        subApp.setUpdId(SessionUtils.getUserInformation().getLoginCode());
        subApp.setUpdDate(openDay);
        subApp.setUpdateTime(new Date());
        if (isUpdate){
            subApp.setPkId(origiPkId);
            return lmtIntbankAppSubMapper.updateByPrimaryKey(subApp);
        }else{
            subApp.setPkId(pkValue);
            subApp.setSerno(serno);
            subApp.setSubSerno(subSernoValue);
            subApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            subApp.setInputBrId(SessionUtils.getUserInformation().getOrg().getCode());
            subApp.setInputId(SessionUtils.getUserInformation().getLoginCode());
            subApp.setInputDate(openDay);
            subApp.setUpdDate(openDay);
            subApp.setUpdId(SessionUtils.getUserInformation().getLoginCode());
            subApp.setUpdBrId(SessionUtils.getUserInformation().getOrg().getCode());
            subApp.setCreateTime(new Date());
            subApp.setUpdateTime(new Date());
            return lmtIntbankAppSubMapper.insert(subApp);
        }
    }

    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public LmtIntbankAppSub selectBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtIntbankAppSub> lmtIntbankAppSubs = selectByModel(queryModel);
        if (lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
            return lmtIntbankAppSubs.get(0);
        }
        return null;
    }


    /**
     * 根据申请流水号，获取同业授信分项申请信息列表
     * @param serno
     * @return
     */
    public List<LmtIntbankAppSub> selectSubListBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtIntbankAppSub> lmtIntbankAppSubs = selectByModel(queryModel);
        if (lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
            return lmtIntbankAppSubs;
        }
        return null;
    }

    /**
     * 根据申请流水号，获取同业授信分项申请信息列表
     * @param serno
     * @return
     */
    public List<LmtIntbankAppSub> selectUnOptTypeBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtIntbankAppSub> lmtIntbankAppSubs = selectByModel(queryModel);
        if (lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
            return lmtIntbankAppSubs;
        }
        return null;
    }

    /**
     * 判断改分项是否存在该分项申请中
     * @param model
     * @return
     */
    public boolean checkSubLimitPrdId(QueryModel model){
        List<LmtIntbankAppSub> lmtIntbankAppSubs = selectByModel(model);
        if (lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
            return false;
        }
        return true;
    }
}
