package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 资金业务授信申报审批流程申请流程
 * 作者：李召星
 */
@Service
public class TYSX02BizService implements ClientBizInterface {

    private final Logger logger = LoggerFactory.getLogger(TYSX02BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService ;

    @Autowired
    private LmtIntbankApprService lmtIntbankApprService ;

    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService ;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String bizType = resultInstanceDto.getBizType();
        logger.info("后业务处理类型{}", currentOpType);
        try {
            //资金同业单笔授信流程处理
            if(CmisBizConstants.STD_ZB_ZJTY_TY011.equals(bizType) || CmisBizConstants.STD_ZB_ZJTY_TY012.equals(bizType)
                    || CmisBizConstants.STD_ZB_ZJTY_TY013.equals(bizType)){
                lmtSigInvestAppFlow(resultInstanceDto) ;
            }
            //同业授信单笔业务处理
            if(CmisBizConstants.STD_ZB_ZJTY_TY004.equals(bizType) || CmisBizConstants.STD_ZB_ZJTY_TY005.equals(bizType)
                    || CmisBizConstants.STD_ZB_ZJTY_TY006.equals(bizType)){
                lmtIntbankAppFlow(resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("资金业务授信申报审批流程申请流程审批后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.TYSX02.equals(flowCode);
    }

    /**
     *  TY011单笔投资授信新增（总行部门）、TY012单笔投资授信变更（总行部门）、TY013单笔投资授信复议（总行部门） 该流程
     *  资金业务授信申报审批流程（总行发起-除债券池）流程审批通过 （调用流程 TYSX02）
     * @param resultInstanceDto
     * @throws Exception
     */
    public void lmtSigInvestAppFlow (ResultInstanceDto resultInstanceDto) throws Exception{
        String currentOpType = resultInstanceDto.getCurrentOpType();
        //业务流水号
        String grtSerno = resultInstanceDto.getBizId();
        //流程参数，防止在页面的param 中
        Map<String, Object> paramMap = resultInstanceDto.getParam();
        //审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        //主键，通过主键查寻申请深吸
        String pkId = (String)paramMap.get("BizPkId") ;
        //获取授信申请信息
        LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.selectBySerno(grtSerno);

        //加载路由条件 add by zhangjw 20210721
        Map<String,Object> varParam = investPut2VarParam(resultInstanceDto,grtSerno);
        logger.info("资金业务授信申报审批流程申请启用【{}】，路由条件加载-----：【{}】", grtSerno,varParam);

        if (OpType.STRAT.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程发起操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.RUN.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程提交操作，流程参数【{}】", grtSerno, resultInstanceDto);
            //判断当前节点是否出具审查报告、授信批复节点
            String issueReportType = "";
            //获取下一审批节点信息
            String nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
            if(resultInstanceDto.getNextNodeInfos()!=null && resultInstanceDto.getNextNodeInfos().size()>0){
                nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
            }
            //判断下一审批节点是否包含出具审查报告、出具批复页面
            if(CmisBizConstants.TYSX02_01.contains(nextNodeId+",")){
                issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_01;
            }else if(CmisBizConstants.TYSX02_03.contains(nextNodeId+",")){
                issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_03;
            }
            String currNodeId = resultInstanceDto.getCurrentNodeId();
            if (CmisBizConstants.TYSX02_START.equals(currNodeId)) {
                lmtSigInvestAppService.handleAfterStart(lmtSigInvestApp, issueReportType, varParam);
            }else{
                String cur_next_id = currNodeId+";"+nextNodeId;
                lmtSigInvestApprService.generateSigInvestApprService(lmtSigInvestApp,issueReportType, cur_next_id,currentUserId,currentOrgId);
            }
        } else if (OpType.JUMP.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程跳转操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.END.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程结束操作，流程参数【{}】", grtSerno, resultInstanceDto);
            // 针对流程到办结节点，进行以下处理
            lmtSigInvestAppService.handleAfterEnd(lmtSigInvestApp,CmisBizConstants.APPLY_STATE_PASS,resultInstanceDto);
        } else if (OpType.RETURN_BACK.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程退回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                extractedSigInvestBack(resultInstanceDto, currentUserId, currentOrgId, lmtSigInvestApp);
            }
        } else if (OpType.CALL_BACK.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程打回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                extractedSigInvestBack(resultInstanceDto, currentUserId, currentOrgId, lmtSigInvestApp);
            }
        } else if (OpType.TACK_BACK.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程拿回操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.REFUSE.equals(currentOpType)) {
            // 否决改变标志 审批中 111-> 审批不通过 998
            logger.info("资金业务授信申报审批流程申请启用【{}】，流程否决操作，流程参数【{}】", grtSerno, resultInstanceDto);
            lmtSigInvestAppService.handleAfterEnd(lmtSigInvestApp,CmisBizConstants.APPLY_STATE_REFUSE,resultInstanceDto);
        } else {
            logger.warn("资金业务授信申报审批流程申请" + grtSerno + "未知操作:" + resultInstanceDto);
        }
    }

    /**
     * @作者:lizx
     * @方法名称: extractedBack
     * @方法描述:  打回或退回操作处理
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 14:41
     * @param resultInstanceDto: 
     * @param currentUserId: 
     * @param currentOrgId: 
     * @param lmtSigInvestApp: 
     * @return: void
     * @算法描述: 无
    */
    private void extractedSigInvestBack(ResultInstanceDto resultInstanceDto, String currentUserId, String currentOrgId, LmtSigInvestApp lmtSigInvestApp) throws Exception {
        logger.warn("资金业务授信申报审批流程申请{}，打回或退回操作", lmtSigInvestApp.getSerno());

        //针对流程到办结节点，进行以下处理
        lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
        lmtSigInvestAppService.update(lmtSigInvestApp);

        //推送首页提醒事项 add by zhangjw 20210630
        lmtSigInvestAppService.sendWbMsgNotice(lmtSigInvestApp,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId(),"退回");
    }

    /**
     *  TY004同业授信新增、TY005同业授信变更、TY006同业授信复议（调用流程 TYSX02）
     *  资金业务授信申报审批流程（总行发起-除债券池）流程审批通过
     * @param resultInstanceDto
     * @throws Exception
     */
    public void lmtIntbankAppFlow (ResultInstanceDto resultInstanceDto) throws Exception{
        String currentOpType = resultInstanceDto.getCurrentOpType();
        //业务流水号
        String grtSerno = resultInstanceDto.getBizId();
        //流程参数，防止在页面的param 中
        Map<String, Object> paramMap = resultInstanceDto.getParam();
        //审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        //主键，通过主键查询
        String pkId = (String)paramMap.get("BizPkId") ;
        //获取授信申请信息
        LmtIntbankApp lmtIntbankApp = lmtIntbankAppService.selectBySerno(grtSerno);

        //加载路由条件
        Map<String,Object> varParam = put2VarParam(resultInstanceDto, grtSerno);
        logger.info("同业授信申报审批流程申请启用【{}】，路由条件加载-----：【{}】", grtSerno,varParam);

        if (OpType.STRAT.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用【{}】，流程发起操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.RUN.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程提交操作，流程参数【{}】", grtSerno, resultInstanceDto);
            //判断当前节点是否出具审查报告、授信批复节点
            String issueReportType = "";
            //获取下一审批节点信息
            String nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
            if(resultInstanceDto.getNextNodeInfos()!=null && resultInstanceDto.getNextNodeInfos().size()>0){
                nextNodeId = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId();
            }
            //判断下一审批节点是否包含出具审查报告、出具批复页面
            if(CmisBizConstants.TYSX02_01.contains(nextNodeId+",")){
                issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_01;
            }else if(CmisBizConstants.TYSX02_03.contains(nextNodeId+",")){
                issueReportType = CmisBizConstants.STD_ISSUE_REPORT_TYPE_03;
            }

            //如果当前节点是发起节点，则从申请表copy数据到审批表；如果当前节点非发起节点，则copy审批表中最新的数据至审批表
            String currNodeId = resultInstanceDto.getCurrentNodeId();
            if(CmisBizConstants.TYSX02_START.equals(currNodeId)){
                //从申请表生成对应的审批表信息
                lmtIntbankAppService.handleAfterStart(lmtIntbankApp, issueReportType, varParam);
            }else{
                String cur_next_id = currNodeId+";"+nextNodeId;

                //TODO 如果当前节点非发起节点，则copy审批表中最新的数据至审批表-根据create_time字段倒序取最新，将 issueReportType 出具报告类型一并带入
                lmtIntbankApprService.generateLmtIntbankApprService(lmtIntbankApp,issueReportType,cur_next_id);
            }

        } else if (OpType.JUMP.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程跳转操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.END.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程结束操作，流程参数【{}】", grtSerno, resultInstanceDto);
            // 针对流程到办结节点，进行以下处理
            lmtIntbankAppService.handleAfterEnd(lmtIntbankApp,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_PASS,resultInstanceDto);
        } else if (OpType.RETURN_BACK.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程退回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            //流程打回至发起人，则更新申请表数据为打回状态，发送首页提醒
            if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                extractedIntbankApp(resultInstanceDto, currentUserId, currentOrgId, lmtIntbankApp);
            }
        } else if (OpType.CALL_BACK.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程打回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            //流程打回至发起人，则更新申请表数据为打回状态，发送首页提醒
            if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                extractedIntbankApp(resultInstanceDto, currentUserId, currentOrgId, lmtIntbankApp);
            }
        } else if (OpType.TACK_BACK.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程拿回操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
        } else if (OpType.REFUSE.equals(currentOpType)) {
            // 否决改变标志 审批中 111-> 审批不通过 998
            logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程否决操作，流程参数【{}】", grtSerno, resultInstanceDto);
            lmtIntbankAppService.handleAfterEnd(lmtIntbankApp,currentUserId,currentOrgId,CmisBizConstants.APPLY_STATE_REFUSE,resultInstanceDto);
        } else {
            logger.warn("同业授信申报审批流程申请启用授信申报审批流程申请" + grtSerno + "未知操作:" + resultInstanceDto);
        }
    }
    /**
     * @作者:lizx
     * @方法名称: extractedIntbankApp
     * @方法描述:  同业客户申请执行打回或那会操作
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/7/1 14:48
     * @param resultInstanceDto: 
     * @param currentUserId: 
     * @param currentOrgId: 
     * @param lmtIntbankApp: 
     * @return: void
     * @算法描述: 无
    */
    private void extractedIntbankApp(ResultInstanceDto resultInstanceDto, String currentUserId, String currentOrgId, LmtIntbankApp lmtIntbankApp) throws Exception {
        logger.info("同业授信申报审批流程申请启用授信申报审批流程申请启用【{}】，流程打回或退回操作", lmtIntbankApp.getSerno());
            //针对流程到办结节点，进行以下处理
        lmtIntbankApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
        lmtIntbankAppService.update(lmtIntbankApp);

        //推送首页提醒事项 add by zhangjw 20210630
        lmtIntbankAppService.sendWbMsgNotice(lmtIntbankApp,CmisBizConstants.STD_WB_NOTICE_TYPE_1,
                resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId(),"打回");
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数-同业授信
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: zhangjw 20210719
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtIntbankAppService.getRouterMapResult(resultInstanceDto,serno);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        return params;
    }

    /**
     * @方法名称: investPut2VarParam
     * @方法描述: 重置流程参数-资金业务
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: zhangjw 20210719
     * @创建时间: 2021-07-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> investPut2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtSigInvestAppService.getRouterMapResult(resultInstanceDto,serno);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        return params;
    }
}
