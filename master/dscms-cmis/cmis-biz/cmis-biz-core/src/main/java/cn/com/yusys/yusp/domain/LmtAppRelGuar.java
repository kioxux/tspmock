/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelGuar
 * @类描述: lmt_app_rel_guar数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 19:06:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_rel_guar")
public class LmtAppRelGuar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 增信人客户编号 **/
	@Column(name = "GRT_CUS_ID", unique = false, nullable = true, length = 40)
	private String grtCusId;
	
	/** 增信人客户名称 **/
	@Column(name = "GRT_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grtCusName;
	
	/** 客户大类 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String cusCatalog;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 担保类型 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 评估价值 **/
	@Column(name = "ASS_EVA_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assEvaAmt;
	
	/** 与发行人关系 **/
	@Column(name = "ISSUE_REL", unique = false, nullable = true, length = 200)
	private String issueRel;
	
	/** 是否为合格担保 **/
	@Column(name = "IS_VALID_GUAR", unique = false, nullable = true, length = 5)
	private String isValidGuar;
	
	/** 是否发行人的关联方 **/
	@Column(name = "IS_ISSUE_RELATED_PARTY", unique = false, nullable = true, length = 5)
	private String isIssueRelatedParty;
	
	/** 担保/增信方式 **/
	@Column(name = "GRT_MODE", unique = false, nullable = true, length = 20)
	private String grtMode;
	
	/** 抵质押物名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 200)
	private String pldimnMemo;
	
	/** 抵押物类型 **/
	@Column(name = "PLDIMN_TYPE", unique = false, nullable = true, length = 10)
	private String pldimnType;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param grtCusId
	 */
	public void setGrtCusId(String grtCusId) {
		this.grtCusId = grtCusId;
	}
	
    /**
     * @return grtCusId
     */
	public String getGrtCusId() {
		return this.grtCusId;
	}
	
	/**
	 * @param grtCusName
	 */
	public void setGrtCusName(String grtCusName) {
		this.grtCusName = grtCusName;
	}
	
    /**
     * @return grtCusName
     */
	public String getGrtCusName() {
		return this.grtCusName;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param assEvaAmt
	 */
	public void setAssEvaAmt(java.math.BigDecimal assEvaAmt) {
		this.assEvaAmt = assEvaAmt;
	}
	
    /**
     * @return assEvaAmt
     */
	public java.math.BigDecimal getAssEvaAmt() {
		return this.assEvaAmt;
	}
	
	/**
	 * @param issueRel
	 */
	public void setIssueRel(String issueRel) {
		this.issueRel = issueRel;
	}
	
    /**
     * @return issueRel
     */
	public String getIssueRel() {
		return this.issueRel;
	}
	
	/**
	 * @param isValidGuar
	 */
	public void setIsValidGuar(String isValidGuar) {
		this.isValidGuar = isValidGuar;
	}
	
    /**
     * @return isValidGuar
     */
	public String getIsValidGuar() {
		return this.isValidGuar;
	}
	
	/**
	 * @param isIssueRelatedParty
	 */
	public void setIsIssueRelatedParty(String isIssueRelatedParty) {
		this.isIssueRelatedParty = isIssueRelatedParty;
	}
	
    /**
     * @return isIssueRelatedParty
     */
	public String getIsIssueRelatedParty() {
		return this.isIssueRelatedParty;
	}
	
	/**
	 * @param grtMode
	 */
	public void setGrtMode(String grtMode) {
		this.grtMode = grtMode;
	}
	
    /**
     * @return grtMode
     */
	public String getGrtMode() {
		return this.grtMode;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param pldimnType
	 */
	public void setPldimnType(String pldimnType) {
		this.pldimnType = pldimnType;
	}
	
    /**
     * @return pldimnType
     */
	public String getPldimnType() {
		return this.pldimnType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}