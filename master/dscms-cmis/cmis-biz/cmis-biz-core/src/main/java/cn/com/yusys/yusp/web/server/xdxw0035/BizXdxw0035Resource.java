package cn.com.yusys.yusp.web.server.xdxw0035;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0035.req.Xdxw0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.PldList;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.Xdxw0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0035.Xdxw0035Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * 接口处理类:根据流水号查询无还本续贷抵押信息
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDXW0035:根据流水号查询无还本续贷抵押信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0035Resource.class);

    @Autowired
    private Xdxw0035Service xdxw0035Service;
    /**
     * 交易码：xdxw0035
     * 交易描述：根据流水号查询无还本续贷抵押信息
     *
     * @param xdxw0035DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("根据流水号查询无还本续贷抵押信息")
    @PostMapping("/xdxw0035")
    protected @ResponseBody
    ResultDto<Xdxw0035DataRespDto> xdxw0035(@Validated @RequestBody Xdxw0035DataReqDto xdxw0035DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035DataReqDto));
        Xdxw0035DataRespDto  xdxw0035DataRespDto  = new Xdxw0035DataRespDto();// 响应Dto:根据流水号查询无还本续贷抵押信息
        ResultDto<Xdxw0035DataRespDto>xdxw0035DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0035DataReqDto获取业务值进行业务逻辑处理
            String indgtSerno = xdxw0035DataReqDto.getIndgtSerno();//调查流水号
            // 调用xdxw0035Service层开始
            List<PldList> pldList = xdxw0035Service.getPldListByindgtSerno(indgtSerno);
            if (CollectionUtils.isEmpty(pldList)) {
                PldList xdxw0035RespPldList = new PldList();
                xdxw0035RespPldList.setPldRate(new BigDecimal(0L));// 抵押率
                xdxw0035RespPldList.setPawnType(StringUtils.EMPTY);// 抵押类型
                xdxw0035RespPldList.setAddress(StringUtils.EMPTY);// 地址
                xdxw0035RespPldList.setOwnershipId(StringUtils.EMPTY);// 所有权人
                xdxw0035RespPldList.setPawnName(StringUtils.EMPTY);// 抵押物名称
                pldList =  Arrays.asList(xdxw0035RespPldList);
            }
            xdxw0035DataRespDto.setPldList(pldList);
            // 封装xdxw0035DataResultDto中正确的返回码和返回信息
            xdxw0035DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0035DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, e.getMessage());
            // 封装xdxw0035DataResultDto中异常返回码和返回信息
            xdxw0035DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0035DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0035DataRespDto到xdxw0035DataResultDto中
        xdxw0035DataResultDto.setData(xdxw0035DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035DataResultDto));
        return xdxw0035DataResultDto;
    }
}