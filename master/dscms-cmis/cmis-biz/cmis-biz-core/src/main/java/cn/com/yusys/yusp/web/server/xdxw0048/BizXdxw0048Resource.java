package cn.com.yusys.yusp.web.server.xdxw0048;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0048.req.Xdxw0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0048.resp.Xdxw0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0048.Xdxw0048Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:增享贷2.0风控模型B生成批复
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0048:增享贷2.0风控模型B生成批复")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0048Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0048Resource.class);

    @Autowired
    private Xdxw0048Service xdxw0048Service;

    /**
     * 交易码：xdxw0048
     * 交易描述：增享贷2.0风控模型B生成批复
     *
     * @param xdxw0048DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("增享贷2.0风控模型B生成批复")
    @PostMapping("/xdxw0048")
    protected @ResponseBody
    ResultDto<Xdxw0048DataRespDto> xdxw0048(@Validated @RequestBody Xdxw0048DataReqDto xdxw0048DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataReqDto));
        Xdxw0048DataRespDto xdxw0048DataRespDto = new Xdxw0048DataRespDto();// 响应Dto:增享贷2.0风控模型B生成批复
        ResultDto<Xdxw0048DataRespDto> xdxw0048DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0048DataReqDto获取业务值进行业务逻辑处理
            String task_id = xdxw0048DataReqDto.getTask_id();//办理流水号
            String model_result = xdxw0048DataReqDto.getModel_result();//模型结果
            String model_appr_time = xdxw0048DataReqDto.getModel_appr_time();//模型审批时间
            String apply_no = xdxw0048DataReqDto.getApply_no();//业务唯一编号
            String opinion = xdxw0048DataReqDto.getOpinion();//模型意见
            String cus_id = xdxw0048DataReqDto.getCus_id();//客户号
            String loan_term = xdxw0048DataReqDto.getLoan_term();//贷款期限
            BigDecimal final_amount = xdxw0048DataReqDto.getFinal_amount();//最终金额
            BigDecimal final_rate = xdxw0048DataReqDto.getFinal_rate();//最终利率
            if (StringUtils.isEmpty(task_id)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(model_result)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(model_appr_time)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(apply_no)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(opinion)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(cus_id)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (StringUtils.isEmpty(loan_term)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (0 == final_amount.compareTo(BigDecimal.ZERO)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            if (0 == final_rate.compareTo(BigDecimal.ZERO)) {
                xdxw0048DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0048DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0048DataResultDto;
            }
            xdxw0048DataRespDto = xdxw0048Service.xdxw0048(xdxw0048DataReqDto);
            xdxw0048DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0048DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0048DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0048DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, e.getMessage());
            // 封装xdxw0048DataResultDto中异常返回码和返回信息
            xdxw0048DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0048DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0048DataRespDto到xdxw0048DataResultDto中
        xdxw0048DataResultDto.setData(xdxw0048DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataRespDto));
        return xdxw0048DataResultDto;
    }
}
