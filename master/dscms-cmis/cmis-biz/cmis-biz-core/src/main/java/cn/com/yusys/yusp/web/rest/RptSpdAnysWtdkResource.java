/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtRecheckDetail;
import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.domain.RptBasicInfoShar;
import cn.com.yusys.yusp.service.RptBasicInfoSharService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysWtdk;
import cn.com.yusys.yusp.service.RptSpdAnysWtdkService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWtdkResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 15:53:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyswtdk")
public class RptSpdAnysWtdkResource {
    @Autowired
    private RptSpdAnysWtdkService rptSpdAnysWtdkService;

    @Autowired
    private RptBasicInfoSharService rptBasicInfoSharService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysWtdk>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysWtdk> list = rptSpdAnysWtdkService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysWtdk>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysWtdk>> index(QueryModel queryModel) {
        List<RptSpdAnysWtdk> list = rptSpdAnysWtdkService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysWtdk>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysWtdk> show(@PathVariable("serno") String serno) {
        RptSpdAnysWtdk rptSpdAnysWtdk = rptSpdAnysWtdkService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysWtdk>(rptSpdAnysWtdk);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysWtdk> create(@RequestBody RptSpdAnysWtdk rptSpdAnysWtdk) throws URISyntaxException {
        rptSpdAnysWtdkService.insert(rptSpdAnysWtdk);
        return new ResultDto<RptSpdAnysWtdk>(rptSpdAnysWtdk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysWtdk rptSpdAnysWtdk) throws URISyntaxException {
        int result = rptSpdAnysWtdkService.update(rptSpdAnysWtdk);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysWtdkService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysWtdkService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysWtdk> selectBySerno(@RequestBody String serno) {
        RptSpdAnysWtdk rptSpdAnysWtdk = rptSpdAnysWtdkService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysWtdk);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysWtdk> save(@RequestBody RptSpdAnysWtdk rptSpdAnysWtdk) throws URISyntaxException {
        RptSpdAnysWtdk rptSpdAnysWtdk1 = rptSpdAnysWtdkService.selectByPrimaryKey(rptSpdAnysWtdk.getSerno());
        if(rptSpdAnysWtdk1!=null){
            rptSpdAnysWtdkService.update(rptSpdAnysWtdk);
        }else{
            rptSpdAnysWtdkService.insert(rptSpdAnysWtdk);
        }
        return ResultDto.success(rptSpdAnysWtdk);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询股东情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryStockHolder")
    protected ResultDto<List<RptBasicInfoShar>> queryStockHolder(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptBasicInfoShar> rptBasicInfoShars = rptBasicInfoSharService.queryStockHolder(serno);
        return  ResultDto.success(rptBasicInfoShars);
    }
}
