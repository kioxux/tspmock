package cn.com.yusys.yusp.web.server.cmisbiz0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.req.CmisBiz0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.resp.CmisBiz0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.LmtAppService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:单一客户授信复审
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "CMISBIZ0001:单一客户授信复审")
@RestController
@RequestMapping("/api/biz4inner")
public class CmisBiz0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0001Resource.class);
    @Autowired
    private LmtAppService lmtAppService;

    /**
     * 交易码：cmisbiz0001
     * 交易描述：单一客户授信复审
     *
     * @param cmisbiz0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("单一客户授信复审")
    @PostMapping("/cmisbiz0001")
    protected @ResponseBody
    ResultDto<CmisBiz0001RespDto> cmisbiz0001(@Validated @RequestBody CmisBiz0001ReqDto cmisbiz0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisbiz0001ReqDto));
        CmisBiz0001RespDto cmisbiz0001RespDto = new CmisBiz0001RespDto();// 响应Dto:单一客户授信复审
        ResultDto<CmisBiz0001RespDto> cmisbiz0001ResultDto = new ResultDto<>();
        try {
            Map rtnData = new HashMap();
            LmtApp lmtApp = new LmtApp();
            BeanUtils.copyProperties(cmisbiz0001ReqDto, lmtApp);

            // 从cmisbiz0001ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisbiz0001ReqDto));
            rtnData = lmtAppService.onReview(lmtApp);
            cmisbiz0001RespDto.setSerno((String) rtnData.get("serno"));
            cmisbiz0001RespDto.setRtnCode((String) rtnData.get("rtnCode"));
            cmisbiz0001RespDto.setRtnMsg((String) rtnData.get("rtnMsg"));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisbiz0001RespDto));
            // 封装cmisbiz0001ResultDto中正确的返回码和返回信息
            cmisbiz0001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbiz0001ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, e.getMessage());
            // 封装cmisbiz0001ResultDto中异常返回码和返回信息
            cmisbiz0001ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbiz0001ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbiz0001RespDto到cmisbiz0001ResultDto中
        cmisbiz0001ResultDto.setData(cmisbiz0001RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0001.key, DscmsEnum.TRADE_CODE_CMISBIZ0001.value, JSON.toJSONString(cmisbiz0001ResultDto));
        return cmisbiz0001ResultDto;
    }
}