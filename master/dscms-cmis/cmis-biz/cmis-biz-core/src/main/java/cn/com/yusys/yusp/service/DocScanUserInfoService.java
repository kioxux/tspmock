/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.DocScanUserInfo;
import cn.com.yusys.yusp.repository.mapper.DocScanUserInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocScanUserInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocScanUserInfoService {

    @Autowired
    private DocScanUserInfoMapper docScanUserInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocScanUserInfo selectByPrimaryKey(String dsuiSerno) {
        return docScanUserInfoMapper.selectByPrimaryKey(dsuiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocScanUserInfo> selectAll(QueryModel model) {
        List<DocScanUserInfo> records = (List<DocScanUserInfo>) docScanUserInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocScanUserInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocScanUserInfo> list = docScanUserInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocScanUserInfo record) {
        return docScanUserInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocScanUserInfo record) {
        return docScanUserInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocScanUserInfo record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docScanUserInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocScanUserInfo record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docScanUserInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dsuiSerno) {
        return docScanUserInfoMapper.deleteByPrimaryKey(dsuiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docScanUserInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据档案归档流水号删除扫描人信息
     *
     * @author jijian_yx
     * @date 2021/6/21 10:12
     **/
    public int deleteByDocSerno(String docSerno) {
        return docScanUserInfoMapper.deleteByDocSerno(docSerno);
    }

    /**
     * 根据档案流水号获取扫描人
     *
     * @author jijian_yx
     * @date 2021/6/21 16:02
     **/
    public List<DocScanUserInfo> queryByDocSerno(QueryModel queryModel) {
        return docScanUserInfoMapper.selectByModel(queryModel);
    }

    /**
     * 根据档案流水号获取扫描人
     *
     * @author jijian_yx
     * @date 2021/6/21 16:02
     **/
    public List<DocScanUserInfo> queryByDocNo(QueryModel queryModel) {
        return docScanUserInfoMapper.selectByModel(queryModel);
    }
}
