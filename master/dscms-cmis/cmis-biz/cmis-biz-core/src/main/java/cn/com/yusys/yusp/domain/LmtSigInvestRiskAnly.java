/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRiskAnly
 * @类描述: lmt_sig_invest_risk_anly数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 20:57:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_risk_anly")
public class LmtSigInvestRiskAnly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 主要风险分析 **/
	@Column(name = "MAIN_RISK_ANALY", unique = false, nullable = true, length = 65535)
	private String mainRiskAnaly;
	
	/** 委托人/原始权益人调查结论 **/
	@Column(name = "TRUE_INDGT_RESULT", unique = false, nullable = true, length = 65535)
	private String trueIndgtResult;
	
	/** 授信可行性分析 **/
	@Column(name = "LMT_FEASY_ANALY", unique = false, nullable = true, length = 65535)
	private String lmtFeasyAnaly;
	
	/** 授信必要性分析 **/
	@Column(name = "LMT_NECEY_ANALY", unique = false, nullable = true, length = 65535)
	private String lmtNeceyAnaly;
	
	/** 政策风险 **/
	@Column(name = "POLICY_RISK", unique = false, nullable = true, length = 65535)
	private String policyRisk;
	
	/** 行业风险 **/
	@Column(name = "TRADE_RISK", unique = false, nullable = true, length = 65535)
	private String tradeRisk;
	
	/** 市场风险 **/
	@Column(name = "MARKET_RISK", unique = false, nullable = true, length = 65535)
	private String marketRisk;
	
	/** 经营管理风险 **/
	@Column(name = "OPER_MANA_RISK", unique = false, nullable = true, length = 65535)
	private String operManaRisk;
	
	/** 财务风险 **/
	@Column(name = "FINA_RISK", unique = false, nullable = true, length = 65535)
	private String finaRisk;
	
	/** 财务风险图片路径 **/
	@Column(name = "FINA_RISK_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String finaRiskPicturePath;
	
	/** 互保、循环担保、关联担保的风险 **/
	@Column(name = "GRT_RISK", unique = false, nullable = true, length = 65535)
	private String grtRisk;
	
	/** 过度授信风险 **/
	@Column(name = "OVER_LMT_RISK", unique = false, nullable = true, length = 65535)
	private String overLmtRisk;
	
	/** 预警客户风险说明 **/
	@Column(name = "ALT_CUS_RISK_DESC", unique = false, nullable = true, length = 65535)
	private String altCusRiskDesc;
	
	/** 其他风险说明 **/
	@Column(name = "OTHER_RISK_DESC", unique = false, nullable = true, length = 65535)
	private String otherRiskDesc;
	
	/** 综合业务风险分析 **/
	@Column(name = "INTE_BIZ_RISK_ANALY", unique = false, nullable = true, length = 65535)
	private String inteBizRiskAnaly;
	
	/** 风险防控措施说明 **/
	@Column(name = "RISK_PACM_DESC", unique = false, nullable = true, length = 65535)
	private String riskPacmDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainRiskAnaly
	 */
	public void setMainRiskAnaly(String mainRiskAnaly) {
		this.mainRiskAnaly = mainRiskAnaly;
	}
	
    /**
     * @return mainRiskAnaly
     */
	public String getMainRiskAnaly() {
		return this.mainRiskAnaly;
	}
	
	/**
	 * @param trueIndgtResult
	 */
	public void setTrueIndgtResult(String trueIndgtResult) {
		this.trueIndgtResult = trueIndgtResult;
	}
	
    /**
     * @return trueIndgtResult
     */
	public String getTrueIndgtResult() {
		return this.trueIndgtResult;
	}
	
	/**
	 * @param lmtFeasyAnaly
	 */
	public void setLmtFeasyAnaly(String lmtFeasyAnaly) {
		this.lmtFeasyAnaly = lmtFeasyAnaly;
	}
	
    /**
     * @return lmtFeasyAnaly
     */
	public String getLmtFeasyAnaly() {
		return this.lmtFeasyAnaly;
	}
	
	/**
	 * @param lmtNeceyAnaly
	 */
	public void setLmtNeceyAnaly(String lmtNeceyAnaly) {
		this.lmtNeceyAnaly = lmtNeceyAnaly;
	}
	
    /**
     * @return lmtNeceyAnaly
     */
	public String getLmtNeceyAnaly() {
		return this.lmtNeceyAnaly;
	}
	
	/**
	 * @param policyRisk
	 */
	public void setPolicyRisk(String policyRisk) {
		this.policyRisk = policyRisk;
	}
	
    /**
     * @return policyRisk
     */
	public String getPolicyRisk() {
		return this.policyRisk;
	}
	
	/**
	 * @param tradeRisk
	 */
	public void setTradeRisk(String tradeRisk) {
		this.tradeRisk = tradeRisk;
	}
	
    /**
     * @return tradeRisk
     */
	public String getTradeRisk() {
		return this.tradeRisk;
	}
	
	/**
	 * @param marketRisk
	 */
	public void setMarketRisk(String marketRisk) {
		this.marketRisk = marketRisk;
	}
	
    /**
     * @return marketRisk
     */
	public String getMarketRisk() {
		return this.marketRisk;
	}
	
	/**
	 * @param operManaRisk
	 */
	public void setOperManaRisk(String operManaRisk) {
		this.operManaRisk = operManaRisk;
	}
	
    /**
     * @return operManaRisk
     */
	public String getOperManaRisk() {
		return this.operManaRisk;
	}
	
	/**
	 * @param finaRisk
	 */
	public void setFinaRisk(String finaRisk) {
		this.finaRisk = finaRisk;
	}
	
    /**
     * @return finaRisk
     */
	public String getFinaRisk() {
		return this.finaRisk;
	}
	
	/**
	 * @param finaRiskPicturePath
	 */
	public void setFinaRiskPicturePath(String finaRiskPicturePath) {
		this.finaRiskPicturePath = finaRiskPicturePath;
	}
	
    /**
     * @return finaRiskPicturePath
     */
	public String getFinaRiskPicturePath() {
		return this.finaRiskPicturePath;
	}
	
	/**
	 * @param grtRisk
	 */
	public void setGrtRisk(String grtRisk) {
		this.grtRisk = grtRisk;
	}
	
    /**
     * @return grtRisk
     */
	public String getGrtRisk() {
		return this.grtRisk;
	}
	
	/**
	 * @param overLmtRisk
	 */
	public void setOverLmtRisk(String overLmtRisk) {
		this.overLmtRisk = overLmtRisk;
	}
	
    /**
     * @return overLmtRisk
     */
	public String getOverLmtRisk() {
		return this.overLmtRisk;
	}
	
	/**
	 * @param altCusRiskDesc
	 */
	public void setAltCusRiskDesc(String altCusRiskDesc) {
		this.altCusRiskDesc = altCusRiskDesc;
	}
	
    /**
     * @return altCusRiskDesc
     */
	public String getAltCusRiskDesc() {
		return this.altCusRiskDesc;
	}
	
	/**
	 * @param otherRiskDesc
	 */
	public void setOtherRiskDesc(String otherRiskDesc) {
		this.otherRiskDesc = otherRiskDesc;
	}
	
    /**
     * @return otherRiskDesc
     */
	public String getOtherRiskDesc() {
		return this.otherRiskDesc;
	}
	
	/**
	 * @param inteBizRiskAnaly
	 */
	public void setInteBizRiskAnaly(String inteBizRiskAnaly) {
		this.inteBizRiskAnaly = inteBizRiskAnaly;
	}
	
    /**
     * @return inteBizRiskAnaly
     */
	public String getInteBizRiskAnaly() {
		return this.inteBizRiskAnaly;
	}
	
	/**
	 * @param riskPacmDesc
	 */
	public void setRiskPacmDesc(String riskPacmDesc) {
		this.riskPacmDesc = riskPacmDesc;
	}
	
    /**
     * @return riskPacmDesc
     */
	public String getRiskPacmDesc() {
		return this.riskPacmDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}