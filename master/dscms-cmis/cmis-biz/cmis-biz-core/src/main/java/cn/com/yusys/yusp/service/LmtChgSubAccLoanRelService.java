/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtChgSubAccLoanRel;
import cn.com.yusys.yusp.repository.mapper.LmtChgSubAccLoanRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtChgSubAccLoanRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:17:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtChgSubAccLoanRelService {
    private static final Logger log = LoggerFactory.getLogger(LmtChgSubAccLoanRelService.class);

    @Autowired
    private LmtChgSubAccLoanRelMapper lmtChgSubAccLoanRelMapper;

    @Autowired
    private AccLoanService accLoanService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtChgSubAccLoanRel selectByPrimaryKey(String pkId) {
        return lmtChgSubAccLoanRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtChgSubAccLoanRel> selectAll(QueryModel model) {
        List<LmtChgSubAccLoanRel> records = (List<LmtChgSubAccLoanRel>) lmtChgSubAccLoanRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtChgSubAccLoanRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtChgSubAccLoanRel> list = lmtChgSubAccLoanRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtChgSubAccLoanRel record) {
        return lmtChgSubAccLoanRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtChgSubAccLoanRel record) {
        return lmtChgSubAccLoanRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtChgSubAccLoanRel record) {
        return lmtChgSubAccLoanRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtChgSubAccLoanRel record) {
        return lmtChgSubAccLoanRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtChgSubAccLoanRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtChgSubAccLoanRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccLoan> queryBySubPrdSerno(String subPrdSerno) {
        Map map = new HashMap<>();
        map.put("subPrdSerno",subPrdSerno);
        List<LmtChgSubAccLoanRel> list = lmtChgSubAccLoanRelMapper.queryByParams(map);
        List<AccLoan> accLoanList = new ArrayList<>();
        AccLoan accLoan = new AccLoan();
        LmtChgSubAccLoanRel lmtChgSubAccLoanRel = new LmtChgSubAccLoanRel();
        if(list != null && list.size() > 0){
            for(int i = 0; i<list.size();i++){
                lmtChgSubAccLoanRel = list.get(i);
                accLoan = accLoanService.selectByBillNo(lmtChgSubAccLoanRel.getBillNo());
                accLoanList.add(accLoan);
            }
        }
        PageHelper.clearPage();
        return accLoanList;
    }

    /**
     * @方法名称: deleteByBillNo
     * @方法描述: 根据借据编号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByBillNo(String billNo) {
        return lmtChgSubAccLoanRelMapper.deleteByBillNo(billNo);
    }

    public Map saveLmtChgSubAccLoanRel(Map<String,String> map){
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtChgSubAccLoanRel> list = lmtChgSubAccLoanRelMapper.queryByParams(map);
        if(list != null && list.size()>0){
            rtnCode = EcbEnum.ECB010026.key;
            rtnMsg = EcbEnum.ECB010026.value;
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            return rtnData;
        }
        LmtChgSubAccLoanRel lmtChgSubAccLoanRel = new LmtChgSubAccLoanRel();
        lmtChgSubAccLoanRel.setSubPrdSerno(map.get("subPrdSerno"));
        lmtChgSubAccLoanRel.setBillNo(map.get("billNo"));
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            rtnCode = EcbEnum.ECB010004.key;
            rtnMsg = EcbEnum.ECB010004.value;
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            return rtnData;
        } else {
            lmtChgSubAccLoanRel.setOprType(CmisCommonConstants.ADD_OPR);
            lmtChgSubAccLoanRel.setInputId(userInfo.getLoginCode());
            lmtChgSubAccLoanRel.setInputBrId(userInfo.getOrg().getCode());
            lmtChgSubAccLoanRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgSubAccLoanRel.setUpdId(userInfo.getLoginCode());
            lmtChgSubAccLoanRel.setUpdBrId(userInfo.getOrg().getCode());
            lmtChgSubAccLoanRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtChgSubAccLoanRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            lmtChgSubAccLoanRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        }
        int count = lmtChgSubAccLoanRelMapper.insert(lmtChgSubAccLoanRel);
        if(count != 1){
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",还本借据保存失败！");
        }
        rtnData.put("rtnCode", rtnCode);
        rtnData.put("rtnMsg", rtnMsg);
        return rtnData;
    }

    /**
     * @方法名称: updateByBillNo
     * @方法描述: 根据借据编号修改
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByBillNo(Map map) {
        int updateCount = 0;
        updateCount = lmtChgSubAccLoanRelMapper.updateByBillNo(map);
        if(updateCount<=0){
            log.error("保存授信变更偿还借据关联信息【{}】失败！",(String) map.get("billNo"));
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        // 给本笔借据打上借新还旧标志
        AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo((String) map.get("billNo"));
        accLoan.setRefinancingFlag((String) map.get("refinancingFlag"));
        updateCount = accLoanService.updateSelective(accLoan);
        if(updateCount<=0){
            log.error("保存借据信息【{}】失败！",(String) map.get("billNo"));
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        return updateCount;
    }
}
