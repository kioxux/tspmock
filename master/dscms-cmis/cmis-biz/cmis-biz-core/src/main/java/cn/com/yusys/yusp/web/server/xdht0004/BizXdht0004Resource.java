package cn.com.yusys.yusp.web.server.xdht0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0004.req.Xdht0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0004.resp.Xdht0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0004.Xdht0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:合同信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0004:合同信息列表查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0004Resource.class);

    @Autowired
    private Xdht0004Service xdht0004Service;
    /**
     * 交易码：xdht0004
     * 交易描述：合同信息列表查询
     *
     * @param xdht0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息列表查询")
    @PostMapping("/xdht0004")
    protected @ResponseBody
    ResultDto<Xdht0004DataRespDto> xdht0004(@Validated @RequestBody Xdht0004DataReqDto xdht0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataReqDto));
        Xdht0004DataRespDto xdht0004DataRespDto = new Xdht0004DataRespDto();// 响应Dto:合同信息列表查询
        ResultDto<Xdht0004DataRespDto> xdht0004DataResultDto = new ResultDto<>();
        // 从xdht0004DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataReqDto));
            xdht0004DataRespDto = xdht0004Service.getXdht0004(xdht0004DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataRespDto));
            // 封装xdht0004DataResultDto中正确的返回码和返回信息
            xdht0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            // 封装xdht0004DataResultDto中异常返回码和返回信息
            xdht0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0004DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            // 封装xdht0004DataResultDto中异常返回码和返回信息
            xdht0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0004DataRespDto到xdht0004DataResultDto中
        xdht0004DataResultDto.setData(xdht0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataResultDto));
        return xdht0004DataResultDto;
    }

}
