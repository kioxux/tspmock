/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJqdCustomer
 * @类描述: rpt_spd_anys_jqd_customer数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:30:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_jqd_customer")
public class RptSpdAnysJqdCustomer extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 主要供应商 **/
	@Column(name = "MAIN_SUPPLIER", unique = false, nullable = true, length = 40)
	private String mainSupplier;
	
	/** 主要供应商采购比例 **/
	@Column(name = "SUPPLIER_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal supplierPrec;
	
	/** 主要供应商结账周期 **/
	@Column(name = "SUPPLIER_TERM", unique = false, nullable = true, length = 5)
	private String supplierTerm;
	
	/** 主要销售商 **/
	@Column(name = "MAIN_SELLER", unique = false, nullable = true, length = 40)
	private String mainSeller;
	
	/** 主要销售商采购比例 **/
	@Column(name = "SELLER_PREC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sellerPrec;
	
	/** 主要销售商结账周期 **/
	@Column(name = "SELLER_TERM", unique = false, nullable = true, length = 5)
	private String sellerTerm;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainSupplier
	 */
	public void setMainSupplier(String mainSupplier) {
		this.mainSupplier = mainSupplier;
	}
	
    /**
     * @return mainSupplier
     */
	public String getMainSupplier() {
		return this.mainSupplier;
	}
	
	/**
	 * @param supplierPrec
	 */
	public void setSupplierPrec(java.math.BigDecimal supplierPrec) {
		this.supplierPrec = supplierPrec;
	}
	
    /**
     * @return supplierPrec
     */
	public java.math.BigDecimal getSupplierPrec() {
		return this.supplierPrec;
	}
	
	/**
	 * @param supplierTerm
	 */
	public void setSupplierTerm(String supplierTerm) {
		this.supplierTerm = supplierTerm;
	}
	
    /**
     * @return supplierTerm
     */
	public String getSupplierTerm() {
		return this.supplierTerm;
	}
	
	/**
	 * @param mainSeller
	 */
	public void setMainSeller(String mainSeller) {
		this.mainSeller = mainSeller;
	}
	
    /**
     * @return mainSeller
     */
	public String getMainSeller() {
		return this.mainSeller;
	}
	
	/**
	 * @param sellerPrec
	 */
	public void setSellerPrec(java.math.BigDecimal sellerPrec) {
		this.sellerPrec = sellerPrec;
	}
	
    /**
     * @return sellerPrec
     */
	public java.math.BigDecimal getSellerPrec() {
		return this.sellerPrec;
	}
	
	/**
	 * @param sellerTerm
	 */
	public void setSellerTerm(String sellerTerm) {
		this.sellerTerm = sellerTerm;
	}
	
    /**
     * @return sellerTerm
     */
	public String getSellerTerm() {
		return this.sellerTerm;
	}


}