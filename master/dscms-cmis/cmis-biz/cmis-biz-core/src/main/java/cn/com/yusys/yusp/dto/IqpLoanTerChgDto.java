package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 贷款投向调整dto
 */
public class IqpLoanTerChgDto implements Serializable{
	private static final long serialVersionUID = 1L;
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;

	/** 合同编号**/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;

	/** 中文合同编号 **/
	@Column(name = "CN_CONT_NO", unique = false, nullable = false, length = 40)
	private String cnContNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 币种 STD_ZB_CUR_TYP币 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;

	/** 合同起始日期**/
	@Column(name = "CONT_START_DATE", unique = false, nullable = true, length = 10)
	private String contStartDate;

	/** 合同到期日期 **/
	@Column(name = "CONT_END_DATE", unique = false, nullable = true, length = 10)
	private String contEndDate;

	/** 合同签订日期 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 10)
	private String signDate;

	/** 原贷款投向 **/
	@Column(name = "OLD_LOAN_TER", unique = false, nullable = true, length = 100)
	private String oldLoanTer;

	/** 原工业转型升级标识 STD_ZB_YES_NO **/
	@Column(name = "OLD_COM_UP_INDTIFY", unique = false, nullable = true, length = 5)
	private String oldComUpIndtify;

	/** 原战略新兴产业类型 STD_ZB_SEIS_TYP **/
	@Column(name = "OLD_STRATEGY_NEW_TYPE", unique = false, nullable = true, length = 5)
	private String oldStrategyNewType;

	/** 行业投向 STD_GB_4754-2011 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 100)
	private String loanTer;

	/** 工业转型升级标识 STD_ZB_YES_NO **/
	@Column(name = "COM_UP_INDTIFY", unique = false, nullable = true, length = 5)
	private String comUpIndtify;

	/** 战略新兴产业类型 STD_ZB_SEIS_TYP**/
	@Column(name = "STRATEGY_NEW_TYPE", unique = false, nullable = true, length = 5)
	private String stratrgyNewType;

	/** 是否文化产业 STD_ZB_YES_NO **/
	@Column(name = "IS_CUL_ESTATE", unique = false, nullable = true, length = 5)
	private String isCulEstate;

	/** 调整原因 **/
	@Column(name = "CHANGE_RESN", unique = false, nullable = true, length = 250)
	private String changeResn;

	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 登记机构**/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 申请状态 STD_ZB_APP_ST**/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String optType;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getIqpSerno() {
		return iqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getCnContNo() {
		return cnContNo;
	}

	public void setCnContNo(String cnContNo) {
		this.cnContNo = cnContNo;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getCurType() {
		return curType;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getContStartDate() {
		return contStartDate;
	}

	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}

	public String getContEndDate() {
		return contEndDate;
	}

	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getOldLoanTer() {
		return oldLoanTer;
	}

	public void setOldLoanTer(String oldLoanTer) {
		this.oldLoanTer = oldLoanTer;
	}

	public String getOldComUpIndtify() {
		return oldComUpIndtify;
	}

	public void setOldComUpIndtify(String oldComUpIndtify) {
		this.oldComUpIndtify = oldComUpIndtify;
	}

	public String getOldStrategyNewType() {
		return oldStrategyNewType;
	}

	public void setOldStrategyNewType(String oldStrategyNewType) {
		this.oldStrategyNewType = oldStrategyNewType;
	}

	public String getLoanTer() {
		return loanTer;
	}

	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}

	public String getComUpIndtify() {
		return comUpIndtify;
	}

	public void setComUpIndtify(String comUpIndtify) {
		this.comUpIndtify = comUpIndtify;
	}

	public String getStratrgyNewType() {
		return stratrgyNewType;
	}

	public void setStratrgyNewType(String stratrgyNewType) {
		this.stratrgyNewType = stratrgyNewType;
	}

	public String getIsCulEstate() {
		return isCulEstate;
	}

	public void setIsCulEstate(String isCulEstate) {
		this.isCulEstate = isCulEstate;
	}

	public String getChangeResn() {
		return changeResn;
	}

	public void setChangeResn(String changeResn) {
		this.changeResn = changeResn;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getOptType() {
		return optType;
	}

	public void setOptType(String optType) {
		this.optType = optType;
	}
}