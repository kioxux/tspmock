package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.service.LmtAppSubPrdService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author 马顺
 * @version 1.0.0
 * @date 2021/7/23 9:54 下午
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class MapUtils {
    private static final Logger log = LoggerFactory.getLogger(MapUtils.class);
    /**
     * @描述 检查map中是否存在空的元素
     * @author 马顺
     * @version 1.0.0
     * @date 2021/7/23 9:54 下午
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static Boolean checkExistsEmptyEnum(Map<String, Object> map){
        log.info("检查map中是否存在空的元素,输入参数为" + JSON.toJSONString(map));
        Set<Map.Entry<String, Object>> set = map.entrySet();
        Iterator<Map.Entry<String, Object>> it = set.iterator();
        while (it.hasNext()){
            Map.Entry<String, Object> en = it.next();
            if(!(en.getValue() instanceof Map)){
                if(null == en.getValue() || "".equals(en.getValue())){
                    log.info("检查map中是否存在空的元素,存在空元素:" + en.getKey());
                    return true;
                }
            }
        }
        return false;
    }

}
