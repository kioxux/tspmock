/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOtherAppNoAddGuar
 * @类描述: rpt_other_app_no_add_guar数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 21:23:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_other_app_no_add_guar")
public class RptOtherAppNoAddGuar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 项目类型 **/
	@Column(name = "PRO_TYPE", unique = false, nullable = true, length = 40)
	private String proType;
	
	/** 具体免追加理由说明 **/
	@Column(name = "NO_ADD_REASON", unique = false, nullable = true, length = 65535)
	private String noAddReason;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType;
	}
	
    /**
     * @return proType
     */
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param noAddReason
	 */
	public void setNoAddReason(String noAddReason) {
		this.noAddReason = noAddReason;
	}
	
    /**
     * @return noAddReason
     */
	public String getNoAddReason() {
		return this.noAddReason;
	}


}