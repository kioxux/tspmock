/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrHighAmtAgrCont;
import cn.com.yusys.yusp.domain.CtrHighAmtAgrContDto;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.AccCommonDto;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.service.CtrHighAmtAgrContService;
import cn.com.yusys.yusp.service.GrtGuarContService;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrHighAmtAgrContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-12 14:34:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "最高额授信协议")
@RequestMapping("/api/ctrhighamtagrcont")
public class CtrHighAmtAgrContResource {
    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrHighAmtAgrCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.selectAll(queryModel);
        return new ResultDto<List<CtrHighAmtAgrCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrHighAmtAgrCont>> index(QueryModel queryModel) {
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.selectByModel(queryModel);
        return new ResultDto<List<CtrHighAmtAgrCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrHighAmtAgrCont> show(@PathVariable("pkId") String pkId) {
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrHighAmtAgrCont>(ctrHighAmtAgrCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrHighAmtAgrCont> create(@RequestBody CtrHighAmtAgrCont ctrHighAmtAgrCont) throws URISyntaxException {
        ctrHighAmtAgrContService.insert(ctrHighAmtAgrCont);
        return new ResultDto<CtrHighAmtAgrCont>(ctrHighAmtAgrCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrHighAmtAgrCont ctrHighAmtAgrCont) throws URISyntaxException {
        int result = ctrHighAmtAgrContService.update(ctrHighAmtAgrCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrHighAmtAgrContService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrHighAmtAgrContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:查询反显，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("数据反显")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("contNo",(String)map.get("contNo"));
        ResultDto<Object> resultDto = new ResultDto<Object>();
        CtrHighAmtAgrCont temp = new CtrHighAmtAgrCont();
        CtrHighAmtAgrCont studyDemo = ctrHighAmtAgrContService.queryCtrHighAmtAgrContDataByParams(queryData);
        if (studyDemo != null) {
            resultDto.setCode(0);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(300);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:contSign
     * @函数描述:合同签订，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("最高额授信协议签订")
    @PostMapping("/contsign")
    public ResultDto<Map> contSign(@RequestBody Map map) {
        Map rtnData = ctrHighAmtAgrContService.contSign(map);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:contLogout
     * @函数描述:合同签订，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("最高额授信协议注销")
    @PostMapping("/contlogout")
    public ResultDto<Map> contLogout(@RequestBody Map map) throws Exception {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("contNo",(String)map.get("contNo"));
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.queryCtrHighAmtAgrContDataByParams(queryData);
        Map rtnData = ctrHighAmtAgrContService.contLogout(ctrHighAmtAgrCont);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:contReSign
     * @函数描述:普通贷款合同重签
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/contReSign")
    protected ResultDto<Map> contReSign(@RequestBody Map params) throws Exception {
        Map rtnData= ctrHighAmtAgrContService.contReSign(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrHighAmtAgrCont>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.toSignlist(queryModel);
        ResultDto<List<CtrHighAmtAgrCont>> resultDto = new ResultDto<List<CtrHighAmtAgrCont>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrHighAmtAgrCont>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<CtrHighAmtAgrCont>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<CtrHighAmtAgrCont>>(list);
    }

    /**
     * 项下业务信息查询方法
     * @param
     * @return
     */
    @ApiOperation("项下业务信息列表")
    @PostMapping("/queryacc")
    protected ResultDto<List<AccCommonDto>> getAccInfo(@RequestBody Map map) throws URISyntaxException {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("contNo","");
        List<AccCommonDto> result = ctrHighAmtAgrContService.queryAccInfo(queryMap);
        return new ResultDto<List<AccCommonDto>>(result);
    }

    @ApiOperation("最高额授信协议申请关联合同主表")
    @PostMapping("/selectctrloan")
    protected ResultDto<List<CtrLoanContDto>> getSubInfo(@RequestBody QueryModel queryModel) throws URISyntaxException {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        queryModel.getCondition().put("lmtAccNo", "");
        List<CtrLoanContDto> result = ctrHighAmtAgrContService.selectCtrCont(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrLoanContDto>>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待签定列表")
    @PostMapping("/tosignlistforzhcx")
    protected ResultDto<List<CtrHighAmtAgrContDto>> toSignZhcxlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.toSignlist(queryModel);
        ResultDto<List<CtrHighAmtAgrCont>> resultDto = new ResultDto<List<CtrHighAmtAgrCont>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        List<CtrHighAmtAgrContDto> list1 = new ArrayList<CtrHighAmtAgrContDto>();
        for(int i =0; i <list.size();i++){
           BigDecimal agrAmt =  list.get(i).getAgrAmt();
           BigDecimal agrContHighAvlAmt = list.get(i).getAgrContHighAvlAmt();
            BigDecimal useAmt;
           if(agrContHighAvlAmt == null){
               useAmt = agrAmt;
           }else{
               useAmt = agrAmt.subtract(agrContHighAvlAmt);
           }

            CtrHighAmtAgrContDto ctrHighAmtAgrContDto = new CtrHighAmtAgrContDto();
            BeanUtils.beanCopy(list.get(i),ctrHighAmtAgrContDto);
           ctrHighAmtAgrContDto.setUseAmt(useAmt);
            list1.add(ctrHighAmtAgrContDto);
        }
        return new ResultDto<List<CtrHighAmtAgrContDto>>(list1);
    }

    /**
     * @函数名称:selectByQuerymodel
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据入参查询合同数据")
    @PostMapping("/selectbyquerymodel")
    protected ResultDto<List<CtrHighAmtAgrCont>> selectByQuerymodel(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContService.selectByQuerymodel(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<CtrHighAmtAgrCont>>(list);
    }

    /**
     * @函数名称:queryCtrHighAmtAgrContDataBySerno
     * @函数描述:根据流水号查询合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据流水号查询合同信息")
    @PostMapping("/queryctrhighamtagrcontdatabyserno")
    protected ResultDto<CtrHighAmtAgrCont> queryCtrHighAmtAgrContDataBySerno(@RequestBody String serno) {
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectByIqpSerno(serno);
        return new ResultDto<CtrHighAmtAgrCont>(ctrHighAmtAgrCont);
    }
}
