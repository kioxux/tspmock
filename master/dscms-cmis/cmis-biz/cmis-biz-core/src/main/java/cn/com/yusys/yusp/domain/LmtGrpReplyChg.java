/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyChg
 * @类描述: lmt_grp_reply_chg数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-21 14:02:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_reply_chg")
public class LmtGrpReplyChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 集团申请流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;

	/** 集团批复流水号 **/
	@Column(name = "GRP_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String grpReplySerno;

	/** 批复生效日期 **/
	@Column(name = "REPLY_INURE_DATE", unique = false, nullable = true, length = 10)
	private String replyInureDate;

	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;

	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;

	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;

	/** 终审机构 **/
	@Column(name = "FINAL_APPR_BR_ID", unique = false, nullable = true, length = 20)
	private String finalApprBrId;

	/** 批复结论 **/
	@Column(name = "REPLY_RESULT", unique = false, nullable = true, length = 5)
	private String replyResult;

	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;

	/** 用信审核方式 **/
	@Column(name = "LOAN_APPR_MODE", unique = false, nullable = true, length = 5)
	private String loanApprMode;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;

	/** 贷后管理要求 **/
	@Column(name = "PSP_MANA_NEED", unique = false, nullable = true, length = 4000)
	private String pspManaNeed;

	/** 变更事项说明 **/
	@Column(name = "REPLY_CHG_CONTENT_MEMO", unique = false, nullable = true, length = 4000)
	private String replyChgContentMemo;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	/**
	 * @return grpSerno
	 */
	public String getGrpSerno() {
		return this.grpSerno;
	}

	/**
	 * @param grpReplySerno
	 */
	public void setGrpReplySerno(String grpReplySerno) {
		this.grpReplySerno = grpReplySerno;
	}

	/**
	 * @return grpReplySerno
	 */
	public String getGrpReplySerno() {
		return this.grpReplySerno;
	}

	/**
	 * @param replyInureDate
	 */
	public void setReplyInureDate(String replyInureDate) {
		this.replyInureDate = replyInureDate;
	}

	/**
	 * @return replyInureDate
	 */
	public String getReplyInureDate() {
		return this.replyInureDate;
	}

	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}

	/**
	 * @return grpCusId
	 */
	public String getGrpCusId() {
		return this.grpCusId;
	}

	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}

	/**
	 * @return grpCusName
	 */
	public String getGrpCusName() {
		return this.grpCusName;
	}

	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}

	/**
	 * @return apprMode
	 */
	public String getApprMode() {
		return this.apprMode;
	}

	/**
	 * @param finalApprBrId
	 */
	public void setFinalApprBrId(String finalApprBrId) {
		this.finalApprBrId = finalApprBrId;
	}

	/**
	 * @return finalApprBrId
	 */
	public String getFinalApprBrId() {
		return this.finalApprBrId;
	}

	/**
	 * @param replyResult
	 */
	public void setReplyResult(String replyResult) {
		this.replyResult = replyResult;
	}

	/**
	 * @return replyResult
	 */
	public String getReplyResult() {
		return this.replyResult;
	}

	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}

	/**
	 * @return replyStatus
	 */
	public String getReplyStatus() {
		return this.replyStatus;
	}

	/**
	 * @param loanApprMode
	 */
	public void setLoanApprMode(String loanApprMode) {
		this.loanApprMode = loanApprMode;
	}

	/**
	 * @return loanApprMode
	 */
	public String getLoanApprMode() {
		return this.loanApprMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	/**
	 * @return lmtTerm
	 */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}

	/**
	 * @param pspManaNeed
	 */
	public void setPspManaNeed(String pspManaNeed) {
		this.pspManaNeed = pspManaNeed;
	}

	/**
	 * @return pspManaNeed
	 */
	public String getPspManaNeed() {
		return this.pspManaNeed;
	}

	/**
	 * @param replyChgContentMemo
	 */
	public void setReplyChgContentMemo(String replyChgContentMemo) {
		this.replyChgContentMemo = replyChgContentMemo;
	}

	/**
	 * @return replyChgContentMemo
	 */
	public String getReplyChgContentMemo() {
		return this.replyChgContentMemo;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

}