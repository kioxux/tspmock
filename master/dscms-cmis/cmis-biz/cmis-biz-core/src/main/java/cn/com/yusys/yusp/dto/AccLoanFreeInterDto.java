package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanFreeInter
 * @类描述: acc_loan_free_inter数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-08 22:06:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AccLoanFreeInterDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 是否免息 **/
	private String isFreeInter;
	
	/** 免息天数 **/
	private Integer freeInterDay;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param isFreeInter
	 */
	public void setIsFreeInter(String isFreeInter) {
		this.isFreeInter = isFreeInter == null ? null : isFreeInter.trim();
	}
	
    /**
     * @return IsFreeInter
     */	
	public String getIsFreeInter() {
		return this.isFreeInter;
	}
	
	/**
	 * @param freeInterDay
	 */
	public void setFreeInterDay(Integer freeInterDay) {
		this.freeInterDay = freeInterDay;
	}
	
    /**
     * @return FreeInterDay
     */	
	public Integer getFreeInterDay() {
		return this.freeInterDay;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}