package cn.com.yusys.yusp.web.server.xdqt0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0009.req.Xdqt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.resp.Xdqt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0009.Xdqt0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDQT0009:微业贷信贷文件处理通知）")
@RestController
@RequestMapping("/api/bizqt4bsp")
public class BizXdqt0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdqt0009Resource.class);

    @Autowired
	private Xdqt0009Service xdqt0009Service;
    /**
     * 交易码：xdqt0009
     * 交易描述：微业贷信贷文件处理通知
     *
     * @param xdqt0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("微业贷信贷文件处理通知")
    @PostMapping("/xdqt0009")
    protected @ResponseBody
    ResultDto<Xdqt0009DataRespDto> xdqt0009(@Validated @RequestBody Xdqt0009DataReqDto xdqt0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009DataReqDto));
        Xdqt0009DataRespDto xdqt0009DataRespDto = new Xdqt0009DataRespDto();// 响应Dto:微业贷信贷文件处理通知
        ResultDto<Xdqt0009DataRespDto> xdqt0009DataResultDto = new ResultDto<>();

        try {
            // 从xdqt0009DataReqDto获取业务值进行业务逻辑处理
			xdqt0009DataRespDto = xdqt0009Service.xdqt0009(xdqt0009DataReqDto);

            // 封装xdqt0009DataResultDto中正确的返回码和返回信息
            xdqt0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, e.getMessage());
            // 封装xdqt0009DataResultDto中异常返回码和返回信息
            xdqt0009DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0009DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0009DataRespDto到xdqt0009DataResultDto中
        xdqt0009DataResultDto.setData(xdqt0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009DataResultDto));
        return xdqt0009DataResultDto;
    }
}
