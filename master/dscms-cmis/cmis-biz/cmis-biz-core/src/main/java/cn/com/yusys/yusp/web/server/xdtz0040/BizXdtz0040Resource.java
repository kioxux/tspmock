package cn.com.yusys.yusp.web.server.xdtz0040;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0040.req.Xdtz0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.resp.Xdtz0040DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.xdtz0040.Xdtz0040Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:申请人在本行当前逾期贷款数量
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0040:申请人在本行当前逾期贷款数量")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0040Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0040Resource.class);


    @Autowired
    private Xdtz0040Service xdtz0040Service;

    @Autowired
    private CommonService commonService;

    /**
     * 交易码：xdtz0040
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param xdtz0040DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0040:申请人在本行当前逾期贷款数量")
    @PostMapping("/xdtz0040")
    protected @ResponseBody
    ResultDto<Xdtz0040DataRespDto> xdtz0040(@Validated @RequestBody Xdtz0040DataReqDto xdtz0040DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, JSON.toJSONString(xdtz0040DataReqDto));
        Xdtz0040DataRespDto xdtz0040DataRespDto = new Xdtz0040DataRespDto();// 响应Dto:申请人在本行当前逾期贷款数量
        ResultDto<Xdtz0040DataRespDto> xdtz0040DataResultDto = new ResultDto<>();
        // 从xdtz0040DataReqDto获取业务值进行业务逻辑处理
        String certNo = xdtz0040DataReqDto.getCertNo();//证件号码
        try {
            if (StringUtil.isEmpty(certNo)) {
                xdtz0040DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0040DataResultDto.setMessage("证件号码【certNo】不能为空！");
                return xdtz0040DataResultDto;
            }
            List<String> cusIds = commonService.getCusBaseByCertCode(certNo);
            Map queryMap = new HashMap();
            queryMap.put("cusIds", cusIds);
            //查询条件
            if (CollectionUtils.isEmpty(cusIds)) {//20211115确认客户信息不存在返回0，不要返回错误提示
                xdtz0040DataRespDto.setTotalNum(0);
            } else {
                // 调用XXXXXService层开始
                xdtz0040DataRespDto = xdtz0040Service.getContNumByCertCont(queryMap);
                // 封装xdtz0040DataResultDto中正确的返回码和返回信息
            }
            xdtz0040DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0040DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, e.getMessage());
            // 封装xdtz0040DataResultDto中异常返回码和返回信息
            xdtz0040DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0040DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0040DataRespDto到xdtz0040DataResultDto中
        xdtz0040DataResultDto.setData(xdtz0040DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, JSON.toJSONString(xdtz0040DataResultDto));
        return xdtz0040DataResultDto;
    }
}