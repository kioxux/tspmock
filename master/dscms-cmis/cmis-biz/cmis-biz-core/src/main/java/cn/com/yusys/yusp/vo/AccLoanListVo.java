package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "贷款台账实时报表", fileType = ExcelCsv.ExportFileType.XLS)
public class AccLoanListVo {

    /*
   借据编号
    */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    产品名称
     */
    @ExcelField(title = "产品名称", viewLength = 20)
    private String prdName;

    /*
    产品名称
     */
    @ExcelField(title = "产品类型属性", viewLength = 20, dictCode = "STD_PRD_TYPE_PROP")
    private String prdTypeProp;

    /*
    科目号
     */
    @ExcelField(title = "科目号", viewLength = 20)
    private String subjectNo;

    /*
    担保方式
     */
    @ExcelField(title = "担保方式", viewLength = 20, dictCode = "STD_ZB_GUAR_WAY")
    private String guarMode;

    /*
    币种
     */
    @ExcelField(title = "币种", viewLength = 20,dictCode = "STD_ZB_CUR_TYP")
    private String contCurType;

    /*
    贷款标识
    */
    @ExcelField(title = "贷款标识", viewLength = 20,dictCode = "STD_LOAN_FLAG")
    private String loanFlag;

    /*
    贷款金额
     */
    @ExcelField(title = "贷款金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal loanAmt;

    /*
    贷款余额
     */
    @ExcelField(title = "贷款余额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal loanBalance;

    /*
    贷款起始日
     */
    @ExcelField(title = "贷款起始日", viewLength = 20)
    private String loanStartDate;

    /*
    贷款到期日
     */
    @ExcelField(title = "贷款到期日", viewLength = 20)
    private String loanEndDate;

    /*
    账务机构
     */
    @ExcelField(title = "账务机构", viewLength = 20)
    private String finaBrIdName;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
    */
    @ExcelField(title = "台账状态", viewLength = 20,dictCode = "STD_ACC_STATUS")
    private String accStatus;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    public String getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(String subjectNo) {
        this.subjectNo = subjectNo;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public String getLoanFlag() {
        return loanFlag;
    }

    public void setLoanFlag(String loanFlag) {
        this.loanFlag = loanFlag;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
