package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.LmtCrdReplyInfoService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 小微放款申请校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0110Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0110Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService; // 放款申请表

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService; // 授信批复表

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/8/30 21:20
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0110(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = queryModel.getCondition().get("bizId").toString();
        log.info("*************小微放款申请校验开始***********【{}】", pvpSerno);
        String replySerno = StringUtils.EMPTY; // 批复编号
        String repayMode = StringUtils.EMPTY;  // 还款方式
        if (StringUtils.isBlank(pvpSerno)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
        if (Objects.isNull(pvpLoanApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
            return riskResultDto;
        } else {
            replySerno = pvpLoanApp.getReplyNo();
        }
        log.info("***小微放款申请校验-->根据批复编号：【{}】，查询批复信息***", replySerno);
        if (StringUtils.isBlank(replySerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11001);
            return riskResultDto;
        } else {
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replySerno);
            if (Objects.isNull(lmtCrdReplyInfo)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11002);
                return riskResultDto;
            } else {
                repayMode = lmtCrdReplyInfo.getRepayMode();
            }
        }
        log.info("***小微放款申请校验-->放款还款方式：【{}】，查询批复信息***", replySerno);

        // 判断还款方式是否与批复还款方式一致
        if (!StringUtils.isEmpty(repayMode)) {
            if (!Objects.equals(pvpLoanApp.getRepayMode(), repayMode)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11003);
                return riskResultDto;
            }
        } else {
            log.info("批复信息中还款方式为空！不作处理，默认通过！");
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************小微放款申请校验开始***********【{}】", pvpSerno);
        return riskResultDto;
    }

}
