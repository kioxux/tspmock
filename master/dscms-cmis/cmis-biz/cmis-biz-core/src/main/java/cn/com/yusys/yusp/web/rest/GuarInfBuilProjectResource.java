/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBuilProject;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfBuilProject;
import cn.com.yusys.yusp.service.GuarInfBuilProjectService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuilProjectResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfbuilproject")
public class GuarInfBuilProjectResource {
    @Autowired
    private GuarInfBuilProjectService guarInfBuilProjectService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfBuilProject>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfBuilProject> list = guarInfBuilProjectService.selectAll(queryModel);
        return new ResultDto<List<GuarInfBuilProject>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfBuilProject>> index(QueryModel queryModel) {
        List<GuarInfBuilProject> list = guarInfBuilProjectService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfBuilProject>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<GuarInfBuilProject> show(@PathVariable("serno") String serno) {
        GuarInfBuilProject guarInfBuilProject = guarInfBuilProjectService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfBuilProject>(guarInfBuilProject);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfBuilProject> create(@RequestBody GuarInfBuilProject guarInfBuilProject) throws URISyntaxException {
        guarInfBuilProjectService.insert(guarInfBuilProject);
        return new ResultDto<GuarInfBuilProject>(guarInfBuilProject);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfBuilProject guarInfBuilProject) throws URISyntaxException {
        int result = guarInfBuilProjectService.update(guarInfBuilProject);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfBuilProjectService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfBuilProjectService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfBuilProject
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfBuilProject")
    protected ResultDto<Integer> preserveGuarInfBuilProject(@RequestBody GuarInfBuilProject guarInfBuilProject) {
        int result = guarInfBuilProjectService.preserveGuarInfBuilProject(guarInfBuilProject);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfBuilProject> queryBySerno(@RequestBody GuarInfBuilProject record) {
        GuarInfBuilProject guarInfBuilProject = guarInfBuilProjectService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfBuilProject>(guarInfBuilProject);
    }
}
