/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignPlanApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.service.LmtSigInvestAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignOrAllPldApp;
import cn.com.yusys.yusp.service.OtherRecordAccpSignOrAllPldAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignOrAllPldAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-06 10:10:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherrecordaccpsignorallpldapp")
public class OtherRecordAccpSignOrAllPldAppResource {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(OtherRecordAccpSignOrAllPldAppResource.class);
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private OtherRecordAccpSignOrAllPldAppService otherRecordAccpSignOrAllPldAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherRecordAccpSignOrAllPldApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherRecordAccpSignOrAllPldApp> list = otherRecordAccpSignOrAllPldAppService.selectAll(queryModel);
        return new ResultDto<List<OtherRecordAccpSignOrAllPldApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<OtherRecordAccpSignOrAllPldApp>> index(@RequestBody QueryModel queryModel) {
        List<OtherRecordAccpSignOrAllPldApp> list = otherRecordAccpSignOrAllPldAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordAccpSignOrAllPldApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherRecordAccpSignOrAllPldApp> show(@PathVariable("serno") String serno) {
        OtherRecordAccpSignOrAllPldApp otherRecordAccpSignOrAllPldApp = otherRecordAccpSignOrAllPldAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherRecordAccpSignOrAllPldApp>(otherRecordAccpSignOrAllPldApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<OtherRecordAccpSignOrAllPldApp> create(@RequestBody OtherRecordAccpSignOrAllPldApp otherRecordAccpSignOrAllPldApp) throws URISyntaxException {
        otherRecordAccpSignOrAllPldAppService.insert(otherRecordAccpSignOrAllPldApp);
        return new ResultDto<OtherRecordAccpSignOrAllPldApp>(otherRecordAccpSignOrAllPldApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherRecordAccpSignOrAllPldApp otherRecordAccpSignOrAllPldApp) throws URISyntaxException {
        //否决、打回
        if(CmisBizConstants.OPR_TYPE_02.equals(otherRecordAccpSignOrAllPldApp.getOprType())
                &&CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherRecordAccpSignOrAllPldApp.getApproveStatus())){
            otherRecordAccpSignOrAllPldApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherRecordAccpSignOrAllPldApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}",otherRecordAccpSignOrAllPldApp.getSerno());
            workflowCoreClient.deleteByBizId(otherRecordAccpSignOrAllPldApp.getSerno());
        }
        int result = otherRecordAccpSignOrAllPldAppService.update(otherRecordAccpSignOrAllPldApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherRecordAccpSignOrAllPldAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherRecordAccpSignOrAllPldAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getotherdiscperferrateapp
     * @方法描述: 根据入参获取当前客户经理名下银票签发及全资质押类业务申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherrecordaccpsignorallpldapp")
    protected ResultDto<List<OtherRecordAccpSignOrAllPldApp>> getOtherRecordAccpSignOrAllPldApp(@RequestBody QueryModel model) {
        List<OtherRecordAccpSignOrAllPldApp> list = otherRecordAccpSignOrAllPldAppService.selectByModel(model);
        return new ResultDto<List<OtherRecordAccpSignOrAllPldApp>>(list);
    }

    /**
     * @方法名称: upIsUpperApprAuth
     * @方法描述:  上调权限处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/upIsUpperApprAuth")
    protected ResultDto<Integer> upIsUpperApprAuth(@RequestBody Map<String,String> params) {
        int result = 0;
        // 获取流水号
        String serno =  params.get("serno");
        // 是否上调权限
        String  isUpperApprAuth = params.get("isUpperApprAuth");
        if(!"".equals(serno) && null != serno){
            OtherRecordAccpSignOrAllPldApp otherRecordAccpSignOrAllPldApp =  otherRecordAccpSignOrAllPldAppService.selectByPrimaryKey(serno);
            if(null != otherRecordAccpSignOrAllPldApp){
                otherRecordAccpSignOrAllPldApp.setIsUpperApprAuth(isUpperApprAuth);
                result = otherRecordAccpSignOrAllPldAppService.update(otherRecordAccpSignOrAllPldApp);
            }
        }
        return new ResultDto<Integer>(result);
    }
}
