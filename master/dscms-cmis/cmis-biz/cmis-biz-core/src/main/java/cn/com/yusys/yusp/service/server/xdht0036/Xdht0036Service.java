package cn.com.yusys.yusp.service.server.xdht0036;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0036.req.Xdht0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0036.resp.Xdht0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0036Service
 * @类描述: #服务类
 * @功能描述:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
 * @创建人: xull
 * @创建时间: 2021-05-25 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0036Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0036Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 查询优企贷贷款合同信息一览
     *
     * @param xdht0036DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0036DataRespDto queryContStatusByCertCode(Xdht0036DataReqDto xdht0036DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value);
        Xdht0036DataRespDto xdht0036DataRespDto = new Xdht0036DataRespDto();
        String queryType = xdht0036DataReqDto.getQueryType();//查询类型
        String cusId = xdht0036DataReqDto.getCusId();//客户号
        String loanStartDate = xdht0036DataReqDto.getLoanStartDate();//贷款起始日
        String managerId = xdht0036DataReqDto.getManagerId();//管户客户经理编号
        String surveySerno = xdht0036DataReqDto.getSurveySerno();
        try {
            //根据客户号,管护经理号和贷款起始日期查询优企贷贷款合同信息一览
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cusId);//客户号
            queryModel.addCondition("loanStartDate", loanStartDate);//贷款起始日
            queryModel.addCondition("managerId", managerId);//管户客户经理编号
            queryModel.addCondition("surveySerno", surveySerno);//调查流水

            if (DscmsBizHtEnum.YQD_TYPE_01.key.equals(queryType)) {//优企贷
                logger.info("*********XDHT0036**优企贷信息查询****START*********:");
                queryModel.addCondition("prdName", DscmsBizHtEnum.YQD_TYPE_01.value);//查询类型
                java.util.List<cn.com.yusys.yusp.dto.server.xdht0036.resp.List> lists = ctrLoanContMapper.queryYqdCtrloanContDetailsByCusId(queryModel);
                xdht0036DataRespDto.setList(lists);
            } else {//优农贷
                logger.info("*********XDHT0036**优农贷信息查询****START*********:");
                queryModel.addCondition("prdName", DscmsBizHtEnum.YQD_TYPE_02.value);//查询类型
                java.util.List<cn.com.yusys.yusp.dto.server.xdht0036.resp.List> lists = ctrLoanContMapper.queryYndCtrloanContDetailsByCusId(queryModel);
                xdht0036DataRespDto.setList(lists);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value);
        return xdht0036DataRespDto;
    }
}
