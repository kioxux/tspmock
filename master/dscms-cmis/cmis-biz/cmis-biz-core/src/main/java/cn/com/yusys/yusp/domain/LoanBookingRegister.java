/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LoanBookingRegister
 * @类描述: loan_booking_register数据实体类
 * @功能描述:
 * @创建人: Acer
 * @创建时间: 2021-06-18 00:04:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "loan_booking_register")
public class LoanBookingRegister extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 贷款服务行号 **/
	@Column(name = "LOAN_SERVER_BANK_NO", unique = false, nullable = true, length = 80)
	private String loanServerBankNo;

	/** 贷款服务行地址 **/
	@Column(name = "LOAN_SERVER_BANK_ADDR", unique = false, nullable = true, length = 200)
	private String loanServerBankAddr;

	/** 贷款服务行邮编 **/
	@Column(name = "LOAN_SERVER_BANK_POST", unique = false, nullable = true, length = 6)
	private String loanServerBankPost;

	/** 贷款服务行电话 **/
	@Column(name = "LOAN_SERVER_BANK_PHONE", unique = false, nullable = true, length = 20)
	private String loanServerBankPhone;

	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
	private String loanPurp;

	/** 贷款类型 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;

	/** 担保期限 **/
	@Column(name = "GUAR_TERM", unique = false, nullable = true, length = 10)
	private Integer guarTerm;

	/** 抵押物类型 **/
	@Column(name = "PAWN_TYPE", unique = false, nullable = true, length = 5)
	private String pawnType;

	/** 质押物类型 **/
	@Column(name = "PLED_TYPE", unique = false, nullable = true, length = 5)
	private String pledType;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;

	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;

	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;

	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = true, length = 5)
	private String edu;

	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;

	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 5)
	private String duty;

	/** 是否本地户口 **/
	@Column(name = "IS_LOCAL_REGIST", unique = false, nullable = true, length = 5)
	private String isLocalRegist;

	/** 居住地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 200)
	private String resiAddr;

	/** 单位名称 **/
	@Column(name = "UNIT_NAME", unique = false, nullable = true, length = 200)
	private String unitName;

	/** 单位性质 **/
	@Column(name = "INDIV_COM_TYP", unique = false, nullable = true, length = 5)
	private String indivComTyp;

	/** 个人年收入 **/
	@Column(name = "INDIV_YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal indivYearn;

	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 审批意见 **/
	@Column(name = "APPROVE_ADVICE", unique = false, nullable = true, length = 200)
	private String approveAdvice;

	/** 审批人 **/
	@Column(name = "APPR_ID", unique = false, nullable = true, length = 20)
	private String apprId;

	/** 对应经理状态 **/
	@Column(name = "CORRE_MANAGER_STATUS", unique = false, nullable = true, length = 5)
	private String correManagerStatus;

	/** 信息来源 **/
	@Column(name = "INFO_SOURCE", unique = false, nullable = true, length = 5)
	private String infoSource;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param loanServerBankNo
	 */
	public void setLoanServerBankNo(String loanServerBankNo) {
		this.loanServerBankNo = loanServerBankNo;
	}

	/**
	 * @return loanServerBankNo
	 */
	public String getLoanServerBankNo() {
		return this.loanServerBankNo;
	}

	/**
	 * @param loanServerBankAddr
	 */
	public void setLoanServerBankAddr(String loanServerBankAddr) {
		this.loanServerBankAddr = loanServerBankAddr;
	}

	/**
	 * @return loanServerBankAddr
	 */
	public String getLoanServerBankAddr() {
		return this.loanServerBankAddr;
	}

	/**
	 * @param loanServerBankPost
	 */
	public void setLoanServerBankPost(String loanServerBankPost) {
		this.loanServerBankPost = loanServerBankPost;
	}

	/**
	 * @return loanServerBankPost
	 */
	public String getLoanServerBankPost() {
		return this.loanServerBankPost;
	}

	/**
	 * @param loanServerBankPhone
	 */
	public void setLoanServerBankPhone(String loanServerBankPhone) {
		this.loanServerBankPhone = loanServerBankPhone;
	}

	/**
	 * @return loanServerBankPhone
	 */
	public String getLoanServerBankPhone() {
		return this.loanServerBankPhone;
	}

	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}

	/**
	 * @return loanPurp
	 */
	public String getLoanPurp() {
		return this.loanPurp;
	}

	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	/**
	 * @return loanType
	 */
	public String getLoanType() {
		return this.loanType;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}

	/**
	 * @return guarAmt
	 */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}

	/**
	 * @param guarTerm
	 */
	public void setGuarTerm(Integer guarTerm) {
		this.guarTerm = guarTerm;
	}

	/**
	 * @return guarTerm
	 */
	public Integer getGuarTerm() {
		return this.guarTerm;
	}

	/**
	 * @param pawnType
	 */
	public void setPawnType(String pawnType) {
		this.pawnType = pawnType;
	}

	/**
	 * @return pawnType
	 */
	public String getPawnType() {
		return this.pawnType;
	}

	/**
	 * @param pledType
	 */
	public void setPledType(String pledType) {
		this.pledType = pledType;
	}

	/**
	 * @return pledType
	 */
	public String getPledType() {
		return this.pledType;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return sex
	 */
	public String getSex() {
		return this.sex;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}

	/**
	 * @return certType
	 */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	/**
	 * @return certCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}

	/**
	 * @return edu
	 */
	public String getEdu() {
		return this.edu;
	}

	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}

	/**
	 * @return marStatus
	 */
	public String getMarStatus() {
		return this.marStatus;
	}

	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}

	/**
	 * @return duty
	 */
	public String getDuty() {
		return this.duty;
	}

	/**
	 * @param isLocalRegist
	 */
	public void setIsLocalRegist(String isLocalRegist) {
		this.isLocalRegist = isLocalRegist;
	}

	/**
	 * @return isLocalRegist
	 */
	public String getIsLocalRegist() {
		return this.isLocalRegist;
	}

	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}

	/**
	 * @return resiAddr
	 */
	public String getResiAddr() {
		return this.resiAddr;
	}

	/**
	 * @param unitName
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	/**
	 * @return unitName
	 */
	public String getUnitName() {
		return this.unitName;
	}

	/**
	 * @param indivComTyp
	 */
	public void setIndivComTyp(String indivComTyp) {
		this.indivComTyp = indivComTyp;
	}

	/**
	 * @return indivComTyp
	 */
	public String getIndivComTyp() {
		return this.indivComTyp;
	}

	/**
	 * @param indivYearn
	 */
	public void setIndivYearn(java.math.BigDecimal indivYearn) {
		this.indivYearn = indivYearn;
	}

	/**
	 * @return indivYearn
	 */
	public java.math.BigDecimal getIndivYearn() {
		return this.indivYearn;
	}

	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	/**
	 * @return loanStartDate
	 */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param approveAdvice
	 */
	public void setApproveAdvice(String approveAdvice) {
		this.approveAdvice = approveAdvice;
	}

	/**
	 * @return approveAdvice
	 */
	public String getApproveAdvice() {
		return this.approveAdvice;
	}

	/**
	 * @param apprId
	 */
	public void setApprId(String apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return apprId
	 */
	public String getApprId() {
		return this.apprId;
	}

	/**
	 * @param correManagerStatus
	 */
	public void setCorreManagerStatus(String correManagerStatus) {
		this.correManagerStatus = correManagerStatus;
	}

	/**
	 * @return correManagerStatus
	 */
	public String getCorreManagerStatus() {
		return this.correManagerStatus;
	}

	/**
	 * @param infoSource
	 */
	public void setInfoSource(String infoSource) {
		this.infoSource = infoSource;
	}

	/**
	 * @return infoSource
	 */
	public String getInfoSource() {
		return this.infoSource;
	}


}