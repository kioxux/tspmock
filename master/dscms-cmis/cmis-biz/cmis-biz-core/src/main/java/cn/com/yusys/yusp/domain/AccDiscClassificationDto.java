/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import org.apache.poi.hpsf.Decimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccDiscClassificationDto
 * @类描述:贷款风险分类支行汇总表（实时）
 * @功能描述:
 * @创建人: cainingbo_yx
 * @创建时间: 2021-04-27 21:42:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

public class AccDiscClassificationDto extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;


	/** 支行名称 **/
	private String managerBrIdName;

	/** 支行编号 **/
	private String managerBrId;

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/** 贷款余额 **/
	private BigDecimal loanBalance;

	/** 正常类 **/
	private BigDecimal normalCount;

	/** 关注类 **/
	private BigDecimal followCount;

	/** 次级类 **/
	private BigDecimal inferiorityCount;

	/** 可疑类 **/
	private BigDecimal suspiciousCount;

	/** 损失类 **/
	private BigDecimal damageCount;

	/** 小计 **/
	private BigDecimal subtotal;

	/** 正常1类 **/
	private BigDecimal normalCount1;

	/** 正常2类 **/
	private BigDecimal normalCount2;

	/** 正常3类 **/
	private BigDecimal normalCount3;

	/** 关注1类 **/
	private BigDecimal followCount1;

	/** 关注2类 **/
	private BigDecimal followCount2;

	/** 关注3类 **/
	private BigDecimal followCount3;

	/** 次级1类 **/
	private BigDecimal inferiorityCount1;

	/** 次级2类 **/
	private BigDecimal inferiorityCount2;

	/** 可疑类 **/
	private BigDecimal suspiciousCount1;

	/** 损失类 **/
	private BigDecimal damageCount1;

	/** 小计 **/
	private BigDecimal subtotal1;

	/** 正常类 **/
	private BigDecimal normalCount4;

	/** 关注类 **/
	private BigDecimal followCount4;

	/** 次级类 **/
	private BigDecimal inferiorityCount4;

	/** 可疑类 **/
	private BigDecimal suspiciousCount4;

	/** 损失类 **/
	private BigDecimal damageCount4;

	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public BigDecimal getNormalCount() {
		return normalCount;
	}

	public void setNormalCount(BigDecimal normalCount) {
		this.normalCount = normalCount;
	}

	public BigDecimal getFollowCount() {
		return followCount;
	}

	public void setFollowCount(BigDecimal followCount) {
		this.followCount = followCount;
	}

	public BigDecimal getInferiorityCount() {
		return inferiorityCount;
	}

	public void setInferiorityCount(BigDecimal inferiorityCount) {
		this.inferiorityCount = inferiorityCount;
	}

	public BigDecimal getSuspiciousCount() {
		return suspiciousCount;
	}

	public void setSuspiciousCount(BigDecimal suspiciousCount) {
		this.suspiciousCount = suspiciousCount;
	}

	public BigDecimal getDamageCount() {
		return damageCount;
	}

	public void setDamageCount(BigDecimal damageCount) {
		this.damageCount = damageCount;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getNormalCount1() {
		return normalCount1;
	}

	public void setNormalCount1(BigDecimal normalCount1) {
		this.normalCount1 = normalCount1;
	}

	public BigDecimal getNormalCount2() {
		return normalCount2;
	}

	public void setNormalCount2(BigDecimal normalCount2) {
		this.normalCount2 = normalCount2;
	}

	public BigDecimal getNormalCount3() {
		return normalCount3;
	}

	public void setNormalCount3(BigDecimal normalCount3) {
		this.normalCount3 = normalCount3;
	}

	public BigDecimal getFollowCount1() {
		return followCount1;
	}

	public void setFollowCount1(BigDecimal followCount1) {
		this.followCount1 = followCount1;
	}

	public BigDecimal getFollowCount2() {
		return followCount2;
	}

	public void setFollowCount2(BigDecimal followCount2) {
		this.followCount2 = followCount2;
	}

	public BigDecimal getFollowCount3() {
		return followCount3;
	}

	public void setFollowCount3(BigDecimal followCount3) {
		this.followCount3 = followCount3;
	}

	public BigDecimal getInferiorityCount1() {
		return inferiorityCount1;
	}

	public void setInferiorityCount1(BigDecimal inferiorityCount1) {
		this.inferiorityCount1 = inferiorityCount1;
	}

	public BigDecimal getInferiorityCount2() {
		return inferiorityCount2;
	}

	public void setInferiorityCount2(BigDecimal inferiorityCount2) {
		this.inferiorityCount2 = inferiorityCount2;
	}

	public BigDecimal getSuspiciousCount1() {
		return suspiciousCount1;
	}

	public void setSuspiciousCount1(BigDecimal suspiciousCount1) {
		this.suspiciousCount1 = suspiciousCount1;
	}

	public BigDecimal getDamageCount1() {
		return damageCount1;
	}

	public void setDamageCount1(BigDecimal damageCount1) {
		this.damageCount1 = damageCount1;
	}

	public BigDecimal getSubtotal1() {
		return subtotal1;
	}

	public void setSubtotal1(BigDecimal subtotal1) {
		this.subtotal1 = subtotal1;
	}

	public BigDecimal getNormalCount4() {
		return normalCount4;
	}

	public void setNormalCount4(BigDecimal normalCount4) {
		this.normalCount4 = normalCount4;
	}

	public BigDecimal getFollowCount4() {
		return followCount4;
	}

	public void setFollowCount4(BigDecimal followCount4) {
		this.followCount4 = followCount4;
	}

	public BigDecimal getInferiorityCount4() {
		return inferiorityCount4;
	}

	public void setInferiorityCount4(BigDecimal inferiorityCount4) {
		this.inferiorityCount4 = inferiorityCount4;
	}

	public BigDecimal getSuspiciousCount4() {
		return suspiciousCount4;
	}

	public void setSuspiciousCount4(BigDecimal suspiciousCount4) {
		this.suspiciousCount4 = suspiciousCount4;
	}

	public BigDecimal getDamageCount4() {
		return damageCount4;
	}

	public void setDamageCount4(BigDecimal damageCount4) {
		this.damageCount4 = damageCount4;
	}
}