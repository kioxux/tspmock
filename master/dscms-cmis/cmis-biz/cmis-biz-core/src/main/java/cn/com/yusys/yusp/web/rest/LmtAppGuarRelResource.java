/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.service.LmtAppGuarRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppGuarRel;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppGuarRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-13 11:04:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtappguarrel")
public class LmtAppGuarRelResource {
    @Autowired
    private LmtAppGuarRelService lmtAppGuarRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppGuarRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppGuarRel> list = lmtAppGuarRelService.selectAll(queryModel);
        return new ResultDto<List<LmtAppGuarRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAppGuarRel>> index(QueryModel queryModel) {
        List<LmtAppGuarRel> list = lmtAppGuarRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppGuarRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppGuarRel> show(@PathVariable("pkId") String pkId) {
        LmtAppGuarRel lmtAppGuarRel = lmtAppGuarRelService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppGuarRel>(lmtAppGuarRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppGuarRel> create(@RequestBody LmtAppGuarRel lmtAppGuarRel) throws URISyntaxException {
        lmtAppGuarRelService.insert(lmtAppGuarRel);
        return new ResultDto<LmtAppGuarRel>(lmtAppGuarRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppGuarRel lmtAppGuarRel) throws URISyntaxException {
        int result = lmtAppGuarRelService.update(lmtAppGuarRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppGuarRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppGuarRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:importGuarColl
     * @函数描述:引入押品，保存授信分项与押品关系
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/importguarcoll")
    protected ResultDto<Map> importGuarColl(@RequestBody LmtAppGuarRel lmtAppGuarRel) {
        Map result = lmtAppGuarRelService.importGuarColl(lmtAppGuarRel);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:deleteGuarRel
     * @函数描述:根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteguarrel")
    protected ResultDto<Map> deleteGuarRel(@RequestBody LmtAppGuarRel lmtAppGuarRel) {
        Map result = lmtAppGuarRelService.deleteGuarRel(lmtAppGuarRel);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据授信流水号获取担保关系信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectbylmtserno/{lmtSerno}")
    protected ResultDto<List<LmtAppGuarRel>> selectByLmtSerno(@PathVariable String lmtSerno) {
        List<LmtAppGuarRel> list = lmtAppGuarRelService.selectByLmtSerno(lmtSerno);
        return new ResultDto<List<LmtAppGuarRel>>(list);
    }

    /**
     * @方法名称: selectByGuarNo
     * @方法描述: 根据授信流水号获取担保关系信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectByGuarNo/{guarNo}")
    protected ResultDto<LmtAppGuarRel> selectByGuarNo(@PathVariable String guarNo) {
        LmtAppGuarRel lmtAppGuarRel = lmtAppGuarRelService.selectByGuarNo(guarNo);
        return new ResultDto<LmtAppGuarRel>(lmtAppGuarRel);
    }
}
