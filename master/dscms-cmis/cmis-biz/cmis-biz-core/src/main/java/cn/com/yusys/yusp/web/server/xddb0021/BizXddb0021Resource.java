package cn.com.yusys.yusp.web.server.xddb0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0021.req.Xddb0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0021.resp.Xddb0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0021.Xddb0021Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

/**
 * 接口处理类:根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0021:押品共有人信息查询")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0021Resource.class);

    @Autowired
    private Xddb0021Service xddb0021sService;

    /**
     * 交易码：xddb0021
     * 交易描述：押品共有人信息查询
     *
     * @param xddb0021DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品共有人信息查询")
    @PostMapping("/xddb0021")
    protected @ResponseBody
    ResultDto<Xddb0021DataRespDto> xddb0021(@Validated @RequestBody Xddb0021DataReqDto xddb0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataReqDto));
        Xddb0021DataRespDto xddb0021DataRespDto = new Xddb0021DataRespDto();// 响应Dto:根据客户名查询抵押物类型
        ResultDto<Xddb0021DataRespDto> xddb0021DataResultDto = new ResultDto<>();
        String guaranty_id = xddb0021DataReqDto.getGuaranty_id();//押品编号
        try {
            // 从xddb0021DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataReqDto));
            xddb0021DataRespDto = xddb0021sService.xddb0021(xddb0021DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataRespDto));
            // 封装xddb0021DataResultDto中正确的返回码和返回信息
            xddb0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, e.getMessage());
            // 封装xddb0021DataResultDto中异常返回码和返回信息
            xddb0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0021DataRespDto到xddb0021DataResultDto中
        xddb0021DataResultDto.setData(xddb0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataResultDto));
        return xddb0021DataResultDto;
    }
}
