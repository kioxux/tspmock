package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGrade
 * @类描述: major_grade数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 20:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class JyxwydRentIncomeDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 承租人 **/
	private String lessee;

	/** 现有租赁合同期限 **/
	private Integer termOfExistingLeaseContract;

	/** xx年 **/
	private String year;

	/** 金额*/
	private BigDecimal amt;

	public String getLessee() {
		return lessee;
	}

	public void setLessee(String lessee) {
		this.lessee = lessee;
	}

	public Integer getTermOfExistingLeaseContract() {
		return termOfExistingLeaseContract;
	}

	public void setTermOfExistingLeaseContract(Integer termOfExistingLeaseContract) {
		this.termOfExistingLeaseContract = termOfExistingLeaseContract;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
}