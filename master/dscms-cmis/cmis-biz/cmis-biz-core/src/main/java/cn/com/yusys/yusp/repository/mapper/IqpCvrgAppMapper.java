/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpCvrgApp;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCvrgAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 15:33:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpCvrgAppMapper {
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    IqpCvrgApp selectByPrimaryKey(@Param("pkId") String pkId);
    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpCvrgApp> selectByModel(QueryModel model);
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(IqpCvrgApp record);
    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int insertSelective(IqpCvrgApp record);
    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKey(IqpCvrgApp record);
    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(IqpCvrgApp record);
    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("pkId") String pkId);
    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 根据流水号更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatus(@Param("iqpSerno") String iqpSerno, @Param("approveStatus") String approveStatus);
    /**
     * @方法名称: selectByCvrgSernoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    IqpCvrgApp selectByCvrgSernoKey(String serno);


    /**
     * @方法名称：selectByLmtAccNO
     * @方法描述：根据授信台账流水查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 22:31
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<IqpCvrgApp> selectByLmtAccNo(@Param("lmtAccNo")String lmtAccNo);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    IqpCvrgApp selectBySerno(String serno);
}