package cn.com.yusys.yusp.web.server.xdtz0043;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0043.req.Xdtz0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.resp.Xdtz0043DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0049.resp.Xdxw0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.xdtz0043.Xdtz0043Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类:统计客户行内信用类贷款余额
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0043:统计客户行内信用类贷款余额")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0043Resource.class);

    @Autowired
    private Xdtz0043Service xdtz0043Service;

    @Autowired
    private CommonService commonService;

    /**
     * 交易码：xdtz0043
     * 交易描述：统计客户行内信用类贷款余额
     *
     * @param xdtz0043DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("统计客户行内信用类贷款余额")
    @PostMapping("/xdtz0043")
    protected @ResponseBody
    ResultDto<Xdtz0043DataRespDto> xdtz0043(@Validated @RequestBody Xdtz0043DataReqDto xdtz0043DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043DataReqDto));
        Xdtz0043DataRespDto xdtz0043DataRespDto = new Xdtz0043DataRespDto();// 响应Dto:统计客户行内信用类贷款余额
        ResultDto<Xdtz0043DataRespDto> xdtz0043DataResultDto = new ResultDto<>();
        // 从xdtz0043DataReqDto获取业务值进行业务逻辑处理
        String certNo = xdtz0043DataReqDto.getCertNo();
        try {
            if (StringUtil.isEmpty(certNo)) {
                xdtz0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0043DataResultDto.setMessage("证件号码【certNo】不能为空！");
                return xdtz0043DataResultDto;
            }
            Map queryMap = new HashMap();
            List<String> cusIds = commonService.getCusBaseByCertCode(certNo);
            queryMap.put("cusIds", cusIds);
            //查询条件
            if (CollectionUtils.isEmpty(cusIds)) {
                xdtz0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0043DataResultDto.setMessage("证件号码对应的客户信息不存在,请确认！");
                return xdtz0043DataResultDto;
            }
            // 调用xdtz0043Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, certNo);
            xdtz0043DataRespDto = Optional.ofNullable(xdtz0043Service.getLoanSumByCertNo(queryMap))
                    .orElse(new Xdtz0043DataRespDto(new BigDecimal("0")));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, xdtz0043DataRespDto);
            // 封装xdtz0043DataResultDto中正确的返回码和返回信息
            xdtz0043DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0043DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, e.getMessage());
            // 封装xdtz0043DataResultDto中异常返回码和返回信息
            xdtz0043DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0043DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0043DataRespDto到xdtz0043DataResultDto中
        xdtz0043DataResultDto.setData(xdtz0043DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043DataResultDto));
        return xdtz0043DataResultDto;
    }
}
