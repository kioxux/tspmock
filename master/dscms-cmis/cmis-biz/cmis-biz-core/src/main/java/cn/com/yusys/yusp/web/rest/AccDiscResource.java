/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccDisc;
import cn.com.yusys.yusp.domain.AccDiscClassificationDto;
import cn.com.yusys.yusp.service.AccDiscService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccDiscResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:42:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accdisc")
public class AccDiscResource {
    @Autowired
    private AccDiscService accDiscService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccDisc>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccDisc> list = accDiscService.selectAll(queryModel);
        return new ResultDto<List<AccDisc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccDisc>> index(QueryModel queryModel) {
        List<AccDisc> list = accDiscService.selectByModel(queryModel);
        return new ResultDto<List<AccDisc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccDisc> show(@PathVariable("pkId") String pkId) {
        AccDisc accDisc = accDiscService.selectByPrimaryKey(pkId);
        return new ResultDto<AccDisc>(accDisc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccDisc> create(@RequestBody AccDisc accDisc) throws URISyntaxException {
        accDiscService.insert(accDisc);
        return new ResultDto<AccDisc>(accDisc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccDisc accDisc) throws URISyntaxException {
        int result = accDiscService.update(accDisc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accDiscService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accDiscService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:
     * @函数描述:综合查询（贷款风险分类支行汇总表（实时））
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectClassificationSubBranchSummary")
    protected ResultDto<List<AccDiscClassificationDto>> selectClassificationSubBranchSummary(@RequestBody  QueryModel queryModel) {
        List<AccDiscClassificationDto> list = accDiscService.selectClassificationSubBranchSummary(queryModel);
        return new ResultDto<List<AccDiscClassificationDto>>(list);
    }

    /**
     * 异步下载综合查询（贷款风险分类支行汇总表（实时））
     */
    @PostMapping("/exportClassificationSubBranchSummary")
    public ResultDto<ProgressDto> exportClassificationSubBranchSummary(@RequestBody QueryModel model) {
        ProgressDto progressDto = accDiscService.exportClassificationSubBranchSummary(model);
        return ResultDto.success(progressDto);
    }

  /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccDisc>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccDisc> list = accDiscService.selectByModel(queryModel);
        return new ResultDto<List<AccDisc>>(list);
    }

    /**
     * @函数名称:selectByBillNo
     * @函数描述: 根据借据编号查询贴现台账列表信息
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @PostMapping("/selectByBillNo")
    protected ResultDto<AccDisc> selectByBillNo(@RequestBody String billNo) {
        AccDisc accDisc = accDiscService.selectByBillNo(billNo);
        return new ResultDto<AccDisc>(accDisc);
    }

    /**
     * 异步下载贴现台账列表
     */
    @PostMapping("/exportAccDisc")
    public ResultDto<ProgressDto> asyncExportAccDisc(@RequestBody QueryModel model) {
        ProgressDto progressDto = accDiscService.asyncExportAccDisc(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:
     * @函数描述:综合查询（贷款风险分类支行汇总表（实时））
     * @参数与返回说明:
     * @param queryModel
     *            根据查询条件查询台账信息并返回
     * @算法描述:
     */
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccDisc>> querymodelByCondition(@RequestBody  QueryModel queryModel) {
        List<AccDisc> list = accDiscService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccDisc>>(list);
    }




}
