package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: chenlong9
 * @创建时间: 2021-04-21 09:13:16
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpAccpAppService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(PvpAccpAppService.class);

    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    @Autowired
    private PvpAccpAppMapper pvpAccpAppMapper;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private IqpAccpAppPorderSubService iqpAccpAppPorderSubService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private PvpAccpAppDrftSubService pvpAccpAppDrftSubService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PvpAccpApp selectByPrimaryKey(String pkId) {
        return pvpAccpAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PvpAccpApp> selectAll(QueryModel model) {
        List<PvpAccpApp> records = (List<PvpAccpApp>) pvpAccpAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PvpAccpApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpAccpApp> list = pvpAccpAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(PvpAccpApp record) {
        return pvpAccpAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(PvpAccpApp record) {
        return pvpAccpAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(PvpAccpApp record) {
        return pvpAccpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(PvpAccpApp record) {
        return pvpAccpAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return pvpAccpAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return pvpAccpAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: handInvaild
     * @方法描述: 根据主键手工作废银承出账
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int handInvaild(String pvpSerno) {
        PvpAccpApp pvpAccpApp = this.selectBySerno(pvpSerno);
        if (CmisCommonConstants.WF_STATUS_992.equals(pvpAccpApp.getApproveStatus())) {
            pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            //删除流程实例
            ResultDto<ResultMessageDto> resultMessageDtoResultDto = workflowCoreClient.deleteByBizId(pvpAccpApp.getPvpSerno());
            //作废恢复额度
            this.onCancel(pvpAccpApp.getPvpSerno());
        } else if (CmisCommonConstants.WF_STATUS_000.equals(pvpAccpApp.getApproveStatus())) {
            //待发起状态作废
            pvpAccpApp.setOprType(CmisBizConstants.OPR_TYPE_02);
        } else if (CmisCommonConstants.WF_STATUS_998.equals(pvpAccpApp.getApproveStatus())) {
            pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            pvpAccpApp.setOprType(CmisBizConstants.OPR_TYPE_02);
            ResultDto<ResultMessageDto> resultMessageDtoResultDto = workflowCoreClient.deleteByBizId(pvpAccpApp.getPvpSerno());
        }
        return pvpAccpAppMapper.updateByPrimaryKey(pvpAccpApp);
    }

    /**
     * @param pvpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author qw
     * @date 2021/9/11 14:04
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void onCancel(String pvpSerno) {
        PvpAccpApp pvpAccpApp = this.selectBySerno(pvpSerno);
        String inputId = "";
        String inputBrId = "";
        String inputDate = "";
        // 获取当前的登录人信息
        User userInfo = SessionUtils.getUserInformation();
        if (Objects.nonNull(userInfo)) {
            inputId = userInfo.getLoginCode();
            inputBrId = userInfo.getOrg().getCode();
            inputDate = stringRedisTemplate.opsForValue().get("openDay");
        }
        // 根据担保方式判断是否低风险
        String guarMode = pvpAccpApp.getGuarMode();
        // 恢复敞口余额
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
        // 恢复总额
        BigDecimal recoverAmtCny = BigDecimal.ZERO;

        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            recoverAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
            recoverSpacAmtCny = BigDecimal.ZERO;
        } else {
            recoverAmtCny = pvpAccpApp.getAppAmt();
            recoverSpacAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
        }

        // 组装报文进行台账恢复
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpAccpApp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(pvpAccpApp.getPvpSerno());//交易流水号
        cmisLmt0014ReqDto.setInputId(inputId);//登记人
        cmisLmt0014ReqDto.setInputBrId(inputBrId);//登记机构
        cmisLmt0014ReqDto.setInputDate(inputDate);//登记日期

        List<CmisLmt0014ReqdealBizListDto> dealBizList = new ArrayList<>();

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpAccpApp.getPvpSerno());//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
        cmisLmt0014ReqdealBizListDto.setRecoverStd(CmisLmtConstants.STD_ZB_RECOVER_STD_02);//恢复标准值
        cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(BigDecimal.ZERO);//台账总余额（人民币）
        cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);//台账敞口余额（人民币）
        cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口余额(人民币)
        cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额(人民币)
        cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
        cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
        dealBizList.add(cmisLmt0014ReqdealBizListDto);
        cmisLmt0014ReqDto.setDealBizList(dealBizList);
        log.info("台账【{}】恢复，前往额度系统进行额度恢复开始,请求报文为:【{}】", pvpAccpApp.getPvpSerno(), cmisLmt0014ReqDto.toString());
        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        log.info("台账【{}】恢复，前往额度系统进行额度恢复结束,响应报文为:【{}】", pvpAccpApp.getPvpSerno(), cmisLmt0014RespDtoResultDto.toString());
        if (!"0".equals(cmisLmt0014RespDtoResultDto.getCode())) {
            log.info("台账恢复，前往额度系统进行额度恢复异常");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        if (!"0000".equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
            log.info("台账【{}】恢复，前往额度系统进行额度恢复异常", pvpAccpApp.getPvpSerno());
            throw BizException.error(null, cmisLmt0014RespDtoResultDto.getData().getErrorCode(), cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
        }
    }


    /**
     * @方法名称: toSignlist
     * @方法描述: 查询待发起数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAccpApp> toSignlist(QueryModel model) {
        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return pvpAccpAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询已处理数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<PvpAccpApp> doneSignlist(QueryModel model) {
        model.addCondition("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return pvpAccpAppMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param pvpSerno
     * @return
     */
    public PvpAccpApp selectBySerno(String pvpSerno) {
        return pvpAccpAppMapper.selectBySerno(pvpSerno);
    }

    /**
     * 银承出账
     *
     * @param pvpAccpApp
     * @returnf
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonUpdatePvpAccpApp(PvpAccpApp pvpAccpApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (pvpAccpApp == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                pvpAccpApp.setUpdId(userInfo.getLoginCode());
                pvpAccpApp.setUpdBrId(userInfo.getOrg().getCode());
                pvpAccpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                pvpAccpApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            }

            Map seqMap = new HashMap();


            int updCount = pvpAccpAppMapper.updateByPrimaryKeySelective(pvpAccpApp);
            if (updCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }


        } catch (YuspException e) {
            log.error("银承出账保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("银承出账异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取银承贷款申请" + pvpSerno + "申请主表信息");
            PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(pvpSerno);
            if (pvpAccpApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }

            int updateCount = 0;
            log.info("流程发起-更新银承贷款申请" + pvpSerno + "流程审批状态为【111】-审批中");
            updateCount = this.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_111);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }

            //获取贸易融资合同数据，并根据合同中的业务类型进行数据判断
            log.info("贸易融资放款申请发起流程-获取放款申请" + pvpSerno + "获取业务合同数据");
            //判断是否低风险占额
            String guarMode = pvpAccpApp.getGuarMode();
            BigDecimal bizAmtCny = pvpAccpApp.getAppAmt();
            BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                bizSpacAmtCny = BigDecimal.ZERO;
            } else {
                bizSpacAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
            }

            //调票据接口查询票据起始日到期日
            String endDate = "";
            Xdpj23ReqDto xdpj23ReqDto = new Xdpj23ReqDto();
            xdpj23ReqDto.setsBatchNo(pvpSerno);
            xdpj23ReqDto.setTurnPageShowNum("100");
            xdpj23ReqDto.setTurnPageBeginPos("1");
            LocalDateTime now = LocalDateTime.now();
            xdpj23ReqDto.setMac("");//mac地址
            xdpj23ReqDto.setIpaddr("");//ip地址
            xdpj23ReqDto.setServdt(tranDateFormtter.format(now));//交易日期
            xdpj23ReqDto.setServti(tranTimestampFormatter.format(now));//交易时间
            log.info("通过批次号【{}】，前往票据系统查询票据明细开始,请求报文为:【{}】", pvpSerno, JSON.toJSONString(xdpj23ReqDto));
            ResultDto<Xdpj23RespDto> xdpj23RespDtoResultDto = dscms2PjxtClientService.xdpj23(xdpj23ReqDto);
            log.info("通过批次号【{}】，前往票据系统查询票据明细结束,响应报文为:【{}】", pvpSerno, JSON.toJSONString(xdpj23RespDtoResultDto));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj23RespDtoResultDto.getCode())
                    && Objects.nonNull(xdpj23RespDtoResultDto.getData())) {
                Xdpj23RespDto data = xdpj23RespDtoResultDto.getData();
                List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> xdpj23List = data.getList();
                if(CollectionUtils.nonEmpty(xdpj23List)){
                    List<String> collect = xdpj23List.stream().filter(Objects::nonNull).sorted((e1, e2) -> e2.getDueDt().compareTo(e1.getDueDt())).map(e -> e.getDueDt()).collect(Collectors.toList());
                    endDate = collect.get(0);
                }
            }
            log.info("票据到期日为：【{}】",endDate);

            //组装报文，进行国结票据出账额度占用
            CmisLmt0034ReqDto cmisLmt0034ReqDto = new CmisLmt0034ReqDto();
            cmisLmt0034ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0034ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpAccpApp.getManagerBrId()));//金融机构代码
            cmisLmt0034ReqDto.setSerno(pvpAccpApp.getPvpSerno());//交易流水号
            cmisLmt0034ReqDto.setBizNo(pvpAccpApp.getContNo());//合同编号
            cmisLmt0034ReqDto.setInputId(pvpAccpApp.getInputId());//登记人
            cmisLmt0034ReqDto.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
            cmisLmt0034ReqDto.setInputDate(pvpAccpApp.getInputDate());//登记日期
            //台账明细
            List<CmisLmt0034DealBizListReqDto> cmisLmt0034DealBizListReqDtoList = new ArrayList<CmisLmt0034DealBizListReqDto>();
            CmisLmt0034DealBizListReqDto cmisLmt0034DealBizListReqDto = new CmisLmt0034DealBizListReqDto();
            cmisLmt0034DealBizListReqDto.setDealBizNo(pvpAccpApp.getPvpSerno());//台账编号
            cmisLmt0034DealBizListReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0034DealBizListReqDto.setOrigiDealBizNo("");//原交易业务编号
            cmisLmt0034DealBizListReqDto.setOrigiRecoverType("");//原交易业务恢复类型
            cmisLmt0034DealBizListReqDto.setCusId(pvpAccpApp.getCusId());//客户编号
            cmisLmt0034DealBizListReqDto.setCusName(pvpAccpApp.getCusName());//客户名称
            cmisLmt0034DealBizListReqDto.setPrdNo(pvpAccpApp.getPrdId());//产品编号
            cmisLmt0034DealBizListReqDto.setPrdName(pvpAccpApp.getPrdName());//产品名称
            cmisLmt0034DealBizListReqDto.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());//授信品种类型
            cmisLmt0034DealBizListReqDto.setDealBizAmtCny(bizAmtCny);//台账占用总额(人民币)
            cmisLmt0034DealBizListReqDto.setDealBizSpacAmtCny(bizSpacAmtCny);//台账占用敞口(人民币)
            cmisLmt0034DealBizListReqDto.setStartDate(stringRedisTemplate.opsForValue().get("openDay"));//台账起始日
            cmisLmt0034DealBizListReqDto.setEndDate(endDate);//台账到期日
            cmisLmt0034DealBizListReqDto.setDealBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);//台账状态
            cmisLmt0034DealBizListReqDtoList.add(cmisLmt0034DealBizListReqDto);
            cmisLmt0034ReqDto.setDealBizList(cmisLmt0034DealBizListReqDtoList);

            //是否他行代签为是时，需要占用同业客户额度
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(pvpAccpApp.getIsOtherSign())){
                List<CmisLmt0034OccRelListReqDto> cmisLmt0034OccRelListReqDtoList = new ArrayList<CmisLmt0034OccRelListReqDto>();
                CmisLmt0034OccRelListReqDto cmisLmt0034OccRelListReqDto = new CmisLmt0034OccRelListReqDto();
                cmisLmt0034OccRelListReqDto.setDealBizNo(pvpAccpApp.getPvpSerno());//台账编号
                cmisLmt0034OccRelListReqDto.setLmtCusId("8001284352");//同业客户编号(中国人民银行客户编号)
                cmisLmt0034OccRelListReqDto.setBizSpacAmtCny(pvpAccpApp.getAppAmt());//占用敞口
                cmisLmt0034OccRelListReqDtoList.add(cmisLmt0034OccRelListReqDto);
                cmisLmt0034ReqDto.setOccRelList(cmisLmt0034OccRelListReqDtoList);
            }
            log.info("银承出账申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", pvpSerno, cmisLmt0034ReqDto.toString());
            ResultDto<CmisLmt0034RespDto> resultDtoDto = cmisLmtClientService.cmislmt0034(cmisLmt0034ReqDto);
            log.info("银承出账申请【{}】，前往额度系统进行额度占用结束,响应报文为:【{}】", pvpSerno, resultDtoDto.toString());
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("银承出账申请【{}】占用额度异常！",pvpSerno);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if (!SuccessEnum.SUCCESS.key.equals(code)) {
                log.error("银承出账申请【{}】占用额度异常！",pvpSerno);
                throw BizException.error(null,code,resultDtoDto.getData().getErrorMsg());
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("银承业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 通过放款申请流水号更新放款申请状态
     * 释放额度
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterCallBack(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + pvpSerno + "申请信息");
            PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(pvpSerno);

            if (pvpAccpApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            log.info("放款申请发起流程-更新放款申请" + pvpSerno + "流程状态-退回");
            int updCount = updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
            if (updCount <= 0) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    @Transactional
    public int updateApprStatus(String iqpSerno, String apprStatus) {
        return pvpAccpAppMapper.updateApprStatus(iqpSerno, apprStatus);
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param pvpSerno
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ResultDto handleBusinessDataAfterEnd(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("银承出票审批通过-获取银承出票申请" + pvpSerno + "申请主表信息");
            PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(pvpSerno);
            if (pvpAccpApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("银承出票审批通过-获取银承出票申请" + pvpSerno + "获取业务合同数据");
            String cont_no = pvpAccpApp.getContNo();
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("contNo", cont_no);
            CtrAccpCont ctrAccpCont = ctrAccpContService.queryCtrAccpContByDataParams(queryMap);

            log.info("银承出票审批通过-获取银承出票申请" + pvpSerno + "获取银承协议申请汇票明细");
            int updateCount = 0;
//            QueryModel queryModel = new QueryModel();
//            queryModel.addCondition("serno", ctrAccpCont.getSerno());
//            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(ctrAccpCont.getSerno());

            /** 计算保证金金额 */
            BigDecimal secAmtThis = BigDecimal.ZERO;
            BigDecimal secAmtSum = BigDecimal.ZERO;
            BigDecimal procFeeAmount2This = BigDecimal.ZERO;// 现金
            BigDecimal procFeeAmount2Sum = BigDecimal.ZERO;// 现金

            BigDecimal procFeeAmountThis = BigDecimal.ZERO;// 转账
            BigDecimal procFeeAmountSum = BigDecimal.ZERO;// 转账
            BigDecimal secAmtTotal = pvpAccpApp.getBailAmt();// 保证金

            BigDecimal procFeeAmount = pvpAccpApp.getChrgAmt();// 手续费(转账)
            BigDecimal procFeeAmount2 = pvpAccpApp.getChrgAmt();// 手续费(现金)
            BigDecimal procFeePercent = pvpAccpApp.getChrgRate();//手续费率

            // IqpAccpAppPorderSub iqpAccpAppPorderSub = list.get(i);
            log.info("银承出票审批通过-放款申请" + pvpSerno + "生成授权信息");
            PvpAuthorize pvpAuthorize = new PvpAuthorize();
            if (pvpAccpApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            //出账号
            String loanNo = "";
            String loanStartDate = pvpAccpApp.getStartDate();//合同起始日
            String loanEndDate = pvpAccpApp.getEndDate();//合同到期日
            //获取当前日期
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            Map seqMap = new HashMap();
            String tran_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
            pvpAuthorize.setTranSerno(tran_serno);// 交易流水号
            pvpAuthorize.setPvpSerno(pvpAccpApp.getPvpSerno());// 出账号
            pvpAuthorize.setContNo(pvpAccpApp.getContNo());//非报文字段(校验担保品是否入库用到)
            pvpAuthorize.setFldvalue33(pvpAccpApp.getGuarMode());//非报文字段(校验担保品是否入库用到)
            pvpAuthorize.setFldvalue34("");//非报文字段(校验担保品是否入库用到)
            pvpAuthorize.setFldvalue40("【银票】");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(pvpAccpApp.getManagerId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            pvpAuthorize.setFldvalue39(adminSmUserDto.getUserName());//客户经理姓名

            // 借据编号 = {contNo}{SEQUENCE}
            seqMap.put("contNo", pvpAccpApp.getContNo());
            String bill_no = pvpAccpApp.getPvpSerno();//申请流水号 票据系统申请时传入的值
            pvpAuthorize.setBillNo(bill_no);//汇票编号
            pvpAuthorize.setTranId("CRE800");// 交易码
            pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1);// 交易状态
            pvpAuthorize.setTranDate(stringRedisTemplate.opsForValue().get("openDay"));// 交易日期
            pvpAuthorize.setTranAmt(pvpAccpApp.getAppAmt());//申请金额
            BigDecimal drftAmt = pvpAccpApp.getAppAmt();//票面金额（申请金额）
            BigDecimal secRt = pvpAccpApp.getBailPerc();//保证金比例

            pvpAuthorize.setPrdId(pvpAccpApp.getPrdId());//产品编号
            pvpAuthorize.setPrdName(pvpAccpApp.getPrdName());//产品名称
            pvpAuthorize.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());
            pvpAuthorize.setFinaBrId(pvpAccpApp.getFinaBrId());//账务机构
            pvpAuthorize.setInputId(pvpAccpApp.getInputId());//登记人
            pvpAuthorize.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
            pvpAuthorize.setManagerId(pvpAccpApp.getManagerId());//责任人
            pvpAuthorize.setManagerBrId(pvpAccpApp.getManagerBrId());//申请机构
            pvpAuthorize.setCusId(pvpAccpApp.getCusId());//客户编号
            pvpAuthorize.setCusName(pvpAccpApp.getCusName());//客户名称
            pvpAuthorize.setFldvalue01(bill_no);//汇票编号
            pvpAuthorize.setFldvalue02(pvpAccpApp.getDaorgNo());//出票人账号
            //  todo  待办 iqp_accp_app
            pvpAuthorize.setFldvalue03(String.valueOf(pvpAccpApp.getAppAmt()));//出票金额
            pvpAuthorize.setFldvalue07(pvpAccpApp.getContNo());//协议编号
            pvpAuthorize.setFldvalue08("");//保证金账号

            //todo /** 计算保证金金额 和手续费金额*/

            pvpAuthorize.setFldvalue09(secAmtThis + "");
            pvpAuthorize.setFldvalue10(pvpAccpApp.getEndDate());//汇票到期日
            pvpAuthorize.setFldvalue11(pvpAccpApp.getChrgRate() + "");//手续费率
            pvpAuthorize.setFldvalue15(pvpAccpApp.getContCurType());//币别
            pvpAuthorize.setFldvalue16("");//保证金计息方式
            pvpAuthorize.setFldvalue17(pvpAccpApp.getReturnDraftInterestType());//保证金退票计息方式
            pvpAuthorize.setFldvalue18("");//保证金协议月利率
            pvpAuthorize.setFldvalue19("");//结算账号
            pvpAuthorize.setFldvalue20("");//结算账号户名
            pvpAuthorize.setFldvalue21(String.valueOf(procFeeAmount));//手续费金额(转账)(每张票的)
            pvpAuthorize.setFldvalue22(String.valueOf(procFeeAmount2));//手续费金额(现金)(每张票的)
            pvpAuthorize.setFldvalue23(String.valueOf(pvpAccpApp.getBailAmt()));//保证金金额
            pvpAuthorize.setFldvalue24(String.valueOf(pvpAccpApp.getApplyPorder()));//汇票张数
            pvpAuthorize.setFldvalue25(String.valueOf(pvpAccpApp.getOverdueRate()));//逾期垫款利率

            pvpAuthorize.setFldvalue27(pvpAccpApp.getAorgNo());//承兑机构
            pvpAuthorize.setFldvalue35(pvpAccpApp.getAorgNo());//承兑行编号
            pvpAuthorize.setFldvalue36(pvpAccpApp.getIssuedOrgNo());//签发机构
            pvpAuthorize.setFldvalue37(pvpAccpApp.getContCurType());//币种
            pvpAuthorize.setFldvalue38(pvpAccpApp.getEndDate());//到期日

            int resultAu = pvpAuthorizeService.insertSelective(pvpAuthorize);

            log.info("放款审批通过-放款申请" + pvpSerno + "生成借据信息");

            AccAccp accAccp = new AccAccp();
            String pkId = StringUtils.uuid(true);
            accAccp.setPkId(pkId);//主键
            accAccp.setCoreBillNo(bill_no);//借据编号
            accAccp.setPvpSerno(pvpAccpApp.getPvpSerno());//出账流水号
            accAccp.setContNo(pvpAccpApp.getContNo());//合同编号
            accAccp.setCusId(pvpAccpApp.getCusId());//客户编号
            accAccp.setCusName(pvpAccpApp.getCusName());//客户名称
            accAccp.setPrdId(pvpAccpApp.getPrdId());//产品编号
            accAccp.setPrdName(pvpAccpApp.getPrdName());//产品名称
            accAccp.setCurType(pvpAccpApp.getContCurType());//贷款发放币种
            accAccp.setGuarMode(pvpAccpApp.getGuarMode());//担保方式
            accAccp.setIsEDrft(pvpAccpApp.getIsEDrft());//是否电子票据
            accAccp.setDrftTotalAmt(pvpAccpApp.getAppAmt());//票据总金额(申请金额)
            accAccp.setDrftBalanceAmt(pvpAccpApp.getAppAmt());//票面总余额(申请金额)
            accAccp.setOrigiOpenAmt(pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt()));//原始敞口金额
            accAccp.setIsSxep(pvpAccpApp.getIsSxep());//是否省心E票
            accAccp.setIsseCnt(pvpAccpApp.getApplyPorder() + "");//开票张数
            accAccp.setIsseDate(pvpAccpApp.getDraftSignDate());//出票日期
            accAccp.setBailPerc(pvpAccpApp.getBailPerc());//保证金比例
            accAccp.setBailAmt(pvpAccpApp.getBailAmt());//保证金金额
            accAccp.setChrgRate(pvpAccpApp.getChrgRate());//手续费率
            accAccp.setSpacAmt(drftAmt.subtract(secAmtThis));//敞口金额
            accAccp.setChrgAmt(pvpAccpApp.getChrgAmt());//手续费金额
            accAccp.setChrgType(pvpAccpApp.getChrgType());//手续费类型
            accAccp.setOverdueRate(pvpAccpApp.getOverdueRate());//逾期垫款年利率
            accAccp.setReturnDraftInterestType(pvpAccpApp.getReturnDraftInterestType());//退票计息方式
            accAccp.setAorgType(pvpAccpApp.getAorgType());//承兑行类型
            accAccp.setAorgNo(pvpAccpApp.getAorgNo());//承兑行行号
            accAccp.setAorgName(pvpAccpApp.getAorgName());//承兑行名称
            accAccp.setIsUtilLmt(pvpAccpApp.getIsUtilLmt());//是否使用授信额度
            accAccp.setLmtAccNo(pvpAccpApp.getLmtAccNo());//授信额度编号
            accAccp.setReplyNo(pvpAccpApp.getReplyNo());//批复编号
            accAccp.setFinaBrId(pvpAccpApp.getFinaBrId());//账务机构编号
            accAccp.setFinaBrIdName(pvpAccpApp.getFinaBrIdName());//账务机构名称
            accAccp.setDisbOrgName(pvpAccpApp.getIssuedOrgName());//放款机构名称
            accAccp.setDisbOrgNo(pvpAccpApp.getIssuedOrgNo());//放款机构编号
            accAccp.setFiveClass("10");//五级分类
            accAccp.setTenClass("11");//十级分类
            accAccp.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//分类日期
            accAccp.setAccStatus(CmisCommonConstants.ACC_STATUS_1);//台账状态
            accAccp.setIsOtherSign(pvpAccpApp.getIsOtherSign());//是否他行代签
            accAccp.setPrdTypeProp(pvpAccpApp.getPrdTypeProp());//产品类型属性
            accAccp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型

            User userInfo = SessionUtils.getUserInformation();
            if (Objects.nonNull(userInfo)) {
                accAccp.setUpdId(userInfo.getLoginCode());
                accAccp.setUpdBrId(userInfo.getOrg().getCode());
            } else {
                accAccp.setUpdId(pvpAccpApp.getManagerId());
                accAccp.setUpdBrId(pvpAccpApp.getManagerBrId());
            }
            accAccp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            accAccp.setManagerId(pvpAccpApp.getManagerId());
            accAccp.setManagerBrId(pvpAccpApp.getManagerBrId());
            accAccp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            int resultAc = accAccpService.insertSelective(accAccp);

            log.info("流程发起-更新银承贷款申请" + pvpSerno + "流程审批状态为【997】-审批结束");
            updateCount = pvpAccpAppMapper.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_997);
            // 判断是否自动出账 STD_PVP_MODE
            try {
                if ("1".equals(pvpAccpApp.getPvpMode())) {
                    log.info("银承贷款自动出账" + pvpSerno);
                    Map map = pvpAuthorizeService.sendBill(pvpAuthorize);
                    if (Objects.nonNull(map)) {
                        if (Objects.equals("0000", (String) map.get("rtnCode"))) {
                            log.info("银承贷款自动出账成功" + pvpSerno);
                            // 需要占用额度 c
                            return new ResultDto(null).message("自动出账成功").code("0");
                        } else {
                            return new ResultDto(null).message((String) map.get("rtnCode")).code((String) map.get("rtnCode"));
                        }
                    }
                }
            } catch (BizException e) {
                log.info("流程结束-银承贷款自动出账" + pvpSerno + "失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            } catch (Exception e) {
                log.info("流程结束-银承贷款自动出账" + pvpSerno + "失败");
                return new ResultDto(null).message(e.getMessage()).code("9999");
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            log.error("银承业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
        return new ResultDto(null).message("出账申请成功").code("0");
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【解除】，业务与担保合同关系结果表数据更新为【解除】
     * <p>
     * 放款申请拒绝后，仅将当前申请状态变更为“否决”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 拒绝
     *
     * @param pvpSerno
     */
    public void handleBusinessAfterRefuse(String pvpSerno) {
        try {
            if (StringUtils.isBlank(pvpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "申请信息");
            PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(pvpSerno);

            if (pvpAccpApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }

            //获取合同数据，并根据合同中的业务类型进行数据判断
            log.info("放款申请拒绝流程-获取放款申请" + pvpSerno + "获取业务合同数据");
            String cont_no = pvpAccpApp.getContNo();
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("contNo", cont_no);
            CtrAccpCont ctrAccpCont = ctrAccpContService.queryCtrAccpContByDataParams(queryMap);
            if (ctrAccpCont == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_CONTNULL_EXCEPTION.value);
            }

            int updCount = 0;
            updCount = this.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_998);

            if (updCount <= 0) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION.value);
            }
            //流程打回，恢复占额
            String guarMode = pvpAccpApp.getGuarMode();
            //恢复敞口
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            //恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;
            if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                recoverSpacAmtCny = BigDecimal.ZERO;
                recoverAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
            } else {
                recoverSpacAmtCny = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getBailAmt());
                recoverAmtCny = pvpAccpApp.getAppAmt();
            }
            CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
            cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpAccpApp.getManagerBrId()));//金融机构代码
            cmisLmt0014ReqDto.setSerno(pvpAccpApp.getPvpSerno());//交易流水号
            cmisLmt0014ReqDto.setInputId(pvpAccpApp.getInputId());//登记人
            cmisLmt0014ReqDto.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
            cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//登记日期

            List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqDtoList = new ArrayList<>();
            CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
            cmisLmt0014ReqdealBizListDto.setDealBizNo(pvpAccpApp.getPvpSerno());//台账编号
            cmisLmt0014ReqdealBizListDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(recoverAmtCny);//恢复总额
            cmisLmt0014ReqdealBizListDto.setDealBizBailPreRate(BigDecimal.ZERO);//追加后保证金比例
            cmisLmt0014ReqdealBizListDto.setSecurityAmt(BigDecimal.ZERO);//追加后保证金金额
            cmisLmt0014ReqDtoList.add(cmisLmt0014ReqdealBizListDto);
            cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqDtoList);
            log.info("贷款出账业务申请" + pvpSerno + "，前往额度系统恢复占用额度开始");
            ResultDto<CmisLmt0014RespDto> resultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            log.info("贷款出账业务申请" + pvpSerno + "，前往额度系统恢复占用额度结束");
            if (Objects.isNull(resultDto.getData()) || !"0000".equals(resultDto.getData().getErrorCode())) {
                log.error("业务申请恢复额度异常！");
                throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + pvpSerno + "流程拒绝业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    private AccAccp getAccAccpInfo(PvpAccpApp pvpAccpApp, IqpAccpAppPorderSub iqpAccpAppPorderSub) {

        AccAccp accAccp = new AccAccp();

        String contNo = pvpAccpApp.getContNo();
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("contNo", contNo);
        CtrAccpCont ctrAccpCont = ctrAccpContService.queryCtrAccpContByDataParams(queryMap);
        Map seqMap = new HashMap();
        String bill_no = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, seqMap);
        String pkId = StringUtils.uuid(true);
//        accAccp.setPkId(pkId);//主键
//        accAccp.setBillNo("");//借据编号
//        accAccp.setPvpSerno(pvpAccpApp.getPvpSerno());//出账流水号
//        accAccp.setContNo(pvpAccpApp.getContNo());//合同编号
//        accAccp.setCusId(pvpAccpApp.getCusId());//客户编号
//        accAccp.setCusName(pvpAccpApp.getCusName());//客户名称
//        accAccp.setPrdId(pvpAccpApp.getPrdId());//产品编号
//        accAccp.setPrdName(pvpAccpApp.getPrdName());//产品名称
//        accAccp.setCurType(pvpAccpApp.getContCurType());//贷款发放币种
//        accAccp.setGuarMode(pvpAccpApp.getGuarMode());//担保方式
//        accAccp.setIsEDrft(pvpAccpApp.getIsEDrft());//是否电子票据
//        accAccp.setDrftTotalAmt(pvpAccpApp.getContAmt());//票据总金额
//        accAccp.setIsseCnt("0");//开票张数
//        accAccp.setIsseDate("");//出票日期
//        accAccp.setBailPerc(pvpAccpApp.getBailPerc());//保证金比例
//        accAccp.setBailAmt(pvpAccpApp.getBailAmt());//保证金金额
//        accAccp.setChrgPerc(pvpAccpApp.getChrgPerc());//手续费率
//        accAccp.setSpacAmt(new BigDecimal("0"));//敞口金额
//        accAccp.setChrgAmt(pvpAccpApp.getChrgAmt());//手续费金额
//        accAccp.setChrgType(pvpAccpApp.getChrgType());//手续费类型
//        accAccp.setOverdueRate(pvpAccpApp.getOverdueRate());//逾期垫款年利率
//        accAccp.setReturnDraftInterestType("");//退票计息方式
//        accAccp.setAorgType(pvpAccpApp.getAorgType());//承兑行类型
//        accAccp.setAorgNo(pvpAccpApp.getAorgNo());//承兑行行号
//        accAccp.setAorgName(pvpAccpApp.getAorgName());//承兑行名称
//        accAccp.setIsUtilLmt(pvpAccpApp.getIsUtilLmt());//是否使用授信额度
//        accAccp.setLmtNo(pvpAccpApp.getLmtNo());//授信额度编号
//        accAccp.setRstNo(pvpAccpApp.getRstNo());//批复编号
//        accAccp.setFinaBrId(pvpAccpApp.getFinaBrId());//账务机构编号
//        accAccp.setFinaBrIdName(pvpAccpApp.getFinaBrIdName());//账务机构名称
//        accAccp.setDisbOrgName("");//放款机构名称
//        accAccp.setDisbOrgNo("");//放款机构编号
//        accAccp.setFiveClass("");//五级分类
//        accAccp.setTenClass("");//十级分类
//        accAccp.setClassDate("");//分类日期
//        accAccp.setAccStatus("001");//台账状态
//        accAccp.setOprType("01");//操作类型
//
//        User userInfo = SessionUtils.getUserInformation();
//        accAccp.setUpdId(userInfo.getLoginCode());
//        accAccp.setUpdBrId(userInfo.getOrg().getCode());
//        accAccp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
//        accAccp.setManagerId(pvpAccpApp.getManagerId());
//        accAccp.setManagerBrId(pvpAccpApp.getManagerBrId());
//        accAccp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));

        return accAccp;
    }

    private PvpAuthorize getPvpAuthorize(PvpAccpApp pvpAccpApp, IqpAccpAppPorderSub iqpAccpAppPorderSub) {
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
//
//        if (pvpAccpApp == null) {
//            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
//        }
//        //出账号
//        String loanNo = "";
//        String loanStartDate = pvpAccpApp.getStartDate();            //合同起始日
//        String loanEndDate = pvpAccpApp.getEndDate();                //合同到期日
//        //获取当前日期
//        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
//
//
//        Map seqMap = new HashMap();
//        String tran_serno =  sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO,seqMap);
//        pvpAuthorize.setTranSerno(tran_serno);
//        pvpAuthorize.setPvpSerno(pvpAccpApp.getPvpSerno());// 出账号
//
//        pvpAuthorize.setContNo(pvpAccpApp.getContNo());//非报文字段(校验担保品是否入库用到)
//        pvpAuthorize.setFldvalue33(pvpAccpApp.getGuarMode());//非报文字段(校验担保品是否入库用到)
//        pvpAuthorize.setFldvalue34("");//非报文字段(校验担保品是否入库用到)
//        pvpAuthorize.setFldvalue40("【银票】");
//        ResultDto<AdminSmUserDto>  resultDto = adminSmUserService.getByLoginCode(pvpAccpApp.getManagerId());
//        AdminSmUserDto adminSmUserDto = resultDto.getData();
//        pvpAuthorize.setFldvalue39(adminSmUserDto.getUserName());//客户经理姓名
//
//
//        String bill_no =  sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ,seqMap);
//        pvpAuthorize.setBillNo(bill_no);//汇票编号
//
//        pvpAuthorize.setTranId("CRE800");// 交易码
//        pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1); // 交易状态
//        pvpAuthorize.setTranDate(stringRedisTemplate.opsForValue().get("openDay"));// 交易日期
//        pvpAuthorize.setTranAmt(pvpAccpApp.getAppAmt());            //申请金额
//
//
//        pvpAuthorize.setAcctBrId(pvpAccpApp.getFinaBrId());                 //账务机构
//        pvpAuthorize.setManagerId(pvpAccpApp.getManagerId());//责任人
//        pvpAuthorize.setManagerBrId(pvpAccpApp.getManagerBrId());//申请机构
//        pvpAuthorize.setCusId(pvpAccpApp.getCusId());//客户编号
//        pvpAuthorize.setCusName(pvpAccpApp.getCusName());//客户名称
//        pvpAuthorize.setFldvalue01(bill_no);//汇票编号
//        pvpAuthorize.setFldvalue02(pvpAccpApp.getDaorgNo());//出票人账号
//        pvpAuthorize.setFldvalue03(iqpAccpAppPorderSub.getDrftAmt().toString());//出票金额
//        pvpAuthorize.setFldvalue04(iqpAccpAppPorderSub.getPyeeAccno());//收款账号
//        pvpAuthorize.setFldvalue05(iqpAccpAppPorderSub.getPyeeName());//收款人姓名
//        pvpAuthorize.setFldvalue06(iqpAccpAppPorderSub.getPyeeAcctsvcrName());//收款行名称
//        pvpAuthorize.setFldvalue07(pvpAccpApp.getContNo());//协议编号
//        pvpAuthorize.setFldvalue08("");//保证金账号
//
//
//         //todo /** 计算保证金金额 和手续费金额*/
////        if(i == (iqpAccpBillList.size() -1)){
////            secAmtThis = BigDecimalUtil.sub(secAmtTotal, secAmtSum, 2, BigDecimal.ROUND_HALF_UP);//保证金
////
////            procFeeAmount2This = BigDecimalUtil.sub(procFeeAmount2, procFeeAmount2Sum, 2, BigDecimal.ROUND_HALF_UP);//手续费(现金)
////
////            procFeeAmountThis = BigDecimalUtil.sub(procFeeAmount, procFeeAmountSum, 2, BigDecimal.ROUND_HALF_UP);//手续费(转账)
////
////        }else{
////            secAmtThis = BigDecimalUtil.mul(bill.getBillAmount(), secRt, 2, BigDecimal.ROUND_HALF_UP);//保证金
////            secAmtSum = BigDecimalUtil.add(secAmtSum, secAmtThis, 2, BigDecimal.ROUND_HALF_UP);//保证金
////
////            procFeeAmount2This = BigDecimalUtil.mul(bill.getBillAmount(), procFeePercent, 2, BigDecimal.ROUND_HALF_UP);//手续费(现金)
////            procFeeAmount2Sum = BigDecimalUtil.add(procFeeAmount2Sum, procFeeAmount2This, 2, BigDecimal.ROUND_HALF_UP);//手续费(现金)
////
////            procFeeAmountThis = BigDecimalUtil.mul(bill.getBillAmount(), procFeePercent, 2, BigDecimal.ROUND_HALF_UP);//手续费(转账)
////            procFeeAmountSum = BigDecimalUtil.add(procFeeAmountSum, procFeeAmountThis, 2, BigDecimal.ROUND_HALF_UP);//手续费(转账)
////        }
////        if(pvpAccp.getProcFeeType().equals("01"))//现金
////        {
////            procFeeAmountThis = 0;//转账
////        }
////        else if (pvpAccp.getProcFeeType().equals("02"))//转账
////        {
////            procFeeAmount2This = 0;//现金
////
////        }
//
//
//        pvpAuthorize.setFldvalue09(secAmtThis + "");
//
//
//        pvpAuthorize.setFldvalue10(iqpAccpAppPorderSub.getEndDate());//汇票到期日
//        pvpAuthorize.setFldvalue11(pvpAccpApp.getChrgRate() + "");//手续费率
//
//        pvpAuthorize.setFldvalue15(pvpAccpApp.getContCurType()); //币别
//        pvpAuthorize.setFldvalue16("");//保证金计息方式
//        pvpAuthorize.setFldvalue17(pvpAccpApp.getReturnDraftInterestType());//保证金退票计息方式
//        pvpAuthorize.setFldvalue18("");//保证金协议月利率
//        pvpAuthorize.setFldvalue19("");//结算账号
//        pvpAuthorize.setFldvalue20("");//结算账号户名
//        pvpAuthorize.setFldvalue21(procFeeAmountThis+"");//手续费金额(转账)(每张票的)
//        pvpAuthorize.setFldvalue22(procFeeAmount2This+"");//手续费金额(现金)(每张票的)
//        pvpAuthorize.setFldvalue23(pvpAccpApp.getBailAmt()+"");//保证金金额
//        pvpAuthorize.setFldvalue24(pvpAccpApp.getApplyPorder()+"");//汇票张数
//        pvpAuthorize.setFldvalue25(pvpAccpApp.getOverdueRate()+"");//逾期垫款利率
//
//        int ii = i+1;
//        String sii = "";
//        if(ii+"".length()==1){
//            sii = "00"+ii;
//        }
//        if(ii+"".length()==2){
//            sii = "0"+ii;
//        }
//        if(ii+"".length()==3){
//            sii = ii+"";
//        }
//        pvpAuthorize.setFldvalue26(sii);//顺序号
//        pvpAuthorize.setFldvalue27(pvpAccpApp.getAorgNo()+"");//承兑机构
//        pvpAuthorize.setFldvalue28(iqpAccpAppPorderSub.getPyeeAccno()+"");//收款人账号
//        pvpAuthorize.setFldvalue29(iqpAccpAppPorderSub.getPyeeName()+"");//收款人户名
//        pvpAuthorize.setFldvalue30(iqpAccpAppPorderSub.getPyeeAcctsvcrName()+"");//收款人开户行
//        pvpAuthorize.setFldvalue31(iqpAccpAppPorderSub.getPyeeName());//收款人开户名
//        pvpAuthorize.setFldvalue32(iqpAccpAppPorderSub.getPyeeAccno());//收款人账号
//        pvpAuthorize.setFldvalue34(iqpAccpAppPorderSub.getPyeeAcctsvcrName());//收款人开户行名称
//        pvpAuthorize.setFldvalue35(pvpAccpApp.getAorgNo());//承兑行编号
//        pvpAuthorize.setFldvalue36(pvpAccpApp.getIssuedOrgNo());//签发机构
//        pvpAuthorize.setFldvalue37(pvpAccpApp.getContCurType());//币种
//        pvpAuthorize.setFldvalue38(pvpAccpApp.getEndDate());//到期日

        return pvpAuthorize;
    }

    /**
     * @方法名称: countPvpAccpAppCountByContNo
     * @方法描述: 根据普通合同编号查询出账申请数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int countPvpAccpAppCountByContNo(Map pvpQueryMap) {
        return pvpAccpAppMapper.countPvpAccpAppCountByContNo(pvpQueryMap);
    }

    /**
     * 出账申请审批中的敞口总额
     *
     * @param pvpAccpAppMap
     * @return
     */
    public BigDecimal queryApprovingSpacAmtByMap(Map pvpAccpAppMap) {
        return pvpAccpAppMapper.queryApprovingSpacAmtByMap(pvpAccpAppMap);
    }

    /**
     * @方法名称: updateAppBySerno
     * @方法描述: 逻辑删除 查询待发起、退回、生效的  电子银行承兑汇票出账申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateAppBySerno(@Param("pvpSerno") String pvpSerno) {
        return pvpAccpAppMapper.updateAppBySerno(pvpSerno);
    }

    /**
     * 出账审批通过尚未生成银承台账的敞口总额
     *
     * @param pvpAccpAppMap
     * @return
     */
    public BigDecimal queryApprovedSpacAmtByMap(Map pvpAccpAppMap) {
        return pvpAccpAppMapper.queryApprovedSpacAmtByMap(pvpAccpAppMap);
    }

    /**
     * 获取当前合同编号下的所有在途申请
     *
     * @param contNo
     * @return
     */
    public List<PvpAccpApp> selectByContNo(String contNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("contNo", contNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectAll(queryModel);
    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 20:33
     **/
    public void createImageSpplInfo(String pvpSerno, String bizType, PvpAccpApp pvpAccpApp, String instanceId,String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(pvpSerno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(pvpSerno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_01);// 影像补扫类型 01:放款影像补扫
            docImageSpplClientDto.setSpplBizType(CmisCommonConstants.STD_SPPL_BIZ_TYPE_02);
            docImageSpplClientDto.setCusId(pvpAccpApp.getCusId());// 客户号
            docImageSpplClientDto.setCusName(pvpAccpApp.getCusName());// 客户名称
            docImageSpplClientDto.setContNo(pvpAccpApp.getContNo());// 合同编号
            docImageSpplClientDto.setInputId(pvpAccpApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(pvpAccpApp.getManagerBrId());// 登记机构
            docImageSpplClientDto.setPrdId(pvpAccpApp.getPrdId());
            docImageSpplClientDto.setPrdName(pvpAccpApp.getPrdName());
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), pvpAccpApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", pvpAccpApp.getCusName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * @方法名称: toSelectAspl
     * @方法描述: 查询资产池的银承台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PvpAccpApp> toSelectAspl(QueryModel queryModel) {
        return pvpAccpAppMapper.toSelectAspl(queryModel);
    }

    /**
     * 1、出账申请通过后，生成归档任务；
     * 2、省心e票业务，网银发起出账，同一合同编号，只生成一次放款类资料归档任务
     *
     * @author jijian_yx
     * @date 2021/9/10 23:52
     **/
    public void createDocArchive(PvpAccpApp pvpAccpApp) {
        String isSxep = pvpAccpApp.getIsSxep();// 是否省心e票
        // 省心e票业务，网银发起出账，同一合同编号，只生成一次放款类资料归档任务
        if ("1".equals(isSxep)) {
            // 判断该笔业务流水号是否已生成在途的归档任务
            Map<String, String> map = new HashMap<>();
            map.put("cusId", pvpAccpApp.getCusId());// 客户号
            map.put("contNo", pvpAccpApp.getContNo());// 合同号
            map.put("taskFlag", "01");// 任务标识 01系统发起;02人工发起
            int count = docArchiveInfoService.selectByBizSerno(map);
            if (count > 1) {
                return;
            }
        }
        log.info("开始系统生成档案归档信息");
        String cusId = pvpAccpApp.getCusId();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
        docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
        docArchiveClientDto.setDocType("14");//14:承兑业务
        docArchiveClientDto.setBizSerno(pvpAccpApp.getPvpSerno());
        docArchiveClientDto.setCusId(cusId);
        docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
        docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
        docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
        docArchiveClientDto.setManagerId(pvpAccpApp.getManagerId());
        docArchiveClientDto.setManagerBrId(pvpAccpApp.getManagerBrId());
        docArchiveClientDto.setInputId(pvpAccpApp.getInputId());
        docArchiveClientDto.setInputBrId(pvpAccpApp.getInputBrId());
        docArchiveClientDto.setContNo(pvpAccpApp.getContNo());
        docArchiveClientDto.setLoanAmt(pvpAccpApp.getAppAmt());
        docArchiveClientDto.setStartDate(pvpAccpApp.getStartDate());
        docArchiveClientDto.setEndDate(pvpAccpApp.getEndDate());
        docArchiveClientDto.setPrdId(pvpAccpApp.getPrdId());
        docArchiveClientDto.setPrdName(pvpAccpApp.getPrdName());
        int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
        if (num < 1) {
            log.info("系统生成承兑出账业务档案归档信息失败,业务流水号[{}]", pvpAccpApp.getPvpSerno());
        }
    }

    public ResultDto<Map<String, String>> selectContByContNo(String contNo) {
        log.info("根据合同号【{}】查询合同信息开始",contNo);
        // 银承合同对象信息
        CtrAccpCont ctrAccpCont = null;
        // 最高额授信协议对象信息
        CtrHighAmtAgrCont ctrHighAmtAgrCont = null;
        // 资产池协议
        CtrAsplDetails ctrAsplDetails = null;
        Map<String,String> map = new HashMap<String,String>();
        ctrAccpCont = ctrAccpContService.selectByContNo(contNo);
        if (Objects.isNull(ctrAccpCont)) {
            ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
            if (Objects.isNull(ctrHighAmtAgrCont)) {
                ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
                if (Objects.isNull(ctrAsplDetails)) {
                    return new ResultDto(null).message("未查询到合同信息").code("9999");
                }else{
                    map.put("iqpSerno",ctrAsplDetails.getSerno());
                }
            }else{
                map.put("iqpSerno",ctrHighAmtAgrCont.getSerno());
            }
        }else{
            map.put("iqpSerno",ctrAccpCont.getSerno());
        }
        return new ResultDto(map);
    }

    /**
     * @方法名称：synAccpDetail
     * @方法描述：同步票据明细
     * @参数与返回说明：
     * @算法描述：
     * @创建人：lihh
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    public ResultDto synAccpDetail(String pcSerno) {
        ResultDto resultDto = new ResultDto<>();
        resultDto.setCode(0);
        Map<String, String> map = new HashMap<>();
        if (StringUtils.isBlank(pcSerno)) {
            map.put("code", "9999");
            map.put("message", "批次号不能为空！");
            resultDto.setMessage("批次号不能为空！");
        } else {
            PvpAccpApp pvpAccpApp = this.selectBySerno(pcSerno);
            if (Objects.isNull(pvpAccpApp)) {
                map.put("code", "9999");
                map.put("message", "根据批次号未查询到银承出账申请！");
                resultDto.setMessage("根据批次号未查询到银承出账申请！");
            } else {
                Xdpj23ReqDto xdpj23ReqDto = new Xdpj23ReqDto();
                xdpj23ReqDto.setsBatchNo(pcSerno);
                xdpj23ReqDto.setTurnPageShowNum("100");
                xdpj23ReqDto.setTurnPageBeginPos("1");
                LocalDateTime now = LocalDateTime.now();
                xdpj23ReqDto.setMac("");//mac地址
                xdpj23ReqDto.setIpaddr("");//ip地址
                xdpj23ReqDto.setServdt(tranDateFormtter.format(now));//交易日期
                xdpj23ReqDto.setServti(tranTimestampFormatter.format(now));//交易时间
                List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> list = null;
                log.info("通过批次号【{}】，前往票据系统查询票据明细开始,请求报文为:【{}】", pcSerno, JSON.toJSONString(xdpj23ReqDto));
                ResultDto<Xdpj23RespDto> xdpj23RespDtoResultDto = dscms2PjxtClientService.xdpj23(xdpj23ReqDto);
                log.info("通过批次号【{}】，前往票据系统查询票据明细结束,响应报文为:【{}】", pcSerno, JSON.toJSONString(xdpj23RespDtoResultDto));
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdpj23RespDtoResultDto.getCode())
                        && Objects.nonNull(xdpj23RespDtoResultDto.getData())) {
                    Xdpj23RespDto data = xdpj23RespDtoResultDto.getData();
                    List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List>  xdpj23List = data.getList();
                    if(CollectionUtils.nonEmpty(xdpj23List)){
                        pvpAccpAppDrftSubService.deleteByPcSerno(pcSerno);
                        for (cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List listData : xdpj23List) {
                            PvpAccpAppDrftSub pvpAccpAppDrftSub = new PvpAccpAppDrftSub();
                            pvpAccpAppDrftSub.setPkId(UUID.randomUUID().toString());//票号
                            pvpAccpAppDrftSub.setSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>()));//业务流水还
                            pvpAccpAppDrftSub.setDrftNo(listData.getBillno());//票号
                            pvpAccpAppDrftSub.setAccpPvpSeq(pcSerno);//批次号
                            pvpAccpAppDrftSub.setDrftAmt(listData.getfBillAmount());//票面金额
                            pvpAccpAppDrftSub.setStartDate(listData.getIsseDt());//出票日期
                            pvpAccpAppDrftSub.setEndDate(listData.getDueDt());//到期日
                            pvpAccpAppDrftSub.setDrwr(listData.getDrwrNm());//出票人
                            pvpAccpAppDrftSub.setPyee(listData.getPyeeNm());//收款人
                            pvpAccpAppDrftSub.setPyeeAccno(listData.getPyeeAcctId());//收款人账号
                            pvpAccpAppDrftSub.setPyeeAcctsvcrName(listData.getPayeeBankName());//收款人开户行名称
                            pvpAccpAppDrftSub.setAccptr(listData.getAccptrNm());//承兑行名称
                            pvpAccpAppDrftSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                            pvpAccpAppDrftSub.setInputId(pvpAccpApp.getInputId());//登记人
                            pvpAccpAppDrftSub.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
                            pvpAccpAppDrftSub.setInputDate(pvpAccpApp.getInputDate());//登记日期
                            pvpAccpAppDrftSub.setManagerId(pvpAccpApp.getManagerId());//责任人
                            pvpAccpAppDrftSub.setManagerBrId(pvpAccpApp.getManagerBrId());//责任机构
                            Date nowDate = DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
                            pvpAccpAppDrftSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                            pvpAccpAppDrftSub.setCreateTime(nowDate);
                            pvpAccpAppDrftSub.setUpdateTime(nowDate);
                            pvpAccpAppDrftSubService.insert(pvpAccpAppDrftSub);
                        }
                    }
                    map.put("code", "0");
                    map.put("message", "同步成功！");
                    resultDto.setMessage("同步成功！");
                } else {
                    map.put("code", "9999");
                    map.put("message", "从票据系统查询票据明细失败！");
                    resultDto.setMessage("从票据系统查询票据明细失败！");
                }
            }
        }
        resultDto.setData(map);
        return resultDto;
    }
}
