package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocCreditArchiveInfo
 * @类描述: doc_credit_archive_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:13:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocCreditArchiveInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 档案流水号 **/
	private String dcaiSerno;
	
	/** 关联业务流水号 **/
	private String bizSerno;
	
	/** 主借款人名称 **/
	private String borrowerCusName;
	
	/** 主借款人证件号码 **/
	private String borrowerCertCode;
	
	/** 征信查询对象名称 **/
	private String cusName;
	
	/** 征信查询对象证件号码 **/
	private String certCode;
	
	/** 授权书日期 **/
	private String authbookDate;
	
	/** 查询人 **/
	private String qryUser;
	
	/** 查询机构 **/
	private String qryOrg;
	
	/** 生成日期 **/
	private String createDate;
	
	/** 接收人 **/
	private String receiverId;
	
	/** 接收日期 **/
	private String receiverDate;
	
	/** 核对日期 **/
	private String checkDate;
	
	/** 征信档案状态 **/
	private String docStauts;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 核对人 **/
	private String checkId;
	
	
	/**
	 * @param dcaiSerno
	 */
	public void setDcaiSerno(String dcaiSerno) {
		this.dcaiSerno = dcaiSerno == null ? null : dcaiSerno.trim();
	}
	
    /**
     * @return DcaiSerno
     */	
	public String getDcaiSerno() {
		return this.dcaiSerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param borrowerCusName
	 */
	public void setBorrowerCusName(String borrowerCusName) {
		this.borrowerCusName = borrowerCusName == null ? null : borrowerCusName.trim();
	}
	
    /**
     * @return BorrowerCusName
     */	
	public String getBorrowerCusName() {
		return this.borrowerCusName;
	}
	
	/**
	 * @param borrowerCertCode
	 */
	public void setBorrowerCertCode(String borrowerCertCode) {
		this.borrowerCertCode = borrowerCertCode == null ? null : borrowerCertCode.trim();
	}
	
    /**
     * @return BorrowerCertCode
     */	
	public String getBorrowerCertCode() {
		return this.borrowerCertCode;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param authbookDate
	 */
	public void setAuthbookDate(String authbookDate) {
		this.authbookDate = authbookDate == null ? null : authbookDate.trim();
	}
	
    /**
     * @return AuthbookDate
     */	
	public String getAuthbookDate() {
		return this.authbookDate;
	}
	
	/**
	 * @param qryUser
	 */
	public void setQryUser(String qryUser) {
		this.qryUser = qryUser == null ? null : qryUser.trim();
	}
	
    /**
     * @return QryUser
     */	
	public String getQryUser() {
		return this.qryUser;
	}
	
	/**
	 * @param qryOrg
	 */
	public void setQryOrg(String qryOrg) {
		this.qryOrg = qryOrg == null ? null : qryOrg.trim();
	}
	
    /**
     * @return QryOrg
     */	
	public String getQryOrg() {
		return this.qryOrg;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate == null ? null : createDate.trim();
	}
	
    /**
     * @return CreateDate
     */	
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId == null ? null : receiverId.trim();
	}
	
    /**
     * @return ReceiverId
     */	
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverDate
	 */
	public void setReceiverDate(String receiverDate) {
		this.receiverDate = receiverDate == null ? null : receiverDate.trim();
	}
	
    /**
     * @return ReceiverDate
     */	
	public String getReceiverDate() {
		return this.receiverDate;
	}
	
	/**
	 * @param checkDate
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate == null ? null : checkDate.trim();
	}
	
    /**
     * @return CheckDate
     */	
	public String getCheckDate() {
		return this.checkDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts == null ? null : docStauts.trim();
	}
	
    /**
     * @return DocStauts
     */	
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param checkId
	 */
	public void setCheckId(String checkId) {
		this.checkId = checkId == null ? null : checkId.trim();
	}
	
    /**
     * @return CheckId
     */	
	public String getCheckId() {
		return this.checkId;
	}


}