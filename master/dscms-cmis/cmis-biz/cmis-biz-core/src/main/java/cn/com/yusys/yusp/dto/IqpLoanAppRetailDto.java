package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-06 14:13:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanAppRetailDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 证件号码 **/
	private String certCode;

	/** 手机号码 **/
	private String phone;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 贷款形式 **/
	private String loanModal;

	/** 合同类型 **/
	private String contType;

	/** 送达地址 **/
	private String deliveryAddr;

	private String appChnl;
	private String managerId;
	private String managerBrId;
	private String inputId;
	private String inputBrId;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getLoanModal() {
		return loanModal;
	}

	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType;
	}

	public String getDeliveryAddr() {
		return deliveryAddr;
	}

	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}


	public String getAppChnl() {
		return appChnl;
	}

	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}


}