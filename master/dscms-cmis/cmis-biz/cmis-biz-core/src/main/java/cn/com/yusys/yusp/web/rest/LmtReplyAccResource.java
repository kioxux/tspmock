/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.LmtGrpReplyAcc;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;
import cn.com.yusys.yusp.service.LmtReplyAccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyAcc;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplyacc")
public class LmtReplyAccResource {
    @Autowired
    private LmtReplyAccService lmtReplyAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyAcc> list = lmtReplyAccService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyAcc>> index(QueryModel queryModel) {
        List<LmtReplyAcc> list = lmtReplyAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyAcc> show(@PathVariable("pkId") String pkId) {
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyAcc> create(@RequestBody LmtReplyAcc lmtReplyAcc) throws URISyntaxException {
        lmtReplyAccService.insert(lmtReplyAcc);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyAcc lmtReplyAcc) throws URISyntaxException {
        int result = lmtReplyAccService.update(lmtReplyAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
       * @方法名称：replyAccForManager
       * @方法描述：当前登录人下的批复台账信息
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-29 上午 9:19
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/replyaccformanager")
    protected ResultDto<List<LmtReplyAcc>> replyAccForManager(@RequestBody QueryModel queryModel) {
        List<LmtReplyAcc> lmtReplyAccList = lmtReplyAccService.selectForManager(queryModel);
        return new ResultDto<>(lmtReplyAccList);
    }

    /**
     * @方法名称: getLmtReplyAccByAccNo
     * @方法描述: 根据台账编号获取授信台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtreplyaccbyaccno")
    protected ResultDto<LmtReplyAcc> getLmtReplyAccByAccNo(String accno) {
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(accno);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }

    /**
     * @方法名称: getLmtAccList
     * @方法描述: 获取当前客户经理名下所有授信台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtacclist")
    protected ResultDto<List<LmtReplyAcc>> getLmtAccList(@RequestBody QueryModel queryModel) {
        List<LmtReplyAcc> list = lmtReplyAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAcc>>(list);
    }

    /**
     * @方法名称: getReplyAccForRefine
     * @方法描述: 过滤得到可进行预授信变更的授信批复台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getreplyaccforrefine")
    protected ResultDto<List<LmtReplyAcc>> getReplyAccForRefine(@RequestBody QueryModel params) {
        List<LmtReplyAcc> list = lmtReplyAccService.getReplyAccForRefine(params);
        return new ResultDto<List<LmtReplyAcc>>(list);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:单一客户额度查询
     * @参数与返回说明:
     * @param queryModel
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<LmtReplyAcc>> queryAll(@RequestBody QueryModel queryModel) {
        List<LmtReplyAcc> list = lmtReplyAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyAcc>>(list);
    }

    /**
     * @方法名称: getReplyAccForRefine
     * @方法描述: 根据批复编号获取批复台账详情信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getAccNoData")
    protected ResultDto<LmtReplyAcc> getAccNoData(@RequestBody String replySerno) {
        HashMap map = new HashMap();
        map.put("replySerno",replySerno);
        map.put("oprType",CommonConstance.OPR_TYPE_ADD);
        map.put("accStatus",CommonConstance.ACC_STATUS_01);
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }

    /**
     * @方法名称: queryLmtReplyAccDataByParams
     * @方法描述: 根据入参获取批复台账详情信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/querylmtreplyaccdatabyparams")
    protected ResultDto<LmtReplyAcc> queryLmtReplyAccDataByParams(@RequestBody String cusId) {
        HashMap map = new HashMap();
        map.put("cusId",cusId);
        map.put("oprType",CommonConstance.OPR_TYPE_ADD);
        map.put("accStatus",CommonConstance.ACC_STATUS_01);
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }

    /**
     * @方法名称: getLmtReplyAccBySingleSerno
     * @方法描述: 根据申请流水号查询授信批复台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getlmtreplyaccbysingleserno")
    protected ResultDto<List<LmtReplyAcc>> getLmtReplyAccBySingleSerno(@RequestBody String singleSerno) {
        List<LmtReplyAcc> lmtReplyAccByGrpSerno = lmtReplyAccService.getLmtReplyAccBySingleSerno(singleSerno);
        return new ResultDto<>(lmtReplyAccByGrpSerno);
    }

    /**
     * @方法名称：getallreplyaccinfo
     * @方法描述：获取批复台账所有数据，包括分项、产品、用信条件
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-04-28 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getallreplyaccinfo")
    protected ResultDto<Map> getAllReplyAccInfo(@RequestBody String accNo) {
        Map map = lmtReplyAccService.getAllReplyAccInfo(accNo);
        return new ResultDto<Map>(map);
    }

    /**
     * @方法名称: isExistMultipleAndLowRiskLmt
     * @方法描述: 根据客户编号判断是否存在综合授信额度以及低风险分项
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/isexistmultipleandlowrisklmt")
    protected ResultDto<Map> isExistMultipleAndLowRiskLmt(@RequestBody Map map) {
        Map responseMap = lmtReplyAccService.isExistMultipleAndLowRiskLmt(map);
        return ResultDto.success(responseMap);
    }

    /**
     * @方法名称：getLastLmtReplyAcc
     * @方法描述：获取客户最新的生效的批复台账
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-10-28 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getlastreplyacc")
    protected ResultDto<LmtReplyAcc> getLastLmtReplyAcc(@RequestBody String cusId) {
        LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(cusId);
        return new ResultDto<LmtReplyAcc>(lmtReplyAcc);
    }
}
