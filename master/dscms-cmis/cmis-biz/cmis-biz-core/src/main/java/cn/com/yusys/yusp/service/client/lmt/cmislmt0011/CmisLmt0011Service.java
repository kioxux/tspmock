package cn.com.yusys.yusp.service.client.lmt.cmislmt0011;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class CmisLmt0011Service
 * @date 2021/8/25 19:10
 * @desc 占用合同额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0011Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0011Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0011ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0011.resp.CmisLmt0011RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 占用合同额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0011RespDto cmisLmt0011(CmisLmt0011ReqDto cmisLmt0011ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, JSON.toJSONString(cmisLmt0011ReqDto));
        ResultDto<CmisLmt0011RespDto> cmisLmt0011ResultDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value, JSON.toJSONString(cmisLmt0011ResultDto));

        String cmisLmt0011Code = Optional.ofNullable(cmisLmt0011ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0011Meesage = Optional.ofNullable(cmisLmt0011ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0011RespDto cmisLmt0011RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0011ResultDto.getCode()) && cmisLmt0011ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0011ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0011RespDto = cmisLmt0011ResultDto.getData();
        } else {
            if (cmisLmt0011ResultDto.getData() != null) {
                cmisLmt0011Code = cmisLmt0011ResultDto.getData().getErrorCode();
                cmisLmt0011Meesage = cmisLmt0011ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0011Code = EpbEnum.EPB099999.key;
                cmisLmt0011Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0011Code, cmisLmt0011Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0011.value);
        return cmisLmt0011RespDto;
    }
}
