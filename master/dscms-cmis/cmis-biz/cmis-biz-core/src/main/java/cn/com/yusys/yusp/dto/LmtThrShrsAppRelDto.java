package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtThrShrsAppRel
 * @类描述: lmt_thr_shrs_app_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-23 16:40:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtThrShrsAppRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 类型 STD_ZB_CORRE_TYP **/
	private String type;
	
	/** 所属机构编号 **/
	private String belgOrgNo;
	
	/** 所属机构名称 **/
	private String belgOrgName;
	
	/** 操作类型 STD_ZB_OPR_TYP **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}
	
    /**
     * @return Type
     */	
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param belgOrgNo
	 */
	public void setBelgOrgNo(String belgOrgNo) {
		this.belgOrgNo = belgOrgNo == null ? null : belgOrgNo.trim();
	}
	
    /**
     * @return BelgOrgNo
     */	
	public String getBelgOrgNo() {
		return this.belgOrgNo;
	}
	
	/**
	 * @param belgOrgName
	 */
	public void setBelgOrgName(String belgOrgName) {
		this.belgOrgName = belgOrgName == null ? null : belgOrgName.trim();
	}
	
    /**
     * @return BelgOrgName
     */	
	public String getBelgOrgName() {
		return this.belgOrgName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}