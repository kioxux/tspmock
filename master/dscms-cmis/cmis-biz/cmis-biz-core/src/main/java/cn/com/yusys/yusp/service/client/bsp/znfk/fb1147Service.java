package cn.com.yusys.yusp.service.client.bsp.znfk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.req.Fb1147ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.resp.Fb1147RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：无还本续贷申请
 *
 * @author quwen
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class fb1147Service {
    private static final Logger logger = LoggerFactory.getLogger(fb1147Service.class);

    // 1）注入：BSP封装调用智能风控系统的接口
    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    /**
     * 业务逻辑处理方法：无还本续贷申请
     *
     * @param fb1147ReqDto
     * @return
     */
    @Transactional
    public Fb1147RespDto fb1147(Fb1147ReqDto fb1147ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value, JSON.toJSONString(fb1147ReqDto));
        ResultDto<Fb1147RespDto> fb1147RespDtoResultDto = dscms2CircpClientService.fb1147(fb1147ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value, JSON.toJSONString(fb1147RespDtoResultDto));

        String fb1147Code = Optional.ofNullable(fb1147RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fb1147Meesage = Optional.ofNullable(fb1147RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fb1147RespDto fb1147RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fb1147RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            fb1147RespDto = fb1147RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, fb1147Code, fb1147Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value);
        return fb1147RespDto;
    }
}
