/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AccEntrustLoanTxnDetailsDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.server.xddh0007.Xddh0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccEntrustLoanTxnDetails;
import cn.com.yusys.yusp.service.AccEntrustLoanTxnDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccEntrustLoanTxnDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-02 21:36:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accentrustloantxndetails")
public class AccEntrustLoanTxnDetailsResource {
    @Autowired
    private AccEntrustLoanTxnDetailsService accEntrustLoanTxnDetailsService;

    @Autowired
    private Xddh0007Service xddh0007Service;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccEntrustLoanTxnDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccEntrustLoanTxnDetails> list = accEntrustLoanTxnDetailsService.selectAll(queryModel);
        return new ResultDto<List<AccEntrustLoanTxnDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccEntrustLoanTxnDetails>> index(QueryModel queryModel) {
        List<AccEntrustLoanTxnDetails> list = accEntrustLoanTxnDetailsService.selectByModel(queryModel);
        return new ResultDto<List<AccEntrustLoanTxnDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccEntrustLoanTxnDetails> show(@PathVariable("pkId") String pkId) {
        AccEntrustLoanTxnDetails accEntrustLoanTxnDetails = accEntrustLoanTxnDetailsService.selectByPrimaryKey(pkId);
        return new ResultDto<AccEntrustLoanTxnDetails>(accEntrustLoanTxnDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccEntrustLoanTxnDetails> create(@RequestBody AccEntrustLoanTxnDetails accEntrustLoanTxnDetails) throws URISyntaxException {
        accEntrustLoanTxnDetailsService.insert(accEntrustLoanTxnDetails);
        return new ResultDto<AccEntrustLoanTxnDetails>(accEntrustLoanTxnDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccEntrustLoanTxnDetails accEntrustLoanTxnDetails) throws URISyntaxException {
        int result = accEntrustLoanTxnDetailsService.update(accEntrustLoanTxnDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accEntrustLoanTxnDetailsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accEntrustLoanTxnDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccEntrustLoanTxnDetails>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccEntrustLoanTxnDetails> list = accEntrustLoanTxnDetailsService.selectByModel(queryModel);
        return new ResultDto<List<AccEntrustLoanTxnDetails>>(list);
    }

    /**
     * @author 刘权
     **/
    @ApiOperation("交易明细")
    @PostMapping("/queryAllBillDetail")
    protected ResultDto<List<AccEntrustLoanTxnDetailsDto>> queryAllBillDetail(@RequestBody QueryModel queryModel) {
        List<AccEntrustLoanTxnDetailsDto> list = accEntrustLoanTxnDetailsService.queryAllBillDetail(queryModel);
        return new ResultDto<List<AccEntrustLoanTxnDetailsDto>>(list);
    }
}
