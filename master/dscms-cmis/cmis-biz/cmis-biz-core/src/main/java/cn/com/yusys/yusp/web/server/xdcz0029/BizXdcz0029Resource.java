package cn.com.yusys.yusp.web.server.xdcz0029;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0029.req.Xdcz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.resp.Xdcz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0029.Xdcz0029Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:网银推送省心快贷审核任务至信贷
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0029:网银推送省心快贷审核任务至信贷")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0029Resource.class);

    @Autowired
    private Xdcz0029Service xdcz0029Service;
    /**
     * 交易码：xdcz0029
     * 交易描述：网银推送省心快贷审核任务至信贷
     *
     * @param xdcz0029DataReqDto
     * @throws Exception
     * @return xdcz0029DataResultDto
     */
    @ApiOperation("网银推送省心快贷审核任务至信贷")
    @PostMapping("/xdcz0029")
    protected @ResponseBody
    ResultDto<Xdcz0029DataRespDto>  xdcz0029(@Validated @RequestBody Xdcz0029DataReqDto xdcz0029DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029DataReqDto));
        Xdcz0029DataRespDto  xdcz0029DataRespDto  = new Xdcz0029DataRespDto();// 响应Dto:小贷用途承诺书文本生成pdf
        ResultDto<Xdcz0029DataRespDto>xdcz0029DataResultDto = new ResultDto<>();
        String flag = xdcz0029DataReqDto.getFlag();//标志
        String cusId = xdcz0029DataReqDto.getCusId();//客户编号
        String cusName = xdcz0029DataReqDto.getCusName();//客户名称
        String billNo = xdcz0029DataReqDto.getBillNo();//借据号
        String contNo = xdcz0029DataReqDto.getContNo();//合同号
        BigDecimal loanAmt = xdcz0029DataReqDto.getLoanAmt();//放款金额
        String loanStartDate = xdcz0029DataReqDto.getLoanStartDate();//发放日期
        String loanEndDate = xdcz0029DataReqDto.getLoanEndDate();//到期日期
        String loanAcctNo = xdcz0029DataReqDto.getLoanAcctNo();//放款账号
        String entruPayAcctNo = xdcz0029DataReqDto.getEntruPayAcctNo();//受托支付账号
        String entruPayAcctName = xdcz0029DataReqDto.getEntruPayAcctName();//受托支付账号中文
        BigDecimal amt = xdcz0029DataReqDto.getAmt();//金额
        String isBankFlag = xdcz0029DataReqDto.getIsBankFlag();//是否行内行外
        String acctsvcrAcctNo = xdcz0029DataReqDto.getAcctsvcrAcctNo();//开户行号
        String acctsvcrName = xdcz0029DataReqDto.getAcctsvcrName();//开户行名
        String entruPayAcctNoNum = xdcz0029DataReqDto.getEntruPayAcctNoNum();//受托支付账户数
        try {
            // 从xdcz0029DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdcz0029DataReqDto));
            xdcz0029DataRespDto = xdcz0029Service.getSxkdInfo(xdcz0029DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdcz0029DataRespDto));
            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0029DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdcz0029DataRespDto对象结束
            // 封装xdcz0029DataResultDto中正确的返回码和返回信息
            xdcz0029DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0029DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value,e.getMessage());
            // 封装xdcz0029DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0029DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0029DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0029DataRespDto到xdcz0029DataResultDto中
        xdcz0029DataResultDto.setData(xdcz0029DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029DataRespDto));
        return xdcz0029DataResultDto;
    }
}
