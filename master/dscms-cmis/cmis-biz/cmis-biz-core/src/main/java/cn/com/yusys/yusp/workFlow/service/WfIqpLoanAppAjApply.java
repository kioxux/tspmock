package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.IqpAppReplyService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * @功能描述:零售按揭类业务申请逻辑
 * @param
 * @author: 王玉坤
 * @date: 19:29 2021/4/13
 * @return:
 */
@Service
public class WfIqpLoanAppAjApply implements ClientBizInterface {
    private final Logger logger = LoggerFactory.getLogger(WfIqpLoanAppAjApply.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAppReplyService iqpAppReplyService;

    @Autowired
    private CtrLoanContService ctrLoanContService;


    /**
     * @param resultInstanceDto
     * @return void
     * @author 王玉坤
     * @date 2021/5/4 20:10
     * @version 1.0.0
     * @desc 零售按揭类业务流程处理逻辑
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String grtSerno = resultInstanceDto.getBizId();
        logger.info("后业务处理类型{}", currentOpType);
        IqpLoanApp iqpLoanApp = null;
        try {
            iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(grtSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程发起操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程提交操作，流程参数【{}】", grtSerno, resultInstanceDto);
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                iqpLoanAppService.update(iqpLoanApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程跳转操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程结束操作，流程参数【{}】", grtSerno, resultInstanceDto);
                // 针对流程到办结节点，进行以下处理
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                iqpLoanAppService.update(iqpLoanApp);
                // 业务逻辑处理
                iqpLoanAppService.handleBizEnd(iqpLoanApp);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程退回操作，流程参数【{}】", grtSerno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程打回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                iqpLoanAppService.update(iqpLoanApp);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程拿回操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                iqpLoanAppService.update(iqpLoanApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                logger.info("零售按揭类业务申请【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                iqpLoanAppService.update(iqpLoanApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                // 否决改变标志 审批中 111-> 审批不通过 998
                logger.info("零售按揭类业务申请【{}】，流程拿回初始节点操作，流程参数【{}】", grtSerno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                iqpLoanAppService.update(iqpLoanApp);
            } else {
                logger.warn("担保变更申请" + grtSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            logger.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                logger.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "WF_IQP_LOAN_APP_AJ".equals(flowCode);
    }
}
