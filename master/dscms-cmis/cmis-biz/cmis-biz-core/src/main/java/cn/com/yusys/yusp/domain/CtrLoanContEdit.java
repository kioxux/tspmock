package cn.com.yusys.yusp.domain;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/4/23 16:02
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class CtrLoanContEdit {
    private CtrLoanCont ctrLoanCont;
    private List<RepayCapPlan> repayCapPlanList;
    private List<IqpLoanAppDramPlanSub> iqpLoanAppDramPlanSubList;

    public CtrLoanCont getCtrLoanCont() {
        return ctrLoanCont;
    }

    public void setCtrLoanCont(CtrLoanCont ctrLoanCont) {
        this.ctrLoanCont = ctrLoanCont;
    }


    public List<IqpLoanAppDramPlanSub> getIqpLoanAppDramPlanSubList() {
        return iqpLoanAppDramPlanSubList;
    }

    public void setIqpLoanAppDramPlanSubList(List<IqpLoanAppDramPlanSub> iqpLoanAppDramPlanSubList) {
        this.iqpLoanAppDramPlanSubList = iqpLoanAppDramPlanSubList;
    }
    public List<RepayCapPlan> getRepayCapPlanList() {
        return repayCapPlanList;
    }

    public void setRepayCapPlanList(List<RepayCapPlan> repayCapPlanList) {
        this.repayCapPlanList = repayCapPlanList;
    }
}
