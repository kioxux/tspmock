/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalApply
 * @类描述: guar_eval_apply数据实体类
 * @功能描述: 
 * @创建人: PvpLoanAppistrator
 * @创建时间: 2021-03-26 15:21:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalApplyOtherInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	private String guarNo;
	private String evalOutDate;
	private String inputDate;
	private String evalOrgName;
	private String linkName;
	private String linkmanFphone;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getEvalOutDate() {
		return evalOutDate;
	}

	public void setEvalOutDate(String evalOutDate) {
		this.evalOutDate = evalOutDate;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getEvalOrgName() {
		return evalOrgName;
	}

	public void setEvalOrgName(String evalOrgName) {
		this.evalOrgName = evalOrgName;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkmanFphone() {
		return linkmanFphone;
	}

	public void setLinkmanFphone(String linkmanFphone) {
		this.linkmanFphone = linkmanFphone;
	}
}