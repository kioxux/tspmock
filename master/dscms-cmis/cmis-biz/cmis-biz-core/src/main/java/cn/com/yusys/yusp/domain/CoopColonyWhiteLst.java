/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopColonyWhiteLst
 * @类描述: coop_colony_white_lst数据实体类
 * @功能描述:
 * @创建人: AbsonZ
 * @创建时间: 2021-04-16 15:47:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_colony_white_lst")
public class CoopColonyWhiteLst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 合作方案编号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "PK_ID")
    private String pkId;

    /**
     * 合作方案编号
     **/
    @Column(name = "coop_plan_no", unique = false, nullable = true, length = 40)
    private String coopPlanNo;

    /**
     * 合作方编号
     **/
    @Column(name = "partner_no", unique = false, nullable = true, length = 60)
    private String partnerNo;

    /**
     * 合作方名称
     **/
    @Column(name = "partner_name", unique = false, nullable = true, length = 120)
    private String partnerName;

    /**
     * 关联方类型
     **/
    @Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 20)
    private String partnerType;

    /**
     * 客户编号
     **/
    @Column(name = "cus_id", unique = false, nullable = true, length = 60)
    private String cusId;

    /**
     * 客户名称
     **/
    @Column(name = "cus_name", unique = false, nullable = true, length = 120)
    private String cusName;

    /**
     * 证件类型
     **/
    @Column(name = "cert_type", unique = false, nullable = true, length = 10)
    private String certType;

    /**
     * 证件号码
     **/
    @Column(name = "cert_code", unique = false, nullable = true, length = 60)
    private String certCode;

    /**
     * 操作时间
     **/
    @Column(name = "opr_time", unique = false, nullable = true, length = 19)
    private java.util.Date oprTime;

    /**
     * 操作类型
     **/
    @Column(name = "opr_type", unique = false, nullable = true, length = 10)
    private String oprType;

    /**
     * 登记人
     **/
    @Column(name = "input_id", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "input_br_id", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "input_date", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "upd_id", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "upd_date", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "create_time", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "update_time", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;


    /**
     * @param coopPlanNo
     */
    public void setCoopPlanNo(String coopPlanNo) {
        this.coopPlanNo = coopPlanNo;
    }

    /**
     * @return coopPlanNo
     */
    public String getCoopPlanNo() {
        return this.coopPlanNo;
    }

    /**
     * @param partnerNo
     */
    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    /**
     * @return partnerNo
     */
    public String getPartnerNo() {
        return this.partnerNo;
    }

    /**
     * @param partnerName
     */
    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    /**
     * @return partnerName
     */
    public String getPartnerName() {
        return this.partnerName;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * @return certType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param oprTime
     */
    public void setOprTime(java.util.Date oprTime) {
        this.oprTime = oprTime;
    }

    /**
     * @return oprTime
     */
    public java.util.Date getOprTime() {
        return this.oprTime;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
}