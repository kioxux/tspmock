package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfLivingRoom;

public class GuarBasicGuarInfLivingRoomVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfLivingRoom guarInfLivingRoom;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfLivingRoom getGuarInfLivingRoom() {
        return guarInfLivingRoom;
    }

    public void setGuarInfLivingRoom(GuarInfLivingRoom guarInfLivingRoom) {
        this.guarInfLivingRoom = guarInfLivingRoom;
    }
}
