/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysMcd;
import cn.com.yusys.yusp.service.RptSpdAnysMcdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-23 23:15:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysmcd")
public class RptSpdAnysMcdResource {
    @Autowired
    private RptSpdAnysMcdService rptSpdAnysMcdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysMcd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysMcd> list = rptSpdAnysMcdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysMcd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysMcd>> index(QueryModel queryModel) {
        List<RptSpdAnysMcd> list = rptSpdAnysMcdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysMcd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysMcd> show(@PathVariable("serno") String serno) {
        RptSpdAnysMcd rptSpdAnysMcd = rptSpdAnysMcdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysMcd>(rptSpdAnysMcd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysMcd> create(@RequestBody RptSpdAnysMcd rptSpdAnysMcd) throws URISyntaxException {
        rptSpdAnysMcdService.insert(rptSpdAnysMcd);
        return new ResultDto<RptSpdAnysMcd>(rptSpdAnysMcd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysMcd rptSpdAnysMcd) throws URISyntaxException {
        int result = rptSpdAnysMcdService.update(rptSpdAnysMcd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysMcdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysMcdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysMcd> selectBySerno(@RequestBody String serno){
        return new ResultDto<RptSpdAnysMcd>(rptSpdAnysMcdService.selectByPrimaryKey(serno));
    }

    @PostMapping("/initMcd")
    protected ResultDto<RptSpdAnysMcd> initMcd(@RequestBody Map map){
        return new ResultDto<RptSpdAnysMcd>(rptSpdAnysMcdService.initMcd(map));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptSpdAnysMcd rptSpdAnysMcd){
        return new ResultDto<Integer>(rptSpdAnysMcdService.update(rptSpdAnysMcd));
    }
}
