/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtHighCurfundEval;
import cn.com.yusys.yusp.service.LmtHighCurfundEvalService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: LmtHighCurfundEvalResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-01 19:14:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmthighcurfundeval")
public class LmtHighCurfundEvalResource {
    @Autowired
    private LmtHighCurfundEvalService lmtHighCurfundEvalService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtHighCurfundEval>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtHighCurfundEval> list = lmtHighCurfundEvalService.selectAll(queryModel);
        return new ResultDto<List<LmtHighCurfundEval>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtHighCurfundEval>> index(QueryModel queryModel) {
        List<LmtHighCurfundEval> list = lmtHighCurfundEvalService.selectByModel(queryModel);
        return new ResultDto<List<LmtHighCurfundEval>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtHighCurfundEval> show(@PathVariable("pkId") String pkId) {
        LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtHighCurfundEval>(lmtHighCurfundEval);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtHighCurfundEval> create(@RequestBody LmtHighCurfundEval lmtHighCurfundEval) throws URISyntaxException {
        lmtHighCurfundEvalService.insert(lmtHighCurfundEval);
        return new ResultDto<LmtHighCurfundEval>(lmtHighCurfundEval);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtHighCurfundEval lmtHighCurfundEval) throws URISyntaxException {
        int result = lmtHighCurfundEvalService.update(lmtHighCurfundEval);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtHighCurfundEvalService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtHighCurfundEvalService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }



    /**
       * @方法名称：calOptAmt
       * @方法描述：测算运营资金量
       * @参数与返回说明：
       * @算法描述：调用智能风控接口
       * @创建人：zhangming12
       * @创建时间：2021-04-10 下午 1:34
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/calOptAmt")
    protected ResultDto<Map> calOptAmt(@RequestBody LmtHighCurfundEval lmtHighCurfundEval) {
        Map result = lmtHighCurfundEvalService.calOptAmt(lmtHighCurfundEval);
        return new ResultDto<Map>(result);
    }

    /**
       * @方法名称：calLimit
       * @方法描述：测算额度
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-15 下午 3:11
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/calLimit")
    protected ResultDto<Map> calLimit(@RequestBody LmtHighCurfundEval lmtHighCurfundEval) {
        Map result = lmtHighCurfundEvalService.calLimit(lmtHighCurfundEval);
        return new ResultDto<Map>(result);
    }

    /**
       * @方法名称：insertSelective
       * @方法描述：保存测算数据
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-15 下午 9:51
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/insertSelective")
    protected ResultDto<LmtHighCurfundEval> insertSelective(@RequestBody LmtHighCurfundEval lmtHighCurfundEval) throws URISyntaxException {
        lmtHighCurfundEvalService.insertSelective(lmtHighCurfundEval);
        return new ResultDto<LmtHighCurfundEval>(lmtHighCurfundEval);
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyserno/{serno}")
    protected ResultDto<LmtHighCurfundEval> selectBySerno(@PathVariable("serno") String serno) {
        LmtHighCurfundEval lmtHighCurfundEval = lmtHighCurfundEvalService.selectBySerno(serno);
        return new ResultDto<LmtHighCurfundEval>(lmtHighCurfundEval);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<LmtHighCurfundEval> selectEvalBySerno(@RequestBody String serno){
        return new ResultDto<LmtHighCurfundEval>(lmtHighCurfundEvalService.selectBySerno(serno));
    }
}
