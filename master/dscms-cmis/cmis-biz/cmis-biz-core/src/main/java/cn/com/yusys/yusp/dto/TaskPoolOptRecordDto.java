package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskPoolOptRecord
 * @类描述: task_pool_opt_record数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class TaskPoolOptRecordDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	private String taskNo;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 业务类型 **/
	private String bizType;
	
	/** 业务类型细分 **/
	private String bizSubType;
	
	/** 任务类型 **/
	private String taskType;
	
	/** 操作类型   **/
	private String optType;
	
	/** 操作人 **/
	private String optUsr;
	
	/** 操作机构 **/
	private String optOrg;
	
	/** 操作时间 **/
	private String optTime;
	
	/** 操作原因 **/
	private String optReason;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo == null ? null : taskNo.trim();
	}
	
    /**
     * @return TaskNo
     */	
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType == null ? null : bizType.trim();
	}
	
    /**
     * @return BizType
     */	
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizSubType
	 */
	public void setBizSubType(String bizSubType) {
		this.bizSubType = bizSubType == null ? null : bizSubType.trim();
	}
	
    /**
     * @return BizSubType
     */	
	public String getBizSubType() {
		return this.bizSubType;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType == null ? null : taskType.trim();
	}
	
    /**
     * @return TaskType
     */	
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param optType
	 */
	public void setOptType(String optType) {
		this.optType = optType == null ? null : optType.trim();
	}
	
    /**
     * @return OptType
     */	
	public String getOptType() {
		return this.optType;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr == null ? null : optUsr.trim();
	}
	
    /**
     * @return OptUsr
     */	
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg == null ? null : optOrg.trim();
	}
	
    /**
     * @return OptOrg
     */	
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param optTime
	 */
	public void setOptTime(String optTime) {
		this.optTime = optTime == null ? null : optTime.trim();
	}
	
    /**
     * @return OptTime
     */	
	public String getOptTime() {
		return this.optTime;
	}
	
	/**
	 * @param optReason
	 */
	public void setOptReason(String optReason) {
		this.optReason = optReason == null ? null : optReason.trim();
	}
	
    /**
     * @return OptReason
     */	
	public String getOptReason() {
		return this.optReason;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}