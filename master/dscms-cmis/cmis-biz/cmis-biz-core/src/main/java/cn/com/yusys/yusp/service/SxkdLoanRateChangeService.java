/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.domain.SxkdLoanRateChange;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.SxkdLoanRateChangeMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdLoanRateChangeService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-17 21:47:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class SxkdLoanRateChangeService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Autowired
    private SxkdLoanRateChangeMapper sxkdLoanRateChangeMapper;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public SxkdLoanRateChange selectByPrimaryKey(String serno) {
        return sxkdLoanRateChangeMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<SxkdLoanRateChange> selectAll(QueryModel model) {
        List<SxkdLoanRateChange> records = (List<SxkdLoanRateChange>) sxkdLoanRateChangeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<SxkdLoanRateChange> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<SxkdLoanRateChange> list = sxkdLoanRateChangeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(SxkdLoanRateChange record) {
        return sxkdLoanRateChangeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(SxkdLoanRateChange record) {
        return sxkdLoanRateChangeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(SxkdLoanRateChange record) {
        return sxkdLoanRateChangeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(SxkdLoanRateChange record) {
        return sxkdLoanRateChangeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return sxkdLoanRateChangeMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return sxkdLoanRateChangeMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: insertSxkdLoanRateChange
     * @方法描述: 省心快贷利率修改新增
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zl
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map insertSxkdLoanRateChange(SxkdLoanRateChange record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String serno = "";
        try {
            // 获取创建，修改日期
            record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            // 生成新流水
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            record.setSerno(serno);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            int count = sxkdLoanRateChangeMapper.insert(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷利率修改新增失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存房抵e点贷授信押品关联数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public SxkdLoanRateChange selectBySerno(String serno) {
        return sxkdLoanRateChangeMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: sxkdLoanRateChangedelete
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map sxkdLoanRateChangedelete(SxkdLoanRateChange sxkdLoanRateChange) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String serno = sxkdLoanRateChange.getSerno();
            log.info(String.format("根据主键%s对省心快贷利率修改进行删除", serno));
            sxkdLoanRateChangeMapper.deleteByPrimaryKey(serno);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除对省心快贷利率修改出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: selectSxkdLoanRateChangelist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（省心快贷线上合同修改申请）
     * @参数与返回说明:
     * @创建者: zl
     * @算法描述: 无
     */
    @Transactional
    public List<SxkdLoanRateChange> selectSxkdLoanRateChangelist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return sxkdLoanRateChangeMapper.selectSxkdLoanRateChangelist(model);
    }

    /**
     * @方法名称: selectSxkdLoanRateChangeHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（省心快贷利率修改历史）
     * @参数与返回说明:
     * @创建者: zl
     * @算法描述: 无
     */
    @Transactional
    public List<SxkdLoanRateChange> selectSxkdLoanRateChangeHislist(QueryModel model) {
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return sxkdLoanRateChangeMapper.selectSxkdLoanRateChangelist(model);
    }
}
