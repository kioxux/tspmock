/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.domain.LmtReplyAccNeedSub;
import cn.com.yusys.yusp.domain.LmtReplyLoanCond;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyAccLoanCond;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccLoanCondMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccLoanCondService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 16:06:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccLoanCondService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyAccLoanCondService.class);

    @Autowired
    private LmtReplyAccLoanCondMapper lmtReplyAccLoanCondMapper;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyAccLoanCond selectByPrimaryKey(String pkId) {
        return lmtReplyAccLoanCondMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyAccLoanCond> selectAll(QueryModel model) {
        List<LmtReplyAccLoanCond> records = (List<LmtReplyAccLoanCond>) lmtReplyAccLoanCondMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyAccLoanCond> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccLoanCond> list = lmtReplyAccLoanCondMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtReplyAccLoanCond record) {
        return lmtReplyAccLoanCondMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyAccLoanCond record) {
        return lmtReplyAccLoanCondMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReplyAccLoanCond record) {
        return lmtReplyAccLoanCondMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyAccLoanCond record) {
        return lmtReplyAccLoanCondMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccLoanCondMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyAccLoanCondMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplyAccLoanCondByParams
     * @方法描述: 通过条件查询
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-09-22 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyAccLoanCond> queryLmtReplyAccLoanCondByParams(HashMap<String, String> queryMap){
        return lmtReplyAccLoanCondMapper.queryLmtReplyAccLoanCondByParams(queryMap);
    }

    /**
     * @方法名称: generateNewLmtReplyAccNeedSubHandle
     * @方法描述: 生成全新的授信台账管理要求
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-22 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplyAccLoanCondHandle(LmtReply lmtReply, String accNo) {
        HashMap<String, String> lmtReplyLoanCondMap = new HashMap<String, String>();
        lmtReplyLoanCondMap.put("replySerno", lmtReply.getReplySerno());
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByParams(lmtReplyLoanCondMap);
        for (LmtReplyLoanCond lmtReplyLoanCond : lmtReplyLoanCondList) {
            generateNewLmtReplyAccLoanCondByLmtReplyLoanCond(lmtReplyLoanCond, accNo);
        }
    }

    /**
     * @方法名称: generateNewLmtReplyAccLoanCondByLmtReplyLoanCond
     * @方法描述: 生成全新的授信台账管理要求
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-09-22 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplyAccLoanCondByLmtReplyLoanCond(LmtReplyLoanCond lmtReplyLoanCond, String accNo) {
        LmtReplyAccLoanCond lmtReplyAccLoanCond = new LmtReplyAccLoanCond();
        BeanUtils.copyProperties(lmtReplyLoanCond, lmtReplyAccLoanCond);
        lmtReplyAccLoanCond.setPkId(UUID.randomUUID().toString());
        lmtReplyAccLoanCond.setAccNo(accNo);
        this.insert(lmtReplyAccLoanCond);
        log.info("生成新的批复台账管理条件:" + lmtReplyAccLoanCond);
    }

    /**
     * @方法名称: updateLmtReplyAccLoanCondByLmtReplyNeed
     * @方法描述: 根据批复管理要求生成批复台账的管理要求
     * @参数与返回说明:
     * @算法描述: 根据批复台账删除原管理要求内容，插入新的管理要求。
     * @创建人: zhangliang15
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccLoanCondByLmtReplyNeed(LmtReply lmtReply, String accNo) {
        HashMap<String, String> lmtReplyLoanCondMap = new HashMap<String, String>();
        lmtReplyLoanCondMap.put("replySerno", lmtReply.getReplySerno());
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByParams(lmtReplyLoanCondMap);

        lmtReplyAccLoanCondMapper.deleteByAccNo(accNo);

        for (LmtReplyLoanCond lmtReplyLoanCond : lmtReplyLoanCondList) {
            this.generateNewLmtReplyAccLoanCondByLmtReplyLoanCond(lmtReplyLoanCond, accNo);
        }
    }
}
