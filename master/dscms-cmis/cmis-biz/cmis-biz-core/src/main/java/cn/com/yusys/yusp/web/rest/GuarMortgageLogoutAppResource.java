/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarMortgageLogoutApp;
import cn.com.yusys.yusp.service.GuarMortgageLogoutAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageLogoutAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-22 09:44:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarmortgagelogoutapp")
public class GuarMortgageLogoutAppResource {
    @Autowired
    private GuarMortgageLogoutAppService guarMortgageLogoutAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query")
    protected ResultDto<List<GuarMortgageLogoutApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarMortgageLogoutApp> list = guarMortgageLogoutAppService.selectAll(queryModel);
        return new ResultDto<List<GuarMortgageLogoutApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/all")
    protected ResultDto<List<GuarMortgageLogoutApp>> index(QueryModel queryModel) {
        List<GuarMortgageLogoutApp> list = guarMortgageLogoutAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarMortgageLogoutApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<GuarMortgageLogoutApp> show(@PathVariable("serno") String serno) {
        GuarMortgageLogoutApp guarMortgageLogoutApp = guarMortgageLogoutAppService.selectByPrimaryKey(serno);
        return new ResultDto<GuarMortgageLogoutApp>(guarMortgageLogoutApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarMortgageLogoutApp> create(@RequestBody GuarMortgageLogoutApp guarMortgageLogoutApp) throws URISyntaxException {
        guarMortgageLogoutAppService.insert(guarMortgageLogoutApp);
        return new ResultDto<GuarMortgageLogoutApp>(guarMortgageLogoutApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarMortgageLogoutApp guarMortgageLogoutApp) throws URISyntaxException {
        int result = guarMortgageLogoutAppService.update(guarMortgageLogoutApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody GuarMortgageLogoutApp guarMortgageLogoutApp) throws URISyntaxException {
        int result = guarMortgageLogoutAppService.updateSelective(guarMortgageLogoutApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarMortgageLogoutAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarMortgageLogoutAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchupdate
     * @函数描述:批量对象逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchupdate/{ids}")
    protected ResultDto<Integer> updates(@PathVariable String ids) {
        int result = guarMortgageLogoutAppService.updateByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @GetMapping("/getSequences")
    protected ResultDto getSequences(){
        ResultDto result = guarMortgageLogoutAppService.getSequences();
        return result;
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertSelective")
    protected ResultDto<GuarMortgageLogoutApp> insertSelective(@RequestBody GuarMortgageLogoutApp guarMortgageLogoutApp) throws URISyntaxException {
        guarMortgageLogoutAppService.insertSelective(guarMortgageLogoutApp);
        return new ResultDto<GuarMortgageLogoutApp>(guarMortgageLogoutApp);
    }

    /**
     * @函数名称:deleteOnLogic
     * @函数描述:单个对象逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteOnLogic")
    protected ResultDto<Integer> deleteOnLogic(@RequestBody GuarMortgageLogoutApp record) {
        int result = guarMortgageLogoutAppService.updateSelective(record);
        return new ResultDto<Integer>(result);
    }
}
