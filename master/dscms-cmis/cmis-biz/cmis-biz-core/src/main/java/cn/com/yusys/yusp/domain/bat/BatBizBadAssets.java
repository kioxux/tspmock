/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.bat;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizBadAssets
 * @类描述: bat_biz_bad_assets数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_biz_bad_assets")
public class BatBizBadAssets extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = false, length = 30)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 30)
	private String managerBrId;
	
	/** 本月新增不良资产户数 **/
	@Column(name = "MONTH_ADD_BADCUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer monthAddBadcusCount;
	
	/** 本月新增不良资产笔数 **/
	@Column(name = "MONTH_ADD_BADACC_COUNT", unique = false, nullable = true, length = 10)
	private Integer monthAddBadaccCount;
	
	/** 本月新增不良资产余额 **/
	@Column(name = "MONTH_ADD_BADTOTAL_BALANCE", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal monthAddBadtotalBalance;
	
	/** 本年度新增不良资产户数 **/
	@Column(name = "YEAR_ADD_BADCUS_COUNT", unique = false, nullable = true, length = 10)
	private Integer yearAddBadcusCount;
	
	/** 本年度新增不良资产笔数 **/
	@Column(name = "YEAR_ADD_BADACC_COUNT", unique = false, nullable = true, length = 10)
	private Integer yearAddBadaccCount;
	
	/** 本年度新增不良资产余额 **/
	@Column(name = "YEAR_ADD_BADTOTAL_BALANCE", unique = false, nullable = true, length = 21)
	private java.math.BigDecimal yearAddBadtotalBalance;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param monthAddBadcusCount
	 */
	public void setMonthAddBadcusCount(Integer monthAddBadcusCount) {
		this.monthAddBadcusCount = monthAddBadcusCount;
	}
	
    /**
     * @return monthAddBadcusCount
     */
	public Integer getMonthAddBadcusCount() {
		return this.monthAddBadcusCount;
	}
	
	/**
	 * @param monthAddBadaccCount
	 */
	public void setMonthAddBadaccCount(Integer monthAddBadaccCount) {
		this.monthAddBadaccCount = monthAddBadaccCount;
	}
	
    /**
     * @return monthAddBadaccCount
     */
	public Integer getMonthAddBadaccCount() {
		return this.monthAddBadaccCount;
	}
	
	/**
	 * @param monthAddBadtotalBalance
	 */
	public void setMonthAddBadtotalBalance(java.math.BigDecimal monthAddBadtotalBalance) {
		this.monthAddBadtotalBalance = monthAddBadtotalBalance;
	}
	
    /**
     * @return monthAddBadtotalBalance
     */
	public java.math.BigDecimal getMonthAddBadtotalBalance() {
		return this.monthAddBadtotalBalance;
	}
	
	/**
	 * @param yearAddBadcusCount
	 */
	public void setYearAddBadcusCount(Integer yearAddBadcusCount) {
		this.yearAddBadcusCount = yearAddBadcusCount;
	}
	
    /**
     * @return yearAddBadcusCount
     */
	public Integer getYearAddBadcusCount() {
		return this.yearAddBadcusCount;
	}
	
	/**
	 * @param yearAddBadaccCount
	 */
	public void setYearAddBadaccCount(Integer yearAddBadaccCount) {
		this.yearAddBadaccCount = yearAddBadaccCount;
	}
	
    /**
     * @return yearAddBadaccCount
     */
	public Integer getYearAddBadaccCount() {
		return this.yearAddBadaccCount;
	}
	
	/**
	 * @param yearAddBadtotalBalance
	 */
	public void setYearAddBadtotalBalance(java.math.BigDecimal yearAddBadtotalBalance) {
		this.yearAddBadtotalBalance = yearAddBadtotalBalance;
	}
	
    /**
     * @return yearAddBadtotalBalance
     */
	public java.math.BigDecimal getYearAddBadtotalBalance() {
		return this.yearAddBadtotalBalance;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}