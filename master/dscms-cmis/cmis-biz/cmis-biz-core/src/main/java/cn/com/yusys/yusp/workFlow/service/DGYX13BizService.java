package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CashAdvance;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.SxkdLoanRateChange;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 垫款申请审批流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX13BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX13BizService.class);//定义log

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CashAdvanceService cashAdvanceService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 保函垫款审批流程或信用证垫款审批流程
        if ("YX021".equals(bizType)||"YX022".equals(bizType)){
            pvpLoanAppBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/8/26 21:38
     * @version 1.0.0
     * @desc 省心快贷利率修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void pvpLoanAppBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            CashAdvance cashAdvance = cashAdvanceService.selectByPrimaryKey(serno);
            log.info("垫款申请审批流程:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("垫款申请审批流程:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("垫款申请审批流程:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("垫款申请审批流程:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("垫款申请审批流程:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("垫款申请审批流程:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("垫款申请审批流程:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                cashAdvance.setTradeStatus("1");
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_997);
                // 判断是否无缝衔接
                String isFollowBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
                // 原交易业务编号
                String origiDealBizNo = "";
                // 原交易业务状态
                String origiDealBizStatus = "";
                // 原交易恢复类型
                String origiRecoverType = "";
                // 原交易属性
                String origiBizAttr = "";
                //发台账占用接口占合同额度
                CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
                cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(cashAdvance.getManagerBrId()));//金融机构代码
                String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, new HashMap<>());
                cmisLmt0013ReqDto.setSerno(pkId);//交易流水号
                cmisLmt0013ReqDto.setBizNo(cashAdvance.getContNo());//合同编号
                cmisLmt0013ReqDto.setInputId(cashAdvance.getInputId());//登记人
                cmisLmt0013ReqDto.setInputBrId(cashAdvance.getInputBrId());//登记机构
                cmisLmt0013ReqDto.setInputDate(cashAdvance.getInputDate());//登记日期

                List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDto = new ArrayList<CmisLmt0013ReqDealBizListDto>();
                CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizDto = new CmisLmt0013ReqDealBizListDto();
                cmisLmt0013ReqDealBizDto.setDealBizNo(cashAdvance.getBillNo());//台账编号
                cmisLmt0013ReqDealBizDto.setIsFollowBiz(isFollowBiz);//是否无缝衔接
                cmisLmt0013ReqDealBizDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0013ReqDealBizDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0013ReqDealBizDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0013ReqDealBizDto.setOrigiBizAttr(origiBizAttr);//原交易属性
                cmisLmt0013ReqDealBizDto.setCusId(cashAdvance.getCusId());//客户编号
                cmisLmt0013ReqDealBizDto.setCusName(cashAdvance.getCusName());//客户名称
                cmisLmt0013ReqDealBizDto.setPrdId(cashAdvance.getPrdId());//产品编号
                cmisLmt0013ReqDealBizDto.setPrdName(cashAdvance.getPrdName());//产品名称
                cmisLmt0013ReqDealBizDto.setPrdTypeProp(cashAdvance.getPrdTypeProp());//产品类型属性
                cmisLmt0013ReqDealBizDto.setDealBizAmtCny(cashAdvance.getCashAmt());//台账占用总额(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizSpacAmtCny(cashAdvance.getCashAmt());//台账占用敞口(人民币)
                cmisLmt0013ReqDealBizDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                cmisLmt0013ReqDealBizDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金金额
                cmisLmt0013ReqDealBizDto.setStartDate(cashAdvance.getLoanStartDate());//台账起始日
                cmisLmt0013ReqDealBizDto.setEndDate(cashAdvance.getLoanStartDate());//垫款台账起始日=台账到期日
                cmisLmt0013ReqDealBizDto.setDealBizStatus(CmisLmtConstants.STD_ZB_BIZ_STATUS_100);//台账状态
                cmisLmt0013ReqDealBizListDto.add(cmisLmt0013ReqDealBizDto);
                cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDto);
                log.info("放款申请【{}】前往额度系统进行额度占用开始,请求报文为【{}】", cashAdvance, cmisLmt0013ReqDealBizDto.toString());
                ResultDto<CmisLmt0013RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
                log.info("放款申请【{}】前往额度系统进行额度占用结束,响应报文为【{}】", cashAdvance, resultDtoDto);
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("接口调用异常！");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(resultDtoDto.getData().getErrorCode())) {
                    log.error("业务申请占用额度异常,错误信息为【{}】！", resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null, resultDtoDto.getData().getErrorCode(), resultDtoDto.getData().getErrorMsg());
                }

            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("垫款申请审批流程:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(cashAdvance, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("垫款申请审批流程:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 流程审批状态更新
     * 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (CashAdvance cashAdvance, String approveStatus){
        cashAdvance.setApproveStatus(approveStatus);
        cashAdvanceService.updateSelective(cashAdvance);
    }


    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return ("DGYX13").equals(flowCode);
    }
}
