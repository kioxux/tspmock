/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyReportComInfo
 * @类描述: survey_report_com_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-12 13:55:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "survey_report_com_info")
public class SurveyReportComInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_NO")
	private String surveyNo;
	
	/** 企业名称 **/
	@Column(name = "CON_NAME", unique = false, nullable = true, length = 80)
	private String conName;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 80)
	private String legal;
	
	/** 经营地址 **/
	@Column(name = "OPER_ADDR", unique = false, nullable = true, length = 500)
	private String operAddr;
	
	/** 经营期限 **/
	@Column(name = "OPER_TERM", unique = false, nullable = true, length = 20)
	private String operTerm;
	
	/** 主营业务 **/
	@Column(name = "MAIN_BUS", unique = false, nullable = true, length = 40)
	private String mainBus;
	
	/** 企业类型 **/
	@Column(name = "CON_TYPE", unique = false, nullable = true, length = 5)
	private String conType;
	
	/** 营业执照年限 **/
	@Column(name = "BLIC_YEARS", unique = false, nullable = true, length = 20)
	private String blicYears;
	
	/** 行业 **/
	@Column(name = "TRADE", unique = false, nullable = true, length = 5)
	private String trade;
	
	/** 企业证件类型 **/
	@Column(name = "CON_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String conCertType;
	
	/** 企业组织机构代码 **/
	@Column(name = "CON_CMON_CODE", unique = false, nullable = true, length = 40)
	private String conCmonCode;
	
	/** 企业统一社会信用代码 **/
	@Column(name = "CON_UNIFY_SOC_CDT_CODE", unique = false, nullable = true, length = 40)
	private String conUnifySocCdtCode;
	
	/** 实际经营地址 **/
	@Column(name = "ACT_OPER_ADDR", unique = false, nullable = true, length = 80)
	private String actOperAddr;
	
	/** 实际行业 **/
	@Column(name = "ACT_TRADE", unique = false, nullable = true, length = 5)
	private String actTrade;
	
	/** 是否实际经营人 **/
	@Column(name = "IS_ACT_OPER_PERSON", unique = false, nullable = true, length = 1)
	private String isActOperPerson;
	
	/** 经营是否正常 **/
	@Column(name = "OPER_IS_NORMAL", unique = false, nullable = true, length = 1)
	private String operIsNormal;
	
	/** 司法审核结果 **/
	@Column(name = "JUD_AUDIT_RST", unique = false, nullable = true, length = 20)
	private String judAuditRst;
	
	/** 企业当前税务信用评级 **/
	@Column(name = "CON_CURT_TAX_CDT_EVAL", unique = false, nullable = true, length = 5)
	private String conCurtTaxCdtEval;
	
	/** 企业近1年综合应纳税额 **/
	@Column(name = "CON_LATEST_1YEAR_INTE_TAXDUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal conLatest1yearInteTaxdue;
	
	/** 企业近1年税前利润率 **/
	@Column(name = "CON_LATEST_1YEAR_PRETAX_PROFIT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal conLatest1yearPretaxProfitRate;
	
	/** 企业近1年销售收入 **/
	@Column(name = "CON_LATEST_1YEAR_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal conLatest1yearSaleIncome;
	
	/** 税务模型评级结果 **/
	@Column(name = "TAX_MODEL_EVAL_RST", unique = false, nullable = true, length = 5)
	private String taxModelEvalRst;
	
	/** 税务模型评分 **/
	@Column(name = "TAX_MODEL_GRADE", unique = false, nullable = true, length = 5)
	private String taxModelGrade;
	
	/** 模型建议金额 **/
	@Column(name = "MODEL_ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceAmt;
	
	/** 模型建议利率 **/
	@Column(name = "MODEL_ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceRate;
	
	/** 参考利率 **/
	@Column(name = "REF_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal refRate;
	
	/** 建议金额 **/
	@Column(name = "ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	@Column(name = "ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceRate;
	
	/** 建议期限 **/
	@Column(name = "ADVICE_TERM", unique = false, nullable = true, length = 20)
	private String adviceTerm;
	
	/** 是否续贷 **/
	@Column(name = "IS_CONTINU_LOAN", unique = false, nullable = true, length = 1)
	private String isContinuLoan;
	
	/** 是否现场勘验 **/
	@Column(name = "IS_IOTSPOT", unique = false, nullable = true, length = 1)
	private String isIotspot;
	
	/** 模型初步结果 **/
	@Column(name = "MODEL_FST_RST", unique = false, nullable = true, length = 5)
	private String modelFstRst;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 申请企业完整纳税月份数 **/
	@Column(name = "TAX_MONTH", unique = false, nullable = true, length = 10)
	private String taxMonth;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	
    /**
     * @return surveyNo
     */
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param conName
	 */
	public void setConName(String conName) {
		this.conName = conName;
	}
	
    /**
     * @return conName
     */
	public String getConName() {
		return this.conName;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr;
	}
	
    /**
     * @return operAddr
     */
	public String getOperAddr() {
		return this.operAddr;
	}
	
	/**
	 * @param operTerm
	 */
	public void setOperTerm(String operTerm) {
		this.operTerm = operTerm;
	}
	
    /**
     * @return operTerm
     */
	public String getOperTerm() {
		return this.operTerm;
	}
	
	/**
	 * @param mainBus
	 */
	public void setMainBus(String mainBus) {
		this.mainBus = mainBus;
	}
	
    /**
     * @return mainBus
     */
	public String getMainBus() {
		return this.mainBus;
	}
	
	/**
	 * @param conType
	 */
	public void setConType(String conType) {
		this.conType = conType;
	}
	
    /**
     * @return conType
     */
	public String getConType() {
		return this.conType;
	}
	
	/**
	 * @param blicYears
	 */
	public void setBlicYears(String blicYears) {
		this.blicYears = blicYears;
	}
	
    /**
     * @return blicYears
     */
	public String getBlicYears() {
		return this.blicYears;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}
	
    /**
     * @return trade
     */
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param conCertType
	 */
	public void setConCertType(String conCertType) {
		this.conCertType = conCertType;
	}
	
    /**
     * @return conCertType
     */
	public String getConCertType() {
		return this.conCertType;
	}
	
	/**
	 * @param conCmonCode
	 */
	public void setConCmonCode(String conCmonCode) {
		this.conCmonCode = conCmonCode;
	}
	
    /**
     * @return conCmonCode
     */
	public String getConCmonCode() {
		return this.conCmonCode;
	}
	
	/**
	 * @param conUnifySocCdtCode
	 */
	public void setConUnifySocCdtCode(String conUnifySocCdtCode) {
		this.conUnifySocCdtCode = conUnifySocCdtCode;
	}
	
    /**
     * @return conUnifySocCdtCode
     */
	public String getConUnifySocCdtCode() {
		return this.conUnifySocCdtCode;
	}
	
	/**
	 * @param actOperAddr
	 */
	public void setActOperAddr(String actOperAddr) {
		this.actOperAddr = actOperAddr;
	}
	
    /**
     * @return actOperAddr
     */
	public String getActOperAddr() {
		return this.actOperAddr;
	}
	
	/**
	 * @param actTrade
	 */
	public void setActTrade(String actTrade) {
		this.actTrade = actTrade;
	}
	
    /**
     * @return actTrade
     */
	public String getActTrade() {
		return this.actTrade;
	}
	
	/**
	 * @param isActOperPerson
	 */
	public void setIsActOperPerson(String isActOperPerson) {
		this.isActOperPerson = isActOperPerson;
	}
	
    /**
     * @return isActOperPerson
     */
	public String getIsActOperPerson() {
		return this.isActOperPerson;
	}
	
	/**
	 * @param operIsNormal
	 */
	public void setOperIsNormal(String operIsNormal) {
		this.operIsNormal = operIsNormal;
	}
	
    /**
     * @return operIsNormal
     */
	public String getOperIsNormal() {
		return this.operIsNormal;
	}
	
	/**
	 * @param judAuditRst
	 */
	public void setJudAuditRst(String judAuditRst) {
		this.judAuditRst = judAuditRst;
	}
	
    /**
     * @return judAuditRst
     */
	public String getJudAuditRst() {
		return this.judAuditRst;
	}
	
	/**
	 * @param conCurtTaxCdtEval
	 */
	public void setConCurtTaxCdtEval(String conCurtTaxCdtEval) {
		this.conCurtTaxCdtEval = conCurtTaxCdtEval;
	}
	
    /**
     * @return conCurtTaxCdtEval
     */
	public String getConCurtTaxCdtEval() {
		return this.conCurtTaxCdtEval;
	}
	
	/**
	 * @param conLatest1yearInteTaxdue
	 */
	public void setConLatest1yearInteTaxdue(java.math.BigDecimal conLatest1yearInteTaxdue) {
		this.conLatest1yearInteTaxdue = conLatest1yearInteTaxdue;
	}
	
    /**
     * @return conLatest1yearInteTaxdue
     */
	public java.math.BigDecimal getConLatest1yearInteTaxdue() {
		return this.conLatest1yearInteTaxdue;
	}
	
	/**
	 * @param conLatest1yearPretaxProfitRate
	 */
	public void setConLatest1yearPretaxProfitRate(java.math.BigDecimal conLatest1yearPretaxProfitRate) {
		this.conLatest1yearPretaxProfitRate = conLatest1yearPretaxProfitRate;
	}
	
    /**
     * @return conLatest1yearPretaxProfitRate
     */
	public java.math.BigDecimal getConLatest1yearPretaxProfitRate() {
		return this.conLatest1yearPretaxProfitRate;
	}
	
	/**
	 * @param conLatest1yearSaleIncome
	 */
	public void setConLatest1yearSaleIncome(java.math.BigDecimal conLatest1yearSaleIncome) {
		this.conLatest1yearSaleIncome = conLatest1yearSaleIncome;
	}
	
    /**
     * @return conLatest1yearSaleIncome
     */
	public java.math.BigDecimal getConLatest1yearSaleIncome() {
		return this.conLatest1yearSaleIncome;
	}
	
	/**
	 * @param taxModelEvalRst
	 */
	public void setTaxModelEvalRst(String taxModelEvalRst) {
		this.taxModelEvalRst = taxModelEvalRst;
	}
	
    /**
     * @return taxModelEvalRst
     */
	public String getTaxModelEvalRst() {
		return this.taxModelEvalRst;
	}
	
	/**
	 * @param taxModelGrade
	 */
	public void setTaxModelGrade(String taxModelGrade) {
		this.taxModelGrade = taxModelGrade;
	}
	
    /**
     * @return taxModelGrade
     */
	public String getTaxModelGrade() {
		return this.taxModelGrade;
	}
	
	/**
	 * @param modelAdviceAmt
	 */
	public void setModelAdviceAmt(java.math.BigDecimal modelAdviceAmt) {
		this.modelAdviceAmt = modelAdviceAmt;
	}
	
    /**
     * @return modelAdviceAmt
     */
	public java.math.BigDecimal getModelAdviceAmt() {
		return this.modelAdviceAmt;
	}
	
	/**
	 * @param modelAdviceRate
	 */
	public void setModelAdviceRate(java.math.BigDecimal modelAdviceRate) {
		this.modelAdviceRate = modelAdviceRate;
	}
	
    /**
     * @return modelAdviceRate
     */
	public java.math.BigDecimal getModelAdviceRate() {
		return this.modelAdviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return refRate
     */
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return adviceAmt
     */
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return adviceRate
     */
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm;
	}
	
    /**
     * @return adviceTerm
     */
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param isContinuLoan
	 */
	public void setIsContinuLoan(String isContinuLoan) {
		this.isContinuLoan = isContinuLoan;
	}
	
    /**
     * @return isContinuLoan
     */
	public String getIsContinuLoan() {
		return this.isContinuLoan;
	}
	
	/**
	 * @param isIotspot
	 */
	public void setIsIotspot(String isIotspot) {
		this.isIotspot = isIotspot;
	}
	
    /**
     * @return isIotspot
     */
	public String getIsIotspot() {
		return this.isIotspot;
	}
	
	/**
	 * @param modelFstRst
	 */
	public void setModelFstRst(String modelFstRst) {
		this.modelFstRst = modelFstRst;
	}
	
    /**
     * @return modelFstRst
     */
	public String getModelFstRst() {
		return this.modelFstRst;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param taxMonth
	 */
	public void setTaxMonth(String taxMonth) {
		this.taxMonth = taxMonth;
	}
	
    /**
     * @return taxMonth
     */
	public String getTaxMonth() {
		return this.taxMonth;
	}


}