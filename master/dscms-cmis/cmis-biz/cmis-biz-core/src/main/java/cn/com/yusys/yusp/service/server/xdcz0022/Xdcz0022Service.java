package cn.com.yusys.yusp.service.server.xdcz0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.server.xdcz0022.req.Xdcz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.resp.Xdcz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * 接口处理类:风控发送相关信息至信贷进行支用校验
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0022Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0022Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 业务场景：对公智能风控系统放款前校验
     * 接口说明：信贷根据请求参数中的合同号、申请借据金额、申请借据起始日期、申请借据到期日期校验合同余额、授信余额是否充足、借据起止日期是否符合要求
     * 查询合同金额、合同下已发放贷款余额、合同对应授信分项金额
     * 业务逻辑：
     * 1.合同可用余额校验
     * 	1).获取合同金额apply_amount，根据请求的合同编号CONT_NO查询合同表CTR_LOAN_CONT;
     *  2).获取当前合同下贷款总余额SUM(LOAN_BALANCE)，根据CONT_NO查询贷款台账表ACC_LOAN;
     *  3).计算合同可用额度，ht_avail_amt=apply_amount-SUM(LOAN_BALANCE);
     *  4).满足(ht_avail_amt<0 || 申请借据金额>ht_avail_amt)时,提示合同额度不足并拦截
     * 2.授信合规检查，调用额度系统校验接口
     * 3.日期检查
     * 	1).申请借据起始日期不能早于当前日期
     * 	2).申请借据到期日期不能晚于合同到期日
     *
     * 备注:对应老系统代码Xdfk11Action.java
     * @author lihh
     * @param xdcz0022DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0022DataRespDto sendMessageToCheck(Xdcz0022DataReqDto xdcz0022DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataReqDto));
        Xdcz0022DataRespDto xdcz0022DataRespDto = new Xdcz0022DataRespDto();
        String cont_no = xdcz0022DataReqDto.getCont_no();
        BigDecimal apply_amount = Optional.ofNullable(xdcz0022DataReqDto.getApply_amount()).orElse(BigDecimal.ZERO);
        String loan_start_date = xdcz0022DataReqDto.getLoan_start_date();
        String loan_end_date = xdcz0022DataReqDto.getLoan_end_date();
        try {
            // 获取合同金额apply_amount，根据请求的合同编号CONT_NO查询合同表CTR_LOAN_CONT;
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectContByContno(cont_no);
            String contStatus = Optional.ofNullable(ctrLoanCont.getContStatus()).orElse(StringUtils.EMPTY);
            // 合同状态不是生效状态的（注销、作废、中止、撤回等）
            if (!CommonConstance.STD_ZB_CONT_TYP_200.equals(contStatus)) {
                throw new BizException(null, "", null, "该合同不是生效状态");
            }
            BigDecimal contAmount = Optional.ofNullable(ctrLoanCont.getHighAvlAmt()).orElse(BigDecimal.ZERO);
            String htEndDate = Optional.ofNullable(ctrLoanCont.getContEndDate()).orElse(StringUtils.EMPTY);
            // 获取当前合同下贷款总余额SUM(LOAN_BALANCE)，根据CONT_NO查询贷款台账表ACC_LOAN;
            BigDecimal loanBalance = Optional.ofNullable(accLoanMapper.getSumBalanceByContNo(cont_no)).orElse(BigDecimal.ZERO);
            // 计算合同可用额度，ht_avail_amt=apply_amount-SUM(LOAN_BALANCE);
            BigDecimal ht_avail_amt = contAmount.subtract(loanBalance);
            // 满足(ht_avail_amt<0 || 申请借据金额>ht_avail_amt)时,提示合同额度不足并拦截
            if (BigDecimal.ZERO.compareTo(ht_avail_amt) > 0 || apply_amount.compareTo(ht_avail_amt) > 0) {
                throw new BizException(null, "", null, "合同额度不足");
            } else {

                /**  日期检查
                 * 	1).申请借据起始日期不能早于当前日期
                 * 	2).申请借据到期日期不能晚于合同到期日
                 */
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                LocalDate now = LocalDate.parse(openDay, dateTimeFormatter);
                if (now.isAfter(LocalDate.parse(loan_start_date, dateTimeFormatter))) {
                    throw new BizException(null, "", null, "申请日期不能早于当前日期");
                }

                if (LocalDate.parse(loan_end_date, dateTimeFormatter).isAfter(LocalDate.parse(htEndDate, dateTimeFormatter))) {
                    throw new BizException(null, "", null, "支用到期日期不能晚于合同到期日期");
                }

            }
            xdcz0022DataRespDto.setOpFlag(CmisBizConstants.YES);
            xdcz0022DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, e.getMessage());
            throw new BizException(null, "", null, e.getMessage());
        } catch (Exception e) {
            xdcz0022DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdcz0022DataRespDto.setOpMsg("系统异常");
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataRespDto));
        return xdcz0022DataRespDto;
    }
}
