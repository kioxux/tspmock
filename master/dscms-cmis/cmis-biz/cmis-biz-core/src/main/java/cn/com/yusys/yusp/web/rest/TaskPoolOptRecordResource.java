/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.TaskPoolOptRecord;
import cn.com.yusys.yusp.service.TaskPoolOptRecordService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskPoolOptRecordResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/taskpooloptrecord")
public class TaskPoolOptRecordResource {
    @Autowired
    private TaskPoolOptRecordService taskPoolOptRecordService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<TaskPoolOptRecord>> query() {
        QueryModel queryModel = new QueryModel();
        List<TaskPoolOptRecord> list = taskPoolOptRecordService.selectAll(queryModel);
        return new ResultDto<List<TaskPoolOptRecord>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<TaskPoolOptRecord>> index(@RequestBody QueryModel queryModel) {
        List<TaskPoolOptRecord> list = taskPoolOptRecordService.selectByModel(queryModel);
        return new ResultDto<List<TaskPoolOptRecord>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<TaskPoolOptRecord> show(@PathVariable("taskNo") String taskNo) {
        TaskPoolOptRecord taskPoolOptRecord = taskPoolOptRecordService.selectByPrimaryKey(taskNo);
        return new ResultDto<TaskPoolOptRecord>(taskPoolOptRecord);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<TaskPoolOptRecord> create(@RequestBody TaskPoolOptRecord taskPoolOptRecord) throws URISyntaxException {
        taskPoolOptRecordService.insert(taskPoolOptRecord);
        return new ResultDto<TaskPoolOptRecord>(taskPoolOptRecord);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody TaskPoolOptRecord taskPoolOptRecord) throws URISyntaxException {
        int result = taskPoolOptRecordService.update(taskPoolOptRecord);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = taskPoolOptRecordService.deleteByPrimaryKey(taskNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = taskPoolOptRecordService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
