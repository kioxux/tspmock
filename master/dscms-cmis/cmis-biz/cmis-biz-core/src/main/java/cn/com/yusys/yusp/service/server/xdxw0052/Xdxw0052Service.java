package cn.com.yusys.yusp.service.server.xdxw0052;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0052.req.Xdxw0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0052.resp.Xdxw0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0052Service
 * @类描述: #查询优抵贷客户调查表
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0052Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0052Service.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;//调查报告基本信息
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;//客户授信调查主表
    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;//调查报告企业信息
    @Autowired
    private LmtSurveyReportOtherInfoMapper lmtSurveyReportOtherInfoMapper;//调查报告其他信息
    @Autowired
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;//调查结论表
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;//贷款合同表
    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 查询优抵贷客户调查表
     *
     * @param xdxw0052DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0052DataRespDto xdxw0052(Xdxw0052DataReqDto xdxw0052DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value);
        Xdxw0052DataRespDto xdxw0052DataRespDto = new Xdxw0052DataRespDto();
        try {
            String queryType = xdxw0052DataReqDto.getQueryType();//查询类型
            String indgtSerno = xdxw0052DataReqDto.getIndgtSerno();//客户调查表编号
            String applySerno = xdxw0052DataReqDto.getApplySerno();//业务唯一编号
            String surveySerno = StringUtils.EMPTY;
            //查询条件
            QueryModel queryModel = new QueryModel();

            // 2021年10月21日21:30:40 hubp queryType为02，则是无还本，需通过其传入的applySerno，查出调查流水号
            if ("02".equals(queryType)) {
                logger.info("***********进入无还本续贷处理逻辑***********查询类型：【{}】，业务唯一编号：【{}】", queryType ,applySerno);
                if (StringUtils.isBlank(applySerno)) {
                    throw BizException.error(null, "9999", "查询类型为02，则业务唯一编号不能为空");
                } else {
                    logger.info("***********通过业务唯一编号查找调查主表信息***********业务唯一编号：【{}】", applySerno);
                    LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByListSerno(applySerno);
                    logger.info("***********通过业务唯一编号查找调查主表***********返回信息：【{}】", JSON.toJSONString(mainInfo));
                    if (Objects.isNull(mainInfo)) {
                        throw BizException.error(null, "9999", "查询类型为02，通过调查业务唯一编号未查询到调查主表信息");
                    }
                    surveySerno = mainInfo.getSurveySerno();
                    queryModel.addCondition("surveySerno", surveySerno);
                }
            } else {
                logger.info("***********进入优抵贷处理逻辑***********查询类型：【{}】", queryType);
                if (StringUtils.isBlank(indgtSerno)) {
                    throw BizException.error(null, "9999", "查询类型为02，则业务唯一编号不能为空");
                }
                surveySerno = indgtSerno;
                queryModel.addCondition("surveySerno", indgtSerno);
            }
            //查询客户授信调查主表
            logger.info("1根据业务唯一编号客户调查表编号查询客户授信调查主表开始,查询参数为:{}", JSON.toJSONString(queryModel));
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
            List<LmtSurveyReportMainInfo> LmtSurveyReportlist = lmtSurveyReportMainInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtSurveyReportlist)) {
                lmtSurveyReportMainInfo = LmtSurveyReportlist.get(0);
            }
            logger.info("根据业务唯一编号客户调查表编号查询客户授信调查主表结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));

            //查询调查报告基本信息************************************************************************************************************
            logger.info("2根据业务唯一编号客户调查表编号查询调查报告基本信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
            List<LmtSurveyReportBasicInfo> LmtSurveyReportBasiclist = lmtSurveyReportBasicInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtSurveyReportBasiclist)) {
                lmtSurveyReportBasicInfo = LmtSurveyReportBasiclist.get(0);
            }
            logger.info("根据业务唯一编号客户调查表编号查询调查报告基本信息结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));

            //调查报告其他信息************************************************************************************************************
            logger.info("根据业务唯一编号客户调查表编号查询报告企业信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            LmtSurveyReportComInfo lmtSurveyReportComInfo = new LmtSurveyReportComInfo();
            List<LmtSurveyReportComInfo> LmtSurveyReportComlist = lmtSurveyReportComInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtSurveyReportComlist)) {
                lmtSurveyReportComInfo = LmtSurveyReportComlist.get(0);
            }
            logger.info("根据业务唯一编号客户调查表编号查询报告企业信息结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportComInfo));

            //查询调查报告企业信息************************************************************************************************************
            logger.info("根据业务唯一编号客户调查表编号查询报告企业信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo = new LmtSurveyReportOtherInfo();
            List<LmtSurveyReportOtherInfo> LmtSurveyReportotherlist = lmtSurveyReportOtherInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtSurveyReportotherlist)) {
                lmtSurveyReportOtherInfo = LmtSurveyReportotherlist.get(0);
            }
            logger.info("根据业务唯一编号客户调查表编号查询报告企业信息结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportOtherInfo));

            //查询调查结论表信息************************************************************************************************************
            logger.info("根据业务唯一编号查询调查结论表信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            LmtSurveyConInfo lmtSurveyConInfo = new LmtSurveyConInfo();
            List<LmtSurveyConInfo> LmtSurveyReportconlist = lmtSurveyConInfoMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(LmtSurveyReportconlist)) {
                lmtSurveyConInfo = LmtSurveyReportconlist.get(0);
            }
            logger.info("根据业务唯一编号查询调查结论表信息结束,返回结果为:{}", JSON.toJSONString(lmtSurveyConInfo));

            //获取客户经理号及中文名称
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            String managerId = Optional.ofNullable(lmtSurveyReportMainInfo.getManagerId()).orElse(StringUtils.EMPTY);
            String managerIdName = StringUtils.EMPTY;
            if (StringUtil.isNotEmpty(managerId)) {
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    managerIdName = adminSmUserDto.getUserName();
                }
            }
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");

            //查询贷款合同表信息************************************************************************************************************
            logger.info("*********根据业务唯一编号查询贷款合同表信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            CtrLoanCont ctrLoanCont = new CtrLoanCont();
            QueryModel query = new QueryModel();
            query.addCondition("surveySerno",surveySerno);
            List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(ctrLoanContList)) {
                ctrLoanCont = ctrLoanContList.get(0);
            }
            logger.info("*********根据业务唯一编号查询贷款合同表信息结束,返回结果为:{}", JSON.toJSONString(ctrLoanCont));

            //处理返回信息
            xdxw0052DataRespDto.setIndgtSerno(surveySerno);// 客户调查表编号
            xdxw0052DataRespDto.setApplySerno(Optional.ofNullable(lmtSurveyReportMainInfo.getListSerno()).orElse(StringUtils.EMPTY));// 业务唯一编号
            xdxw0052DataRespDto.setRqstrName(Optional.ofNullable(lmtSurveyReportMainInfo.getCusName()).orElse(StringUtils.EMPTY));// 申请人
            xdxw0052DataRespDto.setCertNo(Optional.ofNullable(lmtSurveyReportMainInfo.getCertCode()).orElse(StringUtils.EMPTY));// 身份证号
            xdxw0052DataRespDto.setMobile(Optional.ofNullable(lmtSurveyReportBasicInfo.getPhone()).orElse(StringUtils.EMPTY));// 手机号
            xdxw0052DataRespDto.setBillNo(StringUtils.EMPTY);// 借据号
            xdxw0052DataRespDto.setAssureMeansMain(Optional.ofNullable(ctrLoanCont.getGuarWay()).orElse(StringUtils.EMPTY));// 主担保方式
            xdxw0052DataRespDto.setAssureMeans(Optional.ofNullable(lmtSurveyConInfo.getGuarMode()).orElse(StringUtils.EMPTY));// 担保方式
            xdxw0052DataRespDto.setTpGuarWays(StringUtils.EMPTY);// 追加担保方式数值
            xdxw0052DataRespDto.setLoanType(StringUtils.EMPTY);// 贷款类型
            xdxw0052DataRespDto.setManagerId(managerId);// 主办客户经理号
            xdxw0052DataRespDto.setManagerName(managerIdName);// 客户经理中文名
            xdxw0052DataRespDto.setOnlineDate(Optional.ofNullable(lmtSurveyReportMainInfo.getInputDate()).orElse(StringUtils.EMPTY));// 上线日期
            xdxw0052DataRespDto.setApproveStatus(Optional.ofNullable(lmtSurveyReportMainInfo.getApproveStatus()).orElse(StringUtils.EMPTY));// 审批状态
            xdxw0052DataRespDto.setBizSour(Optional.ofNullable(lmtSurveyReportMainInfo.getDataSource()).orElse(StringUtils.EMPTY));// 业务来源
            xdxw0052DataRespDto.setRecomId(Optional.ofNullable(lmtSurveyReportMainInfo.getMarId()).orElse(StringUtils.EMPTY));// 推荐人
            xdxw0052DataRespDto.setBizType(Optional.ofNullable(lmtSurveyReportMainInfo.getSurveyType()).orElse(StringUtils.EMPTY));// 业务类型
            xdxw0052DataRespDto.setMultiplexSerno(StringUtils.EMPTY);// 复用流水
            xdxw0052DataRespDto.setLastApprTime(Optional.ofNullable(lmtSurveyReportMainInfo.getUpdDate()).orElse(StringUtils.EMPTY));// 最后一次审批时间
            xdxw0052DataRespDto.setPayType(StringUtils.EMPTY);// 支付方式
            xdxw0052DataRespDto.setContType(Optional.ofNullable(ctrLoanCont.getContType()).orElse(StringUtils.EMPTY));// 合同类型
            xdxw0052DataRespDto.setThisCrdAmt(new BigDecimal(0L));// 本次授信金额
            xdxw0052DataRespDto.setThisLoanAmt(new BigDecimal(0L));// 本次用信金额
            xdxw0052DataRespDto.setIsLoanAndReturn(StringUtils.EMPTY);// 是否随借随还
            xdxw0052DataRespDto.setCancelFlag(StringUtils.EMPTY);// 撤销标志
            xdxw0052DataRespDto.setOrgNo(StringUtils.EMPTY);// 所属机构
            xdxw0052DataRespDto.setMultiplexedSerno(StringUtils.EMPTY);// 被复用流水号
            xdxw0052DataRespDto.setOldRate(Optional.ofNullable(lmtSurveyConInfo.getOldBillBalance()).orElse(new BigDecimal(0L)));// 原利率
            xdxw0052DataRespDto.setRate(Optional.ofNullable(lmtSurveyConInfo.getAdviceRate()).orElse(new BigDecimal(0L)));// 利率
            xdxw0052DataRespDto.setLoanReason(StringUtils.EMPTY);// 贷款原因
            xdxw0052DataRespDto.setLoanUseType(Optional.ofNullable(lmtSurveyReportBasicInfo.getLoanPurp()).orElse(StringUtils.EMPTY));// 贷款用途
            xdxw0052DataRespDto.setBillBal(Objects.isNull(lmtSurveyReportBasicInfo.getOldBillBalance()) ? StringUtils.EMPTY: lmtSurveyReportBasicInfo.getOldBillBalance().toPlainString());// 借据余额
            xdxw0052DataRespDto.setLoanAmt(Objects.isNull(lmtSurveyReportBasicInfo.getAppAmt()) ? StringUtils.EMPTY: lmtSurveyReportBasicInfo.getAppAmt().toPlainString());// 贷款金额
            xdxw0052DataRespDto.setLoanTerm(new BigDecimal(Optional.ofNullable(lmtSurveyConInfo.getLoanTerm()).orElse("0")));// 贷款期限
            xdxw0052DataRespDto.setRepayType(Optional.ofNullable(lmtSurveyConInfo.getRepayMode()).orElse(StringUtils.EMPTY));// 还款方式
            xdxw0052DataRespDto.setCusId(Optional.ofNullable(lmtSurveyReportMainInfo.getCusId()).orElse(StringUtils.EMPTY));// 客户号
            xdxw0052DataRespDto.setIsOperNormal(Optional.ofNullable(lmtSurveyConInfo.getIsCusOperNormal()).orElse(StringUtils.EMPTY));// 借款人经营是否正常
            xdxw0052DataRespDto.setResult1(Optional.ofNullable(lmtSurveyConInfo.getIsCusRealOperator()).orElse(StringUtils.EMPTY));// 客户是否实际经营人
            xdxw0052DataRespDto.setIsOperOwnershipChange(Optional.ofNullable(lmtSurveyReportBasicInfo.getManagementBelongFlag()).orElse(StringUtils.EMPTY));// 借款人经营所有权是否发生重大变化
            xdxw0052DataRespDto.setResult2(StringUtils.EMPTY);// 借款人经营所有权是否发生重大变化原因
            xdxw0052DataRespDto.setIsDebtAdd50(Optional.ofNullable(lmtSurveyReportBasicInfo.getDebtFlag()).orElse(StringUtils.EMPTY));// 借款人负债较上期增加是否超50
            xdxw0052DataRespDto.setResult3(StringUtils.EMPTY);// 借款人负债较上期增加是否超50%原因
            xdxw0052DataRespDto.setIsMoreAssure(Optional.ofNullable(lmtSurveyReportBasicInfo.getGuarFlag()).orElse(StringUtils.EMPTY));// 借款人对外是否提供过多担保或大量资产被抵押
            xdxw0052DataRespDto.setResult4(StringUtils.EMPTY);// 过多担保或大量资产被抵押原因
            xdxw0052DataRespDto.setIsFamilyAdt(Optional.ofNullable(lmtSurveyReportBasicInfo.getAccidentFlag()).orElse(StringUtils.EMPTY));// 借款人及其家庭是否发生意外（包括借款人死亡、被拘留、家庭破裂、涉及黄赌毒等）
            xdxw0052DataRespDto.setResult5(StringUtils.EMPTY);// 发生意外原因
            xdxw0052DataRespDto.setIsPldimnMoreDvl(Optional.ofNullable(lmtSurveyReportBasicInfo.getGuaranteeFlag()).orElse(StringUtils.EMPTY));// 抵/质押物是否损毁或大幅贬值，变现能力与审批贷款时有无较大差异
            xdxw0052DataRespDto.setResult6(StringUtils.EMPTY);// 有较大差异原因
            xdxw0052DataRespDto.setRateApplyReason(StringUtils.EMPTY);// 利率申请原因
            xdxw0052DataRespDto.setIsWxbxd(StringUtils.EMPTY);// 是否优转续贷
            xdxw0052DataRespDto.setCusLevel(StringUtils.EMPTY);// 客户分层
            xdxw0052DataRespDto.setConName(Optional.ofNullable(lmtSurveyReportComInfo.getConName()).orElse(StringUtils.EMPTY));// 企业名称
            xdxw0052DataRespDto.setReprName(Optional.ofNullable(lmtSurveyReportComInfo.getLegal()).orElse(StringUtils.EMPTY));// 法人代表
            xdxw0052DataRespDto.setOperAddr(Optional.ofNullable(lmtSurveyReportComInfo.getOperAddr()).orElse(StringUtils.EMPTY));// 经营地址
            xdxw0052DataRespDto.setMainOperBusi(Optional.ofNullable(lmtSurveyReportComInfo.getMainBusi()).orElse(StringUtils.EMPTY));// 主营业务
            xdxw0052DataRespDto.setBsinsLicYear(Optional.ofNullable(lmtSurveyReportComInfo.getBlicYears()).orElse(StringUtils.EMPTY));// 营业执照年限
            xdxw0052DataRespDto.setTrade(Optional.ofNullable(lmtSurveyReportComInfo.getTrade()).orElse(StringUtils.EMPTY));// 行业
            xdxw0052DataRespDto.setIndivCredit(Optional.ofNullable(lmtSurveyReportOtherInfo.getIndivCreditStatus()).orElse(StringUtils.EMPTY));// 个人征信
            xdxw0052DataRespDto.setIndivCreditMemo(Optional.ofNullable(lmtSurveyReportOtherInfo.getIndivCreditRemark()).orElse(StringUtils.EMPTY));// 个人征信备注
            xdxw0052DataRespDto.setConCredit(Optional.ofNullable(lmtSurveyReportOtherInfo.getCorpCreditStatus()).orElse(StringUtils.EMPTY));// 企业征信
            xdxw0052DataRespDto.setConCreditMemo(Optional.ofNullable(lmtSurveyReportOtherInfo.getCorpCreditRemark()).orElse(StringUtils.EMPTY));// 企业征信备注
            xdxw0052DataRespDto.setIndivMarSt(Optional.ofNullable(lmtSurveyReportBasicInfo.getMarStatus()).orElse(StringUtils.EMPTY));// 婚姻状况
            xdxw0052DataRespDto.setSpouseName(Optional.ofNullable(lmtSurveyReportBasicInfo.getSpouseName()).orElse(StringUtils.EMPTY));// 配偶姓名
            xdxw0052DataRespDto.setSpouseCertNo(Optional.ofNullable(lmtSurveyReportBasicInfo.getSpouseCertCode()).orElse(StringUtils.EMPTY));// 配偶身份证
            xdxw0052DataRespDto.setSpousePhone(Optional.ofNullable(lmtSurveyReportBasicInfo.getSpousePhone()).orElse(StringUtils.EMPTY));// 配偶电话
            xdxw0052DataRespDto.setOldBillAmt(Optional.ofNullable(lmtSurveyReportBasicInfo.getOldBillAmt()).orElse(new BigDecimal(0L)).toString());// 原借据金额
            xdxw0052DataRespDto.setApprId(StringUtils.EMPTY);// 审核人工号
            xdxw0052DataRespDto.setCheckDate(StringUtils.EMPTY);// 检查日期
            xdxw0052DataRespDto.setCopyFrom(StringUtils.EMPTY);// 从哪复制
            xdxw0052DataRespDto.setAnnualRepayAmt(new BigDecimal(0L));// 每年归还本金（元）
            xdxw0052DataRespDto.setIsNewEmployee(Optional.ofNullable(lmtSurveyReportBasicInfo.getIsNewEmployee()).orElse(StringUtils.EMPTY));// 是否新员工
            xdxw0052DataRespDto.setNewEmployeeName(Optional.ofNullable(lmtSurveyReportBasicInfo.getNewEmployeeName()).orElse(StringUtils.EMPTY));// 新员工名称
            xdxw0052DataRespDto.setEmployeePhone(Optional.ofNullable(lmtSurveyReportBasicInfo.getNewEmployeePhone()).orElse(StringUtils.EMPTY));// 新员工电话
            xdxw0052DataRespDto.setApprApplyStatus(Optional.ofNullable(lmtSurveyReportMainInfo.getApproveStatus()).orElse(StringUtils.EMPTY));// 审批申请状态

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value);
        return xdxw0052DataRespDto;
    }
}
