package cn.com.yusys.yusp.web.server.xdqt0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0005.req.Xdqt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.resp.Xdqt0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0005.Xdqt0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:贷款申请预约（企业客户）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0005:贷款申请预约（企业客户）")
@RestController
@RequestMapping("/api/bizqt4bsp")
public class BizXdqt0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdqt0005Resource.class);

    @Autowired
	private Xdqt0005Service xdqt0005Service;
    /**
     * 交易码：xdqt0005
     * 交易描述：贷款申请预约（企业客户）
     *
     * @param xdqt0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷款申请预约（企业客户）")
    @PostMapping("/xdqt0005")
    protected @ResponseBody
    ResultDto<Xdqt0005DataRespDto> xdqt0005(@Validated @RequestBody Xdqt0005DataReqDto xdqt0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005DataReqDto));
        Xdqt0005DataRespDto xdqt0005DataRespDto = new Xdqt0005DataRespDto();// 响应Dto:贷款申请预约（企业客户）
        ResultDto<Xdqt0005DataRespDto> xdqt0005DataResultDto = new ResultDto<>();
		String loanBank = xdqt0005DataReqDto.getLoanBank();//贷款服务行
		String addr = xdqt0005DataReqDto.getAddr();//地址
		String procde = xdqt0005DataReqDto.getProcde();//邮编
		String phone = xdqt0005DataReqDto.getPhone();//电话
		String loanUse = xdqt0005DataReqDto.getLoanUse();//贷款用途
		BigDecimal loanBal = xdqt0005DataReqDto.getLoanBal();//贷款金额
		String loanTerm = xdqt0005DataReqDto.getLoanTerm();//贷款期限
		String guarType = xdqt0005DataReqDto.getGuarType();//担保方式
		String cusName = xdqt0005DataReqDto.getCusName();//姓名
		String sex = xdqt0005DataReqDto.getSex();//性别
		String certType = xdqt0005DataReqDto.getCertType();//证件类型
		String certNo = xdqt0005DataReqDto.getCertNo();//证件号
		String mobile = xdqt0005DataReqDto.getMobile();//手机号
		String edu = xdqt0005DataReqDto.getEdu();//学历
		String marStatus = xdqt0005DataReqDto.getMarStatus();//婚姻状况
		String indivRelComName = xdqt0005DataReqDto.getIndivRelComName();//单位名称
		String indivComTyp = xdqt0005DataReqDto.getIndivComTyp();//单位性质
		String isLocal = xdqt0005DataReqDto.getIsLocal();//是否本地户口
		BigDecimal yearn = xdqt0005DataReqDto.getYearn();//年收入
		String pldType = xdqt0005DataReqDto.getPldType();//抵押物类型
		String collType = xdqt0005DataReqDto.getCollType();//质押物类型
		String duty = xdqt0005DataReqDto.getDuty();//职务
		String indivRsdAddr = xdqt0005DataReqDto.getIndivRsdAddr();//居住地址
		String infoSour = xdqt0005DataReqDto.getInfoSour();//信息来源
		String loanType = xdqt0005DataReqDto.getLoanType();//经营或消费性贷款
		String cusType = xdqt0005DataReqDto.getCusType();//客户类型
		String yearSaleRange = xdqt0005DataReqDto.getYearSaleRange();//年销售范围
		String linkMan = xdqt0005DataReqDto.getLinkMan();//联系人
        try {
            // 从xdqt0005DataReqDto获取业务值进行业务逻辑处理
            // 调用XXXXXService层开始
			xdqt0005DataRespDto = xdqt0005Service.WYtoCmisAction(xdqt0005DataReqDto);
            // 调用XXXXXService层结束
            // TODO 封装xdqt0005DataRespDto对象开始
            // TODO 封装xdqt0005DataRespDto对象结束
            // 封装xdqt0005DataResultDto中正确的返回码和返回信息
            xdqt0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
			logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, e.getMessage());
			// 封装xdqt0005DataResultDto中异常返回码和返回信息
			xdqt0005DataResultDto.setCode(EcbEnum.ECB019999.key);
			xdqt0005DataResultDto.setMessage(EcbEnum.ECB019999.value);
		} catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, e.getMessage());
            // 封装xdqt0005DataResultDto中异常返回码和返回信息
            xdqt0005DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0005DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0005DataRespDto到xdqt0005DataResultDto中
        xdqt0005DataResultDto.setData(xdqt0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005DataResultDto));
        return xdqt0005DataResultDto;
    }
}
