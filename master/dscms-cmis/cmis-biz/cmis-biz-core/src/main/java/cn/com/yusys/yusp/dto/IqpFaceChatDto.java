package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpFaceChat
 * @类描述: iqp_face_chat数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:19:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpFaceChatDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 面谈人工号 **/
	private String mgrCode;
	
	/** 面谈地点  STD_ZB_FACETALK_ADDR **/
	private String faceTalkAddr;
	
	/** 面谈地点备注 **/
	private String addrExt;
	
	/** 面谈时间 STD_ZB_FACETALK_TIME **/
	private String faceTalkTime;
	
	/** 面谈方式 STD_ZB_FACETALK_TYPE **/
	private String faceTalkType;
	
	/** 面谈对象 STD_ZB_FACETALK_KIND **/
	private String faceTalkKind;
	
	/** 借款人身份是否真实 **/
	private String isRealAplyerInfo;
	
	/** 借款人的购房行为是否真实 **/
	private String isRealBuyHouse;
	
	/** 拟购房屋价格是否符合市场实际 **/
	private String isReasonablePrice;
	
	/** 借款人收入情况是否存在疑点 **/
	private String isDoubtIncome;
	
	/** 借款人是否符合贷款条件 **/
	private String isOkLoanCondi;
	
	/** 借款人提交资料是否齐全 **/
	private String isCompleteSouce;
	
	/** 首付款(元) **/
	private java.math.BigDecimal firstAmt;
	
	/** 首付款来源 STD_ZB_FIRST_AMT_FROM **/
	private String firstAmtFrom;
	
	/** 实际已支付首付款金额（元） **/
	private java.math.BigDecimal hasPayAmt;
	
	/** 贷款成数 **/
	private java.math.BigDecimal loanPercent;
	
	/** 首付款成数 **/
	private java.math.BigDecimal firstAmtPercent;
	
	/** 备注 **/
	private String note;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param mgrCode
	 */
	public void setMgrCode(String mgrCode) {
		this.mgrCode = mgrCode == null ? null : mgrCode.trim();
	}
	
    /**
     * @return MgrCode
     */	
	public String getMgrCode() {
		return this.mgrCode;
	}
	
	/**
	 * @param faceTalkAddr
	 */
	public void setFaceTalkAddr(String faceTalkAddr) {
		this.faceTalkAddr = faceTalkAddr == null ? null : faceTalkAddr.trim();
	}
	
    /**
     * @return FaceTalkAddr
     */	
	public String getFaceTalkAddr() {
		return this.faceTalkAddr;
	}
	
	/**
	 * @param addrExt
	 */
	public void setAddrExt(String addrExt) {
		this.addrExt = addrExt == null ? null : addrExt.trim();
	}
	
    /**
     * @return AddrExt
     */	
	public String getAddrExt() {
		return this.addrExt;
	}
	
	/**
	 * @param faceTalkTime
	 */
	public void setFaceTalkTime(String faceTalkTime) {
		this.faceTalkTime = faceTalkTime == null ? null : faceTalkTime.trim();
	}
	
    /**
     * @return FaceTalkTime
     */	
	public String getFaceTalkTime() {
		return this.faceTalkTime;
	}
	
	/**
	 * @param faceTalkType
	 */
	public void setFaceTalkType(String faceTalkType) {
		this.faceTalkType = faceTalkType == null ? null : faceTalkType.trim();
	}
	
    /**
     * @return FaceTalkType
     */	
	public String getFaceTalkType() {
		return this.faceTalkType;
	}
	
	/**
	 * @param faceTalkKind
	 */
	public void setFaceTalkKind(String faceTalkKind) {
		this.faceTalkKind = faceTalkKind == null ? null : faceTalkKind.trim();
	}
	
    /**
     * @return FaceTalkKind
     */	
	public String getFaceTalkKind() {
		return this.faceTalkKind;
	}
	
	/**
	 * @param isRealAplyerInfo
	 */
	public void setIsRealAplyerInfo(String isRealAplyerInfo) {
		this.isRealAplyerInfo = isRealAplyerInfo == null ? null : isRealAplyerInfo.trim();
	}
	
    /**
     * @return IsRealAplyerInfo
     */	
	public String getIsRealAplyerInfo() {
		return this.isRealAplyerInfo;
	}
	
	/**
	 * @param isRealBuyHouse
	 */
	public void setIsRealBuyHouse(String isRealBuyHouse) {
		this.isRealBuyHouse = isRealBuyHouse == null ? null : isRealBuyHouse.trim();
	}
	
    /**
     * @return IsRealBuyHouse
     */	
	public String getIsRealBuyHouse() {
		return this.isRealBuyHouse;
	}
	
	/**
	 * @param isReasonablePrice
	 */
	public void setIsReasonablePrice(String isReasonablePrice) {
		this.isReasonablePrice = isReasonablePrice == null ? null : isReasonablePrice.trim();
	}
	
    /**
     * @return IsReasonablePrice
     */	
	public String getIsReasonablePrice() {
		return this.isReasonablePrice;
	}
	
	/**
	 * @param isDoubtIncome
	 */
	public void setIsDoubtIncome(String isDoubtIncome) {
		this.isDoubtIncome = isDoubtIncome == null ? null : isDoubtIncome.trim();
	}
	
    /**
     * @return IsDoubtIncome
     */	
	public String getIsDoubtIncome() {
		return this.isDoubtIncome;
	}
	
	/**
	 * @param isOkLoanCondi
	 */
	public void setIsOkLoanCondi(String isOkLoanCondi) {
		this.isOkLoanCondi = isOkLoanCondi == null ? null : isOkLoanCondi.trim();
	}
	
    /**
     * @return IsOkLoanCondi
     */	
	public String getIsOkLoanCondi() {
		return this.isOkLoanCondi;
	}
	
	/**
	 * @param isCompleteSouce
	 */
	public void setIsCompleteSouce(String isCompleteSouce) {
		this.isCompleteSouce = isCompleteSouce == null ? null : isCompleteSouce.trim();
	}
	
    /**
     * @return IsCompleteSouce
     */	
	public String getIsCompleteSouce() {
		return this.isCompleteSouce;
	}
	
	/**
	 * @param firstAmt
	 */
	public void setFirstAmt(java.math.BigDecimal firstAmt) {
		this.firstAmt = firstAmt;
	}
	
    /**
     * @return FirstAmt
     */	
	public java.math.BigDecimal getFirstAmt() {
		return this.firstAmt;
	}
	
	/**
	 * @param firstAmtFrom
	 */
	public void setFirstAmtFrom(String firstAmtFrom) {
		this.firstAmtFrom = firstAmtFrom == null ? null : firstAmtFrom.trim();
	}
	
    /**
     * @return FirstAmtFrom
     */	
	public String getFirstAmtFrom() {
		return this.firstAmtFrom;
	}
	
	/**
	 * @param hasPayAmt
	 */
	public void setHasPayAmt(java.math.BigDecimal hasPayAmt) {
		this.hasPayAmt = hasPayAmt;
	}
	
    /**
     * @return HasPayAmt
     */	
	public java.math.BigDecimal getHasPayAmt() {
		return this.hasPayAmt;
	}
	
	/**
	 * @param loanPercent
	 */
	public void setLoanPercent(java.math.BigDecimal loanPercent) {
		this.loanPercent = loanPercent;
	}
	
    /**
     * @return LoanPercent
     */	
	public java.math.BigDecimal getLoanPercent() {
		return this.loanPercent;
	}
	
	/**
	 * @param firstAmtPercent
	 */
	public void setFirstAmtPercent(java.math.BigDecimal firstAmtPercent) {
		this.firstAmtPercent = firstAmtPercent;
	}
	
    /**
     * @return FirstAmtPercent
     */	
	public java.math.BigDecimal getFirstAmtPercent() {
		return this.firstAmtPercent;
	}
	
	/**
	 * @param note
	 */
	public void setNote(String note) {
		this.note = note == null ? null : note.trim();
	}
	
    /**
     * @return Note
     */	
	public String getNote() {
		return this.note;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}