/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppRepayBillRelMapper;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpJxhjRepayLoan;
import cn.com.yusys.yusp.service.PvpJxhjRepayLoanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpJxhjRepayLoanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-22 19:18:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvpjxhjrepayloan")
public class PvpJxhjRepayLoanResource {
    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpJxhjRepayLoan>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpJxhjRepayLoan> list = pvpJxhjRepayLoanService.selectAll(queryModel);
        return new ResultDto<List<PvpJxhjRepayLoan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpJxhjRepayLoan>> index(QueryModel queryModel) {
        List<PvpJxhjRepayLoan> list = pvpJxhjRepayLoanService.selectByModel(queryModel);
        return new ResultDto<List<PvpJxhjRepayLoan>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpJxhjRepayLoan> show(@PathVariable("pkId") String pkId) {
        PvpJxhjRepayLoan pvpJxhjRepayLoan = pvpJxhjRepayLoanService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpJxhjRepayLoan>(pvpJxhjRepayLoan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpJxhjRepayLoan> create(@RequestBody PvpJxhjRepayLoan pvpJxhjRepayLoan) throws URISyntaxException {
        pvpJxhjRepayLoanService.insert(pvpJxhjRepayLoan);
        return new ResultDto<PvpJxhjRepayLoan>(pvpJxhjRepayLoan);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpJxhjRepayLoan pvpJxhjRepayLoan) throws URISyntaxException {
        int result = pvpJxhjRepayLoanService.update(pvpJxhjRepayLoan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpJxhjRepayLoanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpJxhjRepayLoanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectrepayloan")
    protected ResultDto<List<PvpJxhjRepayLoan>> selectRepayLoan(@RequestBody QueryModel queryModel) {
        List<PvpJxhjRepayLoan> list = pvpJxhjRepayLoanService.selectByModel(queryModel);
        return new ResultDto<List<PvpJxhjRepayLoan>>(list);
    }

    /**
     * @函数名称:ClaJxhjLoanRepay
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/clajxhjloanrepay")
    protected ResultDto<Object> ClaJxhjLoanRepay(@RequestBody QueryModel queryModel) {
        Map rtnData =  pvpJxhjRepayLoanService.ClaJxhjLoanRepay(queryModel);
        return new ResultDto<Object>(rtnData);
    }
}
