package cn.com.yusys.yusp.web.server.xdxw0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0036.req.Xdxw0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0036.resp.Xdxw0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0036.Xdxw0036Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:查询优抵贷调查结论
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0036:查询优抵贷调查结论")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0036Resource.class);

    @Resource
    private Xdxw0036Service xdxw0036Service;

    /**
     * 交易码：xdxw0036
     * 交易描述：查询优抵贷调查结论
     *
     * @param xdxw0036DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷调查结论")
    @PostMapping("/xdxw0036")
    protected @ResponseBody
    ResultDto<Xdxw0036DataRespDto> xdxw0036(@Validated @RequestBody Xdxw0036DataReqDto xdxw0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036DataReqDto));
        Xdxw0036DataRespDto xdxw0036DataRespDto = new Xdxw0036DataRespDto();// 响应Dto:查询优抵贷调查结论
        ResultDto<Xdxw0036DataRespDto> xdxw0036DataResultDto = new ResultDto<>();
        String indgtSerno = xdxw0036DataReqDto.getIndgtSerno();//客户调查表编号
        try {
            // 从xdxw0036DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036DataReqDto));
            xdxw0036DataRespDto = xdxw0036Service.xdxw0036(xdxw0036DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0036DataRespDto));
            // 封装xdxw0036DataResultDto中正确的返回码和返回信息
            xdxw0036DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0036DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, e.getMessage());
            // 封装xdxw0036DataResultDto中异常返回码和返回信息
            xdxw0036DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0036DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0036DataRespDto到xdxw0036DataResultDto中
        xdxw0036DataResultDto.setData(xdxw0036DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036DataResultDto));
        return xdxw0036DataResultDto;
    }
}
