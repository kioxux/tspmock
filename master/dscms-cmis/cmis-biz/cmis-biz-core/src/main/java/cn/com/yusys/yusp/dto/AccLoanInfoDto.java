package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class AccLoanInfoDto {
    private String pvpSerno;
    private String billNo;
    private String contNo;
    private String lmtAccNo;
    private String prdId;
    private String prdName;
    private String cusId;
    private String cusName;
    private String loanModal;
    private String guarMode;
    private String curType;
    private BigDecimal marginRatio;
    private BigDecimal marginBalance;
    private BigDecimal balance;
    private BigDecimal loanBalance;
    private BigDecimal padBalance;
    private BigDecimal interestBalance;
    private BigDecimal handBalance;
    private String loanStartDate;
    private String loanEndDate;
    private String loanTer;
    private String guaranteeNo;
    private String guaranteeName;
    private BigDecimal loancardDue;
    private Integer forwardDays;
    private String proDetails;
    private String loanPaymMtd;
    private String isinCard;
    private String isinRevocation;
    private Integer loanDue;
    private String status;
    private String bankstatus;
    private String inputId;
    private String inputBrId;
    private String managerId;
    private String managerBrId;
    private String finaBrId;
    private String clearStatus;
    private String lowCost;

    public String getClearStatus() {
        return clearStatus;
    }

    public void setClearStatus(String clearStatus) {
        this.clearStatus = clearStatus;
    }

    public String getLowCost() {
        return lowCost;
    }

    public void setLowCost(String lowCost) {
        this.lowCost = lowCost;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getMarginRatio() {
        return marginRatio;
    }

    public void setMarginRatio(BigDecimal marginRatio) {
        this.marginRatio = marginRatio;
    }

    public BigDecimal getMarginBalance() {
        return marginBalance;
    }

    public void setMarginBalance(BigDecimal marginBalance) {
        this.marginBalance = marginBalance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getPadBalance() {
        return padBalance;
    }

    public void setPadBalance(BigDecimal padBalance) {
        this.padBalance = padBalance;
    }

    public BigDecimal getInterestBalance() {
        return interestBalance;
    }

    public void setInterestBalance(BigDecimal interestBalance) {
        this.interestBalance = interestBalance;
    }

    public BigDecimal getHandBalance() {
        return handBalance;
    }

    public void setHandBalance(BigDecimal handBalance) {
        this.handBalance = handBalance;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getGuaranteeNo() {
        return guaranteeNo;
    }

    public void setGuaranteeNo(String guaranteeNo) {
        this.guaranteeNo = guaranteeNo;
    }

    public String getGuaranteeName() {
        return guaranteeName;
    }

    public void setGuaranteeName(String guaranteeName) {
        this.guaranteeName = guaranteeName;
    }

    public BigDecimal getLoancardDue() {
        return loancardDue;
    }

    public void setLoancardDue(BigDecimal loancardDue) {
        this.loancardDue = loancardDue;
    }

    public Integer getForwardDays() {
        return forwardDays;
    }

    public void setForwardDays(Integer forwardDays) {
        this.forwardDays = forwardDays;
    }

    public String getProDetails() {
        return proDetails;
    }

    public void setProDetails(String proDetails) {
        this.proDetails = proDetails;
    }

    public String getLoanPaymMtd() {
        return loanPaymMtd;
    }

    public void setLoanPaymMtd(String loanPaymMtd) {
        this.loanPaymMtd = loanPaymMtd;
    }

    public String getIsinCard() {
        return isinCard;
    }

    public void setIsinCard(String isinCard) {
        this.isinCard = isinCard;
    }

    public String getIsinRevocation() {
        return isinRevocation;
    }

    public void setIsinRevocation(String isinRevocation) {
        this.isinRevocation = isinRevocation;
    }

    public Integer getLoanDue() {
        return loanDue;
    }

    public void setLoanDue(Integer loanDue) {
        this.loanDue = loanDue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBankstatus() {
        return bankstatus;
    }

    public void setBankstatus(String bankstatus) {
        this.bankstatus = bankstatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }
}
