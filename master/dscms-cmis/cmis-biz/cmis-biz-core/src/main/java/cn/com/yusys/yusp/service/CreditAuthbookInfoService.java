/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CreditAuthbookInfo;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.repository.mapper.CreditAuthbookInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.CreditAuthbookInfoVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditAuthbookInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-26 15:05:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditAuthbookInfoService {

    private static final Logger log = LoggerFactory.getLogger(CreditAuthbookInfoService.class);

    @Autowired
    private CreditAuthbookInfoMapper creditAuthbookInfoMapper;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private CommonService commonService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditAuthbookInfo selectByPrimaryKey(String caiSerno) {
        return creditAuthbookInfoMapper.selectByPrimaryKey(caiSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditAuthbookInfo> selectAll(QueryModel model) {
        List<CreditAuthbookInfo> records = (List<CreditAuthbookInfo>) creditAuthbookInfoMapper.selectByModel(model);
        return records;
    }
    public List<CreditAuthbookInfo> selectallByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("createTime desc");
        List<CreditAuthbookInfo> list = creditAuthbookInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditAuthbookInfo> selectByModel(QueryModel model) {
        model.setSort("updateTime desc");
        boolean flag = false;//  总行征信相关岗位标识
        boolean flag2 = false;// 客户经理标识
        // 根据岗位控制数据权限
        if (null != model.getCondition().get("dutyList")) {
            String dutyList = model.getCondition().get("dutyList").toString();
            if (dutyList.contains("code=ZHT01") || dutyList.contains("code=ZHT02")  || dutyList.contains("code=ZHT03") || dutyList.contains("code=ZHT04") || dutyList.contains("code=ZHT05") || dutyList.contains("code=ZHT06") || dutyList.contains("code=ZHT07") || dutyList.contains("code=ZHT14") || dutyList.contains("code=ZHT15") || dutyList.contains("code=ZHT19")) {
                // ZHT01 ZHT02 ZHT03 ZHT04 ZHT05 ZHT06 ZHT07 ZHT14 ZHT15 ZHT19  总行征信相关岗位
                flag = true;
            } else if (dutyList.contains("code=FZH01") || dutyList.contains("code=FZH02") || dutyList.contains("code=XWB01")) {
                // 客户经理 FZH01 ; 零售客户经理 FZH02 ; 小微客户经理 XWB01 ;
                flag2 = true;
            }
        }
        if (flag) {
            model.getCondition().remove("managerId");
        } else if (flag2) {
            String managerId = SessionUtils.getLoginCode();
            model.getCondition().remove("managerId");
            model.getCondition().put("flag", "manager");
            model.getCondition().put("loginCode", managerId);
        } else {
            User userinfo = SessionUtils.getUserInformation();
            String orgCode = userinfo.getOrg().getCode();
            // 权限范围登陆机构及其下级机构
            List<String> orgCodes = commonService.getLowerOrgId(orgCode);
            String orgCodeList = "";
            if (null != orgCodes && orgCodes.size() > 0) {
                orgCodeList = org.apache.commons.lang.StringUtils.join(orgCodes, ",");
            }
            model.getCondition().remove("managerId");
            model.getCondition().put("flag", "other");
            model.getCondition().put("orgCodeList", orgCodeList);

            User loginUser = SessionUtils.getUserInformation();
            List<? extends UserIdentity> roles = loginUser.getRoles();
            if (null != roles && roles.size() > 0) {
                for (int i = 0; i < roles.size(); i++) {
                    // 集中作业档案岗 R1034；集中作业管理人员 R1036
                    if ("R1034".equals(roles.get(i).getCode()) || "R1036".equals(roles.get(i).getCode())) {
                        model.addCondition("localFlag", "true");
                        break;
                    }
                }
            }
        }
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditAuthbookInfo> list = creditAuthbookInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    public List<CreditAuthbookInfoVo> selectByModelVo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("createTime desc");
        List<CreditAuthbookInfoVo> list = creditAuthbookInfoMapper.selectByModelVo(model);
        PageHelper.clearPage();
        list.stream().forEach(item -> {
            item.setAuthbookContent(creditReportQryLstService.handleAuthbookContent(item.getAuthbookContent()));
            item.setOtherAuthbookContent(creditReportQryLstService.handleAuthbookContent(item.getOtherAuthbookContent()));
        });
        return list;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditAuthbookInfo record) {
        return creditAuthbookInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditAuthbookInfo record) {
        return creditAuthbookInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditAuthbookInfo record) {
        return creditAuthbookInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditAuthbookInfo record) {
        return creditAuthbookInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String caiSerno) {
        return creditAuthbookInfoMapper.deleteByPrimaryKey(caiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditAuthbookInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: asyncExportCreditAuthbook
     * @方法描述: 异步导出底层资产明细数据
     * @参数与返回说明: 导出进度信息
     * @算法描述: 无
     */

    public ProgressDto asyncExportCreditAuthbook(QueryModel model) {
        DataAcquisition dataAcquisition = (size, page, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(size);
            queryModeTemp.setSize(page);
            String apiUrl = "/api/creditauthbookinfo/asyncexportcreditauthbook";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if(StringUtils.nonBlank(dataAuth)){
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectByModelVo(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(CreditAuthbookInfoVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: queryAuthbook
     * @方法描述: 授权书列表查询接口
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditAuthbookInfo> queryAuthbook(String certCode, String managerId) {
        /**
         * 根据客户证件号和客户经理,获取作为主借款人未结清的小微业务借据关联的授权书编号数据
         * */
        // 1. 根据客户证件号和客户经理,获取主借款人征信信息
        List<CreditReportQryLst> creditReportQryLsts = creditReportQryLstMapper.selectByCertCodeAndManagerId(certCode, managerId);
        Set<String> certCodeSet = new HashSet<>();
        // 2. 获取作为主借款人未结清的小微业务借据
        for (CreditReportQryLst record : creditReportQryLsts) {
            if (StringUtils.nonBlank(record.getBorrowerCertCode())) {
                if (checkAccLoan(record.getBorrowerCertCode())) {
                    certCodeSet.add(record.getBorrowerCertCode());
                }
            } else {
                if ("001".equals(record.getBorrowRel())) {
                    if (checkAccLoan(record.getCertCode())) {
                        certCodeSet.add(record.getCertCode());
                    }
                }
            }
        }
        // 3. 获取关联的授权书编号数据
        List<CreditAuthbookInfo> list = new ArrayList<>();
        String certCodes = StringUtils.concat(certCodeSet, ",");
        if (StringUtils.nonBlank(certCodes)) {
            list = creditAuthbookInfoMapper.selectByCertCode(certCodes);
        }
        return list;
    }

    /**
     * @方法名称: selectAuthbook
     * @方法描述: 贷后授权书列表查询接口
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditAuthbookInfo> selectAuthbook(QueryModel model) {
        /**
         * 根据客户证件号和客户经理,获取作为主借款人未结清的小微业务借据关联的授权书编号数据
         * */
        // 1. 根据客户证件号和客户经理,获取主借款人征信信息
        //List<CreditReportQryLst> creditReportQryLsts = creditReportQryLstMapper.selectByCertCodeAndManagerId((String) Optional.ofNullable(model.getCondition().get("certCode")).orElse(StringUtils.EMPTY), (String) Optional.ofNullable(model.getCondition().get("managerId")).orElse(StringUtils.EMPTY));
        List<CreditReportQryLst> creditReportQryLsts = creditReportQryLstService.selectByModel(model);
        Set<String> certCodeSet = new HashSet<>();
        // 2. 获取作为主借款人未结清的小微业务借据
        for (CreditReportQryLst record : creditReportQryLsts) {
            if (StringUtils.nonBlank(record.getBorrowerCertCode())) {
                if (checkAccLoan(record.getBorrowerCertCode())) {
                    certCodeSet.add(record.getBorrowerCertCode());
                }
            } else {
                if ("001".equals(record.getBorrowRel())) {
                    if (checkAccLoan(record.getCertCode())) {
                        certCodeSet.add(record.getCertCode());
                    }
                }
            }
        }
        // 3. 获取关联的授权书编号数据
        List<CreditAuthbookInfo> list = new ArrayList<>();
        String certCodes = StringUtils.concat(certCodeSet, ",");
        if (StringUtils.nonBlank(certCodes)) {
            model.getCondition().put("certCodes", certCodes);
            PageHelper.startPage(model.getPage(), model.getSize());
            list = creditAuthbookInfoMapper.queryByCertCode(model);
            PageHelper.clearPage();
        }
        return list;
    }

    /**
     * @方法名称: checkAccLoan
     * @方法描述: 判断客户是否有未结清借据信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean checkAccLoan (String certCode) {
        //根据借款人证件号 获取借款人客户号 调用cus服务
        CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certCode);
        if (cusBaseClientDto != null) {
            BigDecimal existAcc = accLoanService.checkCusNotClosedAccZXByCusId(cusBaseClientDto.getCusId());
            if (existAcc.compareTo(BigDecimal.ZERO) <= 0) {
                log.info("客户没有未结清的借据信息");
                return false;
            } else {
                log.info("客户有未结清的借据信息");
                return true;
            }
        } else {
            log.info("信贷系统无此用户信息，证件号为：{}", certCode);
            return false;
        }
    }

    /**
     * @方法名称: queryAuthBookList
     * @方法描述: 查询授权书列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditAuthbookInfo> queryAuthBookList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("updateTime desc");
        List<CreditAuthbookInfo> list = creditAuthbookInfoMapper.queryAuthBookList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryAuthBookListByModel
     * @方法描述: 查询授权书列表 规则带入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditAuthbookInfo> queryAuthBookListByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("authbookDate desc");
        List<CreditAuthbookInfo> list = creditAuthbookInfoMapper.queryAuthBookListByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryAuthBookinfo
     * @方法描述: 该客户经理发起查询过的，且与主借款人关系一致的最近一份授权书（优化贷后授权书规则，放开同一个客户经理的限制）
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditAuthbookInfo queryAuthBookinfoByMap(Map map) {
        String bcusId = (String) map.get("bcusId");
        String borrowRel = (String) map.get("borrowRel");
        QueryModel model = new QueryModel();
        model.getCondition().put("bcusId", bcusId);
        model.getCondition().put("borrowRel", borrowRel);
        List<CreditAuthbookInfo> list = queryAuthBookListByModel(model);
        CreditAuthbookInfo creditAuthbookInfo = null;
        if (list.size() > 0) {
            creditAuthbookInfo = list.get(0);
        }
        return creditAuthbookInfo;
    }
}
