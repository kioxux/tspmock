package cn.com.yusys.yusp.service.client.bsp.irs.irs21;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import cn.com.yusys.yusp.service.Dscms2IrsClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：单一客户限额测算信息同步
 *
 * @author quwen
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Irs21Service {
    private static final Logger logger = LoggerFactory.getLogger(Irs21Service.class);

    // 1）注入：BSP封装调用非零内评系统的接口
    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;

    /**
     * 业务逻辑处理方法：单一客户限额测算信息同步
     *
     * @param irs21ReqDto
     * @return
     */
    @Transactional
    public Irs21RespDto irs21(Irs21ReqDto irs21ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value, JSON.toJSONString(irs21ReqDto));
        ResultDto<Irs21RespDto> irs21ResultDto = dscms2IrsClientService.irs21(irs21ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value, JSON.toJSONString(irs21ResultDto));

        String irs21Code = Optional.ofNullable(irs21ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String irs21Meesage = Optional.ofNullable(irs21ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Irs21RespDto irs21RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, irs21ResultDto.getCode())) {
            //  获取相关的值并解析
            irs21RespDto = irs21ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, irs21Code, irs21Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value);
        return irs21RespDto;
    }
}
