/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.LmtCobInfo;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.IqpCusCreditInfoRetailDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credzb.CredzbService;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpCusCreditInfo;
import cn.com.yusys.yusp.repository.mapper.IqpCusCreditInfoMapper;

import javax.persistence.Column;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-24 15:44:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpCusCreditInfoService {
    private static final Logger logger = LoggerFactory.getLogger(LmtDebitCreditInfoService.class);
    @Autowired
    private IqpCusCreditInfoMapper iqpCusCreditInfoMapper;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private CredzbService credzbService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpCusCreditInfo selectByPrimaryKey(String bizNo) {
        return iqpCusCreditInfoMapper.selectByPrimaryKey(bizNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpCusCreditInfo> selectAll(QueryModel model) {
        List<IqpCusCreditInfo> records = (List<IqpCusCreditInfo>) iqpCusCreditInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpCusCreditInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpCusCreditInfo> list = iqpCusCreditInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpCusCreditInfo record) {
        return iqpCusCreditInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpCusCreditInfo record) {
        return iqpCusCreditInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpCusCreditInfo record) {
        return iqpCusCreditInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpCusCreditInfo record) {
        return iqpCusCreditInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bizNo) {
        return iqpCusCreditInfoMapper.deleteByPrimaryKey(bizNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpCusCreditInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectSerno
     * @方法描述: 根据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpCusCreditInfo selectSerno(String serno) {
        return iqpCusCreditInfoMapper.selectSerno(serno);
    }

    /**
     * @param iqpCusCreditInfoRetailDto
     * @return cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto
     * @author shenli
     * @date 2021-9-8 22:09:05
     * @version 1.0.0
     * @desc 根据征信风险指标查询信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int  insertIqpCusCreditInfo (IqpCusCreditInfoRetailDto iqpCusCreditInfoRetailDto) {

        logger.info("新增客户及关联人信用信息表开始： "+iqpCusCreditInfoRetailDto.toString());

        BigDecimal CONSUMPTIONBALANCE = new BigDecimal("0");//消费类融资余额
        BigDecimal CONSUMPTIONREPAYMENT = new BigDecimal("0");//消费类融资月还款额
        BigDecimal CURRENTLOANOVERDUEAMOUNT = new BigDecimal("0");//贷款当前逾期金额
        BigDecimal MAXLOANOVERDUEAMOUNT = new BigDecimal("0");//贷款单月最高逾期金额
        BigDecimal MAXMONTHSOFLOANOVERDUE = new BigDecimal("0");//贷款最长逾期月数
        BigDecimal CURRENTCREDITCARDOVERDUEAMOUNT = new BigDecimal("0");//贷记卡当前逾期金额
        BigDecimal MAXCREDITCARDOVERDUEAMOUNT = new BigDecimal("0");//贷记卡单月最高逾期金额
        BigDecimal MAXMONTHSOFCREDITCARDOVERDUE = new BigDecimal("0");//贷记卡最长逾期月数
        BigDecimal OVERDUENUMOFTWOYEARS = new BigDecimal("0");//两年内逾期次数（贷记卡+贷款）
        BigDecimal OVERDUENUMOVERTWOYEARS = new BigDecimal("0");//两年外逾期次数（贷记卡+贷款）

        try {
            // 根据业务申请流水号查询申请信息
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpCusCreditInfoRetailDto.getIqpSerno());
            if (Objects.isNull(iqpLoanApp)) {
                throw BizException.error(null, "9999", "未查询到业务申请信息！");
            }
            //组装报文
            CredzbReqDto credzbReqDto = new CredzbReqDto();
            credzbReqDto.setCustomName(iqpCusCreditInfoRetailDto.getCusName());
            credzbReqDto.setCertificateNum(iqpCusCreditInfoRetailDto.getCertCode());
            credzbReqDto.setRuleCode("R008");
            credzbReqDto.setBrchno(iqpLoanApp.getManagerBrId());

            QueryModel model = new QueryModel();
            model.addCondition("bizSerno" , iqpCusCreditInfoRetailDto.getIqpSerno());
            model.addCondition("certCode" , iqpCusCreditInfoRetailDto.getCertCode());
            model.setSort("reportCreateTime desc");
            List<CreditReportQryLstDto> reportList = creditReportQryLstService.selectCreditReportQryLstByCrqlSerno(model);
            if(reportList.size() > 0){

                //003 征信查询成功
                if(reportList.get(0).getQryStatus() == null || !"003".equals(reportList.get(0).getQryStatus())){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该客户尚未发起征信或发起失败");
                }

                if(reportList.get(0).getReportNo() == null || "".equals(reportList.get(0).getReportNo())){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该客户为白户，暂无征信报告");
                }
                credzbReqDto.setReportId(reportList.get(0).getReportNo());
            } else {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "未查询到征信信息");
            }

            logger.info("根据征信风险指标查询信息请求报文： " + credzbReqDto.toString());
            CredzbRespDto credzbRespDto = credzbService.credzb(credzbReqDto);
            logger.info("根据征信风险指标查询信息返回报文： " + credzbRespDto.toString());

            CONSUMPTIONBALANCE = credzbRespDto.getR008().getCONSUMPTIONBALANCE();
            CONSUMPTIONREPAYMENT = credzbRespDto.getR008().getCONSUMPTIONREPAYMENT();
            CURRENTLOANOVERDUEAMOUNT = credzbRespDto.getR008().getCURRENTLOANOVERDUEAMOUNT();
            MAXLOANOVERDUEAMOUNT = credzbRespDto.getR008().getMAXLOANOVERDUEAMOUNT();
            MAXMONTHSOFLOANOVERDUE = credzbRespDto.getR008().getMAXMONTHSOFLOANOVERDUE();
            CURRENTCREDITCARDOVERDUEAMOUNT = credzbRespDto.getR008().getCURRENTCREDITCARDOVERDUEAMOUNT();
            MAXCREDITCARDOVERDUEAMOUNT = credzbRespDto.getR008().getMAXCREDITCARDOVERDUEAMOUNT();
            MAXMONTHSOFCREDITCARDOVERDUE = credzbRespDto.getR008().getMAXMONTHSOFCREDITCARDOVERDUE();
            OVERDUENUMOFTWOYEARS = credzbRespDto.getR008().getOVERDUENUMOFTWOYEARS();
            OVERDUENUMOVERTWOYEARS = credzbRespDto.getR008().getOVERDUENUMOVERTWOYEARS();

        } catch (Exception e) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "根据征信风险指标查询信息异常：" +e.getMessage());
        }

        String openday = stringRedisTemplate.opsForValue().get("openDay");
        IqpCusCreditInfo IqpCusCreditInfo =  new IqpCusCreditInfo();
        IqpCusCreditInfo.setPkId(UUID.randomUUID().toString());
        IqpCusCreditInfo.setIqpSerno(iqpCusCreditInfoRetailDto.getIqpSerno());
        /** 与借款人关系 **/
        IqpCusCreditInfo.setDebitRela(iqpCusCreditInfoRetailDto.getDebitRela());
        /** 证件号码 **/
        IqpCusCreditInfo.setCertCode(iqpCusCreditInfoRetailDto.getCertCode());
        /** 现有消费类融资余额(万元) **/
        IqpCusCreditInfo.setConsumeFinBal(CONSUMPTIONBALANCE);
        /** 现有消费类融资月还款额(万元) **/
        IqpCusCreditInfo.setConsumeMonRepay(CONSUMPTIONREPAYMENT);
        /** 贷款当前逾期金额 **/
        IqpCusCreditInfo.setLoanCurtOverdueAmt(CURRENTLOANOVERDUEAMOUNT);
        /** 贷款单月最高逾期金额(元) **/
        IqpCusCreditInfo.setLoanHighOverdueAmt(MAXLOANOVERDUEAMOUNT);
        /** 贷款最长逾期月数 **/
        IqpCusCreditInfo.setLoanLgstOverdueMon(MAXMONTHSOFLOANOVERDUE+"");
        /** 贷记卡当前逾期金额 **/
        IqpCusCreditInfo.setDebitCurtOverdueAmt(CURRENTCREDITCARDOVERDUEAMOUNT);
        /** 贷记卡单月最高逾期金额(元) **/
        IqpCusCreditInfo.setDebitHighOverdueAmt(MAXCREDITCARDOVERDUEAMOUNT);
        /** 贷记卡最长逾期月数 **/
        IqpCusCreditInfo.setDebitLgstOverdueMon(MAXMONTHSOFCREDITCARDOVERDUE+"");
        /** 两年内逾期次数 **/
        IqpCusCreditInfo.setInTwoOverdueTimes(OVERDUENUMOFTWOYEARS+"");
        /** 两年外逾期次数 **/
        IqpCusCreditInfo.setOutTwoOverdueTimes(OVERDUENUMOVERTWOYEARS+"");
        /** 登记日期 **/
        IqpCusCreditInfo.setInputDate(openday);
        /** 最后修改日期 **/
        IqpCusCreditInfo.setUpdDate(openday);

        iqpCusCreditInfoMapper.insertSelective(IqpCusCreditInfo);
        logger.info("新增客户及关联人信用信息表结束");
        return 0;
    }

    /**
     * @方法名称: deleteIqpCusCreditInfo
     * @方法描述: 共同借款人删除是联动信用信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteIqpCusCreditInfo(LmtCobInfo lmtCobInfo) {

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("iqpSerno",lmtCobInfo.getSurveySerno());
        queryModel.addCondition("certCode",lmtCobInfo.getCommonDebitCertCode());
        if("102000".equals(lmtCobInfo.getCommonDebitCorre())){//102000-配偶
            queryModel.addCondition("debitRela","C");//C-配偶
        }else{
            queryModel.addCondition("debitRela","B");//B-共同借款人
        }
        List<IqpCusCreditInfo> iqpCusCreditInfoList = iqpCusCreditInfoMapper.selectByModel(queryModel);
        for(int i = 0 ; iqpCusCreditInfoList.size()>i; i++){
            IqpCusCreditInfo iqpCusCreditInfo = iqpCusCreditInfoList.get(i);
            iqpCusCreditInfoMapper.deleteByPrimaryKey(iqpCusCreditInfo.getPkId());
        }
        return 0;
    }



}
