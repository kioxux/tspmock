/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.IqpPrePaymentService;
import cn.com.yusys.yusp.workFlow.service.BGYW11BizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpPrePaymentResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 10:34:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpprepayment")
public class IqpPrePaymentResource {
    @Autowired
    private IqpPrePaymentService iqpPrePaymentService;
    @Autowired
    private BGYW11BizService bgyw11BizService;
	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<IqpPrePayment>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpPrePayment> list = iqpPrePaymentService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpPrePayment>> index(@RequestBody QueryModel queryModel) {
        List<IqpPrePayment> list = iqpPrePaymentService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{iqpSerno}")
    protected ResultDto<IqpPrePayment> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpPrePayment iqpPrePayment = iqpPrePaymentService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<>(iqpPrePayment);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpPrePayment> create(@RequestBody IqpPrePayment iqpPrePayment) throws URISyntaxException {
        iqpPrePaymentService.insertSelective(iqpPrePayment);
        return new ResultDto<>(iqpPrePayment);
    }


    /**
     * @param iqpPrePayment
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpPrePayment>
     * @author tangxun
     * @date 2021/5/26 14:29
     * @version 1.0.0
     * @desc ln3111
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryln3111")
    protected ResultDto<IqpPrePayment> ln3111(@RequestBody IqpPrePayment iqpPrePayment) throws URISyntaxException {
        iqpPrePaymentService.queryLn3111(iqpPrePayment);
        return new ResultDto<>(iqpPrePayment);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpPrePayment iqpPrePayment) throws URISyntaxException {
        int result = iqpPrePaymentService.updateSelective(iqpPrePayment);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpPrePaymentService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpPrePaymentService.deleteByIds(ids);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:sendhx
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendhx")
    protected ResultDto<String> sendhx(@RequestBody IqpPrePayment iqpPrePayment) {
        String result = iqpPrePaymentService.sendHxToRepay(iqpPrePayment);
        return new ResultDto<>(result);
    }
    /**
     * @函数名称:refund
     * @函数描述:主动还款接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/refund/{ids}")
    protected ResultDto<String> refund(@PathVariable String ids) {

        ResultDto<Ln3041RespDto> ln3041ResultDto = bgyw11BizService.sendHxToRepay(ids);
        IqpPrePayment iqpPrePayment = iqpPrePaymentService.selectByPrimaryKey(ids);
        String repayType = iqpPrePayment.getRepayType();//贷款收回方式  02 保证金代偿
        String ln3041Meesage = Optional.ofNullable(ln3041ResultDto.getMessage())
                .orElse(SuccessEnum.SUCCESS.value);
        if (!Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3041ResultDto.getCode())) {
            iqpPrePayment.setStatus("0"); // 还款失败
            iqpPrePayment.setResn("核心返回："+ ln3041Meesage);
        } else {
            ln3041Meesage = "success";
            iqpPrePayment.setStatus("1"); // 还款成功
            iqpPrePayment.setResn("核心返回流水号："+ ln3041Meesage);
            // 修改贷款余额：信贷发送ln3041交易成功后，esb会继续调用核心ln3100交易，将查询的内容返回给信贷（gxloan）xdtz0059，并且返回给国结
            bgyw11BizService.changeAccLoan(iqpPrePayment.getIqpSerno());
        }
        if(null != ln3041ResultDto && null != ln3041ResultDto.getData()){
            iqpPrePayment.setHxSerno(ln3041ResultDto.getData().getJiaoyils());
            iqpPrePayment.setHxDate(ln3041ResultDto.getData().getJiaoyirq());
        }
        iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        iqpPrePaymentService.updateSelective(iqpPrePayment);
        return new ResultDto<>(ln3041Meesage);
    }


    /**
     * @param iqpPrePayment
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.IqpPrePayment>
     * @author hubp
     * @date 2021/8/14 11:46
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyiqpserno")
    protected ResultDto<IqpPrePayment> selectByIqpSerno(@RequestBody IqpPrePayment iqpPrePayment) {
        IqpPrePayment iqpPrePaymentTemp = iqpPrePaymentService.selectByPrimaryKey(iqpPrePayment.getIqpSerno());
        return new ResultDto<>(iqpPrePaymentTemp);
    }


    /**
     * @函数名称:refundCz
     * @函数描述:还款冲正
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/refundCz/{ids}")
    protected ResultDto<String> refundCz(@PathVariable String ids) {
        String ln3236Meesage = iqpPrePaymentService.refundCz(ids);
        return new ResultDto<>(ln3236Meesage);
    }
}
