package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0069Service
 * @类描述: 还款能力分析校验
 * @功能描述: 还款能力分析校验
 * @创建人: hubp
 * @创建时间: 2021年8月2日15:23:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0069Service {

    @Autowired
    private RptLmtRepayAnysGuarLowRiskService rptLmtRepayAnysGuarLowRiskService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private RptLmtRepayAnysService rptLmtRepayAnysService;

    @Autowired
    private RptLmtRepayAnysGuarCorpService rptLmtRepayAnysGuarCorpService;

    @Autowired
    private RptLmtRepayAnysGuarPersonService rptLmtRepayAnysGuarPersonService;

    @Autowired
    private RptLmtRepayAnysGuarSpeCorpService rptLmtRepayAnysGuarSpeCorpService;

    @Autowired
    private RptLmtRepayAnysGuarPldService rptLmtRepayAnysGuarPldService;

    @Autowired
    private LmtAppService lmtAppService;

    /**
     * @方法名称: riskItem0069
     * @方法描述: 还款能力分析校验
     * @参数与返回说明:
     * @算法描述: 第二还款来源分析信息补全校验
     * @创建人: lyj
     * @创建时间: 2021年8月2日15:23:06
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0069(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";

        if (Objects.nonNull(queryModel.getCondition().get("bizId"))) {
            serno = queryModel.getCondition().get("bizId").toString();
        }
        String bizType = queryModel.getCondition().get("bizType").toString();
        if ("SX003".equals(bizType) || "SX004".equals(bizType) || "SX005".equals(bizType) || "SX006".equals(bizType) || "SX026".equals(bizType) || "SX027".equals(bizType) || "SX028".equals(bizType) || "SX031".equals(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            return riskResultDto;
        }
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        String rptType = lmtApp.getRptType();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        if (StringUtils.isBlank(rptType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06701); //调查报告类型为空
            return riskResultDto;
        }
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        //调查报告类型为低风险版
        if (rptType.equals("C03")) {
            List<RptLmtRepayAnysGuarLowRisk> rptLmtRepayAnysGuarLowRiskList = rptLmtRepayAnysGuarLowRiskService.selectByModel(model);
            if (CollectionUtils.isEmpty(rptLmtRepayAnysGuarLowRiskList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("低风险版信息不全！");
                return riskResultDto;
            }
        }
        //获取担保人信息
        List<Map<String, Object>> list = guarGuaranteeService.queryGuarGuaranteeBySerno(serno);
        int corpCount = 0;
        int personCount = 0;
        int speCorpCount = 0;
        int pldCount = 0;
        if (CollectionUtils.nonEmpty(list)) {
            for (Map<String, Object> map : list) {
                String guarMode = "";
                String cusTyp = "";
                String isGuarCom = "";
                //获取担保方式
                guarMode = map.get("guarMode").toString();
                //获取担保人类型
                if (null != map.get("cusTyp")) {
                    cusTyp = map.get("cusTyp").toString();
                }
                //获取是否为专业担保公司
                if (null != map.get("isGuarCom")) {
                    isGuarCom = map.get("isGuarCom").toString();
                }
                if (StringUtils.nonBlank(guarMode)) {
                    if ("10".equals(guarMode) || "20".equals(guarMode)) {
                        //担保方式为抵质押
                        pldCount++;
                    } else if ("00".equals(guarMode)) {
                        //担保方式为信用
                        RptLmtRepayAnys rptLmtRepayAnys = rptLmtRepayAnysService.selectByPrimaryKey(serno);
                        if (Objects.isNull(rptLmtRepayAnys) || null == rptLmtRepayAnys.getOtherGuarModeDesc() || "".equals(rptLmtRepayAnys.getOtherGuarModeDesc())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc("其他担保方式信息不全！");
                            return riskResultDto;
                        }
                    }

                }
                if (StringUtils.nonBlank(cusTyp)) {
                    if ("10002".equals(cusTyp)) {
                        if (Objects.nonNull(map.get("isAddGuar"))) {
                            if (CmisBizConstants.STD_GUAR_TYPE_101.equals(map.get("isAddGuar").toString())) {
                                personCount++;
                            }
                        }
                        //担保人为自然人
                    } else if ("10003".equals(cusTyp)) {
                        //担保人为公司
                        if ("1".equals(isGuarCom)) {
                            //担保人为专业担保公司
                            speCorpCount++;
                        } else {
                            if (Objects.nonNull(map.get("isAddGuar"))) {
                                if (CmisBizConstants.STD_GUAR_TYPE_101.equals(map.get("isAddGuar").toString())) {
                                    corpCount++;
                                }
                            }
                        }
                    } else if ("10001".equals(cusTyp)) {
                        if ("1".equals(isGuarCom)) {
                            //担保人为专业担保公司
                            speCorpCount++;
                        }
                    }
                }

            }
        }
        //担保人为公司信息校验
        if (corpCount > 0) {
            List<RptLmtRepayAnysGuarCorp> rptLmtRepayAnysGuarCorpList = rptLmtRepayAnysGuarCorpService.selectByModel(model);
            if (CollectionUtils.isEmpty(rptLmtRepayAnysGuarCorpList) || rptLmtRepayAnysGuarCorpList.size() < corpCount) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("担保人为公司信息不全！");
                return riskResultDto;
            }
        }
        //担保人为自然人信息校验
        if (personCount > 0) {
            List<RptLmtRepayAnysGuarPerson> rptLmtRepayAnysGuarPersonList = rptLmtRepayAnysGuarPersonService.selectByModel(model);
            if (CollectionUtils.isEmpty(rptLmtRepayAnysGuarPersonList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("担保人为自然人信息不全！");
                return riskResultDto;
            }
        }
        //担保人为专业担保公司信息校验
        if (speCorpCount > 0) {
            List<RptLmtRepayAnysGuarSpeCorp> rptLmtRepayAnysGuarSpeCorpList = rptLmtRepayAnysGuarSpeCorpService.selectByModel(model);
            if (CollectionUtils.isEmpty(rptLmtRepayAnysGuarSpeCorpList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("担保人为专业担保公司信息不全！");
                return riskResultDto;
            }
        }
        //抵质押物信息校验
        if (pldCount > 0) {
            RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld = rptLmtRepayAnysGuarPldService.selectBySerno(serno);
            if (Objects.isNull(rptLmtRepayAnysGuarPld)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("抵质押物信息不全！");
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
