/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccDiscClassificationDto;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.AccLoanAndPvpDto;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.AccLoanToppDto;
import cn.com.yusys.yusp.dto.BizAccLoanDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.resp.Xdtz0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.resp.Xdtz0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.Xdtz0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.resp.Xdtz0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.AccLoanList;
import cn.com.yusys.yusp.dto.server.xdtz0039.resp.Xdtz0039DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.resp.Xdtz0040DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.resp.Xdtz0042DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.resp.Xdtz0043DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.resp.Xdtz0049DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.req.Xdtz0060DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-28 19:39:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccLoanMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccLoan selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(AccLoan record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(AccLoan record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(AccLoan record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(AccLoan record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 通过参数集合查询借据台账信息
     *
     * @param params
     * @return
     */
    List<AccLoan> selectAccLoanByParams(Map params);

    /**
     * @方法名称: selectAccLoanByContNO
     * @方法描述: 根据业务流水号查询额度信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> selectAccLoanByContNo(Map params);

    /**
     * @方法名称: selectAccLoan
     * @方法描述: 获取贷款台账信息获取当前主办人，且台账状态为正常（1-正常）的数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<BizAccLoanDto> selectAccLoan(@Param("managerId") String managerId, @Param("accStatus") String accStatus, @Param("bizType") String bizType);

    /**
     * 业务统计-余额（万元）
     *
     * @param map
     * @return
     */
    List<Map> selectBusBalance(Map map);

    /**
     * 业务提醒-N天待还贷款台账统计
     */
    int countAccLoanByDays(@Param("dayNum") int dayNum);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoanToppDto> selectAccLoanSubDto(QueryModel model);


    /**
     * @方法名称: selectAccLoanForxdtz
     * @方法描述: Xdtz0013 查询小微借据余额、结清日期、贷款类型
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> selectAccLoanForxdtz(Map queryMap);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    /**
     * 交易码：xdtz0042
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param cusId
     * @return
     * @throws Exception
     */
    Xdtz0042DataRespDto getAccByCusId(String cusId);

    /**
     * @方法名称: selectxdtzAccLoanForxdtz
     * @方法描述: Xdtz0062 查询客户的个人消费贷款
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> selectxdtzAccLoanForxdtz(Map queryMap);

    /**
     * 交易码：selectAccLoancountByRelCusComIdList
     * 交易描述：根据客户号查找出该客户关联的企业是否存在生效合同或未结清贷款业务
     *
     * @param queryMap
     * @return
     * @throws Exception
     */
    int selectAccLoancountByRelCusComIdList(Map queryMap);

    /**
     * 交易码：selectNotColseAccLoanCountByCusId
     * 交易描述：根据客户号查询是否有未关闭的非消费类借据信息
     *
     * @param queryMap
     * @return
     * @throws Exception
     */
    int selectNotColseAccLoanCountByCusId(Map queryMap);

    /**
     * @创建人 WH
     * @创建时间 2021-04-27 20:20
     * @注释 查询借据信息
     */
    List<AccLoan> selectbilllist(String surveySerno);

    /**
     * 统计客户行内信用类贷款余额
     *
     * @param queryMap
     * @return
     */
    Xdtz0043DataRespDto getLoanSumByCertNo(Map queryMap);

    /**
     * @param
     * @return int
     * @author shenli
     * @date 2021/4/26 0026 21:52
     * @version 1.0.0
     * @desc 根据合同编号查询台账笔数
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int selectAccLoanCountByContNo(@Param("contNo") String contNo);

    /**
     * @创建人 WH
     * @创建时间 2021/7/29 16:49
     * @注释 根据合同编号查询未结清 （非已关闭 已废除）的合同数量
     */
    int selectAccLoanCountByContNobyxw(@Param("contNo") String contNo);

    /**
     * @param contNo
     * @return countAccLoanCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    int countAccLoanCountByContNo(@Param("contNo") String contNo);

    /**
     * @param
     * @return int
     * @author shenli
     * @date 2021/4/26 0026 21:52
     * @version 1.0.0
     * @desc 根据合同编号查询贷款余额大于0的台账笔数
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int selectAccLoanBalanceByContNo(@Param("contNo") String contNo);

    /**
     * 查询是否存在逾期
     *
     * @param cusId
     * @return
     */
    int queryOverDueFromCmis(@Param("cusId") String cusId);

    /**
     * 申请人在我行对公存在存量余额
     *
     * @param cusId
     * @return
     */
    BigDecimal queryCunlye(String cusId);

    /**
     * @param cusId
     * @return
     */
    int queryPojyxdk(String cusId);

    /**
     * 查询客户所担保的行内当前贷款逾期件数
     *
     * @param cusId
     * @return
     */
    int queryGuaranteeOverDueFromCmis(@Param("cusId") String cusId);

    /**
     * 交易码：xdtz0039
     * 交易描述：根据企业名称查询申请企业在本行是否存在当前逾期贷款
     *
     * @param cusName
     * @return
     */
    Xdtz0039DataRespDto getHasOverByCusName(@Param("cusName") String cusName, @Param("cusId") String cusId);

    /**
     * 申请人在本行当前逾期贷款数量
     *
     * @param queryMap
     * @return
     */
    Xdtz0040DataRespDto getContNumByCertCont(Map queryMap);

    /**
     * 根据客户号获取正常周转次数
     *
     * @param queryMap
     * @return
     */
    int queryTurnovTimes(Map queryMap);

    /**
     * 查询客户经理不良率
     *
     * @param queryMap
     * @return
     */
    BigDecimal queryBadRate(Map queryMap);

    /**
     * 根据身份证号查找借据编号
     *
     * @param certCode
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0010.resp.List> getBillInfoByCertCode(@Param("certCode") String certCode);

    /**
     * 根据核心客户号查询惠享贷累计我行贷款次数
     *
     * @param cusId
     * @return
     */
    int getLoanNumByCusId(@Param("cusId") String cusId);


    /**
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param queryMap
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList> queryBusinessLoantails(Map queryMap);


    /**
     * 根据核心客户号查询申请人在本行信用类经营性贷款借据余额汇总
     *
     * @param cusId
     * @return
     */
    BigDecimal getSumBalanceByCusId(String cusId);

    /**
     * 根据核心客户号查询申请人最近一笔借据的结清日期
     *
     * @param cusId
     * @return
     */
    String getLoanEndDateByCusId(String cusId);

    /**
     * 根据核心客户号查询申请人在本行借据余额汇总
     *
     * @param cusId
     * @return
     */
    BigDecimal getAllSumBalanceByCusId(String cusId);

    /**
     * 根据证件号查询借据信息
     *
     * @param queryMap
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0006.resp.List> getWjqList(Map queryMap);

    /**
     * 查询小微借据余额
     *
     * @param map
     * @return
     */
    Xdtz0003DataRespDto getXWLoanBal(Map map);

    /**
     * 查询借据信息
     *
     * @param cusId
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0011.resp.List> getBillInfoListByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询我行现有融资情况
     *
     * @param cusId
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0028.resp.LoanList> getLoanBalanceInfoByCusId(@Param("cusId") String cusId);


    /**
     * 根据客户号查询我行现有融资情况(经营性贷款金额和信用消费性贷款金额)
     *
     * @param cusId
     * @return
     */
    HashMap<String, BigDecimal> getLoanCreditConsumeAmtByCusId(@Param("cusId") String cusId);

    /**
     * 查询借据信息
     *
     * @param billNo
     * @return
     */
    Xdtz0011DataRespDto getBillInfoByBillNo(@Param("billNo") String billNo);

    /**
     * 根据客户号获取非信用方式发放贷款的最长到期日
     *
     * @param cusId
     * @return
     */
    Xdtz0007DataRespDto getLastLoanEndDate(String cusId);

    /**
     * XDTZ0012查询配偶经营性贷款余额
     *
     * @param cusId
     * @return
     */
    BigDecimal getCoupleLoanBalList(@Param("cusId") String cusId, @Param("prdId") String prdId);

    /**
     * 客户贷款信息查询
     *
     * @param cusId
     * @return
     */
    List<Xdtz0049DataRespDto> xdtz0049(@Param("cusId") String cusId);


    /**
     * N代表非提前周转走还本续贷sq
     *
     * @param queryMap
     * @return
     */
    Xdtz0027DataRespDto getIsHavingLoanByCusIds(Map queryMap);

    /**
     * Y代表提前周转走无还本续贷sql
     *
     * @param queryMap
     * @return
     */
    Xdtz0027DataRespDto getYseIsHavingLoanByCusIds(Map queryMap);

    /**
     * 根据合同号获取借据信息
     *
     * @param queryMap
     * @return
     */
    List<AccLoanList> getLoanList(Map queryMap);


    /**
     * 根据借据号查询信贷房贷贷款期限
     *
     * @param queryMap
     * @return
     */
    int queryLoanMonthFormAccLoan(Map queryMap);

    /**
     * 根据借据号查询信贷房贷借据金额
     *
     * @param queryMap
     * @return
     */
    BigDecimal queryLoanAmountFormAccLoan(Map queryMap);

    /**
     * 根据借据号取信贷账务机构
     *
     * @param queryMap
     * @return
     */
    String queryFinaBrIdFromAccLoan(Map queryMap);

    /**
     * 根据借据号查询本行其他贷款拖欠情况
     *
     * @param queryMap
     * @return
     */
    int queryOverTimesTotalFromAccLoan(Map queryMap);

    /**
     * 查询是否有信贷记录有余额的经营性贷款总数
     *
     * @return
     */
    int getHaveLoanBalByCusIdList(java.util.List<String> cusIdList);

    /**
     * 根据借据编号查询客户编号
     * xdtz0053
     *
     * @param billNO
     * @return
     */
    String queryCusIdByBillNo(@Param("billNO") String billNO);

    AccLoan selectByPvpSerno(@Param("pvpSerno") String pvpSerno);

    /**
     * 查询客户所担保的行内贷款五级分类非正常状态件数
     * Xdtz0033
     *
     * @param cusId
     * @return
     */
    int queryFiveclassByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: queryAccLoanDataByBillNo
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    AccLoan queryAccLoanDataByBillNo(@Param("billNo") String billNo);

    /**
     * 查询指定贷款开始日的优企贷客户贷款余额合计
     *
     * @param loanStartDate
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdtz0025.resp.List> queryLoanBalanceByLoanStartDate(String loanStartDate);

    /**
     * 根据客户号查询现有融资总额、总余额、担保方式
     *
     * @param cusId
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0029.resp.List> getXdxw0029(@Param("cusId") String cusId);

    /**
     * @方法名称：selectForAccLoanInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 14:43
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccLoan> selectForAccLoanInfo(@Param("cusId") String cusId);

    /**
     * @方法名称：selectForAccLoanInfoByCusId
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0047.resp.List> selectForAccLoanInfoByCusId(@Param("cusId") String cusId);

    /**
     * 根据流水号查询客户调查的放款信息（在途需求）
     *
     * @param surveySerno
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdtz0057.resp.List> getAccInfoBySurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 14:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccLoan> selectByContNo(@Param("contNo") String contNo);

    /**
     * @author zlf
     * @date 2021/5/25 10:48
     * @version 1.0.0
     * @desc 根据借据号查询单条信息
     * @修改历史 修改时间 修改人员 修改原因
     */
    AccLoan selectByBillNo(@Param("billNo") String billNo);

    /**
     * @author zhoumw
     * @date 2021年6月1日16:35:45
     * @version 1.0.0
     * @desc 根据合同号，查询合同下未结清借据
     * @修改历史 修改时间 修改人员 修改原因
     */
    List<AccLoanDto> queryAccLoanByContNoForNpam(List<String> bizTypeList);

    /**
     * 获取当前合同下贷款总余额SUM(LOAN_BALANCE)，根据CONT_NO查询贷款台账表ACC_LOAN;
     *
     * @param cont_no
     * @return
     */
    BigDecimal getSumBalanceByContNo(@Param("contNo") String cont_no);

    /**
     * 获取台账明细
     *
     * @param cont_no
     * @return
     */
    List<CmisLmt0010ReqDealBizListDto> getDealBizList(@Param("contNo") String cont_no);

    /**
     * 根据客户号获取贷款列表
     *
     * @param cusId
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLoan> selectListLoanByCusId(@Param("cusId") String cusId, @Param("prdId") String prdId);

    /**
     * @方法名称：selectFirstAccLoanByParams
     * @方法描述：取第一笔线下支用的贷款
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    AccLoan selectFirstAccLoanByParams(@Param("contNo") String contNo);

    /**
     * @方法名称：selectOverDueCountByParams
     * @方法描述：客户的贷款逾期条数
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    int selectOverDueCountByParams(@Param("cusId") String cusId, @Param("openDay") String openDay);

    /**
     * @方法名称：selectSumLoanAmtByParams
     * @方法描述：根据合同号,台账状态查询台帐出账总额
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    BigDecimal selectSumLoanAmtByParams(@Param("contNo") String contNo, @Param("sts") String[] status);

    /**
     * @方法名称：selectSumLoanAmtByParams
     * @方法描述：根据合同号，台账状态查询贷款总余额
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    BigDecimal selectSumLoanBalAmtByParams(@Param("contNo") String contNo, @Param("sts") String[] status);

    /**
     * @方法名称：selectSumLoanAmtByParams
     * @方法描述：查询7日内小贷台帐出账总额
     * @创建人：
     * @创建时间：2021/6/5 10:45
     * @修改记录：修改时间 修改人员 修改时间
     */
    BigDecimal selectXdSumLoanAmtIn7th(@Param("cusId") String cusId, @Param("openDay") String openDay, @Param("dayBefore") String dayBefore);

    /**
     * 获取贷款余额
     *
     * @param contNo
     * @return
     */
    BigDecimal getLoanBalanceByContNo(String contNo);

    /**
     * 查询客户所在企业是否有逾期、欠息贷款
     *
     * @param queryMap
     * @return
     */
    int queryAccLoanDebitInit(Map<String, String> queryMap);

    /**
     * 根据billNo获取台账
     *
     * @param bill_no
     * @return
     */
    int getAccLoanCountByBillNo(String bill_no);

    /**
     * 根据billNo获取台账
     *
     * @param cusId
     * @return
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    List<AccLoanDto> selectByCusIdOnAssetType(@Param("cusId") String cusId);

    /**
     * 根据billNo获取台账
     *
     * @return
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    List<AccLoanDto> selectByCusId(AccLoanDto accLoandto);

    /**
     * @param billno: 台账编号
     * @Description:根据台账编号，查询产品Id
     * @Author: YX-WJ
     * @Date: 2021/6/5 18:53
     * @return: java.lang.String
     **/
    String selectPrdIdByBillNo(@Param("billno") String billno);

    /**
     * @param billno: 借据编号
     * @Description:根据借据编号，查询产品类型
     * @Author: zl
     * @Date: 2021/6/11 18:53
     * @return: java.lang.String
     **/
    String selectPrdTypeByBillNo(@Param("billno") String billno);

    /**
     * 根据客户号查询是否存在未关闭的贷款台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAccByCusId(String cusId);

    /**
     * 根据合同号查询贷款台账信息
     *
     * @param contNo
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0012.resp.BillList> getAccLoanList(@Param("contNo") String contNo);


    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询  不包括台账状态
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    List<AccLoanDto> selectByCusIdNotType(@Param("cusId") String cusId);

    /**
     * 根据借据号查询可还款的台账
     *
     * @param billNo
     * @param managerId
     * @return
     */
    List<AccLoan> selectRepayAccLoanByParam(@Param("billNo") String billNo, @Param("managerId") String managerId);

    /**
     * 根据借据编号获取科目类型
     *
     * @param old_bill_no
     * @return
     */
    String getSubjectNoByBillNo(@Param("billNo") String old_bill_no);

    /**
     * @param cusId: 客户号
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    void deleteByBillNo(@Param("billNo") String billNo);

    /**
     * @param
     * @Description:
     * @Author:ZMW
     * @Date: 2021/6/9 23:40
     * @return: int
     **/
    List<AccLoanDto> queryAccLoanByCusId(QueryModel model);


    /**
     * 根据合同号查询借据所对应的汇率
     *
     * @param contNo
     * @return
     */
    List<AccLoan> selectRateAccLoanByContNo(@Param("contNo") String contNo);

    /**
     * 计算借据下贷款余额
     *
     * @param billNo
     * @return
     */
    BigDecimal selectAccLoanAmtByBillNo(@Param("billNo") String billNo);


    /**
     * 根据借合同编号获得借据编号
     *
     * @param cont_id
     * @return
     * @Author: YX-QX
     */
    String getBillNoByContNo(@Param("cont_id") String cont_id);

    /**
     * 该客户所在企业贷款历史总额
     *
     * @param cus_id
     * @return
     * @Author: YX-LZW
     */
    BigDecimal getHisLoanAmt(@Param("cus_id") String cus_id);

    /**
     * 获取全行线上签约的合同的总贷款余额
     *
     * @return
     * @Author: YX-LZW
     */
    BigDecimal getTotalOnlineLoanBalance();

    /**
     * 获取该客户名下有效的优企贷贷款余额
     * @param cusId
     * @return
     * @Author:
     */
    BigDecimal getLoanBalanceBycusIdforYqd(@Param("cusId") String cusId);

    /**
     * 根据原合同编号查询对应的借据的到期日期
     *
     * @param cont_no
     * @return
     */
    String selectLoanEndDateBycontno(@Param("cont_no") String cont_no);

    /**
     * 获取合同下7天内的放款金额
     *
     * @return
     * @Author: YX-LZW
     */
    BigDecimal get7DayTotalLoanAmtByContNo(Map<String, String> queryMap);

    /**
     * 根据合同号查询货款余额
     *
     * @param contNo
     * @return
     */
    BigDecimal selectAccloanByContnoOne(@Param("contNo") String contNo);

    /**
     * 根据合同号查询货款金额
     *
     * @param contNo
     * @return
     */
    BigDecimal selectAccLoangetByContnogetAmt(@Param("contNo") String contNo);

    /**
     * 根据合同号查询贷款余额
     *
     * @param cont_no
     * @return
     */
    String selectAccLoanBycontno(@Param("cont_no") String cont_no);

    /**
     * @param queryMap: 借据编号
     * @Description:根据借据编号更新贷款台账信息
     * @Date: 2021/6/13 23:40
     * @return: int
     **/
    int updateAccLoanByBillNo(Map<String, String> queryMap);

    /**
     * @param
     * @Description:查询客户房贷列表
     * @Author:
     * @Date: 2021/6/18 23:40
     * @return:
     **/
    List<AccLoanDto> queryHouseAccLoanListByCus(@Param("cusId") String cusId, @Param("cusName") String cusName);

    /**
     * @方法名称：selectInfoByCusIdDate
     * @方法描述：根据客户编号灵活查询
     * @创建人：
     * @创建时间：
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model);

    /**
     * @param model 分页查询类
     * @函数名称:
     * @函数描述:查询贷款台账下的机构信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<String> selectSigDisbOrgIdListByModel(QueryModel model);

    /**
     * @param
     * @函数名称:
     * @函数描述:综合查询（贷款风险分类支行汇总表（实时））
     * @参数与返回说明:
     * @算法描述:
     */
    AccDiscClassificationDto selectClassificationSubBranchSummary(@Param("orgCode") String orgCode);

    /**
     * @param
     * @函数名称:
     * @函数描述:查询企业：企业在我行有未结清逾期、呆账、呆滞、垫款业务；
     * @参数与返回说明:
     * @算法描述:
     */
    int getEnterpriseCount(Xdtz0060DataReqDto xdtz0060DataReqDto);

    /**
     * @param
     * @函数名称:
     * @函数描述:查询企业实际控制人：企业实际控制人在我行贷款存在逾期、呆账、呆滞 （台账状态）
     * @参数与返回说明:
     * @算法描述:
     */
    int getControllerCount(Xdtz0060DataReqDto xdtz0060DataReqDto);

    /**
     * @param queryMap: 借据编号
     * @Description: 优企贷还款账号变更
     * @Date: 2021/9/13 23:40
     * @return: int
     **/
    int updateAccLoanRepayAcctNo(Map<String, String> queryMap);

    /**
     * 根据查询条件查询台账信息并返回
     *
     * @param model
     * @return
     */
    List<AccLoan> querymodelByCondition(QueryModel model);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询(pvp_authorize)
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> selectByModelAll(QueryModel model);

    AccLoanAndPvpDto selectByBillNoPvp(AccLoan accLoan);

    /**
     * 根据担保合同编号查询对应的借款合同关联的未结清贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    int countUsedRecordsByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据担保合同编号查询对应的借款合同关联的贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    int countAccLoanRecordsByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据合同编号查询未结清贷款台账记录数
     *
     * @param contNo
     * @return
     */
    int countUsedRecordsByContNo(@Param("contNo") String contNo);


    /**
     * 查询担保合同对应的核销及转让贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    int countHxAndZrAccLoanRecords(@Param("guarContNo") String guarContNo);

    /**
     * 查询担保合同编号对应的产品是个人一手住房按揭贷款、个人一手商用房按揭贷款的贷款台账记录数
     *
     * @param guarContNo
     * @return
     */
    int countFirstHouseAccLoanRecords(@Param("guarContNo") String guarContNo);

    /**
     * 根据业务流水查询贷款余额
     *
     * @param iqpSerno
     * @return
     */
    BigDecimal queryAccLoanLoanBalanceSum(@Param("iqpSerno") String iqpSerno);

    /**
     * 根据客户号查询最新一笔抵押借据的余额
     *
     * @param cusId
     * @return
     */
    BigDecimal getNewestGrtAccLoanLoanBalance(@Param("cusId") String cusId);

    /**
     * 根据客户号查询是否存在已关闭的贷款台账列表信息
     *
     * @param model
     * @return
     */
    List<AccLoan> queryAccLoanListByCusId(QueryModel model);

    /**
     * 获取机构限额
     *
     * @param orgIdList
     * @param belgLine
     * @return
     */
    BigDecimal getCurrMonthAllowLoanBalance(@Param("orgIdList") List<String> orgIdList, @Param("belgLine") String belgLine, @Param("prdIds") List<String> prdIds);

    /**
     * @方法名称: queryAccLoanByContNo
     * @方法描述: 根据业务流水号查询额度信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AccLoan> queryAccLoanByContNo(Map params);

    /**
     * @方法名称: selectUnClearByContNo
     * @方法描述: 根据合同编号查询未结清的贷款余额
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectUnClearByContNo(@Param("contNo") String contNo);

    /**
     * @return
     * @方法名称: selectSumLoanByTContNo
     * @方法描述: 根据合同编号查询未结清
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectSumLoanByTContNo(String tContNo);

    /**
     * @param cusId
     * @return
     * @方法名称: selectFiveClassByCusId
     * @方法描述: 通过客户编号查询台账的五级分类
     * @参数与返回说明:
     * @算法描述: 无
     * 刘权
     */
    AccLoan selectFiveClassByCusId(@Param("cusId") String cusId);


    /**
     * 根据客户号查所有零售按揭贷款余额
     *
     * @param cusId
     * @return
     */
    BigDecimal selectAccLoanBalanceAj(@Param("cusId") String cusId);

    /**
     * 根据客户号查所有零售非按揭贷款余额
     *
     * @param params
     * @return
     */
    BigDecimal selectAccLoanBalanceFaj(Map params);

    /**
     * 根据客户号查所有零售按揭贷款金额
     *
     * @param cusId
     * @return
     */
    BigDecimal selectLoanAmtAj(@Param("cusId") String cusId);

    /**
     * 根据客户号查所有零售非按揭贷款金额
     *
     * @param params
     * @return
     */
    BigDecimal selectLoanAmtFaj(Map params);

    /**
     * @param
     * @Description:
     * @Author:ZMW
     * @Date: 2021/6/9 23:40
     * @return: int
     **/
    List<AccLoanDto> queryBadCus(QueryModel model);

    /**
     * @param model
     * @函数名称:asplAccLoanList
     * @函数描述:资产池贷款台账
     * @参数与返回说明:
     * @算法描述:
     */
    List<AccLoan> asplAccLoanList(QueryModel model);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateAccLoan(AccLoanDto accLoanDto);

    /**
     * 获取多笔借据号下最大逾期天数
     * @author jijian_yx
     * @date 2021/10/13 21:19
     **/
    int getMaxOverdueDay(@Param("billNos") List<String> billNos);

    /**
     * 根据合同号获取各合同余额
     * @author zdl
     **/
    @IgnoredDataAuthority
    List<LoanContList> getLoanBalanceByContNos(@Param("contNos") List<String> contNos);


    /**
     * 根据客户号查询是否存在未关闭的贷款台账ZX
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAccZXByCusId(String cusId);

    /**
     * 根据客户号查询是否存在未结清的银承台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAccpByCusId(String cusId);

    /**
     * 根据客户号查询是否存在未结清的贴现台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAccdByCusId(String cusId);

    /**
     * 根据客户号查询是否存在未结清的保函台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAcccByCusId(String cusId);

    /**
     * 根据客户号查询是否存在未结清的开证台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAcctByCusId(String cusId);

    /**
     * 根据客户号查询是否存在未结清的开证台账
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusNotClosedAcceByCusId(String cusId);

    /**
     * 根据合同编号查询贷款余额总和
     * @param contNos
     * @return
     */
    BigDecimal selectTotalSpacAmtByContNos(@Param("contNos") String contNos);

    /**
     * @author zhoumw
     * @date 2021年10月22日16:52:28
     * @version 1.0.0
     * @desc 根据借据编号查询五级十级分类
     * @修改历史 修改时间 修改人员 修改原因
     */
    AccLoanDto selectClassByBillNo(@Param("billNo") String billNo);

    List<AccLoan> selectByContNoAndStatus(@Param("contNo") String contNo);

    List<AccLoan> selectByPvpSernoAndModal(QueryModel model);
}