package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrYddSignResult
 * @类描述: ctr_ydd_sign_result数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-10 20:42:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrYddSignResultDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 证件号码 **/
	private String certCode;
	/** 合同编号 **/
	private String contNo;
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 借款合同签约状态 **/
	private String loanContSignStatus;
	
	/** 担保合同签约状态 **/
	private String guarContSignStatus;
	
	/** 主债权合同及抵押合同表签约状态 **/
	private String claimContSignStatus;
	
	/** 不动产登记申请书签约状态 **/
	private String registAppSignStatus;
	
	/** 影像目录 **/
	private String imageDir;
	
	/** 主债权合同及抵押合同表编号 **/
	private String claimContNo;
	
	/** 不动产登记申请书编号 **/
	private String registAppNo;
	
	/** 签约时间 **/
	private String signDate;
	
	/** 借款合同影像流水号 **/
	private String loanContImageNo;
	
	/** 担保合同影像流水号 **/
	private String guarContImageNo;
	
	/** 主债权合同影像流水号 **/
	private String claimContImageNo;
	
	/** 登记申请书影像流水号 **/
	private String registAppImageNo;
	
	/** 是否主借款人 **/
	private String isMainBorrower;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param loanContSignStatus
	 */
	public void setLoanContSignStatus(String loanContSignStatus) {
		this.loanContSignStatus = loanContSignStatus == null ? null : loanContSignStatus.trim();
	}
	
    /**
     * @return LoanContSignStatus
     */	
	public String getLoanContSignStatus() {
		return this.loanContSignStatus;
	}
	
	/**
	 * @param guarContSignStatus
	 */
	public void setGuarContSignStatus(String guarContSignStatus) {
		this.guarContSignStatus = guarContSignStatus == null ? null : guarContSignStatus.trim();
	}
	
    /**
     * @return GuarContSignStatus
     */	
	public String getGuarContSignStatus() {
		return this.guarContSignStatus;
	}
	
	/**
	 * @param claimContSignStatus
	 */
	public void setClaimContSignStatus(String claimContSignStatus) {
		this.claimContSignStatus = claimContSignStatus == null ? null : claimContSignStatus.trim();
	}
	
    /**
     * @return ClaimContSignStatus
     */	
	public String getClaimContSignStatus() {
		return this.claimContSignStatus;
	}
	
	/**
	 * @param registAppSignStatus
	 */
	public void setRegistAppSignStatus(String registAppSignStatus) {
		this.registAppSignStatus = registAppSignStatus == null ? null : registAppSignStatus.trim();
	}
	
    /**
     * @return RegistAppSignStatus
     */	
	public String getRegistAppSignStatus() {
		return this.registAppSignStatus;
	}
	
	/**
	 * @param imageDir
	 */
	public void setImageDir(String imageDir) {
		this.imageDir = imageDir == null ? null : imageDir.trim();
	}
	
    /**
     * @return ImageDir
     */	
	public String getImageDir() {
		return this.imageDir;
	}
	
	/**
	 * @param claimContNo
	 */
	public void setClaimContNo(String claimContNo) {
		this.claimContNo = claimContNo == null ? null : claimContNo.trim();
	}
	
    /**
     * @return ClaimContNo
     */	
	public String getClaimContNo() {
		return this.claimContNo;
	}
	
	/**
	 * @param registAppNo
	 */
	public void setRegistAppNo(String registAppNo) {
		this.registAppNo = registAppNo == null ? null : registAppNo.trim();
	}
	
    /**
     * @return RegistAppNo
     */	
	public String getRegistAppNo() {
		return this.registAppNo;
	}
	
	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate == null ? null : signDate.trim();
	}
	
    /**
     * @return SignDate
     */	
	public String getSignDate() {
		return this.signDate;
	}
	
	/**
	 * @param loanContImageNo
	 */
	public void setLoanContImageNo(String loanContImageNo) {
		this.loanContImageNo = loanContImageNo == null ? null : loanContImageNo.trim();
	}
	
    /**
     * @return LoanContImageNo
     */	
	public String getLoanContImageNo() {
		return this.loanContImageNo;
	}
	
	/**
	 * @param guarContImageNo
	 */
	public void setGuarContImageNo(String guarContImageNo) {
		this.guarContImageNo = guarContImageNo == null ? null : guarContImageNo.trim();
	}
	
    /**
     * @return GuarContImageNo
     */	
	public String getGuarContImageNo() {
		return this.guarContImageNo;
	}
	
	/**
	 * @param claimContImageNo
	 */
	public void setClaimContImageNo(String claimContImageNo) {
		this.claimContImageNo = claimContImageNo == null ? null : claimContImageNo.trim();
	}
	
    /**
     * @return ClaimContImageNo
     */	
	public String getClaimContImageNo() {
		return this.claimContImageNo;
	}
	
	/**
	 * @param registAppImageNo
	 */
	public void setRegistAppImageNo(String registAppImageNo) {
		this.registAppImageNo = registAppImageNo == null ? null : registAppImageNo.trim();
	}
	
    /**
     * @return RegistAppImageNo
     */	
	public String getRegistAppImageNo() {
		return this.registAppImageNo;
	}
	
	/**
	 * @param isMainBorrower
	 */
	public void setIsMainBorrower(String isMainBorrower) {
		this.isMainBorrower = isMainBorrower == null ? null : isMainBorrower.trim();
	}
	
    /**
     * @return IsMainBorrower
     */	
	public String getIsMainBorrower() {
		return this.isMainBorrower;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}