package cn.com.yusys.yusp.service.server.xdtz0050;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0050.resp.Xdtz0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：FRUIT
 * @创建时间：2021/6/15 22:14
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xdtz0050Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0050Service.class);
    @Autowired
    private LmtAppMapper lmtAppMapper;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private CtrDiscContMapper ctrDiscContMapper;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;

    @Autowired
    private CtrTfLocContMapper ctrTfLocContMapper;

    @Autowired
    private CtrContExtMapper ctrContExtMapper;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;

    @Autowired
    private AccAccpMapper accAccpMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private AccDiscMapper accDiscMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private IqpContExtMapper iqpContExtMapper;

    @Autowired
    private AccTfLocMapper accTfLocMapper;

    @Autowired
    private CoopPartnerLstInfoMapper coopPartnerLstInfoMapper;

    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;

    /**
     * @param cusId:客户号
     * @Description:根据传入的cusId，检测客户号是否可以删除或者归并
     * @Author: YX-WJ
     * @Date: 2021/6/15 22:15
     * @return: cn.com.yusys.yusp.dto.server.xdtz0050.resp.Xdtz0050DataRespDto
     **/
    public Xdtz0050DataRespDto xdtz0050(String cusId) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value,JSON.toJSONString(cusId));
        //标准返回
        Xdtz0050DataRespDto xdtz0050DataRespDto = new Xdtz0050DataRespDto();
        try {
            //客户存在业务信息的数量默认是0，存在是1
            Integer flagInt = 0;
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, cusId);
            //检查该客户是否存在业务信息，存在，则flagInt+1，结束方法
            checkIfHasBusiness(xdtz0050DataRespDto, flagInt, cusId);
            // 23、存在返回可删除标志为0，不存在返回1 提示： 信贷管理系统存在" + returnStr +信息
            if (flagInt == 0) {
                xdtz0050DataRespDto.setDeletableFlag("1");
                xdtz0050DataRespDto.setOpMsg("该客户不存在业务信息。");
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, cusId);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value,JSON.toJSONString(xdtz0050DataRespDto));
        return xdtz0050DataRespDto;
    }

    /**
     * @param tableNameInfo: 存在业务信息的表的信息
     * @Description:存在业务信息则直接返回
     * @Author: YX-WJ
     * @Date: 2021/6/15 23:23
     * @return: void
     **/
    private void returnErrorInfo(String tableNameInfo, Xdtz0050DataRespDto xdtz0050DataRespDto) {
        xdtz0050DataRespDto.setDeletableFlag("0");
        xdtz0050DataRespDto.setOpMsg("信贷管理系统存在" + tableNameInfo + "信息");
        return;
    }

    /**
     *
     * @param xdtz0050DataRespDto
     * @param flagInt
     * @param cusId :客户号
     * @Description:检查客户是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/10 18:40
     * @return: void
     **/
    private void checkIfHasBusiness(Xdtz0050DataRespDto xdtz0050DataRespDto, int flagInt, String cusId) {
        String tableNameInfo = "";
        // 根据客户号校验是否存在授信申请表 LMT_APP 信息
        flagInt += lmtAppMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "授信申请表 LMT_APP";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在银票协议详情 CTR_ACCP_CONT
        flagInt += ctrAccpContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "银票协议详情 CTR_ACCP_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保函协议详情 CTR_CVRG_CONT
        flagInt += ctrCvrgContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "保函协议详情 CTR_CVRG_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贴现协议详情 CTR_DISC_CONT
        flagInt += ctrDiscContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "贴现协议详情 CTR_DISC_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贷款合同详情 CTR_LOAN_CONT
        flagInt += ctrLoanContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "贷款合同详情 CTR_LOAN_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在委托贷款合同详情 CTR_ENTRUST_LOAN_CONT
        flagInt += ctrEntrustLoanContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "委托贷款合同详情 CTR_ENTRUST_LOAN_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在开证合同详情 CTR_TF_LOC_CONT
        flagInt += ctrTfLocContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "开证合同详情 CTR_TF_LOC_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }

        // 根据客户号校验是否存在提货担保协议 CTR_TF_PGAS 老信贷无数据表 todo

        //根据客户号校验是否存在 展期协议  iqp_cont_ext
        flagInt += iqpContExtMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "展期协议 iqp_cont_ext";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }

        // 根据客户号校验是否存在展期协议主表 ctr_cont_ext
        flagInt += ctrContExtMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "展期协议主表 ctr_cont_ext";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }

        // 根据客户号校验是否存在 CTR_TF_LOC_EXT 进口开证修改合同  新信贷未找到此表 todo

        // 根据客户号校验是否存在担保合同表 GRT_GUAR_CONT
        flagInt += grtGuarContMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "担保合同表 GRT_GUAR_CONT";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在 GUAR_BASE_INFO 抵质押信息
        flagInt += guarBaseInfoMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "GUAR_BASE_INFO 抵质押信息";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保证人 GUAR_GUARANTEE 信息
        flagInt += guarGuaranteeMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "保证人 GUAR_GUARANTEE";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验合作方额度信息 COOP_PARTNER_LST_INFO
        flagInt += coopPartnerLstInfoMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "合作方额度信息 COOP_PARTNER_LST_INFO";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }

        // 根据客户号校验是否存在银承台账 ACC_ACCP
        flagInt += accAccpMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "银承台账 ACC_ACCP";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在保函台账 ACC_CVRS
        flagInt += accCvrsMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "保函台账 ACC_CVRS";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贴现台账 ACC_DISC
        flagInt += accDiscMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "贴现台账 ACC_DISC";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在贷款台账 ACC_LOAN
        flagInt += accLoanMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "贷款台账 ACC_LOAN";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在委托贷款台账 ACC_ENTRUST_LOAN
        flagInt += accEntrustLoanMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "委托贷款台账 ACC_ENTRUST_LOAN";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在信用证台帐 ACC_TF_LOC
        flagInt += accTfLocMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "信用证台帐 ACC_TF_LOC";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }
        // 根据客户号校验是否存在 ACC_TF_PGAS 提货担保台帐 信贷新信贷无此表 todo


        // 根据客户号校验是否存在合作方案台账信息 COOP_PLAN_ACC_INFO
        flagInt += coopPlanAccInfoMapper.selectContByCusId(cusId);
        if (flagInt != 0) {
            // 存在业务信息则直接返回
            tableNameInfo = "合作方案台账信息 COOP_PLAN_ACC_INFO";
            returnErrorInfo(tableNameInfo, xdtz0050DataRespDto);
            return;
        }

    }


}
