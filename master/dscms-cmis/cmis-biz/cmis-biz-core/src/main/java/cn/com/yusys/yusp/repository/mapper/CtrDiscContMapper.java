/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrDiscCont;
import cn.com.yusys.yusp.dto.server.xdht0003.req.Xdht0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.Xdht0003DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrDiscContMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-13 09:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrDiscContMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CtrDiscCont selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrDiscCont> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrDiscCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrDiscCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrDiscCont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrDiscCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 信贷贴现合同号查询
     * @param xdht0003DataReqDto
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0003.resp.List> getXdht0003(Xdht0003DataReqDto xdht0003DataReqDto);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrDiscCont> selectByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);


    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:20
     * @修改记录：修改时间 修改人员 修改时间
    */
    CtrDiscCont selectByIqpSerno(@Param("serno") String serno);

    CtrDiscCont selectByContNo(@Param("contNo") String contNo);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * @Description:根据合同号更新lmtAccNo
     * @Author: YX-lihh
     * @Date: 2021/6/9 23:40
     * @param record
     * @return: int
     **/
    int updateLmtAccNoByContNo(CtrDiscCont record);

    /**
     * 根据额度编号查询合同金额总和
     *
     * @param lmtAccNo
     * @return
     */
    BigDecimal getSumContAmt(String lmtAccNo);
}