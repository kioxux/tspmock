package cn.com.yusys.yusp.service.server.xddh0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.service.CmisPspClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/5 17:29
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xddh0001Service {

    @Autowired
    private CmisPspClientService cmisPspClientService;

    public Xddh0001DataRespDto xddh0001(Xddh0001DataReqDto xddh0001DataReqDto) {
        Xddh0001DataRespDto xddh0001DataRespDto = new Xddh0001DataRespDto();

        String iotspotTime = xddh0001DataReqDto.getIotspotTime();
        String status = xddh0001DataReqDto.getStatus();
        String serno = xddh0001DataReqDto.getSerno();
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("checkDate", iotspotTime);
        paramMap.put("checkType", status);
        paramMap.put("taskNo", serno);
        ResultDto<Boolean> booleanResultDto = cmisPspClientService.updatePspTaskListInfo(paramMap);
        Boolean data = booleanResultDto.getData();
        //3.更新完毕后返回serno
        if (data) {
            xddh0001DataRespDto.setSerno(serno);
        }
        return xddh0001DataRespDto;
    }
}
