package cn.com.yusys.yusp.service.server.xdxw0071;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0071.req.Xdxw0071DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0071.resp.Xdxw0071DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


/**
 * @Author zhangpeng
 * @Date 2021/4/26 16:20
 * @Version 1.0
 */
@Service
public class Xdxw0071Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0071Service.class);

    @Autowired
    private LmtGuareInfoMapper lmtGuareInfoMapper;

    /**
     * 查询优抵贷抵质押品信息
     *
     * @param xdxw0071DataReqDto
     * @return
     */
    public Xdxw0071DataRespDto xdxw0071(Xdxw0071DataReqDto xdxw0071DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataReqDto));
        Xdxw0071DataRespDto xdxw0071DataRespDto = new Xdxw0071DataRespDto();
        try {
            String indgtSerno = xdxw0071DataReqDto.getIndgtSerno();//客户调查表编号
            String queryType = xdxw0071DataReqDto.getQueryType(); //获取查询类型

            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0071.resp.List> list = new ArrayList<>();

            if("01".equals(queryType)){
                list = lmtGuareInfoMapper.selectBySurveyNoFirst(indgtSerno);
            }else{
                list = lmtGuareInfoMapper.selectBySurveyNoSecond(indgtSerno);
            }
            //返回信息
            xdxw0071DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataRespDto));
        return xdxw0071DataRespDto;
    }
}
