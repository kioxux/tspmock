package cn.com.yusys.yusp.service.client.bsp.core.ln3025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：垫款开户
 *
 * @author zrcbank-fengjj
 * @version 1.0
 * @since 2021年11月16日 上午10:56:06
 */
@Service
public class Ln3025Service {
    private static final Logger logger = LoggerFactory.getLogger(Ln3025Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：垫款开户
     *
     * @param ln3025ReqDto
     * @return
     */
    public Ln3025RespDto ln3025(Ln3025ReqDto ln3025ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value, JSON.toJSONString(ln3025ReqDto));
        ResultDto<Ln3025RespDto> ln3025ResultDto = dscms2CoreLnClientService.ln3025(ln3025ReqDto);
        System.out.println("ln3025ReqDto: "+ ln3025ReqDto.toString());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value, JSON.toJSONString(ln3025ResultDto));
        String ln3025Code = Optional.ofNullable(ln3025ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3025Meesage = Optional.ofNullable(ln3025ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3025RespDto ln3025RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3025ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3025RespDto = ln3025ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3025Code, ln3025Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value);
        return ln3025RespDto;
    }


}
