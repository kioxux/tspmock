package cn.com.yusys.yusp.service.server.xdht0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.resp.List;
import cn.com.yusys.yusp.dto.server.xdht0007.resp.Xdht0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0007Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-0611:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0007Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0007Service.class);

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Resource
    private GrtGuarContMapper grtGuarContMapper;

    /**
     * 借款合同/担保合同列表信息查询
     *
     * @param xdht0007DataReqDto
     * @return xdht0007DataRespDto
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0007DataRespDto getXdht0007(Xdht0007DataReqDto xdht0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataReqDto));
        Xdht0007DataRespDto xdht0007DataRespDto = new Xdht0007DataRespDto();
        try {
            //合同类型
            //01-借款（列表）
            //02-担保
            String contType = xdht0007DataReqDto.getContType();
            java.util.List<List> list = new ArrayList<>();
            Integer pageNum = xdht0007DataReqDto.getPageNum();
            Integer pageSize = xdht0007DataReqDto.getPageSize();
            if (Objects.equals(contType, CommonConstance.CONT_QRY_TYPE_01)) {
                //查询借款合同列表
                PageHelper.startPage(pageNum, pageSize);
                //签约视频
                list = ctrLoanContMapper.getCrtContList(xdht0007DataReqDto);
            } else if (Objects.equals(contType, CommonConstance.CONT_QRY_TYPE_02)) {
                String contNo = xdht0007DataReqDto.getContNo();
                if (Objects.nonNull(contNo)) {
                    //查询是否有可用担保编号
                    java.util.List<String> contList = grtGuarContMapper.getGuarContNo(contNo);
                    if (contList.size() > 0) {
                        list = grtGuarContMapper.getGrtContList(contNo);
                    } else {
                        list = ctrLoanContMapper.getCrtContByContNo(contNo);
                    }
                }
            }
            xdht0007DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataRespDto));
        return xdht0007DataRespDto;
    }
}
