/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.RptFncSituBsLowRisk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarLowRisk;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarLowRiskService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarLowRiskResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-23 21:20:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarlowrisk")
public class RptLmtRepayAnysGuarLowRiskResource {
    @Autowired
    private RptLmtRepayAnysGuarLowRiskService rptLmtRepayAnysGuarLowRiskService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarLowRisk>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarLowRisk> list = rptLmtRepayAnysGuarLowRiskService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarLowRisk>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarLowRisk>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarLowRisk> list = rptLmtRepayAnysGuarLowRiskService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarLowRisk>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptLmtRepayAnysGuarLowRisk> show(@PathVariable("pkId") String pkId) {
        RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGuarLowRisk = rptLmtRepayAnysGuarLowRiskService.selectByPrimaryKey(pkId);
        return new ResultDto<RptLmtRepayAnysGuarLowRisk>(rptLmtRepayAnysGuarLowRisk);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarLowRisk> create(@RequestBody RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGuarLowRisk) throws URISyntaxException {
        rptLmtRepayAnysGuarLowRiskService.insert(rptLmtRepayAnysGuarLowRisk);
        return new ResultDto<RptLmtRepayAnysGuarLowRisk>(rptLmtRepayAnysGuarLowRisk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGuarLowRisk) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarLowRiskService.update(rptLmtRepayAnysGuarLowRisk);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptLmtRepayAnysGuarLowRiskService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptLmtRepayAnysGuarLowRiskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGrptlmtrepayanysuarLowRisk
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptLmtRepayAnysGuarLowRisk>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptLmtRepayAnysGuarLowRisk>>(rptLmtRepayAnysGuarLowRiskService.selectByModel(model));
    }
    @PostMapping("/deleteLowRisk")
    protected ResultDto<Integer> deleteLowRisk(@RequestBody RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGuarLowRisk){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarLowRiskService.deleteByPrimaryKey(rptLmtRepayAnysGuarLowRisk.getPkId()));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptLmtRepayAnysGuarLowRisk rptLmtRepayAnysGuarLowRisk){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarLowRiskService.save(rptLmtRepayAnysGuarLowRisk));
    }
}
