package cn.com.yusys.yusp.web.server.xdtz0046;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0046.req.Xdtz0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0046.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0046.resp.Xdtz0046DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:根据借据号获取共同借款人信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0046:根据借据号获取共同借款人信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0046Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0046Resource.class);

    /**
     * 交易码：xdtz0046
     * 交易描述：根据借据号获取共同借款人信息
     *
     * @param xdtz0046DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据号获取共同借款人信息")
    @PostMapping("/xdtz0046")
    protected @ResponseBody
    ResultDto<Xdtz0046DataRespDto> xdtz0046(@Validated @RequestBody Xdtz0046DataReqDto xdtz0046DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046DataReqDto));
        Xdtz0046DataRespDto xdtz0046DataRespDto = new Xdtz0046DataRespDto();// 响应Dto:根据借据号获取共同借款人信息
        ResultDto<Xdtz0046DataRespDto> xdtz0046DataResultDto = new ResultDto<>();
        String billNo = xdtz0046DataReqDto.getBillNo();//借据号
        String certNo = xdtz0046DataReqDto.getCertNo();//身份证号
        try {
            // 从xdtz0046DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdtz0046DataRespDto对象开始
            List list = new List();
            xdtz0046DataRespDto.setLevel(StringUtils.EMPTY);// 层级
            xdtz0046DataRespDto.setDebit(StringUtils.EMPTY);// 借款人
            xdtz0046DataRespDto.setDebitCertNo(new BigDecimal(0L));// 借款人身份证号
            xdtz0046DataRespDto.setContNo(new BigDecimal(0L));// 合同号
            xdtz0046DataRespDto.setPRInterzone(StringUtils.EMPTY);// lpr区间
            xdtz0046DataRespDto.setPRRate(StringUtils.EMPTY);// lpr利率
            xdtz0046DataRespDto.setAddDeclFlag(new BigDecimal(0L));// 加减标志
            xdtz0046DataRespDto.setIrFloatNum(new BigDecimal(0L));// 浮动点数
            xdtz0046DataRespDto.setIsHouseLoan(StringUtils.EMPTY);// 是否为房贷
            xdtz0046DataRespDto.setRealityIrY(StringUtils.EMPTY);// 执行利率年
            xdtz0046DataRespDto.setIrAdjustType(new BigDecimal(0L));// 利率调整方式
            xdtz0046DataRespDto.setIsCommHouse(new BigDecimal(0L));// 是否为商用房
            list.setCommonDebitCertNo(StringUtils.EMPTY);// 共同借款人证件号
            list.setCommonDebitName(StringUtils.EMPTY);// 共同借款人名称
            list.setSignFlag(new BigDecimal(0L));// 是否签约
            list.setSubRela(new BigDecimal(0L));// 主副关系
            xdtz0046DataRespDto.setList(Arrays.asList(list));
            // TODO 封装xdtz0046DataRespDto对象结束
            // 封装xdtz0046DataResultDto中正确的返回码和返回信息
            xdtz0046DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0046DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, e.getMessage());
            // 封装xdtz0046DataResultDto中异常返回码和返回信息
            xdtz0046DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0046DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0046DataRespDto到xdtz0046DataResultDto中
        xdtz0046DataResultDto.setData(xdtz0046DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046DataResultDto));
        return xdtz0046DataResultDto;
    }
}
