package cn.com.yusys.yusp.service.server.xdtz0029;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0029.resp.Xdtz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询指定票号在信贷台账中是否已贴现
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0029Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0029Service.class);

    @Autowired
    private AccDiscMapper accDiscMapper;

    /**
     * 查询指定票号在信贷台账中是否已贴现
     * @param porderNo
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0029DataRespDto getPorderByPorderNo(String porderNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, porderNo);
        Xdtz0029DataRespDto xdtz0029DataRespDto = accDiscMapper.getPorderByPorderNo(porderNo);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataRespDto));
        return xdtz0029DataRespDto;
    }


}
