/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBuilProject;
import cn.com.yusys.yusp.domain.GuarInfBuildUse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarInfBuildUse;
import cn.com.yusys.yusp.repository.mapper.GuarInfBuildUseMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuildUseService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarInfBuildUseService {

    @Autowired
    private GuarInfBuildUseMapper guarInfBuildUseMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarInfBuildUse selectByPrimaryKey(String serno) {
        return guarInfBuildUseMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarInfBuildUse> selectAll(QueryModel model) {
        List<GuarInfBuildUse> records = (List<GuarInfBuildUse>) guarInfBuildUseMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarInfBuildUse> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarInfBuildUse> list = guarInfBuildUseMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarInfBuildUse record) {
        return guarInfBuildUseMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarInfBuildUse record) {
        return guarInfBuildUseMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarInfBuildUse record) {
        return guarInfBuildUseMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarInfBuildUse record) {
        return guarInfBuildUseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarInfBuildUseMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarInfBuildUseMapper.deleteByIds(ids);
    }

    /**
     * 对guarInfBuildUse单个对象保存(新增或修改),如果没有数据新增,有数据修改
     * @param guarInfBuildUse
     * @return
     */
    public int preserveGuarInfBuildUse(GuarInfBuildUse guarInfBuildUse) {
        String serno = guarInfBuildUse.getSerno();
        //查询该对象是否存在
        GuarInfBuildUse queryGuarInfBuildUse = guarInfBuildUseMapper.selectByPrimaryKey(serno);
        if (null == queryGuarInfBuildUse){
            return guarInfBuildUseMapper.insert(guarInfBuildUse);
        }else{
            return guarInfBuildUseMapper.updateByPrimaryKey(guarInfBuildUse);
        }
    }

    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明: 存在 为 1 ,不存在为 0
     * @算法描述:
     */
    public int checkGuarInfoIsExist(String serno) {
        GuarInfBuildUse queryGuarInfBuildUse = guarInfBuildUseMapper.selectByPrimaryKey(serno);
        return (null==queryGuarInfBuildUse || null==queryGuarInfBuildUse.getSerno()||"".equals(queryGuarInfBuildUse.getSerno())) ? 0 : 1 ;
    }

    /**
     * @方法名称: selectByGuarNo
     * @方法描述: 根据通用押品编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarInfBuildUse selectByGuarNo(String guarNo) {
        return guarInfBuildUseMapper.selectByGuarNo(guarNo);
    }
}
