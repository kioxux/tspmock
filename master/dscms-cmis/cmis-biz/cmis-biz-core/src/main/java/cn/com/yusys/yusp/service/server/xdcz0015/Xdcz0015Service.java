package cn.com.yusys.yusp.service.server.xdcz0015;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.DocImageSpplInfo;
import cn.com.yusys.yusp.dto.server.xdcz0015.req.Xdcz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.resp.Xdcz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.DocImageSpplInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdcz0015Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-11 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0015Service.class);

    @Resource
    private DocImageSpplInfoMapper DocImageSpplInfoMapper;

    /**
     * 信息锁定标志同步
     *
     * @param xdcz0015DataReqDto
     * @return
     */
    @Transactional
    public Xdcz0015DataRespDto xdcz0015(Xdcz0015DataReqDto xdcz0015DataReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataReqDto));
        Xdcz0015DataRespDto xdcz0015DataRespDto = new Xdcz0015DataRespDto();
        String videoNo = xdcz0015DataReqDto.getVideoNo();//影像编号
        String isCollect = xdcz0015DataReqDto.getIsCollect();//是否收集  1是

        //默认成功
        xdcz0015DataRespDto.setOpFlag(DscmsCusEnum.RETURN_SUCCESS.key);
        xdcz0015DataRespDto.setOpMsg(DscmsCusEnum.RETURN_SUCCESS.value);
        try {
            //返回结果
            int result = 0;
            // 企业网银影像补录后通知信贷，信贷修改影像补录任务DOC_IMAGE_SPPL_INFO中补录标志yp_sj='1'
            DocImageSpplInfo docImageSpplInfo = new DocImageSpplInfo();
            docImageSpplInfo.setDisiSerno(videoNo);
            if (Objects.equals("1", isCollect)) {
                docImageSpplInfo.setApproveStatus("997");
                result = DocImageSpplInfoMapper.updateByPrimaryKeySelective(docImageSpplInfo);
            }
            //返回结果
            xdcz0015DataRespDto.setOpFlag(DscmsCusEnum.RETURN_SUCCESS.key);
            xdcz0015DataRespDto.setOpMsg(DscmsCusEnum.RETURN_SUCCESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, e.getMessage());
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataReqDto));
        return xdcz0015DataRespDto;
    }
}
