/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHouse
 * @类描述: iqp_house数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 22:54:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_house")
public class IqpHouse extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;
	
	/** 交易类型 **/
	@Column(name = "TRADE_TYPE", unique = false, nullable = true, length = 5)
	private String tradeType;
	
	/** 房屋交易价格 (元） **/
	@Column(name = "HOUSE_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseTotal;
	
	/** 首付款金额（元） **/
	@Column(name = "FIRSTPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstpayAmt;
	
	/** 首付比例（%） **/
	@Column(name = "FIRSTPAY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstpayPerc;
	
	/** 商贷月还款额(元) **/
	@Column(name = "MONTH_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal monthRepayAmt;
	
	/** 购买楼盘/中介机构 **/
	@Column(name = "HOUSE_BCH", unique = false, nullable = true, length = 100)
	private String houseBch;
	
	/** 竣工日期 **/
	@Column(name = "COMPETION_DATE", unique = false, nullable = true, length = 10)
	private String competionDate;
	
	/** 建造年份 **/
	@Column(name = "BULDING_YEAR", unique = false, nullable = true, length = 10)
	private String buldingYear;
	
	/** 二手房卖方姓名 **/
	@Column(name = "SEC_BUYER_NAME", unique = false, nullable = true, length = 80)
	private String secBuyerName;
	
	/** 二手房卖方联系电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 12)
	private String phone;
	
	/** 开发商名称 **/
	@Column(name = "DEVELOPERS_NAME", unique = false, nullable = true, length = 80)
	private String developersName;
	
	/** 建筑面积（平米） **/
	@Column(name = "HOUSE_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseSqu;
	
	/** 房屋详细地址 **/
	@Column(name = "HOUSE_ADDR", unique = false, nullable = true, length = 250)
	private String houseAddr;
	
	/** 我行认定的房屋价值（元） **/
	@Column(name = "HOUSE_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseValue;
	
	/** 购房合同编号 **/
	@Column(name = "PUR_HOUSE_CONT_NO", unique = false, nullable = true, length = 150)
	private String purHouseContNo;
	
	/** 按揭贷款成数（%） **/
	@Column(name = "SCHED_LOAN_PCT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal schedLoanPct;
	
	/** 是否担保公司担保 **/
	@Column(name = "IS_GUAR_CPRT_GUAR", unique = false, nullable = true, length = 5)
	private String isGuarCprtGuar;
	
	/** 原贷款银行 **/
	@Column(name = "ORIGI_LOAN_BANK", unique = false, nullable = true, length = 80)
	private String origiLoanBank;
	
	/** 原贷款余额（元） **/
	@Column(name = "ORIGI_LOAN_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLoanBal;
	
	/** 是否开发商担保 **/
	@Column(name = "IS_DEVELOPERS_GUA", unique = false, nullable = true, length = 5)
	private String isDevelopersGua;
	
	/** 公积金贷款金额(元) **/
	@Column(name = "PUND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pundAmt;
	
	/** 公积金贷款期限(月) **/
	@Column(name = "PUND_TERM", unique = false, nullable = true, length = 10)
	private Integer pundTerm;
	
	/** 公积金贷款利率(%) **/
	@Column(name = "PUND_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pundRate;
	
	/** 公积金贷款还款方式 **/
	@Column(name = "PUND_REPAY_MODE", unique = false, nullable = true, length = 5)
	private String pundRepayMode;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 预售许可证编号 **/
	@Column(name = "PRESELL_PERMIT_NO", unique = false, nullable = true, length = 50)
	private String presellPermitNo;
	
	/** 每月物业费 **/
	@Column(name = "PROPERTY_EXPENSE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal propertyExpense;
	
	/** 购房套数 **/
	@Column(name = "PUR_HOUSE_NUM", unique = false, nullable = true, length = 5)
	private String purHouseNum;
	
	/** 购房次数 **/
	@Column(name = "PUR_HOUSE_TIMES", unique = false, nullable = true, length = 10)
	private Integer purHouseTimes;
	
	/** 房屋结构 **/
	@Column(name = "HOUSE_STR", unique = false, nullable = true, length = 50)
	private String houseStr;
	
	/** 土地使用方式 **/
	@Column(name = "LAND_UTIL_MODE", unique = false, nullable = true, length = 5)
	private String landUtilMode;
	
	/** 房屋建成时间 **/
	@Column(name = "HOUSE_BUILD_TIME", unique = false, nullable = true, length = 20)
	private String houseBuildTime;
	
	/** 土地剩余使用年限 **/
	@Column(name = "LAND_SURPLUS_YEARS", unique = false, nullable = true, length = 10)
	private Integer landSurplusYears;
	
	/** 房龄 **/
	@Column(name = "HOUSE_YEARS", unique = false, nullable = true, length = 10)
	private Integer houseYears;
	
	/** 是否限购区域 **/
	@Column(name = "PUR_RES_FLAG", unique = false, nullable = true, length = 5)
	private String purResFlag;
	
	/** 房屋坐落区县 **/
	@Column(name = "HOUSE_COUNTY", unique = false, nullable = true, length = 100)
	private String houseCounty;
	
	/** 房屋状况 **/
	@Column(name = "HOUSE_STATUS", unique = false, nullable = true, length = 5)
	private String houseStatus;
	
	/** 房屋用途 **/
	@Column(name = "HOUSE_USE", unique = false, nullable = true, length = 5)
	private String houseUse;
	
	/** 房屋类型 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 5)
	private String houseType;
	
	/** 特殊情况说明 **/
	@Column(name = "SPE_CASE_DESC", unique = false, nullable = true, length = 500)
	private String speCaseDesc;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 500)
	private String otherDesc;
	
	/** 交房日期 **/
	@Column(name = "HOUSE_DATE", unique = false, nullable = true, length = 10)
	private String houseDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 托管协议编号 **/
	@Column(name = "TGXY_NO", unique = false, nullable = true, length = 100)
	private String tgxyNo;

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param tradeType
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
    /**
     * @return tradeType
     */
	public String getTradeType() {
		return this.tradeType;
	}
	
	/**
	 * @param houseTotal
	 */
	public void setHouseTotal(java.math.BigDecimal houseTotal) {
		this.houseTotal = houseTotal;
	}
	
    /**
     * @return houseTotal
     */
	public java.math.BigDecimal getHouseTotal() {
		return this.houseTotal;
	}
	
	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}
	
    /**
     * @return firstpayAmt
     */
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}
	
	/**
	 * @param firstpayPerc
	 */
	public void setFirstpayPerc(java.math.BigDecimal firstpayPerc) {
		this.firstpayPerc = firstpayPerc;
	}
	
    /**
     * @return firstpayPerc
     */
	public java.math.BigDecimal getFirstpayPerc() {
		return this.firstpayPerc;
	}
	
	/**
	 * @param monthRepayAmt
	 */
	public void setMonthRepayAmt(java.math.BigDecimal monthRepayAmt) {
		this.monthRepayAmt = monthRepayAmt;
	}
	
    /**
     * @return monthRepayAmt
     */
	public java.math.BigDecimal getMonthRepayAmt() {
		return this.monthRepayAmt;
	}
	
	/**
	 * @param houseBch
	 */
	public void setHouseBch(String houseBch) {
		this.houseBch = houseBch;
	}
	
    /**
     * @return houseBch
     */
	public String getHouseBch() {
		return this.houseBch;
	}
	
	/**
	 * @param competionDate
	 */
	public void setCompetionDate(String competionDate) {
		this.competionDate = competionDate;
	}
	
    /**
     * @return competionDate
     */
	public String getCompetionDate() {
		return this.competionDate;
	}
	
	/**
	 * @param buldingYear
	 */
	public void setBuldingYear(String buldingYear) {
		this.buldingYear = buldingYear;
	}
	
    /**
     * @return buldingYear
     */
	public String getBuldingYear() {
		return this.buldingYear;
	}
	
	/**
	 * @param secBuyerName
	 */
	public void setSecBuyerName(String secBuyerName) {
		this.secBuyerName = secBuyerName;
	}
	
    /**
     * @return secBuyerName
     */
	public String getSecBuyerName() {
		return this.secBuyerName;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param developersName
	 */
	public void setDevelopersName(String developersName) {
		this.developersName = developersName;
	}
	
    /**
     * @return developersName
     */
	public String getDevelopersName() {
		return this.developersName;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(java.math.BigDecimal houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return houseSqu
     */
	public java.math.BigDecimal getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param houseAddr
	 */
	public void setHouseAddr(String houseAddr) {
		this.houseAddr = houseAddr;
	}
	
    /**
     * @return houseAddr
     */
	public String getHouseAddr() {
		return this.houseAddr;
	}
	
	/**
	 * @param houseValue
	 */
	public void setHouseValue(java.math.BigDecimal houseValue) {
		this.houseValue = houseValue;
	}
	
    /**
     * @return houseValue
     */
	public java.math.BigDecimal getHouseValue() {
		return this.houseValue;
	}
	
	/**
	 * @param purHouseContNo
	 */
	public void setPurHouseContNo(String purHouseContNo) {
		this.purHouseContNo = purHouseContNo;
	}
	
    /**
     * @return purHouseContNo
     */
	public String getPurHouseContNo() {
		return this.purHouseContNo;
	}
	
	/**
	 * @param schedLoanPct
	 */
	public void setSchedLoanPct(java.math.BigDecimal schedLoanPct) {
		this.schedLoanPct = schedLoanPct;
	}
	
    /**
     * @return schedLoanPct
     */
	public java.math.BigDecimal getSchedLoanPct() {
		return this.schedLoanPct;
	}
	
	/**
	 * @param isGuarCprtGuar
	 */
	public void setIsGuarCprtGuar(String isGuarCprtGuar) {
		this.isGuarCprtGuar = isGuarCprtGuar;
	}
	
    /**
     * @return isGuarCprtGuar
     */
	public String getIsGuarCprtGuar() {
		return this.isGuarCprtGuar;
	}
	
	/**
	 * @param origiLoanBank
	 */
	public void setOrigiLoanBank(String origiLoanBank) {
		this.origiLoanBank = origiLoanBank;
	}
	
    /**
     * @return origiLoanBank
     */
	public String getOrigiLoanBank() {
		return this.origiLoanBank;
	}
	
	/**
	 * @param origiLoanBal
	 */
	public void setOrigiLoanBal(java.math.BigDecimal origiLoanBal) {
		this.origiLoanBal = origiLoanBal;
	}
	
    /**
     * @return origiLoanBal
     */
	public java.math.BigDecimal getOrigiLoanBal() {
		return this.origiLoanBal;
	}
	
	/**
	 * @param isDevelopersGua
	 */
	public void setIsDevelopersGua(String isDevelopersGua) {
		this.isDevelopersGua = isDevelopersGua;
	}
	
    /**
     * @return isDevelopersGua
     */
	public String getIsDevelopersGua() {
		return this.isDevelopersGua;
	}
	
	/**
	 * @param pundAmt
	 */
	public void setPundAmt(java.math.BigDecimal pundAmt) {
		this.pundAmt = pundAmt;
	}
	
    /**
     * @return pundAmt
     */
	public java.math.BigDecimal getPundAmt() {
		return this.pundAmt;
	}
	
	/**
	 * @param pundTerm
	 */
	public void setPundTerm(Integer pundTerm) {
		this.pundTerm = pundTerm;
	}
	
    /**
     * @return pundTerm
     */
	public Integer getPundTerm() {
		return this.pundTerm;
	}
	
	/**
	 * @param pundRate
	 */
	public void setPundRate(java.math.BigDecimal pundRate) {
		this.pundRate = pundRate;
	}
	
    /**
     * @return pundRate
     */
	public java.math.BigDecimal getPundRate() {
		return this.pundRate;
	}
	
	/**
	 * @param pundRepayMode
	 */
	public void setPundRepayMode(String pundRepayMode) {
		this.pundRepayMode = pundRepayMode;
	}
	
    /**
     * @return pundRepayMode
     */
	public String getPundRepayMode() {
		return this.pundRepayMode;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo;
	}
	
    /**
     * @return presellPermitNo
     */
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param propertyExpense
	 */
	public void setPropertyExpense(java.math.BigDecimal propertyExpense) {
		this.propertyExpense = propertyExpense;
	}
	
    /**
     * @return propertyExpense
     */
	public java.math.BigDecimal getPropertyExpense() {
		return this.propertyExpense;
	}
	
	/**
	 * @param purHouseNum
	 */
	public void setPurHouseNum(String purHouseNum) {
		this.purHouseNum = purHouseNum;
	}
	
    /**
     * @return purHouseNum
     */
	public String getPurHouseNum() {
		return this.purHouseNum;
	}
	
	/**
	 * @param purHouseTimes
	 */
	public void setPurHouseTimes(Integer purHouseTimes) {
		this.purHouseTimes = purHouseTimes;
	}
	
    /**
     * @return purHouseTimes
     */
	public Integer getPurHouseTimes() {
		return this.purHouseTimes;
	}
	
	/**
	 * @param houseStr
	 */
	public void setHouseStr(String houseStr) {
		this.houseStr = houseStr;
	}
	
    /**
     * @return houseStr
     */
	public String getHouseStr() {
		return this.houseStr;
	}
	
	/**
	 * @param landUtilMode
	 */
	public void setLandUtilMode(String landUtilMode) {
		this.landUtilMode = landUtilMode;
	}
	
    /**
     * @return landUtilMode
     */
	public String getLandUtilMode() {
		return this.landUtilMode;
	}
	
	/**
	 * @param houseBuildTime
	 */
	public void setHouseBuildTime(String houseBuildTime) {
		this.houseBuildTime = houseBuildTime;
	}
	
    /**
     * @return houseBuildTime
     */
	public String getHouseBuildTime() {
		return this.houseBuildTime;
	}
	
	/**
	 * @param landSurplusYears
	 */
	public void setLandSurplusYears(Integer landSurplusYears) {
		this.landSurplusYears = landSurplusYears;
	}
	
    /**
     * @return landSurplusYears
     */
	public Integer getLandSurplusYears() {
		return this.landSurplusYears;
	}
	
	/**
	 * @param houseYears
	 */
	public void setHouseYears(Integer houseYears) {
		this.houseYears = houseYears;
	}
	
    /**
     * @return houseYears
     */
	public Integer getHouseYears() {
		return this.houseYears;
	}
	
	/**
	 * @param purResFlag
	 */
	public void setPurResFlag(String purResFlag) {
		this.purResFlag = purResFlag;
	}
	
    /**
     * @return purResFlag
     */
	public String getPurResFlag() {
		return this.purResFlag;
	}
	
	/**
	 * @param houseCounty
	 */
	public void setHouseCounty(String houseCounty) {
		this.houseCounty = houseCounty;
	}
	
    /**
     * @return houseCounty
     */
	public String getHouseCounty() {
		return this.houseCounty;
	}
	
	/**
	 * @param houseStatus
	 */
	public void setHouseStatus(String houseStatus) {
		this.houseStatus = houseStatus;
	}
	
    /**
     * @return houseStatus
     */
	public String getHouseStatus() {
		return this.houseStatus;
	}
	
	/**
	 * @param houseUse
	 */
	public void setHouseUse(String houseUse) {
		this.houseUse = houseUse;
	}
	
    /**
     * @return houseUse
     */
	public String getHouseUse() {
		return this.houseUse;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param speCaseDesc
	 */
	public void setSpeCaseDesc(String speCaseDesc) {
		this.speCaseDesc = speCaseDesc;
	}
	
    /**
     * @return speCaseDesc
     */
	public String getSpeCaseDesc() {
		return this.speCaseDesc;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param houseDate
	 */
	public void setHouseDate(String houseDate) {
		this.houseDate = houseDate;
	}
	
    /**
     * @return houseDate
     */
	public String getHouseDate() {
		return this.houseDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getTgxyNo() {
		return tgxyNo;
	}

	public void setTgxyNo(String tgxyNo) {
		this.tgxyNo = tgxyNo;
	}


}