/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopColonyWhiteLst
 * @类描述: coop_colony_white_lst数据实体类
 * @功能描述:
 * @创建人: AbsonZ
 * @创建时间: 2021-04-16 15:47:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@ExcelCsv(namePrefix = "集群白名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CoopColonyWhiteLstVo {

    /**
     * 合作方案编号
     **/
    @ExcelField(title = "合作方案编号", viewLength = 20)
    private String coopPlanNo;

    /**
     * 合作方编号
     **/
    @ExcelField(title = "合作方编号", viewLength = 20)
    private String partnerNo;

    /**
     * 合作方名称
     **/
    @ExcelField(title = "合作方名称", viewLength = 20)
    private String partnerName;

    /**
     * 关联方类型
     **/
    @ExcelField(title = "客户大类1-对私2对公", viewLength = 20)
    private String partnerType;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /**
     * 证件类型
     **/
    @ExcelField(title = "证件类型", viewLength = 20)
    private String certType;

    /**
     * 证件号码
     **/
    @ExcelField(title = "证件号码", viewLength = 20)
    private String certCode;

    public String getCoopPlanNo() {
        return coopPlanNo;
    }

    public void setCoopPlanNo(String coopPlanNo) {
        this.coopPlanNo = coopPlanNo;
    }

    public String getPartnerNo() {
        return partnerNo;
    }

    public void setPartnerNo(String partnerNo) {
        this.partnerNo = partnerNo;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }
}