package cn.com.yusys.yusp.web.server.xdcz0019;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0019.req.Xdcz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.resp.Xdcz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0019.Xdcz0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:小贷额度支用申请书生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0019:小贷额度支用申请书生成pdf")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0019Resource.class);

    @Resource
    private Xdcz0019Service xdcz0019Service;
    /**
     * 交易码：xdcz0019
     * 交易描述：小贷额度支用申请书生成pdf
     *
     * @param xdcz0019DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("小贷额度支用申请书生成pdf")
    @PostMapping("/xdcz0019")
    protected @ResponseBody
    ResultDto<Xdcz0019DataRespDto>  xdcz0019(@Validated @RequestBody Xdcz0019DataReqDto xdcz0019DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataReqDto));
        Xdcz0019DataRespDto  xdcz0019DataRespDto  = new Xdcz0019DataRespDto();// 响应Dto:小贷额度支用申请书生成pdf
        ResultDto<Xdcz0019DataRespDto>xdcz0019DataResultDto = new ResultDto<>();
        String cusName = xdcz0019DataReqDto.getCusName();//客户名称
        String contNo = xdcz0019DataReqDto.getContNo();//合同编号
        String cnContNo = xdcz0019DataReqDto.getCnContNo();//中文合同编号
        BigDecimal loanAmt = xdcz0019DataReqDto.getLoanAmt();//放款金额
        String curType = xdcz0019DataReqDto.getCurType();//币种
        String loanStartDate = xdcz0019DataReqDto.getLoanStartDate();//借款起始日期
        String loanEndDate = xdcz0019DataReqDto.getLoanEndDate();//借款到期日期
        String loanType = xdcz0019DataReqDto.getLoanType();//借款种类
        String loanUseType = xdcz0019DataReqDto.getLoanUseType();//借款用途
        String rateExeMode = xdcz0019DataReqDto.getRateExeMode();//利率执行方式
        String lprInterzone = xdcz0019DataReqDto.getLprInterzone();//lpr区间
        BigDecimal lprBasePoint   = xdcz0019DataReqDto.getLprBasePoint  ();//lpr基点
        BigDecimal yearRate = xdcz0019DataReqDto.getYearRate();//年利率
        String floatRateAdjMode = xdcz0019DataReqDto.getFloatRateAdjMode();//浮动利率调整方式
        String drawIntervalDate = xdcz0019DataReqDto.getDrawIntervalDate();//提款间隔日
        String eiMode = xdcz0019DataReqDto.getEiMode();//结息方式
        String acctNo = xdcz0019DataReqDto.getAcctNo();//账号
        String acctsvcr = xdcz0019DataReqDto.getAcctsvcr();//开户行
        String payType = xdcz0019DataReqDto.getPayType();//支付方式
        String repayType = xdcz0019DataReqDto.getRepayType();//还款方式
        String applyDate = xdcz0019DataReqDto.getApplyDate();//申请日期

        try {
            //必输字段校验
            if (StringUtil.isEmpty(contNo)) {
                xdcz0019DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0019DataResultDto.setMessage("借款合同号【contNo】不能为空！");
                return xdcz0019DataResultDto;
            }
            // 从xdcz0019DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataReqDto));
            xdcz0019DataRespDto = xdcz0019Service.xdcz0019(xdcz0019DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataRespDto));
            // 封装xdcz0019DataResultDto中正确的返回码和返回信息
            xdcz0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value,e.getMessage());
            // 封装xdcz0019DataResultDto中异常返回码和返回信息
            xdcz0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0019DataRespDto到xdcz0019DataResultDto中
        xdcz0019DataResultDto.setData(xdcz0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataRespDto));
        return xdcz0019DataResultDto;
    }
}
