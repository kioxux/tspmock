package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Service
public class RiskItem0097Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0097Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private CtrLoanContService ctrLoanContService;//合同服务

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    /**
     * @方法名称: riskItem0097
     * @方法描述: 定价利率检查
     * @参数与返回说明:
     * @算法描述:
     * 1、普通贷款合同、贸易融资合同、委托贷款合同 (一般合同申请提交时) 风险拦截校验，对应的批复台账分项品种中的 利率是否有值
     * 2、贷款出账申请、委托贷款出账申请，如果对应的合同是最高额合同或者最高额授信协议，分项拦截对应的批复台账分项品种中的利率是否有值
     * 如果没值，分项拦截错误提示为"对应授信品种利率不存在，请进行人民币定价申请或完成审批"
     * @创建人: yfs
     * @创建时间: 2021-08-24 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0097(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("定价利率检查开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_YX003.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            if(CmisBizConstants.STD_CONT_TYPE_1.equals(iqpLoanApp.getContType())) {
                if(!CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanApp.getGuarWay()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay())
                        && !CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay())){
                    try {
                        lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpLoanApp.getLmtAccNo());
                    } catch (Exception e) {
                        e.printStackTrace();
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09601);
                        return riskResultDto;
                    }
                    if(Objects.isNull(lmtReplyAccSubPrd.getRateYear())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09602);
                        return riskResultDto;
                    }
                    if(!CmisCommonConstants.STD_BUSI_TYPE_04.equals(iqpLoanApp.getBizType())){
                        log.info("定价申请的利率为【{}】",lmtReplyAccSubPrd.getRateYear());
                        log.info("贷款合同申请的利率为【{}】",iqpLoanApp.getExecRateYear());
                        if(iqpLoanApp.getExecRateYear().compareTo(lmtReplyAccSubPrd.getRateYear())<0){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_009701);
                            return riskResultDto;
                        }
                    }
                }
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)) {
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(serno);
            if (Objects.isNull(iqpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            if(CmisBizConstants.STD_CONT_TYPE_1.equals(iqpEntrustLoanApp.getContType())) {
                try {
                    lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpEntrustLoanApp.getLmtAccNo());
                } catch (Exception e) {
                    e.printStackTrace();
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09601);
                    return riskResultDto;
                }
                if(Objects.isNull(lmtReplyAccSubPrd.getRateYear())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09602);
                    return riskResultDto;
                }
                log.info("定价申请的利率为【{}】",lmtReplyAccSubPrd.getRateYear());
                log.info("委托贷款合同申请的利率为【{}】",iqpEntrustLoanApp.getExecRateYear());
                if(iqpEntrustLoanApp.getExecRateYear().compareTo(lmtReplyAccSubPrd.getRateYear())<0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_009701);
                    return riskResultDto;
                }
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) || CmisFlowConstants.FLOW_TYPE_DHD02.equals(bizType)
                ||CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            CtrLoanCont ctrLoanCon = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
            if (Objects.isNull(ctrLoanCon)) {
                CtrHighAmtAgrCont ctrhighamtagrcont = ctrHighAmtAgrContService.selectDataByContNo(pvpLoanApp.getContNo());
                if (Objects.isNull(ctrhighamtagrcont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                } else {
                    lmtReplyAccSubPrd = lmtReplyAccService.isExistAccSubOnHighAmtAgrCont(pvpLoanApp.getLmtAccNo(), pvpLoanApp.getPrdId());
                    if (Objects.isNull(lmtReplyAccSubPrd)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02613);
                        return riskResultDto;
                    }
                }
            }else {
                try {
                    lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(pvpLoanApp.getLmtAccNo());
                } catch (Exception e) {
                    e.printStackTrace();
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02612);
                    return riskResultDto;
                }
                if (Objects.isNull(lmtReplyAccSubPrd)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02601);
                    return riskResultDto;
                }
            }
            // 房抵e点贷不参与校验
            if("P034".equals(pvpLoanApp.getPrdTypeProp())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
                return riskResultDto;
            }
            // 外币不参与校验
            if (!Objects.equals(pvpLoanApp.getCurType(), "CNY")) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }

            if(Objects.isNull(lmtReplyAccSubPrd.getRateYear())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09602);
                return riskResultDto;
            }
            log.info("定价申请的利率为【{}】",lmtReplyAccSubPrd.getRateYear());
            log.info("贷款出账申请的利率为【{}】",pvpLoanApp.getExecRateYear());
            if(pvpLoanApp.getExecRateYear().compareTo(lmtReplyAccSubPrd.getRateYear())<0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_009702);
                return riskResultDto;
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType) || CmisFlowConstants.FLOW_TYPE_DHD04.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGD04.equals(bizType)) {
            // 委托贷款出账申请
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            if (Objects.isNull(pvpEntrustLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(pvpEntrustLoanApp.getContNo());
            if (Objects.isNull(ctrEntrustLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                return riskResultDto;
            }
            try {
                lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(pvpEntrustLoanApp.getLmtAccNo());
            } catch (Exception e) {
                e.printStackTrace();
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02612);
                return riskResultDto;
            }
            if(Objects.isNull(lmtReplyAccSubPrd)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02612);
                return riskResultDto;
            }
            if (Objects.isNull(lmtReplyAccSubPrd.getRateYear())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09602);
                return riskResultDto;
            }
            log.info("定价申请的利率为【{}】",lmtReplyAccSubPrd.getRateYear());
            log.info("委托贷款出账申请的利率为【{}】",pvpEntrustLoanApp.getExecRateYear());
            if(pvpEntrustLoanApp.getExecRateYear().compareTo(lmtReplyAccSubPrd.getRateYear())<0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_009703);
                return riskResultDto;
            }
        }

        log.info("定价利率检查结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
