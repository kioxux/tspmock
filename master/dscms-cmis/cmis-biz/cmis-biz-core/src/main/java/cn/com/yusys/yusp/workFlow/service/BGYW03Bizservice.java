package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:01
 * @desc 担保变更
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW03Bizservice implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW03Bizservice.class);

    @Autowired
    private IqpGuarChgAppService iqpGuarChgAppService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private IqpGuarChgAppRelService iqpGuarChgAppRelService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 担保变更
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG008.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG009.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG010.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG011.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH07.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_DHH07.equals(bizType)) {
            iqpContExtApplyApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG029.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG030.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG031.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH08.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_DHH08.equals(bizType)) {
            iqpContExtApply(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    private void iqpContExtApplyApp(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpGuarChgApp iqpGuarChgApp = iqpGuarChgAppService.selectByPrimaryKey(iqpSerno);
             // 获取合同号
            String contNo = iqpGuarChgApp.getContNo();
            // 获取合同信息
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            // 获取批复号
            String replySerno = "";
            if(null != ctrLoanCont){
                replySerno = ctrLoanCont.getReplyNo();
            }
            if(null != replySerno && !"".equals(replySerno)){
                //  根据批复编号获取批复信息
                HashMap map = new HashMap();
                map.put("replySerno",replySerno);
                map.put("oprType", CommonConstance.OPR_TYPE_ADD);
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.selectAccNoDataByParams(map);
                // 获取授信流水号
                String LmtSerno ="";
                if(null != lmtReplyAcc ){
                    LmtSerno = lmtReplyAcc.getSerno();
                }
                if(!"".equals(LmtSerno) && null != LmtSerno){
                    // 加载路由条件
                    put2VarParam(instanceInfo, LmtSerno);
                }
            }

            // 企业类型处理
            WFBizParamDto param = new WFBizParamDto();
            Map<String, Object> params = new HashMap<>();
            param.setBizId(instanceInfo.getBizId());
            param.setInstanceId(instanceInfo.getInstanceId());
            String isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_0;
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(iqpGuarChgApp.getCusId()).getData();
            if (cusCorpDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusCorpDto.getIsSmconCus())) {
                isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            params.put("isSmconCus",isSmconCus);
            // 是否对公客户，判断协办客户经理节点
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(iqpGuarChgApp.getCusId());
            log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_1);
            } else {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            params.put("prdId", iqpGuarChgApp.getPrdId());
            params.put("prdName", iqpGuarChgApp.getPrdName());
            // 获取分成比例协办客户经理编号
            String managerIdXB = null;
            CusMgrDividePercDto cusMgrDividePercDto = iCusClientService.selectXBManagerId(iqpGuarChgApp.getCusId()).getData();
            log.info("根据客户号【{}】查询客户协办客户经理，返回为{}", iqpGuarChgApp.getCusId(), cusMgrDividePercDto);
            if (cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())) {
                managerIdXB = cusMgrDividePercDto.getManagerId();
            }
            params.put("managerId", managerIdXB);
            if (!OpType.REFUSE.equals(currentOpType)) {// 否决除外
                params.put("nextSubmitNodeId", instanceInfo.getNextNodeInfos().get(0).getNextNodeId());
            }
            params.put("commitDeptTypeXDGLB","0");
            params.put("isUpAppr","0");
            params.put("upApprType","0");
            params.put("isDownAppr","0");
            params.put("isDAELmtAppr","0");
            // 会签模式处理
            String apprMode = "";
            try {
                // 获取客户批复
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(iqpGuarChgApp.getCusId());
                if(Objects.nonNull(lmtReplyAcc)){
                    if(null != lmtReplyAcc.getApprMode() && !"".equals(lmtReplyAcc.getApprMode())){
                        apprMode = lmtReplyAcc.getApprMode();
                    }
                }
            } catch (Exception e) {
                log.info("未获取到授信批复会签模式信息");
            }

            // 集团会签模式处理
            try {
                // 根据客户号获取集团客户号
                String grpNo = "";
                /**
                 * 获取集团客户号 cmiscus0001
                 */
                CmisCus0001ReqDto cmisCus0001ReqDto = new CmisCus0001ReqDto();
                cmisCus0001ReqDto.setCusId(iqpGuarChgApp.getCusId());
                ResultDto<CmisCus0001RespDto> cmisCus0001RespDtoResultDto = cmisCusClientService.cmiscus0001(cmisCus0001ReqDto);
                if (cmisCus0001RespDtoResultDto != null
                        && SuccessEnum.SUCCESS.key.equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())
                        && !"40012".equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())){
                    CmisCus0001RespDto data = cmisCus0001RespDtoResultDto.getData();
                    if(Objects.nonNull(data)){
                        grpNo = data.getGrpNo();//集团客户号
                    }
                }
                if(!"".equals(grpNo) && null != grpNo){
                    // 获取集团批复
                    LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(grpNo);
                    if(Objects.nonNull(lmtGrpReplyAcc)){
                        if(null != lmtGrpReplyAcc.getApprMode() && !"".equals(lmtGrpReplyAcc.getApprMode())){
                            apprMode = lmtGrpReplyAcc.getApprMode();
                        }
                    }
                }
            } catch (Exception e) {
                log.info("未获取到集团授信批复会签模式信息");
            }

            if("".equals(apprMode) || null == apprMode){
                apprMode = "05";
            }
            params.put("apprMode",apprMode);
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                iqpGuarChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                this.updateGrtGuarBizRel(iqpGuarChgApp.getGuarChgType(),iqpGuarChgApp.getIqpSerno(),currentOpType);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",instanceInfo.getBizType(),currentOpType,iqpSerno);
                // iqpGuarChgAppService.createImageSpplInfo(iqpSerno,instanceInfo.getBizType(),instanceInfo.getInstanceId());
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.更新业务申请状态 由审批中111 -> 审批通过 997
                iqpGuarChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                this.updateGrtGuarBizRel(iqpGuarChgApp.getGuarChgType(),iqpGuarChgApp.getIqpSerno(),currentOpType);
                try {
//                    if(!CmisFlowConstants.FLOW_TYPE_TYPE_BG011.equals(instanceInfo.getBizType())){
//                        // 资料未全生成影像补扫任务
//                        log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",instanceInfo.getBizType(),currentOpType,iqpSerno);
//                        iqpGuarChgAppService.createImageSpplInfo(iqpSerno,instanceInfo.getBizType(),instanceInfo.getInstanceId(),"担保变更申请",CmisCommonConstants.STD_SPPL_TYPE_08);
//                    }
                    Map<String, String> map = new HashMap<>();
                    map.put("cusName", iqpGuarChgApp.getCusName());
                    map.put("prdName", "担保变更申请");
                    map.put("result", "通过");
                    ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(iqpGuarChgApp.getManagerId());
                    sendMessage.sendMessage("MSG_CF_M_0016",map,"1",iqpGuarChgApp.getManagerId(),byLoginCode.getData().getUserMobilephone());
                } catch (Exception e) {
                    log.error("发送消息失败，短信编号【{MSG_CF_M_0016}】");
                }
                try {
                    if (CmisFlowConstants.FLOW_TYPE_TYPE_BG009.equals(instanceInfo.getBizType())) {
                        // 翻译管护经理名称和管护经理机构名称
                        ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(iqpGuarChgApp.getManagerId());
                        if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                            log.error("业务申请: " + iqpSerno + " 查询用户数据异常");
                            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                        }
                        String managerName = adminSmUserDtoResultDto.getData().getUserName();
                        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(iqpGuarChgApp.getManagerBrId());
                        if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                            log.error("业务申请: " + iqpSerno + " 查询用户数据异常");
                            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                        }
                        String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                        //短信填充参数
                        Map paramMap = new HashMap();
                        paramMap.put("cusName", iqpGuarChgApp.getCusName());
                        paramMap.put("managerId", managerName);
                        paramMap.put("managerBrId", managerBrName);
                        paramMap.put("instanceId", instanceInfo.getInstanceId());
                        paramMap.put("nodeSign", instanceInfo.getNodeSign());
                        // 首页消息提醒
                        messageCommonService.sendonlinepldremind("MSG_DG_M_0001", "1", paramMap);
                    }
                } catch (Exception e) {
                    log.error("发送集中作业合同初审岗提醒失败!");
                }
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回操作:" + instanceInfo);
                    iqpGuarChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                }
                Map<String, String> map = new HashMap<>();
                map.put("cusName", iqpGuarChgApp.getCusName());
                map.put("prdName", "担保变更申请");
                map.put("result","打回");
                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(iqpGuarChgApp.getManagerId());
                sendMessage.sendMessage("MSG_CF_M_0016",map,"1",iqpGuarChgApp.getManagerId(),byLoginCode.getData().getUserMobilephone());
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpGuarChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }
    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getRouterMapResult(serno);
        // 会签模式判断 apprMode
        String apprMode = lmtAppService.getApprMode(serno);
        params.put("apprMode",apprMode);
        params.remove("approveStatus");
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }


    private void iqpContExtApply(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpGuarChgApp iqpGuarChgApp = iqpGuarChgAppService.selectByPrimaryKey(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                //档案
                String bizType = instanceInfo.getBizType();
                String nodeId = instanceInfo.getNodeId();
                if ((CmisFlowConstants.FLOW_TYPE_TYPE_BG029.equals(bizType) && "243_5".equals(nodeId)) ||
                        (CmisFlowConstants.FLOW_TYPE_TYPE_BG030.equals(bizType))){
                    this.createCentralFileTask(bizType,iqpSerno,instanceInfo);
                }
                iqpGuarChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,iqpSerno);
                // iqpGuarChgAppService.createImageSpplInfo(iqpSerno,instanceInfo.getBizType(),instanceInfo.getInstanceId());
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                iqpGuarChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                //2.更新业务申请状态 由审批中111 -> 审批通过 997

                this.updateSelectiveByGuarContNo(iqpGuarChgApp.getGuarChgType(),iqpGuarChgApp.getIqpSerno());

                QueryModel model = new QueryModel();
                String contNo = iqpGuarChgApp.getContNo();
                model.addCondition("contNo",contNo);
                List<AccLoan> list = accLoanService.selectByModel(model);
                Boolean fkFlag = false;
                if(list.size() > 0) {
                    for (AccLoan accloan : list) {
                        String accStatus = accloan.getAccStatus();
                        if (accStatus != null && !"".equals(accStatus)){
                            if(!"6".equals(accStatus) && !"7".equals(accStatus)){
                                fkFlag = true;
                                break;
                            }
                        }
                    }
                }
                if(fkFlag) {
                    //校验 合同项下是否放款，已放款 生成单独的归档任务，未放款 不生成
                    // 生成归档任务
                    log.info("开始系统生成档案归档信息");
                    String cusId = iqpGuarChgApp.getCusId();
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    docArchiveClientDto.setArchiveMode("");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                    docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                    docArchiveClientDto.setDocType("24");// 24:业务变更-担保变更
                    docArchiveClientDto.setBizSerno(iqpSerno);
                    docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
                    docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                    docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                    docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                    docArchiveClientDto.setManagerId(iqpGuarChgApp.getManagerId());
                    docArchiveClientDto.setManagerBrId(iqpGuarChgApp.getManagerBrId());
                    docArchiveClientDto.setInputId(iqpGuarChgApp.getInputId());
                    docArchiveClientDto.setInputBrId(iqpGuarChgApp.getInputBrId());
                    docArchiveClientDto.setContNo(iqpGuarChgApp.getContNo());
                    docArchiveClientDto.setLoanAmt(iqpGuarChgApp.getContAmt());
                    docArchiveClientDto.setStartDate(iqpGuarChgApp.getContStartDate());
                    docArchiveClientDto.setEndDate(iqpGuarChgApp.getContEndDate());
                    docArchiveClientDto.setPrdId(iqpGuarChgApp.getPrdId());
                    docArchiveClientDto.setPrdName(iqpGuarChgApp.getPrdName());
                    int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                    if (num < 1) {
                        log.info("系统生成档案归档信息失败");
                    }
                }
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",instanceInfo.getBizType(),currentOpType,iqpSerno);
                iqpGuarChgAppService.createImageSpplInfo(iqpSerno,instanceInfo.getBizType(),instanceInfo.getInstanceId(),"担保变更协议",CmisCommonConstants.STD_SPPL_TYPE_10);
                log.info("结束操作完成:" + instanceInfo);
                // 推送用印系统
                try {
                    cmisBizXwCommonService.sendYk(currentUserId,iqpSerno,iqpGuarChgApp.getCusName());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回操作:" + instanceInfo);
                    iqpGuarChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpGuarChgApp.setContApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpGuarChgAppService.updateSelective(iqpGuarChgApp);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /**
     * 解除担保关系
     * @param guarChgType 担保变更类型
     * @param iqpSerno 担保变更流水
     * @param currentOpType 流程节点
     * **/

    public void updateGrtGuarBizRel(String guarChgType ,String iqpSerno ,String currentOpType){
        //担保变更类型为 担保撤销、担保置换时
        if("03".equals(guarChgType) || "02".equals(guarChgType) ){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",iqpSerno);
            queryModel.addCondition("opFlag","03");//解除
            List<IqpGuarChgAppRel> iqpGuarChgAppRelList = iqpGuarChgAppRelService.selectByModel(queryModel);
            for (IqpGuarChgAppRel iqpGuarChgAppRel: iqpGuarChgAppRelList) {
                QueryModel queryModel1 = new QueryModel();
                queryModel1.addCondition("contNo",iqpGuarChgAppRel.getContNo());//合同编号
                queryModel1.addCondition("guarContNo",iqpGuarChgAppRel.getGuarContNo());//担保合同号
                //能确定唯一记录
                List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectByModel(queryModel1);
                GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelList.get(0);
                if(OpType.END.equals(currentOpType)) {
                    grtGuarBizRstRel.setCorreRel("3");//解除
                }else{
                    grtGuarBizRstRel.setCorreRel("5");//解除中
                }
                grtGuarBizRstRelService.update(grtGuarBizRstRel);
            }
        }
    }

    public void updateSelectiveByGuarContNo(String guarChgType ,String iqpSerno ){
        //担保变更类型为 担保追加、担保置换时 审批流程通过 需要 将担保合同改为生效
        if("01".equals(guarChgType) || "02".equals(guarChgType) ){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",iqpSerno);
            queryModel.addCondition("opFlag","02");//新增
            List<IqpGuarChgAppRel> iqpGuarChgAppRelList = iqpGuarChgAppRelService.selectByModel(queryModel);
            for (IqpGuarChgAppRel iqpGuarChgAppRel: iqpGuarChgAppRelList) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(iqpGuarChgAppRel.getGuarContNo());
                grtGuarCont.setSignDate(DateUtils.getCurrDateStr());//签订日期
                grtGuarCont.setGuarContState("101");//生效
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);
            }
        }
        //担保置换时更新担保方式cjd
        if("02".equals(guarChgType)){
            //取新担保方式
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",iqpSerno);
            queryModel.addCondition("opFlag","02");//新增
            queryModel.addCondition("isUnderCont","0");// 是否原合同项下  0 否
            String guarWay = iqpGuarChgAppRelService.selectByModel(queryModel).get(0).getGuarWay();
            //查询解除的主担保
            QueryModel queryModel1 = new QueryModel();
            queryModel1.addCondition("serno",iqpSerno);
            queryModel1.addCondition("opFlag","03");//解除
            List<IqpGuarChgAppRel> iqpGuarChgAppRelList = iqpGuarChgAppRelService.selectByModel(queryModel1);
            for (IqpGuarChgAppRel iqpGuarChgAppRel: iqpGuarChgAppRelList) {
               // GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(iqpGuarChgAppRel.getGuarContNo());
                String contNo = iqpGuarChgAppRel.getContNo();
                // 根据业务合同号和担保合同号获取关系表信息,取关系表中是否追加担保字段
                QueryModel model  = new QueryModel();
                model.addCondition("guarContNo",iqpGuarChgAppRel.getGuarContNo());
                model.addCondition("contNo",contNo);
                List<GrtGuarBizRstRel> grtGuarBizRstRelList  = grtGuarBizRstRelService.selectByModel(model);
                for (int i = 0; i < grtGuarBizRstRelList.size(); i++) {
                    GrtGuarBizRstRel grtGuarBizRstRel= grtGuarBizRstRelList.get(i);
                    if(null != grtGuarBizRstRel && "0".equals(grtGuarBizRstRel.getIsAddGuar())){//是否追加担保为否的就是主担保
                        //更新台账担保方式
                        List<AccLoan> accLoanList = accLoanService.selectByContNo(contNo);
                        for (int j = 0; j < accLoanList.size(); j++) {
                            AccLoan accLoan = accLoanList.get(i);
                            accLoan.setGuarMode(guarWay);
                            accLoanService.updateSelective(accLoan);
                        }
                        //更新合同主表担保方式
                        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(contNo);
                        if(Objects.nonNull(ctrLoanCont)){
                            ctrLoanCont.setGuarWay(guarWay);
                            ctrLoanContService.updateSelective(ctrLoanCont);
                        }
                        // 最高额协议修改
                        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
                        if(Objects.nonNull(ctrHighAmtAgrCont)){
                            ctrHighAmtAgrCont.setGuarMode(guarWay);
                            ctrHighAmtAgrContService.updateSelective(ctrHighAmtAgrCont);
                        }
                    }
                }
            }
        }
    }

    public void createCentralFileTask(String bizType , String iqpSerno,ResultInstanceDto instanceInfo){
        IqpGuarChgApp iqpGuarChgApp = iqpGuarChgAppService.selectByPrimaryKey(iqpSerno);
        try {
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpGuarChgApp.getInputBrId());
            String orgType = resultDto.getData().getOrgType();
            //        0-总行部室
            //        1-异地支行（有分行）
            //        2-异地支行（无分行）
            //        3-异地分行
            //        4-中心支行
            //        5-综合支行
            //        6-对公支行
            //        7-零售支行
            if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType)){
                //新增临时档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(iqpGuarChgApp.getIqpSerno());
                centralFileTaskdto.setCusId(iqpGuarChgApp.getCusId());
                centralFileTaskdto.setCusName(iqpGuarChgApp.getCusName());
                centralFileTaskdto.setBizType(bizType); //
                centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
                centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setInputId(iqpGuarChgApp.getInputId());
                centralFileTaskdto.setInputBrId(iqpGuarChgApp.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setTaskType("02"); // 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);
            }
        }catch (Exception e){
            log.info("利率变更审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
        }
    }


    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW03.equals(flowCode);
    }
}
