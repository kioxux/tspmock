package cn.com.yusys.yusp.dto;

/**
 * @className ImportIqpLoanAppDto
 * @Date 2020/12/16 : 11:14
 */

public class ImportIqpLoanAppDto extends IqpLoanAppDto {
    /**第三方额度类型**/
    private String thirdLimitType;
    /**第三方额度编号**/
    private String thirdLimitId;

    public String getThirdLimitType() {
        return thirdLimitType;
    }

    public void setThirdLimitType(String thirdLimitType) {
        this.thirdLimitType = thirdLimitType;
    }

    public String getThirdLimitId() {
        return thirdLimitId;
    }

    public void setThirdLimitId(String thirdLimitId) {
        this.thirdLimitId = thirdLimitId;
    }
}
