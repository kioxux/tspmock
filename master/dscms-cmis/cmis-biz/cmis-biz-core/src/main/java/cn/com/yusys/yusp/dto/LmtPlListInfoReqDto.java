package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.LmtPlListInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportOtherInfo;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/1819:41
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class LmtPlListInfoReqDto {
    private List<LmtPlListInfo> list;
    private LmtSurveyReportOtherInfo otherInfo;

    public List<LmtPlListInfo> getList() {
        return list;
    }

    public void setList(List<LmtPlListInfo> list) {
        this.list = list;
    }

    public LmtSurveyReportOtherInfo getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(LmtSurveyReportOtherInfo otherInfo) {
        this.otherInfo = otherInfo;
    }
}
