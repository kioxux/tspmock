package cn.com.yusys.yusp.service.server.xdxw0035;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.PldList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *根据流水号查询无还本续贷抵押信息
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0035Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0035Service.class);

    @Autowired
    private LmtGuareInfoMapper lmtGuareInfoMapper;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 交易码：xdxw0035
     * 交易描述：根据流水号查询无还本续贷抵押信息
     *
     * @param indgtSerno
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public List<PldList> getPldListByindgtSerno(String indgtSerno) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, indgtSerno);
        List<PldList> result = null;
        try {
            if (StringUtils.isEmpty(indgtSerno)) {
                throw BizException.error(null, EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
            } else {
                result = lmtGuareInfoMapper.getPldListByindgtSerno(indgtSerno);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(result));
        return result;
    }

}
