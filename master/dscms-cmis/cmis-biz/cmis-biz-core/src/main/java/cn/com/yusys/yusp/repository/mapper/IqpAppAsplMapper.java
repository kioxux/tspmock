/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.AsplBailAcctDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpAppAspl;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAppAsplMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 13:52:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpAppAsplMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpAppAspl selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpAppAspl> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpAppAspl record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpAppAspl record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpAppAspl record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpAppAspl record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据入参查询资产池协议申请数据
     *
     * @param queryMap
     * @return
     */
    IqpAppAspl queryIqpAppAsplDataByParams(Map queryMap);


    /**
     * 根据流水号查询资产池协议申请数据
     *
     * @param serno
     * @return
     */
    List<IqpAppAspl> queryIqpAppAsplDataBySerno(@Param("serno") String serno);

    /**
     * 根据入参获取账户信息
     *
     * @param map
     * @return
     */
    List<AsplBailAcctDto> queryAsplBailAcctDtoDataByParams(Map map);

    /**
     * 资产池协议变更
     * @param chgFlag contNo
     * @return
     */
    int updateChgFlag(@Param("chgFlag") String chgFlag, @Param("contNo") String contNo);

    /**
     * 根据资产池协议编号获取当前有效的资产池协议申请
     * @param  contNo
     * @return
     */
    IqpAppAspl selectAvalByContNo(String contNo);

    /**
     * 根据资产池协议流水号更新资产池协议变更申请
     * @param  serno approveStatus
     * @return
     */
    int updateStatus(@Param("serno") String serno, @Param("approveStatus") String approveStatus);

    /**
     * 根据资产池协议流水号删除资产池协议申请
     * @param  serno
     * @return
     */
    int deleteBySerno(@Param("serno") String serno);

    /**
     * 根据资产池协原议协议编号查询 生效的资产池协议变更
     * @param  cusId
     * @return
     */
    IqpAppAspl selectAvalChgByCusId(@Param("cusId")String cusId);

    int updateSelectiveBySerno(IqpAppAspl iqpAppAspl);
}