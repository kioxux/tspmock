/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.CusIndivUnitDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysWmdMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWmdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-19 22:07:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysWmdService {

    @Autowired
    private RptSpdAnysWmdMapper rptSpdAnysWmdMapper;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;
    @Autowired
    private RptCptlSituLegalRepreLoanService rptCptlSituLegalRepreLoanService;
    @Autowired
    private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private RptOperationService rptOperationService;
    @Autowired
    private RptBasicInfoRealEstateService rptBasicInfoRealEstateService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private RptFncSituBsAnysService rptFncSituBsAnysService;
    @Autowired
    private RptCptlSituExtGuarService rptCptlSituExtGuarService;
    @Autowired
    private ICusClientService iCusClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysWmd selectByPrimaryKey(String serno) {
        return rptSpdAnysWmdMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptSpdAnysWmd> selectAll(QueryModel model) {
        List<RptSpdAnysWmd> records = (List<RptSpdAnysWmd>) rptSpdAnysWmdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysWmd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysWmd> list = rptSpdAnysWmdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysWmd record) {
        return rptSpdAnysWmdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysWmd record) {
        return rptSpdAnysWmdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysWmd record) {
        return rptSpdAnysWmdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysWmd record) {
        return rptSpdAnysWmdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptSpdAnysWmdMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysWmdMapper.deleteByIds(ids);
    }

    public int autoValue(Map params) {
        String serno = params.get("serno").toString();
        int count = 0;
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        //客户经理类别: 01 主办客户经理，02协办客户经理
        String managerType = params.get("managerType").toString();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        BigDecimal curAmt = BigDecimal.ZERO;
        List<String> bankList = new ArrayList<>();
        List<RptCptlSituCorp> rptCptlSituCorpList = rptCptlSituCorpService.selectByModel(model);
        int cptlCount = 0;
        //第5项打分
        int value5 = 0;
        if (CollectionUtils.nonEmpty(rptCptlSituCorpList)) {
            int totalTimes = 0;
            for (RptCptlSituCorp rptCptlSituCorp : rptCptlSituCorpList) {
                bankList.add(rptCptlSituCorp.getBelongBank());
                int debtTimes = rptCptlSituCorp.getDebtTimes();
                int overdueTimes = rptCptlSituCorp.getOverdueTimes();
                totalTimes += debtTimes + overdueTimes;
                curAmt = curAmt.add(rptCptlSituCorp.getCurMonthAmt());
            }
            if (totalTimes == 0) {
                value5 = 5;
            } else if (totalTimes == 1) {
                value5 = 3;
            } else if (totalTimes == 2) {
                value5 = 2;
            } else {
                value5 = 0;
            }
        } else {
            value5 = 5;
        }
        //第1项打分
        int value1 = 0;
        //第2项打分
        int value2 = 1;
        //第三项打分
        int value3 = 0;
        List<RptCptlSituLegalRepreLoan> rptCptlSituLegalRepreLoanList = rptCptlSituLegalRepreLoanService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptCptlSituLegalRepreLoanList)) {
            int debtTimes = 0;
            int cardDebtTimes = 0;
            int totalTime = 0;
            for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : rptCptlSituLegalRepreLoanList) {
                //实际控制人
                if (rptCptlSituLegalRepreLoan.getEnterprisesRelation().equals("02")) {
                    curAmt = curAmt.add(rptCptlSituLegalRepreLoan.getCptlBalance());
                    totalTime += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    if (rptCptlSituLegalRepreLoan.getCptlType().equals("1")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("2")) {
                        cardDebtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("3")) {
                        cardDebtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("4")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                        bankList.add(rptCptlSituLegalRepreLoan.getCptlBankName());
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("5")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("6")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    }
                }
                //实际控制人配偶
                if (rptCptlSituLegalRepreLoan.getEnterprisesRelation().equals("04")) {
                    curAmt = curAmt.add(rptCptlSituLegalRepreLoan.getCptlBalance());
                }
                //法人代表
                if (rptCptlSituLegalRepreLoan.getEnterprisesRelation().equals("01")) {
                    totalTime += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    if (rptCptlSituLegalRepreLoan.getCptlType().equals("1")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("2")) {
                        cardDebtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("3")) {
                        cardDebtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("4")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("5")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("6")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    }
                }
            }
            if (totalTime == 0) {
                value1 = 10;
            } else if (debtTimes <= 1 && cardDebtTimes <= 3) {
                value1 = 7;
            } else if ((debtTimes >= 1 && debtTimes <= 2) && (cardDebtTimes >= 3 && cardDebtTimes <= 5)) {
                value1 = 5;
            } else {
                value1 = 0;
            }

        } else {
            value1 = 10;
        }
        if (CollectionUtils.nonEmpty(bankList)) {
            //去重
            List<String> bankNewList = new ArrayList<>();
            for (String bank : bankList) {
                if (!bankNewList.contains(bank)) {
                    bankNewList.add(bank);
                }
            }
            cptlCount = bankNewList.size();
        }
        if (cptlCount == 0) {
            value3 = 6;
        } else if (cptlCount == 1) {
            value3 = 5;
        } else if (cptlCount == 2) {
            value3 = 4;
        } else if (cptlCount == 3) {
            value3 = 3;
        } else if (cptlCount > 3) {
            value3 = 2;
        }
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssetsList = rptBasicInfoPersFamilyAssetsService.selectByModel(model);
        BigDecimal zyValue = BigDecimal.ZERO;
        BigDecimal hxValue = BigDecimal.ZERO;
        BigDecimal zyjzValue = BigDecimal.ZERO;
        BigDecimal qyhxValue = BigDecimal.ZERO;
        List<RptBasicInfoRealEstate> rptBasicInfoRealEstates = rptBasicInfoRealEstateService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptBasicInfoRealEstates)) {
            for (RptBasicInfoRealEstate rptBasicInfoRealEstate : rptBasicInfoRealEstates) {
                if (Objects.nonNull(rptBasicInfoRealEstate.getEquityCertPaperValue())) {
                    hxValue = hxValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue());
                    qyhxValue = qyhxValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue());
                }
                if (Objects.nonNull(rptBasicInfoRealEstate.getCurrGuarCaseLoanAmt())) {
                    zyjzValue = zyjzValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue().subtract(rptBasicInfoRealEstate.getCurrGuarCaseLoanAmt()));
                }
                if (Objects.nonNull(rptBasicInfoRealEstate.getEquityPaperValue())) {
                    zyValue = zyValue.add(rptBasicInfoRealEstate.getEquityPaperValue());
                    zyjzValue = zyjzValue.add(rptBasicInfoRealEstate.getEquityPaperValue());
                }
            }
        }
        if (CollectionUtils.nonEmpty(rptBasicInfoPersFamilyAssetsList)) {
            for (RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets : rptBasicInfoPersFamilyAssetsList) {
                //自有资产价值
                if (rptBasicInfoPersFamilyAssets.getAssetRange().equals("1")) {
                    if ("02".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "04".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "05".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation())) {
                        zyValue = zyValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                        zyjzValue = zyjzValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                    }
                }//核心资产价值
                else if (rptBasicInfoPersFamilyAssets.getAssetRange().equals("2")) {
                    if ("02".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "04".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "05".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation())) {
                        hxValue = hxValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                        zyjzValue = zyjzValue.add(rptBasicInfoPersFamilyAssets.getMarketValue().subtract(rptBasicInfoPersFamilyAssets.getHasGuarAmt()));
                    }
                    if (rptBasicInfoPersFamilyAssets.getEnterprisesRelation().equals("05")) {
                        qyhxValue = qyhxValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                    }
                }
            }
        }
        if (hxValue.compareTo(BigDecimal.ZERO) > 0 && curAmt.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal divide = curAmt.divide(hxValue, 6, RoundingMode.HALF_UP);
            if (divide.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                value2 = 5;
            } else if (divide.compareTo(BigDecimal.valueOf(0.8)) <= 0) {
                value2 = 4;
            } else if (divide.compareTo(BigDecimal.valueOf(1)) <= 0) {
                value2 = 3;
            } else if (divide.compareTo(BigDecimal.valueOf(1.5)) <= 0) {
                value2 = 2;
            } else if (divide.compareTo(BigDecimal.valueOf(1.5)) > 0) {
                value2 = 1;
            }
        }

        //第4项打分
        int value4 = 0;
        value4 = this.value4(serno, lmtApp.getRptType());
        //第6项打分
        int value6 = 0;
        String cusId = lmtApp.getCusId();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
            String fjobDate = "";
            Map<String, Object> map1 = new HashMap<>();
            map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
            map1.put("cusIdRel", lmtApp.getCusId());
            ResultDto<CusCorpMgrDto> cusCorpMgrByParams = cmisCusClientService.getCusCorpMgrByParams(map1);
            //获取实际控制人从业日期
            if (cusCorpMgrByParams != null && cusCorpMgrByParams.getData() != null) {
                fjobDate = cusCorpMgrByParams.getData().getFjobDate();
            }
            if (StringUtils.nonBlank(fjobDate)) {
                int month = DateUtils.getMonthsByTwoDatesDef(fjobDate, DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                double year = month / 12;
                if (year >= 5) {
                    value6 = 5;
                } else if (year >= 1 && year < 5) {
                    value6 = 2;
                } else if (year < 1) {
                    value6 = 0;
                }
            }
        }else if(CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)){
            ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
            if(cusIndivUnitDtoResultDto!=null&&cusIndivUnitDtoResultDto.getData()!=null){
                CusIndivUnitDto data = cusIndivUnitDtoResultDto.getData();
                String fjobDate = data.getWorkDate();
                String currentDate = DateUtils.getCurrentDate(DateFormatEnum.YEAR);
                if(StringUtils.nonBlank(fjobDate)){
                    int year = Integer.parseInt(currentDate)-Integer.parseInt(fjobDate);
                    if (year >= 5) {
                        value6 = 5;
                    } else if (year >= 1 && year < 5) {
                        value6 = 2;
                    } else if (year < 1) {
                        value6 = 0;
                    }
                }
            }
        }
        //第7项打分
        int value7 = 1;
        RptSpdAnysWmd rptSpdAnysWmd1 = selectByPrimaryKey(serno);
        //国际结算量
        BigDecimal lastYearTotalExports = BigDecimal.ZERO;
        if (StringUtils.nonBlank(rptSpdAnysWmd1.getLastYearTotalExports())) {
            lastYearTotalExports = BigDecimal.valueOf(Double.parseDouble(rptSpdAnysWmd1.getLastYearTotalExports()));
        }
        BigDecimal ownVolume = BigDecimal.ZERO;
        if (StringUtils.nonBlank(rptSpdAnysWmd1.getOwnVolume())) {
            ownVolume = BigDecimal.valueOf(Double.parseDouble(rptSpdAnysWmd1.getOwnVolume()));
        }
        //我行国际结算量
        if (lastYearTotalExports.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal amount7 = ownVolume.divide(lastYearTotalExports, 6, RoundingMode.HALF_UP);
            if (amount7.compareTo(BigDecimal.valueOf(0.8)) >= 0) {
                value7 = 5;
            } else if (amount7.compareTo(BigDecimal.valueOf(0.6)) >= 0) {
                value7 = 4;
            } else if (amount7.compareTo(BigDecimal.valueOf(0.4)) >= 0) {
                value7 = 3;
            } else if (amount7.compareTo(BigDecimal.valueOf(0.2)) >= 0) {
                value7 = 2;
            } else if (amount7.compareTo(BigDecimal.valueOf(0.2)) < 0) {
                value7 = 1;
            }

        }

        //第8项打分
        int value8 = 0;
        if (lastYearTotalExports.compareTo(BigDecimal.valueOf(10000000)) >= 0) {
            value8 = 12;
        } else if (lastYearTotalExports.compareTo(BigDecimal.valueOf(5000000)) >= 0 && lastYearTotalExports.compareTo(BigDecimal.valueOf(10000000)) < 0) {
            value8 = 10;
        } else if (lastYearTotalExports.compareTo(BigDecimal.valueOf(3000000)) >= 0 && lastYearTotalExports.compareTo(BigDecimal.valueOf(5000000)) < 0) {
            value8 = 8;
        } else if (lastYearTotalExports.compareTo(BigDecimal.valueOf(1000000)) >= 0 && lastYearTotalExports.compareTo(BigDecimal.valueOf(3000000)) < 0) {
            value8 = 4;
        } else if (lastYearTotalExports.compareTo(BigDecimal.valueOf(1000000)) < 0) {
            value8 = 0;
        }

        //第9项打分
        int value9 = 2;
        value9 = this.value9(serno, lmtApp.getRptType());
        //第14项打分
        int value14 = 4;
        //业务申请金额
        BigDecimal lmtAmt = BigDecimal.ZERO;
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectSubPrdBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
            String subSerno = "";
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                if (StringUtils.nonBlank(lmtBizTypeProp)) {
                    if ("P032".equals(lmtBizTypeProp)) {
                        lmtAmt = lmtAppSubPrd.getLmtAmt();
                        break;
                    }
                }
            }
        }
        if (zyjzValue.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal divide = lmtAmt.divide(zyjzValue, 6, RoundingMode.HALF_UP);
            if (divide.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                value14 = 8;
            } else if (divide.compareTo(BigDecimal.valueOf(0.6)) <= 0) {
                value14 = 7;
            } else if (divide.compareTo(BigDecimal.valueOf(0.7)) <= 0) {
                value14 = 6;
            } else if (divide.compareTo(BigDecimal.valueOf(1)) <= 0) {
                value14 = 4;
            }

        }
        RptSpdAnysWmd rptSpdAnysWmd = new RptSpdAnysWmd();
        rptSpdAnysWmd.setSerno(serno);
        User userInfo = SessionUtils.getUserInformation();
        if (managerType.equals("01")) {
            rptSpdAnysWmd.setPfkWmd1Grade1(value1);
            rptSpdAnysWmd.setPfkWmd2Grade1(value2);
            rptSpdAnysWmd.setPfkWmd3Grade1(value3);
            rptSpdAnysWmd.setPfkWmd4Grade1(value4);
            rptSpdAnysWmd.setPfkWmd5Grade1(value5);
            rptSpdAnysWmd.setPfkWmd6Grade1(value6);
            rptSpdAnysWmd.setPfkWmd7Grade1(value7);
            rptSpdAnysWmd.setPfkWmd8Grade1(value8);
            rptSpdAnysWmd.setPfkWmd9Grade1(value9);
            rptSpdAnysWmd.setPfkWmd14Grade1(value14);
            //rptSpdAnysWmd.setMainManagerId(userInfo.getLoginCode());
            count = rptSpdAnysWmdMapper.updateByPrimaryKeySelective(rptSpdAnysWmd);
        } else if (managerType.equals("02")) {
            RptSpdAnysWmd temp = selectByPrimaryKey(serno);
            if (Objects.nonNull(temp)) {
                rptSpdAnysWmd.setPfkWmd7Grade2(temp.getPfkWmd7Grade1());
                rptSpdAnysWmd.setPfkWmd10Grade2(temp.getPfkWmd10Grade1());
                rptSpdAnysWmd.setPfkWmd11Grade2(temp.getPfkWmd11Grade1());
                rptSpdAnysWmd.setPfkWmd12Grade2(temp.getPfkWmd12Grade1());
                rptSpdAnysWmd.setPfkWmd13Grade2(temp.getPfkWmd13Grade1());
                rptSpdAnysWmd.setPfkWmd15Grade2(temp.getPfkWmd15Grade1());
                rptSpdAnysWmd.setPfkWmd16Grade2(temp.getPfkWmd16Grade1());
            }
            rptSpdAnysWmd.setPfkWmd1Grade2(value1);
            rptSpdAnysWmd.setPfkWmd2Grade2(value2);
            rptSpdAnysWmd.setPfkWmd3Grade2(value3);
            rptSpdAnysWmd.setPfkWmd4Grade2(value4);
            rptSpdAnysWmd.setPfkWmd5Grade2(value5);
            rptSpdAnysWmd.setPfkWmd6Grade2(value6);
            rptSpdAnysWmd.setPfkWmd8Grade2(value8);
            rptSpdAnysWmd.setPfkWmd9Grade2(value9);
            rptSpdAnysWmd.setPfkWmd14Grade2(value14);
            //rptSpdAnysWmd.setAssistManagerId(userInfo.getLoginCode());
            count = rptSpdAnysWmdMapper.updateByPrimaryKeySelective(rptSpdAnysWmd);
        }

        return count;
    }

    /**
     * 外贸贷第4项打分
     *
     * @param serno
     * @param rptType
     * @return
     */
    public int value4(String serno, String rptType) {
        BigDecimal currAmt = BigDecimal.ZERO;
        List<RptFncSituBsAnys> rptFncSituBsAnysList = new ArrayList<>();
        if (CmisBizConstants.STD_RPT_TYPE_C11.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C14.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_4);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_I04.equals(rptType) || CmisBizConstants.STD_RPT_TYPE_I05.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_3);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        }
        if (CollectionUtils.nonEmpty(rptFncSituBsAnysList)) {
            for (RptFncSituBsAnys rptFncSituBsAnys : rptFncSituBsAnysList) {
                if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
                    if ("所有者权益".equals(rptFncSituBsAnys.getSubjectName())) {
                        currAmt = rptFncSituBsAnys.getAmtSr();
                    }
                } else {
                    if ("所有者权益".equals(rptFncSituBsAnys.getSubjectName())) {
                        currAmt = rptFncSituBsAnys.getCurYearLastMonth();
                    }
                }
            }
        }
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        BigDecimal extGuarAmt = BigDecimal.ZERO;
        List<RptCptlSituExtGuar> rptCptlSituExtGuars = rptCptlSituExtGuarService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptCptlSituExtGuars)) {
            for (RptCptlSituExtGuar rptCptlSituExtGuar : rptCptlSituExtGuars) {
                extGuarAmt = extGuarAmt.add(rptCptlSituExtGuar.getBalance());
            }
        }
        if (extGuarAmt.compareTo(BigDecimal.ZERO) == 0) {
            return 10;
        } else {
            BigDecimal subtract = extGuarAmt.subtract(currAmt);
            if (subtract.compareTo(BigDecimal.ZERO) <= 0) {
                return 10;
            }
            if (currAmt.compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal divide = extGuarAmt.divide(currAmt, 6, RoundingMode.HALF_UP);
                if (divide.compareTo(BigDecimal.valueOf(5)) <= 0) {
                    return 8;
                } else if (divide.compareTo(BigDecimal.valueOf(10)) <= 0) {
                    return 4;
                } else if (divide.compareTo(BigDecimal.valueOf(10)) > 0) {
                    return 0;
                }
            }
        }
        return 0;
    }

    /**
     * 外贸贷第9项打分
     *
     * @param serno
     * @param rptType
     * @return
     */
    public int value9(String serno, String rptType) {
        BigDecimal currAmt = BigDecimal.ZERO;
        List<RptFncSituBsAnys> rptFncSituBsAnysList = new ArrayList<>();
        if (CmisBizConstants.STD_RPT_TYPE_C11.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C14.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_4);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_I04.equals(rptType) || CmisBizConstants.STD_RPT_TYPE_I05.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_3);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        }
        if (CollectionUtils.nonEmpty(rptFncSituBsAnysList)) {
            for (RptFncSituBsAnys rptFncSituBsAnys : rptFncSituBsAnysList) {
                if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
                    if ("应收账款周转率".equals(rptFncSituBsAnys.getSubjectName())) {
                        currAmt = rptFncSituBsAnys.getAmtSr();
                    }
                } else {
                    if ("应收账款周转率".equals(rptFncSituBsAnys.getSubjectName())) {
                        currAmt = rptFncSituBsAnys.getCurYearLastMonth();
                    }
                }
            }
        }
        if (currAmt.compareTo(BigDecimal.valueOf(6)) >= 0) {
            return 6;
        } else if ((currAmt.compareTo(BigDecimal.valueOf(5)) >= 0) && (currAmt.compareTo(BigDecimal.valueOf(6)) < 0)) {
            return 5;
        } else if ((currAmt.compareTo(BigDecimal.valueOf(3)) >= 0) && (currAmt.compareTo(BigDecimal.valueOf(5)) < 0)) {
            return 4;
        } else if ((currAmt.compareTo(BigDecimal.valueOf(2)) >= 0) && (currAmt.compareTo(BigDecimal.valueOf(3)) < 0)) {
            return 3;
        } else if (currAmt.compareTo(BigDecimal.valueOf(2)) < 2) {
            return 2;
        } else {
            return 2;
        }
    }

}
