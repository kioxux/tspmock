package cn.com.yusys.yusp.web.server.xddh0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0004.req.Xddh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0004.Xddh0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询还款是否在途
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0004:查询还款是否在途")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0004Resource.class);

    @Autowired
    private Xddh0004Service xddh0004Service;

    /**
     * 交易码：xddh0004
     * 交易描述：查询还款是否在途
     *
     * @param xddh0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询还款是否在途")
    @PostMapping("/xddh0004")
    protected @ResponseBody
    ResultDto<Xddh0004DataRespDto> xddh0004(@Validated @RequestBody Xddh0004DataReqDto xddh0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004DataReqDto));
        Xddh0004DataRespDto xddh0004DataRespDto = new Xddh0004DataRespDto();// 响应Dto:查询还款是否在途
        ResultDto<Xddh0004DataRespDto> xddh0004DataResultDto = new ResultDto<>();
        String billNo = xddh0004DataReqDto.getBillNo();//借据号
        try {
            //校验传入的billNo是否有效（不为空串或null，暂时没有进行正则校验）
            if (StringUtils.isBlank(billNo)){
                xddh0004DataResultDto.setCode(EpbEnum.EPB090009.key);
                xddh0004DataResultDto.setMessage(EpbEnum.EPB090009.value);
                xddh0004DataResultDto.setData(xddh0004DataRespDto);
                return xddh0004DataResultDto;
            }
            // 从xddh0004DataReqDto获取业务值进行业务逻辑处理
            xddh0004DataRespDto = xddh0004Service.xddh0004(billNo);

            xddh0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, e.getMessage());
            // 封装xddh0004DataResultDto中异常返回码和返回信息
            xddh0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0004DataRespDto到xddh0004DataResultDto中
        xddh0004DataResultDto.setData(xddh0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004DataResultDto));
        return xddh0004DataResultDto;
    }
}
