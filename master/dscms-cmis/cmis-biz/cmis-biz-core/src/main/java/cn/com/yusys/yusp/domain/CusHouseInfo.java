/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusHouseInfo
 * @类描述: cus_house_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_house_info")
public class CusHouseInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 面签流水号 **/
	@Column(name = "SIGNATURE_SERNO", unique = false, nullable = true, length = 40)
	private String signatureSerno;
	
	/** 省编号 **/
	@Column(name = "PROVINCE_ID", unique = false, nullable = true, length = 40)
	private String provinceId;
	
	/** 省名称 **/
	@Column(name = "PROVINCE_NAME", unique = false, nullable = true, length = 40)
	private String provinceName;
	
	/** 省别名 **/
	@Column(name = "PROVINCE_ALIAS", unique = false, nullable = true, length = 40)
	private String provinceAlias;
	
	/** 省国际编码 **/
	@Column(name = "PROVINCE_INTER_CODE", unique = false, nullable = true, length = 40)
	private String provinceInterCode;
	
	/** 城市编号 **/
	@Column(name = "CITY_ID", unique = false, nullable = true, length = 40)
	private String cityId;
	
	/** 城市名称 **/
	@Column(name = "CITY_NAME", unique = false, nullable = true, length = 40)
	private String cityName;
	
	/** 城市别名 **/
	@Column(name = "CITY_ALIAS", unique = false, nullable = true, length = 40)
	private String cityAlias;
	
	/** 城市国际编码 **/
	@Column(name = "CITY_CODE", unique = false, nullable = true, length = 40)
	private String cityCode;
	
	/** 区县编号 **/
	@Column(name = "COUNTY_ID", unique = false, nullable = true, length = 40)
	private String countyId;
	
	/** 区县名称 **/
	@Column(name = "COUNTY_NAME", unique = false, nullable = true, length = 40)
	private String countyName;
	
	/** 区县国际编码 **/
	@Column(name = "COUNTY_CODE", unique = false, nullable = true, length = 40)
	private String countyCode;
	
	/** 楼盘编号 **/
	@Column(name = "COMMUNITY_ID", unique = false, nullable = true, length = 40)
	private String communityId;
	
	/** 楼盘名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 40)
	private String communityName;
	
	/** 楼栋编号 **/
	@Column(name = "BUILDING_ID", unique = false, nullable = true, length = 40)
	private String buildingId;
	
	/** 楼栋名称 **/
	@Column(name = "BUILDING_NAME", unique = false, nullable = true, length = 40)
	private String buildingName;
	
	/** 楼层(实际层) **/
	@Column(name = "FLOOR", unique = false, nullable = true, length = 10)
	private Integer floor;
	
	/** 总层 **/
	@Column(name = "GENERAL_FLOOR", unique = false, nullable = true, length = 10)
	private Integer generalFloor;
	
	/** 房号编号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 40)
	private String roomNum;
	
	/** 房号名称 **/
	@Column(name = "ROOM_NAME", unique = false, nullable = true, length = 40)
	private String roomName;
	
	/** 房产类型 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 9)
	private String houseType;
	
	/** 房产面积 **/
	@Column(name = "HOUSE_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseSqu;
	
	/** 产权证号 **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 100)
	private String houseLandNo;
	
	/** 区位信息 **/
	@Column(name = "LOCATION_INFO", unique = false, nullable = true, length = 200)
	private String locationInfo;
	
	/** 土地面积 **/
	@Column(name = "LAND_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landSqu;
	
	/** 是否出租 **/
	@Column(name = "IS_LEASE", unique = false, nullable = true, length = 5)
	private String isLease;
	
	/** 评估方式 **/
	@Column(name = "EVAL_TYPE", unique = false, nullable = true, length = 5)
	private String evalType;
	
	/** 评估价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 抵押物所有权人 **/
	@Column(name = "PLDIMN_OWNER", unique = false, nullable = true, length = 40)
	private String pldimnOwner;
	
	/** 自行车库面积 **/
	@Column(name = "BICYCLE_PARKING_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bicycleParkingSqu;
	
	/** 车位面积 **/
	@Column(name = "CARPORT_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal carportSqu;
	
	/** 阁楼面积 **/
	@Column(name = "ATTIC_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal atticSqu;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 自行车库价值 **/
	@Column(name = "BICYCLE_PARKING_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bicycleParkingAmt;
	
	/** 车位价值 **/
	@Column(name = "CARPORT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal carportAmt;
	
	/** 阁楼价值 **/
	@Column(name = "ATTIC_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal atticAmt;
	
	/** 总评估价值 **/
	@Column(name = "EVAL_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalTotalAmt;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param signatureSerno
	 */
	public void setSignatureSerno(String signatureSerno) {
		this.signatureSerno = signatureSerno;
	}
	
    /**
     * @return signatureSerno
     */
	public String getSignatureSerno() {
		return this.signatureSerno;
	}
	
	/**
	 * @param provinceId
	 */
	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}
	
    /**
     * @return provinceId
     */
	public String getProvinceId() {
		return this.provinceId;
	}
	
	/**
	 * @param provinceName
	 */
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	
    /**
     * @return provinceName
     */
	public String getProvinceName() {
		return this.provinceName;
	}
	
	/**
	 * @param provinceAlias
	 */
	public void setProvinceAlias(String provinceAlias) {
		this.provinceAlias = provinceAlias;
	}
	
    /**
     * @return provinceAlias
     */
	public String getProvinceAlias() {
		return this.provinceAlias;
	}
	
	/**
	 * @param provinceInterCode
	 */
	public void setProvinceInterCode(String provinceInterCode) {
		this.provinceInterCode = provinceInterCode;
	}
	
    /**
     * @return provinceInterCode
     */
	public String getProvinceInterCode() {
		return this.provinceInterCode;
	}
	
	/**
	 * @param cityId
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	
    /**
     * @return cityId
     */
	public String getCityId() {
		return this.cityId;
	}
	
	/**
	 * @param cityName
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
    /**
     * @return cityName
     */
	public String getCityName() {
		return this.cityName;
	}
	
	/**
	 * @param cityAlias
	 */
	public void setCityAlias(String cityAlias) {
		this.cityAlias = cityAlias;
	}
	
    /**
     * @return cityAlias
     */
	public String getCityAlias() {
		return this.cityAlias;
	}
	
	/**
	 * @param cityCode
	 */
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	
    /**
     * @return cityCode
     */
	public String getCityCode() {
		return this.cityCode;
	}
	
	/**
	 * @param countyId
	 */
	public void setCountyId(String countyId) {
		this.countyId = countyId;
	}
	
    /**
     * @return countyId
     */
	public String getCountyId() {
		return this.countyId;
	}
	
	/**
	 * @param countyName
	 */
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	
    /**
     * @return countyName
     */
	public String getCountyName() {
		return this.countyName;
	}
	
	/**
	 * @param countyCode
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	
    /**
     * @return countyCode
     */
	public String getCountyCode() {
		return this.countyCode;
	}
	
	/**
	 * @param communityId
	 */
	public void setCommunityId(String communityId) {
		this.communityId = communityId;
	}
	
    /**
     * @return communityId
     */
	public String getCommunityId() {
		return this.communityId;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param buildingId
	 */
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	
    /**
     * @return buildingId
     */
	public String getBuildingId() {
		return this.buildingId;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	
    /**
     * @return buildingName
     */
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	
    /**
     * @return floor
     */
	public Integer getFloor() {
		return this.floor;
	}
	
	/**
	 * @param generalFloor
	 */
	public void setGeneralFloor(Integer generalFloor) {
		this.generalFloor = generalFloor;
	}
	
    /**
     * @return generalFloor
     */
	public Integer getGeneralFloor() {
		return this.generalFloor;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param roomName
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
    /**
     * @return roomName
     */
	public String getRoomName() {
		return this.roomName;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(java.math.BigDecimal houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return houseSqu
     */
	public java.math.BigDecimal getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param locationInfo
	 */
	public void setLocationInfo(String locationInfo) {
		this.locationInfo = locationInfo;
	}
	
    /**
     * @return locationInfo
     */
	public String getLocationInfo() {
		return this.locationInfo;
	}
	
	/**
	 * @param landSqu
	 */
	public void setLandSqu(java.math.BigDecimal landSqu) {
		this.landSqu = landSqu;
	}
	
    /**
     * @return landSqu
     */
	public java.math.BigDecimal getLandSqu() {
		return this.landSqu;
	}
	
	/**
	 * @param isLease
	 */
	public void setIsLease(String isLease) {
		this.isLease = isLease;
	}
	
    /**
     * @return isLease
     */
	public String getIsLease() {
		return this.isLease;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType;
	}
	
    /**
     * @return evalType
     */
	public String getEvalType() {
		return this.evalType;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param pldimnOwner
	 */
	public void setPldimnOwner(String pldimnOwner) {
		this.pldimnOwner = pldimnOwner;
	}
	
    /**
     * @return pldimnOwner
     */
	public String getPldimnOwner() {
		return this.pldimnOwner;
	}
	
	/**
	 * @param bicycleParkingSqu
	 */
	public void setBicycleParkingSqu(java.math.BigDecimal bicycleParkingSqu) {
		this.bicycleParkingSqu = bicycleParkingSqu;
	}
	
    /**
     * @return bicycleParkingSqu
     */
	public java.math.BigDecimal getBicycleParkingSqu() {
		return this.bicycleParkingSqu;
	}
	
	/**
	 * @param carportSqu
	 */
	public void setCarportSqu(java.math.BigDecimal carportSqu) {
		this.carportSqu = carportSqu;
	}
	
    /**
     * @return carportSqu
     */
	public java.math.BigDecimal getCarportSqu() {
		return this.carportSqu;
	}
	
	/**
	 * @param atticSqu
	 */
	public void setAtticSqu(java.math.BigDecimal atticSqu) {
		this.atticSqu = atticSqu;
	}
	
    /**
     * @return atticSqu
     */
	public java.math.BigDecimal getAtticSqu() {
		return this.atticSqu;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param bicycleParkingAmt
	 */
	public void setBicycleParkingAmt(java.math.BigDecimal bicycleParkingAmt) {
		this.bicycleParkingAmt = bicycleParkingAmt;
	}
	
    /**
     * @return bicycleParkingAmt
     */
	public java.math.BigDecimal getBicycleParkingAmt() {
		return this.bicycleParkingAmt;
	}
	
	/**
	 * @param carportAmt
	 */
	public void setCarportAmt(java.math.BigDecimal carportAmt) {
		this.carportAmt = carportAmt;
	}
	
    /**
     * @return carportAmt
     */
	public java.math.BigDecimal getCarportAmt() {
		return this.carportAmt;
	}
	
	/**
	 * @param atticAmt
	 */
	public void setAtticAmt(java.math.BigDecimal atticAmt) {
		this.atticAmt = atticAmt;
	}
	
    /**
     * @return atticAmt
     */
	public java.math.BigDecimal getAtticAmt() {
		return this.atticAmt;
	}
	
	/**
	 * @param evalTotalAmt
	 */
	public void setEvalTotalAmt(java.math.BigDecimal evalTotalAmt) {
		this.evalTotalAmt = evalTotalAmt;
	}
	
    /**
     * @return evalTotalAmt
     */
	public java.math.BigDecimal getEvalTotalAmt() {
		return this.evalTotalAmt;
	}


}