/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarZy
 * @类描述: rpt_lmt_repay_anys_guar_zy数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 23:10:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_zy")
public class RptLmtRepayAnysGuarZy extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 质押物情况 **/
	@Column(name = "PLED_INFOR", unique = false, nullable = true, length = 65535)
	private String pledInfor;
	
	/** 所有权人 **/
	@Column(name = "OWNER", unique = false, nullable = true, length = 80)
	private String owner;
	
	/** 质押物现价 **/
	@Column(name = "PLED_CUR_VAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pledCurVal;
	
	/** 质押率 **/
	@Column(name = "PLED_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pledRate;
	
	/** 存在风险 **/
	@Column(name = "EXITS_RISK", unique = false, nullable = true, length = 40)
	private String exitsRisk;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param pledInfor
	 */
	public void setPledInfor(String pledInfor) {
		this.pledInfor = pledInfor;
	}
	
    /**
     * @return pledInfor
     */
	public String getPledInfor() {
		return this.pledInfor;
	}
	
	/**
	 * @param owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
    /**
     * @return owner
     */
	public String getOwner() {
		return this.owner;
	}
	
	/**
	 * @param pledCurVal
	 */
	public void setPledCurVal(java.math.BigDecimal pledCurVal) {
		this.pledCurVal = pledCurVal;
	}
	
    /**
     * @return pledCurVal
     */
	public java.math.BigDecimal getPledCurVal() {
		return this.pledCurVal;
	}
	
	/**
	 * @param pledRate
	 */
	public void setPledRate(java.math.BigDecimal pledRate) {
		this.pledRate = pledRate;
	}
	
    /**
     * @return pledRate
     */
	public java.math.BigDecimal getPledRate() {
		return this.pledRate;
	}
	
	/**
	 * @param exitsRisk
	 */
	public void setExitsRisk(String exitsRisk) {
		this.exitsRisk = exitsRisk;
	}
	
    /**
     * @return exitsRisk
     */
	public String getExitsRisk() {
		return this.exitsRisk;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}