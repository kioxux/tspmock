package cn.com.yusys.yusp.web.server.xdtz0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0017.req.Xdtz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.resp.Xdtz0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0017.Xdtz0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:票据更换（通知信贷更改票据暂用额度台账）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0017:票据更换（通知信贷更改票据暂用额度台账）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0017Resource.class);

    @Autowired
    private Xdtz0017Service xdtz0017Service;

    /**
     * 交易码：xdtz0017
     * 交易描述：票据更换（通知信贷更改票据暂用额度台账）
     *
     * @param xdtz0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("票据更换（通知信贷更改票据暂用额度台账）")
    @PostMapping("/xdtz0017")
    protected @ResponseBody
    ResultDto<Xdtz0017DataRespDto> xdtz0017(@Validated @RequestBody Xdtz0017DataReqDto xdtz0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataReqDto));
        Xdtz0017DataRespDto xdtz0017DataRespDto = new Xdtz0017DataRespDto();// 响应Dto:票据更换（通知信贷更改票据暂用额度台账）
        ResultDto<Xdtz0017DataRespDto> xdtz0017DataResultDto = new ResultDto<>();

        String newDrftNo = xdtz0017DataReqDto.getNewDrftNo();//新票号
        String pyeeName = xdtz0017DataReqDto.getPyeeName();//收款人名称
        String pyeeAcctbNo = xdtz0017DataReqDto.getPyeeAcctbNo();//收款人开户行行号
        String pyeeAcctNo = xdtz0017DataReqDto.getPyeeAcctNo();//收款人账号
        try {
            // 从xdtz0017DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataReqDto));
            xdtz0017DataRespDto = xdtz0017Service.xdtz0017(xdtz0017DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataRespDto));
            // 封装xdtz0017DataResultDto中正确的返回码和返回信息
            xdtz0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, e.getMessage());
            // 封装xdtz0017DataResultDto中异常返回码和返回信息
            xdtz0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0017DataRespDto到xdtz0017DataResultDto中
        xdtz0017DataResultDto.setData(xdtz0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataResultDto));
        return xdtz0017DataResultDto;
    }
}
