/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccTContRel;
import cn.com.yusys.yusp.service.AccTContRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTContRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 16:43:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/acctcontrel")
public class AccTContRelResource {
    @Autowired
    private AccTContRelService accTContRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccTContRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccTContRel> list = accTContRelService.selectAll(queryModel);
        return new ResultDto<List<AccTContRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccTContRel>> index(QueryModel queryModel) {
        List<AccTContRel> list = accTContRelService.selectByModel(queryModel);
        return new ResultDto<List<AccTContRel>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccTContRel> create(@RequestBody AccTContRel accTContRel) throws URISyntaxException {
        accTContRelService.insert(accTContRel);
        return new ResultDto<AccTContRel>(accTContRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccTContRel accTContRel) throws URISyntaxException {
        int result = accTContRelService.update(accTContRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String pvpSerno) {
        int result = accTContRelService.deleteByPrimaryKey(pkId, pvpSerno);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:selectbyaccp
     * @函数描述: 根据核心银承编号
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyPvpSerno")
    protected ResultDto<List<AccTContRel>> selectbyaccp(@RequestBody QueryModel model) {
        List<AccTContRel> results = accTContRelService.selectByModel(model);
        return new ResultDto<List<AccTContRel>>(results);
    }
}
