/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherCnyRateLoanRelOldRate;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateLoanRelOldRateMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateLoanRelOldRateService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-05 14:43:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateLoanRelOldRateService {

    @Autowired
    private OtherCnyRateLoanRelOldRateMapper otherCnyRateLoanRelOldRateMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateLoanRelOldRate selectByPrimaryKey(String pkId) {
        return otherCnyRateLoanRelOldRateMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateLoanRelOldRate> selectAll(QueryModel model) {
        List<OtherCnyRateLoanRelOldRate> records = (List<OtherCnyRateLoanRelOldRate>) otherCnyRateLoanRelOldRateMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateLoanRelOldRate> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateLoanRelOldRate> list = otherCnyRateLoanRelOldRateMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateLoanRelOldRate record) {
        return otherCnyRateLoanRelOldRateMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateLoanRelOldRate record) {
        return otherCnyRateLoanRelOldRateMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateLoanRelOldRate record) {
        return otherCnyRateLoanRelOldRateMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateLoanRelOldRate record) {
        return otherCnyRateLoanRelOldRateMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return otherCnyRateLoanRelOldRateMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateLoanRelOldRateMapper.deleteByIds(ids);
    }
}
