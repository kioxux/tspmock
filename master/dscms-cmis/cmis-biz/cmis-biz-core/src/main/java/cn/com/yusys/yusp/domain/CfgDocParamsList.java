/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsList
 * @类描述: cfg_doc_params_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_doc_params_list")
public class CfgDocParamsList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 配置流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CDPL_SERNO")
	private String cdplSerno;
	
	/** 档案分类 **/
	@Column(name = "DOC_CLASS", unique = false, nullable = true, length = 5)
	private String docClass;
	
	/** 档案类型 **/
	@Column(name = "DOC_TYPE", unique = false, nullable = true, length = 5)
	private String docType;
	
	/** 是否启用 **/
	@Column(name = "IS_BEGIN", unique = false, nullable = true, length = 5)
	private String isBegin;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param cdplSerno
	 */
	public void setCdplSerno(String cdplSerno) {
		this.cdplSerno = cdplSerno;
	}
	
    /**
     * @return cdplSerno
     */
	public String getCdplSerno() {
		return this.cdplSerno;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass;
	}
	
    /**
     * @return docClass
     */
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
    /**
     * @return docType
     */
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin;
	}
	
    /**
     * @return isBegin
     */
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}