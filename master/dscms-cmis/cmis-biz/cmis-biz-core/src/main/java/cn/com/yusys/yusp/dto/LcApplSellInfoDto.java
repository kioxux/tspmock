package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LcApplSellInfo
 * @类描述: lc_appl_sell_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:19:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LcApplSellInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 人员类型 STD_ZB_MGR_USR_KIND **/
	private String usrKind;
	
	/** 营销（管户）人员姓名 **/
	private String usrName;
	
	/** 营销（管户）人员编号 **/
	private String usrCode;
	
	/** 业务系数 **/
	private java.math.BigDecimal bsRate;
	
	/** 责任认定系数 **/
	private java.math.BigDecimal resBsRate;
	
	/** 登记时间 **/
	private String createTime;
	
	/** 登记人 **/
	private String createUsr;
	
	/** 最后修改时间 **/
	private String updDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 状态  STD_ZB_DATA_STS **/
	private String status;
	
	/** 操作类型 **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param usrKind
	 */
	public void setUsrKind(String usrKind) {
		this.usrKind = usrKind == null ? null : usrKind.trim();
	}
	
    /**
     * @return UsrKind
     */	
	public String getUsrKind() {
		return this.usrKind;
	}
	
	/**
	 * @param usrName
	 */
	public void setUsrName(String usrName) {
		this.usrName = usrName == null ? null : usrName.trim();
	}
	
    /**
     * @return UsrName
     */	
	public String getUsrName() {
		return this.usrName;
	}
	
	/**
	 * @param usrCode
	 */
	public void setUsrCode(String usrCode) {
		this.usrCode = usrCode == null ? null : usrCode.trim();
	}
	
    /**
     * @return UsrCode
     */	
	public String getUsrCode() {
		return this.usrCode;
	}
	
	/**
	 * @param bsRate
	 */
	public void setBsRate(java.math.BigDecimal bsRate) {
		this.bsRate = bsRate;
	}
	
    /**
     * @return BsRate
     */	
	public java.math.BigDecimal getBsRate() {
		return this.bsRate;
	}
	
	/**
	 * @param resBsRate
	 */
	public void setResBsRate(java.math.BigDecimal resBsRate) {
		this.resBsRate = resBsRate;
	}
	
    /**
     * @return ResBsRate
     */	
	public java.math.BigDecimal getResBsRate() {
		return this.resBsRate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}
	
    /**
     * @return CreateTime
     */	
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param createUsr
	 */
	public void setCreateUsr(String createUsr) {
		this.createUsr = createUsr == null ? null : createUsr.trim();
	}
	
    /**
     * @return CreateUsr
     */	
	public String getCreateUsr() {
		return this.createUsr;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}