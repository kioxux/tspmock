package cn.com.yusys.yusp.service.client.bsp.xwd.xwd013;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req.Xwd013ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2XwywglptClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
public class Xwd013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xwd013Service.class);

    // 1）注入：BSP封装调用新微贷系统的接口
    @Autowired
    private Dscms2XwywglptClientService dscms2XwywglptClientService;

    @Transactional
    public Xwd013RespDto xwd013(Xwd013ReqDto xwd013ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value, JSON.toJSONString(xwd013ReqDto));
        ResultDto<Xwd013RespDto> xwd013ResultDto = dscms2XwywglptClientService.xwd013(xwd013ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value, JSON.toJSONString(xwd013ResultDto));
        String xwd013Code = Optional.ofNullable(xwd013ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xwd013Meesage = Optional.ofNullable(xwd013ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Xwd013RespDto xwd013RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xwd013ResultDto.getCode())) {
            //  获取相关的值并解析
            xwd013RespDto = xwd013ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(xwd013Code, xwd013Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value);
        return xwd013RespDto;
    }

}
