/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.IqpApplAppt;
import cn.com.yusys.yusp.domain.IqpApplApptRel;
import cn.com.yusys.yusp.domain.IqpApptCompany;
import cn.com.yusys.yusp.domain.IqpApptSelfMgr;
import cn.com.yusys.yusp.dto.IqpApplApptDetailDto;
import cn.com.yusys.yusp.dto.IqpApplApptDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpApplApptMapper;
import cn.com.yusys.yusp.repository.mapper.IqpApplApptRelMapper;
import cn.com.yusys.yusp.repository.mapper.IqpApptCompanyMapper;
import cn.com.yusys.yusp.repository.mapper.IqpApptSelfMgrMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:27:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpApplApptService {

    @Autowired
    private IqpApplApptMapper iqpApplApptMapper;

    @Autowired
    private IqpApplApptRelMapper iqpApplApptRelMapper;

    @Autowired
    private IqpApptCompanyMapper iqpApptCompanyMapper;

    @Autowired
    private IqpApptSelfMgrMapper iqpApptSelfMgrMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpApplAppt selectByPrimaryKey(String apptCode) {
        return iqpApplApptMapper.selectByPrimaryKey(apptCode);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpApplAppt> selectAll(QueryModel model) {
        List<IqpApplAppt> records = (List<IqpApplAppt>) iqpApplApptMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpApplAppt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpApplAppt> list = iqpApplApptMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpApplAppt record) {
        return iqpApplApptMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpApplAppt record) {
        return iqpApplApptMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpApplAppt record) {
        return iqpApplApptMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpApplAppt record) {
        return iqpApplApptMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String apptCode) {
        return iqpApplApptMapper.deleteByPrimaryKey(apptCode);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpApplApptMapper.deleteByIds(ids);
    }


    /**
     * 保存申请人详细信息
     * @param iqpApplApptDetailDto
     */
    public void saveIqpApplApptDetailDto(IqpApplApptDetailDto iqpApplApptDetailDto) {
        //1.从dto中  复制  申请人详细信息 保存。
        //2. 根据 apptCode 获取 单位信息，有更新，没有插入
        //3. 根据 apptCode 获取 经营信息，有更新，没有插入
        IqpApplAppt iqpApplAppt = new IqpApplAppt();
        BeanUtils.copyProperties(iqpApplApptDetailDto,iqpApplAppt);
        iqpApplApptMapper.updateByPrimaryKeySelective(iqpApplAppt);

        String apptCode = iqpApplApptDetailDto.getApptCode();

        //2. 根据 apptCode 获取 单位信息，有更新，没有插入.
        IqpApptCompany iqpApptCompany = new IqpApptCompany();
        BeanUtils.copyProperties(iqpApplApptDetailDto,iqpApptCompany);
        /*处理下模板的两个tel重名 tel1 公司电话， tel 个人电话*/
        iqpApptCompany.setTel(iqpApplApptDetailDto.getTel1());
        if( iqpApptCompanyMapper.selectByApptCode(apptCode) == null){
            //新增  设置公司信息的序列号
            String serno = sequenceTemplateService.getSequenceTemplate(SeqConstant.YPSEQ, new HashMap<>());
            iqpApptCompany.setSerno(serno);
            iqpApptCompanyMapper.insertSelective(iqpApptCompany);
        } else {
            //更新
            String serno = iqpApptCompanyMapper.selectByApptCode(apptCode).getSerno();
            iqpApptCompany.setSerno(serno);
            iqpApptCompanyMapper.updateByPrimaryKeySelective(iqpApptCompany);
        }

        //3. 根据 apptCode 获取 经营信息，有更新，没有插入
        IqpApptSelfMgr iqpApptSelfMgr = new IqpApptSelfMgr();
        BeanUtils.copyProperties(iqpApplApptDetailDto,iqpApptSelfMgr);
        if(iqpApptSelfMgrMapper.selectByApptCode(apptCode) == null ){
            //新增  设置经营信息的序列号
            String serno = sequenceTemplateService.getSequenceTemplate(SeqConstant.YPSEQ, new HashMap<>());
            iqpApptSelfMgr.setSerno(serno);
            iqpApptSelfMgrMapper.insertSelective(iqpApptSelfMgr);
        } else {
            //更新
            String serno = iqpApptSelfMgrMapper.selectByApptCode(apptCode).getSerno();
            iqpApptSelfMgr.setSerno(serno);
            iqpApptSelfMgrMapper.updateByPrimaryKeySelective(iqpApptSelfMgr);
        }

    }

    /**
     * 根据  相关人编号 删除 相关人详细信息
     * 1.删除 单位信息
     * 2.删除关系表信息
     * 3.删除 主申请表信息
     * @param apptCode
     */
    public void deleteIqpApptOtherDetailByApptCode(String apptCode, String iqpSerno) {
        iqpApptCompanyMapper.deleteByApptCode(apptCode);
        iqpApplApptRelMapper.deleteByIqpSernoAndApptCode(iqpSerno,apptCode);
        iqpApplApptMapper.deleteByPrimaryKey(apptCode);
    }

    /**
     * 获取资产汇总信息
     * @param iqpSerno
     * @return
     */
    public List<HashMap<String, String>> getSumInfo(String iqpSerno) {
        return iqpApplApptMapper.getSumInfo(iqpSerno);
    }

    public List<IqpApplAppt> selectByIqpSerno(String iqpSernoOld) {
        return iqpApplApptMapper.selectByIqpSerno(iqpSernoOld);
    }
}
