/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpChgTrupayAcctApp;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.dto.server.xdht0040.req.Xdht0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0041.req.Xdht0041DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpChgTrupayAcctAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 屈文
 * @创建时间: 2021-04-14 20:06:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpChgTrupayAcctAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpChgTrupayAcctApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpChgTrupayAcctApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpChgTrupayAcctApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpChgTrupayAcctApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpChgTrupayAcctApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpChgTrupayAcctApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * 查询受托记录状态
     * @param xdht0041DataReqDto
     * @return
     */
    int getXdht0041(Xdht0041DataReqDto xdht0041DataReqDto);

    /**
     * @方法名称: updateIqpChgTrupayAcctAppBySerno
     * @方法描述: 修改受托信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateIqpChgTrupayAcctAppBySerno(Xdht0040DataReqDto xdht0040DataReqDto);

    /**
     * 查询审批通过数量
     * @param xdht0041DataReqDto
     * @return
     */
    int getApprPassNum(Xdht0041DataReqDto xdht0041DataReqDto);

    /**
     * @方法名称: selectByCvrgSernoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    // IqpChgTrupayAcctApp selectByIqpChgTrupayAcctHxSerno(String serno);

    /**
     * @方法名称: selectByIqpChgTrupayAcctAppSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    IqpChgTrupayAcctApp selectByIqpChgTrupayAcctAppSerno(String serno);

    /**
     * @Description:根据客户id或者客户账号查询受托信息
     * @Author: YX-WJ
     * @Date: 2021/6/5 0:05
     * @param cusId: 客户id
     * @param cusAcctNo: 客户账号
     * @return: cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto
     **/
    java.util.List<cn.com.yusys.yusp.dto.server.xdtz0055.resp.List> selectByCusIdOrCusAcctNo(@Param("cusId")String cusId, @Param("cusAcctNo")String cusAcctNo);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    IqpChgTrupayAcctApp selectBySerno(String serno);

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 根据流水号更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatus(@Param("iqpSerno") String iqpSerno, @Param("approveStatus") String approveStatus);

    /**
     * @创建人 YD
     * @创建时间 2021/6/9 20:36
     * @注释 查询退回记录
     */
     int countIqpChgTrupayAcctAppBySerno(Map QueryMap);

}