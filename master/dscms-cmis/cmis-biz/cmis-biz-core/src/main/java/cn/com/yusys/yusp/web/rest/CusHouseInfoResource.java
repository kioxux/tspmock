/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusHouseInfo;
import cn.com.yusys.yusp.service.CusHouseInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusHouseInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cushouseinfo")
public class CusHouseInfoResource {
    @Autowired
    private CusHouseInfoService cusHouseInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusHouseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusHouseInfo> list = cusHouseInfoService.selectAll(queryModel);
        return new ResultDto<List<CusHouseInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusHouseInfo>> index(QueryModel queryModel) {
        List<CusHouseInfo> list = cusHouseInfoService.selectByModel(queryModel);
        return new ResultDto<List<CusHouseInfo>>(list);
    }

    /**
     * @函数名称:queryCusHouseInfo
     * @函数描述:房抵e点贷房抵e点贷房产信息分页查询
     * @参数与返回说明:queryModel
     * @创建人:zhangliang15
     */
    @ApiOperation("房抵e点贷房抵e点贷房产信息分页查询")
    @PostMapping("/queryCusHouseInfo")
    protected ResultDto<List<CusHouseInfo>> queryCusHouseInfo(@RequestBody QueryModel queryModel) {
        List<CusHouseInfo> list = cusHouseInfoService.queryCusHouseInfo(queryModel);
        return new ResultDto<List<CusHouseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusHouseInfo> show(@PathVariable("serno") String serno) {
        CusHouseInfo cusHouseInfo = cusHouseInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CusHouseInfo>(cusHouseInfo);
    }

    /**
     * @方法名称: queryCusHouseInfoByParams
     * @方法描述: 根据入参查询房屋信息数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询数据")
    @PostMapping("/queryCusHouseInfoByParams")
    protected ResultDto<CusHouseInfo> queryCusHouseInfoByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        CusHouseInfo cusHouseInfo = cusHouseInfoService.queryCusHouseInfoByParams(queryData);
        return new ResultDto<CusHouseInfo>(cusHouseInfo);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusHouseInfo> create(@RequestBody CusHouseInfo cusHouseInfo) throws URISyntaxException {
        cusHouseInfoService.insert(cusHouseInfo);
        return new ResultDto<CusHouseInfo>(cusHouseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusHouseInfo cusHouseInfo) throws URISyntaxException {
        int result = cusHouseInfoService.update(cusHouseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusHouseInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusHouseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
