/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysDbrPersonOper;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysDbrPersonOperMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysDbrPersonOperService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-24 11:17:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysDbrPersonOperService {

    @Autowired
    private RptSpdAnysDbrPersonOperMapper rptSpdAnysDbrPersonOperMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptSpdAnysDbrPersonOper selectByPrimaryKey(String serno) {
        return rptSpdAnysDbrPersonOperMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptSpdAnysDbrPersonOper> selectAll(QueryModel model) {
        List<RptSpdAnysDbrPersonOper> records = (List<RptSpdAnysDbrPersonOper>) rptSpdAnysDbrPersonOperMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptSpdAnysDbrPersonOper> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysDbrPersonOper> list = rptSpdAnysDbrPersonOperMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptSpdAnysDbrPersonOper record) {
        return rptSpdAnysDbrPersonOperMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysDbrPersonOper record) {
        return rptSpdAnysDbrPersonOperMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptSpdAnysDbrPersonOper record) {
        return rptSpdAnysDbrPersonOperMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysDbrPersonOper record) {
        return rptSpdAnysDbrPersonOperMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptSpdAnysDbrPersonOperMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysDbrPersonOperMapper.deleteByIds(ids);
    }

    public int save(RptSpdAnysDbrPersonOper rptSpdAnysDbrPersonOper){
        RptSpdAnysDbrPersonOper temp = rptSpdAnysDbrPersonOperMapper.selectByPrimaryKey(rptSpdAnysDbrPersonOper.getSerno());
        if(Objects.nonNull(temp)){
            return update(rptSpdAnysDbrPersonOper);
        }else {
            return insert(rptSpdAnysDbrPersonOper);
        }
    }
}
