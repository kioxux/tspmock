package cn.com.yusys.yusp.service.server.xdxw0070;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0070.req.Xdxw0070DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0070.resp.Xdxw0070DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyProfileInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:授信侧面调查信息查询
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdxw0070Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0070Service.class);
    @Autowired
    private LmtSurveyProfileInfoMapper lmtSurveyProfileInfoMapper;

    /**
     * 授信侧面调查信息查询
     *
     * @param Xdxw0070DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0070DataRespDto xdxw0070(Xdxw0070DataReqDto Xdxw0070DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value);
        //返回信息
        Xdxw0070DataRespDto Xdxw0070DataRespDto = new Xdxw0070DataRespDto();
        try {
            //请求字段
            String certCode = Xdxw0070DataReqDto.getCert_code();//证件号
            String prdName = Xdxw0070DataReqDto.getPrd_name();//产品名称
            String prdId = Xdxw0070DataReqDto.getPrd_code();//产品编号
            String approveStatus = Xdxw0070DataReqDto.getApprove_status();//审批状态
            String surveySerno = Xdxw0070DataReqDto.getSurvey_serno();//流水号

            //根据客户调查表编号授信侧面调查信息查询
            Map queryMap = new HashMap();
            queryMap.put("certCode", certCode);
            queryMap.put("prdName", prdName);
            queryMap.put("prdId", prdId);
            queryMap.put("approveStatus", approveStatus);
            queryMap.put("surveySerno", surveySerno);

            //查询授信调查侧面信息开始
            logger.info("***********查询授信调查侧面信息开始,查询参数为:{}", JSON.toJSONString(queryMap));
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0070.resp.List> list = lmtSurveyProfileInfoMapper.queryLmtSurveyProfileInfoBymap(queryMap);
            logger.info("***********查询授信调查侧面信息结束,返回结果为:{}", JSON.toJSONString(list));
            //处理返回信息
            Xdxw0070DataRespDto.setList(list);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value);
        return Xdxw0070DataRespDto;
    }
}
