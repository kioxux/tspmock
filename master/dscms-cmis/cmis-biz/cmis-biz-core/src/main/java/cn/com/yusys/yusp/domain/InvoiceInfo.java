/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: InvoiceInfo
 * @类描述: invoice_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 22:43:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "invoice_info")
@JsonPropertyOrder(alphabetic = true)
public class InvoiceInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 补录流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	@JsonProperty(value = "serno")
	private String serno;
	
	/** 发票类型 **/
	@Column(name = "BILL_TYPE", unique = false, nullable = true, length = 5)
	@JsonProperty(value = "billType")
	private String billType;
	
	/** 购销合同编号 **/
	@Column(name = "T_CONT_NO", unique = false, nullable = false, length = 40)
	@JsonProperty(value = "tContNo")
	private String tContNo;
	
	/** 发票影像流水号 **/
	@Column(name = "BILL_IMG_ID", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "billImgId")
	private String billImgId;
	
	/** 发票是否补录 **/
	@Column(name = "IS_ADDP_RECO", unique = false, nullable = true, length = 5)
	@JsonProperty(value = "isAddpReco")
	private String isAddpReco;
	
	/** 开票日期 **/
	@Column(name = "OPEN_DATE", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "openDate")
	private String openDate;
	
	/** 票号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "billNo")
	private String billNo;
	
	/** 购买方名称 **/
	@Column(name = "BUY_BR_ID", unique = false, nullable = true, length = 80)
	@JsonProperty(value = "buyBrId")
	private String buyBrId;
	
	/** 购买方纳税人识别号 **/
	@Column(name = "BUY_TAX_NO", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "buyTaxNo")
	private String buyTaxNo;
	
	/** 购买方地址 **/
	@Column(name = "BUY_ADDR", unique = false, nullable = true, length = 500)
	@JsonProperty(value = "buyAddr")
	private String buyAddr;
	
	/** 购买方电话 **/
	@Column(name = "BUY_PHONE", unique = false, nullable = true, length = 32)
	@JsonProperty(value = "buyPhone")
	private String buyPhone;
	
	/** 购买方开户行 **/
	@Column(name = "BUY_BANK", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "buyBank")
	private String buyBank;
	
	/** 购买方账号 **/
	@Column(name = "BUY_ACC", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "buyAcc")
	private String buyAcc;
	
	/** 产品的名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 200)
	@JsonProperty(value = "prdName")
	private String prdName;
	
	/** 规格型号 **/
	@Column(name = "NORMS", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "norms")
	private String norms;
	
	/** 数量 **/
	@Column(name = "QUANT", unique = false, nullable = true, length = 16)
	@JsonProperty(value = "quant")
	private java.math.BigDecimal quant;
	
	/** 单价 **/
	@Column(name = "PRICE", unique = false, nullable = true, length = 16)
	@JsonProperty(value = "price")
	private java.math.BigDecimal price;
	
	/** 金额 **/
	@Column(name = "AMT", unique = false, nullable = true, length = 16)
	@JsonProperty(value = "amt")
	private java.math.BigDecimal amt;
	
	/** 税率 **/
	@Column(name = "TAX_RATE", unique = false, nullable = true, length = 16)
	@JsonProperty(value = "taxRate")
	private java.math.BigDecimal taxRate;
	
	/** 税额 **/
	@Column(name = "TAX_AMT", unique = false, nullable = true, length = 16)
	@JsonProperty(value = "taxAmt")
	private java.math.BigDecimal taxAmt;
	
	/** 销售方名称 **/
	@Column(name = "SELL_BR_ID", unique = false, nullable = true, length = 80)
	@JsonProperty(value = "sellBrId")
	private String sellBrId;
	
	/** 销售纳税人识别号 **/
	@Column(name = "SELL_TAX_NO", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "sellTaxNo")
	private String sellTaxNo;
	
	/** 销售地址 **/
	@Column(name = "SELL_ADDR", unique = false, nullable = true, length = 500)
	@JsonProperty(value = "sellAddr")
	private String sellAddr;
	
	/** 销售电话 **/
	@Column(name = "SELL_PHONE", unique = false, nullable = true, length = 32)
	@JsonProperty(value = "sellPhone")
	private String sellPhone;
	
	/** 销售开户行 **/
	@Column(name = "SELL_BANK", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "sellBank")
	private String sellBank;
	
	/** 销售账号 **/
	@Column(name = "SELL_ACC", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "sellAcc")
	private String sellAcc;
	
	/** 收款人 **/
	@Column(name = "PAYEE", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "payee")
	private String payee;
	
	/** 复核人 **/
	@Column(name = "CHECK_NAME", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "checkName")
	private String checkName;
	
	/** 开票人 **/
	@Column(name = "OPEN_NAME", unique = false, nullable = true, length = 40)
	@JsonProperty(value = "openName")
	private String openName;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	@JsonProperty(value = "remark")
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "inputId")
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "inputBrId")
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "inputDate")
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "updId")
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "updBrId")
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "updDate")
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "managerId")
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	@JsonProperty(value = "managerBrId")
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	@JsonProperty(value = "createTime")
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	@JsonProperty(value = "updateTime")
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billType
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}
	
    /**
     * @return billType
     */
	public String getBillType() {
		return this.billType;
	}
	
	/**
	 * @param tContNo
	 */
	public void setTContNo(String tContNo) {
		this.tContNo = tContNo;
	}
	
    /**
     * @return tContNo
     */
	public String getTContNo() {
		return this.tContNo;
	}
	
	/**
	 * @param billImgId
	 */
	public void setBillImgId(String billImgId) {
		this.billImgId = billImgId;
	}
	
    /**
     * @return billImgId
     */
	public String getBillImgId() {
		return this.billImgId;
	}
	
	/**
	 * @param isAddpReco
	 */
	public void setIsAddpReco(String isAddpReco) {
		this.isAddpReco = isAddpReco;
	}
	
    /**
     * @return isAddpReco
     */
	public String getIsAddpReco() {
		return this.isAddpReco;
	}
	
	/**
	 * @param openDate
	 */
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	
    /**
     * @return openDate
     */
	public String getOpenDate() {
		return this.openDate;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param buyBrId
	 */
	public void setBuyBrId(String buyBrId) {
		this.buyBrId = buyBrId;
	}
	
    /**
     * @return buyBrId
     */
	public String getBuyBrId() {
		return this.buyBrId;
	}
	
	/**
	 * @param buyTaxNo
	 */
	public void setBuyTaxNo(String buyTaxNo) {
		this.buyTaxNo = buyTaxNo;
	}
	
    /**
     * @return buyTaxNo
     */
	public String getBuyTaxNo() {
		return this.buyTaxNo;
	}
	
	/**
	 * @param buyAddr
	 */
	public void setBuyAddr(String buyAddr) {
		this.buyAddr = buyAddr;
	}
	
    /**
     * @return buyAddr
     */
	public String getBuyAddr() {
		return this.buyAddr;
	}
	
	/**
	 * @param buyPhone
	 */
	public void setBuyPhone(String buyPhone) {
		this.buyPhone = buyPhone;
	}
	
    /**
     * @return buyPhone
     */
	public String getBuyPhone() {
		return this.buyPhone;
	}
	
	/**
	 * @param buyBank
	 */
	public void setBuyBank(String buyBank) {
		this.buyBank = buyBank;
	}
	
    /**
     * @return buyBank
     */
	public String getBuyBank() {
		return this.buyBank;
	}
	
	/**
	 * @param buyAcc
	 */
	public void setBuyAcc(String buyAcc) {
		this.buyAcc = buyAcc;
	}
	
    /**
     * @return buyAcc
     */
	public String getBuyAcc() {
		return this.buyAcc;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param norms
	 */
	public void setNorms(String norms) {
		this.norms = norms;
	}
	
    /**
     * @return norms
     */
	public String getNorms() {
		return this.norms;
	}
	
	/**
	 * @param quant
	 */
	public void setQuant(java.math.BigDecimal quant) {
		this.quant = quant;
	}
	
    /**
     * @return quant
     */
	public java.math.BigDecimal getQuant() {
		return this.quant;
	}
	
	/**
	 * @param price
	 */
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}
	
    /**
     * @return price
     */
	public java.math.BigDecimal getPrice() {
		return this.price;
	}
	
	/**
	 * @param amt
	 */
	public void setAmt(java.math.BigDecimal amt) {
		this.amt = amt;
	}
	
    /**
     * @return amt
     */
	public java.math.BigDecimal getAmt() {
		return this.amt;
	}
	
	/**
	 * @param taxRate
	 */
	public void setTaxRate(java.math.BigDecimal taxRate) {
		this.taxRate = taxRate;
	}
	
    /**
     * @return taxRate
     */
	public java.math.BigDecimal getTaxRate() {
		return this.taxRate;
	}
	
	/**
	 * @param taxAmt
	 */
	public void setTaxAmt(java.math.BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}
	
    /**
     * @return taxAmt
     */
	public java.math.BigDecimal getTaxAmt() {
		return this.taxAmt;
	}
	
	/**
	 * @param sellBrId
	 */
	public void setSellBrId(String sellBrId) {
		this.sellBrId = sellBrId;
	}
	
    /**
     * @return sellBrId
     */
	public String getSellBrId() {
		return this.sellBrId;
	}
	
	/**
	 * @param sellTaxNo
	 */
	public void setSellTaxNo(String sellTaxNo) {
		this.sellTaxNo = sellTaxNo;
	}
	
    /**
     * @return sellTaxNo
     */
	public String getSellTaxNo() {
		return this.sellTaxNo;
	}
	
	/**
	 * @param sellAddr
	 */
	public void setSellAddr(String sellAddr) {
		this.sellAddr = sellAddr;
	}
	
    /**
     * @return sellAddr
     */
	public String getSellAddr() {
		return this.sellAddr;
	}
	
	/**
	 * @param sellPhone
	 */
	public void setSellPhone(String sellPhone) {
		this.sellPhone = sellPhone;
	}
	
    /**
     * @return sellPhone
     */
	public String getSellPhone() {
		return this.sellPhone;
	}
	
	/**
	 * @param sellBank
	 */
	public void setSellBank(String sellBank) {
		this.sellBank = sellBank;
	}
	
    /**
     * @return sellBank
     */
	public String getSellBank() {
		return this.sellBank;
	}
	
	/**
	 * @param sellAcc
	 */
	public void setSellAcc(String sellAcc) {
		this.sellAcc = sellAcc;
	}
	
    /**
     * @return sellAcc
     */
	public String getSellAcc() {
		return this.sellAcc;
	}
	
	/**
	 * @param payee
	 */
	public void setPayee(String payee) {
		this.payee = payee;
	}
	
    /**
     * @return payee
     */
	public String getPayee() {
		return this.payee;
	}
	
	/**
	 * @param checkName
	 */
	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}
	
    /**
     * @return checkName
     */
	public String getCheckName() {
		return this.checkName;
	}
	
	/**
	 * @param openName
	 */
	public void setOpenName(String openName) {
		this.openName = openName;
	}
	
    /**
     * @return openName
     */
	public String getOpenName() {
		return this.openName;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}