/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.data.authority.annotation.IgnoredDataAuthority;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfoAndCrd;
import cn.com.yusys.yusp.dto.server.xdxw0007.resp.Xdxw0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.req.Xdxw0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.resp.Xdxw0057DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0065.req.Xdxw0065DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-25 13:58:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCrdReplyInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtCrdReplyInfo selectByPrimaryKey(@Param("replySerno") String replySerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtCrdReplyInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtCrdReplyInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtCrdReplyInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtCrdReplyInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtCrdReplyInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("replySerno") String replySerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 批复信息查询
     * @param contNo
     * @return
     */
    Xdxw0007DataRespDto queryByContNo(String contNo);

    /**
     * 根据核心客户号查询经营性贷款批复额度
     * @param xdxw0057DataReqDto
     * @return
     */
    Xdxw0057DataRespDto getLmtByQueryType(Xdxw0057DataReqDto xdxw0057DataReqDto);

    /**
     * 调查报告审批结果信息查询
     * @param xdxw0065DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0065.resp.List> getLmtReplyInfoList(Xdxw0065DataReqDto xdxw0065DataReqDto);

    /**
     * @创建人 WH
     * @创建时间 2021/5/31 20:33
     * @注释 通过授信流水号修改批复数据
     */
    int updateSurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * @创建人 WH
     * @创建时间 2021/6/3 15:35
     * @注释 通过调查流水号查批复信息
     */
    LmtCrdReplyInfo selectBySurveySerno(@Param("surveySerno")String surveySerno);

    /**
     * @创建人 SCN
     * @创建时间 2021/6/4 14:00
     * @注释 通过客户号和调查流水号处理新信贷数据 为无效
     */
    int updateByCusIdSurveySeqNo(@Param("cusId")String cusId, @Param("surveySerno")String surveySerno);

    /**
     * @创建人 SCN
     * @创建时间 2021/6/4 14:00
     * @注释 通过客户号和调查流水号查询台账编号
     */
    List<String> selectReplySernoByCusIdSurveySeqNo(@Param("cusId")String cusId, @Param("surveySerno")String serno);

    /**
     * @创建人
     * @创建时间 2021/11/20 14:00
     * @注释 查询是否存在有效的授信批复
     */
    int selectLmtCrdReplySernoByCusId(@Param("cusId")String cusId);

    /**
     * @创建人 sunzhen
     * @创建时间 2021/6/9 23:02
     * @注释 根据调查编号删除
     */
    int deleteBySurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * @创建人 leezonw
     * @创建时间 2021/6/10 20:44
     * @注释 根据证件号和日期查询最近的一笔授信决议
     */
    LmtCrdReplyInfo selectLastApprovalResult(Map map);

    /**
     * @创建人 zdl
     * @创建时间 2021/10/19 14:44
     * @注释 根据调查流水号获取
     */
    @IgnoredDataAuthority
    List<LmtSurveyReportMainInfoAndCrd> findBySurveySernos(@Param("surveySernos")List<String > surveySernos);
   
}