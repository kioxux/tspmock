package cn.com.yusys.yusp.web.server.xdzc0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0011.req.Xdzc0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0011.resp.Xdzc0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0011.Xdzc0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池超短贷放款接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0011:资产池超短贷放款接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0011Resource.class);

    @Autowired
    private Xdzc0011Service xdzc0011Service;
  /**
     * 交易码：xdzc0011
     * 交易描述：资产池超短贷放款接口
     *
     * @param xdzc0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池超短贷放款接口")
    @PostMapping("/xdzc0011")
    protected @ResponseBody
    ResultDto<Xdzc0011DataRespDto> xdzc0011(@Validated @RequestBody Xdzc0011DataReqDto xdzc0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value, JSON.toJSONString(xdzc0011DataReqDto));
        Xdzc0011DataRespDto xdzc0011DataRespDto = new Xdzc0011DataRespDto();// 响应Dto:资产池超短贷放款接口
        ResultDto<Xdzc0011DataRespDto> xdzc0011DataResultDto = new ResultDto<>();
        try {
            xdzc0011DataRespDto = xdzc0011Service.xdzc0011Service(xdzc0011DataReqDto);
            xdzc0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value, e.getMessage());
            // 封装xdzc0011DataResultDto中异常返回码和返回信息
            xdzc0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0011DataRespDto到xdzc0011DataResultDto中
        xdzc0011DataResultDto.setData(xdzc0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0011.key, DscmsEnum.TRADE_CODE_XDZC0011.value, JSON.toJSONString(xdzc0011DataResultDto));
        return xdzc0011DataResultDto;
    }
}
