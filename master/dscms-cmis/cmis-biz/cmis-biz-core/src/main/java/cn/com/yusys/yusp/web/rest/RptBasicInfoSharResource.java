/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoShar;
import cn.com.yusys.yusp.service.RptBasicInfoSharService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoSharResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-10 15:46:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinfoshar")
public class RptBasicInfoSharResource {
    @Autowired
    private RptBasicInfoSharService rptBasicInfoSharService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoShar>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoShar> list = rptBasicInfoSharService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoShar>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoShar>> index(QueryModel queryModel) {
        List<RptBasicInfoShar> list = rptBasicInfoSharService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoShar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptBasicInfoShar> show(@PathVariable("pkId") String pkId) {
        RptBasicInfoShar rptBasicInfoShar = rptBasicInfoSharService.selectByPrimaryKey(pkId);
        return new ResultDto<RptBasicInfoShar>(rptBasicInfoShar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoShar> create(@RequestBody RptBasicInfoShar rptBasicInfoShar) throws URISyntaxException {
        rptBasicInfoSharService.insert(rptBasicInfoShar);
        return new ResultDto<RptBasicInfoShar>(rptBasicInfoShar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoShar rptBasicInfoShar) throws URISyntaxException {
        int result = rptBasicInfoSharService.update(rptBasicInfoShar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoSharService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoSharService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptBasicInfoShar>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptBasicInfoShar>>(rptBasicInfoSharService.selectByModel(model));
    }
    /**
     * 引入股东数据
     */
    @PostMapping("/initShar")
    protected ResultDto<List<RptBasicInfoShar>> initShar(@RequestBody QueryModel model){
        return new ResultDto<List<RptBasicInfoShar>>(rptBasicInfoSharService.initShar(model));
    }

    @PostMapping("/deleteShar")
    protected ResultDto<Integer> delectShar(@RequestBody RptBasicInfoShar rptBasicInfoShar){
        return new ResultDto<Integer>(rptBasicInfoSharService.deleteByPrimaryKey(rptBasicInfoShar.getPkId()));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptBasicInfoShar rptBasicInfoShar){
        return new ResultDto<Integer>(rptBasicInfoSharService.save(rptBasicInfoShar));
    }
}
