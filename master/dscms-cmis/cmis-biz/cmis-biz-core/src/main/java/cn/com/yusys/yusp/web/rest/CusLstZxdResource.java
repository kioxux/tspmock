/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.BizCusLstZxdDto;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.BeanUtils;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstZxd;
import cn.com.yusys.yusp.service.CusLstZxdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstZxdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-26 20:11:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstzxd")
public class CusLstZxdResource {

    private static final Logger log = LoggerFactory.getLogger(CusLstZxdResource.class);

    @Autowired
    private CusLstZxdService cusLstZxdService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstZxd>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstZxd> list = cusLstZxdService.selectAll(queryModel);
        return new ResultDto<List<CusLstZxd>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstZxd>> index(@RequestBody QueryModel queryModel) {
        List<CusLstZxd> list = cusLstZxdService.selectByModel(queryModel);
        return new ResultDto<List<CusLstZxd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querybyserno/{serno}")
    protected ResultDto<CusLstZxd> show(@PathVariable("serno") String serno) {
        CusLstZxd cusLstZxd = cusLstZxdService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstZxd>(cusLstZxd);
    }

    /**
     * 根据 serno查询CusLstZxd
     * @author shangzy
     * @param serno
     * @return BizCusLstZxdDto
     */
    @PostMapping("/selectbyprimarykey")
    protected ResultDto<BizCusLstZxdDto> selectbyprimarykey(@RequestBody String serno) {
        CusLstZxd cusLstZxd = cusLstZxdService.selectByPrimaryKey(serno);
        BizCusLstZxdDto bizCusLstZxdDto = new BizCusLstZxdDto();
        try {
            BeanUtils.copyProperties(cusLstZxd, bizCusLstZxdDto);
        } catch (BeansException e) {
            log.info(e.getMessage());
        }
        return new ResultDto<BizCusLstZxdDto>(bizCusLstZxdDto);
    }

    /**
     * 更新增享贷信息
     * @author shangzy
     * @param bizCusLstZxdDto
     * @return BizCusLstZxdDto
     */
    @PostMapping("/updatebyprimarykeyselective")
    protected ResultDto<Integer> updatebyprimarykeyselective(@RequestBody BizCusLstZxdDto bizCusLstZxdDto) {
        CusLstZxd cusLstZxd = new CusLstZxd();
        try {
            BeanUtils.copyProperties(bizCusLstZxdDto, cusLstZxd);
        } catch (BeansException e) {
            log.info(e.getMessage());
        }
        int i = cusLstZxdService.updateSelective(cusLstZxd);
        return new ResultDto<Integer>(i);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstZxd> create(@RequestBody CusLstZxd cusLstZxd) throws URISyntaxException {
        cusLstZxdService.insert(cusLstZxd);
        return new ResultDto<CusLstZxd>(cusLstZxd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstZxd cusLstZxd) throws URISyntaxException {
        int result = cusLstZxdService.update(cusLstZxd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstZxdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstZxdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/8/5 14:11
     * @注释 查询增享贷白名单 根据客户号和状态
     */
    @PostMapping("/selectzxdbycusid")
    protected ResultDto<CusLstZxdDto> selectzxdbycusid(@RequestBody CusLstZxdDto cusLstZxdDto){
        CusLstZxdDto  ZxdDto =cusLstZxdService.selectzxdbycusid(cusLstZxdDto);
        return new ResultDto<>(ZxdDto);
    }

    /**
     * @创建人 SZY
     * @创建时间 2021/9/14 20:51
     * @注释 生成调查表
     */
    @PostMapping("/generatesurvey")
    protected ResultDto generatesurvey(@RequestBody CusLstZxdDto cusLstZxdDto){
        return cusLstZxdService.generatesurvey(cusLstZxdDto);
    }

}
