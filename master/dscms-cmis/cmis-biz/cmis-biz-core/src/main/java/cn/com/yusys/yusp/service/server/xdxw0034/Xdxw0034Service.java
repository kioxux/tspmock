package cn.com.yusys.yusp.service.server.xdxw0034;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSurveyConInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.dto.server.xdxw0034.req.Xdxw0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0034.resp.Xdxw0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import cn.com.yusys.yusp.service.LmtSurveyConInfoService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0034Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-18 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional(rollbackFor = {BizException.class, Exception.class})
public class Xdxw0034Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdxw0034.Xdxw0034Service.class);

    @Resource
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;
    @Resource
    private CommonService commonService;
    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    /**
     * 根据流水号查询无还本续贷基本信息
     *
     * @param xdxw0034DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0034DataRespDto xdxw0034(Xdxw0034DataReqDto xdxw0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataReqDto));
        Xdxw0034DataRespDto xdxw0034DataRespDto = new Xdxw0034DataRespDto();

        String indgtSerno = xdxw0034DataReqDto.getIndgtSerno();//调查流水号
        try {
            //查询合同状态
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("surveySerno", indgtSerno);
            logger.info("***********XDXW0034***根据流水号查询无还本续贷基本信息开始,查询参数为:{}", JSON.toJSONString(queryModel));
            List<LmtSurveyReportBasicInfo> lmtSurveyReportBasicInfoList = lmtSurveyReportBasicInfoMapper.selectByModel(queryModel);
            logger.info("**********XDXW0034***根据流水号查询无还本续贷基本信息结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportBasicInfoList));
            // 2021年11月19日14:53:33 hubp 问题编号：20211117-0084
            logger.info("***********XDXW0034***根据流水号查询无还本续贷调查结论信息开始,查询流水号为:{}", indgtSerno);
            LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(indgtSerno);
            logger.info("***********XDXW0034***根据流水号查询无还本续贷调查结论信息开始,返回参数为:{}", JSON.toJSONString(lmtSurveyConInfo));

            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoList.get(0);
            String cus_id = lmtSurveyReportBasicInfo.getCusId();
            xdxw0034DataRespDto.setCusNo(cus_id);// 客户号
            xdxw0034DataRespDto.setCusName(lmtSurveyReportBasicInfo.getCusName());// 客户名称
            xdxw0034DataRespDto.setCertNo(lmtSurveyReportBasicInfo.getCertCode());// 客户证件号
            xdxw0034DataRespDto.setAssureMeans(lmtSurveyReportBasicInfo.getGuarMode());// 担保方式
            xdxw0034DataRespDto.setRepayType(lmtSurveyReportBasicInfo.getRepayMode());// 还款方式
            xdxw0034DataRespDto.setLoanTerm(lmtSurveyConInfo.getAdviceTerm());// 贷款期限
            xdxw0034DataRespDto.setSpouseName(lmtSurveyReportBasicInfo.getSpouseName());// 配偶姓名
            xdxw0034DataRespDto.setApprId(StringUtils.EMPTY);// 审批人工号
            xdxw0034DataRespDto.setRiskMsg(StringUtils.EMPTY);// 风险提示
            xdxw0034DataRespDto.setPhone(lmtSurveyReportBasicInfo.getPhone());// 手机号
            xdxw0034DataRespDto.setSpousePhone(lmtSurveyReportBasicInfo.getSpousePhone());// 配偶手机号
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataReqDto));
        return xdxw0034DataRespDto;
    }
}
