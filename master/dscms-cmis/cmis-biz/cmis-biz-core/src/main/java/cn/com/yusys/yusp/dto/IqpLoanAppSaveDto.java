package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.*;

import java.io.Serializable;

/**
 * @className IqpLoanAppSaveDto
 * @Description 单笔单批业务保存dto
 * @Date 2020/12/3 : 17:11
 */

public class IqpLoanAppSaveDto implements Serializable {
    /*基础信息*/
    private IqpLoanAppBaseDto iqpLoanAppBaseDto;
    /*辅助信息*/
    private IqpLoanAppAssistDto iqpLoanAppAssistDto;
    /*还款信息*/
    private IqpLoanAppRepay iqpLoanAppRepay;
    /*授信/第三方 关联记录表*/
    private IqpLmtRelDto iqpLmtRelDto;
    /*借据关联记录表*/
    private IqpBillRel iqpBillRel;

    //操作标识位，用于进行特殊判断
    private String opFlag;

    //借据编号集合
    private String billNos;

    public String getBillNos() {
        return billNos;
    }

    public void setBillNos(String billNos) {
        this.billNos = billNos;
    }

    public IqpLoanAppBaseDto getIqpLoanAppBaseDto() {
        return iqpLoanAppBaseDto;
    }

    public void setIqpLoanAppBaseDto(IqpLoanAppBaseDto iqpLoanAppBaseDto) {
        this.iqpLoanAppBaseDto = iqpLoanAppBaseDto;
    }

    public IqpLoanAppAssistDto getIqpLoanAppAssistDto() {
        return iqpLoanAppAssistDto;
    }

    public void setIqpLoanAppAssistDto(IqpLoanAppAssistDto iqpLoanAppAssistDto) {
        this.iqpLoanAppAssistDto = iqpLoanAppAssistDto;
    }

    public IqpLoanAppRepay getIqpLoanAppRepay() {
        return iqpLoanAppRepay;
    }

    public void setIqpLoanAppRepay(IqpLoanAppRepay iqpLoanAppRepay) {
        this.iqpLoanAppRepay = iqpLoanAppRepay;
    }

    public IqpLmtRelDto getIqpLmtRelDto() { return iqpLmtRelDto; }
    public void setIqpLmtRelDto(IqpLmtRelDto iqpLmtRelDto) { this.iqpLmtRelDto = iqpLmtRelDto; }

    public IqpBillRel getIqpBillRel() { return iqpBillRel; }
    public void setIqpBillRel(IqpBillRel iqpBillRel) { this.iqpBillRel = iqpBillRel; }

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }
}
