package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.domain.bat.BatBizRiskSign;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.bat.BatBizRiskSignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0101Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0101Service.class);

    @Autowired
    private CoopPlanAppService coopPlanAppService;

    @Autowired
    private BatBizRiskSignService batBizRiskSignService;
    /**
     * @方法名称: riskItem0101
     * @方法描述: 合作方预警信息校验
     * @参数与返回说明:
     * @算法描述:
     * 1、合作方存在预警信号的，提交合作方准入申请时，作出提示拦截
     * @创建人: yfs
     * @创建时间: 2021-08-26 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0101(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("合作方预警信息校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        if(Objects.equals("HZ001",bizType) || Objects.equals("HZ002",bizType) || Objects.equals("HZ003",bizType) || Objects.equals("HZ004",bizType)) {
            CoopPlanApp coopPlanApp = coopPlanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(coopPlanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10101);
                return riskResultDto;
            }
            QueryModel model = new QueryModel();
            model.addCondition("custId",coopPlanApp.getPartnerNo());
            List<BatBizRiskSign> list =  batBizRiskSignService.selectByModel(model);
            if(CollectionUtils.nonEmpty(list)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10102);
                return riskResultDto;
            }

        }
        log.info("合作方预警信息校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
