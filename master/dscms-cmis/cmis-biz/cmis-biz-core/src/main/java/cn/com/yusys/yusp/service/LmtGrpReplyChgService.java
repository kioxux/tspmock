/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyChgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-21 14:02:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpReplyChgService {

    private final Logger log = LoggerFactory.getLogger(LmtReplyChgService.class);

    @Autowired
    private LmtGrpReplyChgMapper lmtGrpReplyChgMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtReplyChgSubPrdService lmtReplyChgSubPrdService;

    @Autowired
    private LmtReplyChgSubService lmtReplyChgSubService;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtReplyChgCondService lmtReplyChgCondService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtReplyChgService lmtReplyChgService;

    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;

    // 流程自行退出服务
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtGrpReplyChg selectByPrimaryKey(String pkId) {
        return lmtGrpReplyChgMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpReplyChg> selectAll(QueryModel model) {
        List<LmtGrpReplyChg> records = (List<LmtGrpReplyChg>) lmtGrpReplyChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtGrpReplyChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpReplyChg> list = lmtGrpReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtGrpReplyChg record) {
        record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        return lmtGrpReplyChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpReplyChg record) {
        return lmtGrpReplyChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtGrpReplyChg record) {
        return lmtGrpReplyChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpReplyChg record) {
        return lmtGrpReplyChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpReplyChgMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpReplyChgMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpReplyChg> queryAll(QueryModel model) {
        List<LmtGrpReplyChg> records = (List<LmtGrpReplyChg>) lmtGrpReplyChgMapper.queryAll(model);
        return records;
    }

    /**
     * @方法名称: queryHis
     * @方法描述: 查询历史
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpReplyChg> queryHis(QueryModel model) {
        List<LmtGrpReplyChg> records = (List<LmtGrpReplyChg>) lmtGrpReplyChgMapper.queryHis(model);
        return records;
    }

    /**
     * @方法名称: insert
     * @方法描述: 集团批复变更向导下一步插入
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：ywl
     */
    @Transactional(rollbackFor = Exception.class)
    public LmtGrpReplyChg insertLmtGrpReplyChg(LmtGrpReplyChg lmtGrpReplyChg) {
        log.info("集团授信批复变更新增逻辑开始,流水号[{}]", lmtGrpReplyChg.getGrpReplySerno());
        // 批复变更流水号
        String serno = "";

        // 校验参数
        if(StringUtils.isBlank(lmtGrpReplyChg.getGrpReplySerno())){
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        // 校验当前批复编号是否存在在途变更
        if(checkIsAppringLmtGrpReplyChg(lmtGrpReplyChg.getGrpReplySerno())){
            throw BizException.error(null, EcbEnum.ECB010085.key, EcbEnum.ECB010085.value);
        }

        log.info("集团客户授信批复变更新增逻辑开始,批复编号为[{}]" , lmtGrpReplyChg.getGrpReplySerno());
        serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_CHG_SERNO, new HashMap<>());
        if(StringUtils.isBlank(serno)){
            throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
        }
        log.info("集团客户授信批复变更新增逻辑开始,生成流水号为[{}]" , lmtGrpReplyChg.getGrpReplySerno());
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByGrpReplySerno(lmtGrpReplyChg.getGrpReplySerno());

        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null){
            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
        }

        BeanUtils.copyProperties(lmtGrpReplyChg, lmtGrpReply);
        lmtGrpReplyChg.setPkId(UUID.randomUUID().toString());
        lmtGrpReplyChg.setGrpSerno(serno);
        lmtGrpReplyChg.setApproveStatus(CmisFlowConstants.WF_STATUS_000);
        lmtGrpReplyChg.setInputId(userInfo.getLoginCode());
        lmtGrpReplyChg.setInputBrId(userInfo.getOrg().getCode());
        lmtGrpReplyChg.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtGrpReplyChg.setUpdId(userInfo.getLoginCode());
        lmtGrpReplyChg.setUpdBrId(userInfo.getOrg().getCode());
        lmtGrpReplyChg.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtGrpReplyChg.setManagerId(userInfo.getLoginCode());
        lmtGrpReplyChg.setManagerBrId(userInfo.getOrg().getCode());
        lmtGrpReplyChg.setCreateTime(DateUtils.getCurrDate());
        lmtGrpReplyChg.setUpdateTime(DateUtils.getCurrDate());
        this.insert(lmtGrpReplyChg);

        // 去除关于成员客户批复变更的操作 -- 2021-07-22 马顺、周帅
        /*log.info("集团客户授信批复变更新增逻辑开始,插入批复变更数据[{}]" , lmtGrpReplyChg.toString());

        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpReplyChg.getGrpReplySerno());
        log.info("集团客户授信批复变更新增逻辑开始,查询集团批复与单一授信批复关系[{}]" , lmtGrpMemRelList.toString());
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            LmtReplyChg lmtReplyChg = new LmtReplyChg();
            lmtReplyChg.setReplySerno(lmtGrpMemRel.getSingleSerno());
            // String singleSerno = lmtReplyChgService.insertAllReplyChg(lmtReplyChg);
            LmtGrpMemRel newlmtGrpMemRel = new LmtGrpMemRel();
            BeanUtils.copyProperties(lmtGrpMemRel, newlmtGrpMemRel);
            newlmtGrpMemRel.setPkId(UUID.randomUUID().toString());
            newlmtGrpMemRel.setGrpSerno(serno);
            // newlmtGrpMemRel.setSingleSerno(singleSerno);
            lmtGrpMemRelService.insert(newlmtGrpMemRel);
            log.info("集团客户授信批复变更新增逻辑开始,插入集团批复与单一授信批复关系[{}]" , lmtGrpMemRelList.toString());
        }*/
        return lmtGrpReplyChg;
    }

    /**
     * @方法名称: checkIsAppringLmtGrpReplyChg
     * @方法描述: 集团批复变更系只能校验
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：ywl
     */

    private boolean checkIsAppringLmtGrpReplyChg(String grpReplySerno) {
        boolean resBoolean = false;
        HashMap map = new HashMap();
        map.put("grpReplySerno",grpReplySerno);
        map.put("oprType",CmisCommonConstants.OP_TYPE_01);
        map.put("approveStatusS",CmisCommonConstants.WF_STATUS_000992111);
        log.info("集团客户批复变更校验开始，集团批复编号为：" + grpReplySerno);
        List<LmtGrpReplyChg> lmtGrpReplyChgs = lmtGrpReplyChgMapper.queryLmtGrpReplyChgDataByParams(map);
        if(lmtGrpReplyChgs.size() > 0){
            throw BizException.error(null, EcbEnum.ECB010085.key, EcbEnum.ECB010085.value);
        }else{
            resBoolean = false;
        }
        return resBoolean;
    }

    /**
     * @方法名称: queryByGrpserno
     * @方法描述: 根据批复号查询批复变更
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public LmtGrpReplyChg queryByGrpserno(HashMap<String, String> paramMap) throws Exception{
        paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpReplyChg> lmtGrpReplyChgs = lmtGrpReplyChgMapper.queryByGrpserno(paramMap);
        if (lmtGrpReplyChgs != null && lmtGrpReplyChgs.size() > 0) {
            return lmtGrpReplyChgs.get(0);
        } else {
            throw new Exception("查询授信批复变更数据异常");
        }
    }

    /**
     * @方法名称: queryLmtGrpReplyChgDataByGrpSerno
     * @方法描述: 根据流水号查询批复变更
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpReplyChg queryLmtGrpReplyChgDataByGrpSerno(HashMap<String, String> paramMap) throws Exception{
        paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpReplyChg> lmtGrpReplyChgs = lmtGrpReplyChgMapper.queryByGrpserno(paramMap);
        if (lmtGrpReplyChgs != null && lmtGrpReplyChgs.size() > 0) {
            return lmtGrpReplyChgs.get(0);
        } else {
            throw BizException.error(null, EcbEnum.ECB010073.key, EcbEnum.ECB010073.value);
        }
    }



    /**
     * @方法名称: queryLmtGrpReplyChgBySerno
     * @方法描述: 通过流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpReplyChg queryLmtGrpReplyChgBySerno(String serno) throws Exception {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("grpSerno", serno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpReplyChg> lmtGrpReplyChgList = lmtGrpReplyChgMapper.queryLmtGrpReplyChgDataByParams(queryMap);
        if (lmtGrpReplyChgList != null && lmtGrpReplyChgList.size() == 1) {
            return lmtGrpReplyChgList.get(0);
        } else {
            throw new Exception("查询授信批复变更数据异常");
        }

    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 集团授信批复变更流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) throws Exception {
        LmtGrpReplyChg lmtGrpReplyChg = this.queryLmtGrpReplyChgBySerno(serno);
        lmtGrpReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        this.update(lmtGrpReplyChg);
    }


    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信批复变更流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno) throws Exception {
        // 1.将审批状态更新为998
        LmtGrpReplyChg lmtGrpReplyChg = this.queryLmtGrpReplyChgBySerno(serno);
        lmtGrpReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        this.update(lmtGrpReplyChg);

    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信批复变更流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) throws Exception {
        // 1.将审批状态更新为992
        LmtGrpReplyChg lmtGrpReplyChg = this.queryLmtGrpReplyChgBySerno(serno);
        lmtGrpReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        this.update(lmtGrpReplyChg);

    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信批复变更流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.从根据流水号获批复变更数据，根据数据生成批复数据
     * 3.根据批复数据更新批复台账数据
     * 4.将批复台账推送至额度系统
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno, String currentUserId, String currentOrgId) throws Exception {
        // 1.将审批状态更新为997
        log.info("将集团申请流水号【"+serno+"】的集团批复变更申请的审批状态更改为997--通过");
        LmtGrpReplyChg lmtGrpReplyChg = this.queryLmtGrpReplyChgBySerno(serno);
        lmtGrpReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.update(lmtGrpReplyChg);

        // 2.更新批复表以及批复台账中 贷后管理要求 字段
        lmtGrpReplyService.updateLmtGrpReplyByLmtGrpReplyChg(lmtGrpReplyChg,currentUserId, currentOrgId);
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(lmtGrpReplyChg.getGrpReplySerno());
        lmtGrpReplyAccService.updateLmtGrpReplyAccUnderReplyChg(lmtGrpReply,CmisCommonConstants.YES_NO_1);

        /*// 2.根据流水号获批复变更数据，根据数据更新批复数据
        lmtGrpReplyService.updateLmtGrpReplyHandleByLmtGrpReplyChg(lmtGrpReplyChg, currentUserId, currentOrgId);

        // 查询新的批复数据
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.queryLmtGrpReplyByReplySerno(lmtGrpReplyChg.getGrpReplySerno());
        // 3.根据授信批复更新授信批复台账
        lmtGrpReplyAccService.updateLmtGrpReplyAccUnderReplyChg(lmtGrpReply, CmisCommonConstants.YES_NO_1);

        // 4.推送授信台账至额度系统
        lmtGrpReplyAccService.synLmtReplyAccToLmtSys(lmtGrpReply.getGrpCusId());*/

        log.info("授信批复变更审批业务逻辑处理完成" + serno);
    }

    /**
     * @函数名称:updateLigicDelete
     * @函数描述: 集团批复变更申请列表逻辑删除
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */

    public int updateLigicDelete(LmtGrpReplyChg lmtGrpReplyChg) {
        int resNum = 0;
        if(lmtGrpReplyChg.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_992)){
            lmtGrpReplyChg.setOprType(CmisCommonConstants.OP_TYPE_01);
            lmtGrpReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            int exit = updateSelective(lmtGrpReplyChg);
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(lmtGrpReplyChg.getGrpSerno());
        }else{
            lmtGrpReplyChg.setOprType(CmisCommonConstants.OP_TYPE_02);
            resNum = updateSelective(lmtGrpReplyChg);
        }
        return resNum;
    }
}
