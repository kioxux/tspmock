package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0116Service
 * @类描述: 还款计划变更金额校验
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021-09-15 21:15:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0116Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0116Service.class);

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    /**
     * @方法名称: riskItem0116
     * @方法描述: 还款计划变更金额校验
     * @参数与返回说明:
     * @算法描述:
     * 还款计划变更金额校验
     * 还款方式变为按期付息,按计划还本时,校验还款金额合计必须等于贷款余额
     * @创建人: ys
     * @创建时间: 2021-09-15 21:15:12
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public  RiskResultDto riskItem0116(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("还款方式变为按期付息,按计划还本时,校验还款金额合计必须等于贷款余额  开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        IqpRepayWayChg iqpRepayWayChg = iqpRepayWayChgService.selectByPrimaryKey(serno);
        if (iqpRepayWayChg != null) {
            // 还款方式变为A040-按期付息,按计划还本
            if ("A040".equals(iqpRepayWayChg.getRepayMode())) {
                BigDecimal loanBalance = iqpRepayWayChg.getLoanBalance();
                QueryModel model = new QueryModel();
                model.addCondition("serno", serno);
                List<RepayCapPlan> repayCapPlanList = repayCapPlanService.selectAll(model);
                BigDecimal repayAmtTotal = new BigDecimal(0);
                if (CollectionUtils.nonEmpty(repayCapPlanList)) {
                    for (int i = 0; i < repayCapPlanList.size(); i++) {
                        repayAmtTotal = repayAmtTotal.add(repayCapPlanList.get(i).getRepayAmt());
                    }
                }
                // 校验还款金额合计必须等于贷款余额
                if (loanBalance.compareTo(repayAmtTotal) == 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("还款方式变为按期付息,按计划还本时,校验还款金额合计必须等于贷款余额！");
                }
            } else { // 其他还款方式不需要检验
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("通过申请流水号未获取到对应的还款计划变更申请信息！");
        }
        log.info("还款方式变为按期付息,按计划还本时,校验还款金额合计必须等于贷款余额  结束*******************业务流水号：【{}】", serno);
        return riskResultDto;
    }
}