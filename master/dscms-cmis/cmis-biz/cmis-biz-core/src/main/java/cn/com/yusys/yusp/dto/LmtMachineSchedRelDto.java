package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtMachineSchedRel
 * @类描述: lmt_machine_sched_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 11:45:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtMachineSchedRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 设备名称 **/
	private String equipName;
	
	/** 设备品牌 **/
	private String equipBrand;
	
	/** 型号 **/
	private String model;
	
	/** 销售价格 **/
	private java.math.BigDecimal salePrice;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param equipName
	 */
	public void setEquipName(String equipName) {
		this.equipName = equipName == null ? null : equipName.trim();
	}
	
    /**
     * @return EquipName
     */	
	public String getEquipName() {
		return this.equipName;
	}
	
	/**
	 * @param equipBrand
	 */
	public void setEquipBrand(String equipBrand) {
		this.equipBrand = equipBrand == null ? null : equipBrand.trim();
	}
	
    /**
     * @return EquipBrand
     */	
	public String getEquipBrand() {
		return this.equipBrand;
	}
	
	/**
	 * @param model
	 */
	public void setModel(String model) {
		this.model = model == null ? null : model.trim();
	}
	
    /**
     * @return Model
     */	
	public String getModel() {
		return this.model;
	}
	
	/**
	 * @param salePrice
	 */
	public void setSalePrice(java.math.BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	
    /**
     * @return SalePrice
     */	
	public java.math.BigDecimal getSalePrice() {
		return this.salePrice;
	}


}