package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSub;
import cn.com.yusys.yusp.domain.WydBatRecord;
import cn.com.yusys.yusp.domain.XbdInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.XbdInfoMapper;
import cn.com.yusys.yusp.repository.mapper.wyd.WydBatRecordMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: WydBatRecordService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-08-04 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class  WydBatRecordService{

    private static final Logger log = LoggerFactory.getLogger(WydBatRecordService.class);

    @Resource
    private WydBatRecordMapper wydBatRecordMapper;

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<WydBatRecord> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<WydBatRecord> list = wydBatRecordMapper.selectByCondition(model);
        PageHelper.clearPage();
        return list;
    }



}
