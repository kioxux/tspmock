package cn.com.yusys.yusp.web.server.xdtz0047;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;

import cn.com.yusys.yusp.dto.server.xdtz0047.resp.List;
import cn.com.yusys.yusp.service.server.xdtz0047.Xdtz0047Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0047.req.Xdtz0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.resp.Xdtz0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:借据信息查询（按证件号）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0047:借据信息查询（按证件号）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0047Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0047Resource.class);
    @Autowired
    private Xdtz0047Service xdtz0047Service;

    /**
     * 交易码：xdtz0047
     * 交易描述：借据信息查询（按证件号）
     *
     * @param xdtz0047DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据信息查询（按证件号）")
    @PostMapping("/xdtz0047")
    protected @ResponseBody
    ResultDto<Xdtz0047DataRespDto> xdtz0047(@Validated @RequestBody Xdtz0047DataReqDto xdtz0047DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataReqDto));
        Xdtz0047DataRespDto xdtz0047DataRespDto = new Xdtz0047DataRespDto();// 响应Dto:借据信息查询（按证件号）
        ResultDto<Xdtz0047DataRespDto> xdtz0047DataResultDto = new ResultDto<>();
        String query_type = xdtz0047DataReqDto.getQuery_type();//查询类型
        String cert_code = xdtz0047DataReqDto.getCert_code();//证件号
        String cus_id = xdtz0047DataReqDto.getCus_id();//客户编号
        try {
            if (StringUtil.isEmpty(query_type)) {
                xdtz0047DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0047DataResultDto.setMessage("查询类型【query_type】不能为空！");
                return xdtz0047DataResultDto;
            }else{
                if("01".equals(query_type)){//查询个人
                    if(StringUtil.isEmpty(cert_code)){//证件号不能为空
                        xdtz0047DataResultDto.setCode(EpbEnum.EPB099999.key);
                        xdtz0047DataResultDto.setMessage("证件号【query_type】不能为空！");
                        return xdtz0047DataResultDto;
                    }
                }else if("02".equals(query_type)){//查询公司
                    if(StringUtil.isEmpty(cus_id)){//客户号不能为空
                        xdtz0047DataResultDto.setCode(EpbEnum.EPB099999.key);
                        xdtz0047DataResultDto.setMessage("客户编号【cus_id】不能为空！");
                        return xdtz0047DataResultDto;
                    }
                }
            }
            // 从xdtz0047DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataReqDto));
            xdtz0047DataRespDto = xdtz0047Service.xdtz0047(xdtz0047DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataRespDto));
            // 封装xdtz0047DataResultDto中正确的返回码和返回信息
            xdtz0047DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0047DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, e.getMessage());
            // 封装xdtz0047DataResultDto中异常返回码和返回信息
            xdtz0047DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0047DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0047DataRespDto到xdtz0047DataResultDto中
        xdtz0047DataResultDto.setData(xdtz0047DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataResultDto));
        return xdtz0047DataResultDto;
    }
}
