/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.repository.mapper.AccExtMapper;
import cn.com.yusys.yusp.web.rest.CtrLoanContResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccExtService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-23 16:31:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class YqdRiskInfoService {

    @Autowired
    private AccExtMapper accExtMapper;
    private static final Logger log = LoggerFactory.getLogger(CtrLoanContResource.class);

    /**
     * @param
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @author shenli
     * @date 2021/4/25 0025 20:13
     * @version 1.0.0
     * @desc 优企贷-诉讼信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String, String>> queryLawsuit() {

        List<Map<String, String>> list = new ArrayList<>();

        Map <String, String> parame1 = new HashMap<String, String>();
        parame1.put("busiRole","借款人");
        parame1.put("cusName","張三");
        parame1.put("caseno","00001");
        parame1.put("lawsuitStats","原告");
        list.add(parame1);
        return list;
    }

    /**
     * @param 
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @author shenli
     * @date 2021/4/25 0025 20:13
     * @version 1.0.0
     * @desc 优企贷-征信风险提示
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String, String>> queryCreditRisk() {

        List<Map<String, String>> list = new ArrayList<>();

        Map <String, String> parame1 = new HashMap<String, String>();
        parame1.put("name","张三");
        parame1.put("item","贷款逾期");
        parame1.put("result","989");
        list.add(parame1);

        Map <String, String> parame2 = new HashMap<String, String>();
        parame1.put("name","李四");
        parame1.put("item","贷款正常");
        parame1.put("result","231");
        list.add(parame1);

        Map <String, String> parame3 = new HashMap<String, String>();
        parame3.put("name","王二");
        parame3.put("item","贷款逾期");
        parame3.put("result","342");
        list.add(parame3);
        return list;
    }

    
    /**
     * @param
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @author shenli
     * @date 2021/4/25 0025 20:13
     * @version 1.0.0
     * @desc 优企贷-历史信贷表现（灰名单）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<Map<String, String>> queryBlackList() {

        List<Map<String, String>> list = new ArrayList<>();

        Map <String, String> parame1 = new HashMap<String, String>();
        parame1.put("managerName","张三");
        parame1.put("certCode","2110031955****0286");
        parame1.put("name","优企贷测试00001");
        parame1.put("amt","30000");
        parame1.put("guarType","STD_ZB_ASSURE_MEANS");
        parame1.put("busType","经营性贷款");
        parame1.put("productName","经营贷二");
        parame1.put("riks","正常");
        parame1.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：拒绝");
        list.add(parame1);

        Map <String, String> parame2 = new HashMap<String, String>();
        parame2.put("managerName","李四 ");
        parame2.put("certCode","9137010****5461553");
        parame2.put("name","优企贷测试00002");
        parame2.put("amt","30000");
        parame2.put("guarType","STD_ZB_ASSURE_MEANS");
        parame2.put("busType","经营性贷款");
        parame2.put("productName","经营贷二");
        parame2.put("riks","正常");
        parame2.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：同意");
        list.add(parame2);

        Map <String, String> parame3 = new HashMap<String, String>();
        parame3.put("managerName","王二");
        parame3.put("certCode","2110031955****2312");
        parame3.put("name","优企贷测试00003");
        parame3.put("amt","30000");
        parame3.put("guarType","STD_ZB_ASSURE_MEANS");
        parame3.put("busType","经营性贷款");
        parame3.put("productName","经营贷二");
        parame3.put("riks","正常");
        parame3.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：同意");
        list.add(parame3);

        return list;
    }




    /**
     * @param
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author shenli
     * @date 2021/4/24 0024 14:06
     * @version 1.0.0
     * @desc 根据合同编号，查询合同基本信息，关联担保合同列表，客户信息，批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> queryRiskInfo() {

        Map<String,Object> map = new HashMap<>();

        try {


            List<Map<String, String>> litgatInfoList = new ArrayList<>();
            Map <String, String> litgatInfoParame1 = new HashMap<String, String>();
            litgatInfoParame1.put("busi_role","01");
            litgatInfoParame1.put("cus_name","优企贷测试0001");
            litgatInfoParame1.put("caseno","yqd000000000002");
            litgatInfoParame1.put("lawsuit_stats","原告");
            litgatInfoList.add(litgatInfoParame1);

            Map <String, String> litgatInfoParame2 = new HashMap<String, String>();
            litgatInfoParame2.put("busi_role","02");
            litgatInfoParame2.put("cus_name","优企贷测试0002");
            litgatInfoParame2.put("caseno","yqd000000000002");
            litgatInfoParame2.put("lawsuit_stats","被告");
            litgatInfoList.add(litgatInfoParame2);

            Map <String, String> litgatInfoParame3 = new HashMap<String, String>();
            litgatInfoParame3.put("busi_role","03");
            litgatInfoParame3.put("cus_name","优企贷测试0003");
            litgatInfoParame3.put("caseno","yqd000000000003");
            litgatInfoParame3.put("lawsuit_stats","被告");
            litgatInfoList.add(litgatInfoParame3);



            List<Map<String, String>> creditRiskList = new ArrayList<>();
            Map <String, String> creditRiskParame1 = new HashMap<String, String>();
            creditRiskParame1.put("name","张三");
            creditRiskParame1.put("item","贷款逾期");
            creditRiskParame1.put("result","989");
            creditRiskList.add(creditRiskParame1);

            Map <String, String> creditRiskParame2 = new HashMap<String, String>();
            creditRiskParame2.put("name","李四");
            creditRiskParame2.put("item","贷款正常");
            creditRiskParame2.put("result","231");
            creditRiskList.add(creditRiskParame2);

            Map <String, String> creditRiskParame3 = new HashMap<String, String>();
            creditRiskParame3.put("name","王二");
            creditRiskParame3.put("item","贷款逾期");
            creditRiskParame3.put("result","342");
            creditRiskList.add(creditRiskParame3);

            map.put("creditRiskList",creditRiskList);



            List<Map<String, String>> hisCreditList = new ArrayList<>();

            Map <String, String> hisCreditParame1 = new HashMap<String, String>();
            hisCreditParame1.put("manager_name","张三");
            hisCreditParame1.put("cert_code","2110031955****0286");
            hisCreditParame1.put("name","优企贷测试00001");
            hisCreditParame1.put("amt","30000");
            hisCreditParame1.put("guar_type","STD_ZB_ASSURE_MEANS");
            hisCreditParame1.put("bus_type","经营性贷款");
            hisCreditParame1.put("product_name","经营贷二");
            hisCreditParame1.put("riks","正常");
            hisCreditParame1.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：拒绝");
            hisCreditList.add(hisCreditParame1);

            Map <String, String> hisCreditParame2 = new HashMap<String, String>();
            hisCreditParame2.put("manager_name","李四 ");
            hisCreditParame2.put("cert_code","9137010****5461553");
            hisCreditParame2.put("name","优企贷测试00002");
            hisCreditParame2.put("amt","30000");
            hisCreditParame2.put("guar_type","STD_ZB_ASSURE_MEANS");
            hisCreditParame2.put("bus_type","经营性贷款");
            hisCreditParame2.put("product_name","经营贷二");
            hisCreditParame2.put("riks","正常");
            hisCreditParame2.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：同意");
            hisCreditList.add(hisCreditParame2);

            Map <String, String> hisCreditParame3 = new HashMap<String, String>();
            hisCreditParame3.put("manager_name","王二");
            hisCreditParame3.put("cert_code","2110031955****2312");
            hisCreditParame3.put("name","优企贷测试00003");
            hisCreditParame3.put("amt","30000");
            hisCreditParame3.put("guar_type","STD_ZB_ASSURE_MEANS");
            hisCreditParame3.put("bus_type","经营性贷款");
            hisCreditParame3.put("product_name","经营贷二");
            hisCreditParame3.put("riks","正常");
            hisCreditParame3.put("reason","通过对客户经理上报的影像资料与调查报告的审核工作，并经特殊贷款处理审议，出具以下审批意见：同意");
            hisCreditList.add(hisCreditParame3);
            map.put("hisCreditList",hisCreditList);


        }catch (Exception e) {
            log.error("调用接口异常",e);
        }
        return map;
    }
}
