package cn.com.yusys.yusp.command;

import cn.com.yusys.yusp.service.RedisAuthCacheService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;

import java.util.concurrent.*;

/**
 * 启动时加载数据到缓存
 *
 * @author yangzai
 * @since 2021/4/23
 **/
public class RedisCacheStartLoader implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(RedisCacheStartLoader.class);

    @Autowired
    @Lazy
    private RedisAuthCacheService redisAuthCacheService;


    ThreadFactory guavaThreadFactory = new ThreadFactoryBuilder().setNameFormat("RedisCacheLoader-pool-").build();

    ExecutorService executorService = new ThreadPoolExecutor(1, 1,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(10), guavaThreadFactory);

    @Override
    public void run(String... args) {
        // 启动时加载，为不影响启动，单独开启一个线程用户执行数据查询及缓存初始化操作
        executorService.execute(() -> {
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>开始加载区域中心负责人下小微客户经理信息到Redis>>>>>>>>>>>>>>>>>>>");
            redisAuthCacheService.refershCache();
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>加载区域中心负责人下小微客户经理信息到Redis结束>>>>>>>>>>>>>>>>>>>");
        });
    }

}