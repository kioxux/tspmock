/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.req.Ln3077ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.resp.Ln3077RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.req.Ln3251ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp.Ln3251RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.service.client.bsp.core.ln3077.Ln3077Service;
import cn.com.yusys.yusp.service.client.bsp.core.ln3251.Ln3251Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpDemandRepayAppInfo;
import cn.com.yusys.yusp.repository.mapper.PvpDemandRepayAppInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpDemandRepayAppInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-27 10:56:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpDemandRepayAppInfoService {

    private static final Logger logger = LoggerFactory.getLogger(PvpDemandRepayAppInfoService.class);
    @Autowired
    private PvpDemandRepayAppInfoMapper pvpDemandRepayAppInfoMapper;
    @Autowired
    private Ln3251Service ln3251Service;
    @Autowired
    private Ln3077Service ln3077Service;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpDemandRepayAppInfo selectByPrimaryKey(String pkId, String billNo) {
        return pvpDemandRepayAppInfoMapper.selectByPrimaryKey(pkId, billNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpDemandRepayAppInfo> selectAll(QueryModel model) {
        List<PvpDemandRepayAppInfo> records = (List<PvpDemandRepayAppInfo>) pvpDemandRepayAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpDemandRepayAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpDemandRepayAppInfo> list = pvpDemandRepayAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpDemandRepayAppInfo record) {
        return pvpDemandRepayAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpDemandRepayAppInfo record) {
        return pvpDemandRepayAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpDemandRepayAppInfo record) {
        return pvpDemandRepayAppInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpDemandRepayAppInfo record) {
        return pvpDemandRepayAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String billNo) {
        return pvpDemandRepayAppInfoMapper.deleteByPrimaryKey(pkId, billNo);
    }

    /**
     * @param pkId, billNo
     * @return int
     * @author hubp
     * @date 2021/11/4 21:46
     * @version 1.0.0
     * @desc 如果为流程被退回的，先删除流程，再删除申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteByTowKey(String pkId, String billNo) {
        PvpDemandRepayAppInfo pvpDemandRepayAppInfo = pvpDemandRepayAppInfoMapper.selectByPrimaryKey(pkId, billNo);
        if(Objects.nonNull(pvpDemandRepayAppInfo) &&
                StringUtils.nonBlank(pvpDemandRepayAppInfo.getApproveStatus()) &&
                    CmisBizConstants.APPLY_STATE_CALL_BACK.equals(pvpDemandRepayAppInfo.getApproveStatus())){
            // 如果为流程被退回的，先删除流程，再删除申请
            workflowCoreClient.deleteByBizId(pkId);
        }
        return pvpDemandRepayAppInfoMapper.deleteByPrimaryKey(pkId, billNo);
    }

    /**
     * @方法名称: selectByPkId
     * @方法描述: 根据主键查询 -- 流程专用  hubp
     * @参数与返回说明:
     * @算法描述: 2021年9月8日22:33:59
     */

    public PvpDemandRepayAppInfo selectByPkId(String pkId) {
        return pvpDemandRepayAppInfoMapper.selectByPkId(pkId);
    }

    /**
     * @param record
     * @return int
     * @author hubp
     * @date 2021/8/28 11:03
     * @version 1.0.0
     * @desc  第三方账号专用保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto updatePvpDemandRepayAppInfo(PvpDemandRepayAppInfo record) {
        PvpDemandRepayAppInfo demandInfo = pvpDemandRepayAppInfoMapper.selectByPrimaryKey(record.getPkId(),record.getBillNo());
        if(Objects.isNull(demandInfo)){
            return new ResultDto().code(9999).message("查找基本数据异常！");
        }
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(record.getAcctIdType())){
            demandInfo.setOtherRepayAcctNo(record.getOtherRepayAcctNo());
        }else{
            demandInfo.setRepayAcctNo(record.getRepayAcctNo());
        }
        demandInfo.setAcctIdType(record.getAcctIdType());
        int result = pvpDemandRepayAppInfoMapper.updateByPrimaryKey(demandInfo);
        return new ResultDto(result);
    }
    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/8/27 15:17
     * @version 1.0.0
     * @desc  新增一条数据，并将数据返回
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto addPvpDemandRepayApp(PvpDemandRepayAppInfo pvpDemandRepayAppInfo) {
        if(StringUtils.isBlank(pvpDemandRepayAppInfo.getBillNo())){
            return new ResultDto().code(9999).message("借据编号不能为空！");
        }
        pvpDemandRepayAppInfo.setPkId(UUID.randomUUID().toString());
        pvpDemandRepayAppInfoMapper.insert(pvpDemandRepayAppInfo);
        return new ResultDto(pvpDemandRepayAppInfo);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/8/27 23:05
     * @version 1.0.0
     * @desc    通过借据编号获取核心交易信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto selectCoreData (QueryModel queryModel){
        String billNo = queryModel.getCondition().get("billNo").toString();
        String page = String.valueOf(queryModel.getPage());
        String size = String.valueOf(queryModel.getSize());
        logger.info("************获取核心交易信息开始***********",billNo);
        Ln3251RespDto ln3251RespDto = null;
        try {
            Ln3251ReqDto ln3251ReqDto = new Ln3251ReqDto();
            ln3251ReqDto.setDkjiejuh(billNo);
            ln3251ReqDto.setQishibis(page);
            ln3251ReqDto.setChxunbis(size);
            ln3251RespDto = ln3251Service.ln3251(ln3251ReqDto);
        } catch (Exception e) {
            logger.error("************获取核心交易信息异常***********");
            throw e;
        }finally {
            logger.info("************获取核心交易信息结束***********");
        }
        return new ResultDto(ln3251RespDto.getLstkhzmx_ARRAY());
    }

    /**
     * @param pvpDemandRepayAppInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/8/27 23:53
     * @version 1.0.0
     * @desc   发送核心进行冲正
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto sendCoreData (PvpDemandRepayAppInfo pvpDemandRepayAppInfo){
        logger.info("************获取核心交易信息开始***********");
        Ln3077RespDto ln3077RespDto = null;
        try {
            Ln3077ReqDto ln3077ReqDto = new Ln3077ReqDto();
            //  daikczbz   业务操作标志1--录入  2--复核  3--直通
            ln3077ReqDto.setDaikczbz("3");
            //  dkjiejuh   贷款借据号
            ln3077ReqDto.setDkjiejuh(pvpDemandRepayAppInfo.getBillNo());
            //   zhchbjin   正常本金
            ln3077ReqDto.setZhchbjin(pvpDemandRepayAppInfo.getGuihzcbj());
            //   yuqibjin   逾期本金
            ln3077ReqDto.setYuqibjin(pvpDemandRepayAppInfo.getGuihyqbj());
            //   dzhibjin   呆滞本金
            // ln3077ReqDto.setDzhibjin();
            //   daizbjin   呆账本金
            //   ysyjlixi   应收应计利息 ghysyjlx
            ln3077ReqDto.setYsyjlixi(pvpDemandRepayAppInfo.getGhysyjlx());
            //   csyjlixi   催收应计利息
            ln3077ReqDto.setCsyjlixi(pvpDemandRepayAppInfo.getGhcsyjlx());
            //   ysqianxi   应收欠息
            ln3077ReqDto.setYsqianxi(pvpDemandRepayAppInfo.getGhynshqx());
            //   csqianxi   催收欠息
            ln3077ReqDto.setCsqianxi(pvpDemandRepayAppInfo.getGhcushqx());
            //   ysyjfaxi   应收应计罚息
            ln3077ReqDto.setYsyjfaxi(pvpDemandRepayAppInfo.getGhysyjfx());
            //   csyjfaxi   催收应计罚息
            ln3077ReqDto.setCsyjfaxi(pvpDemandRepayAppInfo.getGhcsyjfx());
            //   yshofaxi   应收罚息
            ln3077ReqDto.setYshofaxi(pvpDemandRepayAppInfo.getGhynshfx());
            //  cshofaxi   催收罚息
            ln3077ReqDto.setCshofaxi(pvpDemandRepayAppInfo.getGhcushfx());
            //   yingjifx   应计复息
            ln3077ReqDto.setYingjifx(pvpDemandRepayAppInfo.getGhyjfuxi());
            //   fuxiiiii   复息
            ln3077ReqDto.setFuxiiiii(pvpDemandRepayAppInfo.getGhfxfuxi());
            //   yingjitx   应计贴息
            //ln3077ReqDto.setYingjitx();
            //   yingshtx   应收贴息
            //ln3077ReqDto.setYingshtx();
            //   hexiaobj   核销本金
            //ln3077ReqDto.setHexiaobj();
            //   hexiaolx   核销利息
            //ln3077ReqDto.setHexiaolx();
            //   yihxbjlx   已核销本金利息
            //ln3077ReqDto.setYihxbjlx();
            //   yingshfj   应收罚金
            ln3077ReqDto.setYingshfj(pvpDemandRepayAppInfo.getGhfajinn());
            //   huankjee   还款金额
            //   hexiaoje   核销来源金额
            //  hexiaozh   核销来源账号
            //  hexiaozx   核销来源账号子序号
            //  huankzhh   还款账号
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(pvpDemandRepayAppInfo.getAcctIdType())){
                ln3077ReqDto.setHuankzhh(pvpDemandRepayAppInfo.getOtherRepayAcctNo());
            }else{
                ln3077ReqDto.setHuankzhh(pvpDemandRepayAppInfo.getRepayAcctNo());
            }
            //  hkzhhzxh   还款账号子序号
            ln3077RespDto = ln3077Service.ln3077(ln3077ReqDto);
        } catch (Exception e) {
            logger.error("************获取核心交易信息异常:[{}]***********",e.getMessage());
            throw BizException.error(null, "9999", e.getMessage());
        }finally {
            logger.info("************获取核心交易信息结束***********");
        }
        return new ResultDto(ln3077RespDto);
    }

}
