/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LatestLmtInfoDto;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.LmtGrpMemRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.LmtRecheckDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRecheckDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 19:41:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtrecheckdetail")
public class LmtRecheckDetailResource {
    @Autowired
    private LmtRecheckDetailService lmtRecheckDetailService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRecheckDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRecheckDetail> list = lmtRecheckDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtRecheckDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRecheckDetail>> index(QueryModel queryModel) {
        List<LmtRecheckDetail> list = lmtRecheckDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtRecheckDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtRecheckDetail> show(@PathVariable("pkId") String pkId) {
        LmtRecheckDetail lmtRecheckDetail = lmtRecheckDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtRecheckDetail>(lmtRecheckDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtRecheckDetail> create(@RequestBody LmtRecheckDetail lmtRecheckDetail) throws URISyntaxException {
        lmtRecheckDetailService.insert(lmtRecheckDetail);
        return new ResultDto<LmtRecheckDetail>(lmtRecheckDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRecheckDetail lmtRecheckDetail) throws URISyntaxException {
        int result = lmtRecheckDetailService.update(lmtRecheckDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtRecheckDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtRecheckDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据申请流水号查询授信情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryLmtConditionByLmtSerno")
    protected ResultDto<List<LmtAppSubPrd>> queryLmtConditionByLmtSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<LmtAppSubPrd> lmtAppSubPrds = lmtRecheckDetailService.queryDetailByLmtSerno(serno);
        return  ResultDto.success(lmtAppSubPrds);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据申请流水号查询授信情况(包含目前用信净额)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryLmtAndLoanConditionByLmtSerno")
    protected ResultDto<List<Map>> queryLmtAndLoanConditionByLmtSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<Map> lmtAppSubPrds = lmtRecheckDetailService.queryLmtAndLoanConditionByLmtSerno(serno);
        return  ResultDto.success(lmtAppSubPrds);
    }

    /**
     * @函数名称:queryFinancSituationByLmtSerno
     * @函数描述:根据申请流水号查询近三期总融资情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryFinancSituationByLmtSerno")
    protected ResultDto<List<RptCptlSituCorp>> queryFinancSituationByLmtSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptCptlSituCorp> rptCptlSituCorps = lmtRecheckDetailService.queryFinancSituationByLmtSerno(serno);
        return  ResultDto.success(rptCptlSituCorps);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询抵押担保情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryGuarByLmtSerno")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> queryGuarByLmtSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssets = lmtRecheckDetailService.queryGuarByLmtSerno(serno);
        return  ResultDto.success(rptBasicInfoPersFamilyAssets);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtRecheckDetail> save(@RequestBody LmtRecheckDetail lmtRecheckDetail) throws URISyntaxException {
        LmtRecheckDetail lmtRecheckDetail1 = lmtRecheckDetailService.selectByLmtSerno(lmtRecheckDetail.getLmtSerno());
        User userInfo = SessionUtils.getUserInformation();
        if(Objects.isNull(lmtRecheckDetail1)){
            lmtRecheckDetail.setPkId(UUID.randomUUID().toString());
            lmtRecheckDetail.setOprType(CmisCommonConstants.OP_TYPE_01);
            lmtRecheckDetail.setInputId(userInfo.getUserId());
            lmtRecheckDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtRecheckDetail.setInputDate(DateUtils.getCurrDateStr());
            lmtRecheckDetail.setUpdId(userInfo.getUserId());
            lmtRecheckDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtRecheckDetail.setUpdDate(DateUtils.getCurrDateStr());
            lmtRecheckDetail.setCreateTime(DateUtils.getCurrTimestamp());
            lmtRecheckDetailService.insert(lmtRecheckDetail);
        }else{
            lmtRecheckDetail1.setInteAnaly(lmtRecheckDetail.getInteAnaly());
            lmtRecheckDetail1.setControlOpinion(lmtRecheckDetail.getControlOpinion());
            lmtRecheckDetail1.setRestDesc(lmtRecheckDetail.getRestDesc());

            lmtRecheckDetail1.setUpdId(userInfo.getUserId());
            lmtRecheckDetail1.setUpdBrId(userInfo.getOrg().getCode());
            lmtRecheckDetail1.setUpdDate(DateUtils.getCurrDateStr());
            lmtRecheckDetailService.updateSelective(lmtRecheckDetail1);
        }
        return ResultDto.success(lmtRecheckDetail);
    }

    /**
     * 根据申请流水号查询数据
     * @param lmtSerno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtRecheckDetail> selectBySerno(@RequestBody String lmtSerno) {
        LmtRecheckDetail lmtRecheckDetail = lmtRecheckDetailService.selectByLmtSerno(lmtSerno);
        return  ResultDto.success(lmtRecheckDetail);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据集团申请流水号查询授信情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryLmtConditionByGrpSerno")
    protected ResultDto<List<LmtAppSubPrd>> queryLmtConditionByGrpSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<LmtGrpMemRel> lmtGrpMemRels= lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        List<LmtAppSubPrd> lmtAppSubPrds = new ArrayList<LmtAppSubPrd>();
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
            String singleSerno = lmtGrpMemRel.getSingleSerno();
            if(!StringUtils.isEmpty(singleSerno)){
                List<LmtAppSubPrd> lmtAppSubPrd1s =  lmtRecheckDetailService.queryDetailByLmtSerno(singleSerno);
                for (LmtAppSubPrd lmtAppSubPrd1 : lmtAppSubPrd1s) {
                    lmtAppSubPrds.add(lmtAppSubPrd1);
                }
            }
        }
        return  ResultDto.success(lmtAppSubPrds);
    }

    /**
     * @函数名称:queryFinancSituationByLmtSerno
     * @函数描述:根据集团申请流水号查询近三期总融资情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryFinancSituationByGrpSerno")
    protected ResultDto<List<RptCptlSituCorp>> queryFinancSituationByGrpSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptCptlSituCorp> rptCptlSituCorps = new ArrayList<RptCptlSituCorp>();
        List<LmtGrpMemRel> lmtGrpMemRels= lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        for(int i = 0; i <lmtGrpMemRels.size()-1;i++){
            String singleSerno = lmtGrpMemRels.get(i).getSingleSerno();
            if(!StringUtils.isEmpty(singleSerno)){
                List<RptCptlSituCorp> rptCptlSituCorp1s =  lmtRecheckDetailService.queryFinancSituationByLmtSerno(singleSerno);
                for(int j = 0; j <rptCptlSituCorp1s.size()-1;j++){
                    rptCptlSituCorps.add(rptCptlSituCorp1s.get(i));
                }
            }
        }
        return  ResultDto.success(rptCptlSituCorps);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据集团申请流水号查询抵押担保情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryGuarByGrpSerno")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> queryGuarByGrpSerno(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssets = new ArrayList<RptBasicInfoPersFamilyAssets>();
        List<LmtGrpMemRel> lmtGrpMemRels= lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
        for(int i = 0; i <lmtGrpMemRels.size()-1;i++){
            String singleSerno = lmtGrpMemRels.get(i).getSingleSerno();
            if(!StringUtils.isEmpty(singleSerno)){
                List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAsset1s =  lmtRecheckDetailService.queryGuarByLmtSerno(singleSerno);
                for(int j = 0; j <rptBasicInfoPersFamilyAsset1s.size()-1;j++){
                    rptBasicInfoPersFamilyAssets.add(rptBasicInfoPersFamilyAsset1s.get(i));
                }
            }
        }
        return  ResultDto.success(rptBasicInfoPersFamilyAssets);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据上期集团申请流水号查询上期授信情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryLatestInfoByGrpSerno")
    protected ResultDto<List<Map>> queryLatestInfoByGrpSerno(@RequestBody Map<String,String> map) {
        List<Map> latestLmtInfoDtos = lmtRecheckDetailService.queryLatestInfoByGrpSerno(map);
        return  ResultDto.success(latestLmtInfoDtos);
    }

    /**
     * @函数名称:queryEnterpriseChangeData
     * @函数描述:查询复审表企业经营变动情况表数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryenterprisechangedata")
    protected ResultDto<List<Map>> queryEnterpriseChangeData(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        String refTable = map.get("refTable");
        List<Map> mapList = lmtRecheckDetailService.queryEnterpriseChangeData(serno,refTable);
        return  ResultDto.success(mapList);
    }

    /**
     * @函数名称:saveRefTableData
     * @函数描述:保存复审表页面表格数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveRefTableData")
    protected ResultDto<Integer> saveRefTableData(@RequestBody Map<String,String> map) {
        int resNum = lmtRecheckDetailService.saveRefTableData(map);
        return  ResultDto.success(resNum);
    }


}
