/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSubPrd;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccOperAppSubPrdMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppSubPrdService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:30:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccOperAppSubPrdService {

    @Autowired
    private LmtReplyAccOperAppSubPrdMapper lmtReplyAccOperAppSubPrdMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyAccOperAppSubPrd selectByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppSubPrdMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyAccOperAppSubPrd> selectAll(QueryModel model) {
        List<LmtReplyAccOperAppSubPrd> records = (List<LmtReplyAccOperAppSubPrd>) lmtReplyAccOperAppSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyAccOperAppSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccOperAppSubPrd> list = lmtReplyAccOperAppSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtReplyAccOperAppSubPrd record) {
        return lmtReplyAccOperAppSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyAccOperAppSubPrd record) {
        return lmtReplyAccOperAppSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtReplyAccOperAppSubPrd record) {
        return lmtReplyAccOperAppSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyAccOperAppSubPrd record) {
        return lmtReplyAccOperAppSubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppSubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyAccOperAppSubPrdMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplyAccOperAppSubPrdBySerno
     * @方法描述: 通过授信台账操作流水号查询授信台账分项适用品种数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyAccOperAppSubPrd> queryLmtReplyAccOperAppSubPrdBySerno(String serno){
        return lmtReplyAccOperAppSubPrdMapper.queryLmtReplyAccOperAppSubPrdBySerno(serno);
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据授信申请流水号查询对应所有授信分项
     * @参数与返回说明:
     * @算法描述: 无
     * @更新人: ywl
     */
    public List<LmtReplyAccOperAppSubPrd> selectByParams(Map queryMap) {
        return lmtReplyAccOperAppSubPrdMapper.selectByParams(queryMap);
    }

    /**
     * @方法名称: updateSubPrd
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSubPrd(LmtReplyAccOperAppSubPrd record) {
        LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd = lmtReplyAccOperAppSubPrdMapper.queryLmtReplyAccOperAppSubPrdByPrdSerno(record.getSubPrdSerno());
        lmtReplyAccOperAppSubPrd.setAfterLmtStatus(record.getAfterLmtStatus());
        return lmtReplyAccOperAppSubPrdMapper.updateByPrimaryKey(lmtReplyAccOperAppSubPrd);
    }
}
