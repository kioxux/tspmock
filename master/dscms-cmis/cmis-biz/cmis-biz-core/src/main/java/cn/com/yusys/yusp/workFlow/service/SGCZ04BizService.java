package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SGCZ04BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(SGCZ04BizService.class);//定义log

    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String serno = resultInstanceDto.getBizId();
        String bizType = resultInstanceDto.getBizType();
        if (CmisFlowConstants.FLOW_TYPE_SGC01.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC02.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC03.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC04.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC05.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC06.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC07.equals(bizType)) {
            handleDhLmtAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_SGC08.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC09.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC10.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC11.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC12.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC13.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_SGC14.equals(bizType)){
            handleDhGrpLmtAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH05.equals(bizType)) {
            // 展期申请 --寿光
            bgyw02BizService.bizOp(resultInstanceDto);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH07.equals(bizType)) {
            // 担保变更申请 --寿光
            bgyw03Bizservice.bizOp(resultInstanceDto);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 授信申请处理
    private void handleDhLmtAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "单一客户授信新增（寿光）" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            // 加载路由条件
            put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterStart(serno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                sendImage(resultInstanceDto);
                lmtAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                    // 加载路由条件
                    put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                    // 加载路由条件
                    put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterRefuse(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    /**
     * @方法名称: put2VarParamGrp
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamGrp(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtGrpAppService.getRouterMapResult(serno);
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType()) && !OpType.END.equals(resultInstanceDto.getCurrentOpType()) && !OpType.RE_START.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }

    }

    // 集团客户处理
    private void handleDhGrpLmtAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "对公授信申报审批" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            // 重置流程参数
            put2VarParamGrp(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                lmtGrpAppService.handleBusinessAfterStart(serno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                // 流程结束，发起影像审核
                sendImage(resultInstanceDto);
                lmtGrpAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterRefuse(serno, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列并将自消息移除队列！！！！");
        }
    }
    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: cp
     * @创建时间: 2021-08-29 17:07:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getRouterMapResult(serno);
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType()) && !OpType.END.equals(resultInstanceDto.getCurrentOpType()) && !OpType.RE_START.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }

    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.SGCZ04.equals(flowCode);
    }


    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        // 一般情况下，直接获取流程实列中的参数。
        Map<String, Object> map = (Map) JSON.parse(resultInstanceDto.getFlowParam());
        String topOutsystemCode = (String) map.get("topOutsystemCode");
        if(StringUtils.isEmpty(topOutsystemCode)){
            throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
        }
        Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
        String docid = imageParams.get("businessid");
        if(StringUtils.isEmpty(docid)){
            throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
        }
        String[] arr = topOutsystemCode.split(";");
        for (int i = 0; i < arr.length; i++) {
            ImageApprDto imageApprDto = new ImageApprDto();
            imageApprDto.setDocId(docid);//任务编号
            imageApprDto.setApproval("同意");//审批意见
            imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
            imageApprDto.setOutcode(arr[i]);//文件类型根节点
            imageApprDto.setOpercode("");//这个流程不需要审批人员
            cmisBizXwCommonService.sendImage(imageApprDto);
        }
    }
}
