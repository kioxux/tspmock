/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.CtrLoanContAndPvp;
import cn.com.yusys.yusp.domain.XbdInfo;
import cn.com.yusys.yusp.dto.BizCtrLoanContDto;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.GuarContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.HxdLoanContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.req.Xdxw0014DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: XbdInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-08-04 16:04:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface XbdInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    XbdInfo selectByPrimaryKey(@Param("pvpSerno") String pvpSerno);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(XbdInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(XbdInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pvpSerno") String pvpSerno);

}