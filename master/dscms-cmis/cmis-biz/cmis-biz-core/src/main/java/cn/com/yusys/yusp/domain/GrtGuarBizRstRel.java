/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarBizRstRel
 * @类描述: grt_guar_biz_rst_rel数据实体类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-20 17:02:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "grt_guar_biz_rst_rel")
public class GrtGuarBizRstRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 担保合同流水号  **/
	@Column(name = "GUAR_PK_ID", unique = false, nullable = true, length = 40)
	private String guarPkId;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 本次担保金额 **/
	@Column(name = "THIS_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 是否追加担保 STD_ZB_YES_NO **/
	@Column(name = "IS_ADD_GUAR", unique = false, nullable = true, length = 10)
	private String isAddGuar;
	
	/** 是否阶段性担保 STD_ZB_YES_NO **/
	@Column(name = "IS_PER_GUR", unique = false, nullable = true, length = 10)
	private String isPerGur;
	
	/** 关联关系 STD_ZB_CORRE_REL **/
	@Column(name = "CORRE_REL", unique = false, nullable = true, length = 10)
	private String correRel;
	
	/** 关联类型 **/
	@Column(name = "R_TYPE", unique = false, nullable = true, length = 10)
	private String rType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private Date updateTime;

	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId;
	}
	
    /**
     * @return guarPkId
     */
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}

    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param isAddGuar
	 */
	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar;
	}
	
    /**
     * @return isAddGuar
     */
	public String getIsAddGuar() {
		return this.isAddGuar;
	}
	
	/**
	 * @param isPerGur
	 */
	public void setIsPerGur(String isPerGur) {
		this.isPerGur = isPerGur;
	}
	
    /**
     * @return isPerGur
     */
	public String getIsPerGur() {
		return this.isPerGur;
	}
	
	/**
	 * @param correRel
	 */
	public void setCorreRel(String correRel) {
		this.correRel = correRel;
	}
	
    /**
     * @return correRel
     */
	public String getCorreRel() {
		return this.correRel;
	}
	
	/**
	 * @param rType
	 */
	public void setRType(String rType) {
		this.rType = rType;
	}
	
    /**
     * @return rType
     */
	public String getRType() {
		return this.rType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "GrtGuarBizRstRel{" +
				"pkId='" + pkId + '\'' +
				", serno='" + serno + '\'' +
				", contNo='" + contNo + '\'' +
				", guarPkId='" + guarPkId + '\'' +
				", guarContNo='" + guarContNo + '\'' +
				", guarAmt=" + guarAmt +
				", isAddGuar='" + isAddGuar + '\'' +
				", isPerGur='" + isPerGur + '\'' +
				", correRel='" + correRel + '\'' +
				", rType='" + rType + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate=" + inputDate +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate=" + updDate +
				", oprType='" + oprType + '\'' +
				", managerId='" + managerId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				", createTime='" + createTime + '\'' +
				", updateTime='" + updateTime + '\'' +
				'}';
	}
}