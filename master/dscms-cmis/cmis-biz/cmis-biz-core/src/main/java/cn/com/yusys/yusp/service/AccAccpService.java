/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.dto.AccAccpDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req.Xdpj22ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.Xdpj22RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccAccpMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.vo.AccAccpVo;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccAccpService {
    private static final Logger logger = LoggerFactory.getLogger(AccAccpService.class);

    @Autowired
    private AccAccpMapper accAccpMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccAccp selectByPrimaryKey(String pkId) {
        return accAccpMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccAccp> selectAll(QueryModel model) {
        List<AccAccp> records = (List<AccAccp>) accAccpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccAccp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccp> list = accAccpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccAccp record) {
        return accAccpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccAccp record) {
        return accAccpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccAccp record) {
        return accAccpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccAccp record) {
        return accAccpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accAccpMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accAccpMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectAccLoanShow
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccAccp> selectAccLoanShow(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccp> list = accAccpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：selectForAccAccpInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:58
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccAccp> selectForAccAccpInfo(String cusId) {
        return accAccpMapper.selectForAccAccpInfo(cusId);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:58
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccAccp> selectByContNo(String contNo) {
        return accAccpMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: selectAccTfLocRepay
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @author:zhanyb
     */
    public List<AccAccp> selectAccAccpRepay(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccp> list = accAccpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param contNo
     * @return countAccLoanCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    public int countAccAccpCountByContNo(String contNo) {
        return accAccpMapper.countAccAccpCountByContNo(contNo);
    }


    /**
     * @return
     * @方法名称: selectAccAccpByBullNo
     * @方法描述: 查询是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectAccAccpByBullNo(String serno) {
        return accAccpMapper.selectAccAccpByBullNo(serno);
    }

    /**
     * @方法名称: updateAccAccpByserno
     * @方法描述: 根据批次号修改状态为3
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateAccAccpByserno(String serno) {
        return accAccpMapper.updateAccAccpByserno(serno);
    }


    /***
     * 取台账中到期票信息
     * @param
     * @param
     * @return
     */

    public List<AccAccp> getDqAccAccpByCondition(Map map) {
        return accAccpMapper.getDqAccAccpByCondition(map);
    }

    /**
     * 获取到期票面总金额
     *
     * @param
     * @param
     * @return
     */

    public BigDecimal getTotalAmount(Map map) {
        return accAccpMapper.getTotalAmount(map);
    }

    ;

    /**
     * 获取到期票面总余额
     *
     * @param
     * @return
     */
    public BigDecimal getTotalSecurityMoneyAmount(Map map) {
        return accAccpMapper.getTotalSecurityMoneyAmount(map);
    }

    /**
     * 获取到期票面总余额
     *
     * @param
     * @return
     */
    public int updateAccAccpBysernoAndOpenday(Map map) {
        return accAccpMapper.updateAccAccpBysernoAndOpenday(map);
    }

    /**
     * 获取到期票面总余额
     *
     * @param BillNo
     * @return
     */
    public int updateAccAccpByBillNo(String BillNo) {
        return accAccpMapper.updateAccAccpBysernoAndOpenday(BillNo);
    }

    /**
     * 日间还款交易
     *
     * @param addSecurityMoneyAmt
     * @param riskOpenMoneyAmt
     * @param newSecurityRate
     * @param ACC_STATUS
     * @param riskOpenRate
     * @param BillNo
     * @return
     */
    public int updateAccAccpByParms(String BillNo, BigDecimal addSecurityMoneyAmt, BigDecimal riskOpenMoneyAmt, BigDecimal newSecurityRate, BigDecimal ACC_STATUS, BigDecimal riskOpenRate) {
        return accAccpMapper.updateAccAccpByParms(BillNo, addSecurityMoneyAmt, riskOpenMoneyAmt, newSecurityRate, ACC_STATUS, riskOpenRate);
    }

    /**
     * 日间还款交易
     *
     * @param BillNo
     * @param addSecurityMoneyAmt
     * @param riskOpenMoneyAmt
     * @param newSecurityRate
     * @param riskOpenRate
     * @return
     */
    public int updateAccAccpByParms2(String BillNo, BigDecimal addSecurityMoneyAmt, BigDecimal riskOpenMoneyAmt, BigDecimal newSecurityRate, BigDecimal riskOpenRate) {
        return accAccpMapper.updateAccAccpByParms2(BillNo, addSecurityMoneyAmt, riskOpenMoneyAmt, newSecurityRate, riskOpenRate);
    }

    /**
     * 通过hxcontno查询区分票据池。如果是票据池产生备款，则将票状态置为结清
     *
     * @param hxcontno
     * @return
     */
    public List<AccAccp> selectAccAccpByContNo(String hxcontno) {
        return accAccpMapper.selectAccAccpByContNo(hxcontno);
    }

    /**
     * 如果在票据池中 ，则将票状态置为结清
     *
     * @param hxcontno
     * @return
     */
    public int updateAccAccpByhxcontno(String hxcontno) {
        return accAccpMapper.updateAccAccpByhxcontno(hxcontno);
    }

    /**
     * //成功备款金额=应备款金额
     *
     * @param hxcontno
     * @param openday  hxcontno
     * @return
     */
    public int updateAccAccpSucceed(String hxcontno, String openday) {
        return accAccpMapper.updateAccAccpSucceed(hxcontno, openday);
    }

    /**
     * 汇总有效银承台账敞口金额
     *
     * @param accAccpMap
     * @return
     */
    public BigDecimal querySpacAmtByAccAccpMap(Map accAccpMap) {
        return accAccpMapper.querySpacAmtByAccAccpMap(accAccpMap);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据根据核心银承编号回显银承台账信息
     * @参数与返回说明:
     * @算法描述: 无
     * liuquan
     */

    public AccAccpDto queryByCoreBillNo(AccAccp accAccp) {
        AccAccpDto accAccpDto = accAccpMapper.queryByCoreBillNo(accAccp);
        if (null != accAccpDto) {
            accAccpDto.setInputIdName(OcaTranslatorUtils.getUserName(accAccpDto.getInputId()));// 登记人
            accAccpDto.setInputBrIdName(OcaTranslatorUtils.getOrgName(accAccpDto.getInputBrId()));// 登记机构
            accAccpDto.setManagerIdName(OcaTranslatorUtils.getUserName(accAccpDto.getManagerId()));// 责任人
            accAccpDto.setManagerBrIdName(OcaTranslatorUtils.getOrgName(accAccpDto.getManagerBrId()));// 责任机构
        }
        return accAccpDto;
    }

    /**
     * 异步导出诉讼申请列表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccAccp(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("isseDate desc");
            String apiUrl = "/api/accaccp/exportAccAccp";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return selectAccAccpRepayByExcel(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccAccpVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    public List<AccAccpVo> selectAccAccpRepayByExcel(QueryModel model) {
        List<AccAccpVo> AccAccpVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccp> list = accAccpMapper.querymodelByCondition(model);
        PageHelper.clearPage();

        for (AccAccp accAccp : list) {
            AccAccpVo accAccpVo = new AccAccpVo();
            String orgName = OcaTranslatorUtils.getOrgName(accAccp.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(accAccp.getManagerId());   //责任人
            org.springframework.beans.BeanUtils.copyProperties(accAccp, accAccpVo);
            accAccpVo.setManagerBrIdName(orgName);
            accAccpVo.setManagerIdName(userName);
            AccAccpVoList.add(accAccpVo);
        }
        return AccAccpVoList;
    }


    /**
     * @方法名称: querymodelByCondition
     * @方法描述: 根据查询条件查询台账信息并返回
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccAccp> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccp> list = accAccpMapper.querymodelByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectSumLoanByTContNo
     * @方法描述: 根据根据购销合同查询台账总额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectSumLoanByTContNo(String tContNo) {
        return accAccpMapper.selectSumLoanByTContNo(tContNo);
    }

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public AccAccp selectByPvpSerno(String pvpSerno) {
        return accAccpMapper.selectByPvpSerno(pvpSerno);
    }

    /**
     * @方法名称: selectSumAmtByContNo
     * @方法描述: 计算台账余额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectSumAmtByContNo(String contNo) {
        return accAccpMapper.selectSumAmtByContNo(contNo);
    }

    /**
     * @方法名称：selecByBillContNo
     * @方法描述： 根据客户号灵活查询
     * @创建人：xs
     * @创建时间：2021/5/25 16:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model) {
        return accAccpMapper.selectInfoByCusIdDate(model);
    }

    /**
     * 根据合同编号查询用信敞口余额
     *
     * @param contNos
     * @return
     */
    public BigDecimal selectTotalSpacAmtByContNos(String contNos) {
        return accAccpMapper.selectTotalSpacAmtByContNos(contNos);
    }

    /**
     * 风险分类审批结束更新客户未结清银承台账五十级分类结果
     *
     * @author jijian_yx
     * @date 2021/10/25 23:24
     **/
    public int updateLoanFiveAndTenClassByCusId(Map<String, String> map) {
        return accAccpMapper.updateLoanFiveAndTenClassByCusId(map);
    }

    public int updateAccStatusByPvpSerno(String serno) {
        return accAccpMapper.updateAccStatusByPvpSerno(serno);
    }

    /**
     * 查询票据系统批次台账信息更新银承台账信息
     * @param accaccp
     * @return
     */
    public ResultDto<Map<String, String>> doSynchronize(AccAccp accaccp) {
        String code = "0";
        String msg ="同步成功";
        Map map = new HashMap<String,String>();
        // 票据总余额
        BigDecimal drftBalanceAmt = BigDecimal.ZERO;
        // 票据总敞口金额
        BigDecimal spacBalanceAmtCny = BigDecimal.ZERO;

        // 作废的票面总金额
        BigDecimal zfBillAmtTotal = BigDecimal.ZERO;
        // 作废的票面保证金金额
        BigDecimal zfBzjAmtTotal = BigDecimal.ZERO;

        // 是否结清(默认 结清)
        Boolean isaccStatus = true;
        try{
            // 发送xdpj22接口查询
            Xdpj22ReqDto reqDto = new Xdpj22ReqDto();
            reqDto.setSBatchNo(accaccp.getCoreBillNo());
            logger.info("发送xdpj22接口查询,请求参数:"+ Objects.toString(reqDto));
            ResultDto<Xdpj22RespDto> Xdpj22ResultDto = dscms2PjxtClientService.xdpj22(reqDto);
            logger.info("获取xdpj22接口查询,响应报文:"+ Objects.toString(Xdpj22ResultDto));
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(Xdpj22ResultDto.getCode())) {
                // 更新银承台账信息
                if(Objects.nonNull(Xdpj22ResultDto.getData())){
                    Xdpj22RespDto xdpj22RespDto = Xdpj22ResultDto.getData();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List> lists = xdpj22RespDto.getList();
                    if(CollectionUtils.nonEmpty(lists)){
                        for(cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List e :lists){
                            if(!Objects.equals("1",e.getChargeFlag())){
                                // 只要有一张票没结清, 台账状态就是未结清
                                isaccStatus = false;
                                if(!Objects.equals("CD_114",e.getStatusFlag()) && !Objects.equals("CD_115",e.getStatusFlag())
                                        && !Objects.equals("TY_002",e.getStatusFlag())&& !Objects.equals("TY_001",e.getStatusFlag())){
                                    // 作废和结清不计算
                                    BigDecimal fBillAmount = new BigDecimal(e.getfBillAmount());
                                    BigDecimal bigDecimal = new BigDecimal(e.getBailAmt());
                                    drftBalanceAmt = drftBalanceAmt.add(fBillAmount);
                                    spacBalanceAmtCny = spacBalanceAmtCny.add(fBillAmount.subtract(bigDecimal));
                                }else{
                                    //作废的票的 票面金额  票面敞口金额   累加
                                    BigDecimal zfBillAmt =  new BigDecimal(e.getfBillAmount());
                                    BigDecimal zfBzjAmt = new BigDecimal(e.getBailAmt());
                                    zfBillAmtTotal = zfBillAmtTotal.add(zfBillAmt);
                                    zfBzjAmtTotal = zfBillAmtTotal.multiply(accaccp.getBailPerc());
                                    zfBzjAmtTotal = zfBzjAmtTotal.setScale(2, BigDecimal.ROUND_HALF_UP);
                                    logger.info("银票批次【"+accaccp.getCoreBillNo()+"】同步：计算作废票据明细 zfBillAmtTotal 【"+zfBillAmtTotal+"】 累加 【"+zfBillAmt+"】 ");
                                }
                            }
                        };
                    }else{
                        throw BizException.error(null,"9999","票据系统获取数据失败");
                    }
                }else{
                    throw BizException.error(null,"9999","票据系统获取数据失败");
                }
                // 同步额度
                accaccp.setDrftBalanceAmt(drftBalanceAmt);
                accaccp.setSpacAmt(spacBalanceAmtCny); //票据敞口金额
                accaccp.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
                if(isaccStatus){
                    //判断是否结清
                    accaccp.setAccStatus("3");
                }

                String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());

                ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = null;

                //若作废的银票大于0，则计算更新原始敞口金额
                if(zfBillAmtTotal.compareTo(BigDecimal.ZERO) > 0){
                    //原始敞口金额 = (批次总金额 - 作废的票面金额 ) * (1-保证金比例)
                    // PS:20211119同李海华确认，追加保证金情况下保证金比例不变
                    logger.info("银票批次【"+accaccp.getCoreBillNo()+"】同步：因作废票金额>0，重新计算原始敞口金额：zfBillAmtTotal【"+zfBillAmtTotal+"】 zfBzjAmtTotal【"+zfBzjAmtTotal+"】 bailPerc【"+accaccp.getBailPerc()+"】 ");
                    BigDecimal ysckje = (accaccp.getDrftTotalAmt().subtract(zfBillAmtTotal) ).multiply((BigDecimal.valueOf(1).subtract(accaccp.getBailPerc())));
                    ysckje = ysckje.setScale(2, BigDecimal.ROUND_HALF_UP);
                    logger.info("银票批次【"+accaccp.getCoreBillNo()+"】同步：计算原始敞口金额得 ysckje 【"+ysckje.toString()+"】 ");
                    accaccp.setOrigiOpenAmt(ysckje);//更新银票原始敞口金额

                    BigDecimal yszje = accaccp.getDrftTotalAmt().subtract(zfBillAmtTotal);
                    cmisLmt0014RespDtoResultDto = sendCmisLmt0014WYTH(serno, accaccp, "08","01",yszje,ysckje);
                }else{
                    cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, accaccp, "03","01");
                }

                if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                        && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
                        && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                    // 占额成功，更新台账
                    accAccpMapper.updateByPrimaryKeySelective(accaccp);
                    code = "0";
                    msg = "同步成功";
                } else {
                    throw BizException.error(null,"9999","额度恢复异常");
                }
            }else{
                throw BizException.error(null,"9999","票据系统获取数据失败");
            }
        }catch (BizException e){
            code = e.getErrorCode();
            msg = e.getMessage();
            throw BizException.error(null,e.getErrorCode(),"【银承台账同步】:"+e.getMessage());
        }finally{
            return new ResultDto(map).code(code).message(msg);
        }
    }
    /**
     * 发送台账恢复接口
     * @param serno
     * @param accAccp
     */
    private ResultDto<CmisLmt0014RespDto> sendCmisLmt0014(String serno, AccAccp accAccp, String recoverType,String recoverStd) {
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accAccp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(serno);//交易流水号
        cmisLmt0014ReqDto.setInputId(accAccp.getInputId());//登记人
        cmisLmt0014ReqDto.setInputBrId(accAccp.getInputBrId());//登记机构
        cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//登记日期

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(accAccp.getCoreBillNo());//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(recoverType);
        cmisLmt0014ReqdealBizListDto.setRecoverStd(recoverStd);// 恢复标准值
        cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(accAccp.getDrftBalanceAmt());// 银承台账余额
        // 判断有无敞口金额 21低风险质押 40全额保证金
        if(!Objects.equals("21",accAccp.getGuarMode()) && !Objects.equals("40",accAccp.getGuarMode())){
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(accAccp.getSpacAmt());// 台账敞口余额（人民币）
        }else{
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);// 台账敞口余额（人民币）
        }
        cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
        logger.info("银票台账同步" + serno + "，前往额度系统恢复占用额度开始,参数" + JSON.toJSONString(cmisLmt0014ReqDto));
        ResultDto<CmisLmt0014RespDto> resultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        logger.info("银票台账同步" + serno + "，前往额度系统恢复占用额度结束,参数" + JSON.toJSONString(resultDto));
        return resultDto;
    }

    /**
     * 发送台账恢复接口(未用退回)
     * @param serno
     * @param accAccp
     */
    private ResultDto<CmisLmt0014RespDto> sendCmisLmt0014WYTH(String serno, AccAccp accAccp, String recoverType,String recoverStd,BigDecimal yszje,BigDecimal ysckje) {
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accAccp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(serno+"WYTH");//交易流水号
        cmisLmt0014ReqDto.setInputId(accAccp.getInputId());//登记人
        cmisLmt0014ReqDto.setInputBrId(accAccp.getInputBrId());//登记机构
        cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//登记日期

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(accAccp.getCoreBillNo());//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(recoverType);
        cmisLmt0014ReqdealBizListDto.setRecoverStd(recoverStd);// 恢复标准值
        cmisLmt0014ReqdealBizListDto.setBalanceAmtCny(accAccp.getDrftBalanceAmt());// 银承台账余额
        cmisLmt0014ReqdealBizListDto.setCreditTotal(yszje);//原始占用金额
        // 判断有无敞口金额 21低风险质押 40全额保证金
        if(!Objects.equals("21",accAccp.getGuarMode()) && !Objects.equals("40",accAccp.getGuarMode())){
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(accAccp.getSpacAmt());// 台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setCreditSpacTotal(ysckje);//原始敞口金额
        }else{
            cmisLmt0014ReqdealBizListDto.setSpacBalanceAmtCny(BigDecimal.ZERO);// 台账敞口余额（人民币）
            cmisLmt0014ReqdealBizListDto.setCreditSpacTotal(BigDecimal.ZERO);//原始敞口金额
        }
        cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
        logger.info("银票台账同步（未用退回）" + serno + "，前往额度系统恢复占用额度开始,参数" + JSON.toJSONString(cmisLmt0014ReqDto));
        ResultDto<CmisLmt0014RespDto> resultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        logger.info("银票台账同步（未用退回）" + serno + "，前往额度系统恢复占用额度结束,参数" + JSON.toJSONString(resultDto));
        return resultDto;
    }
}
