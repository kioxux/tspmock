/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtAppRelGuar;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppRelGuarMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelGuarService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-18 19:01:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppRelGuarService extends BizInvestCommonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LmtAppRelGuarService.class);

    @Autowired
    private LmtAppRelGuarMapper lmtAppRelGuarMapper;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    @Autowired
    private LmtSigInvestRelFinaInfoService lmtSigInvestRelFinaInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtAppRelGuar selectByPrimaryKey(String pkId) {
        return lmtAppRelGuarMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtAppRelGuar> selectAll(QueryModel model) {
        List<LmtAppRelGuar> records = (List<LmtAppRelGuar>) lmtAppRelGuarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtAppRelGuar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        /**
         * 增加列表类型判断
         * 01:担保人/增信人信息
         * 02:抵质押品信息
         */
        String listType = (String) model.getCondition().get("listType");
        if (!StringUtils.isBlank(listType)) {
            if ("01".equals(listType)) {
                List<String> guarTypes = new ArrayList<>();
                guarTypes.add(CmisBizConstants.STD_ZB_ASSURE_MEANS.BAO_ZHENG);
                model.addCondition("guarTypes", guarTypes);
            }
            if ("02".equals(listType)) {
                List<String> guarTypes = new ArrayList<>();
                guarTypes.add(CmisBizConstants.STD_ZB_ASSURE_MEANS.DI_YA);
                guarTypes.add(CmisBizConstants.STD_ZB_ASSURE_MEANS.ZHI_YA);
                model.addCondition("guarTypes", guarTypes);
            }
        }
        List<LmtAppRelGuar> list = lmtAppRelGuarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtAppRelGuar record) {
        record.setCreateTime(new Date());
        record.setInputBrId(getCurrentUser().getOrg().getCode());
        record.setInputId(getCurrentUser().getLoginCode());
        record.setInputDate(DateUtils.getCurrDateStr());
        record.setUpdateTime(new Date());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdId(getCurrentUser().getLoginCode());
        return lmtAppRelGuarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtAppRelGuar record) {
        return lmtAppRelGuarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtAppRelGuar record) {
        record.setUpdateTime(new Date());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdId(getCurrentUser().getLoginCode());
        return lmtAppRelGuarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtAppRelGuar record) {
        return lmtAppRelGuarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppRelGuarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppRelGuarMapper.deleteByIds(ids);
    }

    /**
     * 逻辑删除
     * 1.删除增新人、担保人信息表
     * 2.删除关联客户申请表
     *
     * @param pkId
     * @return
     */
    @Transactional
    public int deleteLogicByPkId(String pkId) {
        int result = 1;
        LmtAppRelGuar lmtAppRelGuar = selectByPrimaryKey(pkId);
        if (lmtAppRelGuar != null) {
            LOGGER.info("【{}】删除担保人信息【{}-{}】",lmtAppRelGuar.getSerno(),lmtAppRelGuar.getGrtCusId(),lmtAppRelGuar.getGrtCusName());
            lmtAppRelGuar.setOprType(CmisBizConstants.OPR_TYPE_02);
            result = update(lmtAppRelGuar);
            if (result == 1 && CmisBizConstants.STD_ZB_ASSURE_MEANS.BAO_ZHENG.equals(lmtAppRelGuar.getGuarType())) {
                result = lmtAppRelCusInfoService.deleteLogic(lmtAppRelGuar.getGrtCusId(),lmtAppRelGuar.getSerno());
            }
        }
        return result;
    }

    /**
     * 根据serno删除当前申请的担保人信息
     * @param serno
     */
    public void deleteLogicBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtAppRelGuar> lmtAppRelGuars = selectAll(queryModel);
        if (CollectionUtils.isEmpty(lmtAppRelGuars)){
            return;
        }
        for (LmtAppRelGuar lmtAppRelGuar : lmtAppRelGuars) {
            LOGGER.info("【{}】删除担保人信息【{}-{}】",serno,lmtAppRelGuar.getGrtCusId(),lmtAppRelGuar.getGrtCusName());
            lmtAppRelGuar.setOprType(CmisBizConstants.OPR_TYPE_02);
            update(lmtAppRelGuar);
            if (CmisBizConstants.STD_ZB_ASSURE_MEANS.BAO_ZHENG.equals(lmtAppRelGuar.getGuarType())){
                lmtAppRelCusInfoService.deleteLogic(lmtAppRelGuar.getGrtCusId(), serno);
            }
        }
    }



    /**
     * 担保人增新人信息 <br/>
     * 增加限制 <br/>
     * 1.添加担保人增新人信息（lmt_app_rel_guar） <br/>
     * 2.添加关联客户信息表：（lmt_app_rel_cus_info）（担保类型为保证）(担保人信息) <br/>
     * 3.添加申请关联股东表（lmt_app_corre_shd）（担保人信息-股东表） <br/>
     * 4.添加授信主体及增信人财务信息总表（lmt_sig_invest_rel_fina_info） <br/>
     * 5.添加授信主体及增信人财务信息明细表（lmt_sig_invest_rel_fina_details） <br/>
     *
     * @param record
     * @param needRelFinaInfo 是否添加主题分析
     * @return
     */
    @Transactional
    public int insertLmtAppRelGuar(LmtAppRelGuar record, boolean needRelFinaInfo) {
        int result = 1;
        try {
            //判断当前添加类型是否为保证
            if (CmisBizConstants.STD_ZB_ASSURE_MEANS.BAO_ZHENG.equals(record.getGuarType())) {
                //检测当前所选择客户是否存在（包含主体授信关联客户）
                checkIsExist(record.getSerno(), record.getGrtCusId());
                //添加担保人相关信息
                //TODO 添加企业相关信息（lmt_app_rel_cus_info、lmt_app_corre_shd、lmt_sig_invest_rel_fina_info（授信主体及增信人财务信息总表））
                result = lmtAppRelCusInfoService.insertCusInfoApp(
                        record.getSerno(), record.getGrtCusId(), record.getCusType(), record.getCusCatalog(), needRelFinaInfo);
            }
            if (result == 1) {
                //添加增新人\担保人信息
                result = insert(record);
            }
        } catch (BizException be) {
            // 判断是否为BizException 不进行拦截
            LOGGER.error(be.getMessage(), be);
            throw be;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_ASSURE_FAILED.key, EclEnum.LMT_SIG_INVESTAPP_ASSURE_FAILED.value);
        }
        return result;
    }

    /**
     * 检测当前所选择客户是否存在（包含主体授信关联客户）
     *
     * @param serno
     */
    private void checkIsExist(String serno, String grtCusId) {
        LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.selectBySerno(serno);
        if (lmtSigInvestApp != null) {
            //不能选择当前授信关联客户
            if (lmtSigInvestApp.getCusId().equals(grtCusId)) {
                throw BizException.error(null, EclEnum.LMT_CALCULATE_DANBAOREN_ERROR_1.key, EclEnum.LMT_CALCULATE_DANBAOREN_ERROR_1.value);
            }
            //判断当前所选择客户是否存在
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", serno);
            BizInvestCommonService.checkParamsIsNull("serno",serno);
            queryModel.addCondition("grtCusId", grtCusId);
            BizInvestCommonService.checkParamsIsNull("grtCusId",grtCusId);
            queryModel.addCondition("guarType", CmisBizConstants.STD_ZB_ASSURE_MEANS.BAO_ZHENG);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
            List<LmtAppRelGuar> lmtAppRelGuars = selectAll(queryModel);
            if (lmtAppRelGuars != null && lmtAppRelGuars.size() > 0) {
                throw BizException.error(null, EclEnum.LMT_CALCULATE_DANBAOREN_ERROR_2.key, EclEnum.LMT_CALCULATE_DANBAOREN_ERROR_2.value);
            }
        }
    }

    /**
     * 获取主体批复中担保方式
     *
     * @param serno
     * @return
     */
    public Map<String, Object> selectGuarTypeName(String serno) {
        return lmtAppRelGuarMapper.selectGuarTypeName(serno);
    }

    /**
     * 拷贝原授信中的增信人、担保人 信息
     *
     * @param origiSerno
     * @param serno
     */
    public void copyLmtAppRelGuarList(String origiSerno, String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", origiSerno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        //获取关联担保人增新人记录
        List<LmtAppRelGuar> lmtAppRelGuars = selectByModel(queryModel);
        if (lmtAppRelGuars != null && lmtAppRelGuars.size() > 0) {
            for (LmtAppRelGuar lmtAppRelGuar : lmtAppRelGuars) {
                LmtAppRelGuar newLmtAppRelGuar = new LmtAppRelGuar();
                //复制对象属性值
                copyProperties(lmtAppRelGuar, newLmtAppRelGuar);
                //初始化(新增)通用domain信息
                initInsertDomainProperties(newLmtAppRelGuar);
                //设置新申请流水号
                newLmtAppRelGuar.setSerno(serno);
                //保存增新 、担保 信息
                insertLmtAppRelGuar(newLmtAppRelGuar, false);
                //拷贝原主体分析
                lmtSigInvestRelFinaInfoService.copyRelFinaInfo(origiSerno, serno, newLmtAppRelGuar.getGrtCusId());
            }
        }
    }

}
