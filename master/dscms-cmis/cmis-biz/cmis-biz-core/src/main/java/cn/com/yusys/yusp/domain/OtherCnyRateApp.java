/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateApp
 * @类描述: other_cny_rate_app数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-06 15:20:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_app")
public class OtherCnyRateApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 所属行业 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 企业规模 **/
	@Column(name = "CORP_SCALE", unique = false, nullable = true, length = 5)
	private String corpScale;
	
	/** 主营业务 **/
	@Column(name = "MAIN_OPT_SCP", unique = false, nullable = true, length = 2000)
	private String mainOptScp;
	
	/** 申请理由 **/
	@Column(name = "APP_RESN", unique = false, nullable = true, length = 2000)
	private String appResn;
	
	/** 定价申请类型 **/
	@Column(name = "RATE_APP_TYPE", unique = false, nullable = true, length = 40)
	private String rateAppType;
	
	/** 上期授信金额 **/
	@Column(name = "PRE_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preLmtAmt;
	
	/** 上期其中贷款 **/
	@Column(name = "PRE_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preLoanAmt;
	
	/** 本期授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 本期其中贷款 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 他行融资总额 **/
	@Column(name = "OTHER_BANK_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherBankLmtAmt;
	
	/** 他行其中贷款 **/
	@Column(name = "OTHER_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherLoanAmt;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 业务类型（特殊需要） **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale;
	}
	
    /**
     * @return corpScale
     */
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param mainOptScp
	 */
	public void setMainOptScp(String mainOptScp) {
		this.mainOptScp = mainOptScp;
	}
	
    /**
     * @return mainOptScp
     */
	public String getMainOptScp() {
		return this.mainOptScp;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}
	
    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param rateAppType
	 */
	public void setRateAppType(String rateAppType) {
		this.rateAppType = rateAppType;
	}
	
    /**
     * @return rateAppType
     */
	public String getRateAppType() {
		return this.rateAppType;
	}
	
	/**
	 * @param preLmtAmt
	 */
	public void setPreLmtAmt(java.math.BigDecimal preLmtAmt) {
		this.preLmtAmt = preLmtAmt;
	}
	
    /**
     * @return preLmtAmt
     */
	public java.math.BigDecimal getPreLmtAmt() {
		return this.preLmtAmt;
	}
	
	/**
	 * @param preLoanAmt
	 */
	public void setPreLoanAmt(java.math.BigDecimal preLoanAmt) {
		this.preLoanAmt = preLoanAmt;
	}
	
    /**
     * @return preLoanAmt
     */
	public java.math.BigDecimal getPreLoanAmt() {
		return this.preLoanAmt;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param otherBankLmtAmt
	 */
	public void setOtherBankLmtAmt(java.math.BigDecimal otherBankLmtAmt) {
		this.otherBankLmtAmt = otherBankLmtAmt;
	}
	
    /**
     * @return otherBankLmtAmt
     */
	public java.math.BigDecimal getOtherBankLmtAmt() {
		return this.otherBankLmtAmt;
	}
	
	/**
	 * @param otherLoanAmt
	 */
	public void setOtherLoanAmt(java.math.BigDecimal otherLoanAmt) {
		this.otherLoanAmt = otherLoanAmt;
	}
	
    /**
     * @return otherLoanAmt
     */
	public java.math.BigDecimal getOtherLoanAmt() {
		return this.otherLoanAmt;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}
}