package cn.com.yusys.yusp.web.server.xdht0032;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0032.req.Xdht0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0032.resp.Xdht0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0032.Xdht0032Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据核心客户号查询我行信用类合同金额汇总
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0032:根据核心客户号查询我行信用类合同金额汇总")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0032Resource.class);
    @Autowired
    private Xdht0032Service xdht0032Service;

    /**
     * 交易码：xdht0032
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0032DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询我行信用类合同金额汇总")
    @PostMapping("/xdht0032")
    protected @ResponseBody
    ResultDto<Xdht0032DataRespDto> xdht0032(@Validated @RequestBody Xdht0032DataReqDto xdht0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataReqDto));
        Xdht0032DataRespDto xdht0032DataRespDto = new Xdht0032DataRespDto();// 响应Dto:根据核心客户号查询我行信用类合同金额汇总
        ResultDto<Xdht0032DataRespDto> xdht0032DataResultDto = new ResultDto<>();
        try {
            // 从xdht0032DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0032DataReqDto));
            xdht0032DataRespDto = xdht0032Service.xdht0032(xdht0032DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0032DataResultDto));
            // 封装xdht0032DataResultDto中正确的返回码和返回信息
            xdht0032DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0032DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, e.getMessage());
            // 封装xdht0032DataResultDto中异常返回码和返回信息
            xdht0032DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0032DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0032DataRespDto到xdht0032DataResultDto中
        xdht0032DataResultDto.setData(xdht0032DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataResultDto));
        return xdht0032DataResultDto;
    }
}