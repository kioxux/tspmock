/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtRediDetail;
import cn.com.yusys.yusp.domain.MajorGrade;
import cn.com.yusys.yusp.service.BizInvestCommonService;
import cn.com.yusys.yusp.service.LmtAppService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReconsideDetail;
import cn.com.yusys.yusp.service.LmtReconsideDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReconsideDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-10 00:27:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreconsidedetail")
public class LmtReconsideDetailResource {
    @Autowired
    private LmtReconsideDetailService lmtReconsideDetailService;

    @Autowired
    private BizInvestCommonService bizInvestCommonService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReconsideDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReconsideDetail> list = lmtReconsideDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtReconsideDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReconsideDetail>> index(QueryModel queryModel) {
        List<LmtReconsideDetail> list = lmtReconsideDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtReconsideDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReconsideDetail> show(@PathVariable("pkId") String pkId) {
        LmtReconsideDetail lmtReconsideDetail = lmtReconsideDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReconsideDetail>(lmtReconsideDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReconsideDetail> create(@RequestBody LmtReconsideDetail lmtReconsideDetail) throws URISyntaxException {
        lmtReconsideDetailService.insert(lmtReconsideDetail);
        return new ResultDto<LmtReconsideDetail>(lmtReconsideDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReconsideDetail lmtReconsideDetail) throws URISyntaxException {
        int result = lmtReconsideDetailService.update(lmtReconsideDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReconsideDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReconsideDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtReconsideDetail> save(@RequestBody LmtReconsideDetail lmtReconsideDetail) throws URISyntaxException {
        LmtReconsideDetail lmtReconsideDetail1 = lmtReconsideDetailService.selectByLmtSerno(lmtReconsideDetail.getLmtSerno());
        lmtReconsideDetail.setOprType("01");
        if(lmtReconsideDetail1 != null){
            lmtReconsideDetailService.updateByLmtSerno(lmtReconsideDetail);
        }else{
            lmtReconsideDetailService.insert(lmtReconsideDetail);
        }
        return ResultDto.success(lmtReconsideDetail);
    }

    /**
     * @函数名称:show
     * @函数描述:进入页面查询数据返回前台
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtReconsideDetail> selectBySerno(@RequestBody String lmtSerno) {
        LmtReconsideDetail lmtReconsideDetail = lmtReconsideDetailService.selectByLmtSerno(lmtSerno);
        return  ResultDto.success(lmtReconsideDetail);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetailByLmtSerno")
    protected ResultDto<List<Map<String,String>>> queryDetailByLmtSerno(@RequestBody Map<String,String> map) {
        String lmtSerno = map.get("lmtSerno");
        List<Map<String,String>> lmtReconsideDsetails = lmtReconsideDetailService.queryDetailByLmtSerno(lmtSerno);
        return  ResultDto.success(lmtReconsideDsetails);
    }


    /**
     * 根据pkId更新授信复议申请书
     * @param lmtReconsideDetail
     * @return
     */
    @PostMapping("/updateFysqb")
    protected ResultDto<Integer> updateFysqb(@RequestBody LmtReconsideDetail lmtReconsideDetail){
        bizInvestCommonService.updateMustCheckStatus(lmtReconsideDetail.getLmtSerno(),"fysqb");
        int result = lmtReconsideDetailService.update(lmtReconsideDetail);
        return new ResultDto<>(result);
    }


}
