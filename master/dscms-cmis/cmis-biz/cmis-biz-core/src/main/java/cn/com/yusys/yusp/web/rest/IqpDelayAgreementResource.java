/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDelayAgreement;
import cn.com.yusys.yusp.service.IqpDelayAgreementService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDelayAgreementResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 15:07:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpdelayagreement")
public class IqpDelayAgreementResource {
    @Autowired
    private IqpDelayAgreementService iqpDelayAgreementService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDelayAgreement>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDelayAgreement> list = iqpDelayAgreementService.selectAll(queryModel);
        return new ResultDto<List<IqpDelayAgreement>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpDelayAgreement>> index(@RequestBody QueryModel queryModel) {
        List<IqpDelayAgreement> list = iqpDelayAgreementService.selectByModel(queryModel);
        return new ResultDto<List<IqpDelayAgreement>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{agrSerno}")
    protected ResultDto<IqpDelayAgreement> show(@PathVariable("agrSerno") String agrSerno) {
        IqpDelayAgreement iqpDelayAgreement = iqpDelayAgreementService.selectByPrimaryKey(agrSerno);
        return new ResultDto<IqpDelayAgreement>(iqpDelayAgreement);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpDelayAgreement> create(@RequestBody IqpDelayAgreement iqpDelayAgreement) throws URISyntaxException {
        iqpDelayAgreementService.insert(iqpDelayAgreement);
        return new ResultDto<IqpDelayAgreement>(iqpDelayAgreement);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpDelayAgreement iqpDelayAgreement) throws URISyntaxException {
        int result = iqpDelayAgreementService.update(iqpDelayAgreement);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{agrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("agrSerno") String agrSerno) {
        int result = iqpDelayAgreementService.deleteByPrimaryKey(agrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDelayAgreementService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
