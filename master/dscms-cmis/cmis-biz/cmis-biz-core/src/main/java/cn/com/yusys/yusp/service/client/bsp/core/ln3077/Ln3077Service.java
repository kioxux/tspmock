package cn.com.yusys.yusp.service.client.bsp.core.ln3077;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.req.Ln3077ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.resp.Ln3077RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/5/21 10:33
 * @desc 客户账本息调整
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ln3077Service {
    private static final Logger logger = LoggerFactory.getLogger(Ln3077Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param ln3077ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3077.Ln3077RespDto
     * @author 王玉坤
     * @date 2021/8/27 22:44
     * @version 1.0.0
     * @desc 客户账本息调整
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Ln3077RespDto ln3077(Ln3077ReqDto ln3077ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value, JSON.toJSONString(ln3077ReqDto));
        ResultDto<Ln3077RespDto> ln3077ResultDto = dscms2CoreLnClientService.ln3077(ln3077ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value, JSON.toJSONString(ln3077ResultDto));
        String ln3077Code = Optional.ofNullable(ln3077ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3077Meesage = Optional.ofNullable(ln3077ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3077RespDto ln3077RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3077ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3077RespDto = ln3077ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(ln3077Code, ln3077Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value);
        return ln3077RespDto;
    }
}
