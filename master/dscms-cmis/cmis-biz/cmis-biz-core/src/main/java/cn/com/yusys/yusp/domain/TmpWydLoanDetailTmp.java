/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydLoanDetail
 * @类描述: tmp_wyd_loan_detail数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_loan_detail_tmp")
public class TmpWydLoanDetailTmp {
	
	/** 借据编号 **/
	@Id
	@Column(name = "LENDING_REF")
	private String lendingRef;

	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 科目号 **/
	@Column(name = "IETM_CD", unique = false, nullable = true, length = 20)
	private String ietmCd;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 合同编号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 64)
	private String contractNo;
	
	/** 申请号 **/
	@Column(name = "APPLY_NO", unique = false, nullable = true, length = 64)
	private String applyNo;
	
	/** 贷款种类 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 20)
	private String loanType;
	
	/** 是否涉农贷款 **/
	@Column(name = "AGR_REL_FLG", unique = false, nullable = true, length = 1)
	private String agrRelFlg;
	
	/** 是否贴息贷款 **/
	@Column(name = "SUBSIDIZED_FLG", unique = false, nullable = true, length = 1)
	private String subsidizedFlg;
	
	/** 涉农贷款性质 **/
	@Column(name = "AGR_REL_PROP", unique = false, nullable = true, length = 10)
	private String agrRelProp;
	
	/** 支农贷款类型 **/
	@Column(name = "AGR_SPT_TYPE", unique = false, nullable = true, length = 10)
	private String agrSptType;
	
	/** 房地产类型 **/
	@Column(name = "REAL_ESTATE_TYPE", unique = false, nullable = true, length = 10)
	private String realEstateType;
	
	/** 下岗失业人员小额贷款类型 **/
	@Column(name = "LAIDOFF_LOAN_TYPE", unique = false, nullable = true, length = 10)
	private String laidoffLoanType;
	
	/** 客户号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 40)
	private String custId;
	
	/** 客户证件类型 **/
	@Column(name = "CUST_IDTYPE", unique = false, nullable = true, length = 20)
	private String custIdtype;
	
	/** 客户证件号码 **/
	@Column(name = "CUST_IDNO", unique = false, nullable = true, length = 30)
	private String custIdno;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 60)
	private String custName;
	
	/** 投向行业 **/
	@Column(name = "LOAN_PURPOSE", unique = false, nullable = true, length = 10)
	private String loanPurpose;
	
	/** 账户状态 **/
	@Column(name = "CARD_STATE", unique = false, nullable = true, length = 10)
	private String cardState;
	
	/** 发放日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String maturityDate;
	
	/** 约定到期日期 **/
	@Column(name = "SCH_MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String schMaturityDate;
	
	/** 宽限期 **/
	@Column(name = "GRACE_PERIOD", unique = false, nullable = true, length = 11)
	private String gracePeriod;
	
	/** 执行利率 **/
	@Column(name = "RATE", unique = false, nullable = true, length = 12)
	private String rate;
	
	/** 基准利率 **/
	@Column(name = "BASE_RATE", unique = false, nullable = true, length = 12)
	private String baseRate;
	
	/** 币种 **/
	@Column(name = "CCY_CD", unique = false, nullable = true, length = 10)
	private String ccyCd;
	
	/** 发放金额 **/
	@Column(name = "AMOUNT", unique = false, nullable = true, length = 20)
	private String amount;
	
	/** 贷款余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 20)
	private String balance;
	
	/** 还款频率 **/
	@Column(name = "PAYMENT_FEQ", unique = false, nullable = true, length = 10)
	private String paymentFeq;
	
	/** 支付方式 **/
	@Column(name = "PAY_WAY", unique = false, nullable = true, length = 10)
	private String payWay;
	
	/** 下一利率重定价日 **/
	@Column(name = "REPRICING_DATE", unique = false, nullable = true, length = 10)
	private String repricingDate;
	
	/** 利率类型 **/
	@Column(name = "RATE_TYPE", unique = false, nullable = true, length = 10)
	private String rateType;
	
	/** 逾期分类 **/
	@Column(name = "OVERDUE_TYPE", unique = false, nullable = true, length = 10)
	private String overdueType;
	
	/** 逾期天数 **/
	@Column(name = "OVERDUE_DAYS", unique = false, nullable = true, length = 11)
	private String overdueDays;
	
	/** 计息标志 **/
	@Column(name = "INTR_TYP", unique = false, nullable = true, length = 1)
	private String intrTyp;
	
	/** 应收利息 **/
	@Column(name = "INTEREST", unique = false, nullable = true, length = 20)
	private String interest;
	
	/** 本金逾期日期 **/
	@Column(name = "PRIN_OD_DATE", unique = false, nullable = true, length = 10)
	private String prinOdDate;
	
	/** 欠本金额 **/
	@Column(name = "PRIN_OD_AMT", unique = false, nullable = true, length = 20)
	private String prinOdAmt;
	
	/** 利息逾期日期 **/
	@Column(name = "INT_OD_DATE", unique = false, nullable = true, length = 10)
	private String intOdDate;
	
	/** 欠息金额 **/
	@Column(name = "INT_OD_AMT", unique = false, nullable = true, length = 20)
	private String intOdAmt;
	
	/** 应收贴息 **/
	@Column(name = "SUBSIDIZED_INT", unique = false, nullable = true, length = 20)
	private String subsidizedInt;
	
	/** 实收贴息 **/
	@Column(name = "ACT_SUBSIDIZED_INT", unique = false, nullable = true, length = 20)
	private String actSubsidizedInt;
	
	/** 贷款资金来源 **/
	@Column(name = "FUND_SOURCE", unique = false, nullable = true, length = 10)
	private String fundSource;
	
	/** 是否展期 **/
	@Column(name = "EXTENSION_FLG", unique = false, nullable = true, length = 1)
	private String extensionFlg;
	
	/** 展期金额 **/
	@Column(name = "EXTENSION_AMT", unique = false, nullable = true, length = 20)
	private String extensionAmt;
	
	/** 展期起始日期 **/
	@Column(name = "EXTENSION_START", unique = false, nullable = true, length = 10)
	private String extensionStart;
	
	/** 展期到期日期 **/
	@Column(name = "EXTENSION_MATURITY", unique = false, nullable = true, length = 10)
	private String extensionMaturity;
	
	/** 是否重组贷款 **/
	@Column(name = "RECOM_FLG", unique = false, nullable = true, length = 1)
	private String recomFlg;
	
	/** 重组日期 **/
	@Column(name = "RECOM_DATE", unique = false, nullable = true, length = 10)
	private String recomDate;
	
	/** 保证金金额 **/
	@Column(name = "BOND_AMT", unique = false, nullable = true, length = 20)
	private String bondAmt;
	
	/** 资本金比例 **/
	@Column(name = "CAPITAL_FUND", unique = false, nullable = true, length = 12)
	private String capitalFund;
	
	/** 已有住房套数 **/
	@Column(name = "HOUSE_NUM", unique = false, nullable = true, length = 11)
	private String houseNum;
	
	/** 月物业费 **/
	@Column(name = "TENEMENT_FEE", unique = false, nullable = true, length = 20)
	private String tenementFee;
	
	/** 是否个人住房抵押追加贷款 **/
	@Column(name = "PERSON_ADD_LOAN_FLG", unique = false, nullable = true, length = 1)
	private String personAddLoanFlg;
	
	/** 是否经营性物业贷款 **/
	@Column(name = "MANAGEMENT_FLAG", unique = false, nullable = true, length = 1)
	private String managementFlag;
	
	/** 铝冶炼细分 **/
	@Column(name = "SMELT_TYPE", unique = false, nullable = true, length = 1)
	private String smeltType;
	
	/** 战略新兴产业类型 **/
	@Column(name = "STRATEGY_TYPE", unique = false, nullable = true, length = 10)
	private String strategyType;
	
	/** 工作转型升级标识 **/
	@Column(name = "UPGRADE_FLG", unique = false, nullable = true, length = 1)
	private String upgradeFlg;
	
	/** 一般减值准备 **/
	@Column(name = "GENERAL_RESERVE", unique = false, nullable = true, length = 20)
	private String generalReserve;
	
	/** 特殊减值准备 **/
	@Column(name = "SPECIAL_PREP", unique = false, nullable = true, length = 20)
	private String specialPrep;
	
	/** 专项减值准备 **/
	@Column(name = "PRE_SPE", unique = false, nullable = true, length = 20)
	private String preSpe;
	
	/** 减值准备 **/
	@Column(name = "RESERVE", unique = false, nullable = true, length = 20)
	private String reserve;
	
	/** 购买住房面积 **/
	@Column(name = "HOUSE_BUY_COUNT", unique = false, nullable = true, length = 10)
	private String houseBuyCount;
	
	/** 贷款资金使用位置 **/
	@Column(name = "USED_LOCAT", unique = false, nullable = true, length = 10)
	private String usedLocat;
	
	/** 利率浮动类型 **/
	@Column(name = "RATE_FLOAT_TYPE", unique = false, nullable = true, length = 10)
	private String rateFloatType;
	
	/** 担保方式 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 10)
	private String guarType;
	
	/** 原始期限_月 **/
	@Column(name = "ORIGINAL_MATURITY_M", unique = false, nullable = true, length = 5)
	private String originalMaturityM;
	
	/** 原始期限_日 **/
	@Column(name = "ORIGINAL_MATURITY_D", unique = false, nullable = true, length = 5)
	private String originalMaturityD;
	
	/** 剩余期限_月 **/
	@Column(name = "REMAINING_MATURITY_M", unique = false, nullable = true, length = 5)
	private String remainingMaturityM;
	
	/** 剩余期限_日 **/
	@Column(name = "REMAINING_MATURITY_D", unique = false, nullable = true, length = 5)
	private String remainingMaturityD;
	
	/** 年初五级分类 **/
	@Column(name = "BEGIN_LOAN_GRADE", unique = false, nullable = true, length = 10)
	private String beginLoanGrade;
	
	/** 逾期总金额 **/
	@Column(name = "P_OVERDUE_AMT", unique = false, nullable = true, length = 20)
	private String pOverdueAmt;
	
	/** 账龄 **/
	@Column(name = "AGE_CD", unique = false, nullable = true, length = 32)
	private String ageCd;
	
	/** 撤销日期 **/
	@Column(name = "P_CANCEL_DATE", unique = false, nullable = true, length = 10)
	private String pCancelDate;
	
	/** 总期数 **/
	@Column(name = "P_INIT_TERM", unique = false, nullable = true, length = 11)
	private String pInitTerm;
	
	/** 利率重定价期限_月 **/
	@Column(name = "REPRICING_MATURITY_M", unique = false, nullable = true, length = 5)
	private String repricingMaturityM;
	
	/** 利率重定价期限_日 **/
	@Column(name = "REPRICING_MATURITY_D", unique = false, nullable = true, length = 5)
	private String repricingMaturityD;
	
	/** 文化产业标识 **/
	@Column(name = "CULTURE_SIGN", unique = false, nullable = true, length = 1)
	private String cultureSign;
	
	/** 入账日期 **/
	@Column(name = "ACTIVATE_DATE", unique = false, nullable = true, length = 10)
	private String activateDate;
	
	/** 本期应还款日期 **/
	@Column(name = "PMT_DUE_DATE", unique = false, nullable = true, length = 10)
	private String pmtDueDate;
	
	/** 银团编号 **/
	@Column(name = "TERMINATE_DATE", unique = false, nullable = true, length = 10)
	private String bankGroupId;
	
	/** 终止日期 **/
	@Column(name = "TERMINATE_DATE", unique = false, nullable = true, length = 10)

	private String terminateDate;

	/** 相关业务编号 **/
	@Column(name = "SER_NO", unique = false, nullable = true, length = 32)
	private String serNo;
	
	/** 保险代偿标志 **/
	@Column(name = "INSURANCE_PAYMENT_FLAG", unique = false, nullable = true, length = 1)
	private String insurancePaymentFlag;
	
	/** 保险代偿日期 **/
	@Column(name = "INSURANCE_PAYMENT_DATE", unique = false, nullable = true, length = 10)
	private String insurancePaymentDate;
	
	/** 保险代偿本金 **/
	@Column(name = "INSURANCE_PAYMENT_PRIN", unique = false, nullable = true, length = 20)
	private String insurancePaymentPrin;
	
	/** 保险代偿利息 **/
	@Column(name = "INSURANCE_PAYMENT_FEE", unique = false, nullable = true, length = 20)
	private String insurancePaymentFee;
	
	/** 终止原因 **/
	@Column(name = "TERMINATE_REASON_CD", unique = false, nullable = true, length = 10)
	private String terminateReasonCd;
	
	/** 资管计划no **/
	@Column(name = "ASSET_PLAN_NO", unique = false, nullable = true, length = 32)
	private String assetPlanNo;
	
	/** 合作机构 **/
	@Column(name = "ASSET_TRANSFER_ORG", unique = false, nullable = true, length = 32)
	private String assetTransferOrg;
	
	/** 资产转让金额 **/
	@Column(name = "ASSET_TRANSFER_AMT", unique = false, nullable = true, length = 20)
	private String assetTransferAmt;
	
	/** 资产转让标记 **/
	@Column(name = "ASSET_TRANSFER_FLAG", unique = false, nullable = true, length = 10)
	private String assetTransferFlag;
	
	/** 资产转让日期 **/
	@Column(name = "ASSET_TRANSFER_DATE", unique = false, nullable = true, length = 10)
	private String assetTransferDate;
	
	/** 应计利息冲抵金额 **/
	@Column(name = "INTEREST_PAID_AMT", unique = false, nullable = true, length = 20)
	private String interestPaidAmt;
	
	/** 应计利息全部冲抵日期 **/
	@Column(name = "INTEREST_ALL_PAID_DATE", unique = false, nullable = true, length = 10)
	private String interestAllPaidDate;
	
	/** 事件标志 **/
	@Column(name = "EVENT_FLAG", unique = false, nullable = true, length = 10)
	private String eventFlag;
	
	/** 事件日期 **/
	@Column(name = "EVENT_DATE", unique = false, nullable = true, length = 10)
	private String eventDate;
	
	/** 贷款类型 **/
	@Column(name = "LOAN_TYPE_STAGE", unique = false, nullable = true, length = 20)
	private String loanTypeStage;
	
	/** 子产品代码 **/
	@Column(name = "PRODUCT_STCODE", unique = false, nullable = true, length = 10)
	private String productStcode;
	
	/** 当前期数 **/
	@Column(name = "P_CURR_TERM", unique = false, nullable = true, length = 11)
	private String pCurrTerm;
	
	/** 保证金币种 **/
	@Column(name = "BOND_CCY", unique = false, nullable = true, length = 10)
	private String bondCcy;
	
	/** 客户类型 **/
	@Column(name = "TYPE_OF_CUST", unique = false, nullable = true, length = 20)
	private String typeOfCust;
	
	/** 结清日期 **/
	@Column(name = "PAID_OUT_DATE", unique = false, nullable = true, length = 10)
	private String paidOutDate;
	
	/** 核销日期 **/
	@Column(name = "WTO_DATE", unique = false, nullable = true, length = 10)
	private String wtoDate;
	
	/** cnc卡号 **/
	@Column(name = "CARD_NO", unique = false, nullable = true, length = 40)
	private String cardNo;
	
	/** 授信协议号 **/
	@Column(name = "LIMIT_REPORT_NO", unique = false, nullable = true, length = 32)
	private String limitReportNo;
	
	/** 银团回购日期 **/
	@Column(name = "BG_REPO_DATE", unique = false, nullable = true, length = 10)
	private String bgRepoDate;
	
	/** 借据状态 **/
	@Column(name = "LOAN_PROCESS_FLAG", unique = false, nullable = true, length = 10)
	private String loanProcessFlag;
	
	/** 状态变更日期 **/
	@Column(name = "CLAIMED_DATE", unique = false, nullable = true, length = 10)
	private String claimedDate;
	
	/** 无还本续贷借据 **/
	@Column(name = "RELATIVEDDLOANNO", unique = false, nullable = true, length = 400)
	private String relativeddloanno;
	
	/** 月息率 **/
	@Column(name = "MONTH_RATE", unique = false, nullable = true, length = 12)
	private String monthRate;
	
	/** 封包日期 **/
	@Column(name = "PACKET_DATE", unique = false, nullable = true, length = 10)
	private String packetDate;
	
	/** 封包金额 **/
	@Column(name = "PACKET_DATE", unique = false, nullable = true, length = 20)
	private String packetBalance;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param ietmCd
	 */
	public void setIetmCd(String ietmCd) {
		this.ietmCd = ietmCd;
	}
	
    /**
     * @return ietmCd
     */
	public String getIetmCd() {
		return this.ietmCd;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param applyNo
	 */
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	
    /**
     * @return applyNo
     */
	public String getApplyNo() {
		return this.applyNo;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param agrRelFlg
	 */
	public void setAgrRelFlg(String agrRelFlg) {
		this.agrRelFlg = agrRelFlg;
	}
	
    /**
     * @return agrRelFlg
     */
	public String getAgrRelFlg() {
		return this.agrRelFlg;
	}
	
	/**
	 * @param subsidizedFlg
	 */
	public void setSubsidizedFlg(String subsidizedFlg) {
		this.subsidizedFlg = subsidizedFlg;
	}
	
    /**
     * @return subsidizedFlg
     */
	public String getSubsidizedFlg() {
		return this.subsidizedFlg;
	}
	
	/**
	 * @param agrRelProp
	 */
	public void setAgrRelProp(String agrRelProp) {
		this.agrRelProp = agrRelProp;
	}
	
    /**
     * @return agrRelProp
     */
	public String getAgrRelProp() {
		return this.agrRelProp;
	}
	
	/**
	 * @param agrSptType
	 */
	public void setAgrSptType(String agrSptType) {
		this.agrSptType = agrSptType;
	}
	
    /**
     * @return agrSptType
     */
	public String getAgrSptType() {
		return this.agrSptType;
	}
	
	/**
	 * @param realEstateType
	 */
	public void setRealEstateType(String realEstateType) {
		this.realEstateType = realEstateType;
	}
	
    /**
     * @return realEstateType
     */
	public String getRealEstateType() {
		return this.realEstateType;
	}
	
	/**
	 * @param laidoffLoanType
	 */
	public void setLaidoffLoanType(String laidoffLoanType) {
		this.laidoffLoanType = laidoffLoanType;
	}
	
    /**
     * @return laidoffLoanType
     */
	public String getLaidoffLoanType() {
		return this.laidoffLoanType;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custIdtype
	 */
	public void setCustIdtype(String custIdtype) {
		this.custIdtype = custIdtype;
	}
	
    /**
     * @return custIdtype
     */
	public String getCustIdtype() {
		return this.custIdtype;
	}
	
	/**
	 * @param custIdno
	 */
	public void setCustIdno(String custIdno) {
		this.custIdno = custIdno;
	}
	
    /**
     * @return custIdno
     */
	public String getCustIdno() {
		return this.custIdno;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param loanPurpose
	 */
	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}
	
    /**
     * @return loanPurpose
     */
	public String getLoanPurpose() {
		return this.loanPurpose;
	}
	
	/**
	 * @param cardState
	 */
	public void setCardState(String cardState) {
		this.cardState = cardState;
	}
	
    /**
     * @return cardState
     */
	public String getCardState() {
		return this.cardState;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param maturityDate
	 */
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
    /**
     * @return maturityDate
     */
	public String getMaturityDate() {
		return this.maturityDate;
	}
	
	/**
	 * @param schMaturityDate
	 */
	public void setSchMaturityDate(String schMaturityDate) {
		this.schMaturityDate = schMaturityDate;
	}
	
    /**
     * @return schMaturityDate
     */
	public String getSchMaturityDate() {
		return this.schMaturityDate;
	}
	
	/**
	 * @param gracePeriod
	 */
	public void setGracePeriod(String gracePeriod) {
		this.gracePeriod = gracePeriod;
	}
	
    /**
     * @return gracePeriod
     */
	public String getGracePeriod() {
		return this.gracePeriod;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}
	
    /**
     * @return rate
     */
	public String getRate() {
		return this.rate;
	}
	
	/**
	 * @param baseRate
	 */
	public void setBaseRate(String baseRate) {
		this.baseRate = baseRate;
	}
	
    /**
     * @return baseRate
     */
	public String getBaseRate() {
		return this.baseRate;
	}
	
	/**
	 * @param ccyCd
	 */
	public void setCcyCd(String ccyCd) {
		this.ccyCd = ccyCd;
	}
	
    /**
     * @return ccyCd
     */
	public String getCcyCd() {
		return this.ccyCd;
	}
	
	/**
	 * @param amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
    /**
     * @return amount
     */
	public String getAmount() {
		return this.amount;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public String getBalance() {
		return this.balance;
	}
	
	/**
	 * @param paymentFeq
	 */
	public void setPaymentFeq(String paymentFeq) {
		this.paymentFeq = paymentFeq;
	}
	
    /**
     * @return paymentFeq
     */
	public String getPaymentFeq() {
		return this.paymentFeq;
	}
	
	/**
	 * @param payWay
	 */
	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}
	
    /**
     * @return payWay
     */
	public String getPayWay() {
		return this.payWay;
	}
	
	/**
	 * @param repricingDate
	 */
	public void setRepricingDate(String repricingDate) {
		this.repricingDate = repricingDate;
	}
	
    /**
     * @return repricingDate
     */
	public String getRepricingDate() {
		return this.repricingDate;
	}
	
	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	
    /**
     * @return rateType
     */
	public String getRateType() {
		return this.rateType;
	}
	
	/**
	 * @param overdueType
	 */
	public void setOverdueType(String overdueType) {
		this.overdueType = overdueType;
	}
	
    /**
     * @return overdueType
     */
	public String getOverdueType() {
		return this.overdueType;
	}
	
	/**
	 * @param overdueDays
	 */
	public void setOverdueDays(String overdueDays) {
		this.overdueDays = overdueDays;
	}
	
    /**
     * @return overdueDays
     */
	public String getOverdueDays() {
		return this.overdueDays;
	}
	
	/**
	 * @param intrTyp
	 */
	public void setIntrTyp(String intrTyp) {
		this.intrTyp = intrTyp;
	}
	
    /**
     * @return intrTyp
     */
	public String getIntrTyp() {
		return this.intrTyp;
	}
	
	/**
	 * @param interest
	 */
	public void setInterest(String interest) {
		this.interest = interest;
	}
	
    /**
     * @return interest
     */
	public String getInterest() {
		return this.interest;
	}
	
	/**
	 * @param prinOdDate
	 */
	public void setPrinOdDate(String prinOdDate) {
		this.prinOdDate = prinOdDate;
	}
	
    /**
     * @return prinOdDate
     */
	public String getPrinOdDate() {
		return this.prinOdDate;
	}
	
	/**
	 * @param prinOdAmt
	 */
	public void setPrinOdAmt(String prinOdAmt) {
		this.prinOdAmt = prinOdAmt;
	}
	
    /**
     * @return prinOdAmt
     */
	public String getPrinOdAmt() {
		return this.prinOdAmt;
	}
	
	/**
	 * @param intOdDate
	 */
	public void setIntOdDate(String intOdDate) {
		this.intOdDate = intOdDate;
	}
	
    /**
     * @return intOdDate
     */
	public String getIntOdDate() {
		return this.intOdDate;
	}
	
	/**
	 * @param intOdAmt
	 */
	public void setIntOdAmt(String intOdAmt) {
		this.intOdAmt = intOdAmt;
	}
	
    /**
     * @return intOdAmt
     */
	public String getIntOdAmt() {
		return this.intOdAmt;
	}
	
	/**
	 * @param subsidizedInt
	 */
	public void setSubsidizedInt(String subsidizedInt) {
		this.subsidizedInt = subsidizedInt;
	}
	
    /**
     * @return subsidizedInt
     */
	public String getSubsidizedInt() {
		return this.subsidizedInt;
	}
	
	/**
	 * @param actSubsidizedInt
	 */
	public void setActSubsidizedInt(String actSubsidizedInt) {
		this.actSubsidizedInt = actSubsidizedInt;
	}
	
    /**
     * @return actSubsidizedInt
     */
	public String getActSubsidizedInt() {
		return this.actSubsidizedInt;
	}
	
	/**
	 * @param fundSource
	 */
	public void setFundSource(String fundSource) {
		this.fundSource = fundSource;
	}
	
    /**
     * @return fundSource
     */
	public String getFundSource() {
		return this.fundSource;
	}
	
	/**
	 * @param extensionFlg
	 */
	public void setExtensionFlg(String extensionFlg) {
		this.extensionFlg = extensionFlg;
	}
	
    /**
     * @return extensionFlg
     */
	public String getExtensionFlg() {
		return this.extensionFlg;
	}
	
	/**
	 * @param extensionAmt
	 */
	public void setExtensionAmt(String extensionAmt) {
		this.extensionAmt = extensionAmt;
	}
	
    /**
     * @return extensionAmt
     */
	public String getExtensionAmt() {
		return this.extensionAmt;
	}
	
	/**
	 * @param extensionStart
	 */
	public void setExtensionStart(String extensionStart) {
		this.extensionStart = extensionStart;
	}
	
    /**
     * @return extensionStart
     */
	public String getExtensionStart() {
		return this.extensionStart;
	}
	
	/**
	 * @param extensionMaturity
	 */
	public void setExtensionMaturity(String extensionMaturity) {
		this.extensionMaturity = extensionMaturity;
	}
	
    /**
     * @return extensionMaturity
     */
	public String getExtensionMaturity() {
		return this.extensionMaturity;
	}
	
	/**
	 * @param recomFlg
	 */
	public void setRecomFlg(String recomFlg) {
		this.recomFlg = recomFlg;
	}
	
    /**
     * @return recomFlg
     */
	public String getRecomFlg() {
		return this.recomFlg;
	}
	
	/**
	 * @param recomDate
	 */
	public void setRecomDate(String recomDate) {
		this.recomDate = recomDate;
	}
	
    /**
     * @return recomDate
     */
	public String getRecomDate() {
		return this.recomDate;
	}
	
	/**
	 * @param bondAmt
	 */
	public void setBondAmt(String bondAmt) {
		this.bondAmt = bondAmt;
	}
	
    /**
     * @return bondAmt
     */
	public String getBondAmt() {
		return this.bondAmt;
	}
	
	/**
	 * @param capitalFund
	 */
	public void setCapitalFund(String capitalFund) {
		this.capitalFund = capitalFund;
	}
	
    /**
     * @return capitalFund
     */
	public String getCapitalFund() {
		return this.capitalFund;
	}
	
	/**
	 * @param houseNum
	 */
	public void setHouseNum(String houseNum) {
		this.houseNum = houseNum;
	}
	
    /**
     * @return houseNum
     */
	public String getHouseNum() {
		return this.houseNum;
	}
	
	/**
	 * @param tenementFee
	 */
	public void setTenementFee(String tenementFee) {
		this.tenementFee = tenementFee;
	}
	
    /**
     * @return tenementFee
     */
	public String getTenementFee() {
		return this.tenementFee;
	}
	
	/**
	 * @param personAddLoanFlg
	 */
	public void setPersonAddLoanFlg(String personAddLoanFlg) {
		this.personAddLoanFlg = personAddLoanFlg;
	}
	
    /**
     * @return personAddLoanFlg
     */
	public String getPersonAddLoanFlg() {
		return this.personAddLoanFlg;
	}
	
	/**
	 * @param managementFlag
	 */
	public void setManagementFlag(String managementFlag) {
		this.managementFlag = managementFlag;
	}
	
    /**
     * @return managementFlag
     */
	public String getManagementFlag() {
		return this.managementFlag;
	}
	
	/**
	 * @param smeltType
	 */
	public void setSmeltType(String smeltType) {
		this.smeltType = smeltType;
	}
	
    /**
     * @return smeltType
     */
	public String getSmeltType() {
		return this.smeltType;
	}
	
	/**
	 * @param strategyType
	 */
	public void setStrategyType(String strategyType) {
		this.strategyType = strategyType;
	}
	
    /**
     * @return strategyType
     */
	public String getStrategyType() {
		return this.strategyType;
	}
	
	/**
	 * @param upgradeFlg
	 */
	public void setUpgradeFlg(String upgradeFlg) {
		this.upgradeFlg = upgradeFlg;
	}
	
    /**
     * @return upgradeFlg
     */
	public String getUpgradeFlg() {
		return this.upgradeFlg;
	}
	
	/**
	 * @param generalReserve
	 */
	public void setGeneralReserve(String generalReserve) {
		this.generalReserve = generalReserve;
	}
	
    /**
     * @return generalReserve
     */
	public String getGeneralReserve() {
		return this.generalReserve;
	}
	
	/**
	 * @param specialPrep
	 */
	public void setSpecialPrep(String specialPrep) {
		this.specialPrep = specialPrep;
	}
	
    /**
     * @return specialPrep
     */
	public String getSpecialPrep() {
		return this.specialPrep;
	}
	
	/**
	 * @param preSpe
	 */
	public void setPreSpe(String preSpe) {
		this.preSpe = preSpe;
	}
	
    /**
     * @return preSpe
     */
	public String getPreSpe() {
		return this.preSpe;
	}
	
	/**
	 * @param reserve
	 */
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
    /**
     * @return reserve
     */
	public String getReserve() {
		return this.reserve;
	}
	
	/**
	 * @param houseBuyCount
	 */
	public void setHouseBuyCount(String houseBuyCount) {
		this.houseBuyCount = houseBuyCount;
	}
	
    /**
     * @return houseBuyCount
     */
	public String getHouseBuyCount() {
		return this.houseBuyCount;
	}
	
	/**
	 * @param usedLocat
	 */
	public void setUsedLocat(String usedLocat) {
		this.usedLocat = usedLocat;
	}
	
    /**
     * @return usedLocat
     */
	public String getUsedLocat() {
		return this.usedLocat;
	}
	
	/**
	 * @param rateFloatType
	 */
	public void setRateFloatType(String rateFloatType) {
		this.rateFloatType = rateFloatType;
	}
	
    /**
     * @return rateFloatType
     */
	public String getRateFloatType() {
		return this.rateFloatType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param originalMaturityM
	 */
	public void setOriginalMaturityM(String originalMaturityM) {
		this.originalMaturityM = originalMaturityM;
	}
	
    /**
     * @return originalMaturityM
     */
	public String getOriginalMaturityM() {
		return this.originalMaturityM;
	}
	
	/**
	 * @param originalMaturityD
	 */
	public void setOriginalMaturityD(String originalMaturityD) {
		this.originalMaturityD = originalMaturityD;
	}
	
    /**
     * @return originalMaturityD
     */
	public String getOriginalMaturityD() {
		return this.originalMaturityD;
	}
	
	/**
	 * @param remainingMaturityM
	 */
	public void setRemainingMaturityM(String remainingMaturityM) {
		this.remainingMaturityM = remainingMaturityM;
	}
	
    /**
     * @return remainingMaturityM
     */
	public String getRemainingMaturityM() {
		return this.remainingMaturityM;
	}
	
	/**
	 * @param remainingMaturityD
	 */
	public void setRemainingMaturityD(String remainingMaturityD) {
		this.remainingMaturityD = remainingMaturityD;
	}
	
    /**
     * @return remainingMaturityD
     */
	public String getRemainingMaturityD() {
		return this.remainingMaturityD;
	}
	
	/**
	 * @param beginLoanGrade
	 */
	public void setBeginLoanGrade(String beginLoanGrade) {
		this.beginLoanGrade = beginLoanGrade;
	}
	
    /**
     * @return beginLoanGrade
     */
	public String getBeginLoanGrade() {
		return this.beginLoanGrade;
	}
	
	/**
	 * @param pOverdueAmt
	 */
	public void setPOverdueAmt(String pOverdueAmt) {
		this.pOverdueAmt = pOverdueAmt;
	}
	
    /**
     * @return pOverdueAmt
     */
	public String getPOverdueAmt() {
		return this.pOverdueAmt;
	}
	
	/**
	 * @param ageCd
	 */
	public void setAgeCd(String ageCd) {
		this.ageCd = ageCd;
	}
	
    /**
     * @return ageCd
     */
	public String getAgeCd() {
		return this.ageCd;
	}
	
	/**
	 * @param pCancelDate
	 */
	public void setPCancelDate(String pCancelDate) {
		this.pCancelDate = pCancelDate;
	}
	
    /**
     * @return pCancelDate
     */
	public String getPCancelDate() {
		return this.pCancelDate;
	}
	
	/**
	 * @param pInitTerm
	 */
	public void setPInitTerm(String pInitTerm) {
		this.pInitTerm = pInitTerm;
	}
	
    /**
     * @return pInitTerm
     */
	public String getPInitTerm() {
		return this.pInitTerm;
	}
	
	/**
	 * @param repricingMaturityM
	 */
	public void setRepricingMaturityM(String repricingMaturityM) {
		this.repricingMaturityM = repricingMaturityM;
	}
	
    /**
     * @return repricingMaturityM
     */
	public String getRepricingMaturityM() {
		return this.repricingMaturityM;
	}
	
	/**
	 * @param repricingMaturityD
	 */
	public void setRepricingMaturityD(String repricingMaturityD) {
		this.repricingMaturityD = repricingMaturityD;
	}
	
    /**
     * @return repricingMaturityD
     */
	public String getRepricingMaturityD() {
		return this.repricingMaturityD;
	}
	
	/**
	 * @param cultureSign
	 */
	public void setCultureSign(String cultureSign) {
		this.cultureSign = cultureSign;
	}
	
    /**
     * @return cultureSign
     */
	public String getCultureSign() {
		return this.cultureSign;
	}
	
	/**
	 * @param activateDate
	 */
	public void setActivateDate(String activateDate) {
		this.activateDate = activateDate;
	}
	
    /**
     * @return activateDate
     */
	public String getActivateDate() {
		return this.activateDate;
	}
	
	/**
	 * @param pmtDueDate
	 */
	public void setPmtDueDate(String pmtDueDate) {
		this.pmtDueDate = pmtDueDate;
	}
	
    /**
     * @return pmtDueDate
     */
	public String getPmtDueDate() {
		return this.pmtDueDate;
	}
	
	/**
	 * @param bankGroupId
	 */
	public void setBankGroupId(String bankGroupId) {
		this.bankGroupId = bankGroupId;
	}
	
    /**
     * @return bankGroupId
     */
	public String getBankGroupId() {
		return this.bankGroupId;
	}
	
	/**
	 * @param terminateDate
	 */
	public void setTerminateDate(String terminateDate) {
		this.terminateDate = terminateDate;
	}
	
    /**
     * @return terminateDate
     */
	public String getTerminateDate() {
		return this.terminateDate;
	}
	
	/**
	 * @param serNo
	 */
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	
    /**
     * @return serNo
     */
	public String getSerNo() {
		return this.serNo;
	}
	
	/**
	 * @param insurancePaymentFlag
	 */
	public void setInsurancePaymentFlag(String insurancePaymentFlag) {
		this.insurancePaymentFlag = insurancePaymentFlag;
	}
	
    /**
     * @return insurancePaymentFlag
     */
	public String getInsurancePaymentFlag() {
		return this.insurancePaymentFlag;
	}
	
	/**
	 * @param insurancePaymentDate
	 */
	public void setInsurancePaymentDate(String insurancePaymentDate) {
		this.insurancePaymentDate = insurancePaymentDate;
	}
	
    /**
     * @return insurancePaymentDate
     */
	public String getInsurancePaymentDate() {
		return this.insurancePaymentDate;
	}
	
	/**
	 * @param insurancePaymentPrin
	 */
	public void setInsurancePaymentPrin(String insurancePaymentPrin) {
		this.insurancePaymentPrin = insurancePaymentPrin;
	}
	
    /**
     * @return insurancePaymentPrin
     */
	public String getInsurancePaymentPrin() {
		return this.insurancePaymentPrin;
	}
	
	/**
	 * @param insurancePaymentFee
	 */
	public void setInsurancePaymentFee(String insurancePaymentFee) {
		this.insurancePaymentFee = insurancePaymentFee;
	}
	
    /**
     * @return insurancePaymentFee
     */
	public String getInsurancePaymentFee() {
		return this.insurancePaymentFee;
	}
	
	/**
	 * @param terminateReasonCd
	 */
	public void setTerminateReasonCd(String terminateReasonCd) {
		this.terminateReasonCd = terminateReasonCd;
	}
	
    /**
     * @return terminateReasonCd
     */
	public String getTerminateReasonCd() {
		return this.terminateReasonCd;
	}
	
	/**
	 * @param assetPlanNo
	 */
	public void setAssetPlanNo(String assetPlanNo) {
		this.assetPlanNo = assetPlanNo;
	}
	
    /**
     * @return assetPlanNo
     */
	public String getAssetPlanNo() {
		return this.assetPlanNo;
	}
	
	/**
	 * @param assetTransferOrg
	 */
	public void setAssetTransferOrg(String assetTransferOrg) {
		this.assetTransferOrg = assetTransferOrg;
	}
	
    /**
     * @return assetTransferOrg
     */
	public String getAssetTransferOrg() {
		return this.assetTransferOrg;
	}
	
	/**
	 * @param assetTransferAmt
	 */
	public void setAssetTransferAmt(String assetTransferAmt) {
		this.assetTransferAmt = assetTransferAmt;
	}
	
    /**
     * @return assetTransferAmt
     */
	public String getAssetTransferAmt() {
		return this.assetTransferAmt;
	}
	
	/**
	 * @param assetTransferFlag
	 */
	public void setAssetTransferFlag(String assetTransferFlag) {
		this.assetTransferFlag = assetTransferFlag;
	}
	
    /**
     * @return assetTransferFlag
     */
	public String getAssetTransferFlag() {
		return this.assetTransferFlag;
	}
	
	/**
	 * @param assetTransferDate
	 */
	public void setAssetTransferDate(String assetTransferDate) {
		this.assetTransferDate = assetTransferDate;
	}
	
    /**
     * @return assetTransferDate
     */
	public String getAssetTransferDate() {
		return this.assetTransferDate;
	}
	
	/**
	 * @param interestPaidAmt
	 */
	public void setInterestPaidAmt(String interestPaidAmt) {
		this.interestPaidAmt = interestPaidAmt;
	}
	
    /**
     * @return interestPaidAmt
     */
	public String getInterestPaidAmt() {
		return this.interestPaidAmt;
	}
	
	/**
	 * @param interestAllPaidDate
	 */
	public void setInterestAllPaidDate(String interestAllPaidDate) {
		this.interestAllPaidDate = interestAllPaidDate;
	}
	
    /**
     * @return interestAllPaidDate
     */
	public String getInterestAllPaidDate() {
		return this.interestAllPaidDate;
	}
	
	/**
	 * @param eventFlag
	 */
	public void setEventFlag(String eventFlag) {
		this.eventFlag = eventFlag;
	}
	
    /**
     * @return eventFlag
     */
	public String getEventFlag() {
		return this.eventFlag;
	}
	
	/**
	 * @param eventDate
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
    /**
     * @return eventDate
     */
	public String getEventDate() {
		return this.eventDate;
	}
	
	/**
	 * @param loanTypeStage
	 */
	public void setLoanTypeStage(String loanTypeStage) {
		this.loanTypeStage = loanTypeStage;
	}
	
    /**
     * @return loanTypeStage
     */
	public String getLoanTypeStage() {
		return this.loanTypeStage;
	}
	
	/**
	 * @param productStcode
	 */
	public void setProductStcode(String productStcode) {
		this.productStcode = productStcode;
	}
	
    /**
     * @return productStcode
     */
	public String getProductStcode() {
		return this.productStcode;
	}
	
	/**
	 * @param pCurrTerm
	 */
	public void setPCurrTerm(String pCurrTerm) {
		this.pCurrTerm = pCurrTerm;
	}
	
    /**
     * @return pCurrTerm
     */
	public String getPCurrTerm() {
		return this.pCurrTerm;
	}
	
	/**
	 * @param bondCcy
	 */
	public void setBondCcy(String bondCcy) {
		this.bondCcy = bondCcy;
	}
	
    /**
     * @return bondCcy
     */
	public String getBondCcy() {
		return this.bondCcy;
	}
	
	/**
	 * @param typeOfCust
	 */
	public void setTypeOfCust(String typeOfCust) {
		this.typeOfCust = typeOfCust;
	}
	
    /**
     * @return typeOfCust
     */
	public String getTypeOfCust() {
		return this.typeOfCust;
	}
	
	/**
	 * @param paidOutDate
	 */
	public void setPaidOutDate(String paidOutDate) {
		this.paidOutDate = paidOutDate;
	}
	
    /**
     * @return paidOutDate
     */
	public String getPaidOutDate() {
		return this.paidOutDate;
	}
	
	/**
	 * @param wtoDate
	 */
	public void setWtoDate(String wtoDate) {
		this.wtoDate = wtoDate;
	}
	
    /**
     * @return wtoDate
     */
	public String getWtoDate() {
		return this.wtoDate;
	}
	
	/**
	 * @param cardNo
	 */
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
    /**
     * @return cardNo
     */
	public String getCardNo() {
		return this.cardNo;
	}
	
	/**
	 * @param limitReportNo
	 */
	public void setLimitReportNo(String limitReportNo) {
		this.limitReportNo = limitReportNo;
	}
	
    /**
     * @return limitReportNo
     */
	public String getLimitReportNo() {
		return this.limitReportNo;
	}
	
	/**
	 * @param bgRepoDate
	 */
	public void setBgRepoDate(String bgRepoDate) {
		this.bgRepoDate = bgRepoDate;
	}
	
    /**
     * @return bgRepoDate
     */
	public String getBgRepoDate() {
		return this.bgRepoDate;
	}
	
	/**
	 * @param loanProcessFlag
	 */
	public void setLoanProcessFlag(String loanProcessFlag) {
		this.loanProcessFlag = loanProcessFlag;
	}
	
    /**
     * @return loanProcessFlag
     */
	public String getLoanProcessFlag() {
		return this.loanProcessFlag;
	}
	
	/**
	 * @param claimedDate
	 */
	public void setClaimedDate(String claimedDate) {
		this.claimedDate = claimedDate;
	}
	
    /**
     * @return claimedDate
     */
	public String getClaimedDate() {
		return this.claimedDate;
	}
	
	/**
	 * @param relativeddloanno
	 */
	public void setRelativeddloanno(String relativeddloanno) {
		this.relativeddloanno = relativeddloanno;
	}
	
    /**
     * @return relativeddloanno
     */
	public String getRelativeddloanno() {
		return this.relativeddloanno;
	}
	
	/**
	 * @param monthRate
	 */
	public void setMonthRate(String monthRate) {
		this.monthRate = monthRate;
	}
	
    /**
     * @return monthRate
     */
	public String getMonthRate() {
		return this.monthRate;
	}
	
	/**
	 * @param packetDate
	 */
	public void setPacketDate(String packetDate) {
		this.packetDate = packetDate;
	}
	
    /**
     * @return packetDate
     */
	public String getPacketDate() {
		return this.packetDate;
	}
	
	/**
	 * @param packetBalance
	 */
	public void setPacketBalance(String packetBalance) {
		this.packetBalance = packetBalance;
	}
	
    /**
     * @return packetBalance
     */
	public String getPacketBalance() {
		return this.packetBalance;
	}


}