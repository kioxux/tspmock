package cn.com.yusys.yusp.web.server.xdcz0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0006.req.Xdcz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.resp.Xdcz0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0006.Xdcz0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:出账记录详情查看
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0006:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0006.BizXdcz0006Resource.class);

    @Autowired
    private Xdcz0006Service xdcz0006Service;

    /**
     * 交易码：xdcz0006
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0006")
    protected @ResponseBody
    ResultDto<Xdcz0006DataRespDto> xdcz0006(@Validated @RequestBody Xdcz0006DataReqDto xdcz0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataReqDto));
        Xdcz0006DataRespDto xdcz0006DataRespDto = new Xdcz0006DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0006DataRespDto> xdcz0006DataResultDto = new ResultDto<>();
        String acctNo = xdcz0006DataReqDto.getAcctNo();//账号
        String seq = xdcz0006DataReqDto.getSeq();//批次号
        try {
            // 从xdcz0006DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataReqDto));
            xdcz0006DataRespDto = xdcz0006Service.xdcz0006(xdcz0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataRespDto));
            // 封装xdcz0006DataResultDto中正确的返回码和返回信息
            xdcz0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, e.getMessage());
            // 封装xdcz0006DataResultDto中异常返回码和返回信息
            xdcz0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0006DataRespDto到xdcz0006DataResultDto中
        xdcz0006DataResultDto.setData(xdcz0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataRespDto));
        return xdcz0006DataResultDto;
    }
}
