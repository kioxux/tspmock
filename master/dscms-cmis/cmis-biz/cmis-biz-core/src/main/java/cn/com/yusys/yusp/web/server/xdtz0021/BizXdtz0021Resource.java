package cn.com.yusys.yusp.web.server.xdtz0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0020.Xdtz0020Service;
import cn.com.yusys.yusp.service.server.xdtz0021.Xdtz0021Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0021.req.Xdtz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.resp.Xdtz0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:台账入账（贸易融资\福费廷）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0021:台账入账（贸易融资\\福费廷）")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0021Resource.class);

    @Autowired
    private Xdtz0021Service xdtz0021Service;
    /**
     * 交易码：xdtz0021
     * 交易描述：台账入账（贸易融资\福费廷）
     *
     * @param xdtz0021DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账（贸易融资\\福费廷）")
    @PostMapping("/xdtz0021")
    protected @ResponseBody
    ResultDto<Xdtz0021DataRespDto> xdtz0021(@Validated @RequestBody Xdtz0021DataReqDto xdtz0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021DataReqDto));
        Xdtz0021DataRespDto xdtz0021DataRespDto = new Xdtz0021DataRespDto();// 响应Dto:台账入账（贸易融资\福费廷）
        ResultDto<Xdtz0021DataRespDto> xdtz0021DataResultDto = new ResultDto<>();
        String bizType = xdtz0021DataReqDto.getBizType();//业务类型
        String oprtype = xdtz0021DataReqDto.getOprtype();//操作类型
        String billNo = xdtz0021DataReqDto.getBillNo();//借据号
        String prdName = xdtz0021DataReqDto.getPrdName();//产品名称
        String prdId = xdtz0021DataReqDto.getPrdId();//产品编号
        String contNo = xdtz0021DataReqDto.getContNo();//合同编号
        String cusId = xdtz0021DataReqDto.getCusId();//客户代码
        String cusName = xdtz0021DataReqDto.getCusName();//客户名称
        BigDecimal loanAmt = xdtz0021DataReqDto.getLoanAmt();//贷款金额
        String curType = xdtz0021DataReqDto.getCurType();//币种
        BigDecimal loanBalance = xdtz0021DataReqDto.getLoanBalance();//贷款余额
        String loanStartDate = xdtz0021DataReqDto.getLoanStartDate();//贷款起始日
        String loanEndDate = xdtz0021DataReqDto.getLoanEndDate();//贷款到期日
        BigDecimal rulingIr = xdtz0021DataReqDto.getRulingIr();//基准利率年
        BigDecimal realityIrY = xdtz0021DataReqDto.getRealityIrY();//执行利率年
        BigDecimal rateFloat = xdtz0021DataReqDto.getRateFloat();//利率浮动值
        String accStatus = xdtz0021DataReqDto.getAccStatus();//台账状态
        String inputId = xdtz0021DataReqDto.getInputId();//登记人
        String finaBrId = xdtz0021DataReqDto.getFinaBrId();//账务机构
        String managerId = xdtz0021DataReqDto.getManagerId();//责任人
        String managerBrId = xdtz0021DataReqDto.getManagerBrId();//责任机构
        BigDecimal cretQuotationExchgRate = xdtz0021DataReqDto.getCretQuotationExchgRate();//信贷牌价汇率
        try {
            // 从xdtz0021DataReqDto获取业务值进行业务逻辑处理
            xdtz0021DataRespDto = xdtz0021Service.xdtz0021(xdtz0021DataReqDto);
            // 封装xdtz0021DataResultDto中正确的返回码和返回信息
            xdtz0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, e.getMessage());
            // 封装xdtz0021DataResultDto中异常返回码和返回信息
            xdtz0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0021DataRespDto到xdtz0021DataResultDto中
        xdtz0021DataResultDto.setData(xdtz0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021DataResultDto));
        return xdtz0021DataResultDto;
    }
}
