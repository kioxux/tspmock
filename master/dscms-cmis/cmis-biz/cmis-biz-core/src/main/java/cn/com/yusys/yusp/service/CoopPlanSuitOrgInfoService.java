/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import feign.QueryMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPlanSuitOrgInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPlanSuitOrgInfoMapper;

import javax.management.Query;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanSuitOrgInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:08:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanSuitOrgInfoService {

    @Autowired
    private CoopPlanSuitOrgInfoMapper coopPlanSuitOrgInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CoopPlanSuitOrgInfo selectByPrimaryKey(String pkId) {
        return coopPlanSuitOrgInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CoopPlanSuitOrgInfo> selectAll(QueryModel model) {
        List<CoopPlanSuitOrgInfo> records = (List<CoopPlanSuitOrgInfo>) coopPlanSuitOrgInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopPlanSuitOrgInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanSuitOrgInfo> list = coopPlanSuitOrgInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CoopPlanSuitOrgInfo record) {
        int cnt = 0;
        QueryModel queryModel = new QueryModel();
        String serno = record.getSerno();
        String coopPlanNo = record.getCoopPlanNo();
        queryModel.addCondition("serno",serno);
        String[] orgIds = record.getSuitOrgNo().split(",");
        String[] orgNames = record.getSuitOrgName().split(",");
        for(int i=0;i<orgIds.length;i++){
            queryModel.addCondition("suitOrgNo",orgIds[i]);
            int orgCnt = coopPlanSuitOrgInfoMapper.validateOrgCnt(queryModel);
            if (orgCnt > 0){
                throw BizException.error(null, null, "机构【" + orgIds[i] + "】已在适用机构列表中，禁止重复添加！");
            }
            record = new CoopPlanSuitOrgInfo();
            record.setCoopPlanNo(coopPlanNo);
            record.setSerno(serno);
            record.setSuitOrgNo(orgIds[i]);
            record.setSuitOrgName(orgNames[i]);
            cnt += coopPlanSuitOrgInfoMapper.insert(record);
        }
        return cnt;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CoopPlanSuitOrgInfo record) {
        return coopPlanSuitOrgInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CoopPlanSuitOrgInfo record) {
        return coopPlanSuitOrgInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CoopPlanSuitOrgInfo record) {
        return coopPlanSuitOrgInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return coopPlanSuitOrgInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPlanSuitOrgInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteAll
     * @方法描述: 删除所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteAll(CoopPlanSuitOrgInfo coopPlanSuitOrgInfo) {
        return coopPlanSuitOrgInfoMapper.deleteAll(coopPlanSuitOrgInfo);
    }
}
