package cn.com.yusys.yusp.service.server.xdcz0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.req.Xdcz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.resp.Xdcz0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import cn.com.yusys.yusp.service.PvpAuthorizeService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:支用(微信小程序)
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0017Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0017Service.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;

    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;


    /**
     * 业务场景：支用(微信小程序)
     * 接口说明：根据出账申请表的影像流水，出账授权表的借据号，查询待放款的出账信息，调用核心系统的一系列放款交易，
     * 大致逻辑同3020放款交易接口（SendAuthtoCoreAndHsOp.java）,
     * 调用完成后，更新出账申请表、出账授权表的状态。
     * <p>
     * 备注:对应老系统代码SendAuthtoCoreAndHsOp.java
     *
     * @param xdcz0017DataReqDto
     * @return
     * @author
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0017DataRespDto xdcz0017(Xdcz0017DataReqDto xdcz0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataReqDto));
        Xdcz0017DataRespDto xdcz0017DataRespDto = new Xdcz0017DataRespDto();
        try {
            // 贷款来源
            String loanSour = xdcz0017DataReqDto.getLoanSour();
            // 借据编号
            String billNo = xdcz0017DataReqDto.getBillNo();
            // 影像流水
            String imageSer = xdcz0017DataReqDto.getImageSer();

            // 非空校验
            if ("".equals(billNo) || "".equals(imageSer) || "".equals(loanSour)) {
                throw BizException.error(null, null, "缺少借据号，影像流水号，或贷款来源！");
            }

            // 判断出账授权信息状态是否正确，
            PvpAuthorize pvpAuthorize = pvpAuthorizeService.selectPvpAuthorizeByBillNo(billNo);
            if (Objects.isNull(pvpAuthorize)) {
                throw BizException.error(null, null, "未查询到出账授权信息！");
            }

            // 仅支持未出帐数据 0--未出帐
            if (!Objects.equals("0", pvpAuthorize.getAuthStatus())) {
                throw BizException.error(null, null, "该笔出账授权状态不为未出帐，出账申请拒绝！");
            }

            // 调用出账交易
            pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, e.getMessage());
            throw e;
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, e.getMessage());
            throw e;
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataRespDto));
        return xdcz0017DataRespDto;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
            ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

            String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            SenddxRespDto senddxRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                //  获取相关的值并解析
                senddxRespDto = senddxResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(senddxCode, senddxMeesage);
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.info("errmsg：" + e.getMessage());
            throw new Exception("发送短信失败！");
        }
    }

    /**
     * 客户基本信息
     *
     * @param cusId
     * @return
     */
    private Xdkh0001DataRespDto queryXdkh0001DataRespDto(String cusId) {
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();//请求Dto：查询个人客户基本信息
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;//响应Dto：查询个人客户基本信息
        xdkh0001DataReqDto.setCusId(cusId);
        xdkh0001DataReqDto.setQueryType("01");

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        ResultDto<Xdkh0001DataRespDto> xdkh0001DataRespDtoResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDtoResultDto));

        String xdkh0001Code = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdkh0001Meesage = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            xdkh0001DataRespDto = xdkh0001DataRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xdkh0001Code, xdkh0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value);
        return xdkh0001DataRespDto;
    }

}
