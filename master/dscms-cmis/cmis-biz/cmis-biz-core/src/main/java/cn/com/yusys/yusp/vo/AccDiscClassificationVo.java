package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "贷款风险分类支行汇总表", fileType = ExcelCsv.ExportFileType.XLS)
public class AccDiscClassificationVo {

    /** 支行名称 **/
    @ExcelField(title = "支行名称",viewLength = 25)
    private String managerBrIdName;

    /** 支行编号 **/
    // @ExcelField(title = "支行编号",viewLength = 25)
    // private String managerBrId;

    /** 贷款余额 **/
    @ExcelField(title = "贷款余额",viewLength = 25, format = "#0.00")
    private BigDecimal loanBalance;

    /** 正常类 **/
    @ExcelField(title = "正常类",viewLength = 25, format = "#0.00")
    private BigDecimal normalCount;

    /** 关注类 **/
    @ExcelField(title = "关注类",viewLength = 25, format = "#0.00")
    private BigDecimal followCount;

    /** 次级类 **/
    @ExcelField(title = "次级类",viewLength = 25, format = "#0.00")
    private BigDecimal inferiorityCount;

    /** 可疑类 **/
    @ExcelField(title = "可疑类",viewLength = 25, format = "#0.00")
    private BigDecimal suspiciousCount;

    /** 损失类 **/
    @ExcelField(title = "损失类",viewLength = 25, format = "#0.00")
    private BigDecimal damageCount;

    /** 对公贷款小计 **/
    @ExcelField(title = "对公贷款小计",viewLength = 25, format = "#0.00")
    private BigDecimal subtotal;

    /** 对公贷款正常1类 **/
    @ExcelField(title = "对公贷款正常1类",viewLength = 25, format = "#0.00")
    private BigDecimal normalCount1;

    /** 对公贷款正常2类 **/
    @ExcelField(title = "对公贷款正常2类",viewLength = 25, format = "#0.00")
    private BigDecimal normalCount2;

    /** 对公贷款正常3类 **/
    @ExcelField(title = "对公贷款正常3类",viewLength = 25, format = "#0.00")
    private BigDecimal normalCount3;

    /** 对公贷款关注1类 **/
    @ExcelField(title = "对公贷款关注1类",viewLength = 25, format = "#0.00")
    private BigDecimal followCount1;

    /** 对公贷款关注2类 **/
    @ExcelField(title = "对公贷款关注2类",viewLength = 25, format = "#0.00")
    private BigDecimal followCount2;

    /** 对公贷款关注3类 **/
    @ExcelField(title = "对公贷款关注3类",viewLength = 25, format = "#0.00")
    private BigDecimal followCount3;

    /** 对公贷款次级1类 **/
    @ExcelField(title = "对公贷款次级1类",viewLength = 25, format = "#0.00")
    private BigDecimal inferiorityCount1;

    /** 对公贷款次级2类 **/
    @ExcelField(title = "对公贷款次级2类",viewLength = 25, format = "#0.00")
    private BigDecimal inferiorityCount2;

    /** 对公贷款可疑类 **/
    @ExcelField(title = "对公贷款可疑类",viewLength = 25, format = "#0.00")
    private BigDecimal suspiciousCount1;

    /** 对公贷款损失类 **/
    @ExcelField(title = "对公贷款损失类",viewLength = 25, format = "#0.00")
    private BigDecimal damageCount1;

    /** 对私贷款小计 **/
    @ExcelField(title = "对私贷款小计",viewLength = 25, format = "#0.00")
    private BigDecimal subtotal1;

    /** 对私贷款正常类 **/
    @ExcelField(title = "对私贷款正常类",viewLength = 25, format = "#0.00")
    private BigDecimal normalCount4;

    /** 对私贷款关注类 **/
    @ExcelField(title = "对私贷款关注类",viewLength = 25, format = "#0.00")
    private BigDecimal followCount4;

    /** 对私贷款次级类 **/
    @ExcelField(title = "对私贷款次级类",viewLength = 25, format = "#0.00")
    private BigDecimal inferiorityCount4;

    /** 对私贷款可疑类 **/
    @ExcelField(title = "对私贷款可疑类",viewLength = 25, format = "#0.00")
    private BigDecimal suspiciousCount4;

    /** 对私贷款损失类 **/
    @ExcelField(title = "对私贷款损失类",viewLength = 25, format = "#0.00")
    private BigDecimal damageCount4;

//    public String getManagerBrIdName() {
//        return managerBrIdName;
//    }
//
//    public void setManagerBrIdName(String managerBrIdName) {
//        this.managerBrIdName = managerBrIdName;
//    }
//
//    public String getManagerBrId() {
//        return managerBrId;
//    }
//
//    public void setManagerBrId(String managerBrId) {
//        this.managerBrId = managerBrId;
//    }


    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getNormalCount() {
        return normalCount;
    }

    public void setNormalCount(BigDecimal normalCount) {
        this.normalCount = normalCount;
    }

    public BigDecimal getFollowCount() {
        return followCount;
    }

    public void setFollowCount(BigDecimal followCount) {
        this.followCount = followCount;
    }

    public BigDecimal getInferiorityCount() {
        return inferiorityCount;
    }

    public void setInferiorityCount(BigDecimal inferiorityCount) {
        this.inferiorityCount = inferiorityCount;
    }

    public BigDecimal getSuspiciousCount() {
        return suspiciousCount;
    }

    public void setSuspiciousCount(BigDecimal suspiciousCount) {
        this.suspiciousCount = suspiciousCount;
    }

    public BigDecimal getDamageCount() {
        return damageCount;
    }

    public void setDamageCount(BigDecimal damageCount) {
        this.damageCount = damageCount;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getNormalCount1() {
        return normalCount1;
    }

    public void setNormalCount1(BigDecimal normalCount1) {
        this.normalCount1 = normalCount1;
    }

    public BigDecimal getNormalCount2() {
        return normalCount2;
    }

    public void setNormalCount2(BigDecimal normalCount2) {
        this.normalCount2 = normalCount2;
    }

    public BigDecimal getNormalCount3() {
        return normalCount3;
    }

    public void setNormalCount3(BigDecimal normalCount3) {
        this.normalCount3 = normalCount3;
    }

    public BigDecimal getFollowCount1() {
        return followCount1;
    }

    public void setFollowCount1(BigDecimal followCount1) {
        this.followCount1 = followCount1;
    }

    public BigDecimal getFollowCount2() {
        return followCount2;
    }

    public void setFollowCount2(BigDecimal followCount2) {
        this.followCount2 = followCount2;
    }

    public BigDecimal getFollowCount3() {
        return followCount3;
    }

    public void setFollowCount3(BigDecimal followCount3) {
        this.followCount3 = followCount3;
    }

    public BigDecimal getInferiorityCount1() {
        return inferiorityCount1;
    }

    public void setInferiorityCount1(BigDecimal inferiorityCount1) {
        this.inferiorityCount1 = inferiorityCount1;
    }

    public BigDecimal getInferiorityCount2() {
        return inferiorityCount2;
    }

    public void setInferiorityCount2(BigDecimal inferiorityCount2) {
        this.inferiorityCount2 = inferiorityCount2;
    }

    public BigDecimal getSuspiciousCount1() {
        return suspiciousCount1;
    }

    public void setSuspiciousCount1(BigDecimal suspiciousCount1) {
        this.suspiciousCount1 = suspiciousCount1;
    }

    public BigDecimal getDamageCount1() {
        return damageCount1;
    }

    public void setDamageCount1(BigDecimal damageCount1) {
        this.damageCount1 = damageCount1;
    }

    public BigDecimal getSubtotal1() {
        return subtotal1;
    }

    public void setSubtotal1(BigDecimal subtotal1) {
        this.subtotal1 = subtotal1;
    }

    public BigDecimal getNormalCount4() {
        return normalCount4;
    }

    public void setNormalCount4(BigDecimal normalCount4) {
        this.normalCount4 = normalCount4;
    }

    public BigDecimal getFollowCount4() {
        return followCount4;
    }

    public void setFollowCount4(BigDecimal followCount4) {
        this.followCount4 = followCount4;
    }

    public BigDecimal getInferiorityCount4() {
        return inferiorityCount4;
    }

    public void setInferiorityCount4(BigDecimal inferiorityCount4) {
        this.inferiorityCount4 = inferiorityCount4;
    }

    public BigDecimal getSuspiciousCount4() {
        return suspiciousCount4;
    }

    public void setSuspiciousCount4(BigDecimal suspiciousCount4) {
        this.suspiciousCount4 = suspiciousCount4;
    }

    public BigDecimal getDamageCount4() {
        return damageCount4;
    }

    public void setDamageCount4(BigDecimal damageCount4) {
        this.damageCount4 = damageCount4;
    }
}
