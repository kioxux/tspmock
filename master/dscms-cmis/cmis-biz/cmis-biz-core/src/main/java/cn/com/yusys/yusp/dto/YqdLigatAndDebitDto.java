package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.civil.Cases;

import java.util.List;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/2 10:56
 * @desc 用来存放优企贷风险提示信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class YqdLigatAndDebitDto {

    /**  黑灰名单 */
    private List<HhmdkhRespDto> HhmdkhList;
    /** 征信指标*/
    private List<R007Dto> r007Dto;

    public List<HhmdkhRespDto> getHhmdkhList() {
        return HhmdkhList;
    }

    public void setHhmdkhList(List<HhmdkhRespDto> hhmdkhList) {
        HhmdkhList = hhmdkhList;
    }

    public List<R007Dto> getR007Dto() {
        return r007Dto;
    }

    public void setR007Dto(List<R007Dto> r007Dto) {
        this.r007Dto = r007Dto;
    }
}
