package cn.com.yusys.yusp.web.server.xdqt0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0004.req.Xdqt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.resp.Xdqt0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0004.Xdqt0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:贷款申请预约（个人客户）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0004:贷款申请预约（个人客户）")
@RestController
@RequestMapping("/api/bizqt4bsp")
public class BizXdqt0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdqt0004Resource.class);

    @Autowired
	private Xdqt0004Service xdqt0004Service;
    /**
     * 交易码：xdqt0004
     * 交易描述：贷款申请预约（个人客户）
     *
     * @param xdqt0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷款申请预约（个人客户）")
    @PostMapping("/xdqt0004")
    protected @ResponseBody
    ResultDto<Xdqt0004DataRespDto> xdqt0004(@Validated @RequestBody Xdqt0004DataReqDto xdqt0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004DataReqDto));
        Xdqt0004DataRespDto xdqt0004DataRespDto = new Xdqt0004DataRespDto();// 响应Dto:贷款申请预约（个人客户）
        ResultDto<Xdqt0004DataRespDto> xdqt0004DataResultDto = new ResultDto<>();
		String loanBank = xdqt0004DataReqDto.getLoanBank();//贷款服务行
		String addr = xdqt0004DataReqDto.getAddr();//地址
		String procde = xdqt0004DataReqDto.getProcde();//邮编
		String phone = xdqt0004DataReqDto.getPhone();//电话
		String loanUse = xdqt0004DataReqDto.getLoanUse();//贷款用途
		BigDecimal loanBal = xdqt0004DataReqDto.getLoanBal();//贷款金额
		String loanTerm = xdqt0004DataReqDto.getLoanTerm();//贷款期限
		String guarType = xdqt0004DataReqDto.getGuarType();//担保方式
		String cusName = xdqt0004DataReqDto.getCusName();//姓名
		String sex = xdqt0004DataReqDto.getSex();//性别
		String certType = xdqt0004DataReqDto.getCertType();//证件类型
		String certNo = xdqt0004DataReqDto.getCertNo();//证件号
		String mobile = xdqt0004DataReqDto.getMobile();//手机号
		String edu = xdqt0004DataReqDto.getEdu();//学历
		String marStatus = xdqt0004DataReqDto.getMarStatus();//婚姻状况
		String indivRelComName = xdqt0004DataReqDto.getIndivRelComName();//单位名称
		String indivComTyp = xdqt0004DataReqDto.getIndivComTyp();//单位性质
		String isLocal = xdqt0004DataReqDto.getIsLocal();//是否本地户口
		BigDecimal yearn = xdqt0004DataReqDto.getYearn();//年收入
		String pldType = xdqt0004DataReqDto.getPldType();//抵押物类型
		String collType = xdqt0004DataReqDto.getCollType();//质押物类型
		String duty = xdqt0004DataReqDto.getDuty();//职务
		String indivRsdAddr = xdqt0004DataReqDto.getIndivRsdAddr();//居住地址
		String infoSour = xdqt0004DataReqDto.getInfoSour();//信息来源
		String loanType = xdqt0004DataReqDto.getLoanType();//经营或消费性贷款
        try {
            // 从xdqt0004DataReqDto获取业务值进行业务逻辑处理
            // 调用XXXXXService层开始
			xdqt0004DataRespDto = xdqt0004Service.WYtoCmisAction(xdqt0004DataReqDto);
            // 调用XXXXXService层结束
            // 封装xdqt0004DataRespDto对象开始
            // 封装xdqt0004DataRespDto对象结束
            // 封装xdqt0004DataResultDto中正确的返回码和返回信息
            xdqt0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
			logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, e.getMessage());
			// 封装xdqt0004DataResultDto中异常返回码和返回信息
			xdqt0004DataResultDto.setCode(EcbEnum.ECB019999.key);
			xdqt0004DataResultDto.setMessage(EcbEnum.ECB019999.value);
		}
        catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, e.getMessage());
            // 封装xdqt0004DataResultDto中异常返回码和返回信息
            xdqt0004DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0004DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0004DataRespDto到xdqt0004DataResultDto中
        xdqt0004DataResultDto.setData(xdqt0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004DataResultDto));
        return xdqt0004DataResultDto;
    }
}
