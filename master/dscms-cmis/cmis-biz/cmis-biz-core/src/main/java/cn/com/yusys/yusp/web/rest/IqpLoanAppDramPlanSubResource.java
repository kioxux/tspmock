/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constant.CommonConstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub;
import cn.com.yusys.yusp.service.IqpLoanAppDramPlanSubService;

import javax.management.Query;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppDramPlanSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-23 14:36:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploanappdramplansub")
public class IqpLoanAppDramPlanSubResource {
    @Autowired
    private IqpLoanAppDramPlanSubService iqpLoanAppDramPlanSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanAppDramPlanSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanAppDramPlanSub> list = iqpLoanAppDramPlanSubService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanAppDramPlanSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanAppDramPlanSub>> index(QueryModel queryModel) {
        List<IqpLoanAppDramPlanSub> list = iqpLoanAppDramPlanSubService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanAppDramPlanSub>>(list);
    }
    /***
     * @param iqpLoanAppDramPlanSub
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub>>
     * @author hubp
     * @date 2021/4/24 11:06
     * @version 1.0.0
     * @desc    根据合同号查询提款信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectByContNo")
    protected ResultDto<List<IqpLoanAppDramPlanSub>> selectByContNo(@RequestBody IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub) {
        List<IqpLoanAppDramPlanSub> list = iqpLoanAppDramPlanSubService.selectByContNo(iqpLoanAppDramPlanSub.getContNo());
        return new ResultDto<List<IqpLoanAppDramPlanSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpLoanAppDramPlanSub> show(@PathVariable("pkId") String pkId) {
        IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub = iqpLoanAppDramPlanSubService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpLoanAppDramPlanSub>(iqpLoanAppDramPlanSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpLoanAppDramPlanSub> create(@RequestBody IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub) throws URISyntaxException {
        iqpLoanAppDramPlanSubService.insert(iqpLoanAppDramPlanSub);
        return new ResultDto<IqpLoanAppDramPlanSub>(iqpLoanAppDramPlanSub);
    }
    /***
     * @param iqpLoanAppDramPlanSub
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/4/24 9:43
     * @version 1.0.0
     * @desc    合同管理-修改-提款方式保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryiqploanappdramplansub")
    protected ResultDto<Integer> queryIqpLoanAppDramPlanSub(@RequestBody IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub) throws URISyntaxException {
        int result = iqpLoanAppDramPlanSubService.queryIqpLoanAppDramPlanSub(iqpLoanAppDramPlanSub);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub) throws URISyntaxException {
        int result = iqpLoanAppDramPlanSubService.update(iqpLoanAppDramPlanSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpLoanAppDramPlanSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanAppDramPlanSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByIqpSerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByIqpSerno")
    protected ResultDto<List<IqpLoanAppDramPlanSub>> selectByIqpSerno(@RequestBody QueryModel model) {
        List<IqpLoanAppDramPlanSub> result = iqpLoanAppDramPlanSubService.selectByIqpSerno(model);
        return new ResultDto<List<IqpLoanAppDramPlanSub>>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPkId")
    protected ResultDto<Integer> deleteByPkId(@RequestBody Map map) {
        int result = 0;
        IqpLoanAppDramPlanSub iqpLoanAppDramPlanSub = iqpLoanAppDramPlanSubService.selectByPrimaryKey((String) map.get("pkId"));
        if(iqpLoanAppDramPlanSub != null){
            iqpLoanAppDramPlanSub.setOprType(CommonConstance.OPR_TYPE_DELETE);
            result = iqpLoanAppDramPlanSubService.updateSelective(iqpLoanAppDramPlanSub);
        }else{
            // 不做操作  直接返回
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addOrUpdateAllTable
     * @函数描述:新增或保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addOrUpdateAllTable")
    protected ResultDto<Boolean> addOrUpdateAllTable(@RequestBody List<IqpLoanAppDramPlanSub> list) {
        boolean result = iqpLoanAppDramPlanSubService.addOrUpdateAllTable(list);
        return new ResultDto<Boolean>(result);
    }
}
