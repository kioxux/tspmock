package cn.com.yusys.yusp.service.server.xdxw0060;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0060.req.Xdxw0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0060.resp.Xdxw0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 业务逻辑类:客户及配偶信用类小微业务贷款授信金额
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdxw0060Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0060Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CmisCus0006Service cmisCus0006Service;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 交易码：xdxw0060
     * 交易描述：客户及配偶信用类小微业务贷款授信金额
     *
     * @param xdxw0060DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0060DataRespDto xdxw0060(Xdxw0060DataReqDto xdxw0060DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataReqDto));
        Xdxw0060DataRespDto xdxw0060DataRespDto = new Xdxw0060DataRespDto();
        try {
            List<String> cusIds = new ArrayList<>();
            String certNo = xdxw0060DataReqDto.getCertNo();//身份证号
            /***********************第一步：通过客户证件号查询客户信息*********************************/
            logger.info("****************根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(certNo)).orElse(new CusBaseClientDto());
            logger.info("****************根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusBaseClientDto));//通过客户证件号查询客户信息
            // 客户编号
            String cusId = cusBaseClientDto.getCusId();
            if (StringUtil.isEmpty(cusId)) {
                logger.info("根据证件号码【{}】未查询到该客户信息,该客户为新客户！", certNo);
                xdxw0060DataRespDto.setTotlApplyAmount(BigDecimal.ZERO);
                return xdxw0060DataRespDto;
            }
            cusIds.add(cusId);
            /***********************第二步：根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号**********/
            String spcusId = "";
            logger.info("************根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号开始,查询参数为:{}", JSON.toJSONString(certNo));
            ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = cmisCusClientService.cmiscus0013(certNo);
            logger.info("************根据证件编号,证件类型查询是否存在配偶 存在则返回配偶客户编号结束,返回结果为:{}", JSON.toJSONString(cmisCus0013RespDtoResultDto));
            if (ResultDto.success().getCode().equals(cmisCus0013RespDtoResultDto.getCode())) {
                CmisCus0013RespDto cmisCus0013RespDto = cmisCus0013RespDtoResultDto.getData();
                if (!Objects.isNull(cmisCus0013RespDto)) {
                    spcusId = cmisCus0013RespDto.getCusId();//配偶客户号
                    cusIds.add(spcusId);
                }
            }
            /**********************第三步：根据申请人证件号码汇总客户及配偶信用类小微业务贷款授信金额**********/
            Map queryMap = new HashMap<>();
            queryMap.put("cusIds", cusIds);
            // 根据借据号查询本行其他贷款拖欠情况
            logger.info("根据申请人证件号码汇总客户及配偶信用类小微业务贷款授信金额开始,查询参数为:{}", JSON.toJSONString(queryMap));
            BigDecimal totlApplyAmount = ctrLoanContMapper.queryCusAndSpouseForCreditCrdLoan(queryMap);// 授信总金额
            logger.info("根据申请人证件号码汇总客户及配偶信用类小微业务贷款授信金额结束,返回结果为:{}", JSON.toJSONString(totlApplyAmount));
            xdxw0060DataRespDto.setTotlApplyAmount(totlApplyAmount);// 授信总金额
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060DataRespDto));
        return xdxw0060DataRespDto;
    }
}
