/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpAccpApp;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpTfLocApp;
import cn.com.yusys.yusp.service.IqpTfLocAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpTfLocAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:07:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "开证合同申请")
@RequestMapping("/api/iqptflocapp")
public class IqpTfLocAppResource {
    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpTfLocApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpTfLocApp> list = iqpTfLocAppService.selectAll(queryModel);
        return new ResultDto<List<IqpTfLocApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpTfLocApp>> index(QueryModel queryModel) {
        List<IqpTfLocApp> list = iqpTfLocAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpTfLocApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpTfLocApp> show(@PathVariable("pkId") String pkId) {
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpTfLocApp>(iqpTfLocApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpTfLocApp> create(@RequestBody IqpTfLocApp iqpTfLocApp) throws URISyntaxException {
        iqpTfLocAppService.insert(iqpTfLocApp);
        return new ResultDto<IqpTfLocApp>(iqpTfLocApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpTfLocApp iqpTfLocApp) throws URISyntaxException {
        int result = iqpTfLocAppService.update(iqpTfLocApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpTfLocAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpTfLocAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 开证申请保存方法
     * @param iqpTfLocApp
     * @return
     */
    @ApiOperation("开证申请保存方法")
    @PostMapping("/saveiqptflocappinfo")
    public ResultDto<Map> saveIqpTflocAppInfo(@RequestBody IqpTfLocApp iqpTfLocApp){
        Map result = iqpTfLocAppService.saveIqpTfLocAppInfo(iqpTfLocApp);
        return new ResultDto<>(result);
    }

    /**
     * 开证申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("开证申请通用的保存方法")
    @PostMapping("/commonsaveiqptflocappinfo")
    public ResultDto<Map> commonSaveIqpTfLocAppInfo(@RequestBody Map params){
        Map rtnData = iqpTfLocAppService.commonSaveIqpTfLocAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:开证申请待发起列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("开证申请待发起列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpTfLocApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpTfLocApp> list = iqpTfLocAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpTfLocApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:开证申请历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("开证申请历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpTfLocApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpTfLocApp> list = iqpTfLocAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpTfLocApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("开证合同申请明细展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByTfLocSernoKey((String)map.get("serno"));
        if (iqpTfLocApp != null) {
            resultDto.setCode(0);
            resultDto.setData(iqpTfLocApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(0);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpTfLocAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * 开证合同申请逻辑删除方法
     * @param iqpTfLocApp
     * @return
     */
    @ApiOperation("开证合同申请逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpTfLocApp iqpTfLocApp){
        Integer result = iqpTfLocAppService.logicDelete(iqpTfLocApp);
        return new ResultDto<Integer>(result);
    }
}
