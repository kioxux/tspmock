/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.domain.RptSpdAnysGdxmd;
import cn.com.yusys.yusp.domain.RptSpdAnysSxkd;
import cn.com.yusys.yusp.dto.PfkInfoDto;
import cn.com.yusys.yusp.dto.PfkInfosDto;
import cn.com.yusys.yusp.service.RptBasicInfoPersFamilyAssetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysJxd;
import cn.com.yusys.yusp.service.RptSpdAnysJxdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJxdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-21 20:19:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysjxd")
public class RptSpdAnysJxdResource {
    @Autowired
    private RptSpdAnysJxdService rptSpdAnysJxdService;

    @Autowired
    private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysJxd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysJxd> list = rptSpdAnysJxdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysJxd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysJxd>> index(QueryModel queryModel) {
        List<RptSpdAnysJxd> list = rptSpdAnysJxdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysJxd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysJxd> show(@PathVariable("serno") String serno) {
        RptSpdAnysJxd rptSpdAnysJxd = rptSpdAnysJxdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysJxd>(rptSpdAnysJxd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysJxd> create(@RequestBody RptSpdAnysJxd rptSpdAnysJxd) throws URISyntaxException {
        rptSpdAnysJxdService.insert(rptSpdAnysJxd);
        return new ResultDto<RptSpdAnysJxd>(rptSpdAnysJxd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysJxd rptSpdAnysJxd) throws URISyntaxException {
        int result = rptSpdAnysJxdService.update(rptSpdAnysJxd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysJxdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysJxdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysJxd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysJxd rptSpdAnysJxd = rptSpdAnysJxdService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysJxd);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysJxd> save(@RequestBody RptSpdAnysJxd rptSpdAnysJxd) throws URISyntaxException {
        RptSpdAnysJxd rptSpdAnysJxd1 = rptSpdAnysJxdService.selectByPrimaryKey(rptSpdAnysJxd.getSerno());
        if(rptSpdAnysJxd1!=null){
            rptSpdAnysJxdService.update(rptSpdAnysJxd);
        }else{
            rptSpdAnysJxdService.insert(rptSpdAnysJxd);
        }
        return ResultDto.success(rptSpdAnysJxd);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询抵押担保情况
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryAssetInfo")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> queryAssetInfo(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssets = rptBasicInfoPersFamilyAssetsService.queryGuarByLmtSerno(serno);
        return  ResultDto.success(rptBasicInfoPersFamilyAssets);
    }

    @PostMapping("/initJxd")
    protected ResultDto<Integer> initJxd(@RequestBody RptSpdAnysJxd rptSpdAnysJxd){
        return new ResultDto<Integer>(rptSpdAnysJxdService.insertSelective(rptSpdAnysJxd));
    }

    /**
     * 评分卡自动打分
     * @param params
     * @return
     */
    @PostMapping("/autoValue")
    protected ResultDto<Integer> autoValue(@RequestBody Map params){
        return new ResultDto<Integer>(rptSpdAnysJxdService.autoValue(params));
    }
}
