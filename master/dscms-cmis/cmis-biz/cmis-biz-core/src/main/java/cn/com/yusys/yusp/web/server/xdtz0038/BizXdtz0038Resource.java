package cn.com.yusys.yusp.web.server.xdtz0038;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0038.req.Xdtz0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.resp.Xdtz0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0038.Xdtz0038Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:台账信息通用列表查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0038:台账信息通用列表查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0038Resource.class);

    @Autowired
    private Xdtz0038Service xdtz0038Service;

    /**
     * 交易码：xdtz0038
     * 交易描述：台账信息通用列表查询
     *
     * @param xdtz0038DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账信息通用列表查询")
    @PostMapping("/xdtz0038")
    protected @ResponseBody
    ResultDto<Xdtz0038DataRespDto> xdtz0038(@Validated @RequestBody Xdtz0038DataReqDto xdtz0038DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataReqDto));
        Xdtz0038DataRespDto xdtz0038DataRespDto = new Xdtz0038DataRespDto();// 响应Dto:台账信息通用列表查询
        ResultDto<Xdtz0038DataRespDto> xdtz0038DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0038DataReqDto));
            xdtz0038DataRespDto = xdtz0038Service.xdtz0038(xdtz0038DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0038DataRespDto));
            // 封装xdtz0038DataResultDto中正确的返回码和返回信息
            xdtz0038DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0038DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, e.getMessage());
            // 封装xdtz0038DataResultDto中异常返回码和返回信息

            xdtz0038DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0038DataResultDto.setMessage(EpbEnum.EPB099999.value);

        }
        // 封装xdtz0038DataRespDto到xdtz0038DataResultDto中
        xdtz0038DataResultDto.setData(xdtz0038DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataRespDto));
        return xdtz0038DataResultDto;
    }
}
