/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtIntbankReply;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import cn.com.yusys.yusp.domain.LmtSigInvestRst;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRstMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-16 15:34:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtSigInvestRstMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtSigInvestRst selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtSigInvestRst> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtSigInvestRst record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtSigInvestRst record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtSigInvestRst record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtSigInvestRst record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称：queryLmtSigInvestRstDataByParams
     * @方法描述：通过参数查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangjw
     * @创建时间：2021-07-22
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtSigInvestRst> queryLmtSigInvestRstDataByParams(HashMap<String, String> paramMap);

    /**
     * @方法名称: selectSigInvestInvestType
     * @方法描述: 查询符合条件的债券投资授信额度，可以将其纳入进债券池额度内
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtSigInvestRst> selectSigInvestInvestType(QueryModel model);

    /**
     * 根据批复流水号查询授信批复信息
     * @param replySerno
     * @return
     */
    LmtSigInvestRst selectByReplySerno(@Param("replySerno") String replySerno);
   
}