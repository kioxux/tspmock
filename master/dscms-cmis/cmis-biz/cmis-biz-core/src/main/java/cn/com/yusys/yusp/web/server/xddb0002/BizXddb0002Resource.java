package cn.com.yusys.yusp.web.server.xddb0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0002.req.Xddb0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0002.resp.Xddb0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0002.Xddb0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:押品状态查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "xddb0002:押品状态查询")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXddb0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0002Resource.class);

    @Autowired
    private Xddb0002Service xddb0002Service;

    /**
     * 交易码：xddb0002
     * 交易描述：押品状态查询
     *
     * @param xddb0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态查询")
    @PostMapping("/xddb0002")
    protected @ResponseBody
    ResultDto<Xddb0002DataRespDto> xddb0002(@Validated @RequestBody Xddb0002DataReqDto xddb0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002DataReqDto));
        Xddb0002DataRespDto xddb0002DataRespDto = new Xddb0002DataRespDto();// 响应Dto:押品状态查询
        ResultDto<Xddb0002DataRespDto> xddb0002DataResultDto = new ResultDto<>();
        // 从xddb0002DataReqDto获取业务值进行业务逻辑处理
        try {
            //获取押品编号
            String coreGrtNo = xddb0002DataReqDto.getGuarNo();//押品编号
            if (StringUtil.isNotEmpty(coreGrtNo)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(coreGrtNo));
                xddb0002DataRespDto = xddb0002Service.selectStateByguarNo(xddb0002DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xddb0002DataRespDto));
                // 封装xddb0002DataResultDto中正确的返回码和返回信息
                xddb0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                logger.info("******请求参数为空*****");
                xddb0002DataResultDto.setCode(EcbEnum.ECB010001.key);
                xddb0002DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            // 封装xddb0002DataResultDto中异常返回码和返回信息
            xddb0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0002DataRespDto到xddb0002DataResultDto中
        xddb0002DataResultDto.setData(xddb0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002DataResultDto));
        return xddb0002DataResultDto;
    }
}
