/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskXdGuaranty
 * @类描述: RISK_XD_GUARANTY数据实体类
 * @功能描述:
 * @创建人: zy
 * @创建时间: 2021-06-05 12:28:49
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "RISK_XD_GUARANTY")
public class RiskXdGuaranty extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 抵押品编号 **/
	@Column(name = "DY_NO", unique = false, nullable = true, length = 40)
	private String dyNo;

	/** 不动产证号 **/
	@Column(name = "BDC_NO", unique = false, nullable = true, length = 100)
	private String bdcNo;

	/** 借款人名称 **/
	@Column(name = "JK_CUS_NAME", unique = false, nullable = true, length = 40)
	private String jkCusName;

	/** 押品所有人名称 **/
	@Column(name = "YP_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String ypManagerId;

	/** 押品权证编号 **/
	@Column(name = "YP_BOOK_SERNO", unique = false, nullable = true, length = 150)
	private String ypBookSerno;

	/** 查询查封结果 **/
	@Column(name = "CX_CF_RESULT", unique = false, nullable = true, length = 500)
	private String cxCfResult;

	/** 接收风控信息时间 **/
	@Column(name = "APPLY_TIME", unique = false, nullable = true, length = 100)
	private String applyTime;

	/** 客户经理号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 100)
	private String managerId;

	/** 修改次数 **/
	@Column(name = "IS_UPDATE", unique = false, nullable = true, length = 10)
	private String isUpdate;

	/** 查询查封提交时间 **/
	@Column(name = "SUBMIT_TIME", unique = false, nullable = true, length = 40)
	private String submitTime;

	/** 合同号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 60)
	private String contNo;

	/** 申请流水 **/
	@Column(name = "PRE_APP_NO", unique = false, nullable = true, length = 60)
	private String preAppNo;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 3)
	private String approveStatus;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param dyNo
	 */
	public void setDyNo(String dyNo) {
		this.dyNo = dyNo;
	}

	/**
	 * @return dyNo
	 */
	public String getDyNo() {
		return this.dyNo;
	}

	/**
	 * @param bdcNo
	 */
	public void setBdcNo(String bdcNo) {
		this.bdcNo = bdcNo;
	}

	/**
	 * @return bdcNo
	 */
	public String getBdcNo() {
		return this.bdcNo;
	}

	/**
	 * @param jkCusName
	 */
	public void setJkCusName(String jkCusName) {
		this.jkCusName = jkCusName;
	}

	/**
	 * @return jkCusName
	 */
	public String getJkCusName() {
		return this.jkCusName;
	}

	/**
	 * @param ypManagerId
	 */
	public void setYpManagerId(String ypManagerId) {
		this.ypManagerId = ypManagerId;
	}

	/**
	 * @return ypManagerId
	 */
	public String getYpManagerId() {
		return this.ypManagerId;
	}

	/**
	 * @param ypBookSerno
	 */
	public void setYpBookSerno(String ypBookSerno) {
		this.ypBookSerno = ypBookSerno;
	}

	/**
	 * @return ypBookSerno
	 */
	public String getYpBookSerno() {
		return this.ypBookSerno;
	}

	/**
	 * @param cxCfResult
	 */
	public void setCxCfResult(String cxCfResult) {
		this.cxCfResult = cxCfResult;
	}

	/**
	 * @return cxCfResult
	 */
	public String getCxCfResult() {
		return this.cxCfResult;
	}

	/**
	 * @param applyTime
	 */
	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	/**
	 * @return applyTime
	 */
	public String getApplyTime() {
		return this.applyTime;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param isUpdate
	 */
	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}

	/**
	 * @return isUpdate
	 */
	public String getIsUpdate() {
		return this.isUpdate;
	}

	/**
	 * @param submitTime
	 */
	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	/**
	 * @return submitTime
	 */
	public String getSubmitTime() {
		return this.submitTime;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param preAppNo
	 */
	public void setPreAppNo(String preAppNo) {
		this.preAppNo = preAppNo;
	}

	/**
	 * @return preAppNo
	 */
	public String getPreAppNo() {
		return this.preAppNo;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}


}