/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDisAssetNFinan;
import cn.com.yusys.yusp.repository.mapper.IqpDisAssetNFinanMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetNFinanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpDisAssetNFinanService {

    @Autowired
    private IqpDisAssetNFinanMapper iqpDisAssetNFinanMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpDisAssetNFinan selectByPrimaryKey(String nfinanPk) {
        return iqpDisAssetNFinanMapper.selectByPrimaryKey(nfinanPk);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDisAssetNFinan> selectAll(QueryModel model) {
        List<IqpDisAssetNFinan> records = (List<IqpDisAssetNFinan>) iqpDisAssetNFinanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpDisAssetNFinan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDisAssetNFinan> list = iqpDisAssetNFinanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpDisAssetNFinan record) {
        return iqpDisAssetNFinanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpDisAssetNFinan record) {
        return iqpDisAssetNFinanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpDisAssetNFinan record) {
        return iqpDisAssetNFinanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpDisAssetNFinan record) {
        return iqpDisAssetNFinanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String nfinanPk) {
        return iqpDisAssetNFinanMapper.deleteByPrimaryKey(nfinanPk);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDisAssetNFinanMapper.deleteByIds(ids);
    }

    public List<IqpDisAssetNFinan> selectByIqpSerno(String iqpSernoOld) {
        return iqpDisAssetNFinanMapper.selectByIqpSerno(iqpSernoOld);
    }
}
