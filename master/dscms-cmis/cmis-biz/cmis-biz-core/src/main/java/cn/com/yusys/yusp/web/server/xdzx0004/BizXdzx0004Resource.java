package cn.com.yusys.yusp.web.server.xdzx0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzx0004.Xdzx0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZX0004:授权结果反馈接口")
@RestController
@RequestMapping("/api/bizzx4bsp")
public class BizXdzx0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzx0004Resource.class);

    @Autowired
    private Xdzx0004Service xdzx0004Service;

    /**
     * 交易码：xdzx0004
     * 交易描述：授权结果反馈接口
     *
     * @param xdzx0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授权结果反馈接口")
    @PostMapping("/xdzx0004")
    protected @ResponseBody
    ResultDto<Xdzx0004DataRespDto> xdzx0004(@Validated @RequestBody Xdzx0004DataReqDto xdzx0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataReqDto));
        Xdzx0004DataRespDto xdzx0004DataRespDto = new Xdzx0004DataRespDto();// 响应Dto:授权结果反馈接口
        ResultDto<Xdzx0004DataRespDto> xdzx0004DataResultDto = new ResultDto<>();
        try {
            // 从xdzx0004DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataReqDto));
            xdzx0004DataRespDto = xdzx0004Service.xdzx0004(xdzx0004DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataRespDto));
            // 封装xdzx0004DataResultDto中正确的返回码和返回信息
            xdzx0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzx0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, e.getMessage());
            // 封装xdzx0004DataResultDto中异常返回码和返回信息
            xdzx0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzx0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzx0004DataRespDto到xdzx0004DataResultDto中
        xdzx0004DataResultDto.setData(xdzx0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataResultDto));
        return xdzx0004DataResultDto;
    }
}
