package cn.com.yusys.yusp.web.server.xdls0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdls0005.req.Xdls0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0005.resp.Xdls0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:智能风控调用信贷通知
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDLS0005:智能风控调用信贷通知")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXdls0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdls0005Resource.class);

    /**
     * 交易码：xdls0005
     * 交易描述：智能风控调用信贷通知
     *
     * @param xdls0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("智能风控调用信贷通知")
    @PostMapping("/xdls0005")
    protected @ResponseBody
    ResultDto<Xdls0005DataRespDto> xdls0005(@Validated @RequestBody Xdls0005DataReqDto xdls0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005DataReqDto));
        Xdls0005DataRespDto xdls0005DataRespDto = new Xdls0005DataRespDto();// 响应Dto:智能风控调用信贷通知
        ResultDto<Xdls0005DataRespDto> xdls0005DataResultDto = new ResultDto<>();
        try {
			String serviceId = xdls0005DataReqDto.getServiceId();//服务编号
			String opflag = xdls0005DataReqDto.getOpflag();//操作类型
            // 从xdls0005DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdls0005DataRespDto对象开始
			xdls0005DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
			xdls0005DataRespDto.setFilePath(StringUtils.EMPTY);// 文件路径
			xdls0005DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdls0005DataRespDto对象结束
            // 封装xdls0005DataResultDto中正确的返回码和返回信息
            xdls0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdls0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, e.getMessage());
            // 封装xdls0005DataResultDto中异常返回码和返回信息
            // TODO EpbEnum.EPB099999 待调整 开始
            xdls0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdls0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EpbEnum.EPB099999 待调整  结束
        }
        // 封装xdls0005DataRespDto到xdls0005DataResultDto中
        xdls0005DataResultDto.setData(xdls0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005DataRespDto));
        return xdls0005DataResultDto;
    }
}
