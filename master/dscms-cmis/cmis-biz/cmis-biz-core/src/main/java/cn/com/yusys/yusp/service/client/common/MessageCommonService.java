package cn.com.yusys.yusp.service.client.common;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.service.MessageSendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: MessageCommonService
 * @类描述: 短信发送方法
 * @功能描述: 短信发送方法
 * @创建人: 王玉坤
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class MessageCommonService {
    private Logger logger = LoggerFactory.getLogger(MessageCommonService.class);

    @Autowired
    private MessageSendService messageSendService;

    /**
     * @param paramMap: 短信填充参数
     *                  messageType：短信编号
     *                  receivedUserType：接收人员类型 1--客户经理 2--借款人
     *                  managerId: 客户经理工号
     *                  linkWay: 联系方式， 可支持电话号码/邮箱/微信号 借款人必输
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author 王玉坤
     * @date 2021/9/5 16:05
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<Integer> sendMessage(String messageType, Map paramMap, String receivedUserType, String managerId, String linkWay) {
        logger.info("发送消息开始，短信编号【{}】、接收人员类型【{}】、客户经理工号【{}】、联系方式【{}】、发送内容【{}】",
                messageType, receivedUserType, managerId, linkWay, paramMap.toString());
        // 短信发送对象
        MessageSendDto messageSendDto = new MessageSendDto();
        // 发送人员列表，可支持多个
        List<ReceivedUserDto> receivedUserList = null;
        // 发送人员信息
        ReceivedUserDto receivedUserDto = null;
        // 返回对象
        ResultDto<Integer> resultDto = null;
        try {
            messageSendDto.setMessageType(messageType);
            messageSendDto.setParams(paramMap);

            receivedUserList = new ArrayList<>();
            receivedUserDto = new ReceivedUserDto();
            receivedUserDto.setReceivedUserType(receivedUserType);
            receivedUserDto.setUserId(managerId);
            receivedUserDto.setMobilePhone(linkWay);
            receivedUserList.add(receivedUserDto);
            messageSendDto.setReceivedUserList(receivedUserList);

            resultDto = messageSendService.sendMessage(messageSendDto);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("发送消息失败，短信编号【{}】", messageType);
            return new ResultDto<Integer>(-1);
        }
        logger.info("发送消息结束，短信编号【{}】、接收人员类型【{}】、客户经理工号【{}】、联系方式【{}】、发送内容【{}】",
                messageType, receivedUserType, managerId, linkWay, paramMap.toString());
        return resultDto;
    }

    /**
     * @param paramMap: 短信填充参数
     *                  messageType：短信编号
     *                  receivedUserType：接收人员类型 1--客户经理 2--借款人
     *                  managerId: 客户经理工号
     *                  linkWay: 联系方式， 可支持电话号码/邮箱/微信号 借款人必输
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author 王玉坤
     * @date 2021/9/5 16:05
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<Integer> sendonlinepldremind(String messageType, String receivedUserType, Map paramMap) {
        logger.info("发送消息开始，短信编号【{}】、接收人员类型【{}】、发送内容【{}】",
                messageType, receivedUserType, paramMap.toString());
        // 短信发送对象
        MessageSendDto messageSendDto = new MessageSendDto();
        // 返回对象
        ResultDto<Integer> resultDto = null;
        try {
            messageSendDto.setMessageType(messageType);
            messageSendDto.setParams(paramMap);
            resultDto = messageSendService.sendOnlinePldRemind(messageSendDto);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("发送消息失败，短信编号【{}】", messageType);
            return new ResultDto<Integer>(-1);
        }
        logger.info("发送消息开始，短信编号【{}】、接收人员类型【{}】、发送内容【{}】",
                messageType, receivedUserType, paramMap.toString());
        return resultDto;
    }
}
