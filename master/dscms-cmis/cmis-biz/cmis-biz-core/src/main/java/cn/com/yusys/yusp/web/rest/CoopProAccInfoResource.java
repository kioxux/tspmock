/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CoopProAccInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopProAccInfo;
import cn.com.yusys.yusp.service.CoopProAccInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopProAccInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-14 23:45:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopproaccinfo")
public class CoopProAccInfoResource {
    @Autowired
    private CoopProAccInfoService coopProAccInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopProAccInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopProAccInfo> list = coopProAccInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopProAccInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopProAccInfo>> index(QueryModel queryModel) {
        List<CoopProAccInfo> list = coopProAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopProAccInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{proNo}")
    protected ResultDto<CoopProAccInfo> show(@PathVariable("proNo") String proNo) {
        CoopProAccInfo coopProAccInfo = coopProAccInfoService.selectByPrimaryKey(proNo);
        return new ResultDto<CoopProAccInfo>(coopProAccInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopProAccInfo> create(@RequestBody CoopProAccInfo coopProAccInfo) throws URISyntaxException {
        coopProAccInfoService.insert(coopProAccInfo);
        return new ResultDto<CoopProAccInfo>(coopProAccInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopProAccInfo coopProAccInfo) throws URISyntaxException {
        int result = coopProAccInfoService.update(coopProAccInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateStatusByProNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatestatusbyprono")
    protected ResultDto<Integer> updateStatusByProNo(@RequestBody CoopProAccInfoDto coopProAccInfoDto) throws URISyntaxException {
        int result = coopProAccInfoService.updateStatusByProNo(coopProAccInfoDto);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{proNo}")
    protected ResultDto<Integer> delete(@PathVariable("proNo") String proNo) {
        int result = coopProAccInfoService.deleteByPrimaryKey(proNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopProAccInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopProAccInfo>> query(@RequestBody QueryModel queryModel) {
        List<CoopProAccInfo> list = coopProAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopProAccInfo>>(list);
    }
}
