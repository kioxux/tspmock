/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubAppr;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoSubApprMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:22:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoSubApprService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoSubApprMapper lmtSigInvestBasicInfoSubApprMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSubAppr selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoSubAppr> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoSubAppr> records = (List<LmtSigInvestBasicInfoSubAppr>) lmtSigInvestBasicInfoSubApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoSubAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoSubAppr> list = lmtSigInvestBasicInfoSubApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoSubAppr record) {
        return lmtSigInvestBasicInfoSubApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoSubAppr record) {
        return lmtSigInvestBasicInfoSubApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoSubAppr record) {
        return lmtSigInvestBasicInfoSubApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoSubAppr record) {
        return lmtSigInvestBasicInfoSubApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoSubApprMapper.deleteByIds(ids);
    }


    /**
     * 根据申请流水号获取
     * @param serno
     * @return
     */
    public List<LmtSigInvestBasicInfoSubAppr> selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(serno)) throw new BizException(null,"",null,"申请流水号不允许为空！");
        queryModel.addCondition("serno",serno);
        queryModel.addCondition("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoSubApprList!=null && lmtSigInvestBasicInfoSubApprList.size()>0){
            return lmtSigInvestBasicInfoSubApprList ;
        }
        return null;
    }


    /**
     * @方法名称: initLmtSigInvestBasicInfoSubApprInfo
     * @方法描述: 根据申请表数据，生成审批表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSigInvestBasicInfoSubAppr initLmtSigInvestBasicInfoSubApprInfo(LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub) {
        //主键
        String pkValue = generatePkId();
        //初始化对象
        LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr = new LmtSigInvestBasicInfoSubAppr() ;
        //拷贝数
        BeanUtils.copyProperties(lmtSigInvestBasicInfoSub, lmtSigInvestBasicInfoSubAppr);
        //主键
        lmtSigInvestBasicInfoSubAppr.setPkId(pkValue);
        //最新更新日期
        lmtSigInvestBasicInfoSubAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicInfoSubAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicInfoSubAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //返回对象信息
        return lmtSigInvestBasicInfoSubAppr;
    }

    public LmtSigInvestBasicInfoSubAppr selectByBasicSerno(String basicSerno){
        return  lmtSigInvestBasicInfoSubApprMapper.selectByBasicSerno(basicSerno);
    }

    /**
     * 根据申请流水号获取
     * @param approveSerno
     * @return
     */
    public List<LmtSigInvestBasicInfoSubAppr> selectByApproveSerno(String approveSerno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(approveSerno)) throw new BizException(null,"",null,"审批编号不允许为空！");
        queryModel.addCondition("approveSerno",approveSerno);
        queryModel.addCondition("oprType", CmisCommonConstants.ADD_OPR);
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprMapper.selectByModel(queryModel);
        if (lmtSigInvestBasicInfoSubApprList!=null && lmtSigInvestBasicInfoSubApprList.size()>0){
            return lmtSigInvestBasicInfoSubApprList ;
        }
        return null;
    }

    public LmtSigInvestBasicInfoSubAppr selectByBasicSernoAndApproveSerno(String basicSerno, String approveSerno) {
        QueryModel queryModel = new QueryModel();
        if(StringUtils.isBlank(approveSerno)) throw new BizException(null,"",null,"审批流水号不允许为空！");
        if(StringUtils.isBlank(basicSerno)) throw new BizException(null,"",null,"底层申请流水号不允许为空！");
        queryModel.addCondition("approveSerno",approveSerno);
        queryModel.addCondition("basicSerno",basicSerno);
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprs = lmtSigInvestBasicInfoSubApprMapper.selectByModel(queryModel);
        if (CollectionUtils.isNotEmpty(lmtSigInvestBasicInfoSubApprs)){
            return lmtSigInvestBasicInfoSubApprs.get(0);
        }
        return null;
    }
}
