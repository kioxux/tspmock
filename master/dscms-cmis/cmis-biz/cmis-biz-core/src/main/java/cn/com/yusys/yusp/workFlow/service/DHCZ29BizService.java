package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 利率定价流程-经营性及消费非按揭 --东海村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class DHCZ29BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ29BizService.class);

    @Autowired
    private SGCZ29BizService sGCZ29BizService;
    @Autowired
    private BGYW01BizService bGYW01BizService;
    @Autowired
    private QTSX02BizService qTSX02BizService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //  利率定价流程 东海村镇
        if(CmisFlowConstants.FLOW_TYPE_TYPE_DHF02.equals(bizType)){
            sGCZ29BizService.iRetailPrimeRateBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_DHH01.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_DHH02.equals(bizType)){
            bGYW01BizService.iqpRateChgAppBizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_DHF01.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_DHF04.equals(bizType)){
            // 人民币利率定价
            qTSX02BizService.QT002biz(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.DHCZ29.equals(flowCode);
    }
}
