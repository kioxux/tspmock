/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubRst
 * @类描述: lmt_sig_invest_basic_info_sub_rst数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-23 21:27:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_basic_info_sub_rst")
public class LmtSigInvestBasicInfoSubRst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = false, length = 40)
	private String replySerno;
	
	/** 底层批复流水号 **/
	@Column(name = "BASIC_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String basicReplySerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 底层申请流水号 **/
	@Column(name = "BASIC_SERNO", unique = false, nullable = true, length = 40)
	private String basicSerno;
	
	/** 底层客户编号 **/
	@Column(name = "BASIC_CUS_ID", unique = false, nullable = true, length = 20)
	private String basicCusId;
	
	/** 底层客户名称 **/
	@Column(name = "BASIC_CUS_NAME", unique = false, nullable = true, length = 80)
	private String basicCusName;
	
	/** 底层客户大类 **/
	@Column(name = "BASIC_CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String basicCusCatalog;
	
	/** 底层客户类型 **/
	@Column(name = "BASIC_CUS_TYPE", unique = false, nullable = true, length = 5)
	private String basicCusType;
	
	/** 是否申报底层授信 **/
	@Column(name = "IS_APP_BASIC_LMT", unique = false, nullable = true, length = 5)
	private String isAppBasicLmt;
	
	/** 占用底层授信分项编号 **/
	@Column(name = "USE_BASIC_LMT_SUB_SERNO", unique = false, nullable = true, length = 40)
	private String useBasicLmtSubSerno;
	
	/** 底层资产类型 **/
	@Column(name = "BASIC_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String basicAssetType;
	
	/** 本项底层资产余额 **/
	@Column(name = "BASIC_ASSET_BALANCE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal basicAssetBalanceAmt;
	
	/** 底层资产名称 **/
	@Column(name = "BASIC_ASSET_NAME", unique = false, nullable = true, length = 80)
	private String basicAssetName;
	
	/** 底层资产到期日 **/
	@Column(name = "BASIC_ASSET_END_DATE", unique = false, nullable = true, length = 20)
	private String basicAssetEndDate;
	
	/** 底层资产剩余期限 **/
	@Column(name = "BASIC_ASSET_BALANCE_TERM", unique = false, nullable = true, length = 10)
	private Integer basicAssetBalanceTerm;
	
	/** 原底层关联台账号 **/
	@Column(name = "ORIGI_BASIC_ACC_NO", unique = false, nullable = true, length = 40)
	private String origiBasicAccNo;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param basicReplySerno
	 */
	public void setBasicReplySerno(String basicReplySerno) {
		this.basicReplySerno = basicReplySerno;
	}
	
    /**
     * @return basicReplySerno
     */
	public String getBasicReplySerno() {
		return this.basicReplySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param basicSerno
	 */
	public void setBasicSerno(String basicSerno) {
		this.basicSerno = basicSerno;
	}
	
    /**
     * @return basicSerno
     */
	public String getBasicSerno() {
		return this.basicSerno;
	}
	
	/**
	 * @param basicCusId
	 */
	public void setBasicCusId(String basicCusId) {
		this.basicCusId = basicCusId;
	}
	
    /**
     * @return basicCusId
     */
	public String getBasicCusId() {
		return this.basicCusId;
	}
	
	/**
	 * @param basicCusName
	 */
	public void setBasicCusName(String basicCusName) {
		this.basicCusName = basicCusName;
	}
	
    /**
     * @return basicCusName
     */
	public String getBasicCusName() {
		return this.basicCusName;
	}
	
	/**
	 * @param basicCusCatalog
	 */
	public void setBasicCusCatalog(String basicCusCatalog) {
		this.basicCusCatalog = basicCusCatalog;
	}
	
    /**
     * @return basicCusCatalog
     */
	public String getBasicCusCatalog() {
		return this.basicCusCatalog;
	}
	
	/**
	 * @param basicCusType
	 */
	public void setBasicCusType(String basicCusType) {
		this.basicCusType = basicCusType;
	}
	
    /**
     * @return basicCusType
     */
	public String getBasicCusType() {
		return this.basicCusType;
	}
	
	/**
	 * @param isAppBasicLmt
	 */
	public void setIsAppBasicLmt(String isAppBasicLmt) {
		this.isAppBasicLmt = isAppBasicLmt;
	}
	
    /**
     * @return isAppBasicLmt
     */
	public String getIsAppBasicLmt() {
		return this.isAppBasicLmt;
	}
	
	/**
	 * @param useBasicLmtSubSerno
	 */
	public void setUseBasicLmtSubSerno(String useBasicLmtSubSerno) {
		this.useBasicLmtSubSerno = useBasicLmtSubSerno;
	}
	
    /**
     * @return useBasicLmtSubSerno
     */
	public String getUseBasicLmtSubSerno() {
		return this.useBasicLmtSubSerno;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType;
	}
	
    /**
     * @return basicAssetType
     */
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param basicAssetBalanceAmt
	 */
	public void setBasicAssetBalanceAmt(java.math.BigDecimal basicAssetBalanceAmt) {
		this.basicAssetBalanceAmt = basicAssetBalanceAmt;
	}
	
    /**
     * @return basicAssetBalanceAmt
     */
	public java.math.BigDecimal getBasicAssetBalanceAmt() {
		return this.basicAssetBalanceAmt;
	}
	
	/**
	 * @param basicAssetName
	 */
	public void setBasicAssetName(String basicAssetName) {
		this.basicAssetName = basicAssetName;
	}
	
    /**
     * @return basicAssetName
     */
	public String getBasicAssetName() {
		return this.basicAssetName;
	}
	
	/**
	 * @param basicAssetEndDate
	 */
	public void setBasicAssetEndDate(String basicAssetEndDate) {
		this.basicAssetEndDate = basicAssetEndDate;
	}
	
    /**
     * @return basicAssetEndDate
     */
	public String getBasicAssetEndDate() {
		return this.basicAssetEndDate;
	}
	
	/**
	 * @param basicAssetBalanceTerm
	 */
	public void setBasicAssetBalanceTerm(Integer basicAssetBalanceTerm) {
		this.basicAssetBalanceTerm = basicAssetBalanceTerm;
	}
	
    /**
     * @return basicAssetBalanceTerm
     */
	public Integer getBasicAssetBalanceTerm() {
		return this.basicAssetBalanceTerm;
	}
	
	/**
	 * @param origiBasicAccNo
	 */
	public void setOrigiBasicAccNo(String origiBasicAccNo) {
		this.origiBasicAccNo = origiBasicAccNo;
	}
	
    /**
     * @return origiBasicAccNo
     */
	public String getOrigiBasicAccNo() {
		return this.origiBasicAccNo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}