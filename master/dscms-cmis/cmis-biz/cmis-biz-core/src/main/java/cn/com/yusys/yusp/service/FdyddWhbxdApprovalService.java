/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.FdyddWhbxdApproval;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req.Fb1146ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp.Fb1146RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.FdyddWhbxdApprovalMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FdyddWhbxdApprovalService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-06 15:47:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FdyddWhbxdApprovalService {

    @Autowired
    private FdyddWhbxdApprovalMapper fdyddWhbxdApprovalMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(FdyddWhbxdApprovalService.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public FdyddWhbxdApproval selectByPrimaryKey(String serno) {
        return fdyddWhbxdApprovalMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<FdyddWhbxdApproval> selectAll(QueryModel model) {
        List<FdyddWhbxdApproval> records = (List<FdyddWhbxdApproval>) fdyddWhbxdApprovalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<FdyddWhbxdApproval> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FdyddWhbxdApproval> list = fdyddWhbxdApprovalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: fdyddWhbxdApprovallist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（获取申请信息押品查询查封）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<FdyddWhbxdApproval> fdyddWhbxdApprovallist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return fdyddWhbxdApprovalMapper.selectByModel(model);
    }
    /**

     * @方法名称: fdyddWhbxdApprovalHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（获取申请信息押品查询查封）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<FdyddWhbxdApproval> fdyddWhbxdApprovalHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return fdyddWhbxdApprovalMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public FdyddWhbxdApproval selectBySerno(String serno) {
        return fdyddWhbxdApprovalMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(FdyddWhbxdApproval record) {
        return fdyddWhbxdApprovalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(FdyddWhbxdApproval record) {
        return fdyddWhbxdApprovalMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(FdyddWhbxdApproval record) {
        return fdyddWhbxdApprovalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(FdyddWhbxdApproval record) {
        return fdyddWhbxdApprovalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return fdyddWhbxdApprovalMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return fdyddWhbxdApprovalMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: managerVetoSubmit
     * @方法描述: 房抵e点贷无还本续贷客户经理否决
     * @参数与返回说明: 交易码fb1146
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public ResultDto<String> managerVetoSubmit(FdyddWhbxdApproval record) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (record != null) {
            log.info("房抵e点贷客户经理否决调用受托信息审核开始,流水号【{}】", record.getSerno());
            Fb1146ReqDto fb1146ReqDto = new Fb1146ReqDto();
            fb1146ReqDto.setApp_no(record.getSerno()); //申请流水号
            fb1146ReqDto.setVrify_task (record.getApproveResult()); //受托审核状态  0审核未通过1审核通过
            // 是否调整
            if("1".equals(record.getAdjustOrNot())){
                fb1146ReqDto.setExec_rate(record.getAdjustInterestRate());//调整后利率
            }
            log.info("房抵e点贷客户经理否决调用受托信息审核请求：" + fb1146ReqDto);
            ResultDto<Fb1146RespDto> fb1146RespResultDto = dscms2CircpClientService.fb1146(fb1146ReqDto);
            log.info("房抵e点贷客户经理否决调用受托信息审核返回：" + fb1146RespResultDto);
            String fb1146Code = Optional.ofNullable(fb1146RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String fb1146Meesage = Optional.ofNullable(fb1146RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("房抵e点贷客户经理否决调用受托信息审核返回信息：" + fb1146Meesage);
            if (Objects.equals(fb1146Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 更新审批状态为否决
                record.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                this.updateSelective(record);
            } else {
                resultDto.setCode(fb1146Code);
                resultDto.setMessage(fb1146Meesage);
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        return resultDto;
    }
}
