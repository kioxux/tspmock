/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayPlan;
import cn.com.yusys.yusp.service.IqpRepayPlanService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayPlanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-23 14:36:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepayplan")
public class IqpRepayPlanResource {
    @Autowired
    private IqpRepayPlanService iqpRepayPlanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRepayPlan>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRepayPlan> list = iqpRepayPlanService.selectAll(queryModel);
        return new ResultDto<List<IqpRepayPlan>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRepayPlan>> index(QueryModel queryModel) {
        List<IqpRepayPlan> list = iqpRepayPlanService.selectByModel(queryModel);
        return new ResultDto<List<IqpRepayPlan>>(list);
    }
    /***
     * @param iqpRepayPlan
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpRepayPlan>>
     * @author hubp
     * @date 2021/4/24 14:07
     * @version 1.0.0
     * @desc    根据合同号查询还款信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectByContNo")
    protected ResultDto<List<IqpRepayPlan>> selectByContNo(@RequestBody IqpRepayPlan iqpRepayPlan) {
        List<IqpRepayPlan> list = iqpRepayPlanService.selectByContNo(iqpRepayPlan.getContNo());
        return new ResultDto<List<IqpRepayPlan>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpRepayPlan> show(@PathVariable("pkId") String pkId) {
        IqpRepayPlan iqpRepayPlan = iqpRepayPlanService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpRepayPlan>(iqpRepayPlan);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpRepayPlan> create(@RequestBody IqpRepayPlan iqpRepayPlan) throws URISyntaxException {
        iqpRepayPlanService.insert(iqpRepayPlan);
        return new ResultDto<IqpRepayPlan>(iqpRepayPlan);
    }
    /***
     * @param iqpRepayPlan
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/4/24 14:09
     * @version 1.0.0
     * @desc    根据信息判断是插入还是更新还款信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryiqprepayplan")
    protected ResultDto<Integer> queryIqpRepayPlan(@RequestBody IqpRepayPlan iqpRepayPlan) throws URISyntaxException {
        int result = iqpRepayPlanService.queryIqpRepayPlan(iqpRepayPlan);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRepayPlan iqpRepayPlan) throws URISyntaxException {
        int result = iqpRepayPlanService.update(iqpRepayPlan);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpRepayPlanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRepayPlanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
