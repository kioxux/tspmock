/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AreaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.repository.mapper.AreaAdminUserMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaAdminUserService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-27 15:34:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AreaAdminUserService {

    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AreaAdminUser selectByPrimaryKey(String userNo) {
        return areaAdminUserMapper.selectByPrimaryKey(userNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AreaAdminUser> selectAll(QueryModel model) {
        List<AreaAdminUser> records = (List<AreaAdminUser>) areaAdminUserMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AreaAdminUser> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AreaAdminUser> list = areaAdminUserMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AreaAdminUser record) {
        return areaAdminUserMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AreaAdminUser record) {
        return areaAdminUserMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AreaAdminUser record) {
        return areaAdminUserMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AreaAdminUser record) {
        return areaAdminUserMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String userNo) {
        return areaAdminUserMapper.deleteByPrimaryKey(userNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return areaAdminUserMapper.deleteByIds(ids);
    }
    /**
     * @author zlf
     * @date 2021/5/27 15:39
     * @version 1.0.0
     * @desc  从表维护
     * @修改历史  修改时间 修改人员 修改原因
     */
    public int save(AreaAdminUser record) {
        int result=0;
        AreaAdminUser data= new AreaAdminUser();
        if(StringUtils.isEmpty(record.getUserNo())){
            result=0;
        }else{
            data=areaAdminUserMapper.selectByPrimaryKey(record.getUserNo());
            if(data!=null){
                if(!"".equals(data.getUserNo())){
                    record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    result=areaAdminUserMapper.updateByPrimaryKey(record);
                }
            }else{
                record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                result=areaAdminUserMapper.insert(record);
            }
        }
        return result;
    }

    public int deleteOne(String userNo) {
        int result =0;
        AreaAdminUser areaAdminUser=null;
        if(!"".equals(userNo)){
            areaAdminUser=areaAdminUserMapper.selectByPrimaryKey(userNo);
        }
        if(areaAdminUser == null){
            //无从表信息
            result=2;
        }else {
            result=areaAdminUserMapper.deleteByPrimaryKey(userNo);
        }
        return result;
    }

    /**
     * @param orgCode
     * @return cn.com.yusys.yusp.domain.AreaAdminUser
     * @author 王玉坤
     * @date 2021/9/8 23:53
     * @version 1.0.0
     * @desc 根据机构号查询用户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<AreaAdminUser> selectByOrgCodes(List<String> orgCodes) {
        return areaAdminUserMapper.selectByOrgCodes(org.apache.commons.lang.StringUtils.join(orgCodes.toArray(), ","));
    }


}
