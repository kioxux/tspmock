/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpAppDiscContDetails
 * @类描述: iqp_app_disc_cont_details数据实体类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-07 09:36:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_app_disc_cont_details")
public class IqpAppDiscContDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 贴现协议类型   STD_ZB_DISC_CONT_TYPE **/
	@Column(name = "DISC_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String discContType;
	
	/** 票据种类   STD_ZB_DRFT_TYPE **/
	@Column(name = "DRFT_TYPE", unique = false, nullable = true, length = 5)
	private String drftType;
	
	/** 是否电子票据  STD_ZB_YES_NO **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;
	
	/** 申请人账号 **/
	@Column(name = "RQSTR_ACC_NO", unique = false, nullable = true, length = 40)
	private String rqstrAccNo;
	
	/** 申请人账户名称 **/
	@Column(name = "RQSTR_ACC_NAME", unique = false, nullable = true, length = 40)
	private String rqstrAccName;
	
	/** 买入类型  STD_ZB_PUR_TYPE **/
	@Column(name = "PUR_TYPE", unique = false, nullable = true, length = 5)
	private String purType;
	
	/** 贴现币种  STD_ZX_CUR_TYPE **/
	@Column(name = "DISC_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String discCurType;
	
	/** 票面总金额 **/
	@Column(name = "DRFT_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftTotalAmt;
	
	/** 是否先贴后查  STD_ZB_YES_NO **/
	@Column(name = "IS_ATCF", unique = false, nullable = true, length = 5)
	private String isAtcf;
	
	/** 付息方式  STD_ZB_PINT_MODE **/
	@Column(name = "PINT_MODE", unique = false, nullable = true, length = 5)
	private String pintMode;
	
	/** 申请人授信额度编号 **/
	@Column(name = "RQSTR_LMT_LIMIT_NO", unique = false, nullable = true, length = 40)
	private String rqstrLmtLimitNo;
	
	/** 申请人批复编号 **/
	@Column(name = "RQSTR_REPLY_NO", unique = false, nullable = true, length = 40)
	private String rqstrReplyNo;
	
	/** 承兑企业授信额度编号 **/
	@Column(name = "ACPT_CRP_LMT_NO", unique = false, nullable = true, length = 40)
	private String acptCrpLmtNo;
	
	/** 承兑企业批复编号 **/
	@Column(name = "ACPT_CRP_REPLY_NO", unique = false, nullable = true, length = 40)
	private String acptCrpReplyNo;
	
	/** 承兑企业客户编号 **/
	@Column(name = "ACPT_CRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String acptCrpCusId;
	
	/** 承兑企业客户名称 **/
	@Column(name = "ACPT_CRP_CUS_NAME", unique = false, nullable = true, length = 40)
	private String acptCrpCusName;
	
	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 40)
	private String debtLevel;
	
	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 40)
	private String ead;
	
	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 40)
	private String lgd;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private Date updateTime;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param discContType
	 */
	public void setDiscContType(String discContType) {
		this.discContType = discContType;
	}
	
    /**
     * @return discContType
     */
	public String getDiscContType() {
		return this.discContType;
	}
	
	/**
	 * @param drftType
	 */
	public void setDrftType(String drftType) {
		this.drftType = drftType;
	}
	
    /**
     * @return drftType
     */
	public String getDrftType() {
		return this.drftType;
	}
	
	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}
	
    /**
     * @return isEDrft
     */
	public String getIsEDrft() {
		return this.isEDrft;
	}
	
	/**
	 * @param rqstrAccNo
	 */
	public void setRqstrAccNo(String rqstrAccNo) {
		this.rqstrAccNo = rqstrAccNo;
	}
	
    /**
     * @return rqstrAccNo
     */
	public String getRqstrAccNo() {
		return this.rqstrAccNo;
	}
	
	/**
	 * @param rqstrAccName
	 */
	public void setRqstrAccName(String rqstrAccName) {
		this.rqstrAccName = rqstrAccName;
	}
	
    /**
     * @return rqstrAccName
     */
	public String getRqstrAccName() {
		return this.rqstrAccName;
	}
	
	/**
	 * @param purType
	 */
	public void setPurType(String purType) {
		this.purType = purType;
	}
	
    /**
     * @return purType
     */
	public String getPurType() {
		return this.purType;
	}
	
	/**
	 * @param discCurType
	 */
	public void setDiscCurType(String discCurType) {
		this.discCurType = discCurType;
	}
	
    /**
     * @return discCurType
     */
	public String getDiscCurType() {
		return this.discCurType;
	}
	
	/**
	 * @param drftTotalAmt
	 */
	public void setDrftTotalAmt(java.math.BigDecimal drftTotalAmt) {
		this.drftTotalAmt = drftTotalAmt;
	}
	
    /**
     * @return drftTotalAmt
     */
	public java.math.BigDecimal getDrftTotalAmt() {
		return this.drftTotalAmt;
	}
	
	/**
	 * @param isAtcf
	 */
	public void setIsAtcf(String isAtcf) {
		this.isAtcf = isAtcf;
	}
	
    /**
     * @return isAtcf
     */
	public String getIsAtcf() {
		return this.isAtcf;
	}
	
	/**
	 * @param pintMode
	 */
	public void setPintMode(String pintMode) {
		this.pintMode = pintMode;
	}
	
    /**
     * @return pintMode
     */
	public String getPintMode() {
		return this.pintMode;
	}
	
	/**
	 * @param rqstrLmtLimitNo
	 */
	public void setRqstrLmtLimitNo(String rqstrLmtLimitNo) {
		this.rqstrLmtLimitNo = rqstrLmtLimitNo;
	}
	
    /**
     * @return rqstrLmtLimitNo
     */
	public String getRqstrLmtLimitNo() {
		return this.rqstrLmtLimitNo;
	}
	
	/**
	 * @param rqstrReplyNo
	 */
	public void setRqstrReplyNo(String rqstrReplyNo) {
		this.rqstrReplyNo = rqstrReplyNo;
	}
	
    /**
     * @return rqstrReplyNo
     */
	public String getRqstrReplyNo() {
		return this.rqstrReplyNo;
	}
	
	/**
	 * @param acptCrpLmtNo
	 */
	public void setAcptCrpLmtNo(String acptCrpLmtNo) {
		this.acptCrpLmtNo = acptCrpLmtNo;
	}
	
    /**
     * @return acptCrpLmtNo
     */
	public String getAcptCrpLmtNo() {
		return this.acptCrpLmtNo;
	}
	
	/**
	 * @param acptCrpReplyNo
	 */
	public void setAcptCrpReplyNo(String acptCrpReplyNo) {
		this.acptCrpReplyNo = acptCrpReplyNo;
	}
	
    /**
     * @return acptCrpReplyNo
     */
	public String getAcptCrpReplyNo() {
		return this.acptCrpReplyNo;
	}
	
	/**
	 * @param acptCrpCusId
	 */
	public void setAcptCrpCusId(String acptCrpCusId) {
		this.acptCrpCusId = acptCrpCusId;
	}
	
    /**
     * @return acptCrpCusId
     */
	public String getAcptCrpCusId() {
		return this.acptCrpCusId;
	}
	
	/**
	 * @param acptCrpCusName
	 */
	public void setAcptCrpCusName(String acptCrpCusName) {
		this.acptCrpCusName = acptCrpCusName;
	}
	
    /**
     * @return acptCrpCusName
     */
	public String getAcptCrpCusName() {
		return this.acptCrpCusName;
	}
	
	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}
	
    /**
     * @return debtLevel
     */
	public String getDebtLevel() {
		return this.debtLevel;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(String ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public String getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(String lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public String getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}