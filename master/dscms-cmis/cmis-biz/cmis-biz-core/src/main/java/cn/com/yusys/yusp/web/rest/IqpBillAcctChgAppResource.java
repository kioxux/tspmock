/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpBillAcctChgApp;
import cn.com.yusys.yusp.service.IqpBillAcctChgAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillAcctChgAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-19 21:31:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpbillacctchgapp")
public class IqpBillAcctChgAppResource {
    @Autowired
    private IqpBillAcctChgAppService iqpBillAcctChgAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<IqpBillAcctChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpBillAcctChgApp> list = iqpBillAcctChgAppService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpBillAcctChgApp>> index(@RequestBody QueryModel queryModel) {
        List<IqpBillAcctChgApp> list = iqpBillAcctChgAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{iqpSerno}")
    protected ResultDto<IqpBillAcctChgApp> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpBillAcctChgApp iqpBillAcctChgApp = iqpBillAcctChgAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<>(iqpBillAcctChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpBillAcctChgApp> create(@RequestBody IqpBillAcctChgApp iqpBillAcctChgApp) throws URISyntaxException {
        iqpBillAcctChgAppService.insertSelective(iqpBillAcctChgApp);
        return new ResultDto<>(iqpBillAcctChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpBillAcctChgApp iqpBillAcctChgApp) throws URISyntaxException {
        int result = iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpBillAcctChgAppService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpBillAcctChgAppService.deleteByIds(ids);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称: updateApproveStatus
     * @函数描述: 还款账户变更为本人本行其他账号,不需要审批
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/updateApproveStatus")
    protected ResultDto<Integer> updateApproveStatus(@RequestBody String iqpSerno) {
        int result = iqpBillAcctChgAppService.updateApproveStatus(iqpSerno);
        return new ResultDto<>(result);
    }

}
