package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.resp.GrtGuarContBySernoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContRelMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-17 13:51:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class GrtGuarContService {

    private static final Logger log = LoggerFactory.getLogger(GrtGuarContService.class);

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;

    //担保合同和业务合同（主借款合同）的关系表
    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private GrtGuarContRelMapper grtGuarContRelMapper;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private CtrDiscContService ctrDiscContService;

    @Autowired
    private CtrCvrgContService ctrCvrgContService;

    @Autowired
    private CtrTfLocContService ctrTfLocContService;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;//开证合同申请详情

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;//委托贷款

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GrtGuarCont selectByPrimaryKey(String guarPkId) {
        return grtGuarContMapper.selectByPrimaryKey(guarPkId);
    }


    /**
     * @方法名称: selectByGuarContNo
     * @方法描述: 根据担保合同号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GrtGuarCont selectByGuarContNo(String guarContNo) {
        return grtGuarContMapper.selectContNo(guarContNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<GrtGuarCont> selectAll(QueryModel model) {
        List<GrtGuarCont> records = grtGuarContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GrtGuarCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarCont> list = grtGuarContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectGuarContByContNo
     * @方法描述: 根据合同号查询担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GrtGuarCont> selectGuarContByContNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarCont> list = grtGuarContMapper.selectGuarContByContNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GrtGuarCont record) {
        return grtGuarContMapper.insert(record);
    }

    /**
     * @方法名称: 小微用信新增方法
     * @方法描述: 插入担保合同和业务关系表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertGrt(GrtGuarCont grtGuarCont) throws ParseException {
        GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();

        grtGuarBizRstRel.setPkId(grtGuarCont.getGuarPkId());    //主键
        grtGuarBizRstRel.setContNo(grtGuarCont.getGuarContNo());   //合同编号
        grtGuarBizRstRel.setGuarContNo(grtGuarCont.getGuarContNo());  //担保合同编号
        grtGuarBizRstRel.setGuarAmt(grtGuarCont.getGuarAmt());  //本次担保金额
        grtGuarBizRstRel.setInputId(grtGuarCont.getInputId());  //登记人
        grtGuarBizRstRel.setInputBrId(grtGuarCont.getInputBrId());  //登记机构

        //转换日期
        grtGuarBizRstRel.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));  //登记日期
        grtGuarBizRstRel.setUpdId(grtGuarCont.getUpdId());       //最后修改人
        grtGuarBizRstRel.setUpdBrId(grtGuarCont.getUpdBrId());    //最后修改机构
        grtGuarBizRstRel.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));  //最后修改日期
        grtGuarBizRstRel.setOprType(grtGuarCont.getOprType());   //操作类型
        grtGuarBizRstRel.setManagerId(grtGuarCont.getManagerId());   //责任人
        grtGuarBizRstRel.setManagerBrId(grtGuarCont.getManagerBrId());   //责任机构
        grtGuarBizRstRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarBizRstRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarCont.setBizLine("01");

        grtGuarBizRstRelMapper.insert(grtGuarBizRstRel);
        return grtGuarContMapper.insert(grtGuarCont);
    }

    /**
     * @方法名称: 零售用信新增方法
     * @方法描述: 插入担保合同和业务关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertRetall(GrtGuarContBySernoDto grtGuarContBySernoDto) throws ParseException {

        GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
        GrtGuarCont grtGuarCont = new GrtGuarCont();
        grtGuarCont = grtGuarContBySernoDto.getGrtGuarCont();

        HashMap<String, String> param = new HashMap<>();
        param.put("bizType", "ls");
        String guarContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, param);

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", grtGuarContBySernoDto.getSerno());
        List<GuarBizRelGuaranteeDto> list = guarGuaranteeMapper.selectByIqpSernoModel(queryModel); //获取保证人列表里的担保金额

        //保证人列表里的所以担保金额相加
        BigDecimal sum = new BigDecimal(0);
        for (GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : list) {
            sum = sum.add(new BigDecimal(guarBizRelGuaranteeDto.getGuarAmt()));
        }

        // BigDecimal bigDecimal = new BigDecimal(grtGuarCont.getGuarAmt());
        grtGuarBizRstRel.setGuarAmt(grtGuarCont.getGuarAmt());  //本次担保金额
        if (grtGuarCont.getGuarAmt().compareTo(sum) > 0) {
            throw BizException.error(null, "9999", "不能大于所有保证人提供的担保金额或押品评估之和");
        }

        grtGuarBizRstRel.setPkId(UUID.randomUUID().toString());    //主键
        grtGuarBizRstRel.setSerno(grtGuarContBySernoDto.getSerno());   //业务流水号
        grtGuarBizRstRel.setContNo(grtGuarContBySernoDto.getContNo());  //合同编号
        grtGuarBizRstRel.setGuarContNo(guarContNo);  //担保合同编号

        grtGuarBizRstRel.setInputId(grtGuarCont.getInputId());  //登记人
        grtGuarBizRstRel.setInputBrId(grtGuarCont.getInputBrId());  //登记机构

        //转换日期 ASSURE_CERT_CODE
        grtGuarBizRstRel.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));  //登记日期
        grtGuarBizRstRel.setUpdId(grtGuarCont.getUpdId());       //最后修改人
        grtGuarBizRstRel.setUpdBrId(grtGuarCont.getUpdBrId());    //最后修改机构
        grtGuarBizRstRel.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));  //最后修改日期
        grtGuarBizRstRel.setOprType(grtGuarCont.getOprType());   //操作类型
        grtGuarBizRstRel.setManagerId(grtGuarCont.getManagerId());   //责任人
        grtGuarBizRstRel.setManagerBrId(grtGuarCont.getManagerBrId());   //责任机构
        grtGuarBizRstRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarBizRstRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarCont.setBizLine("02");                         //业务条线
        grtGuarCont.setGuarContType("A");                    //担保合同类型
        grtGuarCont.setGuarContState("100");                  //担保合同状态

        //将关联数据插入到关系表里
        grtGuarBizRstRelMapper.insert(grtGuarBizRstRel);
        //插入到担保合同表里
        //grtGuarCont.setGuarContNo(guarContNo);
        int result = grtGuarContMapper.insert(grtGuarCont);
        return result;
    }


    /**
     * @方法名称: 对公用信管理新增方法
     * @方法描述: 插入担保合同和业务关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertPublic(GrtGuarContBySernoDto grtGuarContBySernoDto) throws ParseException {
        String serno = grtGuarContBySernoDto.getSerno();
        String contNo = grtGuarContBySernoDto.getContNo();

        GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
        GrtGuarCont grtGuarCont = new GrtGuarCont();
        grtGuarCont = grtGuarContBySernoDto.getGrtGuarCont();

        HashMap<String, String> param = new HashMap<>();
        String guarContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, param);

        grtGuarBizRstRel.setPkId(UUID.randomUUID().toString());    //主键
        grtGuarBizRstRel.setSerno(serno);   //业务流水号
        grtGuarBizRstRel.setContNo(contNo);  //合同编号
        grtGuarBizRstRel.setGuarContNo(guarContNo);  //担保合同编号
        grtGuarBizRstRel.setGuarPkId(grtGuarCont.getGuarPkId()); //担保合同流水号

        grtGuarBizRstRel.setInputId(grtGuarCont.getInputId());  //登记人
        grtGuarBizRstRel.setInputBrId(grtGuarCont.getInputBrId());  //登记机构

        //转换日期 ASSURE_CERT_CODE
        grtGuarBizRstRel.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));  //登记日期
        grtGuarBizRstRel.setUpdId(grtGuarCont.getUpdId());       //最后修改人
        grtGuarBizRstRel.setUpdBrId(grtGuarCont.getUpdBrId());    //最后修改机构
        grtGuarBizRstRel.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));  //最后修改日期
        grtGuarBizRstRel.setOprType(grtGuarCont.getOprType());   //操作类型
        grtGuarBizRstRel.setManagerId(grtGuarCont.getManagerId());   //责任人
        grtGuarBizRstRel.setManagerBrId(grtGuarCont.getManagerBrId());   //责任机构
        grtGuarBizRstRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarBizRstRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarCont.setBizLine("03");                         //业务条线

        //将关联数据插入到关系表里
        grtGuarBizRstRelMapper.insert(grtGuarBizRstRel);
        //插入到担保合同表里
        int result = grtGuarContMapper.insert(grtGuarCont);
        return result;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GrtGuarCont record) {
        return grtGuarContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GrtGuarCont record) {
        return grtGuarContMapper.updateByPrimaryKey(record);
    }
    /**
     * @方法名称: update1
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update1(GrtGuarCont record) {
        return grtGuarContMapper.updateByGuarContNo(record);
    }
    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(GrtGuarCont record) {
        return grtGuarContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelectiveByParams
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelectiveByParams(Map params) {
        int result = 0;
        String guarPkId = (String) params.get("guarPkId");
        String myop = (String) params.get("myop");
        try {
            log.info("担保合同申请信息【{}】", JSONObject.toJSON(params));
            if (StringUtils.isBlank(guarPkId)) {
                throw BizException.error(null, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.key, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.value);
            }
            int num = grtGuarContMapper.selectByPrimaryKeyNum(guarPkId);
            if (num < 1) {
                throw BizException.error(null, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.key, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.value);
            }
            GrtGuarCont grtGuarCont = JSONObject.parseObject(JSON.toJSONString(params), GrtGuarCont.class);

            if (CmisBizConstants.OPT_TYPE_SIGN.equals(myop)) {//担保签订
                grtGuarCont.setGuarContState(CmisBizConstants.GUAR_CONT_STATE_01);
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                result = grtGuarContMapper.updateByPrimaryKeySelective(grtGuarCont);
                if (result < 0) {
                    throw BizException.error(null, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.key, EcbEnum.QUERT_GUARCONTNO_EXCEPTION.value);
                }
            } else if (CmisBizConstants.OPT_TYPE_ONLOGOUT.equals(myop)) {//担保注销
                grtGuarCont.setGuarContState(CmisBizConstants.GUAR_CONT_STATE_02);
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                result = grtGuarContMapper.updateByPrimaryKeySelective(grtGuarCont);
                if (result < 0) {
                    throw BizException.error(null, EcbEnum.GRT_GUAR_CONT_NO_ONLOGOUT.key, EcbEnum.GRT_GUAR_CONT_NO_ONLOGOUT.value);
                }
                //担保合同类型
                String guarContType = grtGuarCont.getGuarContType();
                //担保合同编号
                String guarContNo = grtGuarCont.getGuarContNo();

                if (CmisBizConstants.STD_ZB_GUAR_CONT_TYPE_B.equals(guarContType)){
                    //最高额担保合同注销时，把关联的押品的记录的状态改为失效
                    grtGuarContRelMapper.updateStatusInvalidByGuarContNo(guarContNo);
                    log.info("最高额担保合同【"+guarContNo+"】注销时，把关联的押品的记录的状态改为失效");
                }

                resumeAorgLmtWhite(guarContNo);
            } else if (CmisBizConstants.OPT_TYPE_ONPRINT.equals(myop)) {//担保打印
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                result = grtGuarContMapper.updateByPrimaryKeySelective(grtGuarCont);
            } else {
                log.info("校验担保合同申请信息【{}】", JSONObject.toJSON(grtGuarCont));
                Map recode = new HashMap();
                recode.put("guarContNo", grtGuarCont.getGuarContNo());
                recode.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                recode.put("correRel", CmisBizConstants.CORRE_REL_3);
                recode.put("guarContState", CmisBizConstants.GUAR_CONT_STATE_02);
                int rtnData = iqpGuarBizRelAppService.selectByGuarContNoKey(recode);
                if (rtnData > 0) {
                    throw BizException.error(null, EcbEnum.GRT_CONT_IQPGUARBIZ_EXCEPTION.key, EcbEnum.GRT_CONT_IQPGUARBIZ_EXCEPTION.value);
                }
                grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                result = grtGuarContMapper.updateByGuarContNoKey(grtGuarCont);
                if (result < 0) {
                    throw BizException.error(null, EcbEnum.GRT_GUAR_CONT_NO_DELETE.key, EcbEnum.GRT_GUAR_CONT_NO_DELETE.value);
                }
            }
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String guarPkId) {
        return grtGuarContMapper.deleteByPrimaryKey(guarPkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return grtGuarContMapper.deleteByIds(ids);
    }

    /**
     * @函数名称: getGrtGuarCont
     * @函数描述: 获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GrtGuarContClientDto> getGrtGuarCont(GrtGuarContClientDto grtGuarContDto) {
        return grtGuarContMapper.getGrtGuarCont(grtGuarContDto);
    }

    /**
     * 校验担保合同金额与押品的担保合同金额
     * 1、担保金额变化时，需要担保金额入参
     * 2、担保金额变化【新增担保品/保证人】时
     *
     * @param params
     * @return
     */
    public Map checkGuarAmtAndAppAmt(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String guarContNo = (String) params.get("guarContNo");
            log.info("获取担保申请" + guarContNo + "担保金额信息");
            BigDecimal guarAmt = new BigDecimal(0);
            String appAmtString = params.get("guarAmt").toString();
            if (StringUtils.isBlank(appAmtString)) {
                log.info("入参的担保金额为空，查询担保合同申请" + guarContNo + "主表信息获取担保金额");
                GrtGuarCont grtGuarCont = grtGuarContMapper.selectByPrimaryKey(guarContNo);
                if (grtGuarCont == null) {
                    throw new YuspException(EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.key, EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.value);
                }
                guarAmt = grtGuarCont.getGuarAmt();
                if (guarAmt == null || guarAmt.compareTo(new BigDecimal(0)) <= 0) {
                    rtnMsg = "担保金额为空";
                    return rtnData;
                }
            } else {
                //因为前端入参的金额，存在，分隔符，因此在转化数值前需要将，替换
                guarAmt = new BigDecimal(appAmtString.replace(CmisCommonConstants.COMMON_SPLIT_COMMA, ""));
            }
            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            String guarContNos = (String) params.get("guarContNo");
            Map guarTotalAmtMap = grtGuarContRelService.getGuarTotalAmt(guarContNos, "");
            BigDecimal guarTotalAmt = new BigDecimal(0);
            if (CollectionUtils.isEmpty(guarTotalAmtMap)) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.key, EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.value);
            }
            String amtRtnCode = (String) guarTotalAmtMap.get("rtnCode");
            String amtRtnMsg = (String) guarTotalAmtMap.get("rtnMsg");
            if (!EcbEnum.IQP_SUCCESS_DEF.key.equals(amtRtnCode)) {
                throw new YuspException(amtRtnCode, amtRtnMsg);
            }
            guarTotalAmt = (BigDecimal) guarTotalAmtMap.get("guarTotalAmt");
            log.info("获取业务申请" + guarContNo + "下担保总金额为：" + guarTotalAmt);
            if (guarAmt.compareTo(guarTotalAmt) > 0) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_GUARAMTSMALL_EXCEPTION.key, EcbEnum.APPAMT_CHECK_GUARAMTSMALL_EXCEPTION.value);
            }
        } catch (YuspException e) {
            log.error("校验担保金额与申请金额异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 业务申请时 新增/修改  一般担保合同数据保存操作
     * 0、校验数据  校验押品的评估价值/保证人的保证
     * 1、新增/修改 保存担保合同数据
     * 2、新增 业务与担保合同关系数据
     *
     * @param params
     * @return
     */
    public Map saveIqpGrtGuarCont(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.GRTGUAR_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.GRTGUAR_SUCCESS_DEF.value;
        try {
            String guarContNo = (String) params.get("guarContNo");
            String guarPkId = (String) params.get("guarPkId");
            String guarAmtString = (String) params.get("guarAmt");
            String iqpSerno = (String) params.get("iqpSerno");
            if (StringUtils.isBlank(guarContNo) || StringUtils.isBlank(guarPkId) || StringUtils.isBlank(guarAmtString)
                    || StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.GRT_IQP_CONT_SAVE_PARAMS_EXCEPTION.key, EcbEnum.GRT_IQP_CONT_SAVE_PARAMS_EXCEPTION.value);
            }

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "获取担保金额：" + guarAmtString);
            BigDecimal guarAmt = new BigDecimal(guarAmtString.replace(",", ""));

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "获取押品/保证总金额");
            String guarContNos = (String) params.get("guarContNo");
            // 通过合同编号获取所有押品价值数据() TODO 可能存在问题
            Map guarTotalAmtMap = grtGuarContRelService.getGuarTotalAmt(guarContNos, "");

           /* if (CollectionUtils.isEmpty(guarTotalAmtMap)) {
                throw new YuspException(EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.key, EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.value);
            }*/
            if (null == guarTotalAmtMap) {
                throw new YuspException(EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.key, EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.value);
            }
            String amtRtnCode = (String) guarTotalAmtMap.get("rtnCode");
            String amtRtnMsg = (String) guarTotalAmtMap.get("rtnMSg");
            if (StringUtils.isBlank(amtRtnCode)) {
                throw new YuspException(EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.key, EcbEnum.GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION.value);
            }
            if (!EcbEnum.GRTGUAR_SUCCESS_DEF.key.equals(amtRtnCode)) {
                throw new YuspException(amtRtnCode, amtRtnMsg);
            }
            BigDecimal guarTotalAmt = (BigDecimal) guarTotalAmtMap.get("guarTotalAmt");

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "引入的押品总价值为：" + guarTotalAmt);

            if (guarAmt.compareTo(guarTotalAmt) > 0) {
                throw new YuspException(EcbEnum.GRT_IQP_CONT_SAVE_GUARAMTSMALL_EXCEPTION.key, EcbEnum.GRT_IQP_CONT_SAVE_GUARAMTSMALL_EXCEPTION.value);
            }

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "-处理合同数据");
            //查询数据是否存在
            GrtGuarCont grtGuarCont = grtGuarContMapper.selectByPrimaryKey(guarPkId);
            String opFlag = "";
            //定义数据库操作对象
            GrtGuarCont opGrtGuarCont = new GrtGuarCont();
            if (grtGuarCont == null) {
                opFlag = CmisBizConstants.OP_FLAG_INSERT;
                opGrtGuarCont = JSONObject.parseObject(JSON.toJSONString(params), GrtGuarCont.class);
            } else {
                opFlag = CmisBizConstants.OP_FLAG_UPDATE;
                //获取查询出来的担保合同数据集合
                Map grtGuarContMap = JSON.parseObject(JSON.toJSONString(grtGuarCont, SerializerFeature.WriteMapNullValue), Map.class);
                //循环处理并将前端入参赋值
                Set<String> keySet = grtGuarContMap.keySet();
                for (String key : keySet) {
                    if (params.containsKey(key) && params.get(key) != null) {
                        grtGuarContMap.put(key, params.get(key));
                    }
                }
                //转换为担保合同对象
                opGrtGuarCont = JSONObject.parseObject(JSON.toJSONString(grtGuarContMap), GrtGuarCont.class);
            }

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "-保存合同数据");

            if (StringUtils.isEmpty(opGrtGuarCont.getBizLine())){
                log.error("担保合同【"+opGrtGuarCont.getGuarContNo()+"】的业务条线为空！");
            }

            if (CmisBizConstants.OP_FLAG_INSERT.equals(opFlag)) {
                grtGuarContMapper.insertSelective(opGrtGuarCont);
            } else {
                grtGuarContMapper.updateByPrimaryKeySelective(opGrtGuarCont);
            }

            log.info("业务申请" + iqpSerno + "保存一般担保合同" + guarContNo + "-处理关系表数据");
            //针对担保合同新增的场景，直接新增关系数据
            if (CmisBizConstants.OP_FLAG_INSERT.equals(opFlag)) {
                IqpGuarBizRelApp iqpGuarBizRelApp = JSONObject.parseObject(JSON.toJSONString(params), IqpGuarBizRelApp.class);
                iqpGuarBizRelApp.setPkId(StringUtils.uuid(true));

                iqpGuarBizRelAppService.insertSelective(iqpGuarBizRelApp);
            } else {
                //针对担保合同修改的场景，同样进行修改操作
                iqpGuarBizRelAppService.updateByParams(params);
            }

        } catch (YuspException e) {
            log.error("保存担保合同数据出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存担保合同数据出现异常！", e);
            rtnCode = EcbEnum.GRTGUAR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.GRTGUAR_EXCEPTION_DEF.value + "，" + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 根据入参获取权证信息
     * * @param iqpSerno
     *
     * @return
     **/


    public String querySelectGuarNo(String managerId, String certiState, String optType, String inoutApptype) {
        String rtnData = "";
        log.info("权证信息申请" + "查询担保与押品关系表信息");
        List<GrtGuarCont> grtGuarContList = grtGuarContMapper.selectDataByGuarNo(managerId);
        if (CollectionUtils.isEmpty(grtGuarContList) || grtGuarContList.size() == 0) {
            log.info("权证出入库申请" + managerId + "当前登录用户暂无数据！");
            return rtnData;
        }
        String guarNo = "";
        for (GrtGuarCont grtGuarCont : grtGuarContList) {
            guarNo = guarNo + grtGuarCont.getGuarContNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }
        if (StringUtils.nonBlank(guarNo)) {
            guarNo = guarNo.substring(0, guarNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        } else {
            throw new YuspException(EcbEnum.IQP_CERI_GETGUARNO_EXCEPTION.key, EcbEnum.IQP_CERI_GETGUARNO_EXCEPTION.value);
        }
        Map params = new HashMap();
        params.put("guarNo", guarNo);
        params.put("certiStatu", certiState);
        params.put("optType", optType);
        params.put("inoutApptype", inoutApptype);
        //TODO
        //return iqpCertiInoutRelService.queryIGBRAByGuarNo(params);
        return null;
    }

    /**
     * 通过入参查询担保合同数据
     *
     * @param queryMap
     * @return
     */

    public List<GrtGuarCont> queryByParams(Map queryMap) {
        return grtGuarContMapper.queryByParams(queryMap);
    }


    /**
     * 获取
     *
     * @param cusId
     * @return
     */
    public List<GrtGuarCont> selectByCusId(String cusId) {
        return grtGuarContMapper.selectByCusId(cusId);
    }

    /**
     * @方法名称：queryGuarContByContNo
     * @方法描述：根据授信分项号取得担保合同信息
     * @创建人：zhangming12
     * @创建时间：2021/5/17 19:28
     * @修改记录：修改时间   修改人员   修改原因
     *        2021/10/17  fengjj   连表查询SQL优化
     */
    public List<GrtGuarCont> queryGuarContByContNo(String contNo) {
        List<String> grtGuarPkIds = grtGuarBizRstRelMapper.getGuarPkIdsByContNo(contNo);
        List<GrtGuarCont> result = new ArrayList<>();
        if(CollectionUtils.nonEmpty(grtGuarPkIds)){
            result = grtGuarContMapper.selectEffectByGuarPkIds(grtGuarPkIds);
        }
        return result;
    }
    /**
     * @方法名称：queryLmtSubGuarCont
     * @方法描述：根据授信分项号取得担保合同信息
     * @创建人：zhangming12
     * @创建时间：2021/5/17 19:28
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<GrtGuarCont> queryLmtSubGuarCont(String subSerno) {
        return grtGuarContMapper.queryLmtSubGuarCont(subSerno);
    }

    public int updateSelectiveByGuarContNo(GrtGuarCont grtGuarCont) {
        return grtGuarContMapper.updateSelectiveByGuarContNo(grtGuarCont);
    }


    /**
     * @方法名称：getStockGuarCont
     * @方法描述：获取合同下的担保合同
     * @创建人：zhangming12
     * @创建时间：2021/5/22 10:08
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<GrtGuarCont> getStockGuarCont(QueryModel queryModel) {
        String contNo = (String) queryModel.getCondition().get("contNo");
        if (StringUtils.isBlank(contNo)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.getByContNo(contNo);
        List<GrtGuarCont> grtGuarContList = new ArrayList<>();
        if (grtGuarBizRstRelList.isEmpty()) {
            throw BizException.error(null, EclEnum.ECL070096.key, EclEnum.ECL070096.value);
        }
        for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
            String guarPkId = grtGuarBizRstRel.getGuarPkId();
            GrtGuarCont grtGuarCont = this.selectByPrimaryKey(guarPkId);
            grtGuarContList.add(grtGuarCont);
        }
        return grtGuarContList;
    }

    /**
     * @方法名称：deleteGrtGuarContRelLink
     * @方法描述：删除担保合同和抵押物的关系
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    public int deleteGrtGuarContRelLink(GrtGuarContRelDto grtGuarContRel) {
        int result = grtGuarContMapper.deleteGrtGuarContRelLink(grtGuarContRel);
        return result;
    }

    /**
     * @方法名称：tempSave
     * @方法描述：担保合同暂存/保存
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional
    public int tempSave(GrtGuarContDto grtGuarContDto) {
        if (grtGuarContDto == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String guarContNo = grtGuarContDto.getGuarContNo();
        GrtGuarCont grtGuarCont = grtGuarContMapper.selectContNo(guarContNo);
        GrtGuarCont grtGuarContData = new GrtGuarCont();
        int result = -1;

        String guarWay = grtGuarContDto.getGuarWay();//担保方式 00信用 10抵押 20质押30保证
        //所有保证人/抵质押物评估之和应大于一般担保合同金额
        BigDecimal guarAmt = grtGuarContDto.getGuarAmt();//担保合同金额
        BigDecimal amtSum = BigDecimal.ZERO;//所有保证人/抵质押物评估之和
        //担保合同--保证担保方式
        String assureWay = grtGuarContDto.getAssureWay();

        //更改担保合同和押品关系表状态为 1生效
        List<GrtGuarContRel> byGuarContNoList = grtGuarContRelMapper.getByGuarContNo(guarContNo);
        GuarBaseInfo guarBaseInfoByGuarNo = null;
        GuarGuarantee guarGuarantee = null;
        for (GrtGuarContRel grtGuarContRel : byGuarContNoList) {
            grtGuarContRel.setStatus("1");
            result = grtGuarContRelMapper.updateByPrimaryKey(grtGuarContRel);
            if ("10".equals(guarWay) || "20".equals(guarWay)) {
                //获取押品信息,计算评估之和
                guarBaseInfoByGuarNo = guarBaseInfoService.getGuarBaseInfoByGuarNo(grtGuarContRel.getGuarNo());
                //取权利价值，而不是押品评估价值
                amtSum = amtSum.add(PUBUtilTools.getString(grtGuarContRel.getCertiAmt()));
            } else if ("30".equals(guarWay)) {
                //获取保证人信息,计算担保金额之和
                guarGuarantee = guarGuaranteeService.selectByPrimaryKey(grtGuarContRel.getGuarNo());
                //保证人--保证担保形式
                String guarantyType = guarGuarantee.getGuarantyType();

                if (StringUtils.nonEmpty(assureWay) && !assureWay.equals(guarantyType)){
                    log.info("将保证_ID为【"+guarGuarantee.getGuarantyId()+"】的保证人信息里的保证担保形式由【"+guarantyType+"】改为【"+assureWay+"】");
                    guarGuarantee.setGuarantyType(assureWay);
                    guarGuaranteeMapper.updateByPrimaryKey(guarGuarantee);
                }
                amtSum = amtSum.add(PUBUtilTools.getString(guarGuarantee.getGuarAmt()));
            }
        }

        //所有保证人/抵质押物评估之和应大于一般担保合同金额
        if (!"40".equals(guarWay) && amtSum.compareTo(guarAmt) < 0) {
            log.error("担保合同金额【"+guarAmt+"】,押品总价值【"+amtSum+"】");
            throw BizException.error(null, EcbEnum.GRT_IQP_CONT_SAVE_GUARAMTSMALL_EXCEPTION.key,
                    EcbEnum.GRT_IQP_CONT_SAVE_GUARAMTSMALL_EXCEPTION.value);
        }

        GrtGuarBizRstRel grtGuarBizRstRel = grtGuarContDto.getGrtGuarBizRstRel();
        GrtGuarBizRstRel grtGuarBizRstRelData = new GrtGuarBizRstRel();

        //合同号
        String contNo = grtGuarBizRstRel.getContNo();

        if (StringUtils.isEmpty(grtGuarBizRstRel.getContNo())){
            //针对小微合同申请新增担保合同的场景
            String iqpSerno = grtGuarBizRstRel.getSerno();

            if (!StringUtils.isEmpty(iqpSerno)){
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);

                if (iqpLoanApp!=null && StringUtils.nonEmpty(iqpLoanApp.getIqpSerno())){
                    contNo = iqpLoanApp.getContNo();

                    if (StringUtils.isEmpty(contNo)){
                        throw BizException.error(null,"","流水号为【"+iqpSerno+"】的申请记录里的合同号为空！");
                    }
                }
            }
        }

        BeanUtils.copyProperties(grtGuarContDto, grtGuarContData);
        BeanUtils.copyProperties(grtGuarBizRstRel, grtGuarBizRstRelData);

        grtGuarBizRstRelData.setOprType(CmisBizConstants.OPR_TYPE_01);

        if (!StringUtils.isEmpty(contNo)){
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);

            if (ctrLoanCont!=null && !StringUtils.isEmpty(ctrLoanCont.getBelgLine())){
                grtGuarContData.setBizLine(ctrLoanCont.getBelgLine());
                log.info("担保合同的业务条线由【"+grtGuarContData.getBizLine()+"】改为【"+ctrLoanCont.getBelgLine()+"】");
            }
        }

        if (StringUtils.isEmpty(grtGuarContData.getBizLine())){
            log.error("担保合同【"+grtGuarContData.getGuarContNo()+"】的业务条线为空！");
        }

        if (grtGuarCont == null) {
            grtGuarContData.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            grtGuarContData.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
            grtGuarBizRstRelData.setGuarPkId(grtGuarContData.getGuarPkId());
            grtGuarBizRstRelData.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));

            if (StringUtils.isEmpty(grtGuarBizRstRel.getContNo())) {
                grtGuarBizRstRelData.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                grtGuarBizRstRelData.setContNo(contNo);//合同号
            } else {
                grtGuarBizRstRelData.setInputDate(null);
                grtGuarBizRstRelData.setContNo(grtGuarBizRstRel.getContNo());//合同号
            }

            grtGuarContMapper.insert(grtGuarContData);
            result = grtGuarBizRstRelMapper.insert(grtGuarBizRstRelData);
        } else {
            // grtGuarContData.setInputDate(null);
            grtGuarContData.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
            grtGuarContMapper.updateByPrimaryKeySelective(grtGuarContData);

            if (StringUtils.isEmpty(grtGuarBizRstRelData.getContNo())){
                grtGuarBizRstRelData.setContNo(contNo);
            }
            result = grtGuarBizRstRelMapper.updateByPrimaryKeySelective(grtGuarBizRstRelData);
        }

        return result;
    }

    /**
     * @方法名称: selectContStatuBycontNo
     * @方法描述: 根据合同号查询合同状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public HashMap<String, Object> selectContStatuByContNo(@Param("contNo") String contNo) {
        return grtGuarContMapper.selectContStatuByContNo(contNo);
    }

    /**
     * @方法名称: updateContStatusByContNo
     * @方法描述: 根据合同号更新担保合同签约状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateContStatusByContNo(Map queryMap) {
        return grtGuarContMapper.updateContStatusByContNo(queryMap);
    }

    /**
     * @方法名称：maxContCancelImport
     * @方法描述：最高额担保合同取消引入
     * @创建人：zfq
     * @创建时间：2021/6/8 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    public int maxContCancelImport(GrtGuarBizRstRel grtGuarBizRstRel) {
        if (grtGuarBizRstRel == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        int result = grtGuarContMapper.maxContCancelImport(grtGuarBizRstRel);
        return result;
    }

    /**
     * @方法名称：deleteOnLogic
     * @方法描述：担保合同逻辑删除,同时逻辑删除关系表(担保合同和押品,担保合同和合同)
     * @创建人：zfq
     * @创建时间：2021/6/8 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional
    public int deleteOnLogic(GrtGuarContDto grtGuarContDto) {
        if (grtGuarContDto == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        //担保合同编号
        String guarContNo = grtGuarContDto.getGuarContNo();
        //业务流水号
        String serno = grtGuarContDto.getSerno();
        int result = -1;
        //先删除担保合同和押品的关系表
        List<GrtGuarContRel> byGuarContNoList = grtGuarContRelMapper.getByGuarContNo(guarContNo);
        for (GrtGuarContRel grtGuarContRel : byGuarContNoList) {
            grtGuarContRel.setOprType("02");
            result = grtGuarContRelMapper.updateByPrimaryKey(grtGuarContRel);
        }
        //删除担保合同
        GrtGuarCont grtContInfoByGuarNo = grtGuarContMapper.selectContNo(guarContNo);
        grtContInfoByGuarNo.setOprType("02");
        result = grtGuarContMapper.updateByPrimaryKey(grtContInfoByGuarNo);
        //删除担保合同和合同的关系表
        GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
        grtGuarBizRstRel.setSerno(serno);
        grtGuarBizRstRel.setGuarContNo(guarContNo);
        result = grtGuarContMapper.maxContCancelImport(grtGuarBizRstRel);
        return result;
    }

    /**
     * @方法名称: queryGrtGuarContBycontNo
     * @方法描述: 根据借款合同编号查询担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人： 周茂伟
     */

    public List<GrtGuarContClientDto> queryGrtGuarContByContNo(List<String> contNoList) {
        List<GrtGuarContClientDto> list = grtGuarContMapper.queryGrtGuarContByContNo(contNoList);
        return list;
    }

    /**
     * @方法名称: getGrtGuarContByContNo
     * @方法描述: 根据借款合同编号查询担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：lyj
     */

    public List<GrtGuarContClientDto> getGrtGuarContByContNo(List<String> contNoList) {
        List<GrtGuarContClientDto> list = grtGuarContMapper.getGrtGuarContByContNo(contNoList);
        return list;
    }

    /**
     * @方法名称：selectGuarContByBizRst
     * @方法描述： 根据担保合同和业务的关系的合同编号 查询担保合同信息
     * @创建人：xs
     * @创建时间：2021/06/012 21:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional
    public List<GrtGuarCont> selectGuarContByBizRst(String contNo) {
        return grtGuarContMapper.selectGuarContByBizRst(contNo);
    }

    /**
     * @方法名称: riskItem0033
     * @方法描述: 低风险质押时担保品范围校验
     * @参数与返回说明:
     * @算法描述: （1）担保方式=低分险质押时，押品只能是存单/国债/银票/一级理财/信用证收益权，否则拦截
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0033(QueryModel queryModel) {
        /* 1）担保方式=低分险质押时，押品只能是存单/国债/银票/一级理财/信用证收益权，否则拦截
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = queryModel.getCondition().get("bizId").toString(); // 获取合同编号
        String bizType =  queryModel.getCondition().get("bizType").toString(); // 获取流程编号
        log.info("低风险质押时担保品范围校验开始*******************业务流水号：【{}】",contNo);
        String guarWay = StringUtils.EMPTY;
        String contType = StringUtils.EMPTY;
        BigDecimal contAmt = BigDecimal.ZERO;
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = null;
        String startDate = null;
        String endDate = null;
        if (StringUtils.isBlank(contNo)){
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        /** 小微合同、普通贷款合同、贸易融资、申请是在申请表中 */
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX003,bizType)){
            // 查申请表
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(contNo);
            if(Objects.isNull(iqpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpLoanApp.getStartDate();
            endDate  = iqpLoanApp.getEndDate();
            contType = iqpLoanApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpLoanApp.getContAmt()) ? iqpLoanApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpLoanApp.getGuarWay())) {
                guarWay = iqpLoanApp.getGuarWay();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);

        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006,bizType)){ // 银承贷款合同申请流程
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(contNo);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpAccpApp.getStartDate();
            endDate  = iqpAccpApp.getEndDate();
            contType = iqpAccpApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpAccpApp.getContAmt()) ? iqpAccpApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpAccpApp.getGuarMode())) {
                guarWay = iqpAccpApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005,bizType)){ // 开证贷款合同申请流程
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(contNo);
            if(Objects.isNull(iqpTfLocApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpTfLocApp.getStartDate();
            endDate  = iqpTfLocApp.getEndDate();
            contType = iqpTfLocApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpTfLocApp.getContAmt()) ? iqpTfLocApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpTfLocApp.getGuarMode())) {
                guarWay = iqpTfLocApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007,bizType)){ // 保函贷款合同申请流程
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(contNo);
            if(Objects.isNull(iqpCvrgApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpCvrgApp.getStartDate();
            endDate  = iqpCvrgApp.getEndDate();
            contType = iqpCvrgApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpCvrgApp.getContAmt()) ? iqpCvrgApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpCvrgApp.getGuarMode())) {
                guarWay = iqpCvrgApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX008,bizType)){ // 委托贷款合同申请流程
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(contNo);
            if(Objects.isNull(iqpEntrustLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpEntrustLoanApp.getStartDate();
            endDate  = iqpEntrustLoanApp.getEndDate();
            contType = iqpEntrustLoanApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpEntrustLoanApp.getContAmt()) ? iqpEntrustLoanApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpEntrustLoanApp.getGuarMode())) {
                guarWay = iqpEntrustLoanApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType)){ // 零售合同申请
            // 通过合同编号去查询合同信息，获取担保方式
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            if(ctrLoanCont == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0104);
                return riskResultDto;
            }
            startDate = ctrLoanCont.getContStartDate();
            endDate  = ctrLoanCont.getContEndDate();
            contType = ctrLoanCont.getContType();
            contAmt = contAmt.add(ctrLoanCont.getContAmt());
            if (!StringUtils.isBlank(ctrLoanCont.getGuarWay())) {
                guarWay = ctrLoanCont.getGuarWay();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03302);
                return riskResultDto;
            }
            // 通过申请流水号查询担保合同列表
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(ctrLoanCont.getIqpSerno());
        } else {
            log.info("担保合同与借款合同一致性校验结束*******************业务流水号：【{}】",contNo);
            // 通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        //担保方式=低风险质押时，押品只能是存单/国债/银票/一级理财/信用证收益权，否则拦截
        if (CmisCommonConstants.GUAR_MODE_21.equals(guarWay)) {
            if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03303);
                return riskResultDto;
            }
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                List<GuarBaseInfo> guarBaseList = guarBaseInfoService.queryBaseInfobyGuarContOn(grtGuarBizRstRel.getGuarContNo());
                if (CollectionUtils.nonEmpty(guarBaseList)) {
                    for (GuarBaseInfo guarBaseInfo : guarBaseList) {
                        // 银票(银行承兑汇票纸票 ZY0301003、银行承兑汇票电票 ZY0301005)
                        // 存单(其他存单类 ZY0102999、我行本币存单 ZY0102001
                        // 、我行外币存单 ZY0102002、我行本币同业存单 ZY0102005 、我行外币同业存单 ZY0102006）
                        // 国债(ZY0401001	凭证式储蓄国债（我行承销）
                        //ZY0401002	电子式储蓄国债（我行承销）
                        //ZY0401003	记账式国债（我行承销）
                        //ZY0401004	电子式储蓄国债（非我行承销）
                        //ZY0401005	记账式国债（非我行承销）
                        //ZY0401998	其他国债（我行承销）
                        //ZY0401999	其他国债（非我行承销）)
                        // 一级理财(ZY9901001	本行保本型理财产品 ZY9901005	我行本币结构性存款 ZY9901006	我行外币结构性存款)
                        // todo 信用证收益权
                        if(!Objects.equals("ZY0301003",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0301005",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0102999",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0102001",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0102002",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0102005",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0102006",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401001",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401002",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401003",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401004",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401005",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401998",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY0401999",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY9901001",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY9901005",guarBaseInfo.getGuarTypeCd()) &&
                                !Objects.equals("ZY9901006",guarBaseInfo.getGuarTypeCd())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03301);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        log.info("低风险质押时担保品范围校验结束*******************业务流水号：【{}】",contNo);
        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称：lmtAutoCreateGrtGuarCont
     * @方法描述： 对公授信, 自动创建最高额担保合同和关联表
     * @创建人：xs
     * @创建时间：2021/06/24 21:26
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional
    public void lmtAutoCreateGrtGuarCont(GrtGuarContDto grtGuarContDto, String contNo) {
        //担保方式  10抵押 20 质押 30保证
        String guarWay = grtGuarContDto.getGuarWay();

        if (CmisCommonConstants.GUAR_MODE_00.equals(guarWay)) {
            //担保方式为信用的时候，不生成担保
            return;
        }
        //授信额度编号
        String lmtAccNo = grtGuarContDto.getLmtAccNo();

        log.info("授信额度编号【"+lmtAccNo+"】生成主担保合同信息 开始");
        lmtAutoCreateGrtGuarCont(grtGuarContDto,contNo,CmisCommonConstants.STD_ZB_YES_NO_0);
        log.info("授信额度编号【"+lmtAccNo+"】生成主担保合同信息 结束");

        log.info("授信额度编号【"+lmtAccNo+"】生成追加担保合同信息 开始");
        lmtAutoCreateGrtGuarCont(grtGuarContDto,contNo,CmisCommonConstants.STD_ZB_YES_NO_1);
        log.info("授信额度编号【"+lmtAccNo+"】生成追加担保合同信息 结束");
    }

    /**
     * @方法名称: queryGrtGuarContByGuarNo
     * @方法描述: 根据押品统一编号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GrtGuarCont> queryGrtGuarContByGuarNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarCont> list = null;
        if (StringUtils.nonEmpty(Optional.ofNullable(model.getCondition().get("guarNo")).orElse(StringUtils.EMPTY).toString())) {
            list = grtGuarContMapper.queryGrtGuarContByGuarNo(model);
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryGrtGuarContByCusId
     * @方法描述: 根据客户编号获取为他人提供的担保
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GrtGuarContAndContNoDto> queryGrtGuarContByCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarContAndContNoDto> list = grtGuarContMapper.queryGrtGuarContByCusId(model);

        if (CollectionUtils.nonEmpty(list)){
            for (GrtGuarContAndContNoDto grtGuarContAndContNoDto : list) {
                String guarContNo = grtGuarContAndContNoDto.getGuarContNo();
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("guarContNo",guarContNo);
                //查询担保合同关联的借款合同
                String contNo = grtGuarBizRstRelService.selectContNoByGuarContNo(queryModel);
                grtGuarContAndContNoDto.setContNo(contNo);
            }
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryGrtGuarContByBorrowerCusId
     * @方法描述: 根据客户编号获取他人为其提供的担保
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GrtGuarCont> queryGrtGuarContByBorrowerCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarCont> list = list = grtGuarContMapper.queryGrtGuarContByBorrowerCusId(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据pkid集合查询所有的数据
     *
     * @param guarPkIds
     */
    List<GrtGuarCont> selectByGuarPkIds(List<String> guarPkIds) {
        return grtGuarContMapper.selectByGuarPkIds(guarPkIds);
    }

    /**
     * 担保合同注销校验 （是否存在在途有效主合同）
     *
     * @param guarContNo
     */
    public Boolean checkOnLogout(String guarContNo) {
        log.info("担保合同注销校验开始");
        try {
            List<GrtGuarBizRstRel> GrtGuarBizRstRelList = grtGuarBizRstRelMapper.selectListByGuarContNo(guarContNo);
            List<String> contNoList = GrtGuarBizRstRelList.parallelStream()
                    .map(GrtGuarBizRstRel::getContNo)
                    .filter(e -> {
                        return StringUtils.nonBlank(e);
                    })
                    .collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(contNoList)) {

                for (String contNo : contNoList) {
                    QueryModel model = new QueryModel();
                    // 合同生效状态
                    model.addCondition("contStatus", CmisCommonConstants.CONT_STATUS_200);
                    // 合同编号
                    model.addCondition("contNo", contNo);
                    model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                    // 查看是否含有生效合同
                    List<CtrLoanCont> ctrLoanConts = ctrLoanContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrLoanConts)) {
                        log.info("贷款合同校验不通过");
                        throw new BizException(null,"",null,"贷款合同【"+ctrLoanConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrHighAmtAgrCont> ctrHighAmtAgrConts = ctrHighAmtAgrContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrHighAmtAgrConts)) {
                        log.info("最高额授信校验不通过");
                        throw new BizException(null,"",null,"最高额授信【"+ctrHighAmtAgrConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrDiscCont> ctrDiscConts = ctrDiscContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrDiscConts)) {
                        log.info("贴现协议校验不通过");
                        throw new BizException(null,"",null,"贴现协议【"+ctrDiscConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrCvrgCont> ctrCvrgConts = ctrCvrgContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrCvrgConts)) {
                        log.info("保函协议校验不通过");
                        throw new BizException(null,"",null,"保函协议【"+ctrCvrgConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrTfLocCont> ctrTfLocConts = ctrTfLocContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrTfLocConts)) {
                        log.info("开证合同校验不通过");
                        throw new BizException(null,"",null,"开证合同【"+ctrTfLocConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrAccpCont> ctrAccpConts = ctrAccpContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrAccpConts)) {
                        log.info("银承合同校验不通过");
                        throw new BizException(null,"",null,"银承合同【"+ctrAccpConts.get(0).getContNo()+"】是生效状态");
                    }

                    List<CtrEntrustLoanCont> ctrEntrustLoanConts = ctrEntrustLoanContService.selectAll(model);
                    if (CollectionUtils.nonEmpty(ctrEntrustLoanConts)) {
                        log.info("委托贷款合同校验不通过");
                        throw new BizException(null,"",null,"委托贷款合同【"+ctrEntrustLoanConts.get(0).getContNo()+"】是生效状态");
                    }

                    log.info("借款合同号【"+contNo+"】，担保合同注销校验通过");
                }
            } else {
                log.info("未查询到对应主合同编号，当前担保合同编号：" + guarContNo + ";默认校验通过");
                return true;
            }
        } catch (Exception e) {
            log.info("担保合同注销校验异常,"+e.getMessage());
            throw new BizException(null,"",null, e.getMessage());
        }
        return true;
    }

    /**
     * 根据担保合同编号查询对应的担保合同状态
     *
     * @param guarContNo
     * @return
     */
    public String selectGuarContStateByGuarContNo(String guarContNo) {
        return grtGuarContMapper.selectGuarContStateByGuarContNo(guarContNo);
    }

    /**
     * 根据主合同编号查询关联的担保合同数据
     * @param contNo
     * @return
     */
    public GrtGuarCont queryGrtGuarContDataByGuarPkId(String contNo){
        return grtGuarContMapper.queryGrtGuarContDataByGuarPkId(contNo);
    }

    /**
     * 根据guarContNo集合查询所有的数据
     *
     * @param guarContNos
     */
    List<GrtGuarCont> selectByGuarContNos(List<String> guarContNos) {
        return grtGuarContMapper.selectByGuarContNos(guarContNos);
    }

    public List<GrtGuarContClientDto> queryGrtGuarContByContNohtdy(List<String> contNoList) {
        List<GrtGuarContClientDto> list = grtGuarContMapper.queryGrtGuarContByContNohtdy(contNoList);
        return list;
    }

    /**
     * 对公授信, 自动创建最高额担保合同和关联表
     * @param grtGuarContDto
     * @param contNo
     * @param isAddGuar
     */
    @Transactional
    public void lmtAutoCreateGrtGuarCont(GrtGuarContDto grtGuarContDto, String contNo,String isAddGuar){
        //担保方式
        String guarWay = grtGuarContDto.getGuarWay();//担保方式   //10抵押 20 质押 30保证
        //授信额度编号
        String lmtAccNo = grtGuarContDto.getLmtAccNo();
        String contNos = ctrLoanContService.selectContNosByLmtAccNo(lmtAccNo);
        log.info("担保方式【"+guarWay+"】根据授信额度编号查询关联的所有非否决非自行退出的合同编号:"+contNos);

        QueryModel model = new QueryModel();
        model.addCondition("serno", grtGuarContDto.getSubSerno());

        if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(isAddGuar)){
            model.addCondition("isAddGuar",CmisBizConstants.STD_GUAR_TYPE_101);
        }else if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isAddGuar)){
            model.addCondition("isAddGuar",CmisBizConstants.STD_GUAR_TYPE_102);
        }

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("lmtAccNo",lmtAccNo);
        queryModel.addCondition("isSuperaddGuar", isAddGuar);

        if ("10".equals(guarWay) || "20".equals(guarWay) || "21".equals(guarWay)) {
            String guarNos = "";
            queryModel.addCondition("guarWay",guarWay);

            //根据授信额度编号查询关联的所有非否决非自行退出的合同编号
            if (!StringUtils.isEmpty(contNos)){
                //查询合同编号对应的担保合同编号
                String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNos(contNos);
                log.info("担保方式【"+guarWay+"】查询合同编号对应的担保合同编号:"+guarContNos);

                if (!StringUtils.isEmpty(guarContNos)){
                    //查询授信额度分项关联担保合同下的押品编号
                    queryModel.addCondition("guarContNos",guarContNos);
                    guarNos = guarBaseInfoService.selectGuarNosByLmtAccNo(queryModel);
                    log.info("担保方式【"+guarWay+"】查询授信额度分项关联担保合同下的押品编号:"+guarNos);
                }
            }

            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(isAddGuar)){
                //如果是追加担保，则只查询担保方式和授信分项一致的押品
                model.addCondition("grtFlag","10".equals(guarWay) ? "01" : "02");
            }
            //查询授信额度分项关联的押品
            List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseDtoList = guarBaseInfoService.queryGuarInfoSellNoPage(model);

            if (CollectionUtils.isEmpty(guarBizRelGuarBaseDtoList)){
                //如果不存在押品，则无需新增担保合同
                return;
            }

            //新增担保合同与押品关联关系的押品列表
            List<String> guarNoList = new ArrayList<>();

            if (guarNos == null){
                guarNos = "";
            }

            for (GuarBizRelGuarBaseDto guarBizRelGuarBaseDto : guarBizRelGuarBaseDtoList) {
                //押品编号
                String guarNo = guarBizRelGuarBaseDto.getGuarNo();

                if (!guarNos.contains(guarNo)){
                    //该押品编号未与担保合同关联，则新增关联关系
                    guarNoList.add(guarNo);
                }
            }

            log.info("guarNoList:"+guarNoList);

            if (CollectionUtils.nonEmpty(guarNoList)){
                //担保合同编号
                String guarContNo = createGrtGuarCont(grtGuarContDto, contNo, isAddGuar);

                for (String guarNo : guarNoList) {
                    //生成担保合同和担保人(物)的关系
                    GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
                    BeanUtils.copyProperties(grtGuarContDto, grtGuarContRel);//copy公告字段数据
                    grtGuarContRel.setPkId(UUID.randomUUID().toString());
                    grtGuarContRel.setGuarContNo(guarContNo);
                    grtGuarContRel.setContNo(contNo);
                    grtGuarContRel.setGuarNo(guarNo);
                    grtGuarContRel.setIsMainGuar("");//是否主押品 1是 0否
                    grtGuarContRel.setStatus("1");//1生效 0失效
                    grtGuarContRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarContRelService.insert(grtGuarContRel);
                }
            }
        }else if ("30".equals(guarWay)) {
            //保证人id
            String guarantyIds = "";

            if (!StringUtils.isEmpty(contNos)){
                //查询合同编号对应的担保合同编号
                String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNos(contNos);
                log.info("担保方式【信用】，查询合同编号对应的担保合同编号:"+guarContNos);

                if (!StringUtils.isEmpty(guarContNos)){
                    //查询授信额度分项关联担保合同下的押品编号
                    queryModel.addCondition("guarContNos",guarContNos);
                    //根据授信分项编号查询担保合同关联的保证人id
                    guarantyIds = guarGuaranteeService.selectGuarantyIdsByLmtAccNo(queryModel);

                    log.info("担保方式【信用】，根据授信分项编号查询担保合同关联的保证人id:"+guarantyIds);
                }
            }

            //根据授信分项编号查询关联的保证人id
            List<GuarBizRelGuaranteeDto> guarBizRelGuaranteeDtoList = guarGuaranteeService.queryGuarInfoSellNoPage(model);

            if (CollectionUtils.isEmpty(guarBizRelGuaranteeDtoList)){
                //如果不存在保证人，则无需新增担保合同
                return;
            }

            if (guarantyIds == null){
                guarantyIds = "";
            }
            //新增担保合同与保证人关联关系的保证人列表
            List<String> guarantyIdList = new ArrayList<>();

            for (GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : guarBizRelGuaranteeDtoList) {
                //保证人id
                String guarantyId = guarBizRelGuaranteeDto.getGuarantyId();

                if (!guarantyIds.contains(guarantyId)){
                    //该保证人未与担保合同关联，则新增关联关系
                    guarantyIdList.add(guarantyId);
                }
            }

            log.info("guarantyIdList:"+guarantyIdList);

            if (CollectionUtils.nonEmpty(guarantyIdList)){
                //担保合同编号
                String guarContNo = createGrtGuarCont(grtGuarContDto, contNo, isAddGuar);

                for (String guarantyId : guarantyIdList) {
                    //生成担保合同和担保人(物)的关系
                    GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
                    BeanUtils.copyProperties(grtGuarContDto, grtGuarContRel);//copy公告字段数据
                    grtGuarContRel.setPkId(UUID.randomUUID().toString());
                    grtGuarContRel.setGuarContNo(guarContNo);
                    grtGuarContRel.setContNo(contNo);
                    grtGuarContRel.setGuarNo(guarantyId);
                    grtGuarContRel.setIsMainGuar("");//是否主押品 1是 0否
                    grtGuarContRel.setStatus("1");//1生效 0失效
                    grtGuarContRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    grtGuarContRelService.insert(grtGuarContRel);
                }
            }
        }
    }

    /**
     * 生成担保合同、担保合同与借款合同关系
     * @param grtGuarContDto
     * @param contNo
     * @param isAddGuar
     * @return 担保合同编号
     */
    public String createGrtGuarCont(GrtGuarContDto grtGuarContDto, String contNo,String isAddGuar){
        //原合同为最高额授信协议时才生成最高额担保合同
        String guarContType = "";
        if (CmisCommonConstants.STD_CONT_TYPE_3.equals(grtGuarContDto.getGuarContType())
                || CmisCommonConstants.STD_CONT_TYPE_2.equals(grtGuarContDto.getGuarContType())) {
            guarContType = CmisCommonConstants.STD_ZB_GUAR_CONT_TYPE_B;
        } else {
            guarContType = CmisCommonConstants.STD_ZB_GUAR_CONT_TYPE_A;
        }

        String serno = grtGuarContDto.getSerno();//业务流水号
        String cusId = grtGuarContDto.getCusId();//借款人编号
        String cusName = grtGuarContDto.getCusName();//借款人名称
        BigDecimal guarAmt = grtGuarContDto.getGuarAmt();//担保金额
        Integer guarTerm = grtGuarContDto.getGuarTerm();//担保期限
        String guarStartDate = grtGuarContDto.getGuarStartDate();//担保起始日
        String guarEndDate = grtGuarContDto.getGuarEndDate();//担保终止日
        String replyNO = grtGuarContDto.getReplyNo();//批复编号
        String lmtAccNo = grtGuarContDto.getLmtAccNo();//授信额度编号
        String subSerno = grtGuarContDto.getSubSerno();//授信分项流水号

        //非必输数据
        String isOnlinePld = grtGuarContDto.getIsOnlinePld();//是否在线抵押 1是 0否
        String lnCardNo = grtGuarContDto.getLnCardNo();//贷款卡号
        String curType = grtGuarContDto.getCurType();//币种

        //默认值
        String guarContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GRT_CONT_NO, new HashMap<>());

//        String guarContNo = sequenceTemplateClient.getSequenceTemplate("GRT_GUAR_SERNO", new HashMap<>());//担保合同号
        String pkId = UUID.randomUUID().toString();

        //生成合同和担保合同关系表 grt_guar_biz_rst_rel
        GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
        BeanUtils.copyProperties(grtGuarContDto, grtGuarBizRstRel);//copy公告字段数据
        grtGuarBizRstRel.setPkId(pkId);//主键
        grtGuarBizRstRel.setSerno(serno);//申请流水号
        grtGuarBizRstRel.setContNo(contNo);//合同编号
        grtGuarBizRstRel.setGuarPkId(pkId);//担保合同流水号
        grtGuarBizRstRel.setGuarContNo(guarContNo);//担保合同编号
        grtGuarBizRstRel.setGuarAmt(new BigDecimal(0));//担保金额
        grtGuarBizRstRel.setIsAddGuar(isAddGuar);//是否追加担保 1是 0否
        grtGuarBizRstRel.setIsPerGur(CmisCommonConstants.STD_ZB_YES_NO_0);//是否阶段性担保 1是 0否
        grtGuarBizRstRel.setCorreRel(CmisCommonConstants.STD_ZB_YES_NO_1);//关联关系 1生效 2 新增 3解除
        grtGuarBizRstRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        grtGuarBizRstRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarBizRstRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarBizRstRelService.insert(grtGuarBizRstRel);

        //生成最高额担保合同 grtGuarCont
        GrtGuarCont grtGuarCont = new GrtGuarCont();
        BeanUtils.copyProperties(grtGuarContDto, grtGuarCont);//copy公告字段数据/api/accloan
        grtGuarCont.setGuarPkId(pkId);//担保合同流水号
        grtGuarCont.setBizLine(grtGuarContDto.getBizLine());//担保合同流水号
        grtGuarCont.setGuarContNo(guarContNo);//担保合同编号
        grtGuarCont.setGuarContCnNo("");//担保合同中文合同编号
        grtGuarCont.setIsSuperaddGuar(isAddGuar);//是否追加担保1是 0否
        grtGuarCont.setGuarContType(guarContType);//担保合同类型 A一般担保合同 B 最高额担保合同
        grtGuarCont.setGuarWay(grtGuarContDto.getGuarWay());//担保方式  //10抵押 20 质押 30保证
        grtGuarCont.setIsOnlinePld(isOnlinePld);//是否在线抵押 1是 0否
        grtGuarCont.setIsUnderLmt(grtGuarContDto.getIsUnderLmt());//是否授信项下 1是 0否
        grtGuarCont.setLnCardNo(lnCardNo);//贷款卡号
        grtGuarCont.setCurType("CNY");//币种
        grtGuarCont.setGuarAmt(guarAmt);//担保金额
        grtGuarCont.setTermType("");//期限类型 D日  M月  Y年
        grtGuarCont.setGuarTerm(guarTerm);//担保期限
        grtGuarCont.setGuarStartDate(guarStartDate);//担保起始日
        grtGuarCont.setGuarEndDate(guarEndDate);//担保终止日
        grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_100);//担保合同状态//100 未生效 101 生效 104 注销  107 作废
        grtGuarCont.setReplyNo(replyNO);//批复编号
        grtGuarCont.setLmtAccNo(lmtAccNo);//授信额度编号
        grtGuarCont.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

        if (StringUtils.isEmpty(grtGuarCont.getBizLine())){
            log.error("担保合同【"+guarContNo+"】的业务条线为空！");
        }
        grtGuarContMapper.insert(grtGuarCont);

        return guarContNo;
    }

    /**
     * 根据流水号查询担保合同集合
     * @param serno
     * @return
     */
    public List<GrtGuarCont> selectDataBySerno(String serno){
        List<String> guarContNos = grtGuarBizRstRelMapper.selectguarContNosBySerno(serno);
        List<GrtGuarCont> result = new ArrayList<>();
        if(CollectionUtils.nonEmpty(guarContNos)){
            result = grtGuarContMapper.selectByGuarContNos(guarContNos);
        }
        return result;
        //return grtGuarContMapper.selectDataBySerno(serno);
    }

    /**
     * 存在担保合同项下的押品，抵质押标识为质押且担保分类代码为"ZY0301005"或"ZY0301003"，需要恢复承兑行白名单额度
     * @param guarContNo
     */
    public void resumeAorgLmtWhite(String guarContNo){
        String guarNos = grtGuarContRelService.selectAorgInfoByGuarContNo(guarContNo);

        if (StringUtils.nonEmpty(guarNos)){
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
            }

            //当前登录人
            String currentUserId = userInfo.getLoginCode();
            //当前登录机构
            String orgId = userInfo.getOrg().getCode();
            //系统营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");

            String[] guarNoArr = guarNos.split(",");

            CmisLmt0025ReqDto cmisLmt0025ReqDto = new CmisLmt0025ReqDto();
            List<CmisLmt0025OccRelListReqDto> occRelList = new ArrayList<>();

            for (String guarNo : guarNoArr) {
                CmisLmt0025OccRelListReqDto dealBiz = new CmisLmt0025OccRelListReqDto();
                dealBiz.setDealBizNo(guarNo);//交易业务编号取押品编号
                occRelList.add(dealBiz);
            }

            cmisLmt0025ReqDto.setOccRelList(occRelList);
            // 系统编号
            cmisLmt0025ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);
            // 金融机构代码
            cmisLmt0025ReqDto.setInstuCde("001");
            cmisLmt0025ReqDto.setInputId(currentUserId);//登记人取当前登录人
            cmisLmt0025ReqDto.setInputBrId(orgId);//登记机构取当前登录机构
            cmisLmt0025ReqDto.setInputDate(openDay);//登记日期取系统营业日期

            log.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025ReqDto));
            ResultDto<CmisLmt0025RespDto> cmisLmt0025RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0025(cmisLmt0025ReqDto)).orElse(new ResultDto<>());
            log.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025RespDtoResultDto));

            if (Objects.equals(cmisLmt0025RespDtoResultDto.getCode(), "0")) {
                if(!Objects.equals(cmisLmt0025RespDtoResultDto.getData().getErrorCode(), "0000")){
                    throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getData().getErrorMsg());
                }
            }else{
                throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getMessage());
            }
        }
    }
}
