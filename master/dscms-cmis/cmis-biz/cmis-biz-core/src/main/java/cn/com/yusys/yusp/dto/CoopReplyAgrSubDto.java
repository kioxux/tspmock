package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAgrSub
 * @类描述: coop_reply_agr_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-26 17:10:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopReplyAgrSubDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 批复编号 **/
	private String replyNo;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 分项编号 **/
	private String subNo;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 合作产品编号 **/
	private String coopPrdId;
	
	/** 产品属性 **/
	private String prdTypeProp;
	
	/** 本次签订金额 **/
	private java.math.BigDecimal signAmt;
	
	/** 单个产品合作额度(元) **/
	private java.math.BigDecimal singlePrdCoopLmt;
	
	/** 单笔最低缴存金额(元) **/
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}
	
    /**
     * @return ReplyNo
     */	
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subNo
	 */
	public void setSubNo(String subNo) {
		this.subNo = subNo == null ? null : subNo.trim();
	}
	
    /**
     * @return SubNo
     */	
	public String getSubNo() {
		return this.subNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param coopPrdId
	 */
	public void setCoopPrdId(String coopPrdId) {
		this.coopPrdId = coopPrdId == null ? null : coopPrdId.trim();
	}
	
    /**
     * @return CoopPrdId
     */	
	public String getCoopPrdId() {
		return this.coopPrdId;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp == null ? null : prdTypeProp.trim();
	}
	
    /**
     * @return PrdTypeProp
     */	
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param signAmt
	 */
	public void setSignAmt(java.math.BigDecimal signAmt) {
		this.signAmt = signAmt;
	}
	
    /**
     * @return SignAmt
     */	
	public java.math.BigDecimal getSignAmt() {
		return this.signAmt;
	}
	
	/**
	 * @param singlePrdCoopLmt
	 */
	public void setSinglePrdCoopLmt(java.math.BigDecimal singlePrdCoopLmt) {
		this.singlePrdCoopLmt = singlePrdCoopLmt;
	}
	
    /**
     * @return SinglePrdCoopLmt
     */	
	public java.math.BigDecimal getSinglePrdCoopLmt() {
		return this.singlePrdCoopLmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return SigLowDepositAmt
     */	
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return BailPerc
     */	
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}