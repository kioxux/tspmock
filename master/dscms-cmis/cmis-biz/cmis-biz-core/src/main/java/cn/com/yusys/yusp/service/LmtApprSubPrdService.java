package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtApprSubPrdMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.management.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprSubPrdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:35:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtApprSubPrdService {

    private static final Logger log = LoggerFactory.getLogger(LmtAppSubPrdService.class);

    @Resource
    private LmtApprSubPrdMapper lmtApprSubPrdMapper;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtApprSubPrd selectByPrimaryKey(String pkId) {
        return lmtApprSubPrdMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtApprSubPrd> selectAll(QueryModel model) {
        List<LmtApprSubPrd> records = (List<LmtApprSubPrd>) lmtApprSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtApprSubPrd> list = lmtApprSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtApprSubPrd record) {
        return lmtApprSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtApprSubPrd record) {
        return lmtApprSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtApprSubPrd record) {
        return lmtApprSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtApprSubPrd record) {
        return lmtApprSubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtApprSubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtApprSubPrdMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectLmtApprSubPrdByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprSubPrd> selectLmtApprSubPrdByParams(HashMap paramsMap) {
        return lmtApprSubPrdMapper.selectLmtApprSubPrdByParams(paramsMap);
    }

    /**
     * @方法名称: generateLmtApprSubPrdByLmtAppSub
     * @方法描述: 通过授信申请分项生成授信审批分项适用数据
     * @参数与返回说明: 接受授信申请流水号，返回授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubPrdByLmtAppSub(String currentUserId, String currentOrgId, LmtAppSub lmtAppSub, String apprSubSerno) throws Exception {
        // 遍历分项适用品种
        HashMap<String, String> querySubPrdMap = new HashMap<>();
        querySubPrdMap.put("subSerno", lmtAppSub.getSubSerno());
        querySubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectByParams(querySubPrdMap);
        if (lmtAppSubPrdList != null && !lmtAppSubPrdList.isEmpty()) {
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                generateLmtApprSubPrdByLmtAppSubPrd(currentUserId, currentOrgId , apprSubSerno, lmtAppSubPrd);
            }
        } else {
            throw new Exception("授信申请分项适用品种数据查询异常!");
        }
    }

    /**
     * @方法名称: generateLmtApprSubPrdByLmtAppSub
     * @方法描述: 通过授信申请分项适用品种生成授信审批分项适用品种数据
     * @参数与返回说明: 接受授信申请流水号，返回授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubPrdByLmtAppSubPrd(String currentUserId, String currentOrgId, String apprSubSerno, LmtAppSubPrd lmtAppSubPrd) {
        LmtApprSubPrd newLmtApprSubPrd = new LmtApprSubPrd();
        BeanUtils.copyProperties(lmtAppSubPrd, newLmtApprSubPrd);
        String apprSubPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
        newLmtApprSubPrd.setPkId(UUID.randomUUID().toString());
        newLmtApprSubPrd.setApprSubPrdSerno(apprSubPrdSerno);
        newLmtApprSubPrd.setApproveSubSerno(apprSubSerno);
        newLmtApprSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSubPrd.setUpdId(currentUserId);
        newLmtApprSubPrd.setUpdBrId(currentOrgId);
        newLmtApprSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtApprSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(newLmtApprSubPrd);

        // 新增 还款计划 落数
        List<RepayCapPlan> list = repayCapPlanService.selectBySerno(lmtAppSubPrd.getSubPrdSerno());
        log.info("新增还款计划落库处理-----------原申请时还款计划为:"+ JSON.toJSONString(list));
        for(RepayCapPlan repayCapPlan : list){
            repayCapPlan.setPkId(UUID.randomUUID().toString());
            repayCapPlan.setSerno(apprSubPrdSerno);
            repayCapPlan.setUpdId(currentUserId);
            repayCapPlan.setUpdBrId(currentOrgId);
            repayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            repayCapPlan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            repayCapPlanService.insert(repayCapPlan);
        }
    }


    /**
     * @方法名称: generateLmtApprSubPrdByLmtAppSub
     * @方法描述: 通过授信审批分项生成授信审批分项适用品种数据
     * @参数与返回说明: 接受授信申请流水号，返回授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubPrdByLmtApprSub(String currentUserId,String currentOrgId, LmtApprSub lmtApprSub, String repaySubSerno) throws Exception {
        HashMap<String, String> querySubPrdMap = new HashMap<>();
        querySubPrdMap.put("apprSubSerno", lmtApprSub.getApproveSubSerno());
        querySubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("根据授信审批分项信息生成授信审批分项品种数据,查询的审批分项表流水号为:{}", lmtApprSub.getApproveSubSerno());
        List<LmtApprSubPrd> lmtApprSubPrdList = this.selectLmtApprSubPrdByParams(querySubPrdMap);
        if (lmtApprSubPrdList != null && lmtApprSubPrdList.size() > 0) {
            for (LmtApprSubPrd lmtApprSubPrd : lmtApprSubPrdList) {
                generateLmtApprSubPrdByLmtApprSubPrd(currentUserId, currentOrgId, repaySubSerno, lmtApprSubPrd);
            }
        } else {
            throw new Exception("授信审批分项适用品种数据查询异常!");
        }
    }

    /**
     * @方法名称: generateLmtApprSubPrdByLmtApprSubPrd
     * @方法描述: 通过授信审批分项适用品种生成授信审批分项适用品种数据
     * @参数与返回说明: 接受授信申请流水号，返回授信审批流水号
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubPrdByLmtApprSubPrd(String currentUserId, String currentOrgId, String repaySubSerno, LmtApprSubPrd lmtApprSubPrd) {
        LmtApprSubPrd newLmtApprSubPrd = new LmtApprSubPrd();
        BeanUtils.copyProperties(lmtApprSubPrd, newLmtApprSubPrd);
        String repaySubPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
        newLmtApprSubPrd.setPkId(UUID.randomUUID().toString());
        newLmtApprSubPrd.setApprSubPrdSerno(repaySubPrdSerno);
        newLmtApprSubPrd.setApproveSubSerno(repaySubSerno);
        newLmtApprSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
//        lmtApprSubPrd.setUpdId(currentUserId);
//        lmtApprSubPrd.setUpdBrId(currentOrgId);
        newLmtApprSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtApprSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        log.info("根据原审批分项品种生成新的审批分项品种信息,新审批分项品种流水号为:{}", repaySubPrdSerno);
        this.insert(newLmtApprSubPrd);

        // 新增 还款计划 落数
        List<RepayCapPlan> list = repayCapPlanService.selectBySerno(lmtApprSubPrd.getApprSubPrdSerno());
        log.info("新增还款计划落库处理----------上一审批人审批时还款计划为:"+ JSON.toJSONString(list));
        for(RepayCapPlan repayCapPlan : list){
            repayCapPlan.setPkId(UUID.randomUUID().toString());
            repayCapPlan.setSerno(repaySubPrdSerno);
            repayCapPlan.setUpdId(currentUserId);
            repayCapPlan.setUpdBrId(currentOrgId);
            repayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            repayCapPlan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            repayCapPlanService.insert(repayCapPlan);
        }
    }

    /**
     * @方法名称: queryLmtApproveSubPrdByParams
     * @方法描述: 通过参数查询授信批复分项使用品种数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprSubPrd> queryLmtApproveSubPrdByParams(HashMap<String, String> queryMaps) {
        return lmtApprSubPrdMapper.queryLmtApproveSubPrdByParams(queryMaps);
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据授信分项流水号查询适用授信产品
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-05-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtApprSubPrd> selectBySubSerno(String approveSubSerno) {
        Map map = new HashMap();
        map.put("approveSubSerno", approveSubSerno);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApprSubPrd> list = lmtApprSubPrdMapper.selectByParams(map);
        return list;
    }

    /**
     * @方法名称: deleteByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map deleteByPkId(LmtApprSubPrd record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            int count = lmtApprSubPrdMapper.updateByPkId(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",删除失败！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("删除授信明细情况！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @方法名称: saveLmtApprSubPrd
     * @方法描述: 新增使用授信分项明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map saveLmtApprSubPrd(LmtApprSubPrd lmtApprSubPrd) {

        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        Map pramas = new HashMap();
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String subPrdSerno = "";
        try {

            // TODO 保存前校验 待补充

            String subSerno = lmtApprSubPrd.getApproveSubSerno();
            String lmtBizType = lmtApprSubPrd.getLmtBizType();
            String subPrdName = lmtApprSubPrd.getLmtBizTypeName();
            LmtApprSub lmtApprSub = lmtApprSubService.selectBySubSerno(subSerno);
            String subName = lmtApprSub.getSubName();

//            BigDecimal lmtAmt = lmtApprSub.getLmtAmt();
//            BigDecimal lmtAmtTotal = lmtApprSubPrdMapper.calLmtAmt(subSerno);
//            BigDecimal lmtAmtSubPrd = lmtApprSubPrd.getLmtAmt();
//            if (lmtAmt.compareTo(lmtAmtTotal.add(lmtAmtSubPrd)) < 0) {
//                rtnCode = EcbEnum.ECB010007.key;
//                rtnMsg = EcbEnum.ECB010007.value;
//                return rtnData;
//            }
            subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(subPrdSerno)) {
                rtnCode = EcbEnum.ECB010003.key;
                rtnMsg = EcbEnum.ECB010003.value;
                return rtnData;
            }
            log.info("保存适用授信产品分项数据,生成流水号{}", subPrdSerno);


            // 流水号   由于当前数据是审批中新增的数据,那么申请时的分项明细编号不需要赋值
            lmtApprSubPrd.setApprSubPrdSerno(subPrdSerno);
            // 数据操作标志为新增
            lmtApprSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 币种
            lmtApprSubPrd.setCurType(CmisCommonConstants.CUR_TYPE_CNY);

            log.info("保存授信适用产品数据{}-获取当前登录用户数据", subPrdSerno);
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.ECB010004.key;
                rtnMsg = EcbEnum.ECB010004.value;
                return rtnData;
            } else {
                lmtApprSubPrd.setPkId(UUID.randomUUID().toString());
                lmtApprSubPrd.setInputId(userInfo.getLoginCode());
                lmtApprSubPrd.setInputBrId(userInfo.getOrg().getCode());
                lmtApprSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtApprSubPrd.setUpdId(userInfo.getLoginCode());
                lmtApprSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                lmtApprSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtApprSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                lmtApprSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }
            if ("".equals(subName) || subName == null) {
                subName = subName + subPrdName;
            } else {
                subName = subName + "," + subPrdName;
            }
            lmtApprSub.setSubName(subName);
            log.info("保存适用分项数据,流水号{}", subSerno);
            int countSub = lmtApprSubService.update(lmtApprSub);

            log.info("校验分项与分项明细下的规则------end---------");
            log.info("保存适用授信产品明细数据,流水号{}", subPrdSerno);
            int count = lmtApprSubPrdMapper.insert(lmtApprSubPrd);
            if (count != 1 || countSub != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",保存失败！");
            }

//            log.info("校验分项与分项明细下的规则------start---------");
//            String checkString = checkPrdLmtDataRules(lmtApprSubPrd.getApproveSubSerno(),lmtApprSubPrd);
//            if(checkString.equals(EcbEnum.ECB010000.key)){
//                rtnCode = EcbEnum.ECB010000.key;
//                rtnMsg = EcbEnum.ECB010000.value;
//            }else if(checkString.equals(EcbEnum.ECB020049.key)){
//                rtnCode = EcbEnum.ECB020049.key;
//                rtnMsg = EcbEnum.ECB020049.value;
//                throw BizException.error(null, EcbEnum.ECB020049.key, EcbEnum.ECB020049.value);
//            }else if(checkString.equals(EcbEnum.ECB020050.key)){
//                rtnCode = EcbEnum.ECB020050.key;
//                rtnMsg = EcbEnum.ECB020050.value;
//                throw BizException.error(null, EcbEnum.ECB020050.key, EcbEnum.ECB020050.value);
//            }else{
//                lmtApprSubPrdMapper.deleteByPrimaryKey(lmtApprSubPrd.getPkId());
//                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
//            }
//            log.info("校验分项与分项明细下的规则------end---------");
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存适用产品授信分项数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("subPrdSerno", subPrdSerno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: checkPrdLmtDataRules
     * @方法描述: 校验分项与分项明细规则
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String checkPrdLmtDataRules(String apprSubSerno, LmtApprSubPrd lmtApprSubPrd) {
        String rtnCode = EcbEnum.ECB010000.key;
        try{
            log.info("当前分项品种信息:"+ JSON.toJSONString(lmtApprSubPrd));
            LmtApprSub lmtApprSub = lmtApprSubService.selectBySubSerno(apprSubSerno);
            log.info("当前分项品种对应的分项信息:"+ JSON.toJSONString(lmtApprSub));
            // 判断当前分项品种额度是否已超出分项额度
            if(lmtApprSubPrd.getLmtAmt().compareTo(lmtApprSub.getLmtAmt()) > 0){
                rtnCode = EcbEnum.ECB020049.key;
                return rtnCode;
            }
            BigDecimal sumSubAmt = new BigDecimal(0);
            HashMap map = new HashMap();
            map.put("apprSubSerno",apprSubSerno);
            map.put("oprType",CmisCommonConstants.OP_TYPE_01);
            List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdMapper.selectLmtApprSubPrdByParams(map);
            log.info("当前分项品种对应的分项项下所有的品种信息:"+ JSON.toJSONString(lmtApprSubPrdList));
            for(LmtApprSubPrd lmtApprSubPrd1 : lmtApprSubPrdList){
                if(lmtApprSubPrd1.getLmtAmt() != null && !lmtApprSubPrd.getApprSubPrdSerno().equals(lmtApprSubPrd1.getApprSubPrdSerno())){
                    sumSubAmt = sumSubAmt.add(lmtApprSubPrd1.getLmtAmt());
                }
            }
            sumSubAmt = sumSubAmt.add(lmtApprSubPrd.getLmtAmt());
            log.info("当前分项品种对应的分项项下所有的品种额度之和:"+ JSON.toJSONString(sumSubAmt));
            if(lmtApprSub.getLmtAmt().compareTo(sumSubAmt) > 0){
                rtnCode = EcbEnum.ECB020050.key;
                return rtnCode;
            }
        }catch (Exception e){
            rtnCode = String.valueOf(e.hashCode());
            throw BizException.error(null,String.valueOf(e.hashCode()),e.getMessage());
        }
        return rtnCode;
    }

    /**
     * @方法名称: selectBySubPrdSerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtApprSubPrd selectBySubPrdSerno(String subPrdSerno) {
        return lmtApprSubPrdMapper.selectBySubPrdSerno(subPrdSerno);
    }

    /**
     * @方法名称: updateLmtApprSubPrd
     * @方法描述: 根据前台传入表单数据更新授信分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map updateLmtApprSubPrd(LmtApprSubPrd lmtApprSubPrd) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {

            // TODO 保存前校验 待补充
            LmtApprSubPrd lmtAppSubPrdOrigin = lmtApprSubPrdMapper.selectByPrimaryKey(lmtApprSubPrd.getPkId());
            String subSerno = lmtApprSubPrd.getSubSerno();
            User userInfo = SessionUtils.getUserInformation();
            lmtApprSubPrd.setUpdId(userInfo.getLoginCode());
            lmtApprSubPrd.setUpdBrId(userInfo.getOrg().getCode());
            lmtApprSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtApprSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

            //更新授信适用产品数据
            log.info("更新授信适用产品信息");
            int count = lmtApprSubPrdMapper.updateByPrimaryKey(lmtApprSubPrd);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新授信适用产品信息失败！");
            }

            log.info("校验分项与分项明细下的规则------start---------");
            String checkString = checkPrdLmtDataRules(lmtApprSubPrd.getApproveSubSerno(),lmtApprSubPrd);
            if(checkString.equals(EcbEnum.ECB010000.key)){
                rtnCode = EcbEnum.ECB010000.key;
                rtnMsg = EcbEnum.ECB010000.value;
            }else if(checkString.equals(EcbEnum.ECB020049.key)){
                rtnCode = EcbEnum.ECB020049.key;
                rtnMsg = EcbEnum.ECB020049.value;
                lmtApprSubPrdMapper.deleteByPrimaryKey(lmtApprSubPrd.getPkId());
                lmtApprSubPrdMapper.insert(lmtAppSubPrdOrigin);
                return rtnData;
            }else if(checkString.equals(EcbEnum.ECB020050.key)){
                rtnCode = EcbEnum.ECB020050.key;
                rtnMsg = EcbEnum.ECB020050.value;
                lmtApprSubPrdMapper.deleteByPrimaryKey(lmtApprSubPrd.getPkId());
                lmtApprSubPrdMapper.insert(lmtAppSubPrdOrigin);
                return rtnData;
            }else{
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
            }
            log.info("校验分项与分项明细下的规则------end---------");

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新授信分项信息出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 根据业务流水号与授信类型查询授信分项
     *
     * @param serno
     * @param lmtBizType
     * @return
     */
    public ArrayList<LmtApprSubPrd> queryLmtApprSubBySerno(String serno, String lmtBizType) {
        return lmtApprSubPrdMapper.queryLmtApprSubBySerno(serno, lmtBizType);
    }

    /**
     * @函数名称: queryLmtApprSubDataBySerno
     * @函数描述: 根据申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */

    public ArrayList<LmtApprSubPrd> queryLmtApprSubDataBySerno(Map<String, String> params) {
        return lmtApprSubPrdMapper.queryLmtApprSubDataBySerno(params);
    }

    /**
     * @函数名称: queryLmtApprSubDataByApprSerno
     * @函数描述: 根据申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */

    public ArrayList<LmtApprSubPrd> queryLmtApprSubDataByApprSerno(Map<String, String> params) {
        return lmtApprSubPrdMapper.queryLmtApprSubDataByApprSerno(params);
    }

    /**
     * @函数名称: queryLmtApprSubDataByGrpApproveSerno
     * @函数描述: 根据集团申请流水号查询授信审批过程中分项品种数据
     * @创建人: css
     */

    public ArrayList<LmtApprSubPrd> queryLmtApprSubDataByGrpApproveSerno(Map<String, String> params) {
        ArrayList<LmtApprSubPrd> lmtApprSubPrdArrayList = new ArrayList<>();
        // 避免传值为空的情况
        if(StringUtils.nonBlank((String) params.get("grpApproveSerno"))){
            List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno((String) params.get("grpApproveSerno"));
            if(CollectionUtils.nonEmpty(lmtGrpMemRels)){
                for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels){
                    params.put("apprSerno",lmtGrpMemRel.getSingleSerno());
                    log.info("当前成员审批数据:"+JSON.toJSONString(lmtGrpMemRel));
                    ArrayList<LmtApprSubPrd> lmtApprSubPrds = lmtApprSubPrdMapper.queryLmtApprSubDataByApprSerno(params);
                    lmtApprSubPrdArrayList.addAll(lmtApprSubPrds);
                }
            }
        }
        return lmtApprSubPrdArrayList;
    }

    /**
     * @函数名称:batchUpdate
     * @函数描述:批量对象修改
     * @参数与返回说明:
     * @算法描述:
     */

    public int batchUpdate(List<LmtApprSubPrd> lmtApprSubPrdList) {
        int num = 0;
        if(!lmtApprSubPrdList.isEmpty() && lmtApprSubPrdList.size() > 0){
            for(LmtApprSubPrd lmtApprSubPrd :lmtApprSubPrdList){
                LmtApprSubPrd oldLmtApprSubPrd = this.selectByPrimaryKey(lmtApprSubPrd.getPkId());
                oldLmtApprSubPrd.setRepayPlanDesc(lmtApprSubPrd.getRepayPlanDesc());
                num += update(oldLmtApprSubPrd);
            }
        }
        return num;
    }

    /**
     * @函数名称: afterInitControlPanelShowOrHide
     * @函数描述: 复审表后处理控制显隐
     * @创建人: css
     */
    public Map afterInitControlPanelShowOrHide(Map<String, String> params) throws Exception {
        Map map = new HashMap();
        String lmtSerno = params.get("serno");
        if(StringUtils.nonBlank(lmtSerno)){
            // 除却抵押和保证外的担保方式 都显示第四个
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtSerno);
            if(CollectionUtils.nonEmpty(lmtAppSubList)){
                // 获取上期信息
                LmtApp lmtApp = lmtAppService.selectBySerno(lmtSerno);
                if(lmtApp != null ){
                    LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtApp.getOrigiLmtReplySerno());
                    if(lmtReply != null){
                        map.put("oldLmtStartDate",lmtReply.getReplyInureDate());
                        map.put("oldLmtTerm",lmtReply.getLmtTerm());
                    }
                }
                for(LmtAppSub lmtAppSub : lmtAppSubList){
                    // 抵押
                    if(lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_10)){

                    }else if(lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_30)){
                        // 保证
                        // 查看当前申请项下的所有的分项中主担保标识的保证人信息
                        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.getMainGuarGuaranteeBySerno(lmtSerno,CmisCommonConstants.STD_GUAR_TYPE_101);
                        if(CollectionUtils.nonEmpty(guarGuaranteeList)){
                            for(GuarGuarantee guarGuarantee : guarGuaranteeList){
                                if(CmisCommonConstants.STD_ASSURE_CUS_TYPE_10003.equals(guarGuarantee.getCusTyp())){
                                    // 如果存在企业保证人 显示第一个菜单
                                    map.put("mainGuarWay1",true);
                                }else if(CmisCommonConstants.STD_ASSURE_CUS_TYPE_10001.equals(guarGuarantee.getCusTyp())){
                                    // 存在 担保公司保证人 显示第二个菜单
                                    map.put("mainGuarWay3",true);
                                }else if(CmisCommonConstants.STD_ASSURE_CUS_TYPE_10002.equals(guarGuarantee.getCusTyp())){
                                    // 存在 个人保证人 显示第四个菜单
                                    map.put("mainGuarWay4",true);
                                }else{
                                    continue;
                                }
                            }
                        }
                    }else{
                        map.put("mainGuarWay4",true);
                    }
                    // 上面已经判断了 主担保为担保公司的要展示 新增 主担保不是担保公司但是存在担保公司的也要显示
                    List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.getMainGuarGuaranteeBySerno(lmtSerno,CmisCommonConstants.STD_GUAR_TYPE_102);
                    if(CollectionUtils.nonEmpty(guarGuaranteeList)){
                        map.put("mainGuarWay3",true);
                    }
                    // 直接查询押品信息  存在则显示第三个标题
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno",lmtAppSub.getSubSerno());
                    List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.queryGuarBaseInfoDataByParams(queryModel);
                    if(CollectionUtils.nonEmpty(guarBaseInfoList)){
                        map.put("mainGuarWay2",true);
                    }
                }
            }

        }
        return map;
    }

    /**
     * @函数名称: refshAppData
     * @函数描述: 重新拉去授信申请数据
     * @创建人: css
     */

    public LmtAppr refshAppData(Map<String, String> params) {
        LmtAppr newlmtAppr = new LmtAppr();
        LmtAppr oldlmtAppr = new LmtAppr();
        int num = 0;
        if(StringUtils.nonBlank(params.get("serno"))){
            newlmtAppr = lmtApprService.queryFinalLmtApprBySerno(params.get("serno"));
            if(!StringUtils.isBlank(newlmtAppr.getIssueReportType())){
                log.info("查询当前流水号对应的最新的审批中数据,已存在出具报告类型，所以不做处理:"+JSON.toJSONString(newlmtAppr));
                return newlmtAppr;
            }
            // 1、查询当前流水号对应的最新的审批中数据
            log.info("查询当前流水号对应的最新的审批中数据:"+JSON.toJSONString(newlmtAppr));
            // 2、查询当前流水号、出具报告类型 对应的的最新的审批中数据
            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            oldlmtAppr = lmtApprService.queryInfoBySernoAndIssueReportType(params);
            log.info("查询当前流水号、出具报告类型 对应的的最新的审批中数据:"+JSON.toJSONString(oldlmtAppr));
            if(oldlmtAppr.getApproveSerno().equals(newlmtAppr.getApproveSerno())){
                log.info("根据当前流水号以及出具报告类型查询的审批表数据与最新的审批表数据一致,不做处理:"+JSON.toJSONString(oldlmtAppr));
                return newlmtAppr;
            }
            // 3、将核查报告和审查报告中的页面个别字段（）和用信条件表中的数据重新建立新的关系
            // 通过授信审批用信条件生成新的授信审批用信条件
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(newlmtAppr, oldlmtAppr);
            newlmtAppr.setIssueReportType(StringUtils.nonBlank(params.get("issueReportType"))?params.get("issueReportType"):"");
            // 审查
            newlmtAppr.setLoanApprMode(oldlmtAppr.getLoanApprMode());//用信审核方式
            newlmtAppr.setReviewContent(oldlmtAppr.getReviewContent());//审查内容
            newlmtAppr.setRiskFactor(oldlmtAppr.getRiskFactor());//风险因素
            newlmtAppr.setReviewConclusion(oldlmtAppr.getReviewConclusion());//评审结论
            newlmtAppr.setRestDesc(oldlmtAppr.getRestDesc());// 结论性描述
            newlmtAppr.setIsRestruLoan(oldlmtAppr.getIsRestruLoan());//是否重组贷款
            newlmtAppr.setGuarAssureAcc(oldlmtAppr.getGuarAssureAcc());//抵质押担保其他事项说明
            newlmtAppr.setGeneralAssureAcc(oldlmtAppr.getGeneralAssureAcc());//保证担保其他事项说明
            newlmtAppr.setIsRequestManageAfterLoan(oldlmtAppr.getIsRequestManageAfterLoan());//是否按要求进行贷后管理
            newlmtAppr.setIsPerGur(oldlmtAppr.getIsPerGur());//是否涉及阶段性担保
            newlmtAppr.setPerGurTerm(oldlmtAppr.getPerGurTerm());// 阶段性担保期限（月）
            newlmtAppr.setIsOutLmtQuotaMana(oldlmtAppr.getIsOutLmtQuotaMana());//是否超限额管理要求
            newlmtAppr.setOtherDesc(oldlmtAppr.getOtherDesc());//其他说明
            // 核查
            newlmtAppr.setIndgtReportMode(oldlmtAppr.getIndgtReportMode());//核查报告模式
            newlmtAppr.setIndgtReportPath(oldlmtAppr.getIndgtReportPath());//核查报告路径
            newlmtAppr.setInteEvlu(oldlmtAppr.getInteEvlu());//综合评价
            newlmtAppr.setLmtQuotaSitu(oldlmtAppr.getLmtQuotaSitu());//授信限额情况
            log.info("将核查报告和审查报告中的页面个别字段（）和用信条件表中的数据重新建立新的关系:"+JSON.toJSONString(newlmtAppr));
            num = lmtApprService.updateSelective(newlmtAppr);
        }
        return newlmtAppr;
    }
}
