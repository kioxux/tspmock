/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FdyddLmtSubGuar;
import cn.com.yusys.yusp.service.FdyddLmtSubGuarService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FdyddLmtSubGuarResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-17 11:02:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/fdyddlmtsubguar")
public class FdyddLmtSubGuarResource {
    @Autowired
    private FdyddLmtSubGuarService fdyddLmtSubGuarService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FdyddLmtSubGuar>> query() {
        QueryModel queryModel = new QueryModel();
        List<FdyddLmtSubGuar> list = fdyddLmtSubGuarService.selectAll(queryModel);
        return new ResultDto<List<FdyddLmtSubGuar>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FdyddLmtSubGuar>> index(QueryModel queryModel) {
        List<FdyddLmtSubGuar> list = fdyddLmtSubGuarService.selectByModel(queryModel);
        return new ResultDto<List<FdyddLmtSubGuar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<FdyddLmtSubGuar> show(@PathVariable("pkId") String pkId) {
        FdyddLmtSubGuar fdyddLmtSubGuar = fdyddLmtSubGuarService.selectByPrimaryKey(pkId);
        return new ResultDto<FdyddLmtSubGuar>(fdyddLmtSubGuar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FdyddLmtSubGuar> create(@RequestBody FdyddLmtSubGuar fdyddLmtSubGuar) throws URISyntaxException {
        fdyddLmtSubGuarService.insert(fdyddLmtSubGuar);
        return new ResultDto<FdyddLmtSubGuar>(fdyddLmtSubGuar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FdyddLmtSubGuar fdyddLmtSubGuar) throws URISyntaxException {
        int result = fdyddLmtSubGuarService.update(fdyddLmtSubGuar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = fdyddLmtSubGuarService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = fdyddLmtSubGuarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:fdyddLmtSubGuarlist
     * @函数描述: 获取申请信息结果
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷授信押品关联")
    @PostMapping("/fdyddLmtSubGuarlist")
    protected ResultDto<List<FdyddLmtSubGuar>> fdyddLmtSubGuarlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<FdyddLmtSubGuar> list = fdyddLmtSubGuarService.fdyddLmtSubGuarlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<FdyddLmtSubGuar>>(list);
    }

    /**
     * @函数名称:fdyddLmtSubGuarHislist
     * @函数描述: 获取已审批结果
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷授信押品关联")
    @PostMapping("/fdyddLmtSubGuarHislist")
    protected ResultDto<List<FdyddLmtSubGuar>> fdyddLmtSubGuarHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<FdyddLmtSubGuar> list = fdyddLmtSubGuarService.fdyddLmtSubGuarHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<FdyddLmtSubGuar>>(list);
    }

    /**
     * @函数名称:insertFdyddLmtSubGuar
     * @函数描述:房抵e点贷授信押品关联新增
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("")
    @PostMapping("/insertFdyddLmtSubGuar")
    protected ResultDto<Map> insertFdyddLmtSubGuar(@RequestBody FdyddLmtSubGuar fdyddLmtSubGuar) throws URISyntaxException {
        Map rtnData = fdyddLmtSubGuarService.insertFdyddLmtSubGuar(fdyddLmtSubGuar);
        return new ResultDto<>(rtnData);
    }

}
