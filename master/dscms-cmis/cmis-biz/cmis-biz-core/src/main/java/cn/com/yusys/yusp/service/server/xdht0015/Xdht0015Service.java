package cn.com.yusys.yusp.service.server.xdht0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrYddSignResult;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.req.GyypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.resp.GyypbhRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.req.Xddb05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.resp.Xddb05RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.req.Xddb06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.Xddb06RespDto;
import cn.com.yusys.yusp.dto.server.xdht0015.req.Xdht0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0015.resp.List;
import cn.com.yusys.yusp.dto.server.xdht0015.resp.Xdht0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrYddSignResultMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantInfoMapper;
import cn.com.yusys.yusp.service.Dscms2YphsxtClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称: 合同房产人员信息查询
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021/6/11 9:13
 * @修改备注:(交易码：khqy01 Khqy01Action)
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0015Service.class);
    @Resource
    private ICusClientService icusClientService;
    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private CtrYddSignResultMapper ctrYddSignResultMapper;
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Autowired
    private ICusClientService iCusClientService;

    /**
     * 合同房产人员信息查询
     *
     * @param xdht0015DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0015DataRespDto xdht0015Execute(Xdht0015DataReqDto xdht0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value);
        Xdht0015DataRespDto xdht0015DataRespDto = new Xdht0015DataRespDto();
        java.util.List<List> contStsInfoLists = new java.util.ArrayList<List>(0);
        try {
            // 身份证号码
            String certNo = xdht0015DataReqDto.getCertNo();
            // 产品码
            String prdCode = xdht0015DataReqDto.getPrdCode();
            if (StringUtils.isEmpty(certNo)) {
                throw BizException.error(null, EcbEnum.ECB010048.key, EcbEnum.ECB010048.value);
            }
            if (StringUtils.isEmpty(prdCode)) {
                throw BizException.error(null, EcbEnum.ECB010051.key, EcbEnum.ECB010051.value);
            }
            // 查询客户信息
            CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
            if (null == cusBaseClientDto) {
                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
            }
            // 客户号
            String cus_id = cusBaseClientDto.getCusId();
            // 借款人姓名
            String cus_name = cusBaseClientDto.getCusName();
            // 是否主借款人
            String is_main = "";
            // 根据客户号和业务类型，查询合同编号、担保合同号、押品编号、押品大类型
            Map queryMap = new HashMap();
            queryMap.put("cusId", cus_id);
            queryMap.put("prdType", prdCode);// 业务类型
            List ctrLoancontInfo = ctrLoanContMapper.selectCtrLoanContAndGrtGuarContByCustId(queryMap);
            logger.info("**********XDHT0015**查询优抵贷不见面抵押合同,返回参数为:{}", JSON.toJSONString(ctrLoancontInfo));
            if (ctrLoancontInfo == null) {//证件号查询对应合同信息结果为空
                ctrLoancontInfo = new cn.com.yusys.yusp.dto.server.xdht0015.resp.List();
                //throw BizException.error(null, EcbEnum.ECB010017.key, "查询不到相关合同信息！");
            }

            String cont_no = ctrLoancontInfo.getLoanContNo();// 合同编号
            String guar_cont_no = ctrLoancontInfo.getGrtContNo();// 担保合同编号
            String guar_no = ctrLoancontInfo.getMainClaimsPldContNo();// 押品编号
            String GUAR_TYPE_CD = ctrLoancontInfo.getRealEstateRegiAppbookNo();//押品类型
            String guar_cont_cn_no = ctrLoancontInfo.getCnGrtContNo();
            String cont_cn_no = ctrLoancontInfo.getCnLoanContNo();
            //4、查询权证台账信息，判断押品是否入库，如是入库状态，则不查询对应信息
            Map map = new HashMap();
            map.put("guarNo", guar_no);
            int count = guarWarrantInfoMapper.queryCountFromGuarWarrntInfoByGuarrNo(map);
            logger.info("**********XDHT0015**查询权证台账信息，判断押品是否入库，如是入库状态，则不查询对应信息,返回参数为:{}", JSON.toJSONString(count));
            //押品类型不能为空 且 如是入库状态，则不查询对应信息
            if (StringUtil.isNotEmpty(GUAR_TYPE_CD) && count == 0) {
                // 作为主借款人时，查询信息
                String guaranty_id = guar_no;
                //
//                if ("10001".equals(guar_type) || "10002".equals(guar_type) || "10003".equals(guar_type)) {
//                    guaranty_id = guar_no;
//                } else if ("10040".equals(guar_type)) {
//                    guaranty_id = guar_no + "-01";
//                } else {
//                    throw BizException.error(null, EcbEnum.ECB010055.key, EcbEnum.ECB010055.value);
//                }
                /**********************************新信贷直接调用xddb01接口，传入押品编号 guaranty_id 和押品类型 guar_type*****************************************/
                Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
                xddb01ReqDto.setGuaranty_id(guaranty_id);
                xddb01ReqDto.setType(GUAR_TYPE_CD);
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(xddb01ReqDto));
                ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
                String resultXddb01Code = Optional.ofNullable(resultXddb01RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String resultXddb01Meesage = Optional.ofNullable(resultXddb01RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                Xddb01RespDto xddb01RespDto = new Xddb01RespDto();
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultXddb01RespDto.getCode())) {
                    //获取相关的值并解析
                    xddb01RespDto = resultXddb01RespDto.getData();
                } else {
                    //抛出错误异常
                    throw BizException.error(null, resultXddb01Code, resultXddb01Meesage);
                }
                /*************************************************新信贷直接调用xddb01接口结束*********************************************************************/
                String area_code = xddb01RespDto.getCounty_cd();//区县地址
                if (StringUtil.isEmpty(area_code)) {//区县地址为空
                    throw BizException.error(null, EcbEnum.ECB010056.key, "该抵押物[" + guar_no + "]未查询到所在区域！");
                }

                String areaCode = area_code.substring(0, 4);
                if (!"3205".equals(areaCode)) {//非苏州地区抵押物
                    throw BizException.error(null, EcbEnum.ECB010057.key, "该抵押物[" + guar_no + "]非苏州地区抵押物！");
                }
                is_main = "Y";
                // 调用公共方法查询合同信息
                logger.info("**********XDHT0015**调用公共方法查询合同信息开始,参数为" + cont_cn_no + ";" + cont_no + ";" + cus_id + ";" + is_main);
                List contStsInfo = this.getContStsInfo(cont_cn_no, guar_cont_cn_no, cont_no, guar_cont_no, cus_id, certNo, cus_name, is_main);
                logger.info("**********XDHT0015**调用公共方法查询合同信息结束,返回参数为:{}", JSON.toJSONString(contStsInfo));
                if (null != contStsInfo) {
                    contStsInfoLists.add(contStsInfo);
                }
                // 查询是否存在共有人（存在共有则返回相应信息）
                // 调用押品系统xddb05接口，查询得到一个结果集，取common_assets_ind
                Xddb05ReqDto xddb05ReqDto = new Xddb05ReqDto();
                xddb05ReqDto.setGuaranty_id(guaranty_id);
                xddb05ReqDto.setType(GUAR_TYPE_CD);
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value, JSON.toJSONString(xddb05ReqDto));
                ResultDto<Xddb05RespDto> resultXddb05RespDto = dscms2YphsxtClientService.xddb05(xddb05ReqDto);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value, JSON.toJSONString(resultXddb05RespDto));
                String resultXddb05Code = Optional.ofNullable(resultXddb05RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String resultXddb05Meesage = Optional.ofNullable(resultXddb05RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                Xddb05RespDto xddb05RespDto = new Xddb05RespDto();
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultXddb05RespDto.getCode())) {
                    //获取相关的值并解析
                    xddb05RespDto = resultXddb05RespDto.getData();
                } else {
                    //抛出错误异常
                    throw BizException.error(null, resultXddb05Code, resultXddb05Meesage);
                }
                String common_assets_ind = xddb05RespDto.getCommon_assets_ind();
                if ("1".equals(common_assets_ind)) {
                    // 调用押品系统接口xddb06查询共有人信息，获取相关参数
                    Xddb06ReqDto xddb06ReqDto = new Xddb06ReqDto();
                    xddb06ReqDto.setGuaranty_id(guaranty_id);
                    logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(xddb06ReqDto));
                    ResultDto<Xddb06RespDto> resultXddb06RespDto = dscms2YphsxtClientService.xddb06(xddb06ReqDto);
                    logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(resultXddb06RespDto));
                    String resultXddb06Code = Optional.ofNullable(resultXddb06RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String resultXddb06Meesage = Optional.ofNullable(resultXddb06RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Xddb06RespDto xddb06RespDto = new Xddb06RespDto();
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultXddb06RespDto.getCode())) {
                        //获取相关的值并解析
                        xddb06RespDto = resultXddb06RespDto.getData();
                    } else {
                        throw BizException.error(null, resultXddb06Code, resultXddb06Meesage);
                    }
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List> xddb06RespDtoList = xddb06RespDto.getList();
                    for (int i = 0; i < xddb06RespDtoList.size(); i++) {
                        String common_cert_id = xddb06RespDtoList.get(i).getCommon_cert_code();
                        String common_cus_id = xddb06RespDtoList.get(i).getCommon_owner_id();
                        String common_cus_name = xddb06RespDtoList.get(i).getCommon_owner_name();
                        is_main = "N";
                        List contStsInfo2 = this.getContStsInfo(cont_cn_no, guar_cont_cn_no, cont_no, guar_cont_no, common_cus_id, common_cert_id, common_cus_name, is_main);
                        if (null != contStsInfo2) {
                            contStsInfoLists.add(contStsInfo2);
                        }
                    }
                }
            }
            // 以共有人身份查询 此时cert_no是共有人的证件号码
            //  调用押品系统交易gyypbh，根据共有人证件号码查询押品编号，返回的是一个列表
            //通过客户证件号查询客户信息
            logger.info("****************根据输入参数【{}】查询客户信息开始", JSON.toJSONString(certNo));
            CusBaseClientDto cusIndivClientDto = iCusClientService.queryCusByCertCode(certNo);
            logger.info("****************根据输入参数【{}】查询客户信息结果为【{}】", JSON.toJSONString(certNo), JSON.toJSONString(cusIndivClientDto));
            if (cusIndivClientDto != null && StringUtil.isNotEmpty(cusIndivClientDto.getCusId())) {
                String guarNo = "";//押品编号
                GyypbhReqDto gyypbhReqDto = new GyypbhReqDto();
                gyypbhReqDto.setCommon_owner_id(cusIndivClientDto.getCusId());//共有人客户编号
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value, JSON.toJSONString(gyypbhReqDto));
                ResultDto<GyypbhRespDto> resultDto = dscms2YphsxtClientService.gyypbh(gyypbhReqDto);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value, JSON.toJSONString(resultDto));
                GyypbhRespDto gyypbhRespDto = resultDto.getData();
                if (gyypbhRespDto != null) {//查询结果不为空
                    guarNo = gyypbhRespDto.getGuar_no();
                }
                map.put("guarNo", guar_no);
                int num = guarWarrantInfoMapper.queryCountFromGuarWarrntInfoByGuarrNo(map);
                logger.info("**********XDHT0015**查询权证台账信息，判断押品是否入库，如是入库状态，则不查询对应信息,返回参数为:{}", JSON.toJSONString(count));
                if (StringUtil.isNotEmpty(guarNo) && num == 0) {
                    GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.getGuarBaseInfoByGuarNo(guarNo);
                    if (null != guarBaseInfo) {
                        Map queryMap2 = new HashMap();
                        String borrow_cus_id = guarBaseInfo.getGuarCusId();// 主借款人客户号
                        queryMap2.put("cusId", borrow_cus_id);
                        queryMap2.put("prdType", prdCode);// 业务类型
                        List ctrLoancontInfo2 = ctrLoanContMapper.selectCtrLoanContAndGrtGuarContByCustId(queryMap2);
                        String borrow_cert_code = ""; // 主借款人身份证
                        String borrow_cus_name = ""; // 主借款人名称
                        if (ctrLoancontInfo2 != null) {//查询结果不为空
                            cont_no = ctrLoancontInfo2.getLoanContNo();
                            guar_cont_no = ctrLoancontInfo2.getGrtContNo();
                            guar_cont_cn_no = ctrLoancontInfo2.getCnGrtContNo();
                            cont_cn_no = ctrLoancontInfo2.getCnLoanContNo();
                            borrow_cert_code = ctrLoancontInfo2.getCertNo(); // 主借款人身份证
                            borrow_cus_name = ctrLoancontInfo2.getDebitName(); // 主借款人名称
                        }
                        // 共有人信息
                        is_main = "N";
                        // 调用公共方法查询合同信息
                        List contStsInfo = this.getContStsInfo(cont_cn_no, guar_cont_cn_no, cont_no, guar_cont_no, cus_id, certNo, cus_name, is_main);
                        if (null != contStsInfo) {
                            contStsInfoLists.add(contStsInfo);
                        }
                        // 主借款人查询合同签订信息
                        is_main = "Y";
                        List contStsInfo2 = this.getContStsInfo(cont_cn_no, guar_cont_cn_no, cont_no, guar_cont_no, borrow_cus_id, borrow_cert_code, borrow_cus_name, is_main);
                        if (null != contStsInfo2) {
                            contStsInfoLists.add(contStsInfo2);
                        }
                    }
                }
            }
            xdht0015DataRespDto.setList(contStsInfoLists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015DataRespDto));
        return xdht0015DataRespDto;
    }

    /**
     * 公共方法查询合同信息
     *
     * @return
     */
    private cn.com.yusys.yusp.dto.server.xdht0015.resp.List getContStsInfo(String cont_cn_no, String guar_cont_cn_no, String cont_no, String guar_cont_no,
                                                                           String cus_id, String certNo, String cus_name, String is_main) throws Exception {
        cn.com.yusys.yusp.dto.server.xdht0015.resp.List contStsInfo = new cn.com.yusys.yusp.dto.server.xdht0015.resp.List();
        try {
            String zq_contno = "";//主债权合同及抵押合同表编号
            String dj_contno = "";//不动产登记申请书编号
            String jk_status = "";// 借款合同签约状态
            String db_status = "";// 担保合同签约状态
            String zq_status = "";// 主债权合同及抵押合同表签约状态
            String dj_status = "";// 不动产登记申请书签约状态
            String zong_sign = ""; // 总签约状态
            logger.info("**********XDHT0015**查询优抵贷不见面抵押签约信息表开始,参数为证件号:{};合同号:{};担保合同号:{}", certNo, cont_no, guar_cont_no);
            CtrYddSignResult ctrYddSignResult = ctrYddSignResultMapper.selectByPrimaryKey(certNo, cont_no, guar_cont_no);
            logger.info("**********XDHT0015**查询优抵贷不见面抵押签约信息表结束,返回参数为:{}", JSON.toJSONString(ctrYddSignResult));
            if (ctrYddSignResult != null) {
                jk_status = ctrYddSignResult.getLoanContSignStatus();// 借款合同签约状态
                db_status = ctrYddSignResult.getGuarContSignStatus();// 担保合同签约状态
                zq_status = ctrYddSignResult.getClaimContSignStatus();// 主债权合同及抵押合同表签约状态
                dj_status = ctrYddSignResult.getRegistAppSignStatus();// 不动产登记申请书签约状态
                zq_contno = ctrYddSignResult.getClaimContNo();// 主债权合同及抵押合同表编号
                dj_contno = ctrYddSignResult.getRegistAppNo();//不动产登记申请书编号
                zong_sign = ""; // 总签约状态
                if ("Y".equals(is_main)) {
                    if ("1".equals(jk_status) && "1".equals(db_status) && "1".equals(zq_status) && "1".equals(dj_status)) {
                        zong_sign = "1";
                    } else {
                        zong_sign = "0";
                    }
                } else if ("N".equals(is_main)) {
                    if ("1".equals(db_status) && "1".equals(zq_status) && "1".equals(dj_status)) {
                        zong_sign = "1";
                    } else {
                        zong_sign = "0";
                    }
                }
            }
            contStsInfo.setLoanContNo(cont_no);
            contStsInfo.setCnLoanContNo(cont_no);
            contStsInfo.setCnGrtContNo(guar_cont_no);
            contStsInfo.setGrtContNo(guar_cont_no);
            contStsInfo.setMainClaimsPldContNo(zq_contno);
            contStsInfo.setRealEstateRegiAppbookNo(dj_contno);
            contStsInfo.setCertNo(certNo);
            contStsInfo.setDebitName(cus_name);
            contStsInfo.setIsMborrow(is_main);
            contStsInfo.setLoanContSignStatus(jk_status);
            contStsInfo.setGrtContSignStatus(db_status);
            contStsInfo.setMainClaimsPldContSignStatus(zq_status);
            contStsInfo.setRealEstateRegiAppbookSignStatus(dj_status);
            contStsInfo.setTotlSignStatus(zong_sign);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            throw new Exception("查询业务信息异常");
        }
        return contStsInfo;
    }
}
