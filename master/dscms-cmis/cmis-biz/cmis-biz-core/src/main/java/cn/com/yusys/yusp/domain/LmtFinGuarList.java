/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarList
 * @类描述: lmt_fin_guar_list数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fin_guar_list")
public class LmtFinGuarList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 共享范围 STD_ZB_SHR_SCOPE **/
	@Column(name = "shared_scope", unique = false, nullable = true, length = 5)
	private String sharedScope;
	
	/** 产品编号 **/
	@Column(name = "prd_id", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 总限额 **/
	@Column(name = "totl_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totlAmt;
	
	/** 单户限额 **/
	@Column(name = "sig_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigAmt;
	
	/** 单笔限额 **/
	@Column(name = "one_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oneAmt;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 50)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 30)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 30)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 30)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 30)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 30)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param sharedScope
	 */
	public void setSharedScope(String sharedScope) {
		this.sharedScope = sharedScope;
	}
	
    /**
     * @return sharedScope
     */
	public String getSharedScope() {
		return this.sharedScope;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param totlAmt
	 */
	public void setTotlAmt(java.math.BigDecimal totlAmt) {
		this.totlAmt = totlAmt;
	}
	
    /**
     * @return totlAmt
     */
	public java.math.BigDecimal getTotlAmt() {
		return this.totlAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return sigAmt
     */
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return oneAmt
     */
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}