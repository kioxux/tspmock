package cn.com.yusys.yusp.web.server.xdcz0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0018.req.Xdcz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.resp.Xdcz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0018.Xdcz0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:家速贷、极速快贷贷款申请
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0018:家速贷、极速快贷贷款申请")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0018Resource.class);

    @Autowired
    private Xdcz0018Service xdcz0018Service;

    /**
     * 交易码：xdcz0018
     * 交易描述：家速贷、极速快贷贷款申请
     *
     * @param xdcz0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("家速贷、极速快贷贷款申请")
    @PostMapping("/xdcz0018")
    protected @ResponseBody
    ResultDto<Xdcz0018DataRespDto> xdcz0018(@Validated @RequestBody Xdcz0018DataReqDto xdcz0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataReqDto));
        Xdcz0018DataRespDto xdcz0018DataRespDto = new Xdcz0018DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0018DataRespDto> xdcz0018DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0018DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataReqDto));
            xdcz0018DataRespDto = xdcz0018Service.xdcz0018(xdcz0018DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataRespDto));
            // 封装xdcz0018DataResultDto中正确的返回码和返回信息
            xdcz0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, e.getMessage());
            // 封装xdcz0018DataResultDto中异常返回码和返回信息
            xdcz0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0018DataRespDto到xdcz0018DataResultDto中
        xdcz0018DataResultDto.setData(xdcz0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataRespDto));
        return xdcz0018DataResultDto;
    }
}
