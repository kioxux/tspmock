package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class GuarBaseInfoRelDto implements Serializable {
    /**
     * 业务流水号
     **/
    private String serno;

    /**
     * 押品统一编号
     **/
    private String guarNo;

    /**
     * 押品标识
     **/
    private String grtFlag;

    /**
     * 抵质押物名称（抵质押物类型名称）
     **/
    private String pldimnMemo;

    /**
     * 担保分类代码
     **/
    private String guarTypeCd;

    /**
     * 抵质押分类（大类）STD_GAGE_TYPE
     **/
    private String guarType;

    /**
     * 押品初次登记时间
     **/
    private String guarFirstCreateTime;

    /**
     * 借款人
     **/
    private String cusId;

    /**
     * 押品所有人编号
     **/
    private String guarCusId;

    /**
     * 押品所有人名称
     **/
    private String guarCusName;

    /**
     * 押品所有人类型 STD_CUS_TYPE
     **/
    private String guarCusType;

    /**
     * 押品所有人证件类型 STD_CERT_TYPE
     **/
    private String guarCertType;

    /**
     * 押品所有人证件号码
     **/
    private String guarCertCode;

    /**
     * 押品所有人贷款卡号(抵押)  或 出质人贷款卡号(质押)
     **/
    private String assureCardNo;

    /**
     * 是否实质正相关  STD_YES_NO
     **/
    private String relationInt;

    /**
     * 是否共有财产  STD_YES_NO
     **/
    private String commonAssetsInd;

    /**
     * 押品所有人所占份额
     **/
    private String occupyofowner;

    /**
     * 是否需要办理保险 STD_YES_NO
     **/
    private String insuranceInd;

    /**
     * 是否需要办理公证 STD_YES_NO
     **/
    private String notarizationInd;

    /**
     * 人工认定结果 STD_FORCECREATE_IND
     **/
    private String forcecreateInd;

    /**
     * 强制创建理由
     **/
    private String forcecreateReason;

    /**
     * 押品所在业务阶段 STD_GUAR_BUSISTATE
     **/
    private String guarBusistate;

    /**
     * 押品状态 STD_GUAR_STATE
     **/
    private String guarState;

    /**
     * 是否权属清晰  STD_YES_NO
     **/
    private String isOwnershipClear;

    /**
     * 是否抵债资产  STD_YES_NO
     **/
    private String isDebtAsset;

    /**
     * 法律规定禁止流通财产  STD_YES_NO
     **/
    private String forbidCirBelogFlag;

    /**
     * 是否被查封、扣押或监管  STD_YES_NO
     **/
    private String supervisionFlag;

    /**
     * 查封时间
     **/
    private String supervisionDate;

    /**
     * 是否有强行执行条款  STD_YES_NO
     **/
    private String enforeFlag;

    /**
     * 抵权证编号及其他编号
     **/
    private String rightOtherNo;

    /**
     * 押品登记办理状态 STD_REG_STATE
     **/
    private String regState;

    /**
     * 担保权生效方式 STD_DEF_EFFECT_TYPE
     **/
    private String defEffectType;

    /**
     * 他行是否已设定担保权  STD_YES_NO
     **/
    private String otherBackGuarInd;

    /**
     * 抵质押物与借款人相关性
     **/
    private String pldimnDebitRelative;

    /**
     * 查封便利性
     **/
    private String supervisionConvenience;

    /**
     * 法律有效性
     **/
    private String lawValidity;

    /**
     * 抵质押品通用性
     **/
    private String pldimnCommon;

    /**
     * 抵质押品变现能力
     **/
    private String pldimnCashability;

    /**
     * 价格波动性
     **/
    private String priceWave;

    /**
     * 是否扫描资料
     **/
    private String isScanMater;

    /**
     * 权证编号及其他编号
     **/
    private String ringhtNoAndOtherNo;

    /**
     * 我行担保权受偿顺序
     **/
    private String mybackGuarFirstSeq;

    /**
     * 法定优先受偿款(元)
     **/
    private BigDecimal legalPriPayment;

    /**
     * 创建系统/来源系统 STD_DATA_SOURCE
     **/
    private String createSys;

    /**
     * 备注
     **/
    private String remark;

    /**
     * 管户人
     **/
    private String accountManager;

    /**
     * 押品评估价值
     **/
    private BigDecimal evalAmt;

    /**
     * 评估日期
     **/
    private String evalDate;

    /**
     * 押品认定价值
     **/
    private BigDecimal confirmAmt;

    /**
     * 币种 STD_CUR_TYPE
     **/
    private String curType;

    /**
     * 权证凭证号
     **/
    private String certiRecordId;

    /**
     * 权证到期日
     **/
    private String certiEndDate;

    /**
     * 最高抵押率（%）
     **/
    private BigDecimal mortagageMaxRate;

    /**
     * 设定抵押率（%）
     **/
    private BigDecimal mortagageRate;

    /**
     * 最高可抵质押金额（元）
     **/
    private BigDecimal maxMortagageAmt;

    /**
     * 抵押物存放地点
     **/
    private String pldLocation;

    /**
     * 共有权人名称
     **/
    private String refName;

    /**
     * 是否权益争议STD_ZB_YES_NO
     **/
    private String isDisputed;

    /**
     * 核心担保品编号
     **/
    private String coreGuarantyNo;

    /**
     * 核心担保品序号
     **/
    private String coreGuarantySeq;

    /**
     * 账务机构
     **/
    private String finaBrId;

    /**
     * 认定日期
     **/
    private String confirmDate;

    /**
     * 评估方式 STD_EVAL_TYPE
     **/
    private String evalType;

    /**
     * 评估机构
     **/
    private String evalOrg;

    /**
     * 评估机构组织机构代码
     **/
    private String evalOrgInsCode;

    /**
     * 购入金额
     **/
    private BigDecimal buyAmt;

    /**
     * 内部评估确认金额（元）
     **/
    private BigDecimal innerEvalAmt;

    /**
     * 下次估值到期日期
     **/
    private String nextEvalEndDate;

    /**
     * 下次估值日期
     **/
    private String nextEvalDate;

    /**
     * 是否资产保全 STD_ZB_YES_NO
     **/
    private String isSpecial;

    /**
     * 是否保全资产STD_ZB_YES_NO
     **/
    private String isSpecialAsset;

    /**
     * 是否成品房 STD_ZB_YES_NO
     **/
    private String isExistinghome;

    /**
     * 是否需要修改 STD_ZB_YES_NO
     **/
    private String isUpdate;

    /**
     * 授信评估金额
     **/
    private BigDecimal lmtEvalAmt;

    /**
     * 所在区域编号
     **/
    private String areaCode;

    /**
     * 所在区域名称
     **/
    private String areaName;

    /**
     * 复制来源
     **/
    private String copyFrom;

    /**
     * 是否变化 STD_ZB_YES_NO
     **/
    private String isChanged;

    /**
     * 出入库流水号
     **/
    private String inoutSerno;

    /**
     * 入库人编号
     **/
    private String inUser;

    /**
     * 从属类型  STD_RIGHT_TYPE
     **/
    private String rightType;

    /**
     * 权属证件类型 STD_RIGHT_CERT_TYPE_CODE
     **/
    private String rightCertTypeCode;

    /**
     * 权属证件号
     **/
    private String rightCertNo;

    /**
     * 权属登记机关
     **/
    private String rightOrg;

    /**
     * 他项权证号到期日
     **/
    private String registerEndDate;

    /**
     * 来源渠道
     **/
    private String sourcePath;

    /**
     * 创建人用户编号
     **/
    private String createUserId;

    /**
     * 登记证明编号/止付通知书编号
     **/
    private String registerNo;

    /**
     * 抵押登记机关
     **/
    private String registerOrg;

    /**
     * 登记/止付日期
     **/
    private String registerDate;

    /**
     * 投保险种
     **/
    private String assuranceType;

    /**
     * 保单编号
     **/
    private String assuranceNo;

    /**
     * 保险金额（元）
     **/
    private BigDecimal assuranceAmt;

    /**
     * 投保日期
     **/
    private String assuranceDate;

    /**
     * 保险到期日
     **/
    private String assuranceEndDate;

    /**
     * 保险公司名称
     **/
    private String assuranceComName;

    /**
     * 租赁情况  STD_TENANCY_CIRCE
     **/
    private String tenancyCirce;

    /**
     * 租赁到期日期
     **/
    private String tenancyEndDate;

    /**
     * 年租金（元）
     **/
    private BigDecimal tenancyAmt;

    /**
     * 保管人
     **/
    private String keepUser;

    /**
     * 申请入库时间
     **/
    private String appInDate;

    /**
     * 入库时间
     **/
    private String inDate;

    /**
     * 申请出库时间
     **/
    private String appOutDate;

    /**
     * 出库时间
     **/
    private String outDate;

    /**
     * 出库事由
     **/
    private String outReason;

    /**
     * 新押品编码
     **/
    private String newcode;

    /**
     * 新押品编码名称
     **/
    private String newlabel;

    /**
     * 质押特定类型
     **/
    private String style;

    /**
     * 外部评级机构
     **/
    private String outerLevelOrg;

    /**
     * 外部评级等级
     **/
    private String outerLevel;

    /**
     * 是否到期 STD_ZB_YES_NO
     **/
    private String isEndDate;

    /**
     * 是否票据池 STD_ZB_YES_NO
     **/
    private String isPjc;

    /**
     * 票据池状态 STD_PJC_STATUS
     **/
    private String rcState;

    /**
     * 票据入池时质押率
     **/
    private BigDecimal pjcZyl;

    /**
     * 入池日期
     **/
    private String rcDate;

    /**
     * 出池日期
     **/
    private String ccDate;

    /**
     * 票据池合同编号
     **/
    private String pjcContNo;

    /**
     * 车架号
     **/
    private String vehicleFlagCd;

    /**
     * 审批状态STD_APP_ST
     **/
    private String approveStatus;

    /**
     * 责任人
     **/
    private String managerId;

    /**
     * 责任机构
     **/
    private String managerBrId;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 操作类型  STD_OPR_TYPE
     **/
    private String oprType;

    /**
     * 权重法下变现能力/代偿能力要求 STD_ZB_SALE_STATE
     **/
    private String pctAbility;
    /**
     * 初级内部评级法下变现能力/代偿能力要求 STD_ZB_SALE_STATE
     **/
    private String innerAbility;

    /**
     * 押品系统跳转调用页面
     **/
    private String callMethod;

    /**
     * 担保合同借款人编号,保证人调整押品系统时用
     **/
    private String borrowNo;

    /**
     * 权证编号,抵押注销选押品时用
     **/
    private String warrantNo;

    private BigDecimal correFinAmt;

    private String squ;


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getGrtFlag() {
        return grtFlag;
    }

    public void setGrtFlag(String grtFlag) {
        this.grtFlag = grtFlag;
    }

    public String getPldimnMemo() {
        return pldimnMemo;
    }

    public void setPldimnMemo(String pldimnMemo) {
        this.pldimnMemo = pldimnMemo;
    }

    public String getGuarTypeCd() {
        return guarTypeCd;
    }

    public void setGuarTypeCd(String guarTypeCd) {
        this.guarTypeCd = guarTypeCd;
    }

    public String getGuarType() {
        return guarType;
    }

    public void setGuarType(String guarType) {
        this.guarType = guarType;
    }

    public String getGuarFirstCreateTime() {
        return guarFirstCreateTime;
    }

    public void setGuarFirstCreateTime(String guarFirstCreateTime) {
        this.guarFirstCreateTime = guarFirstCreateTime;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getGuarCusId() {
        return guarCusId;
    }

    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public String getGuarCusType() {
        return guarCusType;
    }

    public void setGuarCusType(String guarCusType) {
        this.guarCusType = guarCusType;
    }

    public String getGuarCertType() {
        return guarCertType;
    }

    public void setGuarCertType(String guarCertType) {
        this.guarCertType = guarCertType;
    }

    public String getGuarCertCode() {
        return guarCertCode;
    }

    public void setGuarCertCode(String guarCertCode) {
        this.guarCertCode = guarCertCode;
    }

    public String getAssureCardNo() {
        return assureCardNo;
    }

    public void setAssureCardNo(String assureCardNo) {
        this.assureCardNo = assureCardNo;
    }

    public String getRelationInt() {
        return relationInt;
    }

    public void setRelationInt(String relationInt) {
        this.relationInt = relationInt;
    }

    public String getCommonAssetsInd() {
        return commonAssetsInd;
    }

    public void setCommonAssetsInd(String commonAssetsInd) {
        this.commonAssetsInd = commonAssetsInd;
    }

    public String getOccupyofowner() {
        return occupyofowner;
    }

    public void setOccupyofowner(String occupyofowner) {
        this.occupyofowner = occupyofowner;
    }

    public String getInsuranceInd() {
        return insuranceInd;
    }

    public void setInsuranceInd(String insuranceInd) {
        this.insuranceInd = insuranceInd;
    }

    public String getNotarizationInd() {
        return notarizationInd;
    }

    public void setNotarizationInd(String notarizationInd) {
        this.notarizationInd = notarizationInd;
    }

    public String getForcecreateInd() {
        return forcecreateInd;
    }

    public void setForcecreateInd(String forcecreateInd) {
        this.forcecreateInd = forcecreateInd;
    }

    public String getForcecreateReason() {
        return forcecreateReason;
    }

    public void setForcecreateReason(String forcecreateReason) {
        this.forcecreateReason = forcecreateReason;
    }

    public String getGuarBusistate() {
        return guarBusistate;
    }

    public void setGuarBusistate(String guarBusistate) {
        this.guarBusistate = guarBusistate;
    }

    public String getGuarState() {
        return guarState;
    }

    public void setGuarState(String guarState) {
        this.guarState = guarState;
    }

    public String getIsOwnershipClear() {
        return isOwnershipClear;
    }

    public void setIsOwnershipClear(String isOwnershipClear) {
        this.isOwnershipClear = isOwnershipClear;
    }

    public String getIsDebtAsset() {
        return isDebtAsset;
    }

    public void setIsDebtAsset(String isDebtAsset) {
        this.isDebtAsset = isDebtAsset;
    }

    public String getForbidCirBelogFlag() {
        return forbidCirBelogFlag;
    }

    public void setForbidCirBelogFlag(String forbidCirBelogFlag) {
        this.forbidCirBelogFlag = forbidCirBelogFlag;
    }

    public String getSupervisionFlag() {
        return supervisionFlag;
    }

    public void setSupervisionFlag(String supervisionFlag) {
        this.supervisionFlag = supervisionFlag;
    }

    public String getSupervisionDate() {
        return supervisionDate;
    }

    public void setSupervisionDate(String supervisionDate) {
        this.supervisionDate = supervisionDate;
    }

    public String getEnforeFlag() {
        return enforeFlag;
    }

    public void setEnforeFlag(String enforeFlag) {
        this.enforeFlag = enforeFlag;
    }

    public String getRightOtherNo() {
        return rightOtherNo;
    }

    public void setRightOtherNo(String rightOtherNo) {
        this.rightOtherNo = rightOtherNo;
    }

    public String getRegState() {
        return regState;
    }

    public void setRegState(String regState) {
        this.regState = regState;
    }

    public String getDefEffectType() {
        return defEffectType;
    }

    public void setDefEffectType(String defEffectType) {
        this.defEffectType = defEffectType;
    }

    public String getOtherBackGuarInd() {
        return otherBackGuarInd;
    }

    public void setOtherBackGuarInd(String otherBackGuarInd) {
        this.otherBackGuarInd = otherBackGuarInd;
    }

    public String getPldimnDebitRelative() {
        return pldimnDebitRelative;
    }

    public void setPldimnDebitRelative(String pldimnDebitRelative) {
        this.pldimnDebitRelative = pldimnDebitRelative;
    }

    public String getSupervisionConvenience() {
        return supervisionConvenience;
    }

    public void setSupervisionConvenience(String supervisionConvenience) {
        this.supervisionConvenience = supervisionConvenience;
    }

    public String getLawValidity() {
        return lawValidity;
    }

    public void setLawValidity(String lawValidity) {
        this.lawValidity = lawValidity;
    }

    public String getPldimnCommon() {
        return pldimnCommon;
    }

    public void setPldimnCommon(String pldimnCommon) {
        this.pldimnCommon = pldimnCommon;
    }

    public String getPldimnCashability() {
        return pldimnCashability;
    }

    public void setPldimnCashability(String pldimnCashability) {
        this.pldimnCashability = pldimnCashability;
    }

    public String getPriceWave() {
        return priceWave;
    }

    public void setPriceWave(String priceWave) {
        this.priceWave = priceWave;
    }

    public String getIsScanMater() {
        return isScanMater;
    }

    public void setIsScanMater(String isScanMater) {
        this.isScanMater = isScanMater;
    }

    public String getRinghtNoAndOtherNo() {
        return ringhtNoAndOtherNo;
    }

    public void setRinghtNoAndOtherNo(String ringhtNoAndOtherNo) {
        this.ringhtNoAndOtherNo = ringhtNoAndOtherNo;
    }

    public String getMybackGuarFirstSeq() {
        return mybackGuarFirstSeq;
    }

    public void setMybackGuarFirstSeq(String mybackGuarFirstSeq) {
        this.mybackGuarFirstSeq = mybackGuarFirstSeq;
    }

    public BigDecimal getLegalPriPayment() {
        return legalPriPayment;
    }

    public void setLegalPriPayment(BigDecimal legalPriPayment) {
        this.legalPriPayment = legalPriPayment;
    }

    public String getCreateSys() {
        return createSys;
    }

    public void setCreateSys(String createSys) {
        this.createSys = createSys;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public BigDecimal getEvalAmt() {
        return evalAmt;
    }

    public void setEvalAmt(BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    public String getEvalDate() {
        return evalDate;
    }

    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate;
    }

    public BigDecimal getConfirmAmt() {
        return confirmAmt;
    }

    public void setConfirmAmt(BigDecimal confirmAmt) {
        this.confirmAmt = confirmAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getCertiRecordId() {
        return certiRecordId;
    }

    public void setCertiRecordId(String certiRecordId) {
        this.certiRecordId = certiRecordId;
    }

    public String getCertiEndDate() {
        return certiEndDate;
    }

    public void setCertiEndDate(String certiEndDate) {
        this.certiEndDate = certiEndDate;
    }

    public BigDecimal getMortagageMaxRate() {
        return mortagageMaxRate;
    }

    public void setMortagageMaxRate(BigDecimal mortagageMaxRate) {
        this.mortagageMaxRate = mortagageMaxRate;
    }

    public BigDecimal getMortagageRate() {
        return mortagageRate;
    }

    public void setMortagageRate(BigDecimal mortagageRate) {
        this.mortagageRate = mortagageRate;
    }

    public BigDecimal getMaxMortagageAmt() {
        return maxMortagageAmt;
    }

    public void setMaxMortagageAmt(BigDecimal maxMortagageAmt) {
        this.maxMortagageAmt = maxMortagageAmt;
    }

    public String getPldLocation() {
        return pldLocation;
    }

    public void setPldLocation(String pldLocation) {
        this.pldLocation = pldLocation;
    }

    public String getRefName() {
        return refName;
    }

    public void setRefName(String refName) {
        this.refName = refName;
    }

    public String getIsDisputed() {
        return isDisputed;
    }

    public void setIsDisputed(String isDisputed) {
        this.isDisputed = isDisputed;
    }

    public String getCoreGuarantyNo() {
        return coreGuarantyNo;
    }

    public void setCoreGuarantyNo(String coreGuarantyNo) {
        this.coreGuarantyNo = coreGuarantyNo;
    }

    public String getCoreGuarantySeq() {
        return coreGuarantySeq;
    }

    public void setCoreGuarantySeq(String coreGuarantySeq) {
        this.coreGuarantySeq = coreGuarantySeq;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(String confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getEvalType() {
        return evalType;
    }

    public void setEvalType(String evalType) {
        this.evalType = evalType;
    }

    public String getEvalOrg() {
        return evalOrg;
    }

    public void setEvalOrg(String evalOrg) {
        this.evalOrg = evalOrg;
    }

    public String getEvalOrgInsCode() {
        return evalOrgInsCode;
    }

    public void setEvalOrgInsCode(String evalOrgInsCode) {
        this.evalOrgInsCode = evalOrgInsCode;
    }

    public BigDecimal getBuyAmt() {
        return buyAmt;
    }

    public void setBuyAmt(BigDecimal buyAmt) {
        this.buyAmt = buyAmt;
    }

    public BigDecimal getInnerEvalAmt() {
        return innerEvalAmt;
    }

    public void setInnerEvalAmt(BigDecimal innerEvalAmt) {
        this.innerEvalAmt = innerEvalAmt;
    }

    public String getNextEvalEndDate() {
        return nextEvalEndDate;
    }

    public void setNextEvalEndDate(String nextEvalEndDate) {
        this.nextEvalEndDate = nextEvalEndDate;
    }

    public String getNextEvalDate() {
        return nextEvalDate;
    }

    public void setNextEvalDate(String nextEvalDate) {
        this.nextEvalDate = nextEvalDate;
    }

    public String getIsSpecial() {
        return isSpecial;
    }

    public void setIsSpecial(String isSpecial) {
        this.isSpecial = isSpecial;
    }

    public String getIsSpecialAsset() {
        return isSpecialAsset;
    }

    public void setIsSpecialAsset(String isSpecialAsset) {
        this.isSpecialAsset = isSpecialAsset;
    }

    public String getIsExistinghome() {
        return isExistinghome;
    }

    public void setIsExistinghome(String isExistinghome) {
        this.isExistinghome = isExistinghome;
    }

    public String getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(String isUpdate) {
        this.isUpdate = isUpdate;
    }

    public BigDecimal getLmtEvalAmt() {
        return lmtEvalAmt;
    }

    public void setLmtEvalAmt(BigDecimal lmtEvalAmt) {
        this.lmtEvalAmt = lmtEvalAmt;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCopyFrom() {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom) {
        this.copyFrom = copyFrom;
    }

    public String getIsChanged() {
        return isChanged;
    }

    public void setIsChanged(String isChanged) {
        this.isChanged = isChanged;
    }

    public String getInoutSerno() {
        return inoutSerno;
    }

    public void setInoutSerno(String inoutSerno) {
        this.inoutSerno = inoutSerno;
    }

    public String getInUser() {
        return inUser;
    }

    public void setInUser(String inUser) {
        this.inUser = inUser;
    }

    public String getRightType() {
        return rightType;
    }

    public void setRightType(String rightType) {
        this.rightType = rightType;
    }

    public String getRightCertTypeCode() {
        return rightCertTypeCode;
    }

    public void setRightCertTypeCode(String rightCertTypeCode) {
        this.rightCertTypeCode = rightCertTypeCode;
    }

    public String getRightCertNo() {
        return rightCertNo;
    }

    public void setRightCertNo(String rightCertNo) {
        this.rightCertNo = rightCertNo;
    }

    public String getRightOrg() {
        return rightOrg;
    }

    public void setRightOrg(String rightOrg) {
        this.rightOrg = rightOrg;
    }

    public String getRegisterEndDate() {
        return registerEndDate;
    }

    public void setRegisterEndDate(String registerEndDate) {
        this.registerEndDate = registerEndDate;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getRegisterOrg() {
        return registerOrg;
    }

    public void setRegisterOrg(String registerOrg) {
        this.registerOrg = registerOrg;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getAssuranceType() {
        return assuranceType;
    }

    public void setAssuranceType(String assuranceType) {
        this.assuranceType = assuranceType;
    }

    public String getAssuranceNo() {
        return assuranceNo;
    }

    public void setAssuranceNo(String assuranceNo) {
        this.assuranceNo = assuranceNo;
    }

    public BigDecimal getAssuranceAmt() {
        return assuranceAmt;
    }

    public void setAssuranceAmt(BigDecimal assuranceAmt) {
        this.assuranceAmt = assuranceAmt;
    }

    public String getAssuranceDate() {
        return assuranceDate;
    }

    public void setAssuranceDate(String assuranceDate) {
        this.assuranceDate = assuranceDate;
    }

    public String getAssuranceEndDate() {
        return assuranceEndDate;
    }

    public void setAssuranceEndDate(String assuranceEndDate) {
        this.assuranceEndDate = assuranceEndDate;
    }

    public String getAssuranceComName() {
        return assuranceComName;
    }

    public void setAssuranceComName(String assuranceComName) {
        this.assuranceComName = assuranceComName;
    }

    public String getTenancyCirce() {
        return tenancyCirce;
    }

    public void setTenancyCirce(String tenancyCirce) {
        this.tenancyCirce = tenancyCirce;
    }

    public String getTenancyEndDate() {
        return tenancyEndDate;
    }

    public void setTenancyEndDate(String tenancyEndDate) {
        this.tenancyEndDate = tenancyEndDate;
    }

    public BigDecimal getTenancyAmt() {
        return tenancyAmt;
    }

    public void setTenancyAmt(BigDecimal tenancyAmt) {
        this.tenancyAmt = tenancyAmt;
    }

    public String getKeepUser() {
        return keepUser;
    }

    public void setKeepUser(String keepUser) {
        this.keepUser = keepUser;
    }

    public String getAppInDate() {
        return appInDate;
    }

    public void setAppInDate(String appInDate) {
        this.appInDate = appInDate;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public String getAppOutDate() {
        return appOutDate;
    }

    public void setAppOutDate(String appOutDate) {
        this.appOutDate = appOutDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getOutReason() {
        return outReason;
    }

    public void setOutReason(String outReason) {
        this.outReason = outReason;
    }

    public String getNewcode() {
        return newcode;
    }

    public void setNewcode(String newcode) {
        this.newcode = newcode;
    }

    public String getNewlabel() {
        return newlabel;
    }

    public void setNewlabel(String newlabel) {
        this.newlabel = newlabel;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getOuterLevelOrg() {
        return outerLevelOrg;
    }

    public void setOuterLevelOrg(String outerLevelOrg) {
        this.outerLevelOrg = outerLevelOrg;
    }

    public String getOuterLevel() {
        return outerLevel;
    }

    public void setOuterLevel(String outerLevel) {
        this.outerLevel = outerLevel;
    }

    public String getIsEndDate() {
        return isEndDate;
    }

    public void setIsEndDate(String isEndDate) {
        this.isEndDate = isEndDate;
    }

    public String getIsPjc() {
        return isPjc;
    }

    public void setIsPjc(String isPjc) {
        this.isPjc = isPjc;
    }

    public String getRcState() {
        return rcState;
    }

    public void setRcState(String rcState) {
        this.rcState = rcState;
    }

    public BigDecimal getPjcZyl() {
        return pjcZyl;
    }

    public void setPjcZyl(BigDecimal pjcZyl) {
        this.pjcZyl = pjcZyl;
    }

    public String getRcDate() {
        return rcDate;
    }

    public void setRcDate(String rcDate) {
        this.rcDate = rcDate;
    }

    public String getCcDate() {
        return ccDate;
    }

    public void setCcDate(String ccDate) {
        this.ccDate = ccDate;
    }

    public String getPjcContNo() {
        return pjcContNo;
    }

    public void setPjcContNo(String pjcContNo) {
        this.pjcContNo = pjcContNo;
    }

    public String getVehicleFlagCd() {
        return vehicleFlagCd;
    }

    public void setVehicleFlagCd(String vehicleFlagCd) {
        this.vehicleFlagCd = vehicleFlagCd;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getPctAbility() {
        return pctAbility;
    }

    public void setPctAbility(String pctAbility) {
        this.pctAbility = pctAbility;
    }

    public String getInnerAbility() {
        return innerAbility;
    }

    public void setInnerAbility(String innerAbility) {
        this.innerAbility = innerAbility;
    }

    public String getCallMethod() {
        return callMethod;
    }

    public void setCallMethod(String callMethod) {
        this.callMethod = callMethod;
    }

    public String getBorrowNo() {
        return borrowNo;
    }

    public void setBorrowNo(String borrowNo) {
        this.borrowNo = borrowNo;
    }

    public String getWarrantNo() {
        return warrantNo;
    }

    public void setWarrantNo(String warrantNo) {
        this.warrantNo = warrantNo;
    }

    public BigDecimal getCorreFinAmt() {
        return correFinAmt;
    }

    public void setCorreFinAmt(BigDecimal correFinAmt) {
        this.correFinAmt = correFinAmt;
    }

    public String getSqu() {
        return squ;
    }

    public void setSqu(String squ) {
        this.squ = squ;
    }
}
