package cn.com.yusys.yusp.service.server.xdsx0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.req.Xdsx0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.resp.Xdsx0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:授信作废（房抵e点贷）
 *
 * @author xs
 * @version 1.0
 */
@Service
@Transactional(rollbackFor = {BizException.class, Exception.class})
public class Xdsx0014Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0014Service.class);

    @Resource
    private CommonService commonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private LmtAppMapper lmtAppMapper;//授信申请主表
    @Autowired
    private LmtAppSubMapper lmtAppSubMapper;//授信分项表
    @Autowired
    private LmtAppSubPrdMapper lmtAppSubPrdMapper;//授信分项产品表
    @Autowired
    private LmtReplyAccMapper lmtReplyAccMapper;//授信批复台账表
    @Autowired
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;//授信批复台账明细表
    @Autowired
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;//授信批复分项产品表
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;//押品信息创建base
    @Autowired
    private GuarBizRelMapper guarBizRelMapper;//授信与押品关系表
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;//贷款申请主表
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;//贷款申请主表
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;//担保合同表
    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;//贷款申请与担保合同关系表
    @Autowired
    private GrtGuarContRelMapper grtGuarContRelMapper;//担保合同与押品关系表
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;
    @Autowired
    private CusHouseInfoMapper cusHouseInfoMapper;
    @Autowired
    private EntlQyInfoMapper entlQyInfoMapper;

    @Autowired
    private LmtReplyMapper lmtReplyMapper;

    @Autowired
    private LmtReplySubMapper lmtReplySubMapper;

    @Autowired
    private LmtReplySubPrdMapper lmtReplySubPrdMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 1，授信：
     * 授信申请主表     LMT_APP
     * 授信分项表       LMT_APP_SUB
     * 授信分项产品表   LMT_APP_SUB_PRD
     * 授信批复台账表   LMT_REPLY_ACC
     * 授信批复台账明细表   LMT_REPLY_ACC_SUB
     * 授信批复台账分项产品表 LMT_REPLY_ACC_SUB_PRD
     * 押品信息创建base  GUAR_BASE_INFO
     * 授信与押品关系表  GUAR_BIZ_REL
     * 2，用信
     * 贷款申请主表  IQP_LOAN_APP
     * 担保合同表	GRT_GUAR_CONT
     * 贷款申请与担保合同关系表  GRT_GUAR_BIZ_RST_REL
     * 担保合同与押品关系表      GRT_GUAR_CONT_REL
     *
     * @param xdsx0014DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0014DataRespDto xdsx0014(Xdsx0014DataReqDto xdsx0014DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value);
        //返回对象
        Xdsx0014DataRespDto xdsx0014DataRespDto = new Xdsx0014DataRespDto();
        //请求参数
        String fx_serno = xdsx0014DataReqDto.getFx_serno();//授信协议分项编号
        String fx_type = xdsx0014DataReqDto.getFx_type();//授信协议分项类型
        String app_no = xdsx0014DataReqDto.getApp_no();//企业信息流水号app_no
        String fx_amt = xdsx0014DataReqDto.getFx_amt();//授信协议分项金额

        try {
            if (StringUtils.isBlank(fx_serno)) {
                cusHouseInfoMapper.deleteByCrpSerno(app_no);
                visaXdRiskMapper.deleteByCrpSerno(app_no);
                entlQyInfoMapper.deleteByPrimaryKey(app_no);
                xdsx0014DataRespDto.setZf_flag("0");
            } else {
                //1、根据授信协议分项编号查询LMT_REPLY_ACC_SUB_PRD获取授信分项流水号以及授信申请流水号；
                Map map = new HashMap();
                map.put("accSubNo", fx_serno);
                List<LmtReplyAccSubPrd> LmtReplyAccSubPrdList = lmtReplyAccSubPrdMapper.queryLmtReplyAccSubPrdByParams(map);
                if (LmtReplyAccSubPrdList != null && LmtReplyAccSubPrdList.size() > 0) {
                    LmtReplyAccSubPrd lmtReplyAccSubPrd = LmtReplyAccSubPrdList.get(0);
                    String accSubPrdNo = lmtReplyAccSubPrd.getAccSubPrdNo();//授信分项编号
                    String subSerno = lmtReplyAccSubPrd.getSubSerno();//授信申请分项流水号
                    map.put("accSubNo", fx_serno);
                    List<LmtReplyAccSub> LmtReplyAccSubList = lmtReplyAccSubMapper.queryLmtReplyAccSubByParams(map);
                    String serno = LmtReplyAccSubList.get(0).getSerno(); //授信流水号
                    String accNo = LmtReplyAccSubList.get(0).getAccNo(); //授信协议编号
                    BigDecimal lmtAmt = LmtReplyAccSubList.get(0).getLmtAmt();
                    //2、当使用授信协议分项编号关联查询iqp_loan_app中审批状态非000且非111且非992，使用授信协议台账编号查询贷款合同表CTR_LOAN_CONT，以上均无记录时，则执行以下操作：
                    String onApproveIqpFlag = iqpLoanAppMapper.selectOnApproveIqpByLmtAccNo(accSubPrdNo);
                    if (Objects.equals("0", onApproveIqpFlag)) {
                        logger.info("xdsx0014不存在iqp_loan_app中审批状态为000、111、992执行逻辑开始");
                        String ctrFlag = ctrLoanContMapper.selectCtrByLmtAccNo(accSubPrdNo);
                        String checkCtrFlag = ctrLoanContMapper.checkCusSxContByAccSubNo(accSubPrdNo);
                        if ("0".equals(ctrFlag)) {
                            //1)根据企业信息流水号查询面签推送风控VISA_XD_RISK获取最新流水号；根据最新流水号关联删除面签房屋信息cus_house_info；根据最新流水号关联删除VISA_XD_RISK；
                            cusHouseInfoMapper.deleteByCrpSerno(app_no);
                            visaXdRiskMapper.deleteByCrpSerno(app_no);
                            entlQyInfoMapper.deleteByPrimaryKey(app_no);
                            //2）根据授信协议台账编号查询授信协议台账下的房抵押品，关联删除押品关联的未生效担保合同Grt_Guar_Cont、担保合同与押品的关联GUAR_CONT_GRT_REL以及押品信息GUAR_BASE_INFO；
                            map.put("accSubNo", subSerno);
                            List<GrtGuarCont> GrtGuarContList = grtGuarContMapper.queryByParams(map);
                            for (int i = 0; i < GrtGuarContList.size(); i++) {
                                GrtGuarCont grtGuarCont = GrtGuarContList.get(i);
                                grtGuarContMapper.deleteByPrimaryKey(grtGuarCont.getGuarPkId());
                                guarBaseInfoMapper.deleteGuarBaseInfoByGuarContNo(grtGuarCont.getGuarContNo());
                                grtGuarContRelMapper.deleteGrtGuarContRelByGuarContNo(grtGuarCont.getGuarContNo());
                            }
                            //3）根据授信分项流水号关联删除授信分项申请与担保品关联表GUAR_BIZ_REL记录；
                            guarBizRelMapper.deleteByAccSubNo(subSerno);
                            //4）根据1中申请流水号关联删除合同申请iqp_loan_app；
                            iqpLoanAppMapper.deleteByAccSubNo(accSubPrdNo);
                            //5）根据授信分项流水号关联删除授信分项申请表LMT_APP_SUB、LMT_APP_SUB_PRD；
                            lmtAppSubMapper.deleteBySubSerno(subSerno);
                            lmtAppSubPrdMapper.deleteBySubSerno(subSerno);
                            //6）关联删除授信申请LMT_APP;
                            lmtAppMapper.deleteBySerno(serno);
                            //7）删除关联的授信协议分项LMT_REPLY_ACC_SUB_PRD、LMT_REPLY_ACC_SUB；判断授信协议下是否存在授信协议分项，若不存在则直接删除授信协议；
                            lmtReplyAccSubMapper.deleteByAccSubNo(fx_serno);
                            lmtReplyAccSubPrdMapper.deleteByAccSubNo(fx_serno);
                            lmtReplyAccMapper.deleteByAccNoAndNoExitsSub(accNo);

                            //8)删除关联的授信批复和授信批复分项
                            lmtReplyMapper.deleteBySerno(serno);
                            lmtReplySubPrdMapper.deleteBySerno(serno);
                            lmtReplySubMapper.deleteBySerno(serno);
                            //9) 额度作废
                            sendCmisLmt0008(lmtReplyAccSubPrd, accNo);

                            xdsx0014DataRespDto.setZf_flag("0");
                            logger.info("xdsx0014不存在iqp_loan_app中审批状态为000、111、992执行逻辑结束");
                        } else if ("1".equals(checkCtrFlag)) {
                            //3、当使用授信协议编号查询贷款合同表ctr_loan_cont，若只存在结清或者注销的线上房抵e点贷合同，不存在生效的线上房抵e点贷合同，则执行以下操作：
                            //1）根据企业信息流水号查询面签推送风控VISA_XD_RISK获取最新流水号；根据最新流水号关联删除面签房屋信息cus_house_info；根据最新流水号关联删除VISA_XD_RISK；
                            cusHouseInfoMapper.deleteByCrpSerno(app_no);
                            visaXdRiskMapper.deleteByCrpSerno(app_no);
                            entlQyInfoMapper.deleteByPrimaryKey(app_no);
                            //2)根据授信分项流水号关联删除授信分项申请表LMT_APP_SUB、LMT_APP_SUB_PRD；
                            lmtAppSubMapper.deleteBySubSerno(subSerno);
                            lmtAppSubPrdMapper.deleteBySubSerno(subSerno);
                            //3）关联删除授信申请LMT_APP;
                            lmtAppMapper.deleteBySerno(serno);
                            //4）更新授信批复数据，包括授信协议LMT_REPLY_ACC和授信申请LMT_APP的循环额度、总额度、起始日期、到期日期；更新授信协议宽限期；
                            map.put("accNo", accNo);
                            map.put("lmtAmt", lmtAmt.multiply(new BigDecimal(-1)));
                            lmtReplyAccMapper.updateLmtReplyAccAmtByAccNo(map);

                            //5）删除关联的授信协议分项LMT_REPLY_ACC_SUB_PRD、LMT_REPLY_ACC_SUB；判断授信协议下是否存在授信协议分项，若不存在则直接删除授信协议；
                            lmtReplyAccSubMapper.deleteByAccSubNo(fx_serno);
                            lmtReplyAccSubPrdMapper.deleteByAccSubNo(fx_serno);
                            lmtReplyAccMapper.deleteByAccNoAndNoExitsSub(accNo);
                            //6)删除关联的授信批复和授信批复分项
                            lmtReplyMapper.deleteBySerno(serno);
                            lmtReplySubPrdMapper.deleteBySerno(serno);
                            lmtReplySubMapper.deleteBySerno(serno);
                            //7) 额度作废
                            sendCmisLmt0008(lmtReplyAccSubPrd, accNo);
                            xdsx0014DataRespDto.setZf_flag("0");
                        } else {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "此条数据不符合作废条件！");
                        }
                    } else {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "此条数据不符合作废条件！");
                    }

                } else {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "信贷系统无此条数据！");
                }
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
            xdsx0014DataRespDto.setZf_flag("1");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value);
        return xdsx0014DataRespDto;
    }

    private void sendCmisLmt0008(LmtReplyAccSubPrd lmtReplyAccSubPrd, String accNo) {
        CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
        cmisLmt0008ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0008ReqDto.setLmtType("01");//授信类型
        cmisLmt0008ReqDto.setInputId(lmtReplyAccSubPrd.getInputId());//登记人
        cmisLmt0008ReqDto.setInputBrId(lmtReplyAccSubPrd.getInputBrId());//登记机构
        cmisLmt0008ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期

        CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
        cmisLmt0008ApprListReqDto.setCusId(lmtReplyAccSubPrd.getCusId());
        cmisLmt0008ApprListReqDto.setOptType("02");
        cmisLmt0008ApprListReqDto.setApprSerno(accNo);
        cmisLmt0008ReqDto.setApprList(Arrays.asList(cmisLmt0008ApprListReqDto));

        logger.info("根据业务客户编号【" + lmtReplyAccSubPrd.getCusId() + "】前往额度系统-额度提前终止请求报文：" + JSON.toJSONString(cmisLmt0008ReqDto));
        ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
        logger.info("根据业务客户编号【" + lmtReplyAccSubPrd.getCusId() + "】前往额度系统-额度提前终止返回报文：" + JSON.toJSONString(cmisLmt0008RespDto));

        if (!Objects.equals(cmisLmt0008RespDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key) || !Objects.equals(cmisLmt0008RespDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
            throw BizException.error(null, EpbEnum.EPB099999.key, cmisLmt0008RespDto.getData().getErrorCode());
        }

    }
}
