package cn.com.yusys.yusp.service.server.xdxw0080;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtPerferRateApplyInfo;
import cn.com.yusys.yusp.dto.server.xdxw0080.req.Xdxw0080DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0080.resp.Xdxw0080DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.LmtPerferRateApplyInfoService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 小微优惠利率通知接口
 *
 * @author 王玉坤
 * @version 1.0
 */
@Service
public class Xdxw0080Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0080Service.class);

    @Autowired
    private LmtPerferRateApplyInfoService lmtPerferRateApplyInfoService;

    /**
     * 小微优惠利率通知接口
     *
     * @param xdxw0080DataReqDto
     * @return
     * @throws BizException
     * @throws Exception
     * @author 王玉坤
     */
    public Xdxw0080DataRespDto xdxw0080(Xdxw0080DataReqDto xdxw0080DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataReqDto));
        Xdxw0080DataRespDto xdxw0080DataRespDto = new Xdxw0080DataRespDto();
        LmtPerferRateApplyInfo lmtPerferRateApplyInfo = null;
        try {
            // 获取第三方业务流水号
            String reqId = xdxw0080DataReqDto.getReqId();
            // 审批结果 997-通过 998-否决
            String approveStatus = xdxw0080DataReqDto.getApproveStatus();
            // 批复利率
            String approveRate = xdxw0080DataReqDto.getApproveRate();
            // 拒绝原因
            String refuseReason = xdxw0080DataReqDto.getRefuseReason();

            // 根据第三方流水号查询优惠利率申请信息
            logger.info("根据第三方业务流水号【{}】查询优惠利率申请信息开始", reqId);
            lmtPerferRateApplyInfo = lmtPerferRateApplyInfoService.selectByBizSerno(reqId);
            logger.info("根据第三方业务流水号【{}】查询优惠利率申请信息结束", JSON.toJSONString(lmtPerferRateApplyInfo));

            if (Objects.isNull(lmtPerferRateApplyInfo)) {
                throw BizException.error(null, "9999", "未查询到优惠利率申请信息");
            }

            // 根据优惠利率申请结果更新优惠利率率信息
            if (CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) {
                logger.info("优惠利率审批通过，调查流水号【{}】、批复利率【{}】", lmtPerferRateApplyInfo.getSurveySerno(), approveRate);

                // 更新优惠利率信息
                lmtPerferRateApplyInfo.setAppRate(new BigDecimal(approveRate));
                lmtPerferRateApplyInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            } else {
                logger.info("优惠利率审批拒绝，调查流水号【{}】、决绝原因【{}】", lmtPerferRateApplyInfo.getSurveySerno(), refuseReason);
                // 更新优惠利率信息
                lmtPerferRateApplyInfo.setAppRate(new BigDecimal(approveRate));
                lmtPerferRateApplyInfo.setRefuseReasons(refuseReason);
                lmtPerferRateApplyInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            }
            lmtPerferRateApplyInfoService.update(lmtPerferRateApplyInfo);

            xdxw0080DataRespDto.setOpFlag("S");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataRespDto));
        return xdxw0080DataRespDto;
    }
}
