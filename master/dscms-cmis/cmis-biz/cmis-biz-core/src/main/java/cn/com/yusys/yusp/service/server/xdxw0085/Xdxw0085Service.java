package cn.com.yusys.yusp.service.server.xdxw0085;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdxw0085.req.Xdxw0085DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0085.resp.Xdxw0085DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCobInfoMapper;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

/**
 * 接口处理类:共借人送达地址确认书文本生成PDF
 *
 * @author zrcbank
 * @version 1.0
 */
@Service
public class Xdxw0085Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0085Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;
    @Autowired
    private LmtCobInfoMapper lmtCobInfoMapper;

    /**
     * 共借人送达地址确认书文本生成PDF
     *
     * @param xdxw0085DataReqDto
     * @return
     */
    public Xdxw0085DataRespDto xdxw0085(Xdxw0085DataReqDto xdxw0085DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataReqDto));
        Xdxw0085DataRespDto xdxw0085DataRespDto = new Xdxw0085DataRespDto();

        try {
            String cusName = xdxw0085DataReqDto.getCusName();
            String userName = xdxw0085DataReqDto.getUserName();
            String contNo = xdxw0085DataReqDto.getContNo();
            String signDate = xdxw0085DataReqDto.getSignDate();
            String type = xdxw0085DataReqDto.getType();
            String other = xdxw0085DataReqDto.getOther();
            String accoNo = xdxw0085DataReqDto.getAccoNo();


            //1、第一步：确认要生成prd的模板名称
            String cptModelName = "gjrsm.cpt";//帆软模板名称
            String pdfFileName = "xdxwbook" + contNo + ""; //生成的pdf文件名称

            // 获取配置信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            //查询存储地址信息
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String filePath = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //调用帆软的生成pdf的方法
            //2、传入帆软报表生成需要的参数
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("cusName", cusName);//客户名
            parameterMap.put("userName", userName);
            parameterMap.put("signDate", signDate);
            parameterMap.put("contNo", contNo);
            parameterMap.put("accoNo", accoNo);
            //借款人与声明人关系
            /********
             * 103000	子女
             * 105000	其他血亲关系
             * 110000	同学
             * 102000	配偶
             * 112000	其他关系
             * 109000	师生
             * 107000	同事
             * 108000	朋友
             * 104000	兄弟姐妹
             * 106000	其他姻亲关系
             * 111000	合伙人
             * 101000	父母
             * **********/
            //根据合同号查询共有人关系
            String coree = lmtCobInfoMapper.selectRelByContNo(contNo);
            logger.info("根据合同号查询共有人关系：" + coree);
            if ("102000".equals(coree) || "04".equals(type)) {
                parameterMap.put("type", "04");
            } else {
                parameterMap.put("type", "05");
                if (StringUtils.isEmpty(coree)) {
                    coree = "112000";
                }
                //字典翻译
                other = DictTranslatorUtils.findValueByDictKey("STD_ZB_SELFPER_REL_TYP", coree);
                parameterMap.put("other", other);
            }
            //传入公共参数
            parameterMap.put("TempleteName", cptModelName);//模板名称（附带路径）
            parameterMap.put("saveFileName", pdfFileName);//待生成的PDF文件名称
            parameterMap.put("path", path);

            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(cptModelName);//模板名称
                frptPdfArgsDto.setNewFileName(pdfFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(contNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }

            //返回参数
            xdxw0085DataRespDto.setPdfurl(path);
            xdxw0085DataRespDto.setPdffilename(pdfFileName + ".pdf");
            xdxw0085DataRespDto.setHtip(ip);
            xdxw0085DataRespDto.setPort(port);
            xdxw0085DataRespDto.setUsername(username);
            xdxw0085DataRespDto.setPassword(password);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }

        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataRespDto));
        return xdxw0085DataRespDto;
    }
}
