package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.LmtAppMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
public class DGSX01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGSX01BizService.class);//定义log

    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private OtherItemAppService otherItemAppService;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07BizService;
    @Autowired
    private CentralFileTaskService centralFileTaskService;
    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private LmtReplyAccService lmtReplyAccService;
    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if (CmisFlowConstants.FLOW_TYPE_TYPE_SX001.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX002.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX003.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX004.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX005.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX006.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX007.equals(bizType)) {
            handleLmtAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SX008.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX009.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX010.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX011.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX012.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX013.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX014.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX033.equals(bizType)) {
            handleLmtGrpAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG005.equals(bizType)) {
            //展期申请（对公）
            bgyw02BizService.bizOp(resultInstanceDto);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG008.equals(bizType)) {
            //担保变更（对公）
            bgyw03Bizservice.bizOp(resultInstanceDto);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG015.equals(bizType)) {
            //还款计划变更（对公）
            bgyw07BizService.bizOp(resultInstanceDto);
        } else if (BizFlowConstant.QT012.equals(bizType)) {
            //其他申请事项
            bizOp4QT012(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getRouterMapResult(serno);
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType()) && !OpType.END.equals(resultInstanceDto.getCurrentOpType()) && !OpType.RE_START.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }
    }

    /**
     * @方法名称: put2VarParamGrp
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamGrp(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtGrpAppService.getRouterMapResult(serno);
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType()) && !OpType.END.equals(resultInstanceDto.getCurrentOpType()) && !OpType.RE_START.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }
    }

    /**
     * put2VarParamOther
     *
     * @param resultInstanceDto
     * @param serno
     */
    public void put2VarParamOther(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getRouterMapResult(serno);
        params.remove("approveStatus");
        if (!OpType.REFUSE.equals(resultInstanceDto.getCurrentOpType()) && !OpType.END.equals(resultInstanceDto.getCurrentOpType()) && !OpType.RE_START.equals(resultInstanceDto.getCurrentOpType())) { // 否决除外
            params.put("nextSubmitNodeId", resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
            // 会签模式判断 apprMode
            String apprMode = lmtAppService.getApprMode(serno);
            if(null == apprMode || "".equals(apprMode)){
                apprMode = "01";
            }
            params.put("apprMode",apprMode);
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }
    }

    // 单一客户处理
    private void handleLmtAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "单一客户授信申请" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            // 获取当前业务申请路程类型
            String bizType = resultInstanceDto.getBizType();
            // 加载路由条件
            put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterStart(serno);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                // 判断提交节点是否为首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask(serno, lmtApp.getCusId(), lmtApp.getCusName(), bizType,resultInstanceDto.getInstanceId(), lmtApp.getInputId(), lmtApp.getInputBrId());
                }
                if ("256_30".equals(currNodeId)) {
                    createCentralFileTask(serno, lmtApp.getCusId(), lmtApp.getCusName(), lmtApp.getInputId(), lmtApp.getInputBrId(), bizType,resultInstanceDto);
                }
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                // lmtAppService.createImageSpplInfo(serno,bizType,lmtApp,resultInstanceDto.getInstanceId());
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                // 流程结束，发起影像审核
                sendImage(resultInstanceDto);
                lmtAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                lmtAppService.createImageSpplInfo(serno,bizType,lmtApp,resultInstanceDto.getInstanceId(),"对公授信申报");
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                    // 加载路由条件
                    put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                    // 加载路由条件
                    put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterRefuse(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    // 集团客户处理
    private void handleLmtGrpAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "对公授信申报审批" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
            // 获取当前业务申请路程类型
            String bizType = resultInstanceDto.getBizType();
            // 重置流程参数
            put2VarParamGrp(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程通过操作，流程参数：" + resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterStart(serno);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto) && !CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpApp.getLmtType())) {
                    createGrpUrgentTask(lmtGrpApp, bizType,resultInstanceDto.getInstanceId());
                }
/*                if ("256_30".equals(currNodeId)) {
                    createGrpCentralFileTask(serno, lmtGrpApp.getInputId(), lmtGrpApp.getInputBrId(), bizType,resultInstanceDto);
                }*/
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                // lmtGrpAppService.createImageSpplInfo(serno,bizType,resultInstanceDto.getInstanceId());
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                // 流程结束，发起影像审核
                sendImage(resultInstanceDto);
                lmtGrpAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                lmtGrpAppService.createImageSpplInfo(serno,bizType,resultInstanceDto.getInstanceId(),"对公授信申报");
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                    // 重置流程参数
                    put2VarParamGrp(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                    // 重置流程参数
                    put2VarParamGrp(resultInstanceDto, serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterRefuse(serno, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列并将自消息移除队列！！！！");
        }
    }

    /**
     * 其他申请事项
     *
     * @param instanceInfo
     */
    public void bizOp4QT012(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        try {
            // 获取申请信息 other_item_app
            OtherItemApp otherItemApp = otherItemAppService.selectByPrimaryKey(serno);
            // 获取授信流水号
            String LmtSerno ="";
            if(null != otherItemApp){
                LmtSerno = otherItemApp.getLmtSerno();
            }
            if(!"".equals(LmtSerno) && null != LmtSerno){
                // 加载路由条件
                put2VarParamOther(instanceInfo, LmtSerno);
            }
            // 企业类型处理
            WFBizParamDto param = new WFBizParamDto();
            Map<String, Object> params = new HashMap<>();
            param.setBizId(instanceInfo.getBizId());
            param.setInstanceId(instanceInfo.getInstanceId());
            String isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_0;
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(otherItemApp.getCusId()).getData();
            if (cusCorpDto != null && CmisCommonConstants.STD_ZB_YES_NO_1.equals(cusCorpDto.getIsSmconCus())) {
                isSmconCus = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            params.put("isSmconCus",isSmconCus);
            // 是否对公客户，判断协办客户经理节点
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(otherItemApp.getCusId());
            log.info("获取客户基本信息数据:{}", cusBaseClientDto.toString());
            if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_1);
            } else {
                params.put("isCusCom", CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            params.put("approveResult", "0");
            params.put("isSubAutoAppr", "0");
            params.put("sxkdRiskResult", "0");
            params.put("isGrp", "0");
            // 获取分成比例协办客户经理编号
            String managerIdXB = null;
            CusMgrDividePercDto cusMgrDividePercDto = iCusClientService.selectXBManagerId(otherItemApp.getCusId()).getData();
            log.info("根据客户号【{}】查询客户协办客户经理，返回为{}", otherItemApp.getCusId(), cusMgrDividePercDto);
            if (cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())) {
                managerIdXB = cusMgrDividePercDto.getManagerId();
            }
            params.put("managerId", managerIdXB);
            if (!OpType.REFUSE.equals(currentOpType)) {// 否决除外
                params.put("nextSubmitNodeId", instanceInfo.getNextNodeInfos().get(0).getNextNodeId());
            }

            // 是否公司部处理  isGSBAppr
            String isCprtApprove = otherItemApp.getIsCprtApprove();
            if("1".equals(isCprtApprove)){
                params.put("isGSBAppr","1");
            } else {
                params.put("isGSBAppr","0");
            }

            params.put("commitDeptTypeXDGLB","0");
            params.put("isUpAppr","0");
            params.put("upApprType","0");
            params.put("isDownAppr","0");
            params.put("isDAELmtAppr","0");
            // 会签模式处理
            String apprMode = "";
            try {
                // 获取客户批复
                LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(otherItemApp.getCusId());
                if(Objects.nonNull(lmtReplyAcc)){
                    if(null != lmtReplyAcc.getApprMode() && !"".equals(lmtReplyAcc.getApprMode())){
                        apprMode = lmtReplyAcc.getApprMode();
                    }
                }
            } catch (Exception e) {
                log.info("未获取到授信批复会签模式信息");
            }

            // 集团会签模式处理
            try {
                // 根据客户号获取集团客户号
                String grpNo = "";
                /**
                 * 获取集团客户号 cmiscus0001
                 */
                CmisCus0001ReqDto cmisCus0001ReqDto = new CmisCus0001ReqDto();
                cmisCus0001ReqDto.setCusId(otherItemApp.getCusId());
                ResultDto<CmisCus0001RespDto> cmisCus0001RespDtoResultDto = cmisCusClientService.cmiscus0001(cmisCus0001ReqDto);
                if (cmisCus0001RespDtoResultDto != null
                        && SuccessEnum.SUCCESS.key.equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())
                        && !"40012".equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())){
                    CmisCus0001RespDto data = cmisCus0001RespDtoResultDto.getData();
                    if(Objects.nonNull(data)){
                       grpNo = data.getGrpNo();//集团客户号
                    }
                }
                if(!"".equals(grpNo) && null != grpNo){
                    // 获取集团批复
                    LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.getLastLmtReplyAcc(grpNo);
                    if(Objects.nonNull(lmtGrpReplyAcc)){
                        if(null != lmtGrpReplyAcc.getApprMode() && !"".equals(lmtGrpReplyAcc.getApprMode())){
                            apprMode = lmtGrpReplyAcc.getApprMode();
                        }
                    }
                }
            } catch (Exception e) {
                log.info("未获取到集团授信批复会签模式信息");
            }

            if("".equals(apprMode) || null == apprMode){
                apprMode = "05";
            }
            params.put("apprMode",apprMode);
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);

            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherItemAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                // 结束后处理
                otherItemAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                otherItemAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                //添加路由条件
                put2VarParamOther(instanceInfo, serno);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                otherItemAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                //添加路由条件
                put2VarParamOther(instanceInfo, serno);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                otherItemAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGSX01.equals(flowCode);
    }

    /**
     * 新增档案任务
     *
     * @param serno
     * @param cusId
     * @param cusName
     * @param inputId
     * @param inputBrId
     * @param bizType
     */
    public void createCentralFileTask(String serno, String cusId, String cusName, String inputId, String inputBrId, String bizType,ResultInstanceDto resultInstanceDto) {
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(serno);
        centralFileTaskdto.setCusId(cusId);
        centralFileTaskdto.setCusName(cusName);
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        centralFileTaskdto.setInputId(inputId);
        centralFileTaskdto.setInputBrId(inputBrId);
        centralFileTaskdto.setOptType("01"); // 纯指令
        centralFileTaskdto.setTaskType("01"); // 档案接收
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        //获取分项信息
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            boolean flag = false;
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String guarMode = lmtAppSub.getGuarMode();
                if (StringUtils.nonBlank(guarMode)) {
                    //若分项担保方式全部为低风险,则为加急
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || flag) {
                        flag = true;
                        break;
                    }
                }
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                        //获取分项明细产品类型属性
                        String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                        //若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
                        if (StringUtils.nonBlank(lmtBizTypeProp)) {
                            if ("P032".equals(lmtBizTypeProp) || "P011".equals(lmtBizTypeProp) || "P009".equals(lmtBizTypeProp) || "P013".equals(lmtBizTypeProp) || "P014".equals(lmtBizTypeProp) || "P016".equals(lmtBizTypeProp)) {
                                centralFileTaskdto.setTaskUrgentFlag("3"); //系统加急
                                break;
                            }
                        }
                    }
                }
            }
        }
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    /**
     * 新增加急任务
     *
     * @param serno
     * @param cusId
     * @param cusName
     * @param bizType
     */
    public void createUrgentTask(String serno, String cusId, String cusName, String bizType,String instanceId, String inputId, String inputBrId) {
        // 若分项担保方式全部为低风险,则为加急
        boolean flag0 = true;
        // 若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
        boolean flag1 = false;
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubList)) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String guarMode = lmtAppSub.getGuarMode();
                if (StringUtils.nonBlank(guarMode)) {
                    //若分项担保方式不为低风险
                    if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode)) {
                        flag0 = false;
                    }
                }

                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                        //获取分项明细产品类型属性
                        String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                        //若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
                        if (StringUtils.nonBlank(lmtBizTypeProp)) {
                            if ("P032".equals(lmtBizTypeProp) || "P011".equals(lmtBizTypeProp) || "P009".equals(lmtBizTypeProp)
                                    || "P013".equals(lmtBizTypeProp) || "P014".equals(lmtBizTypeProp) || "P016".equals(lmtBizTypeProp) || flag1) {
                                flag1 = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if(flag0 || flag1) {
            TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
            taskUrgentAppDto.setBizType(bizType);
            taskUrgentAppDto.setCusId(cusId);
            taskUrgentAppDto.setCusName(cusName);
            taskUrgentAppDto.setSerno(serno);
            taskUrgentAppDto.setUrgentType("3");
            taskUrgentAppDto.setUrgentResn("系统加急");
            taskUrgentAppDto.setPwbrSerno(instanceId);
            taskUrgentAppDto.setManagerBrId(inputBrId);
            taskUrgentAppDto.setManagerId(inputId);
            taskUrgentAppDto.setInputBrId(inputBrId);
            taskUrgentAppDto.setInputId(inputId);
            taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
            taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
            ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
            if(!"0".equals(result.getCode()) || 1 != result.getData()){
                throw BizException.error(null, "999999","新增系统加急记录失败！");
            }
        }
    }

    /**
     * 集团新增档案任务
     *
     * @param grpSerno
     * @param inputId
     * @param inputBrId
     * @param bizType
     */
    public void createGrpCentralFileTask(String grpSerno, String inputId, String inputBrId, String bizType,ResultInstanceDto instanceInfo) {
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        if (CollectionUtils.nonEmpty(lmtGrpMemRelList)) {
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                String serno = lmtGrpMemRel.getSingleSerno();
                String cusName = lmtGrpMemRel.getCusName();
                String cusId = lmtGrpMemRel.getCusId();
                if (StringUtils.nonBlank(serno)) {
                    this.createCentralFileTask(serno, cusId, cusName, inputId, inputBrId, bizType,instanceInfo);
                }
            }
        }
    }

    /**
     * 集团新增档案任务
     *
     * @param
     * @param bizType
     */
    public void createGrpUrgentTask(LmtGrpApp lmtGrpApp, String bizType,String instanceId) {
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApp.getGrpSerno());
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            // 若分项担保方式全部为低风险,则为加急
            boolean flag0 = true;
            // 若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
            boolean flag1 = false;
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(lmtGrpMemRel.getSingleSerno());
            if (CollectionUtils.nonEmpty(lmtAppSubList)) {
                for (LmtAppSub lmtAppSub : lmtAppSubList) {
                    String guarMode = lmtAppSub.getGuarMode();
                    if (StringUtils.nonBlank(guarMode)) {
                        //若分项担保方式全部为低风险,则为加急
                        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode)) {
                            flag0 = false;
                        }
                    }

                    List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                    if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                        for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                            //获取分项明细产品类型属性
                            String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                            //若产品类型属性为外贸贷，省心快贷，征信贷，结息贷，优税贷，诚易贷则为加急
                            if (StringUtils.nonBlank(lmtBizTypeProp)) {
                                if ("P032".equals(lmtBizTypeProp) || "P011".equals(lmtBizTypeProp) || "P009".equals(lmtBizTypeProp)
                                        || "P013".equals(lmtBizTypeProp) || "P014".equals(lmtBizTypeProp) || "P016".equals(lmtBizTypeProp) || flag1) {
                                    flag1 = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if(flag0 || flag1) {
                TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
                taskUrgentAppDto.setBizType(bizType);
                taskUrgentAppDto.setCusId(lmtGrpApp.getGrpCusId());
                taskUrgentAppDto.setCusName(lmtGrpApp.getGrpCusName());
                taskUrgentAppDto.setSerno(lmtGrpApp.getGrpSerno());
                taskUrgentAppDto.setUrgentType("3");
                taskUrgentAppDto.setUrgentResn("系统加急");
                taskUrgentAppDto.setPwbrSerno(instanceId);
                taskUrgentAppDto.setManagerBrId(lmtGrpApp.getManagerBrId());
                taskUrgentAppDto.setManagerId(lmtGrpApp.getManagerId());
                taskUrgentAppDto.setInputBrId(lmtGrpApp.getInputBrId());
                taskUrgentAppDto.setInputId(lmtGrpApp.getInputId());
                taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
                taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
                ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
                if(!"0".equals(result.getCode()) || 1 != result.getData()){
                    throw BizException.error(null, "999999","新增系统加急记录失败！");
                }
                break;
            }
        }
    }
    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        // 一般情况下，直接获取流程实列中的参数。
        Map<String, Object> map = (Map)JSON.parse(resultInstanceDto.getFlowParam());
        String topOutsystemCode = (String) map.get("topOutsystemCode");
        if(StringUtils.isEmpty(topOutsystemCode)){
            throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
        }
        Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
        String docid = imageParams.get("businessid");
        if(StringUtils.isEmpty(docid)){
            throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
        }
        String[] arr = topOutsystemCode.split(";");
        for (int i = 0; i < arr.length; i++) {
            ImageApprDto imageApprDto = new ImageApprDto();
            imageApprDto.setDocId(docid);//任务编号
            imageApprDto.setApproval("同意");//审批意见
            imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
            imageApprDto.setOutcode(arr[i]);//文件类型根节点
            imageApprDto.setOpercode("");//这个流程不需要审批人员
            cmisBizXwCommonService.sendImage(imageApprDto);
        }
    }
}
