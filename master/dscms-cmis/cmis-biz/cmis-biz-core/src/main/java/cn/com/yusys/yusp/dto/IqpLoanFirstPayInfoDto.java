package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanFirstPayInfo
 * @类描述: iqp_loan_first_pay_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-28 15:38:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanFirstPayInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	private String bizNo;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 购房合同号 **/
	private String purHouseCont;
	
	/** 支付日期 **/
	private String defrayDate;
	
	/** 支付方式 **/
	private String defrayType;
	
	/** 支付金额 **/
	private java.math.BigDecimal defrayAmt;
	
	/** 是否通过银行流水核实 **/
	private String bankFlowVerifyFlag;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo == null ? null : bizNo.trim();
	}
	
    /**
     * @return BizNo
     */	
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param purHouseCont
	 */
	public void setPurHouseCont(String purHouseCont) {
		this.purHouseCont = purHouseCont == null ? null : purHouseCont.trim();
	}
	
    /**
     * @return PurHouseCont
     */	
	public String getPurHouseCont() {
		return this.purHouseCont;
	}
	
	/**
	 * @param defrayDate
	 */
	public void setDefrayDate(String defrayDate) {
		this.defrayDate = defrayDate == null ? null : defrayDate.trim();
	}
	
    /**
     * @return DefrayDate
     */	
	public String getDefrayDate() {
		return this.defrayDate;
	}
	
	/**
	 * @param defrayType
	 */
	public void setDefrayType(String defrayType) {
		this.defrayType = defrayType == null ? null : defrayType.trim();
	}
	
    /**
     * @return DefrayType
     */	
	public String getDefrayType() {
		return this.defrayType;
	}
	
	/**
	 * @param defrayAmt
	 */
	public void setDefrayAmt(java.math.BigDecimal defrayAmt) {
		this.defrayAmt = defrayAmt;
	}
	
    /**
     * @return DefrayAmt
     */	
	public java.math.BigDecimal getDefrayAmt() {
		return this.defrayAmt;
	}
	
	/**
	 * @param bankFlowVerifyFlag
	 */
	public void setBankFlowVerifyFlag(String bankFlowVerifyFlag) {
		this.bankFlowVerifyFlag = bankFlowVerifyFlag == null ? null : bankFlowVerifyFlag.trim();
	}
	
    /**
     * @return BankFlowVerifyFlag
     */	
	public String getBankFlowVerifyFlag() {
		return this.bankFlowVerifyFlag;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}