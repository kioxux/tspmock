package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarCommonOwner
 * @类描述: guar_common_owner数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 14:29:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarCommonOwnerDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 押品编号 **/
	private String guarNo;
	
	/** 共有人客户编号 **/
	private String commonOwnerId;
	
	/** 共有人客户名称 **/
	private String commonOwnerName;
	
	/** 共有人客户类型 **/
	private String commonOwnerType;
	
	/** 共有人证件类型 **/
	private String commonCertType;
	
	/** 共有人证件号码 **/
	private String commonCertCode;
	
	/** 共有人所占份额 **/
	private java.math.BigDecimal commonHoldPortio;
	
	/** 房产证（共有权证）号码 **/
	private String commonHouseLandNo;
	
	/** 土地证（共有权证）号码 **/
	private String commonLandNo;
	
	/** 共有人类型 **/
	private String shareOwnerType;
	
	/** 共有人地址 **/
	private String commonAddr;
	
	/** 共有人电话 **/
	private String commonTel;
	
	/** 不动产（房产证号） **/
	private String houseNo;
	
	/** 共有人QQ号码 **/
	private String commonQq;
	
	/** 共有人微信号码 **/
	private String commonWxin;
	
	/** 共有人电子邮箱 **/
	private String commonMail;
	
	/** 联系人 **/
	private String legalName;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param commonOwnerId
	 */
	public void setCommonOwnerId(String commonOwnerId) {
		this.commonOwnerId = commonOwnerId == null ? null : commonOwnerId.trim();
	}
	
    /**
     * @return CommonOwnerId
     */	
	public String getCommonOwnerId() {
		return this.commonOwnerId;
	}
	
	/**
	 * @param commonOwnerName
	 */
	public void setCommonOwnerName(String commonOwnerName) {
		this.commonOwnerName = commonOwnerName == null ? null : commonOwnerName.trim();
	}
	
    /**
     * @return CommonOwnerName
     */	
	public String getCommonOwnerName() {
		return this.commonOwnerName;
	}
	
	/**
	 * @param commonOwnerType
	 */
	public void setCommonOwnerType(String commonOwnerType) {
		this.commonOwnerType = commonOwnerType == null ? null : commonOwnerType.trim();
	}
	
    /**
     * @return CommonOwnerType
     */	
	public String getCommonOwnerType() {
		return this.commonOwnerType;
	}
	
	/**
	 * @param commonCertType
	 */
	public void setCommonCertType(String commonCertType) {
		this.commonCertType = commonCertType == null ? null : commonCertType.trim();
	}
	
    /**
     * @return CommonCertType
     */	
	public String getCommonCertType() {
		return this.commonCertType;
	}
	
	/**
	 * @param commonCertCode
	 */
	public void setCommonCertCode(String commonCertCode) {
		this.commonCertCode = commonCertCode == null ? null : commonCertCode.trim();
	}
	
    /**
     * @return CommonCertCode
     */	
	public String getCommonCertCode() {
		return this.commonCertCode;
	}
	
	/**
	 * @param commonHoldPortio
	 */
	public void setCommonHoldPortio(java.math.BigDecimal commonHoldPortio) {
		this.commonHoldPortio = commonHoldPortio;
	}
	
    /**
     * @return CommonHoldPortio
     */	
	public java.math.BigDecimal getCommonHoldPortio() {
		return this.commonHoldPortio;
	}
	
	/**
	 * @param commonHouseLandNo
	 */
	public void setCommonHouseLandNo(String commonHouseLandNo) {
		this.commonHouseLandNo = commonHouseLandNo == null ? null : commonHouseLandNo.trim();
	}
	
    /**
     * @return CommonHouseLandNo
     */	
	public String getCommonHouseLandNo() {
		return this.commonHouseLandNo;
	}
	
	/**
	 * @param commonLandNo
	 */
	public void setCommonLandNo(String commonLandNo) {
		this.commonLandNo = commonLandNo == null ? null : commonLandNo.trim();
	}
	
    /**
     * @return CommonLandNo
     */	
	public String getCommonLandNo() {
		return this.commonLandNo;
	}
	
	/**
	 * @param shareOwnerType
	 */
	public void setShareOwnerType(String shareOwnerType) {
		this.shareOwnerType = shareOwnerType == null ? null : shareOwnerType.trim();
	}
	
    /**
     * @return ShareOwnerType
     */	
	public String getShareOwnerType() {
		return this.shareOwnerType;
	}
	
	/**
	 * @param commonAddr
	 */
	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr == null ? null : commonAddr.trim();
	}
	
    /**
     * @return CommonAddr
     */	
	public String getCommonAddr() {
		return this.commonAddr;
	}
	
	/**
	 * @param commonTel
	 */
	public void setCommonTel(String commonTel) {
		this.commonTel = commonTel == null ? null : commonTel.trim();
	}
	
    /**
     * @return CommonTel
     */	
	public String getCommonTel() {
		return this.commonTel;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo == null ? null : houseNo.trim();
	}
	
    /**
     * @return HouseNo
     */	
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param commonQq
	 */
	public void setCommonQq(String commonQq) {
		this.commonQq = commonQq == null ? null : commonQq.trim();
	}
	
    /**
     * @return CommonQq
     */	
	public String getCommonQq() {
		return this.commonQq;
	}
	
	/**
	 * @param commonWxin
	 */
	public void setCommonWxin(String commonWxin) {
		this.commonWxin = commonWxin == null ? null : commonWxin.trim();
	}
	
    /**
     * @return CommonWxin
     */	
	public String getCommonWxin() {
		return this.commonWxin;
	}
	
	/**
	 * @param commonMail
	 */
	public void setCommonMail(String commonMail) {
		this.commonMail = commonMail == null ? null : commonMail.trim();
	}
	
    /**
     * @return CommonMail
     */	
	public String getCommonMail() {
		return this.commonMail;
	}
	
	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName == null ? null : legalName.trim();
	}
	
    /**
     * @return LegalName
     */	
	public String getLegalName() {
		return this.legalName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}