package cn.com.yusys.yusp.domain.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ZxdPreLmtApply
 * @类描述: zxd_pre_lmt_apply数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 23:40:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ZxdPreLmtApplyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 对接记录ID **/
	private String applyRecordId;
	
	/** 企业代码 **/
	private String corpId;
	
	/** 企业名称 **/
	private String corpName;
	
	/** 借款人预期借款额度 **/
	private java.math.BigDecimal borrowerLossLmt;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 授信期限 **/
	private Integer lmtTerm;
	
	/** 纳税评级 **/
	private String taxGrade;
	
	/** 征信报告信用评分 **/
	private String reportCreditGrade;
	
	/** 信用贷款用信余额 **/
	private java.math.BigDecimal creditBalance;
	
	/** 是否黑灰名单客户 **/
	private String ifBlack;
	
	/** 苏州地方征信是否显示纳税信息 **/
	private String ifShowTax;
	
	/** 上年度纳税金额 **/
	private java.math.BigDecimal ltYearTaxAmt;
	
	/** 是否租用 **/
	private String ifRent;
	
	/** 是否自有 **/
	private String ifSelf;
	
	/** 是否具备自有产权证 **/
	private String ifHaveCert;
	
	/** 企业苏州地方征信信用情况（系统评分） **/
	private Integer corpCreditInfo;
	
	/** 资产负债率 **/
	private java.math.BigDecimal assetRate;
	
	/** 企业存量贷款与销售收入比率 **/
	private java.math.BigDecimal corpStockRateSys;
	
	/** 专利信息情况（系统评分） **/
	private Integer patentInfoSys;
	
	/** 利润增长情况（系统评分） **/
	private Integer profitIncreaseSys;
	
	/** 实际控制人从事本行业年限（系统评分） **/
	private Integer industryYearSys;
	
	/** 企业所属行业 **/
	private String corpTrade;
	
	/** 公积金、社保缴纳情况（系统评分） **/
	private Integer socialPaymentSys;
	
	/** 水电气费缴纳情况（系统评分） **/
	private Integer waterElecSys;
	
	/** 法人或实际控制人落户情况（系统评分） **/
	private Integer settleSys;
	
	/** 总分值（系统评分） **/
	private Integer scoreTotalSys;
	
	/** 是否准入（自动测算，无需手填） **/
	private String ifAdmitSys;
	
	/** 最大预授信额度 **/
	private java.math.BigDecimal maxLmtAmt;
	
	/** 状态 **/
	private String status;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 发起机构 **/
	private String launchOrg;
	
	
	/**
	 * @param applyRecordId
	 */
	public void setApplyRecordId(String applyRecordId) {
		this.applyRecordId = applyRecordId == null ? null : applyRecordId.trim();
	}
	
    /**
     * @return ApplyRecordId
     */	
	public String getApplyRecordId() {
		return this.applyRecordId;
	}
	
	/**
	 * @param corpId
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId == null ? null : corpId.trim();
	}
	
    /**
     * @return CorpId
     */	
	public String getCorpId() {
		return this.corpId;
	}
	
	/**
	 * @param corpName
	 */
	public void setCorpName(String corpName) {
		this.corpName = corpName == null ? null : corpName.trim();
	}
	
    /**
     * @return CorpName
     */	
	public String getCorpName() {
		return this.corpName;
	}
	
	/**
	 * @param borrowerLossLmt
	 */
	public void setBorrowerLossLmt(java.math.BigDecimal borrowerLossLmt) {
		this.borrowerLossLmt = borrowerLossLmt;
	}
	
    /**
     * @return BorrowerLossLmt
     */	
	public java.math.BigDecimal getBorrowerLossLmt() {
		return this.borrowerLossLmt;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return LmtTerm
     */	
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param taxGrade
	 */
	public void setTaxGrade(String taxGrade) {
		this.taxGrade = taxGrade == null ? null : taxGrade.trim();
	}
	
    /**
     * @return TaxGrade
     */	
	public String getTaxGrade() {
		return this.taxGrade;
	}
	
	/**
	 * @param reportCreditGrade
	 */
	public void setReportCreditGrade(String reportCreditGrade) {
		this.reportCreditGrade = reportCreditGrade == null ? null : reportCreditGrade.trim();
	}
	
    /**
     * @return ReportCreditGrade
     */	
	public String getReportCreditGrade() {
		return this.reportCreditGrade;
	}
	
	/**
	 * @param creditBalance
	 */
	public void setCreditBalance(java.math.BigDecimal creditBalance) {
		this.creditBalance = creditBalance;
	}
	
    /**
     * @return CreditBalance
     */	
	public java.math.BigDecimal getCreditBalance() {
		return this.creditBalance;
	}
	
	/**
	 * @param ifBlack
	 */
	public void setIfBlack(String ifBlack) {
		this.ifBlack = ifBlack == null ? null : ifBlack.trim();
	}
	
    /**
     * @return IfBlack
     */	
	public String getIfBlack() {
		return this.ifBlack;
	}
	
	/**
	 * @param ifShowTax
	 */
	public void setIfShowTax(String ifShowTax) {
		this.ifShowTax = ifShowTax == null ? null : ifShowTax.trim();
	}
	
    /**
     * @return IfShowTax
     */	
	public String getIfShowTax() {
		return this.ifShowTax;
	}
	
	/**
	 * @param ltYearTaxAmt
	 */
	public void setLtYearTaxAmt(java.math.BigDecimal ltYearTaxAmt) {
		this.ltYearTaxAmt = ltYearTaxAmt;
	}
	
    /**
     * @return LtYearTaxAmt
     */	
	public java.math.BigDecimal getLtYearTaxAmt() {
		return this.ltYearTaxAmt;
	}
	
	/**
	 * @param ifRent
	 */
	public void setIfRent(String ifRent) {
		this.ifRent = ifRent == null ? null : ifRent.trim();
	}
	
    /**
     * @return IfRent
     */	
	public String getIfRent() {
		return this.ifRent;
	}
	
	/**
	 * @param ifSelf
	 */
	public void setIfSelf(String ifSelf) {
		this.ifSelf = ifSelf == null ? null : ifSelf.trim();
	}
	
    /**
     * @return IfSelf
     */	
	public String getIfSelf() {
		return this.ifSelf;
	}
	
	/**
	 * @param ifHaveCert
	 */
	public void setIfHaveCert(String ifHaveCert) {
		this.ifHaveCert = ifHaveCert == null ? null : ifHaveCert.trim();
	}
	
    /**
     * @return IfHaveCert
     */	
	public String getIfHaveCert() {
		return this.ifHaveCert;
	}
	
	/**
	 * @param corpCreditInfo
	 */
	public void setCorpCreditInfo(Integer corpCreditInfo) {
		this.corpCreditInfo = corpCreditInfo;
	}
	
    /**
     * @return CorpCreditInfo
     */	
	public Integer getCorpCreditInfo() {
		return this.corpCreditInfo;
	}
	
	/**
	 * @param assetRate
	 */
	public void setAssetRate(java.math.BigDecimal assetRate) {
		this.assetRate = assetRate;
	}
	
    /**
     * @return AssetRate
     */	
	public java.math.BigDecimal getAssetRate() {
		return this.assetRate;
	}
	
	/**
	 * @param corpStockRateSys
	 */
	public void setCorpStockRateSys(java.math.BigDecimal corpStockRateSys) {
		this.corpStockRateSys = corpStockRateSys;
	}
	
    /**
     * @return CorpStockRateSys
     */	
	public java.math.BigDecimal getCorpStockRateSys() {
		return this.corpStockRateSys;
	}
	
	/**
	 * @param patentInfoSys
	 */
	public void setPatentInfoSys(Integer patentInfoSys) {
		this.patentInfoSys = patentInfoSys;
	}
	
    /**
     * @return PatentInfoSys
     */	
	public Integer getPatentInfoSys() {
		return this.patentInfoSys;
	}
	
	/**
	 * @param profitIncreaseSys
	 */
	public void setProfitIncreaseSys(Integer profitIncreaseSys) {
		this.profitIncreaseSys = profitIncreaseSys;
	}
	
    /**
     * @return ProfitIncreaseSys
     */	
	public Integer getProfitIncreaseSys() {
		return this.profitIncreaseSys;
	}
	
	/**
	 * @param industryYearSys
	 */
	public void setIndustryYearSys(Integer industryYearSys) {
		this.industryYearSys = industryYearSys;
	}
	
    /**
     * @return IndustryYearSys
     */	
	public Integer getIndustryYearSys() {
		return this.industryYearSys;
	}
	
	/**
	 * @param corpTrade
	 */
	public void setCorpTrade(String corpTrade) {
		this.corpTrade = corpTrade == null ? null : corpTrade.trim();
	}
	
    /**
     * @return CorpTrade
     */	
	public String getCorpTrade() {
		return this.corpTrade;
	}
	
	/**
	 * @param socialPaymentSys
	 */
	public void setSocialPaymentSys(Integer socialPaymentSys) {
		this.socialPaymentSys = socialPaymentSys;
	}
	
    /**
     * @return SocialPaymentSys
     */	
	public Integer getSocialPaymentSys() {
		return this.socialPaymentSys;
	}
	
	/**
	 * @param waterElecSys
	 */
	public void setWaterElecSys(Integer waterElecSys) {
		this.waterElecSys = waterElecSys;
	}
	
    /**
     * @return WaterElecSys
     */	
	public Integer getWaterElecSys() {
		return this.waterElecSys;
	}
	
	/**
	 * @param settleSys
	 */
	public void setSettleSys(Integer settleSys) {
		this.settleSys = settleSys;
	}
	
    /**
     * @return SettleSys
     */	
	public Integer getSettleSys() {
		return this.settleSys;
	}
	
	/**
	 * @param scoreTotalSys
	 */
	public void setScoreTotalSys(Integer scoreTotalSys) {
		this.scoreTotalSys = scoreTotalSys;
	}
	
    /**
     * @return ScoreTotalSys
     */	
	public Integer getScoreTotalSys() {
		return this.scoreTotalSys;
	}
	
	/**
	 * @param ifAdmitSys
	 */
	public void setIfAdmitSys(String ifAdmitSys) {
		this.ifAdmitSys = ifAdmitSys == null ? null : ifAdmitSys.trim();
	}
	
    /**
     * @return IfAdmitSys
     */	
	public String getIfAdmitSys() {
		return this.ifAdmitSys;
	}
	
	/**
	 * @param maxLmtAmt
	 */
	public void setMaxLmtAmt(java.math.BigDecimal maxLmtAmt) {
		this.maxLmtAmt = maxLmtAmt;
	}
	
    /**
     * @return MaxLmtAmt
     */	
	public java.math.BigDecimal getMaxLmtAmt() {
		return this.maxLmtAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param launchOrg
	 */
	public void setLaunchOrg(String launchOrg) {
		this.launchOrg = launchOrg == null ? null : launchOrg.trim();
	}
	
    /**
     * @return LaunchOrg
     */	
	public String getLaunchOrg() {
		return this.launchOrg;
	}


}