/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.service.IqpAccpAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAccpAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-14 13:45:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "银承合同申请")
@RequestMapping("/api/iqpaccpapp")
public class IqpAccpAppResource {
    @Autowired
    private IqpAccpAppService iqpAccpAppService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAccpApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAccpApp> list = iqpAccpAppService.selectAll(queryModel);
        return new ResultDto<List<IqpAccpApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAccpApp>> index(QueryModel queryModel) {
        List<IqpAccpApp> list = iqpAccpAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpAccpApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAccpApp> show(@PathVariable("pkId") String pkId) {
        IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAccpApp>(iqpAccpApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAccpApp> create(@RequestBody IqpAccpApp iqpAccpApp) throws URISyntaxException {
        iqpAccpAppService.insert(iqpAccpApp);
        return new ResultDto<IqpAccpApp>(iqpAccpApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAccpApp iqpAccpApp) throws URISyntaxException {
        int result = iqpAccpAppService.update(iqpAccpApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAccpAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAccpAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 银承申请保存方法
     * @param iqpAccpApp
     * @return
     */
    @ApiOperation("银承申请保存方法")
    @PostMapping("/saveiqpaccpappinfo")
    public ResultDto<Map> saveIqpAccpAppInfo(@RequestBody IqpAccpApp iqpAccpApp) {
        Map result = iqpAccpAppService.saveIqpAccpAppInfo(iqpAccpApp);
        return new ResultDto<>(result);
    }

    /**
     * 银承申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("银承申请通用的保存方法")
    @PostMapping("/commonsaveiqpaccpappinfo")
    public ResultDto<Map> commonSaveIqpAccpAppInfo(@RequestBody Map params){
        Map rtnData = iqpAccpAppService.commonSaveIqpAccpAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:银承申请待发起列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("银承申请待发起列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpAccpApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpAccpApp> list = iqpAccpAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpAccpApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:银承申请历史列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("银承申请历史列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpAccpApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpAccpApp> list = iqpAccpAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpAccpApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("银承合同申请明细展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpAccpApp temp = new IqpAccpApp();
        IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey((String)map.get("serno"));
        if (iqpAccpApp != null) {
            resultDto.setCode(0);
            resultDto.setData(iqpAccpApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(0);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpAccpAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * 银承合同申请逻辑删除方法
     * @param iqpAccpApp
     * @return
     */
    @ApiOperation("银承合同申请逻辑删除方法")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpAccpApp iqpAccpApp){
        Integer result = iqpAccpAppService.logicDelete(iqpAccpApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:iqpAccpAppSubmitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/iqpAccpAppSubmitNoFlow")
    protected ResultDto<Map> iqpAccpAppSubmitNoFlow(@RequestBody String serno) throws URISyntaxException {
        Map rtnData = iqpAccpAppService.iqpAccpAppSubmitNoFlow(serno);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:showDetialByContNo
     * @函数描述:通过合同号查询单条数据
     * @参数与返回说明:
     * @算法描述:
     * @创建者:zhangliang15
     */
    @ApiOperation("通过合同号查询单条数据")
    @PostMapping("/showDetialByContNo")
    protected ResultDto<Object> showDetialByContNo(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("contNo",(String)map.get("contNo"));
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpAccpApp iqpAccpApp = iqpAccpAppService.queryIqpAccpAppDataByParams(queryData);
        if (iqpAccpApp != null) {
            resultDto.setCode(200);
            resultDto.setData(iqpAccpApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(300);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }
}
