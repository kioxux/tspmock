/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanStpUnstpApp
 * @类描述: coop_plan_stp_unstp_app数据实体类
 * @功能描述: 
 * @创建人: AbsonZ
 * @创建时间: 2021-04-17 09:18:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_plan_stp_unstp_app")
public class CoopPlanStpUnstpApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 申请类型 **/
	@Column(name = "app_type", unique = false, nullable = true, length = 10)
	private String appType;
	
	/** 合作方案编号 **/
	@Column(name = "coop_plan_no", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方编号 **/
	@Column(name = "partner_no", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "partner_name", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 总合作额度（元） **/
	@Column(name = "totl_coop_lmt_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totlCoopLmtAmt;
	
	/** 合作起始日 **/
	@Column(name = "coop_start_date", unique = false, nullable = true, length = 20)
	private String coopStartDate;
	
	/** 合作到期日 **/
	@Column(name = "coop_end_date", unique = false, nullable = true, length = 20)
	private String coopEndDate;
	
	/** 申请原因 **/
	@Column(name = "app_resn", unique = false, nullable = true, length = 500)
	private String appResn;
	
	/** 审批状态 **/
	@Column(name = "appr_status", unique = false, nullable = true, length = 10)
	private String apprStatus;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "create_time", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "update_time", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param totlCoopLmtAmt
	 */
	public void setTotlCoopLmtAmt(java.math.BigDecimal totlCoopLmtAmt) {
		this.totlCoopLmtAmt = totlCoopLmtAmt;
	}
	
    /**
     * @return totlCoopLmtAmt
     */
	public java.math.BigDecimal getTotlCoopLmtAmt() {
		return this.totlCoopLmtAmt;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate;
	}
	
    /**
     * @return coopStartDate
     */
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate;
	}
	
    /**
     * @return coopEndDate
     */
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}
	
    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}