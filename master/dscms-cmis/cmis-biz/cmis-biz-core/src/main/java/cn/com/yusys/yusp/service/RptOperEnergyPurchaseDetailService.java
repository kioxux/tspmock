/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.RptOperProductionOper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperEnergyPurchaseDetail;
import cn.com.yusys.yusp.repository.mapper.RptOperEnergyPurchaseDetailMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergyPurchaseDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:49:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperEnergyPurchaseDetailService {

    @Autowired
    private RptOperEnergyPurchaseDetailMapper rptOperEnergyPurchaseDetailMapper;

    @Autowired
    private RptOperProductionOperService rptOperProductionOperService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptOperEnergyPurchaseDetail selectByPrimaryKey(String pkId) {
        return rptOperEnergyPurchaseDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptOperEnergyPurchaseDetail> selectAll(QueryModel model) {
        List<RptOperEnergyPurchaseDetail> records = (List<RptOperEnergyPurchaseDetail>) rptOperEnergyPurchaseDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptOperEnergyPurchaseDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperEnergyPurchaseDetail> list = rptOperEnergyPurchaseDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptOperEnergyPurchaseDetail record) {
        return rptOperEnergyPurchaseDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptOperEnergyPurchaseDetail record) {
        return rptOperEnergyPurchaseDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptOperEnergyPurchaseDetail record) {
        return rptOperEnergyPurchaseDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptOperEnergyPurchaseDetail record) {
        return rptOperEnergyPurchaseDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptOperEnergyPurchaseDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperEnergyPurchaseDetailMapper.deleteByIds(ids);
    }

    public int insertPurchaseDetail(RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail) {
        int count = 0;
        RptOperProductionOper rptOperProductionOper = new RptOperProductionOper();
        rptOperProductionOper.setSerno(rptOperEnergyPurchaseDetail.getSerno());
        rptOperProductionOper.setUpperstreamCusAnaly("");
        rptOperProductionOper.setBuyOtherNeedDesc("");
        count += rptOperProductionOperService.updateSelective(rptOperProductionOper);
        count += rptOperEnergyPurchaseDetailMapper.insertSelective(rptOperEnergyPurchaseDetail);
        return count;
    }
}
