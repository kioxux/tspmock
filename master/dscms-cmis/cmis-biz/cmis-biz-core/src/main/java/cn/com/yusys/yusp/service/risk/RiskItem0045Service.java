package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/19 19:06
 * @desc 零售产品首付款校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0045Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0045Service.class);
    @Autowired
    private IqpHouseService iqpHouseService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/7/19 19:08
     * @version 1.0.0
     * @desc 零售产品首付款校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0045(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        log.info("*************零售产品首付款校验***********【{}】", iqpSerno);
        String prdType = StringUtils.EMPTY;
        BigDecimal totalAmt = BigDecimal.ZERO;
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        // 通过申请流水号获取申请信息
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        //按揭,校验房屋信息是否输入
        if (null != iqpLoanApp && !StringUtils.isBlank(iqpLoanApp.getPrdId())) {
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
            //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
            prdType = CfgPrdBasicinfoDto.getData().getPrdType();
            ;
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04501); //业务流水号为空
            return riskResultDto;
        }
        if (prdType.equals("10")) {
            // 按揭类
            // 1、申请住房贷款时，首付金额不能小于房屋总价的20%
            // 2、按揭类贷款业务申请时，校验首付金额+贷款金额必须小于等于购房总额
            // 3、如果为公积金组合贷款，首付+商贷+公积金贷款大于房价，则拦截
            IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);
            if (null == iqpHouse || null == iqpHouse.getHouseTotal() || null == iqpHouse.getFirstpayAmt()) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04502);
                return riskResultDto;
            } else {
                if (iqpHouse.getHouseTotal().multiply(new BigDecimal("0.2")).compareTo(iqpHouse.getFirstpayAmt()) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04503);
                    return riskResultDto;
                }

                if (CmisCommonConstants.YES_NO_1.equals(iqpLoanApp.getFundUnionFlag())) {
                    totalAmt = iqpHouse.getFirstpayAmt().add(iqpLoanApp.getAppAmt()).add(iqpHouse.getPundAmt()); //首付+商贷+公积金贷款
                    log.info("首付+商贷+公积金贷款金额总和为：【{}】，房价为：【{}】", totalAmt.toPlainString(), iqpHouse.getHouseTotal().toPlainString());
                    if (totalAmt.compareTo(iqpHouse.getHouseTotal()) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04518);
                        return riskResultDto;
                    }
                } else {
                    totalAmt = iqpHouse.getFirstpayAmt().add(iqpLoanApp.getAppAmt()); // 首付金额+贷款金额
                    log.info("首付金额+贷款金额总和为：【{}】，房价为：【{}】", totalAmt.toPlainString(), iqpHouse.getHouseTotal().toPlainString());
                    if (totalAmt.compareTo(iqpHouse.getHouseTotal()) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04504);
                        return riskResultDto;
                    }
                }
            }

        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************零售产品首付款校验***********【{}】", iqpSerno);
        return riskResultDto;
    }
}
