/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtIntbankAccSub;
import cn.com.yusys.yusp.domain.LmtIntbankApprSub;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChgSub;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankAccSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAccSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 14:22:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankAccSubService extends BizInvestCommonService{

    @Autowired
    private LmtIntbankAccSubMapper lmtIntbankAccSubMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService ;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankAccSub selectByPrimaryKey(String pkId) {
        return lmtIntbankAccSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankAccSub> selectAll(QueryModel model) {
        List<LmtIntbankAccSub> records = (List<LmtIntbankAccSub>) lmtIntbankAccSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankAccSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAccSub> list = lmtIntbankAccSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankAccSub record) {
        return lmtIntbankAccSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankAccSub record) {
        return lmtIntbankAccSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankAccSub record) {
        return lmtIntbankAccSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankAccSub record) {
        return lmtIntbankAccSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankAccSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankAccSubMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: initLmtIntbankAccSubInfo
     * @方法描述: 根据同业授信审批分项表 或 同业授信申请表产生 台账分项信息
     * @参数与返回说明: lmtIntbankAccSub
     * @算法描述: 无
     */

    public LmtIntbankAccSub initLmtIntbankAccSubInfo(LmtIntbankApprSub subBean, String lmtType) throws Exception {
        LmtIntbankAccSub lmtIntbankAccSub = null ;
        //同业授信申请表，期限，起始日，到期日等字段需要重新赋值
        Integer lmtTerm = subBean.getLmtTerm() ;
        if(lmtTerm==null) lmtTerm=0 ;
        //是否变更标识
        String chgFlag = subBean.getChgFlag() ;
        String origiLmtAccSubNo = subBean.getOrigiLmtAccSubNo() ;

        if(CmisLmtConstants.STD_SX_LMT_TYPE_02.equals(lmtType) && StringUtils.nonBlank(origiLmtAccSubNo)){
            QueryModel queryModel = new QueryModel() ;
            queryModel.addCondition("accSubNo", origiLmtAccSubNo);
            BizInvestCommonService.checkParamsIsNull("accSubNo",origiLmtAccSubNo);
            List<LmtIntbankAccSub> list = lmtIntbankAccSubService.selectByModel(queryModel) ;
            if(list!=null && list.size()>0){
                lmtIntbankAccSub = list.get(0);
            }else{
                throw new Exception(EclEnum.ECL070113.value) ;
            }

            //起始日期
            String startDate = lmtIntbankAccSub.getStartDate() ;
            String pkId = lmtIntbankAccSub.getPkId() ;
            BeanUtils.copyProperties(subBean, lmtIntbankAccSub);
            lmtIntbankAccSub.setPkId(pkId);
            //到期日
            String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
            //期限
            lmtIntbankAccSub.setLmtTerm(lmtTerm);
            //到日期
            lmtIntbankAccSub.setEndDate(endDate);

            //温昕要求，变更责任人要改
            lmtIntbankAccSub.setInputBrId(subBean.getInputBrId());
            lmtIntbankAccSub.setInputId(subBean.getInputId());
        }else{
            //生成主键
            Map paramMap= new HashMap<>() ;
            String pkValue = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, paramMap);
            String accSubNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.INTBANK_LMT_ACC_SUB_SEQ, paramMap);

            lmtIntbankAccSub = new LmtIntbankAccSub() ;
            BeanUtils.copyProperties(subBean, lmtIntbankAccSub);
            //设置主键
            lmtIntbankAccSub.setPkId(pkValue);
            lmtIntbankAccSub.setAccSubNo(accSubNo);
            //起始日期
            String startDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT) ;
            //到期日
            String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
            //期限
            lmtIntbankAccSub.setLmtTerm(lmtTerm);
            //起始日期
            lmtIntbankAccSub.setStartDate(startDate);
            //到日期
            lmtIntbankAccSub.setEndDate(endDate);
            //登记日期
            lmtIntbankAccSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            //创建日期
            lmtIntbankAccSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //操作类型
            //lmtIntbankAccSub.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
        }
        //最新更新日期
        lmtIntbankAccSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //更新日期
        lmtIntbankAccSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtIntbankAccSub ;
    }

    /**
     * 根据台账编号查询
     * @param accNo
     * @return
     */
    public List<LmtIntbankAccSub> selectByAccNo(String accNo){
        return lmtIntbankAccSubMapper.selectByAccNo(accNo);
    }

    /**
     * 根据申请流水号查询
     * @param serno
     * @return
     */
    public List<LmtIntbankAccSub> selectBySerno(String serno){
        return lmtIntbankAccSubMapper.selectBySerno(serno);
    }

    /**
     * 根据分项额度编号查询
     * @param accSubNo
     * @return
     */
    public LmtIntbankAccSub selectByAccSubNo(String accSubNo){
        return lmtIntbankAccSubMapper.selectByAccSubNo(accSubNo);
    }

    /**
     * 逻辑删除台账原分项信息
     * @param accNo
     */
    public void deleteLogicByAccNo(String accNo) {
        lmtIntbankAccSubMapper.deleteLogicByAccNo(accNo);
    }

    /**
     * 批复变更-生成台账信息
     * @param lmtIntbankReplyChgSub
     * @param serno
     * @param newReplySerno
     * @param currentOrgId
     * @param currentUserId
     * @param accNo
     */
    public void insertAccSub(LmtIntbankReplyChgSub lmtIntbankReplyChgSub, String serno, String newReplySerno, String currentOrgId, String currentUserId, String accNo) {
        LmtIntbankAccSub lmtIntbankAccSub = new LmtIntbankAccSub();
        copyProperties(lmtIntbankReplyChgSub,lmtIntbankAccSub);
        lmtIntbankAccSub.setAccSubNo(generateSerno(CmisBizConstants.BIZ_SERNO));
        //使用台账流水号
        lmtIntbankAccSub.setAccNo(accNo);
        //使用原申请流水号
        lmtIntbankAccSub.setSerno(serno);
        //使用新的批复流水号
        lmtIntbankAccSub.setReplySubSerno(newReplySerno);
        //初始化新增通用属性信息
        initInsertDomainProperties(lmtIntbankAccSub,currentUserId,currentOrgId);
        insert(lmtIntbankAccSub);
    }



    /**
     * 根据批复分项流水号获取分项台账信息
     * @param replySubSerno
     * @return
     */
    public LmtIntbankAccSub selectByReplySubSerno(String replySubSerno){
        return lmtIntbankAccSubMapper.selectByReplySubSerno(replySubSerno);
    }
}
