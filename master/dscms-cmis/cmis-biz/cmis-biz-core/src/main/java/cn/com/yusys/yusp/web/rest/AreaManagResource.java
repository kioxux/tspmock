/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AreaManag;
import cn.com.yusys.yusp.service.AreaManagService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaManagResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zlf
 * @创建时间: 2021-04-12 17:24:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/areamanag")
public class AreaManagResource {
    @Autowired
    private AreaManagService areaManagService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AreaManag>> query() {
        QueryModel queryModel = new QueryModel();
        List<AreaManag> list = areaManagService.selectAll(queryModel);
        return new ResultDto<List<AreaManag>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AreaManag>> index(QueryModel queryModel) {
        List<AreaManag> list = areaManagService.selectByModel(queryModel);
        return new ResultDto<List<AreaManag>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{areaNo}")
    protected ResultDto<AreaManag> show(@PathVariable("areaNo") String areaNo) {
        AreaManag areaManag = areaManagService.selectByPrimaryKey(areaNo);
        return new ResultDto<AreaManag>(areaManag);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<AreaManag> create(@RequestBody AreaManag areaManag) throws URISyntaxException {
        areaManagService.insert(areaManag);
        return new ResultDto<AreaManag>(areaManag);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AreaManag areaManag) throws URISyntaxException {
        int result = areaManagService.update(areaManag);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{areaNo}")
    protected ResultDto<Integer> delete(@PathVariable("areaNo") String areaNo) {
        int result = areaManagService.deleteByPrimaryKey(areaNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = areaManagService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
