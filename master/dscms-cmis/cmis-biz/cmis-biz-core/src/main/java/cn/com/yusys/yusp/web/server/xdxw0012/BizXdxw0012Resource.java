package cn.com.yusys.yusp.web.server.xdxw0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.XDXW0012.Xdxw0012Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0012.req.Xdxw0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0012.resp.Xdxw0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * 接口处理类:小贷授信申请文本生成pdf
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0012:小贷授信申请文本生成pdf")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0012Resource.class);

    @Resource
    private Xdxw0012Service xdxw0012Service;

    /**
     * 交易码：xdxw0012
     * 交易描述：小贷授信申请文本生成pdf
     *
     * @param xdxw0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷授信申请文本生成pdf")
    @PostMapping("/xdxw0012")
    protected @ResponseBody
    ResultDto<Xdxw0012DataRespDto> xdxw0012(@Validated @RequestBody Xdxw0012DataReqDto xdxw0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataReqDto));
        Xdxw0012DataRespDto xdxw0012DataRespDto = new Xdxw0012DataRespDto();// 响应Dto:小贷授信申请文本生成pdf
        ResultDto<Xdxw0012DataRespDto> xdxw0012DataResultDto = new ResultDto<>();

        try {
            String cusName = xdxw0012DataReqDto.getCusName();//客户名称
            String certNo = xdxw0012DataReqDto.getCertNo();//身份证号码
            String certType = xdxw0012DataReqDto.getCertType();//身份证件类型
            String applyDate = xdxw0012DataReqDto.getApplyDate();//申请日期
            String lmtLmtType = xdxw0012DataReqDto.getLmtLmtType();//授信文件类型
            String cus_type = xdxw0012DataReqDto.getCus_type();////查询类型 01企业；02个人
            String cusname_type = xdxw0012DataReqDto.getCusname_type();

            if (StringUtil.isEmpty(cusName)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("客户名称【cusName】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(certNo)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("身份证号码【certNo】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(lmtLmtType)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("授信文件类型【lmtLmtType】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(cus_type)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("查询类型【cus_type】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(cusname_type)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("授权人类型【cusname_type】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(certType)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("证件类型【certType】不能为空！");
                return xdxw0012DataResultDto;
            }
            if (StringUtil.isEmpty(applyDate)) {
                xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0012DataResultDto.setMessage("申请日期【applyDate】不能为空！");
                return xdxw0012DataResultDto;
            }

            // 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataReqDto));
            xdxw0012DataRespDto = xdxw0012Service.updateSignStatByCommonCertNo(xdxw0012DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataRespDto));
            // 调用XXXXXService层结束
            // 封装xdxw0012DataResultDto中正确的返回码和返回信息
            xdxw0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, e.getMessage());
            // 封装xdxw0012DataResultDto中异常返回码和返回信息
            xdxw0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0012DataRespDto到xdxw0012DataResultDto中
        xdxw0012DataResultDto.setData(xdxw0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataResultDto));
        return xdxw0012DataResultDto;
    }
}
