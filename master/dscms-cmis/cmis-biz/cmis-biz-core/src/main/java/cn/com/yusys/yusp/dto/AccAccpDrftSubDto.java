package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

public class AccAccpDrftSubDto {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 借据编号 **/
    private String billNo;

    /** 核心银承编号 **/
    private String coreBillNo;

    /** 合同编号 **/
    private String contNo;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

    /** 产品类型属性 **/
    private String prdTypeProp;

    /** 担保方式 **/
    private String guarMode;

    /** 是否电子票据 **/
    private String isEDrft;

    /** 汇票号码 **/
    private String porderNo;

    /** 是否他行代签 **/
    private String isOtherBankSign;

    /** 币种 **/
    private String curType;

    /** 票据金额 **/
    private java.math.BigDecimal draftAmt;

    /** 出票日期 **/
    private String isseDate;

    /** 到期日期 **/
    private String endDate;

    /** 保证金比例 **/
    private java.math.BigDecimal bailPerc;

    /** 保证金金额 **/
    private java.math.BigDecimal bailAmt;

    /** 敞口金额 **/
    private java.math.BigDecimal spacAmt;

    /** 收款人名称 **/
    private String pyeeName;

    /** 收款人账号 **/
    private String pyeeAccno;

    /** 收款人开户行行号 **/
    private String pyeeAcctsvcrNo;

    /** 收款人开户行名称 **/
    private String pyeeAcctsvcrName;

    /** 承兑行类型 **/
    private String aorgType;

    /** 承兑行行号 **/
    private String aorgNo;

    /** 承兑行名称 **/
    private String aorgName;

    /** 出票人开户行账号 **/
    private String daorgNo;

    /** 出票人开户户名 **/
    private String daorgName;

    /** 是否使用授信额度 **/
    private String isUtilLmt;

    /** 授信台账编号 **/
    private String lmtAccNo;

    /** 批复编号 **/
    private String replyNo;

    /** 账务机构编号 **/
    private String finaBrId;

    /** 账务机构名称 **/
    private String finaBrIdName;

    /** 签发机构编号 **/
    private String issuedOrgNo;

    /** 签发机构名称 **/
    private String issuedOrgName;

    /** 兑付机构编号 **/
    private String payOrgNo;

    /** 兑付机构名称 **/
    private String payOrgName;

    /** 五级分类 **/
    private String fiveClass;

    /** 十级分类 **/
    private String tenClass;

    /** 分类日期 **/
    private String classDate;

    /** 台账状态 **/
    private String accStatus;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 主管客户经理 **/
    private String managerId;

    /** 主管机构 **/
    private String managerBrId;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    private String inputIdName;

    private String inputBrIdName;

    private String managerIdName;

    private String managerBrIdName;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCoreBillNo() {
        return coreBillNo;
    }

    public void setCoreBillNo(String coreBillNo) {
        this.coreBillNo = coreBillNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getIsEDrft() {
        return isEDrft;
    }

    public void setIsEDrft(String isEDrft) {
        this.isEDrft = isEDrft;
    }

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public String getIsOtherBankSign() {
        return isOtherBankSign;
    }

    public void setIsOtherBankSign(String isOtherBankSign) {
        this.isOtherBankSign = isOtherBankSign;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getDraftAmt() {
        return draftAmt;
    }

    public void setDraftAmt(BigDecimal draftAmt) {
        this.draftAmt = draftAmt;
    }

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(BigDecimal bailPerc) {
        this.bailPerc = bailPerc;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public BigDecimal getSpacAmt() {
        return spacAmt;
    }

    public void setSpacAmt(BigDecimal spacAmt) {
        this.spacAmt = spacAmt;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAccno() {
        return pyeeAccno;
    }

    public void setPyeeAccno(String pyeeAccno) {
        this.pyeeAccno = pyeeAccno;
    }

    public String getPyeeAcctsvcrNo() {
        return pyeeAcctsvcrNo;
    }

    public void setPyeeAcctsvcrNo(String pyeeAcctsvcrNo) {
        this.pyeeAcctsvcrNo = pyeeAcctsvcrNo;
    }

    public String getPyeeAcctsvcrName() {
        return pyeeAcctsvcrName;
    }

    public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
        this.pyeeAcctsvcrName = pyeeAcctsvcrName;
    }

    public String getAorgType() {
        return aorgType;
    }

    public void setAorgType(String aorgType) {
        this.aorgType = aorgType;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getDaorgNo() {
        return daorgNo;
    }

    public void setDaorgNo(String daorgNo) {
        this.daorgNo = daorgNo;
    }

    public String getDaorgName() {
        return daorgName;
    }

    public void setDaorgName(String daorgName) {
        this.daorgName = daorgName;
    }

    public String getIsUtilLmt() {
        return isUtilLmt;
    }

    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getIssuedOrgNo() {
        return issuedOrgNo;
    }

    public void setIssuedOrgNo(String issuedOrgNo) {
        this.issuedOrgNo = issuedOrgNo;
    }

    public String getIssuedOrgName() {
        return issuedOrgName;
    }

    public void setIssuedOrgName(String issuedOrgName) {
        this.issuedOrgName = issuedOrgName;
    }

    public String getPayOrgNo() {
        return payOrgNo;
    }

    public void setPayOrgNo(String payOrgNo) {
        this.payOrgNo = payOrgNo;
    }

    public String getPayOrgName() {
        return payOrgName;
    }

    public void setPayOrgName(String payOrgName) {
        this.payOrgName = payOrgName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getTenClass() {
        return tenClass;
    }

    public void setTenClass(String tenClass) {
        this.tenClass = tenClass;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getInputIdName() {
        return inputIdName;
    }

    public void setInputIdName(String inputIdName) {
        this.inputIdName = inputIdName;
    }

    public String getInputBrIdName() {
        return inputBrIdName;
    }

    public void setInputBrIdName(String inputBrIdName) {
        this.inputBrIdName = inputBrIdName;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }
}
