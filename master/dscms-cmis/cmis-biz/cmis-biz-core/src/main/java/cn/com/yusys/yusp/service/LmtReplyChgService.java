/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtReplyChgSubDto;
import cn.com.yusys.yusp.dto.LmtReplyChgSubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.LmtReplyChgMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 10:22:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyChgService {

    private final Logger log = LoggerFactory.getLogger(LmtReplyChgService.class);
    @Autowired
    private LmtReplyChgMapper lmtReplyChgMapper;
    @Autowired
    private LmtReplyService lmtReplyService;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private LmtReplySubService lmtReplySubService;
    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;
    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;
    @Autowired
    private LmtReplyChgSubService lmtReplyChgSubService;
    @Autowired
    private LmtReplyChgSubPrdService lmtReplyChgSubPrdService;
    @Autowired
    private LmtReplyChgCondService lmtReplyChgCondService;
    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplyChg selectByPrimaryKey(String pkId) {
        return lmtReplyChgMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplyChg> selectAll(QueryModel model) {
        List<LmtReplyChg> records = (List<LmtReplyChg>) lmtReplyChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyChg> list = lmtReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyChg record) {
        return lmtReplyChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyChg record) {
        return lmtReplyChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(LmtReplyChg record) {
        return lmtReplyChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyChg record) {
        return lmtReplyChgMapper.updateSelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyChgMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyChgMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：queryByManagerId
     * @方法描述：当前登录人下的批复变更清单
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-27 下午 9:19
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyChg> queryByManagerId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        String managerId = userInfo.getLoginCode();
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.addCondition("managerId", managerId);
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.addCondition("apprStatuss", apprStatuss);
        List<LmtReplyChg> list = lmtReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称：insertAllReplyChg
     * @方法描述：这里把批复表的数据新增到批复变更一系列表
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-26 下午 9:19
     * @修改记录：修改时间 修改人员  修改原因
     */
    @Transactional
    public String insertAllReplyChg(LmtReplyChg lmtReplyChg) {
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        // 批复变更流水号
        String serno = "";
        // 校验参数
        if(StringUtils.isBlank(lmtReplyChg.getReplySerno())){
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        // 校验当前批复编号是否存在在途变更
        if(checkIsAppringLmtReplyChg(lmtReplyChg.getReplySerno())){
            throw BizException.error(null, EcbEnum.ECB010094.key, EcbEnum.ECB010094.value);
        }

        log.info("单一客户授信批复变更新增逻辑开始,批复编号为[{}]" , lmtReplyChg.getReplySerno());
        serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_CHG_SERNO, new HashMap<>());
        if(StringUtils.isBlank(serno)){
            throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
        }
        log.info("单一客户授信批复变更新增逻辑开始,生成流水号为[{}]" , lmtReplyChg.getReplySerno());
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyChg.getReplySerno());

        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null){
            throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
        }

        // 将批复变更数据生成
        BeanUtils.beanCopy(lmtReply, lmtReplyChg);
        lmtReplyChg.setReplyResult(lmtReply.getApprResult());
        lmtReplyChg.setPkId(UUID.randomUUID().toString());
        lmtReplyChg.setSerno(serno);
        lmtReplyChg.setApproveStatus(CmisFlowConstants.WF_STATUS_000);
        lmtReplyChg.setInputId(userInfo.getLoginCode());
        lmtReplyChg.setInputBrId(userInfo.getOrg().getCode());
        lmtReplyChg.setInputDate(openday);
        lmtReplyChg.setUpdId(userInfo.getLoginCode());
        lmtReplyChg.setUpdBrId(userInfo.getOrg().getCode());
        lmtReplyChg.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        lmtReplyChg.setManagerId(userInfo.getLoginCode());
        lmtReplyChg.setManagerBrId(userInfo.getOrg().getCode());
        lmtReplyChg.setCreateTime(DateUtils.getCurrDate());
        lmtReplyChg.setUpdateTime(DateUtils.getCurrDate());
        this.insert(lmtReplyChg);
        log.info("单一客户授信批复变更新增,插入批复变更数据[{}]" , JSON.toJSONString(lmtReplyChg));

        HashMap<String, String> querySubMap = new HashMap<String, String>();
        querySubMap.put("replySerno",lmtReplyChg.getReplySerno());
        querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplySub> lmtReplyChgSubList = lmtReplySubService.queryLmtReplySubByParams(querySubMap);
        for (LmtReplySub lmtReplySub: lmtReplyChgSubList) {
            String subSerno = "";
            LmtReplyChgSub lmtReplyChgSub = new LmtReplyChgSub();
            lmtReplyChgSub = BeanUtils.beanCopy(lmtReplySub, lmtReplyChgSub);
            subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_CHG_SERNO, new HashMap<>());
            if(StringUtils.isBlank(subSerno)){
                throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
            }
            lmtReplyChgSub.setSerno(serno);
            lmtReplyChgSub.setSubSerno(subSerno);
            lmtReplyChgSub.setPkId(UUID.randomUUID().toString());
            lmtReplyChgSub.setInputId(userInfo.getLoginCode());
            lmtReplyChgSub.setInputBrId(userInfo.getOrg().getCode());
            lmtReplyChgSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            lmtReplyChgSub.setUpdId(userInfo.getLoginCode());
            lmtReplyChgSub.setUpdBrId(userInfo.getOrg().getCode());
            lmtReplyChgSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            lmtReplyChgSub.setCreateTime(DateUtils.getCurrDate());
            lmtReplyChgSub.setUpdateTime(DateUtils.getCurrDate());
            lmtReplyChgSubService.insert(lmtReplyChgSub);
            log.info("单一客户授信批复变更新增,插入批复变更分项数据[{}]" , lmtReplyChg.toString());

            HashMap<String, String> querySubPrdMap = new HashMap<String, String>();
            querySubPrdMap.put("replySubSerno", lmtReplySub.getReplySubSerno());
            querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtReplySubPrd> lmtReplySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(querySubPrdMap);
            for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                String subPrdSerno = "";
                LmtReplyChgSubPrd lmtReplyChgSubPrd = new LmtReplyChgSubPrd();
                lmtReplyChgSubPrd = BeanUtils.beanCopy(lmtReplySubPrd, lmtReplyChgSubPrd);
                subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_CHG_SERNO, new HashMap<>());
                if(StringUtils.isBlank(subSerno)){
                    throw BizException.error(null, EcbEnum.ECB010003.key, EcbEnum.ECB010003.value);
                }
                lmtReplyChgSubPrd.setSubPrdSerno(subPrdSerno);
                lmtReplyChgSubPrd.setSubSerno(subSerno);
                lmtReplyChgSubPrd.setPkId(UUID.randomUUID().toString());
                lmtReplyChgSubPrd.setInputId(userInfo.getLoginCode());
                lmtReplyChgSubPrd.setInputBrId(userInfo.getOrg().getCode());
                lmtReplyChgSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                lmtReplyChgSubPrd.setUpdId(userInfo.getLoginCode());
                lmtReplyChgSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                lmtReplyChgSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                lmtReplyChgSubPrd.setCreateTime(DateUtils.getCurrDate());
                lmtReplyChgSubPrd.setUpdateTime(DateUtils.getCurrDate());
                lmtReplyChgSubPrdService.insert(lmtReplyChgSubPrd);
                log.info("单一客户授信批复变更新增,插入批复变更分项品种数据[{}]" , lmtReplyChg.toString());
            }
        }
        HashMap<String, String> queryLoanCondMap = new HashMap<String, String>();
        queryLoanCondMap.put("replySerno", lmtReplyChg.getReplySerno());
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByParams(queryLoanCondMap);
        for (LmtReplyLoanCond lmtReplyLoanCond : lmtReplyLoanCondList) {
            LmtReplyChgCond lmtReplyChgCond = new LmtReplyChgCond();
            lmtReplyChgCond = BeanUtils.beanCopy(lmtReplyLoanCond, lmtReplyChgCond);
            lmtReplyChgCond.setPkId(UUID.randomUUID().toString());
            lmtReplyChgCond.setSerno(serno);
            lmtReplyChgCond.setCreateTime(DateUtils.getCurrDate());
            lmtReplyChgCond.setUpdateTime(DateUtils.getCurrDate());
            lmtReplyChgCondService.insert(lmtReplyChgCond);
            log.info("单一客户授信批复变更新增,插入批复变更分项品种数据[{}]" , lmtReplyChg.toString());
        }
        return serno;
    }

    private boolean checkIsAppringLmtReplyChg(String replySerno) {
        boolean resBoolean = false;
        HashMap map = new HashMap();
        map.put("replySerno",replySerno);
        map.put("oprType",CmisCommonConstants.OP_TYPE_01);
        map.put("approveStatuss",CmisCommonConstants.WF_STATUS_000992111);
        log.info("单一客户批复变更校验开始，单一批复编号为：" + replySerno);
        List<LmtReplyChg> lmtReplyChgs = lmtReplyChgMapper.queryLmtReplyChgDataByParams(map);
        if(lmtReplyChgs.size() > 0){
            resBoolean = true;
        }
        return resBoolean;
    }

    /**
     * @方法名称：queryLmtReplyChgDataByParams
     * @方法描述：根据条件查批复变更
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-30 上午 9:15
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyChg> queryLmtReplyChgDataByParams(HashMap<String, String> paramMap) {
        return lmtReplyChgMapper.queryLmtReplyChgDataByParams(paramMap);
    }

    /**
     * @方法名称：getAllReplyChgInfo
     * @方法描述：获取完整的批复变更信息
     * @参数与返回说明：
     * @算法描述：根据批复变更流水号获取
     * @创建人：zhangming12
     * @创建时间：2021-04-30 上午 9:45
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map<String, Object> getAllReplyChgInfo(HashMap<String, String> paramMap) {
        Map<String, Object> rtnData = new HashMap<>(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        LmtReplyChg lmtReplyChg = null;
        List<LmtReplyChgCond> lmtReplyChgCondList = null;
        List<LmtReplyChgSubDto> lmtReplyChgSubDtos = null;
        try {
            List<LmtReplyChg> lmtReplyChgList = queryLmtReplyChgDataByParams(paramMap);
            lmtReplyChg = lmtReplyChgList.get(0);
            List<LmtReplyChgSub> lmtReplyChgSubs = lmtReplyChgSubService.queryLmtReplyChgSubByParams(paramMap);
            lmtReplyChgSubDtos = (List<LmtReplyChgSubDto>) BeanUtils.beansCopy(lmtReplyChgSubs, LmtReplyChgSubDto.class);
            for (LmtReplyChgSubDto lmtReplyChgSubDto : lmtReplyChgSubDtos) {
                String subSerno = lmtReplyChgSubDto.getSubSerno();
                List<LmtReplyChgSubPrdDto> lmtReplyChgSubPrdDtos = null;
                if (StringUtils.nonBlank(subSerno)) {
                    HashMap<String, String> subSernoMap = new HashMap<>();
                    subSernoMap.put("subSerno", subSerno);
                    subSernoMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtReplyChgSubPrd> lmtReplyChgSubPrdList = lmtReplyChgSubPrdService.queryLmtReplyChgSubPrdByParams(subSernoMap);
                    lmtReplyChgSubPrdDtos = (List<LmtReplyChgSubPrdDto>) BeanUtils.beansCopy(lmtReplyChgSubPrdList, LmtReplyChgSubPrdDto.class);
                    lmtReplyChgSubPrdDtos.forEach(lmtReplyChgSubPrdDto -> {
                        lmtReplyChgSubPrdDto.setLmtChgDrawNo(lmtReplyChgSubPrdDto.getSubPrdSerno());
                        lmtReplyChgSubPrdDto.setLmtChgDrawType(lmtReplyChgSubPrdDto.getLmtBizTypeName());
                    });
                }
                lmtReplyChgSubDto.setChildren(lmtReplyChgSubPrdDtos);
                lmtReplyChgSubDto.setLmtChgDrawNo(lmtReplyChgSubDto.getSubSerno());
                lmtReplyChgSubDto.setLmtChgDrawType(lmtReplyChgSubDto.getSubName());
            }
            lmtReplyChgCondList = lmtReplyChgCondService.queryLmtReplyChgCondByParams(paramMap);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复变更数据数据出现异常！", e);
//            rtnCode = EcbEnum.ECB019999.key;
//            rtnMsg = EcbEnum.ECB019999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtReplyChgSubDtos", lmtReplyChgSubDtos);
            rtnData.put("lmtReplyChg", lmtReplyChg);
            rtnData.put("lmtReplyChgCondList", lmtReplyChgCondList);
            rtnData.put("managerId", lmtReplyChg.getManagerId());
            rtnData.put("managerBrId", lmtReplyChg.getManagerBrId());
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;

    }

    /**
     * @方法名称: queryLmtReplyChgBySerno
     * @方法描述: 通过流水号查询数据
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyChg queryLmtReplyChgBySerno(String serno) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("serno", serno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyChg> lmtReplyChgList = lmtReplyChgMapper.queryLmtReplyChgDataByParams(queryMap);
        if (lmtReplyChgList != null && lmtReplyChgList.size() == 1) {
            return lmtReplyChgList.get(0);
        } else {
            throw BizException.error(null, EcbEnum.ECB010066.key, EcbEnum.ECB010066.value);
        }

    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 授信批复变更流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) throws Exception {
        LmtReplyChg lmtReplyChg = this.queryLmtReplyChgBySerno(serno);
        lmtReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        lmtReplyChgMapper.updateByPrimaryKey(lmtReplyChg);
    }


    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信批复变更流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno) throws Exception {
        // 1.将审批状态更新为998
        LmtReplyChg lmtReplyChg = this.queryLmtReplyChgBySerno(serno);
        lmtReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        lmtReplyChgMapper.updateByPrimaryKey(lmtReplyChg);
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信批复变更流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) throws Exception {
        // 1.将审批状态更新为992
        LmtReplyChg lmtReplyChg = this.queryLmtReplyChgBySerno(serno);
        lmtReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        lmtReplyChgMapper.updateByPrimaryKey(lmtReplyChg);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信批复变更流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.从根据流水号获批复变更数据，根据数据生成批复数据
     * 3.根据批复数据更新批复台账数据
     * 4.将批复台账推送至额度系统
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno, String currentUserId, String currentOrgId) throws Exception {
        // 1.将审批状态更新为997
        LmtReplyChg lmtReplyChg = this.queryLmtReplyChgBySerno(serno);
        lmtReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        this.update(lmtReplyChg);

        // 只更新审批意见
        // 2.从根据流水号获批复变更数据，根据数据生成批复数据
        lmtReplyService.updateLmtReplyHandleByLmtReplyChg(lmtReplyChg, currentUserId, currentOrgId);

        // 3.根据授信批复更新授信批复台账
        lmtReplyAccService.updateLmtReplyAccByReplySerno(lmtReplyChg.getReplySerno());

        // 4.推送授信台账至额度系统
        //lmtReplyAccService.synLmtReplyAccToLmtSys(lmtReplyChg.getCusId());

        log.info("授信批复变更审批业务逻辑处理完成" + serno);
    }

    /**
     * @方法名称：deleteReplyChg
     * @方法描述：逻辑删批复变更
     * @创建人：zhangming12
     * @创建时间：2021/5/14 10:40
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteReplyChg(LmtReplyChg lmtReplyChg) {
        int rowCount = 0;
        String serno = lmtReplyChg.getSerno();
        String approveStatus = lmtReplyChg.getApproveStatus();
        LmtReplyChg newLmtReplyChg = new LmtReplyChg();
        if (CmisCommonConstants.WF_STATUS_992.equals(approveStatus)) {
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(serno);
            newLmtReplyChg.setPkId(lmtReplyChg.getPkId());
            newLmtReplyChg.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            rowCount = this.updateSelective(newLmtReplyChg);
            log.info("打回状态执行自行退出处理!");
            return rowCount;
        }
        newLmtReplyChg.setPkId(lmtReplyChg.getPkId());
        newLmtReplyChg.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        rowCount += this.updateSelective(newLmtReplyChg);
        log.info("逻辑删除处理!");
        rowCount += lmtReplyChgSubService.updateLmtReplyChgSubOprDeleteByChg(serno);
        return rowCount;
    }

    /**
     * @方法名称：getLmtReplyChgHis
     * @方法描述：批復變更歷史
     * @创建人：zhangming12
     * @创建时间：2021/5/14 14:12
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<LmtReplyChg> getLmtReplyChgHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo == null) {
            throw new YuspException(EcbEnum.ECB010004.key, EcbEnum.ECB010004.value + ",登录异常！");
        }
        Map<String, Object> condition = model.getCondition();
        condition.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        condition.put("managerId",userInfo.getLoginCode());
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_990);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_991);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        condition.put("apprStatuss",apprStatuss);
        List<LmtReplyChg> lmtReplyChgList = lmtReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return lmtReplyChgList;
    }
}
