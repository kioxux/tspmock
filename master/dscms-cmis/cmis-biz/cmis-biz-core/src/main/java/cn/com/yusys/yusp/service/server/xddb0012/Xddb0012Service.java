package cn.com.yusys.yusp.service.server.xddb0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.req.Xddb06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.Xddb06RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.dto.server.xddb0012.req.Xddb0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.MortgagorsList;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.Xddb0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0012Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0012Service.class);

    @Resource
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private CommonService commonService;
    @Resource
    private CmisCusClientService cmisCusClientService;
    @Resource
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Resource
    private GrtGuarContMapper grtGuarContMapper;
    @Resource
    private GrtGuarContRelMapper grtGuarContRelMapper;
    @Resource
    private IqpLoanAppMapper iqpLoanAppMapper;
    @Resource
    private IqpAccpAppMapper iqpAccpAppMapper;
    @Resource
    private IqpCvrgAppMapper iqpCvrgAppMapper;
    @Resource
    private IqpTfLocAppMapper iqpTfLocAppMapper;
    @Resource
    private IqpEntrustLoanAppMapper iqpEntrustLoanAppMapper;
    @Resource
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Resource
    private IqpHighAmtAgrAppMapper iqpHighAmtAgrAppMapper;
    @Resource
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;


    /**
     * 抵押登记获取押品信息
     * @param xddb0012DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0012DataRespDto getXddb0012(Xddb0012DataReqDto xddb0012DataReqDto)throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
        Xddb0012DataRespDto xddb0012DataRespDto = new Xddb0012DataRespDto();
        try {
            //押品编号
            String collid = xddb0012DataReqDto.getGuarid();
            String guaranty_id = "";
            xddb0012DataRespDto.setCollid(collid);
            //获取guarid前两个字段判断是新建登记还是新建注销（YP是注销，DW、DY是新建）
            String id = collid.substring(0,2);
            if(!CmisBizConstants.DW.equals(id) && !CmisBizConstants.DY.equals(id)){
                //TODO guarWarrantInApp表已变更 根据核心担保编号查询押品编号
                guaranty_id = guarWarrantInfoMapper.getGuarNoByCoreGrtNo(collid);
                logger.info("根据核心担保编号查询押品编号为[{}]"+collid);
            }else{
                guaranty_id = collid;
            }
            //对象-抵押权人（银行）
            //抵押权人-姓名/名称
            xddb0012DataRespDto.setMoname(CmisBizConstants.MONAME);
            //抵押权人-证件类型-代码
            xddb0012DataRespDto.setDotyco(CmisBizConstants.DOTYCO);
            //抵押权人-证件类型-名称dotyco
            xddb0012DataRespDto.setDotyna(CmisBizConstants.DOTYNA);
            //抵押权人-证件号
            xddb0012DataRespDto.setModomu(CmisBizConstants.MODOMU);
            //抵押权人-法人/负责人
            xddb0012DataRespDto.setMonalr(CmisBizConstants.MONALR);
            //抵押权人-电话
            xddb0012DataRespDto.setMophon(CmisBizConstants.MOPHON);
            //根据押品编号获取押品信息
            GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.queryBaseInfoByGuarId(guaranty_id);
            if(Objects.isNull(guarBaseInfo)){
                throw BizException.error(null, EcbEnum.ECB010018.key,EcbEnum.ECB010018.value);
            }
            //对象-抵押权人代理人（银行办理业务的人，一般指客户经理）
            //抵押权人代理人-姓名/名称
            String moagna = "";
            moagna = guarBaseInfo.getManagerId();
            //抵押权人代理人-证件号
            String moagdn = "";
            //抵押权人代理人-电话
            String moagph = "";
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(moagna);
            if(ResultDto.success().getCode().equals(resultDto.getCode())){
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if(!Objects.isNull(adminSmUserDto)){
                    moagna = adminSmUserDto.getUserName();
                    moagdn = adminSmUserDto.getCertNo();
                    moagph = adminSmUserDto.getUserMobilephone();
                }
            }
            xddb0012DataRespDto.setMoagna(moagna);
            xddb0012DataRespDto.setMoagdn(moagdn);
            xddb0012DataRespDto.setMoagph(moagph);
            //抵押权人代理人-证件类型-代码
            xddb0012DataRespDto.setMoagtc(CmisBizConstants.MOAGTC);
            //抵押权人代理人-证件类型-名称
            xddb0012DataRespDto.setMoagtn(CmisBizConstants.MOAGTN);
            String guarCusId = guarBaseInfo.getGuarCusId();
            String guarCusName = guarBaseInfo.getGuarCusName();
            boolean flag = checkIfComCus(guarCusId);
            List<MortgagorsList> mortgagorsLists = new ArrayList<>();
            //抵押人-姓名/名称
            String mogana = "";
            //抵押人-证件类型
            String modoty = "";
            //抵押人-证件号
            String modonu = "";
            //抵押人-法人/负责人
            String molere = "";
            //抵押人-电话
            String moleph = "";
            if(flag){
                //对公
                //查询根据客户编号查询法人名称,名称，证件类型，证件号，电话 cus_base,cus_indiv,cus_corp_mgr
                //TODO 对公客户phone 取值待定
                CmisCus0008ReqDto cmisCus0008ReqDto = new CmisCus0008ReqDto();
                cmisCus0008ReqDto.setCusId(guarCusId);
                ResultDto<CmisCus0008RespDto> cmisCusResultRespDto = cmisCusClientService.cmiscus0008(cmisCus0008ReqDto);
                if(ResultDto.success().getCode().equals(cmisCusResultRespDto.getCode())){
                    CmisCus0008RespDto cmisCus0008RespDto = cmisCusResultRespDto.getData();
                    if(Objects.isNull(cmisCus0008RespDto)){
                        mogana = cmisCus0008RespDto.getCusName();
                        modoty = cmisCus0008RespDto.getCertType();
                        modoty = getChangeCertCode(modoty);
                        modonu = cmisCus0008RespDto.getCertCode();
                        molere = cmisCus0008RespDto.getMrgName();
                        moleph = cmisCus0008RespDto.getPhone();
                    }
                }
                MortgagorsList mortgagorsList = new MortgagorsList();
                mortgagorsList.setMogana(mogana);
                mortgagorsList.setModoty(modoty);
                mortgagorsList.setModonu(modonu);
                mortgagorsList.setMolere(molere);
                mortgagorsList.setMoleph(moleph);
                mortgagorsLists.add(mortgagorsList);
            }else{
                //对私
                CusBaseDto cusBaseDto1 = commonService.getCusBaseByCusId(guarCusId);
                if(!Objects.isNull(cusBaseDto1)){
	                mogana = cusBaseDto1.getCusName();
	                modoty = cusBaseDto1.getCertType();
	                modoty = getChangeCertCode(modoty);
	                modonu = cusBaseDto1.getCertCode();
                }
                molere = "-";
                moleph = "-";

                MortgagorsList mortgagorsList = new MortgagorsList();
                mortgagorsList.setMogana(mogana);
                mortgagorsList.setModoty(modoty);
                mortgagorsList.setModonu(modonu);
                mortgagorsList.setMolere(molere);
                mortgagorsList.setMoleph(moleph);
                mortgagorsLists.add(mortgagorsList);
                //查询共有人信息
                // 2021-09-07 交易码xdypgyrcx调整为xddb06
//                XdypgyrcxReqDto xdypgyrcxReqDto = new XdypgyrcxReqDto();
//                xdypgyrcxReqDto.setGuaranty_id(guaranty_id);
//                ResultDto<XdypgyrcxRespDto> xdypgyrcxResultRespDto = dscms2YpxtClientService.xdypgyrcx(xdypgyrcxReqDto);
                Xddb06ReqDto xddb06ReqDto = new Xddb06ReqDto();
                xddb06ReqDto.setGuaranty_id(guaranty_id);
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(xddb06ReqDto));
                ResultDto<Xddb06RespDto> resultXddb06RespDto = dscms2YphsxtClientService.xddb06(xddb06ReqDto);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(resultXddb06RespDto));
                if(ResultDto.success().getCode().equals(resultXddb06RespDto.getCode())){
                    Xddb06RespDto xddb06RespDto = resultXddb06RespDto.getData();
                    //java.util.List<cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.List> list =xdypgyrcxRespDto.getList();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List> list = xddb06RespDto.getList();
                    if((CollectionUtils.nonEmpty(list)&&list.size()>0)){
                        for(int i = 0;i<list.size();i++){
                            MortgagorsList mortgagorsListt = new MortgagorsList();
                            mogana = list.get(i).getCommon_owner_name();
                            modoty = list.get(i).getCommon_cert_type();
                            modoty = getChangeCertCode(modoty);
                            modonu = list.get(i).getCommon_cert_code();
                            molere="-";
                            moleph="-";
                            mortgagorsListt.setMogana(mogana);
                            mortgagorsListt.setModoty(modoty);
                            mortgagorsListt.setModonu(modonu);
                            mortgagorsListt.setMolere(molere);
                            mortgagorsListt.setMoleph(moleph);
                            mortgagorsLists.add(mortgagorsListt);
                        }
                        // mogana = xdypgyrcxRespDto.getCommon_owner_name();
                        //TODO 共有人返回缺少证件类型
                        // modoty = xdypgyrcxRespDto.getCommon_cert_type();
                        // modonu = xdypgyrcxRespDto.getCommon_cert_code();

                    }
                }else{
                    logger.error("接口【xdypgyrcx】发送【押品系统】异常！");
                }
            }
            xddb0012DataRespDto.setMortgagorsList(mortgagorsLists);
            //对象-抵押人代理人（无指定就是没有代理人）
            //抵押人代理人-姓名/名称
            xddb0012DataRespDto.setMoorna("");
            //抵押人代理人-证件类型-代码（1-身份证；2-港澳台身份证；3-护照；4-户口簿；5-军官证；6-组织机构代码；7-营业执照；99-其他）
            xddb0012DataRespDto.setMoordc("1");
            //抵押人代理人-证件类型-名称
            xddb0012DataRespDto.setMoordn("");
            //抵押人代理人-证件号
            xddb0012DataRespDto.setMoordu("");
            //抵押人代理人-电话
            xddb0012DataRespDto.setMoorph("");

            //担保分类代码
            String grttyp = guarBaseInfo.getGuarTypeCd();
            String guar_no = guaranty_id;
            if("DY0401001".equals(grttyp)||"B70101".equals(grttyp)) {
                //不动产取子项中房产的数据
                GuarBaseInfo guarBaseInfo1 = guarBaseInfoMapper.getGuarInfoByGuarId("%" + guaranty_id+"-%");
                guar_no = guarBaseInfo1.getGuarNo();
                grttyp = guarBaseInfo1.getGuarTypeCd();
            }
            BdcqcxReqDto bdcqcxReqDto = new BdcqcxReqDto();
            bdcqcxReqDto.setGrttyp(grttyp);
            bdcqcxReqDto.setGuarid(guar_no);
            //不动产信息-所属区县-代码
            String reeidc="";
            //不动产信息-所属区县-名称
            String reeidn="";
            //不动产信息-坐落
            String reeilo="";
            //不动产信息-房屋-不动产单元号
            String reiheu="";
            //不动产信息-房屋-产权证书号
            String reihcn="";
            //区域编码是苏州工业园区(320513)，不动产权证书号去掉中文只传字段中的数字--徐徐确认
            if("320513".equals(reeidc)){
                reihcn=getChangeBookNo(reihcn);
            }
            //不动产信息-房屋-产权证书号简称
            String reihcs="";
            //不动产信息-房屋-产权面积
            BigDecimal reeiha=BigDecimal.ZERO;
            //不动产信息-房屋-用途-代码
            String reihuc="";
            //不动产信息-房屋-用途-名称
            String reihun="";
            //不动产信息-土地-证号
            String reilcn="";
            //不动产信息-土地-面积
            BigDecimal reeila=BigDecimal.ZERO;
            //不动产信息-土地-用途-代码
            String reiluc="";
            //不动产信息-土地-用途-名称
            String reilun="";
            ResultDto<BdcqcxRespDto> bdcqcxRespDtoResultDto = dscms2YpxtClientService.bdcqcx(bdcqcxReqDto);
            if(ResultDto.success().getCode().equals(bdcqcxRespDtoResultDto.getCode())){
                BdcqcxRespDto bdcqcxRespDto = bdcqcxRespDtoResultDto.getData();
                if(true){
                    reeidc = bdcqcxRespDto.getBdqxdm();
                    reeidn = bdcqcxRespDto.getBdqxmc();
                    reeilo = bdcqcxRespDto.getBdqzll();
                    reiheu = bdcqcxRespDto.getBdcdyh();
                    reihcn = bdcqcxRespDto.getBdcnoo();
                    if("320513".equals(reeidc)){//区域编码是苏州工业园区(320513)，不动产权证书号去掉中文只传字段中的数字--徐徐确认
                        reihcn=getChangeBookNo(reihcn);
                    }
                    reihcs = bdcqcxRespDto.getBdcqjc();
                    reeiha = bdcqcxRespDto.getBdcqmj();
                    reihuc = bdcqcxRespDto.getBdytdm();
                    reihun = bdcqcxRespDto.getBdytmc();
                    reilcn = bdcqcxRespDto.getTdzhno();
                    reeila = bdcqcxRespDto.getTdaera();
                    reiluc = bdcqcxRespDto.getTdytdm();
                    reilun = bdcqcxRespDto.getTdytmc();
                }
            }else{
                logger.error("接口【bdcqcx】发送【押品系统】异常！");
            }
            xddb0012DataRespDto.setReeidc(reeidc);
            xddb0012DataRespDto.setReeidn(reeidn);
            xddb0012DataRespDto.setReeilo(reeilo);
            xddb0012DataRespDto.setReiheu(reiheu);
            xddb0012DataRespDto.setReihcn(reihcn);
            xddb0012DataRespDto.setReihcs(reihcs);
            xddb0012DataRespDto.setReeiha(reeiha);
            xddb0012DataRespDto.setReihuc(reihuc);
            xddb0012DataRespDto.setReihun(reihun);
            xddb0012DataRespDto.setReilcn(reilcn);
            xddb0012DataRespDto.setReeila(reeila);
            xddb0012DataRespDto.setReiluc(reiluc);
            xddb0012DataRespDto.setReilun(reilun);
            //对象-抵押信息
            //抵押信息-担保范围
            String migusc="详见合同";
            //抵押信息-抵押物价值(万元)
            BigDecimal micova = BigDecimal.ZERO;
            micova = guarBaseInfo.getMaxMortagageAmt();
            micova = changeAmt(micova);
            //抵押信息-是否包含车库
            String mihaga = "";
            //抵押信息-是否包含阁楼
            String mihalo = "";

            // 将xdypbdccx调整为xddb01
//            XdypbdccxReqDto xdypbdccxReqDto = new XdypbdccxReqDto();
//            xdypbdccxReqDto.setGuaranty_id(guaranty_id);
//            xdypbdccxReqDto.setType(grttyp);
//            ResultDto<XdypbdccxRespDto> xdypbdccxRespDtoResultDto = dscms2YpxtClientService.xdypbdccx(xdypbdccxReqDto);

            /**********************************新信贷直接调用xddb01接口，传入押品编号 guaranty_id 和押品类型 guar_type*****************************************/
            Xddb01ReqDto xddb01ReqDto = new Xddb01ReqDto();
            xddb01ReqDto.setGuaranty_id(guaranty_id);
            xddb01ReqDto.setType(grttyp);
            logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(xddb01ReqDto));
            ResultDto<Xddb01RespDto> resultXddb01RespDto = dscms2YphsxtClientService.xddb01(xddb01ReqDto);
            logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(resultXddb01RespDto));
            if(ResultDto.success().getCode().equals(resultXddb01RespDto.getCode())){
                Xddb01RespDto xddb01RespDto = resultXddb01RespDto.getData();
                if(!Objects.isNull(xddb01RespDto)){
                    mihaga = xddb01RespDto.getIs_carport();
                    mihaga = getChangeYesNo(mihaga);
                    mihalo = xddb01RespDto.getIs_attic();
                    mihalo = getChangeYesNo(mihalo);
                }
            }else{
                logger.error("接口【xddb01】发送【押品系统】异常！");
            }
            xddb0012DataRespDto.setMigusc(migusc);
            xddb0012DataRespDto.setMicova(micova);
            xddb0012DataRespDto.setMihaga(mihaga);
            xddb0012DataRespDto.setMihalo(mihalo);
            //抵押信息-不动产登记证明号
            String micenu="";
            if(CmisBizConstants.DW.equals(id) || CmisBizConstants.DY.equals(id)){
                //抵押信息-不动产登记证明号(入库时客户经理填的，无法获取)
                micenu="";
            }else if(CmisBizConstants.YP.equals(id)){
                //注销时可以获取到不动产登记证明号
                micenu= guarWarrantInfoMapper.getCreditRecordIdByCoreGuarNo(collid);
            }
            xddb0012DataRespDto.setMicenu(micenu);
            //抵押信息-抵押合同编号
            String micono="";
            //抵押信息-被担保债权数额(万元)
            BigDecimal mideam=BigDecimal.ZERO;
            //抵押信息-债务开始时间
            String midsti="";
            //抵押信息-债务结束时间
            String mideti="";
            //抵押信息-债务人-姓名/名称
            String midena="";
            //抵押信息-债务人-证件类型-代码
            String middtc="";
            //抵押信息-债务人-证件类型-名称
            String middtn="";
            //抵押信息-债务人-证件号
            String middnu="";
            //交易信息-买卖合同编号
            String dicono="";
            //交易信息-合同债权金额
            BigDecimal dicova=BigDecimal.ZERO;
            String cus_id="";
            //抵押信息-抵押方式-代码
            String mimmco="";
            //抵押信息-抵押方式-名称
            String mimmna="";
            //抵押顺位
            String miorde = "";
            //根据押品编号查询担保合同编号
            String guarContNo ="";
            //产品编号
            String prd_id = "";
            int num = 0;
            //抵押登记和抵押登记注销获取担保合同信息
            GrtGuarCont grtGuarCont = null;
            if(CmisBizConstants.DW.equals(id) || CmisBizConstants.DY.equals(id)){
                grtGuarCont = grtGuarContMapper.getGrtContInfoByGuarNo(guaranty_id);
            }else if(CmisBizConstants.YP.equals(id)){
                grtGuarCont = grtGuarContMapper.getGrtContInfoByGrpNo(collid);
            }

            if(grtGuarCont!=null){
                //xddb0012DataRespDto.setMicono(grtGuarCont.getGuarContNo());
                micono = grtGuarCont.getGuarContNo();
                //担保类别需要转换类别
                mimmco = grtGuarCont.getGuarContType();
                if("A".equals(mimmco)){
                    mimmco = "1";
                }else{
                    mimmco = "2";
                }
                xddb0012DataRespDto.setMimmco(mimmco);
                xddb0012DataRespDto.setMidsti(grtGuarCont.getGuarStartDate());
                xddb0012DataRespDto.setMideti(grtGuarCont.getGuarEndDate());
                xddb0012DataRespDto.setMiorde(grtGuarCont.getPldOrder());
                //在线抵押优化：担保合同只有一个抵押物，返回担保合同金额；多个，最高额抵押合同返回押品评估价值；一般合同返回空
                guarContNo = grtGuarCont.getGuarContNo();
                num = grtGuarContRelMapper.getCountByGuarNo(guarContNo);
                if(num == 1){
                    mideam = grtGuarCont.getGuarAmt();//抵押信息-被担保债权数额(万元)
                }else{
                    if("1".equals(mimmco)){
                        mideam = BigDecimal.ZERO;
                    }else if("2".equals(mimmco)){
                        mideam = guarBaseInfo.getMaxMortagageAmt();
                    }
                }
            }
            mideam = changeAmt(mideam);
            xddb0012DataRespDto.setMideam(mideam);
            if("1".equals(mimmco)){
                mimmna="一般抵押";
            }else if("2".equals(mimmco)){
                mimmna="最高额抵押";
            }
            xddb0012DataRespDto.setMimmna(mimmna);
            //根据担保合同编号查询合同编号
            String contNo = "";
            String serno = "";
            GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelMapper.selectByGuarContNo(guarContNo);
            if(!Objects.isNull(grtGuarBizRstRel)){
                contNo = grtGuarBizRstRel.getContNo();
                serno = grtGuarBizRstRel.getSerno();
            }
            //String contNo = grtGuarContRelMapper.getContNoByGuarContNo(guarContNo);
            //因集中作业模式审批完才会更新合同表，先合同取合同申请表中的数据
            CusBaseDto cusBaseDto = new CusBaseDto();
            if(contNo!=null&&contNo.length()>0){
                //贷款合同
                IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByIqpSerno(serno);
                if(iqpLoanApp!=null){
                    //买卖合同编号
                    dicono = iqpLoanApp.getContNo();
                    //交易信息-合同债权金额
                    dicova = iqpLoanApp.getContAmt();
                    cus_id = iqpLoanApp.getCusId();
                    prd_id = iqpLoanApp.getPrdId();
                }else{
                    //银承合同
                    IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByIqpSerno(serno);
                    if(iqpAccpApp!=null){
                        dicono = iqpAccpApp.getContNo();
                        //保证金金额
                        BigDecimal bailAmt = iqpAccpApp.getBailAmt();
                        //合同金额
                        BigDecimal contAmt = iqpAccpApp.getContAmt();
                        // TODO 汇率信息 need cmis_cfg
                        BigDecimal rate = BigDecimal.ONE;
                        dicova = contAmt.multiply(rate).subtract(bailAmt);
                        cus_id = iqpAccpApp.getCusId();
                        prd_id = iqpAccpApp.getPrdId();
                    }else{
                        //保函合同
                        IqpCvrgApp iqpCvrgApp = iqpCvrgAppMapper.selectBySerno(serno);
                        if(null!=iqpCvrgApp){
                            dicono = iqpCvrgApp.getContNo();
                            //保证金金额
                            BigDecimal bailAmt = iqpCvrgApp.getBailAmt();
                            //合同金额
                            BigDecimal contAmt = iqpCvrgApp.getContAmt();
                            // TODO 汇率信息 need cmis_cfg
                            BigDecimal rate = BigDecimal.ONE;
                            dicova = contAmt.multiply(rate).subtract(bailAmt);
                            cus_id = iqpCvrgApp.getCusId();
                            prd_id = iqpCvrgApp.getPrdId();
                        }else{
                            //开证合同
                            IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByIqpSerno(serno);
                            if (iqpTfLocApp!=null) {
                                dicono = iqpTfLocApp.getContNo();
                                //TODO 保证金金额?
                                BigDecimal bailAmt = BigDecimal.ZERO;
                                //合同金额
                                if(null == iqpTfLocApp){
                                    throw new RuntimeException("未查询开证合同详情数据！");
                                }
                                BigDecimal contAmt = iqpTfLocApp.getCvtCnyAmt();
                                // TODO 汇率信息 need cmis_cfg
                                BigDecimal rate = BigDecimal.ONE;
                                dicova = contAmt.subtract(contAmt);
                                cus_id = iqpTfLocApp.getCusId();
                                prd_id = iqpTfLocApp.getPrdId();
                            } else {
                                //委托合同
                                IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(serno);
                                if (iqpEntrustLoanApp!=null) {
                                    dicono = iqpEntrustLoanApp.getContNo();
                                    //合同金额
                                    dicova = iqpEntrustLoanApp.getContAmt();
                                    //合同金额
                                    if(null == iqpEntrustLoanApp){
                                        throw new RuntimeException("未查询委托合同详情数据！");
                                    }
                                    cus_id = iqpEntrustLoanApp.getCusId();
                                    prd_id = iqpEntrustLoanApp.getPrdId();
                                } else {
                                    //最高额授信协议
                                    IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByContNo(contNo);
                                    if(iqpHighAmtAgrApp!=null){
                                        //买卖合同编号
                                        dicono = "最高额授信协议";
                                        //交易信息-合同债权金额
                                        dicova = iqpHighAmtAgrApp.getAgrContHighAvlAmt();
                                        cus_id = iqpHighAmtAgrApp.getCusId();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            CfgCtrContPrintReqDto cfgCtrContPrintReqDto = new CfgCtrContPrintReqDto();
            cfgCtrContPrintReqDto.setSuitContType(mimmco);
            cfgCtrContPrintReqDto.setSuitPrd(prd_id);
            String suitReportName = iCmisCfgClientService.querySuitReportName(cfgCtrContPrintReqDto);
            if(suitReportName!=null&&suitReportName.length()>0){
                if("grzgexfdbjkht.cpt".equals(suitReportName) || "grgfdbjkht.cpt".equals(suitReportName)){
                    micono = contNo;
                }
            }
            if(!StringUtils.isEmpty(cus_id)){
                cusBaseDto = commonService.getCusBaseByCusId(cus_id);
                //抵押信息-债务人-姓名/名称
                midena = cusBaseDto.getCusName();
                //抵押信息-债务人-证件号
                middnu = cusBaseDto.getCertCode();
                //抵押信息-债务人-证件类型-代码
                middtc = cusBaseDto.getCertType();
                middtc=getChangeCertCode(middtc);
                //抵押信息-债务人-证件类型-名称
                middtn=getChangeCertName(middtc);
            }
            dicova = changeAmt(dicova);
            xddb0012DataRespDto.setMicono(micono);
            xddb0012DataRespDto.setDicono(dicono);
            xddb0012DataRespDto.setDicova(dicova);
            xddb0012DataRespDto.setMidena(midena);
            xddb0012DataRespDto.setMiddnu(middnu);
            xddb0012DataRespDto.setMiddtc(middtc);
            xddb0012DataRespDto.setMiddtn(middtn);
            //交易信息-存量业务
            xddb0012DataRespDto.setDistbu("");
            //交易信息-交易编号
            xddb0012DataRespDto.setDinumb("");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch (Exception e){
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataRespDto));
        return xddb0012DataRespDto;
    }

    /**
     * 检查该客户是否时对公客户
     * @param cusId
     * @return
     */
    public Boolean checkIfComCus(String cusId){
        CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);
        if(Objects.nonNull(cusBaseDto)){
            return CmisCusConstants.CUS_CATALOG_PUB.equals(cusBaseDto.getCusCatalog());
        }
        return false;
    }

    private String getChangeBookNo(String BookNo){
        BookNo=ToDBC(BookNo);
        String regEx="[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(BookNo);
        BookNo=m.replaceAll("").trim();
        return BookNo;
    }

    /**
     * 全角转半角
     * @param BookNo
     * @return
     */
    private String ToDBC(String BookNo){
        char[] Book = BookNo.toCharArray();
        for(int i = 0;i<Book.length;i++){
            if(Book[i]==12288){
                Book[i] = (char)32;
                continue;
            }
            if(Book[i]>65280&&Book[i]<65375) {
                Book[i] = (char)(Book[i]-65248);
            }
        }
        return new String (Book);
    }
    //转换证件号字典项
    private String getChangeCertCode(String code){
        if("10".equals(code) || "A".equals(code)){
            code="1";//身份证
        }else if("12".equals(code) || "B".equals(code)){
            code="3";//护照
        }else if("11".equals(code) || "C".equals(code)){
            code="4";//户口簿
        }else if("13".equals(code)  || "G".equals(code)){
            code="5";//军官证
        }else if("20".equals(code)||"25".equals(code) || "Q".equals(code) || "R".equals(code)){
            code="6";//组织机构代码+统一社会信用代码
        }else if("24".equals(code) || "M".equals(code)){
            code="7";//营业执照
        }else{
            code="99";
        }
        return code;
    }
    //转换证件号字典项
    private String getChangeCertName(String code){
        if("1".equals(code) || "A".equals(code)){
            code="身份证";//身份证
        }else if("3".equals(code) || "B".equals(code)){
            code="护照";//护照
        }else if("4".equals(code) || "C".equals(code)){
            code="户口簿";//户口簿
        }else if("5".equals(code) || "G".equals(code)){
            code="军官证";//军官证
        }else if("6".equals(code) || "Q".equals(code) || "R".equals(code)){
            code="组织机构代码";//组织机构代码+统一社会信用代码
        }else if("7".equals(code) || "M".equals(code)){
            code="营业执照";//营业执照
        }else{
            code="其他";
        }
        return code;
    }

    private BigDecimal changeAmt(BigDecimal amt){
        if(amt !=null){
            amt = amt.divide(new BigDecimal("10000"));
        }else{
            amt = BigDecimal.ZERO;
        }
        return amt;
    }

    //转换是否字典项
    private String getChangeYesNo(String code){
        if("1".equals(code)){
            code="true";
        }else if("0".equals(code)){
            code="false";
        }else{
            code="";
        }
        return code;
    }
}
