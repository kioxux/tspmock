/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.XdWhbDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstMclWhbxd;
import cn.com.yusys.yusp.service.CusLstMclWhbxdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstMclWhbxdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 尚志勇
 * @创建时间: 2021-08-23 15:36:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstmclwhbxd")
public class CusLstMclWhbxdResource {
    @Autowired
    private CusLstMclWhbxdService cusLstMclWhbxdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<CusLstMclWhbxd>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstMclWhbxd> list = cusLstMclWhbxdService.selectAll(queryModel);
        return new ResultDto<List<CusLstMclWhbxd>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CusLstMclWhbxd>> index(QueryModel queryModel) {
        List<CusLstMclWhbxd> list = cusLstMclWhbxdService.selectByModel(queryModel);
        return new ResultDto<List<CusLstMclWhbxd>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CusLstMclWhbxd> create(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) throws URISyntaxException {
        cusLstMclWhbxdService.insert(cusLstMclWhbxd);
        return new ResultDto<CusLstMclWhbxd>(cusLstMclWhbxd);
    }

    /**
     * @函数名称:insertCusLstMclWhbxd
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertCusLstMclWhbxd")
    protected ResultDto<CusLstMclWhbxd> insertCusLstMclWhbxd(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) throws URISyntaxException {
        cusLstMclWhbxdService.insertCusLstMclWhbxd(cusLstMclWhbxd);
        return new ResultDto<CusLstMclWhbxd>(cusLstMclWhbxd);
    }

    /**
     * @函数名称:query
     * @函数描述:条件查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstMclWhbxd>> query(@RequestBody QueryModel queryModel) {
        List<CusLstMclWhbxd> list = cusLstMclWhbxdService.selectByModel(queryModel);
        return new ResultDto<List<CusLstMclWhbxd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/show")
    protected ResultDto<CusLstMclWhbxd> show(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) {
        CusLstMclWhbxd res = cusLstMclWhbxdService.selectByPrimaryKey(cusLstMclWhbxd.getSerno(), cusLstMclWhbxd.getBillNo());
        return new ResultDto<CusLstMclWhbxd>(res);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) throws URISyntaxException {
        int result = cusLstMclWhbxdService.update(cusLstMclWhbxd);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) {
        int result = cusLstMclWhbxdService.deleteByPrimaryKey(cusLstMclWhbxd.getSerno(), cusLstMclWhbxd.getBillNo());
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:credit
     * @函数描述:预授信
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/credit/")
    protected ResultDto<Boolean> credit(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) {
        return cusLstMclWhbxdService.credit(cusLstMclWhbxd);
    }

    /**
     * @函数名称:getXdWhbInfo
     * @函数描述:获取小微无还本续贷借据信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getxdwhbinfo/")
    protected ResultDto<List<XdWhbDto>> getXdWhbInfo(@RequestBody QueryModel queryModel) {
        List<XdWhbDto> list = cusLstMclWhbxdService.getXdWhbInfo(queryModel);
        return new ResultDto<List<XdWhbDto>>(list);
    }

    @PostMapping("/selectbyserno")
    protected ResultDto<CusLstMclWhbxd> selectbyserno(@RequestBody CusLstMclWhbxd cusLstMclWhbxd) {
        CusLstMclWhbxd record = cusLstMclWhbxdService.selectByPrimaryKey(cusLstMclWhbxd.getSerno(),cusLstMclWhbxd.getBillNo());
        return new ResultDto<>(record);
    }
}
