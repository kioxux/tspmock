package cn.com.yusys.yusp.web.rest;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.ComLoanInfoDto;
import cn.com.yusys.yusp.service.ComLoanInfoServices;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DoubleViewResource
 * @类描述: #优企贷无还本名单查询
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-30 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "优企贷无还本名单查询")
@RequestMapping("/api/comloanquery")
public class ComLoanInfoResource {

    @Autowired
    private ComLoanInfoServices comLoanInfoServices;

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 优企贷无还本名单查询(0：查询；1：新增；2：删除)
     */
    @PostMapping("/yqdbmd")
    protected ResultDto<List<ComLoanInfoDto>> yqdbmd(@RequestBody ComLoanInfoDto comLoanInfoDto) {
        List<ComLoanInfoDto> comLoanInfoDtolist = comLoanInfoServices.yqdbmd(comLoanInfoDto);
        return new ResultDto<List<ComLoanInfoDto>>(comLoanInfoDtolist);
    }
    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 合同查询
     */
    @PostMapping("/yqdcht")
    protected ResultDto<List<ComLoanInfoDto>> yqdcht(@RequestBody ComLoanInfoDto comLoanInfoDto) {
        List<ComLoanInfoDto> comLoanInfoDtolist = comLoanInfoServices.yqdcht(comLoanInfoDto);
        return new ResultDto<List<ComLoanInfoDto>>(comLoanInfoDtolist);
    }


}
