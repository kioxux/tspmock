package cn.com.yusys.yusp.web.server.xdtz0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0045.req.Xdtz0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.resp.Xdtz0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0045.Xdtz0045Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:商贷分户实时查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0045:商贷分户实时查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0045Resource.class);
    @Autowired
    private Xdtz0045Service xdtz0045Service;

    /**
     * 交易码：xdtz0045
     * 交易描述：商贷分户实时查询
     *
     * @param xdtz0045DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("商贷分户实时查询")
    @PostMapping("/xdtz0045")
    protected @ResponseBody
    ResultDto<Xdtz0045DataRespDto> xdtz0045(@Validated @RequestBody Xdtz0045DataReqDto xdtz0045DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataReqDto));
        Xdtz0045DataRespDto xdtz0045DataRespDto = new Xdtz0045DataRespDto();// 响应Dto:商贷分户实时查询
        ResultDto<Xdtz0045DataRespDto> xdtz0045DataResultDto = new ResultDto<>();

        try {
            // 从xdtz0045DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0045DataReqDto));
            xdtz0045DataRespDto = xdtz0045Service.xdtz0045(xdtz0045DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0045DataRespDto));

            // 封装xdtz0045DataResultDto中正确的返回码和返回信息
            xdtz0045DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0045DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, e.getMessage());
            // 封装xdtz0045DataResultDto中异常返回码和返回信息
            xdtz0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0045DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, e.getMessage());
            // 封装xdtz0045DataResultDto中异常返回码和返回信息
            xdtz0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0045DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0045DataRespDto到xdtz0045DataResultDto中
        xdtz0045DataResultDto.setData(xdtz0045DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataResultDto));
        return xdtz0045DataResultDto;
    }
}
