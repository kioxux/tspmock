package cn.com.yusys.yusp.service.server.xdcz0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req.Xdpj24ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.Xdpj24RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.req.Xdcz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.resp.Xdcz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.service.AccAccpService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.Dscms2PjxtClientService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class Xdcz0004Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0004Service.class);
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Autowired
    private PvpAccpAppMapper pvpAccpAppMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0004DataRespDto xdcz0004(Xdcz0004DataReqDto xdcz0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value);
        Xdcz0004DataRespDto xdcz0004DataRespDto = new Xdcz0004DataRespDto();
        HashMap map = new HashMap();
        try {
            String pvpStatus = xdcz0004DataReqDto.getPvpStatus();//出账状态
            String drfpoIsseMk = xdcz0004DataReqDto.getDrfpoIsseMk();//票据池出票标记
            BigDecimal paySecurityAmt = xdcz0004DataReqDto.getPaySecurityAmt();//已交保证金金额
            String serno = xdcz0004DataReqDto.getSerno();//批次号
            String openday = stringRedisTemplate.opsForValue().get("openday");
            //首先对批次号和出账状态非空判断
            if ((serno == null || "".equals(serno)) && !"6".equals(pvpStatus)) {
                //返回信息
                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,批次号为空！");
            } else if (pvpStatus == null || "".equals(pvpStatus)) {
                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,出账状态为空！");
            } else {
                PvpAccpApp pvpAccpApp = pvpAccpAppMapper.selectBySerno(serno);
                if (!"4".equals(pvpStatus) && !"5".equals(pvpStatus) && !"6".equals(pvpStatus)) {
                    if (pvpAccpApp == null) {
                        xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xdcz0004DataRespDto.setOpMsg("出账信息不存在");
                        return xdcz0004DataRespDto;
                    }

                    if (pvpStatus.equals("1")) {//出账
                        pvpAuthorizeMapper.UpdatePvpAuthorizeByserno1(serno);
                        // 银承通知票据系统出账后，通知额度系统更新台账状态
                        CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
                        cmisLmt0029ReqDto.setDealBizNo(serno);//交易流水号
                        cmisLmt0029ReqDto.setOriginAccNo(serno);//原台账编号
                        cmisLmt0029ReqDto.setNewAccNo("");//新台账编号
                        cmisLmt0029ReqDto.setStartDate("");//合同起始日
                        cmisLmt0029ReqDto.setEndDate("");//合同到期日
                        cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
                        logger.info("银承出账通知【{}】，前往额度系统调额度出账通知开始,请求报文为:【{}】", serno, JSON.toJSONString(cmisLmt0029ReqDto));
                        ResultDto<CmisLmt0029RespDto> resultDtoData = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
                        logger.info("银承出账通知【{}】，前往额度系统调额度出账通知结束,响应报文为:【{}】", serno, JSON.toJSONString(resultDtoData));
                        if (Objects.nonNull(resultDtoData) && Objects.nonNull(resultDtoData.getData()) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDtoData.getCode())) {
                            if (Objects.equals(SuccessEnum.SUCCESS.key, resultDtoData.getData().getErrorCode())) {
                                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,修改借据状态出账成功！");
                            } else {
                                xdcz0004DataRespDto.setOpFlag(resultDtoData.getData().getErrorCode());
                                xdcz0004DataRespDto.setOpMsg(resultDtoData.getData().getErrorMsg());
                                return xdcz0004DataRespDto;
                            }
                        } else {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                            xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
                            return xdcz0004DataRespDto;
                        }
                    } else if (pvpStatus.equals("2")) {//整批退回、冲正
                        pvpAuthorizeMapper.UpdatePvpAuthorizeByserno2(serno);
                        pvpAccpAppMapper.UpdatePvpAccpAppByserno(serno);
                        accAccpService.updateAccStatusByPvpSerno(serno);
                        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "06", xdcz0004DataReqDto);
                        if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                                && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
                                && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                            if (Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,修改借据状态作废成功！");
                            } else {
                                xdcz0004DataRespDto.setOpFlag(cmisLmt0014RespDtoResultDto.getData().getErrorCode());
                                xdcz0004DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                                return xdcz0004DataRespDto;
                            }
                        } else {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                            xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
                            return xdcz0004DataRespDto;
                        }
                    } else if (pvpStatus.equals("3")) {//撤销审批
                        pvpAuthorizeMapper.UpdatePvpAuthorizeByserno2(serno);
                        accAccpService.updateAccStatusByPvpSerno(serno);
                        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "06", xdcz0004DataReqDto);
                        if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                                && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
                                && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                            if (Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,修改借据状态作废成功！");
                            } else {
                                xdcz0004DataRespDto.setOpFlag(cmisLmt0014RespDtoResultDto.getData().getErrorCode());
                                xdcz0004DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                                return xdcz0004DataRespDto;
                            }
                        } else {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                            xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
                            return xdcz0004DataRespDto;
                        }
                    } else {
                        xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,查询不到存在的出账授权信息!！");
                    }
                     /*   } else {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                            xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,查询不到存在的出账申请信息!！");
                        }
                    }*/
                } else if (pvpStatus.equals("4")) {//结清（到期扣款）(日间还款分摊保证金)
                    ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = null;
                    BigDecimal totalSecurityMoneyAmount = Optional.ofNullable(accAccpService.getTotalSecurityMoneyAmount(map)).orElse(BigDecimal.ZERO);//到期票总票面余额
                    if (totalSecurityMoneyAmount.compareTo(xdcz0004DataReqDto.getPaySecurityAmt()) < 0) {
                        cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "03", xdcz0004DataReqDto);
                    } else {
                        cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "01", xdcz0004DataReqDto);
                    }
                    if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                            && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
                            && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                        if (Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                            xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,交易成功！");
                        } else {
                            xdcz0004DataRespDto.setOpFlag(cmisLmt0014RespDtoResultDto.getData().getErrorCode());
                            xdcz0004DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                            return xdcz0004DataRespDto;
                        }
                    } else {
                        xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                        xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
                        return xdcz0004DataRespDto;
                    }

                } else if (pvpStatus.equals("5")) {//迁移电票 结清（到期扣款）
                    int accAccp = accAccpService.selectAccAccpByBullNo(serno);
                    if (accAccp > 0) {
                        accAccpService.updateAccAccpByserno(serno);
                        ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "01", xdcz0004DataReqDto);
                        if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
                                && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
                                && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                            if (Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
                                xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                                xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,修改借据状态结清成功！");
                            } else {
                                xdcz0004DataRespDto.setOpFlag(cmisLmt0014RespDtoResultDto.getData().getErrorCode());
                                xdcz0004DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                                return xdcz0004DataRespDto;
                            }
                        } else {
                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                            xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
                            return xdcz0004DataRespDto;
                        }
                    } else {
                        xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,根据提供的serno,查询不到存在的台账信息!！");
                    }
                } else if (pvpStatus.equals("6")) {
//                    int count = 0;
//                    BigDecimal sumAmt = null;
//                    //从票据系统获取当日到期票的日出备款金额   平摊金额等于  （单笔票面金额/批次票面总额）*批次保证金新增金额
//                    List<AccAccp> list = accAccpService.getDqAccAccpByCondition(map);
//                    count = list.size();
//                    sumAmt = null;
//                    //票据系统获取    CORE_BILL_NO 核心银承编号
//                    Xdpj24ReqDto reqDto = new Xdpj24ReqDto();
//                    reqDto.setProvisionDay(openday);
//                    ResultDto<Xdpj24RespDto> piaoju = dscms2PjxtClientService.xdpj24(reqDto);
//                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, piaoju.getCode())) {
//                        Xdpj24RespDto data = piaoju.getData();
//                        BigDecimal bjzamount = data.getTotalFAmount();
//                        BigDecimal totle_famount = data.getSuccessAmount();
//                        String hx_cont_no = data.getBatchNo();
//
//                        //区分票据池。如果是票据池产生备款，则将票状态置为结清
//                        List<AccAccp> pjc = accAccpService.selectAccAccpByContNo(hx_cont_no);
//                        if (pjc != null && pjc.size() > 0) {
//                            accAccpService.updateAccAccpByhxcontno(hx_cont_no);
//                        } else {
//                            //成功备款金额=应备款金额
//                            if (totle_famount.compareTo(bjzamount) == 0) {
//                                accAccpService.updateAccAccpSucceed(hx_cont_no, openday);
//                            } else {
//                                BigDecimal totalAmount = accAccpService.getTotalAmount(map);
//                                if (list != null && list.size() > 0) {
//                                    for (int i = 0; i < list.size(); i++) {
//                                        AccAccp accp = list.get(i);
//                                        BigDecimal addSecurityMoneyAmt = paySecurityAmt.multiply(accp.getDrftTotalAmt().divide(totalAmount));
//                                        BigDecimal multiplys = addSecurityMoneyAmt.multiply(BigDecimal.valueOf(100));
//                                        BigDecimal divide = multiplys.divide(BigDecimal.valueOf(100));
//                                        accAccpService.updateAccAccpByBillNo(accp.getCoreBillNo());
//                                        addSecurityMoneyAmt = accp.getDrftTotalAmt();
//                                        if (count - 1 != i) {
//                                            sumAmt = sumAmt.add(addSecurityMoneyAmt);
//                                        } else {
//                                            BigDecimal newSecurityRate = null;
//                                            if (count - 1 == i) {
//                                                BigDecimal othAmt = bjzamount.subtract(sumAmt);
//                                                String amt = othAmt.toString();
//                                                addSecurityMoneyAmt = sumAmt;
//                                            } else {
//                                                sumAmt = sumAmt.add(addSecurityMoneyAmt);
//                                            }
//                                            //保证金金额+当前补交保证金金额除以对象获取的票据面额
//                                            //1-保证金比例=风险敞口比例
//                                            BigDecimal riskOpenRate = BigDecimal.valueOf(1).subtract(newSecurityRate);
//                                            //保证金
//                                            BigDecimal riskOpenMoneyAmt = accp.getDrftTotalAmt().multiply(riskOpenRate);
//                                            accAccpService.updateAccAccpByParms2(accp.getCoreBillNo(), newSecurityRate, riskOpenRate, riskOpenMoneyAmt, addSecurityMoneyAmt);
//
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = sendCmisLmt0014(serno, pvpAccpApp, "01");
//                    if (Objects.nonNull(cmisLmt0014RespDtoResultDto)
//                            && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData())
//                            && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
//                        if (Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0014RespDtoResultDto.getData().getErrorCode())) {
//                            xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
//                            xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_SUCCESS.value);
//                        } else {
//                            xdcz0004DataRespDto.setOpFlag(cmisLmt0014RespDtoResultDto.getData().getErrorCode());
//                            xdcz0004DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
//                            return xdcz0004DataRespDto;
//                        }
//                    } else {
//                        xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
//                        xdcz0004DataRespDto.setOpMsg(DscmsBizDbEnum.RETURN_FAIL.value);
//                        return xdcz0004DataRespDto;
//                    }
                    xdcz0004DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                    xdcz0004DataRespDto.setOpMsg("Xdcz0004Service,出账状态:6,目前已弃用!！");
              }
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value);
        return xdcz0004DataRespDto;

    }

    /**
     * 发送台账恢复接口
     * @param serno
     * @param pvpAccpApp
     */
    private ResultDto<CmisLmt0014RespDto> sendCmisLmt0014(String serno, PvpAccpApp pvpAccpApp, String recoverType, Xdcz0004DataReqDto xdcz0004DataReqDto) {
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
        cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(pvpAccpApp.getManagerBrId()));//金融机构代码
        cmisLmt0014ReqDto.setSerno(serno);//交易流水号
        cmisLmt0014ReqDto.setInputId(pvpAccpApp.getInputId());//登记人
        cmisLmt0014ReqDto.setInputBrId(pvpAccpApp.getInputBrId());//登记机构
        cmisLmt0014ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//登记日期

        CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
        cmisLmt0014ReqdealBizListDto.setDealBizNo(serno);//台账编号
        cmisLmt0014ReqdealBizListDto.setRecoverType(recoverType);
        if (Objects.equals(recoverType, "03")) {
            cmisLmt0014ReqdealBizListDto.setRecoverSpacAmtCny(xdcz0004DataReqDto.getPaySecurityAmt());
            cmisLmt0014ReqdealBizListDto.setRecoverAmtCny(xdcz0004DataReqDto.getPaySecurityAmt());
        }
        cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
        logger.info("贷款出账业务申请" + serno + "，前往额度系统恢复占用额度开始,参数" + JSON.toJSONString(cmisLmt0014ReqDto));
        ResultDto<CmisLmt0014RespDto> resultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
        logger.info("贷款出账业务申请" + serno + "，前往额度系统恢复占用额度结束,参数" + JSON.toJSONString(resultDto));
        return resultDto;
    }


}
