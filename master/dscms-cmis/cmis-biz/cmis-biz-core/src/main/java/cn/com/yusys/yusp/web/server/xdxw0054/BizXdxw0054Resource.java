package cn.com.yusys.yusp.web.server.xdxw0054;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0054.req.Xdxw0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0054.resp.Xdxw0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0054.Xdxw0054Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:查询优抵贷损益表明细
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0054:查询优抵贷损益表明细")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0054Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0054Resource.class);

    @Autowired
    private Xdxw0054Service xdxw0054Service;

    /**
     * 交易码：xdxw0054
     * 交易描述：查询优抵贷损益表明细
     *
     * @param xdxw0054DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷损益表明细")
    @PostMapping("/xdxw0054")
    protected @ResponseBody
    ResultDto<Xdxw0054DataRespDto> xdxw0054(@Validated @RequestBody Xdxw0054DataReqDto xdxw0054DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataReqDto));
        Xdxw0054DataRespDto xdxw0054DataRespDto = new Xdxw0054DataRespDto();// 响应Dto:查询优抵贷损益表明细
        ResultDto<Xdxw0054DataRespDto> xdxw0054DataResultDto = new ResultDto<>();
        String indgtSerno = xdxw0054DataReqDto.getIndgtSerno();//客户调查表编号
        String itemValue = xdxw0054DataReqDto.getItemValue();//项目值
        // 从xdxw0054DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0054Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataReqDto));
            xdxw0054DataRespDto = Optional.ofNullable(xdxw0054Service.queryNetProfit(xdxw0054DataReqDto)).orElseGet(() -> {
                Xdxw0054DataRespDto temp = new Xdxw0054DataRespDto();
                temp.setNearlyYear(new BigDecimal(0L));// 最近一个完整年度
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataRespDto));
            // 封装xdxw0054DataResultDto中正确的返回码和返回信息
            xdxw0054DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0054DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, e.getMessage());
            // 封装xdxw0054DataResultDto中异常返回码和返回信息
            xdxw0054DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0054DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0054DataRespDto到xdxw0054DataResultDto中
        xdxw0054DataResultDto.setData(xdxw0054DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataRespDto));
        return xdxw0054DataResultDto;
    }
}
