package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageRegisterApp
 * @类描述: guar_mortgage_register_app数据实体类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-04-19 15:58:32
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarMortgageRegisterAppDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String serno;

	/** 押品统一编号 **/
	private String guarNo;

	/** 担保分类代码 **/
	private String guarTypeCd;

	/** 抵质押物名称 **/
	private String pldimnMemo;

	/** 押品所有人编号 **/
	private String guarCusId;

	/** 押品所有人名称 **/
	private String guarCusName;

	/** 担保合同编号 **/
	private String guarContNo;

	/** 担保合同类型 **/
	private String guarContType;

	/** 本次担保金额 **/
	private java.math.BigDecimal guarAmt;

	/** 申请日期 **/
	private String appDate;

	/** 登记原因 **/
	private String regReason;

	/** 登记机构类型 **/
	private String regOrgType;

	/** 抵质押登记机构名称 **/
	private String regOrgName;

	/** 抵质押登记价值 **/
	private java.math.BigDecimal regValue;

	/** 抵质押登记日期 **/
	private String regDate;

	/** 抵质押登记失效日期 **/
	private String regInvalidDate;

	/** 是否在线办理抵押 **/
	private String isRegOnline;

	/** 是否预抵押 **/
	private String beforehandInd;

	/** 预抵押登记日期 **/
	private String beforehandRegStartDate;

	/** 预抵押登记失效日期 **/
	private String beforehandRegEndDate;

	/** 预登记方式 **/
	private String regWay;

	/** 预计办理正式登记日期 **/
	private String foreRegDate;

	/** 放款模式 **/
	private String loanMode;

	/** 登记办理状态STD_REG_STATUS **/
	private String regStatus;

	/** 抵押办理类型STD_REG_TYPE **/
	private String regType;

	/** 收件收据编号/合同备案号/其他 **/
	private String value1;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 操作类型 **/
	private String oprType;

	/** 申请状态 **/
	private String approveStatus;

	/** 责任人 **/
	private String managerId;

	/** 责任机构 **/
	private String managerBrId;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}

	/**
	 * @return GuarNo
	 */
	public String getGuarNo() {
		return this.guarNo;
	}

	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd == null ? null : guarTypeCd.trim();
	}

	/**
	 * @return GuarTypeCd
	 */
	public String getGuarTypeCd() {
		return this.guarTypeCd;
	}

	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}

	/**
	 * @return PldimnMemo
	 */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}

	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}

	/**
	 * @return GuarCusId
	 */
	public String getGuarCusId() {
		return this.guarCusId;
	}

	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}

	/**
	 * @return GuarCusName
	 */
	public String getGuarCusName() {
		return this.guarCusName;
	}

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}

	/**
	 * @return GuarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType == null ? null : guarContType.trim();
	}

	/**
	 * @return GuarContType
	 */
	public String getGuarContType() {
		return this.guarContType;
	}

	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}

	/**
	 * @return GuarAmt
	 */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}

	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}

	/**
	 * @return AppDate
	 */
	public String getAppDate() {
		return this.appDate;
	}

	/**
	 * @param regReason
	 */
	public void setRegReason(String regReason) {
		this.regReason = regReason == null ? null : regReason.trim();
	}

	/**
	 * @return RegReason
	 */
	public String getRegReason() {
		return this.regReason;
	}

	/**
	 * @param regOrgType
	 */
	public void setRegOrgType(String regOrgType) {
		this.regOrgType = regOrgType == null ? null : regOrgType.trim();
	}

	/**
	 * @return RegOrgType
	 */
	public String getRegOrgType() {
		return this.regOrgType;
	}

	/**
	 * @param regOrgName
	 */
	public void setRegOrgName(String regOrgName) {
		this.regOrgName = regOrgName == null ? null : regOrgName.trim();
	}

	/**
	 * @return RegOrgName
	 */
	public String getRegOrgName() {
		return this.regOrgName;
	}

	/**
	 * @param regValue
	 */
	public void setRegValue(java.math.BigDecimal regValue) {
		this.regValue = regValue;
	}

	/**
	 * @return RegValue
	 */
	public java.math.BigDecimal getRegValue() {
		return this.regValue;
	}

	/**
	 * @param regDate
	 */
	public void setRegDate(String regDate) {
		this.regDate = regDate == null ? null : regDate.trim();
	}

	/**
	 * @return RegDate
	 */
	public String getRegDate() {
		return this.regDate;
	}

	/**
	 * @param regInvalidDate
	 */
	public void setRegInvalidDate(String regInvalidDate) {
		this.regInvalidDate = regInvalidDate == null ? null : regInvalidDate.trim();
	}

	/**
	 * @return RegInvalidDate
	 */
	public String getRegInvalidDate() {
		return this.regInvalidDate;
	}

	/**
	 * @param isRegOnline
	 */
	public void setIsRegOnline(String isRegOnline) {
		this.isRegOnline = isRegOnline == null ? null : isRegOnline.trim();
	}

	/**
	 * @return IsRegOnline
	 */
	public String getIsRegOnline() {
		return this.isRegOnline;
	}

	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd == null ? null : beforehandInd.trim();
	}

	/**
	 * @return BeforehandInd
	 */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}

	/**
	 * @param beforehandRegStartDate
	 */
	public void setBeforehandRegStartDate(String beforehandRegStartDate) {
		this.beforehandRegStartDate = beforehandRegStartDate == null ? null : beforehandRegStartDate.trim();
	}

	/**
	 * @return BeforehandRegStartDate
	 */
	public String getBeforehandRegStartDate() {
		return this.beforehandRegStartDate;
	}

	/**
	 * @param beforehandRegEndDate
	 */
	public void setBeforehandRegEndDate(String beforehandRegEndDate) {
		this.beforehandRegEndDate = beforehandRegEndDate == null ? null : beforehandRegEndDate.trim();
	}

	/**
	 * @return BeforehandRegEndDate
	 */
	public String getBeforehandRegEndDate() {
		return this.beforehandRegEndDate;
	}

	/**
	 * @param regWay
	 */
	public void setRegWay(String regWay) {
		this.regWay = regWay == null ? null : regWay.trim();
	}

	/**
	 * @return RegWay
	 */
	public String getRegWay() {
		return this.regWay;
	}

	/**
	 * @param foreRegDate
	 */
	public void setForeRegDate(String foreRegDate) {
		this.foreRegDate = foreRegDate == null ? null : foreRegDate.trim();
	}

	/**
	 * @return ForeRegDate
	 */
	public String getForeRegDate() {
		return this.foreRegDate;
	}

	/**
	 * @param loanMode
	 */
	public void setLoanMode(String loanMode) {
		this.loanMode = loanMode == null ? null : loanMode.trim();
	}

	/**
	 * @return LoanMode
	 */
	public String getLoanMode() {
		return this.loanMode;
	}

	/**
	 * @param regStatus
	 */
	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus == null ? null : regStatus.trim();
	}

	/**
	 * @return RegStatus
	 */
	public String getRegStatus() {
		return this.regStatus;
	}

	/**
	 * @param regType
	 */
	public void setRegType(String regType) {
		this.regType = regType == null ? null : regType.trim();
	}

	/**
	 * @return RegType
	 */
	public String getRegType() {
		return this.regType;
	}

	/**
	 * @param value1
	 */
	public void setValue1(String value1) {
		this.value1 = value1 == null ? null : value1.trim();
	}

	/**
	 * @return Value1
	 */
	public String getValue1() {
		return this.value1;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}

	/**
	 * @return ApproveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}