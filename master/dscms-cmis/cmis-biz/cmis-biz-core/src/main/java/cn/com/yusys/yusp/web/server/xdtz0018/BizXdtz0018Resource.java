package cn.com.yusys.yusp.web.server.xdtz0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0018.Xdtz0018Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0018.req.Xdtz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.resp.Xdtz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:额度占用释放接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0018:额度占用释放接口")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0018Resource.class);

    @Autowired
    private Xdtz0018Service xdtz0018Service;

    /**
     * 交易码：xdtz0018
     * 交易描述：额度占用释放接口
     *
     * @param xdtz0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("额度占用释放接口")
    @PostMapping("/xdtz0018")
    protected @ResponseBody
    ResultDto<Xdtz0018DataRespDto> xdtz0018(@Validated @RequestBody Xdtz0018DataReqDto xdtz0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataReqDto));
        Xdtz0018DataRespDto xdtz0018DataRespDto = new Xdtz0018DataRespDto();// 响应Dto:额度占用释放接口
        ResultDto<Xdtz0018DataRespDto> xdtz0018DataResultDto = new ResultDto<>();
        try {
            // 从xdtz0018DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataReqDto));
            xdtz0018DataRespDto = xdtz0018Service.xdtz0018(xdtz0018DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataRespDto));
            // 封装xdtz0018DataResultDto中正确的返回码和返回信息
            xdtz0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, e.getMessage());
            // 封装xdtz0018DataResultDto中异常返回码和返回信息
            xdtz0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0018DataRespDto到xdtz0018DataResultDto中
        xdtz0018DataResultDto.setData(xdtz0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataResultDto));
        return xdtz0018DataResultDto;
    }
}
