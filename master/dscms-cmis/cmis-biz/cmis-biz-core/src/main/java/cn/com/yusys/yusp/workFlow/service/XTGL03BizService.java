package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.DataModify;
import cn.com.yusys.yusp.dto.CfgAccountClassChooseDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.DataModifyService;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * 人数据修改统计分类或科目投向审批流程
 *
 * @author macm
 * @version 1.0
 */
@Service
public class XTGL03BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(XTGL03BizService.class);

    @Autowired
    private DataModifyService dataModifyService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        // XT005 数据修改-统计分类（对公及零售）  XT005数据修改-科目投向（对公及零售）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_XT005.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_XT008.equals(bizType)) {
            iqpDataModify(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void iqpDataModify(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        // 根据流水号获取申请信息
        DataModify dataModify = dataModifyService.selectByPrimaryKey(serno);

        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                dataModify.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                dataModifyService.update(dataModify);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                dataModify.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                dataModifyService.update(dataModify);
                // 修改台账统计分类信息
                changeAccLoan(dataModify);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                dataModify.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                dataModifyService.update(dataModify);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                dataModify.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                dataModifyService.update(dataModify);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                dataModify.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                dataModifyService.update(dataModify);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void changeAccLoan(DataModify dataModify) throws Exception {
        // 借据号
        String billNo = dataModify.getBillNo();
        // 借据信息
        AccLoan  accLoan = accLoanService.selectByBillNo(billNo);
        if(Objects.nonNull(accLoan)){
            // 修改类型
            String modifyType = dataModify.getModifyType();
            log.info("修改类型:" + modifyType);
            /**
             * 01	统计分类
             * 02	科目投向
             * 03	其他信息
             */
            // 01	统计分类
            if("01".equals(modifyType)){
                // 贷款类别
                accLoan.setLoanTypeDetail(dataModify.getLoanTypeDetail());
                // 是否落实贷款
                accLoan.setIsPactLoan(dataModify.getIsPactLoan());
                // 是否绿色产业
                accLoan.setIsGreenIndustry(dataModify.getIsGreenIndustry());
                // 是否经营性物业贷款
                accLoan.setIsOperPropertyLoan(dataModify.getIsOperPropertyLoan());
                // 是否钢贸行业贷款
                accLoan.setIsSteelLoan(dataModify.getIsSteelLoan());
                // 是否不锈钢行业贷款
                accLoan.setIsStainlessLoan(dataModify.getIsStainlessLoan());
                // 是否扶贫贴息贷款
                accLoan.setIsPovertyReliefLoan(dataModify.getIsPovertyReliefLoan());
                // 是否劳动密集型小企业贴息贷款
                accLoan.setIsLaborIntenSbsyLoan(dataModify.getIsLaborIntenSbsyLoan());
                // 保障性安居工程贷款
                accLoan.setGoverSubszHouseLoan(dataModify.getGoverSubszHouseLoan());
                // 项目贷款节能环保
                accLoan.setEngyEnviProteLoan(dataModify.getEngyEnviProteLoan());
                // 是否农村综合开发贷款标志
                accLoan.setIsCphsRurDelpLoan(dataModify.getIsCphsRurDelpLoan());
                // 房地产贷款
                accLoan.setRealproLoan(dataModify.getRealproLoan());
                // 房产开发贷款资本金比例
                accLoan.setRealproLoanRate(dataModify.getRealproLoanRate());
                // 担保方式明细
                accLoan.setGuarDetailMode(dataModify.getGuarDetailMode());
                //农户类型
                accLoan.setAgriType(dataModify.getAgriType());
                //涉农贷款投向
                accLoan.setAgriLoanTer(dataModify.getAgriLoanTer());

                accLoanService.updateSelective(accLoan);
            } else if("02".equals(modifyType)){// 02	科目投向
                if(StringUtils.isBlank(dataModify.getSubjectNo())){
                    throw new Exception("贷款科目号为空");
                }
                if(dataModify.getSubjectNo().equals(dataModify.getOldSubjectNo())){
                    accLoan.setLoanTer(dataModify.getLoanTer());
                    accLoanService.updateSelective(accLoan);
                }else {
                    ResultDto<CfgAccountClassChooseDto> cfgAccountClassChoose =iCmisCfgClientService.queryHxAccountClassByAcccountClass(dataModify.getSubjectNo());
                    String subjectNo=cfgAccountClassChoose.getData().getHxAccountClass();
                    // 调用 ln3100 查询借据信息
                    Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto();
                    ln3100ReqDto.setDkjiejuh(billNo);
                    ResultDto<Ln3100RespDto> ln3100ResultDto  = dscms2CoreLnClientService.ln3100(ln3100ReqDto);

                    String ln3100Code = Optional.ofNullable(ln3100ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String ln3100Meesage = Optional.ofNullable(ln3100ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    Ln3100RespDto ln3100RespDto = null;
                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3100Code)) {
                        //  获取相关的值并解析
                        ln3100RespDto = ln3100ResultDto.getData();
                        // 调用 ln3079 修改科目投向
                        Ln3079ReqDto ln3079ReqDto = new Ln3079ReqDto();
                        // 业务操作标志
                        ln3079ReqDto.setDaikczbz("3");
                        // 借据号
                        ln3079ReqDto.setDkjiejuh(billNo);
                        // 产品代码
                        ln3079ReqDto.setChanpdma(ln3100RespDto.getChanpdma());
                        // 客户号
                        ln3079ReqDto.setKehuhaoo(ln3100RespDto.getKehuhaoo());
                        // 客户名称
                        ln3079ReqDto.setKehmingc(ln3100RespDto.getKehmingc());
                        // 合同编号
                        ln3079ReqDto.setHetongbh("");
                        // 产品名称
                        ln3079ReqDto.setChanpmch("");
                        // 贷款账号
                        ln3079ReqDto.setDkzhangh("");
                        // 新产品代码
                        ln3079ReqDto.setXinchpdm(ln3100RespDto.getChanpdma());
                        // 新产品名称
                        ln3079ReqDto.setXinchpmc(ln3100RespDto.getChanpdma());
                        // 出账号
                        ln3079ReqDto.setChuzhhao("");

                        // 会计类别(科目号)
                        ln3079ReqDto.setKuaijilb(subjectNo);
                        // 复核机构
                        ln3079ReqDto.setFuhejgou("");
                        // 业务属性1
                        ln3079ReqDto.setYewusx01("");
                        // 业务属性2
                        ln3079ReqDto.setYewusx02("");
                        // 业务属性3
                        ln3079ReqDto.setYewusx03("");
                        // 业务属性4
                        ln3079ReqDto.setYewusx04("");
                        // 业务属性5
                        ln3079ReqDto.setYewusx05("");

                        ResultDto<Ln3079RespDto> ln3079ResultDto  = dscms2CoreLnClientService.ln3079(ln3079ReqDto);
                        String ln3079Code = Optional.ofNullable(ln3079ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3079Code)) {
                            accLoan.setSubjectNo(dataModify.getSubjectNo());
                            accLoan.setLoanTer(dataModify.getLoanTer());
                            accLoanService.updateSelective(accLoan);
                        }else{
                            //  抛出错误异常
                            throw BizException.error(null, ln3079Code, ln3079ResultDto.getMessage());
                        }
                    }else{
                        //  抛出错误异常
                        throw BizException.error(null, ln3100Code, ln3100Meesage);
                    }
                }
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.XTGL03.equals(flowCode);
    }
}
