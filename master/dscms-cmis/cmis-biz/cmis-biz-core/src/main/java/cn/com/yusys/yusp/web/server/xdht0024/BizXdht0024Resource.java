package cn.com.yusys.yusp.web.server.xdht0024;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.xdht0024.req.Xdht0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import cn.com.yusys.yusp.service.server.xdht0024.Xdht0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类:合同详情查看
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0024:合同详情查看")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0024Resource.class);

    @Autowired
    private Xdht0024Service xdht0024Service;

    /**
     * 交易码：xdht0024
     * 交易描述：合同详情查看
     *
     * @param xdht0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同详情查看")
    @PostMapping("/xdht0024")
    protected @ResponseBody
    ResultDto<Xdht0024DataRespDto> xdht0024(@Validated @RequestBody Xdht0024DataReqDto xdht0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataReqDto));
        ResultDto<Xdht0024DataRespDto> xdht0024DataResultDto = new ResultDto<>();
        try {
            // 从xdht0024DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataReqDto));
            xdht0024DataResultDto = xdht0024Service.getContDetail(xdht0024DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataResultDto));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, e.getMessage());
            // 封装xdht0024DataResultDto中异常返回码和返回信息
            xdht0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0024DataRespDto到xdht0024DataResultDto中
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataResultDto));
        return xdht0024DataResultDto;
    }
}