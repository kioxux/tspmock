package cn.com.yusys.yusp.service.client.common;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.dto.oca.Datadict;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 通用外围接口调用类:字典项码值翻译
 * @author xll
 * @version 1.0
 */
@Service
public class SmLookupItemService {

    private static final Logger logger = LoggerFactory.getLogger(CommonService.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 通过字典项类型、码值查询对应的字典中文名称
     * @param lookCode
     * @return
     */
    public String getLookUpItemNameByLookUpCode(String lookCode,String lookItemCode) {
        String lookItemName = "";//中文字典

        logger.info("从Redis中获取STD_ZB_ASSURE_MEANS字典项缓存,缓存值为{}开始", CacheKeyEnum.CACHE_KEY_DATADICT.key);
        HashOperations<String, Object, Object> operations = stringRedisTemplate.opsForHash();
        Cursor<Map.Entry<Object, Object>> dictCursor = operations.scan(CacheKeyEnum.CACHE_KEY_DATADICT.key, ScanOptions.NONE);
        String dictKey = StringUtils.EMPTY;
        String dictValue = StringUtils.EMPTY;
        while (dictCursor.hasNext()) {
            dictKey = dictCursor.next().getKey().toString();
            if (Objects.equals(dictKey, lookCode)) {
                dictValue = dictCursor.next().getValue().toString();
            }
        }
        java.util.List<Datadict> dictvalueList = JSON.parseArray(dictValue, Datadict.class);
        Optional.ofNullable(dictvalueList).orElseThrow(() -> new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value));
        Map<String, String> datadictMap = dictvalueList.parallelStream().collect(Collectors.toMap(Datadict::getKey, Datadict::getValue));

        logger.info("从Redis中获取"+lookCode+"字典项缓存,缓存值为{}结束,获得值为{}", CacheKeyEnum.CACHE_KEY_DATADICT.key, dictvalueList);
        lookItemName = datadictMap.get(lookItemCode);

        return lookItemName;
    }
}
