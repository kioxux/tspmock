/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplStatementInfo
 * @类描述: iqp_appl_statement_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:18:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appl_statement_info")
public class IqpApplStatementInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 对账单通知方式 STD_ZB__NOTI_METHOD **/
	@Column(name = "STATEMENT_NOTI_METHOD", unique = false, nullable = true, length = 5)
	private String statementNotiMethod;
	
	/** 电话号码/手机号 **/
	@Column(name = "TEL_NO", unique = false, nullable = true, length = 30)
	private String telNo;
	
	/** 省/直辖市 **/
	@Column(name = "ADDR_PROVIENCE", unique = false, nullable = true, length = 12)
	private String addrProvience;
	
	/** 地级市/区 **/
	@Column(name = "ADDR_CITY", unique = false, nullable = true, length = 12)
	private String addrCity;
	
	/** 市级行政区/县/县级市 **/
	@Column(name = "ADDR_AREAR", unique = false, nullable = true, length = 12)
	private String addrArear;
	
	/** 通讯地址补充 **/
	@Column(name = "ADDR_DETAIL", unique = false, nullable = true, length = 200)
	private String addrDetail;
	
	/** 邮编 **/
	@Column(name = "COMM_ZIP", unique = false, nullable = true, length = 10)
	private String commZip;
	
	/** EMAIL对账单标志最近最后修改日期 **/
	@Column(name = "DZD_MODIFIED_DATE", unique = false, nullable = true, length = 10)
	private String dzdModifiedDate;
	
	/** EMAIL地址 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 255)
	private String email;
	
	/** EMAIL对账单发送标志 **/
	@Column(name = "EMAIL_DZD_FLAG", unique = false, nullable = true, length = 10)
	private String emailDzdFlag;
	
	/** EMAIL还款计划发送标志 **/
	@Column(name = "EMAIL_RETU_PLAN_FLAG", unique = false, nullable = true, length = 10)
	private String emailRetuPlanFlag;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param statementNotiMethod
	 */
	public void setStatementNotiMethod(String statementNotiMethod) {
		this.statementNotiMethod = statementNotiMethod;
	}
	
    /**
     * @return statementNotiMethod
     */
	public String getStatementNotiMethod() {
		return this.statementNotiMethod;
	}
	
	/**
	 * @param telNo
	 */
	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}
	
    /**
     * @return telNo
     */
	public String getTelNo() {
		return this.telNo;
	}
	
	/**
	 * @param addrProvience
	 */
	public void setAddrProvience(String addrProvience) {
		this.addrProvience = addrProvience;
	}
	
    /**
     * @return addrProvience
     */
	public String getAddrProvience() {
		return this.addrProvience;
	}
	
	/**
	 * @param addrCity
	 */
	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}
	
    /**
     * @return addrCity
     */
	public String getAddrCity() {
		return this.addrCity;
	}
	
	/**
	 * @param addrArear
	 */
	public void setAddrArear(String addrArear) {
		this.addrArear = addrArear;
	}
	
    /**
     * @return addrArear
     */
	public String getAddrArear() {
		return this.addrArear;
	}
	
	/**
	 * @param addrDetail
	 */
	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}
	
    /**
     * @return addrDetail
     */
	public String getAddrDetail() {
		return this.addrDetail;
	}
	
	/**
	 * @param commZip
	 */
	public void setCommZip(String commZip) {
		this.commZip = commZip;
	}
	
    /**
     * @return commZip
     */
	public String getCommZip() {
		return this.commZip;
	}
	
	/**
	 * @param dzdModifiedDate
	 */
	public void setDzdModifiedDate(String dzdModifiedDate) {
		this.dzdModifiedDate = dzdModifiedDate;
	}
	
    /**
     * @return dzdModifiedDate
     */
	public String getDzdModifiedDate() {
		return this.dzdModifiedDate;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param emailDzdFlag
	 */
	public void setEmailDzdFlag(String emailDzdFlag) {
		this.emailDzdFlag = emailDzdFlag;
	}
	
    /**
     * @return emailDzdFlag
     */
	public String getEmailDzdFlag() {
		return this.emailDzdFlag;
	}
	
	/**
	 * @param emailRetuPlanFlag
	 */
	public void setEmailRetuPlanFlag(String emailRetuPlanFlag) {
		this.emailRetuPlanFlag = emailRetuPlanFlag;
	}
	
    /**
     * @return emailRetuPlanFlag
     */
	public String getEmailRetuPlanFlag() {
		return this.emailRetuPlanFlag;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}