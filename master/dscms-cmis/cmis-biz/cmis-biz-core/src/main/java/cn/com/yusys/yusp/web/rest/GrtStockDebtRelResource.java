/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CtrLoanCont;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GrtStockDebtRel;
import cn.com.yusys.yusp.service.GrtStockDebtRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtStockDebtRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-15 10:54:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/grtstockdebtrel")
public class GrtStockDebtRelResource {
    @Autowired
    private GrtStockDebtRelService grtStockDebtRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/selectbyguarcontno/{guarContNo}")
    protected ResultDto<List<CtrLoanCont>> selectByGuarContNo(@PathVariable("guarContNo") String guarContNo) {
        List<CtrLoanCont> list = grtStockDebtRelService.selectByGuarContNo(guarContNo);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GrtStockDebtRel>> index(QueryModel queryModel) {
        List<GrtStockDebtRel> list = grtStockDebtRelService.selectByModel(queryModel);
        return new ResultDto<List<GrtStockDebtRel>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GrtStockDebtRel> create(@RequestBody GrtStockDebtRel grtStockDebtRel) throws URISyntaxException {
        grtStockDebtRelService.insert(grtStockDebtRel);
        return new ResultDto<GrtStockDebtRel>(grtStockDebtRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GrtStockDebtRel grtStockDebtRel) throws URISyntaxException {
        int result = grtStockDebtRelService.update(grtStockDebtRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = grtStockDebtRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletebymodel")
    protected ResultDto<Integer> delete(@RequestBody QueryModel queryModel) {
        int result = grtStockDebtRelService.deleteByModel(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tempsave")
    protected ResultDto<Integer> tempSave(@RequestBody List<GrtStockDebtRel> grtStockDebtRels) throws URISyntaxException {
        int result = grtStockDebtRelService.tempSave(grtStockDebtRels);
        return new ResultDto<>(result);
    }

}
