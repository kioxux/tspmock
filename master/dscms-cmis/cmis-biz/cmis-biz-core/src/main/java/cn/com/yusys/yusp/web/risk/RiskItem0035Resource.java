package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0035Resource
 * @类描述: 权证出库校验押品关联合同状态
 * @功能描述:
 * @创建人: dumingdi
 * @创建时间: 2021-07-13 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0035权证出库校验押品关联合同状态")
@RestController
@RequestMapping("/api/riskcheck/riskItem0035")
public class RiskItem0035Resource {

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    /**
     * @方法名称: riskItem0035
     * @方法描述: 权证出库校验关联合同状态
     * @参数与返回说明:
     * @算法描述:
     * @创建人: dumingdi
     * @创建时间: 2021-07-13 11:14:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "权证出库校验押品关联合同状态")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0035(@RequestBody QueryModel queryModel) {
        // TODO 判断实现逻辑
        //当流程中打回到客户经理节点时获取不到数据 因已走过风险拦截所以放行
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return ResultDto.success(guarWarrantManageAppService.riskItem0035(queryModel.getCondition().get("bizId").toString()));
    }
}