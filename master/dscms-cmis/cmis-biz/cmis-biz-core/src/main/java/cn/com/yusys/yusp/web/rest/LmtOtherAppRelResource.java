/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.LmtOtherAppRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtOtherAppRel;
import cn.com.yusys.yusp.service.LmtOtherAppRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtOtherAppRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:18:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtotherapprel")
public class LmtOtherAppRelResource {
    @Autowired
    private LmtOtherAppRelService lmtOtherAppRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtOtherAppRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtOtherAppRel> list = lmtOtherAppRelService.selectAll(queryModel);
        return new ResultDto<List<LmtOtherAppRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtOtherAppRel>> index(QueryModel queryModel) {
        List<LmtOtherAppRel> list = lmtOtherAppRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtOtherAppRel>>(list);
    }

    /**
     * @方法名称：showByPrimaryKey
     * @方法描述：主键查
     * @创建人：zhangming12
     * @创建时间：2021/5/19 10:49
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/showbyprimarykey")
    protected ResultDto<LmtOtherAppRel> showByPrimaryKey(@RequestBody String pkId) {
        LmtOtherAppRel lmtOtherAppRel = lmtOtherAppRelService.selectByPrimaryKey(pkId);
        return new ResultDto<>(lmtOtherAppRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtOtherAppRel> create(@RequestBody LmtOtherAppRel lmtOtherAppRel) throws URISyntaxException {
        lmtOtherAppRelService.insert(lmtOtherAppRel);
        return new ResultDto<LmtOtherAppRel>(lmtOtherAppRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtOtherAppRel lmtOtherAppRel) throws URISyntaxException {
        int result = lmtOtherAppRelService.update(lmtOtherAppRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtOtherAppRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtOtherAppRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：queryByLmtSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/18 22:06
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/querybylmtserno")
    protected ResultDto<List<LmtOtherAppRel>> queryByLmtSerno(@RequestBody QueryModel queryModel) {
        List<LmtOtherAppRel> lmtOtherAppRels = lmtOtherAppRelService.selectByLmtSerno(queryModel);
        return new ResultDto<>(lmtOtherAppRels);
    }

    /**
     * @方法名称：queryByGrpLmtSerno
     * @方法描述：
     * @创建人：yangwl
     * @创建时间：2021/6/18 22:06
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/querybygrplmtserno")
    protected ResultDto<List<LmtOtherAppRelDto>> queryByGrpLmtSerno(@RequestBody QueryModel queryModel) {
        List<LmtOtherAppRelDto> lmtOtherAppRels = lmtOtherAppRelService.queryByGrpLmtSerno(queryModel);
        return new ResultDto<>(lmtOtherAppRels);
    }

    /**
     * @方法名称：insertSelective
     * @方法描述新增非空
     * @创建人：zhangming12
     * @创建时间：2021/5/19 10:29
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/insertselective")
    protected ResultDto<Integer> insertSelective(@RequestBody LmtOtherAppRel lmtOtherAppRel){
        int affectRow = lmtOtherAppRelService.insertSelective(lmtOtherAppRel);
        return new ResultDto<>(affectRow);
    }

    /**
     * @方法名称：updateOprType
     * @方法描述：根据主键逻辑删除
     * @创建人：zhangming12
     * @创建时间：2021/5/19 10:44
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/updateoprtype")
    protected ResultDto<Integer> updateOprType(@RequestBody String pkId){
        int affectRow = lmtOtherAppRelService.updateOprType(pkId);
        return new ResultDto<>(affectRow);
    }


    /**
     * @方法名称：updateSelective
     * @方法描述：根据主键跟新非空字段
     * @创建人：zhangming12
     * @创建时间：2021/5/19 11:09
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/updateselective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtOtherAppRel lmtOtherAppRel){
        int affectRow = lmtOtherAppRelService.updateSelective(lmtOtherAppRel);
        return new ResultDto<>(affectRow);
    }

    /**
     * @方法名称：updateOprType
     * @方法描述：逻辑删除
     * @创建人：css
     * @创建时间：2021/7/2 10:44
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/deletememreldata")
    protected ResultDto<Integer> deleteMemRelData(@RequestBody String serno){
        int affectRow = lmtOtherAppRelService.deleteMemRelData(serno);
        return new ResultDto<>(affectRow);
    }

}
