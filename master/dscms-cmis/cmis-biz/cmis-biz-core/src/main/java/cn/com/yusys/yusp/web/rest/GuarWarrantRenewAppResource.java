/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarWarrantRenewApp;
import cn.com.yusys.yusp.service.GuarWarrantRenewAppService;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarWarrantRenewAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-14 16:47:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarwarrantrenewapp")
public class GuarWarrantRenewAppResource {
    @Autowired
    private GuarWarrantRenewAppService guarWarrantRenewAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarWarrantRenewApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarWarrantRenewApp> list = guarWarrantRenewAppService.selectAll(queryModel);
        return new ResultDto<List<GuarWarrantRenewApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarWarrantRenewApp>> index(QueryModel queryModel) {
        List<GuarWarrantRenewApp> list = guarWarrantRenewAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarWarrantRenewApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarWarrantRenewApp> show(@PathVariable("serno") String serno) {
        GuarWarrantRenewApp guarWarrantRenewApp = guarWarrantRenewAppService.selectByPrimaryKey(serno);
        return new ResultDto<GuarWarrantRenewApp>(guarWarrantRenewApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarWarrantRenewApp> create(@RequestBody GuarWarrantRenewApp guarWarrantRenewApp) throws URISyntaxException {
        guarWarrantRenewAppService.insert(guarWarrantRenewApp);
        return new ResultDto<GuarWarrantRenewApp>(guarWarrantRenewApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarWarrantRenewApp guarWarrantRenewApp) throws URISyntaxException {
        int result = guarWarrantRenewAppService.update(guarWarrantRenewApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarWarrantRenewAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarWarrantRenewAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:vilaIsInWayRenewApp
     * @函数描述:判断该押品是否存在途申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/vilaisinwayrenewapp")
    protected ResultDto<Integer> vilaIsInWayRenewApp(@RequestBody Map paramMap) {
        int result = guarWarrantRenewAppService.vilaIsInWayRenewApp(paramMap);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:vilaIsInWayRenewApp
     * @函数描述:判断该押品是否存在途申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/vilaisinwayrenewappandinsert")
    protected ResultDto<Map> vilaIsInWayAndInsertApp(@RequestBody Map paramMap) {
        Map resultMap = guarWarrantRenewAppService.vilaIsInWayAndInsertApp(paramMap);
        return new ResultDto<Map>(resultMap);
    }






}
