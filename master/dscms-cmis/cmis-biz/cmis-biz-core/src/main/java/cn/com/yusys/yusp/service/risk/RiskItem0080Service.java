package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0099Service
 * @类描述: 产品类型属性适配校验
 * @功能描述: 产品类型属性适配校验
 * @创建人: yfs
 * @创建时间: 2021年8月12日08:38:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0080Service {

    private static final Logger log = LoggerFactory.getLogger(CtrLoanContService.class);

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    //担保方式 00信用 10抵押 20质押30保证
    // 00信用
    Map<String,String> creditMap = new HashMap<String,String>() {
        {
            put("P001","00");
            put("P004","00");
            put("P007","00");
            put("P008","00");
            put("P009","00");
            put("P010","00");
            put("P013","00");
            put("P014","00");
            put("P015","00");
            put("P016","00");
            put("P029","00");
            put("P032","00");
            put("P033","00");
            put("P040","00");
            put("P041","00");
            put("P042","00");
        }
    };
    // 10抵押
    Map<String,String> mortgageMap = new HashMap<String,String>() {
        {
            put("P001","10");
            put("P007","10");
            put("P008","10");
            put("P011","10");
            put("P019","10");
            put("P025","10");
            put("P028","10");
            put("P034","10");
            put("P035","10");
            put("P036","10");
            put("P040","10");
            put("P041","10");
        }
    };
    // 20质押
    Map<String,String> pledgeMap = new HashMap<String,String>() {
        {
            put("P001","20");
            put("P007","20");
            put("P008","20");
            put("P018","20");
            put("P026","20");
            put("P027","20");
            put("P036","20");
            put("P038","20");
            put("P039","20");
            put("P040","20");
            put("P041","20");
        }
    };
    // 30保证
    Map<String,String> ensureMap = new HashMap<String,String>() {
        {
            put("P001","30");
            put("P002","30");
            put("P003","30");
            put("P004","30");
            put("P005","30");
            put("P007","30");
            put("P008","30");
            put("P009","30");
            put("P010","30");
            put("P013","30");
            put("P014","30");
            put("P015","30");
            put("P017","30");
            put("P018","30");
            put("P019","30");
            put("P020","30");
            put("P021","30");
            put("P022","30");
            put("P023","30");
            put("P024","30");
            put("P028","30");
            put("P029","30");
            put("P030","30");
            put("P031","30");
            put("P032","30");
            put("P033","30");
            put("P035","30");
            put("P036","30");
            put("P037","30");
            put("P038","30");
            put("P039","30");
            put("P040","30");
            put("P041","30");
            put("P042","30");

        }
    };
    /**
     * @方法名称: riskItem0099
     * @方法描述: 产品类型属性适配校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: yfs
     * @创建时间: 2021年8月12日08:38:26
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0080(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("产品类型属性适配校验开始*******************业务流水号：【{}】",serno);
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("产品类型属性适配校验开始，查询分项参数："+ JSON.toJSONString(paramsSub));
        List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        Map paramsSubPrd = new HashMap();
        for (LmtAppSub lmtAppSub : subList) {
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("产品类型属性适配校验开始，查询品种参数："+ JSON.toJSONString(paramsSubPrd));
            List<LmtAppSubPrd> subPrdList= lmtAppSubPrdService.selectByParams(paramsSubPrd);
            if(CollectionUtils.nonEmpty(subPrdList)) {
                for(LmtAppSubPrd lmtAppSubPrd : subPrdList) {
                    // 信用的时候
                    if(Objects.equals("00",lmtAppSub.getGuarMode())) {
                        if(StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("00",creditMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08001);
                            return riskResultDto;
                        }
                    }
                    // 抵押的时候
                    if(Objects.equals("10",lmtAppSub.getGuarMode())) {
                        if(StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("10",mortgageMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08001);
                            return riskResultDto;
                        }
                    }
                    // 质押的时候
                    if(Objects.equals("20",lmtAppSub.getGuarMode())) {
                        if(StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("20",pledgeMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08001);
                            return riskResultDto;
                        }
                    }
                    // 保证的时候
                    if(Objects.equals("30",lmtAppSub.getGuarMode())) {
                        if(StringUtils.nonBlank(lmtAppSubPrd.getLmtBizTypeProp()) && !Objects.equals("30",ensureMap.get(lmtAppSubPrd.getLmtBizTypeProp()))) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08001);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        log.info("产品类型属性适配校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
