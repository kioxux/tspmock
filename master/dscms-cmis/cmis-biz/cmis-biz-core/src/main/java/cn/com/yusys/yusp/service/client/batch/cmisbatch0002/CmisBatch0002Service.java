package cn.com.yusys.yusp.service.client.batch.cmisbatch0002;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询[核心系统-历史表-贷款账户主表]信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBatch0002Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0002Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查询[核心系统-历史表-贷款账户主表]信息
     *
     * @param cmisbatch0002ReqDto
     * @return
     */
    @Transactional
    public Cmisbatch0002RespDto cmisbatch0002(Cmisbatch0002ReqDto cmisbatch0002ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ReqDto));
        ResultDto<Cmisbatch0002RespDto> cmisbatch0002ResultDto = cmisBatchClientService.cmisbatch0002(cmisbatch0002ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ResultDto));

        String cmisbatch0002Code = Optional.ofNullable(cmisbatch0002ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisbatch0002Meesage = Optional.ofNullable(cmisbatch0002ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Cmisbatch0002RespDto cmisbatch0002RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0002ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbatch0002RespDto = cmisbatch0002ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, cmisbatch0002Code, cmisbatch0002Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        return cmisbatch0002RespDto;
    }
}
