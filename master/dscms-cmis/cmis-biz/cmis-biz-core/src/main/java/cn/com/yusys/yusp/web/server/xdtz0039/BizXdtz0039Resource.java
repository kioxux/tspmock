package cn.com.yusys.yusp.web.server.xdtz0039;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0039.req.Xdtz0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.resp.Xdtz0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0039.Xdtz0039Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:根据企业名称查询申请企业在本行是否存在当前逾期贷款
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0039:根据企业名称查询申请企业在本行是否存在当前逾期贷款")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0039Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0039Resource.class);

    @Autowired
    private Xdtz0039Service xdtz0039Service;

    /**
     * 交易码：xdtz0039
     * 交易描述：根据企业名称查询申请企业在本行是否存在当前逾期贷款
     *
     * @param xdtz0039DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0039:根据企业名称查询申请企业在本行是否存在当前逾期贷款")
    @PostMapping("/xdtz0039")
    protected @ResponseBody
    ResultDto<Xdtz0039DataRespDto> xdtz0039(@Validated @RequestBody Xdtz0039DataReqDto xdtz0039DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039DataReqDto));
        Xdtz0039DataRespDto xdtz0039DataRespDto = new Xdtz0039DataRespDto();// 响应Dto:根据企业名称查询申请企业在本行是否存在当前逾期贷款
        ResultDto<Xdtz0039DataRespDto> xdtz0039DataResultDto = new ResultDto<>();
        // 从xdtz0039DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用XXXXXService层开始
            xdtz0039DataRespDto = Optional.ofNullable(xdtz0039Service.getHasOverByCusName(xdtz0039DataReqDto))
                    .orElse(new Xdtz0039DataRespDto(new BigDecimal("0"), StringUtils.EMPTY));
            // 封装xdtz0039DataResultDto中正确的返回码和返回信息
            xdtz0039DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0039DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, e.getMessage());
            // 封装xdtz0039DataResultDto中异常返回码和返回信息
            xdtz0039DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0039DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0039DataRespDto到xdtz0039DataResultDto中
        xdtz0039DataResultDto.setData(xdtz0039DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039DataResultDto));
        return xdtz0039DataResultDto;
    }
}
