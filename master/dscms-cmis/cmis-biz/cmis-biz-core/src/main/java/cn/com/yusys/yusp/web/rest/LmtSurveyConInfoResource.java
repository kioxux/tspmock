/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyConInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.dto.LmtSurveyConInfoDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.service.LmtSurveyConInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyConInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:39:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "调查结论")
@RequestMapping("/api/lmtsurveyconinfo")
public class LmtSurveyConInfoResource {
    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyConInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyConInfo> list = lmtSurveyConInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyConInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyConInfo>> index(QueryModel queryModel) {
        List<LmtSurveyConInfo> list = lmtSurveyConInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyConInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @PostMapping("/{surveySerno}")
    protected ResultDto<LmtSurveyConInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyConInfo>(lmtSurveyConInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyConInfo> create(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) throws URISyntaxException {
        lmtSurveyConInfoService.insert(lmtSurveyConInfo);
        return new ResultDto<LmtSurveyConInfo>(lmtSurveyConInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) throws URISyntaxException {
        int result = lmtSurveyConInfoService.update(lmtSurveyConInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtSurveyConInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyConInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/27 14:09
     * @注释 单条数据查询
     */
    @ApiOperation("查询单个对象，公共API接口")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<LmtSurveyConInfo> selectbysurveyserno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtSurveyConInfo>(lmtSurveyConInfo);
    }

    /**
     * @创建人 李帅
     * @创建时间 2021/8/4 14:54
     * @注释 单条数据查询
     */
    @ApiOperation("查询单个对象，公共API接口")
    @PostMapping("/selectconinfobysurveyserno")
    protected ResultDto<LmtSurveyConInfoDto> selectConInfoBySurveySerno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        LmtSurveyConInfoDto lmtSurveyConInfo = lmtSurveyConInfoService.selectConInfoBySurveySerno(lmtSurveyReportDto.getSurveySerno());
        return new ResultDto<LmtSurveyConInfoDto>(lmtSurveyConInfo);
    }

    /**
     * @param lmtSurveyConInfo
     * @return ResultDto<Integer>
     * @author 王玉坤
     * @date 2021/4/24 18:59
     * @version 1.0.0
     * @desc 保存调查结论信息
     * @修改历史: 修改时间  2021年6月18日20:16:00  修改人员  王浩  修改原因  废弃这个借口 用 saveorupdatecon
     */
    @ApiOperation("保存调查结论信息")
    @PostMapping("/saveSurveyConInfo/")
    protected ResultDto<Integer> addSurveyConInfo(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) {
        int result = lmtSurveyConInfoService.saveOrUpdateCon(lmtSurveyConInfo);
        return new ResultDto<Integer>(result);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-03 10:08
     * @注释 利率 测算 挡板接口
     */
    @ApiOperation("增享待利率测算")
    @PostMapping("/calculate/")
    protected ResultDto calculate(@RequestBody LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo) {
        return lmtSurveyConInfoService.calculate(lmtSurveyReportBasicInfo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-03 10:08
     * @注释 保存调查结论信息
     */
    @ApiOperation("保存调查结论信息")
    @PostMapping("/saveorupdatecon")
    protected ResultDto<Integer> saveorupdatecon(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) {
        int result = lmtSurveyConInfoService.saveOrUpdateCon(lmtSurveyConInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 李帅
     * @创建时间 2021-08-04 14:33
     * @注释 保存调查结论信息 录入调查报告基本信息表中‘IS_TQSD’等字段
     */
    @ApiOperation("保存调查结论信息")
    @PostMapping("/saveorupdateconinfo")
    protected ResultDto<Integer> saveOrUpdateConInfo(@RequestBody LmtSurveyConInfoDto lmtSurveyConInfoDto) {
        int result = lmtSurveyConInfoService.saveOrUpdateConInfo(lmtSurveyConInfoDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 李帅
     * @创建时间 2021-08-05 13:33
     * @注释 查询客户名下上笔业务余额，调查建议期限，调查还款方式
     */
    @ApiOperation("查询客户名下上笔业务余额，调查建议期限，调查还款方式")
    @PostMapping("/getCusLastContInfo/{cusId}")
    protected ResultDto<LmtSurveyConInfoDto> getCusLastContInfo(@PathVariable("cusId") String cusId) {
        LmtSurveyConInfoDto result = lmtSurveyConInfoService.getCusLastContInfo(cusId);
        return new ResultDto<LmtSurveyConInfoDto>(result);
    }



    /**
     * @param lmtSurveyConInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Double>
     * @author hubp
     * @date 2021/4/16 10:47
     * @version 1.0.0
     * @desc 惠享待 获取参考利率
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/getrefrate")
    public ResultDto<Fbxw06RespDto> getRefRate(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) {
        Fbxw06RespDto resultDto = lmtSurveyConInfoService.getRefRate(lmtSurveyConInfo);
        return new ResultDto<Fbxw06RespDto>(resultDto);
    }



    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.LmtSurveyConInfo>
     * @author hubp
     * @date 2021/5/20 20:25
     * @version 1.0.0
     * @desc    通过调查流水号查询单个对象---------作废
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过调查流水号查询单个对象")
    @PostMapping("/querydata")
    protected ResultDto<LmtSurveyConInfo> queryData(@RequestBody String surveySerno) {
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyConInfo>(lmtSurveyConInfo);
    }
}
