/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.LmtReplySubDto;
import cn.com.yusys.yusp.service.LmtReplySubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplySub;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplySubResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:34:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplysub")
public class LmtReplySubResource {
    @Autowired
    private LmtReplySubService lmtReplySubService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplySub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplySub> list = lmtReplySubService.selectAll(queryModel);
        return new ResultDto<List<LmtReplySub>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplySub>> index(QueryModel queryModel) {
        List<LmtReplySub> list = lmtReplySubService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplySub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplySub> show(@PathVariable("pkId") String pkId) {
        LmtReplySub lmtReplySub = lmtReplySubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplySub>(lmtReplySub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplySub> create(@RequestBody LmtReplySub lmtReplySub) throws URISyntaxException {
        lmtReplySubService.insert(lmtReplySub);
        return new ResultDto<LmtReplySub>(lmtReplySub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplySub lmtReplySub) throws URISyntaxException {
        int result = lmtReplySubService.update(lmtReplySub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplySubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplySubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getReplySubInfo
     * @方法描述：获取批复分项信息
     * @创建人：zhangming12
     * @创建时间：2021/5/19 16:02
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/getreplysubinfo")
    protected ResultDto<List<LmtReplySubDto>> getReplySubInfo(@RequestBody QueryModel queryModel) {
        String replySerno = (String) queryModel.getCondition().get("replySerno");
        List<LmtReplySubDto> lmtReplySubDtoList = lmtReplySubService.getReplySubInfo(replySerno);
        return new ResultDto<>(lmtReplySubDtoList);
    }

    /**
     * @方法名称：getreplysubinfobygrpserno
     * @方法描述：根据集团申请流水号获取批复分项信息
     * @创建人：yangwl
     * @创建时间：2021/5/24 14:12
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/getreplysubinfobygrpserno")
    protected ResultDto<List<LmtReplySubDto>> getReplySubInfoByGrpSerno(@RequestBody QueryModel queryModel) throws Exception {
        String grpSerno = (String) queryModel.getCondition().get("grpSerno");
        List<LmtReplySubDto> lmtReplySubDtoList = lmtReplySubService.getReplySubInfoByGrpSerno(grpSerno);
        return new ResultDto<>(lmtReplySubDtoList);
    }
}
