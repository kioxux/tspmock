/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.template.FileSystemTemplate;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.dto.LmtGrpAppSubDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtGrpApprMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpApprService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-14 09:54:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpApprService {
    private static final Logger log = LoggerFactory.getLogger(LmtGrpApprService.class);

    @Resource
    private LmtGrpApprMapper lmtGrpApprMapper;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    @Autowired
    private LmtAppService lmtAppService;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private LmtApprSubService lmtApprSubService;
    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private LmtApprService lmtApprService;
    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Resource(name="sftpFileSystemTemplate")
    public FileSystemTemplate sftpFileSystemTemplate;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtGrpAppr selectByPrimaryKey(String pkId) {
        return lmtGrpApprMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpAppr> selectAll(QueryModel model) {
        List<LmtGrpAppr> records = (List<LmtGrpAppr>) lmtGrpApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtGrpAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpAppr> list = lmtGrpApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtGrpAppr record) {
        return lmtGrpApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpAppr record) {
        return lmtGrpApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtGrpAppr record) {
        if(CmisCommonConstants.LMT_TYPE_08.equals(record.getLmtType())){
            // 如果为预授信的模式下，校验调整前的额度是否为调整后的额度
            BigDecimal originOpenAmt = new BigDecimal("0.0");
            BigDecimal originLowRiskAmt = new BigDecimal("0.0");
            BigDecimal newOpenAmt = new BigDecimal("0.0");
            BigDecimal newLowRiskAmt = new BigDecimal("0.0");
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(record.getGrpSerno());
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                LmtApp lmtApp = lmtAppService.selectBySerno(lmtGrpMemRel.getSingleSerno());
                originOpenAmt = originOpenAmt.add(lmtApp.getOrigiOpenTotalLmtAmt());
                originLowRiskAmt = originLowRiskAmt.add(lmtApp.getOrigiLowRiskTotalLmtAmt());
                newOpenAmt = newOpenAmt.add(lmtApp.getOpenTotalLmtAmt());
                newLowRiskAmt = newLowRiskAmt.add(lmtApp.getLowRiskTotalLmtAmt());
            }
            if(originOpenAmt.compareTo(newOpenAmt) != 0 || originLowRiskAmt.compareTo(newLowRiskAmt) != 0){
                throw new BizException(null, EcbEnum.ECB010099.key,null,EcbEnum.ECB010099.value);
            }
        }
        return lmtGrpApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpAppr record) {
        return lmtGrpApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpApprMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: selectLmtGrpApprByParams
     * @方法描述: 通过条件查询授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpAppr> selectLmtGrpApprByParams(HashMap paramsMap){
        return lmtGrpApprMapper.selectLmtGrpApprByParams(paramsMap);
    }

    /**
     * @方法名称: queryFinalLmtApprBySerno
     * @方法描述: 根据集团授信申请流水号查询最新一笔的集团授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpAppr queryFinalLmtGrpApprBySerno(String serno){
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("grpSerno", serno);
        // 录入时间倒序排序
        queryMap.put("sort", "input_date desc ");
        List<LmtGrpAppr> lmtGrpApprList = selectLmtGrpApprByParams(queryMap);
        LmtGrpAppr LmtGrpAppr = null;
        if (lmtGrpApprList != null && lmtGrpApprList.size() > 0){
            LmtGrpAppr = lmtGrpApprList.get(0);
        }
        return LmtGrpAppr;
    }



    /**
     * @方法名称:
     * @方法描述: 生成集团授信审批表信息
     * @参数与返回说明: 接受集团授信申请流水号，返回集团授信审批流水号
     * @算法描述:
     * 1.通过集团授信申请流水号查询最新的一笔集团授信审批（如果查不到查询集团授信申请）
     * 2.将查询到的数据插入授信审批表（如果是从审批中查询数据，则条件信息也重新插入）
     * 3.返回授信审批流水号
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtGrpApprInfo(String serno,String approveStatus) throws Exception {
        LmtGrpAppr newLmtGrpAppr;
        // 1.通过授信申请流水号查询最新的一笔授信审批（如果查不到查询授信申请）
        LmtGrpAppr lmtGrpAppr = queryFinalLmtGrpApprBySerno(serno);
        log.info("根据当前流水号查询集团客户授信申报流程审批表中最新数据信息:"+JSON.toJSONString(lmtGrpAppr));
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
        log.info("根据当前流水号查询集团客户授信申报数据信息:"+JSON.toJSONString(lmtGrpApp));
        User userInfo = SessionUtils.getUserInformation();
        if(lmtGrpAppr != null && lmtGrpAppr.getPkId() != null && !CmisFlowConstants.WF_STATUS_992.equals(approveStatus)){
            if (CmisCommonConstants.LMT_TYPE_06.equals(lmtGrpApp.getLmtType()) && CmisFlowConstants.WF_STATUS_000.equals(approveStatus)){
                log.info("授信再议时,客户经理发起流程时需要重新从申请表数据取数:"+JSON.toJSONString(lmtGrpApp));
                // 根据集团授信申请信息生成集团授信审批数据
                newLmtGrpAppr = generateLmtGrpApprByLmtGrpApp(userInfo, lmtGrpApp);
                // 通过集团授信申请，集团审批数据生成集团成员的授信审批数据
                lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp(userInfo, newLmtGrpAppr, lmtGrpApp,approveStatus);
            }else{
                log.info("审批表数据存在,则自动生成新的一条审批数据,原审批表审批流水号为:"+lmtGrpAppr.getGrpApproveSerno());
                // 根据集团授信审批信息生成集团授信审批数据
                newLmtGrpAppr = generateLmtGrpApprByLmtGrpAppr(userInfo, lmtGrpAppr);
                // 通过集团授信申请，集团审批数据生成集团成员的授信审批数据
                log.info("根据新审批表数据自动生成成员客户相关信息,新生成的审批表数据流水为:"+newLmtGrpAppr.getGrpApproveSerno());
                lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpApprByLmtGrpAppr(userInfo, lmtGrpAppr, newLmtGrpAppr);
            }
        }else{
            log.info("审批表数据不存在,则根据授信申报数据自动生成:"+JSON.toJSONString(lmtGrpApp));
            // 根据集团授信申请信息生成集团授信审批数据
            newLmtGrpAppr = generateLmtGrpApprByLmtGrpApp(userInfo, lmtGrpApp);
            // 通过集团授信申请，集团审批数据生成集团成员的授信审批数据
            lmtGrpMemRelService.generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp(userInfo, newLmtGrpAppr, lmtGrpApp,approveStatus);
        }
        return newLmtGrpAppr.getGrpSerno();
    }

    /**
     * @方法名称:
     * @方法描述: 通过集团授信申请信息生成集团授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtGrpAppr generateLmtGrpApprByLmtGrpApp(User userInfo, LmtGrpApp lmtGrpApp){
        LmtGrpAppr lmtGrpAppr = new LmtGrpAppr();
        BeanUtils.copyProperties(lmtGrpApp, lmtGrpAppr);
        lmtGrpAppr.setPkId(UUID.randomUUID().toString());
        lmtGrpAppr.setGrpApproveSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>()));
        lmtGrpAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpAppr.setUpdId(userInfo==null?"":userInfo.getLoginCode());
        lmtGrpAppr.setUpdBrId(userInfo==null?"":userInfo.getOrg().getCode());
        lmtGrpAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        lmtGrpAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        lmtGrpAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(lmtGrpAppr);
        return lmtGrpAppr;
    }

    /**
     * @方法名称:
     * @方法描述: 通过集团授信申请信息生成集团授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-06 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtGrpAppr generateLmtGrpApprByLmtGrpAppr(User userInfo, LmtGrpAppr lmtGrpAppr){
        LmtGrpAppr newLmtGrpAppr = new LmtGrpAppr();
        BeanUtils.copyProperties(lmtGrpAppr, newLmtGrpAppr);
        newLmtGrpAppr.setPkId(UUID.randomUUID().toString());
        newLmtGrpAppr.setGrpApproveSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>()));
        newLmtGrpAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtGrpAppr.setUpdId(userInfo==null?"":userInfo.getLoginCode());
        newLmtGrpAppr.setUpdBrId(userInfo==null?"":userInfo.getOrg().getCode());
        newLmtGrpAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        // 审批模式
        newLmtGrpAppr.setApprMode(lmtGrpAppService.getApprMode(lmtGrpAppr.getGrpSerno()));
        newLmtGrpAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtGrpAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        // 在流程审批中copy数据时，出局报告类型字段不纳入
        newLmtGrpAppr.setIssueReportType("");
        this.insert(newLmtGrpAppr);
        return newLmtGrpAppr;
    }

    /**
     * @方法名称: queryLmtGrpApprBySerno
     * @方法描述: 通过流水号查询到集团授信审批数据
     * @参数与返回说明:
     * @创建人: quwen
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpAppr queryLmtGrpApprByGrpSerno(String grpSerno) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("grpSerno",grpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("sort","input_date desc");
        List<LmtGrpAppr> lmtGrpApprList = lmtGrpApprMapper.selectLmtGrpApprByParams(queryMap);
        if (lmtGrpApprList != null && lmtGrpApprList.size() >= 1){
            log.info("取当前申请流水对应的最新的审查报告信息:"+JSON.toJSONString(lmtGrpApprList.get(0)));
            return lmtGrpApprList.get(0);
        }
        return new LmtGrpAppr();
    }

    /**
     * @方法名称: queryLmtGrpApprBySerno
     * @方法描述: 通过流水号查询到集团授信审批数据
     * @参数与返回说明:
     * @创建人: quwen
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtGrpAppr queryLmtGrpApprByGrpSernoAndIssueReportType(Map<String,String> params) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("grpSerno",params.get("grpSerno"));
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("issueReportType", params.get("issueReportType"));
        queryMap.put("sort", "input_date desc");
        List<LmtGrpAppr> lmtGrpApprList = lmtGrpApprMapper.selectLmtGrpApprByParams(queryMap);
        log.info("传入参数:"+JSON.toJSONString(params)+"-----查询后的结果:"+JSON.toJSONString(lmtGrpApprList));
        if (CollectionUtils.nonEmpty(lmtGrpApprList)){
            log.info("根据出具报告类型查询最新的审查报告信息:"+JSON.toJSONString(lmtGrpApprList.get(0)));
            return lmtGrpApprList.get(0);
        }else{
            return this.queryLmtGrpApprByGrpSerno(params.get("grpSerno"));
        }
    }

    /**
     * 根据集团申请流水号查询数据
     * @param grpSerno
     * @return
     */
    public LmtGrpAppr queryInfoByGrpSerno(String grpSerno) {
        return lmtGrpApprMapper.queryInfoByGrpSerno(grpSerno);
    }

    /**
     * @函数名称: updateLmtApprChoose
     * @函数描述: 根据流水号更新审批选择条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int updateLmtApprChoose(Map<String, Object> params) {
        LmtGrpAppr lmtGrpAppr = this.queryFinalLmtGrpApprBySerno((String) params.get("serno"));
        int count = 0;
        if(lmtGrpAppr != null){
            lmtGrpAppr.setIsSubToOtherDeptCom((String) params.get("isSubToOtherDeptCom"));
            lmtGrpAppr.setSubOtherDeptCom((String) params.get("subOtherDeptCom"));
            lmtGrpAppr.setIsSubOtherDeptXd((String) params.get("isSubToOtherDeptXd"));
            lmtGrpAppr.setSubOtherDeptXd((String) params.get("subOtherDeptXd"));
            lmtGrpAppr.setIsUpperApprAuth((String) params.get("isUpperApprAuth"));
            lmtGrpAppr.setUpperApprAuthType((String) params.get("upperApprAuthType"));
            lmtGrpAppr.setIsLowerApprAuth((String) params.get("isLowerApprAuth"));
            lmtGrpAppr.setIsBigLmt((String) params.get("isDAELmtApprWeb"));
            lmtGrpAppr.setApprBackReasonType((String) params.get("apprBackReasonType"));
            count = lmtGrpApprMapper.updateByPrimaryKey(lmtGrpAppr);
            log.info("更新审批表用户选择条件成功");

    /*        ResultInstanceDto resultInstanceDto = JSONObject.parseObject((String) params.get("instanceIdInfo"), ResultInstanceDto.class);
            // ResultInstanceDto resultInstanceDto = (ResultInstanceDto) params.get("instanceIdInfo");
            WFBizParamDto wfParams = new WFBizParamDto();
            wfParams.setBizId(resultInstanceDto.getBizId());
            wfParams.setInstanceId(resultInstanceDto.getInstanceId());
            Map<String, Object> routerParams = new HashMap<>();
            routerParams = lmtGrpAppService.getRouterMapResult((String) params.get("serno"));
            wfParams.setParam(routerParams);
            workflowCoreClient.updateFlowParam(wfParams);
            log.info("更新流程路由节点成功");*/
        }
        return  count;
    }

    /**
     * @函数名称: queryLmtApprByGrpSerno
     * @函数描述: 根据集团申请流水号查询成员客户审批信息
     * @参数与返回说明:
     * @算法描述:
     */

    public List<LmtGrpMemRel> queryLmtApprByGrpSerno(String serno) {
        return lmtGrpApprMapper.queryLmtApprByGrpSerno(serno);
    }

    /**
     * @函数名称: queryLmtGrpApprSubAndPrdByGrpApproveSerno
     * @函数描述: 根据集团授信审批流水号获取审批中的集团分项明细
     * @创建人: css
     */

    public List<Map> queryLmtGrpApprSubAndPrdByGrpApproveSerno(Map<String,String> params) {
        List<Map> lmtGrpAppSubDtoList = new ArrayList<>();
        List<LmtGrpAppr> lmtGrpApprList = new ArrayList<>();
        if(StringUtils.isBlank(params.get("grpApproveSerno"))){
            return lmtGrpAppSubDtoList;
        }
        HashMap paramsMap = new HashMap();
        paramsMap.put("grpApproveSerno",params.get("grpApproveSerno"));
        paramsMap.put("issueReportType",params.get("issueReportType"));
        paramsMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        paramsMap.put("sort","INPUT_DATE desc");
        lmtGrpApprList = this.selectLmtGrpApprByParams(paramsMap);
        if(CollectionUtils.isEmpty(lmtGrpApprList)){
            paramsMap.remove("issueReportType");
            lmtGrpApprList.clear();
            lmtGrpApprList = this.selectLmtGrpApprByParams(paramsMap);
        }else{
            log.info("当前申请最新的审批表数据:"+ JSON.toJSONString(lmtGrpApprList.get(0)));
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpApprList.get(0).getGrpApproveSerno());
            log.info("当前申请最新的审批表对应的成员关系表数据:"+ JSON.toJSONString(lmtGrpMemRelList));
            for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
                HashMap subDto = new HashMap();
                if (lmtGrpMemRel.getSingleSerno() != null && !"".equals(lmtGrpMemRel.getSingleSerno())) {
                    LmtAppr lmtAppr = lmtApprService.selectByApproveSerno(lmtGrpMemRel.getSingleSerno());
                    log.info("当前成员客户[{}]对应的最新的审批表数据[{}]", JSON.toJSONString(lmtGrpMemRel.getCusId()),JSON.toJSONString(lmtAppr));
                    if (CmisCommonConstants.LMT_TYPE_02.equals(lmtAppr.getLmtType())) { // 变更
                        if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                            // 不填报时,不校验当前成员客户
                            log.info("当前集团授信变更项下成员[{}]不参与本次变更",lmtAppr.getCusId());
                            continue;
                        }
                    } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtAppr.getLmtType())) { // 预授信细化
                        if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                            // 不填报时,不校验当前成员客户
                            log.info("当前集团授信预授信细化项下成员[{}]不参与本次细化",lmtAppr.getCusId());
                            continue;
                        }
                    } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtAppr.getLmtType())) { // 额度调剂
                        if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                            // 不填报时,不校验当前成员客户
                            log.info("当前集团授信额度调剂项下成员[{}]不参与本次调剂",lmtAppr.getCusId());
                            continue;
                        }
                    } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                        if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                            // 不填报时,不校验当前成员客户
                            log.info("当前集团授信申报项下成员[{}]不参与本次申报",lmtAppr.getCusId());
                            continue;
                        }
                    }
                    subDto.put("singleSerno",lmtAppr.getApproveSerno());
                    subDto.put("serno",lmtAppr.getApproveSerno());
                    subDto.put("cusId",lmtAppr.getCusId());
                    subDto.put("cusName",lmtAppr.getCusName());
                    subDto.put("cusType",lmtAppr.getCusType());
                    subDto.put("subPrdSerno",lmtAppr.getSerno());
                    subDto.put("isAdjustFlag",lmtGrpMemRel.getIsAdjustFlag());
                    subDto.put("lmtBizTypeName","");
                    subDto.put("lmtBizTypeProp","");
                    subDto.put("isRevolvLimit","");
                    subDto.put("openLmtAmt",lmtAppr.getOpenTotalLmtAmt());
                    subDto.put("lowRiskLmtAmt",lmtAppr.getLowRiskTotalLmtAmt());
                    subDto.put("managerId",lmtGrpMemRel.getManagerId());
                    subDto.put("managerBrId",lmtGrpMemRel.getManagerBrId());
                    subDto.put("lmtAmt",(lmtAppr.getOpenTotalLmtAmt()==null?new BigDecimal(0):lmtAppr.getOpenTotalLmtAmt()).add(lmtAppr.getLowRiskTotalLmtAmt()==null?new BigDecimal(0):lmtAppr.getLowRiskTotalLmtAmt()));
                    subDto.put("curType","");
                    subDto.put("rateYear","");
                    subDto.put("bailPreRate","");
                    subDto.put("guarMode","");
                    List<LmtApprSub> lmtApprSubList = lmtApprSubService.selectAllByApprSerno(lmtGrpMemRel.getSingleSerno());
                    log.info("当前成员[{}]项下的分项信息[{}]",JSON.toJSONString(lmtGrpMemRel.getCusId()),JSON.toJSONString(lmtApprSubList));
                    List<Map> subDtoChild = new ArrayList<>();
                    for(LmtApprSub lmtApprSub : lmtApprSubList){
                        List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdService.selectBySubSerno(lmtApprSub.getApproveSubSerno());
                        log.info("当前分项[{}]项下的分项品种数据[{}]",lmtApprSub.getApproveSubSerno(), JSON.toJSONString(lmtApprSubPrdList));
                        for(LmtApprSubPrd lmtApprSubPrd : lmtApprSubPrdList){
                            HashMap subDto1 = new HashMap();
                            subDto1.put("subPrdSerno",lmtApprSubPrd.getSubPrdSerno());
                            subDto1.put("isAdjustFlag",lmtGrpMemRel.getIsAdjustFlag());
                            subDto1.put("lmtBizTypeName",lmtApprSubPrd.getLmtBizTypeName());
                            subDto1.put("lmtBizTypeProp",lmtApprSubPrd.getLmtBizTypeProp());
                            subDto1.put("isRevolvLimit",lmtApprSubPrd.getIsRevolvLimit());
                            subDto1.put("lmtAmt",lmtApprSubPrd.getLmtAmt());
                            subDto1.put("curType",lmtApprSubPrd.getCurType());
                            subDto1.put("rateYear",String.valueOf(lmtApprSubPrd.getRateYear() == null ? new BigDecimal(0) : lmtApprSubPrd.getRateYear()));
                            subDto1.put("bailPreRate", String.valueOf(Optional.ofNullable(lmtApprSubPrd.getBailPreRate()).orElse(BigDecimal.ZERO)));
                            subDto1.put("guarMode",lmtApprSubPrd.getGuarMode());
                            subDtoChild.add(subDto1);
                        }
                    }
                    subDto.put("children",subDtoChild);
                }
                lmtGrpAppSubDtoList.add(subDto);
            }
        }
        return lmtGrpAppSubDtoList;
    }

    /**
     * 获取文件绝对路径
     * @param fileId
     * @param serverPath
     * @param relativePath
     * @param fileSystemTemplate
     * @return
     */
    public String getFileAbsolutePath(String fileId, String serverPath, String relativePath, FileSystemTemplate fileSystemTemplate) {
        String absolutePath = "";
        try {
            //获取图片绝对路径
            FileInfo oldFileInfo = FileInfoUtils.fromIdentity(fileId);
            if (oldFileInfo != null) {
                String fileName = oldFileInfo.getFileName();
                //路径缓存文件长度信息
                StringBuilder sb = new StringBuilder(fileName);
                sb.insert(sb.lastIndexOf("."), "_" + oldFileInfo.getFileSize());
                fileName = sb.toString();

                System.err.println("====fileName====>" + fileName);
                FileInfo newFileInfo = FileInfoUtils.createFileInfo(fileSystemTemplate, fileName, relativePath);
                FileInfoUtils.copy(oldFileInfo, newFileInfo, true);
                Path path = Paths.get(serverPath, newFileInfo.getFilePath(), fileName);
                absolutePath = path.toString();
                String newFileId = FileInfoUtils.toIdentity(newFileInfo);
                log.info("newFileName===>{}  newFileId===》{}", fileName, newFileId);
            }
        } catch (Exception e) {
            log.error("获取文件绝对路径失败===》", e);
        }
        return absolutePath;
    }

    /**
     * 更新核查报告文件路径
     * @param condition
     * @return
     */
    public String updateFilePath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String pkId = (String) condition.get("pkId");
        String indgtReportMode = (String) condition.get("indgtReportMode");
        String serverPath = (String) condition.get("serverPath");

        // 获取当前业务信息
        LmtGrpAppr lmtGrpAppr = new LmtGrpAppr();
        lmtGrpAppr.setPkId(pkId);
        lmtGrpAppr = selectByPrimaryKey(pkId);
        //获取文件绝对路径
        String fileAbsolutePath = "";
        String relativePath = "/LmtGrpAppr/"+lmtGrpAppr.getGrpSerno();
        if (!StringUtils.isBlank(fileId)){
            fileAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,sftpFileSystemTemplate);
        }


        lmtGrpAppr.setIndgtReportMode(indgtReportMode);
        lmtGrpAppr.setIndgtReportPath(fileAbsolutePath);
        updateSelective(lmtGrpAppr);
        return fileAbsolutePath;
    }

     /**
     * @函数名称: refshGrpAppData
     * @函数描述: 重新拉去授信申请数据
     * @创建人: css
     */
    public LmtGrpAppr refshGrpAppData(Map<String, String> params) {
        LmtGrpAppr newlmtGrpAppr = new LmtGrpAppr();
        LmtGrpAppr oldlmtGrpAppr = new LmtGrpAppr();
        int num = 0;
        if(StringUtils.nonBlank(params.get("grpSerno"))){
            newlmtGrpAppr = this.queryFinalLmtGrpApprBySerno(params.get("grpSerno"));
            if(!StringUtils.isBlank(newlmtGrpAppr.getIssueReportType())){
                log.info("查询当前流水号对应的最新的审批中数据,已存在出具报告类型，所以不做处理:"+JSON.toJSONString(newlmtGrpAppr));
                return newlmtGrpAppr;
            }
            log.info("查询当前流水号对应的最新的审批中数据:"+JSON.toJSONString(newlmtGrpAppr));
            HashMap map = new HashMap();
            map.put("grpSerno",params.get("grpSerno"));
            map.put("issueReportType",params.get("issueReportType"));
            map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            // 录入时间倒序排序
            map.put("sort", "input_date desc ");
            List<LmtGrpAppr> oldLmtGrpApprS = this.selectLmtGrpApprByParams(map);
            if(CollectionUtils.isEmpty(oldLmtGrpApprS)){
                return newlmtGrpAppr;
            }
            oldlmtGrpAppr = oldLmtGrpApprS.get(0);
            log.info("查询当前流水号、出具报告类型 对应的的最新的审批中数据:"+JSON.toJSONString(oldlmtGrpAppr));
            if(oldlmtGrpAppr.getGrpApproveSerno().equals(newlmtGrpAppr.getGrpApproveSerno())){
                log.info("根据当前流水号以及出具报告类型查询的审批表数据与最新的审批表数据一致,不做处理:"+JSON.toJSONString(oldlmtGrpAppr));
                return newlmtGrpAppr;
            }
            List<LmtGrpMemRel> newLmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(newlmtGrpAppr.getGrpApproveSerno());
            log.info("查询当前流水号对应的最新的审批中关系表数据:"+JSON.toJSONString(newLmtGrpMemRelList));
            if(CollectionUtils.nonEmpty(newLmtGrpMemRelList)){
                for(LmtGrpMemRel lmtGrpMemRel : newLmtGrpMemRelList){
                    LmtAppr newlmtAppr = lmtApprService.selectByApproveSerno(lmtGrpMemRel.getSingleSerno());
                    log.info("查询当前流水号对应的最新的审批中数据关系表中对应的成员授信审批数据:"+JSON.toJSONString(newlmtAppr));
                    LmtGrpMemRel oldlmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSernoAndCusId(oldlmtGrpAppr.getGrpApproveSerno(),lmtGrpMemRel.getCusId());
                    log.info("查询成员授信审批表对用的前一笔关系表数据:"+JSON.toJSONString(oldlmtGrpMemRel));
                    if(oldlmtGrpMemRel != null && !oldlmtGrpMemRel.getOprType().equals(CmisCommonConstants.OPR_TYPE_DELETE)){
                        LmtAppr oldlmtAppr = lmtApprService.selectByApproveSerno(oldlmtGrpMemRel.getSingleSerno());
                        log.info("查询成员授信审批表对用的前一笔审批表数据:"+JSON.toJSONString(oldlmtAppr));
                        // 审查
                        newlmtAppr.setGuarAssureAcc(oldlmtAppr.getGuarAssureAcc());//抵质押担保其他事项说明
                        newlmtAppr.setGeneralAssureAcc(oldlmtAppr.getGeneralAssureAcc());//保证担保其他事项说明
                        lmtApprService.update(newlmtAppr);
                        log.info("更新成员客户审查报告信息:"+JSON.toJSONString(newlmtAppr));
                        lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(newlmtAppr, oldlmtAppr);
                    }
                }
            }
            // 核查
            newlmtGrpAppr.setIndgtReportMode(oldlmtGrpAppr.getIndgtReportMode());//核查报告模式
            newlmtGrpAppr.setIndgtReportPath(oldlmtGrpAppr.getIndgtReportPath());//核查报告路径
            newlmtGrpAppr.setInteEvlu(oldlmtGrpAppr.getInteEvlu());//综合评价
            newlmtGrpAppr.setIsRestruLoan(oldlmtGrpAppr.getIsRestruLoan());//是否重组贷款
            newlmtGrpAppr.setLmtQuotaSitu(oldlmtGrpAppr.getLmtQuotaSitu());//授信限额情况
            newlmtGrpAppr.setPspManaNeed(oldlmtGrpAppr.getPspManaNeed());//风控建议
            // 审查
            newlmtGrpAppr.setLoanApprMode(oldlmtGrpAppr.getLoanApprMode());//用信审核方式
            newlmtGrpAppr.setReviewContent(oldlmtGrpAppr.getReviewContent());//审查内容
            newlmtGrpAppr.setRiskFactor(oldlmtGrpAppr.getRiskFactor());//风险因素
            newlmtGrpAppr.setReviewConclusion(oldlmtGrpAppr.getReviewConclusion());//评审结论
            newlmtGrpAppr.setRestDesc(oldlmtGrpAppr.getRestDesc());// 结论性描述
            newlmtGrpAppr.setIsOutLmtQuotaMana(oldlmtGrpAppr.getIsOutLmtQuotaMana());//是否超限额管理要求
            newlmtGrpAppr.setOtherDesc(oldlmtGrpAppr.getOtherDesc());//其他说明
            newlmtGrpAppr.setIssueReportType(StringUtils.nonBlank(params.get("issueReportType"))?params.get("issueReportType"):"");
            num = this.updateSelective(newlmtGrpAppr);
        }
        return newlmtGrpAppr;
    }

    /**
     * @函数名称: queryTotalLmtAmtByGrpApproveSerno
     * @函数描述: 根据集团授信审批流水号获取审批中的集团敞口合计和低风险合计
     * @创建人: css
     */
    public Map queryTotalLmtAmtByGrpApproveSerno(Map<String, String> params) {
        Map map = new HashMap();
        LmtGrpAppr lmtGrpAppr = lmtGrpApprMapper.selectLmtGrpApprByGrpApproveSerno(params);
        log.info("集团授信审批数据:"+JSON.toJSONString(lmtGrpAppr));
        List<LmtGrpMemRel> lmtGrpMemRelS = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(params.get("grpApproveSerno"));
        log.info("集团授信成员客户审批数据:"+JSON.toJSONString(lmtGrpMemRelS));
        BigDecimal openTotalLmtAmt = BigDecimal.ZERO;
        BigDecimal lowRiskTotalLmtAmt = BigDecimal.ZERO;
        for(LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelS){
            // 授信新增 如果不参与填报  不为集团成员客户生成审批数据 add 20210716 mashun
            if (CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpAppr.getLmtType())) { // 变更
                log.info("集团授信变更判断");
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                }
            } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpAppr.getLmtType())) { // 预授信细化
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                }
            } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpAppr.getLmtType())) { // 额度调剂
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                }
            } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                if (CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                    log.info("本次成员客户{}不填报", lmtGrpMemRel.getCusId());
                    // 不填报时,不校验当前成员客户
                    continue;
                }
            }
            openTotalLmtAmt = openTotalLmtAmt.add(lmtGrpMemRel.getOpenLmtAmt());
            lowRiskTotalLmtAmt = lowRiskTotalLmtAmt.add(lmtGrpMemRel.getLowRiskLmtAmt());
        }
        map.put("openTotalLmtAmt",openTotalLmtAmt);
        map.put("lowRiskTotalLmtAmt",lowRiskTotalLmtAmt);
        return map;
    }
}
