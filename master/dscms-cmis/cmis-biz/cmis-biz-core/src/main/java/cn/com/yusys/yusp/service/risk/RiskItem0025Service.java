package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.LmtSubPrdCtrLoanContRelDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.req.CmisLmt0061ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0061.resp.CmisLmt0061RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

/**
 * 合同环节日期校验
 */
@Service
public class RiskItem0025Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0025Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private IqpDiscAppService iqpDiscAppService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtSubPrdCtrLoanContRelService lmtSubPrdCtrLoanContRelService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * @方法名称: riskItem0025
     * @方法描述: 合同环节日期校验
     * @参数与返回说明:
     * @算法描述: 1、通用检查：授信起始日<=合同起始日<=授信到期日
     * 2、一般合同：合同到期日必须小于等于授信到期日+宽限期
     * 3、房抵e点贷：
     * （1）合同期限<=授信期限
     * （2）合同到期日<=授信到期日
     * @创建人: lixy
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0025(QueryModel queryModel) throws ParseException {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("合同环节日期校验开始*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        // 最高额授信协议申请  IQP_HIGH_AMT_AGR_APP
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX001.equals(bizType)) {
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
            if (iqpHighAmtAgrApp != null) {
                riskResultDto = checkHighContRules(iqpHighAmtAgrApp.getLmtAccNo(), iqpHighAmtAgrApp.getStartDate(), iqpHighAmtAgrApp.getCusId(), iqpHighAmtAgrApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 普通贷款合同申请  IqpLoanApp  CtrHighAmtAgrCont IqpAccpApp IqpTfLocApp IqpDiscApp IqpEntrustLoanApp PvpLoanApp
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                //合同类型 STD_CONT_TYPE 1：一般 2：最高额
                if (Objects.equals("2", iqpLoanApp.getContType())) {
                    riskResultDto = checkHighContRules(iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                    return riskResultDto;
                }
                riskResultDto = checkContRules(iqpLoanApp.getContType(), iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getEndDate(),
                        iqpLoanApp.getContTerm(), getGraper(iqpLoanApp.getLmtAccNo()), iqpLoanApp.getPrdId(), iqpLoanApp.getPrdTypeProp(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 银承合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX006.equals(bizType)) {
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
            if (iqpAccpApp != null) {
                //合同类型 STD_CONT_TYPE 1：一般 2：最高额
                if (Objects.equals("2", iqpAccpApp.getContType())) {
                    riskResultDto = checkHighContRules(iqpAccpApp.getLmtAccNo(), iqpAccpApp.getStartDate(), iqpAccpApp.getCusId(), iqpAccpApp.getManagerBrId());
                    return riskResultDto;
                }
                riskResultDto = checkContRules(iqpAccpApp.getContType(), iqpAccpApp.getLmtAccNo(), iqpAccpApp.getStartDate(), iqpAccpApp.getEndDate(), iqpAccpApp.getContTerm(),
                        getGraper(iqpAccpApp.getLmtAccNo()), iqpAccpApp.getPrdId(), iqpAccpApp.getPrdTypeProp(), iqpAccpApp.getCusId(), iqpAccpApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 开证合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX005.equals(bizType)) {
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByTfLocSernoKey(serno);
            //合同类型 STD_CONT_TYPE 1：一般 2：最高额
            if (Objects.equals("2", iqpTfLocApp.getContType())) {
                riskResultDto = checkHighContRules(iqpTfLocApp.getLmtAccNo(), iqpTfLocApp.getStartDate(), iqpTfLocApp.getCusId(), iqpTfLocApp.getManagerBrId());
                return riskResultDto;
            }
            if (iqpTfLocApp != null) {
                riskResultDto = checkContRules(iqpTfLocApp.getContType(), iqpTfLocApp.getLmtAccNo(), iqpTfLocApp.getStartDate(), iqpTfLocApp.getEndDate(),
                        iqpTfLocApp.getContTerm(), getGraper(iqpTfLocApp.getLmtAccNo()), iqpTfLocApp.getPrdId(), iqpTfLocApp.getPrdTypeProp(), iqpTfLocApp.getCusId(), iqpTfLocApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 保函合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX007.equals(bizType)) {
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if (iqpCvrgApp != null) {
                //合同类型 STD_CONT_TYPE 1：一般 2：最高额
                if (Objects.equals("2", iqpCvrgApp.getContType())) {
                    riskResultDto = checkHighContRules(iqpCvrgApp.getLmtAccNo(), iqpCvrgApp.getStartDate(), iqpCvrgApp.getCusId(), iqpCvrgApp.getManagerBrId());
                    return riskResultDto;
                }
                riskResultDto = checkContRules(iqpCvrgApp.getContType(), iqpCvrgApp.getLmtAccNo(), iqpCvrgApp.getStartDate(), iqpCvrgApp.getEndDate(),
                        iqpCvrgApp.getContTerm(), getGraper(iqpCvrgApp.getLmtAccNo()), iqpCvrgApp.getPrdId(), iqpCvrgApp.getPrdTypeProp(), iqpCvrgApp.getCusId(), iqpCvrgApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 贴现协议申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX009.equals(bizType)) {
            IqpDiscApp iqpDiscApp = iqpDiscAppService.selectByDiscSernoKey(serno);
            //合同类型 STD_DISC_CONT_TYPE 1：一般贴现协议 2：贴现额度协议
            if (Objects.equals(CmisCommonConstants.STD_DISC_CONT_TYPE_02, iqpDiscApp.getDiscContType())) {
                riskResultDto = checkHighContRules(iqpDiscApp.getLmtAccNo(), iqpDiscApp.getStartDate(), iqpDiscApp.getCusId(), iqpDiscApp.getManagerBrId());
                return riskResultDto;
            }
            if (Objects.isNull(iqpDiscApp)) {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
            // 一般贴现协议，不需要检验（因为没有日期）
            if (!Objects.equals(CmisCommonConstants.STD_DISC_CONT_TYPE_01, iqpDiscApp.getDiscContType())) {
                riskResultDto = checkContRules(iqpDiscApp.getContType(), iqpDiscApp.getLmtAccNo(), iqpDiscApp.getStartDate(), iqpDiscApp.getEndDate(),
                        iqpDiscApp.getContTerm(), getGraper(iqpDiscApp.getLmtAccNo()), iqpDiscApp.getPrdId(), iqpDiscApp.getPrdTypeProp(), iqpDiscApp.getCusId(), iqpDiscApp.getManagerBrId());
                return riskResultDto;
            }
        }
        // 贸易融资合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX003.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            //合同类型 STD_CONT_TYPE 1：一般 2：最高额
            if (Objects.equals("2", iqpLoanApp.getContType())) {
                riskResultDto = checkHighContRules(iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                return riskResultDto;
            }
            if (iqpLoanApp != null) {
                riskResultDto = checkContRules(iqpLoanApp.getContType(), iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getEndDate(),
                        iqpLoanApp.getContTerm(), getGraper(iqpLoanApp.getLmtAccNo()), iqpLoanApp.getPrdId(), iqpLoanApp.getPrdTypeProp(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                return riskResultDto;

            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 福费廷合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX004.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            //合同类型 STD_CONT_TYPE 1：一般 2：最高额
            if (Objects.equals("2", iqpLoanApp.getContType())) {
                riskResultDto = checkHighContRules(iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                return riskResultDto;
            }
            if (iqpLoanApp != null) {
                riskResultDto = checkContRules(iqpLoanApp.getContType(), iqpLoanApp.getLmtAccNo(), iqpLoanApp.getStartDate(), iqpLoanApp.getEndDate(),
                        iqpLoanApp.getContTerm(), getGraper(iqpLoanApp.getLmtAccNo()), iqpLoanApp.getPrdId(), iqpLoanApp.getPrdTypeProp(), iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 委托贷款合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)) {
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(serno);
            //合同类型 STD_CONT_TYPE 1：一般 2：最高额
            if (Objects.equals("2", iqpEntrustLoanApp.getContType())) {
                riskResultDto = checkHighContRules(iqpEntrustLoanApp.getLmtAccNo(), iqpEntrustLoanApp.getStartDate(), iqpEntrustLoanApp.getCusId(), iqpEntrustLoanApp.getManagerBrId());
                return riskResultDto;
            }
            if (iqpEntrustLoanApp != null) {
                riskResultDto = checkContRules(iqpEntrustLoanApp.getContType(), iqpEntrustLoanApp.getLmtAccNo(), iqpEntrustLoanApp.getStartDate(), iqpEntrustLoanApp.getEndDate(),
                        iqpEntrustLoanApp.getContTerm(), getGraper(iqpEntrustLoanApp.getLmtAccNo()), iqpEntrustLoanApp.getPrdId(), iqpEntrustLoanApp.getPrdTypeProp(), iqpEntrustLoanApp.getCusId(), iqpEntrustLoanApp.getManagerBrId());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 小微合同申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001.equals(bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
            if (iqpLoanApp != null) {
                riskResultDto = checkContRulesXw(iqpLoanApp);
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        // 零售合同申请
        if ("LS004".equals(bizType) || "LS008".equals(bizType)) {
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(ctrLoanCont.getIqpSerno());
            if (Objects.nonNull(ctrLoanCont) && Objects.nonNull(iqpLoanApp)) {
                riskResultDto = checkContRulesLS(ctrLoanCont);
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        log.info("合同环节日期校验结束*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 获取授信宽限期
     *
     * @param lmtAccNo
     * @return
     */
    public String getGraper(String lmtAccNo) {
        String graper = "0";
        LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
        try {
            lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if (Objects.nonNull(lmtReplyAccSubPrd)) {
                graper = Objects.isNull(lmtReplyAccSubPrd.getLmtGraperTerm()) ? "0" : String.valueOf(lmtReplyAccSubPrd.getLmtGraperTerm());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return graper;
    }

    /**
     * 校验
     * 1、通用检查：授信起始日<=合同起始日<=授信到期日
     * 2、一般合同：合同到期日必须小于等于授信到期日+宽限期
     * 3、房抵e点贷：
     * （1）合同期限<=授信期限
     * （2）合同到期日<=授信到期日
     *
     * @param contType    合同类型
     * @param lmtAccNo    授信额度分享编号
     * @param startDate   合同起始日
     * @param endDate     合同到期日
     * @param contTerm    合同期限
     * @param graper      宽限期
     * @param prdId       产品编号
     * @param prdTypeProp 产品类型属性
     * @return
     */
    private RiskResultDto checkContRules(String contType, String lmtAccNo, String startDate, String endDate,
                                         Integer contTerm, String graper, String prdId, String prdTypeProp, String cusId, String managerBrId) {
        log.info("合同环节日期校验执行开始（通用检查、一般合同、房抵e点贷）*******************业务流水号：【{}】", lmtAccNo);
        RiskResultDto riskResultDto = new RiskResultDto();
        try {
            if (Objects.isNull(contTerm)) {
                contTerm = 0;
            }
            //授信起始日
            String sxStartDate = "";
            //授信到期日
            String sxEndDate = "";
            //授信期限
            Integer sxTerm = 0;
            //宽限期
            Integer kxTerm = 0;
            if (!StringUtils.isBlank(graper)) {
                kxTerm = Integer.parseInt(graper);
            }

            boolean isSuccess = false;
            if (lmtAccNo != null && !"".equals(lmtAccNo)) {
                CmisLmt0036ReqDto cmisLmt0036ReqDto = new CmisLmt0036ReqDto();
                cmisLmt0036ReqDto.setAccSubNo(lmtAccNo);
                ResultDto<CmisLmt0036RespDto> cmisLmt0036RespDtoResultDto = cmisLmtClientService.cmislmt0036(cmisLmt0036ReqDto);
                if (Objects.isNull(cmisLmt0036RespDtoResultDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02507);
                    return riskResultDto;
                }
                log.info("0025lmtAccNo:{} retVal===>{}", lmtAccNo, cmisLmt0036RespDtoResultDto.toString());
                System.out.println("cmisLmt0036RespDtoResultDto====>" + cmisLmt0036RespDtoResultDto.toString());
                if (cmisLmt0036RespDtoResultDto.getData().getErrorCode().equals(SuccessEnum.SUCCESS.key)) {
                    CmisLmt0036RespDto data = cmisLmt0036RespDtoResultDto.getData();
                    if (data != null) {
                        sxStartDate = data.getStartDate();
                        sxEndDate = data.getEndDate();
                        sxTerm = Objects.nonNull(data.getTerm()) ? data.getTerm() : 0;
                        isSuccess = true;
                    }
                }
                //1、通用检查：授信起始日<=合同起始日<=授信到期日(拦截条件：相反)
                if (sxStartDate.compareTo(startDate) <= 0
                        && startDate.compareTo(sxEndDate) <= 0) {
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                    log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                    return riskResultDto;
                }

                //
                // 2021年8月30日16:23:23  hubp   一般合同：合同到期日必须小于等于授信到期日+宽限期 -》》》2、通用检查：
                //（1）合同到期日必须小于等于授信到期日+宽限期
                //（2）合同期限不能大于授信期限

                Date contEndDates = DateUtils.parseDate(endDate, "yyyy-MM-dd");
                Date deadLine = DateUtils.addMonth(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd"), kxTerm);
                //(拦截条件：合同到期日 > 授信到期日+宽限期)
                if (contEndDates.getTime() > deadLine.getTime()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02504);
                    log.info("0025校验详情：contEndDate：{} ==》deadLine：{} ", DateUtils.formatDate(contEndDates, "yyyyMMdd"), DateUtils.formatDate(deadLine, "yyyyMMdd"));
                    return riskResultDto;
                }
//                if (contTerm > sxTerm) {
//                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02509);
//                    return riskResultDto;
//                }
                //个人经营性贷款-产品(其他个人经营性贷款) - 房抵e点贷
                // （1）合同期限<=授信期限
                // （2）合同到期日<=授信到期日
                if (CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD_022011.equals(prdId)
                        && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD_022011_P034.equals(prdTypeProp)) {
                    //（1）合同期限<=授信期限 (拦截条件：合同期限>授信期限)
                    if (contTerm > sxTerm) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02501);
                        return riskResultDto;
                    }
                    //（2）合同到期日<=授信到期日 (拦截条件：合同到期日>授信到期日)
                    if (endDate.compareTo(sxEndDate) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02501);
                        return riskResultDto;
                    }
                }
                //诚易融产品
                //合同起始日<=授信关联合同中最早到期合同到期日
                if (CmisBizConstants.CHENG_YI_RONG.equals(prdTypeProp)) {
                    Map param = new HashMap<>();
                    param.put("accSubPrdNo", lmtAccNo);
                    LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryByParams(param);
                    if (Objects.nonNull(lmtReplyAccSubPrd)) {
                        String subPrdSerno = lmtReplyAccSubPrd.getSubPrdSerno();
                        List<LmtSubPrdCtrLoanContRelDto> lmtSubPrdCtrLoanContRelDtos = lmtSubPrdCtrLoanContRelService.selectBySubPrdSerno(subPrdSerno);
                        if (CollectionUtils.nonEmpty(lmtSubPrdCtrLoanContRelDtos)) {
                            String cyrEndDate = "";
                            for (LmtSubPrdCtrLoanContRelDto lmtSubPrdCtrLoanContRelDto : lmtSubPrdCtrLoanContRelDtos) {
                                String contEndDate = lmtSubPrdCtrLoanContRelDto.getContEndDate();
                                if (StringUtils.isBlank(cyrEndDate)) {
                                    cyrEndDate = contEndDate;
                                } else {
                                    if (contEndDate.compareTo(cyrEndDate) < 0) {
                                        cyrEndDate = contEndDate;
                                    }
                                }
                            }
                            if (startDate.compareTo(cyrEndDate) > 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02508);
                                return riskResultDto;
                            }
                        }
                    }
                }
            } else {
                // 低风险反向生成额度的情况，需要与批复台账的起始日到期日比较
                String cusType = "";
                String substring = cusId.substring(0, 1);
                if ("8".equals(substring)) {
                    cusType = CmisLmtConstants.STD_ZB_CUS_CATALOG2;
                } else {
                    cusType = CmisLmtConstants.STD_ZB_CUS_CATALOG1;
                }
                CmisLmt0061ReqDto cmisLmt0061ReqDto = new CmisLmt0061ReqDto();
                cmisLmt0061ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerBrId));
                cmisLmt0061ReqDto.setCusId(cusId);
                cmisLmt0061ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
                cmisLmt0061ReqDto.setCusType(cusType);
                log.info("通过客户编号【{}】，前往额度系统查询客户的批复信息开始,请求报文为:【{}】", cusId, cmisLmt0061ReqDto.toString());
                ResultDto<CmisLmt0061RespDto> resultDtoDto = cmisLmtClientService.cmislmt0061(cmisLmt0061ReqDto);
                log.info("通过客户编号【{}】，前往额度系统查询客户的批复信息结束,响应报文为:【{}】", cusId, resultDtoDto.toString());
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("接口调用失败！");
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(resultDtoDto.getData().getErrorCode())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(resultDtoDto.getData().getErrorMsg());
                    return riskResultDto;
                }
                sxStartDate = resultDtoDto.getData().getStartDate();
                sxEndDate = resultDtoDto.getData().getEndDate();
                if (DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxStartDate, "yyyy-MM-dd")) < 0 || DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                    log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                    return riskResultDto;
                }
            }
        } catch (Exception e) {
            log.error("合同环节日期校验失败！", e);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02506);
            return riskResultDto;
        }
        log.info("合同环节日期校验执行结束（通用检查、一般合同、房抵e点贷）*******************业务流水号：【{}】", lmtAccNo);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 校验
     * 最高额合同、最高额授信协议：授信起始日<=合同起始日<=授信到期日
     *
     * @param lmtAccNo  授信额度编号
     * @param startDate 合同起始日
     * @return
     */
    private RiskResultDto checkHighContRules(String lmtAccNo, String startDate, String cusId, String managerBrId) throws ParseException {
        log.info("合同环节日期校验执行开始（最高额授信协议、最高额合同）*******************授信额度编号：【{}】", lmtAccNo);
        RiskResultDto riskResultDto = new RiskResultDto();
        //授信起始日
        String sxStartDate = "";
        //授信到期日
        String sxEndDate = "";
        if (lmtAccNo != null && !"".equals(lmtAccNo)) {
            CmisLmt0036ReqDto cmisLmt0036ReqDto = new CmisLmt0036ReqDto();
            cmisLmt0036ReqDto.setAccSubNo(lmtAccNo);
            log.info("前往额度系统进行查询授信日期信息开始,请求报文为:【{}】", cmisLmt0036ReqDto.toString());
            ResultDto<CmisLmt0036RespDto> cmisLmt0036RespDtoResultDto = cmisLmtClientService.cmislmt0036(cmisLmt0036ReqDto);
            log.info("前往额度系统进行查询授信日期信息结束,响应报文为:【{}】", cmisLmt0036RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0036RespDtoResultDto.getCode())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02507);
                return riskResultDto;
            }
            if (!"0000".equals(cmisLmt0036RespDtoResultDto.getData().getErrorCode())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(cmisLmt0036RespDtoResultDto.getData().getErrorMsg());
                return riskResultDto;
            }

            CmisLmt0036RespDto data = cmisLmt0036RespDtoResultDto.getData();
            if (data != null && !"".equals(data.getStartDate())) {
                sxStartDate = data.getStartDate();
                sxEndDate = data.getEndDate();
                if (DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxStartDate, "yyyy-MM-dd")) < 0 || DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd")) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                    log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                    return riskResultDto;
                }
            }
        } else {
            // 低风险反向生成额度的情况，需要与批复台账的起始日到期日比较
            String cusType = "";
            String substring = cusId.substring(0, 1);
            if ("8".equals(substring)) {
                cusType = CmisLmtConstants.STD_ZB_CUS_CATALOG2;
            } else {
                cusType = CmisLmtConstants.STD_ZB_CUS_CATALOG1;
            }
            CmisLmt0061ReqDto cmisLmt0061ReqDto = new CmisLmt0061ReqDto();
            cmisLmt0061ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerBrId));
            cmisLmt0061ReqDto.setCusId(cusId);
            cmisLmt0061ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
            cmisLmt0061ReqDto.setCusType(cusType);
            log.info("通过客户编号【{}】，前往额度系统查询客户的批复信息开始,请求报文为:【{}】", cusId, cmisLmt0061ReqDto.toString());
            ResultDto<CmisLmt0061RespDto> resultDtoDto = cmisLmtClientService.cmislmt0061(cmisLmt0061ReqDto);
            log.info("通过客户编号【{}】，前往额度系统查询客户的批复信息结束,响应报文为:【{}】", cusId, resultDtoDto.toString());
            if (!"0".equals(resultDtoDto.getCode())) {
                log.error("接口调用失败！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (!"0000".equals(resultDtoDto.getData().getErrorCode())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(resultDtoDto.getData().getErrorMsg());
                return riskResultDto;
            }
            sxStartDate = resultDtoDto.getData().getStartDate();
            sxEndDate = resultDtoDto.getData().getEndDate();
            if (DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxStartDate, "yyyy-MM-dd")) < 0 || DateUtils.parseDate(startDate, "yyyy-MM-dd").compareTo(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd")) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                return riskResultDto;
            }
        }
        log.info("合同环节日期校验执行结束（最高额授信协议、最高额合同）*******************授信额度编号：【{}】", lmtAccNo);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/10/24 14:20
     * @version 1.0.0
     * @desc 小微走这个
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto checkContRulesXw(IqpLoanApp iqpLoanApp) {
        log.info("合同环节日期校验执行开始（小微检查）*******************额度分项编号：【{}】", iqpLoanApp.getLmtAccNo());
        String contType = iqpLoanApp.getContType();
        String lmtAccNo = iqpLoanApp.getLmtAccNo();
        String startDate = iqpLoanApp.getStartDate();
        String endDate = iqpLoanApp.getEndDate();
        Integer contTerm = iqpLoanApp.getContTerm();
        String prdId = iqpLoanApp.getPrdId();
        RiskResultDto riskResultDto = new RiskResultDto();
        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = null;
        try {
            if (Objects.isNull(contTerm)) {
                contTerm = 0;
            }
            //授信起始日
            String sxStartDate = "";
            //授信到期日
            String sxEndDate = "";
            //授信期限
            Integer sxTerm = 0;
            //宽限期
            Integer kxTerm = 0;

            // 查询产品信息
            ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {// 未查询到产品信息
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02510);
                return riskResultDto;
            } else {
                cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
            }

            // 前往额度系统查询额度信息
            boolean isSuccess = false;
            CmisLmt0036ReqDto cmisLmt0036ReqDto = new CmisLmt0036ReqDto();
            cmisLmt0036ReqDto.setAccSubNo(lmtAccNo);
            ResultDto<CmisLmt0036RespDto> cmisLmt0036RespDtoResultDto = cmisLmtClientService.cmislmt0036(cmisLmt0036ReqDto);
            if (Objects.isNull(cmisLmt0036RespDtoResultDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02507);
                return riskResultDto;
            }
            log.info("0025lmtAccNo:{} retVal===>{}", lmtAccNo, cmisLmt0036RespDtoResultDto.toString());
            System.out.println("cmisLmt0036RespDtoResultDto====>" + cmisLmt0036RespDtoResultDto.toString());
            if (cmisLmt0036RespDtoResultDto.getData().getErrorCode().equals(SuccessEnum.SUCCESS.key)) {
                CmisLmt0036RespDto data = cmisLmt0036RespDtoResultDto.getData();
                if (data != null) {
                    sxStartDate = data.getStartDate();
                    sxEndDate = data.getEndDate();
                    sxTerm = Objects.nonNull(data.getTerm()) ? data.getTerm() : 0;
                    isSuccess = true;
                }
            }
            if (!isSuccess) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02505);
                return riskResultDto;
            }
                log.info("额度分项编号【{}】,小微合同日期校验开始", lmtAccNo);
                // 宽限期一个月
                kxTerm = 1;
                // 1、通用检查：授信起始日<=合同起始日<=授信到期日(拦截条件：相反)
                if (sxStartDate.compareTo(startDate) <= 0
                        && startDate.compareTo(sxEndDate) <= 0) {
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                    log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                    return riskResultDto;
                }

                /*
                 * 到期日判断
                 * 1、一般合同，合同到期日必须小于等于授信到期日+宽限期
                 * 2、最高额合同 抵押、质押类：批复授信期限5年内（含），合同债权期限最高可签5年，五年外 合同到期日必须小于等于授信到期日+宽限期
                 * 3、最高额合同 其他担保方式：合同到期日必须小于等于授信到期日+宽限期
                 */
                Date contEndDates = DateUtils.parseDate(endDate, "yyyy-MM-dd");
                Date deadLine = DateUtils.addMonth(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd"), kxTerm);
                if (Objects.equals("2", contType)) {// 最高额合同信息 2--最高额合同
                    if (Objects.equals("10", iqpLoanApp.getGuarWay()) || Objects.equals("20", iqpLoanApp.getGuarWay())) { // 担保方式为抵押、质押
                        if (sxTerm <= 60) {// 授信期限小于等于五年，合同债权期限最高可签5年
                            deadLine = DateUtils.addMonth(DateUtils.parseDate(startDate, "yyyy-MM-dd"), 60);
                        } else {// 授信期限大于五年，合同期限小于等于授信期限加宽限期
                            deadLine = deadLine;
                        }
                    } else {// 其他担保方式，合同期限小于等于授信期限加宽限期
                        deadLine = deadLine;
                    }
                } else {// 一般担保合同，同期限小于等于授信期限加宽限期
                    deadLine = deadLine;
                }

                //(拦截条件：合同到期日 > 授信到期日+宽限期)
                if (contEndDates.getTime() > deadLine.getTime()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02504);
                    log.info("0025校验详情：contEndDate：{} ==》deadLine：{} ", DateUtils.formatDate(contEndDates, "yyyyMMdd"), DateUtils.formatDate(deadLine, "yyyyMMdd"));
                    return riskResultDto;
                }
                log.info("额度分项编号【{}】,小微合同日期校验结束", lmtAccNo);
        } catch (Exception e) {
            log.error("合同环节日期校验失败！", e);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02506);
            return riskResultDto;
        }
        log.info("合同环节日期校验执行结束（小微检查）*******************额度分项编号：【{}】", iqpLoanApp.getLmtAccNo());
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/11/25 16:56
     * @version 1.0.0
     * @desc 零售走这个
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto checkContRulesLS(CtrLoanCont ctrLoanCont) {
        log.info("合同环节日期校验执行开始（零售检查）*******************额度分项编号：【{}】", ctrLoanCont.getLmtAccNo());
        String contType = ctrLoanCont.getContType();
        String lmtAccNo = ctrLoanCont.getLmtAccNo();
        String startDate = ctrLoanCont.getContStartDate();
        String endDate = ctrLoanCont.getContEndDate();
        Integer contTerm = ctrLoanCont.getContTerm();
        String prdId = ctrLoanCont.getPrdId();
        RiskResultDto riskResultDto = new RiskResultDto();
        CfgPrdBasicinfoDto cfgPrdBasicinfoDto = null;
        try {
            if (Objects.isNull(contTerm)) {
                contTerm = 0;
            }
            //授信起始日
            String sxStartDate = "";
            //授信到期日
            String sxEndDate = "";
            //授信期限
            Integer sxTerm = 0;
            //宽限期
            Integer kxTerm = 0;

            // 查询产品信息
            ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {// 未查询到产品信息
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02510);
                return riskResultDto;
            } else {
                cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
            }

            // 前往额度系统查询额度信息
            boolean isSuccess = false;
            CmisLmt0036ReqDto cmisLmt0036ReqDto = new CmisLmt0036ReqDto();
            cmisLmt0036ReqDto.setAccSubNo(lmtAccNo);
            ResultDto<CmisLmt0036RespDto> cmisLmt0036RespDtoResultDto = cmisLmtClientService.cmislmt0036(cmisLmt0036ReqDto);
            if (Objects.isNull(cmisLmt0036RespDtoResultDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02507);
                return riskResultDto;
            }
            log.info("0025lmtAccNo:{} retVal===>{}", lmtAccNo, cmisLmt0036RespDtoResultDto.toString());
            if (cmisLmt0036RespDtoResultDto.getData().getErrorCode().equals(SuccessEnum.SUCCESS.key)) {
                CmisLmt0036RespDto data = cmisLmt0036RespDtoResultDto.getData();
                if (data != null) {
                    sxStartDate = data.getStartDate();
                    sxEndDate = data.getEndDate();
                    sxTerm = Objects.nonNull(data.getTerm()) ? data.getTerm() : 0;
                    isSuccess = true;
                }
            }
            if (!isSuccess) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02505);
                return riskResultDto;
            }
                log.info("额度分项编号【{}】,零售合同日期校验开始", lmtAccNo);
                // 宽限期一个月 按揭类 3个月 非按揭类 1个月 11-- 非按揭类  10--按揭类
                kxTerm = Objects.equals("11", cfgPrdBasicinfoDto.getPrdType()) ? 1 : 3;
                // 1、通用检查：授信起始日<=合同起始日<=授信到期日(拦截条件：相反)
                if (sxStartDate.compareTo(startDate) <= 0
                        && startDate.compareTo(sxEndDate) <= 0) {
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02503);
                    log.info("0025校验详情：sxStartDate：{} ==》startDate：{} ===》 sxEndDate：{}", sxStartDate, startDate, sxEndDate);
                    return riskResultDto;
                }

                /*
                 * 到期日判断
                 * 1、一般合同，合同到期日必须小于等于授信到期日+宽限期
                 * 2、最高额合同 抵押、质押类：批复授信期限5年内（含），合同债权期限最高可签5年，五年外 合同到期日必须小于等于授信到期日+宽限期
                 * 3、最高额合同 其他担保方式：合同到期日必须小于等于授信到期日+宽限期
                 */
                Date contEndDates = DateUtils.parseDate(endDate, "yyyy-MM-dd");
                Date deadLine = DateUtils.addMonth(DateUtils.parseDate(sxEndDate, "yyyy-MM-dd"), kxTerm);
                //(拦截条件：合同到期日 > 授信到期日+宽限期)
                if (contEndDates.getTime() > deadLine.getTime()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02504);
                    log.info("0025校验详情：contEndDate：{} ==》deadLine：{} ", DateUtils.formatDate(contEndDates, "yyyyMMdd"), DateUtils.formatDate(deadLine, "yyyyMMdd"));
                    return riskResultDto;
                }
                log.info("额度分项编号【{}】,零售合同日期校验结束", lmtAccNo);
        } catch (Exception e) {
            log.error("合同环节日期校验失败！", e);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02506);
            return riskResultDto;
        }
        log.info("合同环节日期校验执行结束（零售检查）*******************额度分项编号：【{}】", ctrLoanCont.getLmtAccNo());
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
