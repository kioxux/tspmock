package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.ZxdPreLmtApply;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp.Wxp001RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ZxdPreLmtApplyMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: ZxdPreLmtApplyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-08-06 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class ZxdPreLmtApplyService {
    private static final Logger log = LoggerFactory.getLogger(ZxdPreLmtApplyService.class);
    @Autowired
    private ZxdPreLmtApplyMapper zxdPreLmtApplyMapper;
    @Autowired
    private Dscms2WxClientService dscms2WxClientService;

    /**
     * @方法名称: selectByModel
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> selectByModel(String corpId) {
        Map<String, String> map = new HashMap<>();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("corpId", corpId);
        queryModel.addCondition("ifAdmitSys", CmisBizConstants.YES);
        List<ZxdPreLmtApply> zxdPreLmtApplys = zxdPreLmtApplyMapper.selectByModel(queryModel);
        if (CollectionUtils.nonEmpty(zxdPreLmtApplys)) {
            ZxdPreLmtApply zxdPreLmtApply = zxdPreLmtApplys.get(0);
            map.put("maxLmtAmt", zxdPreLmtApply.getMaxLmtAmt().toString());
        } else {
            map.put("maxLmtAmt", "0");
        }
        return map;
    }

    /**
     * @方法名称: queryZxdPreLmtApplyList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<ZxdPreLmtApply> queryZxdPreLmtApplyList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ZxdPreLmtApply> list = zxdPreLmtApplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryZxdPreLmtApplyByParams
     * @方法描述: 根据入参查询申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-08-02 14:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public ZxdPreLmtApply queryZxdPreLmtApplyByParams(Map queryMap) {
        return zxdPreLmtApplyMapper.queryZxdPreLmtApplyByParams(queryMap);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(ZxdPreLmtApply record) {
        return zxdPreLmtApplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: queryZxdPreLmtApply
     * @方法描述: 条件查询 - 查询进行分页,不适用管护条件
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ZxdPreLmtApply> queryZxdPreLmtApply(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ZxdPreLmtApply> list = zxdPreLmtApplyMapper.queryZxdPreLmtApply(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: ZXDSendToWx
     * @方法描述: 征信贷预授信评分卡手动准入
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public Map ZXDSendToWx(ZxdPreLmtApply record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String applyRecordId = "";
        String corpName = "";
        String corpId = "";
        String admres = "";
        String reason = "";
        BigDecimal maxLmtAmt = BigDecimal.ZERO ;
        String launchOrg = "";
        try {
            applyRecordId = record.getApplyRecordId();
            corpName = record.getCorpName();
            corpId = record.getCorpId();
            admres = "AGREED";//手动准入默认同意
            reason= "同意";
            maxLmtAmt = record.getMaxLmtAmt();
            launchOrg =  record.getLaunchOrg();

            // 调用Wxp001接口,当返回成功时，更新发送状态与是否准入状态
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端,对接记录ID【{}】", record.getApplyRecordId());
            Wxp001ReqDto wxp001ReqDto = new Wxp001ReqDto();
            wxp001ReqDto.setApplid(applyRecordId);
            wxp001ReqDto.setEntnam(corpName);
            wxp001ReqDto.setEntpid(corpId);
            wxp001ReqDto.setAdmres(admres);
            wxp001ReqDto.setReason(reason);
            wxp001ReqDto.setPreamt(maxLmtAmt);
//            wxp001ReqDto.setEsbFlag("Y");
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端请求：" + wxp001ReqDto);
            ResultDto<Wxp001RespDto> wxp001RespResultDto = dscms2WxClientService.wxp001(wxp001ReqDto);
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端返回：" + wxp001RespResultDto);
            String wxp001Code = Optional.ofNullable(wxp001RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String wxp001Meesage = Optional.ofNullable(wxp001RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端返回信息：" + wxp001Meesage);
            if (Objects.equals(wxp001Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                record.setStatus("2");//状态变成已发送移动端
                record.setIfAdmitSys(admres);//是否准入变成准入
                record.setMaxLmtAmt(maxLmtAmt);
                log.info("更新征信贷预授信评分卡准入，状态及最大授信金额：" + record);
                this.update(record);
            } else {
                rtnCode = wxp001Code;
                rtnMsg = wxp001Meesage;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("征信贷预授信评分卡提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: ZXDSendToWxpRefuse
     * @方法描述: 征信贷预授信评分卡手动拒绝
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public Map ZXDSendToWxpRefuse(ZxdPreLmtApply record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String applyRecordId = "";
        String corpName = "";
        String corpId = "";
        String admres = "";
        String reason = "";
        BigDecimal maxLmtAmt = BigDecimal.ZERO ;
        String launchOrg = "";
        try {
            applyRecordId = record.getApplyRecordId();
            corpName = record.getCorpName();
            corpId = record.getCorpId();
            admres = "DISAGREED";//手动准入默认同意
            reason= "不同意";
            maxLmtAmt = BigDecimal.ZERO;//手动拒绝默认0
            launchOrg =  record.getLaunchOrg();

            // 调用Wxp001接口,当返回成功时，更新发送状态与是否准入状态
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端,对接记录ID【{}】", record.getApplyRecordId());
            Wxp001ReqDto wxp001ReqDto = new Wxp001ReqDto();
            wxp001ReqDto.setApplid(applyRecordId);
            wxp001ReqDto.setEntnam(corpName);
            wxp001ReqDto.setEntpid(corpId);
            wxp001ReqDto.setAdmres(admres);
            wxp001ReqDto.setReason(reason);
            wxp001ReqDto.setPreamt(maxLmtAmt);
//            wxp001ReqDto.setEsbFlag("Y");
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端请求：" + wxp001ReqDto);
            ResultDto<Wxp001RespDto> wxp001RespResultDto = dscms2WxClientService.wxp001(wxp001ReqDto);
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端返回：" + wxp001RespResultDto);
            String wxp001Code = Optional.ofNullable(wxp001RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String wxp001Meesage = Optional.ofNullable(wxp001RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("征信贷预授信评分卡：信贷将审批结果推送给移动端返回信息：" + wxp001Meesage);
            if (Objects.equals(wxp001Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                record.setStatus("0");//状态变成已发送移动端
                record.setIfAdmitSys(admres);//是否准入变成准入
                record.setMaxLmtAmt(maxLmtAmt);
                log.info("更新征信贷预授信评分卡准入，状态及最大授信金额：" + record);
                this.update(record);
            } else {
                rtnCode = wxp001Code;
                rtnMsg = wxp001Meesage;
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("征信贷预授信评分卡提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }
}
