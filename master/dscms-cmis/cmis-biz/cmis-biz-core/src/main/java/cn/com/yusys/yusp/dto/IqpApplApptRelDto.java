package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptRel
 * @类描述: iqp_appl_appt_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:23:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApplApptRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 申请人编号 **/
	private String apptCode;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 配偶编号 **/
	private String spouseApptCode;
	
	/** 配偶客户编号 **/
	private String spouseCusId;
	
	/** 关系类型 **/
	private String relType;
	
	/** 最后修改时间 **/
	private String updDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 操作类型 **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param spouseApptCode
	 */
	public void setSpouseApptCode(String spouseApptCode) {
		this.spouseApptCode = spouseApptCode == null ? null : spouseApptCode.trim();
	}
	
    /**
     * @return SpouseApptCode
     */	
	public String getSpouseApptCode() {
		return this.spouseApptCode;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId == null ? null : spouseCusId.trim();
	}
	
    /**
     * @return SpouseCusId
     */	
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param relType
	 */
	public void setRelType(String relType) {
		this.relType = relType == null ? null : relType.trim();
	}
	
    /**
     * @return RelType
     */	
	public String getRelType() {
		return this.relType;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}