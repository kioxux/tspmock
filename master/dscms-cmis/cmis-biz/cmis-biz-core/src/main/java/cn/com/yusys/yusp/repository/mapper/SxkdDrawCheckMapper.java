/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.SxkdDrawCheck;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdDrawCheckMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-11 22:06:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface SxkdDrawCheckMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    SxkdDrawCheck selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<SxkdDrawCheck> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(SxkdDrawCheck record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(SxkdDrawCheck record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(SxkdDrawCheck record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(SxkdDrawCheck record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: getJjhCount
     * @方法描述: 查询数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    int getJjhCount(Map map);

    /**
     * @方法名称: getJjhSernoInfo
     * @方法描述: 获取流水号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getJjhSernoInfo(Map map);

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    SxkdDrawCheck selectByPvpSerno(@Param("pvpSerno")String pvpSerno);
}