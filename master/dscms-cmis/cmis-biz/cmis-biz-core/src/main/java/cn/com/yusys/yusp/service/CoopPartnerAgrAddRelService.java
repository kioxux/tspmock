/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAccInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAccInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAddRel;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAddRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAddRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-10 01:50:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerAgrAddRelService {

    @Autowired
    private CoopPartnerAgrAddRelMapper coopPartnerAgrAddRelMapper;

    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPartnerAgrAddRel selectByPrimaryKey(String pkId) {
        return coopPartnerAgrAddRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPartnerAgrAddRel> selectAll(QueryModel model) {
        List<CoopPartnerAgrAddRel> records = (List<CoopPartnerAgrAddRel>) coopPartnerAgrAddRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPartnerAgrAccInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfos = new ArrayList<CoopPartnerAgrAccInfo>();
        List<CoopPartnerAgrAddRel> list = coopPartnerAgrAddRelMapper.selectByModel(model);
        for(CoopPartnerAgrAddRel coopPartnerAgrAddRel:list){
            CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = coopPartnerAgrAccInfoMapper.selectByPrimaryKey(coopPartnerAgrAddRel.getCoopAgrNo());
            coopPartnerAgrAccInfo.setCoopAgrSerno(coopPartnerAgrAddRel.getPkId());
            coopPartnerAgrAccInfos.add(coopPartnerAgrAccInfo);
        }
        PageHelper.clearPage();
        return coopPartnerAgrAccInfos;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopPartnerAgrAddRel record) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", record.getSerno());
        queryModel.addCondition("coopAgrNo",record.getCoopAgrNo());
        List<CoopPartnerAgrAddRel> coopPartnerAgrAddRels = coopPartnerAgrAddRelMapper.selectByModel(queryModel);
        if(coopPartnerAgrAddRels.size() > 0){
            throw BizException.error(null, null, "协议编号【" + record.getCoopAgrNo() + "】已存在，禁止重复添加");
        }
        return coopPartnerAgrAddRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPartnerAgrAddRel record) {
        return coopPartnerAgrAddRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopPartnerAgrAddRel record) {
        return coopPartnerAgrAddRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPartnerAgrAddRel record) {
        return coopPartnerAgrAddRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return coopPartnerAgrAddRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPartnerAgrAddRelMapper.deleteByIds(ids);
    }
}
