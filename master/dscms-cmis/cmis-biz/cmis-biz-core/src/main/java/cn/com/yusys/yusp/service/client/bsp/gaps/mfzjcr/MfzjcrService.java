package cn.com.yusys.yusp.service.client.bsp.gaps.mfzjcr;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req.MfzjcrReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.resp.MfzjcrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2GapsClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author shenli
 * @version 1.0.0
 * @date 2021-10-12 17:07:29
 * @desc    身份证核查
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class MfzjcrService {
    private static final Logger logger = LoggerFactory.getLogger(MfzjcrService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2GapsClientService dscms2GapsClientService;

    /**
     * @param mfzjcrReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto
     * @author shenli
     * @date 2021-10-12 17:11:50
     * @version 1.0.0
     * @desc    买方资金存入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public MfzjcrRespDto mfzjcr (MfzjcrReqDto mfzjcrReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value, JSON.toJSONString(mfzjcrReqDto));
        ResultDto<MfzjcrRespDto> mfzjcrRespDtoResultDto = dscms2GapsClientService.mfzjcr(mfzjcrReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value, JSON.toJSONString(mfzjcrRespDtoResultDto));
        String idchekCode = Optional.ofNullable(mfzjcrRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String idchekMeesage = Optional.ofNullable(mfzjcrRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        MfzjcrRespDto mfzjcrRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, mfzjcrRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            mfzjcrRespDto = mfzjcrRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(idchekCode, idchekMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value);
        return mfzjcrRespDto;
    }
}
