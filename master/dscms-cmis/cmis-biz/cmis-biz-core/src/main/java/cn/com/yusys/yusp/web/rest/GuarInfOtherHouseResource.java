/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import cn.com.yusys.yusp.domain.GuarInfMortgageOther;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfOtherHouse;
import cn.com.yusys.yusp.service.GuarInfOtherHouseService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfOtherHouseResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfotherhouse")
public class GuarInfOtherHouseResource {
    @Autowired
    private GuarInfOtherHouseService guarInfOtherHouseService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfOtherHouse>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfOtherHouse> list = guarInfOtherHouseService.selectAll(queryModel);
        return new ResultDto<List<GuarInfOtherHouse>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfOtherHouse>> index(QueryModel queryModel) {
        List<GuarInfOtherHouse> list = guarInfOtherHouseService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfOtherHouse>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfOtherHouse> show(@PathVariable("serno") String serno) {
        GuarInfOtherHouse guarInfOtherHouse = guarInfOtherHouseService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfOtherHouse>(guarInfOtherHouse);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfOtherHouse> create(@RequestBody GuarInfOtherHouse guarInfOtherHouse) throws URISyntaxException {
        guarInfOtherHouseService.insert(guarInfOtherHouse);
        return new ResultDto<GuarInfOtherHouse>(guarInfOtherHouse);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfOtherHouse guarInfOtherHouse) throws URISyntaxException {
        int result = guarInfOtherHouseService.update(guarInfOtherHouse);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfOtherHouseService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfOtherHouseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfOtherHouse
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfOtherHouse")
    protected ResultDto<Integer> preserveGuarInfOtherHouse(@RequestBody GuarInfOtherHouse guarInfOtherHouse) {
        int result = guarInfOtherHouseService.preserveGuarInfOtherHouse(guarInfOtherHouse);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfOtherHouse> queryBySerno(@RequestBody GuarInfOtherHouse record) {
        GuarInfOtherHouse guarInfOtherHouse = guarInfOtherHouseService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfOtherHouse>(guarInfOtherHouse);
    }
}
