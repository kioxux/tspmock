/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPld
 * @类描述: rpt_lmt_repay_anys_guar_pld数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 22:52:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_lmt_repay_anys_guar_pld")
public class RptLmtRepayAnysGuarPld extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 总体概述 **/
	@Column(name = "GENERAL_OVERVIEW", unique = false, nullable = true, length = 65535)
	private String generalOverview;
	
	/** 评估公司名称 **/
	@Column(name = "ASSESSMENT_COM_NAME", unique = false, nullable = true, length = 40)
	private String assessmentComName;
	
	/** 评估日期 **/
	@Column(name = "ASSESSMENT_DATE", unique = false, nullable = true, length = 20)
	private String assessmentDate;
	
	/** 抵押物其他说明事项 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 质押物其他说明事项 **/
	@Column(name = "ZY_OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String zyOtherDesc;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param generalOverview
	 */
	public void setGeneralOverview(String generalOverview) {
		this.generalOverview = generalOverview;
	}
	
    /**
     * @return generalOverview
     */
	public String getGeneralOverview() {
		return this.generalOverview;
	}
	
	/**
	 * @param assessmentComName
	 */
	public void setAssessmentComName(String assessmentComName) {
		this.assessmentComName = assessmentComName;
	}
	
    /**
     * @return assessmentComName
     */
	public String getAssessmentComName() {
		return this.assessmentComName;
	}
	
	/**
	 * @param assessmentDate
	 */
	public void setAssessmentDate(String assessmentDate) {
		this.assessmentDate = assessmentDate;
	}
	
    /**
     * @return assessmentDate
     */
	public String getAssessmentDate() {
		return this.assessmentDate;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param zyOtherDesc
	 */
	public void setZyOtherDesc(String zyOtherDesc) {
		this.zyOtherDesc = zyOtherDesc;
	}
	
    /**
     * @return zyOtherDesc
     */
	public String getZyOtherDesc() {
		return this.zyOtherDesc;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}