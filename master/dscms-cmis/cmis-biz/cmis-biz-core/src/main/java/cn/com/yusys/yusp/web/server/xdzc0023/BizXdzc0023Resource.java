package cn.com.yusys.yusp.web.server.xdzc0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0023.req.Xdzc0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.resp.Xdzc0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0023.Xdzc0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池白名单
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0023:资产池白名单")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0023Resource.class);
    @Autowired
    private Xdzc0023Service xdzc0023Service;

    /**
     * 交易码：xdzc0023
     * 交易描述：资产池白名单
     *
     * @param xdzc0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池白名单")
    @PostMapping("/xdzc0023")
    protected @ResponseBody
    ResultDto<Xdzc0023DataRespDto> xdzc0023(@Validated @RequestBody Xdzc0023DataReqDto xdzc0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023DataReqDto));
        Xdzc0023DataRespDto xdzc0023DataRespDto = new Xdzc0023DataRespDto();// 响应Dto:资产池白名单
        ResultDto<Xdzc0023DataRespDto> xdzc0023DataResultDto = new ResultDto<>();

        try {
            // 从xdzc0023DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdzc0023DataRespDto = xdzc0023Service.xdzc0023(xdzc0023DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdzc0023DataRespDto对象开始
            //  xdzc0023DataRespDto.setOpFlag(StringUtils.EMPTY);// 返回码

            // TODO 封装xdzc0023DataRespDto对象结束
            // 封装xdzc0023DataResultDto中正确的返回码和返回信息
            xdzc0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, e.getMessage());
            // 封装xdzc0023DataResultDto中异常返回码和返回信息
            xdzc0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0023DataRespDto到xdzc0023DataResultDto中
        xdzc0023DataResultDto.setData(xdzc0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023DataResultDto));
        return xdzc0023DataResultDto;
    }
}
