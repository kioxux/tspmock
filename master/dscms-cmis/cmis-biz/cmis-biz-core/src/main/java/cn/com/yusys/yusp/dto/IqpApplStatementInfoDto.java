package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplStatementInfo
 * @类描述: iqp_appl_statement_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:18:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApplStatementInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 对账单通知方式 STD_ZB__NOTI_METHOD **/
	private String statementNotiMethod;
	
	/** 电话号码/手机号 **/
	private String telNo;
	
	/** 省/直辖市 **/
	private String addrProvience;
	
	/** 地级市/区 **/
	private String addrCity;
	
	/** 市级行政区/县/县级市 **/
	private String addrArear;
	
	/** 通讯地址补充 **/
	private String addrDetail;
	
	/** 邮编 **/
	private String commZip;
	
	/** EMAIL对账单标志最近最后修改日期 **/
	private String dzdModifiedDate;
	
	/** EMAIL地址 **/
	private String email;
	
	/** EMAIL对账单发送标志 **/
	private String emailDzdFlag;
	
	/** EMAIL还款计划发送标志 **/
	private String emailRetuPlanFlag;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param statementNotiMethod
	 */
	public void setStatementNotiMethod(String statementNotiMethod) {
		this.statementNotiMethod = statementNotiMethod == null ? null : statementNotiMethod.trim();
	}
	
    /**
     * @return StatementNotiMethod
     */	
	public String getStatementNotiMethod() {
		return this.statementNotiMethod;
	}
	
	/**
	 * @param telNo
	 */
	public void setTelNo(String telNo) {
		this.telNo = telNo == null ? null : telNo.trim();
	}
	
    /**
     * @return TelNo
     */	
	public String getTelNo() {
		return this.telNo;
	}
	
	/**
	 * @param addrProvience
	 */
	public void setAddrProvience(String addrProvience) {
		this.addrProvience = addrProvience == null ? null : addrProvience.trim();
	}
	
    /**
     * @return AddrProvience
     */	
	public String getAddrProvience() {
		return this.addrProvience;
	}
	
	/**
	 * @param addrCity
	 */
	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity == null ? null : addrCity.trim();
	}
	
    /**
     * @return AddrCity
     */	
	public String getAddrCity() {
		return this.addrCity;
	}
	
	/**
	 * @param addrArear
	 */
	public void setAddrArear(String addrArear) {
		this.addrArear = addrArear == null ? null : addrArear.trim();
	}
	
    /**
     * @return AddrArear
     */	
	public String getAddrArear() {
		return this.addrArear;
	}
	
	/**
	 * @param addrDetail
	 */
	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail == null ? null : addrDetail.trim();
	}
	
    /**
     * @return AddrDetail
     */	
	public String getAddrDetail() {
		return this.addrDetail;
	}
	
	/**
	 * @param commZip
	 */
	public void setCommZip(String commZip) {
		this.commZip = commZip == null ? null : commZip.trim();
	}
	
    /**
     * @return CommZip
     */	
	public String getCommZip() {
		return this.commZip;
	}
	
	/**
	 * @param dzdModifiedDate
	 */
	public void setDzdModifiedDate(String dzdModifiedDate) {
		this.dzdModifiedDate = dzdModifiedDate == null ? null : dzdModifiedDate.trim();
	}
	
    /**
     * @return DzdModifiedDate
     */	
	public String getDzdModifiedDate() {
		return this.dzdModifiedDate;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}
	
    /**
     * @return Email
     */	
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param emailDzdFlag
	 */
	public void setEmailDzdFlag(String emailDzdFlag) {
		this.emailDzdFlag = emailDzdFlag == null ? null : emailDzdFlag.trim();
	}
	
    /**
     * @return EmailDzdFlag
     */	
	public String getEmailDzdFlag() {
		return this.emailDzdFlag;
	}
	
	/**
	 * @param emailRetuPlanFlag
	 */
	public void setEmailRetuPlanFlag(String emailRetuPlanFlag) {
		this.emailRetuPlanFlag = emailRetuPlanFlag == null ? null : emailRetuPlanFlag.trim();
	}
	
    /**
     * @return EmailRetuPlanFlag
     */	
	public String getEmailRetuPlanFlag() {
		return this.emailRetuPlanFlag;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}