package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/2 14:36
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class R007Dto {
    private String name; // 姓名
    private String fxzb; // 风险指标
    private String item; // 指标中文描述
    private Integer result; // 指标结果

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFxzb() {
        return fxzb;
    }

    public void setFxzb(String fxzb) {
        this.fxzb = fxzb;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}
