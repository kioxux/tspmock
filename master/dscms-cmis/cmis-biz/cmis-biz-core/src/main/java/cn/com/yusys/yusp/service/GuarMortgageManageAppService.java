package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.GuarMortgageManageAppDto;
import cn.com.yusys.yusp.dto.GuarMortgageManageRelDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.transaction.annotation.Propagation;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarMortgageManageAppService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(GuarMortgageManageAppService.class);

    @Autowired
    private GuarMortgageManageAppMapper guarMortgageManageAppMapper;

    @Autowired
    private GuarMortgageManageRelService guarMortgageManageRelService;

    @Autowired
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private GuarContRelWarrantMapper guarContRelWarrantMapper;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarMortgageManageApp selectByPrimaryKey(String serno) {
        return guarMortgageManageAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<GuarMortgageManageApp> selectAll(QueryModel model) {
        List<GuarMortgageManageApp> records = (List<GuarMortgageManageApp>) guarMortgageManageAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarMortgageManageApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarMortgageManageApp> list = guarMortgageManageAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(GuarMortgageManageApp record) {
        return guarMortgageManageAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(GuarMortgageManageApp record) {
        return guarMortgageManageAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(GuarMortgageManageApp record) {
        return guarMortgageManageAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(GuarMortgageManageApp record) {
        return guarMortgageManageAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return guarMortgageManageAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return guarMortgageManageAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:deleteOnlogic
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    public int deleteOnlogic(String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        //退回状态的删除,更改状态为自行退出
        if ("992".equals(guarMortgageManageApp.getApproveStatus())) {
            guarMortgageManageApp.setApproveStatus("996");
        } else {
            guarMortgageManageApp.setOprType("02");
        }
        return guarMortgageManageAppMapper.updateByPrimaryKeySelective(guarMortgageManageApp);
    }

    /**
     * @方法名称: isExistByGuarContNo
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<Boolean> isExistByGuarContNo(QueryModel queryModel) {
        ResultDto<Boolean> resultDto = new ResultDto<>();
        String rtnCode = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.value;
        try {
            log.info("修改数据信息开始！获取请求入参数据");
            int counts = guarMortgageManageAppMapper.isExistByGuarContNo(queryModel);
            if (counts > 0) {
                resultDto.data(true);
            } else {
                resultDto.data(false);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            rtnCode = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            resultDto.setCode(rtnCode);
            resultDto.setMessage(rtnMsg);
        }
        return resultDto;
    }

    /**
     * @param serno
     * @return
     * @函数名称:queryGuarMortgageManageAppDtoBySerno
     * @函数描述:根据流水号查询抵押登记申请对象
     */
    public GuarMortgageManageAppDto queryGuarMortgageManageAppDtoBySerno(String serno) {
        GuarMortgageManageAppDto guarMortgageManageAppDto = new GuarMortgageManageAppDto();
        //根据流水号查询guarMortgageManageApp数据
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        //复制数据从guarMortgageManageApp 复制到guarMortgageManageAppDto中
        BeanUtils.copyProperties(guarMortgageManageApp, guarMortgageManageAppDto);
        //根据流水号查询guarMortgageManageRels数据
        List<GuarMortgageManageRelDto> guarMortgageManageRelDtos = guarMortgageManageRelService.queryListBySerno(serno);
        //抵押办理类型
        String regType = guarMortgageManageApp.getRegType();
        //担保合同编号
        String guarContNo = guarMortgageManageApp.getGuarContNo();

        guarMortgageManageAppDto.setGuarMortgageManageRelDtos(guarMortgageManageRelDtos);
        if (guarMortgageManageApp != null) {
            // 登记人
            String inputIdName = OcaTranslatorUtils.getUserName(guarMortgageManageApp.getInputId());
            // 登记机构
            String inputBrIdName = OcaTranslatorUtils.getOrgName(guarMortgageManageApp.getInputBrId());
            //责任人
            String userName = OcaTranslatorUtils.getUserName(guarMortgageManageApp.getManagerId());
            //责任机构
            String orgName = OcaTranslatorUtils.getOrgName(guarMortgageManageApp.getManagerBrId());
            guarMortgageManageAppDto.setInputIdName(inputIdName);
            guarMortgageManageAppDto.setInputBrIdName(inputBrIdName);
            guarMortgageManageAppDto.setManagerIdName(userName);
            guarMortgageManageAppDto.setManagerBrIdName(orgName);
        }
        return guarMortgageManageAppDto;
    }

    /**
     * @param guarMortgageManageAppDto
     * @return
     * @函数名称:tempSave
     * @函数描述:根据抵押登记申请对象暂存(新增/修改)数据
     */
    public int tempSave(GuarMortgageManageAppDto guarMortgageManageAppDto) {
        int result = -1;
        //抵押办理与押品基本信息关联关系列表
        List<GuarMortgageManageRel> guarMortgageManageRelList = guarMortgageManageAppDto.getGuarMortgageManageRelList();
        //获取流水号
        String serno = guarMortgageManageAppDto.getSerno();
        //根据流水号查询是否有数据,没有数据新增,有的话修改
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        //新增
        if (guarMortgageManageApp == null) {
            guarMortgageManageApp = new GuarMortgageManageApp();
            BeanUtils.copyProperties(guarMortgageManageAppDto, guarMortgageManageApp);
            result = guarMortgageManageAppMapper.insert(guarMortgageManageApp);
        } else {//修改
            BeanUtils.copyProperties(guarMortgageManageAppDto, guarMortgageManageApp);
            result = guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
            result = guarMortgageManageRelService.deleteBySerno(serno);
        }
        //遍历插入抵押办理与押品基本信息关联关系表
        for (GuarMortgageManageRel guarMortgageManageRel : guarMortgageManageRelList) {
            guarMortgageManageRel.setSerno(serno);
            result = guarMortgageManageRelService.insert(guarMortgageManageRel);
        }

        //担保合同编号
        String guarContNo = guarMortgageManageApp.getGuarContNo();
        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarContNo);
        //业务条线
        String bizLine = grtGuarCont.getBizLine();

        if (StringUtils.isEmpty(bizLine)){
            //如果担保合同的业务条线为空，则从关联的借款合同里取
            String belgLine = ctrLoanContService.selectBelgLineByGuarContNo(guarContNo);

            if (StringUtils.isEmpty(belgLine)){
                throw BizException.error(null, "","担保合同【"+guarContNo+"】的业务条线为空！");
            }

            //更新担保合同的业务条线
            grtGuarCont.setBizLine(belgLine);
            grtGuarContMapper.updateByPrimaryKey(grtGuarCont);
        }
        return result;
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 抵押登记注销流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);

        if (guarMortgageManageApp!=null && StringUtils.isNotEmpty(guarMortgageManageApp.getSerno())){
            guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
            guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
        }
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 抵押登记注销流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 根据抵押办理类型做不同处理, 待开发//TODO
     * 调用这个方法的流程:
     * DBGL02 抵押登记审批流程_本地机构集中办理模式
     * DBGL03 抵押登记审批流程_本地机构放款后抵押模式
     * DBGL09 抵押注销审批流程_本地机构
     * 1.将审批状态更新为997 通过
     * 2.
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
        String managerId = guarMortgageManageApp.getManagerId();
        String managerBrId = guarMortgageManageApp.getManagerBrId();
        //抵押办理类型 01抵押登记 02抵押注销
        String regType = guarMortgageManageApp.getRegType();
        if ("01".equals(regType)) {
//            createWarrantInRecords(guarMortgageManageApp,managerId,managerBrId);
        } else if ("02".equals(regType)) {
            //通过业务流水号获取押品编号
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", guarMortgageManageApp.getSerno());
            List<GuarMortgageManageRel> guarMortgageManageRels = guarMortgageManageRelService.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(guarMortgageManageRels)) {
                for (GuarMortgageManageRel guarMortgageManageRel : guarMortgageManageRels) {
                    //更改押品押品登记办理状态  01已登记 02未登记 03已注销
                    GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.getGuarBaseInfoByGuarNo(guarMortgageManageRel.getGuarNo());
                    guarBaseInfo.setRegState("03");
                    guarBaseInfoMapper.updateByPrimaryKey(guarBaseInfo);
                }
            }
        }
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 抵押登记注销流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 抵押登记注销流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 打回
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        GuarMortgageManageApp guarMortgageManageApp = guarMortgageManageAppMapper.selectByPrimaryKey(serno);
        guarMortgageManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        guarMortgageManageAppMapper.updateByPrimaryKey(guarMortgageManageApp);
    }

    /**
     * 查询押品在途的抵押登记流水号
     *
     * @param guarNo
     * @return
     */
    public String selectOnTheWaySernosByGuarNo(String guarNo) {
        return guarMortgageManageAppMapper.selectOnTheWaySernosByGuarNo(guarNo);
    }

    /**
     * 查询押品对应的审批通过的抵押登记记录数
     *
     * @param guarNo
     * @return
     */
    public int selectApprovedRecordsByGuarNo(String guarNo) {
        return guarMortgageManageAppMapper.selectApprovedRecordsByGuarNo(guarNo);
    }

    /**
     * 根据担保合同编号查询抵押登记的主合同编号
     * @param guarContNo
     * @return
     */
    public String selectMainContNoByGuarContNo(String guarContNo){
        return guarMortgageManageAppMapper.selectMainContNoByGuarContNo(guarContNo);
    }

    /**
     * 生成权证入库记录
     * @param guarMortgageManageApp
     * @param managerId
     * @param managerBrId
     */
    public void createWarrantInRecords(GuarMortgageManageApp guarMortgageManageApp,String managerId,String managerBrId){
        //通过业务流水号获取押品编号
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", guarMortgageManageApp.getSerno());

        List<GuarMortgageManageRel> guarMortgageManageRels = guarMortgageManageRelService.selectByModel(queryModel);

        if (CollectionUtils.nonEmpty(guarMortgageManageRels)) {

            String grtFlag = "";
            //权证入库流水号
            String warrantInSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
            //核心担保编号
            String coreGuarantyNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>());
            //担保编号
            String guarContNo = guarMortgageManageApp.getGuarContNo();

            for (GuarMortgageManageRel guarMortgageManageRel : guarMortgageManageRels) {
                //更改押品登记办理状态  01已登记 02未登记 03已注销
                String guarNo = guarMortgageManageRel.getGuarNo();
                GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.getGuarBaseInfoByGuarNo(guarNo);
                grtFlag = guarBaseInfo.getGrtFlag();
                guarBaseInfo.setRegState("01");
                guarBaseInfoMapper.updateByPrimaryKey(guarBaseInfo);

                GuarContRelWarrant guarContRelWarrant = new GuarContRelWarrant();
                guarContRelWarrant.setSerno(warrantInSerno);
                guarContRelWarrant.setGuarContNo(guarContNo);
                guarContRelWarrant.setCoreGuarantyNo(coreGuarantyNo);
                guarContRelWarrant.setGuarNo(guarNo);
                guarContRelWarrant.setPldimnMemo(guarBaseInfo.getPldimnMemo());
                guarContRelWarrant.setGuarType(guarBaseInfo.getGuarType());
                guarContRelWarrant.setGuarCusId(guarBaseInfo.getGuarCusId());
                guarContRelWarrant.setGuarCusName(guarBaseInfo.getGuarCusName());
                guarContRelWarrant.setOprType(CmisBizConstants.OPR_TYPE_01);

                guarContRelWarrantMapper.insert(guarContRelWarrant);
            }

            //生成权证入库任务
            GuarWarrantManageApp record = new GuarWarrantManageApp();
            BeanUtils.copyProperties(guarMortgageManageRels.get(0), record);

            record.setGuarContNo(guarContNo);
            record.setWarrantInType("02");//权证入库模式  01-电子权证自动入库模式 02-纸质权证集中入库模式 03-纸质权证柜面入库模式 04-电子权证手工入库模式
            record.setWarrantAppType("01");//权证出入库申请类型 01-入库申请  02-出库申请 03-权证续借
            record.setIsZhblzx("1");//是否出库到集中作业 1 是
            record.setOprType("01");
            record.setGrtFlag(grtFlag);
            record.setGuarContNo(guarContNo);
            record.setCoreGuarantyNo(coreGuarantyNo);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setSerno(warrantInSerno);
            record.setManagerId(managerId);
            record.setManagerBrId(managerBrId);
            record.setInputId(managerId);
            record.setInputBrId(managerBrId);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String inputDate = sdf.format(new Date());
            record.setInputDate(inputDate);
            record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            guarWarrantManageAppMapper.insertSelective(record);
        }
    }

    /**
     * 自动提交事务
     * @param record
     * @return
     */
    @Transactional(propagation= Propagation.NEVER)
    public int insertAutoCommit(GuarMortgageManageApp record) {
        return guarMortgageManageAppMapper.insert(record);
    }

}
