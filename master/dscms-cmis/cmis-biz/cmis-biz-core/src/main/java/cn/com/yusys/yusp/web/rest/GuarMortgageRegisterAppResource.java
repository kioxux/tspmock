/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.vo.GuarBasicBuildingVo;
import cn.com.yusys.yusp.vo.GuarMortgageRegisterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarMortgageRegisterApp;
import cn.com.yusys.yusp.service.GuarMortgageRegisterAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageRegisterAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-16 11:33:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarmortgageregisterapp")
public class GuarMortgageRegisterAppResource {
    @Autowired
    private GuarMortgageRegisterAppService guarMortgageRegisterAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarMortgageRegisterApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarMortgageRegisterApp> list = guarMortgageRegisterAppService.selectAll(queryModel);
        return new ResultDto<List<GuarMortgageRegisterApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarMortgageRegisterApp>> index(QueryModel queryModel) {
        List<GuarMortgageRegisterApp> list = guarMortgageRegisterAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarMortgageRegisterApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarMortgageRegisterApp> show(@PathVariable("serno") String serno) {
        GuarMortgageRegisterApp guarMortgageRegisterApp = guarMortgageRegisterAppService.selectByPrimaryKey(serno);
        return new ResultDto<GuarMortgageRegisterApp>(guarMortgageRegisterApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarMortgageRegisterApp> create(@RequestBody GuarMortgageRegisterApp guarMortgageRegisterApp) throws URISyntaxException {
        guarMortgageRegisterAppService.insert(guarMortgageRegisterApp);
        return new ResultDto<GuarMortgageRegisterApp>(guarMortgageRegisterApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarMortgageRegisterApp guarMortgageRegisterApp) throws URISyntaxException {
        int result = guarMortgageRegisterAppService.update(guarMortgageRegisterApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarMortgageRegisterAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarMortgageRegisterAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 新增数据
     * @函数名称:insertGuarMortgageRegisterApp
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertGuarMortgageRegisterApp")
    protected ResultDto<Integer> insertGuarMortgageRegisterApp(@RequestBody GuarMortgageRegisterApp guarMortgageRegisterApp) throws URISyntaxException {
        int result =guarMortgageRegisterAppService.insert(guarMortgageRegisterApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * 更新数据抵押登记申请数据
     * @函数名称:updateGuarMortgageRegisterApp
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateGuarMortgageRegisterApp")
    protected ResultDto<Map> updateGuarMortgageRegisterApp(@RequestBody GuarMortgageRegisterApp guarMortgageRegisterApp) throws URISyntaxException {
        Map result = guarMortgageRegisterAppService.updateSelectiveToMap(guarMortgageRegisterApp);

        return new ResultDto<Map>(result);
    }

    /**
     * 根据担保合同编号查询是否有在途的抵押登记申请
     * @函数名称:updateGuarMortgageRegisterApp
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/isExistByGuarContNo/{guar_cont_no}")
    protected ResultDto isExistByGuarContNo(@PathVariable("guar_cont_no") String guarContNo){
        return guarMortgageRegisterAppService.isExistByGuarContNo(guarContNo);
    }

    /**
     * @函数名称:
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryByApproveStatus")
    protected ResultDto<List<GuarMortgageRegisterApp>> queryByApproveStatus(@RequestBody QueryModel queryModel) {
        List<GuarMortgageRegisterApp> list = guarMortgageRegisterAppService.selectByModel(queryModel);
        return new ResultDto<List<GuarMortgageRegisterApp>>(list);
    }


    /**
     * @函数名称:deleteOnLogic
     * @函数描述:单个对象逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteOnLogic")
    protected ResultDto<Integer> deleteOnLogic(@RequestBody GuarMortgageRegisterApp record) {
        int result = guarMortgageRegisterAppService.updateSelective(record);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveByGuarMortgageRegisterVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarMortgageRegisterVo")
    protected ResultDto saveByGuarBasicBuildingVo(@RequestBody GuarMortgageRegisterVo record) throws URISyntaxException {
        ResultDto result = guarMortgageRegisterAppService.saveByGuarMortgageRegisterVo(record);
        return result;
    }
}
