/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditQryBizReal;
import cn.com.yusys.yusp.service.CreditQryBizRealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditQryBizRealResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditqrybizreal")
public class CreditQryBizRealResource {
    @Autowired
    private CreditQryBizRealService creditQryBizRealService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditQryBizReal>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditQryBizReal> list = creditQryBizRealService.selectAll(queryModel);
        return new ResultDto<List<CreditQryBizReal>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
     @PostMapping("/")
     protected ResultDto<List<CreditQryBizReal>> index(@RequestBody QueryModel queryModel) {
         List<CreditQryBizReal> list = creditQryBizRealService.selectByModel(queryModel);
         return new ResultDto<List<CreditQryBizReal>>(list);
     }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cqbrSerno}")
    protected ResultDto<CreditQryBizReal> show(@PathVariable("cqbrSerno") String cqbrSerno) {
        CreditQryBizReal creditQryBizReal = creditQryBizRealService.selectByPrimaryKey(cqbrSerno);
        return new ResultDto<CreditQryBizReal>(creditQryBizReal);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CreditQryBizReal> create(@RequestBody CreditQryBizReal creditQryBizReal) throws URISyntaxException {
        creditQryBizRealService.insert(creditQryBizReal);
        return new ResultDto<CreditQryBizReal>(creditQryBizReal);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createBySerno")
    protected ResultDto<CreditQryBizReal> createBySerno(@RequestBody CreditQryBizReal creditQryBizReal) throws URISyntaxException {
        creditQryBizRealService.createBySerno(creditQryBizReal);
        return new ResultDto<CreditQryBizReal>(creditQryBizReal);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditQryBizReal creditQryBizReal) throws URISyntaxException {
        int result = creditQryBizRealService.update(creditQryBizReal);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cqbrSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cqbrSerno") String cqbrSerno) {
        int result = creditQryBizRealService.deleteByPrimaryKey(cqbrSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditQryBizRealService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
