package cn.com.yusys.yusp.service.client.bsp.xwh.xwh001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.resp.Xwh001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2XwhClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：小微公众号
 *
 * @author lihh
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Xwh001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xwh001Service.class);
    
    @Autowired
    private Dscms2XwhClientService dscms2XwhClientService;

    /**
     * 业务逻辑处理方法：借据台账信息接收
     *
     * @param xwh001ReqDto
     * @return
     */
    @Transactional
    public Xwh001RespDto xwh001(Xwh001ReqDto xwh001ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value, JSON.toJSONString(xwh001ReqDto));
        ResultDto<Xwh001RespDto> xwh001ResultDto = dscms2XwhClientService.xwh001(xwh001ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value, JSON.toJSONString(xwh001ResultDto));
        String xwh001Code = Optional.ofNullable(xwh001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xwh001Meesage = Optional.ofNullable(xwh001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Xwh001RespDto xwh001RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xwh001ResultDto.getCode())) {
            //  获取相关的值并解析
            xwh001RespDto = xwh001ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xwh001Code, xwh001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value);
        return xwh001RespDto;
    }
}
