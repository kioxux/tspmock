/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardTelvOtherInfo;
import cn.com.yusys.yusp.service.CreditCardTelvOtherInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvOtherInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "电话调查问题其它信息")
@RestController
@RequestMapping("/api/creditcardtelvotherinfo")
public class CreditCardTelvOtherInfoResource {
    @Autowired
    private CreditCardTelvOtherInfoService creditCardTelvOtherInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardTelvOtherInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardTelvOtherInfo> list = creditCardTelvOtherInfoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardTelvOtherInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardTelvOtherInfo>> index(QueryModel queryModel) {
        List<CreditCardTelvOtherInfo> list = creditCardTelvOtherInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardTelvOtherInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CreditCardTelvOtherInfo> show(@PathVariable("serno") String serno) {
        CreditCardTelvOtherInfo creditCardTelvOtherInfo = creditCardTelvOtherInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CreditCardTelvOtherInfo>(creditCardTelvOtherInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("电话调查问题其它信息保存")
    @PostMapping("/")
    protected ResultDto<CreditCardTelvOtherInfo> create(@RequestBody CreditCardTelvOtherInfo creditCardTelvOtherInfo) throws URISyntaxException {
        creditCardTelvOtherInfoService.insert(creditCardTelvOtherInfo);
        return new ResultDto<CreditCardTelvOtherInfo>(creditCardTelvOtherInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardTelvOtherInfo creditCardTelvOtherInfo) throws URISyntaxException {
        int result = creditCardTelvOtherInfoService.update(creditCardTelvOtherInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据主键删除")
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = creditCardTelvOtherInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardTelvOtherInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param creditCardTelvOtherInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/31 10:55
     * @version 1.0.0
     * @desc 根据业务流水号查询电核其他信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号查询电核其他信息")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardTelvOtherInfo> selectBySerno(@RequestBody CreditCardTelvOtherInfo creditCardTelvOtherInfo) {
        CreditCardTelvOtherInfo creditCardTelvOtherInfo2 = creditCardTelvOtherInfoService.selectByPrimaryKey(creditCardTelvOtherInfo.getSerno());
        return new ResultDto<CreditCardTelvOtherInfo>(creditCardTelvOtherInfo2);
    }
    /**
     * @param creditCardTelvOtherInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/31 16:04
     * @version 1.0.0
     * @desc 电话调查问题其它信息保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("电话调查问题其它信息保存")
    @PostMapping("/insertauto")
    protected ResultDto<Integer> insertAuto(@RequestBody CreditCardTelvOtherInfo creditCardTelvOtherInfo) {
        int result = creditCardTelvOtherInfoService.insertAuto(creditCardTelvOtherInfo);
        return new ResultDto<Integer>(result);
    }

}
