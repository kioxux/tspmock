package cn.com.yusys.yusp.web.server.xdht0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0019.req.Xdht0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0019.resp.Xdht0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0019.Xdht0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同生成
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0019:合同生成")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0019Resource.class);
    @Autowired
    private Xdht0019Service xdht0019Service;

    /**
     * 交易码：xdht0019
     * 交易描述：合同生成
     *
     * @param xdht0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同生成")
    @PostMapping("/xdht0019")
    protected @ResponseBody
    ResultDto<Xdht0019DataRespDto> xdht0019(@Validated @RequestBody Xdht0019DataReqDto xdht0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataReqDto));
        Xdht0019DataRespDto xdht0019DataRespDto = new Xdht0019DataRespDto();// 响应Dto:合同生成
        ResultDto<Xdht0019DataRespDto> xdht0019DataResultDto = new ResultDto<>();
        try {
            String certNo = xdht0019DataReqDto.getCertNo();//客户证件号
            if (StringUtils.isBlank(certNo)) {
                xdht0019DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0019DataResultDto.setMessage("客户证件号【certNo】不能为空！");
                return xdht0019DataResultDto;
            }
            // 从xdht0019DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataReqDto));
            xdht0019DataRespDto = xdht0019Service.xdht0019(xdht0019DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataRespDto));

            // 封装xdht0019DataResultDto中正确的返回码和返回信息
            xdht0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, e.getMessage());
            // 封装xdht0019DataResultDto中异常返回码和返回信息
            xdht0019DataResultDto.setCode(e.getErrorCode());
            xdht0019DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, e.getMessage());
            // 封装xdht0019DataResultDto中异常返回码和返回信息
            xdht0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0019DataRespDto到xdht0019DataResultDto中
        xdht0019DataResultDto.setData(xdht0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataRespDto));
        return xdht0019DataResultDto;
    }
}
