/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitAssetMemo
 * @类描述: lmt_debit_asset_memo数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 10:57:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_debit_asset_memo")
public class LmtDebitAssetMemo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 资产性质 **/
	@Column(name = "ASSET_CHA", unique = false, nullable = true, length = 500)
	private String assetCha;
	
	/** 资产说明 **/
	@Column(name = "ASSET_EXPL", unique = false, nullable = true, length = 200)
	private String assetExpl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param assetCha
	 */
	public void setAssetCha(String assetCha) {
		this.assetCha = assetCha;
	}
	
    /**
     * @return assetCha
     */
	public String getAssetCha() {
		return this.assetCha;
	}
	
	/**
	 * @param assetExpl
	 */
	public void setAssetExpl(String assetExpl) {
		this.assetExpl = assetExpl;
	}
	
    /**
     * @return assetExpl
     */
	public String getAssetExpl() {
		return this.assetExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}