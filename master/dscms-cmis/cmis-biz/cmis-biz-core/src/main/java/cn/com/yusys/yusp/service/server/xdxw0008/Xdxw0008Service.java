package cn.com.yusys.yusp.service.server.xdxw0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.req.CmisCus0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.req.CmisCus0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.resp.CmisCus0019RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0022.req.CmisCus0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0022.resp.CmisCus0022RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0008.req.Xdxw0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0008.resp.Xdxw0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 业务逻辑类:优农贷黑白名单校验
 * 判断该申请人是不是优农贷白名单，
 * 如果是白名单，则把信息插入小贷客户调查表等一系列关联表，自动进行贷前调查信息的插入；
 * 如果不是白名单，则把相关信息插入优农贷小贷信息表，根据经营地址定位到所在地区的区域主管，并发短信通知该区域主管，进行客户的业务分配（指派给
 * 某个客户经理），由客户经理线下去进行人工贷前调查。
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdxw0008Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0008Service.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;//调查报告基本信息
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;//客户授信调查主表
    @Autowired
    private LmtSurveyProfileInfoMapper lmtSurveyProfileInfoMapper;//侧面信息调查表
    @Autowired
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;//调查结论表
    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;//调查报告企业信息
    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;//调查任务分配表
    @Autowired
    private LmtEgcInfoMapper lmtEgcInfoMapper;//紧急联系人信息
    @Autowired
    private DscmsCusClientService dscmsCusClientService;//客户模块信息
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号
    @Autowired
    private AreaUserMapper areaUserMapper;//小微区域管理
    @Autowired
    private AdminSmUserService adminSmUserService;//用户信息模块
    @Autowired
    private CmisCusClientService cmisCusClientService;//客户信息模块
    @Autowired
    private CommonService commonService;
    @Autowired
    private SenddxService senddxService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    /**
     * 交易码：Xdxw0008
     * 交易描述：优农贷黑白名单校验
     *
     * @param xdxw0008DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0008DataRespDto xdxw0008(Xdxw0008DataReqDto xdxw0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataReqDto));
        //定义返回信息
        Xdxw0008DataRespDto xdxw0008DataRespDto = new Xdxw0008DataRespDto();
        boolean flag = true;//判断标识
        String checkResult = "";//返回校验结果

        try {
            //请求字段
            String rqstrName = xdxw0008DataReqDto.getRqstrName();//申请人姓名
            String rqstrCertNo = xdxw0008DataReqDto.getRqstrCertNo();//申请人证件号
            String rqstrMobile = xdxw0008DataReqDto.getRqstrMobile();//申请人手机号
            String actOperAddr = xdxw0008DataReqDto.getActOperAddr();//经营地址
            BigDecimal applyAmt = xdxw0008DataReqDto.getApplyAmt();//申请金额
            BigDecimal applyTerm = xdxw0008DataReqDto.getApplyTerm();//申请期限
            String saleManagerId = xdxw0008DataReqDto.getSaleManagerId();//营销客户经理编号

            // 1、必输项校验
            if (StringUtils.isBlank(rqstrName)) { //申请人姓名
                checkResult = "客户姓名不能为空!";
                xdxw0008DataRespDto.setCheckResult(checkResult);
                return xdxw0008DataRespDto;
            }
            if (StringUtils.isBlank(rqstrCertNo)) { //申请人证件号
                checkResult = "客户证件号不能为空！";
                xdxw0008DataRespDto.setCheckResult(checkResult);
                return xdxw0008DataRespDto;
            }
            if (StringUtils.isBlank(rqstrMobile)) { //申请人手机号
                checkResult = "手机号不能为空！";
                xdxw0008DataRespDto.setCheckResult(checkResult);
                return xdxw0008DataRespDto;
            }
            if (StringUtils.isBlank(actOperAddr)) { //经营地址
                checkResult = "经营地址不能为空！";
                xdxw0008DataRespDto.setCheckResult(checkResult);
                return xdxw0008DataRespDto;
            }


            /******第一步：如果配偶也在优农贷白名单中，则拒绝并提示：配偶双方不能同时申请！根据优农贷白名单的配偶证件号码校验。*******/
            //优农贷名单申请基本信息 CUS_LST_YND_APP
            // select count(*) as  peiou1 from cus_lst_ynd_app where spouse_idcard_no='"+certNo+"'"
            int peiou1 = 0;
            CmisCus0019ReqDto reqDto = new CmisCus0019ReqDto();
            reqDto.setSpouseIdcardNo(rqstrCertNo);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0019.key, DscmsEnum.TRADE_CODE_CMISCUS0019.value, JSON.toJSONString(reqDto));
            ResultDto<CmisCus0019RespDto> cmisCus0019RespDtoResultDto = cmisCusClientService.cmiscus0019(reqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0019.key, DscmsEnum.TRADE_CODE_CMISCUS0019.value, JSON.toJSONString(cmisCus0019RespDtoResultDto));
            if (cmisCus0019RespDtoResultDto != null) {
                CmisCus0019RespDto cmisCus0019RespDto = cmisCus0019RespDtoResultDto.getData();
                String code = cmisCus0019RespDtoResultDto.getCode();
                if (StringUtils.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    if (cmisCus0019RespDto != null) {
                        peiou1 = cmisCus0019RespDto.getResult();
                    }
                }
            }


            //查询优农贷授信调查表 LMT_SURVEY_REPORT_BASIC_INFO
            int peiou2 = lmtSurveyReportBasicInfoMapper.selcetYndRecordBycoerCode(rqstrCertNo);
            if (peiou1 > 0 || peiou2 > 0) {
                checkResult = DscmsBizXwEnum.CHECK_RESULT_00.value;
                xdxw0008DataRespDto.setCheckResult(checkResult);
                return xdxw0008DataRespDto;
            }

            /******第二步：查询客户是否在优农贷名单信息中*******/

            //查询cus_lst_ynd的domain对象
            CmisCus0011ReqDto cmisCus0011ReqDto = new CmisCus0011ReqDto();
            cmisCus0011ReqDto.setCertCode(rqstrCertNo);//用证件号作为条件查询
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0011.key, DscmsEnum.TRADE_CODE_CMISCUS0011.value, JSON.toJSONString(cmisCus0011ReqDto));
            ResultDto<CmisCus0011RespDto> respDto = cmisCusClientService.cmiscus0011(cmisCus0011ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0011.key, DscmsEnum.TRADE_CODE_CMISCUS0011.value, JSON.toJSONString(cmisCus0019RespDtoResultDto));
            //获取优农贷名单申请基本信息 cus_lst_ynd_app对象：AAA
            CmisCus0011RespDto cmisCus0011Resp = Optional.ofNullable(respDto.getData()).orElse(new CmisCus0011RespDto());
            //
            String listStatus = cmisCus0011Resp.getListStatus();//名单状态
            String surveySerno = cmisCus0011Resp.getSerno();//获取流水号
            //根据证件号获取客户基本信息
            String spouseIdcardNo = cmisCus0011Resp.getSpouseIdcardNo();
            String cusId = StringUtils.EMPTY;//客户编号
            String cusName = StringUtils.EMPTY;//客户名称
            String certType = StringUtils.EMPTY;//证件类型
            String certCode = StringUtils.EMPTY;//证件号码
            String managerId = StringUtils.EMPTY;//客户经理
            String managerBrId = StringUtils.EMPTY;//机构
            String edu = StringUtils.EMPTY;//学历
            String marStatus = StringUtils.EMPTY;//婚姻状态
            String isHaveChildren = StringUtils.EMPTY;//有无子女
            String mobileNo = StringUtils.EMPTY;//电话
            String spouseCusId = StringUtils.EMPTY;//配偶客户号
            String spouseName = StringUtils.EMPTY;//配偶姓名
            String spouseMobileNo = StringUtils.EMPTY;//配偶电话
            String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
            logger.info("********XDXW0008调用commonService查询客户信息开始,查询参数为:{}", JSON.toJSONString(spouseIdcardNo));
            List<CusBaseDto> resultList = new ArrayList<>();
            if (StringUtil.isNotEmpty(rqstrCertNo)) {//
                resultList = commonService.getCusBaseListByCertCode(rqstrCertNo);
            }
            logger.info("********XDXW0008调用commonService查询客户信息结束,返回结果为:{}", JSON.toJSONString(resultList));
            if (resultList.size() > 0) {
                CusBaseDto cusBaseDto = resultList.get(0);
                cusId = cusBaseDto.getCusId();
                cusName = cusBaseDto.getCusName();
                certType = cusBaseDto.getCertType();
                certCode = cusBaseDto.getCertCode();
            }else{
                cusName = cmisCus0011Resp.getCusName();
                certCode = cmisCus0011Resp.getCertCode();
            }
            edu = cmisCus0011Resp.getEdu();
            marStatus = cmisCus0011Resp.getMarStatus();
            isHaveChildren = cmisCus0011Resp.getIsHaveChildren();
            mobileNo = cmisCus0011Resp.getMobileNo();
            spouseName = cmisCus0011Resp.getSpouseName();
            spouseIdcardNo = cmisCus0011Resp.getSpouseIdcardNo();
            spouseMobileNo = cmisCus0011Resp.getSpouseMobileNo();
            managerBrId = cmisCus0011Resp.getHandOrg();
            managerId = cmisCus0011Resp.getHuser();
            if(StringUtil.isEmpty(cusName)){
                cusName = rqstrName;
            }
            if(StringUtil.isEmpty(certCode)){
                certCode = rqstrCertNo;
            }
            if(StringUtil.isEmpty(mobileNo)){
                mobileNo = rqstrMobile;
            }
            //checkResult = listStatus;
            if ("03".equals(listStatus)) { //白名单（存在申请记录）
                checkResult = DscmsBizXwEnum.CHECK_RESULT_01.key;
                //如果是白名单，则把信息插入小贷客户调查表
                //判断是否有调查信息
                QueryModel model = new QueryModel();
                model.addCondition("certCode",certCode);
                model.addCondition("prdId","SC020010");
                model.setSort("CREATE_TIME DESC");
                List<LmtSurveyReportMainInfo> existLmtSurveyReportMainInfoList = lmtSurveyReportMainInfoMapper.selectByModel(model);
                if(CollectionUtils.isEmpty(existLmtSurveyReportMainInfoList)){//根据身份证判断是否做过优农贷调查表，如果没有，生成调查表
                    //生成主表信息
                    LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
                    //生成新的流水号
                    String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                    lmtSurveyReportMainInfo.setSurveySerno(serno);
                    lmtSurveyReportMainInfo.setListSerno(surveySerno);//名单流水号
                    lmtSurveyReportMainInfo.setPrdId("SC020010");
                    lmtSurveyReportMainInfo.setPrdName("优农贷");
                    lmtSurveyReportMainInfo.setSurveyType("8");
                    lmtSurveyReportMainInfo.setCusId(cusId);
                    lmtSurveyReportMainInfo.setCusName(cusName);
                    lmtSurveyReportMainInfo.setCertType(certType);
                    lmtSurveyReportMainInfo.setCertCode(certCode);
                    lmtSurveyReportMainInfo.setAppAmt(applyAmt);
                    lmtSurveyReportMainInfo.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_997);
                    lmtSurveyReportMainInfo.setDataSource(CmisBizConstants.STD_DATA_SOUR_01);
                    lmtSurveyReportMainInfo.setPrdType("08");
                    lmtSurveyReportMainInfo.setIsStopOffline("0");
                    lmtSurveyReportMainInfo.setIntoTime(openday);
                    lmtSurveyReportMainInfo.setManagerId(managerId);
                    lmtSurveyReportMainInfo.setManagerBrId(managerBrId);
                    lmtSurveyReportMainInfo.setUpdId(managerId);
                    lmtSurveyReportMainInfo.setUpdBrId(managerBrId);
                    lmtSurveyReportMainInfo.setUpdDate(openday);
                    Date date = new Date();
                    lmtSurveyReportMainInfo.setCreateTime(date);
                    lmtSurveyReportMainInfo.setUpdateTime(date);
                    lmtSurveyReportMainInfoMapper.insertSelective(lmtSurveyReportMainInfo);

                    //生成基本信息
                    LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
                    lmtSurveyReportBasicInfo.setSurveySerno(serno);
                    lmtSurveyReportBasicInfo.setCusId(cusId);
                    lmtSurveyReportBasicInfo.setCusName(cusName);
                    lmtSurveyReportBasicInfo.setCertCode(certCode);
                    lmtSurveyReportBasicInfo.setEdu(edu);
                    lmtSurveyReportBasicInfo.setMarStatus(marStatus);
                    lmtSurveyReportBasicInfo.setIsHaveChildren(isHaveChildren);
                    lmtSurveyReportBasicInfo.setPhone(mobileNo);
                    lmtSurveyReportBasicInfo.setSpouseCusId(spouseCusId);
                    lmtSurveyReportBasicInfo.setSpouseName(spouseName);
                    lmtSurveyReportBasicInfo.setSpousePhone(spouseMobileNo);
                    lmtSurveyReportBasicInfo.setSpouseCertCode(spouseIdcardNo);
                    lmtSurveyReportBasicInfo.setManagerId(managerId);
                    lmtSurveyReportBasicInfo.setManagerBrId(managerBrId);
                    lmtSurveyReportBasicInfo.setUpdId(managerId);
                    lmtSurveyReportBasicInfo.setUpdBrId(managerBrId);
                    lmtSurveyReportBasicInfo.setUpdDate(openday);
                    lmtSurveyReportBasicInfo.setCreateTime(date);
                    lmtSurveyReportBasicInfo.setUpdateTime(date);
                    lmtSurveyReportBasicInfoMapper.insertSelective(lmtSurveyReportBasicInfo);

                    //查询白名单的经营信息
                    CmisCus0022ReqDto cmisCus0022ReqDto = new CmisCus0022ReqDto();
                    cmisCus0022ReqDto.setSerno(surveySerno);//用名单流水号作为条件查询
                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022ReqDto));
                    ResultDto<CmisCus0022RespDto> cmisCus0022RespDto = cmisCusClientService.cmiscus0022(cmisCus0022ReqDto);
                    logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022RespDto));
                    CmisCus0022RespDto cmisCus0022Resp = Optional.ofNullable(cmisCus0022RespDto.getData()).orElse(new CmisCus0022RespDto());
                    if(cmisCus0022Resp != null){
                        LmtSurveyReportComInfo lmtSurveyReportComInfo = new LmtSurveyReportComInfo();
                        lmtSurveyReportComInfo.setSurveySerno(serno);
                        String oneLevelTradeCode = cmisCus0022Resp.getOneLevelTradeCode();
                        String detailsTradeCode = cmisCus0022Resp.getDetailsTradeCode();
                        //String operScale = cmisCus0022Resp.getOperScale();
                        //String operScaleUnit = cmisCus0022Resp.getOperScaleUnit();
                        //BigDecimal yearSaleIncome = cmisCus0022Resp.getYearSaleIncome();
                        //BigDecimal yearProfit = cmisCus0022Resp.getYearProfit();
                        lmtSurveyReportComInfo.setTrade(oneLevelTradeCode + detailsTradeCode);
                        lmtSurveyReportComInfo.setOperAddr(actOperAddr);
                        lmtSurveyReportComInfo.setManagerId(managerId);
                        lmtSurveyReportComInfo.setManagerBrId(managerBrId);
                        lmtSurveyReportComInfo.setUpdId(managerId);
                        lmtSurveyReportComInfo.setUpdBrId(managerBrId);
                        lmtSurveyReportComInfo.setUpdDate(openday);
                        lmtSurveyReportComInfo.setCreateTime(date);
                        lmtSurveyReportComInfo.setUpdateTime(date);
                        lmtSurveyReportComInfoMapper.insertSelective(lmtSurveyReportComInfo);
                    }
                    //生成侧面调查
                    LmtSurveyProfileInfo lmtSurveyProfileInfo = new LmtSurveyProfileInfo();
                    lmtSurveyProfileInfo.setPkId(UUID.randomUUID().toString());
                    lmtSurveyProfileInfo.setSurveySerno(serno);
                    lmtSurveyProfileInfo.setCreateTime(date);
                    lmtSurveyProfileInfo.setUpdateTime(date);
                    lmtSurveyProfileInfoMapper.insertSelective(lmtSurveyProfileInfo);
                    //生成紧急联系人
                    LmtEgcInfo lmtEgcInfo = new LmtEgcInfo();
                    lmtEgcInfo.setPkId(UUID.randomUUID().toString());
                    lmtEgcInfo.setSurveySerno(serno);
                    lmtEgcInfo.setCreateTime(date);
                    lmtEgcInfo.setUpdateTime(date);
                    lmtEgcInfoMapper.insertSelective(lmtEgcInfo);
                    //调查结论
                    LmtSurveyConInfo lmtSurveyConInfo = new LmtSurveyConInfo();
                    lmtSurveyConInfo.setSurveySerno(serno);
                    lmtSurveyConInfo.setAdviceAmt(applyAmt);
                    lmtSurveyConInfo.setLoanTerm(String.valueOf(applyTerm.intValue()));
                    lmtSurveyConInfo.setManagerId(managerId);
                    lmtSurveyConInfo.setManagerBrId(managerBrId);
                    lmtSurveyConInfo.setUpdId(managerId);
                    lmtSurveyConInfo.setUpdBrId(managerBrId);
                    lmtSurveyConInfo.setUpdDate(openday);
                    lmtSurveyConInfo.setCreateTime(date);
                    lmtSurveyConInfo.setUpdateTime(date);
                    lmtSurveyConInfoMapper.insertSelective(lmtSurveyConInfo);
                }else{//如果存在，更新调查表
                    LmtSurveyReportMainInfo lmtSurveyReportMainInfo = existLmtSurveyReportMainInfoList.get(0);
                    String exitsSurveySerno = lmtSurveyReportMainInfo.getSurveySerno();
                    lmtSurveyReportMainInfo.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_997);
                    lmtSurveyReportMainInfo.setIsStopOffline("0");
                    lmtSurveyReportMainInfo.setUpdId(managerId);
                    lmtSurveyReportMainInfo.setUpdBrId(managerBrId);
                    lmtSurveyReportMainInfo.setUpdDate(openday);
                    Date date = new Date();
                    lmtSurveyReportMainInfo.setUpdateTime(date);
                    lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportMainInfo);

                    LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(exitsSurveySerno);
                    if(lmtSurveyReportBasicInfo == null){
                        lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
                        lmtSurveyReportBasicInfo.setSurveySerno(exitsSurveySerno);
                        lmtSurveyReportBasicInfo.setManagerId(managerId);
                        lmtSurveyReportBasicInfo.setManagerBrId(managerBrId);
                        lmtSurveyReportMainInfo.setCreateTime(date);
                        lmtSurveyReportBasicInfoMapper.insertSelective(lmtSurveyReportBasicInfo);
                    }
                    lmtSurveyReportBasicInfo.setCusId(cusId);
                    lmtSurveyReportBasicInfo.setCusName(cusName);
                    lmtSurveyReportBasicInfo.setCertCode(certCode);
                    lmtSurveyReportBasicInfo.setEdu(edu);
                    lmtSurveyReportBasicInfo.setMarStatus(marStatus);
                    lmtSurveyReportBasicInfo.setSpouseCusId(spouseCusId);
                    lmtSurveyReportBasicInfo.setSpouseName(spouseName);
                    lmtSurveyReportBasicInfo.setSpousePhone(spouseMobileNo);
                    lmtSurveyReportBasicInfo.setSpouseCertCode(spouseIdcardNo);
                    lmtSurveyReportBasicInfo.setUpdId(managerId);
                    lmtSurveyReportBasicInfo.setUpdBrId(managerBrId);
                    lmtSurveyReportMainInfo.setUpdateTime(date);
                    lmtSurveyReportBasicInfo.setUpdDate(openday);
                    lmtSurveyReportBasicInfoMapper.updateByPrimaryKey(lmtSurveyReportBasicInfo);
                    //查询白名单的经营信息
                    CmisCus0022ReqDto cmisCus0022ReqDto = new CmisCus0022ReqDto();
                    cmisCus0022ReqDto.setSerno(surveySerno);//用名单流水号作为条件查询
                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022ReqDto));
                    ResultDto<CmisCus0022RespDto> cmisCus0022RespDto = cmisCusClientService.cmiscus0022(cmisCus0022ReqDto);
                    logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022RespDto));
                    CmisCus0022RespDto cmisCus0022Resp = Optional.ofNullable(cmisCus0022RespDto.getData()).orElse(new CmisCus0022RespDto());
                    if(cmisCus0022Resp != null){
                        LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoMapper.selectByPrimaryKey(exitsSurveySerno);
                        if(lmtSurveyReportComInfo == null){
                            lmtSurveyReportComInfo = new LmtSurveyReportComInfo();//
                            lmtSurveyReportComInfo.setSurveySerno(exitsSurveySerno);
                            lmtSurveyReportComInfo.setManagerId(managerId);
                            lmtSurveyReportComInfo.setManagerBrId(managerBrId);
                            lmtSurveyReportComInfo.setCreateTime(date);
                            lmtSurveyReportComInfoMapper.insertSelective(lmtSurveyReportComInfo);
                        }
                        String oneLevelTradeCode = cmisCus0022Resp.getOneLevelTradeCode();
                        String detailsTradeCode = cmisCus0022Resp.getDetailsTradeCode();
                        lmtSurveyReportComInfo.setTrade(oneLevelTradeCode + detailsTradeCode);
                        lmtSurveyReportComInfo.setOperAddr(actOperAddr);
                        lmtSurveyReportComInfo.setUpdId(managerId);
                        lmtSurveyReportComInfo.setUpdBrId(managerBrId);
                        lmtSurveyReportComInfo.setUpdDate(openday);
                        lmtSurveyReportComInfo.setUpdateTime(date);
                        lmtSurveyReportComInfoMapper.updateByPrimaryKey(lmtSurveyReportComInfo);
                    }
                }
            } else {
                //如果客户不在名单中，且状态为生效1 发回值默认04已通知区域主管进行业务分配
                checkResult = DscmsBizXwEnum.CHECK_RESULT_04.key;

                actOperAddr = actOperAddr.trim();//经营地址
                String areaName = adderCheck(actOperAddr);
                if (StringUtils.isBlank(areaName)) {
                    xdxw0008DataRespDto.setCheckResult(DscmsBizXwEnum.CHECK_RESULT_06.key);
                    return xdxw0008DataRespDto;
                }
                //如果该客户已经有分配中的任务，返回 SC020010--优农贷产品代码
                QueryModel model = new QueryModel();
                model.addCondition("certCode", certCode);
                model.addCondition("prdId", "SC020010");// 优农贷
                List<LmtSurveyTaskDivis> lmtSurveyTaskDivisList = lmtSurveyTaskDivisMapper.selectByModel(model);
                if (lmtSurveyTaskDivisList.size() > 0) {
                    xdxw0008DataRespDto.setCheckResult(DscmsBizXwEnum.CHECK_RESULT_05.key);
                    return xdxw0008DataRespDto;
                }

                String actorno = "";//分配人id
                String actorname = "";//分配人姓名
                String telnum = "";//分配人电话
                // 通过区域名称查询“用户号列表”
                List<String> userNoList = areaUserMapper.getUserNoByAreaName(areaName);
                if (userNoList.size() > 0) {
                    actorno = userNoList.get(0);
                    //第三步：在yusp_admin库执行通过角色号和“用户号列表” 查询用户号列表信息
                    logger.info("***********调用AdminSmUserService用户信息查询服务开始*START***查询参数:" + actorno);
                    ResultDto<AdminSmUserDto> userResultDto = adminSmUserService.getByLoginCode(actorno);
                    logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    //结束
                    String userCode = userResultDto.getCode();//返回结果
                    if (StringUtil.isNotEmpty(userCode) && CmisBizConstants.NUM_ZERO.equals(userCode)) {
                        AdminSmUserDto adminSmUserDto = userResultDto.getData();
                        actorname = adminSmUserDto.getUserName();
                        telnum = adminSmUserDto.getUserMobilephone();
                    }
                }

                // 如果查不到分配人
                if (StringUtils.isBlank(actorno)) {
                    xdxw0008DataRespDto.setCheckResult(DscmsBizXwEnum.CHECK_RESULT_06.key);
                    return xdxw0008DataRespDto;
                }
                //发送短信
                if (StringUtils.isNotBlank(telnum)) {
                    String sendMessage = "【张家港农商银行】您的小微信贷系统有一笔优农贷客户" + actorname + "待分配，请及时登录并进行分配。如有疑问，请咨询0512-56968262。";
                    sendMsg(sendMessage, telnum);
                }

                // 查询产品信息表
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = new CfgPrdBasicinfoDto();
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo("SC020010");
                String prdCode = prdresultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    cfgPrdBasicinfoDto = prdresultDto.getData();
                }

                //生成授信调查主表记录
                String taskSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                String lmtSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                LmtSurveyTaskDivis lmtSurveyTaskDivis = new LmtSurveyTaskDivis();
                lmtSurveyTaskDivis.setPkId(taskSerno);//主键
                lmtSurveyTaskDivis.setSurveySerno(lmtSerno);//调查流水号
                lmtSurveyTaskDivis.setBizSerno(lmtSerno);//第三方业务流水号
                lmtSurveyTaskDivis.setPrdName(DscmsBizHtEnum.YQD_TYPE_02.value);//产品名称
                lmtSurveyTaskDivis.setPrdType(cfgPrdBasicinfoDto.getPrdType());//产品类型
                lmtSurveyTaskDivis.setPrdId("SC020010");//产品编号
                lmtSurveyTaskDivis.setAppChnl("");//申请渠道

                lmtSurveyTaskDivis.setCusId(cusId);//客户编号
                lmtSurveyTaskDivis.setCusName(rqstrName);//客户名称
                lmtSurveyTaskDivis.setCertType(certType);//证件类型
                lmtSurveyTaskDivis.setCertCode(rqstrCertNo);//证件号码
                lmtSurveyTaskDivis.setAppAmt(applyAmt);//申请金额
                lmtSurveyTaskDivis.setPhone(rqstrMobile);//手机号码
                lmtSurveyTaskDivis.setWork("");//工作
                lmtSurveyTaskDivis.setLoanPurp("");//贷款用途
                lmtSurveyTaskDivis.setManagerName("");//客户经理名称
                lmtSurveyTaskDivis.setManagerId("");//客户经理编号
                lmtSurveyTaskDivis.setManagerArea("");//客户经理片区
                lmtSurveyTaskDivis.setIsStopOffline(cfgPrdBasicinfoDto.getIdStopOffline());//是否线下调查
                lmtSurveyTaskDivis.setManagerId("");//客户经理编号
                //当前系统日期
                lmtSurveyTaskDivis.setIntoTime(openday);//进件时间
                lmtSurveyTaskDivis.setDivisStatus("101");//调查分配状态="101"  未分配
                lmtSurveyTaskDivis.setMarConfirmStatus("01");//客户经理确认状态="00" 默认已确认
                lmtSurveyTaskDivis.setDivisTime(openday);//分配时间
                lmtSurveyTaskDivis.setDivisId(actorno);//分配人
                lmtSurveyTaskDivis.setMarId(saleManagerId);//营销人
                lmtSurveyTaskDivis.setPrcId("");//处理人
                lmtSurveyTaskDivis.setBelgLine("01");//默认小微条线
                //当前系统日期
                Date date = DateUtils.parseDate(openday, DateFormatEnum.DATETIME.getValue());
                lmtSurveyTaskDivis.setCreateTime(date);//创建时间
                lmtSurveyTaskDivis.setCreateTime(date);//修改时间
                logger.info("********XDXW0008*INSERT*插入lmtSurveyTaskDivis开始,新增参数为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
                lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                logger.info("********XDXW0008*INSERT*插入lmtSurveyTaskDivis结束");
            }
            xdxw0008DataRespDto.setCheckResult(checkResult);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataRespDto));
        return xdxw0008DataRespDto;
    }


    /********
     * 根据地址判断所在区域
     * *********/
    public String adderCheck(String buessineAddr) {
        logger.info("***********根据地址判断所在区域开始*START***查询参数:" + buessineAddr);
        String areaName = "";
        if (buessineAddr.contains(DscmsBizXwEnum.AREA_01.value)) {
            if (buessineAddr.contains(DscmsBizXwEnum.AREA_02.value)) {
                if (buessineAddr.contains(DscmsBizXwEnum.AREA_03.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_44.value)) {
                    areaName = DscmsBizXwEnum.AREA_04.value;
                } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_05.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_06.value)) {
                    areaName = DscmsBizXwEnum.AREA_07.value;
                } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_08.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_09.value)
                        || buessineAddr.contains(DscmsBizXwEnum.AREA_10.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_11.value)) {
                    areaName = DscmsBizXwEnum.AREA_12.value;
                } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_13.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_14.value)
                        || buessineAddr.contains(DscmsBizXwEnum.AREA_15.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_16.value)) {
                    areaName = DscmsBizXwEnum.AREA_17.value;
                }
            } else {//非张家港
                if (buessineAddr.contains(DscmsBizXwEnum.AREA_18.value)) {
                    areaName = DscmsBizXwEnum.AREA_01.value;
                } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_19.value)) {
                    areaName = DscmsBizXwEnum.AREA_19.value;
                } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_20.value)) {
                    areaName = DscmsBizXwEnum.AREA_20.value;
                }
            }
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_21.value)) {
            if (buessineAddr.contains(DscmsBizXwEnum.AREA_18.value)) {
                areaName = DscmsBizXwEnum.AREA_21.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_22.value)) {
                areaName = DscmsBizXwEnum.AREA_22.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_23.value)) {
                areaName = DscmsBizXwEnum.AREA_23.value;
            }
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_24.value)) {
            areaName = DscmsBizXwEnum.AREA_25.value;
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_26.value)) {
            areaName = DscmsBizXwEnum.AREA_27.value;
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_28.value)) {
            areaName = DscmsBizXwEnum.AREA_28.value;
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_29.value)) {
            areaName = DscmsBizXwEnum.AREA_30.value;
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_31.value)) {
            areaName = DscmsBizXwEnum.AREA_32.value;
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_33.value)) {
            if (buessineAddr.contains(DscmsBizXwEnum.AREA_34.value)) {
                areaName = DscmsBizXwEnum.AREA_35.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_36.value)) {
                areaName = DscmsBizXwEnum.AREA_37.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_38.value) || buessineAddr.contains(DscmsBizXwEnum.AREA_39.value)) {
                areaName = DscmsBizXwEnum.AREA_33.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_40.value)) {
                areaName = DscmsBizXwEnum.AREA_40.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_41.value)) {
                areaName = DscmsBizXwEnum.AREA_41.value;
            } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_42.value)) {
                areaName = DscmsBizXwEnum.AREA_42.value;
            }
        } else if (buessineAddr.contains(DscmsBizXwEnum.AREA_43.value)) {
            areaName = DscmsBizXwEnum.AREA_43.value;
        }
        logger.info("***********根据地址判断所在区域结束*END***返回参数:" + areaName);
        return areaName;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            senddxService.senddx(senddxReqDto);
        } catch (Exception e) {
            logger.info("errmsg：" + e.getMessage());
            throw new Exception("发送短信失败！");
        }
    }


    /**
     * 将List中的数据组成用逗号分隔的字符串，如'A','B','B'
     *
     * @param strList
     * @return
     */
    public static String getStr(List<String> strList) {
        String resultStr = "";
        if (strList != null && strList.size() > 0) {
            for (int i = 0; i < strList.size(); i++) {
                resultStr = resultStr + "'" + strList.get(i) + "'" + ',';
            }
            resultStr = resultStr.substring(0, resultStr.length() - 1);
        }
        return resultStr;
    }
}
