package cn.com.yusys.yusp.service.server.xdtz0007;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0007.resp.Xdtz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.web.server.xdtz0007.BizXdtz0007Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0007Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0007Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 根据客户号获取非信用方式发放贷款的最长到期日
     * @param cusId
     * @return
     */
    @Transactional
    public Xdtz0007DataRespDto getLastLoanEndDate(String cusId) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, cusId);
        Xdtz0007DataRespDto result = accLoanMapper.getLastLoanEndDate(cusId);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(result));
        return result;
    }
}
