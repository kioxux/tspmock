package cn.com.yusys.yusp.web.server.xdht0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0003.req.Xdht0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.Xdht0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0003.Xdht0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:信贷贴现合同号查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0003:信贷贴现合同号查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0003Resource.class);

    @Autowired
    private Xdht0003Service xdht0003Service;

    /**
     * 交易码：xdht0003
     * 交易描述：信贷贴现合同号查询
     *
     * @param xdht0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷贴现合同号查询")
    @PostMapping("/xdht0003")
    protected @ResponseBody
    ResultDto<Xdht0003DataRespDto> xdht0003(@Validated @RequestBody Xdht0003DataReqDto xdht0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataReqDto));
        Xdht0003DataRespDto xdht0003DataRespDto = new Xdht0003DataRespDto();// 响应Dto:信贷贴现合同号查询
        ResultDto<Xdht0003DataRespDto> xdht0003DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataReqDto));
            xdht0003DataRespDto = xdht0003Service.xdht0003(xdht0003DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataRespDto));
            // 封装xdht0003DataResultDto中正确的返回码和返回信息
            xdht0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, e.getMessage());
            xdht0003DataResultDto.setCode(e.getErrorCode());
            xdht0003DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, e.getMessage());
            // 封装xdht0003DataResultDto中异常返回码和返回信息
            xdht0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0003DataRespDto到xdht0003DataResultDto中
        xdht0003DataResultDto.setData(xdht0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataResultDto));
        return xdht0003DataResultDto;
    }
}
