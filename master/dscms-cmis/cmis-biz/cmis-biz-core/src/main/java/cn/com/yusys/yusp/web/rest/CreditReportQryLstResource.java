/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.CreditReportDto;
import cn.com.yusys.yusp.dto.CreditReportQryLstAndRealDto;
import cn.com.yusys.yusp.dto.CreditReportQryLstDto;
import cn.com.yusys.yusp.dto.InItCreditReportQryLstDto;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditReportQryLstResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditreportqrylst")
public class CreditReportQryLstResource {
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditReportQryLst>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditReportQryLst> list = creditReportQryLstService.selectAll(queryModel);
        return new ResultDto<List<CreditReportQryLst>>(list);
    }

    /**
     * @param
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CreditReportQryLst>> index(@RequestBody QueryModel queryModel) {
        List<CreditReportQryLst> list = creditReportQryLstService.selectByModel(queryModel);
        return new ResultDto<List<CreditReportQryLst>>(list);
    }

    /**
     * @param
     * @函数名称:query
     * @函数描述:查询对象列表，对外API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CreditReportQryLst>> query(@RequestBody QueryModel queryModel) {
        List<CreditReportQryLst> list = creditReportQryLstService.selectByModel(queryModel);
        return new ResultDto<List<CreditReportQryLst>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{crqlSerno}")
    protected ResultDto<CreditReportQryLst> show(@PathVariable("crqlSerno") String crqlSerno) {
        CreditReportQryLst creditReportQryLst = creditReportQryLstService.selectByPrimaryKey(crqlSerno);
        return new ResultDto<CreditReportQryLst>(creditReportQryLst);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/creditReportQry")
    protected ResultDto<CreditReportQryLst> selectCreditReportQry(@Validated @RequestBody CreditReportQryLstCrqlSernoDto creditReportQryLstCrqlSernoDto) {
        CreditReportQryLst creditReportQryLst = creditReportQryLstService.selectByPrimaryKey(creditReportQryLstCrqlSernoDto.getCrqlSerno());
        return new ResultDto<CreditReportQryLst>(creditReportQryLst);
    }

    static class CreditReportQryLstCrqlSernoDto{
        @NotBlank(message = "征信查询流水号不能为空")
        private String crqlSerno;

        public String getCrqlSerno() {
            return crqlSerno;
        }

        public void setCrqlSerno(String crqlSerno) {
            this.crqlSerno = crqlSerno;
        }
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CreditReportQryLst> create(@RequestBody CreditReportQryLst creditReportQryLst) throws URISyntaxException {
        creditReportQryLstService.insert(creditReportQryLst);
        return new ResultDto<CreditReportQryLst>(creditReportQryLst);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createxw")
    protected ResultDto<CreditReportQryLst> createXw(@RequestBody CreditReportQryLstAndRealDto creditReportQryLstDto) throws URISyntaxException {
        CreditReportQryLst creditReportQryLst = creditReportQryLstService.insertXw(creditReportQryLstDto);
        return new ResultDto<CreditReportQryLst>(creditReportQryLst);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditReportQryLst creditReportQryLst) throws URISyntaxException {
        int result = creditReportQryLstService.update(creditReportQryLst);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{crqlSerno}")
    protected ResultDto<Integer> delete(@PathVariable("crqlSerno") String crqlSerno) {
        int result = creditReportQryLstService.deleteByPrimaryKey(crqlSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditReportQryLstService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getCusRelation
     * @方法描述：获取授信申请的客户及联系人信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getCusRelation")
    protected ResultDto<List<CreditReportQryLst>> getCusRelation(@RequestBody QueryModel queryModel) {
        Map<String, Object> condition = queryModel.getCondition();
        String bizSerno = (String) condition.get("bizSerno");
        List<CreditReportQryLst> list = creditReportQryLstService.selectByApplySerno(bizSerno);
        return new ResultDto<>(list);
    }

    /**
     * @方法名称：getGrpCusRelation
     * @方法描述：获取集团授信申请的客户及联系人信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getGrpCusRelation")
    protected ResultDto<List<CreditReportQryLst>> getGrpCusRelation(@RequestBody QueryModel queryModel) {
        Map<String, Object> condition = queryModel.getCondition();
        String grpSerno = (String) condition.get("grpSerno");
        List<CreditReportQryLst> list = creditReportQryLstService.getGrpCusRelation(grpSerno);
        return new ResultDto<>(list);
    }

    /**
     * 初始化集团客户征信查询对象
     * @param inItCreditReportQryLstDto
     * @return
     */
    @PostMapping("/initgrpcreditbyscene")
    public ResultDto<Integer> initGrpcCreditByScene(@RequestBody InItCreditReportQryLstDto inItCreditReportQryLstDto) {
        int returnValue = creditReportQryLstService.initGrpcCreditByScene(inItCreditReportQryLstDto.getSerno(),inItCreditReportQryLstDto.getPeriod(),inItCreditReportQryLstDto.getBizScene());
        return new ResultDto<Integer>(returnValue);
    }

    /**
     * @方法名称：selectCreditReportQryLstByCrqlSerno
     * @方法描述：根据流水号获取征信信息
     * @参数与返回说明：
     * @算法描述：
     * @创建时间：2021-05-18 下午 21:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/selectCreditInfoByCrqlSerno")
    protected ResultDto<List<CreditReportQryLstDto>> selectCreditReportQryLstByCrqlSerno(@RequestBody QueryModel queryModel) {
        List<CreditReportQryLstDto> list = creditReportQryLstService.selectCreditReportQryLstByCrqlSerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:commitStart
     * @函数描述:提交流程判断条件是否满足
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/commitStart")
    protected ResultDto<Object> commitStart(@RequestBody Map map) throws URISyntaxException {
        String crqlSerno = (String)map.get("crqlSerno");
        String qryCls = (String)map.get("qryCls");
        String outSystemCode = (String)map.get("outSystemCode");
        String imageId = (String) map.get("imageId");
        creditReportQryLstService.commitStart(crqlSerno,qryCls,outSystemCode,imageId);
        return new ResultDto<Object>();
    }

    @PostMapping("/checkauthway")
    protected ResultDto<Object> checkAuthWay(@RequestBody CreditReportQryLst record) {
        creditReportQryLstService.checkAuthWay(record);
        return new ResultDto<Object>();
    }

    /**
     * @方法名称：getCreditInfo
     * @方法描述：根据流水号获取征信台账信息
     * @参数与返回说明：
     * @算法描述：
     * @创建时间：2021-06-01 下午 16:11
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getCreditInfo")
    protected ResultDto<List<CreditReportQryLstDto>> getCreditInfo(@RequestBody QueryModel queryModel) {
        List<CreditReportQryLstDto> list = creditReportQryLstService.getCreditInfo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:asyncExportCreditList
     * @函数描述:异步导出人行征信台账信息明细
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/asyncExportCreditList")
    protected ResultDto<ProgressDto> asyncExportCreditList(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = creditReportQryLstService.asyncExportCreditList(queryModel);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:asyncExportCreditListDZ
     * @函数描述:异步导出苏州地方征信台账信息明细
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/asyncExportCreditListDZ")
    protected ResultDto<ProgressDto> asyncExportCreditListDZ(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = creditReportQryLstService.asyncExportCreditListDZ(queryModel);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:createCreditAndRel
     * @函数描述:业务方创建征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createCreditAndRel")
    protected ResultDto<CreditReportQryLstDto> createCreditAndRel(@RequestBody CreditReportQryLstDto creditReportQryLstDto) throws URISyntaxException {
        creditReportQryLstService.createCreditAndRel(creditReportQryLstDto);
        return new ResultDto<CreditReportQryLstDto>(creditReportQryLstDto);
    }

    /**
     * 根据场景初始化征信查询对象
     * @param inItCreditReportQryLstDto
     * @return
     */
    @PostMapping("/initCreditByScene")
    public ResultDto<Integer> initCreditByScene(@RequestBody InItCreditReportQryLstDto inItCreditReportQryLstDto) {
        int returnValue = creditReportQryLstService.initCreditByScene(inItCreditReportQryLstDto.getSerno(),inItCreditReportQryLstDto.getPeriod(),inItCreditReportQryLstDto.getBizScene());
        return new ResultDto<Integer>(returnValue);
    }

    /**
     * @函数名称:updateCreditAndRel
     * @函数描述:业务方更新征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateCreditAndRel")
    protected ResultDto<Object> update(@RequestBody CreditReportQryLstDto creditReportQryLstDto) throws URISyntaxException {
        creditReportQryLstService.updateCreditAndRel(creditReportQryLstDto);
        return new ResultDto<>();
    }

    /**
     * @函数名称:deleteCreditAndRel
     * @函数描述:业务方删除征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteCreditAndRel")
    protected ResultDto<Object> deleteCreditAndRel(@RequestBody Map map) {
        creditReportQryLstService.deleteCreditAndRel(map);
        return new ResultDto<>();
    }

    /**
     * @函数名称:handleCreitData
     * @函数描述:查询地方征信时，需处理的业务逻辑步骤
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/handlecreitdata")
    protected ResultDto<Object> handleCreitData(@RequestBody String crqlSerno) {
        Map<String, String> map = creditReportQryLstService.handleCreitData(crqlSerno);
        return new ResultDto<Object>(map);
    }

    /**
     * @函数名称:createCreditAuto
     * @函数描述:业务方新增时，自动创建征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createcreditauto")
    protected ResultDto<Integer> createCreditAuto(@RequestBody CreditReportQryLstAndRealDto creditReportQryLstAndRealDto) {
        int count = creditReportQryLstService.createCreditAuto(creditReportQryLstAndRealDto);
        return new ResultDto<>(count);
    }

    /**
     * @函数名称:createAndUpdate
     * @函数描述:通用接口有则更新，无则新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/createandupdate")
    protected ResultDto<Integer> createAndUpdate(@RequestBody CreditReportQryLst creditReportQryLst) {
        int count = creditReportQryLstService.createAndUpdate(creditReportQryLst);
        return new ResultDto<>(count);
    }

    /**
     * @函数名称:checkOutReport
     * @函数描述:校验是否可查看征信报告
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkoutreport")
    protected ResultDto<Boolean> checkOutReport(@RequestBody CreditReportQryLst creditReportQryLst) {
        Boolean result = creditReportQryLstService.checkOutReport(creditReportQryLst);
        return new ResultDto<Boolean>(result);
    }

    /**
     * @函数名称:checkcredreport
     * @函数描述:查询征信
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkcredreport/")
    protected ResultDto<List<CreditReportDto>> checkcredreport (@RequestBody QueryModel queryModel) {
        List<CreditReportDto> result = creditReportQryLstService.checkCredReport(queryModel);
        return new ResultDto<List<CreditReportDto>>(result);
    }


    /**
     * @函数名称: updateCreditReportQryLst
     * @函数描述: 更新征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatecredreport/{crqlserno}")
    protected ResultDto<Integer> updatecreditReportQryLst (@PathVariable String crqlserno, @RequestBody CreditReportDto creditReportDto) {
        int result = creditReportQryLstService.updatecreditReportQryLst(crqlserno, creditReportDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: importCreditReportList
     * @函数描述: 引入30天内已查询的征信报告
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/importcreditreportlist")
    protected ResultDto<List<CreditReportQryLst>> importCreditReportList (@RequestBody QueryModel queryModel) {
        List<CreditReportQryLst> list = creditReportQryLstService.importCreditReportList(queryModel);
        return new ResultDto<List<CreditReportQryLst>>(list);
    }

    /**
     * @函数名称:importCreditAndRel
     * @函数描述:导入征信信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/importcreditandrel")
    protected ResultDto<String> importCreditAndRel(@RequestBody CreditReportQryLstDto creditReportQryLstDto) {
        String crqlSerno = creditReportQryLstService.importCreditAndRel(creditReportQryLstDto);
        return new ResultDto<String>(crqlSerno);
    }
}
