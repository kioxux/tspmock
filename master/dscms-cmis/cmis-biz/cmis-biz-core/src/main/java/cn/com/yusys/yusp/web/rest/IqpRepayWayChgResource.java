/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdzqgjh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayWayChg;
import cn.com.yusys.yusp.service.IqpRepayWayChgService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayWayChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-20 14:25:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepaywaychg")
public class IqpRepayWayChgResource {
    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRepayWayChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRepayWayChg> list = iqpRepayWayChgService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /***
     * @param []
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.IqpRepayWayChg>>
     * @author tangxun
     * @date 2021/5/24 19:16
     * @version 1.0.0
     * @desc huan
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryRepayPlan")
    protected ResultDto<List<Lstdkhk>> queryRepayPlan(@RequestBody QueryModel queryModel) {
        List<Lstdkhk> list = iqpRepayWayChgService.queryRepayPlan(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpRepayWayChg>> index(@RequestBody QueryModel queryModel) {
        List<IqpRepayWayChg> list = iqpRepayWayChgService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{iqpSerno}")
    protected ResultDto<IqpRepayWayChg> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpRepayWayChg iqpRepayWayChg = iqpRepayWayChgService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<>(iqpRepayWayChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpRepayWayChg> create(@RequestBody IqpRepayWayChg iqpRepayWayChg) throws URISyntaxException {
        iqpRepayWayChgService.insertSelective(iqpRepayWayChg);
        return new ResultDto<>(iqpRepayWayChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRepayWayChg iqpRepayWayChg) throws URISyntaxException {
        int result = iqpRepayWayChgService.updateSelective(iqpRepayWayChg);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpRepayWayChgService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRepayWayChgService.deleteByIds(ids);
        return new ResultDto<>(result);
    }
}
