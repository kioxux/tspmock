/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.GrtGuarContDto;
import cn.com.yusys.yusp.dto.IqpCertiInoutAppDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCertiInoutApp;
import cn.com.yusys.yusp.service.IqpCertiInoutAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-20 15:04:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcertiinoutapp")
public class IqpCertiInoutAppResource {
    @Autowired
    private IqpCertiInoutAppService iqpCertiInoutAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpCertiInoutApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpCertiInoutApp> list = iqpCertiInoutAppService.selectAll(queryModel);
        return new ResultDto<List<IqpCertiInoutApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpCertiInoutApp>> index(QueryModel queryModel) {
        List<IqpCertiInoutApp> list = iqpCertiInoutAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpCertiInoutApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<IqpCertiInoutApp> show(@PathVariable("serno") String serno) {
        IqpCertiInoutApp iqpCertiInoutApp = iqpCertiInoutAppService.selectByPrimaryKey(serno);
        return new ResultDto<IqpCertiInoutApp>(iqpCertiInoutApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpCertiInoutApp> create(@RequestBody IqpCertiInoutApp iqpCertiInoutApp) throws URISyntaxException {
        iqpCertiInoutAppService.insert(iqpCertiInoutApp);
        return new ResultDto<IqpCertiInoutApp>(iqpCertiInoutApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpCertiInoutApp iqpCertiInoutApp) throws URISyntaxException {
        int result = iqpCertiInoutAppService.update(iqpCertiInoutApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpCertiInoutAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpCertiInoutAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:updateSelectByParmas
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelectByParmas")
    protected ResultDto<Integer> updateSelectByParmas(@RequestBody Map parmas) throws Exception {
        int rtnData = iqpCertiInoutAppService.updateSelectByParmas(parmas);
        return new ResultDto<>(rtnData);
    }
    
    /**
     * @函数名称:deleteSelectiveByParams
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteSelectiveByParams")
    protected ResultDto<Integer> deleteSelectiveByParams(@RequestBody Map parmas) throws Exception {
        int rtnData = iqpCertiInoutAppService.deleteSelectiveByParams(parmas);
        return new ResultDto<>(rtnData);
    }
    /**
     * 权证初始化担保合同信息
     * @param guarContNo
     * @return
     */
    
    @GetMapping ("/querySelectContNo/{guarContNo}/{pkId}")
    public ResultDto<IqpCertiInoutAppDto> querySelectContNo(@PathVariable("guarContNo") String guarContNo,@PathVariable("pkId") String pkId){
        IqpCertiInoutAppDto iqpCertiInoutAppDto = iqpCertiInoutAppService.querySelectContNo(guarContNo,pkId);
        return new ResultDto<>(iqpCertiInoutAppDto);
    }
    /**
     * 根据获取权证信息
     * @param
     * @return
     */
    @PostMapping ("/getQueryInfo")
    public ResultDto<String> getQueryInfo(@RequestBody Map params){
        String rtnData = iqpCertiInoutAppService.getQueryInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 更新押品权证状态（已记账）
     * @param
     * @return
     */
    @PostMapping ("/updateGuarCertiStatus")
    public ResultDto<Integer> updateGuarCertiStatus(@RequestBody Map params) throws Exception{
        int rtnData = iqpCertiInoutAppService.updateGuarCertiStatus(params);
        return new ResultDto<>(rtnData);
    }
}
