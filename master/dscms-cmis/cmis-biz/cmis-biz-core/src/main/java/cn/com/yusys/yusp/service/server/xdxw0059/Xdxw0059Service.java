package cn.com.yusys.yusp.service.server.xdxw0059;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0059.req.Xdxw0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0059.resp.Xdxw0059DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGuareInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 业务逻辑类:根据调查流水号查询抵押率
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdxw0059Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0059Service.class);
    @Autowired
    private LmtGuareInfoMapper lmtGuareInfoMapper;

    /**
     * 交易码：xdxw0059
     * 交易描述：根据调查流水号查询抵押率
     *
     * @param xdxw0059DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0059DataRespDto xdxw0059(Xdxw0059DataReqDto xdxw0059DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdxw0059DataReqDto));
        Xdxw0059DataRespDto xdxw0059DataRespDto = new Xdxw0059DataRespDto();
        try {
            // 从xdxw0059DataReqDto获取业务值进行业务逻辑处理
            String applyNo = xdxw0059DataReqDto.getApplyNo();//无还本续贷申请表主键
            Map queryMap = new HashMap<>();
            queryMap.put("survey_serno", applyNo);//申请流水号
            logger.info("根据业务唯一编号查询在信贷系统中的抵押率开始,查询参数为:{}", JSON.toJSONString(queryMap));
            BigDecimal pldRate = lmtGuareInfoMapper.queryWhbxdDYL(queryMap);
            logger.info("根据业务唯一编号查询在信贷系统中的抵押率结束,返回结果为:{}", JSON.toJSONString(pldRate));
            xdxw0059DataRespDto.setPldRate(pldRate);// 抵押率
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdxw0059DataRespDto));
        return xdxw0059DataRespDto;
    }
}
