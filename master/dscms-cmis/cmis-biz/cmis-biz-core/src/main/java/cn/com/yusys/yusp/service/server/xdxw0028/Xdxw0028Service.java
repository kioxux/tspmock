package cn.com.yusys.yusp.service.server.xdxw0028;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0028.req.Xdxw0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0028.resp.LoanList;
import cn.com.yusys.yusp.dto.server.xdxw0028.resp.Xdxw0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdxw0028Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0028Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0028Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;//台账信息表

    /**
     * 根据客户号查询我行现有融资情况
     *
     * @param Xdxw0028DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0028DataRespDto xdxw0028(Xdxw0028DataReqDto Xdxw0028DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value);
        //返回信息
        Xdxw0028DataRespDto Xdxw0028DataRespDto = new Xdxw0028DataRespDto();
        try {
            //请求字段
            String cusId = Xdxw0028DataReqDto.getCusNo();//客户号

            //调用额度系统服务接口
            List<LoanList> list = accLoanMapper.getLoanBalanceInfoByCusId(cusId);
            //获取信用经营性贷款金额和信用消费性贷款金额
            logger.info("****XDXW0028***获取信用经营性贷款金额和信用消费性贷款金额开始;客户调查表编号:"+cusId);
            HashMap<String, BigDecimal> map = accLoanMapper.getLoanCreditConsumeAmtByCusId(cusId);
            logger.info("****XDXW0028***获取信用经营性贷款金额和信用消费性贷款金额开始结束,返回结果为:{}", JSON.toJSONString(map));
            BigDecimal creditLoanAmt = map.get("creditloanamt");
            BigDecimal creditConsumeAmt = map.get("creditconsumeamt");

            //查询对应的字典
            logger.info("******XDXW0028***获取字典项缓存开始,查询参数为:{}", JSON.toJSONString(list));
            list = Optional.ofNullable(list).orElseThrow(() -> BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value));
            list = list.parallelStream().map(ret -> {
                LoanList temp = new LoanList();
                BeanUtils.copyProperties(ret, temp);
                //担保方式码值转换
                String guarType = temp.getCnAssureMeans();//参数值
                String guarTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY", guarType);
                temp.setCnAssureMeans(guarTypeName);
                //台账状态码值转换
                String accStatus = temp.getBillStatus();
                String accStatusName = DictTranslatorUtils.findValueByDictKey("STD_ACC_STATUS", accStatus);
                temp.setBillStatus(accStatusName);
                //信用经营性贷款金额和信用消费性贷款金额
                temp.setCreditLoanAmt(creditLoanAmt);
                temp.setCreditConsumeAmt(creditConsumeAmt);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            logger.info("*************获取字典项缓存结束*******************");

            //返回信息
            Xdxw0028DataRespDto.setLoanList(list);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value);
        return Xdxw0028DataRespDto;
    }

}
