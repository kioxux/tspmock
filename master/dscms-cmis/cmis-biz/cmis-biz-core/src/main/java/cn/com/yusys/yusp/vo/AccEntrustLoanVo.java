package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "委托贷款台账列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccEntrustLoanVo {


    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
     借据编号
      */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    担保方式
     */
    @ExcelField(title = "担保方式", viewLength = 20 , dictCode = "STD_ZB_GUAR_WAY")
    private String guarMode;

    /*
    贷款金额
     */
    @ExcelField(title = "贷款金额", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal loanAmt;

    /*
    贷款余额
     */
    @ExcelField(title = "贷款余额", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal loanBalance;

    /*
    贷款起始日
     */
    @ExcelField(title = "贷款起始日", viewLength = 20)
    private String loanStartDate;

    /*
    贷款到期日
     */
    @ExcelField(title = "贷款到期日", viewLength = 20)
    private String loanEndDate;

    /*
    执行年利率
     */
    @ExcelField(title = "执行年利率", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal execRateYear;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
     */
    @ExcelField(title = "台账状态", viewLength = 20 , dictCode = "STD_ACC_STATUS")
    private String accStatus;

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
