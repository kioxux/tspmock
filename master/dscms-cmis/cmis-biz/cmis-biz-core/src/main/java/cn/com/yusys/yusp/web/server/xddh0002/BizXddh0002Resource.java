package cn.com.yusys.yusp.web.server.xddh0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xddh0002.Xddh0002Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;

/**
 * 接口处理类:新增主动还款申请记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0002:新增主动还款申请记录")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0002Resource.class);

    @Autowired
    private Xddh0002Service xddh0002Service;

    /**
     * 交易码：xddh0002
     * 交易描述：新增主动还款申请记录
     *
     * @param xddh0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新增主动还款申请记录")
    @PostMapping("/xddh0002")
    protected @ResponseBody
    ResultDto<Xddh0002DataRespDto> xddh0002(@Validated @RequestBody Xddh0002DataReqDto xddh0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataReqDto));
        Xddh0002DataRespDto xddh0002DataRespDto = new Xddh0002DataRespDto();// 响应Dto:新增主动还款申请记录
        ResultDto<Xddh0002DataRespDto> xddh0002DataResultDto = new ResultDto<>();
        try {
            String repayChannel = xddh0002DataReqDto.getRepayChannel();
            if (StringUtil.isEmpty(repayChannel)) {
                xddh0002DataResultDto.setCode(EpbEnum.EPB099999.key);
                xddh0002DataResultDto.setMessage("还款渠道【repayChannel】不能为空！");
                return xddh0002DataResultDto;
            }
            // 从xddh0002DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataReqDto));
            xddh0002DataRespDto = xddh0002Service.xddh0002(xddh0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataRespDto));
            // 封装xddh0002DataResultDto中正确的返回码和返回信息
            String opFlag = xddh0002DataRespDto.getOpFlag();
            String opMessage = xddh0002DataRespDto.getOpMsg();
            //如果失败，返回9999
            if("F".equals(opFlag)){//出账失败
                xddh0002DataResultDto.setCode(EpbEnum.EPB099999.key);
                xddh0002DataResultDto.setMessage(opMessage);
            }else{
                xddh0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddh0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, e.getMessage());
            // 封装xddh0002DataResultDto中异常返回码和返回信息
            xddh0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0002DataResultDto.setMessage(e.getMessage());
        }
        // 封装xddh0002DataRespDto到xddh0002DataResultDto中
        xddh0002DataResultDto.setData(xddh0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataResultDto));
        return xddh0002DataResultDto;
    }
}
