package cn.com.yusys.yusp.web.server.xdtz0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0022.Xdtz0022Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0022.req.Xdtz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.resp.Xdtz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:保证金台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0022:保证金台账入账")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0022Resource.class);


    @Autowired
    private Xdtz0022Service xdtz0022Service;

    /**
     * 交易码：xdtz0022
     * 交易描述：保证金台账入账
     *
     * @param xdtz0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金台账入账")
    @PostMapping("/xdtz0022")
    protected @ResponseBody
    ResultDto<Xdtz0022DataRespDto> xdtz0022(@Validated @RequestBody Xdtz0022DataReqDto xdtz0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataReqDto));
        Xdtz0022DataRespDto xdtz0022DataRespDto = new Xdtz0022DataRespDto();// 响应Dto:保证金台账入账
        ResultDto<Xdtz0022DataRespDto> xdtz0022DataResultDto = new ResultDto<>();
        String billNo = xdtz0022DataReqDto.getBillNo();//借据号
        String contNo = xdtz0022DataReqDto.getContNo();//合同号
        String busiCurType = xdtz0022DataReqDto.getBusiCurType();//业务币种
        BigDecimal busiBal = xdtz0022DataReqDto.getBusiBal();//业务余额
        BigDecimal applyAmt = xdtz0022DataReqDto.getApplyAmt();//申请金额
        BigDecimal bailAmt = xdtz0022DataReqDto.getBailAmt();//保证金金额
        BigDecimal exchgRate = xdtz0022DataReqDto.getExchgRate();//汇率
        String bizType = xdtz0022DataReqDto.getBizType();//业务品种
        try {
            // 从xdtz0022DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataReqDto));
            xdtz0022DataRespDto = xdtz0022Service.xdtz0022(xdtz0022DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataRespDto));
            // TODO 调用XXXXXService层结束
            // TODO 封装xdtz0022DataRespDto对象开始
//            xdtz0022DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
//            xdtz0022DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xdtz0022DataRespDto对象结束
            // 封装xdtz0022DataResultDto中正确的返回码和返回信息
            xdtz0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, e.getMessage());
            // 封装xdtz0022DataResultDto中异常返回码和返回信息
            xdtz0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0022DataRespDto到xdtz0022DataResultDto中
        xdtz0022DataResultDto.setData(xdtz0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataResultDto));
        return xdtz0022DataResultDto;
    }
}
