/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.GrtGuarContRel;
import cn.com.yusys.yusp.domain.IqpCertiInoutRel;
import cn.com.yusys.yusp.dto.GuarCertiRelaClientDto;
import cn.com.yusys.yusp.dto.GuarClientRsDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpCertiInoutRelMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 10:21:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpCertiInoutRelService {
  private static final Logger log = LoggerFactory.getLogger(IqpCertiInoutRelService.class);
    @Autowired
    private IqpCertiInoutRelMapper iqpCertiInoutRelMapper;
  @Autowired
  private IqpCertiInoutAppService iqpCertiInoutAppService;
  // @Autowired
  // private IGuarClientService iGuarClientService;//押品端服务接口
  @Autowired
  private GrtGuarContService grtGuarContService;
  @Autowired
  private GrtGuarContRelService grtGuarContRelService;

  /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpCertiInoutRel selectByPrimaryKey(String pkId) {
        return iqpCertiInoutRelMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: selectBySernoKey
     * @方法描述: 根据押品出入库申请流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpCertiInoutRel> selectBySernoKey(String serno) {
        return iqpCertiInoutRelMapper.selectBySernoKey(serno);
    }
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpCertiInoutRel> selectAll(QueryModel model) {
        List<IqpCertiInoutRel> records = (List<IqpCertiInoutRel>) iqpCertiInoutRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpCertiInoutRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpCertiInoutRel> list = iqpCertiInoutRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpCertiInoutRel record) {
        return iqpCertiInoutRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpCertiInoutRel record) {
        return iqpCertiInoutRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpCertiInoutRel record) {
        return iqpCertiInoutRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpCertiInoutRel record) {
        return iqpCertiInoutRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpCertiInoutRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpCertiInoutRelMapper.deleteByIds(ids);
    }

    /**
     * 获取权证与权证出入库申请的关系表中的押品统一编号
     * @param map
     * @return
     */
    public List<String> getIqpCertiInoutRelInGuarNo(Map map) {
        return iqpCertiInoutRelMapper.getIqpCertiInoutRelInGuarNo(map);
    }
    
    /**
     * @方法名称: querySelectBySerno
     * @方法描述: 根据押品出入库申请流水号获取权证主键
     * @参数与返回说明:
     * @算法描述: 无
     */
    public  String querySelectBySerno(String serno){
        String result = "";
         List<IqpCertiInoutRel> iqpCertiInoutRelList = iqpCertiInoutRelMapper.querySelectBySerno(serno);
         if (CollectionUtils.isEmpty(iqpCertiInoutRelList) && iqpCertiInoutRelList.size() ==0 ) {
             return result;
         }
         for(IqpCertiInoutRel iqpCertiInoutRel:iqpCertiInoutRelList){
             result = result + iqpCertiInoutRel.getCertiPkId() + CmisCommonConstants.COMMON_SPLIT_COMMA;
         }
         if (StringUtils.nonBlank(result)) {
             result = result.substring(0, result.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
         }
         return result;
    }

  /**
   * 调用接口获取押品编号
   * @param
   * @return
   */
  public String querySelectGuarNo(String optType) {
    String certiState = "";
    String inoutApptype = "";
      String certiPkId = "";
/*    User userInfo = SessionUtils.getUserInformation();
    if(userInfo == null){
      throw new YuspException(EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key,EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value);
    }
    List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataByGuarNo(userInfo.getLoginCode());
    if (CollectionUtils.isEmpty(grtGuarContList) || grtGuarContList.size() == 0 ){
      log.info("权证出入库申请" + userInfo.getLoginCode() + "当前登录用户暂无数据！");
      return certiPkId;
    }*/
      //权证正常入库
      if (optType.equals(CmisBizConstants.OPT_TYPE_ON_STORAGE)) {
          certiState = CmisBizConstants.IQP_CERTI_STATU_01;
      }
      //权证正常出库
      if (optType.equals(CmisBizConstants.OPT_TYPE_ON_OUTBOUND)) {
          certiState = CmisBizConstants.IQP_CERTI_STATU_04;
      }
      //权证归还入库
      if (optType.equals(CmisBizConstants.OPT_TYPE_ON_RETURN)) {
          certiState = CmisBizConstants.IQP_CERTI_STATU_07;
      }
      //权证换证入库
      if (optType.equals(CmisBizConstants.OPT_TYPE_ON_RENEWAL)) {
          certiState = CmisBizConstants.IQP_CERTI_STATU_10;
      }
      //权证出库查看
      if (optType.equals(CmisBizConstants.OPT_TYPE_ON_VIEW)) {
          certiState = CmisBizConstants.IQP_CERTI_STATU_10 + "," + CmisBizConstants.IQP_CERTI_STATU_09 + "," + CmisBizConstants.IQP_CERTI_STATU_08;
      }
      String guarNo = "";
      String oprType = CmisCommonConstants.OPR_TYPE_ADD;
      List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.getGrtGuarContRel(oprType);
      if (CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0) {
      }
      for (GrtGuarContRel grtGuarContRels : grtGuarContRelList) {
          guarNo = guarNo + grtGuarContRels.getGuarNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
      }

      GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
      guarCertiRelaClientDto.setCertiState(certiState);
      log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口开始");
      log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
      GuarClientRsDto guarClientRsDto = null;// iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
      log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口结束");
      if (guarClientRsDto != null) {
          if (EcbEnum.IQP_CHG_SUCCESS_DEF.key.equals(guarClientRsDto.getRtnCode())) {
              List<Map> guarCertiRelaClientDtoList = (List<Map>) guarClientRsDto.getData();
              if (CollectionUtils.isEmpty(guarCertiRelaClientDtoList) || guarCertiRelaClientDtoList.size() == 0) {
                  log.info("权证出入库申请" + guarCertiRelaClientDto + "押品权证不存在");
                  return certiPkId;
              }
              for (Map guarCertiRelaClientDtos : guarCertiRelaClientDtoList) {
                  certiPkId = certiPkId + guarCertiRelaClientDtos.get("pkId") + CmisCommonConstants.COMMON_SPLIT_COMMA;
                  guarNo = guarNo + guarCertiRelaClientDtos.get("guarNo") + CmisCommonConstants.COMMON_SPLIT_COMMA;
              }

              if (StringUtils.nonBlank(guarNo)) {
                  guarNo = guarNo.substring(0, guarNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
              }
              certiPkId = queryIqpCertiInOutApp(guarNo, certiState, optType);
          }
      }
    return queryIqpCertiInOutApp(guarNo,certiState,optType);
  }

  /**
   * 根据获取权证列表信息
   * @param
   * @return
   */
    public String queryIqpCertiInOutApp(String guarNo,String certiState,String optType){
      String certiPkId = "";
      Map map = new HashMap();
      map.put("guarNo",guarNo);

      List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectGrtGuarContRelByGuarNo(map);
      if(CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0){
        log.info("关联押品数据不存在");
        return certiPkId;
      }
      List<Map> list = new ArrayList<>();
      for (int i = 0; i < grtGuarContRelList.size(); i++) {
        Map guarNoMap = new HashMap();
        guarNoMap.put("guarNo",grtGuarContRelList.get(i).getGuarNo());
        list.add(guarNoMap);
      }

      GuarCertiRelaClientDto guarCertiRelaClientDto = new GuarCertiRelaClientDto();
      log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口开始");
      guarCertiRelaClientDto.setCertiState(certiState);
      guarCertiRelaClientDto.setList(list);
      log.info("接口请求入参" + JSON.toJSONString(guarCertiRelaClientDto));
      GuarClientRsDto guarClientRsDto = null;//iGuarClientService.getGuarCertiRelaByGuarNo(guarCertiRelaClientDto);
      log.info("权证出入库申请" + guarCertiRelaClientDto + "调用押品端接口结束");
      if(guarClientRsDto != null ) {
          if (EcbEnum.IQP_CHG_SUCCESS_DEF.key.equals(guarClientRsDto.getRtnCode())) {
              List<Map> guarCertiRelaClientDtoList = (List<Map>) guarClientRsDto.getData();
              if (CollectionUtils.isEmpty(guarCertiRelaClientDtoList) || guarCertiRelaClientDtoList.size() == 0) {
                  log.info("权证出入库申请" + guarCertiRelaClientDto + "押品权证不存在");
                  return certiPkId;
              }
              for (Map guarCertiRelaClientDtos : guarCertiRelaClientDtoList) {
                  certiPkId = certiPkId + guarCertiRelaClientDtos.get("pkId") + CmisCommonConstants.COMMON_SPLIT_COMMA;
              }

              if (StringUtils.nonBlank(certiPkId)) {
                  certiPkId = certiPkId.substring(0, certiPkId.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
              }
          }
      }

        List<IqpCertiInoutRel> iqpCertiInoutRelList = iqpCertiInoutRelMapper.selectByGuarNo(guarNo);
        if (CollectionUtils.isEmpty(iqpCertiInoutRelList) || iqpCertiInoutRelList.size() == 0) {
            log.info(certiPkId + "权证未发生出入库申请");
            return certiPkId;
        }

        String pkId = "";
        String guarPkId = "";//已发生权证申请的权证主键
        //判断权证申请是否审批在途
        for (IqpCertiInoutRel iqpCertiInoutRel : iqpCertiInoutRelList) {
            guarPkId = guarPkId + iqpCertiInoutRel.getCertiPkId() + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }
        if (StringUtils.nonBlank(guarPkId)) {
            guarPkId = guarPkId.substring(0, guarPkId.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }
        //换证出库列表因只展示权证出入库状态为03-换证出库
        if (optType.equals(CmisBizConstants.OPT_TYPE_ON_RENEWAL)) {
            map.put("inoutApptype", CmisBizConstants.IQP_INOUT_APPTYPE_03);
        }
        map.remove("guarNo", guarNo);
        map.put("certiPkId", guarPkId);
        map.put("approveStatus", CmisCommonConstants.WF_STATUS_997);

        List<Map> iqpCertiInOutAppList = iqpCertiInoutAppService.selectIqpCertiInOutApp(map);
        if (CollectionUtils.isEmpty(iqpCertiInOutAppList) || iqpCertiInOutAppList.size() == 0) {
            log.info("权证出入库申请" + certiPkId + "不存在流程审批的权证出入库申请");
            return certiPkId;
        }

        for (Map iqpCertiInOutApp : iqpCertiInOutAppList) {
            pkId = pkId + iqpCertiInOutApp.get("certiPkId") + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }
        if (StringUtils.nonBlank(pkId)) {
            pkId = pkId.substring(0, pkId.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }
        String[] certiPkIdAry = certiPkId.split(CmisCommonConstants.COMMON_SPLIT_COMMA);
        String[] pkIdAry = pkId.split(CmisCommonConstants.COMMON_SPLIT_COMMA);

        for (int i = 0; i < certiPkIdAry.length; i++) {
            String obj = certiPkIdAry[i];
            boolean isExist = false;
            for (int j = 0; j < pkIdAry.length; j++) {
                String cpk = certiPkIdAry[i];
                if (obj.equals(cpk)) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                certiPkId = obj + CmisCommonConstants.COMMON_SPLIT_COMMA;
            }
        }
        return certiPkId;
    }

}


