/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydIouIetm
 * @类描述: tmp_wyd_iou_ietm数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-23 17:10:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_iou_ietm_tmp")
public class TmpWydIouIetmTmp {
	
	/** 数据日期 **/
	@Id
	@Column(name = "LENDING_REF")
	private String lendingRef;
	
	/** 合作机构号 **/
	@Column(name = "IETM_CD", unique = false, nullable = true, length = 20)
	private String ietmCd;
	
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param ietmCd
	 */
	public void setIetmCd(String ietmCd) {
		this.ietmCd = ietmCd;
	}
	
    /**
     * @return ietmCd
     */
	public String getIetmCd() {
		return this.ietmCd;
	}


}