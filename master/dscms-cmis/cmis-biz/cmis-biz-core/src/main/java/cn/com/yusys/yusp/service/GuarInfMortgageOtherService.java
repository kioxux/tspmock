/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfMortgageOther;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarInfMortgageOther;
import cn.com.yusys.yusp.repository.mapper.GuarInfMortgageOtherMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMortgageOtherService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:17:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarInfMortgageOtherService {

    @Autowired
    private GuarInfMortgageOtherMapper guarInfMortgageOtherMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarInfMortgageOther selectByPrimaryKey(String serno) {
        return guarInfMortgageOtherMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarInfMortgageOther> selectAll(QueryModel model) {
        List<GuarInfMortgageOther> records = (List<GuarInfMortgageOther>) guarInfMortgageOtherMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarInfMortgageOther> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarInfMortgageOther> list = guarInfMortgageOtherMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarInfMortgageOther record) {
        return guarInfMortgageOtherMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarInfMortgageOther record) {
        return guarInfMortgageOtherMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarInfMortgageOther record) {
        return guarInfMortgageOtherMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarInfMortgageOther record) {
        return guarInfMortgageOtherMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarInfMortgageOtherMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarInfMortgageOtherMapper.deleteByIds(ids);
    }

    /**
     * 对guarInfMortgageOther单个对象保存(新增或修改),如果没有数据新增,有数据修改
     * @param guarInfMortgageOther
     * @return
     */
    public int preserveGuarInfMortgageOther(GuarInfMortgageOther guarInfMortgageOther) {
        String serno = guarInfMortgageOther.getSerno();
        //查询该对象是否存在
        GuarInfMortgageOther queryGuarInfMortgageOther = guarInfMortgageOtherMapper.selectByPrimaryKey(serno);
        if (null == queryGuarInfMortgageOther){
            return guarInfMortgageOtherMapper.insert(guarInfMortgageOther);
        }else{
            return guarInfMortgageOtherMapper.updateByPrimaryKey(guarInfMortgageOther);
        }
    }
}
