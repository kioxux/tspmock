/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.BailExtractApp;
import cn.com.yusys.yusp.repository.mapper.BailExtractAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailExtractAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 14:50:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BailExtractAppService {

    @Autowired
    private BailExtractAppMapper bailExtractAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BailExtractApp selectByPrimaryKey(String pkId) {
        return bailExtractAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BailExtractApp> selectAll(QueryModel model) {
        List<BailExtractApp> records = (List<BailExtractApp>) bailExtractAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BailExtractApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BailExtractApp> list = bailExtractAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BailExtractApp record) {
        return bailExtractAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BailExtractApp record) {
        return bailExtractAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BailExtractApp record) {
        return bailExtractAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BailExtractApp record) {
        return bailExtractAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return bailExtractAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return bailExtractAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<BailExtractApp> toSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return bailExtractAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: queryBailExtractAppDataByParams
     * @方法描述: 根据入参查询保证金提取数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public BailExtractApp queryBailExtractAppDataByParams(Map queryMap) {
        return bailExtractAppMapper.queryBailExtractAppDataByParams(queryMap);
    }

    /**
     * @方法名称: updateApproveStatusAfterFlow
     * @方法描述: 流程后处理
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-06-15 19:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public void updateApproveStatusAfterFlow(BailExtractApp bailExtractApp, String wfStatus) {
        bailExtractApp.setApproveStatus(wfStatus);
        updateSelective(bailExtractApp);
        // 保证金提取流程 后处理

    }
}
