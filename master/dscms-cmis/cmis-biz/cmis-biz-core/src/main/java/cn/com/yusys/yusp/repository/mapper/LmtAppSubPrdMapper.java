/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubPrdMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:23:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtAppSubPrdMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtAppSubPrd selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtAppSubPrd> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtAppSubPrd record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtAppSubPrd record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtAppSubPrd record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtAppSubPrd record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPkId(LmtAppSubPrd record);

    /**
     * @方法名称: calLmtAmt
     * @方法描述: 根据授信分项编号（sub_serno）计算授信额度总和
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal calLmtAmt(String subSerno);

    /**
     * 通过入参查询授信申请信息
     *
     * @param queryMap
     * @return
     */
    List<LmtAppSubPrd> selectByParams(Map queryMap);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据授信分项流水号逻辑删除授信适用产品信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySubSerno(@Param("subSerno") String subSerno);

    /**
     * 根据适用分项产品编号获取适用产品对象
     *
     * @param subPrdSerno
     * @return
     */
    LmtAppSubPrd selectBySubPrdSerno(String subPrdSerno);

    int insertLmtAppSubPrdList(@Param("list")List<LmtAppSubPrd> list);

    BigDecimal selectHighestLmtAmt(String subSerno);
    /**
     * @方法名称: querySxkdAmtBySerno
     * @方法描述: 获取省心快贷授信金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map querySxkdAmtBySerno(String serno);

    /**
     * 根据subSerno查询一般额度授信信息
     * @param subSerno
     * @return
     */
    List<LmtAppSubPrd> selectBySubSerno(String subSerno);

    /**
     * 根据subSerno查询低风险额度授信信息
     * @param subSerno
     * @return
     */
    List<LmtAppSubPrd> selectLowBySubSerno(String subSerno);
    /**
     * @函数名称:deleteBySubSerno
     * @函数描述:根据授信分项流水号关联删除授信分项产品
     * @参数与返回说明:subSerno 授信分项流水号
     * @算法描述:
     */
    int deleteBySubSerno(String subSerno);

    /**
     * 根据单一客户授信申请流水号获取授信分项品种数据
     * @param serno
     * @return
     */
    List<LmtAppSubPrd> queryAllLmtAppSubPrdBySerno(String serno);

    /**
     * 根据集团授信申请流水号获取所有的授信分项品种数据
     * @param grpSerno
     * @return
     */
    List<LmtAppSubPrd> queryAllLmtAppSubPrdByGrpSerno(String grpSerno);

    /**
     * @方法名称: queryLmtAppSubPrdByParams
     * @方法描述: 根据查询条件查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    int selectCountsByParams(Map queryMap);

    /**
     * @方法名称: queryLmtAppSubPrdByParams
     * @方法描述: 根据查询条件查询保函授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: cainingbo
     */
    int selectNewCountsByParams(String subSerno);

    /**
     * 根据集团申请流水号获取授信品种分项数据
     * @param grpSerno
     * @return
     */
    ArrayList<LmtAppSubPrd> queryLmtApprSubByGrpSerno(String grpSerno);

    /**
     * 根据业务流水号查询授信分项
     * @param serno
     * @return
     */
    List<LmtAppSubPrd> selectSubPrdBySerno(String serno);

    /**
     * 通过入参查询授信申请信息分项中的产品
     *
     * @param queryMap
     * @return
     */
    List<LmtAppSubPrd> selectPrdByParams(Map queryMap);

    /**
     * 通过入参查询授信申请信息分项产品明细
     *
     * @param queryMap
     * @return
     */
    List<LmtAppSubPrd> selectLmtAppSubPrdByParams(Map queryMap);

    /**
     * 根据分项流水号查询分项产品明细
     * @param queryMap
     * @return
     */
    List<LmtAppSubPrd> selectBySubSernos(Map queryMap);

}
