package cn.com.yusys.yusp.service.server.xdtz0006;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0006.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.web.server.xdtz0006.BizXdtz0006Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类:根据证件号查询借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0006Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0006Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 根据证件号查询借据信息
     * @param queryMap
     * @return
     */
    @Transactional
    public java.util.List<List> getWjqList(Map queryMap) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(queryMap));
        java.util.List<List> result = accLoanMapper.getWjqList(queryMap);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(result));
        return result;
    }
}
