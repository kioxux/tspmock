/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.BusinessInformationMapper;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BusinessInformationService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-10 16:59:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BusinessInformationService {

    @Autowired
    private BusinessInformationMapper businessInformationMapper;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private PvpAccpAppService pvpAccpAppService;
    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public BusinessInformation selectByPrimaryKey(String bizId, String bizType) {
        return businessInformationMapper.selectByPrimaryKey(bizId, bizType);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<BusinessInformation> selectAll(QueryModel model) {
        List<BusinessInformation> records = (List<BusinessInformation>) businessInformationMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<BusinessInformation> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BusinessInformation> list = businessInformationMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(BusinessInformation record) {
        return businessInformationMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(BusinessInformation record) {
        return businessInformationMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(BusinessInformation record) {
        return businessInformationMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(BusinessInformation record) {
        return businessInformationMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String bizId, String bizType) {
        return businessInformationMapper.deleteByPrimaryKey(bizId, bizType);
    }

    /**
     * @方法名称: updateComplete
     * @方法描述: 对资料齐全进行校验
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateComplete(Map<String, String> params) {
        System.out.println("对资料齐全进行校验++++++++++++++++++++");
        // 1、获取当前登录人信息
        User userInfo = SessionUtils.getUserInformation();
        System.out.println(userInfo);

        Date date =new Date();
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        String openday = dft.format(date);
        String serno = params.get("serno");//流水号
        String bizType = params.get("bizType");//类型
        String isinforcomplete = params.get("isinforcomplete");//是否资料齐全
        String issignet = params.get("issignet");//是否用印
        String currentUserId  = userInfo.getUserId();
        String currentOrgId = userInfo.getOrg().getId();
        BusinessInformation businessInformation1 = this.selectByPrimaryKey(serno,bizType);
        int count = 0;
        if(businessInformation1 != null){
            businessInformation1.setComplete(isinforcomplete);
            businessInformation1.setSignet(issignet);
            businessInformation1.setUpdateTime(new Date());//修改时间
            businessInformation1.setUpdId(currentUserId);//最近更新人
            businessInformation1.setUpdBrId(currentOrgId);//最近更新机构
            businessInformation1.setUpdDate(openday);//最近更新日期
            count = businessInformationMapper.updateByPrimaryKey(businessInformation1);
        }else{
            BusinessInformation businessInformation = new BusinessInformation();
            businessInformation.setBizId(serno);
            businessInformation.setComplete(isinforcomplete);
            businessInformation.setSignet(issignet);
            businessInformation.setBizType(bizType);
            businessInformation.setInputId(currentUserId);//登记人
            businessInformation.setInputBrId(currentOrgId);//登记机构
            businessInformation.setInputDate(openday);//登记日期
            businessInformation.setUpdId(currentUserId);//最近更新人
            businessInformation.setUpdBrId(currentOrgId);//最近更新机构
            businessInformation.setUpdDate(openday);//最近更新日期
            businessInformation.setCreateTime(new Date());//创建时间
            businessInformation.setUpdateTime(new Date());//修改时间
            count = businessInformationMapper.insertSelective(businessInformation);
        }
        // 如果是出账（贷款出账、委托贷款出账、银承出账）流程，更新出账申请表中的资料全否字段
        if(CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType)){
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(serno);
            pvpLoanApp.setIsMaterComp(isinforcomplete);
            pvpLoanAppService.updateSelective(pvpLoanApp);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX012.equals(bizType)){
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            pvpAccpApp.setFileSufFlag(isinforcomplete);
            pvpAccpAppService.updateSelective(pvpAccpApp);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)){
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            pvpEntrustLoanApp.setFileSufFlag(isinforcomplete);
            pvpEntrustLoanAppService.updateSelective(pvpEntrustLoanApp);
        }else {
        }
        System.out.println("count的值是++++++++++"+count);
        return  count;
    }
    /**
     * @方法名称: updatesignet
     * @方法描述: 是否用印
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updatesignet(Map<String, String> params) {
        System.out.println("是否用印++++++++++++++++++++");
        // 1、获取当前登录人信息
        User userInfo = SessionUtils.getUserInformation();
        System.out.println(userInfo);

        Date date =new Date();
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        String openday = dft.format(date);
        String serno = params.get("serno");//流水号
        String bizType = params.get("bizType");//类型
        String  issignet = params.get("issignet");//是否用印
        String currentUserId  = userInfo.getUserId();
        String currentOrgId = userInfo.getOrg().getId();
        BusinessInformation businessInformation1 = this.selectByPrimaryKey(serno,bizType);
        int count = 0;
        if(businessInformation1 != null){
            businessInformation1.setSignet(issignet);
            businessInformation1.setUpdateTime(new Date());//修改时间
            businessInformation1.setUpdId(currentUserId);//最近更新人
            businessInformation1.setUpdBrId(currentOrgId);//最近更新机构
            businessInformation1.setUpdDate(openday);//最近更新日期
            count = businessInformationMapper.updateByPrimaryKey(businessInformation1);
        }else{
            BusinessInformation businessInformation = new BusinessInformation();
            businessInformation.setBizId(serno);
            businessInformation.setBizType(bizType);
            businessInformation.setSignet(issignet);
            businessInformation.setInputId(currentUserId);//登记人
            businessInformation.setInputBrId(currentOrgId);//登记机构
            businessInformation.setInputDate(openday);//登记日期
            businessInformation.setUpdId(currentUserId);//最近更新人
            businessInformation.setUpdBrId(currentOrgId);//最近更新机构
            businessInformation.setUpdDate(openday);//最近更新日期
            businessInformation.setCreateTime(new Date());//创建时间
            businessInformation.setUpdateTime(new Date());//修改时间
            count = businessInformationMapper.insertSelective(businessInformation);
        }
        return  count;
    }
    
    /**
     * 根据业务流水号获取资料岗保存信息
     * @author jijian_yx
     * @date 2021/8/28 22:08
     **/
    public BusinessInformation selectByBizId(String bizId) {
        return businessInformationMapper.selectByBizId(bizId);
    }
}
