package cn.com.yusys.yusp.service.client.bsp.fxyjxt.hhmdkh;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2FxyjxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/22 15:40
 * @desc 查询黑名单信息
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class HhmdkhService {
    private static final Logger logger = LoggerFactory.getLogger(HhmdkhService.class);
    // 1）注入：封装的接口类:风险预警系统
    @Autowired
    private Dscms2FxyjxtClientService dscms2FxyjxtClientService;

    /**
     * @param hhmdkhReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.rircp
     * @author hubp
     * @date 2021/5/20 14:42
     * @version 1.0.0
     * @desc    查询黑名单信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public HhmdkhRespDto hhmdkh(HhmdkhReqDto hhmdkhReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value, JSON.toJSONString(hhmdkhReqDto));
        ResultDto<HhmdkhRespDto> hhmdkhResultDto = dscms2FxyjxtClientService.hhmdkh(hhmdkhReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value, JSON.toJSONString(hhmdkhResultDto));
        String hhmdkhCode = Optional.ofNullable(hhmdkhResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String hhmdkhMeesage = Optional.ofNullable(hhmdkhResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        HhmdkhRespDto hhmdkhRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, hhmdkhResultDto.getCode())) {
            //  获取相关的值并解析
            hhmdkhRespDto = hhmdkhResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(hhmdkhCode, hhmdkhMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value);
        return hhmdkhRespDto;
    }
}
