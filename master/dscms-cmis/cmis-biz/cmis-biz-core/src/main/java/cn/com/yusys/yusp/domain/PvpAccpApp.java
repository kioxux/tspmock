/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpApp
 * @类描述: pvp_accp_app数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 16:02:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_accp_app")
public class PvpAccpApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = false, length = 40)
	private String pvpSerno;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 10)
	private String cusType;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 出账模式 **/
	@Column(name = "PVP_MODE", unique = false, nullable = true, length = 5)
	private String pvpMode;
	
	/** 是否电子票据 **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;
	
	/** 合同影像是否审核 **/
	@Column(name = "IS_CONT_IMAGE_AUDIT", unique = false, nullable = true, length = 5)
	private String isContImageAudit;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 合同币种 **/
	@Column(name = "CONT_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String contCurType;
	
	/** 本合同项下最高可用金额 **/
	@Column(name = "CONT_HIGH_DISB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighDisb;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 申请汇票数 **/
	@Column(name = "APPLY_PORDER", unique = false, nullable = true, length = 10)
	private Integer applyPorder;
	
	/** 票据签订日期 **/
	@Column(name = "DRAFT_SIGN_DATE", unique = false, nullable = true, length = 10)
	private String draftSignDate;

	/** 是否他行代签 **/
	@Column(name = "IS_OTHER_SIGN", unique = false, nullable = true, length = 5)
	private String isOtherSign;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 手续费率 **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;
	
	/** 手续费金额 **/
	@Column(name = "CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgAmt;
	
	/** 合同类型 **/
	@Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
	private String contType;
	
	/** 手续费类型 **/
	@Column(name = "CHRG_TYPE", unique = false, nullable = true, length = 5)
	private String chrgType;
	
	/** 退票计息方式 **/
	@Column(name = "RETURN_DRAFT_INTEREST_TYPE", unique = false, nullable = true, length = 10)
	private String returnDraftInterestType;
	
	/** 逾期执行款年利率 **/
	@Column(name = "OVERDUE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRate;
	
	/** 承兑行类型 **/
	@Column(name = "AORG_TYPE", unique = false, nullable = true, length = 5)
	private String aorgType;
	
	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;
	
	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;
	
	/** 出票人开户行账号 **/
	@Column(name = "DAORG_NO", unique = false, nullable = true, length = 40)
	private String daorgNo;
	
	/** 出票人开户户名 **/
	@Column(name = "DAORG_NAME", unique = false, nullable = true, length = 80)
	private String daorgName;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 签发机构编号 **/
	@Column(name = "ISSUED_ORG_NO", unique = false, nullable = true, length = 40)
	private String issuedOrgNo;
	
	/** 签发机构名称 **/
	@Column(name = "ISSUED_ORG_NAME", unique = false, nullable = true, length = 80)
	private String issuedOrgName;
	
	/** 兑付机构编号 **/
	@Column(name = "PAY_ORG_NO", unique = false, nullable = true, length = 40)
	private String payOrgNo;
	
	/** 兑付机构名称 **/
	@Column(name = "PAY_ORG_NAME", unique = false, nullable = true, length = 80)
	private String payOrgName;
	
	/** 资料全否 **/
	@Column(name = "FILE_SUF_FLAG", unique = false, nullable = true, length = 5)
	private String fileSufFlag;
	
	/** 网银影像流水号 **/
	@Column(name = "WY_IMAGE_SERNO", unique = false, nullable = true, length = 40)
	private String wyImageSerno;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 是否省心E票(STD_ZB_YES_NO) **/
	@Column(name = "IS_SXEP", unique = false, nullable = true, length = 5)
	private String isSxep;

	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 是否资产池(STD_ZB_YES_NO) **/
	@Column(name = "IS_POOL", unique = false, nullable = true, length = 5)
	private String isPool;

	private PvpAuthorize pvpAuthorize;

	public PvpAuthorize getPvpAuthorize() {
		return pvpAuthorize;
	}

	public void setPvpAuthorize(PvpAuthorize pvpAuthorize) {
		this.pvpAuthorize = pvpAuthorize;
	}
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param pvpMode
	 */
	public void setPvpMode(String pvpMode) {
		this.pvpMode = pvpMode;
	}
	
    /**
     * @return pvpMode
     */
	public String getPvpMode() {
		return this.pvpMode;
	}
	
	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}
	
    /**
     * @return isEDrft
     */
	public String getIsEDrft() {
		return this.isEDrft;
	}
	
	/**
	 * @param isContImageAudit
	 */
	public void setIsContImageAudit(String isContImageAudit) {
		this.isContImageAudit = isContImageAudit;
	}
	
    /**
     * @return isContImageAudit
     */
	public String getIsContImageAudit() {
		return this.isContImageAudit;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contCurType
	 */
	public void setContCurType(String contCurType) {
		this.contCurType = contCurType;
	}
	
    /**
     * @return contCurType
     */
	public String getContCurType() {
		return this.contCurType;
	}
	
	/**
	 * @param contHighDisb
	 */
	public void setContHighDisb(java.math.BigDecimal contHighDisb) {
		this.contHighDisb = contHighDisb;
	}
	
    /**
     * @return contHighDisb
     */
	public java.math.BigDecimal getContHighDisb() {
		return this.contHighDisb;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param applyPorder
	 */
	public void setApplyPorder(Integer applyPorder) {
		this.applyPorder = applyPorder;
	}
	
    /**
     * @return applyPorder
     */
	public Integer getApplyPorder() {
		return this.applyPorder;
	}
	
	/**
	 * @param draftSignDate
	 */
	public void setDraftSignDate(String draftSignDate) {
		this.draftSignDate = draftSignDate;
	}
	
    /**
     * @return draftSignDate
     */
	public String getDraftSignDate() {
		return this.draftSignDate;
	}

	/**
	 * @return isOtherSign
	 */
	public String getIsOtherSign() {
		return this.isOtherSign;
	}

	/**
	 * @param isOtherSign
	 */
	public void setIsOtherSign(String isOtherSign) {
		this.isOtherSign = isOtherSign;
	}

	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}
	
    /**
     * @return chrgRate
     */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}
	
	/**
	 * @param chrgAmt
	 */
	public void setChrgAmt(java.math.BigDecimal chrgAmt) {
		this.chrgAmt = chrgAmt;
	}
	
    /**
     * @return chrgAmt
     */
	public java.math.BigDecimal getChrgAmt() {
		return this.chrgAmt;
	}
	
	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}
	
    /**
     * @return contType
     */
	public String getContType() {
		return this.contType;
	}
	
	/**
	 * @param chrgType
	 */
	public void setChrgType(String chrgType) {
		this.chrgType = chrgType;
	}
	
    /**
     * @return chrgType
     */
	public String getChrgType() {
		return this.chrgType;
	}
	
	/**
	 * @param returnDraftInterestType
	 */
	public void setReturnDraftInterestType(String returnDraftInterestType) {
		this.returnDraftInterestType = returnDraftInterestType;
	}
	
    /**
     * @return returnDraftInterestType
     */
	public String getReturnDraftInterestType() {
		return this.returnDraftInterestType;
	}
	
	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}
	
    /**
     * @return overdueRate
     */
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}
	
	/**
	 * @param aorgType
	 */
	public void setAorgType(String aorgType) {
		this.aorgType = aorgType;
	}
	
    /**
     * @return aorgType
     */
	public String getAorgType() {
		return this.aorgType;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}
	
    /**
     * @return aorgNo
     */
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}
	
    /**
     * @return aorgName
     */
	public String getAorgName() {
		return this.aorgName;
	}
	
	/**
	 * @param daorgNo
	 */
	public void setDaorgNo(String daorgNo) {
		this.daorgNo = daorgNo;
	}
	
    /**
     * @return daorgNo
     */
	public String getDaorgNo() {
		return this.daorgNo;
	}
	
	/**
	 * @param daorgName
	 */
	public void setDaorgName(String daorgName) {
		this.daorgName = daorgName;
	}
	
    /**
     * @return daorgName
     */
	public String getDaorgName() {
		return this.daorgName;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param issuedOrgNo
	 */
	public void setIssuedOrgNo(String issuedOrgNo) {
		this.issuedOrgNo = issuedOrgNo;
	}
	
    /**
     * @return issuedOrgNo
     */
	public String getIssuedOrgNo() {
		return this.issuedOrgNo;
	}
	
	/**
	 * @param issuedOrgName
	 */
	public void setIssuedOrgName(String issuedOrgName) {
		this.issuedOrgName = issuedOrgName;
	}
	
    /**
     * @return issuedOrgName
     */
	public String getIssuedOrgName() {
		return this.issuedOrgName;
	}
	
	/**
	 * @param payOrgNo
	 */
	public void setPayOrgNo(String payOrgNo) {
		this.payOrgNo = payOrgNo;
	}
	
    /**
     * @return payOrgNo
     */
	public String getPayOrgNo() {
		return this.payOrgNo;
	}
	
	/**
	 * @param payOrgName
	 */
	public void setPayOrgName(String payOrgName) {
		this.payOrgName = payOrgName;
	}
	
    /**
     * @return payOrgName
     */
	public String getPayOrgName() {
		return this.payOrgName;
	}
	
	/**
	 * @param fileSufFlag
	 */
	public void setFileSufFlag(String fileSufFlag) {
		this.fileSufFlag = fileSufFlag;
	}
	
    /**
     * @return fileSufFlag
     */
	public String getFileSufFlag() {
		return this.fileSufFlag;
	}
	
	/**
	 * @param wyImageSerno
	 */
	public void setWyImageSerno(String wyImageSerno) {
		this.wyImageSerno = wyImageSerno;
	}
	
    /**
     * @return wyImageSerno
     */
	public String getWyImageSerno() {
		return this.wyImageSerno;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param isSxep
	 */
	public void setIsSxep(String isSxep) {
		this.isSxep = isSxep;
	}
	
    /**
     * @return isSxep
     */
	public String getIsSxep() {
		return this.isSxep;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getIsPool() {
		return isPool;
	}

	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}
}