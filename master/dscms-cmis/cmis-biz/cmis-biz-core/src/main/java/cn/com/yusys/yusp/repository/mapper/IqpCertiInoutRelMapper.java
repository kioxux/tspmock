/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpCertiInoutRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 10:21:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpCertiInoutRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    IqpCertiInoutRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<IqpCertiInoutRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(IqpCertiInoutRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(IqpCertiInoutRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(IqpCertiInoutRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(IqpCertiInoutRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: selectBySernoKey
     * @方法描述: 根据押品出入库申请流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
	  List<IqpCertiInoutRel> selectBySernoKey(String serno);

    /**
     * 获取权证与权证出入库申请的关系表中的押品统一编号
     *
     * @return
     */
    List<String> getIqpCertiInoutRelInGuarNo(Map map);
    
    /**
     * @方法名称: querySelectBySerno
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpCertiInoutRel> querySelectBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectByGuarNo
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpCertiInoutRel> selectByGuarNo(String guarNo);

}