/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareInfo
 * @类描述: lmt_guare_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-19 17:22:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_guare_info")
public class LmtGuareInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 云评估编号 **/
	@Column(name = "CLOUD_EVAL_NO", unique = false, nullable = true, length = 40)
	private String cloudEvalNo;
	
	/** 抵押物类型 **/
	@Column(name = "PAWN_TYPE", unique = false, nullable = true, length = 5)
	private String pawnType;
	
	/** 所有权人 **/
	@Column(name = "OWNER", unique = false, nullable = true, length = 100)
	private String owner;
	
	/** 所有权人客户编号 **/
	@Column(name = "OWNER_CUS_NO", unique = false, nullable = true, length = 40)
	private String ownerCusNo;
	
	/** 所有权人证件号码 **/
	@Column(name = "OWNER_CERT_NO", unique = false, nullable = true, length = 100)
	private String ownerCertNo;
	
	/** 土地性质 **/
	@Column(name = "LAND_CHA", unique = false, nullable = true, length = 100)
	private String landCha;
	
	/** 房屋性质 **/
	@Column(name = "HOUSE_CHA", unique = false, nullable = true, length = 5)
	private String houseCha;
	
	/** 地址 **/
	@Column(name = "ADDR", unique = false, nullable = true, length = 500)
	private String addr;
	
	/** 面积 **/
	@Column(name = "SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal squ;
	
	/** 使用情况 **/
	@Column(name = "UTIL_CASE", unique = false, nullable = true, length = 4)
	private String utilCase;
	
	/** 评估金额 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 一低金额 **/
	@Column(name = "FIRST_PLD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstPldAmt;
	
	/** 一低余额 **/
	@Column(name = "FIRST_PLD_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal firstPldBal;
	
	/** 该抵押物项下贷款总金额 **/
	@Column(name = "PAWN_LOAN_TOTL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pawnLoanTotlAmt;
	
	/** 抵质押率 **/
	@Column(name = "PLDIMN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pldimnRate;
	
	/** 是否一级学区房 **/
	@Column(name = "IS_LEVEL1_SDR", unique = false, nullable = true, length = 1)
	private String isLevel1Sdr;
	
	/** 学区房地址 **/
	@Column(name = "SDR_ADDR", unique = false, nullable = true, length = 500)
	private String sdrAddr;
	
	/** 学区房名称 **/
	@Column(name = "SDR_NAME", unique = false, nullable = true, length = 100)
	private String sdrName;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cloudEvalNo
	 */
	public void setCloudEvalNo(String cloudEvalNo) {
		this.cloudEvalNo = cloudEvalNo;
	}
	
    /**
     * @return cloudEvalNo
     */
	public String getCloudEvalNo() {
		return this.cloudEvalNo;
	}
	
	/**
	 * @param pawnType
	 */
	public void setPawnType(String pawnType) {
		this.pawnType = pawnType;
	}
	
    /**
     * @return pawnType
     */
	public String getPawnType() {
		return this.pawnType;
	}
	
	/**
	 * @param owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
    /**
     * @return owner
     */
	public String getOwner() {
		return this.owner;
	}
	
	/**
	 * @param ownerCusNo
	 */
	public void setOwnerCusNo(String ownerCusNo) {
		this.ownerCusNo = ownerCusNo;
	}
	
    /**
     * @return ownerCusNo
     */
	public String getOwnerCusNo() {
		return this.ownerCusNo;
	}
	
	/**
	 * @param ownerCertNo
	 */
	public void setOwnerCertNo(String ownerCertNo) {
		this.ownerCertNo = ownerCertNo;
	}
	
    /**
     * @return ownerCertNo
     */
	public String getOwnerCertNo() {
		return this.ownerCertNo;
	}
	
	/**
	 * @param landCha
	 */
	public void setLandCha(String landCha) {
		this.landCha = landCha;
	}
	
    /**
     * @return landCha
     */
	public String getLandCha() {
		return this.landCha;
	}
	
	/**
	 * @param houseCha
	 */
	public void setHouseCha(String houseCha) {
		this.houseCha = houseCha;
	}
	
    /**
     * @return houseCha
     */
	public String getHouseCha() {
		return this.houseCha;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
    /**
     * @return addr
     */
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return squ
     */
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param utilCase
	 */
	public void setUtilCase(String utilCase) {
		this.utilCase = utilCase;
	}
	
    /**
     * @return utilCase
     */
	public String getUtilCase() {
		return this.utilCase;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param firstPldAmt
	 */
	public void setFirstPldAmt(java.math.BigDecimal firstPldAmt) {
		this.firstPldAmt = firstPldAmt;
	}
	
    /**
     * @return firstPldAmt
     */
	public java.math.BigDecimal getFirstPldAmt() {
		return this.firstPldAmt;
	}
	
	/**
	 * @param firstPldBal
	 */
	public void setFirstPldBal(java.math.BigDecimal firstPldBal) {
		this.firstPldBal = firstPldBal;
	}
	
    /**
     * @return firstPldBal
     */
	public java.math.BigDecimal getFirstPldBal() {
		return this.firstPldBal;
	}
	
	/**
	 * @param pawnLoanTotlAmt
	 */
	public void setPawnLoanTotlAmt(java.math.BigDecimal pawnLoanTotlAmt) {
		this.pawnLoanTotlAmt = pawnLoanTotlAmt;
	}
	
    /**
     * @return pawnLoanTotlAmt
     */
	public java.math.BigDecimal getPawnLoanTotlAmt() {
		return this.pawnLoanTotlAmt;
	}
	
	/**
	 * @param pldimnRate
	 */
	public void setPldimnRate(java.math.BigDecimal pldimnRate) {
		this.pldimnRate = pldimnRate;
	}
	
    /**
     * @return pldimnRate
     */
	public java.math.BigDecimal getPldimnRate() {
		return this.pldimnRate;
	}
	
	/**
	 * @param isLevel1Sdr
	 */
	public void setIsLevel1Sdr(String isLevel1Sdr) {
		this.isLevel1Sdr = isLevel1Sdr;
	}
	
    /**
     * @return isLevel1Sdr
     */
	public String getIsLevel1Sdr() {
		return this.isLevel1Sdr;
	}
	
	/**
	 * @param sdrAddr
	 */
	public void setSdrAddr(String sdrAddr) {
		this.sdrAddr = sdrAddr;
	}
	
    /**
     * @return sdrAddr
     */
	public String getSdrAddr() {
		return this.sdrAddr;
	}
	
	/**
	 * @param sdrName
	 */
	public void setSdrName(String sdrName) {
		this.sdrName = sdrName;
	}
	
    /**
     * @return sdrName
     */
	public String getSdrName() {
		return this.sdrName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}