/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccEntrustLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpPrePaymentMapper;
import cn.com.yusys.yusp.workFlow.service.BGYW11BizService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpPrePaymentService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-24 10:34:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpPrePaymentService {

    @Autowired
    private IqpPrePaymentMapper iqpPrePaymentMapper;

    @Autowired
    private BGYW11BizService bGYW11BizService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpPrePayment selectByPrimaryKey(String iqpSerno) {
        return iqpPrePaymentMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpPrePayment> selectAll(QueryModel model) {
        List<IqpPrePayment> records = (List<IqpPrePayment>) iqpPrePaymentMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpPrePayment> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpPrePayment> list = iqpPrePaymentMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpPrePayment record) {
        return iqpPrePaymentMapper.insert(record);
    }

    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(IqpPrePayment iqpPrePayment) {
        QueryModel model = new QueryModel();
        model.addCondition("billNo", iqpPrePayment.getBillNo());
        model.addCondition("apply","Y");
        List<IqpPrePayment> list = this.selectByModel(model);
        if(list.size()>0){
            throw BizException.error(null, "999999","\"该借据存在在途主动还款申请，请勿重复发起！\"" + "保存失败！");
        }
        // 调用接口查询是否停息
        Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto();
        ln3100ReqDto.setDkjiejuh(iqpPrePayment.getBillNo());
        ResultDto<Ln3100RespDto> ln3100ResultDto  = dscms2CoreLnClientService.ln3100(ln3100ReqDto);
        String ln3100Code = Optional.ofNullable(ln3100ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3100Meesage = Optional.ofNullable(ln3100ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3100RespDto ln3100RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3100Code)) {
            //  获取相关的值并解析
            ln3100RespDto = ln3100ResultDto.getData();
            // 获取停息标志
            //jixibzhi  计息标志 0 不计息 1 计息
            String jixibzhi = ln3100RespDto.getJixibzhi();
            if("0".equals(jixibzhi)){
                throw BizException.error(null, "999999","\"该借据已停息,请先恢复计息");
            }
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3100Code, ln3100Meesage);
        }

        QueryModel model1 = new QueryModel();
        model1.addCondition("billNo", iqpPrePayment.getBillNo());
        List<AccLoan> accLoanlist = accLoanMapper.selectByModel(model1);

        QueryModel model2 = new QueryModel();
        model2.addCondition("billNo", iqpPrePayment.getBillNo());
        List<AccEntrustLoan> accEntrustLoanlist = accEntrustLoanMapper.selectByModel(model2);
        if (accLoanlist.isEmpty() && accEntrustLoanlist.isEmpty()) {
            //借据号不存在
            throw BizException.error(null, EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
        }
        if (!accLoanlist.isEmpty()) {
            AccLoan accLoan = accLoanlist.get(0);
            //TODO 存在性校验
            iqpPrePayment.setBillNo(accLoan.getBillNo());                                //借据编号
            iqpPrePayment.setCusId(accLoan.getCusId());                                  //客户编号
            iqpPrePayment.setCusName(accLoan.getCusName());                              //客户名称
            iqpPrePayment.setContNo(accLoan.getContNo());                                //合同编号
            iqpPrePayment.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
            iqpPrePayment.setLoanBalance(accLoan.getLoanBalance());                      //贷款余额
            iqpPrePayment.setPrdId(accLoan.getPrdId());                                  //产品ID
            iqpPrePayment.setPrdName(accLoan.getPrdName());                              //产品名称
            iqpPrePayment.setLoanStartDate(accLoan.getLoanStartDate());                  //贷款起始日
            iqpPrePayment.setLoanEndDate(accLoan.getLoanEndDate());                      //贷款到期日
//        iqpPrePayment.setLoanAppChnl(accLoan.getBelgLine());                   //申请渠道 TODO
            iqpPrePayment.setAccStatus(accLoan.getAccStatus());               //台账状态
            iqpPrePayment.setApproveStatus("000");
            iqpPrePayment.setAppChnl("G"); // 还款渠道  G-PC端
            iqpPrePayment.setManagerId(accLoan.getManagerId());// 主管客户经理
            iqpPrePayment.setManagerBrId(accLoan.getManagerBrId());// 主管机构
            User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
            iqpPrePayment.setInputId(userInfo.getLoginCode()); // 当前用户号
            iqpPrePayment.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
            iqpPrePayment.setInputDate(DateUtils.getCurrDateStr());
            iqpPrePayment.setCreateTime(DateUtils.getCurrTimestamp());
            iqpPrePayment.setUpdateTime(iqpPrePayment.getCreateTime());
            iqpPrePayment.setUpdDate(iqpPrePayment.getInputDate());
            iqpPrePayment.setUpdId(iqpPrePayment.getInputId());
            iqpPrePayment.setUpdBrId(iqpPrePayment.getInputBrId());
            return iqpPrePaymentMapper.insertSelective(iqpPrePayment);
        }else{
            AccEntrustLoan accLoan = accEntrustLoanlist.get(0);

            iqpPrePayment.setBillNo(accLoan.getBillNo());                                //借据编号
            iqpPrePayment.setCusId(accLoan.getCusId());                                  //客户编号
            iqpPrePayment.setCusName(accLoan.getCusName());                              //客户名称
            iqpPrePayment.setContNo(accLoan.getContNo());                                //合同编号
            iqpPrePayment.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
            iqpPrePayment.setLoanBalance(accLoan.getLoanBalance());                      //贷款余额
            iqpPrePayment.setPrdId(accLoan.getPrdId());                                  //产品ID
            iqpPrePayment.setPrdName(accLoan.getPrdName());                              //产品名称
            iqpPrePayment.setLoanStartDate(accLoan.getLoanStartDate());                  //贷款起始日
            iqpPrePayment.setLoanEndDate(accLoan.getLoanEndDate());                      //贷款到期日
//        iqpPrePayment.setLoanAppChnl(accLoan.getBelgLine());                   //申请渠道 TODO
            iqpPrePayment.setAccStatus(accLoan.getAccStatus());               //台账状态
            iqpPrePayment.setApproveStatus("000");
            iqpPrePayment.setAppChnl("G"); // 还款渠道  G-PC端
            iqpPrePayment.setManagerId(accLoan.getManagerId());// 主管客户经理
            iqpPrePayment.setManagerBrId(accLoan.getManagerBrId());// 主管机构
            User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
            iqpPrePayment.setInputId(userInfo.getLoginCode()); // 当前用户号
            iqpPrePayment.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
            iqpPrePayment.setInputDate(DateUtils.getCurrDateStr());
            iqpPrePayment.setCreateTime(DateUtils.getCurrTimestamp());
            iqpPrePayment.setUpdateTime(iqpPrePayment.getCreateTime());
            iqpPrePayment.setUpdDate(iqpPrePayment.getInputDate());
            iqpPrePayment.setUpdId(iqpPrePayment.getInputId());
            iqpPrePayment.setUpdBrId(iqpPrePayment.getInputBrId());
            return iqpPrePaymentMapper.insertSelective(iqpPrePayment);
        }
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpPrePayment record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr());
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return iqpPrePaymentMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpPrePayment record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr());
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return iqpPrePaymentMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpPrePaymentMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpPrePaymentMapper.deleteByIds(ids);
    }

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param iqpPrePayment
     * @return void
     * @author tangxun
     * @date 2021/5/26 14:43
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void queryLn3111(IqpPrePayment iqpPrePayment) {
        // 判断是否是核销贷款
        // 台账状态 5	已核销
        String  accStatus = iqpPrePayment.getAccStatus();
        // 还款方式 03	灵活还款
        String repayModeDesc = iqpPrePayment.getRepayModeDesc();
        if("5".equals(accStatus) || "03".equals(repayModeDesc)){
            // 调用 ln3100 查出来不是有各欠息的明细
            Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto();
            ln3100ReqDto.setDkjiejuh(iqpPrePayment.getBillNo());
            ResultDto<Ln3100RespDto> ln3100ResultDto  = dscms2CoreLnClientService.ln3100(ln3100ReqDto);

            String ln3100Code = Optional.ofNullable(ln3100ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3100Meesage = Optional.ofNullable(ln3100ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Ln3100RespDto l = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3100Code)) {
                //  获取相关的值并解析
                 l = ln3100ResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, ln3100Code, ln3100Meesage);
            }
            iqpPrePayment.setZcbjAmt(l.getZhchbjin());     //1-归还正常本金
            iqpPrePayment.setYqbjAmt(l.getYuqibjin());     //9-归还逾期本金
            iqpPrePayment.setDzbjAmt(l.getDzhibjin());     //2-归还呆滞本金
            iqpPrePayment.setDaizbjAmt(l.getDaizbjin());   //10-归还呆账本金
            iqpPrePayment.setYsyjlxXmt(l.getYsyjlixi());   //3-归还应收应计利息
            iqpPrePayment.setCsyjlxAmt(l.getCsyjlixi());   //11-归还催收应计利息
            iqpPrePayment.setYsqxAmt(l.getYsqianxi());     //4-归还应收欠息
            iqpPrePayment.setCsqxAmt(l.getCsqianxi());     //12-归还催收欠息
            iqpPrePayment.setYsyjfxAmt(l.getYsyjfaxi());   //5-归还应收应计罚息
            iqpPrePayment.setCsyjfxAmt(l.getCsyjfaxi());   //13-归还催收应计罚息
            iqpPrePayment.setYsfxAmt(l.getYshofaxi());     //6-归还应收罚息
            iqpPrePayment.setCsfxAmt(l.getCshofaxi());     //14-归还催收罚息
            iqpPrePayment.setYjfxAmt(l.getYingjifx());     //7-归还应计复息
            iqpPrePayment.setFxAmt(l.getFuxiiiii());       //15-归还复息
            iqpPrePayment.setYsfjAmt(l.getYingshfj());     //8-归还应收罚金
            iqpPrePayment.setTotalBjAmt(iqpPrePayment.getZcbjAmt().add(iqpPrePayment.getYqbjAmt())); //本金
            iqpPrePayment.setTotalLxAmt(iqpPrePayment.getYsyjlxXmt()
                    .add(iqpPrePayment.getYsyjfxAmt())
                    .add(iqpPrePayment.getCsyjlxAmt())
                    .add(iqpPrePayment.getCsqxAmt()));       //利息
            iqpPrePayment.setTotalFxAmt(iqpPrePayment.getYsyjfxAmt()
                    .add(iqpPrePayment.getYsfxAmt())
                    .add(iqpPrePayment.getCsyjfxAmt())
                    .add(iqpPrePayment.getCsfxAmt()));     //罚息
            iqpPrePayment.setTotalFuxAmt(iqpPrePayment.getYjfxAmt()
                    .add(iqpPrePayment.getFxAmt()));      //复息
            iqpPrePayment.setTotalHxbjAmt(l.getHexiaobj());     //核销本金
            iqpPrePayment.setTotalHxlxAmt(l.getHexiaolx());     //核销利息
            iqpPrePayment.setTotalTqlxAmt(iqpPrePayment.getTotalLxAmt()
                    .add(iqpPrePayment.getTotalFxAmt())
                    .add(iqpPrePayment.getFxAmt()));     //拖欠利息总额

        } else {
            Ln3111ReqDto ln3111ReqDto = new Ln3111ReqDto();
            ln3111ReqDto.setDkzhangh("");//贷款账号
            ln3111ReqDto.setDkjiejuh(iqpPrePayment.getBillNo());//贷款借据号
            ln3111ReqDto.setHetongbh(iqpPrePayment.getContNo());//合同编号
            ln3111ReqDto.setKehuhaoo(iqpPrePayment.getCusId());//客户号
            ln3111ReqDto.setKehuzwmc(iqpPrePayment.getCusName());//客户名
//        ln3111ReqDto.setHuobdhao("01");//货币代号
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            ln3111ReqDto.setHuankriq(openday.replace("-",""));//还款日期

            ln3111ReqDto.setZhchbjin(iqpPrePayment.getZcbjAmt());//正常本金
//        ln3111ReqDto.setYingjilx();//应计利息
            ln3111ReqDto.setYuqibjin(iqpPrePayment.getYqbjAmt());//逾期本金
            ln3111ReqDto.setDzhibjin(iqpPrePayment.getDzbjAmt());//呆滞本金
            ln3111ReqDto.setDaizbjin(iqpPrePayment.getDaizbjAmt());//呆账本金
            ln3111ReqDto.setYsyjlixi(iqpPrePayment.getYsyjlxXmt());//应收应计利息
            ln3111ReqDto.setCsyjlixi(iqpPrePayment.getCsyjlxAmt());//催收应计利息
            ln3111ReqDto.setYsqianxi(iqpPrePayment.getYsqxAmt());//应收欠息
            ln3111ReqDto.setCsqianxi(iqpPrePayment.getCsqxAmt());//催收欠息
            ln3111ReqDto.setYsyjfaxi(iqpPrePayment.getYsyjfxAmt());//应收应计罚息
            ln3111ReqDto.setCsyjfaxi(iqpPrePayment.getCsyjfxAmt());//催收应计罚息
            ln3111ReqDto.setYshofaxi(iqpPrePayment.getYsfxAmt());//应收罚息
            ln3111ReqDto.setCshofaxi(iqpPrePayment.getCsfxAmt());//催收罚息
            ln3111ReqDto.setYingjifx(iqpPrePayment.getYjfxAmt());//应计复息
            ln3111ReqDto.setFuxiiiii(iqpPrePayment.getFxAmt());//复息
            ln3111ReqDto.setYingshfj(iqpPrePayment.getYsfjAmt());//应收罚金
//        ln3111ReqDto.setQiankzee("");//欠款总额
            //1--结清贷款
            //2--归还欠款
            //3--提前还款

        /*01	提前还款
        02	归还拖欠*/
            String repayMode = iqpPrePayment.getRepayMode();
            if("02".equals(repayMode)){ // 02	归还拖欠 对应核心2
                ln3111ReqDto.setHuankzle("2");//还款种类
            } else { // 01	提前还款 对应核心3
                ln3111ReqDto.setHuankzle("3");//还款种类
            }

            // 还款种类等于3--提前还款时
            if ("3".equals(ln3111ReqDto.getHuankzle())) {
             /*
              信贷码值:                                       核心
             01	提前归还本金及全部利息                      2--指定总额
             02	提前归还本金及本金对应利息                  1--指定本金
             03	灵活还款
             04	归还拖欠的本息                                 ""
             */
//             if ("02".equals(iqpPrePayment.getRepayModeDesc())) {
//                 ln3111ReqDto.setDktqhkzl("1");		// 提前还款种类
//             } else {
//                 ln3111ReqDto.setDktqhkzl("2");		// 提前还款种类
//             }
                // 提前还款试算时，页面上是还本金额
                ln3111ReqDto.setDktqhkzl("1");
                /*
                 * 信贷码值
                 * 01--提前归还本金及全部利息
                 * 02--提前归还本金及本金对应利息
                 * 03--灵活还款
                 * 04--归还拖欠的本息
                 *
                 * 核心码值
                 * 0--不还息
                 * 1--部分还息
                 * 2--全部还息
                 */
                if ("01".equals(iqpPrePayment.getRepayModeDesc())
                        || "03".equals(iqpPrePayment.getRepayModeDesc())) { // 提前还款全部利息
                    ln3111ReqDto.setTqhkhxfs("2");// 提前还款还息方式
                } else {
                    ln3111ReqDto.setTqhkhxfs("1");//提前还款还息方式
                }
            } else if("2".equals(ln3111ReqDto.getHuankzle())){ // 归还拖欠
                ln3111ReqDto.setDktqhkzl("2");  //  2 指定总额
                ln3111ReqDto.setTqhkhxfs("2");// 全部还息
            }
            // 还款金额
            ln3111ReqDto.setHuankjee("02".equals(repayMode) ? iqpPrePayment.getRepayAmt() : iqpPrePayment.getRepayPriAmt());

            ResultDto<Ln3111RespDto> ln3111ResultDto = dscms2CoreLnClientService.ln3111(ln3111ReqDto);
            String ln3110Code = Optional.ofNullable(ln3111ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String ln3110Meesage = Optional.ofNullable(ln3111ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Ln3111RespDto l = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3111ResultDto.getCode())) {
                //  获取相关的值并解析
                l = ln3111ResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, ln3110Code, ln3110Meesage);
            }

            iqpPrePayment.setZcbjAmt(l.getGuihzcbj());     //1-归还正常本金
            iqpPrePayment.setYqbjAmt(l.getGuihyqbj());     //9-归还逾期本金
            iqpPrePayment.setDzbjAmt(l.getGuihdzbj());     //2-归还呆滞本金
            iqpPrePayment.setDaizbjAmt(l.getGhdzhabj());   //10-归还呆账本金
            iqpPrePayment.setYsyjlxXmt(l.getGhysyjlx());   //3-归还应收应计利息
            iqpPrePayment.setCsyjlxAmt(l.getGhcsyjlx());   //11-归还催收应计利息
            iqpPrePayment.setYsqxAmt(l.getGhynshqx());     //4-归还应收欠息
            iqpPrePayment.setCsqxAmt(l.getGhcushqx());     //12-归还催收欠息
            iqpPrePayment.setYsyjfxAmt(l.getGhysyjfx());   //5-归还应收应计罚息
            iqpPrePayment.setCsyjfxAmt(l.getGhcsyjfx());   //13-归还催收应计罚息
            iqpPrePayment.setYsfxAmt(l.getGhynshfx());     //6-归还应收罚息
            iqpPrePayment.setCsfxAmt(l.getGhcushfx());     //14-归还催收罚息
            iqpPrePayment.setYjfxAmt(l.getGhyjfuxi());     //7-归还应计复息
            iqpPrePayment.setFxAmt(l.getGhfxfuxi());       //15-归还复息
            iqpPrePayment.setYsfjAmt(l.getGhfajinn());     //8-归还应收罚金

            iqpPrePayment.setTotalBjAmt(iqpPrePayment.getZcbjAmt().add(iqpPrePayment.getYqbjAmt())); //本金
            iqpPrePayment.setTotalLxAmt(iqpPrePayment.getYsyjlxXmt()
                    .add(iqpPrePayment.getYsyjfxAmt())
                    .add(iqpPrePayment.getCsyjlxAmt())
                    .add(iqpPrePayment.getCsqxAmt()));       //利息
            iqpPrePayment.setTotalFxAmt(iqpPrePayment.getYsyjfxAmt()
                    .add(iqpPrePayment.getYsfxAmt())
                    .add(iqpPrePayment.getCsyjfxAmt())
                    .add(iqpPrePayment.getCsfxAmt()));     //罚息
            iqpPrePayment.setTotalFuxAmt(iqpPrePayment.getYjfxAmt()
                    .add(iqpPrePayment.getFxAmt()));      //复息
            iqpPrePayment.setTotalHxbjAmt(BigDecimal.ZERO);     //核销本金
            iqpPrePayment.setTotalHxlxAmt(BigDecimal.ZERO);     //核销利息
            iqpPrePayment.setTotalTqlxAmt(iqpPrePayment.getTotalLxAmt()
                    .add(iqpPrePayment.getTotalFxAmt())
                    .add(iqpPrePayment.getFxAmt()));     //拖欠利息总额
        }
    }

    public String sendHxToRepay(IqpPrePayment iqpPrePayment) {
        //利率下调不签协议，直接发送核心
        ResultDto<Ln3041RespDto> ln3041ResultDto = bGYW11BizService.sendHxToRepay(iqpPrePayment.getIqpSerno());
        String ln3041Meesage = Optional.ofNullable(ln3041ResultDto.getMessage())
                .orElse(SuccessEnum.SUCCESS.value);
        if (!Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3041ResultDto.getCode())) {
            throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key,
                    EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "核心返回："+ ln3041Meesage);
        } else {
            ln3041Meesage = "success";
        }
        if(null != ln3041ResultDto && null != ln3041ResultDto.getData()){
            iqpPrePayment.setHxSerno(ln3041ResultDto.getData().getJiaoyils());
            iqpPrePayment.setHxDate(ln3041ResultDto.getData().getJiaoyirq());
            this.updateSelective(iqpPrePayment);
        }
        return ln3041Meesage;
    }


    public String refundCz(String ids) {
        // 获取还款信息
        IqpPrePayment iqpPrePayment = this.selectByPrimaryKey(ids);
        // 核心交易流水号
        String hxSerno = iqpPrePayment.getHxSerno();
        // 核心交易日期
        String hxDate = iqpPrePayment.getHxDate();
        // 借据号
        String billNo = iqpPrePayment.getBillNo();
        Ib1241ReqDto req1241Dto = new Ib1241ReqDto();
        req1241Dto.setYgyliush(hxSerno);
        req1241Dto.setYjiaoyrq(hxDate);
        ResultDto<Ib1241RespDto> ib1241ResultDto =  dscms2CoreIbClientService.ib1241(req1241Dto);
        String ln3236Meesage = Optional.ofNullable(ib1241ResultDto.getMessage())
                .orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ib1241ResultDto.getCode())) {
            ln3236Meesage = "success";
        }
        return ln3236Meesage;
    }
}
