package cn.com.yusys.yusp.dto.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/4/24 15:29
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class QueryEnterpriseReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /* 企业名称 */
    @JsonProperty(value = "conName")
    private String conName;

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }
}
