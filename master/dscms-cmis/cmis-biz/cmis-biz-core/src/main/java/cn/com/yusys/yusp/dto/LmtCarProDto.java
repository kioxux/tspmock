package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarPro
 * @类描述: lmt_car_pro数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 16:34:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCarProDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 项目名称 **/
	private String proName;
	
	/** 销售网点数量 **/
	private String saleBrhQnt;
	
	/** 合作品牌 **/
	private String coopBrand;
	
	/** 汽车使用类别 STD_ZB_CAR_UTIL_TYPE **/
	private String carUtilType;
	
	/** 联系人姓名 **/
	private String linkmanName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 联系方式 **/
	private String linkMode;
	
	/** 备注 **/
	private String memo;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName == null ? null : proName.trim();
	}
	
    /**
     * @return ProName
     */	
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param saleBrhQnt
	 */
	public void setSaleBrhQnt(String saleBrhQnt) {
		this.saleBrhQnt = saleBrhQnt == null ? null : saleBrhQnt.trim();
	}
	
    /**
     * @return SaleBrhQnt
     */	
	public String getSaleBrhQnt() {
		return this.saleBrhQnt;
	}
	
	/**
	 * @param coopBrand
	 */
	public void setCoopBrand(String coopBrand) {
		this.coopBrand = coopBrand == null ? null : coopBrand.trim();
	}
	
    /**
     * @return CoopBrand
     */	
	public String getCoopBrand() {
		return this.coopBrand;
	}
	
	/**
	 * @param carUtilType
	 */
	public void setCarUtilType(String carUtilType) {
		this.carUtilType = carUtilType == null ? null : carUtilType.trim();
	}
	
    /**
     * @return CarUtilType
     */	
	public String getCarUtilType() {
		return this.carUtilType;
	}
	
	/**
	 * @param linkmanName
	 */
	public void setLinkmanName(String linkmanName) {
		this.linkmanName = linkmanName == null ? null : linkmanName.trim();
	}
	
    /**
     * @return LinkmanName
     */	
	public String getLinkmanName() {
		return this.linkmanName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkMode
	 */
	public void setLinkMode(String linkMode) {
		this.linkMode = linkMode == null ? null : linkMode.trim();
	}
	
    /**
     * @return LinkMode
     */	
	public String getLinkMode() {
		return this.linkMode;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}