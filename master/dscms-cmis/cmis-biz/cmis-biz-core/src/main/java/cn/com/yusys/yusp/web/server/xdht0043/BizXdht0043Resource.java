package cn.com.yusys.yusp.web.server.xdht0043;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0043.req.Xdht0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0043.resp.Xdht0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0043.Xdht0043Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:小贷借款合同文本生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0043:小贷借款合同文本生成pdf")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0043Resource.class);

    @Resource
    private Xdht0043Service xdht0043Service;
    /**
     * 交易码：xdht0043
     * 交易描述：小贷借款合同文本生成pdf
     *
     * @param xdht0043DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("小贷借款合同文本生成pdf")
    @PostMapping("/xdht0043")
    protected @ResponseBody
    ResultDto<Xdht0043DataRespDto> xdht0043(@Validated @RequestBody Xdht0043DataReqDto xdht0043DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataReqDto));
        Xdht0043DataRespDto  xdht0043DataRespDto  = new Xdht0043DataRespDto();// 响应Dto:小贷借款合同文本生成pdf
        ResultDto<Xdht0043DataRespDto>xdht0043DataResultDto = new ResultDto<>();
        try {
            String contNo = xdht0043DataReqDto.getContNo();//合同编号
            String certNo = xdht0043DataReqDto.getCertNo();//身份证号码

            if (StringUtils.isBlank(contNo)) {
                xdht0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0043DataResultDto.setMessage("合同编号【contNo】不能为空！");
                return xdht0043DataResultDto;
            } else if (StringUtils.isBlank(certNo)) {
                xdht0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0043DataResultDto.setMessage("证件号码【certNo】不能为空！");
                return xdht0043DataResultDto;
            }

            // 从xdht0043DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataReqDto));
            xdht0043DataRespDto = xdht0043Service.getXdJkhtInfo(xdht0043DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataRespDto));
            // 通讯地址不能为空 邮箱、QQ、微信不能同时为空
            String flag = xdht0043DataRespDto.getPassword();
            if("2".equals(flag)){//客户要素信息为空
                xdht0043DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0043DataResultDto.setMessage(xdht0043DataRespDto.getFtpAddr());
                return xdht0043DataResultDto;
            }
            // 封装xdht0043DataResultDto中正确的返回码和返回信息
            xdht0043DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0043DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, e.getMessage());
            // 封装xdht0043DataResultDto中异常返回码和返回信息
            xdht0043DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0043DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0043DataRespDto到xdht0043DataResultDto中
        xdht0043DataResultDto.setData(xdht0043DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataRespDto));
        return xdht0043DataResultDto;
    }
}
