package cn.com.yusys.yusp.service.client.bsp.ecif.s00101;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：对私客户综合信息查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class S00101Service {
    private static final Logger logger = LoggerFactory.getLogger(S00101Service.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * 业务逻辑处理方法：对私客户综合信息查询
     *
     * @param s00101ReqDto
     * @return
     */
    @Transactional
    public S00101RespDto s00101(S00101ReqDto s00101ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value, JSON.toJSONString(s00101ReqDto));
        ResultDto<S00101RespDto> s00101ResultDto = dscms2EcifClientService.s00101(s00101ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value, JSON.toJSONString(s00101ResultDto));

        String s00101Code = Optional.ofNullable(s00101ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String s00101Meesage = Optional.ofNullable(s00101ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        S00101RespDto s00101RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, s00101ResultDto.getCode())) {
            //  获取相关的值并解析
            s00101RespDto = s00101ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(s00101Code, s00101Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value);
        return s00101RespDto;
    }
}
