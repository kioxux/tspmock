/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import cn.com.yusys.yusp.domain.OtherRecordAccpSignOrAllPldApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.OtherBailDepPreferRateAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherBailDepPreferRateApp;
import cn.com.yusys.yusp.service.OtherBailDepPreferRateAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherBailDepPreferRateAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-07 17:47:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherbaildeppreferrateapp")
public class OtherBailDepPreferRateAppResource {
    private static final Logger log = LoggerFactory.getLogger(OtherBailDepPreferRateAppService.class);

    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Autowired
    private OtherBailDepPreferRateAppService otherBailDepPreferRateAppService;

    @Autowired
    private OtherBailDepPreferRateAppMapper otherBailDepPreferRateAppMapper;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherBailDepPreferRateApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppService.selectAll(queryModel);
        return new ResultDto<List<OtherBailDepPreferRateApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherBailDepPreferRateApp>> index(QueryModel queryModel) {
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherBailDepPreferRateApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherBailDepPreferRateApp> show(@PathVariable("serno") String serno) {
        OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherBailDepPreferRateApp>(otherBailDepPreferRateApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherBailDepPreferRateApp> create(@RequestBody OtherBailDepPreferRateApp otherBailDepPreferRateApp) throws URISyntaxException {
        otherBailDepPreferRateAppService.insert(otherBailDepPreferRateApp);
        return new ResultDto<OtherBailDepPreferRateApp>(otherBailDepPreferRateApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherBailDepPreferRateApp otherBailDepPreferRateApp) throws URISyntaxException {
        int result = otherBailDepPreferRateAppService.update(otherBailDepPreferRateApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherBailDepPreferRateAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherBailDepPreferRateAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{serno}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("serno") String serno) {
        int result = 0;
        //2.判断当前申请状态是否为退回，修改为自行退出
        OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppService.selectByPrimaryKey(serno);
        if (otherBailDepPreferRateApp!=null && CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherBailDepPreferRateApp.getApproveStatus())){
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：",otherBailDepPreferRateApp.getSerno());
            // 删除流程实例
            workflowCoreClient.deleteByBizId(otherBailDepPreferRateApp.getSerno());
            otherBailDepPreferRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);

            result = otherBailDepPreferRateAppService.update(otherBailDepPreferRateApp);
            log.info("银票手续费率申请删除结束。。。");
        }else{
            result = otherBailDepPreferRateAppService.deleteInfo(serno);
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateOprTypeUnderLmt
     * @函数描述:授信场景下 逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = 0;
        //2.判断当前申请状态是否为退回，修改为自行退出
        OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppService.selectByPrimaryKey(serno);
        if (otherBailDepPreferRateApp!=null && CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherBailDepPreferRateApp.getApproveStatus())){
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：",otherBailDepPreferRateApp.getSerno());
            // 删除流程实例
            workflowCoreClient.deleteByBizId(otherBailDepPreferRateApp.getSerno());
            otherBailDepPreferRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);

            result = otherBailDepPreferRateAppService.update(otherBailDepPreferRateApp);
            log.info("银票手续费率申请删除结束。。。");
        }else{
            result = otherBailDepPreferRateAppService.deleteInfo(serno);
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getotherbaildeppreferrateapp
     * @方法描述: 根据入参获取当前客户经理名下保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherbaildeppreferrateapp")
    protected ResultDto<List<OtherBailDepPreferRateApp>> getOtherBailDepPreferRateApp(@RequestBody QueryModel model) {
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppService.getOtherBailDepPreferRateAppByModel(model);
        return new ResultDto<List<OtherBailDepPreferRateApp>>(list);
    }

    /**
     * @方法名称: getotherbaildeppreferrateappHis
     * @方法描述: 根据入参获取当前客户经理名下保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherbaildeppreferrateapphis")
    protected ResultDto<List<OtherBailDepPreferRateApp>> getOtherBailDepPreferRateAppHis(@RequestBody QueryModel model) {
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppService.getOtherBailDepPreferRateAppHis(model);
        return new ResultDto<List<OtherBailDepPreferRateApp>>(list);
    }

    /**
     * @方法名称: addotherbaildeppreferrateapp
     * @方法描述: 新增保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addotherbaildeppreferrateapp")
    protected ResultDto addotherbaildeppreferrateapp(@RequestBody OtherBailDepPreferRateApp otherBailDepPreferRateApp) {
        return otherBailDepPreferRateAppService.addotherbaildeppreferrateapp(otherBailDepPreferRateApp);
    }

    /**
     * @方法名称: updateotherbaildeppreferrateapp
     * @方法描述: 修改保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateotherbaildeppreferrateapp")
    protected ResultDto updateotherbaildeppreferrateapp(@RequestBody OtherBailDepPreferRateApp otherBailDepPreferRateApp) {
        return otherBailDepPreferRateAppService.updateotherbaildeppreferrateapp(otherBailDepPreferRateApp);
    }

    /**
     * @方法名称: checkotherbaildeppreferrateapp
     * @方法描述: 校验保证金存款特惠利率申请是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/checkotherbaildeppreferrateapp")
    protected ResultDto checkotherbaildeppreferrateapp(@RequestBody OtherBailDepPreferRateApp otherBailDepPreferRateApp) {
        return otherBailDepPreferRateAppService.checkotherbaildeppreferrateapp(otherBailDepPreferRateApp);
    }

    /**
     * @方法名称: upIsUpperApprAuth
     * @方法描述:  上调权限处理
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/upIsUpperApprAuth")
    protected ResultDto<Integer> upIsUpperApprAuth(@RequestBody Map<String,String> params) {
        int result = 0;
        // 获取流水号
        String serno =  params.get("serno");
        // 是否上调权限
        String  isUpperApprAuth = params.get("isUpperApprAuth");
        if(!"".equals(serno) && null != serno){
            OtherBailDepPreferRateApp otherBailDepPreferRateApp =  otherBailDepPreferRateAppService.selectByPrimaryKey(serno);
            if(null != otherBailDepPreferRateApp){
                otherBailDepPreferRateApp.setIsUpperApprAuth(isUpperApprAuth);
                result = otherBailDepPreferRateAppService.update(otherBailDepPreferRateApp);
            }
        }
        return new ResultDto<Integer>(result);
    }


}
