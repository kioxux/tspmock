package cn.com.yusys.yusp.service.server.xdsx0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.MajorGradeInfo;
import cn.com.yusys.yusp.dto.server.xdsx0006.req.Xdsx0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.resp.Xdsx0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.MajorGradeInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0006Service
 * @类描述: #服务类
 * @功能描述:将入参信息保存或修改至专业贷款评级历次评级信息表
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0006Service {
    @Autowired
    private MajorGradeInfoMapper majorGradeInfoMapper;

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0006Service.class);

    /**
     * 专业贷款评级结果同步
     *
     * @param xdsx0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0006DataRespDto updateMajorGradeInfoDetail(Xdsx0006DataReqDto xdsx0006DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value);
        //返回对象
        Xdsx0006DataRespDto xdsx0006DataRespDto = new Xdsx0006DataRespDto();
        int count = 0;//查询结果
        int result = 0;//执行结果
        try {
            String gradeSerno = xdsx0006DataReqDto.getEvalAppSerno();//评级授信申请流水
            String lmtAppSerno = xdsx0006DataReqDto.getLmtAppSerno();//授信申请流水号
            String cusId = xdsx0006DataReqDto.getCusId();//客户编号
            String cusName = xdsx0006DataReqDto.getCusName();//客户名称
            String certType = xdsx0006DataReqDto.getCertType();//证件类型
            String certNo = xdsx0006DataReqDto.getCertNo();//证件号码
            String evalYear = xdsx0006DataReqDto.getEvalYear();//评级年度
            String evalHppType = xdsx0006DataReqDto.getEvalHppType();//评级发生类型
            String evalModel = xdsx0006DataReqDto.getEvalModel();//评级模型
            String majorLoanStartLvl = xdsx0006DataReqDto.getMajorLoanStartLvl();//专业贷款系统初始等级
            String majorLoanAdjdLvl = xdsx0006DataReqDto.getMajorLoanAdjdLvl();//专业贷款调整后等级
            String majorLoanAdviceLvl = xdsx0006DataReqDto.getMajorLoanAdviceLvl();//专业贷款建议等级
            String majorLoanFinalLvl = xdsx0006DataReqDto.getMajorLoanFinalLvl();//专业贷款最终认定等级
            BigDecimal majorLoanExpectLossRate = xdsx0006DataReqDto.getMajorLoanExpectLossRate();//专业贷款预期损失率
            String evalStartDate = xdsx0006DataReqDto.getEvalStartDate();//评级生效日
            String evalEndDate = xdsx0006DataReqDto.getEvalEndDate();//评级到期日
            String riskTypeMax = xdsx0006DataReqDto.getRiskTypeMax();//风险大类
            String riskType = xdsx0006DataReqDto.getRiskType();//风险种类
            String riskTypeMin = xdsx0006DataReqDto.getRiskTypeMin();//风险小类
            String riskDtghDate = xdsx0006DataReqDto.getRiskDtghDate();//风险划分日期
            if(StringUtil.isNotEmpty(evalStartDate)){
                evalStartDate = evalStartDate.replaceAll("/","-").substring(0,10);
            }
            if(StringUtil.isNotEmpty(evalEndDate)){
                evalEndDate = evalEndDate.replaceAll("/","-").substring(0,10);
            }
            if(StringUtil.isNotEmpty(riskDtghDate)){
                riskDtghDate = riskDtghDate.replaceAll("/","-").substring(0,10);
            }

            /**
             * 1.根据评级授信申请流水查询是否存在该条流水，判定是新增还是更新
             */
            logger.info("***********根据评级授信申请流水查询是否存在该条流水，判定是新增还是更新****START*********:");
            count = majorGradeInfoMapper.selectCountByGradeSerno(gradeSerno);
            logger.info("***********根据评级授信申请流水查询是否存在该条流水，判定是新增还是更新****END*********结果:"+count);

            if (count > 0) {//已存在评级授信流水,执行修改操作
                result = majorGradeInfoMapper.updateCountByGradeSerno(xdsx0006DataReqDto);
            } else {//不存在，新增
                MajorGradeInfo MajorGradeInfo = new MajorGradeInfo();
                MajorGradeInfo.setPkId(gradeSerno);
                MajorGradeInfo.setGradeSerno(gradeSerno);
                MajorGradeInfo.setLmtSerno(lmtAppSerno);
                MajorGradeInfo.setCusId(cusId);
                MajorGradeInfo.setCusName(cusName);
                MajorGradeInfo.setCertType(certType);
                MajorGradeInfo.setCertCode(certNo);
                MajorGradeInfo.setGradeYear(evalYear);
                MajorGradeInfo.setGradeHappenType(evalHppType);
                MajorGradeInfo.setGradeMode(evalModel);
                MajorGradeInfo.setInitGrade(majorLoanStartLvl);
                MajorGradeInfo.setAfterGrade(majorLoanAdjdLvl);
                MajorGradeInfo.setAdviceGrade(majorLoanAdviceLvl);
                MajorGradeInfo.setFinalGrade(majorLoanFinalLvl);
                MajorGradeInfo.setExpectedLossRate(majorLoanExpectLossRate);
                MajorGradeInfo.setBigriskclass(riskTypeMax);
                MajorGradeInfo.setMidriskclass(riskType);
                MajorGradeInfo.setFewriskclass(riskTypeMin);
                MajorGradeInfo.setRiskdividedate(riskDtghDate);
                MajorGradeInfo.setInureDate(evalStartDate);
                MajorGradeInfo.setEndDate(evalEndDate);
                MajorGradeInfo.setOprType(CmisCommonConstants.OP_TYPE_01);
                result = majorGradeInfoMapper.insertCountByGradeSerno(MajorGradeInfo);
            }

            //同步更新 major_grade
            Map QueryMap = new HashMap();
            QueryMap.put("evalModel", evalModel);//评级模型
            QueryMap.put("majorLoanFinalLvl", majorLoanFinalLvl);//专业贷款最终认定等级
            QueryMap.put("majorLoanExpectLossRate", majorLoanExpectLossRate);//专业贷款预期损失率
            QueryMap.put("lmtAppSerno", lmtAppSerno);//授信申请流水号
            logger.info("***********同步更新 major_grade****START*********:");
            int num = majorGradeInfoMapper.updateMajorGradeByGradeSerno(QueryMap);
            logger.info("***********同步更新 major_grade****END*********:");


            if (result == 1) {//专业贷款评级结果同步功
                xdsx0006DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                xdsx0006DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
            } else {//专业贷款评级结果同步失败
                xdsx0006DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
                xdsx0006DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);// 描述信息
            }
            if(num > 0){
                logger.info("***********同步更新 major_grade****成功*********:");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdsx0006DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
            xdsx0006DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value);
        return xdsx0006DataRespDto;
    }
}
