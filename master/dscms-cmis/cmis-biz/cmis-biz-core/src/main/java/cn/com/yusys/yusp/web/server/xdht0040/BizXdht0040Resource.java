package cn.com.yusys.yusp.web.server.xdht0040;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0040.req.Xdht0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0040.resp.Xdht0040DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0040.Xdht0040Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:修改受托信息
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0040:修改受托信息")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0040Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0040Resource.class);

    @Autowired
    private Xdht0040Service xdht0040Service;

    /**
     * 交易码：xdht0040
     * 交易描述：修改受托信息
     *
     * @param xdht0040DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("修改受托信息")
    @PostMapping("/xdht0040")
    protected @ResponseBody
    ResultDto<Xdht0040DataRespDto> xdht0040(@Validated @RequestBody Xdht0040DataReqDto xdht0040DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataReqDto));
        Xdht0040DataRespDto xdht0040DataRespDto = new Xdht0040DataRespDto();// 响应Dto:修改受托信息
        ResultDto<Xdht0040DataRespDto> xdht0040DataResultDto = new ResultDto<>();
        try {
            String serno = xdht0040DataReqDto.getSerno();//流水号
            String cusId = xdht0040DataReqDto.getCusId();//客户号
            String cusName = xdht0040DataReqDto.getCusName();//客户名称
            String billNo = xdht0040DataReqDto.getBillNo();//借据号
            String contNo = xdht0040DataReqDto.getContNo();//合同号
            String toppAcctNo = xdht0040DataReqDto.getToppAcctNo();//交易对手账号
            String toppName = xdht0040DataReqDto.getToppName();//交易对手名称
            String oldToppAcctNo = xdht0040DataReqDto.getOldToppAcctNo();//原交易对手账号
            String oldToppName = xdht0040DataReqDto.getOldToppName();//原交易对手名称
            String toppAmt = xdht0040DataReqDto.getToppAmt();//交易对手金额
            String oldToppAmt = xdht0040DataReqDto.getOldToppAmt();//原交易对手金额
            String acctsvcrAcctNo = xdht0040DataReqDto.getAcctsvcrAcctNo();//开户行号
            String acctsvcrName = xdht0040DataReqDto.getAcctsvcrName();//开户行名称
            String oldAcctsvcrAcctNo = xdht0040DataReqDto.getOldAcctsvcrAcctNo();//原开户行号
            String oldAacctsvcrName = xdht0040DataReqDto.getOldAacctsvcrName();//原开户行名称
            String isOnline = xdht0040DataReqDto.getIsOnline();//是否线上
            String oldIsOnline = xdht0040DataReqDto.getOldIsOnline();//原是否线上
            // 从xdht0040DataReqDto获取业务值进行业务逻辑处理
            //  调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataReqDto));
            xdht0040DataRespDto = xdht0040Service.updateIqpChgTrupayAcctAppBySerno(xdht0040DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataRespDto));
            //  调用XXXXXService层结束
            // 封装xdht0040DataResultDto中正确的返回码和返回信息
            xdht0040DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0040DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, e.getMessage());
            // 封装xdht0040DataResultDto中异常返回码和返回信息
            //  EpbEnum.EPB099999  开始
            xdht0040DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0040DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EpbEnum.EPB099999   结束
        }
        // 封装xdht0040DataRespDto到xdht0040DataResultDto中
        xdht0040DataResultDto.setData(xdht0040DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataRespDto));
        return xdht0040DataResultDto;
    }
}
