/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfUsufLand
 * @类描述: guar_inf_usuf_land数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_usuf_land")
public class GuarInfUsufLand extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 不动产权证号 **/
	@Column(name = "BDC_CRNO", unique = false, nullable = true, length = 40)
	private String bdcCrno;
	
	/** 使用权权证号 **/
	@Column(name = "USE_NO", unique = false, nullable = true, length = 40)
	private String useNo;
	
	/** 使用权抵押登记证号 **/
	@Column(name = "USE_CERT_NO", unique = false, nullable = true, length = 40)
	private String useCertNo;
	
	/** 使用权登记机关 **/
	@Column(name = "USE_CERT_OFFICE", unique = false, nullable = true, length = 100)
	private String useCertOffice;
	
	/** 使用权取得日期 **/
	@Column(name = "USE_CERT_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String useCertBeginDate;
	
	/** 使用权到期日期 **/
	@Column(name = "USE_CERT_END_DATE", unique = false, nullable = true, length = 10)
	private String useCertEndDate;
	
	/** 使用权面积 **/
	@Column(name = "USE_CERT_AREA", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal useCertArea;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 使用权位置 **/
	@Column(name = "USE_CERT_LOCATION", unique = false, nullable = true, length = 100)
	private String useCertLocation;
	
	/** 买卖合同编号 **/
	@Column(name = "BUSINESS_CONTRACT_NO", unique = false, nullable = true, length = 40)
	private String businessContractNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 押品使用情况 **/
	@Column(name = "GUAR_UTIL_CASE", unique = false, nullable = true, length = 10)
	private String guarUtilCase;
	
	/** 林地名称 **/
	@Column(name = "FOREST_NAME", unique = false, nullable = true, length = 100)
	private String forestName;
	
	/** 林种 **/
	@Column(name = "FOREST_VARIET", unique = false, nullable = true, length = 10)
	private String forestVariet;
	
	/** 主要种树 **/
	@Column(name = "FOREST_MAIN", unique = false, nullable = true, length = 100)
	private String forestMain;
	
	/** 取得方式 **/
	@Column(name = "ACQU_MODE", unique = false, nullable = true, length = 10)
	private String acquMode;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param bdcCrno
	 */
	public void setBdcCrno(String bdcCrno) {
		this.bdcCrno = bdcCrno;
	}
	
    /**
     * @return bdcCrno
     */
	public String getBdcCrno() {
		return this.bdcCrno;
	}
	
	/**
	 * @param useNo
	 */
	public void setUseNo(String useNo) {
		this.useNo = useNo;
	}
	
    /**
     * @return useNo
     */
	public String getUseNo() {
		return this.useNo;
	}
	
	/**
	 * @param useCertNo
	 */
	public void setUseCertNo(String useCertNo) {
		this.useCertNo = useCertNo;
	}
	
    /**
     * @return useCertNo
     */
	public String getUseCertNo() {
		return this.useCertNo;
	}
	
	/**
	 * @param useCertOffice
	 */
	public void setUseCertOffice(String useCertOffice) {
		this.useCertOffice = useCertOffice;
	}
	
    /**
     * @return useCertOffice
     */
	public String getUseCertOffice() {
		return this.useCertOffice;
	}
	
	/**
	 * @param useCertBeginDate
	 */
	public void setUseCertBeginDate(String useCertBeginDate) {
		this.useCertBeginDate = useCertBeginDate;
	}
	
    /**
     * @return useCertBeginDate
     */
	public String getUseCertBeginDate() {
		return this.useCertBeginDate;
	}
	
	/**
	 * @param useCertEndDate
	 */
	public void setUseCertEndDate(String useCertEndDate) {
		this.useCertEndDate = useCertEndDate;
	}
	
    /**
     * @return useCertEndDate
     */
	public String getUseCertEndDate() {
		return this.useCertEndDate;
	}
	
	/**
	 * @param useCertArea
	 */
	public void setUseCertArea(java.math.BigDecimal useCertArea) {
		this.useCertArea = useCertArea;
	}
	
    /**
     * @return useCertArea
     */
	public java.math.BigDecimal getUseCertArea() {
		return this.useCertArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param useCertLocation
	 */
	public void setUseCertLocation(String useCertLocation) {
		this.useCertLocation = useCertLocation;
	}
	
    /**
     * @return useCertLocation
     */
	public String getUseCertLocation() {
		return this.useCertLocation;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo;
	}
	
    /**
     * @return businessContractNo
     */
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase;
	}
	
    /**
     * @return guarUtilCase
     */
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param forestName
	 */
	public void setForestName(String forestName) {
		this.forestName = forestName;
	}
	
    /**
     * @return forestName
     */
	public String getForestName() {
		return this.forestName;
	}
	
	/**
	 * @param forestVariet
	 */
	public void setForestVariet(String forestVariet) {
		this.forestVariet = forestVariet;
	}
	
    /**
     * @return forestVariet
     */
	public String getForestVariet() {
		return this.forestVariet;
	}
	
	/**
	 * @param forestMain
	 */
	public void setForestMain(String forestMain) {
		this.forestMain = forestMain;
	}
	
    /**
     * @return forestMain
     */
	public String getForestMain() {
		return this.forestMain;
	}
	
	/**
	 * @param acquMode
	 */
	public void setAcquMode(String acquMode) {
		this.acquMode = acquMode;
	}
	
    /**
     * @return acquMode
     */
	public String getAcquMode() {
		return this.acquMode;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}