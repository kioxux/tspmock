package cn.com.yusys.yusp.web.server.xdht0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0010.req.Xdht0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0010.resp.Xdht0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0019.resp.Xdxw0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdht0010.Xdht0010Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0010:查询符合条件的省心快贷合同")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0010Resource.class);

    @Autowired
    private Xdht0010Service xdht0010Service;
    /**
     * 交易码：xdht0010
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param xdht0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询符合条件的省心快贷合同")
    @PostMapping("/xdht0010")
    protected @ResponseBody
    ResultDto<Xdht0010DataRespDto> xdht0010(@Validated @RequestBody Xdht0010DataReqDto xdht0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataReqDto));
        Xdht0010DataRespDto xdht0010DataRespDto = new Xdht0010DataRespDto();// 响应Dto:查询符合条件的省心快贷合同
        ResultDto<Xdht0010DataRespDto> xdht0010DataResultDto = new ResultDto<>();
        try {
            // 从xdht0010DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataReqDto));
            xdht0010DataRespDto = xdht0010Service.getIndivCusBaseInfo(xdht0010DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataRespDto));
            // 封装xdht0010DataResultDto中正确的返回码和返回信息
            xdht0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, e.getMessage());
            // 封装xdxw0019DataResultDto中异常返回码和返回信息
            xdht0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0010DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, e.getMessage());
            // 封装xdht0010DataResultDto中异常返回码和返回信息
            xdht0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0010DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdht0010DataRespDto到xdht0010DataResultDto中
        xdht0010DataResultDto.setData(xdht0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataRespDto));
        return xdht0010DataResultDto;
    }
}
