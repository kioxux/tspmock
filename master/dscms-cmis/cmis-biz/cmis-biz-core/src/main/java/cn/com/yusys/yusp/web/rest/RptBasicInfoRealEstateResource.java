/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoRealEstate;
import cn.com.yusys.yusp.service.RptBasicInfoRealEstateService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRealEstateResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 13:48:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinforealestate")
public class RptBasicInfoRealEstateResource {
    @Autowired
    private RptBasicInfoRealEstateService rptBasicInfoRealEstateService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoRealEstate>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoRealEstate> list = rptBasicInfoRealEstateService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoRealEstate>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoRealEstate>> index(QueryModel queryModel) {
        List<RptBasicInfoRealEstate> list = rptBasicInfoRealEstateService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoRealEstate>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoRealEstate> create(@RequestBody RptBasicInfoRealEstate rptBasicInfoRealEstate) throws URISyntaxException {
        rptBasicInfoRealEstateService.insert(rptBasicInfoRealEstate);
        return new ResultDto<RptBasicInfoRealEstate>(rptBasicInfoRealEstate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoRealEstate rptBasicInfoRealEstate) throws URISyntaxException {
        int result = rptBasicInfoRealEstateService.update(rptBasicInfoRealEstate);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoRealEstateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoRealEstateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptBasicInfoRealEstate>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptBasicInfoRealEstate>>(rptBasicInfoRealEstateService.selectByModel(model));
    }

    @PostMapping("/updateEstate")
    protected ResultDto<Integer> updateEstate(@RequestBody RptBasicInfoRealEstate rptBasicInfoRealEstate) {
        return new ResultDto<Integer>(rptBasicInfoRealEstateService.updateSelective(rptBasicInfoRealEstate));
    }

    @PostMapping("/insertEstate")
    protected ResultDto<Integer> insertEstate(@RequestBody RptBasicInfoRealEstate rptBasicInfoRealEstate){
        rptBasicInfoRealEstate.setPkId(StringUtils.getUUID());
        return new ResultDto<Integer>(rptBasicInfoRealEstateService.insertSelective(rptBasicInfoRealEstate));
    }
    @PostMapping("/deleteEstate")
    protected ResultDto<Integer> deleteEstate(@RequestBody RptBasicInfoRealEstate rptBasicInfoRealEstate){
        String pkId = rptBasicInfoRealEstate.getPkId();
        return new ResultDto<Integer>(rptBasicInfoRealEstateService.deleteByPrimaryKey(pkId));
    }
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptBasicInfoRealEstate>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptBasicInfoRealEstate>>(rptBasicInfoRealEstateService.selectBySerno(serno));
    }
}
