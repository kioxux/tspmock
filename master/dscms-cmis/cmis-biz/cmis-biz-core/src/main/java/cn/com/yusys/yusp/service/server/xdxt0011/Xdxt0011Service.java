package cn.com.yusys.yusp.service.server.xdxt0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.AreaAdminUser;
//import cn.com.yusys.yusp.dto.CommonUserQueryReqDto;
//import cn.com.yusys.yusp.dto.CommonUserQueryRespDto;
import cn.com.yusys.yusp.dto.CommonUserQueryReqDto;
import cn.com.yusys.yusp.dto.CommonUserQueryRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.req.Xdxt0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.resp.Xdxt0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AreaAdminUserMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑类:客户经理信息通用查询
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxt0011Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0011Service.class);

    @Autowired
    private CommonService commonService;
    @Resource
    private AreaAdminUserMapper areaAdminUserMapper;
    @Resource
    private AccLoanMapper accLoanMapper;
    /**
     * 客户经理信息通用查询
     * @param xdxt0011DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0011DataRespDto getXdxt0011(Xdxt0011DataReqDto xdxt0011DataReqDto) throws BizException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataReqDto));
        Xdxt0011DataRespDto xdxt0011DataRespDto = new Xdxt0011DataRespDto();
        try {
            //查询类型
            //01-按客户经理工号查询
            //02-按客户证件号查询
            //03-按借据号查询
            //04-按客户经理证件号查询
            //05-按客户经理名称查询
            String queryType = xdxt0011DataReqDto.getQueryType();
            //客户经理编号
            String loginCode = "";
            //客户经理通用查询请求参数
            CommonUserQueryReqDto commonUserQueryReqDto = new CommonUserQueryReqDto();
            //客户经理通用查询响应
            List<CommonUserQueryRespDto> commonUserQueryRespDtos = null;

            //贷款余额
            String loanBalance = Strings.EMPTY;
            if(Objects.equals(CommonConstance.QUERY_TYPE_01,queryType)){
                //01-按客户经理工号查询
                loginCode = xdxt0011DataReqDto.getManagerId();
                //客户经理编号为空
                if(StringUtils.isEmpty(loginCode)){
                    throw BizException.error(null, EcbEnum.ECB010046.key,EcbEnum.ECB010046.value);
                }
                commonUserQueryReqDto.setLoginCode(loginCode);
                commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);

            }else if(Objects.equals(CommonConstance.QUERY_TYPE_02,queryType)){
                //02-按客户证件号查询
                String cusCertCode = xdxt0011DataReqDto.getCusCertNo();
                List<CusBaseDto> cusBaseDtos = commonService.getCusBaseListByCertCode(cusCertCode);
                if(CollectionUtils.isEmpty(cusBaseDtos)||cusBaseDtos.size()<1){
                    throw BizException.error(null, EcbEnum.ECB010017.key,EcbEnum.ECB010017.value);
                }
                loginCode = cusBaseDtos.get(0).getManagerId();
                commonUserQueryReqDto.setLoginCode(loginCode);
                commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);
            }else if(Objects.equals(CommonConstance.QUERY_TYPE_03,queryType)){
                //03-按借据号查询
                String billNo = xdxt0011DataReqDto.getBillNo();
                AccLoan accloan = accLoanMapper.selectByBillNo(billNo);
                if(Objects.nonNull(accloan)){
                    loginCode = accloan.getManagerId();
                    loanBalance = Optional.ofNullable(accloan.getLoanBalance()).orElse(BigDecimal.ZERO).toString();
                }
                commonUserQueryReqDto.setLoginCode(loginCode);
                commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);
            }else if(Objects.equals(CommonConstance.QUERY_TYPE_04,queryType)){
                //04-按客户经理证件号查询
                String certNo = xdxt0011DataReqDto.getCertCode();
                if(StringUtils.isEmpty(certNo)){
                    throw BizException.error(null,EcbEnum.ECB010047.key,EcbEnum.ECB010047.value);
                }
                commonUserQueryReqDto.setCertNo(certNo);
                commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);
            }else if(Objects.equals(CommonConstance.QUERY_TYPE_05,queryType)){
                //05-按客户经理名称查询
                String userName = xdxt0011DataReqDto.getManagerName();
                if(StringUtils.isEmpty(userName)){
                    throw BizException.error(null,EcbEnum.ECB010049.key,EcbEnum.ECB010049.value);
                }
                commonUserQueryReqDto.setUserName(userName);
                commonUserQueryRespDtos = commonService.getCommonUser(commonUserQueryReqDto);
            }
            logger.info("************XDXT0011*按【{}】查询结束,返回信息【{}】", queryType, JSON.toJSONString(commonUserQueryRespDtos));
            //查询无数据
            if(CollectionUtils.isEmpty(commonUserQueryRespDtos)||commonUserQueryRespDtos.size()<1){
                throw BizException.error(null, EcbEnum.ECB010017.key,EcbEnum.ECB010017.value);
            }
            AreaAdminUser areaAdminUser = areaAdminUserMapper.selectByPrimaryKey(loginCode);
            //调查模式
            String surveyMode = StringUtils.EMPTY;
            if(Objects.nonNull(areaAdminUser)){
                surveyMode = areaAdminUser.getSurveyMode();
            }

            String str = JSON.toJSONString(commonUserQueryRespDtos);
            commonUserQueryRespDtos = JSONObject.parseArray(str,CommonUserQueryRespDto.class);
            if(commonUserQueryRespDtos.size() != 1){
                throw BizException.error(null, "9999", "未查到客户经理信息");
            }

            CommonUserQueryRespDto commonUserQueryRespDto = commonUserQueryRespDtos.get(0);
            logger.info("************XDXT0011*查询用户返回信息【{}】",JSON.toJSONString(commonUserQueryRespDto));
            loginCode = commonUserQueryRespDto.getLoginCode();
            //查询是否小微客户经理
            String isXWUser = commonService.getIsXWUser(loginCode);
            logger.info("************XDXT0011*查询是否小微客户经理结果:"+isXWUser);
            //组织号 如果是小微客户经理，返回“01600”
            String orgid = isXWUser=="Y"?"01600":commonUserQueryRespDto.getOrgId();
            //客户经理工号
            xdxt0011DataRespDto.setManagerId(loginCode);
            //客户经理名称
            xdxt0011DataRespDto.setManagerName(commonUserQueryRespDto.getUserName());
            //客户经理联系方式
            xdxt0011DataRespDto.setTelnum(commonUserQueryRespDto.getPhone());
            //客户经理所在机构名称
            xdxt0011DataRespDto.setOrgName(commonUserQueryRespDto.getOrgName());
            //客户经理状态  A：生效 I：失效 W：待生效 接口返回：1生效；0失效
            xdxt0011DataRespDto.setStatus(commonUserQueryRespDto.getUserSts()=="A"?"1":"0");
            //客户经理角色码
            xdxt0011DataRespDto.setRoleNo(commonUserQueryRespDto.getRoleCode());
            //客户经理角色名称
            xdxt0011DataRespDto.setRoleName(commonUserQueryRespDto.getRoleName());
            //组织号 如果是小微客户经理，返回“01600”
            xdxt0011DataRespDto.setOrgid(orgid);
            //小贷机构
            xdxt0011DataRespDto.setXfOrgid(commonUserQueryRespDto.getOrgId());
            //小贷机构名称
            xdxt0011DataRespDto.setXfOrgName(commonUserQueryRespDto.getOrgName());
            //调查模式
            xdxt0011DataRespDto.setSurveyModel(surveyMode);
            //寿光小贷机构号
            xdxt0011DataRespDto.setSgOrgid(commonUserQueryRespDto.getOrgId());
            //贷款余额
            xdxt0011DataRespDto.setLoanBalance(loanBalance);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e){
            e.printStackTrace();
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataRespDto));
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataRespDto));
        return xdxt0011DataRespDto;
    }

}
