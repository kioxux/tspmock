/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: RptSpdAnysWmd
 * @类描述: rpt_spd_anys_wmd数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-08-24 14:39:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_wmd")
public class RptSpdAnysWmd extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 纳税评级 **/
	@Column(name = "TAX_RATE", unique = false, nullable = true, length = 40)
	private String taxRate;

	/** 外管货物贸易评级 **/
	@Column(name = "FOREIGN_GOOD_TRADE_GRADE", unique = false, nullable = true, length = 40)
	private String foreignGoodTradeGrade;

	/** 上年度免抵退增值税 **/
	@Column(name = "LAST_YEAR_VAT", unique = false, nullable = true, length = 40)
	private String lastYearVat;

	/** 上年度出口总量 **/
	@Column(name = "LAST_YEAR_TOTAL_EXPORTS", unique = false, nullable = true, length = 40)
	private String lastYearTotalExports;

	/** 上一年国际结算量 **/
	@Column(name = "LAST_YEAR_INTERNATIONAL_VOLUME", unique = false, nullable = true, length = 40)
	private String lastYearInternationalVolume;

	/** 我行国际结算量 **/
	@Column(name = "OWN_VOLUME", unique = false, nullable = true, length = 40)
	private String ownVolume;

	/** 他行企业结算情况说明 **/
	@Column(name = "OTHER_BANK_ENTER_SERT_SITU", unique = false, nullable = true, length = 500)
	private String otherBankEnterSertSitu;

	/** 企业整体经营及投资状况1 **/
	@Column(name = "OVER_OPER_INVE_SITU1", unique = false, nullable = true, length = 500)
	private String overOperInveSitu1;

	/** 企业整体经营及投资状况2 **/
	@Column(name = "OVER_OPER_INVE_SITU2", unique = false, nullable = true, length = 500)
	private String overOperInveSitu2;

	/** 企业整体经营及投资状况3 **/
	@Column(name = "OVER_OPER_INVE_SITU3", unique = false, nullable = true, length = 500)
	private String overOperInveSitu3;

	/** 企业实际控制人或法定代表人个人征信信用情况描述 **/
	@Column(name = "PFK_WMD_1_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd1Desc;

	/** 企业实际控制人或法定代表人个人征信信用主办客户经理评分 **/
	@Column(name = "PFK_WMD_1_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd1Grade1;

	/** 企业实际控制人或法定代表人个人征信信用协办客户经理评分 **/
	@Column(name = "PFK_WMD_1_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd1Grade2;

	/** 企业及实际控制人负债与核心资产的比率简要情况描述 **/
	@Column(name = "PFK_WMD_2_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd2Desc;

	/** 企业及实际控制人负债与核心资产的比率主办客户经理评分 **/
	@Column(name = "PFK_WMD_2_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd2Grade1;

	/** 企业及实际控制人负债与核心资产的比率协办客户经理评分 **/
	@Column(name = "PFK_WMD_2_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd2Grade2;

	/** 企业现有融资情况简要情况描述 **/
	@Column(name = "PFK_WMD_3_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd3Desc;

	/** 企业现有融资情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_3_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd3Grade1;

	/** 企业现有融资情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_3_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd3Grade2;

	/** 对外担保情况简要情况描述 **/
	@Column(name = "PFK_WMD_4_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd4Desc;

	/** 对外担保情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_4_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd4Grade1;

	/** 对外担保情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_4_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd4Grade2;

	/** 企业征信信用情况简要情况描述 **/
	@Column(name = "PFK_WMD_5_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd5Desc;

	/** 企业征信信用情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_5_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd5Grade1;

	/** 企业征信信用情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_5_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd5Grade2;

	/** 实际控制人从事本行业年限简要情况描述 **/
	@Column(name = "PFK_WMD_6_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd6Desc;

	/** 实际控制人从事本行业年限主办客户经理评分 **/
	@Column(name = "PFK_WMD_6_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd6Grade1;

	/** 实际控制人从事本行业年限协办客户经理评分 **/
	@Column(name = "PFK_WMD_6_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd6Grade2;

	/** 企业在我行国际结算量占总国际结算的比例简要情况描述 **/
	@Column(name = "PFK_WMD_7_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd7Desc;

	/** 企业在我行国际结算量占总国际结算的比例主办客户经理评分 **/
	@Column(name = "PFK_WMD_7_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd7Grade1;

	/** 企业在我行国际结算量占总国际结算的比例协办客户经理评分 **/
	@Column(name = "PFK_WMD_7_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd7Grade2;

	/** 上一年国际结算量简要情况描述 **/
	@Column(name = "PFK_WMD_8_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd8Desc;

	/** 上一年国际结算量主办客户经理评分 **/
	@Column(name = "PFK_WMD_8_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd8Grade1;

	/** 上一年国际结算量协办客户经理评分 **/
	@Column(name = "PFK_WMD_8_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd8Grade2;

	/** 应收账款周转率简要情况描述 **/
	@Column(name = "PFK_WMD_9_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd9Desc;

	/** 应收账款周转率主办客户经理评分 **/
	@Column(name = "PFK_WMD_9_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd9Grade1;

	/** 应收账款周转率协办客户经理评分 **/
	@Column(name = "PFK_WMD_9_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd9Grade2;

	/** 企业上下游合作情况简要情况描述 **/
	@Column(name = "PFK_WMD_10_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd10Desc;

	/** 企业上下游合作情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_10_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd10Grade1;

	/** 企业上下游合作情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_10_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd10Grade2;

	/** 下游单客户占比情况简要情况描述 **/
	@Column(name = "PFK_WMD_11_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd11Desc;

	/** 下游单客户占比情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_11_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd11Grade1;

	/** 下游单客户占比情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_11_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd11Grade2;

	/** 法人或实际控制人其他产业经营情况简要情况描述 **/
	@Column(name = "PFK_WMD_12_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd12Desc;

	/** 法人或实际控制人其他产业经营情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_12_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd12Grade1;

	/** 法人或实际控制人其他产业经营情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_12_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd12Grade2;

	/** 企业及实际控人名下房产抵押情况简要情况描述 **/
	@Column(name = "PFK_WMD_13_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd13Desc;

	/** 企业及实际控人名下房产抵押情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_13_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd13Grade1;

	/** 企业及实际控人名下房产抵押情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_13_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd13Grade2;

	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率简要情况描述 **/
	@Column(name = "PFK_WMD_14_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd14Desc;

	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率主办客户经理评分 **/
	@Column(name = "PFK_WMD_14_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd14Grade1;

	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率协办客户经理评分 **/
	@Column(name = "PFK_WMD_14_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd14Grade2;

	/** 法人面谈情况简要情况描述 **/
	@Column(name = "PFK_WMD_15_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd15Desc;

	/** 法人面谈情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_15_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd15Grade1;

	/** 法人面谈情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_15_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd15Grade2;

	/** 法人或实际控制人个人情况简要情况描述 **/
	@Column(name = "PFK_WMD_16_DESC", unique = false, nullable = true, length = 1000)
	private String pfkWmd16Desc;

	/** 法人或实际控制人个人情况主办客户经理评分 **/
	@Column(name = "PFK_WMD_16_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkWmd16Grade1;

	/** 法人或实际控制人个人情况协办客户经理评分 **/
	@Column(name = "PFK_WMD_16_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkWmd16Grade2;

	/** 企业是否按时交纳各项税款，有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_WMD_1", unique = false, nullable = true, length = 65535)
	private String focusWmd1;

	/** 企业有无违规排污，是否切实做好环保治理工作，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_WMD_2", unique = false, nullable = true, length = 65535)
	private String focusWmd2;

	/** 企业实际控制人有无吸毒、赌博等不良嗜好，有无经常往返澳门等地，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_WMD_3", unique = false, nullable = true, length = 65535)
	private String focusWmd3;

	/** 企业实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_WMD_4", unique = false, nullable = true, length = 65535)
	private String focusWmd4;

	/** 企业员工人数是否稳定，有无出现明显裁员情况，员工待遇是否合理 **/
	@Column(name = "FOCUS_WMD_5", unique = false, nullable = true, length = 65535)
	private String focusWmd5;

	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_WMD_6", unique = false, nullable = true, length = 65535)
	private String focusWmd6;

	/** 有无被外管局处罚情况 **/
	@Column(name = "FOCUS_WMD_7", unique = false, nullable = true, length = 65535)
	private String focusWmd7;

	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_WMD_8", unique = false, nullable = true, length = 65535)
	private String focusWmd8;

	/** 其它不利情况请简述 **/
	@Column(name = "FOCUS_WMD_9", unique = false, nullable = true, length = 65535)
	private String focusWmd9;

	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal lmtAmt;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param taxRate
	 */
	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	/**
	 * @return taxRate
	 */
	public String getTaxRate() {
		return this.taxRate;
	}

	/**
	 * @param foreignGoodTradeGrade
	 */
	public void setForeignGoodTradeGrade(String foreignGoodTradeGrade) {
		this.foreignGoodTradeGrade = foreignGoodTradeGrade;
	}

	/**
	 * @return foreignGoodTradeGrade
	 */
	public String getForeignGoodTradeGrade() {
		return this.foreignGoodTradeGrade;
	}

	/**
	 * @param lastYearVat
	 */
	public void setLastYearVat(String lastYearVat) {
		this.lastYearVat = lastYearVat;
	}

	/**
	 * @return lastYearVat
	 */
	public String getLastYearVat() {
		return this.lastYearVat;
	}

	/**
	 * @param lastYearTotalExports
	 */
	public void setLastYearTotalExports(String lastYearTotalExports) {
		this.lastYearTotalExports = lastYearTotalExports;
	}

	/**
	 * @return lastYearTotalExports
	 */
	public String getLastYearTotalExports() {
		return this.lastYearTotalExports;
	}

	/**
	 * @param lastYearInternationalVolume
	 */
	public void setLastYearInternationalVolume(String lastYearInternationalVolume) {
		this.lastYearInternationalVolume = lastYearInternationalVolume;
	}

	/**
	 * @return lastYearInternationalVolume
	 */
	public String getLastYearInternationalVolume() {
		return this.lastYearInternationalVolume;
	}

	/**
	 * @param ownVolume
	 */
	public void setOwnVolume(String ownVolume) {
		this.ownVolume = ownVolume;
	}

	/**
	 * @return ownVolume
	 */
	public String getOwnVolume() {
		return this.ownVolume;
	}

	/**
	 * @param otherBankEnterSertSitu
	 */
	public void setOtherBankEnterSertSitu(String otherBankEnterSertSitu) {
		this.otherBankEnterSertSitu = otherBankEnterSertSitu;
	}

	/**
	 * @return otherBankEnterSertSitu
	 */
	public String getOtherBankEnterSertSitu() {
		return this.otherBankEnterSertSitu;
	}

	/**
	 * @param overOperInveSitu1
	 */
	public void setOverOperInveSitu1(String overOperInveSitu1) {
		this.overOperInveSitu1 = overOperInveSitu1;
	}

	/**
	 * @return overOperInveSitu1
	 */
	public String getOverOperInveSitu1() {
		return this.overOperInveSitu1;
	}

	/**
	 * @param overOperInveSitu2
	 */
	public void setOverOperInveSitu2(String overOperInveSitu2) {
		this.overOperInveSitu2 = overOperInveSitu2;
	}

	/**
	 * @return overOperInveSitu2
	 */
	public String getOverOperInveSitu2() {
		return this.overOperInveSitu2;
	}

	/**
	 * @param overOperInveSitu3
	 */
	public void setOverOperInveSitu3(String overOperInveSitu3) {
		this.overOperInveSitu3 = overOperInveSitu3;
	}

	/**
	 * @return overOperInveSitu3
	 */
	public String getOverOperInveSitu3() {
		return this.overOperInveSitu3;
	}

	/**
	 * @param pfkWmd1Desc
	 */
	public void setPfkWmd1Desc(String pfkWmd1Desc) {
		this.pfkWmd1Desc = pfkWmd1Desc;
	}

	/**
	 * @return pfkWmd1Desc
	 */
	public String getPfkWmd1Desc() {
		return this.pfkWmd1Desc;
	}

	/**
	 * @param pfkWmd1Grade1
	 */
	public void setPfkWmd1Grade1(Integer pfkWmd1Grade1) {
		this.pfkWmd1Grade1 = pfkWmd1Grade1;
	}

	/**
	 * @return pfkWmd1Grade1
	 */
	public Integer getPfkWmd1Grade1() {
		return this.pfkWmd1Grade1;
	}

	/**
	 * @param pfkWmd1Grade2
	 */
	public void setPfkWmd1Grade2(Integer pfkWmd1Grade2) {
		this.pfkWmd1Grade2 = pfkWmd1Grade2;
	}

	/**
	 * @return pfkWmd1Grade2
	 */
	public Integer getPfkWmd1Grade2() {
		return this.pfkWmd1Grade2;
	}

	/**
	 * @param pfkWmd2Desc
	 */
	public void setPfkWmd2Desc(String pfkWmd2Desc) {
		this.pfkWmd2Desc = pfkWmd2Desc;
	}

	/**
	 * @return pfkWmd2Desc
	 */
	public String getPfkWmd2Desc() {
		return this.pfkWmd2Desc;
	}

	/**
	 * @param pfkWmd2Grade1
	 */
	public void setPfkWmd2Grade1(Integer pfkWmd2Grade1) {
		this.pfkWmd2Grade1 = pfkWmd2Grade1;
	}

	/**
	 * @return pfkWmd2Grade1
	 */
	public Integer getPfkWmd2Grade1() {
		return this.pfkWmd2Grade1;
	}

	/**
	 * @param pfkWmd2Grade2
	 */
	public void setPfkWmd2Grade2(Integer pfkWmd2Grade2) {
		this.pfkWmd2Grade2 = pfkWmd2Grade2;
	}

	/**
	 * @return pfkWmd2Grade2
	 */
	public Integer getPfkWmd2Grade2() {
		return this.pfkWmd2Grade2;
	}

	/**
	 * @param pfkWmd3Desc
	 */
	public void setPfkWmd3Desc(String pfkWmd3Desc) {
		this.pfkWmd3Desc = pfkWmd3Desc;
	}

	/**
	 * @return pfkWmd3Desc
	 */
	public String getPfkWmd3Desc() {
		return this.pfkWmd3Desc;
	}

	/**
	 * @param pfkWmd3Grade1
	 */
	public void setPfkWmd3Grade1(Integer pfkWmd3Grade1) {
		this.pfkWmd3Grade1 = pfkWmd3Grade1;
	}

	/**
	 * @return pfkWmd3Grade1
	 */
	public Integer getPfkWmd3Grade1() {
		return this.pfkWmd3Grade1;
	}

	/**
	 * @param pfkWmd3Grade2
	 */
	public void setPfkWmd3Grade2(Integer pfkWmd3Grade2) {
		this.pfkWmd3Grade2 = pfkWmd3Grade2;
	}

	/**
	 * @return pfkWmd3Grade2
	 */
	public Integer getPfkWmd3Grade2() {
		return this.pfkWmd3Grade2;
	}

	/**
	 * @param pfkWmd4Desc
	 */
	public void setPfkWmd4Desc(String pfkWmd4Desc) {
		this.pfkWmd4Desc = pfkWmd4Desc;
	}

	/**
	 * @return pfkWmd4Desc
	 */
	public String getPfkWmd4Desc() {
		return this.pfkWmd4Desc;
	}

	/**
	 * @param pfkWmd4Grade1
	 */
	public void setPfkWmd4Grade1(Integer pfkWmd4Grade1) {
		this.pfkWmd4Grade1 = pfkWmd4Grade1;
	}

	/**
	 * @return pfkWmd4Grade1
	 */
	public Integer getPfkWmd4Grade1() {
		return this.pfkWmd4Grade1;
	}

	/**
	 * @param pfkWmd4Grade2
	 */
	public void setPfkWmd4Grade2(Integer pfkWmd4Grade2) {
		this.pfkWmd4Grade2 = pfkWmd4Grade2;
	}

	/**
	 * @return pfkWmd4Grade2
	 */
	public Integer getPfkWmd4Grade2() {
		return this.pfkWmd4Grade2;
	}

	/**
	 * @param pfkWmd5Desc
	 */
	public void setPfkWmd5Desc(String pfkWmd5Desc) {
		this.pfkWmd5Desc = pfkWmd5Desc;
	}

	/**
	 * @return pfkWmd5Desc
	 */
	public String getPfkWmd5Desc() {
		return this.pfkWmd5Desc;
	}

	/**
	 * @param pfkWmd5Grade1
	 */
	public void setPfkWmd5Grade1(Integer pfkWmd5Grade1) {
		this.pfkWmd5Grade1 = pfkWmd5Grade1;
	}

	/**
	 * @return pfkWmd5Grade1
	 */
	public Integer getPfkWmd5Grade1() {
		return this.pfkWmd5Grade1;
	}

	/**
	 * @param pfkWmd5Grade2
	 */
	public void setPfkWmd5Grade2(Integer pfkWmd5Grade2) {
		this.pfkWmd5Grade2 = pfkWmd5Grade2;
	}

	/**
	 * @return pfkWmd5Grade2
	 */
	public Integer getPfkWmd5Grade2() {
		return this.pfkWmd5Grade2;
	}

	/**
	 * @param pfkWmd6Desc
	 */
	public void setPfkWmd6Desc(String pfkWmd6Desc) {
		this.pfkWmd6Desc = pfkWmd6Desc;
	}

	/**
	 * @return pfkWmd6Desc
	 */
	public String getPfkWmd6Desc() {
		return this.pfkWmd6Desc;
	}

	/**
	 * @param pfkWmd6Grade1
	 */
	public void setPfkWmd6Grade1(Integer pfkWmd6Grade1) {
		this.pfkWmd6Grade1 = pfkWmd6Grade1;
	}

	/**
	 * @return pfkWmd6Grade1
	 */
	public Integer getPfkWmd6Grade1() {
		return this.pfkWmd6Grade1;
	}

	/**
	 * @param pfkWmd6Grade2
	 */
	public void setPfkWmd6Grade2(Integer pfkWmd6Grade2) {
		this.pfkWmd6Grade2 = pfkWmd6Grade2;
	}

	/**
	 * @return pfkWmd6Grade2
	 */
	public Integer getPfkWmd6Grade2() {
		return this.pfkWmd6Grade2;
	}

	/**
	 * @param pfkWmd7Desc
	 */
	public void setPfkWmd7Desc(String pfkWmd7Desc) {
		this.pfkWmd7Desc = pfkWmd7Desc;
	}

	/**
	 * @return pfkWmd7Desc
	 */
	public String getPfkWmd7Desc() {
		return this.pfkWmd7Desc;
	}

	/**
	 * @param pfkWmd7Grade1
	 */
	public void setPfkWmd7Grade1(Integer pfkWmd7Grade1) {
		this.pfkWmd7Grade1 = pfkWmd7Grade1;
	}

	/**
	 * @return pfkWmd7Grade1
	 */
	public Integer getPfkWmd7Grade1() {
		return this.pfkWmd7Grade1;
	}

	/**
	 * @param pfkWmd7Grade2
	 */
	public void setPfkWmd7Grade2(Integer pfkWmd7Grade2) {
		this.pfkWmd7Grade2 = pfkWmd7Grade2;
	}

	/**
	 * @return pfkWmd7Grade2
	 */
	public Integer getPfkWmd7Grade2() {
		return this.pfkWmd7Grade2;
	}

	/**
	 * @param pfkWmd8Desc
	 */
	public void setPfkWmd8Desc(String pfkWmd8Desc) {
		this.pfkWmd8Desc = pfkWmd8Desc;
	}

	/**
	 * @return pfkWmd8Desc
	 */
	public String getPfkWmd8Desc() {
		return this.pfkWmd8Desc;
	}

	/**
	 * @param pfkWmd8Grade1
	 */
	public void setPfkWmd8Grade1(Integer pfkWmd8Grade1) {
		this.pfkWmd8Grade1 = pfkWmd8Grade1;
	}

	/**
	 * @return pfkWmd8Grade1
	 */
	public Integer getPfkWmd8Grade1() {
		return this.pfkWmd8Grade1;
	}

	/**
	 * @param pfkWmd8Grade2
	 */
	public void setPfkWmd8Grade2(Integer pfkWmd8Grade2) {
		this.pfkWmd8Grade2 = pfkWmd8Grade2;
	}

	/**
	 * @return pfkWmd8Grade2
	 */
	public Integer getPfkWmd8Grade2() {
		return this.pfkWmd8Grade2;
	}

	/**
	 * @param pfkWmd9Desc
	 */
	public void setPfkWmd9Desc(String pfkWmd9Desc) {
		this.pfkWmd9Desc = pfkWmd9Desc;
	}

	/**
	 * @return pfkWmd9Desc
	 */
	public String getPfkWmd9Desc() {
		return this.pfkWmd9Desc;
	}

	/**
	 * @param pfkWmd9Grade1
	 */
	public void setPfkWmd9Grade1(Integer pfkWmd9Grade1) {
		this.pfkWmd9Grade1 = pfkWmd9Grade1;
	}

	/**
	 * @return pfkWmd9Grade1
	 */
	public Integer getPfkWmd9Grade1() {
		return this.pfkWmd9Grade1;
	}

	/**
	 * @param pfkWmd9Grade2
	 */
	public void setPfkWmd9Grade2(Integer pfkWmd9Grade2) {
		this.pfkWmd9Grade2 = pfkWmd9Grade2;
	}

	/**
	 * @return pfkWmd9Grade2
	 */
	public Integer getPfkWmd9Grade2() {
		return this.pfkWmd9Grade2;
	}

	/**
	 * @param pfkWmd10Desc
	 */
	public void setPfkWmd10Desc(String pfkWmd10Desc) {
		this.pfkWmd10Desc = pfkWmd10Desc;
	}

	/**
	 * @return pfkWmd10Desc
	 */
	public String getPfkWmd10Desc() {
		return this.pfkWmd10Desc;
	}

	/**
	 * @param pfkWmd10Grade1
	 */
	public void setPfkWmd10Grade1(Integer pfkWmd10Grade1) {
		this.pfkWmd10Grade1 = pfkWmd10Grade1;
	}

	/**
	 * @return pfkWmd10Grade1
	 */
	public Integer getPfkWmd10Grade1() {
		return this.pfkWmd10Grade1;
	}

	/**
	 * @param pfkWmd10Grade2
	 */
	public void setPfkWmd10Grade2(Integer pfkWmd10Grade2) {
		this.pfkWmd10Grade2 = pfkWmd10Grade2;
	}

	/**
	 * @return pfkWmd10Grade2
	 */
	public Integer getPfkWmd10Grade2() {
		return this.pfkWmd10Grade2;
	}

	/**
	 * @param pfkWmd11Desc
	 */
	public void setPfkWmd11Desc(String pfkWmd11Desc) {
		this.pfkWmd11Desc = pfkWmd11Desc;
	}

	/**
	 * @return pfkWmd11Desc
	 */
	public String getPfkWmd11Desc() {
		return this.pfkWmd11Desc;
	}

	/**
	 * @param pfkWmd11Grade1
	 */
	public void setPfkWmd11Grade1(Integer pfkWmd11Grade1) {
		this.pfkWmd11Grade1 = pfkWmd11Grade1;
	}

	/**
	 * @return pfkWmd11Grade1
	 */
	public Integer getPfkWmd11Grade1() {
		return this.pfkWmd11Grade1;
	}

	/**
	 * @param pfkWmd11Grade2
	 */
	public void setPfkWmd11Grade2(Integer pfkWmd11Grade2) {
		this.pfkWmd11Grade2 = pfkWmd11Grade2;
	}

	/**
	 * @return pfkWmd11Grade2
	 */
	public Integer getPfkWmd11Grade2() {
		return this.pfkWmd11Grade2;
	}

	/**
	 * @param pfkWmd12Desc
	 */
	public void setPfkWmd12Desc(String pfkWmd12Desc) {
		this.pfkWmd12Desc = pfkWmd12Desc;
	}

	/**
	 * @return pfkWmd12Desc
	 */
	public String getPfkWmd12Desc() {
		return this.pfkWmd12Desc;
	}

	/**
	 * @param pfkWmd12Grade1
	 */
	public void setPfkWmd12Grade1(Integer pfkWmd12Grade1) {
		this.pfkWmd12Grade1 = pfkWmd12Grade1;
	}

	/**
	 * @return pfkWmd12Grade1
	 */
	public Integer getPfkWmd12Grade1() {
		return this.pfkWmd12Grade1;
	}

	/**
	 * @param pfkWmd12Grade2
	 */
	public void setPfkWmd12Grade2(Integer pfkWmd12Grade2) {
		this.pfkWmd12Grade2 = pfkWmd12Grade2;
	}

	/**
	 * @return pfkWmd12Grade2
	 */
	public Integer getPfkWmd12Grade2() {
		return this.pfkWmd12Grade2;
	}

	/**
	 * @param pfkWmd13Desc
	 */
	public void setPfkWmd13Desc(String pfkWmd13Desc) {
		this.pfkWmd13Desc = pfkWmd13Desc;
	}

	/**
	 * @return pfkWmd13Desc
	 */
	public String getPfkWmd13Desc() {
		return this.pfkWmd13Desc;
	}

	/**
	 * @param pfkWmd13Grade1
	 */
	public void setPfkWmd13Grade1(Integer pfkWmd13Grade1) {
		this.pfkWmd13Grade1 = pfkWmd13Grade1;
	}

	/**
	 * @return pfkWmd13Grade1
	 */
	public Integer getPfkWmd13Grade1() {
		return this.pfkWmd13Grade1;
	}

	/**
	 * @param pfkWmd13Grade2
	 */
	public void setPfkWmd13Grade2(Integer pfkWmd13Grade2) {
		this.pfkWmd13Grade2 = pfkWmd13Grade2;
	}

	/**
	 * @return pfkWmd13Grade2
	 */
	public Integer getPfkWmd13Grade2() {
		return this.pfkWmd13Grade2;
	}

	/**
	 * @param pfkWmd14Desc
	 */
	public void setPfkWmd14Desc(String pfkWmd14Desc) {
		this.pfkWmd14Desc = pfkWmd14Desc;
	}

	/**
	 * @return pfkWmd14Desc
	 */
	public String getPfkWmd14Desc() {
		return this.pfkWmd14Desc;
	}

	/**
	 * @param pfkWmd14Grade1
	 */
	public void setPfkWmd14Grade1(Integer pfkWmd14Grade1) {
		this.pfkWmd14Grade1 = pfkWmd14Grade1;
	}

	/**
	 * @return pfkWmd14Grade1
	 */
	public Integer getPfkWmd14Grade1() {
		return this.pfkWmd14Grade1;
	}

	/**
	 * @param pfkWmd14Grade2
	 */
	public void setPfkWmd14Grade2(Integer pfkWmd14Grade2) {
		this.pfkWmd14Grade2 = pfkWmd14Grade2;
	}

	/**
	 * @return pfkWmd14Grade2
	 */
	public Integer getPfkWmd14Grade2() {
		return this.pfkWmd14Grade2;
	}

	/**
	 * @param pfkWmd15Desc
	 */
	public void setPfkWmd15Desc(String pfkWmd15Desc) {
		this.pfkWmd15Desc = pfkWmd15Desc;
	}

	/**
	 * @return pfkWmd15Desc
	 */
	public String getPfkWmd15Desc() {
		return this.pfkWmd15Desc;
	}

	/**
	 * @param pfkWmd15Grade1
	 */
	public void setPfkWmd15Grade1(Integer pfkWmd15Grade1) {
		this.pfkWmd15Grade1 = pfkWmd15Grade1;
	}

	/**
	 * @return pfkWmd15Grade1
	 */
	public Integer getPfkWmd15Grade1() {
		return this.pfkWmd15Grade1;
	}

	/**
	 * @param pfkWmd15Grade2
	 */
	public void setPfkWmd15Grade2(Integer pfkWmd15Grade2) {
		this.pfkWmd15Grade2 = pfkWmd15Grade2;
	}

	/**
	 * @return pfkWmd15Grade2
	 */
	public Integer getPfkWmd15Grade2() {
		return this.pfkWmd15Grade2;
	}

	/**
	 * @param pfkWmd16Desc
	 */
	public void setPfkWmd16Desc(String pfkWmd16Desc) {
		this.pfkWmd16Desc = pfkWmd16Desc;
	}

	/**
	 * @return pfkWmd16Desc
	 */
	public String getPfkWmd16Desc() {
		return this.pfkWmd16Desc;
	}

	/**
	 * @param pfkWmd16Grade1
	 */
	public void setPfkWmd16Grade1(Integer pfkWmd16Grade1) {
		this.pfkWmd16Grade1 = pfkWmd16Grade1;
	}

	/**
	 * @return pfkWmd16Grade1
	 */
	public Integer getPfkWmd16Grade1() {
		return this.pfkWmd16Grade1;
	}

	/**
	 * @param pfkWmd16Grade2
	 */
	public void setPfkWmd16Grade2(Integer pfkWmd16Grade2) {
		this.pfkWmd16Grade2 = pfkWmd16Grade2;
	}

	/**
	 * @return pfkWmd16Grade2
	 */
	public Integer getPfkWmd16Grade2() {
		return this.pfkWmd16Grade2;
	}

	/**
	 * @param focusWmd1
	 */
	public void setFocusWmd1(String focusWmd1) {
		this.focusWmd1 = focusWmd1;
	}

	/**
	 * @return focusWmd1
	 */
	public String getFocusWmd1() {
		return this.focusWmd1;
	}

	/**
	 * @param focusWmd2
	 */
	public void setFocusWmd2(String focusWmd2) {
		this.focusWmd2 = focusWmd2;
	}

	/**
	 * @return focusWmd2
	 */
	public String getFocusWmd2() {
		return this.focusWmd2;
	}

	/**
	 * @param focusWmd3
	 */
	public void setFocusWmd3(String focusWmd3) {
		this.focusWmd3 = focusWmd3;
	}

	/**
	 * @return focusWmd3
	 */
	public String getFocusWmd3() {
		return this.focusWmd3;
	}

	/**
	 * @param focusWmd4
	 */
	public void setFocusWmd4(String focusWmd4) {
		this.focusWmd4 = focusWmd4;
	}

	/**
	 * @return focusWmd4
	 */
	public String getFocusWmd4() {
		return this.focusWmd4;
	}

	/**
	 * @param focusWmd5
	 */
	public void setFocusWmd5(String focusWmd5) {
		this.focusWmd5 = focusWmd5;
	}

	/**
	 * @return focusWmd5
	 */
	public String getFocusWmd5() {
		return this.focusWmd5;
	}

	/**
	 * @param focusWmd6
	 */
	public void setFocusWmd6(String focusWmd6) {
		this.focusWmd6 = focusWmd6;
	}

	/**
	 * @return focusWmd6
	 */
	public String getFocusWmd6() {
		return this.focusWmd6;
	}

	/**
	 * @param focusWmd7
	 */
	public void setFocusWmd7(String focusWmd7) {
		this.focusWmd7 = focusWmd7;
	}

	/**
	 * @return focusWmd7
	 */
	public String getFocusWmd7() {
		return this.focusWmd7;
	}

	/**
	 * @param focusWmd8
	 */
	public void setFocusWmd8(String focusWmd8) {
		this.focusWmd8 = focusWmd8;
	}

	/**
	 * @return focusWmd8
	 */
	public String getFocusWmd8() {
		return this.focusWmd8;
	}

	/**
	 * @param focusWmd9
	 */
	public void setFocusWmd9(String focusWmd9) {
		this.focusWmd9 = focusWmd9;
	}

	/**
	 * @return focusWmd9
	 */
	public String getFocusWmd9() {
		return this.focusWmd9;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}