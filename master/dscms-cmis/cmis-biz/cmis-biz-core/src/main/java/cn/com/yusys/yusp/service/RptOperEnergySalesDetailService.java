/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.RptOperProductionOper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperEnergySalesDetail;
import cn.com.yusys.yusp.repository.mapper.RptOperEnergySalesDetailMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySalesDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:49:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperEnergySalesDetailService {

    @Autowired
    private RptOperEnergySalesDetailMapper rptOperEnergySalesDetailMapper;

    @Autowired
    private RptOperProductionOperService rptOperProductionOperService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptOperEnergySalesDetail selectByPrimaryKey(String pkId) {
        return rptOperEnergySalesDetailMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptOperEnergySalesDetail> selectAll(QueryModel model) {
        List<RptOperEnergySalesDetail> records = (List<RptOperEnergySalesDetail>) rptOperEnergySalesDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptOperEnergySalesDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperEnergySalesDetail> list = rptOperEnergySalesDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptOperEnergySalesDetail record) {
        return rptOperEnergySalesDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptOperEnergySalesDetail record) {
        return rptOperEnergySalesDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptOperEnergySalesDetail record) {
        return rptOperEnergySalesDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptOperEnergySalesDetail record) {
        return rptOperEnergySalesDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptOperEnergySalesDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperEnergySalesDetailMapper.deleteByIds(ids);
    }

    public int insertSalesDetail(RptOperEnergySalesDetail rptOperEnergySalesDetail) {
        int count = 0;
        RptOperProductionOper rptOperProductionOper = new RptOperProductionOper();
        rptOperProductionOper.setSerno(rptOperEnergySalesDetail.getSerno());
        rptOperProductionOper.setDownstreamCusAnaly("");
        rptOperProductionOper.setCurrOrderStatus("");
        rptOperProductionOper.setSaleOtherNeedDesc("");
        count += rptOperProductionOperService.updateSelective(rptOperProductionOper);
        count += rptOperEnergySalesDetailMapper.insertSelective(rptOperEnergySalesDetail);
        return count;
    }
}
