package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LstacctinfoDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.RedisAuthCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CmisBizXwCommonService
 * @类描述: 小微公共处理类
 * @功能描述: 小微公共处理方法
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping(value = "/api/xwcommonservice")
public class CmisBizXwCommonResource {
    private Logger logger = LoggerFactory.getLogger(CmisBizXwCommonResource.class);

    @Autowired
    CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    @Lazy
    private RedisAuthCacheService redisAuthCacheService;

    /**
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.AdminSmUserDto>
     * @author 王玉坤
     * @date 2021/9/2 11:31
     * @version 1.0.0
     * @desc 根据客户经理查询分中心负责人信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping(value = "/getcentermanageridinfo")
    public ResultDto<AdminSmUserDto> getCenterManagerIdInfo(@RequestBody Map map) {
        String managerId = (String) map.get("managerId");
        return cmisBizXwCommonService.getCenterManagerIdInfo(managerId);
    }

    /**
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.AdminSmUserDto>
     * @author 王玉坤
     * @date 2021/9/2 11:31
     * @version 1.0.0
     * @desc 根据客户经理查询分中心负责人信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @GetMapping(value = "/refershCache")
    public ResultDto<Void> refershCache() {
        redisAuthCacheService.refershCache();
        return new ResultDto<>();
    }

    /**
     * @param map
     * @return int
     * @author 王玉坤
     * @date 2021/9/19 0:23
     * @version 1.0.0
     * @desc 根据日期计算相差月份 yyyy-MM-dd
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping(value = "/getbetweenmonth")
    public ResultDto<Integer> getBetweenMonth(@RequestBody Map map) throws Exception {
        return new ResultDto<>(cmisBizXwCommonService.getBetweenMonth(map.get("startDate").toString(), map.get("endDate").toString()));
    }

    @PostMapping(value = "/imageappr")
    public ResultDto<String> sendImage() {
        ImageApprDto imageApprDto = new ImageApprDto();
        imageApprDto.setDocId("7e39ee7e126643da87f5ff053c3b4f34");
        imageApprDto.setIsApproved("7e39ee7e126643da87f5ff053c3b4f34");
        imageApprDto.setApproval("28570d647e9d4c1482c1ba2710b99cb0");
        imageApprDto.setOutcode("SXBGJBZL");
        imageApprDto.setOpercode("28570d647");
        return cmisBizXwCommonService.sendImage(imageApprDto);
    }

    /**
     * 获取 小微分中心/小微分部部长下的客户经理
     *
     * @return
     * @创建人：周茂伟
     */
    @PostMapping(value = "/queryManagerByXwRole")
    public Map<String, List<String>> queryManagerByXwRole(String userNo) {
        return cmisBizXwCommonService.queryManagerByXwRole(userNo);
    }

    /**
     * 获取  获取所有小微区域管理人员（分中心负责人、分部部长）下的小微客户经理
     *
     * @return
     * @创建人：周茂伟
     */
    @PostMapping(value = "/loadXwAreaManagerDataAuthWithUser")
    public Map<String, List<String>> loadXwAreaManagerDataAuthWithUser() {
        return redisAuthCacheService.loadXwAreaManagerDataAuthWithUser();
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo>
     * @author 王玉坤
     * @date 2021/10/17 15:26
     * @version 1.0.0
     * @desc 根据客户编号查询结算账户信息列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping(value = "/getaccnolistbycusid")
    public ResultDto<List<LstacctinfoDto>> getAccNoListBycusId(@RequestBody QueryModel queryModel) {
        return new ResultDto<List<LstacctinfoDto>>(cmisBizXwCommonService.getAccNoListBycusId(queryModel));
    }
}
