package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRel
 * @类描述: GUAR_BIZ_REL数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarBizRelGuarBaseDto implements Serializable{

	private static final long serialVersionUID = 1L;

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getGuarTypeCd() {
		return guarTypeCd;
	}

	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	public String getGuarCusName() {
		return guarCusName;
	}

	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	public String getEvalAmt() {
		return evalAmt;
	}

	public void setEvalAmt(String evalAmt) {
		this.evalAmt = evalAmt;
	}

	public String getMaxMortagageAmt() {
		return maxMortagageAmt;
	}

	public void setMaxMortagageAmt(String maxMortagageAmt) {
		this.maxMortagageAmt = maxMortagageAmt;
	}

	public String getMortagageRate() {
		return mortagageRate;
	}

	public void setMortagageRate(String mortagageRate) {
		this.mortagageRate = mortagageRate;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getIsAddGuar() {
		return isAddGuar;
	}

	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar;
	}

	public BigDecimal getCorreFinAmt() {
		return correFinAmt;
	}

	public void setCorreFinAmt(BigDecimal correFinAmt) {
		this.correFinAmt = correFinAmt;
	}

	/** 主键 **/
	private String pkId;

	public String getIqpSerno() {
		return iqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	public String getGuarTypeCdCnname() {
		return guarTypeCdCnname;
	}

	public void setGuarTypeCdCnname(String guarTypeCdCnname) {
		this.guarTypeCdCnname = guarTypeCdCnname;
	}

	public String getGuarCusId() {
		return guarCusId;
	}

	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}

	/** 业务流水号 **/
	private String serno;
	/** 业务流水号 **/
	private String iqpSerno;
	/** 抵质押品编号 **/
	private String guarNo;
	/** 担保分类名称 **/
	private String guarTypeCd;
	/** 担保分类名称 **/
	private String guarTypeCdCnname;
	/** 抵质押品所有人 **/
	private String guarCusId;
	/** 抵质押品所有人 **/
	private String guarCusName;
	/** 评估价(元） **/
	private String evalAmt;
	/** 抵/质押物总价（元） **/
	private String maxMortagageAmt;
	/** 是否追加担保 */
	private String isAddGuar;
	/** 对应融资金额 */
	private java.math.BigDecimal correFinAmt;
	/** 抵/质押率（%） **/
	private String mortagageRate;
	/** 登记时间 **/
	private String inputDate;
	/** 更新时间 **/
	private String updDate;

}