package cn.com.yusys.yusp.web.server.xdtz0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0012.req.Xdtz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.resp.Xdtz0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0012.Xdtz0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:根据借款人证件号，判断配偶经营性贷款是否存在余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0012:根据借款人证件号，判断配偶经营性贷款是否存在余额")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0012Resource.class);
    @Autowired
    private Xdtz0012Service xdtz0012Service;
    /**
     * 交易码：xdtz0012
     * 交易描述：根据借款人证件号，判断配偶经营性贷款是否存在余额
     *
     * @param xdtz0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借款人证件号，判断配偶经营性贷款是否存在余额")
    @PostMapping("/xdtz0012")
    protected @ResponseBody
    ResultDto<Xdtz0012DataRespDto> xdtz0012(@Validated @RequestBody Xdtz0012DataReqDto xdtz0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataReqDto));
        Xdtz0012DataRespDto xdtz0012DataRespDto = new Xdtz0012DataRespDto();// 响应Dto:根据借款人证件号，判断配偶经营性贷款是否存在余额
        ResultDto<Xdtz0012DataRespDto> xdtz0012DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataReqDto));
            xdtz0012DataRespDto = xdtz0012Service.getXdtz0012(xdtz0012DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataRespDto));
            // 封装xdtz0012DataResultDto中正确的返回码和返回信息
            xdtz0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, e.getMessage());
            // 封装xdtz0012DataResultDto中异常返回码和返回信息

            xdtz0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0012DataResultDto.setMessage(EpbEnum.EPB099999.value);

        }
        // 封装xdtz0012DataRespDto到xdtz0012DataResultDto中
        xdtz0012DataResultDto.setData(xdtz0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataRespDto));
        return xdtz0012DataResultDto;
    }
}
