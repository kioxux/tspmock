/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AreaManager;
import cn.com.yusys.yusp.service.AreaManagerService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaManagerResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zlf
 * @创建时间: 2021-05-11 16:47:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "微贷区域管理")
@RequestMapping("/api/areamanager")
public class AreaManagerResource {
    @Autowired
    private AreaManagerService areaManagerService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AreaManager>> query() {
        QueryModel queryModel = new QueryModel();
        List<AreaManager> list = areaManagerService.selectAll(queryModel);
        return new ResultDto<List<AreaManager>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AreaManager>> index(QueryModel queryModel) {
        List<AreaManager> list = areaManagerService.selectByModel(queryModel);
        return new ResultDto<List<AreaManager>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{areaNo}")
    protected ResultDto<AreaManager> show(@PathVariable("areaNo") String areaNo) {
        AreaManager areaManager = areaManagerService.selectByPrimaryKey(areaNo);
        return new ResultDto<AreaManager>(areaManager);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<AreaManager> create(@RequestBody AreaManager areaManager) throws URISyntaxException {
        areaManagerService.insert(areaManager);
        return new ResultDto<AreaManager>(areaManager);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<AreaManager> update(@RequestBody AreaManager areaManager){
        areaManagerService.update(areaManager);
        return new ResultDto<AreaManager>(areaManager);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{areaNo}")
    protected ResultDto<Integer> delete(@PathVariable("areaNo") String areaNo) {
        int result = areaManagerService.deleteByPrimaryKey(areaNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = areaManagerService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
    * @author zlf
    * @date 2021/5/26 14:29
    * @version 1.0.0
    * @desc   信息维护
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/save")
    protected ResultDto<AreaManager> save(@RequestBody AreaManager areaManager) {
        areaManagerService.save(areaManager);
        return new ResultDto<AreaManager>(areaManager);
    }

    @PostMapping("/selectexceptother")
    protected ResultDto<List<AreaManager>> selectExceptOther(@RequestBody QueryModel queryModel) {
        List<AreaManager> list =areaManagerService.selectExceptOther(queryModel);
        return new ResultDto<List<AreaManager>>(list);
    }
    /**
    * @author zlf
    * @date 2021/6/3 9:33
    * @version 1.0.0
    * @desc    删除所有关联信息
    * @修改历史  修改时间 修改人员 修改原因
    */
    @ApiOperation("删除区域所有关联信息")
    @PostMapping("/deleteall")
    protected ResultDto deleteAll(@RequestBody AreaManager areaManager) {
        return areaManagerService.deleteAll(areaManager.getAreaNo());
    }
    /**
    * @author zlf
    * @date 2021/6/16 14:02
    * @version 1.0.0
    * @desc     分页查询
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/querymodel")
    protected ResultDto<List<AreaManager>> queryModel(@RequestBody QueryModel queryModel) {
        List<AreaManager> list = areaManagerService.selectByModel(queryModel);
        return new ResultDto<List<AreaManager>>(list);
    }

}
