package cn.com.yusys.yusp.service.server.xdls0001;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002Resp;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0002.Cmisbatch0002RespDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009Resp;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0009.Cmisbatch0009RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.dto.AccLoanDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdls0001.req.Xdls0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0001.resp.Xdls0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpHouseMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:房贷要素查询
 *
 * @author
 * @version 1.0
 */
@Service
public class Xdls0001Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdls0006.Xdls0006Service.class);

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private IqpHouseMapper iqpHouseMapper;

    @Autowired
    private CmisCus0006Service cmisCus0006Service;

    /**
     * 房贷要素
     *
     * @param xdls0001DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdls0001DataRespDto xdls0001(Xdls0001DataReqDto xdls0001DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, JSON.toJSONString(xdls0001DataReqDto));
        //返回对象
        Xdls0001DataRespDto xdls0001DataRespDto = new Xdls0001DataRespDto();
        CmisCus0006ReqDto cmisCus0006ReqDto = new CmisCus0006ReqDto();//请求Dto：查询客户基本信息
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();//响应Dto：查询客户基本信息
        try {
            String cusName = xdls0001DataReqDto.getCusName();//客户名称
            String certNo = xdls0001DataReqDto.getCertNo();//证件号码
            String first_day = "";

            //通过客户证件号查询客户信息
            cmisCus0006ReqDto.setCertCode(certNo);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
            cmisCus0006RespDto = cmisCus0006Service.cmisCus0006(cmisCus0006ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
            List<CusBaseDto> cusBaseList = cmisCus0006RespDto.getCusBaseList();
            // 客户ids
            String cusId = cusBaseList.parallelStream()
                    .filter(cusBaseDto -> Objects.equals(certNo, cusBaseDto.getCertCode()))//判断 请求参数中客户证件号和cusBaseDto中证件号是否相等
                    .map(cusBaseDto -> cusBaseDto.getCusId()).limit(1).collect(Collectors.joining(StringUtils.EMPTY));
            ; // 获取cusBaseDto中客户号
            // 整理的新信贷语句
            // select a.bill_no,a.cont_no from acc_loan a  where a.cus_name='' and a.cus_id='' and a.prd_id in('022002','022001','022040');
            // select replace(b.dkqixian,'M','') tnr from bat_s_core_klna_dkzhzb b where b.dkzhangh='billno';
            // select c.daoqriqi ps_due_dt from bat_s_core_klnb_dkzhqg c where c.dkzhangh='billno' and  c.benqqish=1;
            logger.info("**********XDLS0001**房贷要素记录查询开始,查询参数为:{}", JSON.toJSONString(cusId));
            List<AccLoanDto> accLoanDtos = accLoanMapper.queryHouseAccLoanListByCus(cusId, cusName);
            java.util.List<List> queryResult = new java.util.ArrayList<List>();
            if (CollectionUtils.isNotEmpty(accLoanDtos)) {
                List<cn.com.yusys.yusp.dto.server.xdls0001.resp.List> xdlsList = new ArrayList<>();
                for (int i = 0; i < accLoanDtos.size(); i++) {
                    AccLoanDto accLoanDto = accLoanDtos.get(i);
                    String billNo = accLoanDto.getBillNo();
                    List<Cmisbatch0002Resp> cmisbatch0002List = cmisbatch0002(billNo, cusId).getCmisbatch0002List();
                    String dkqixian = "";
                    if (null != cmisbatch0002List && cmisbatch0002List.size() > 0) {
                        dkqixian = cmisbatch0002List.get(0).getDkqixian();
                    }
//                   String dkqixian="";
//                   (需要查第一期的还款日),新加接口cmisbatch0009
                    List<Cmisbatch0009Resp> cmisbatch0009List = Optional.ofNullable(cmisbatch0009(billNo).getCmisbatch0009List()).orElse(new ArrayList<>());
                    if (CollectionUtils.isNotEmpty(cmisbatch0009List)) {
                        first_day = cmisbatch0009List.get(0).getDaoqriqi();
                    }
                    // 合同号
                    String contNo = accLoanDto.getContNo();
                    List<IqpHouse> iqpHouses = iqpHouseMapper.selectByContNo(contNo);
                    String purHouseNum = "";
                    if (CollectionUtils.isNotEmpty(iqpHouses)) {
                        for (int j = 0; j < iqpHouses.size(); j++) {
                            purHouseNum = String.valueOf(iqpHouses.get(j).getPurHouseNum());
                        }
                    }
                    cn.com.yusys.yusp.dto.server.xdls0001.resp.List xdls0001List = new cn.com.yusys.yusp.dto.server.xdls0001.resp.List();
                    xdls0001List.setContNo(contNo);
                    xdls0001List.setFirstRepayDate(first_day);
                    xdls0001List.setIsFirstHouse(purHouseNum);
                    xdls0001List.setTerm(dkqixian);
                    xdlsList.add(xdls0001List);
                }
                xdls0001DataRespDto.setList(xdlsList);
            }


        }catch (BizException e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, e.getMessage());
            throw BizException.error(null, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value);
        return xdls0001DataRespDto;
    }

    public Cmisbatch0002RespDto cmisbatch0002(String billNo, String cusId) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        Cmisbatch0002RespDto cmisbatch0002RespDto = new Cmisbatch0002RespDto();

        try {
            Cmisbatch0002ReqDto cmisbatch0002ReqDto = new Cmisbatch0002ReqDto();
            cmisbatch0002ReqDto.setDkjiejuh(billNo);
            cmisbatch0002ReqDto.setKehuhaoo(cusId);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002ReqDto));
            ResultDto<Cmisbatch0002RespDto> cmisbatch0002RespDtoResultDto = cmisBatchClientService.cmisbatch0002(cmisbatch0002ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, JSON.toJSONString(cmisbatch0002RespDtoResultDto));

            cmisbatch0002RespDto = Optional.ofNullable(cmisbatch0002RespDtoResultDto).orElse(new ResultDto<Cmisbatch0002RespDto>()).getData();
            if (null == cmisbatch0002RespDto) {
                //  抛出错误异常
                //throw new YuspException(DscmsEnum.TRADE_CODE_CMISBATCH0002.key, "无数据");
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value, e.getMessage());
            throw BizException.error(null, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0002.key, DscmsEnum.TRADE_CODE_CMISBATCH0002.value);
        return cmisbatch0002RespDto;
    }

    public Cmisbatch0009RespDto cmisbatch0009(String billNo) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value);
        Cmisbatch0009RespDto cmisbatch0009RespDto = new Cmisbatch0009RespDto();
        try {
            Cmisbatch0009ReqDto cmisbatch0009ReqDto = new Cmisbatch0009ReqDto();
            cmisbatch0009ReqDto.setDkjiejuh(billNo);
            cmisbatch0009ReqDto.setBenqqish("1");
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009ReqDto));
            ResultDto<Cmisbatch0009RespDto> cmisbatch0009RespDtoResultDto = cmisBatchClientService.cmisbatch0009(cmisbatch0009ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, JSON.toJSONString(cmisbatch0009RespDtoResultDto));

            cmisbatch0009RespDto = Optional.ofNullable(cmisbatch0009RespDtoResultDto).orElse(new ResultDto<Cmisbatch0009RespDto>()).getData();
            if (null == cmisbatch0009RespDto) {
                //  抛出错误异常
                //throw new YuspException(DscmsEnum.TRADE_CODE_CMISBATCH0009.key, "无数据");
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value, e.getMessage());
            throw BizException.error(null, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0009.key, DscmsEnum.TRADE_CODE_CMISBATCH0009.value);
        return cmisbatch0009RespDto;
    }
}
