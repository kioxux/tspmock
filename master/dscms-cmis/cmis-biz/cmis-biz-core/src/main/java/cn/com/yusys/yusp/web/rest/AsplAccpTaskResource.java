/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.AsplAccpDto;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.service.AsplAccpTaskService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccpTaskResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:19:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/asplaccptask")
public class AsplAccpTaskResource {
    @Autowired
    private AsplAccpTaskService asplAccpTaskService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplAccpTask>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplAccpTask> list = asplAccpTaskService.selectAll(queryModel);
        return new ResultDto<List<AsplAccpTask>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplAccpTask>> index(QueryModel queryModel) {
        List<AsplAccpTask> list = asplAccpTaskService.selectByModel(queryModel);
        return new ResultDto<List<AsplAccpTask>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplAccpTask> show(@PathVariable("pkId") String pkId) {
        AsplAccpTask asplAccpTask = asplAccpTaskService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplAccpTask>(asplAccpTask);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplAccpTask> create(@RequestBody AsplAccpTask asplAccpTask) throws URISyntaxException {
        asplAccpTaskService.insert(asplAccpTask);
        return new ResultDto<AsplAccpTask>(asplAccpTask);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplAccpTask asplAccpTask) throws URISyntaxException {
        int result = asplAccpTaskService.update(asplAccpTask);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplAccpTaskService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletebytaskid/{taskId}")
    protected ResultDto<Integer> deletebytaskid(@PathVariable("taskId") String taskId) {
        int result = asplAccpTaskService.deletebytaskid(taskId);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplAccpTaskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: asplaccpchecklist
     * @函数描述: 贸易背景资料审核
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("贸易背景资料审核")
    @PostMapping("/asplaccptasklist")
    protected ResultDto<List<AsplAccpTask>> asplAccpTasklist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<AsplAccpTask> list = asplAccpTaskService.asplAccpTasklist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAccpTask>>(list);
    }


    /**
     * @函数名称: asplaccpchecklist
     * @函数描述: 贸易背景收集新增
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("贸易背景收集新增")
    @PostMapping("/addasplaccptask")
    protected ResultDto<AsplAccpTask> addAsplAccpTask(@RequestBody AsplAccpTask asplAccpTask ) {
        ResultDto<AsplAccpTask> result = asplAccpTaskService.addAsplAccpTask(asplAccpTask);
        return result;
    }

    /**
     * @函数名称: showdetails
     * @函数描述: 根据任务编号查询购销背景详情
     * @参数与返回说明:
     * @算法描述:
     * @创建人: xs
     * @创建时间: 2021-09-4 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据任务编号查询购销背景详情")
    @PostMapping("/select/taskid/{taskId}")
    protected ResultDto<AsplAccpTask> selectByTaskId(@PathVariable("taskId") String taskId) {
        AsplAccpTask asplAccpTask = asplAccpTaskService.selectByTaskId(taskId);
        return new ResultDto<AsplAccpTask>(asplAccpTask);
    }
}
