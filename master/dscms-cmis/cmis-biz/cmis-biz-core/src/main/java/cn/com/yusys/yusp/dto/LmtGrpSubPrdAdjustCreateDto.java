package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustCreateDto
 * @类描述: 调剂额度关系创建Dto
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-05-11 23:05:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGrpSubPrdAdjustCreateDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 集团流水号 **/
	private String grpSerno;

	/** 集团成员编号 **/
	private String cusIds;


	public String getGrpSerno() {
		return grpSerno;
	}

	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	public String getCusIds() {
		return cusIds;
	}

	public void setCusIds(String cusIds) {
		this.cusIds = cusIds;
	}

	@Override
	public String toString() {
		return "LmtGrpSubPrdAdjustCreateDto{" +
				"grpSerno='" + grpSerno + '\'' +
				", cusIds='" + cusIds + '\'' +
				'}';
	}
}