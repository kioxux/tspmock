/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OnlineCvrgCusInfo;
import cn.com.yusys.yusp.service.OnlineCvrgCusInfoService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OnlineCvrgCusInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 20:50:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/onlinecvrgcusinfo")
public class OnlineCvrgCusInfoResource {
    @Autowired
    private OnlineCvrgCusInfoService onlineCvrgCusInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OnlineCvrgCusInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<OnlineCvrgCusInfo> list = onlineCvrgCusInfoService.selectAll(queryModel);
        return new ResultDto<List<OnlineCvrgCusInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OnlineCvrgCusInfo>> index(QueryModel queryModel) {
        List<OnlineCvrgCusInfo> list = onlineCvrgCusInfoService.selectByModel(queryModel);
        return new ResultDto<List<OnlineCvrgCusInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OnlineCvrgCusInfo> show(@PathVariable("serno") String serno) {
        OnlineCvrgCusInfo onlineCvrgCusInfo = onlineCvrgCusInfoService.selectByPrimaryKey(serno);
        return new ResultDto<OnlineCvrgCusInfo>(onlineCvrgCusInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OnlineCvrgCusInfo> create(@RequestBody OnlineCvrgCusInfo onlineCvrgCusInfo) throws URISyntaxException {
        onlineCvrgCusInfoService.insert(onlineCvrgCusInfo);
        return new ResultDto<OnlineCvrgCusInfo>(onlineCvrgCusInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OnlineCvrgCusInfo onlineCvrgCusInfo) throws URISyntaxException {
        int result = onlineCvrgCusInfoService.update(onlineCvrgCusInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = onlineCvrgCusInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = onlineCvrgCusInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:onlineCvrgCusInfolist
     * @函数描述: 获取在线投标保函预约管理在途预约信息
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取在线投标保函预约管理在途预约")
    @PostMapping("/onlineCvrgCusInfolist")
    protected ResultDto<List<OnlineCvrgCusInfo>> onlineCvrgCusInfolist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("input_time desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<OnlineCvrgCusInfo> list = onlineCvrgCusInfoService.onlineCvrgCusInfolist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<OnlineCvrgCusInfo>>(list);
    }

    /**
     * @函数名称:onlineCvrgCusInfoHislist
     * @函数描述: 获取在线投标保函预约管理在历史预约信息
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取在线投标保函预约管理在历史预约信息")
    @PostMapping("/onlineCvrgCusInfoHislist")
    protected ResultDto<List<OnlineCvrgCusInfo>> onlineCvrgCusInfoHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("input_time desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<OnlineCvrgCusInfo> list = onlineCvrgCusInfoService.onlineCvrgCusInfoHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<OnlineCvrgCusInfo>>(list);
    }

    /**
     * @方法名称: queryOnlineCvrgCusInfoByParams
     * @方法描述: 根据入参查询在线投标保函预约
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询数据")
    @PostMapping("/queryOnlineCvrgCusInfoByParams")
    protected ResultDto<OnlineCvrgCusInfo> queryOnlineCvrgCusInfoByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        OnlineCvrgCusInfo onlineCvrgCusInfo = onlineCvrgCusInfoService.queryOnlineCvrgCusInfoByParams(queryData);
        return new ResultDto<OnlineCvrgCusInfo>(onlineCvrgCusInfo);
    }

    /**
     * @函数名称:onlineCvrgCusInfoUpdate
     * @函数描述:在线投标保函预约管理分配任务
     * @参数与返回说明:
     * @算法描述:
     * @创建者:zhangliang15
     */
    @ApiOperation("在线投标保函预约管理分配任务")
    @PostMapping("/onlineCvrgCusInfoUpdate")
    protected ResultDto<Map> onlineCvrgCusInfoUpdate(@RequestBody OnlineCvrgCusInfo onlineCvrgCusInfo) throws URISyntaxException {
        Map rtnData = onlineCvrgCusInfoService.onlineCvrgCusInfoUpdate(onlineCvrgCusInfo);
        return new ResultDto<>(rtnData);
    }
}
