package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AsplWhtls;
import cn.com.yusys.yusp.domain.IqpAppAspl;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultCommentDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @className ZCYW04BizService
 * @Description 资产池协议申请
 * @Date 2020/12/21 : 10:43
 */
@Service
public class ZCYW04BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(ZCYW04BizService.class);

    @Autowired
    private IqpAppAsplService iqpAppAsplService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;



    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        if(CmisFlowConstants.FLOW_ID_ZC004.equals(resultInstanceDto.getBizType())){
            this.handleZC004Biz(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_ID_ZC005.equals(resultInstanceDto.getBizType())){
            this.handleZC005Biz(resultInstanceDto);
        }
    }

    /**
     * 资产池协议变更
     * @param resultInstanceDto
     */
    private void handleZC005Biz(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        try {
            IqpAppAspl iqpAppAspl = iqpAppAsplService.selectBySerno(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                // 正常提交下一步处理   审批中 111
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpAppAsplService.updateSelective(iqpAppAspl);
                // 档案池生成
                sendCreateCentralFileTask(resultInstanceDto,iqpAppAspl);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                iqpAppAsplService.handleBusinessDataAfterEnd(iqpAppAspl);
                sendImage(resultInstanceDto);
                // 推送用印系统
                try {
                    cmisBizXwCommonService.sendYk(resultInstanceDto.getCurrentUserId(),iqpSerno,iqpAppAspl.getCusName());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程退回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程打回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "流程拿回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "拿回初始节点操作，流程参数" + resultInstanceDto);
                //流程拿回到第一个节点，申请主表的业务
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpAppAsplService.updateSelective(iqpAppAspl);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("资产池协议变更申请" + iqpSerno + "否决操作，流程参数" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpAppAsplService.updateSelective(iqpAppAspl);
            } else {
                log.info("资产池协议变更申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("资产池协议变更申请,后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 资产池协议申请
     * @param resultInstanceDto
     */
    private void handleZC004Biz(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        try {
            IqpAppAspl iqpAppAspl = iqpAppAsplService.selectBySerno(iqpSerno);
            // 获取当前业务申请路程类型
            String flowCode = resultInstanceDto.getBizType();
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                // 正常提交下一步处理   审批中 111
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpAppAsplService.updateSelective(iqpAppAspl);
                // 档案池生成
                sendCreateCentralFileTask(resultInstanceDto,iqpAppAspl);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("资产池协议申请申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                // 生成资产池协议台账
                iqpAppAsplService.handleBusinessDataAfterEnd(iqpAppAspl);
                // 发送影像审核
                sendImage(resultInstanceDto);
                // 推送用印系统
                try {
                    cmisBizXwCommonService.sendYk(resultInstanceDto.getCurrentUserId(),iqpSerno,iqpAppAspl.getCusName());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "流程退回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "流程打回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "流程拿回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpAppAsplService.updateSelective(iqpAppAspl);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "拿回初始节点操作，流程参数" + resultInstanceDto);
                //流程拿回到第一个节点，申请主表的业务
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpAppAsplService.updateSelective(iqpAppAspl);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("资产池协议申请" + iqpSerno + "否决操作，流程参数" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpAppAsplService.updateSelective(iqpAppAspl);
            } else {
                log.info("资产池协议申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "ZCYW04".equals(flowCode);
    }

    /**
     * 推送影像审批信息
     * @author xs
     * @date 2021-10-04 19:52:42
     **/
    private void sendImage(ResultInstanceDto resultInstanceDto) {
        //根据流程实例获取所有审批意见
        ResultDto<List<ResultCommentDto>> resultCommentDtos = workflowCoreClient.getAllComments(resultInstanceDto.getInstanceId());
        List<ResultCommentDto> data = resultCommentDtos.getData();
        //审批人
        String approveUserId = "";
        for (ResultCommentDto resultCommentDto : data) {
            String nodeId = resultCommentDto.getNodeId();
            if ("358_20".equals(nodeId)){//集中作业放款复审岗
                String userId = resultCommentDto.getUserId();
                if(!approveUserId.contains(userId)){
                    //审批人不能重复
                    approveUserId = approveUserId+","+userId;
                }
            }
        }
        if(approveUserId.length() > 0){
            // 一般情况下，直接获取流程实列中的参数。
            Map<String, Object> map = (Map) JSON.parse(resultInstanceDto.getFlowParam());
            String topOutsystemCode = (String) map.get("topOutsystemCode");
            if(StringUtils.isEmpty(topOutsystemCode)){
                throw BizException.error(null, "9999", "影像审核参数【topOutsystemCode】为空");
            }
            Map<String, String> imageParams = (Map<String, String>) map.get("imageParams");
            String docid = imageParams.get("businessid");
            if(StringUtils.isEmpty(docid)){
                throw BizException.error(null, "9999", "影像审核参数【businessid】为空");
            }
            approveUserId = approveUserId.substring(1);
            String[] arr = topOutsystemCode.split(";");
            for (int i = 0; i < arr.length; i++) {
                ImageApprDto imageApprDto = new ImageApprDto();
                imageApprDto.setDocId(docid);//任务编号
                imageApprDto.setApproval("同意");//审批意见
                imageApprDto.setIsApproved("1");//审批状态1通过-1不通过3作废
                imageApprDto.setOutcode(arr[i]);//文件类型根节点
                imageApprDto.setOpercode(approveUserId);//审批人员
                cmisBizXwCommonService.sendImage(imageApprDto);
            }
        }
    }

    /**
     * 生成档案池
     * @author xs
     * @date 2021-11-02 19:52:42
     **/
    private void sendCreateCentralFileTask(ResultInstanceDto resultInstanceDto,IqpAppAspl iqpAppAspl) {
        if("358_5".equals(resultInstanceDto.getCurrentNodeId())) {
            try {
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpAppAspl.getInputBrId());
                AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())){
                    log.info("异地机构无法生成档案池任务");
                } else {
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(iqpAppAspl.getSerno());
                    centralFileTaskdto.setCusId(iqpAppAspl.getCusId());
                    centralFileTaskdto.setCusName(iqpAppAspl.getCusName());
                    centralFileTaskdto.setBizType(resultInstanceDto.getBizType()); // 合作协议资料
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    centralFileTaskdto.setInputId(iqpAppAspl.getInputId());
                    centralFileTaskdto.setInputBrId(iqpAppAspl.getInputBrId());
                    centralFileTaskdto.setOptType("02"); // 非纯指令
                    centralFileTaskdto.setTaskType("02"); // 档案暫存
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    ResultDto<Integer> centralFileTask = cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                    if(!centralFileTask.getCode().equals("0")){
                        log.info("生成档案池任务失败" +centralFileTask.getMessage());
                    }
                }
            } catch (Exception e) {
                log.info("生成档案池任务失败"+ e);
            }
        }
    }
}
