/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdDrawCheckService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zl
 * @创建时间: 2021-08-11 22:06:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class SxkdDrawCheckService {
    private static final Logger log = LoggerFactory.getLogger(SxkdDrawCheckService.class);
    @Autowired
    private SxkdDrawCheckMapper sxkdDrawCheckMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;
    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Resource
    private OtherCnyRateAppFinDetailsMapper otherCnyRateAppFinDetailsMapper;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public SxkdDrawCheck selectByPrimaryKey(String serno) {
        return sxkdDrawCheckMapper.selectByPrimaryKey(serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<SxkdDrawCheck> selectAll(QueryModel model) {
        List<SxkdDrawCheck> records = (List<SxkdDrawCheck>) sxkdDrawCheckMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<SxkdDrawCheck> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<SxkdDrawCheck> list = sxkdDrawCheckMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(SxkdDrawCheck record) {
        return sxkdDrawCheckMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(SxkdDrawCheck record) {
        return sxkdDrawCheckMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(SxkdDrawCheck record) {
        return sxkdDrawCheckMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(SxkdDrawCheck record) {
        return sxkdDrawCheckMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return sxkdDrawCheckMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return sxkdDrawCheckMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: sxkdDrawChecklist
     * @方法描述: 查询审核结果为空的数据（获取房抵e点贷受托信息待审核）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<SxkdDrawCheck> sxkdDrawChecklist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("auditStatusIsNull", 1);
        return sxkdDrawCheckMapper.selectByModel(model);
    }
    /**

     * @方法名称: sxkdDrawCheckHislist
     * @方法描述:  查询审批状态为通过、否决、自行退出数据（省心快贷提交审核）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<SxkdDrawCheck> sxkdDrawCheckHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("auditStatusIsNotNull", 1);
        return sxkdDrawCheckMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public SxkdDrawCheck selectBySerno(String serno) {
        return sxkdDrawCheckMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: sxkdDrawCheckBackOut
     * @方法描述: 省心快贷提款审核撤销出账
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map sxkdDrawCheckBackOut(SxkdDrawCheck sxkdDrawCheck) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String pvpSerno = "";
        try {
            pvpSerno = sxkdDrawCheck.getPvpSerno();
            //根据出账流水获取授权信息
            PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            log.info(String.format("省心快贷提款审核撤销出账_删除未出账的通知出账记录、未出账的贷款台账记录以及额度视图开始"));
            pvpAuthorizeService.handInvaild(pvpAuthorize);
            log.info(String.format("省心快贷提款审核撤销出账_删除未出账的通知出账记录、未出账的贷款台账记录以及额度视图结束"));

//            //更新放款状态为作废
//            sxkdDrawCheck.setPvpStatus("3");
//            log.info(String.format("省心快贷提款审核更新放款状态开始"));
//            int count = sxkdDrawCheckMapper.updateByPrimaryKey(sxkdDrawCheck);
//            if (count != 1) {
//                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
//                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷提款审核撤销出账失败！");
//            }
            log.info(String.format("省心快贷提款审核撤销更新放款状态结束"));

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("省心快贷提款审核撤销出账出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: sxkdToHxFk
     * @方法描述: 省心快贷提款审核发送核心放款
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     * @校验：
     */
    @Transactional(rollbackFor = Exception.class)
    public Map sxkdToHxFk(SxkdDrawCheck record) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        String pvpSerno = "";
        BigDecimal contBalance = BigDecimal.ZERO;
        try {
            pvpSerno = record.getPvpSerno();
            //1.放款
            //根据出账流水获取授权信息
            log.info("根据放款流水号【{}】查询授权信息开始", pvpSerno);
            PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            if (pvpAuthorize == null) {
                log.info("根据放款流水号【{}】未查询授权信息", pvpSerno);
                rtnCode = EcbEnum.ECB020060.key;
                rtnMsg = EcbEnum.ECB020060.value;
                return rtnData;
            }
            log.info("根据放款流水号【{}】查询授权信息结束", pvpSerno);

            log.info("省心快贷提款审核发送核心放款" + pvpSerno + "开始");
            pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
            log.info("省心快贷提款审核发送核心放款" + pvpSerno + "结束");

            // 2.省心快贷线上提款，信贷系统点击通知核心核心出账，影像补扫中生成一条待发起的影像补扫记录；（信贷系统发起线下的出账无需生成补扫）
            log.info("省心快贷线上提款生成一条待发起的影像补扫记录开始,业务流水号【{}】", pvpSerno);
            pvpLoanAppService.sxkdOnlinecreateImageSpplInfo(pvpSerno);
            log.info("省心快贷线上提款生成一条待发起的影像补扫记录结束,业务流水号【{}】", pvpSerno);
//            // 省心快贷提款审核 放款状态
//            record.setPvpStatus("2"); //已放款
//
//            // 获取修改日期
//            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
//            log.info("省心快贷提款审核落表开始" + record);
//            int count = sxkdDrawCheckMapper.updateByPrimaryKey(record);
//            if (count != 1) {
//                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
//                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷提款审核提交失败！");
//            }
            log.info("省心快贷提款审核落表结束" + record);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("省心快贷提款审核发送核心放款异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @param contNo
     * @return BigDecimal
     * @author zl
     * @date 2021/10/14 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可用金额
     * @修改历史:
     */
    public BigDecimal getContBalance(String contNo) {
        //可出账金额
        BigDecimal contBalance = BigDecimal.ZERO;
        CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
        if (Objects.isNull(ctrLoanCont)) {
            throw BizException.error(null, EcbEnum.ECB020025.key, EcbEnum.ECB020025.value);
        }
        // 根据合同号调额度接口查询合同项下余额
        CmisLmt0060ReqDto cmisLmt0060ReqDto = new CmisLmt0060ReqDto();
        cmisLmt0060ReqDto.setDealBizNo(contNo);
        log.info("根据合同编号【{}】，前往额度系统查询合同占额开始,请求报文为:【{}】", contNo, cmisLmt0060ReqDto.toString());
        ResultDto<CmisLmt0060RespDto> resultDtoDto = cmisLmtClientService.cmislmt0060(cmisLmt0060ReqDto);
        log.info("根据合同编号【{}】，前往额度系统查询合同占额结束,响应报文为:【{}】", contNo, resultDtoDto.toString());
        if(!"0".equals(resultDtoDto.getCode())){
            log.error("cmisLmt60接口调用失败！");
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",前往额度系统查询合同余额失败！");
        }
        //获取初始占用敞口余额
        BigDecimal spacBalanceCny = resultDtoDto.getData().getSpacBalanceCny();
        contBalance = ctrLoanCont.getContAmt().subtract(spacBalanceCny);
        return contBalance;
    }


    /**
     * @方法名称: sxkdDrawChecksubmit
     * @方法描述: 省心快贷提款审核出账详情提交
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhangliang15
     * @创建时间: 2021-08-05 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    // 取可用余额，当放款余额大于可用余额时，提示错误.否则，更新审批状态为通过
    // pvp_loan_app 出账申请落表
    @Transactional(rollbackFor = Exception.class)
    public Map sxkdDrawChecksubmit(SxkdDrawCheck record) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        String contNo = "";
        String openDay = "";
        String serno = "";//审核表流水
        String pvpSerno = "";//出账流水
        // 合同余额
        BigDecimal contBalance = BigDecimal.ZERO;
        // 省心贷放款金额
        BigDecimal pvpAmt = BigDecimal.ZERO;
        try {
            log.info("省心快贷提款审核" + record.getSerno() + "-审核开始！");
            serno = record.getSerno();
            pvpSerno = record.getPvpSerno();
            pvpAmt = record.getPvpAmt();
            // 获取借款合同号
            contNo = record.getContNo();
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return rtnData;
            }
            // 老信贷取得可用金额逻辑：取线上合同申请金额 与 授信台账额度之间的较小值，减去贷款台账余额后，得到可用金额
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            if (ctrLoanCont == null) {
                throw new YuspException(EcbEnum.E_PVP_UPDATE_CONTNULL_EXCEPTION.key, EcbEnum.E_PVP_UPDATE_CONTNULL_EXCEPTION.value);
            }
            // 可用金额取值
            contBalance = this.getContBalance(contNo);
            // 可用余额<放款金额
            if (contBalance.compareTo(pvpAmt) < 0) {
                  rtnCode = EcbEnum.ECB020021.key;
                  rtnMsg = EcbEnum.ECB020021.value;
                  return rtnData;
            }

            // 获取营业日期
            openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 审核时间
            record.setAuditDate(openDay);
            // 审批状态
//            record.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            // 放款状态
            record.setPvpStatus("1");// 1:未放款
            // 获取修改日期
            record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info("省心快贷提款审核落表开始" + record);
            int count = sxkdDrawCheckMapper.updateByPrimaryKey(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷提款审核提交失败！");
            }
            log.info("省心快贷提款审核落表结束" + record);
            // 1:通过 0:不通过 当审核状态为通过时，生成出账申请表
            if ("1".equals(record.getAuditStatus())){
                // 根据合同表授信额度编号查询人民币定价利率申请表信息
                log.info("根据合同号获取人民币定价利率信息开始",contNo);
                OtherCnyRateAppFinDetails otherCnyRateAppFinDetails =  otherCnyRateAppFinDetailsMapper.selectByLmtAccNo(contNo);
                log.info("根据合同号获取人民币定价利率信息结束",otherCnyRateAppFinDetails);
                if (Objects.isNull(otherCnyRateAppFinDetails) || StringUtils.isBlank(otherCnyRateAppFinDetails.getExecRateYear().toString())){
                    log.info("信贷系统人民币利率定价申请未维护！请维护");
                    rtnCode = EcbEnum.ECB020059.key;
                    rtnMsg = EcbEnum.ECB020059.value;
                    return rtnData;
                }
                // 获取LPR利率 省心快贷默认取一年期Lpr利率
                BigDecimal db_ir_reality_ir_y = queryLprRate("A1");
                otherCnyRateAppFinDetails.setCurLpr(db_ir_reality_ir_y);

                // 组装出账申请表
                PvpLoanApp pvpLoanApp  = this.getPvpLoanAPP(ctrLoanCont,record,userInfo,pvpSerno,otherCnyRateAppFinDetails);
                log.info("省心快贷提款审核生成出账申请表开始" + pvpLoanApp);
                // 出账申请表落表
                int insertCount = pvpLoanAppMapper.insertSelective(pvpLoanApp);
                if (insertCount <= 0) {
                    //若是出现异常则需要回滚，因此直接抛出异常
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",省心快贷提款审核提交失败,出账申请表落表异常！");
                }
                log.info("省心快贷提款审核生成出账申请表结束" + pvpLoanApp);
                // 放款前占额
                log.info("省心快贷提款审核占额开始" + pvpSerno + "开始");
                pvpLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                log.info("省心快贷提款审核占额结束" + pvpSerno + "结束");
                /**
                 * 放款申请通过，后续的业务处理
                 * 1、针对单笔单批业务，更新审批模式子表中的流程状态
                 * 2、生成借据数据，状态为【放款待确认】
                 * 0、放款申请主表申请状态修改为通过-997
                 * 3、根据支付方式生成对应的授权记录
                 * 4、若是受托支付，进行受托支付业务处理
                 * 5、获取账户列表数据，生成账号信息与借据关系表
                 */
                log.info("省心快贷提款审核生成出账通知和台账" + pvpSerno + "开始");
                pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
                log.info("省心快贷提款审核生成出账通知和台账" + pvpSerno + "结束");
            }
            log.info("省心快贷提款审核" + record.getSerno() + "-审核结束！");
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("省心快贷提款审核提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("serno", serno);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(String newVal) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0.0485");
        try {
            log.info("根据贷款期限查询lpr利率开始{}", newVal);
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
                log.info("根据贷款期限查询lpr利率开始{}", curtLprRate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }

    // 组装出账申请表信息
    private PvpLoanApp getPvpLoanAPP(CtrLoanCont ctrLoanCont, SxkdDrawCheck sxkdDrawCheck, User userInfo, String pvpSerno,OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) {
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        //合同对象克隆给放款对象
        BeanUtils.copyProperties(ctrLoanCont, pvpLoanApp);
        pvpLoanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //申请状态
        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        //操作类型
        pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        //合同起始日
        pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
        //合同到期日
        pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
        //利率调整方式
        pvpLoanApp.setRateAdjMode(ctrLoanCont.getIrAdjustType());
        //利率调整选项
        pvpLoanApp.setRateAdjType(ctrLoanCont.getIrFloatType());
        //LPR利率区间 省心快贷默认一年期
        pvpLoanApp.setLprRateIntval("A1");
        //浮动点数
        pvpLoanApp.setRateFloatPoint(ctrLoanCont.getRateFloatPoint());
        //结息间隔周期单位
        pvpLoanApp.setEiIntervalUnit(ctrLoanCont.getEiMode());
        //扣款扣息方式->扣款方式
        pvpLoanApp.setDeductType(ctrLoanCont.getDeductDeduType());
        //还款日->扣款日
        pvpLoanApp.setDeductDay(ctrLoanCont.getRepayDate());
        //逾期浮动
        pvpLoanApp.setOverdueRatePefloat(ctrLoanCont.getOverdueRatePefloat());
        //逾期浮动(年)
        pvpLoanApp.setOverdueExecRate(ctrLoanCont.getOverdueExecRate());
        //借款用途类型
        //pvpLoanApp.setLoanType(ctrLoanCont.getLoanPurp());
        //审批状态
        pvpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
        //担保方式
        pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());
        //贷款形式--默认新增
        pvpLoanApp.setLoanModal(ctrLoanCont.getLoanModal());
        //贷款期限类型 省心快贷默认月
        pvpLoanApp.setLoanTermUnit("M");
        //是否立即发起受托支付
        pvpLoanApp.setIsCfirmPay(ctrLoanCont.getIsCfirmPayWay());
        //工业转型升级标识
        pvpLoanApp.setIndtUpFlag(ctrLoanCont.getComUpIndtify());
        //文化产业标识
        pvpLoanApp.setCulIndustryFlag(ctrLoanCont.getIsCulEstate());
        //合同金额
        pvpLoanApp.setContAmt(ctrLoanCont.getContAmt());
        //合同币种
        pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
        //币种
        pvpLoanApp.setCurType("CNY");
        //所属条线
        pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
        //合同最高可放金额，取合同余额
        pvpLoanApp.setContHighDisb(ctrLoanCont.getHighAvlAmt());
        //节假日是否顺延
        pvpLoanApp.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);
        //登记人
        pvpLoanApp.setInputId(userInfo.getLoginCode());
        //登记机构
        pvpLoanApp.setInputBrId(userInfo.getOrg().getCode());
        //登记人
        pvpLoanApp.setManagerId(userInfo.getLoginCode());
        //登记机构
        pvpLoanApp.setManagerBrId(userInfo.getOrg().getCode());
        //借据编号
        pvpLoanApp.setBillNo(sxkdDrawCheck.getBillNo());
        //贷款期限
        try{
             pvpLoanApp.setLoanTerm(String.valueOf(cmisBizXwCommonService.getBetweenMonth(sxkdDrawCheck.getPvpDate(),sxkdDrawCheck.getBillEndDate())));
        }catch (Exception e){
        }
        //放款金额
        pvpLoanApp.setPvpAmt(sxkdDrawCheck.getPvpAmt());
        //贷款起始日
        pvpLoanApp.setLoanStartDate(sxkdDrawCheck.getPvpDate());
        //贷款到期日
        pvpLoanApp.setLoanEndDate(sxkdDrawCheck.getBillEndDate());
        //执行年利率
        pvpLoanApp.setExecRateYear(otherCnyRateAppFinDetails.getExecRateYear());
        //逾期利率浮动比
        pvpLoanApp.setOverdueRatePefloat(new BigDecimal("0.5"));
        //逾期执行利率
        pvpLoanApp.setOverdueExecRate((otherCnyRateAppFinDetails.getExecRateYear()).multiply(new BigDecimal("1.5")));
        //复息利率浮动比
        pvpLoanApp.setCiRatePefloat(new BigDecimal("0.5"));
        //复息执行利率
        pvpLoanApp.setCiExecRate((otherCnyRateAppFinDetails.getExecRateYear()).multiply(new BigDecimal("1.5")));
        //正常利率浮动方式 00 :LPR加点
        pvpLoanApp.setIrFloatType(CmisBizConstants.IR_FLOAT_TYPE_00);
        //浮动点数 （执行利率 - lpr利率） * 100
        pvpLoanApp.setRateFloatPoint(((otherCnyRateAppFinDetails.getExecRateYear()).subtract(otherCnyRateAppFinDetails.getCurLpr())).multiply(new BigDecimal(100)));
        // 当前LPR利率
        pvpLoanApp.setCurtLprRate(otherCnyRateAppFinDetails.getCurLpr());
        // 扣款日
        pvpLoanApp.setDeductDay("21");
        //结息间隔周期单位
        pvpLoanApp.setEiIntervalUnit("M"); //结息间隔周期单位
        //扣款扣息方式->扣款方式
        pvpLoanApp.setDeductType("AUTO");//扣款扣息方式->扣款方式
        //借款用途类型
        pvpLoanApp.setLoanTypeDetail(null);
        //计息周期
        pvpLoanApp.setEiIntervalCycle("1");
        // 信贷还款方式
        pvpLoanApp.setRepayMode("A001");
        // 所属条线
        pvpLoanApp.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
        // 是否资产池 1是 0 否
        pvpLoanApp.setIsPool("0");
        // 贷款发放账号
        pvpLoanApp.setLoanPayoutAccno(sxkdDrawCheck.getDisbAcct());
        // 贷款入账账号子序号
        pvpLoanApp.setLoanPayoutSubNo("00001");
        // 贷款发放账号名称
        pvpLoanApp.setPayoutAcctName(ctrLoanCont.getCusName());
        //贷款还款账号
        pvpLoanApp.setRepayAccno(sxkdDrawCheck.getDisbAcct());
        //贷款还款账户子序号
        pvpLoanApp.setRepaySubAccno("00001");
        //还款账户名称
        pvpLoanApp.setRepayAcctName(ctrLoanCont.getCusName());
        // 出账模式-默认为：客户经理手工出账 STD_PVP_MODE
        pvpLoanApp.setPvpMode("2");
        // 利率调整方式，默认为固定利率 STD_IR_ADJUST_TYPE
        pvpLoanApp.setRateAdjMode("01");
        // 是否受托支付 默认是
        pvpLoanApp.setIsBeEntrustedPay("1");
        // 放款机构编号
        pvpLoanApp.setDisbOrgNo(sxkdDrawCheck.getFinaBrId());
        // 放款机构名称
        pvpLoanApp.setDisbOrgName(StringUtils.EMPTY);
        // 财务机构
        pvpLoanApp.setFinaBrId(sxkdDrawCheck.getFinaBrId());
        // 财务机构名称
        pvpLoanApp.setFinaBrIdName(StringUtils.EMPTY);
        // 贷款投向
        pvpLoanApp.setLoanTer(sxkdDrawCheck.getLoanTer());
        // 折算人民币金额
        pvpLoanApp.setCvtCnyAmt(sxkdDrawCheck.getPvpAmt());
        // 是否分段计息
        pvpLoanApp.setIsSegInterest("0");
        //主键为放款流水号
        pvpLoanApp.setPvpSerno(pvpSerno);
        //业务流水号非空
        pvpLoanApp.setIqpSerno(ctrLoanCont.getIqpSerno());

        return pvpLoanApp;
    }
}
