/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.compatible.dto.def.UserInfoDTO;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.dto.LmtGrpSubPrdAdjustCreateDto;
import cn.com.yusys.yusp.dto.LmtGrpSubPrdAdjustRelAllListDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.LmtAppService;
import com.jcraft.jsch.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpSubPrdAdjustRel;
import cn.com.yusys.yusp.service.LmtGrpSubPrdAdjustRelService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustRelResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:25:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "集团成员额度调剂")
@RestController
@RequestMapping("/api/lmtgrpsubprdadjustrel")
public class LmtGrpSubPrdAdjustRelResource {
    @Autowired
    private LmtGrpSubPrdAdjustRelService lmtGrpSubPrdAdjustRelService;


    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpSubPrdAdjustRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpSubPrdAdjustRel> list = lmtGrpSubPrdAdjustRelService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpSubPrdAdjustRel>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpSubPrdAdjustRel>> index(QueryModel queryModel) {
        List<LmtGrpSubPrdAdjustRel> list = lmtGrpSubPrdAdjustRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpSubPrdAdjustRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpSubPrdAdjustRel> show(@PathVariable("pkId") String pkId) {
        LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel = lmtGrpSubPrdAdjustRelService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpSubPrdAdjustRel>(lmtGrpSubPrdAdjustRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpSubPrdAdjustRel> create(@RequestBody LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel) throws URISyntaxException {
        lmtGrpSubPrdAdjustRelService.insert(lmtGrpSubPrdAdjustRel);
        return new ResultDto<LmtGrpSubPrdAdjustRel>(lmtGrpSubPrdAdjustRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel) throws URISyntaxException {
        int result = lmtGrpSubPrdAdjustRelService.update(lmtGrpSubPrdAdjustRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpSubPrdAdjustRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpSubPrdAdjustRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: createLmtGrpSubPrdAdjustRelListInfo
     * @方法描述: 根据传入的cusIds的客户号调用创建额度调剂数据, cusIds为cusId用逗号拼接字符串
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据传入的cusIds的客户号调用创建额度调剂数据, cusIds为cusId用逗号拼接字符串")
    @PostMapping("/createlmtgrpsubprdadjustrellistinfo")
    protected ResultDto<Integer> createLmtGrpSubPrdAdjustRelListInfo(@RequestBody LmtGrpSubPrdAdjustCreateDto lmtGrpSubPrdAdjustCreateDto){
        return ResultDto.success(lmtGrpSubPrdAdjustRelService.createLmtGrpSubPrdAdjustRelListInfo(lmtGrpSubPrdAdjustCreateDto));
    }

    /**
     * @方法名称: queryLmtGrpSubPrdAdjustRelByGrpSerno
     * @方法描述: 根据集团授信申请流水号查询调剂数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团授信申请流水号查询调剂数据")
    @PostMapping("/querylmtgrpsubprdadjustrelbygrpserno")
    protected ResultDto<List<LmtGrpSubPrdAdjustRel>> queryLmtGrpSubPrdAdjustRelByGrpSerno(@RequestBody String grpSerno){
        return ResultDto.success().data(lmtGrpSubPrdAdjustRelService.queryLmtGrpSubPrdAdjustRelByGrpSerno(grpSerno));
    }

    /**
     * @方法名称: updateLmtGrpSubPrdAdjustRelData
     * @方法描述: 更新单条数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("更新单条数据")
    @PostMapping("/updatelmtgrpsubprdadjustreldata")
    protected ResultDto<Integer> updateLmtGrpSubPrdAdjustRelData(@RequestBody LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel){
        return ResultDto.success().data(lmtGrpSubPrdAdjustRelService.updateLmtGrpSubPrdAdjustRelData(lmtGrpSubPrdAdjustRel));
    }

    /**
     * @方法名称: updateLmtGrpSubPrdAdjustRelData
     * @方法描述: 校验调整前后的金额是否一致
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("校验调整前后的金额是否一致")
    @PostMapping("/verifyamtsumequalbefore")
    protected ResultDto<Integer> verifyAmtSumEqualBefore(@RequestBody String grpSerno){
        return ResultDto.success().data(lmtGrpSubPrdAdjustRelService.verifyAmtSumEqualBefore(grpSerno));
    }

    /**
     * @方法名称: updateAllListData
     * @方法描述: 更新列表数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-06-08 13:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("批量更新数据")
    @PostMapping("/updateAllListData")
    protected ResultDto<Integer> updateAllListData(@RequestBody LmtGrpSubPrdAdjustRelAllListDto lmtGrpSubPrdAdjustRelAllListDto){
        if(lmtGrpSubPrdAdjustRelAllListDto != null &&lmtGrpSubPrdAdjustRelAllListDto.getLmtGrpSubPrdAdjustRel().size() > 0){
            return ResultDto.success(lmtGrpSubPrdAdjustRelService.updateAllListData(lmtGrpSubPrdAdjustRelAllListDto.getLmtGrpSubPrdAdjustRel()));
        }else{
            return ResultDto.success(0);
        }
    }

}
