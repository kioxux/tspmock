package cn.com.yusys.yusp.service.server.xddb0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.YpList;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 接口处理类:信贷押品列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xddb0010Service {


    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    /**
     * 信贷押品列表查询
     * @param contNo
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public List<YpList> getContListByContNo(String contNo) {
        return grtGuarContMapper.getContListByContNo(contNo);
    }
}
