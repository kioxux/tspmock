/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetConInfo
 * @类描述: iqp_dis_asset_con_info数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-05-06 16:37:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_dis_asset_con_info")
public class IqpDisAssetConInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 家庭合计月收入 **/
	@Column(name = "FEARN_MEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fearnMearn;
	
	/** 该笔贷款月支出与家庭月收入比例 **/
	@Column(name = "INCOME_PERC1", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incomePerc1;
	
	/** 所有消费贷款月支出与家庭月收入比 **/
	@Column(name = "INCOME_PERC2", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incomePerc2;
	
	/** 还款能力 **/
	@Column(name = "REPAY_ABI", unique = false, nullable = true, length = 10)
	private String repayAbi;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param fearnMearn
	 */
	public void setFearnMearn(java.math.BigDecimal fearnMearn) {
		this.fearnMearn = fearnMearn;
	}
	
    /**
     * @return fearnMearn
     */
	public java.math.BigDecimal getFearnMearn() {
		return this.fearnMearn;
	}
	
	/**
	 * @param incomePerc1
	 */
	public void setIncomePerc1(java.math.BigDecimal incomePerc1) {
		this.incomePerc1 = incomePerc1;
	}
	
    /**
     * @return incomePerc1
     */
	public java.math.BigDecimal getIncomePerc1() {
		return this.incomePerc1;
	}
	
	/**
	 * @param incomePerc2
	 */
	public void setIncomePerc2(java.math.BigDecimal incomePerc2) {
		this.incomePerc2 = incomePerc2;
	}
	
    /**
     * @return incomePerc2
     */
	public java.math.BigDecimal getIncomePerc2() {
		return this.incomePerc2;
	}
	
	/**
	 * @param repayAbi
	 */
	public void setRepayAbi(String repayAbi) {
		this.repayAbi = repayAbi;
	}
	
    /**
     * @return repayAbi
     */
	public String getRepayAbi() {
		return this.repayAbi;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}


}