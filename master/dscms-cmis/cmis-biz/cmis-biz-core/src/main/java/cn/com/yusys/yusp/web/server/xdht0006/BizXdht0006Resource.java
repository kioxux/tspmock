package cn.com.yusys.yusp.web.server.xdht0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0006.req.Xdht0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0006.resp.Xdht0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:双录流水与合同号同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0006:双录流水与合同号同步")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0006Resource.class);

    /**
     * 交易码：xdht0006
     * 交易描述：双录流水与合同号同步
     *
     * @param xdht0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("双录流水与合同号同步")
    @PostMapping("/xdht0006")
    protected @ResponseBody
    ResultDto<Xdht0006DataRespDto> xdht0006(@Validated @RequestBody Xdht0006DataReqDto xdht0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006DataReqDto));
        Xdht0006DataRespDto xdht0006DataRespDto = new Xdht0006DataRespDto();// 响应Dto:双录流水与合同号同步
        ResultDto<Xdht0006DataRespDto> xdht0006DataResultDto = new ResultDto<>();
        try {
            // 从xdht0006DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdht0006DataRespDto对象开始
            // TODO 封装xdht0006DataRespDto对象结束
            // 封装xdht0006DataResultDto中正确的返回码和返回信息
            xdht0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, e.getMessage());
            // 封装xdht0006DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0006DataRespDto到xdht0006DataResultDto中
        xdht0006DataResultDto.setData(xdht0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006DataRespDto));
        return xdht0006DataResultDto;
    }
}
