/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportMainInfo
 * @类描述: lmt_survey_report_main_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-05 15:05:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_survey_report_main_info")
public class LmtSurveyReportMainInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_SERNO")
	private String surveySerno;
	
	/** 第三方业务流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 80)
	private String bizSerno;
	
	/** 名单流水号 **/
	@Column(name = "LIST_SERNO", unique = false, nullable = true, length = 80)
	private String listSerno;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 产品类型 **/
	@Column(name = "PRD_TYPE", unique = false, nullable = true, length = 5)
	private String prdType;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 小微调查报告类型 **/
	@Column(name = "SURVEY_TYPE", unique = false, nullable = true, length = 5)
	private String surveyType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 100)
	private String certCode;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 获客来源 **/
	@Column(name = "CUS_CHANNEL", unique = false, nullable = true, length = 5)
	private String cusChannel;

	/** 数据来源 **/
	@Column(name = "DATA_SOURCE", unique = false, nullable = true, length = 20)
	private String dataSource;
	
	/** 进件时间 **/
	@Column(name = "INTO_TIME", unique = false, nullable = true, length = 10)
	private String intoTime;
	
	/** 是否自动分配 **/
	@Column(name = "IS_AUTODIVIS", unique = false, nullable = true, length = 1)
	private String isAutodivis;
	
	/** 是否线下调查 **/
	@Column(name = "IS_STOP_OFFLINE", unique = false, nullable = true, length = 1)
	private String isStopOffline;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 营销人工号 **/
	@Column(name = "MAR_ID", unique = false, nullable = true, length = 32)
	private String marId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param listSerno
	 */
	public void setListSerno(String listSerno) {
		this.listSerno = listSerno;
	}
	
    /**
     * @return listSerno
     */
	public String getListSerno() {
		return this.listSerno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	
    /**
     * @return prdType
     */
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param surveyType
	 */
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	
    /**
     * @return surveyType
     */
	public String getSurveyType() {
		return this.surveyType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	
    /**
     * @return dataSource
     */
	public String getDataSource() {
		return this.dataSource;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime;
	}
	
    /**
     * @return intoTime
     */
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param isAutodivis
	 */
	public void setIsAutodivis(String isAutodivis) {
		this.isAutodivis = isAutodivis;
	}
	
    /**
     * @return isAutodivis
     */
	public String getIsAutodivis() {
		return this.isAutodivis;
	}
	
	/**
	 * @param isStopOffline
	 */
	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline;
	}
	
    /**
     * @return isStopOffline
     */
	public String getIsStopOffline() {
		return this.isStopOffline;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId;
	}
	
    /**
     * @return marId
     */
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getCusChannel() {
		return cusChannel;
	}

	public void setCusChannel(String cusChannel) {
		this.cusChannel = cusChannel;
	}
}