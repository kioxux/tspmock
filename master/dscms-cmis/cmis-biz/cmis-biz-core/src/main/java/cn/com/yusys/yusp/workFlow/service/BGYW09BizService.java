package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.req.Ln3247ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.resp.Ln3247RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:13
 * @desc 还款计划变更
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW09BizService implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW09BizService.class);

    @Autowired
    private IqpDelayPaymentService iqpDelayPaymentService;

    @Autowired
    private IqpDelayAgreementService iqpDelayAgreementService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG019.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG020.equals(bizType)) {
            iqpBillAcctChgBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG037.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG038.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_BG039.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_DHH13.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SGH13.equals(bizType)) {
            iqpBillAcctChgBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void iqpBillAcctChgBizApp(ResultInstanceDto instanceInfo, String currentOpType, String delaySerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpDelayPayment iqpDelayPayment = iqpDelayPaymentService.selectByPrimaryKey(delaySerno);
            if (StringUtils.isBlank(delaySerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请" + delaySerno + "申请主表信息");
            if (iqpDelayPayment == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                //TODO 修改借据表还款方式
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);

                //生成待签订协议
                IqpDelayAgreement iqpDelayAgreement = new IqpDelayAgreement();
                BeanUtils.copyProperties(iqpDelayPayment,iqpDelayAgreement);
                iqpDelayAgreement.setAgrSerno(sequenceTemplateClient.getSequenceTemplate("IQP",new HashMap<>()));
                iqpDelayAgreement.setCoreStatus("100");//未生效
                iqpDelayAgreement.setApproveStatus("000");//待发起
                iqpDelayAgreementService.insertSelective(iqpDelayAgreement);

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpDelayPaymentService.updateSelective(iqpDelayPayment);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpDelayPayment.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpDelayPaymentService.updateSelective(iqpDelayPayment);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    private void iqpBillAcctChgBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            IqpDelayAgreement iqpDelayAgreement = iqpDelayAgreementService.selectByPrimaryKey(iqpSerno);
            log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
            if (iqpDelayAgreement == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                //档案
                String bizType = instanceInfo.getBizType();
                String nodeId = instanceInfo.getNodeId();
                if ((CmisFlowConstants.FLOW_TYPE_TYPE_BG038.equals(bizType) && "243_5".equals(nodeId)) ||
                        (CmisFlowConstants.FLOW_TYPE_TYPE_BG039.equals(bizType))){
                    this.createCentralFileTask(bizType,iqpSerno,instanceInfo);
                }
                iqpDelayAgreement.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpDelayAgreementService.updateSelective(iqpDelayAgreement);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                /*
                  ln3247 开始
                 *************************************************************************
                */
                Ln3247ReqDto reqDto = new Ln3247ReqDto();
                reqDto.setDkjiejuh(iqpDelayAgreement.getBillNo());
                reqDto.setHetongbh(iqpDelayAgreement.getContNo());
                reqDto.setShengxrq(iqpDelayAgreement.getDelayEndDate().replaceAll("-",""));
                reqDto.setShfljshx(iqpDelayAgreement.getValidImmeFlag());
//                reqDto.setHkzhouqi("" + iqpDelayAgreement.getRepayInterval() + "MA"+ iqpDelayAgreement.getAgrSignDate().substring(8,10));
                AccLoan accLoan = accLoanService.selectByBillNo(iqpDelayAgreement.getBillNo());
                PvpLoanApp pvpLoan = pvpLoanAppService.queryPvpLoanAppDataByBillNo(iqpDelayAgreement.getBillNo());
                String syFlag = "N";//  N是顺延   A不顺延
                if(Objects.nonNull(pvpLoan)){
                    if("0".equals(pvpLoan.getIsHolidayDelay())){// 1是 0否
                        syFlag = "A";
                    }
                }
                reqDto.setHkzhouqi(iqpRepayWayChgService.getHkzhouqi(accLoan.getEiIntervalUnit(),
                        iqpDelayAgreement.getRepayInterval().toString(), syFlag, accLoan.getDeductDay()));
                ResultDto<Ln3247RespDto> ln3247RespDto = dscms2CoreLnClientService.ln3247(reqDto);
                String ln3247Code = Optional.ofNullable(ln3247RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String ln3247Meesage = Optional.ofNullable(ln3247RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                Ln3247RespDto l = null;
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3247RespDto.getCode())) {
                    //  获取相关的值并解析
                    l = ln3247RespDto.getData();
                    iqpDelayAgreement.setCoreStatus(CmisCommonConstants.CONT_STATUS_200);

                    // 生成归档任务
                    log.info("开始系统生成档案归档信息");
                    String cusId = iqpDelayAgreement.getCusId();
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    docArchiveClientDto.setArchiveMode("");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                    docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                    docArchiveClientDto.setDocType("27");// 27:业务变更-延期还款
                    docArchiveClientDto.setBizSerno(iqpSerno);
                    docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
                    docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                    docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                    docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                    docArchiveClientDto.setManagerId(iqpDelayAgreement.getManagerId());
                    docArchiveClientDto.setManagerBrId(iqpDelayAgreement.getManagerBrId());
                    docArchiveClientDto.setInputId(iqpDelayAgreement.getInputId());
                    docArchiveClientDto.setInputBrId(iqpDelayAgreement.getInputBrId());
                    docArchiveClientDto.setContNo(iqpDelayAgreement.getContNo());
                    docArchiveClientDto.setBillNo(iqpDelayAgreement.getBillNo());
                    docArchiveClientDto.setLoanAmt(iqpDelayAgreement.getLoanAmt());
                    docArchiveClientDto.setPrdId(iqpDelayAgreement.getPrdId());
                    docArchiveClientDto.setPrdName(iqpDelayAgreement.getPrdName());
                    docArchiveClientDto.setStartDate(accLoan.getLoanStartDate());
                    docArchiveClientDto.setEndDate(accLoan.getLoanEndDate());
                    int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                    if (num < 1) {
                        log.info("系统生成档案归档信息失败");
                    }
                    // 推送用印系统
                    try {
                        cmisBizXwCommonService.sendYk(currentUserId,iqpSerno,iqpDelayAgreement.getCusName());
                        log.info("推送印系统成功:【{}】", iqpSerno);
                    } catch (Exception e) {
                        log.info("推送印系统异常:【{}】", e.getMessage());
                    }
                } else {
                    iqpDelayAgreement.setErrorMessage(ln3247Meesage);
                }
                /*
                  ln3247 结束
                 *************************************************************************
                */

                iqpDelayAgreement.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpDelayAgreementService.updateSelective(iqpDelayAgreement);

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpDelayAgreement.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpDelayAgreementService.updateSelective(iqpDelayAgreement);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpDelayAgreement.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpDelayAgreementService.updateSelective(iqpDelayAgreement);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    public void createCentralFileTask(String bizType , String iqpSerno,ResultInstanceDto instanceInfo){
        IqpDelayAgreement iqpDelayAgreement = iqpDelayAgreementService.selectByPrimaryKey(iqpSerno);
        try {
            ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpDelayAgreement.getInputBrId());
            String orgType = resultDto.getData().getOrgType();
            //        0-总行部室
            //        1-异地支行（有分行）
            //        2-异地支行（无分行）
            //        3-异地分行
            //        4-中心支行
            //        5-综合支行
            //        6-对公支行
            //        7-零售支行
            if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType)){
                //新增临时档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(iqpDelayAgreement.getAgrSerno());
                centralFileTaskdto.setCusId(iqpDelayAgreement.getCusId());
                centralFileTaskdto.setCusName(iqpDelayAgreement.getCusName());
                centralFileTaskdto.setBizType(bizType); //
                centralFileTaskdto.setInstanceId(instanceInfo.getInstanceId());
                centralFileTaskdto.setNodeId(instanceInfo.getNextNodeInfos().get(0).getNextNodeId()); // 集中作业档案岗节点id
                centralFileTaskdto.setInputId(iqpDelayAgreement.getInputId());
                centralFileTaskdto.setInputBrId(iqpDelayAgreement.getInputBrId());
                centralFileTaskdto.setOptType("02"); // 非纯指令
                centralFileTaskdto.setTaskType("02"); // 档案暂存
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);
            }
        }catch (Exception e){
            log.info("利率变更审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW09.equals(flowCode);
    }
}
