/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysWmdSlmDetail;
import cn.com.yusys.yusp.service.RptSpdAnysWmdSlmDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWmdSlmDetailResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-13 21:54:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyswmdslmdetail")
public class RptSpdAnysWmdSlmDetailResource {
    @Autowired
    private RptSpdAnysWmdSlmDetailService rptSpdAnysWmdSlmDetailService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysWmdSlmDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysWmdSlmDetail> list = rptSpdAnysWmdSlmDetailService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysWmdSlmDetail>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysWmdSlmDetail>> index(QueryModel queryModel) {
        List<RptSpdAnysWmdSlmDetail> list = rptSpdAnysWmdSlmDetailService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysWmdSlmDetail>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysWmdSlmDetail> create(@RequestBody RptSpdAnysWmdSlmDetail rptSpdAnysWmdSlmDetail) throws URISyntaxException {
        rptSpdAnysWmdSlmDetailService.insert(rptSpdAnysWmdSlmDetail);
        return new ResultDto<RptSpdAnysWmdSlmDetail>(rptSpdAnysWmdSlmDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysWmdSlmDetail rptSpdAnysWmdSlmDetail) throws URISyntaxException {
        int result = rptSpdAnysWmdSlmDetailService.update(rptSpdAnysWmdSlmDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = rptSpdAnysWmdSlmDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 删除
     * @param rptSpdAnysWmdSlmDetail
     * @return
     */
    @PostMapping("/deleteWmdDetail")
    protected ResultDto<Integer> deleteWmdDetail(@RequestBody RptSpdAnysWmdSlmDetail rptSpdAnysWmdSlmDetail){
        return  new ResultDto<Integer>(rptSpdAnysWmdSlmDetailService.deleteByPrimaryKey(rptSpdAnysWmdSlmDetail.getPkId()));
    }

    /**
     * 保存
     * @param rptSpdAnysWmdSlmDetail
     * @return
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptSpdAnysWmdSlmDetail rptSpdAnysWmdSlmDetail){
        return  new ResultDto<Integer>(rptSpdAnysWmdSlmDetailService.save(rptSpdAnysWmdSlmDetail));
    }

    /**
     * 条件查询
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptSpdAnysWmdSlmDetail>> selectByModel(@RequestBody QueryModel model){
        return  new ResultDto<List<RptSpdAnysWmdSlmDetail>>(rptSpdAnysWmdSlmDetailService.selectByModel(model));
    }
}
