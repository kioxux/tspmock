/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FinanIndicAnaly;
import cn.com.yusys.yusp.service.FinanIndicAnalyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FinanIndicAnalyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 16:12:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/finanindicanaly")
public class FinanIndicAnalyResource {
    @Autowired
    private FinanIndicAnalyService finanIndicAnalyService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FinanIndicAnaly>> query() {
        QueryModel queryModel = new QueryModel();
        List<FinanIndicAnaly> list = finanIndicAnalyService.selectAll(queryModel);
        return new ResultDto<List<FinanIndicAnaly>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FinanIndicAnaly>> index(QueryModel queryModel) {
        List<FinanIndicAnaly> list = finanIndicAnalyService.selectByModel(queryModel);
        return new ResultDto<List<FinanIndicAnaly>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<FinanIndicAnaly> show(@PathVariable("pkId") String pkId) {
        FinanIndicAnaly finanIndicAnaly = finanIndicAnalyService.selectByPrimaryKey(pkId);
        return new ResultDto<FinanIndicAnaly>(finanIndicAnaly);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FinanIndicAnaly> create(@RequestBody FinanIndicAnaly finanIndicAnaly) throws URISyntaxException {
        finanIndicAnalyService.insert(finanIndicAnaly);
        return new ResultDto<FinanIndicAnaly>(finanIndicAnaly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FinanIndicAnaly finanIndicAnaly) throws URISyntaxException {
        int result = finanIndicAnalyService.update(finanIndicAnaly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = finanIndicAnalyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = finanIndicAnalyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectFinanIndicAnalyList
     * @函数描述:通用列表初始化
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/selectfinanindicanalylist")
    protected ResultDto<List<FinanIndicAnaly>> selectFinanIndicAnalyList(@RequestBody Map map) {
        List<FinanIndicAnaly> list = finanIndicAnalyService.selectFinanIndicAnalyList(map);
        return ResultDto.success(list);
    }

    /**
     * @函数名称:selectFinanIndicAnalyListByParam
     * @函数描述:通用列表查询
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/selectfinanindicanalylistbyparam")
    protected ResultDto<List<FinanIndicAnaly>> selectFinanIndicAnalyListByParam(@RequestBody Map map) {
        List<FinanIndicAnaly> list = finanIndicAnalyService.selectFinanIndicAnalyListByParam(map);
        return ResultDto.success(list);
    }

    /**
     * @函数名称:selectFinanIndicAnalyForOption1
     * @函数描述:财务报表表格取数
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    @PostMapping("/selectfinanindicanalyforoption")
    protected ResultDto<Map> selectFinanIndicAnalyForOption(@RequestBody Map map) {
        Map resMap = finanIndicAnalyService.selectFinanIndicAnalyForOption(map);
        return ResultDto.success(resMap);
    }

    /**
     * @函数名称:resetfinanindicanalylist
     * @函数描述:财务指标分析数据重置操作
     * @参数与返回说明:map
     * @算法描述:
     */
    @PostMapping("/resetfinanindicanalylist")
    protected ResultDto<Integer> reSetFinanIndicAnalyList(@RequestBody Map map) throws URISyntaxException {
        int result = finanIndicAnalyService.reSetFinanIndicAnalyBySerno(map);
        return new ResultDto<Integer>(result);
    }
}
