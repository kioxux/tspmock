package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.DocReadAppInfo;
import cn.com.yusys.yusp.domain.DocReadDetailInfo;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.req.Doc003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.resp.Doc003RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.req.Doc006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.resp.Doc006RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.DocReadDetailInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDetailInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 18:57:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocReadDetailInfoService {

    private static Map<String, String> accessDocStatusMap = new HashMap<>();

    static {
        accessDocStatusMap.put("查询成功,档案系统档案状态：3(入库在途)", "02");
        accessDocStatusMap.put("查询成功,档案系统档案状态：6(已入库)", "03");
        accessDocStatusMap.put("查询成功,档案系统档案状态：30(退回申请)", "04");
        accessDocStatusMap.put("查询成功,档案系统档案状态：8(已出库)", "09");
        accessDocStatusMap.put("查询成功,档案系统档案状态：40(退回确认)", "04");
    }

    @Autowired
    private DocReadDetailInfoMapper docReadDetailInfoMapper;

    @Autowired
    private Dscms2DocClientService dscms2DocClientService;

    @Autowired
    private DocAccListService docAccListService;

    @Autowired
    private DocReadAppInfoService docReadAppInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocReadDetailInfo selectByPrimaryKey(String drdiSerno) {
        return docReadDetailInfoMapper.selectByPrimaryKey(drdiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocReadDetailInfo> selectAll(QueryModel model) {
        List<DocReadDetailInfo> records = (List<DocReadDetailInfo>) docReadDetailInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocReadDetailInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocReadDetailInfo> list = docReadDetailInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int saveList(List<DocReadDetailInfo> record) {
        int saveNum = 0;
        if (null != record && record.size() > 0) {
            record.stream().forEach(item -> {
                if (null != docReadDetailInfoMapper.getDataById(item)) {
                    throw BizException.error(null, EcbEnum.ECB020001.key, EcbEnum.ECB020001.value + item.getDocNo());
                } else {
                    DocReadDetailInfo info = item;
                    // 借阅引入数据的时候，把档案状态更改为07：待出库
                    synDocStatus("", "", info);
                }
            });
        }
        return saveNum;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocReadDetailInfo record) {
        return docReadDetailInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocReadDetailInfo record) {
        return docReadDetailInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocReadDetailInfo record) {
        return docReadDetailInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String drdiSerno) {
        DocReadDetailInfo docReadDetailInfo = docReadDetailInfoMapper.selectByPrimaryKey(drdiSerno);
        if (Objects.nonNull(docReadDetailInfo) && docReadDetailInfoMapper.deleteByPrimaryKey(drdiSerno) > 0) {
            // 同步档案状态（借阅明细中删除的时候，档案台账状态回归成03：已入库）
            // docAccListService.synDocAccStatus(docReadDetailInfo.getDocNo(), "03");
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @方法名称: deleteBySerno
     * @方法描述: 根据申请记录ID删除关联档案信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteBySerno(String draiSerno) {
        List<DocReadDetailInfo> list = selectByDraiSerno(draiSerno);
        if (Objects.nonNull(list) && list.size() > 0) {
            list.forEach(docReadDetailInfo -> deleteByPrimaryKey(docReadDetailInfo.getDrdiSerno()));
            return list.size();
        } else {
            return 0;
        }
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docReadDetailInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectByDralSerNO
     * @方法描述: 条件查询 - 根据调阅流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocReadDetailInfo> selectByDraiSerno(String draiSerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("draiSerno", draiSerno);
        List<DocReadDetailInfo> list = docReadDetailInfoMapper.selectByModel(queryModel);
        return list;
    }

    /**
     * @方法名称: updateDocStsBack
     * @方法描述: 出库归还，归还的时候更新档案台账表中的档案状态
     * @参数与返回说明:
     * @算法描述: 错误码
     */
    public Map<String, String> updateDocStsBack(String drdiSerno) {
        Map<String, String> res = new HashMap<>();
        // 同步状态：（当档案调阅接受的时候，表示档案已出库成功）
        Doc006ReqDto doc006ReqDto = new Doc006ReqDto();
        // 流水号，即是：档借阅明细流水号
        doc006ReqDto.setPkbappid(drdiSerno);
        // 归还时间
        doc006ReqDto.setInroomtime(DateUtils.formatDate(new Date(), "yyyy-MM-dd"));
        // 调用档案系统接口，进行提交入库
        ResultDto<Doc006RespDto> doc006RespDto = dscms2DocClientService.doc006(doc006ReqDto);
        String doc006Code = Optional.ofNullable(doc006RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        if (Objects.equals(doc006Code, SuccessEnum.CMIS_SUCCSESS.key) && Objects.nonNull(doc006RespDto.getData())) {
            // 接口发送成功，同步更新借阅明细，档案台账，档案归档状态（同步成11：归还在途）
            synDocStatus(drdiSerno, "11", null);
            res.put("code", "0");
            res.put("msg", "成功发起归还!");
        } else {
            res.put("code", "-1");
            res.put("msg", "发起归还失败!");
        }
        return res;
    }

    /**
     * @方法名称: queryAccessStatus
     * @方法描述: 调阅待出库查询
     * @参数与返回说明:
     * @算法描述: 错误码
     */
    public Map<String, String> updateDocStsByReceive(String drdiSerno) {
        Map<String, String> res = new HashMap<>();
        // 同步状态：（当档案调阅接受的时候，表示档案已出库成功）
        Doc003ReqDto doc003ReqDto = new Doc003ReqDto();
        // 流水号，即是：借阅流水号
        doc003ReqDto.setSerno(drdiSerno);
        // 操作类型：2表示已出库
        doc003ReqDto.setRwtype("2");
        // 调用档案系统接口，进行提交入库
        ResultDto<Doc003RespDto> doc003RespDto = dscms2DocClientService.doc003(doc003ReqDto);
        String doc003Code = Optional.ofNullable(doc003RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        if (Objects.equals(doc003Code, SuccessEnum.CMIS_SUCCSESS.key) && Objects.equals("出库操作成功!", doc003RespDto.getMessage())) {
            // 接口发送成功，同步档案台账，档案归档状态（同步成09：已出库）
            synDocStatus(drdiSerno, "09", null);
            res.put("code", "0");
            res.put("msg", "成功发起接受!");
        } else {
            res.put("code", "-1");
            res.put("msg", "发起接受失败!");
        }
        return res;
    }

    /**
     * 与档案系统交互成功之后，同步更新档案状态
     *
     * @param drdiSerno
     * @param status
     */
    public void synDocStatus(String drdiSerno, String status, DocReadDetailInfo info) {
        // 初始化借阅明细
        DocReadDetailInfo docReadDetailInfo = null;
        // 申请人，申请机构，创建时间
        if (Objects.nonNull(info)) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            DocReadAppInfo readAppInfo = docReadAppInfoService.selectByPrimaryKey(info.getDraiSerno());
            if (null != readAppInfo) {
                info.setInputId(readAppInfo.getInputId());
                info.setInputBrId(readAppInfo.getInputBrId());
                info.setInputDate(openDay);
            } else {
                // 获取用户信息
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo != null) {
                    info.setInputId(userInfo.getLoginCode());
                    info.setInputBrId(userInfo.getOrg().getCode());
                    info.setInputDate(openDay);
                }
            }
            info.setCreateTime(DateUtils.parseDateByDef(openDay));
            if (StringUtils.nonEmpty(status)) {
                info.setDocStauts(status);
            }
            docReadDetailInfoMapper.insert(info);
            docReadDetailInfo = info;
        } else {
            // 获取调阅明细信息
            docReadDetailInfo = docReadDetailInfoMapper.selectByPrimaryKey(drdiSerno);
            if (StringUtils.nonEmpty(status)) {
                docReadDetailInfo.setDocStauts(status);
            }
            docReadDetailInfoMapper.updateByPrimaryKeySelective(docReadDetailInfo);
        }
        if (StringUtils.nonEmpty(status)) {
            // 同步档案台账状态
            docAccListService.synDocAccStatus(docReadDetailInfo.getDocNo(), status);
        }
    }

    /**
     * 同步档案借阅明细状态
     *
     * @param docNo
     * @param status
     */
    public void synReadDetailStatus(String docNo, String status) {
        QueryModel model = new QueryModel();
        model.addCondition("docNo", docNo);
        if (Objects.equals(status, "08")) {
            model.addCondition("docStauts", "07");
        } else if (Objects.equals(status, "03")) {
            model.addCondition("docStauts", "11");
        }
        List<DocReadDetailInfo> list = docReadDetailInfoMapper.selectByModel(model);
        if (Objects.nonNull(list) && list.size() > 0) {
            DocReadDetailInfo docReadDetailInfo = list.get(0);
            docReadDetailInfo.setDocStauts(status);
            docReadDetailInfoMapper.updateByPrimaryKeySelective(docReadDetailInfo);
        }
    }

    /**
     * 调阅申请审否决后还原引入的台账状态为 03:"已入库"
     *
     * @author jijian_yx
     * @date 2021/8/2 15:48
     **/
    public void returnDocAccStatus(DocReadAppInfo info) {
        List<DocReadDetailInfo> list = selectByDraiSerno(info.getDraiSerno());
        if (Objects.nonNull(list) && list.size() > 0) {
            list.forEach(docReadDetailInfo -> docAccListService.synDocAccStatus(docReadDetailInfo.getDocNo(), "03"));
        }
    }
}
