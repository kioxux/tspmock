/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSubPrdLowGuarRel;
import cn.com.yusys.yusp.service.LmtSubPrdLowGuarRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSubPrdLowGuarRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 19:22:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsubprdlowguarrel")
public class LmtSubPrdLowGuarRelResource {
    @Autowired
    private LmtSubPrdLowGuarRelService lmtSubPrdLowGuarRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSubPrdLowGuarRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSubPrdLowGuarRel> list = lmtSubPrdLowGuarRelService.selectAll(queryModel);
        return new ResultDto<List<LmtSubPrdLowGuarRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSubPrdLowGuarRel>> index(QueryModel queryModel) {
        List<LmtSubPrdLowGuarRel> list = lmtSubPrdLowGuarRelService.selectByModel(queryModel);
        return new ResultDto<List<LmtSubPrdLowGuarRel>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSubPrdLowGuarRel> create(@RequestBody LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel) throws URISyntaxException {
        lmtSubPrdLowGuarRelService.insert(lmtSubPrdLowGuarRel);
        return new ResultDto<LmtSubPrdLowGuarRel>(lmtSubPrdLowGuarRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel) throws URISyntaxException {
        int result = lmtSubPrdLowGuarRelService.update(lmtSubPrdLowGuarRel);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String subPrdSerno) {
        int result = lmtSubPrdLowGuarRelService.deleteByPrimaryKey(pkId, subPrdSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * 逻辑删除
     * @param lmtSubPrdLowGuarRel
     * @return
     */
    @PostMapping("/deleteLowGuar")
    protected ResultDto<Integer> deleteLowGuar(@RequestBody LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel){
        lmtSubPrdLowGuarRel.setOprType(CmisCommonConstants.OP_TYPE_02);
        return new ResultDto<Integer>(lmtSubPrdLowGuarRelService.updateSelective(lmtSubPrdLowGuarRel));
    }

    /**
     * 根据分项明细流水号查询
     * @param subPrdSerno
     * @return
     */
    @PostMapping("/selectBySubPrdSerno")
    protected ResultDto<List<LmtSubPrdLowGuarRel>> selectBySubPrdSerno(@RequestBody String subPrdSerno){
        QueryModel model = new QueryModel();
        model.addCondition("subPrdSerno",subPrdSerno);
        model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return new ResultDto<List<LmtSubPrdLowGuarRel>>(lmtSubPrdLowGuarRelService.selectByModel(model));
    }

    @PostMapping("/insertLowGuar")
    protected ResultDto<Integer> insertLowGuar(@RequestBody LmtSubPrdLowGuarRel lmtSubPrdLowGuarRel){
        return new ResultDto<Integer>(lmtSubPrdLowGuarRelService.insertLowGuar(lmtSubPrdLowGuarRel));
    }
}
