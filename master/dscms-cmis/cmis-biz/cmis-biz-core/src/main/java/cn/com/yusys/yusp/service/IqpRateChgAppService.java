/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRateChgApp;
import cn.com.yusys.yusp.repository.mapper.IqpRateChgAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRateChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-19 20:41:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpRateChgAppService {

    @Autowired
    private IqpRateChgAppMapper iqpRateChgAppMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpRateChgApp selectByPrimaryKey(String iqpSerno) {
        return iqpRateChgAppMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpRateChgApp> selectAll(QueryModel model) {
        List<IqpRateChgApp> records = (List<IqpRateChgApp>) iqpRateChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpRateChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRateChgApp> list = iqpRateChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpRateChgApp record) {
        return iqpRateChgAppMapper.insert(record);
    }

    @Autowired
    private AccLoanMapper accLoanMapper;
    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpRateChgApp iqpRateChgApp) {

        QueryModel model1 = new QueryModel();
        model1.addCondition("billNo", iqpRateChgApp.getBillNo());
        model1.addCondition("apply","Y");
        List<IqpRateChgApp> list1 = this.selectByModel(model1);
        if(list1.size()>0){
            throw BizException.error(null, "999999","\"该借据存在在途利率变更申请，请勿重复发起！\"" + "保存失败！");
        }

        QueryModel model = new QueryModel();
        model.addCondition("billNo",iqpRateChgApp.getBillNo());
        List<AccLoan> list =  accLoanMapper.selectByModel(model);
        if(list.isEmpty()){
            //借据号不存在
            throw BizException.error(null, EcbEnum.CHECK_LOAN_IS_SURV.key, EcbEnum.CHECK_LOAN_IS_SURV.value);
        }
        //TODO 存在性校验
        AccLoan accLoan = list.get(0);
        iqpRateChgApp.setBillNo(accLoan.getBillNo());                                //借据编号
        iqpRateChgApp.setCusId(accLoan.getCusId());                                  //客户编号
        iqpRateChgApp.setCusName(accLoan.getCusName());                              //客户名称
        iqpRateChgApp.setContNo(accLoan.getContNo());                                //合同编号
        iqpRateChgApp.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
        iqpRateChgApp.setPrdId(accLoan.getPrdId());                                  //产品ID
        iqpRateChgApp.setPrdName(accLoan.getPrdName());                              //产品名称
        iqpRateChgApp.setLoanStartDate(accLoan.getLoanStartDate());                  //贷款起始日
        iqpRateChgApp.setLoanEndDate(accLoan.getLoanEndDate());                      //贷款到期日
        iqpRateChgApp.setOldRateAdjMode(accLoan.getRateAdjMode());                   //原利率调整方式
        iqpRateChgApp.setOldLprRateIntval(accLoan.getLprRateIntval());               //原LPR授信利率区间
        iqpRateChgApp.setOldCurtLprRate(accLoan.getCurtLprRate());                   //原LPR利率
        iqpRateChgApp.setOldRateFloatPoint(accLoan.getRateFloatPoint());             //原浮动点数
        iqpRateChgApp.setOldExecRateYear(accLoan.getExecRateYear());                 //原执行利率(年)
        iqpRateChgApp.setOldOverdueRatePefloat(accLoan.getOverdueRatePefloat());     //原逾期利率浮动比
        iqpRateChgApp.setOldOverdueExecRate(accLoan.getOverdueExecRate());           //原逾期执行利率(年利率)
        iqpRateChgApp.setOldCiRatePefloat(accLoan.getCiRatePefloat());               //原复息利率浮动比
        iqpRateChgApp.setOldCiExecRate(accLoan.getCiExecRate());                     //原复息执行利率(年利率)
        iqpRateChgApp.setOldRateAdjType(accLoan.getRateAdjType());                   //原利率调整选项
        iqpRateChgApp.setOldNextRateAdjInterval(accLoan.getNextRateAdjInterval());   //原下一次利率调整间隔
        iqpRateChgApp.setOldNextRateAdjUnit(accLoan.getNextRateAdjUnit());           //原下一次利率调整间隔单位
        iqpRateChgApp.setOldFirstAdjDate(accLoan.getFirstAdjDate());                 //原第一次调整日
        iqpRateChgApp.setBelgLine(accLoan.getBelgLine());                            //所属条线
        iqpRateChgApp.setApproveStatus("000");
        iqpRateChgApp.setContApproveStatus("000");
        iqpRateChgApp.setAppChnl("02"); // 申请渠道 02-PC端
        iqpRateChgApp.setManagerId(accLoan.getManagerId());
        iqpRateChgApp.setManagerBrId(accLoan.getManagerBrId());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        iqpRateChgApp.setInputId(userInfo.getLoginCode()); // 当前用户号
        iqpRateChgApp.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
        iqpRateChgApp.setInputDate(DateUtils.getCurrDateStr());
        iqpRateChgApp.setCreateTime(DateUtils.getCurrTimestamp());
        iqpRateChgApp.setUpdateTime(iqpRateChgApp.getCreateTime());
        iqpRateChgApp.setUpdDate(iqpRateChgApp.getInputDate());
        iqpRateChgApp.setUpdId(iqpRateChgApp.getInputId());
        iqpRateChgApp.setUpdBrId(iqpRateChgApp.getInputBrId());
        return iqpRateChgAppMapper.insertSelective(iqpRateChgApp);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpRateChgApp record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return iqpRateChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpRateChgApp record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return iqpRateChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpRateChgAppMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRateChgAppMapper.deleteByIds(ids);
    }
}
