package cn.com.yusys.yusp.service.server.xddb0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgTfRateDto;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xddb0003.req.OwnerList;
import cn.com.yusys.yusp.dto.server.xddb0003.req.Xddb0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0003.resp.Xddb0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.GrtGuarContService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddb0002Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-04-25 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddb0003Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0003Service.class);

    @Autowired
    private CommonService commonService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private GrtGuarContService grtGuarContService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Resource
    private GuarBizRelMapper guarBizRelMapper;
    @Resource
    private GrtGuarContRelMapper grtGuarContRelMapper;
    @Resource
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private GuarCommonOwnerMapper guarCommonOwnerMapper;
    /**
     * 交易码：xddb0003
     * 交易描述：押品信息同步
     *
     * @param xddb0003DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0003DataRespDto xddb0003(Xddb0003DataReqDto xddb0003DataReqDto) throws Exception {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataReqDto));
        Xddb0003DataRespDto xddb0003DataRespDto = new Xddb0003DataRespDto();//响应Data：根据客户号获取正常周转次数
        String pass = "";//是否存在该客户

        try {
            //请求字段
            String lserno = xddb0003DataReqDto.getLserno();//业务流水号
            String iftssp = xddb0003DataReqDto.getIftssp();//是否特殊审批
            String grtflg = xddb0003DataReqDto.getGrtflg();//主担保1或副担保2
            String iffadd = xddb0003DataReqDto.getIffadd();//增加或者修改
            String custyp = xddb0003DataReqDto.getCustyp();//担保人客户类型
            String maid = xddb0003DataReqDto.getMaid();//创建人用户编号
            String inbrid = xddb0003DataReqDto.getInbrid();//登记机构
            String indate = xddb0003DataReqDto.getIndate();//登记日期
            String inid = xddb0003DataReqDto.getInid();//登记ID
            String upBrId = xddb0003DataReqDto.getMabrid();//最后修改人机构
            String cusid = xddb0003DataReqDto.getCusid();//担保人客户号
            String cusna = xddb0003DataReqDto.getCusna();//担保人名称
            String cetype = xddb0003DataReqDto.getCetype();//担保人证件类型  STD_ZB_CERT_TYP
            String cerno = xddb0003DataReqDto.getCerno();//担保人证件号码
            String cfblx = xddb0003DataReqDto.getCfblx();//查封便利性
            String flyxx = xddb0003DataReqDto.getFlyxx();//法律有效性
            String dzywxg = xddb0003DataReqDto.getDzywxg();//抵质押物与借款人相关性
            String dzypty = xddb0003DataReqDto.getDzypty();//抵质押品通用性
            String dzypbx = xddb0003DataReqDto.getDzypbx();//抵质押品变现能力
            String jgbdx = xddb0003DataReqDto.getJgbdx();//价格波动性
            String areacd = xddb0003DataReqDto.getAreacd();//所在区域编号
            String areana = xddb0003DataReqDto.getAreana();//所在区域名称
            String dudate = xddb0003DataReqDto.getDudate();//理财产品到期日
            String rirank = xddb0003DataReqDto.getRirank();//理财风险等级
            String curren = xddb0003DataReqDto.getCurren();//币种 STD_ZX_CUR_TYPE
            BigDecimal guramt = xddb0003DataReqDto.getGuramt();//担保金额
            String relend = xddb0003DataReqDto.getRelend();//与借款人关系  STD_ZB_RELATION
            String ensure = xddb0003DataReqDto.getEnsure();//保证法律有效性
            String gubore = xddb0003DataReqDto.getGubore();//保证人与借款人关联关系
            String isguar = xddb0003DataReqDto.getIsguar();//保证人是否专业担保公司
            String guartp = xddb0003DataReqDto.getGuartp();//保证方式 STD_ZB_GUARANTEE_TY
            String billno = xddb0003DataReqDto.getBillno();//票据号码
            BigDecimal bilamt = xddb0003DataReqDto.getBilamt();//票面金额
            String stdate = xddb0003DataReqDto.getStdate();//出票日期
            String enddt = xddb0003DataReqDto.getEnddt();//到期日期
            String drname = xddb0003DataReqDto.getDrname();//出票人名称
            String dracid = xddb0003DataReqDto.getDracid();//出票人账户
            String rename = xddb0003DataReqDto.getRename();//收款人名称
            String drbank = xddb0003DataReqDto.getDrbank();//出票人开户行行名
            String drbaid = xddb0003DataReqDto.getDrbaid();//出票人开户行行号
            String reacid = xddb0003DataReqDto.getReacid();//收款人账号
            String acpid = xddb0003DataReqDto.getAcpid();//承兑行号
            String acpna = xddb0003DataReqDto.getAcpna();//承兑行名
            BigDecimal rate = xddb0003DataReqDto.getRate();

            //主管机构
            String mabrid = "";

            if (StringUtils.isNotEmpty(maid)){
                ResultDto<AdminSmUserDto> adminSmUser = adminSmUserService.getByLoginCode(maid);

                if (adminSmUser!=null){
                    //主管机构
                    mabrid = adminSmUser.getData().getOrgId();
                }
            }

            // 增加或者修改为空，则提示数据不全。
            if (StringUtils.isEmpty(iffadd)) {
                xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.PARAM_NULL.value);
            }

            //押品共有人列表
            List<cn.com.yusys.yusp.dto.server.xddb0003.req.OwnerList> ownerLists = xddb0003DataReqDto.getOwnerList();

            if (CollectionUtils.isNotEmpty(ownerLists)){
                //先删除原有的押品共有人信息
                guarCommonOwnerMapper.deleteByPrimaryKey(ownerLists.get(0).getGuarid());

                for (OwnerList ownerList : ownerLists) {
                    GuarCommonOwner guarCommonOwner = new GuarCommonOwner();
                    //拷贝数据
                    BeanUtils.copyProperties(ownerList,guarCommonOwner);
                    //押品编号
                    guarCommonOwner.setGuarNo(ownerList.getGuarid());
                    //登记人
                    guarCommonOwner.setInputId(inid);
                    //登记机构
                    guarCommonOwner.setInputBrId(inbrid);
                    //主管客户经理
                    guarCommonOwner.setManagerId(maid);
                    //主管机构
                    guarCommonOwner.setManagerBrId(mabrid);
                    //操作类型
                    guarCommonOwner.setOprType(CmisBizConstants.OPR_TYPE_01);
                    //插入押品共有人表
                    guarCommonOwnerMapper.insert(guarCommonOwner);
                }
            }

            //请求list
            List<cn.com.yusys.yusp.dto.server.xddb0003.req.List> list = xddb0003DataReqDto.getList();
            cn.com.yusys.yusp.dto.server.xddb0003.req.List rcord;
            for (int i = 0; i < list.size(); i++) {
                rcord = list.get(i);
                String dyorzy = rcord.getDyorzy();//抵质或质押
                String newcd = rcord.getNewcd();//新押品类型编码
                String newla = rcord.getNewla();//新押品类型名称
                String guarid = rcord.getGuarid();//抵质押物编码--GUARANTY_ID_NEW 押品统一编号
                String bzsern = rcord.getBzsern();//保证人子项流水号-GUARANTY_ID 保证担保明细流水号
                String gagtyp = rcord.getGagtyp();//抵质押物类型
                String ganame = rcord.getGaname();//抵押物名称
                BigDecimal evamt = rcord.getEvamt();//评估价值（元）
                String evdate = rcord.getEvdate();//评估日期
                String arealo = rcord.getArealo();//抵押物存放地点
                String tenci = rcord.getTenci();//租赁情况  STD_ZB_TENANCY_CIRCE
                String ifexho = rcord.getIfexho();//是否成品房 STD_ZX_YES_NO
                String area = rcord.getArea();//面积
                String riceno = rcord.getRiceno();//权属证件号

                if(StringUtil.isNotEmpty(curren) && !Objects.equals("CNY", curren)){//如果为空,不做查询
                    if (Objects.nonNull(rate)) {
                        if (evamt.compareTo(BigDecimal.ZERO) > 0) {
                            evamt = evamt.multiply(rate);
                        }
                    } else {
                        throw new BizException(null,"",null,"汇率不能为空！");
                    }
                }

                if (newcd.contains(DscmsBizDbEnum.GUARWAY_BZ.key)) { //如果是保证担保
                    logger.info("*************************XDDB0003保证担保客户类型判断开始**START********************");
                    //根据客户号判断客户类型
                    String custype = "";//客户类型
                    String cusName = "";//客户姓名
                    String custypeTwo = DscmsBizDbEnum.CUS_TYPE_10002.key;//客户类型默认个人
                    logger.info("***********XDDB0003根据客户号查询客户信息开始,查询参数为:{}", JSON.toJSONString(cusid));
                    CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusid);//根据客户号
                    logger.info("***********XDDB0003根据客户号查询客户信息结束,返回结果为:{}", JSON.toJSONString(cusBaseDto));
                    if (cusBaseDto == null) {
                        pass = DscmsBizDbEnum.CUS_TYPE_01.key;//信贷无此客户信息标志
                    } else {
                        cusName = cusBaseDto.getCusName();
                        //客户大类
                        String cusCatalog = cusBaseDto.getCusCatalog();

                        if (CmisLmtConstants.STD_ZB_CUS_CATALOG2.equals(cusCatalog)){
                            custypeTwo = DscmsBizDbEnum.CUS_TYPE_10003.key;//企业

                            if (DscmsBizDbEnum.CUS_TYP_02.key.equals(custyp) || DscmsBizDbEnum.CUS_TYP_290.key.equals(custyp)){
                                custypeTwo = DscmsBizDbEnum.CUS_TYPE_10001.key;//担保公司
                            }
                        }
                    }
                    logger.info("*************************XDDB0003保证担保客户类型判断结束**START*******************客户类型为:" + custypeTwo);

                    //更新保证人信息表
                    if (DscmsBizDbEnum.IFFADD_ADD.key.equals(iffadd) || DscmsBizDbEnum.IFFADD_UPDATE.key.equals(iffadd)) {//新增或修改担保人信息表
                        GuarGuarantee guarGuarantee = new GuarGuarantee();
                        guarGuarantee.setGuarantyId(bzsern);//保证id
                        guarGuarantee.setCusId(cusid);//保证人
                        guarGuarantee.setAssureName(cusna);//保证人
                        guarGuarantee.setGuarAmt(guramt);//担保金额
                        guarGuarantee.setRelationLender(relend);//与借款人关系
                        guarGuarantee.setEnsureLegalValidity(ensure);//保证法律有效性
                        guarGuarantee.setGuarantorBorrowerRelation(gubore);//保证人与借款人关联关系
                        guarGuarantee.setRelationLender(gubore);//保证人与借款人关联关系
                        guarGuarantee.setIsGuarCom(isguar);//保证人是否专业担保公司
                        guarGuarantee.setGuaranteeType(guartp);//保证方式 STD_ZB_GUARANTEE_TY
                        guarGuarantee.setCusTyp(custypeTwo);//客户类型
                        guarGuarantee.setManagerId(maid);//管户人
                        guarGuarantee.setManagerBrId(mabrid);//管户机构
                        guarGuarantee.setCerType(CmisCusConstants.certTypeMap.get(cetype));//证件类型
                        guarGuarantee.setAssureCertCode(cerno);//证件号
                        guarGuarantee.setGuarantyIdNew(guarid);//押品统一编号
                        guarGuarantee.setUpdBrId(upBrId);
                        //登记人
                        guarGuarantee.setInputId(inid);
                        //登记机构
                        guarGuarantee.setInputBrId(inbrid);
                        guarGuarantee.setOprType(CmisBizConstants.OPR_TYPE_01);
                        guarGuarantee.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

                        int count;//执行记录结果
                        //根据押品编号是否已经存在记录
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("guarantyId", bzsern);
                        logger.info("XDDB0003查询保证人信息表guarGuarantee开始,查询参数为:{}", JSON.toJSONString(queryModel));
                        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.selectByModel(queryModel);
                        logger.info("XDDB0003查询保证人信息表guarGuarantee结束,返回结果为:{}", JSON.toJSONString(guarGuaranteeList));
                        //不存在则执行新增，如果新增推送失败后修改同步不到信贷，修改为有则更新，无则新增
                        if (guarGuaranteeList.size() == 0) {//新增
                            guarGuarantee.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            logger.info("***********XDDB0003新增保证人信息表guarGuarantee开始,参数为:{}", JSON.toJSONString(guarGuarantee));
                            count = guarGuaranteeMapper.insert(guarGuarantee);
                            logger.info("XDDB0003新增保证人信息表guarGuarantee结束,返回结果为:{}", JSON.toJSONString(count));
                        } else {//修改
                            logger.info("XDDB0003修改保证人信息表guarGuarantee开始******************");
                            count = guarGuaranteeMapper.updateByPrimaryKeySelective(guarGuarantee);
                            logger.info("XDDB0003修改保证人信息表guarGuarantee结束,返回结果为:{}", JSON.toJSONString(count));
                        }
                        if (StringUtil.isNotEmpty(lserno) && (lserno.startsWith("LM") || lserno.startsWith("LS"))) {//业务流水号不为空
                            logger.info("XDDB0003 guarid为【"+guarid+"】,lserno为授信分项流水号/业务申请流水号，新增或修改guar_biz_rel 开始");
                            //根据分项流水号与押品统一编号查询是否存在业务关系记录 （guar_biz_rel）
                            QueryModel model = new QueryModel();
                            model.addCondition("serno", lserno);
                            model.addCondition("guarNo", bzsern); //保证人明细流水号
                            List<GuarBizRel> guarBizRelList = guarBizRelMapper.selectByModel(model);
                            //担保合同关联信息表
                            GuarBizRel guarBizRe = new GuarBizRel();
                            guarBizRe.setPkId(bzsern);// 联合主键pk_id,serno 保证人明细
                            guarBizRe.setGuarNo(bzsern);
                            guarBizRe.setSerno(lserno);
                            guarBizRe.setOprType(CmisBizConstants.OPR_TYPE_01);//操作类型
                            guarBizRe.setInputId(maid);
                            guarBizRe.setInputBrId(mabrid);
                            guarBizRe.setInputDate(indate);
                            guarBizRe.setUpdId(maid);
                            guarBizRe.setUpdBrId(upBrId);
                            guarBizRe.setUpdDate(indate);
                            guarBizRe.setUpdateTime(indate);
                            //存在新增，不存在修改
                            if (guarBizRelList.size() == 0) {//不存在业务关联信息
                                logger.info("XDDB0003新增关联信息表guarBizRel开始******************");
                                count = guarBizRelMapper.insert(guarBizRe);
                            } else {
                                //修改 GRT_GUAR_CONT_REL 记录
                                logger.info("XDDB0003修改关联信息表guarBizRel开始******************");
                                count = guarBizRelMapper.updateByPrimaryKey(guarBizRe);
                            }
                            logger.info("XDDB0003 guarid为【"+guarid+"】,lserno为授信分项流水号，新增或修改guar_biz_rel 结束");
                        }else if (StringUtil.isNotEmpty(lserno) && lserno.startsWith("DB")){
                            logger.info("XDDB0003 guarid为【"+guarid+"】,lserno为担保合同编号，新增或修改grt_guar_cont_rel 开始");
                            GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(lserno);

                            if (grtGuarCont!=null && StringUtils.isNotEmpty(grtGuarCont.getGuarContNo())){
                                //保证担保方式
                                String assureWay = grtGuarCont.getAssureWay();
                                guarGuarantee.setGuarantyType(assureWay);
                                //更新关联的保证人信息的保证方式字段
                                guarGuaranteeMapper.updateByPrimaryKeySelective(guarGuarantee);

                                logger.info("XDDB0003 将保证id为【"+bzsern+"】的保证方式由【"+guarGuarantee.getGuarantyType()+"】改为【"+assureWay+"】");
                            }

                            dealGrtGuarContRel(lserno, bzsern, maid, mabrid, indate, xddb0003DataRespDto);
                            logger.info("XDDB0003 guarid为【"+guarid+"】,lserno为担保合同编号，新增或修改grt_guar_cont_rel 结束");
                        }
                        if (count > 0) {//操作成功
                            logger.info("XDDB0003操作成功******************");
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                        }
                    } else {
                        //保证人信息不能进行删除操作
                        logger.info("XDDB0003押品存在业务关联关系,保证人信息不能进行删除操作******************");
                        xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                        xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.EXIST_BIZ_REL.value);
                    }
                } else {
                    //新增或修改
                    if (DscmsBizDbEnum.IFFADD_ADD.key.equals(iffadd) || DscmsBizDbEnum.IFFADD_UPDATE.key.equals(iffadd)) {
                        //新增guar_base_info表记录
                        logger.info("XDDB0003新增guar_base_info表记录******************");
                        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                        guarBaseInfo.setGuarNo(guarid);//抵质押物编码
                        guarBaseInfo.setNewcode(newcd);//新押品编码
                        guarBaseInfo.setNewlabel(newla);//新押品编码名称
                        guarBaseInfo.setGuarType(gagtyp);
                        guarBaseInfo.setGuarTypeCd(newcd);//抵质押物类型
                        guarBaseInfo.setPldimnMemo(ganame);//抵押物名称
                        guarBaseInfo.setEvalDate(evdate);//评估日期
                        guarBaseInfo.setEvalAmt(evamt);//评估价值（元）
                        guarBaseInfo.setPldLocation(arealo);//抵押物存放地点
                        guarBaseInfo.setTenancyCirce(tenci);//租赁情况
                        guarBaseInfo.setIsExistinghome(ifexho);//是否成品房
                        guarBaseInfo.setSqu(area);//面积
                        guarBaseInfo.setRightCertNo(riceno);//权属证件号
                        int count = insertOrUpdateGaurBaseInfo(xddb0003DataReqDto, guarBaseInfo, iffadd);
                        if (count > 0) {//操作成功
                            logger.info("XDDB0003新增guar_base_info表记录成功*SUCCESS*****************");
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                        }
                        //业务流水号不为空并且以FX开头，则以业务流水号以及押品编号关联查询GUAR_BIZ_REL，若不存在记录则插入GUAR_BIZ_REL；
                        if (StringUtil.isNotEmpty(lserno) && lserno.startsWith("FX")) {
                            logger.info("XDDB0003业务流水号不为空并且以FX开头，则以业务流水号以及押品编号关联查询UAR_BIZ_REL，若不存在记录则插入UAR_BIZ_REL**开始***************");
                            //根据流水号查询是否存在业务关系记录 （guar_biz_rel）
                            QueryModel model = new QueryModel();
                            model.addCondition("serno", lserno);//业务流水号
                            model.addCondition("guarNo", guarid);//押品编号
                            List<GuarBizRel> guarBizRelList = guarBizRelMapper.selectByModel(model);

                            //业务押品关联信息表
                            GuarBizRel guarBizRe = new GuarBizRel();
                            //生成UUID
                            String pkId = UUID.randomUUID().toString();

                            guarBizRe.setGuarNo(guarid);
                            guarBizRe.setSerno(lserno);
                            guarBizRe.setOprType(DscmsBizDbEnum.SYSTEMFLAG_01.key);//操作类型
                            guarBizRe.setIsUnderLmt("1");//是否授信项下1是2否，默认为1
                            guarBizRe.setInputId(maid);
                            guarBizRe.setInputBrId(mabrid);
                            guarBizRe.setInputDate(indate);
                            guarBizRe.setUpdId(maid);
                            guarBizRe.setUpdBrId(upBrId);
                            guarBizRe.setUpdDate(indate);
                            guarBizRe.setUpdateTime(indate);
                            //不存在,则插入
                            if (guarBizRelList.size() == 0) {
                                guarBizRe.setPkId(pkId);
                                guarBizRelMapper.insert(guarBizRe);
                            } else {
                                //存在，则更新
                                for (int t = 0; t < guarBizRelList.size(); t++) {
                                    GuarBizRel guarBizRe1 = guarBizRelList.get(t);
                                    guarBizRe.setPkId(guarBizRe1.getPkId());
                                    guarBizRelMapper.updateByPrimaryKey(guarBizRe);
                                }
                            }
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                            logger.info("XDDB0003业务流水号不为空并且以FX开头，则以业务流水号以及押品编号关联查询UAR_BIZ_REL，若不存在记录则插入UAR_BIZ_REL**结束***************");
                        }
                        //若以DB开头，则以业务流水号以及押品编号查询查询GRT_GUAR_CONT_REL，若不存在记录则插入GRT_GUAR_CONT_REL
                        else if (StringUtil.isNotEmpty(lserno) && lserno.startsWith("DB")) {
                            dealGrtGuarContRel(lserno, guarid, maid, mabrid, indate, xddb0003DataRespDto);
                        }
                        //3 删除
                    } else if (DscmsBizDbEnum.IFFADD_DELETE.key.equals(iffadd)) {
                        //查询 GUAR_BIZ_REL .是否存在记录，存在,则不允许删除
                        logger.info("XDDB0003查询 GUAR_BIZ_REL .是否存在记录，存在,则不允许删除****开始*************");
                        int result = guarBizRelMapper.selectRecordByGuarNo(guarid);
                        if (result > 0) {//存在关系记录
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.EXIST_BIZ_REL.value);
                        } else {
                            //执行逻辑删除GUAR_BASE_INFO
                            int result1 = guarBaseInfoMapper.deleteGuarByGuarNo(guarid);
                            //执行逻辑删除 GRT_GUAR_CONT_REL
                            int result2 = grtGuarContRelMapper.deleteGrtGuarContRelByGuarNo(guarid);

                            if (result1 > 0 && result2 > 0) {//删除成功
                                xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                                xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                            } else {
                                xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                                xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
                            }
                        }
                        logger.info("XDDB0003查询 GUAR_BIZ_REL .是否存在记录，存在,则不允许删除****结束*************");
                        //重新估值
                    } else if (DscmsBizDbEnum.IFFADD_EVAL.key.equals(iffadd)) {
                        //将押品价值更新到押品信息表GUAR_BASE_INFO
                        logger.info("XDDB0003重新估值;将押品价值更新到押品信息表GUAR_BASE_INFO****开始*************");
                        int count = 0;
                        //更新参数
                        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                        guarBaseInfo.setGuarNo(guarid);//抵质押物编码
                        guarBaseInfo.setEvalDate(evdate);//评估日期
                        guarBaseInfo.setEvalAmt(evamt);//评估价值（元）
                        guarBaseInfo.setMaxMortagageAmt(evamt);
                        guarBaseInfo.setConfirmAmt(evamt);
                        guarBaseInfo.setConfirmDate(evdate);

                        QueryModel model = new QueryModel();
                        model.addCondition("guarNo", guarid);
                        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectByModel(model);
                        if (guarBaseInfoList.size() > 0) {
                            //根据主键流水执行更新操作
                            String serno = guarBaseInfoList.get(0).getSerno();
                            guarBaseInfo.setSerno(serno);//主键流水
                            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            count = guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);//
                        }
                        if (count > 0) {//提交成功
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                        } else {
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
                        }
                        //移交
                        // 操作类型为押品管护人移交时，更新GUAR_BASE_INFO表中登记人以及登记机构信息；
                        // 更新入库权证信息表GUAR_WARRANT_IN_APP中入库已登记押品的登记人以及登记机构信息；
                    } else if (DscmsBizDbEnum.IFFADD_TRAN.key.equals(iffadd)) {
                        logger.info("XDDB0003押品管护人移交开始*************");
                        //押品管护人移交
                        int count = 0;
                        //更新参数
                        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                        guarBaseInfo.setGuarNo(guarid);//抵质押物编码
                        guarBaseInfo.setInputId(inid);//登记人
                        guarBaseInfo.setInputBrId(inbrid);//登记机构
                        guarBaseInfo.setManagerId(maid);//创建人
                        guarBaseInfo.setManagerBrId(mabrid);//主管机构
                        guarBaseInfo.setAccountManager(maid);//创建人
                        QueryModel model = new QueryModel();
                        model.addCondition("guarNo", guarid);
                        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectByModel(model);
                        if (guarBaseInfoList.size() > 0) {
                            //根据主键流水执行更新操作
                            String serno = guarBaseInfoList.get(0).getSerno();
                            guarBaseInfo.setSerno(serno);//主键流水
                            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                            count = guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);//
                        }
                        //将该押品所有对应的入库权证全部移交  GUAR_WARRANT_INFO
                        Map map = new HashMap();
                        map.put("guarNo", guarid);
                        map.put("managerId", maid);
                        map.put("managerBrId", mabrid);
                        count = guarWarrantInfoMapper.updateToManagerIdByCuarNo(map);

                        if (count > 0) {//移交成功
                            logger.info("XDDB0003押品管护人移交成功*************");
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
                        } else {
                            logger.info("XDDB0003押品管护人移交失败*************");
                            xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                            xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
                        }
                    }
                }
            }

            if (DscmsBizDbEnum.CUS_TYPE_01.key.equals(pass)) {
                //信贷无该客户,请根据ecif客户数据至信贷新增
                xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.CUS_BASE_NO.value);
            }
            if (StringUtil.isEmpty(pass) && list.size() == 0) {//请求参数为空
                xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
            }


        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, e.getMessage());
            throw new BizException(null,"",null,e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataRespDto));
        return xddb0003DataRespDto;
    }


    /**
     * @方法名称: insertOrUpdateGaurBaseInfo
     * @方法描述: 插入（更新）押品信息表 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int insertOrUpdateGaurBaseInfo(Xddb0003DataReqDto xddb0003DataReqDto, GuarBaseInfo guarBaseInfo, String ctrlType) throws ParseException {
        int count = 0;
        logger.info("************************XDDB0003新增guar_base_info押品基本信息表记录开始***START***************");
        String lserno = xddb0003DataReqDto.getLserno();//业务流水号
        String iftssp = xddb0003DataReqDto.getIftssp();//是否特殊审批
        String grtflg = xddb0003DataReqDto.getGrtflg();//主担保或副担保
        String custyp = xddb0003DataReqDto.getCustyp();//担保人客户类型
        String maid = xddb0003DataReqDto.getMaid();//创建人用户编号
        String inbrid = xddb0003DataReqDto.getInbrid();//登记机构
        String indate = xddb0003DataReqDto.getIndate();//登记日期
        String inid = xddb0003DataReqDto.getInid();//登记ID
        String upBrId = xddb0003DataReqDto.getMabrid();//主管机构
        String cusid = xddb0003DataReqDto.getCusid();//担保人客户号
        String cusna = xddb0003DataReqDto.getCusna();//担保人名称
        String cetype = xddb0003DataReqDto.getCetype();//担保人证件类型  STD_ZB_CERT_TYP
        String cerno = xddb0003DataReqDto.getCerno();//担保人证件号码
        String cfblx = xddb0003DataReqDto.getCfblx();//查封便利性
        String flyxx = xddb0003DataReqDto.getFlyxx();//法律有效性
        String dzywxg = xddb0003DataReqDto.getDzywxg();//抵质押物与借款人相关性
        String dzypty = xddb0003DataReqDto.getDzypty();//抵质押品通用性
        String dzypbx = xddb0003DataReqDto.getDzypbx();//抵质押品变现能力
        String jgbdx = xddb0003DataReqDto.getJgbdx();//价格波动性
        String areacd = xddb0003DataReqDto.getAreacd();//所在区域编号
        String areana = xddb0003DataReqDto.getAreana();//所在区域名称
        String dudate = xddb0003DataReqDto.getDudate();//理财产品到期日
        String rirank = xddb0003DataReqDto.getRirank();//理财风险等级
        String curren = xddb0003DataReqDto.getCurren();//币种 STD_ZX_CUR_TYPE
        BigDecimal guramt = xddb0003DataReqDto.getGuramt();//担保金额
        String relend = xddb0003DataReqDto.getRelend();//与借款人关系  STD_ZB_RELATION
        String ensure = xddb0003DataReqDto.getEnsure();//保证法律有效性
        String gubore = xddb0003DataReqDto.getGubore();//保证人与借款人关联关系
        String isguar = xddb0003DataReqDto.getIsguar();//保证人是否专业担保公司
        String guartp = xddb0003DataReqDto.getGuartp();//保证方式 STD_ZB_GUARANTEE_TY
        String billno = xddb0003DataReqDto.getBillno();//票据号码（票据迁移至押品系统）
        BigDecimal bilamt = xddb0003DataReqDto.getBilamt();//票面金额
        String stdate = xddb0003DataReqDto.getStdate();//出票日期
        String enddt = xddb0003DataReqDto.getEnddt();//到期日期
        String drname = xddb0003DataReqDto.getDrname();//出票人名称
        String dracid = xddb0003DataReqDto.getDracid();//出票人账户
        String rename = xddb0003DataReqDto.getRename();//收款人名称
        String drbank = xddb0003DataReqDto.getDrbank();//出票人开户行行名
        String drbaid = xddb0003DataReqDto.getDrbaid();//出票人开户行行号
        String reacid = xddb0003DataReqDto.getReacid();//收款人账号
        String acpid = xddb0003DataReqDto.getAcpid();//承兑行号
        String acpna = xddb0003DataReqDto.getAcpna();//承兑行名

        ResultDto<AdminSmUserDto> adminSmUser = adminSmUserService.getByLoginCode(maid);
        //主管机构
        String mabrid = adminSmUser.getData().getOrgId();

        //更新字段值
        guarBaseInfo.setGuarCusType(custyp);//押品所有人类型
        guarBaseInfo.setAccountManager(maid);//创建人
        guarBaseInfo.setInputBrId(inbrid);//登记机构
//        //登记日期
//        if (StringUtils.isNotEmpty(indate)) {
//            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
//            Date date = form.parse(indate);
//            String str = form.format(date);
//            guarBaseInfo.setInputDate(str);
//        }
        if (StringUtils.isEmpty(guarBaseInfo.getInputDate())){
            //如果guarBaseInfo里的input_date为空，则取当前系统时间
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            guarBaseInfo.setInputDate(sdf.format(new Date()));// 登记日期
            logger.info("押品【"+guarBaseInfo.getGuarNo()+"】的登记日期不取送过来的indate:"+indate+";取当前系统时间："+guarBaseInfo.getInputDate());
        }
        guarBaseInfo.setInputId(inid);//登记人
        guarBaseInfo.setManagerBrId(mabrid);//主管机构
        guarBaseInfo.setManagerId(maid);
        guarBaseInfo.setGuarCusId(cusid);//担保人客户号
        guarBaseInfo.setGuarCusName(cusna);//担保人名称
        guarBaseInfo.setGuarCertType(cetype);
        guarBaseInfo.setGuarCertCode(cerno);
        guarBaseInfo.setSupervisionConvenience(cfblx);
        guarBaseInfo.setLawValidity(flyxx);//法律有效性
        guarBaseInfo.setPldimnDebitRelative(dzywxg);//抵质押物与借款人相关性
        guarBaseInfo.setPldimnCommon(dzypty);//抵质押品通用性
        guarBaseInfo.setPldimnCashability(dzypbx);//抵质押品变现能力
        guarBaseInfo.setPriceWave(jgbdx);//价格波动性
        guarBaseInfo.setAreaCode(areacd);//所在区域名称
        guarBaseInfo.setAreaName(areana);//所在区域名称
        guarBaseInfo.setCurType(curren);//币种 STD_ZX_CUR_TYPE
        guarBaseInfo.setOprType(CmisBizConstants.OPR_TYPE_01);
        guarBaseInfo.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
        guarBaseInfo.setUpdBrId(upBrId);
        guarBaseInfo.setRegisterDate(dudate);//登记/止付日期 存理财到期日

        guarBaseInfo.setDrftNo(billno);//票据号
        guarBaseInfo.setDrftAmt(bilamt);//票面金额
        guarBaseInfo.setIsseDate(stdate);//出票日期
        guarBaseInfo.setDraftEndDate(enddt);//到期日期
        guarBaseInfo.setDrwrName(drname);//出票人名称
        guarBaseInfo.setDrwrAccno(dracid);//出票人账户
        guarBaseInfo.setPyeeName(rename);//收款人名称
        guarBaseInfo.setDrwrAcctsvcrnm(drbank);//出票人开户行行名
        guarBaseInfo.setDrwrAcctsvcrNo(drbaid);//出票人开户行行号
        guarBaseInfo.setPyeeAccno(reacid);//收款人账号
        guarBaseInfo.setAorgNo(acpid);//承兑行行号
        guarBaseInfo.setAorgName(acpna);//承兑行行名

        guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

        //grt_flag 抵押 01 质押 02
        String newcd = guarBaseInfo.getNewcode();
        if (newcd.contains(DscmsBizDbEnum.GUARWAY_DY.key)) {
            guarBaseInfo.setGrtFlag(CmisBizConstants.STD_GRT_FLAG_01);
            guarBaseInfo.setRegState(CmisBizConstants.GUAR_REG_STATE_02);
        } else {
            guarBaseInfo.setGrtFlag(CmisBizConstants.STD_GRT_FLAG_02);
        }

        //查询是否存在该押品信息
        String guarNo = guarBaseInfo.getGuarNo();
        QueryModel model = new QueryModel();
        model.addCondition("guarNo", guarNo);
        logger.info("***********XDDB0003查询是否存在该押品信息开始,查询参数为:{}", JSON.toJSONString(model));
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoMapper.selectByModel(model);
        logger.info("***********XDDB0003查询是否存在该押品信息结束,返回结果为:{}", JSON.toJSONString(guarBaseInfoList));
        //新增操作
        if (DscmsBizDbEnum.IFFADD_ADD.key.equals(ctrlType) || guarBaseInfoList.size() < 1) {
            logger.info("************************XDDB0003新增操作开始***START*****");
            //生成押品流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>());
            if (StringUtil.isEmpty(serno)) {//自动押品编码
                serno = lserno;
            }
            guarBaseInfo.setSerno(serno);//主键流水
            guarBaseInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            count = guarBaseInfoMapper.insert(guarBaseInfo);
        } else if (DscmsBizDbEnum.IFFADD_UPDATE.key.equals(ctrlType)) {//修改操作
            //根据押品编号执行更新操作
            logger.info("************************XDDB0003根据押品编号执行更新操作开始***START*****");
            String serno = guarBaseInfoList.get(0).getSerno();
            guarBaseInfo.setSerno(serno);//主键流水
            guarBaseInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            count = guarBaseInfoMapper.updateByPrimaryKeySelective(guarBaseInfo);
        }
        logger.info("************************XDDB0003新增guar_base_info押品基本信息表记录结束***END*****");
        //返回插入记录数
        return count;
    }

    /**
     * 处理担保合同与押品关系数据
     * @param lserno
     * @param guarid
     * @param maid
     * @param mabrid
     * @param indate
     * @param xddb0003DataRespDto
     */
    public void dealGrtGuarContRel(String lserno,String guarid,String maid,String mabrid,String indate,Xddb0003DataRespDto xddb0003DataRespDto){
        logger.info("XDDB0003若以DB开头，则以业务流水号以及押品编号查询查询GRT_GUAR_CONT_REL，若不存在记录则插入GRT_GUAR_CONT_REL**开始***************");
        //担保合同关联信息表
        GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
        //生成UUID
        String pkId = UUID.randomUUID().toString();
        grtGuarContRel.setGuarNo(guarid);
        grtGuarContRel.setContNo(lserno);
        grtGuarContRel.setMortContNo(lserno);//抵质押合同编号
        grtGuarContRel.setIsMainGuar(CmisBizConstants.STD_ZB_YES_NO_Y);//是否主要押品1是2否:默认为1
        // grtGuarContRel.setRemark("");//备注
        grtGuarContRel.setOprType(CmisBizConstants.OPR_TYPE_01);//操作类型
        grtGuarContRel.setStatus("1");//0失效1生效 默认：1
        grtGuarContRel.setInputId(maid);
        grtGuarContRel.setInputBrId(mabrid);
        grtGuarContRel.setInputDate(indate);
        grtGuarContRel.setUpdId(maid);
        grtGuarContRel.setUpdBrId(mabrid);
        grtGuarContRel.setUpdDate(indate);
        grtGuarContRel.setManagerId(maid);
        grtGuarContRel.setManagerId(mabrid);
        grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

        //根据流水号查询是否存在业务关系记录 （GRT_GUAR_CONT_REL）
        QueryModel model = new QueryModel();
        model.addCondition("guarContNo", lserno);//担保合同编号
        model.addCondition("guarNo", guarid);//押品编号
        List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelMapper.selectByModel(model);

        if (grtGuarContRelList.size() == 0) {
            //不存在,则插入
            grtGuarContRel.setPkId(pkId);
            grtGuarContRel.setGuarContNo(lserno);
            grtGuarContRelMapper.insert(grtGuarContRel);
        } else {
            //存在，则更新
            for (int k = 0; k < grtGuarContRelList.size(); k++) {
                GrtGuarContRel grtGuarContRel1 = grtGuarContRelList.get(k);
                grtGuarContRel.setPkId(grtGuarContRel1.getPkId());
                grtGuarContRelMapper.updateByPrimaryKey(grtGuarContRel);
            }
        }
        xddb0003DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
        xddb0003DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
        logger.info("XDDB0003若以DB开头，则以业务流水号以及押品编号查询查询GRT_GUAR_CONT_REL，若不存在记录则插入GRT_GUAR_CONT_REL**结束***************");
    }
}