package cn.com.yusys.yusp.service.server.xdht0008;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0008.req.Xdht0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0008.resp.Xdht0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0008Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: gqh
 * @创建时间: 2021-06-0220:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0008Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0008Service.class);

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Resource
    private GrtGuarContMapper grtGuarContMapper;

    /**
     * 借款合同双录流水同步
     *
     * @param xdht0008DataReqDto
     * @return xdht0008DataRespDto
     */
    @Transactional
    public Xdht0008DataRespDto getXdht0008(Xdht0008DataReqDto xdht0008DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataReqDto));
        Xdht0008DataRespDto xdht0008DataRespDto = new Xdht0008DataRespDto();
        try {
            //传入参数非空校验
            String contNo = xdht0008DataReqDto.getContNo();
            String docNo = xdht0008DataReqDto.getDocNo();
            String date = xdht0008DataReqDto.getDate();
            if (StringUtils.isBlank(contNo) || StringUtils.isBlank(docNo) || StringUtils.isBlank(date)) {
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }

            // 更新数据
            logger.info("更新合同主表-双录编号开始,查询参数为:{}", JSON.toJSONString(contNo));
            int result1 = ctrLoanContMapper.updateDoubleRecordNoByContNo(contNo, docNo);
            logger.info("更新合同主表-双录编号结束,返回参数为:{}", JSON.toJSONString(result1));

            logger.info("更新担保合同表-担保双录编号开始,查询参数为:{}", JSON.toJSONString(contNo));
            int result2 = grtGuarContMapper.updateGuarIserchNoByGuarContNo(contNo, docNo);
            logger.info("更新担保合同表-担保双录编号结束,返回参数为:{}", JSON.toJSONString(result2));

            // 判断更新结果
            if (result1 > 0 || result2 > 0) {
                xdht0008DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);// 操作成功标志位
                xdht0008DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);// 描述信息
            } else {
                xdht0008DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作失败标志位
                xdht0008DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);// 描述信息
            }
        } catch (Exception e) {
            xdht0008DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作失败标志位
            xdht0008DataRespDto.setOpMsg(CommonConstance.OP_MSG_F + ":" + e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataRespDto));
        return xdht0008DataRespDto;
    }
}
