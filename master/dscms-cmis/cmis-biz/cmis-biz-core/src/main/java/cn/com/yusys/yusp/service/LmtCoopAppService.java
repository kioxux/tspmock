/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCoopAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCoopAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: liqichao
 * @创建时间: 2021-01-12 19:42:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCoopAppService {
    private static final Logger log = LoggerFactory.getLogger(LmtCoopAppService.class);
    @Autowired
    private LmtCoopAppMapper lmtCoopAppMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private LmtBuildingProService lmtBuildingProService;//楼盘项目信息
    @Autowired
    private LmtCarProService lmtCarProService;//汽车项目信息
    @Autowired
    private LmtMachineEquipService lmtMachineEquipService;//机械设备信息
    @Autowired
    private LmtThrShrsAppRelService lmtThrShrsAppRelService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtCoopApp selectByPrimaryKey(String serno) {
        return lmtCoopAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtCoopApp> selectAll(QueryModel model) {
        List<LmtCoopApp> records = (List<LmtCoopApp>) lmtCoopAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtCoopApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCoopApp> list = lmtCoopAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtCoopApp record) {
        return lmtCoopAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtCoopApp record) {
        return lmtCoopAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtCoopApp record) {
        return lmtCoopAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtCoopApp record) {
        return lmtCoopAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtCoopAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtCoopAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteLmtCoopAppBySerno
     * @方法描述: 逻辑删除关联信息
     * @参数: param
     * */
    @Transactional(rollbackFor=Exception.class)
    public int deleteLmtCoopAppBySerno(Map param){
        int rtnCode = 0;
        //获取业务流水号
        String serno = (String) param.get("serno");
        if("".equals(serno) || serno == null){
            throw new YuspException(EcbEnum.DELETE_PRARM_NULL.key, EcbEnum.DELETE_PRARM_NULL.value);
        }

        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("serno",serno);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType", CmisCommonConstants.OPR_TYPE_DELETE);

        int delCount = 0;
        //逻辑删除合作方额度申请关联的楼盘项目信息
        delCount = lmtBuildingProService.updateByParams(delMap);
        if(delCount < 0){
            throw new YuspException(EcbEnum.LMT_BUILDING_PRO_DELETE_FAILED.key, EcbEnum.LMT_BUILDING_PRO_DELETE_FAILED.value);
        }
        log.info("serno：" + serno + "合作方额度申请关联楼盘项目信息逻辑删除！");

        //逻辑删除合作方额度申请关联的汽车项目信息
        delCount = lmtCarProService.updateByParams(delMap);
        if(delCount < 0){
            throw new YuspException(EcbEnum.LMT_CAR_PRO_DELETE_FAILED.key, EcbEnum.LMT_CAR_PRO_DELETE_FAILED.value);
        }
        log.info("serno：" + serno + "合作方额度申请关联汽车项目信息逻辑删除！");

        //逻辑删除合作方额度申请关联的机械设备信息
        delCount = lmtMachineEquipService.updateByParams(delMap);
        if(delCount < 0){
            throw new YuspException(EcbEnum.LMT_MACHINE_EQUIP_DELETE_FAILED.key, EcbEnum.LMT_MACHINE_EQUIP_DELETE_FAILED.value);
        }
        log.info("serno：" + serno + "合作方额度申请关联机械设备信息逻辑删除！");

        //最后一步，逻辑删除合作方额度申请信息
        delCount = lmtCoopAppMapper.updateByParams(delMap);
        if(delCount < 0){
            throw new YuspException(EcbEnum.LMT_COOP_APP_DELETE_FAILED.key, EcbEnum.LMT_COOP_APP_DELETE_FAILED.value);
        }
        log.info("serno：" + serno + "合作方额度申请信息逻辑删除结束！");
        return rtnCode;
    }

    /**
     * @方法名称: checkProjectDetails
     * @方法描述: 根据业务流水号校验项目信息是否保存
     * @参数: serno
     */

    public Integer checkProjectDetails(String serno) {
        int result = 0;
        String coop_type = lmtCoopAppMapper.selectByPrimaryKey(serno).getCoopType();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        if (CmisBizConstants.COOP_TYPE_001.equals(coop_type)) {
            result += lmtBuildingProService.selectCountByModel(queryModel);
        } else if (CmisBizConstants.COOP_TYPE_002.equals(coop_type)) {
            result += lmtCarProService.selectCountByModel(queryModel);
        } else if (CmisBizConstants.COOP_TYPE_003.equals(coop_type)) {
            result += lmtMachineEquipService.selectCountByModel(queryModel);
        } else {
            result = 1;
        }
        return result;
    }

    /**
     * @方法名称: selectCountByOldSerno
     * @方法描述: 根据原业务流水号查询复议申请
     * @参数: serno
     * */
    public LmtCoopApp selectCountByOldSerno(String serno){
        return lmtCoopAppMapper.selectCountByOldSerno(serno);
    }

    /***
     * 审批通过业务处理
     * lmtCoopApp 业务申请信息
     * */
    @Transactional(rollbackFor = Exception.class)
    public void lmtCoopAppAgree (LmtCoopApp lmtCoopApp){

    }
}
