package cn.com.yusys.yusp.service.server.xdht0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdht0026.req.Xdht0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0026.resp.Xdht0026DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0026.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称:
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zhaoyue
 * @创建时间: 2021/6/11 9:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0026Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0026Service.class);
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 根据客户调查表编号取得贷款合同主表的合同状态
     *
     * @param xdht0026DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0026DataRespDto queryContStatusByCertCode(Xdht0026DataReqDto xdht0026DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value);
        Xdht0026DataRespDto xdht0026DataRespDto = new Xdht0026DataRespDto();

        try {
            //请求字段
            String certCode = xdht0026DataReqDto.getCert_code();
            String contState = xdht0026DataReqDto.getCont_state();

            //返回结果
            String cusId = StringUtils.EMPTY;//客户编号
            String agriFlg  = StringUtils.EMPTY;//是否农户
            String cusType = StringUtils.EMPTY;//是否为个体工商户

            //根据客户证件号查询客户基本信息
            java.util.List<String> cusIds = commonService.getCusBaseByCertCode(certCode);
            if (CollectionUtils.nonEmpty(cusIds)) {
                cusId = cusIds.get(0);
            }
            ResultDto<CusIndivDto> cusBase= cmisCusClientService.queryCusindivByCusid(cusId);
            if (cusBase.getData() != null) {
                cusId = cusBase.getData().getCusId();
                agriFlg = cusBase.getData().getAgriFlg();
                cusType = cusBase.getData().getCusType();//客户类型
                /**
                 * 判断客户类型是否为个体工商户
                 * 260	个体工商户
                 * 120	个体工商户(无字号)
                 * 1  是
                 * 2  否
                 */
                if ("120".equals(cusType) || "".equals(cusType)){
                    cusType = "1";
                } else {
                    cusType = "2";
                }
            }

            //根据客户号和合同状态关联查询合同信息
            Map map = new HashMap<>();
            map.put("cusId",cusId);
            if(StringUtil.isNotEmpty(contState)){
                map.put("contStatus",contState);
            }
            java.util.List<CtrLoanCont> resList = ctrLoanContService.selectCtrLoanContByParams(map);
            if (resList.size()>0) {
                java.util.List resultList = new ArrayList();
                for(CtrLoanCont ctrLoanCont:resList){
                    List list = new List();
                    list.setCus_id(cusId);//客户编号
                    list.setCont_no(ctrLoanCont.getContNo());//借款合同号
                    list.setCn_cont_no(ctrLoanCont.getContCnNo());//中文合同号
                    list.setApply_amount(ctrLoanCont.getContAmt());//合同金额（总额度）
                    list.setAvail_amt(ctrLoanCont.getContBalance());//合同可用余额（可用额度）
                    list.setAssure_means_main(ctrLoanCont.getGuarWay());//担保方式
                    list.setLoan_start_date(ctrLoanCont.getContStartDate());//合同起始日期
                    list.setLoan_end_date(ctrLoanCont.getContEndDate());//合同到期日期
                    list.setLoan_term(ctrLoanCont.getAppTerm().toString());//签订期限
                    list.setCont_type(ctrLoanCont.getContType());//合同类型
                    list.setManager_id(ctrLoanCont.getManagerId());//客户经理
                    list.setManager_br_id(ctrLoanCont.getManagerBrId());//归属机构
                    list.setAgri_flg(agriFlg);//是否农户
                    list.setCus_type(cusType);//是否个体工商户
                    list.setCont_state(ctrLoanCont.getContStatus());//合同状态
                    list.setBiz_type(ctrLoanCont.getBizType());//业务类型代码
                    list.setPrd_name(ctrLoanCont.getPrdName());//业务类型名称
                    list.setBiz_type_mx(ctrLoanCont.getPrdId());//业务类型代码明细(小贷)
                    list.setPrd_name_mx(ctrLoanCont.getPrdId());//业务类型代码明细(小贷)
                    resultList.add(list);
                }
                xdht0026DataRespDto.setList(resultList);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value);
        return xdht0026DataRespDto;
    }
}
