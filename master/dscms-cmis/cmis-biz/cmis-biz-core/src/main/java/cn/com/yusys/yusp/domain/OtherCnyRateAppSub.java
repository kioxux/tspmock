/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherCnyRateAppSub
 * @类描述: other_cny_rate_app_sub数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-08 14:44:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_cny_rate_app_sub")
public class OtherCnyRateAppSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 分项流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SUB_SERNO")
	private String subSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 定价申请类型 **/
	@Column(name = "RATE_APP_TYPE", unique = false, nullable = true, length = 5)
	private String rateAppType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户类别 **/
	@Column(name = "CUS_SIM_TYPE", unique = false, nullable = true, length = 5)
	private String cusSimType;
	
	/** 支行所属区域 **/
	@Column(name = "BANK_BELONG_AREA", unique = false, nullable = true, length = 5)
	private String bankBelongArea;
	
	/** 定价担保类型 **/
	@Column(name = "RATE_GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String rateGuarType;
	
	/** 定价贷款类型 **/
	@Column(name = "RATE_LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String rateLoanType;
	
	/** 规模类型 **/
	@Column(name = "SCALE_TYPE", unique = false, nullable = true, length = 5)
	private String scaleType;
	
	/** 定价贷款品种 **/
	@Column(name = "RATE_LOAN_KIND", unique = false, nullable = true, length = 5)
	private String rateLoanKind;
	
	/** 原贷款期限 **/
	@Column(name = "ORIGI_LOAN_TERM", unique = false, nullable = true, length = 5)
	private String origiLoanTerm;
	
	/** 展期期限 **/
	@Column(name = "EXT_LOAN_TERM", unique = false, nullable = true, length = 5)
	private String extLoanTerm;
	
	/** 贷款期限与展期期限合计 **/
	@Column(name = "TOTAL_TERM", unique = false, nullable = true, length = 5)
	private String totalTerm;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 5)
	private String loanTerm;
	
	/** 存单种类 **/
	@Column(name = "DEPOSIT_TYPE", unique = false, nullable = true, length = 5)
	private String depositType;
	
	/** 存单存期 **/
	@Column(name = "DEPOSIT_TERM", unique = false, nullable = true, length = 5)
	private String depositTerm;
	
	/** 原贷款利率 **/
	@Column(name = "ORIGI_LOAN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLoanRate;
	
	/** 综合收益率 **/
	@Column(name = "COMPRE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal compreRate;
	
	/** 保本利率 **/
	@Column(name = "BREAK_EVEN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal breakEvenRate;
	
	/** 目标利率 **/
	@Column(name = "TARGET_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal targetRate;
	
	/** 本次支行申请利率 **/
	@Column(name = "BANK_APP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankAppRate;
	
	/** 对应审批权限 **/
	@Column(name = "RULING_APPR_AUTH", unique = false, nullable = true, length = 5)
	private String rulingApprAuth;
	
	/** 测算日期 **/
	@Column(name = "EVAL_DATE", unique = false, nullable = true, length = 10)
	private String evalDate;
	
	/** 系统自动计算利率 **/
	@Column(name = "SYS_AUTO_CAL_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sysAutoCalRate;
	
	/** 支行负责人 **/
	@Column(name = "BANK_DIRECTOR", unique = false, nullable = true, length = 20)
	private String bankDirector;
	
	/** 展期利率要求 **/
	@Column(name = "EXT_APP_RATE_NEED", unique = false, nullable = true, length = 4000)
	private String extAppRateNeed;
	
	/** 所有关联客户在我行的总金额 **/
	@Column(name = "ALL_REL_CUS_TATOL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal allRelCusTatolAmt;
	
	/** 上期批复利率 **/
	@Column(name = "LAST_REPLY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastReplyRate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param rateAppType
	 */
	public void setRateAppType(String rateAppType) {
		this.rateAppType = rateAppType;
	}
	
    /**
     * @return rateAppType
     */
	public String getRateAppType() {
		return this.rateAppType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusSimType
	 */
	public void setCusSimType(String cusSimType) {
		this.cusSimType = cusSimType;
	}
	
    /**
     * @return cusSimType
     */
	public String getCusSimType() {
		return this.cusSimType;
	}
	
	/**
	 * @param bankBelongArea
	 */
	public void setBankBelongArea(String bankBelongArea) {
		this.bankBelongArea = bankBelongArea;
	}
	
    /**
     * @return bankBelongArea
     */
	public String getBankBelongArea() {
		return this.bankBelongArea;
	}
	
	/**
	 * @param rateGuarType
	 */
	public void setRateGuarType(String rateGuarType) {
		this.rateGuarType = rateGuarType;
	}
	
    /**
     * @return rateGuarType
     */
	public String getRateGuarType() {
		return this.rateGuarType;
	}
	
	/**
	 * @param rateLoanType
	 */
	public void setRateLoanType(String rateLoanType) {
		this.rateLoanType = rateLoanType;
	}
	
    /**
     * @return rateLoanType
     */
	public String getRateLoanType() {
		return this.rateLoanType;
	}
	
	/**
	 * @param scaleType
	 */
	public void setScaleType(String scaleType) {
		this.scaleType = scaleType;
	}
	
    /**
     * @return scaleType
     */
	public String getScaleType() {
		return this.scaleType;
	}
	
	/**
	 * @param rateLoanKind
	 */
	public void setRateLoanKind(String rateLoanKind) {
		this.rateLoanKind = rateLoanKind;
	}
	
    /**
     * @return rateLoanKind
     */
	public String getRateLoanKind() {
		return this.rateLoanKind;
	}
	
	/**
	 * @param origiLoanTerm
	 */
	public void setOrigiLoanTerm(String origiLoanTerm) {
		this.origiLoanTerm = origiLoanTerm;
	}
	
    /**
     * @return origiLoanTerm
     */
	public String getOrigiLoanTerm() {
		return this.origiLoanTerm;
	}
	
	/**
	 * @param extLoanTerm
	 */
	public void setExtLoanTerm(String extLoanTerm) {
		this.extLoanTerm = extLoanTerm;
	}
	
    /**
     * @return extLoanTerm
     */
	public String getExtLoanTerm() {
		return this.extLoanTerm;
	}
	
	/**
	 * @param totalTerm
	 */
	public void setTotalTerm(String totalTerm) {
		this.totalTerm = totalTerm;
	}
	
    /**
     * @return totalTerm
     */
	public String getTotalTerm() {
		return this.totalTerm;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param depositType
	 */
	public void setDepositType(String depositType) {
		this.depositType = depositType;
	}
	
    /**
     * @return depositType
     */
	public String getDepositType() {
		return this.depositType;
	}
	
	/**
	 * @param depositTerm
	 */
	public void setDepositTerm(String depositTerm) {
		this.depositTerm = depositTerm;
	}
	
    /**
     * @return depositTerm
     */
	public String getDepositTerm() {
		return this.depositTerm;
	}
	
	/**
	 * @param origiLoanRate
	 */
	public void setOrigiLoanRate(java.math.BigDecimal origiLoanRate) {
		this.origiLoanRate = origiLoanRate;
	}
	
    /**
     * @return origiLoanRate
     */
	public java.math.BigDecimal getOrigiLoanRate() {
		return this.origiLoanRate;
	}
	
	/**
	 * @param compreRate
	 */
	public void setCompreRate(java.math.BigDecimal compreRate) {
		this.compreRate = compreRate;
	}
	
    /**
     * @return compreRate
     */
	public java.math.BigDecimal getCompreRate() {
		return this.compreRate;
	}
	
	/**
	 * @param breakEvenRate
	 */
	public void setBreakEvenRate(java.math.BigDecimal breakEvenRate) {
		this.breakEvenRate = breakEvenRate;
	}
	
    /**
     * @return breakEvenRate
     */
	public java.math.BigDecimal getBreakEvenRate() {
		return this.breakEvenRate;
	}
	
	/**
	 * @param targetRate
	 */
	public void setTargetRate(java.math.BigDecimal targetRate) {
		this.targetRate = targetRate;
	}
	
    /**
     * @return targetRate
     */
	public java.math.BigDecimal getTargetRate() {
		return this.targetRate;
	}
	
	/**
	 * @param bankAppRate
	 */
	public void setBankAppRate(java.math.BigDecimal bankAppRate) {
		this.bankAppRate = bankAppRate;
	}
	
    /**
     * @return bankAppRate
     */
	public java.math.BigDecimal getBankAppRate() {
		return this.bankAppRate;
	}
	
	/**
	 * @param rulingApprAuth
	 */
	public void setRulingApprAuth(String rulingApprAuth) {
		this.rulingApprAuth = rulingApprAuth;
	}
	
    /**
     * @return rulingApprAuth
     */
	public String getRulingApprAuth() {
		return this.rulingApprAuth;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}
	
    /**
     * @return evalDate
     */
	public String getEvalDate() {
		return this.evalDate;
	}
	
	/**
	 * @param sysAutoCalRate
	 */
	public void setSysAutoCalRate(java.math.BigDecimal sysAutoCalRate) {
		this.sysAutoCalRate = sysAutoCalRate;
	}
	
    /**
     * @return sysAutoCalRate
     */
	public java.math.BigDecimal getSysAutoCalRate() {
		return this.sysAutoCalRate;
	}
	
	/**
	 * @param bankDirector
	 */
	public void setBankDirector(String bankDirector) {
		this.bankDirector = bankDirector;
	}
	
    /**
     * @return bankDirector
     */
	public String getBankDirector() {
		return this.bankDirector;
	}
	
	/**
	 * @param extAppRateNeed
	 */
	public void setExtAppRateNeed(String extAppRateNeed) {
		this.extAppRateNeed = extAppRateNeed;
	}
	
    /**
     * @return extAppRateNeed
     */
	public String getExtAppRateNeed() {
		return this.extAppRateNeed;
	}
	
	/**
	 * @param allRelCusTatolAmt
	 */
	public void setAllRelCusTatolAmt(java.math.BigDecimal allRelCusTatolAmt) {
		this.allRelCusTatolAmt = allRelCusTatolAmt;
	}
	
    /**
     * @return allRelCusTatolAmt
     */
	public java.math.BigDecimal getAllRelCusTatolAmt() {
		return this.allRelCusTatolAmt;
	}
	
	/**
	 * @param lastReplyRate
	 */
	public void setLastReplyRate(java.math.BigDecimal lastReplyRate) {
		this.lastReplyRate = lastReplyRate;
	}
	
    /**
     * @return lastReplyRate
     */
	public java.math.BigDecimal getLastReplyRate() {
		return this.lastReplyRate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}