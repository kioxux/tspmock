package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class DGSX02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGSX02BizService.class);//定义log

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_SX015.equals(bizType)){
            handleLmtGrpAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParamGrp
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamGrp(ResultInstanceDto resultInstanceDto , LmtGrpApp lmtGrpApp) {
        WFBizParamDto param = new WFBizParamDto();
        Map<String, Object> params = new HashMap<>();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        LmtGrpApp originLmtGrpApp = lmtGrpAppService.queryLmtGrpAppLmtType1ByGrpCusId(lmtGrpApp.getOrigiLmtSerno());
        String mainManagerId = originLmtGrpApp.getManagerId();
        log.info("集团客户额度调剂判断是否由集团主办人提交,提交人【{}】,主办人【{}】",mainManagerId, lmtGrpApp.getManagerId());
        if(lmtGrpApp.getManagerId().equals(mainManagerId)){
            params.put("isMainManager", CmisCommonConstants.STD_ZB_YES_NO_1);
        }else{
            params.put("isMainManager", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        param.setParam(params);
        param.setBizParam1(mainManagerId);
        param.setBizParam2(originLmtGrpApp.getManagerBrId());
        workflowCoreClient.updateFlowParam(param);
    }

    // 集团客户处理
    private void handleLmtGrpAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "集团客户额度调剂"+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
            // 重置流程参数
            put2VarParamGrp(resultInstanceDto, lmtGrpApp);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                lmtGrpAppService.handleBusinessAfterStart(serno);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                    lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
                    // 重置流程参数
                    put2VarParamGrp(resultInstanceDto, lmtGrpApp);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpAppService.handleBusinessAfterBack(serno);
                    lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
                    // 重置流程参数
                    put2VarParamGrp(resultInstanceDto, lmtGrpApp);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterRefuse(serno, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列并将自消息移除队列！！！！");
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGSX02.equals(flowCode);
    }
}
