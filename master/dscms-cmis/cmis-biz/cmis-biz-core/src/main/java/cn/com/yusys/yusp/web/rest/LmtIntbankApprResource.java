/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankAppr;
import cn.com.yusys.yusp.service.LmtIntbankApprService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 14:53:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankappr")
public class LmtIntbankApprResource {
    @Autowired
    private LmtIntbankApprService lmtIntbankApprService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankAppr> list = lmtIntbankApprService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankAppr>> index(QueryModel queryModel) {
        List<LmtIntbankAppr> list = lmtIntbankApprService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankAppr> show(@PathVariable("pkId") String pkId) {
        LmtIntbankAppr lmtIntbankAppr = lmtIntbankApprService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankAppr>(lmtIntbankAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankAppr> create(@RequestBody LmtIntbankAppr lmtIntbankAppr) throws URISyntaxException {
        lmtIntbankApprService.insert(lmtIntbankAppr);
        return new ResultDto<LmtIntbankAppr>(lmtIntbankAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankAppr lmtIntbankAppr) throws URISyntaxException {
        int result = lmtIntbankApprService.update(lmtIntbankAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询
     * @param queryModel
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtIntbankAppr> selectBySerno(@RequestBody QueryModel queryModel){
        List  LmtIntbankApprList = lmtIntbankApprService.selectByModel(queryModel);
        if(LmtIntbankApprList!=null && LmtIntbankApprList.size()>0){
            return new ResultDto<LmtIntbankAppr>(lmtIntbankApprService.selectByModel(queryModel).get(0));
        }else{
            return new ResultDto<LmtIntbankAppr>(null);
        }
    }

    /**
     * 修改审批表信息
     * @param lmtIntbankApprL
     * @return
     */
    @PostMapping("/updateAppr")
    protected  ResultDto<Integer> updateAppr(@RequestBody LmtIntbankAppr lmtIntbankApprL){
        return new ResultDto<Integer>(lmtIntbankApprService.updateSelective(lmtIntbankApprL));
    }


    /**
     * 修改审批表信息
     * @param lmtIntbankApprMap
     * @return
     */
    @PostMapping("/updateUpApprAuth")
    protected  ResultDto<Integer> updateUpApprAuth(@RequestBody Map lmtIntbankApprMap){
        return new ResultDto<Integer>(lmtIntbankApprService.updateUpApprAuth(lmtIntbankApprMap));
    }


    /**
     * @函数名称:updateRestByPkId
     * @函数描述:根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkId")
    protected int updateRestByPkId(@RequestBody QueryModel model){
        return lmtIntbankApprService.updateRestByPkId(model);
    }

    /**
     * @函数名称:updateRestByPkIdZH
     * @函数描述:根据Serno更新信贷管理部风险派驻岗审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkIdZH")
    protected int updateRestByPkIdZH(@RequestBody QueryModel model){
        return lmtIntbankApprService.updateRestByPkIdZH(model);
    }
}
