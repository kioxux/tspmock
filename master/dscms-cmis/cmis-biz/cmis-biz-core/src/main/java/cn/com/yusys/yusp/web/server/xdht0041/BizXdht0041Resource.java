package cn.com.yusys.yusp.web.server.xdht0041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0041.req.Xdht0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0041.resp.Xdht0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0041.Xdht0041Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询受托记录状态
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0041:查询受托记录状态")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0041Resource.class);

    @Autowired
    private Xdht0041Service xdht0041Service;

    /**
     * 交易码：xdht0041
     * 交易描述：查询受托记录状态
     *
     * @param xdht0041DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询受托记录状态")
    @PostMapping("/xdht0041")
    protected @ResponseBody
    ResultDto<Xdht0041DataRespDto> xdht0041(@Validated @RequestBody Xdht0041DataReqDto xdht0041DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataReqDto));
        Xdht0041DataRespDto xdht0041DataRespDto = new Xdht0041DataRespDto();// 响应Dto:查询受托记录状态
        ResultDto<Xdht0041DataRespDto> xdht0041DataResultDto = new ResultDto<>();
        // 从xdht0041DataReqDto获取业务值进行业务逻辑处理
        try {
            String accotPk = xdht0041DataReqDto.getAccotPk();
            String billNo = xdht0041DataReqDto.getBillNo();

            if (StringUtils.isEmpty(billNo)) {
                xdht0041DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0041DataResultDto.setMessage("借据号【billNo】不能为空！");
                return xdht0041DataResultDto;
            }
            if (StringUtils.isEmpty(accotPk)) {
                xdht0041DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0041DataResultDto.setMessage("核心流水【accotPk】不能为空！");
                return xdht0041DataResultDto;
            }
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataReqDto));
            xdht0041DataRespDto = xdht0041Service.getXdht0041(xdht0041DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataRespDto));
            // 封装xdht0041DataResultDto中正确的返回码和返回信息
            xdht0041DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0041DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdht0041DataResultDto.setCode(e.getErrorCode());
            xdht0041DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            // 封装xdht0041DataResultDto中异常返回码和返回信息
            xdht0041DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0041DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0041DataRespDto到xdht0041DataResultDto中
        xdht0041DataResultDto.setData(xdht0041DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataResultDto));
        return xdht0041DataResultDto;
    }
}