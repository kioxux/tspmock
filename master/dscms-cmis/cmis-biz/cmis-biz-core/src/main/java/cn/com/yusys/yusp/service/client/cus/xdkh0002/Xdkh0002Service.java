package cn.com.yusys.yusp.service.client.cus.xdkh0002;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0002.req.Xdkh0002ReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询对公客户信息
 *
 * @author zrcbank-fengjj
 * @version 1.0
 * @since 2021年8月24日 下午1:22:06
 */
@Service
public class Xdkh0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0002Service.class);

    // 1）注入：封装的接口类:客户管理模块
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 业务逻辑处理方法：查询对公客户信息
     *
     * @param xdkh0002ReqDto
     * @return
     */
    @Transactional
    public Xdkh0002DataRespDto xdkh0002(Xdkh0002DataReqDto xdkh0002ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002ReqDto));
        ResultDto<Xdkh0002DataRespDto> xdkh0002ResultDto = dscmsCusClientService.xdkh0002(xdkh0002ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002ResultDto));

        String Xdkh0002Code = Optional.ofNullable(xdkh0002ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String Xdkh0002Meesage = Optional.ofNullable(xdkh0002ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Xdkh0002DataRespDto xdkh0002DataRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0002ResultDto.getCode())) {
            //  获取相关的值并解析
            xdkh0002DataRespDto = xdkh0002ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, Xdkh0002Code, Xdkh0002Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value);
        return xdkh0002DataRespDto;
    }
}
