/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.GuarGuaranteeDto;
import cn.com.yusys.yusp.service.GuarGuaranteeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGuaranteeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-28 20:27:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "保证人")
@RequestMapping("/api/guarguarantee")
public class GuarGuaranteeResource {
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarGuarantee>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarGuarantee> list = guarGuaranteeService.selectAll(queryModel);
        return new ResultDto<List<GuarGuarantee>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<GuarGuarantee>> index(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<GuarGuarantee> list = guarGuaranteeService.selectByModel(queryModel);
        return new ResultDto<List<GuarGuarantee>>(list);
    }

    /**
     * 根据担保合同编号查询关联的保证人信息
     * @param queryModel
     * @return
     */
    @PostMapping("/querylistbyguarcontno")
    protected ResultDto<List<GuarGuarantee>> queryListByGuarContNo(@RequestBody QueryModel queryModel) {
        List<GuarGuarantee> list = guarGuaranteeService.queryListByGuarContNo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{guarantyId}")
    protected ResultDto<GuarGuarantee> show(@PathVariable("guarantyId") String guarantyId) {
        GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarantyId);
        return new ResultDto<GuarGuarantee>(guarGuarantee);
    }

    /**
     * @函数名称:queryByGuarantyId
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByGuarantyId")
    protected ResultDto<GuarGuarantee> queryByGuarantyId(@RequestBody GuarGuarantee guarGuarantee) {
        return new ResultDto<GuarGuarantee>(guarGuaranteeService.selectByPrimaryKey(guarGuarantee.getGuarantyId()));
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarGuarantee> create(@RequestBody GuarGuarantee guarGuarantee) throws URISyntaxException {
        guarGuaranteeService.insert(guarGuarantee);
        return new ResultDto<GuarGuarantee>(guarGuarantee);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarGuarantee guarGuarantee) throws URISyntaxException {
        int result = guarGuaranteeService.updateSelective(guarGuarantee);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{guarantyId}")
    protected ResultDto<Integer> delete(@PathVariable("guarantyId") String guarantyId) {
        int result = guarGuaranteeService.deleteByPrimaryKey(guarantyId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarGuaranteeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return List
     * @author 王祝远
     * @date 2021/4/23 16:42
     * @version 1.0.0
     * @desc 根据业务流水号查询担保人信息
     * @修改历史: V1.0
     */
    @ApiOperation("保证人列表查询")
    @PostMapping("/querguaranteerel")
    protected ResultDto<List<GuarBizRelGuaranteeDto>> queryGuaranteefoSell(@RequestBody QueryModel queryModel) {
        List<GuarBizRelGuaranteeDto> list = (List<GuarBizRelGuaranteeDto>) guarGuaranteeService.queryGuarInfoSell(queryModel);
        return new ResultDto<List<GuarBizRelGuaranteeDto>>(list);
    }

    /**
     * @param serno
     * @return List
     * @author 王祝远
     * @date 2021/4/23 16:42
     * @version 1.0.0
     * @desc 根据业务流水号删除担保人信息
     * @修改历史: V1.0
     */
    @GetMapping("/deleteGuaranteefoSell/{serno}")
    protected ResultDto<Integer> deleteGuaranteefoSell(@PathVariable("serno") String serno) {
        int result = guarGuaranteeService.deleteGuaranteefoSell(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectGuarGuaranteeByGuarContNo
     * @函数描述:根据担保合同编号获取保证人信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectGuarGuaranteeByGuarContNo")
    @ApiOperation("根据担保合同编号获取保证人信息")
    protected ResultDto<List<GuarGuarantee>> selectGuarGuaranteeByGuarContNo(@RequestBody QueryModel queryModel) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
        return new ResultDto<List<GuarGuarantee>>(guarGuaranteeList);
    }

    /**
     * @函数名称:queryGuarGuaranteeIsUnderLmt
     * @函数描述:根据额度编号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据业务流水号查询授信项下的押品保证人信息")
    @PostMapping("/queryGuarGuaranteeIsUnderLmt")
    protected ResultDto<List<GuarGuarantee>> queryGuarGuaranteeIsUnderLmt(@RequestBody QueryModel queryModel) {
        List<GuarGuarantee> list = guarGuaranteeService.queryGuarGuaranteeIsUnderLmt(queryModel);
        return new ResultDto<List<GuarGuarantee>>(list);
    }

    /**
     * 根据业务流水号查询担保人信息
     *
     * @param map
     * @return
     * @author lyj
     */
    @PostMapping("/queryGuarGuaranteeBySerno")
    protected ResultDto<List<Map<String, Object>>> queryGuarGuaranteeBySerno(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        return new ResultDto<List<Map<String, Object>>>(guarGuaranteeService.queryGuarGuaranteeBySerno(serno));
    }


    /**
     * @方法名称：queryCusBaseByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据担保合同编号关联查询保证人信息
     */
    @PostMapping("/queryGuarGuaranteeByGuarContNo")
    protected ResultDto<List<GuarGuaranteeDto>> queryGuarGuaranteeByGuarContNo(@RequestBody String guarContNo) {
        List<GuarGuaranteeDto> list = guarGuaranteeService.queryGuarGuaranteeByGuarContNo(guarContNo);
        return new ResultDto<List<GuarGuaranteeDto>>(list);
    }

    /**
     * @param map
     * @return List
     * @author css
     * @desc 审查报告查询保证人列表
     * @修改历史: V1.0
     */
    @ApiOperation("审查报告查询保证人列表")
    @PostMapping("/querguaranteerelbylmtappserno")
    protected ResultDto<List<Map>> querGuaranteeRelByLmtAppSerno(@RequestBody Map map) {
        List<Map> list = guarGuaranteeService.querGuaranteeRelByLmtAppSerno(map);
        return new ResultDto<List<Map>>(list);
    }

}
