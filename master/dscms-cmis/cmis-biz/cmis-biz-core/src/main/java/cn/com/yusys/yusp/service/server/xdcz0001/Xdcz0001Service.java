package cn.com.yusys.yusp.service.server.xdcz0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.domain.BusiAppCoopRel;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAccInfo;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.dto.CfgTfRateDto;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.req.Xdcz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.resp.Xdcz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.repository.mapper.BusiAppCoopRelMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.DscmsCfgQtClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:电子保函开立
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdcz0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0001Service.class);
    @Autowired
    private BusiAppCoopRelMapper busiAppCoopRelMapper;// 业务与合作方协议关联表
    @Autowired
    private AccCvrsMapper accCvrsMapper;// 保函台账
    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;// 保函协议
    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;// 合作方协议台账
    @Autowired
    private CmisLmtClientService cmisLmtClientService;// 调接口更新台账编号
    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;// 调接口查产品信息
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;// 调接口查汇率信息
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * 电子保函开立
     *
     * @param xdcz0001DataReqDto
     * @return xdcz0001DataRespDto
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0001DataRespDto xdcz0001(Xdcz0001DataReqDto xdcz0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value);
        Xdcz0001DataRespDto xdcz0001DataRespDto = new Xdcz0001DataRespDto();
        initxdcz0001DataRespDto(xdcz0001DataRespDto); //初始化数据

        String contno = xdcz0001DataReqDto.getContno();//合作协议编号
        String custid = xdcz0001DataReqDto.getCustid();//客户代码
        String custna = xdcz0001DataReqDto.getCustna();//客户名称
        String prodno = xdcz0001DataReqDto.getProdno();//产品编号
        String bhtype = xdcz0001DataReqDto.getBhtype();//保函类型
        String curren = xdcz0001DataReqDto.getCurren();//币种
        BigDecimal bhhamt = xdcz0001DataReqDto.getBhhamt();//申请金额
        String bhstda = xdcz0001DataReqDto.getBhstda();//生效日期
        String bhedda = xdcz0001DataReqDto.getBhedda();//到期日期
        String jsacct = xdcz0001DataReqDto.getJsacct();//结算账号
        String jsacna = xdcz0001DataReqDto.getJsacna();//结算账号户名
        BigDecimal sxfamt = xdcz0001DataReqDto.getSxfamt();//手续费金额
        BigDecimal sxflll = xdcz0001DataReqDto.getSxflll();//手续费率
        String xmname = xdcz0001DataReqDto.getXmname();//项目名称
        BigDecimal xmmamt = xdcz0001DataReqDto.getXmmamt();//项目金额
        String coreno = xdcz0001DataReqDto.getCoreno();//合同协议或标书编号
        BigDecimal xghtje = xdcz0001DataReqDto.getXghtje();//相关贸易合同金额
        String syrnam = xdcz0001DataReqDto.getSyrnam();//受益人名称
        String syradd = xdcz0001DataReqDto.getSyradd();//受益人地址
        String syrkhh = xdcz0001DataReqDto.getSyrkhh();//受益人开户行
        String syracc = xdcz0001DataReqDto.getSyracc();//受益人账号
        String bhfkfs = xdcz0001DataReqDto.getBhfkfs();//保函付款方式
        String cftjsm = xdcz0001DataReqDto.getCftjsm();//承付条件说明
        String zwogid = xdcz0001DataReqDto.getZwogid();//账务机构
        String inptid = xdcz0001DataReqDto.getInptid();//登记人
        String mangid = xdcz0001DataReqDto.getMangid();//责任人
        String inbrid = xdcz0001DataReqDto.getInbrid();//登记人机构
        String mabrid = xdcz0001DataReqDto.getMabrid();//责任人机构
        String indate = xdcz0001DataReqDto.getIndate();//登记日期
        String tbcuid = xdcz0001DataReqDto.getTbcuid();//投标人客户号
        String billno = xdcz0001DataReqDto.getBillno();//借据号
        String bhhtno = xdcz0001DataReqDto.getBhhtno();//保函协议编号
        String issece = xdcz0001DataReqDto.getIssece();//是否密文
        if (StringUtils.isBlank(issece)) {
            throw BizException.error(null, EpbEnum.EPB090009.key, "是否密文issece不能为空");
        }
        if ("3".equals(issece)) {
            if (StringUtils.isBlank(contno)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "合作协议编号contno不能为空");
            }
            if (StringUtils.isBlank(tbcuid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "投标人客户号tbcuid不能为空");
            }
        } else if ("4".equals(issece)) {
            if (StringUtils.isBlank(contno)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "合作协议编号contno不能为空");
            }
            if (StringUtils.isBlank(tbcuid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "投标人客户号tbcuid不能为空");
            }
        }
        if ("4".equals(issece) || "5".equals(issece) || "6".equals(issece)) {
            if (StringUtils.isBlank(custid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "客户代码custid不能为空");
            }
            if (StringUtils.isBlank(custna)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "客户名称custna不能为空");
            }
            if (StringUtils.isBlank(prodno)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "产品编号prodno不能为空");
            }
            if (StringUtils.isBlank(bhtype)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "保函类型bhtype不能为空");
            }
            if (StringUtils.isBlank(curren)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "币种curren不能为空");
            }
            if (bhhamt == null) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "申请金额bhhamt不能为空");
            }
            if (StringUtils.isBlank(bhstda)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "生效日期bhstda不能为空");
            }
            if (StringUtils.isBlank(bhedda)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "到期日期bhedda不能为空");
            }
            if (StringUtils.isBlank(jsacct)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "结算账号jsacct不能为空");
            }
            if (StringUtils.isBlank(jsacna)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "结算账号户名jsacna不能为空");
            }
            if (StringUtils.isBlank(coreno)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "合同协议或标书编coreno号不能为空");
            }
            if (StringUtils.isBlank(syrnam)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "受益人名称syrnam不能为空");
            }
            if (StringUtils.isBlank(bhfkfs)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "保函付款方式bhfkfs不能为空");
            }
            if (StringUtils.isBlank(zwogid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "账务机构zwogid不能为空");
            }
            if (StringUtils.isBlank(inptid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "登记人inptid不能为空");
            }
            if (StringUtils.isBlank(mangid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "责任人mangid不能为空");
            }
            if (StringUtils.isBlank(inbrid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "登记人机构inbrid不能为空");
            }
            if (StringUtils.isBlank(mabrid)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "责任人机构mabrid不能为空");
            }
            if (StringUtils.isBlank(indate)) {
                throw BizException.error(null, EpbEnum.EPB090009.key, "登记日期indate不能为空");
            }
        }
        try {
            //公用参数
            HashMap commonMap = new HashMap<String, String>();
            if ("3".equals(issece)) {
                // TODO 生成流水号规则 中文合同编号是否要根据担保方式生成
                //String bill_no = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, new HashMap<>());//借据号
                //String cont_no = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, new HashMap<>());//合同号
                //String cnContNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, new HashMap<>());//中文合同编号
                String bill_no = UUID.randomUUID().toString();//借据号
                String cont_no = UUID.randomUUID().toString();//合同号
                String cnContNo = UUID.randomUUID().toString();//中文合同编号
                // 业务与合作方协议关联表
                BusiAppCoopRel BusiAppCoopRel = new BusiAppCoopRel();
                BusiAppCoopRel.setContNo(cont_no);//合同号
                BusiAppCoopRel.setContCnNo(cnContNo);//中文合同编号
                BusiAppCoopRel.setBillNo(bill_no);//借据号
                BusiAppCoopRel.setCoopAgrNo(contno);//合作协议编号
                // 新增一条记录
                logger.info("场景3：开始新增合作方关联业务记录");
                busiAppCoopRelMapper.insert(BusiAppCoopRel);
                logger.info("场景3：新增合作方关联业务记录完成");

                //返回数据
                xdcz0001DataRespDto.setBillno(bill_no);// 借据号
                xdcz0001DataRespDto.setContno(cont_no);// 保函协议编号
                xdcz0001DataRespDto.setCncono(cnContNo);// 中文合同编号
                xdcz0001DataRespDto.setAsmean("00");//  担保方式 默认信用
            } else if ("4".equals(issece)) {
                //1.0 检查有无有效的合作方协议编号 02 作废 01 生效 coop_partner_agr_acc_info 合作方协议台账信息
                logger.info("场景4：查询合作方台账开始，参数为：{}", contno);
                CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = coopPartnerAgrAccInfoMapper.queryValidByCoopAgrNo(contno);
                logger.info("场景4：查询合作方台账结束，返回为：{}", JSON.toJSONString(coopPartnerAgrAccInfo));
                if (coopPartnerAgrAccInfo == null) {
                    throw BizException.error(null, EpbEnum.EPB090004.key, "未查询到有效的合作协议编号" + contno);
                }
                // TODO 应该取线上保函额度，表中没有，新增后替换
                //2.0 检查当前申请金额是否大于该合作方在线保函额度
                BigDecimal coopAgrAmt = coopPartnerAgrAccInfo.getCoopAgrAmt();//合作协议金额
                if (bhhamt.compareTo(coopAgrAmt) > 0) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "当前申请金额" + bhhamt.toString() + "元大于该合作方在线保函额度" + coopAgrAmt.toString() + "元!");
                }

                //3.0 检查当前申请金额+存量在线保函的签发额度之和是否大于该合作方在线保函额度
                logger.info("场景4：查询存量在线保函的签发额度之和开始，参数为：{}", custid);
                BigDecimal sumBHAmt = accCvrsMapper.selectSumAmtByCusId(custid);//存量在线保函的签发额度之和
                logger.info("场景4：查询存量在线保函的签发额度之和结束，返回为：{}", JSON.toJSONString(sumBHAmt));
                if ((bhhamt.add(sumBHAmt)).compareTo(coopAgrAmt) > 0) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "当前申请金额" + bhhamt.toString() + "元与存量在线保函的签发额度之和" + sumBHAmt.toString() + "元大于该合作方在线保函额度" + coopAgrAmt.toString() + "元!");
                }


                // 获取产品名称
                Xdqt0001DataReqDto xdqt0001DataReqDto = new Xdqt0001DataReqDto();
                xdqt0001DataReqDto.setPrdId(prodno);//产品编号
                logger.info("场景4：查询产品映射开始，参数为：{}", JSON.toJSONString(xdqt0001DataReqDto));
                ResultDto<Xdqt0001DataRespDto> Xdqt0001DataResultDto = dscmsCfgQtClientService.xdqt0001(xdqt0001DataReqDto);
                logger.info("场景4：查询产品映射开始结束，返回为：{}", JSON.toJSONString(Xdqt0001DataResultDto));
                String prdName = Xdqt0001DataResultDto.getData().getPrdName();
                // 获取中文合同编号
                BusiAppCoopRel busiAppCoopRel = busiAppCoopRelMapper.selectByContNo(bhhtno);
                String cnContNo = busiAppCoopRel.getContCnNo();
                // 获取汇率
                CfgTfRateQueryDto cfgTfRateQueryDto = new CfgTfRateQueryDto();
                cfgTfRateQueryDto.setCurType(curren);//币种
                logger.info("场景4：查询汇率开始，参数为：{}", JSON.toJSONString(cfgTfRateQueryDto));
                ResultDto<CfgTfRateDto> CfgTfRateResultDto = iCmisCfgClientService.queryCfgTfRate(cfgTfRateQueryDto);
                logger.info("场景4：查询汇率结束，返回为：{}", JSON.toJSONString(CfgTfRateResultDto));
                String exchangeRate = CfgTfRateResultDto.getData().getExrtst();
                // 设置参数
                commonMap.put("prdName", prdName);//产品名称
                commonMap.put("cnContNo", cnContNo);//中文合同编号
                commonMap.put("exchangeRate", exchangeRate);//汇率
                commonMap.put("guarMode", "00");// 担保方式 默认为 信用
                commonMap.put("bailPerc", "0.00");// 保证金比例(%)
                commonMap.put("bailAmt", "0.00");// 保证金金额
                commonMap.put("bailCurType", "");// 保证金币种
                commonMap.put("bailExchangeRate", "0.00");// 保证金汇率
                commonMap.put("bailCvtCnyAmt", "0.00");// 保证金折算人民币金额
                commonMap.put("isUtilLmt", "0"); //是否使用授信额度 默认否
                commonMap.put("lmtAccNo", "");//授信台账编号


                // 4.0 生成保函协议CTR_CVRG_CONT 保函台账ACC_CVRG;
                logger.info("场景4：开始生成保函协议");
                createCtrCvrgCont(xdcz0001DataReqDto, commonMap);// 生成保函协议
                logger.info("场景4：生成保函协议完成");
                logger.info("场景4：开始生成保函台账");
                createAccCvrs(xdcz0001DataReqDto, commonMap);// 生成保函台账
                logger.info("场景4：生成保函台账完成");

                // 5.0 返回数据
                resultxdcz0001DataRespDto(xdcz0001DataRespDto, xdcz0001DataReqDto, commonMap);
            } else if ("5".equals(issece)) {
                // 1.0 是否已存在保函借据
                AccCvrs accCvrs = accCvrsMapper.selectByBillno(billno);
                if (accCvrs != null) {
                    throw BizException.error(null, "1111", "该保函借据已存在，不可重复开立" + billno);
                }

                //2.0 根据请求参数 校验有无有效的保函协议并获取部分协议字段.
                HashMap queryMap = new HashMap();
                queryMap.put("contNo", bhhtno);
                logger.info("场景5：根据编号【{}】查询有效的保函协议", bhhtno);
                CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.getCvrgContDetailByContNo(queryMap);
                logger.info("场景5：根据编号【{}】查询有效的保函协议，返回值为：{}", bhhtno, JSON.toJSONString(ctrCvrgCont));
                if (ctrCvrgCont != null) {
                    commonMap.put("prdName", ctrCvrgCont.getPrdName());//产品名称
                    commonMap.put("cnContNo", ctrCvrgCont.getContCnNo());//中文合同编号
                    commonMap.put("exchangeRate", ctrCvrgCont.getExchangeRate());//汇率
                    commonMap.put("guarMode", ctrCvrgCont.getGuarMode());// 担保方式 默认为 信用
                    commonMap.put("bailPerc", ctrCvrgCont.getBailPerc() == null ? "0" : ctrCvrgCont.getBailPerc().toString());// 保证金比例(%)
                    commonMap.put("bailAmt", ctrCvrgCont.getBailAmt() == null ? "0" : ctrCvrgCont.getBailAmt().toString());// 保证金金额
                    commonMap.put("bailCurType", ctrCvrgCont.getBailCurType());// 保证金币种
                    commonMap.put("bailExchangeRate", ctrCvrgCont.getBailExchangeRate() == null ? "0" : ctrCvrgCont.getBailExchangeRate().toString());// 保证金汇率
                    commonMap.put("bailCvtCnyAmt", ctrCvrgCont.getBailCvtCnyAmt() == null ? "0" : ctrCvrgCont.getBailCvtCnyAmt().toString());// 保证金折算人民币金额
                    commonMap.put("lmtAccNo", ctrCvrgCont.getLmtAccNo());//授信台账编号
                } else {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "未查询到有效的线上保函协议！");
                }

                // 3.0 生成保函台账
                logger.info("场景5：开始生成保函台账");
                createAccCvrs(xdcz0001DataReqDto, commonMap);// 生成保函台账
                logger.info("场景5：生成保函台账完成");

                // 5.0 返回数据
                resultxdcz0001DataRespDto(xdcz0001DataRespDto, xdcz0001DataReqDto, commonMap);
            } else if ("6".equals(issece)) {
                //1.0 根据请求参数cont_no校验有无有效的保函协议并获取部分协议字段.
                HashMap queryMap = new HashMap();
                queryMap.put("contNo", bhhtno);
                logger.info("场景6：查询有效的保函协议开始,参数为：", JSON.toJSONString(queryMap));
                CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.getCvrgContDetailByContNo(queryMap);
                logger.info("场景6：查询有效的保函协议结束,参数为：", JSON.toJSONString(ctrCvrgCont));
                if (ctrCvrgCont != null) {
                    commonMap.put("prdName", ctrCvrgCont.getPrdName());//产品名称
                    commonMap.put("cnContNo", ctrCvrgCont.getContCnNo());//中文合同编号
                    commonMap.put("exchangeRate", ctrCvrgCont.getExchangeRate());//汇率
                    commonMap.put("guarMode", ctrCvrgCont.getGuarMode());// 担保方式 默认为 信用
                    commonMap.put("bailPerc", ctrCvrgCont.getBailPerc() == null ? "0" : ctrCvrgCont.getBailPerc().toString());// 保证金比例(%)
                    commonMap.put("bailAmt", ctrCvrgCont.getBailAmt() == null ? "0" : ctrCvrgCont.getBailAmt().toString());// 保证金金额
                    commonMap.put("bailCurType", ctrCvrgCont.getBailCurType());// 保证金币种
                    commonMap.put("bailExchangeRate", ctrCvrgCont.getBailExchangeRate() == null ? "0" : ctrCvrgCont.getBailExchangeRate().toString());// 保证金汇率
                    commonMap.put("bailCvtCnyAmt", ctrCvrgCont.getBailCvtCnyAmt() == null ? "0" : ctrCvrgCont.getBailCvtCnyAmt().toString());// 保证金折算人民币金额
                    commonMap.put("lmtAccNo", ctrCvrgCont.getLmtAccNo());//授信台账编号
                } else {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "未查询到有效的线上保函协议！");
                }
                // 2.0 更新保函台账
                logger.info("场景6：开始更新保函台账");
                updateAccCvrs(xdcz0001DataReqDto, commonMap);// 更新保函台账
                logger.info("场景6：更新保函台账完成");

                // 3.0 返回数据
                resultxdcz0001DataRespDto(xdcz0001DataRespDto, xdcz0001DataReqDto, commonMap);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value);
        return xdcz0001DataRespDto;
    }

    /**
     * 生成保函协议
     *
     * @param xdcz0001DataReqDto
     * @return
     */
    public boolean createCtrCvrgCont(Xdcz0001DataReqDto xdcz0001DataReqDto, HashMap<String, String> commonMap) {
        try {
            CtrCvrgCont ctrCvrgCont = new CtrCvrgCont();
            // TODO 生成流水号规则 收益人信息缺失
            //String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, new HashMap<>());
            String serno = UUID.randomUUID().toString();//流水号
            ctrCvrgCont.setSerno(serno);        // 流水号
            ctrCvrgCont.setContNo(xdcz0001DataReqDto.getContno());// 合同编号
            ctrCvrgCont.setContCnNo(commonMap.get("cnContNo"));// 中文合同编号
            ctrCvrgCont.setCusId(xdcz0001DataReqDto.getCustid());// 客户编号
            ctrCvrgCont.setPrdId(xdcz0001DataReqDto.getProdno());// 产品编号
            ctrCvrgCont.setCusName(xdcz0001DataReqDto.getCustna());// 客户名称
            ctrCvrgCont.setPrdName(commonMap.get("prdName"));// 产品名称
            ctrCvrgCont.setBusiType("08");// 业务类型 保函合同
            ctrCvrgCont.setContType("2");// 合同类型 最高额合同
            ctrCvrgCont.setGuarMode(commonMap.get("guarMode"));// 担保方式
            ctrCvrgCont.setCurType(xdcz0001DataReqDto.getCurren());// 合同币种
            ctrCvrgCont.setContAmt(xdcz0001DataReqDto.getBhhamt());// 合同金额
            ctrCvrgCont.setContHighAvlAmt(null);// 本合同项下最高可用信金额
            ctrCvrgCont.setContTerm(null);// 合同期限
            ctrCvrgCont.setStartDate(xdcz0001DataReqDto.getBhstda());// 合同起始日
            ctrCvrgCont.setEndDate(xdcz0001DataReqDto.getBhedda());// 合同到期日
            ctrCvrgCont.setIsRenew(null);// 是否续签
            ctrCvrgCont.setOrigiContNo(null);// 原合同编号
            ctrCvrgCont.setIsUtilLmt("0");// 是否使用授信额度 默认否
            ctrCvrgCont.setLmtAccNo(null);// 授信台账编号
            ctrCvrgCont.setIsOlPld(null);// 是否在线抵押
            ctrCvrgCont.setIsESeal(null);// 是否电子用印
            ctrCvrgCont.setBelgLine(null);// 所属条线
            ctrCvrgCont.setDoubleRecordNo(null);// 双录编号
            ctrCvrgCont.setPaperContSignDate(null);// 纸质合同签订日期
            ctrCvrgCont.setContStatus("200");// 合同状态 默认生效
            ctrCvrgCont.setLinkman(null);// 联系人
            ctrCvrgCont.setPhone(null);// 电话
            ctrCvrgCont.setFax(null);// 传真
            ctrCvrgCont.setEmail(null);// 邮箱
            ctrCvrgCont.setQq(null);// QQ
            ctrCvrgCont.setWechat(null);// 微信
            ctrCvrgCont.setDeliveryAddr(null);// 送达地址
            ctrCvrgCont.setGuarantType(xdcz0001DataReqDto.getBhtype());// 保函类型
            ctrCvrgCont.setGuarantMode(xdcz0001DataReqDto.getBhtype());// 保函种类
            ctrCvrgCont.setExchangeRate(commonMap.get("exchangeRate"));// 汇率
            ctrCvrgCont.setCvtCnyAmt((new BigDecimal(commonMap.get("exchangeRate"))).multiply(xdcz0001DataReqDto.getBhhamt()));// 折算人民币金额
            ctrCvrgCont.setBailPerc(BigDecimal.ZERO);// 保证金比例
            ctrCvrgCont.setBailCurType(null);// 保证金币种
            ctrCvrgCont.setBailAmt(BigDecimal.ZERO);// 保证金金额
            ctrCvrgCont.setBailExchangeRate(BigDecimal.ZERO);// 保证金汇率
            ctrCvrgCont.setBailCvtCnyAmt(BigDecimal.ZERO);// 保证金折算人民币金额
            ctrCvrgCont.setChrgRate(xdcz0001DataReqDto.getSxflll());// 手续费率（‰）
            ctrCvrgCont.setChrgAmt(xdcz0001DataReqDto.getSxfamt());// 手续费金额
            ctrCvrgCont.setIsEGuarant(null);// 是否线上保函
            ctrCvrgCont.setIsAgentbankGuarant("0");// 是否为转开代理行保函 否
            ctrCvrgCont.setAgentbankName(null);// 代理行名称
            ctrCvrgCont.setProName(xdcz0001DataReqDto.getXmname());// 项目名称
            ctrCvrgCont.setProAmt(String.valueOf(xdcz0001DataReqDto.getXmmamt()));// 项目金额
            ctrCvrgCont.setContAgr(xdcz0001DataReqDto.getCoreno());// 合同协议
            ctrCvrgCont.setBeneficiarName(xdcz0001DataReqDto.getSyrnam());// 受益人名称
            ctrCvrgCont.setGuarantPayMode(xdcz0001DataReqDto.getBhfkfs());// 保函付款方式
            ctrCvrgCont.setGuarantHonourCond(xdcz0001DataReqDto.getCftjsm());// 保函承付条件说明
            ctrCvrgCont.setCorreBusnesContAmt(xdcz0001DataReqDto.getXghtje());// 相关贸易合同金额
            ctrCvrgCont.setDebtLevel(null);// 债项等级
            ctrCvrgCont.setLgd(null);// 违约损失率LGD
            ctrCvrgCont.setEad(null);// 违约风险暴露EAD
            ctrCvrgCont.setPd(null);// 转敞口对象的PD
            ctrCvrgCont.setOprType(null);// 操作类型
            ctrCvrgCont.setInputId(xdcz0001DataReqDto.getInptid());// 登记人
            ctrCvrgCont.setInputBrId(xdcz0001DataReqDto.getInbrid());// 登记机构
            ctrCvrgCont.setInputDate(xdcz0001DataReqDto.getIndate());// 登记日期
            ctrCvrgCont.setUpdId(xdcz0001DataReqDto.getInptid());// 最近修改人
            ctrCvrgCont.setUpdBrId(xdcz0001DataReqDto.getInbrid());// 最近修改机构
            ctrCvrgCont.setUpdDate(xdcz0001DataReqDto.getIndate());// 最近修改日期
            ctrCvrgCont.setManagerId(xdcz0001DataReqDto.getMangid());// 主管客户经理
            ctrCvrgCont.setManagerBrId(xdcz0001DataReqDto.getInbrid());// 主管机构
            ctrCvrgCont.setCreateTime(new Date());// 创建时间
            ctrCvrgCont.setUpdateTime(new Date());// 修改时间
            if (ctrCvrgContMapper.insert(ctrCvrgCont) == 0) {
                throw BizException.error(null, EpbEnum.EPB090002.key, "新增保函协议失败");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        return true;
    }

    /**
     * 生成保函台账
     *
     * @param xdcz0001DataReqDto commonMap
     * @return
     */
    public void createAccCvrs(Xdcz0001DataReqDto xdcz0001DataReqDto, HashMap<String, String> commonMap) {
        try {
            AccCvrs accCvrs = new AccCvrs();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
            accCvrs.setPkId(StringUtils.getUUID());
            accCvrs.setPvpSerno(serno);//放款流水号
            accCvrs.setBillNo(xdcz0001DataReqDto.getBillno());//借据编号
            accCvrs.setInputId(xdcz0001DataReqDto.getInptid());//登记人
            accCvrs.setInputBrId(xdcz0001DataReqDto.getInbrid());//登记机构
            accCvrs.setInputDate(xdcz0001DataReqDto.getIndate());//登记日期
            accCvrs.setManagerId(xdcz0001DataReqDto.getMangid());//主管客户经理
            accCvrs.setManagerBrId(xdcz0001DataReqDto.getMabrid());//主管机构
            accCvrs.setCreateTime(new Date());//创建时间
            // 赋值
            setAccCvrs(accCvrs, xdcz0001DataReqDto, commonMap);
            if (accCvrsMapper.insert(accCvrs) == 0) {
                throw BizException.error(null, EpbEnum.EPB090002.key, "新增保函台账失败");
            }
            CmisLmt0013ReqDto cmisLmt0013ReqDto = new CmisLmt0013ReqDto();
            cmisLmt0013ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0013ReqDto.setInstuCde(CmisCommonUtils.getInstucde(xdcz0001DataReqDto.getMabrid()));//金融机构代码
            cmisLmt0013ReqDto.setSerno(serno);
            cmisLmt0013ReqDto.setInputBrId(xdcz0001DataReqDto.getInbrid());
            cmisLmt0013ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            cmisLmt0013ReqDto.setInputId(xdcz0001DataReqDto.getInptid());
            cmisLmt0013ReqDto.setBizNo(xdcz0001DataReqDto.getBhhtno());
            List<CmisLmt0013ReqDealBizListDto> cmisLmt0013ReqDealBizListDtos = new ArrayList<CmisLmt0013ReqDealBizListDto>();
            CmisLmt0013ReqDealBizListDto cmisLmt0013ReqDealBizListDto = new CmisLmt0013ReqDealBizListDto();
            cmisLmt0013ReqDealBizListDto.setDealBizNo(xdcz0001DataReqDto.getBillno());//台账编号
            cmisLmt0013ReqDealBizListDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0013ReqDealBizListDto.setOrigiDealBizNo("");//原交易业务编号
            cmisLmt0013ReqDealBizListDto.setOrigiRecoverType("");//原交易业务恢复类型
            cmisLmt0013ReqDealBizListDto.setOrigiBizAttr("");//原交易属性
            cmisLmt0013ReqDealBizListDto.setCusId(xdcz0001DataReqDto.getCustid());
            cmisLmt0013ReqDealBizListDto.setCusName(xdcz0001DataReqDto.getCustna());
            cmisLmt0013ReqDealBizListDto.setPrdId(xdcz0001DataReqDto.getProdno());
            cmisLmt0013ReqDealBizListDto.setPrdName(commonMap.get("prdName"));
            cmisLmt0013ReqDealBizListDto.setDealBizAmtCny(xdcz0001DataReqDto.getBhhamt());
            cmisLmt0013ReqDealBizListDto.setDealBizSpacAmtCny(xdcz0001DataReqDto.getBhhamt());
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
            cmisLmt0013ReqDealBizListDto.setStartDate(xdcz0001DataReqDto.getBhstda());
            cmisLmt0013ReqDealBizListDto.setEndDate(xdcz0001DataReqDto.getBhedda());
            cmisLmt0013ReqDealBizListDto.setDealBizStatus("200");
            cmisLmt0013ReqDealBizListDtos.add(cmisLmt0013ReqDealBizListDto);
            cmisLmt0013ReqDto.setCmisLmt0013ReqDealBizListDtos(cmisLmt0013ReqDealBizListDtos);


            logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账占用请求报文：" + JSON.toJSONString(cmisLmt0013ReqDto));
            ResultDto<CmisLmt0013RespDto> cmisLmt0013RespDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
            logger.info("根据业务申请编号【" + serno + "】前往额度系统-台账占用返回报文：" + JSON.toJSONString(cmisLmt0013RespDto));

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
    }

    /**
     * 生成保函台账
     *
     * @param xdcz0001DataReqDto commonMap
     * @return
     */
    public void updateAccCvrs(Xdcz0001DataReqDto xdcz0001DataReqDto, HashMap<String, String> commonMap) {
        try {
            AccCvrs accCvrs = accCvrsMapper.selectByBillno(xdcz0001DataReqDto.getBillno());//借据编号
            if(accCvrs == null){
                throw BizException.error(null, EpbEnum.EPB099999.key, "保函台账查询为空");
            }
            // 赋值
            setAccCvrs(accCvrs, xdcz0001DataReqDto, commonMap);
            if (accCvrsMapper.updateByPrimaryKey(accCvrs) == 0) {
                throw BizException.error(null, EpbEnum.EPB090002.key, "更新保函台账失败");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
    }

    /**
     * 赋值保函台账
     *
     * @param accCvrs xdcz0001DataReqDto commonMap
     * @return
     */
    public void setAccCvrs(AccCvrs accCvrs, Xdcz0001DataReqDto xdcz0001DataReqDto, HashMap<String, String> commonMap) {
        try {
            // TODO 受益人信息字段缺失
            accCvrs.setContNo(xdcz0001DataReqDto.getBhhtno());//合同编号
            accCvrs.setCusId(xdcz0001DataReqDto.getCustid());//客户编号
            accCvrs.setCusName(xdcz0001DataReqDto.getCustna());//客户名称
            accCvrs.setPrdId(xdcz0001DataReqDto.getProdno());//产品编号
            accCvrs.setPrdName(commonMap.get("prdName"));//产品名称
            accCvrs.setGuarMode(commonMap.get("guarMode"));//担保方式
            accCvrs.setGuarantType(xdcz0001DataReqDto.getBhtype());//保函类型
            accCvrs.setGuarantMode(xdcz0001DataReqDto.getBhtype());//保函种类
            accCvrs.setCurType(xdcz0001DataReqDto.getCurren());//币种
            accCvrs.setExchangeRate(new BigDecimal(commonMap.get("exchangeRate")));//汇率
            accCvrs.setGuarantAmt(xdcz0001DataReqDto.getBhhamt());//保函金额
            accCvrs.setExchangeRmbAmt((new BigDecimal(commonMap.get("exchangeRate"))).multiply(xdcz0001DataReqDto.getBhhamt()));//折合人民币金额
            accCvrs.setSpacBal(BigDecimal.ZERO);//敞口余额
            accCvrs.setOrigiOpenAmt(BigDecimal.ZERO);//原始敞口金额
            accCvrs.setExchangeRmbSpac(BigDecimal.ZERO);//折合人民币敞口
            accCvrs.setBailPerc(new BigDecimal(commonMap.get("bailPerc")));//保证金比例
            accCvrs.setBailCurType(commonMap.get("bailCurType"));//保证金币种
            accCvrs.setBailAmt(new BigDecimal(commonMap.get("bailAmt")));//保证金金额
            accCvrs.setBailExchangeRate(new BigDecimal(commonMap.get("bailExchangeRate")));//保证金汇率
            accCvrs.setBailCvtCnyAmt(new BigDecimal(commonMap.get("bailCvtCnyAmt")));//保证金折算人民币金额
            accCvrs.setBailCalType(null);//保证金计算方式
            accCvrs.setChrgRate(xdcz0001DataReqDto.getSxflll());//手续费率
            accCvrs.setChrgAmt(xdcz0001DataReqDto.getSxfamt());//手续费金额
            accCvrs.setInureDate(xdcz0001DataReqDto.getBhstda());//生效日期
            accCvrs.setInvlDate(xdcz0001DataReqDto.getBhedda());//失效日期
            accCvrs.setOverdueRate(null);//逾期垫款年利率
            accCvrs.setguarantPayMode(xdcz0001DataReqDto.getBhfkfs());//保函付款方式
            accCvrs.setBeneficiarName(xdcz0001DataReqDto.getSyrnam());//受益人名称
            accCvrs.setSettlAccno(xdcz0001DataReqDto.getJsacct());//结算账号
            accCvrs.setSettlAcctName(xdcz0001DataReqDto.getJsacna());//结算账户名称
            accCvrs.setIsUtilLmt(commonMap.get("isUtilLmt"));//是否使用授信额度
            accCvrs.setLmtAccNo(commonMap.get("lmtAccNo"));//授信额度编号
            accCvrs.setReplyNo(null);//批复编号
            accCvrs.setFinaBrId(xdcz0001DataReqDto.getZwogid());//账务机构编号
            accCvrs.setFinaBrIdName(null);//账务机构名称
            accCvrs.setFiveClass("10");//五级分类
            accCvrs.setTenClass("11");//十级分类
            accCvrs.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));//分类日期
            accCvrs.setAccStatus("1");//台账状态 正常 STD_ACC_STATUS
            accCvrs.setOprType(CmisCommonConstants.OP_TYPE_01);//操作类型
            accCvrs.setUpdId(xdcz0001DataReqDto.getInptid());//最近修改人
            accCvrs.setUpdBrId(xdcz0001DataReqDto.getInbrid());//最近修改机构
            accCvrs.setUpdDate(xdcz0001DataReqDto.getIndate());//最近修改日期
            accCvrs.setUpdateTime(new Date());//修改时间
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
    }


    /**
     * 初始化返回数据 默认为空字符串或为0
     *
     * @param xdcz0001DataRespDto
     * @return
     */
    public Xdcz0001DataRespDto initxdcz0001DataRespDto(Xdcz0001DataRespDto xdcz0001DataRespDto) {
        xdcz0001DataRespDto.setBillno(StringUtils.EMPTY);// 借据号
        xdcz0001DataRespDto.setContno(StringUtils.EMPTY);// 保函协议编号
        xdcz0001DataRespDto.setCncono(StringUtils.EMPTY);// 中文合同编号
        xdcz0001DataRespDto.setCustid(StringUtils.EMPTY);// 客户代码
        xdcz0001DataRespDto.setCustna(StringUtils.EMPTY);// 客户名称
        xdcz0001DataRespDto.setProdno(StringUtils.EMPTY);// 产品编号
        xdcz0001DataRespDto.setBhtype(StringUtils.EMPTY);// 保函类型
        xdcz0001DataRespDto.setAsmean(StringUtils.EMPTY);// 担保方式
        xdcz0001DataRespDto.setCurren(StringUtils.EMPTY);// 币种
        xdcz0001DataRespDto.setBhhamt(new BigDecimal(0L));// 保函金额
        xdcz0001DataRespDto.setBhyeee(new BigDecimal(0L));// 保函余额
        xdcz0001DataRespDto.setBhstda(StringUtils.EMPTY);// 生效日期
        xdcz0001DataRespDto.setBhedda(StringUtils.EMPTY);// 到期日期
        xdcz0001DataRespDto.setJsacct(StringUtils.EMPTY);// 结算账号
        xdcz0001DataRespDto.setJsacna(StringUtils.EMPTY);// 结算账号户名
        xdcz0001DataRespDto.setFxckje(new BigDecimal(0L));// 风险敞口金额
        xdcz0001DataRespDto.setFxckbl(new BigDecimal(0L));// 风险敞口比例
        xdcz0001DataRespDto.setSeacct(StringUtils.EMPTY);// 保证金账号
        xdcz0001DataRespDto.setSecper(new BigDecimal(0L));// 保证金比例
        xdcz0001DataRespDto.setSecamt(new BigDecimal(0L));// 保证金金额
        xdcz0001DataRespDto.setBzjjxx(StringUtils.EMPTY);// 保证金计息方式
        xdcz0001DataRespDto.setSeacna(StringUtils.EMPTY);// 保证金账号户名
        xdcz0001DataRespDto.setSxfamt(new BigDecimal(0L));// 手续费金额
        xdcz0001DataRespDto.setSxflll(new BigDecimal(0L));// 手续费率
        xdcz0001DataRespDto.setXmname(StringUtils.EMPTY);// 项目名称
        xdcz0001DataRespDto.setXmmamt(new BigDecimal(0L));// 项目金额
        xdcz0001DataRespDto.setCoreno(StringUtils.EMPTY);// 合同协议或标书编号
        xdcz0001DataRespDto.setXghtje(new BigDecimal(0L));// 相关贸易合同金额
        xdcz0001DataRespDto.setSyrnam(StringUtils.EMPTY);// 受益人名称
        xdcz0001DataRespDto.setSyradd(StringUtils.EMPTY);// 受益人地址
        xdcz0001DataRespDto.setSyrkhh(StringUtils.EMPTY);// 受益人开户行
        xdcz0001DataRespDto.setSyracc(StringUtils.EMPTY);// 受益人账号
        xdcz0001DataRespDto.setBhfkfs(StringUtils.EMPTY);// 保函付款方式
        xdcz0001DataRespDto.setCftjsm(StringUtils.EMPTY);// 承付条件说明
        xdcz0001DataRespDto.setBhtzzt(StringUtils.EMPTY);// 台帐状态
        xdcz0001DataRespDto.setZwogid(StringUtils.EMPTY);// 账务机构
        xdcz0001DataRespDto.setInptid(StringUtils.EMPTY);// 登记人
        xdcz0001DataRespDto.setMangid(StringUtils.EMPTY);// 责任人
        xdcz0001DataRespDto.setInbrid(StringUtils.EMPTY);// 登记人机构
        xdcz0001DataRespDto.setMabrid(StringUtils.EMPTY);// 责任人机构
        xdcz0001DataRespDto.setIndate(StringUtils.EMPTY);// 登记日期
        return xdcz0001DataRespDto;
    }

    /**
     * 返回报文
     *
     * @param xdcz0001DataRespDto xdcz0001DataReqDto commonMap
     * @return
     */
    public Xdcz0001DataRespDto resultxdcz0001DataRespDto(Xdcz0001DataRespDto xdcz0001DataRespDto, Xdcz0001DataReqDto xdcz0001DataReqDto, HashMap<String, String> commonMap) {
        xdcz0001DataRespDto.setBillno(xdcz0001DataReqDto.getBillno());// 借据号
        xdcz0001DataRespDto.setContno(xdcz0001DataReqDto.getBhhtno());// 保函协议编号
        xdcz0001DataRespDto.setCncono(commonMap.get("cnContNo"));// 中文合同编号
        xdcz0001DataRespDto.setCustid(xdcz0001DataReqDto.getCustid());// 客户代码
        xdcz0001DataRespDto.setCustna(xdcz0001DataReqDto.getCustna());// 客户名称
        xdcz0001DataRespDto.setProdno(xdcz0001DataReqDto.getProdno());// 产品编号
        xdcz0001DataRespDto.setBhtype(xdcz0001DataReqDto.getBhtype());// 保函类型
        xdcz0001DataRespDto.setAsmean(commonMap.get("guarMode"));// 担保方式
        xdcz0001DataRespDto.setCurren(xdcz0001DataReqDto.getCurren());// 币种
        xdcz0001DataRespDto.setBhhamt(xdcz0001DataReqDto.getBhhamt());// 保函金额
        xdcz0001DataRespDto.setBhyeee(xdcz0001DataReqDto.getBhhamt());// 保函余额
        xdcz0001DataRespDto.setBhstda(xdcz0001DataReqDto.getBhstda());// 生效日期
        xdcz0001DataRespDto.setBhedda(xdcz0001DataReqDto.getBhedda());// 到期日期
        xdcz0001DataRespDto.setJsacct(xdcz0001DataReqDto.getJsacct());// 结算账号
        xdcz0001DataRespDto.setJsacna(xdcz0001DataReqDto.getJsacct());// 结算账号户名
        xdcz0001DataRespDto.setFxckje(new BigDecimal(0L));// 风险敞口金额
        xdcz0001DataRespDto.setFxckbl(new BigDecimal(0L));// 风险敞口比例
        xdcz0001DataRespDto.setSeacct(null);// 保证金账号
        xdcz0001DataRespDto.setSecper(new BigDecimal(commonMap.get("bailPerc")));// 保证金比例
        xdcz0001DataRespDto.setSecamt(new BigDecimal(commonMap.get("bailExchangeRate")));// 保证金金额
        xdcz0001DataRespDto.setBzjjxx(null);// 保证金计息方式
        xdcz0001DataRespDto.setSeacna(null);// 保证金账号户名
        xdcz0001DataRespDto.setSxfamt(xdcz0001DataReqDto.getSxfamt());// 手续费金额
        xdcz0001DataRespDto.setSxflll(xdcz0001DataReqDto.getSxflll());// 手续费率
        xdcz0001DataRespDto.setXmname(xdcz0001DataReqDto.getXmname());// 项目名称
        xdcz0001DataRespDto.setXmmamt(xdcz0001DataReqDto.getXmmamt());// 项目金额
        xdcz0001DataRespDto.setCoreno(xdcz0001DataReqDto.getCoreno());// 合同协议或标书编号
        xdcz0001DataRespDto.setXghtje(xdcz0001DataReqDto.getXghtje());// 相关贸易合同金额
        xdcz0001DataRespDto.setSyrnam(xdcz0001DataReqDto.getSyrnam());// 受益人名称
        xdcz0001DataRespDto.setSyradd(xdcz0001DataReqDto.getSyradd());// 受益人地址
        xdcz0001DataRespDto.setSyrkhh(xdcz0001DataReqDto.getSyrkhh());// 受益人开户行
        xdcz0001DataRespDto.setSyracc(xdcz0001DataReqDto.getSyracc());// 受益人账号
        xdcz0001DataRespDto.setBhfkfs(xdcz0001DataReqDto.getBhfkfs());// 保函付款方式
        xdcz0001DataRespDto.setCftjsm(xdcz0001DataReqDto.getCftjsm());// 承付条件说明
        xdcz0001DataRespDto.setBhtzzt("1");// 台帐状态
        xdcz0001DataRespDto.setZwogid(xdcz0001DataReqDto.getZwogid());// 账务机构
        xdcz0001DataRespDto.setInptid(xdcz0001DataReqDto.getInptid());// 登记人
        xdcz0001DataRespDto.setMangid(xdcz0001DataReqDto.getMangid());// 责任人
        xdcz0001DataRespDto.setInbrid(xdcz0001DataReqDto.getInbrid());// 登记人机构
        xdcz0001DataRespDto.setMabrid(xdcz0001DataReqDto.getMabrid());// 责任人机构
        xdcz0001DataRespDto.setIndate(xdcz0001DataReqDto.getIndate());// 登记日期
        return xdcz0001DataRespDto;
    }
}
