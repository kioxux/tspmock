/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppr
 * @类描述: iqp_loan_appr数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-09-24 22:16:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_appr")
public class IqpLoanAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 批复金额 **/
	@Column(name = "REPLY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyAmt;
	
	/** 批复期限 **/
	@Column(name = "REPLY_TERM", unique = false, nullable = true, length = 10)
	private Integer replyTerm;
	
	/** 批复利率 **/
	@Column(name = "REPLY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyRate;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 用信条件 **/
	@Column(name = "LOAN_COND", unique = false, nullable = true, length = 2000)
	private String loanCond;
	
	/** 风控建议 **/
	@Column(name = "RISK_ADVICE", unique = false, nullable = true, length = 2000)
	private String riskAdvice;
	
	/** 是否上调审批权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = true, length = 5)
	private String isUpperApprAuth;
	
	/** 审批结论 **/
	@Column(name = "APPROVE_CONCLUSION", unique = false, nullable = true, length = 100)
	private String approveConclusion;
	
	/** 审批意见 **/
	@Column(name = "APPROVE_ADVICE", unique = false, nullable = true, length = 2000)
	private String approveAdvice;
	
	/** 审核岗位 **/
	@Column(name = "APPROVE_POST", unique = false, nullable = true, length = 100)
	private String approvePost;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 打回原因 **/
	@Column(name = "APPR_BACK_REASON_TYPE", unique = false, nullable = true, length = 50)
	private String apprBackReasonType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param replyAmt
	 */
	public void setReplyAmt(java.math.BigDecimal replyAmt) {
		this.replyAmt = replyAmt;
	}
	
    /**
     * @return replyAmt
     */
	public java.math.BigDecimal getReplyAmt() {
		return this.replyAmt;
	}
	
	/**
	 * @param replyTerm
	 */
	public void setReplyTerm(Integer replyTerm) {
		this.replyTerm = replyTerm;
	}
	
    /**
     * @return replyTerm
     */
	public Integer getReplyTerm() {
		return this.replyTerm;
	}
	
	/**
	 * @param replyRate
	 */
	public void setReplyRate(java.math.BigDecimal replyRate) {
		this.replyRate = replyRate;
	}
	
    /**
     * @return replyRate
     */
	public java.math.BigDecimal getReplyRate() {
		return this.replyRate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param loanCond
	 */
	public void setLoanCond(String loanCond) {
		this.loanCond = loanCond;
	}
	
    /**
     * @return loanCond
     */
	public String getLoanCond() {
		return this.loanCond;
	}
	
	/**
	 * @param riskAdvice
	 */
	public void setRiskAdvice(String riskAdvice) {
		this.riskAdvice = riskAdvice;
	}
	
    /**
     * @return riskAdvice
     */
	public String getRiskAdvice() {
		return this.riskAdvice;
	}
	
	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) {
		this.isUpperApprAuth = isUpperApprAuth;
	}
	
    /**
     * @return isUpperApprAuth
     */
	public String getIsUpperApprAuth() {
		return this.isUpperApprAuth;
	}
	
	/**
	 * @param approveConclusion
	 */
	public void setApproveConclusion(String approveConclusion) {
		this.approveConclusion = approveConclusion;
	}
	
    /**
     * @return approveConclusion
     */
	public String getApproveConclusion() {
		return this.approveConclusion;
	}
	
	/**
	 * @param approveAdvice
	 */
	public void setApproveAdvice(String approveAdvice) {
		this.approveAdvice = approveAdvice;
	}
	
    /**
     * @return approveAdvice
     */
	public String getApproveAdvice() {
		return this.approveAdvice;
	}
	
	/**
	 * @param approvePost
	 */
	public void setApprovePost(String approvePost) {
		this.approvePost = approvePost;
	}
	
    /**
     * @return approvePost
     */
	public String getApprovePost() {
		return this.approvePost;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param apprBackReasonType
	 */
	public void setApprBackReasonType(String apprBackReasonType) {
		this.apprBackReasonType = apprBackReasonType;
	}
	
    /**
     * @return apprBackReasonType
     */
	public String getApprBackReasonType() {
		return this.apprBackReasonType;
	}


}