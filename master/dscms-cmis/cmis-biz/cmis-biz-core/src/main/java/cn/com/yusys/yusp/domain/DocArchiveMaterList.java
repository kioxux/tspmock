/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveMaterList
 * @类描述: doc_archive_mater_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-20 10:54:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_archive_mater_list")
public class DocArchiveMaterList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 清单流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "ADML_SERNO")
	private String admlSerno;
	
	/** 档案流水号 **/
	@Id
	@Column(name = "DOC_SERNO")
	private String docSerno;
	
	/** 材料类型 **/
	@Column(name = "DOC_TYPE_DATA", unique = false, nullable = true, length = 200)
	private String docTypeData;
	
	/** 材料清单 **/
	@Column(name = "DOC_LIST_DATA", unique = false, nullable = true, length = 200)
	private String docListData;
	
	/** 资料页数 **/
	@Column(name = "PAGES_NUM", unique = false, nullable = true, length = 10)
	private String pagesNum;
	
	/** 是否必须原件 **/
	@Column(name = "IS_SOURCE_FLAG", unique = false, nullable = true, length = 5)
	private String isSourceFlag;
	
	/** 备注 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 1000)
	private String memo;
	
	/** 排序 **/
	@Column(name = "SORT", unique = false, nullable = true, length = 5)
	private String sort;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 一级影像目录编号 **/
	@Column(name = "top_outsystem_code", unique = false, nullable = true, length = 50)
	private String topOutsystemCode;
	
	/** 二级影像目录编号 **/
	@Column(name = "sec_outsystem_code", unique = false, nullable = true, length = 50)
	private String secOutsystemCode;
	
	/** 文件影像目录编号 **/
	@Column(name = "file_outsystem_code", unique = false, nullable = true, length = 50)
	private String fileOutsystemCode;

	/** 相同材料类型数量 **/
	@Column(name = "DOC_TYPE_DATA_Count", unique = false, nullable = true, length = 200)
	private Integer docTypeDataCount;

	/**
	 * @return Integer
	 */
	public Integer getDocTypeDataCount() {
		return docTypeDataCount;
	}

	/**
	 * @param docTypeDataCount
	 */
	public void setDocTypeDataCount(Integer docTypeDataCount) {
		this.docTypeDataCount = docTypeDataCount;
	}

	/**
	 * @param admlSerno
	 */
	public void setAdmlSerno(String admlSerno) {
		this.admlSerno = admlSerno;
	}
	
    /**
     * @return admlSerno
     */
	public String getAdmlSerno() {
		return this.admlSerno;
	}
	
	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno;
	}
	
    /**
     * @return docSerno
     */
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docTypeData
	 */
	public void setDocTypeData(String docTypeData) {
		this.docTypeData = docTypeData;
	}
	
    /**
     * @return docTypeData
     */
	public String getDocTypeData() {
		return this.docTypeData;
	}
	
	/**
	 * @param docListData
	 */
	public void setDocListData(String docListData) {
		this.docListData = docListData;
	}
	
    /**
     * @return docListData
     */
	public String getDocListData() {
		return this.docListData;
	}
	
	/**
	 * @param pagesNum
	 */
	public void setPagesNum(String pagesNum) {
		this.pagesNum = pagesNum;
	}
	
    /**
     * @return pagesNum
     */
	public String getPagesNum() {
		return this.pagesNum;
	}
	
	/**
	 * @param isSourceFlag
	 */
	public void setIsSourceFlag(String isSourceFlag) {
		this.isSourceFlag = isSourceFlag;
	}
	
    /**
     * @return isSourceFlag
     */
	public String getIsSourceFlag() {
		return this.isSourceFlag;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param sort
	 */
	public void setSort(String sort) {
		this.sort = sort;
	}
	
    /**
     * @return sort
     */
	public String getSort() {
		return this.sort;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param topOutsystemCode
	 */
	public void setTopOutsystemCode(String topOutsystemCode) {
		this.topOutsystemCode = topOutsystemCode;
	}
	
    /**
     * @return topOutsystemCode
     */
	public String getTopOutsystemCode() {
		return this.topOutsystemCode;
	}
	
	/**
	 * @param secOutsystemCode
	 */
	public void setSecOutsystemCode(String secOutsystemCode) {
		this.secOutsystemCode = secOutsystemCode;
	}
	
    /**
     * @return secOutsystemCode
     */
	public String getSecOutsystemCode() {
		return this.secOutsystemCode;
	}
	
	/**
	 * @param fileOutsystemCode
	 */
	public void setFileOutsystemCode(String fileOutsystemCode) {
		this.fileOutsystemCode = fileOutsystemCode;
	}
	
    /**
     * @return fileOutsystemCode
     */
	public String getFileOutsystemCode() {
		return this.fileOutsystemCode;
	}


}