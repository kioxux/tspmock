/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPld;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysGuarPldMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPldService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 22:52:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysGuarPldService {

    @Autowired
    private RptLmtRepayAnysGuarPldMapper rptLmtRepayAnysGuarPldMapper;
    private static final Logger log = LoggerFactory.getLogger(RptLmtRepayAnysGuarPldService.class);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptLmtRepayAnysGuarPld selectByPrimaryKey(String pkId, String serno) {
        return rptLmtRepayAnysGuarPldMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptLmtRepayAnysGuarPld> selectAll(QueryModel model) {
        List<RptLmtRepayAnysGuarPld> records = (List<RptLmtRepayAnysGuarPld>) rptLmtRepayAnysGuarPldMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptLmtRepayAnysGuarPld> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnysGuarPld> list = rptLmtRepayAnysGuarPldMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnysGuarPld record) {
        return rptLmtRepayAnysGuarPldMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnysGuarPld record) {
        return rptLmtRepayAnysGuarPldMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnysGuarPld record) {
        return rptLmtRepayAnysGuarPldMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnysGuarPld record) {
        return rptLmtRepayAnysGuarPldMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarPldMapper.deleteByPrimaryKey(pkId);
    }
    /**
     * 保存信息
     * @param rptLmtRepayAnysGuarPld
     * @return
     */
    public int savePld(RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld) {
        RptLmtRepayAnysGuarPld rptLmtRepayAnysGuarPld1 = rptLmtRepayAnysGuarPldMapper.selectBySerno(rptLmtRepayAnysGuarPld.getSerno());
        int count = 0;
        if(Objects.isNull(rptLmtRepayAnysGuarPld1)){
            rptLmtRepayAnysGuarPld.setPkId(StringUtils.getUUID());
            count = rptLmtRepayAnysGuarPldMapper.insert(rptLmtRepayAnysGuarPld);
        }else{
            count = rptLmtRepayAnysGuarPldMapper.updateByPrimaryKeySelective(rptLmtRepayAnysGuarPld);
        }

        return count;
    }

    /**
     * @方法名称: riskItem0040
     * @方法描述: 第二还款来源校验
     * @参数与返回说明:
     * @算法描述:发起预授信细化,不补全第二还款来源，则拦截
     * @创建人: yfs
     * @创建时间: 2021-07-13 10:47:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0040(String serno) {
        log.info("第二还款来源校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        QueryModel model = new QueryModel();
        model.addCondition("serno",serno);
        // 通过流水号查询调查报告授信可行性及还款能力分析担保为抵质押物
        List<RptLmtRepayAnysGuarPld> list = selectAll(model);
        if(CollectionUtils.isEmpty(list)) {
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_04001);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            return riskResultDto;
        }
        log.info("第二还款来源校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    public RptLmtRepayAnysGuarPld selectBySerno(String serno){
        return rptLmtRepayAnysGuarPldMapper.selectBySerno(serno);
    }

}
