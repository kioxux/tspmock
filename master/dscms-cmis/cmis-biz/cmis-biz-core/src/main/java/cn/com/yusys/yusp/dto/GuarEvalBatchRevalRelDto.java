package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-eval-core模块
 * @类名称: GuarEvalBatchRevalRel
 * @类描述: guar_eval_batch_reval_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-30 21:20:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalBatchRevalRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pk;
	
	/** 批量申请流水号 **/
	private String batchSerno;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	
	/**
	 * @param pk
	 */
	public void setPk(String pk) {
		this.pk = pk == null ? null : pk.trim();
	}
	
    /**
     * @return Pk
     */	
	public String getPk() {
		return this.pk;
	}
	
	/**
	 * @param batchSerno
	 */
	public void setBatchSerno(String batchSerno) {
		this.batchSerno = batchSerno == null ? null : batchSerno.trim();
	}
	
    /**
     * @return BatchSerno
     */	
	public String getBatchSerno() {
		return this.batchSerno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}


}