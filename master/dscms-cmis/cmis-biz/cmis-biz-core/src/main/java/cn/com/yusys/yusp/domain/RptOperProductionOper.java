/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperProductionOper
 * @类描述: rpt_oper_production_oper数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:03:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_production_oper")
public class RptOperProductionOper extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 特许经营机制 **/
	@Column(name = "FRANCHISE_MECHANISM", unique = false, nullable = true, length = 80)
	private String franchiseMechanism;
	
	/** 主营产品 **/
	@Column(name = "MAIN_PRODUCT", unique = false, nullable = true, length = 80)
	private String mainProduct;
	
	/** 经营模式 **/
	@Column(name = "OPER_MODE", unique = false, nullable = true, length = 5)
	private String operMode;
	
	/** 经营情况介绍 **/
	@Column(name = "BUSI_CASE", unique = false, nullable = true, length = 65535)
	private String busiCase;
	
	/** 主营业务一 **/
	@Column(name = "MAIN_BUSI1", unique = false, nullable = true, length = 80)
	private String mainBusi1;
	
	/** 主营业务一占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI1_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi1Memo;
	
	/** 主营业务二 **/
	@Column(name = "MAIN_BUSI2", unique = false, nullable = true, length = 80)
	private String mainBusi2;
	
	/** 主营业务二占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI2_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi2Memo;
	
	/** 主营业务三 **/
	@Column(name = "MAIN_BUSI3", unique = false, nullable = true, length = 80)
	private String mainBusi3;
	
	/** 主营业务三占比或相关文字说明 **/
	@Column(name = "MAIN_BUSI3_MEMO", unique = false, nullable = true, length = 65535)
	private String mainBusi3Memo;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 生产模式 **/
	@Column(name = "PRODUCT_MODE", unique = false, nullable = true, length = 5)
	private String productMode;
	
	/** 主要工艺流程 **/
	@Column(name = "MAIN_PROCESS_FLOW", unique = false, nullable = true, length = 65535)
	private String mainProcessFlow;
	
	/** 生产周期 **/
	@Column(name = "PRODUCT_CYCLE", unique = false, nullable = true, length = 20)
	private String productCycle;
	
	/** 技术含量 **/
	@Column(name = "TECHNICAL_CONTENT", unique = false, nullable = true, length = 20)
	private String technicalContent;
	
	/** 设备装备水平和开台率 **/
	@Column(name = "EQUIPMENT_LEVEL_OPENING_RATE", unique = false, nullable = true, length = 40)
	private String equipmentLevelOpeningRate;
	
	/** 下游客户分析 **/
	@Column(name = "DOWNSTREAM_CUS_ANALY", unique = false, nullable = true, length = 65535)
	private String downstreamCusAnaly;
	
	/** 目前订单情况 **/
	@Column(name = "CURR_ORDER_STATUS", unique = false, nullable = true, length = 65535)
	private String currOrderStatus;
	
	/** 经营业务概况 **/
	@Column(name = "OPEN_BUSI_SURVEY", unique = false, nullable = true, length = 65535)
	private String openBusiSurvey;
	
	/** 经营模式及盈利模式 **/
	@Column(name = "OPER_PROFIT_MODE", unique = false, nullable = true, length = 5)
	private String operProfitMode;
	
	/** 销售其他需说明事项 **/
	@Column(name = "SALE_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String saleOtherNeedDesc;
	
	/** 销售主要客户群 **/
	@Column(name = "SEAL_MAIN_CUSTOMER", unique = false, nullable = true, length = 40)
	private String sealMainCustomer;
	
	/** 销售一般回款方式 **/
	@Column(name = "SEAL_PAYMENT_COLL_TYPE", unique = false, nullable = true, length = 5)
	private String sealPaymentCollType;
	
	/** 销售目前订单情况 **/
	@Column(name = "SEAL_CURR_ORDER_STATUS", unique = false, nullable = true, length = 65535)
	private String sealCurrOrderStatus;
	
	/** 上游供应商分析 **/
	@Column(name = "UPPERSTREAM_CUS_ANALY", unique = false, nullable = true, length = 65535)
	private String upperstreamCusAnaly;
	
	/** 采购其他需说明事项 **/
	@Column(name = "BUY_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String buyOtherNeedDesc;
	
	/** 采购主要原材料及外购配套件 **/
	@Column(name = "BUY_RAW_MATERIAL", unique = false, nullable = true, length = 65535)
	private String buyRawMaterial;
	
	/** 采购主要供应商 **/
	@Column(name = "BUY_MAIN_SUPPLIER", unique = false, nullable = true, length = 40)
	private String buyMainSupplier;
	
	/** 采购一般付款方式 **/
	@Column(name = "BUY_PAYMENT_TYPE", unique = false, nullable = true, length = 5)
	private String buyPaymentType;
	
	/** 种植养殖情况其他说明 **/
	@Column(name = "BREED_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String breedOtherNeedDesc;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param franchiseMechanism
	 */
	public void setFranchiseMechanism(String franchiseMechanism) {
		this.franchiseMechanism = franchiseMechanism;
	}
	
    /**
     * @return franchiseMechanism
     */
	public String getFranchiseMechanism() {
		return this.franchiseMechanism;
	}
	
	/**
	 * @param mainProduct
	 */
	public void setMainProduct(String mainProduct) {
		this.mainProduct = mainProduct;
	}
	
    /**
     * @return mainProduct
     */
	public String getMainProduct() {
		return this.mainProduct;
	}
	
	/**
	 * @param operMode
	 */
	public void setOperMode(String operMode) {
		this.operMode = operMode;
	}
	
    /**
     * @return operMode
     */
	public String getOperMode() {
		return this.operMode;
	}
	
	/**
	 * @param busiCase
	 */
	public void setBusiCase(String busiCase) {
		this.busiCase = busiCase;
	}
	
    /**
     * @return busiCase
     */
	public String getBusiCase() {
		return this.busiCase;
	}
	
	/**
	 * @param mainBusi1
	 */
	public void setMainBusi1(String mainBusi1) {
		this.mainBusi1 = mainBusi1;
	}
	
    /**
     * @return mainBusi1
     */
	public String getMainBusi1() {
		return this.mainBusi1;
	}
	
	/**
	 * @param mainBusi1Memo
	 */
	public void setMainBusi1Memo(String mainBusi1Memo) {
		this.mainBusi1Memo = mainBusi1Memo;
	}
	
    /**
     * @return mainBusi1Memo
     */
	public String getMainBusi1Memo() {
		return this.mainBusi1Memo;
	}
	
	/**
	 * @param mainBusi2
	 */
	public void setMainBusi2(String mainBusi2) {
		this.mainBusi2 = mainBusi2;
	}
	
    /**
     * @return mainBusi2
     */
	public String getMainBusi2() {
		return this.mainBusi2;
	}
	
	/**
	 * @param mainBusi2Memo
	 */
	public void setMainBusi2Memo(String mainBusi2Memo) {
		this.mainBusi2Memo = mainBusi2Memo;
	}
	
    /**
     * @return mainBusi2Memo
     */
	public String getMainBusi2Memo() {
		return this.mainBusi2Memo;
	}
	
	/**
	 * @param mainBusi3
	 */
	public void setMainBusi3(String mainBusi3) {
		this.mainBusi3 = mainBusi3;
	}
	
    /**
     * @return mainBusi3
     */
	public String getMainBusi3() {
		return this.mainBusi3;
	}
	
	/**
	 * @param mainBusi3Memo
	 */
	public void setMainBusi3Memo(String mainBusi3Memo) {
		this.mainBusi3Memo = mainBusi3Memo;
	}
	
    /**
     * @return mainBusi3Memo
     */
	public String getMainBusi3Memo() {
		return this.mainBusi3Memo;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param productMode
	 */
	public void setProductMode(String productMode) {
		this.productMode = productMode;
	}
	
    /**
     * @return productMode
     */
	public String getProductMode() {
		return this.productMode;
	}
	
	/**
	 * @param mainProcessFlow
	 */
	public void setMainProcessFlow(String mainProcessFlow) {
		this.mainProcessFlow = mainProcessFlow;
	}
	
    /**
     * @return mainProcessFlow
     */
	public String getMainProcessFlow() {
		return this.mainProcessFlow;
	}
	
	/**
	 * @param productCycle
	 */
	public void setProductCycle(String productCycle) {
		this.productCycle = productCycle;
	}
	
    /**
     * @return productCycle
     */
	public String getProductCycle() {
		return this.productCycle;
	}
	
	/**
	 * @param technicalContent
	 */
	public void setTechnicalContent(String technicalContent) {
		this.technicalContent = technicalContent;
	}
	
    /**
     * @return technicalContent
     */
	public String getTechnicalContent() {
		return this.technicalContent;
	}
	
	/**
	 * @param equipmentLevelOpeningRate
	 */
	public void setEquipmentLevelOpeningRate(String equipmentLevelOpeningRate) {
		this.equipmentLevelOpeningRate = equipmentLevelOpeningRate;
	}
	
    /**
     * @return equipmentLevelOpeningRate
     */
	public String getEquipmentLevelOpeningRate() {
		return this.equipmentLevelOpeningRate;
	}
	
	/**
	 * @param downstreamCusAnaly
	 */
	public void setDownstreamCusAnaly(String downstreamCusAnaly) {
		this.downstreamCusAnaly = downstreamCusAnaly;
	}
	
    /**
     * @return downstreamCusAnaly
     */
	public String getDownstreamCusAnaly() {
		return this.downstreamCusAnaly;
	}
	
	/**
	 * @param currOrderStatus
	 */
	public void setCurrOrderStatus(String currOrderStatus) {
		this.currOrderStatus = currOrderStatus;
	}
	
    /**
     * @return currOrderStatus
     */
	public String getCurrOrderStatus() {
		return this.currOrderStatus;
	}
	
	/**
	 * @param openBusiSurvey
	 */
	public void setOpenBusiSurvey(String openBusiSurvey) {
		this.openBusiSurvey = openBusiSurvey;
	}
	
    /**
     * @return openBusiSurvey
     */
	public String getOpenBusiSurvey() {
		return this.openBusiSurvey;
	}
	
	/**
	 * @param operProfitMode
	 */
	public void setOperProfitMode(String operProfitMode) {
		this.operProfitMode = operProfitMode;
	}
	
    /**
     * @return operProfitMode
     */
	public String getOperProfitMode() {
		return this.operProfitMode;
	}
	
	/**
	 * @param saleOtherNeedDesc
	 */
	public void setSaleOtherNeedDesc(String saleOtherNeedDesc) {
		this.saleOtherNeedDesc = saleOtherNeedDesc;
	}
	
    /**
     * @return saleOtherNeedDesc
     */
	public String getSaleOtherNeedDesc() {
		return this.saleOtherNeedDesc;
	}
	
	/**
	 * @param sealMainCustomer
	 */
	public void setSealMainCustomer(String sealMainCustomer) {
		this.sealMainCustomer = sealMainCustomer;
	}
	
    /**
     * @return sealMainCustomer
     */
	public String getSealMainCustomer() {
		return this.sealMainCustomer;
	}
	
	/**
	 * @param sealPaymentCollType
	 */
	public void setSealPaymentCollType(String sealPaymentCollType) {
		this.sealPaymentCollType = sealPaymentCollType;
	}
	
    /**
     * @return sealPaymentCollType
     */
	public String getSealPaymentCollType() {
		return this.sealPaymentCollType;
	}
	
	/**
	 * @param sealCurrOrderStatus
	 */
	public void setSealCurrOrderStatus(String sealCurrOrderStatus) {
		this.sealCurrOrderStatus = sealCurrOrderStatus;
	}
	
    /**
     * @return sealCurrOrderStatus
     */
	public String getSealCurrOrderStatus() {
		return this.sealCurrOrderStatus;
	}
	
	/**
	 * @param upperstreamCusAnaly
	 */
	public void setUpperstreamCusAnaly(String upperstreamCusAnaly) {
		this.upperstreamCusAnaly = upperstreamCusAnaly;
	}
	
    /**
     * @return upperstreamCusAnaly
     */
	public String getUpperstreamCusAnaly() {
		return this.upperstreamCusAnaly;
	}
	
	/**
	 * @param buyOtherNeedDesc
	 */
	public void setBuyOtherNeedDesc(String buyOtherNeedDesc) {
		this.buyOtherNeedDesc = buyOtherNeedDesc;
	}
	
    /**
     * @return buyOtherNeedDesc
     */
	public String getBuyOtherNeedDesc() {
		return this.buyOtherNeedDesc;
	}
	
	/**
	 * @param buyRawMaterial
	 */
	public void setBuyRawMaterial(String buyRawMaterial) {
		this.buyRawMaterial = buyRawMaterial;
	}
	
    /**
     * @return buyRawMaterial
     */
	public String getBuyRawMaterial() {
		return this.buyRawMaterial;
	}
	
	/**
	 * @param buyMainSupplier
	 */
	public void setBuyMainSupplier(String buyMainSupplier) {
		this.buyMainSupplier = buyMainSupplier;
	}
	
    /**
     * @return buyMainSupplier
     */
	public String getBuyMainSupplier() {
		return this.buyMainSupplier;
	}
	
	/**
	 * @param buyPaymentType
	 */
	public void setBuyPaymentType(String buyPaymentType) {
		this.buyPaymentType = buyPaymentType;
	}
	
    /**
     * @return buyPaymentType
     */
	public String getBuyPaymentType() {
		return this.buyPaymentType;
	}
	
	/**
	 * @param breedOtherNeedDesc
	 */
	public void setBreedOtherNeedDesc(String breedOtherNeedDesc) {
		this.breedOtherNeedDesc = breedOtherNeedDesc;
	}
	
    /**
     * @return breedOtherNeedDesc
     */
	public String getBreedOtherNeedDesc() {
		return this.breedOtherNeedDesc;
	}


}