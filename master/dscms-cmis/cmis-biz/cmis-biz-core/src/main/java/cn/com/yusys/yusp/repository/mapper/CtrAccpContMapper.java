/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrAccpCont;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.ContList;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAccpContMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-14 15:01:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrAccpContMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CtrAccpCont selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrAccpCont> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CtrAccpCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CtrAccpCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CtrAccpCont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CtrAccpCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据银承协议编号存在生效的合同
     *
     * @return
     */
    int queryNumContByContNo(@Param("contNo") String contNo);

    /**
     * 查询银承合同信息
     *
     * @param xdht0002DataReqDto
     * @return
     */
    Xdht0002DataRespDto queryContInfoByContNoAndCusId(Xdht0002DataReqDto xdht0002DataReqDto);

    /**
     * 根据客户编号查询银承合同信息
     *
     * @param cusId
     */
    List<ContList> getContInfoByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: queryCtrAccpContByDataParams
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrAccpCont queryCtrAccpContByDataParams(HashMap<String, String> queryMap);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrAccpCont> selectByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * 根据合同编号查询
     *
     * @param contNo
     * @return
     */
    CtrAccpCont selectByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:01
     * @修改记录：修改时间 修改人员 修改时间
     */
    CtrAccpCont selectByIqpSerno(@Param("serno") String serno);


    /**
     * @param cusId: 客户号
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * 根据合同号查询担保方式
     *
     * @param contNo
     * @return
     */
    String selectGuarModeByContNo(String contNo);

    /**
     * 根据合同编号、合同状态查询 授信台账编号
     *
     * @param queryLmtAccNoMap
     * @return
     */
    String queryLmtAccNo(Map queryLmtAccNoMap);

    /**
     * 获取台账明细
     *
     * @param cont_no
     * @return
     */
    List<CmisLmt0010ReqDealBizListDto> getDealBizList(@Param("contNo") String cont_no);

    /**
     * 获取合同信息（union 最高额合同）
     *
     * @param contNo
     * @return
     */
    CtrAccpCont getContUnionHighContByContNo(String contNo);

    /**
     * 通过合同号更新LMTACCNO
     *
     * @param record
     * @return
     */
    int updateLmtAccNoByContNo(CtrAccpCont record);

    /**
     * 根据额度编号查询合同金额总和
     *
     * @param lmtAccNo
     * @return
     */
    BigDecimal getSumContAmt(String lmtAccNo);
}