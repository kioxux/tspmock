package cn.com.yusys.yusp.web.server.xdxw0053;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0053.req.Xdxw0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0053.resp.Xdxw0053DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0053.Xdxw0053Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:查询经营性贷款客户基本信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0053:查询经营性贷款客户基本信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0053Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0053Resource.class);

    @Autowired
    private Xdxw0053Service xdxw0053Service;

    /**
     * 交易码：xdxw0053
     * 交易描述：查询经营性贷款客户基本信息
     *
     * @param xdxw0053DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询经营性贷款客户基本信息")
    @PostMapping("/xdxw0053")
    protected @ResponseBody
    ResultDto<Xdxw0053DataRespDto> xdxw0053(@Validated @RequestBody Xdxw0053DataReqDto xdxw0053DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053DataReqDto));
        Xdxw0053DataRespDto xdxw0053DataRespDto = new Xdxw0053DataRespDto();// 响应Dto:查询经营性贷款客户基本信息
        ResultDto<Xdxw0053DataRespDto> xdxw0053DataResultDto = new ResultDto<>();
        // 从xdxw0053DataReqDto获取业务值进行业务逻辑处理
        try {
            String indgtSerno = xdxw0053DataReqDto.getIndgtSerno();//客户调查表编号
            // 调用xdxw0053Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, indgtSerno);
            xdxw0053DataRespDto = Optional.ofNullable(xdxw0053Service.getSurveyReportByIndgtSerno(indgtSerno)).orElseGet(() -> {
                Xdxw0053DataRespDto temp = new Xdxw0053DataRespDto();
                temp.setIndivMarSt(StringUtils.EMPTY);// 婚姻状况
                temp.setIndivEdt(StringUtils.EMPTY);// 学历
                temp.setLiveYear(new BigDecimal(0L));// 居住年限
                temp.setReprName(StringUtils.EMPTY);// 法人代表
                temp.setOperAddr(StringUtils.EMPTY);// 经营地址
                temp.setBsinsLicYear(StringUtils.EMPTY);// 营业执照年限
                temp.setActOperAddr(StringUtils.EMPTY);// 实际经营地址
                temp.setActTrade(StringUtils.EMPTY);// 实际行业
                temp.setSpouseName(StringUtils.EMPTY);// 配偶姓名
                temp.setSpouseCertNo(StringUtils.EMPTY);// 配偶证件号
                temp.setSpousePhone(StringUtils.EMPTY);// 配偶电话
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053DataRespDto));
            // 封装xdxw0053DataResultDto中正确的返回码和返回信息
            xdxw0053DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0053DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, e.getMessage());
            // 封装xdxw0053DataResultDto中异常返回码和返回信息
            xdxw0053DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0053DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0053DataRespDto到xdxw0053DataResultDto中
        xdxw0053DataResultDto.setData(xdxw0053DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053DataResultDto));
        return xdxw0053DataResultDto;
    }
}
