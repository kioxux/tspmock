package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfCargoPledge
 * @类描述: guar_inf_cargo_pledge数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfCargoPledgeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 货物名称 **/
	private String cargoName;
	
	/** 品牌/厂家/产地 **/
	private String vehicleBrand;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 详细地址 **/
	private String detailsAddress;
	
	/** 货物计量单位 STD_ZB_CARGO_MEASURE_UNIT **/
	private String cargoMeasureUnit;
	
	/** 货物数量 **/
	private java.math.BigDecimal cargoAmount;
	
	/** 最新核定单价 **/
	private java.math.BigDecimal latestApprovedPrice;
	
	/** 是否有监管公司 **/
	private String hasSupervision;
	
	/** 监管公司名称 **/
	private String supervisionCompanyName;
	
	/** 监管公司组织机构代码 **/
	private String supervisionOrgCode;
	
	/** 协议生效日 **/
	private String agreementBeginDate;
	
	/** 协议到期日 **/
	private String agreementEndDate;
	
	/** 供应链质押价值 **/
	private java.math.BigDecimal gylVal;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 货物其他计量单位文本输入 **/
	private String carMeasureUnitOther;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 金额 **/
	private java.math.BigDecimal amt;
	
	/** 保管人 **/
	private String keepId;
	
	/** 货物详细类型 **/
	private String coodsDetailType;
	
	/** 货物型号 **/
	private String goodsModel;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param cargoName
	 */
	public void setCargoName(String cargoName) {
		this.cargoName = cargoName == null ? null : cargoName.trim();
	}
	
    /**
     * @return CargoName
     */	
	public String getCargoName() {
		return this.cargoName;
	}
	
	/**
	 * @param vehicleBrand
	 */
	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand == null ? null : vehicleBrand.trim();
	}
	
    /**
     * @return VehicleBrand
     */	
	public String getVehicleBrand() {
		return this.vehicleBrand;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param detailsAddress
	 */
	public void setDetailsAddress(String detailsAddress) {
		this.detailsAddress = detailsAddress == null ? null : detailsAddress.trim();
	}
	
    /**
     * @return DetailsAddress
     */	
	public String getDetailsAddress() {
		return this.detailsAddress;
	}
	
	/**
	 * @param cargoMeasureUnit
	 */
	public void setCargoMeasureUnit(String cargoMeasureUnit) {
		this.cargoMeasureUnit = cargoMeasureUnit == null ? null : cargoMeasureUnit.trim();
	}
	
    /**
     * @return CargoMeasureUnit
     */	
	public String getCargoMeasureUnit() {
		return this.cargoMeasureUnit;
	}
	
	/**
	 * @param cargoAmount
	 */
	public void setCargoAmount(java.math.BigDecimal cargoAmount) {
		this.cargoAmount = cargoAmount;
	}
	
    /**
     * @return CargoAmount
     */	
	public java.math.BigDecimal getCargoAmount() {
		return this.cargoAmount;
	}
	
	/**
	 * @param latestApprovedPrice
	 */
	public void setLatestApprovedPrice(java.math.BigDecimal latestApprovedPrice) {
		this.latestApprovedPrice = latestApprovedPrice;
	}
	
    /**
     * @return LatestApprovedPrice
     */	
	public java.math.BigDecimal getLatestApprovedPrice() {
		return this.latestApprovedPrice;
	}
	
	/**
	 * @param hasSupervision
	 */
	public void setHasSupervision(String hasSupervision) {
		this.hasSupervision = hasSupervision == null ? null : hasSupervision.trim();
	}
	
    /**
     * @return HasSupervision
     */	
	public String getHasSupervision() {
		return this.hasSupervision;
	}
	
	/**
	 * @param supervisionCompanyName
	 */
	public void setSupervisionCompanyName(String supervisionCompanyName) {
		this.supervisionCompanyName = supervisionCompanyName == null ? null : supervisionCompanyName.trim();
	}
	
    /**
     * @return SupervisionCompanyName
     */	
	public String getSupervisionCompanyName() {
		return this.supervisionCompanyName;
	}
	
	/**
	 * @param supervisionOrgCode
	 */
	public void setSupervisionOrgCode(String supervisionOrgCode) {
		this.supervisionOrgCode = supervisionOrgCode == null ? null : supervisionOrgCode.trim();
	}
	
    /**
     * @return SupervisionOrgCode
     */	
	public String getSupervisionOrgCode() {
		return this.supervisionOrgCode;
	}
	
	/**
	 * @param agreementBeginDate
	 */
	public void setAgreementBeginDate(String agreementBeginDate) {
		this.agreementBeginDate = agreementBeginDate == null ? null : agreementBeginDate.trim();
	}
	
    /**
     * @return AgreementBeginDate
     */	
	public String getAgreementBeginDate() {
		return this.agreementBeginDate;
	}
	
	/**
	 * @param agreementEndDate
	 */
	public void setAgreementEndDate(String agreementEndDate) {
		this.agreementEndDate = agreementEndDate == null ? null : agreementEndDate.trim();
	}
	
    /**
     * @return AgreementEndDate
     */	
	public String getAgreementEndDate() {
		return this.agreementEndDate;
	}
	
	/**
	 * @param gylVal
	 */
	public void setGylVal(java.math.BigDecimal gylVal) {
		this.gylVal = gylVal;
	}
	
    /**
     * @return GylVal
     */	
	public java.math.BigDecimal getGylVal() {
		return this.gylVal;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param carMeasureUnitOther
	 */
	public void setCarMeasureUnitOther(String carMeasureUnitOther) {
		this.carMeasureUnitOther = carMeasureUnitOther == null ? null : carMeasureUnitOther.trim();
	}
	
    /**
     * @return CarMeasureUnitOther
     */	
	public String getCarMeasureUnitOther() {
		return this.carMeasureUnitOther;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param amt
	 */
	public void setAmt(java.math.BigDecimal amt) {
		this.amt = amt;
	}
	
    /**
     * @return Amt
     */	
	public java.math.BigDecimal getAmt() {
		return this.amt;
	}
	
	/**
	 * @param keepId
	 */
	public void setKeepId(String keepId) {
		this.keepId = keepId == null ? null : keepId.trim();
	}
	
    /**
     * @return KeepId
     */	
	public String getKeepId() {
		return this.keepId;
	}
	
	/**
	 * @param coodsDetailType
	 */
	public void setCoodsDetailType(String coodsDetailType) {
		this.coodsDetailType = coodsDetailType == null ? null : coodsDetailType.trim();
	}
	
    /**
     * @return CoodsDetailType
     */	
	public String getCoodsDetailType() {
		return this.coodsDetailType;
	}
	
	/**
	 * @param goodsModel
	 */
	public void setGoodsModel(String goodsModel) {
		this.goodsModel = goodsModel == null ? null : goodsModel.trim();
	}
	
    /**
     * @return GoodsModel
     */	
	public String getGoodsModel() {
		return this.goodsModel;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}


}