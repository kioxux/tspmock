package cn.com.yusys.yusp.web.server.xdtz0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0007.req.Xdtz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.resp.Xdtz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0007.Xdtz0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0007:根据客户号获取非信用方式发放贷款的最长到期日")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0007Resource.class);

    @Autowired
    private Xdtz0007Service xdtz0007Service;

    /**
     * 交易码：xdtz0007
     * 交易描述：根据客户号获取非信用方式发放贷款的最长到期日
     *
     * @param xdtz0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号获取非信用方式发放贷款的最长到期日")
    @PostMapping("/xdtz0007")
    protected @ResponseBody
    ResultDto<Xdtz0007DataRespDto> xdtz0007(@Validated @RequestBody Xdtz0007DataReqDto xdtz0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007DataReqDto));
        Xdtz0007DataRespDto xdtz0007DataRespDto = new Xdtz0007DataRespDto();// 响应Dto:根据客户号获取非信用方式发放贷款的最长到期日
        ResultDto<Xdtz0007DataRespDto> xdtz0007DataResultDto = new ResultDto<>();
        String cusNo = xdtz0007DataReqDto.getCusNo();//客户号
        try {
            // 从xdtz0007DataReqDto获取业务值进行业务逻辑处理
            // 调用xdtz0007Service层开始
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, cusNo);
            xdtz0007DataRespDto = Optional.ofNullable(xdtz0007Service.getLastLoanEndDate(cusNo)).orElseGet(() -> {
                Xdtz0007DataRespDto temp = new Xdtz0007DataRespDto();
                temp.setLoanEndDate(StringUtils.EMPTY);
                return temp;
            });
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007DataRespDto));
            // 封装xdtz0007DataResultDto中正确的返回码和返回信息
            xdtz0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, e.getMessage());
            // 封装xdtz0007DataResultDto中异常返回码和返回信息
            xdtz0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0007DataRespDto到xdtz0007DataResultDto中
        xdtz0007DataResultDto.setData(xdtz0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007DataResultDto));
        return xdtz0007DataResultDto;
    }
}
