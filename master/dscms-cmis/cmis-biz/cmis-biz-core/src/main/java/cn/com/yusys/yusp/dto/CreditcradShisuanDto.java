package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContInsur
 * @类描述: ctr_cont_insur数据实体类
 * @功能描述: 
 * @创建人: zsm
 * @创建时间: 2021-05-27 16:30:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CreditcradShisuanDto implements Serializable{
	private static final long serialVersionUID = 1L;

	//分期总期数
	private String lnittm;

	//分期总本金
	private BigDecimal lnitpn;

	//分期每期应还本金
	private BigDecimal lnfdpt;

	//分期首期应还本金
	private BigDecimal lnfttm;

	//分期末期应还本金
	private BigDecimal lnflt2;

	//分期总手续费
	private BigDecimal lnitfi;

	//分期每期手续费
	private BigDecimal lnfdfi;

	//分期首期手续费
	private BigDecimal lnftfi;

	//分期末期手续费
	private BigDecimal lnfltm;

	//分期近似年化利率
	private BigDecimal  anrate;

	public BigDecimal getAnrate() {
		return anrate;
	}
	public void setAnrate(BigDecimal anrate) {
		this.anrate = anrate;
	}
	public String getLnittm() {
		return lnittm;
	}

	public void setLnittm(String lnittm) {
		this.lnittm = lnittm;
	}

	public BigDecimal getLnitpn() {
		return lnitpn;
	}

	public void setLnitpn(BigDecimal lnitpn) {
		this.lnitpn = lnitpn;
	}

	public BigDecimal getLnfdpt() {
		return lnfdpt;
	}

	public void setLnfdpt(BigDecimal lnfdpt) {
		this.lnfdpt = lnfdpt;
	}

	public BigDecimal getLnfttm() {
		return lnfttm;
	}

	public void setLnfttm(BigDecimal lnfttm) {
		this.lnfttm = lnfttm;
	}

	public BigDecimal getLnflt2() {
		return lnflt2;
	}

	public void setLnflt2(BigDecimal lnflt2) {
		this.lnflt2 = lnflt2;
	}

	public BigDecimal getLnitfi() {
		return lnitfi;
	}

	public void setLnitfi(BigDecimal lnitfi) {
		this.lnitfi = lnitfi;
	}

	public BigDecimal getLnfdfi() {
		return lnfdfi;
	}

	public void setLnfdfi(BigDecimal lnfdfi) {
		this.lnfdfi = lnfdfi;
	}

	public BigDecimal getLnftfi() {
		return lnftfi;
	}

	public void setLnftfi(BigDecimal lnftfi) {
		this.lnftfi = lnftfi;
	}

	public BigDecimal getLnfltm() {
		return lnfltm;
	}

	public void setLnfltm(BigDecimal lnfltm) {
		this.lnfltm = lnfltm;
	}
}