/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtCobInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCobInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-04 09:27:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtCobInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtCobInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtCobInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtCobInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtCobInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtCobInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtCobInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @author hubp
     * @date 2021/5/27 15:25
     * @version 1.0.0
     * @desc   通过流水号删除多个数据
     * @修改历史: 修改时间    修改人员    修改原因
     */

    int deleteBySerno(@Param("surveySerno") String surveySerno);
    /**
     * @param cusId
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>
     * @author hubp
     * @date 2021/6/2 9:46
     * @version 1.0.0
     * @desc    根据客户ID查找信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<LmtCobInfo> selectByCusIdAndSerno(@Param("cusId") String cusId,@Param("surveySerno") String surveySerno);

    /***
     * @param iqpSerno
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>
     * @author hubp
     * @date 2021/4/27 16:29
     * @version 1.0.0
     * @desc    通过流水号查询共有人信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<LmtCobInfo> selectByIqpSerno(String iqpSerno);

    /***
     * @param contNo
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>
     * @author hubp
     * @date 2021/6/6 14:00
     * @version 1.0.0
     * @desc    根据合同号查询名下共借人关系
     * @修改历史: 修改时间    修改人员    修改原因
     */
    String selectRelByContNo(@Param("contNo") String contNo);

    /**
     * 根据合同号、共借人身份证号更新共同借款人信息
     * @param queryMap
     * @return
     */
    int updateSignStateByContNo(Map queryMap);

    /***
     * @param contNo
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>
     * @author hubp
     * @date 2021/6/6 14:00
     * @version 1.0.0
     * @desc    根据合同号查询名下共借人签约状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    String selectSignFalgByContNo(@Param("contNo") String contNo);
}