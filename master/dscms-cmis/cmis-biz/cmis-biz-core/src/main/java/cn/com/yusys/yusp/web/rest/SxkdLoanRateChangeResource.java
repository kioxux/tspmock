/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.SxkdLoanRateChange;
import cn.com.yusys.yusp.service.SxkdLoanRateChangeService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdLoanRateChangeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zl
 * @创建时间: 2021-08-17 21:47:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/sxkdloanratechange")
public class SxkdLoanRateChangeResource {
    @Autowired
    private SxkdLoanRateChangeService sxkdLoanRateChangeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<SxkdLoanRateChange>> query() {
        QueryModel queryModel = new QueryModel();
        List<SxkdLoanRateChange> list = sxkdLoanRateChangeService.selectAll(queryModel);
        return new ResultDto<List<SxkdLoanRateChange>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<SxkdLoanRateChange>> index(QueryModel queryModel) {
        List<SxkdLoanRateChange> list = sxkdLoanRateChangeService.selectByModel(queryModel);
        return new ResultDto<List<SxkdLoanRateChange>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<SxkdLoanRateChange> show(@PathVariable("serno") String serno) {
        SxkdLoanRateChange sxkdLoanRateChange = sxkdLoanRateChangeService.selectByPrimaryKey(serno);
        return new ResultDto<SxkdLoanRateChange>(sxkdLoanRateChange);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<SxkdLoanRateChange> create(@RequestBody SxkdLoanRateChange sxkdLoanRateChange) throws URISyntaxException {
        sxkdLoanRateChangeService.insert(sxkdLoanRateChange);
        return new ResultDto<SxkdLoanRateChange>(sxkdLoanRateChange);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody SxkdLoanRateChange sxkdLoanRateChange) throws URISyntaxException {
        int result = sxkdLoanRateChangeService.update(sxkdLoanRateChange);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = sxkdLoanRateChangeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = sxkdLoanRateChangeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertSxkdLoanRateChange
     * @函数描述:省心快贷利率修改新增
     * @参数与返回说明:
     * @创建者：zl
     * @算法描述:
     */
    @ApiOperation("")
    @PostMapping("/insertSxkdLoanRateChange")
    protected ResultDto<Map> insertSxkdLoanRateChange(@RequestBody SxkdLoanRateChange sxkdLoanRateChange) throws URISyntaxException {
        Map rtnData = sxkdLoanRateChangeService.insertSxkdLoanRateChange(sxkdLoanRateChange);
        return new ResultDto<>(rtnData);
    }
    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @参数与返回说明:
     * @创建者：zl
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        SxkdLoanRateChange sxkdLoanRateChange = sxkdLoanRateChangeService.selectBySerno((String) params.get("serno"));
        if (sxkdLoanRateChange != null) {
            resultDto.setData(sxkdLoanRateChange);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:delete
     * @函数描述:物理删除，公共API接口
     * @参数与返回说明:
     * @创建者：zl
     * @算法描述:
     */
    @ApiOperation("省心快贷利率修改删除")
    @PostMapping("/sxkdLoanRateChangedelete")
    public ResultDto<Map> sxkdLoanRateChangedelete(@RequestBody SxkdLoanRateChange sxkdLoanRateChange) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = sxkdLoanRateChangeService.sxkdLoanRateChangedelete(sxkdLoanRateChange);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:sxkdLoanRateChangelist
     * @函数描述: 省心快贷线上合同修改申请
     * @参数与返回说明:
     * @创建者: zl
     * @算法描述:
     */
    @ApiOperation("省心快贷线上合同修改申请")
    @PostMapping("/selectSxkdLoanRateChangelist")
    protected ResultDto<List<SxkdLoanRateChange>> selectSxkdLoanRateChangelist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SxkdLoanRateChange> list = sxkdLoanRateChangeService.selectSxkdLoanRateChangelist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SxkdLoanRateChange>>(list);
    }

    /**
     * @函数名称:sxkdLoanRateChangeHislist
     * @函数描述: 省心快贷线上合同申请历史
     * @参数与返回说明:
     * @创建者: zl
     * @算法描述:
     */
    @ApiOperation("省心快贷线上合同申请历史")
    @PostMapping("/selectSxkdLoanRateChangeHislist")
    protected ResultDto<List<SxkdLoanRateChange>> selectSxkdLoanRateChangeHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SxkdLoanRateChange> list = sxkdLoanRateChangeService.selectSxkdLoanRateChangeHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SxkdLoanRateChange>>(list);
    }
}
