/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppSpecialService
 * @类描述: #特殊业务申请服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-03 15:22:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpLoanAppSpecialService {

    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppSpecialService.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;//申请主表
    @Autowired
    private IqpLmtRelService iqpLmtRelService;//授信
    @Autowired
    private IqpLoanAppAssistService iqpLoanAppAssistService;//辅助信息
    @Autowired
    private IqpLoanAppRepayService iqpLoanAppRepayService;//还款信息
    @Autowired
    private IqpBillRelService iqpBillRelService;//业务与借据关系服务
    @Autowired
    private BizCorreManagerInfoService bizCorreManagerInfoService;//办理人员

}
