/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayTermChg;
import cn.com.yusys.yusp.service.IqpRepayTermChgService;
import cn.com.yusys.yusp.service.IqpRepayWayChgService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayTermChgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-18 19:24:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepaytermchg")
public class IqpRepayTermChgResource {
    @Autowired
    private IqpRepayTermChgService iqpRepayTermChgService;
    private static final Logger log = LoggerFactory.getLogger(IqpRepayTermChgService.class);

    @Autowired
    private IqpRepayWayChgService iqpRepayWayChgService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRepayTermChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRepayTermChg> list = iqpRepayTermChgService.selectAll(queryModel);
        return new ResultDto<List<IqpRepayTermChg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRepayTermChg>> index(QueryModel queryModel) {
        List<IqpRepayTermChg> list = iqpRepayTermChgService.selectByModel(queryModel);
        return new ResultDto<List<IqpRepayTermChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpRepayTermChg> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpRepayTermChg iqpRepayTermChg = iqpRepayTermChgService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpRepayTermChg>(iqpRepayTermChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpRepayTermChg> create(@RequestBody IqpRepayTermChg iqpRepayTermChg) throws URISyntaxException {
        iqpRepayTermChgService.insert(iqpRepayTermChg);
        return new ResultDto<IqpRepayTermChg>(iqpRepayTermChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRepayTermChg iqpRepayTermChg) throws URISyntaxException {
        int result = iqpRepayTermChgService.update(iqpRepayTermChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpRepayTermChgService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRepayTermChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param iqpRepayTermChg
     * @函数名称: insertIqpRepayTermChgByBillNo
     * @函数描述: 新增还款间隔周期变更申请表数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertIqpRepayTermChgByBillNo")
    protected ResultDto<Integer> insertIqpRepayTermChgByBillNo(@RequestBody IqpRepayTermChg iqpRepayTermChg) throws Exception {
        log.info("新增还款间隔周期变更申请表数据【{}】", JSONObject.toJSON(iqpRepayTermChg));
        int result = iqpRepayTermChgService.insertIqpRepayTermChgByBillNo(iqpRepayTermChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: checkIsExistIqpRepayTermChgBizByBillNo
     * @函数描述: 查询借据编号是否存在在途的变更业务
     * @参数与返回说明:
     * @算法描述:
     * @param iqpRepayTermChg
     */
    @PostMapping("/checkIsExistIqpRepayTermChgBizByBillNo")
    protected ResultDto<Integer> checkIsExistIqpRepayTermChgBizByBillNo(@RequestBody IqpRepayTermChg iqpRepayTermChg) throws  URISyntaxException{
        log.info("查询借据编号是否存在在途的变更业务【{}】", JSONObject.toJSON(iqpRepayTermChg));
        int result = iqpRepayTermChgService.checkIsExistChgBizByBillNo(iqpRepayTermChg.getIqpSerno(),iqpRepayTermChg.getBillNo());
        return new ResultDto<Integer>(result);
    }
}
