package cn.com.yusys.yusp.web.server.xdzc0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0022.req.Xdzc0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.resp.Xdzc0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0022.Xdzc0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:发票补录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0022:发票补录")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0022Resource.class);

    @Autowired
    private Xdzc0022Service xdzc0022Service;
    /**
     * 交易码：xdzc0022
     * 交易描述：发票补录
     *
     * @param xdzc0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("发票补录")
    @PostMapping("/xdzc0022")
    protected @ResponseBody
    ResultDto<Xdzc0022DataRespDto> xdzc0022(@Validated @RequestBody Xdzc0022DataReqDto xdzc0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022DataReqDto));
        Xdzc0022DataRespDto xdzc0022DataRespDto = new Xdzc0022DataRespDto();// 响应Dto:发票补录
        ResultDto<Xdzc0022DataRespDto> xdzc0022DataResultDto = new ResultDto<>();

        try {
            xdzc0022DataRespDto = xdzc0022Service.xdzc0022Service(xdzc0022DataReqDto);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, e.getMessage());
            // 封装xdzc0022DataResultDto中异常返回码和返回信息
            xdzc0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0022DataRespDto到xdzc0022DataResultDto中
        xdzc0022DataResultDto.setData(xdzc0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022DataResultDto));
        return xdzc0022DataResultDto;
    }
}
