package cn.com.yusys.yusp.service.server.xdls0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdls0002.req.Xdls0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0002.resp.Xdls0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtEgcInfoMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 接口处理类:市民贷联系人信息查询
 *
 * @author YX-QX
 * @version 1.0
 */
@Service
public class Xdls0002Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdls0002Service.class);

    @Autowired
    private LmtEgcInfoMapper lmtEgcInfoMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 市民贷联系人信息查询
     *
     * @param xdls0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdls0002DataRespDto xdls0006(Xdls0002DataReqDto xdls0002DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002DataReqDto));
        //返回对象
        Xdls0002DataRespDto xdls0002DataRespDto = new Xdls0002DataRespDto();
        try {
            String indgtSerno = xdls0002DataReqDto.getIndgtSerno();//调查流水号
            logger.info("**********XDLS0002**市民贷联系人信息查询记录开始,查询参数为:{}", JSON.toJSONString(indgtSerno));
            List<cn.com.yusys.yusp.dto.server.xdls0002.resp.List> lists = lmtEgcInfoMapper.getLmtEgcList(indgtSerno);
            logger.info("**********XDLS0002**市民贷联系人信息查询记录结束,查询结果为:{}", JSON.toJSONString(lists));
            for (int i = 0; i < lists.size(); i++) {
                cn.com.yusys.yusp.dto.server.xdls0002.resp.List dto = lists.get(i);
                String cusId = dto.getCusId();
                //通过客户证件号查询客户信息
                ResultDto<CusIndivDto> cusIndiv = cmisCusClientService.queryCusindivByCusid(cusId);
                String certType = cusIndiv.getData().getCertType();
                String certCode = cusIndiv.getData().getCertCode();
                String nation = cusIndiv.getData().getNation();
                String sex = cusIndiv.getData().getSex();
                String certEndDt = cusIndiv.getData().getCertEndDt();
                String indivHouhRegAdd = cusIndiv.getData().getIndivHouhRegAdd();
                dto.setCertNo(certCode);
                dto.setCertType(certType);
                dto.setNati(nation);
                dto.setSex(sex);
                dto.setCertExpDt(certEndDt);
                dto.setLinkmanAddr(indivHouhRegAdd);
            }
            xdls0002DataRespDto.setList(lists);
        } catch (BizException e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002DataRespDto));
        return xdls0002DataRespDto;
    }
}
