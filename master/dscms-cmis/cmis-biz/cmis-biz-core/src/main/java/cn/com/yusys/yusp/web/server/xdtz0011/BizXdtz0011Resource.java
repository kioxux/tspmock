package cn.com.yusys.yusp.web.server.xdtz0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0011.req.Xdtz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.Xdtz0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0011.Xdtz0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:借据明细查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0011:借据明细查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0011Resource.class);

    @Autowired
    private Xdtz0011Service xdtz0011Service;
    /**
     * 交易码：XDTZ0011
     * 交易描述：借据明细查询
     *
     * @param xdtz0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据明细查询")
    @PostMapping("/xdtz0011")
    protected @ResponseBody
    ResultDto<Xdtz0011DataRespDto> xdtz0011(@Validated @RequestBody Xdtz0011DataReqDto xdtz0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataReqDto));
        Xdtz0011DataRespDto xdtz0011DataRespDto = new Xdtz0011DataRespDto();// 响应Dto:借据明细查询
        ResultDto<Xdtz0011DataRespDto> xdtz0011DataResultDto = new ResultDto<>();
        try {
            // 从XDTZ0011DataReqDto获取业务值进行业务逻辑处理
            xdtz0011DataRespDto = xdtz0011Service.xdtz0011(xdtz0011DataReqDto);
            xdtz0011DataResultDto.setData(xdtz0011DataRespDto);
            // 封装XDTZ0011DataResultDto中正确的返回码和返回信息
            xdtz0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, e.getMessage());
            // 封装XDTZ0011DataResultDto中异常返回码和返回信息
            xdtz0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装XDTZ0011DataRespDto到XDTZ0011DataResultDto中
        xdtz0011DataResultDto.setData(xdtz0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataResultDto));
        return xdtz0011DataResultDto;
    }
}
