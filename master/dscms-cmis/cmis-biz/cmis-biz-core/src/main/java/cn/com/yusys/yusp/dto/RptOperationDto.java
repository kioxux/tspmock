package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.RptOperEnergyCons;
import cn.com.yusys.yusp.domain.RptOperEnergySales;
import cn.com.yusys.yusp.domain.RptOperProfitSituation;
import cn.com.yusys.yusp.domain.RptOperation;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperation
 * @类描述: rpt_operation数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 13:55:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptOperationDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private RptOperation rptOperation;

	private RptOperEnergyCons rptOperEnergyCons;

	private RptOperEnergySales rptOperEnergySales;

	private RptOperProfitSituation rptOperProfitSituation;

	public RptOperation getRptOperation() {
		return rptOperation;
	}

	public RptOperEnergyCons getRptOperEnergyCons() {
		return rptOperEnergyCons;
	}

	public RptOperEnergySales getRptOperEnergySales() {
		return rptOperEnergySales;
	}

	public RptOperProfitSituation getRptOperProfitSituation() {
		return rptOperProfitSituation;
	}

	public void setRptOperation(RptOperation rptOperation) {
		this.rptOperation = rptOperation;
	}

	public void setRptOperEnergyCons(RptOperEnergyCons rptOperEnergyCons) {
		this.rptOperEnergyCons = rptOperEnergyCons;
	}

	public void setRptOperEnergySales(RptOperEnergySales rptOperEnergySales) {
		this.rptOperEnergySales = rptOperEnergySales;
	}

	public void setRptOperProfitSituation(RptOperProfitSituation rptOperProfitSituation) {
		this.rptOperProfitSituation = rptOperProfitSituation;
	}
}