/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituExtGuar
 * @类描述: rpt_cptl_situ_ext_guar数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:53:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_cptl_situ_ext_guar")
public class RptCptlSituExtGuar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 担保性质 **/
	@Column(name = "GUAR_CHA", unique = false, nullable = true, length = 5)
	private String guarCha;
	
	/** 被担保企业名称 **/
	@Column(name = "BE_GUAR_CORP_NAME", unique = false, nullable = true, length = 80)
	private String beGuarCorpName;
	
	/** 余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal balance;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 被担保企业目前经营情况 **/
	@Column(name = "BE_GUAR_CORP_CUR_OPERATION", unique = false, nullable = true, length = 65535)
	private String beGuarCorpCurOperation;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param guarCha
	 */
	public void setGuarCha(String guarCha) {
		this.guarCha = guarCha;
	}
	
    /**
     * @return guarCha
     */
	public String getGuarCha() {
		return this.guarCha;
	}
	
	/**
	 * @param beGuarCorpName
	 */
	public void setBeGuarCorpName(String beGuarCorpName) {
		this.beGuarCorpName = beGuarCorpName;
	}
	
    /**
     * @return beGuarCorpName
     */
	public String getBeGuarCorpName() {
		return this.beGuarCorpName;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(java.math.BigDecimal balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public java.math.BigDecimal getBalance() {
		return this.balance;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param beGuarCorpCurOperation
	 */
	public void setBeGuarCorpCurOperation(String beGuarCorpCurOperation) {
		this.beGuarCorpCurOperation = beGuarCorpCurOperation;
	}
	
    /**
     * @return beGuarCorpCurOperation
     */
	public String getBeGuarCorpCurOperation() {
		return this.beGuarCorpCurOperation;
	}


}