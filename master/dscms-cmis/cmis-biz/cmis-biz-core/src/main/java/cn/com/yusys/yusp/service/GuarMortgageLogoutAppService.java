package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.GuarMortgageLogoutApp;
import cn.com.yusys.yusp.domain.GuarRegisterDetail;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarMortgageLogoutAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageLogoutAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 09:44:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarMortgageLogoutAppService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuarMortgageLogoutAppService.class);

    @Autowired
    private GuarMortgageLogoutAppMapper guarMortgageLogoutAppMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarMortgageLogoutApp selectByPrimaryKey(String serno) {
        return guarMortgageLogoutAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<GuarMortgageLogoutApp> selectAll(QueryModel model) {
        List<GuarMortgageLogoutApp> records = (List<GuarMortgageLogoutApp>) guarMortgageLogoutAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarMortgageLogoutApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarMortgageLogoutApp> list = guarMortgageLogoutAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GuarMortgageLogoutApp record) {
        return guarMortgageLogoutAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GuarMortgageLogoutApp record) {
        return guarMortgageLogoutAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GuarMortgageLogoutApp record) {
        return guarMortgageLogoutAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GuarMortgageLogoutApp record) {
        return guarMortgageLogoutAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarMortgageLogoutAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarMortgageLogoutAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateByIds
     * @方法描述: 根据多个主键逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByIds(String ids) {
        return guarMortgageLogoutAppMapper.updateByIds(ids);
    }


    /**
     * 获取sequence流水号
     **/
    public ResultDto getSequences() {
        //TODO...z暂时默认uuid  厦门  cmis-sequence服务没有启动。
        String serno = UUID.randomUUID().toString().replace("-", "");
        //String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.DYL_SERNO, new HashMap<>());
        return new ResultDto(serno);
    }

    //流程最终处理类

    /***
     * 1.只有抵押登记中心同步回来的权证状态为注销的才能结束注销流程
     * @param record
     * @return
     */
    public ResultDto dealEnd(GuarMortgageLogoutApp record) {
        ResultDto<Integer> result = new ResultDto<Integer>();
        record.setApproveStatus(BizFlowConstant.WF_STATUS_997);
        try {
            // 出库经办机构是支行  出库审批状态是 997
            List<GuarRegisterDetail> guarRegisterDetailList = new ArrayList<GuarRegisterDetail>();
            // guarRegDetailService.selectByRegSerno(record.getSerno());
            int flag = -1;
            //检查
            /*guarRegisterDetailList.stream().forEach(a->{

            });*/
            //只有抵押登记中心同步回来的权证状态为注销的才能结束注销流程
            int num = guarMortgageLogoutAppMapper.updateByPrimaryKeySelective(record);
            if (0 > num) {
                result.setCode(CmisCommonConstants.INTERFACE_SUCCESS_CODE);
                result.setMessage(String.valueOf(EcbEnum.COMMON_SUCCESS_DEF));
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            result.setCode(CmisCommonConstants.INTERFACE_FAIL_CODE);
            result.setMessage(String.valueOf(EcbEnum.COMMON_EXCEPTION_DEF));
        }
        return result;
    }

    /**
     * 通过主键逻辑删除
     *
     * @param pkId
     * @return
     */
    public int deleteOnLogic(String pkId) {
        GuarMortgageLogoutApp record = new GuarMortgageLogoutApp();
        record.setSerno(pkId);
        record.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return guarMortgageLogoutAppMapper.updateByPrimaryKeySelective(record);
    }
}
