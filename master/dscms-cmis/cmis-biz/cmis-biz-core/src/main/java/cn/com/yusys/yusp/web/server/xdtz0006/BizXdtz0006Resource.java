package cn.com.yusys.yusp.web.server.xdtz0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0006.req.Xdtz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0006.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0006.resp.Xdtz0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.xdtz0006.Xdtz0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类:根据证件号查询借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0006:根据证件号查询借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0006Resource.class);

    @Autowired
    private Xdtz0006Service xdtz0006Service;

    @Autowired
    private CommonService commonService;

    /**
     * 交易码：xdtz0006
     * 交易描述：根据证件号查询借据信息
     *
     * @param xdtz0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据证件号查询借据信息")
    @PostMapping("/xdtz0006")
    protected @ResponseBody
    ResultDto<Xdtz0006DataRespDto> xdtz0006(@Validated @RequestBody Xdtz0006DataReqDto xdtz0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006DataReqDto));
        Xdtz0006DataRespDto xdtz0006DataRespDto = new Xdtz0006DataRespDto();// 响应Dto:根据证件号查询借据信息
        ResultDto<Xdtz0006DataRespDto> xdtz0006DataResultDto = new ResultDto<>();
        String queryType = xdtz0006DataReqDto.getQueryType();//查询类型
        String certNo = xdtz0006DataReqDto.getCertNo();//证件号
        try {
            // 从xdtz0006DataReqDto获取业务值进行业务逻辑处理
            Map queryMap = new HashMap();
            java.util.List<String> cusIds = commonService.getCusBaseByCertCode(certNo);
            if (cusIds.size() == 0) {
                logger.info("****xdtz0006***根据证件号码【{}】未查询到该客户信息,该客户为新客户！", certNo);
                xdtz0006DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0006DataResultDto.setMessage("根据证件号码未查询到该客户信息");
                return xdtz0006DataResultDto;
            }
            queryMap.put("cusIds", cusIds);
            if (CmisBizConstants.CODE_JQ.equals(queryType)) {
                queryMap.put("isJq",Boolean.TRUE);
            }
            if (CmisBizConstants.CODE_ALL.equals(queryType)) {
                queryMap.put("isJq",Boolean.FALSE);
            }
            // 调用xdtz0006Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(queryMap));
            java.util.List<List> list = Optional.ofNullable(xdtz0006Service.getWjqList(queryMap)).orElseGet(() -> {
                java.util.List<List> temp = xdtz0006DataRespDto.getList();
                temp.get(0).setBillNo(StringUtils.EMPTY);// 借据号
                temp.get(0).setBillBal(new BigDecimal(0L));// 借据余额
                temp.get(0).setPrdName(StringUtils.EMPTY);// 产品名称
                temp.get(0).setCnAssureMeans(StringUtils.EMPTY);// 担保方式中文
                temp.get(0).setRealityIrY(new BigDecimal(0L));// 执行利率（%）
                temp.get(0).setStartDate(StringUtils.EMPTY);// 起始日期
                temp.get(0).setEndDate(StringUtils.EMPTY);// 截至日期
                temp.get(0).setBillStatus(StringUtils.EMPTY);// 借据状态
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(list));


            xdtz0006DataRespDto.setList(list);
            // 封装xdtz0006DataResultDto中正确的返回码和返回信息
            xdtz0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, e.getMessage());
            // 封装xdtz0006DataResultDto中异常返回码和返回信息
            xdtz0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0006DataRespDto到xdtz0006DataResultDto中
        xdtz0006DataResultDto.setData(xdtz0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006DataResultDto));
        return xdtz0006DataResultDto;
    }
}
