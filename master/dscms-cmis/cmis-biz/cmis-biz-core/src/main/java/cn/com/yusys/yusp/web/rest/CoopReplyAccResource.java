/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopReplyAcc;
import cn.com.yusys.yusp.service.CoopReplyAccService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopreplyacc")
public class CoopReplyAccResource {
    @Autowired
    private CoopReplyAccService coopReplyAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopReplyAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopReplyAcc> list = coopReplyAccService.selectAll(queryModel);
        return new ResultDto<List<CoopReplyAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopReplyAcc>> index(QueryModel queryModel) {
        List<CoopReplyAcc> list = coopReplyAccService.selectByModel(queryModel);
        return new ResultDto<List<CoopReplyAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{replyNo}")
    protected ResultDto<CoopReplyAcc> show(@PathVariable("replyNo") String replyNo) {
        CoopReplyAcc coopReplyAcc = coopReplyAccService.selectByPrimaryKey(replyNo);
        return new ResultDto<CoopReplyAcc>(coopReplyAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopReplyAcc> create(@RequestBody CoopReplyAcc coopReplyAcc) throws URISyntaxException {
        coopReplyAccService.insert(coopReplyAcc);
        return new ResultDto<CoopReplyAcc>(coopReplyAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopReplyAcc coopReplyAcc) throws URISyntaxException {
        int result = coopReplyAccService.update(coopReplyAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{replyNo}")
    protected ResultDto<Integer> delete(@PathVariable("replyNo") String replyNo) {
        int result = coopReplyAccService.deleteByPrimaryKey(replyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopReplyAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
