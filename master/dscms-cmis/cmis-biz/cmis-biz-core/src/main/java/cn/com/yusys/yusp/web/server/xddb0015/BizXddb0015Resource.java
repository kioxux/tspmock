package cn.com.yusys.yusp.web.server.xddb0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0015.req.Xddb0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0015.resp.Xddb0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0015.Xddb0015Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:押品状态变更推送
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0015:押品状态变更推送")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0015Resource.class);

    @Autowired
    private Xddb0015Service xddb0015Service;
    /**
     * 交易码：xddb0015
     * 交易描述：押品状态变更推送
     *
     * @param xddb0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态变更推送")
    @PostMapping("/xddb0015")
    protected @ResponseBody
    ResultDto<Xddb0015DataRespDto> xddb0015(@Validated @RequestBody Xddb0015DataReqDto xddb0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataReqDto));
        Xddb0015DataRespDto xddb0015DataRespDto = new Xddb0015DataRespDto();// 响应Dto:押品状态变更推送
        ResultDto<Xddb0015DataRespDto> xddb0015DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataReqDto));
            xddb0015DataRespDto = xddb0015Service.getXddx0015(xddb0015DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataRespDto));
            // 封装xddb0015DataResultDto中正确的返回码和返回信息
            xddb0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, e.getMessage());
            // 封装xddb0015DataResultDto中异常返回码和返回信息
            xddb0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0015DataRespDto到xddb0015DataResultDto中
        xddb0015DataResultDto.setData(xddb0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataRespDto));
        return xddb0015DataResultDto;
    }
}
