/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydLoanOverdue
 * @类描述: tmp_wyd_loan_overdue数据实体类
 * @功能描述: 
 * @创建人: zrcbank-fengjj
 * @创建时间: 2021-09-07 08:56:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_loan_overdue")
public class TmpWydLoanOverdue{
	
	/** 数据日期 **/
	@Id
	@Column(name = "DATA_DT")
	private String dataDt;
	
	/** 借据号 **/
	@Id
	@Column(name = "LENDING_REF")
	private String lendingRef;

	/** 借据号 **/
	@Id
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 64)
	private String cusId;
	
	/** 借据状态 **/
	@Column(name = "CONS_OVERDUE_DAYS", unique = false, nullable = true, length = 10)
	private Integer consOverdueDays;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}


	public Integer getConsOverdueDays() {
		return consOverdueDays;
	}

	public void setConsOverdueDays(Integer consOverdueDays) {
		this.consOverdueDays = consOverdueDays;
	}
}