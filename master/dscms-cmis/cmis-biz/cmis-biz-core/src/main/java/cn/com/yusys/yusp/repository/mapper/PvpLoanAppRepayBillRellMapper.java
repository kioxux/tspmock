/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRel;
import cn.com.yusys.yusp.dto.RepayBillRelDto;
import cn.com.yusys.yusp.dto.RepayBillRellDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRell;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRellMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-15 14:34:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PvpLoanAppRepayBillRellMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PvpLoanAppRepayBillRell selectByPrimaryKey(@Param("pkId") String pkId, @Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PvpLoanAppRepayBillRell> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(PvpLoanAppRepayBillRell record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PvpLoanAppRepayBillRell record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PvpLoanAppRepayBillRell record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PvpLoanAppRepayBillRell record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId, @Param("serno") String serno);

    /**
     * @方法名称: deleteByBillNo
     * @方法描述: 根据借据编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByBillNo(@Param("billNo") String billNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<RepayBillRellDto> selectRepayBillRellByParams(QueryModel model);

    /**
     * @方法名称: selectByPvpSerno
     * @方法描述: 根据出账申请流水号查询关联的原借据
     * @参数与返回说明:
     * @算法描述: 无
     */

    PvpLoanAppRepayBillRell selectByPvpSerno(@Param("pvpSerno") String pvpSerno);

}