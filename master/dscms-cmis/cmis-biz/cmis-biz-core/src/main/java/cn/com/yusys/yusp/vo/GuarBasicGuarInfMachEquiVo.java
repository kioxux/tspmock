package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfMachEqui;

public class GuarBasicGuarInfMachEquiVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfMachEqui guarInfMachEqui;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfMachEqui getGuarInfMachEqui() {
        return guarInfMachEqui;
    }

    public void setGuarInfMachEqui(GuarInfMachEqui guarInfMachEqui) {
        this.guarInfMachEqui = guarInfMachEqui;
    }
}
