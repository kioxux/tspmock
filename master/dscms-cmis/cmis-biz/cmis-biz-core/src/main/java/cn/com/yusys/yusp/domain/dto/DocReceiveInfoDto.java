/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.dto;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadAppInfo
 * @类描述: DocReceiveInfo数据实体类
 * @功能描述: 档案接收信息:调阅申请信息和档案基本信息
 * @创建人: hujun
 * @创建时间: 2021-06-17 17:00:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocReceiveInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 调阅明细流水号 **/
	@Column(name = "DRDI_SERNO")
	private String drdiSerno;

	/** 调阅流水号 **/
	@Column(name = "DRAI_SERNO")
	private String draiSerno;

	/** 调阅形式 **/
	@Column(name = "READ_MODE")
	private String readMode;

	/** 调阅类型 **/
	@Column(name = "READ_TYPE")
	private String readType;

	/** 归还日期 **/
	@Column(name = "BACK_DATE")
	private String backDate;
	
	/** 调阅申请人 **/
	@Column(name = "READ_RQSTR_ID")
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="readRqstrName")
	private String readRqstrId;
	
	/** 调阅申请机构 **/
	@Column(name = "READ_RQSTR_ORG")
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="readRqstrOrgName" )
	private String readRqstrOrg;
	
	/** 调阅申请日期 **/
	@Column(name = "READ_RQSTR_DATE")
	private String readRqstrDate;
	
	/** 调阅原因 **/
	@Column(name = "READ_REASON")
	private String readReason;
	
	/** 原因描述 **/
	@Column(name = "REASON_MOME")
	private String reasonMome;
	
	/** 档案状态 **/
	@Column(name = "DOC_STAUTS")
	private String docStauts;

	/** 档案编号 **/
	@Column(name = "DOC_NO")
	private String docNo;

	/** 档案分类 **/
	@Column(name = "DOC_CLASS")
	private String docClass;

	/** 档案类型 **/
	@Column(name = "DOC_TYPE")
	private String docType;

	/** 关联业务流水 **/
	@Column(name = "BIZ_SERNO")
	private String bizSerno;

	/** 客户号 **/
	@Column(name = "CUS_ID")
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME")
	private String cusName;

	/** 延期归还日期 **/
	@Column(name = "DELAY_BACK_DATE")
	private String delayBackDate;

	public String getDrdiSerno() {
		return drdiSerno;
	}

	public void setDrdiSerno(String drdiSerno) {
		this.drdiSerno = drdiSerno;
	}

	/**
	 * @param draiSerno
	 */
	public void setDraiSerno(String draiSerno) {
		this.draiSerno = draiSerno;
	}
	
    /**
     * @return draiSerno
     */
	public String getDraiSerno() {
		return this.draiSerno;
	}
	
	/**
	 * @param readMode
	 */
	public void setReadMode(String readMode) {
		this.readMode = readMode;
	}
	
    /**
     * @return readMode
     */
	public String getReadMode() {
		return this.readMode;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}
	
    /**
     * @return backDate
     */
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param readRqstrId
	 */
	public void setReadRqstrId(String readRqstrId) {
		this.readRqstrId = readRqstrId;
	}
	
    /**
     * @return readRqstrId
     */
	public String getReadRqstrId() {
		return this.readRqstrId;
	}
	
	/**
	 * @param readRqstrOrg
	 */
	public void setReadRqstrOrg(String readRqstrOrg) {
		this.readRqstrOrg = readRqstrOrg;
	}
	
    /**
     * @return readRqstrOrg
     */
	public String getReadRqstrOrg() {
		return this.readRqstrOrg;
	}
	
	/**
	 * @param readRqstrDate
	 */
	public void setReadRqstrDate(String readRqstrDate) {
		this.readRqstrDate = readRqstrDate;
	}
	
    /**
     * @return readRqstrDate
     */
	public String getReadRqstrDate() {
		return this.readRqstrDate;
	}
	
	/**
	 * @param readReason
	 */
	public void setReadReason(String readReason) {
		this.readReason = readReason;
	}
	
    /**
     * @return readReason
     */
	public String getReadReason() {
		return this.readReason;
	}
	
	/**
	 * @param reasonMome
	 */
	public void setReasonMome(String reasonMome) {
		this.reasonMome = reasonMome;
	}
	
    /**
     * @return reasonMome
     */
	public String getReasonMome() {
		return this.reasonMome;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getDocClass() {
		return docClass;
	}

	public void setDocClass(String docClass) {
		this.docClass = docClass;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getDocStauts() {
		return docStauts;
	}

	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts;
	}

	public String getReadType() {
		return readType;
	}

	public void setReadType(String readType) {
		this.readType = readType;
	}

	public String getDelayBackDate() {
		return delayBackDate;
	}

	public void setDelayBackDate(String delayBackDate) {
		this.delayBackDate = delayBackDate;
	}
}