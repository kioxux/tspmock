package cn.com.yusys.yusp.service.server.xdtz0025;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0025.req.Xdtz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.resp.Xdtz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 接口处理类:查询指定贷款开始日的优企贷客户贷款余额合计
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0025Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0025Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 交易码：Xdtz0025
     * 交易描述：查询指定贷款开始日的优企贷客户贷款余额合计
     *
     * @param xdtz0025DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0025DataRespDto xdtz0025(Xdtz0025DataReqDto xdtz0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataReqDto));
        //定义返回信息
        Xdtz0025DataRespDto xdtz0025DataRespDto = new Xdtz0025DataRespDto();
        try {
            //请求字段
            String loanStartDate = xdtz0025DataReqDto.getLoanStartDate();
            logger.info("*********XDTZ0025*查询台账信息开始,查询参数为:{}", JSON.toJSONString(loanStartDate));
            java.util.List<cn.com.yusys.yusp.dto.server.xdtz0025.resp.List> list = accLoanMapper.queryLoanBalanceByLoanStartDate(loanStartDate);
            logger.info("*********XDTZ0025*查询台账信息结束,返回参数为:{}", JSON.toJSONString(list));
            //返回信息
            xdtz0025DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataRespDto));
        return xdtz0025DataRespDto;
    }
}
