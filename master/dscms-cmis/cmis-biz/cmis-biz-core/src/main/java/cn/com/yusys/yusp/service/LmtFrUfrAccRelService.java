/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtFrUfrAccRel;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtFrUfrAccRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrAccRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:39:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtFrUfrAccRelService {
    private static final Logger log = LoggerFactory.getLogger(LmtFrUfrAccRelService.class);

    @Autowired
    private LmtFrUfrAccRelMapper lmtFrUfrAccRelMapper;
    @Autowired
    private LmtFrUfrAppService lmtFrUfrAppService;
    
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtFrUfrAccRel selectByPrimaryKey(String pkId) {
        return lmtFrUfrAccRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtFrUfrAccRel> selectAll(QueryModel model) {
        List<LmtFrUfrAccRel> records = (List<LmtFrUfrAccRel>) lmtFrUfrAccRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtFrUfrAccRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtFrUfrAccRel> list = lmtFrUfrAccRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtFrUfrAccRel record) {
        return lmtFrUfrAccRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtFrUfrAccRel record) {
        return lmtFrUfrAccRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtFrUfrAccRel record) {
        return lmtFrUfrAccRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtFrUfrAccRel record) {
        return lmtFrUfrAccRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtFrUfrAccRelMapper.deleteByPrimaryKey(pkId);
    }
    
    /**
     * @方法名称: querySelectLmtFrUfrAccRel
     * @方法描述: 根据入参查询冻结/解冻额度台账关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int querySelectLmtFrUfrAccRel(Map params)throws Exception {
        int rtnDta =0;
        String lmtLimitNo =(String)params.get("lmtLimitNo");
        String frUfrSerno=(String)params.get("frUfrSerno");
        String lmtCtrNo=(String)params.get("lmtCtrNo");
        String opeType=(String)params.get("opeType");
        log.info("解冻额度台账查询：" +lmtLimitNo+"----start----");
        Map lmtMap = new HashMap();
        lmtMap.put("lmtLimitNo",lmtLimitNo);
        LmtFrUfrAccRel lmtFrUfrAccRel= lmtFrUfrAccRelMapper.selectByParamsKey(lmtLimitNo,frUfrSerno);
        if(lmtFrUfrAccRel != null){
            throw new YuspException(EcbEnum.LMT_FR_URF_APP_REL_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_REL_EXCEPTION.value);
        }
        //该笔额度冻结/解冻申请，授信协议项下的额度台账未引用
        //判断该额度编号是否在流程审批中
        // 1.审批通过，台账已冻结，申请冻结不可引用;申请解冻可引用
        // 2.审批通过，台账未冻结或已解冻可以引用
        //2.审批中，该台账不可引用
        LmtFrUfrAccRel lmtFrUfrAccRelStr = new LmtFrUfrAccRel();
        lmtFrUfrAccRelStr.setPkId(StringUtils.uuid(true));
        lmtFrUfrAccRelStr.setLmtCtrNo(lmtCtrNo);
        lmtFrUfrAccRelStr.setFrUfrSerno(frUfrSerno);
        lmtFrUfrAccRelStr.setLmtLimitNo(lmtLimitNo);
        List<Map> lmtFrUfrAccRelList = lmtFrUfrAccRelMapper.selectByParamsMap(lmtMap);
        if (CollectionUtils.isEmpty(lmtFrUfrAccRelList) || lmtFrUfrAccRelList.size() == 0) {
            log.info("当前额度台账未引用" + lmtLimitNo);
            rtnDta = lmtFrUfrAccRelMapper.insert(lmtFrUfrAccRelStr);
        }
        for (Map lmtFrUfrAccRelMap : lmtFrUfrAccRelList) {
            String approveStatus = (String) lmtFrUfrAccRelMap.get("approveStatus");
            String appType = (String) lmtFrUfrAccRelMap.get("appType");
            if (approveStatus.equals(CmisCommonConstants.WF_STATUS_997)) {
                if (appType.equals(CmisLmtConstants.LMT_APP_TYPE_01) && opeType.equals(CmisLmtConstants.LMT_OPETYPE_FROZE)) {
                    throw new YuspException(EcbEnum.LMT_FR_URF_APP_APPTYPE_FROZE_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_APPTYPE_FROZE_EXCEPTION.value);
                }
                if ((appType.equals(CmisLmtConstants.LMT_APP_TYPE_01) && opeType.equals(CmisLmtConstants.LMT_OPETYPE_UNFREEZE)) || (appType.equals(CmisLmtConstants.LMT_APP_TYPE_02) && opeType.equals(CmisLmtConstants.LMT_OPETYPE_FROZE))) {
                    rtnDta = lmtFrUfrAccRelMapper.insert(lmtFrUfrAccRelStr);
                }
                if (appType.equals(CmisLmtConstants.LMT_APP_TYPE_02) && opeType.equals(CmisLmtConstants.LMT_OPETYPE_UNFREEZE)) {
                    throw new YuspException(EcbEnum.LMT_FR_URF_APP_APPTYPE_UNFROZE_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_APPTYPE_UNFROZE_EXCEPTION.value);
                }
            } else {
                throw new YuspException(EcbEnum.LMT_FR_URF_APP_REL_AFTEREND_EXCEPTION.key, EcbEnum.LMT_FR_URF_APP_REL_AFTEREND_EXCEPTION.value);
            }
        }
        if(rtnDta == 0){
            throw new YuspException(EcbEnum.UPDATE_LMT_FR_URF_APP_REL_EXCEPTION.key, EcbEnum.UPDATE_LMT_FR_URF_APP_REL_EXCEPTION.value);
        }
        return rtnDta;
    }
    
    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtFrUfrAccRelMapper.deleteByIds(ids);
    }
    
    /**
     * @方法名称: selectByParamsKey
     * @方法描述: 根据入参查询额度台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    public LmtFrUfrAccRel selectByParamsKey(String lmtLimitNo, String frUfrSerno) {
        return lmtFrUfrAccRelMapper.selectByParamsKey(lmtLimitNo,frUfrSerno);
    }
    
    /**
     * @方法名称: selectByFrUfrSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtFrUfrAccRel> selectByFrUfrSerno(String frUfrSerno) {
        return lmtFrUfrAccRelMapper.selectByFrUfrSerno(frUfrSerno);
    }
    
    /**
     * @方法名称: selectByLmtMap
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    public List<LmtFrUfrAccRel> selectByLmtMap(Map lmtMap) {
        return lmtFrUfrAccRelMapper.selectByLmtMap(lmtMap);
    }
    
}
