/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperProductionOper;
import cn.com.yusys.yusp.service.RptOperProductionOperService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperProductionOperResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:03:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperproductionoper")
public class RptOperProductionOperResource {
    @Autowired
    private RptOperProductionOperService rptOperProductionOperService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperProductionOper>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperProductionOper> list = rptOperProductionOperService.selectAll(queryModel);
        return new ResultDto<List<RptOperProductionOper>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperProductionOper>> index(QueryModel queryModel) {
        List<RptOperProductionOper> list = rptOperProductionOperService.selectByModel(queryModel);
        return new ResultDto<List<RptOperProductionOper>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptOperProductionOper> show(@PathVariable("serno") String serno) {
        RptOperProductionOper rptOperProductionOper = rptOperProductionOperService.selectByPrimaryKey(serno);
        return new ResultDto<RptOperProductionOper>(rptOperProductionOper);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperProductionOper> create(@RequestBody RptOperProductionOper rptOperProductionOper) throws URISyntaxException {
        rptOperProductionOperService.insert(rptOperProductionOper);
        return new ResultDto<RptOperProductionOper>(rptOperProductionOper);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperProductionOper rptOperProductionOper) throws URISyntaxException {
        int result = rptOperProductionOperService.update(rptOperProductionOper);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptOperProductionOperService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperProductionOperService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptOperProductionOper> selectBySerno(@RequestBody Map<String,Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptOperProductionOper>(rptOperProductionOperService.selectByPrimaryKey(serno));
    }

    @PostMapping("/updateProductionOper")
    protected ResultDto<Integer> updateProductionOper(@RequestBody RptOperProductionOper rptOperProductionOper){
        return new ResultDto<Integer>(rptOperProductionOperService.updateProductionOper(rptOperProductionOper));
    }
}
