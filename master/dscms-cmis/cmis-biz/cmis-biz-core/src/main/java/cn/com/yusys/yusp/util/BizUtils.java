package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantManageAppMapper;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务流程服务工具类
 */
@Component
public class BizUtils {
    private static final Logger log = LoggerFactory.getLogger(BizUtils.class);

    public BizUtils() {
    }

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;//引入cfg提供的接口服务
    //    @Autowired
//    private UserServiceCacheImpl userService;//注入通用的用户服务接口
    // @Autowired
    // private IGuarClientService iGuarClientService;//押品服务
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private LmtSubService lmtSubService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;

    @Autowired
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private Dscms2YpqzxtClientService dscms2YpqzxtClientService;

    private IqpLoanAppService iqpLoanAppService;
    private static BizUtils bizUtils;


    @PostConstruct
    public void init() {
        bizUtils = this;
        bizUtils.iCmisCfgClientService = this.iCmisCfgClientService;
//        bizUtils.userService = this.userService;
    }

    /**
     * 驼峰命名转为下划线命名
     *
     * @param para 驼峰命名的字符串
     */
    public static String humpToUnderline(String para) {
        String firstLetter = para.substring(0, 1);
        para = firstLetter.toLowerCase() + para.substring(1, para.length());
        StringBuilder sb = new StringBuilder(para);
        int temp = 0;//定位
        if (!para.contains(CmisCommonConstants.COMMON_SPLIT_UNDERLINE)) {
            for (int i = 0; i < para.length(); i++) {
                if (Character.isUpperCase(para.charAt(i))) {
                    sb.insert(i + temp, CmisCommonConstants.COMMON_SPLIT_UNDERLINE);
                    temp += 1;
                }
            }
        }
        return sb.toString().toLowerCase();
    }

    /***
     * 下划线命名转为驼峰命名
     * @param para 下划线命名的字符串
     */
    public static String underlineToHump(String para) {
        StringBuilder result = new StringBuilder();
        String a[] = para.split(CmisCommonConstants.COMMON_SPLIT_UNDERLINE);
        for (String s : a) {
            if (!para.contains(CmisCommonConstants.COMMON_SPLIT_UNDERLINE)) {
                result.append(s);
                continue;
            }
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            } else {
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

    /**
     * 通过cfg接口服务，将源表数据映射到目标表中,针对相同服务下，可获取源对象以及目标对象
     *
     * @param sourceObject
     * @param distObject
     * @return
     */
    public Object getMappingValueBySourceAndDisTable(Object sourceObject, Object distObject) {
        //首先通过类名获取表名
        String sourClassName = sourceObject.getClass().getSimpleName();
        String distClassName = distObject.getClass().getSimpleName();
        log.info("映射处理的源表类名为：" + sourClassName + "，目标表的类名为：" + distClassName);
        Map sourceMap = JSONObject.parseObject(JSON.toJSONString(sourceObject, SerializerFeature.WriteMapNullValue), Map.class);
        log.info("映射处理-转化源表实体为map集合，信息为：" + sourceMap.toString());
        Map distMap = JSONObject.parseObject(JSON.toJSONString(distObject, SerializerFeature.WriteMapNullValue), Map.class);
        log.info("映射处理-转化源表实体为map集合，信息为：" + distMap.toString());

        String sourTableName = BizUtils.humpToUnderline(sourClassName);
        String disTableName = BizUtils.humpToUnderline(distClassName);

        Map returnMap = getDisServiceMappingValueBySourceAndDisTable(sourTableName, disTableName, sourceMap, distMap);
        if (CollectionUtils.isEmpty(returnMap)) {
            throw new YuspException(EcbEnum.E_GETMAPPING_EXCEPTION.key, EcbEnum.E_GETMAPPING_EXCEPTION.value);
        }

        log.info("映射处理-将转化后的目标表map集合转化为目标对象");
        return JSONObject.parseObject(JSON.toJSONString(returnMap), distObject.getClass());
    }

    /**
     * 通过cfg接口服务，将源表数据映射到目标表中,不同服务间的映射，只能通过表名获取映射对象
     *
     * @param sourceTableName
     * @param distTableName
     * @return
     */
    public Map getDisServiceMappingValueBySourceAndDisTable(String sourceTableName, String distTableName, Map sourceMap, Map distMap) {
        //首先通过类名获取表名
        log.info("映射处理-调用cmis-cfg服务接口获取映射关系-开始，源表：" + sourceTableName + "，目标表：" + distTableName);
        CfgToBizFlowDataDto cfgToBizFlowDataDto = new CfgToBizFlowDataDto();
        cfgToBizFlowDataDto.setSourceTableName(sourceTableName);//源表
        cfgToBizFlowDataDto.setDistTableName(distTableName);//目标表
        cfgToBizFlowDataDto.setSourceMap(sourceMap);//源表数据集合
        cfgToBizFlowDataDto.setDistMap(distMap);//目标表数据集合

        try {
            cfgToBizFlowDataDto = bizUtils.iCmisCfgClientService.queryCfgDataFlowInfo4Out(cfgToBizFlowDataDto);
        } catch (Exception e) {
            log.error("调用cmif-cfg接口获取参数异常，异常原因：" + e.getMessage(), e);
            throw new YuspException(EcbEnum.COMMON_EXCEPTION_DEF.key, "调用接口异常！");
        }

        if (cfgToBizFlowDataDto == null) {
            throw new YuspException(EcbEnum.E_GETMAPPING_EXCEPTION.key, EcbEnum.E_GETMAPPING_EXCEPTION.value);
        }
        log.info("映射处理-调用cmis-cfg服务接口获取映射关系-结束，返回信息为：" + JSONObject.parseObject(JSON.toJSONString(cfgToBizFlowDataDto), Map.class).toString());
        String rtnCode = cfgToBizFlowDataDto.getRtnCode();
        //返回成功标志位不为成功的场景，直接抛出异常
        if (!CmisCommonConstants.INTERFACE_SUCCESS_CODE.equals(rtnCode)) {
            throw new YuspException(rtnCode, cfgToBizFlowDataDto.getRtnMsg());
        }

        log.info("映射处理-获取映射完成的数据集合");
        Map returnMap = cfgToBizFlowDataDto.getReturnMap();
        if (CollectionUtils.isEmpty(returnMap)) {
            throw new YuspException(EcbEnum.E_GETMAPPING_EXCEPTION.key, EcbEnum.E_GETMAPPING_EXCEPTION.value);
        }

        return returnMap;
    }

    /**
     * 通过cfg接口服务，将源表数据list映射到目标表list中
     *
     * @param sourceTableName
     * @param distTableName
     * @param sourceMapList
     * @return
     */
    public List<Map> getMappingValueBySourceAndDisTable4List(String sourceTableName, String distTableName, List<Map> sourceMapList) {
        //将源表数据集合处理为map数据集合
        if (CollectionUtils.isEmpty(sourceMapList)) {
            throw new YuspException(EcbEnum.E_GETMAPPING_SOURCELISTDATANULL_EXCEPTION.key, EcbEnum.E_GETMAPPING_SOURCELISTDATANULL_EXCEPTION.value);
        }

        log.info("映射处理-调用cmis-cfg服务接口获取映射关系-开始，源表：" + sourceTableName + "，目标表：" + distTableName);
        CfgToBizFlowDataDto cfgToBizFlowDataDto = new CfgToBizFlowDataDto();
        cfgToBizFlowDataDto.setSourceTableName(sourceTableName);//源表
        cfgToBizFlowDataDto.setDistTableName(distTableName);//目标表
        cfgToBizFlowDataDto.setSourObjList(sourceMapList);//源表数据集合

        try {
            cfgToBizFlowDataDto = bizUtils.iCmisCfgClientService.queryCfgDataFlowInfo4Out4List(cfgToBizFlowDataDto);
        } catch (Exception e) {
            log.error("调用cmif-cfg接口获取参数异常，异常原因：" + e.getMessage(), e);
            throw new YuspException(EcbEnum.COMMON_EXCEPTION_DEF.key, "调用接口异常！");
        }

        if (cfgToBizFlowDataDto == null) {
            throw new YuspException(EcbEnum.E_GETMAPPING_EXCEPTION.key, EcbEnum.E_GETMAPPING_EXCEPTION.value);
        }
        log.info("映射处理-调用cmis-cfg服务接口获取映射关系-结束，返回信息为：" + JSONObject.parseObject(JSON.toJSONString(cfgToBizFlowDataDto), Map.class).toString());

        String rtnCode = cfgToBizFlowDataDto.getRtnCode();
        //返回成功标志位不为成功的场景，直接抛出异常
        if (!CmisCommonConstants.INTERFACE_SUCCESS_CODE.equals(rtnCode)) {
            throw new YuspException(rtnCode, cfgToBizFlowDataDto.getRtnMsg());
        }

        log.info("映射处理-获取映射完成的数据list集合");
        List<Map> rtnMapList = cfgToBizFlowDataDto.getRtnObjList();
        if (CollectionUtils.isEmpty(rtnMapList)) {
            throw new YuspException(EcbEnum.E_GETMAPPING_DISTMAPLISTNULL_EXCEPTION.key, EcbEnum.E_GETMAPPING_DISTMAPLISTNULL_EXCEPTION.value);
        }
        log.info("映射处理-将转化后的目标表map集合转化为目标对象");
        return rtnMapList;
    }

    /**
     * 调用cfg服务获取参数信息配置
     *
     * @return
     */
    public Map getCfgParamFromCfgClient(Map queryMap) {
        Map cfgParamMap = new HashMap();

        if (CollectionUtils.isEmpty(queryMap)) {
            throw new YuspException(EcbEnum.E_GETCFGPARAM_PARAMS_EXPCETION.key, EcbEnum.E_GETCFGPARAM_PARAMS_EXPCETION.value);
        }

        log.info("调用cmif-cfg接口获取参数-入参信息为：" + JSON.toJSONString(queryMap));
        CfgClientParamDto cfgClientParamDto = new CfgClientParamDto();
        CfgBizParamInfoClientDto cfgBizParamInfoClientDto = new CfgBizParamInfoClientDto();
        cfgBizParamInfoClientDto = JSONObject.parseObject(JSON.toJSONString(queryMap), CfgBizParamInfoClientDto.class);
        cfgClientParamDto.setCfgBizParamInfoClientDto(cfgBizParamInfoClientDto);
        try {
            cfgClientParamDto = bizUtils.iCmisCfgClientService.getCfgClientParam(cfgClientParamDto);
        } catch (Exception e) {
            log.error("调用cmif-cfg接口获取参数异常，异常原因：" + e.getMessage(), e);
            throw new YuspException(EcbEnum.COMMON_EXCEPTION_DEF.key, "调用接口异常！");
        }

        if (cfgClientParamDto == null) {
            throw new YuspException(EcbEnum.E_CLIENTRTNNULL_EXPCETION.key, EcbEnum.E_CLIENTRTNNULL_EXPCETION.value);
        }

        cfgBizParamInfoClientDto = cfgClientParamDto.getCfgBizParamInfoClientDto();
        if (cfgBizParamInfoClientDto == null) {
            throw new YuspException(EcbEnum.E_GETCFGPARAM_RTNPARAMSNULL_EXPCETION.key, EcbEnum.E_GETCFGPARAM_RTNPARAMSNULL_EXPCETION.value);
        }
        cfgParamMap = JSONObject.parseObject(JSON.toJSONString(cfgBizParamInfoClientDto, SerializerFeature.WriteMapNullValue), Map.class);

        return cfgParamMap;
    }

    /**
     * 获取当前业务引用的押品编号
     *
     * @param
     * @return
     */
    public Map queryIqpGuarBizRelApp(String iqpSerno) {

        Map params = new HashMap();
        log.info("获取担保和业务关系数据-查询开始");
        Map map = new HashMap();
        map.put("iqpSernos", iqpSerno);
        List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectByParams(map);
        if (CollectionUtils.isEmpty(iqpGuarBizRelAppList) || iqpGuarBizRelAppList.size() == 0) {
            log.info("获取担保和业务关系数据-获取list数据为空");
            return params;
        }
        String guarContNo = "";
        map.remove("iqpSernos", iqpSerno);
        for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
            guarContNo = guarContNo + iqpGuarBizRelApp.getGuarContNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }

        if (StringUtils.nonBlank(guarContNo)) {
            guarContNo = guarContNo.substring(0, guarContNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }
        map.put("guarContNo", guarContNo);
        //获取该业务申请项下所有的押品编号
        List<GrtGuarContRel> guarContRelList = grtGuarContRelService.selectGrtGuarContRelByGuarNo(map);
        if (CollectionUtils.isEmpty(guarContRelList)) {
            log.info("查询担保合同与押品关系数据-获取押品异常");
        }

        List<GuarBaseInfoClientDto> list = new ArrayList<>();
        for (GrtGuarContRel guarContRel : guarContRelList) {
            String guarNo = guarContRel.getGuarNo();
            map.remove("guarContNo", guarContNo);
            map.put("guarNo", guarNo);
            //单笔押品项下被引用担保合同
            List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectGrtGuarContRelByGuarNo(map);
            if (CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0) {
                log.info("查询担保合同与押品关系数据-获取押品异常");
            }
            guarContNo = "";
            for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
                guarContNo = guarContNo + grtGuarContRel.getGuarContNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
            }
            if (StringUtils.nonBlank(guarContNo)) {
                guarContNo = guarContNo.substring(0, guarContNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
            }
            Map guarMap = queryBizServiceStates(guarContNo);
            if (guarMap != null) {
                String guarBusistate = (String) guarMap.get("guarBusistate");
                GuarBaseInfoClientDto guarBaseInfoClientDto = new GuarBaseInfoClientDto();
                guarBaseInfoClientDto.setGuarNo(guarNo);
                guarBaseInfoClientDto.setGuarNo(guarBusistate);
                list.add(guarBaseInfoClientDto);
            }
        }
        GuarBaseInfoClientDto guarBaseInfoClientDtos = new GuarBaseInfoClientDto();
        guarBaseInfoClientDtos.setList(list);
        params = updateGuarBaseInfoByGuarNo(guarBaseInfoClientDtos);
        if (params == null) {
            log.info("业务申请流程审批更新押品所在业务阶段-接口返回为空");
        }
        return params;
    }

    public BizGuarExchangeDto getGuarBusistate(BizGuarExchangeDto bizGuarExchangeDto) {
        String guarNo = bizGuarExchangeDto.getGuarNo();
        Map map = new HashMap();
        map.put("guarNo", guarNo);
        BizGuarExchangeDto bizGuarExchangeDtos = new BizGuarExchangeDto();
        List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectGrtGuarContRelByGuarNo(map);
        if (CollectionUtils.isEmpty(grtGuarContRelList) || grtGuarContRelList.size() == 0) {
            log.info("查询担保合同与押品关系数据-获取押品异常");
            bizGuarExchangeDtos.setRtnCode(CmisCommonConstants.INTERFACE_FAIL_CODE);
            bizGuarExchangeDtos.setRtnMsg("担保合同与押品关系数据不存在");
            return bizGuarExchangeDtos;
        }
        String guarContNo = "";
        for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
            guarContNo = guarContNo + grtGuarContRel.getGuarContNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
        }
        if (StringUtils.nonBlank(guarContNo)) {
            guarContNo = guarContNo.substring(0, guarContNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }
        Map guarMap = queryBizServiceStates(guarContNo);
        if (guarMap != null) {
            bizGuarExchangeDtos.setRtnCode(CmisCommonConstants.INTERFACE_SUCCESS_CODE);
            String guarBusistate = (String) guarMap.get("guarBusistate");
            bizGuarExchangeDtos.setGuarNo(guarNo);
            bizGuarExchangeDtos.setGuarBusistate(guarBusistate);
        }
        return bizGuarExchangeDtos;
    }

    /**
     * 判断押品所在业务状态
     *
     * @param
     * @return
     */
    public Map queryBizServiceStates(String guarContNo) {
        Map guarMap = new HashMap();
        //单笔押品项下的多笔担保合同编号与业务或授信关联的担保
        guarMap.put("guarContNo", guarContNo);
        guarMap.put("unSecure", true);
        guarMap.put("unContNo", true);
        //查询担保结果表非解除状态且存在合同的数据
        List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectByParams(guarMap);
        if (CollectionUtils.isEmpty(iqpGuarBizRelAppList)) {
            log.info("担保合同编号" + guarContNo + "不存在合同");
            //判断是否在流程审批中
            guarMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpGuarBizRelApp> list = iqpGuarBizRelAppService.selectByParams(guarMap);
            if (CollectionUtils.isEmpty(list)) {
                log.info("担保合同编号" + guarContNo + "未关联授信或业务-押品与业务所在阶段为01-已创建");
                guarMap.put("guarBusistate", CmisCommonConstants.GUAR_BUSISTATE_01);
                return guarMap;
            }
            String iqpSerno = "";
            for (IqpGuarBizRelApp iqpGuarBizRelApp : list) {
                iqpSerno = iqpSerno + iqpGuarBizRelApp.getIqpSerno() + CmisCommonConstants.COMMON_SPLIT_COMMA;
            }
            if (StringUtils.nonBlank(iqpSerno)) {
                iqpSerno = iqpSerno.substring(0, iqpSerno.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
            }
            guarMap.put("iqpSerno", iqpSerno);
            List<IqpLoanApp> iqpLoanAppList = iqpLoanAppService.selectIqpLoanAppListByParams(guarMap);
            if (CollectionUtils.isEmpty(iqpLoanAppList)) {
                log.info("业务申请" + iqpSerno + "业务申请不存在-是否与授信关联");
                guarMap = queryLmtInfo(iqpSerno);
            } else {
                log.info("业务申请" + iqpSerno + "业务待发起或审批中，押品与业务所在阶段为04-业务审批中");
                guarMap.put("guarBusistate", CmisCommonConstants.GUAR_BUSISTATE_04);
                return guarMap;
            }
        }
        String contNo = "";
        for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(iqpGuarBizRelApp.getContNo());
            if (CmisCommonConstants.CONT_STATUS_200.equals(ctrLoanCont.getContStatus())) {
                contNo = contNo + iqpGuarBizRelApp.getGuarContNo() + CmisCommonConstants.COMMON_SPLIT_COMMA;
            }
            //押品重置时，可能存在未生效的合同
            if (CmisCommonConstants.CONT_STATUS_100.equals(ctrLoanCont.getContStatus())) {
                log.info("合同编号" + contNo + "存在未签到的合同-押品所在业务阶段05-与业务关联");
                guarMap.put("guarBusistate", CmisCommonConstants.GUAR_BUSISTATE_05);
                return guarMap;
            }
        }
        if (StringUtils.nonBlank(contNo)) {
            contNo = contNo.substring(0, contNo.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
        }

        guarMap.put("contNo", contNo);
        List<AccLoan> accLoanList = accLoanService.selectAccLoanByContNo(guarMap);
        if (CollectionUtils.isEmpty(accLoanList) || accLoanList.size() == 0) {
            log.info("合同编号" + contNo + "未发生放款-押品所在业务阶段05-与业务关联");
            guarMap.put("guarBusistate", CmisCommonConstants.GUAR_BUSISTATE_05);
            return guarMap;
        } else {
            log.info("合同编号" + contNo + "已发生放款押品所在业务阶段06-放款阶段");
            guarMap.put("guarBusistate", CmisCommonConstants.GUAR_BUSISTATE_06);
            return guarMap;
        }
    }

    /**
     * 调用lmt接口获取数据
     *
     * @param iqpSerno
     * @return
     */
    public Map queryLmtInfo(String iqpSerno) {
        Map guarMap = new HashMap();
        return guarMap;
    }

    /**
     * 调用押品服务端更新押品所在业务阶段
     *
     * @param
     * @return
     */
    public Map updateGuarBaseInfoByGuarNo(GuarBaseInfoClientDto guarBaseInfoClientDto) {
        Map params = new HashMap();
        log.info("接口请求入参" + JSON.toJSONString(guarBaseInfoClientDto));
        log.info("业务申请流程审批更新押品所在业务阶段" + guarBaseInfoClientDto + "调用押品端接口开始");
        GuarClientRsDto guarClientRsDto = null;// iGuarClientService.updateGuarBusistate(guarBaseInfoClientDto);
        log.info("业务申请流程审批更新押品所在业务阶段" + guarBaseInfoClientDto + "调用押品端接口结束");

        if (guarClientRsDto == null) {
            log.info("业务申请流程审批更新押品所在业务阶段-接口返回为空");
            return params;
        }
        params = JSONObject.parseObject(JSON.toJSONString(guarClientRsDto, SerializerFeature.WriteMapNullValue), Map.class);
        return params;
    }

    /*****
     * 核心系统与信贷币种码值转换
     * *********/
    public static String changCurType(String curType) {
        String coreCurType = "01";//人民币
        if ("CNY".equals(curType)) {
            coreCurType = "01";//人民币
        } else if ("GBP".equals(curType)) {
            coreCurType = "12";//英镑
        } else if ("HKD".equals(curType)) {
            coreCurType = "13";//港元
        } else if ("USD".equals(curType)) {
            coreCurType = "14";//美元
        } else if ("CHF".equals(curType)) {
            coreCurType = "15";//瑞士法郎
        } else if ("JPY".equals(curType)) {
            coreCurType = "27";//日元
        } else if ("EUR".equals(curType)) {
            coreCurType = "38";//欧元
        } else if ("CAD".equals(curType)) {
            coreCurType = "28";//加元
        } else if ("AUD".equals(curType)) {
            coreCurType = "29";//澳大利亚元
        } else if ("SEK".equals(curType)) {
            coreCurType = "21";//瑞典克郎
        }
        return coreCurType;
    }
}


