/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.RptBasicInfoAsso;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.CusGrpDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasicInfoRelatedGrpCustomers;
import cn.com.yusys.yusp.repository.mapper.RptBasicInfoRelatedGrpCustomersMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRelatedGrpCustomersService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:30:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicInfoRelatedGrpCustomersService {

    @Autowired
    private RptBasicInfoRelatedGrpCustomersMapper rptBasicInfoRelatedGrpCustomersMapper;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptBasicInfoRelatedGrpCustomers selectByPrimaryKey(String serno) {
        return rptBasicInfoRelatedGrpCustomersMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptBasicInfoRelatedGrpCustomers> selectAll(QueryModel model) {
        List<RptBasicInfoRelatedGrpCustomers> records = (List<RptBasicInfoRelatedGrpCustomers>) rptBasicInfoRelatedGrpCustomersMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptBasicInfoRelatedGrpCustomers> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasicInfoRelatedGrpCustomers> list = rptBasicInfoRelatedGrpCustomersMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptBasicInfoRelatedGrpCustomers record) {
        return rptBasicInfoRelatedGrpCustomersMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptBasicInfoRelatedGrpCustomers record) {
        return rptBasicInfoRelatedGrpCustomersMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptBasicInfoRelatedGrpCustomers record) {
        return rptBasicInfoRelatedGrpCustomersMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptBasicInfoRelatedGrpCustomers record) {
        return rptBasicInfoRelatedGrpCustomersMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptBasicInfoRelatedGrpCustomersMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicInfoRelatedGrpCustomersMapper.deleteByIds(ids);
    }

    /**
     * @param params
     * @return
     */
    public int initGrpAsso(Map params) {
        String grpSerno = params.get("grpSerno").toString();
        String grpNo = params.get("grpNo").toString();
        int count = 0;
        ResultDto<CusGrpDto> cusGrpDtoResultDto = iCusClientService.selectCusGrpDtoByGrpNo(grpNo);
        RptBasicInfoRelatedGrpCustomers rptBasicInfoRelatedGrpCustomers = new RptBasicInfoRelatedGrpCustomers();
        if (cusGrpDtoResultDto != null && cusGrpDtoResultDto.getData() != null) {
            //获取核心客户号
            String coreCusId = cusGrpDtoResultDto.getData().getCoreCusId();
            if (StringUtils.nonBlank(coreCusId)) {
                ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(coreCusId);
                if (cusCorpDtoResultDto != null && cusCorpDtoResultDto.getData() != null) {
                    CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
                    BeanUtils.beanCopy(cusCorpDto, rptBasicInfoRelatedGrpCustomers);
                    //集团全称
                    rptBasicInfoRelatedGrpCustomers.setGrpNameTotal(cusCorpDto.getCusName());
                    //所涉行业
                    rptBasicInfoRelatedGrpCustomers.setOperTrade(cusCorpDto.getTradeClass());
                    //注册地址
                    rptBasicInfoRelatedGrpCustomers.setComRegAdd(cusCorpDto.getRegiAddr());
                }
                //获取实际控制人
                Map<String, Object> map1 = new HashMap<>();
                map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
                map1.put("cusIdRel", coreCusId);
                ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType = cmisCusClientService.getCusCorpMgrByParams(map1);
                if (cusCorpMgrByMrgType != null && cusCorpMgrByMrgType.getData() != null) {
                    String realOperCusId = cusCorpMgrByMrgType.getData().getMrgName();
                    rptBasicInfoRelatedGrpCustomers.setRealOperCusId(realOperCusId);
                }
                //获取法人代表
                Map<String, Object> map2 = new HashMap<>();
                map2.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
                map2.put("cusIdRel", coreCusId);
                ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType1 = cmisCusClientService.getCusCorpMgrByParams(map2);
                if (cusCorpMgrByMrgType1 != null && cusCorpMgrByMrgType1.getData() != null) {
                    String legal = cusCorpMgrByMrgType1.getData().getMrgName();
                    rptBasicInfoRelatedGrpCustomers.setLegal(legal);
                }
                //主体评级
                ResultDto<Map<String, String>> mapResultDto =  iCusClientService.selectGradeInfoByCusId(coreCusId);
                if (mapResultDto != null && mapResultDto.getData() != null) {
                    String finalRank = mapResultDto.getData().get("finalRank");
                    rptBasicInfoRelatedGrpCustomers.setSubjectLevel(finalRank);
                }
            }
        }
        rptBasicInfoRelatedGrpCustomers.setSerno(grpSerno);
        count = rptBasicInfoRelatedGrpCustomersMapper.insertSelective(rptBasicInfoRelatedGrpCustomers);
        return count;
    }
}
