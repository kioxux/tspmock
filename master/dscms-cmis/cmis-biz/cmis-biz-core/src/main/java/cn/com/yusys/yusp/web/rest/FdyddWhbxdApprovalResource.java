/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FdyddWhbxdApproval;
import cn.com.yusys.yusp.dto.SfResultInfoDto;
import cn.com.yusys.yusp.service.FdyddWhbxdApprovalService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FdyddWhbxdApprovalResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-06 15:47:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/fdyddwhbxdapproval")
public class FdyddWhbxdApprovalResource {
    @Autowired
    private FdyddWhbxdApprovalService fdyddWhbxdApprovalService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FdyddWhbxdApproval>> query() {
        QueryModel queryModel = new QueryModel();
        List<FdyddWhbxdApproval> list = fdyddWhbxdApprovalService.selectAll(queryModel);
        return new ResultDto<List<FdyddWhbxdApproval>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FdyddWhbxdApproval>> index(QueryModel queryModel) {
        List<FdyddWhbxdApproval> list = fdyddWhbxdApprovalService.selectByModel(queryModel);
        return new ResultDto<List<FdyddWhbxdApproval>>(list);
    }

    /**
     * @函数名称:fdyddWhbxdApprovallist
     * @函数描述: 获取申请信息房抵e点贷无还本续贷申请
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取申请信息无还本续贷申请")
    @PostMapping("/fdyddWhbxdApprovallist")
    protected ResultDto<List<FdyddWhbxdApproval>> fdyddWhbxdApprovallist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<FdyddWhbxdApproval> list = fdyddWhbxdApprovalService.fdyddWhbxdApprovallist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<FdyddWhbxdApproval>>(list);
    }

    /**
     * @函数名称:fdyddWhbxdApprovalHislist
     * @函数描述: 获取申请历史房抵e点贷无还本续贷申请
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取申请历史押品查询查封")
    @PostMapping("/fdyddWhbxdApprovalHislist")
    protected ResultDto<List<FdyddWhbxdApproval>> fdyddWhbxdApprovalHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<FdyddWhbxdApproval> list = fdyddWhbxdApprovalService.fdyddWhbxdApprovalHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<FdyddWhbxdApproval>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过查询流水号查询房抵e点贷无还本续贷申请
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过查询流水号查询房抵e点贷无还本续贷申请")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        FdyddWhbxdApproval fdyddWhbxdApproval = fdyddWhbxdApprovalService.selectBySerno((String) params.get("serno"));
        if (fdyddWhbxdApproval != null) {
            resultDto.setCode(200);
            resultDto.setData(fdyddWhbxdApproval);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<FdyddWhbxdApproval> show(@PathVariable("serno") String serno) {
        FdyddWhbxdApproval fdyddWhbxdApproval = fdyddWhbxdApprovalService.selectByPrimaryKey(serno);
        return new ResultDto<FdyddWhbxdApproval>(fdyddWhbxdApproval);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FdyddWhbxdApproval> create(@RequestBody FdyddWhbxdApproval fdyddWhbxdApproval) throws URISyntaxException {
        fdyddWhbxdApprovalService.insert(fdyddWhbxdApproval);
        return new ResultDto<FdyddWhbxdApproval>(fdyddWhbxdApproval);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FdyddWhbxdApproval fdyddWhbxdApproval) throws URISyntaxException {
        int result = fdyddWhbxdApprovalService.update(fdyddWhbxdApproval);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = fdyddWhbxdApprovalService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = fdyddWhbxdApprovalService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:managerVetoSubmit
     * @函数描述:房抵e点贷无还本续贷客户经理否决
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷无还本续贷客户经理否决")
    @PostMapping("/managerVetoSubmit")
    protected ResultDto<String> managerVetoSubmit(@RequestBody FdyddWhbxdApproval fdyddWhbxdApproval) {
        return  fdyddWhbxdApprovalService.managerVetoSubmit(fdyddWhbxdApproval);
    }
}
