/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpJxhjRepayLoan
 * @类描述: pvp_jxhj_repay_loan数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-23 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_jxhj_repay_loan")
public class PvpJxhjRepayLoan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Column(name = "serno", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 借据编号 **/
	@Column(name = "bill_no", unique = false, nullable = false, length = 40)
	private String billNo;
	
	/** 拖欠本金 **/
	@Column(name = "prcp", unique = false, nullable = false, length = 40)
	private String prcp;
	
	/** 拖欠利息 **/
	@Column(name = "norm_int", unique = false, nullable = false, length = 40)
	private String normInt;
	
	/** 拖欠罚息 **/
	@Column(name = "od_int", unique = false, nullable = false, length = 40)
	private String odInt;
	
	/** 拖欠复利 **/
	@Column(name = "comm_od_int", unique = false, nullable = false, length = 40)
	private String commOdInt;
	
	/** 未到期本金 **/
	@Column(name = "next_prcp", unique = false, nullable = false, length = 40)
	private String nextPrcp;
	
	/** 应计利息 **/
	@Column(name = "next_int", unique = false, nullable = false, length = 40)
	private String nextInt;
	
	/** 自还本金 **/
	@Column(name = "self_prcp", unique = false, nullable = false, length = 40)
	private String selfPrcp;
	
	/** 自还利息 **/
	@Column(name = "self_int", unique = false, nullable = false, length = 40)
	private String selfInt;
	
	/** 试算检查状态000未验证,111已经验证 **/
	@Column(name = "check_status", unique = false, nullable = false, length = 40)
	private String checkStatus;
	
	/** 贷款起始日期 **/
	@Column(name = "loan_start_date", unique = false, nullable = false, length = 40)
	private String loanStartDate;
	
	/** 贷款到期日期 **/
	@Column(name = "loan_end_date", unique = false, nullable = false, length = 40)
	private String loanEndDate;
	
	/** 贷款台帐状态 **/
	@Column(name = "acc_status", unique = false, nullable = false, length = 40)
	private String accStatus;
	
	/** 贷款余额 **/
	@Column(name = "loan_balance", unique = false, nullable = false, length = 40)
	private String loanBalance;
	
	/** 贷款金额 **/
	@Column(name = "loan_amt", unique = false, nullable = false, length = 40)
	private String loanAmt;
	
	/** 借新本金 **/
	@Column(name = "new_loan_amt", unique = false, nullable = false, length = 40)
	private String newLoanAmt;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param prcp
	 */
	public void setPrcp(String prcp) {
		this.prcp = prcp;
	}
	
    /**
     * @return prcp
     */
	public String getPrcp() {
		return this.prcp;
	}
	
	/**
	 * @param normInt
	 */
	public void setNormInt(String normInt) {
		this.normInt = normInt;
	}
	
    /**
     * @return normInt
     */
	public String getNormInt() {
		return this.normInt;
	}
	
	/**
	 * @param odInt
	 */
	public void setOdInt(String odInt) {
		this.odInt = odInt;
	}
	
    /**
     * @return odInt
     */
	public String getOdInt() {
		return this.odInt;
	}
	
	/**
	 * @param commOdInt
	 */
	public void setCommOdInt(String commOdInt) {
		this.commOdInt = commOdInt;
	}
	
    /**
     * @return commOdInt
     */
	public String getCommOdInt() {
		return this.commOdInt;
	}
	
	/**
	 * @param nextPrcp
	 */
	public void setNextPrcp(String nextPrcp) {
		this.nextPrcp = nextPrcp;
	}
	
    /**
     * @return nextPrcp
     */
	public String getNextPrcp() {
		return this.nextPrcp;
	}
	
	/**
	 * @param nextInt
	 */
	public void setNextInt(String nextInt) {
		this.nextInt = nextInt;
	}
	
    /**
     * @return nextInt
     */
	public String getNextInt() {
		return this.nextInt;
	}
	
	/**
	 * @param selfPrcp
	 */
	public void setSelfPrcp(String selfPrcp) {
		this.selfPrcp = selfPrcp;
	}
	
    /**
     * @return selfPrcp
     */
	public String getSelfPrcp() {
		return this.selfPrcp;
	}
	
	/**
	 * @param selfInt
	 */
	public void setSelfInt(String selfInt) {
		this.selfInt = selfInt;
	}
	
    /**
     * @return selfInt
     */
	public String getSelfInt() {
		return this.selfInt;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	
    /**
     * @return checkStatus
     */
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(String loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public String getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(String loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public String getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param newLoanAmt
	 */
	public void setNewLoanAmt(String newLoanAmt) {
		this.newLoanAmt = newLoanAmt;
	}
	
    /**
     * @return newLoanAmt
     */
	public String getNewLoanAmt() {
		return this.newLoanAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}