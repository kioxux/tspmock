package cn.com.yusys.yusp.service.server.xdtz0043;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0043.resp.Xdtz0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 接口处理类:统计客户行内信用类贷款余额
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0043Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0043Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 统计客户行内信用类贷款余额
     *
     * @param queryMap
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0043DataRespDto getLoanSumByCertNo(Map queryMap) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(queryMap));
        Xdtz0043DataRespDto xdtz0043DataRespDto = accLoanMapper.getLoanSumByCertNo(queryMap);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043DataRespDto));
        return xdtz0043DataRespDto;
    }
}
