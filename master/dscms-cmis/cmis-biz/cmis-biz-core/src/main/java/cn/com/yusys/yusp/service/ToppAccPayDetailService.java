/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Lstdkstzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123RespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.req.DjztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.resp.DjztcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.req.HvpsmrReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.resp.HvpsmrRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.req.HvpylsReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.HvpylsRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.ToppAccPayDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAccPayDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 22:53:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ToppAccPayDetailService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(ToppAccPayDetailService.class);

    @Autowired
    private ToppAccPayDetailMapper toppAccPayDetailMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Dscms2HvpylsClientService dscms2HvpylsClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private IqpChgTrupayAcctAppService iqpChgTrupayAcctAppService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    // 入账状态
    Map<String, String> resultMap = new HashMap<String, String>() {
        {
            put("TA00", "TA00-已入账");
            put("TA01", "TA01-待人工确认");
            put("TA02", "TA02-验签失败");
            put("TA03", "TA03-间连无法实时回复");
            put("TA04", "TA04-需退汇");
            put("TA05", "TA05-已退汇");
            put("TA06", "TA06-无此业务");
            put("TA07", "TA07-待定状态");
        }
    };

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ToppAccPayDetail selectByPrimaryKey(String serno) {
        return toppAccPayDetailMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<ToppAccPayDetail> selectAll(QueryModel model) {
        List<ToppAccPayDetail> records = (List<ToppAccPayDetail>) toppAccPayDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<ToppAccPayDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<ToppAccPayDetail> list = toppAccPayDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: toppDetailList
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ToppAccPayDetail> toppDetailList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        // 查询待处理,处理中的数据
        model.addCondition("statusList", "0,1");
        model.setSort("createTime desc");
        List<ToppAccPayDetail> list = toppAccPayDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: toppDetailHisList
     * @方法描述: 条件查询 - 查询进行分页（查询历史）
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<ToppAccPayDetail> toppDetailHisList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        // 历史记录，查询成功,失败的
        model.addCondition("statusList","2,3");
        model.setSort("updateTime desc");
        List<ToppAccPayDetail> list = toppAccPayDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ToppAccPayDetail record) {
        return toppAccPayDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ToppAccPayDetail record) {
        return toppAccPayDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ToppAccPayDetail record) {
        return toppAccPayDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ToppAccPayDetail record) {
        return toppAccPayDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return toppAccPayDetailMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return toppAccPayDetailMapper.deleteByIds(ids);
    }

    /**
     * 发送二代受托支付
     * @param serno
     * @return
     */
    public ToppAccPayDetail sendToppAccPay(String serno) {
        ToppAccPayDetail toppAccPayDetail = selectByPrimaryKey(serno);
        if(Objects.isNull(toppAccPayDetail)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value);
        }
        // 只有待处理的件，才可以发送二代受托支付
        if(Objects.equals("0",toppAccPayDetail.getStatus())) {
            // 修改
            String updId = "";
            // 修改
            String updBrId = "";
            // 修改日期
            String updDate = "";
            // 修改时间
            Date updateTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                updId = userInfo.getLoginCode();
                // 申请机构
                updBrId = userInfo.getOrg().getCode();
                // 申请时间
                updDate = DateUtils.getCurrDateStr();
            }
            // 最近修改人
            toppAccPayDetail.setUpdId(updId);
            // 最近修改机构
            toppAccPayDetail.setUpdBrId(updBrId);
            // 最近修改日期
            toppAccPayDetail.setUpdDate(updDate);
            // 修改时间
            toppAccPayDetail.setUpdateTime(updateTime);
            // 大额往帐一体化（发送二代支付系统进行受托）
            toppAccPayDetail = hvpsmrCall(toppAccPayDetail);
            toppAccPayDetailMapper.updateByPrimaryKey(toppAccPayDetail);
            return toppAccPayDetail;
        }else {
            throw BizException.error(null, "9999", "只有待处理的件才允许发送二代受托支付，请确认！！");
        }
    }

    /**
     * 查询二代受托支付结果
     * @param serno
     * @return
     */
    public ToppAccPayDetail selectToppAccPayResult(String serno) {
        ToppAccPayDetail toppAccPayDetail = selectByPrimaryKey(serno);
        if(Objects.isNull(toppAccPayDetail)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value);
        }
        // 只有处理中的件，才可以进行查询二代受托支付结果
        if(Objects.equals("1",toppAccPayDetail.getStatus())) {
            // 修改
            String updId = "";
            // 修改
            String updBrId = "";
            // 修改日期
            String updDate = "";
            // 修改时间
            Date updateTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                // 申请人
                updId = userInfo.getLoginCode();
                // 申请机构
                updBrId = userInfo.getOrg().getCode();
                // 申请时间
                updDate = DateUtils.getCurrDateStr();
            }
            // 最近修改人
            toppAccPayDetail.setUpdId(updId);
            // 最近修改机构
            toppAccPayDetail.setUpdBrId(updBrId);
            // 最近修改日期
            toppAccPayDetail.setUpdDate(updDate);
            // 修改时间
            toppAccPayDetail.setUpdateTime(updateTime);

            // 获取二代支付正常的响应码   已清算
            AdminSmPropQueryDto sadminSmPropQueryDto = new AdminSmPropQueryDto();
            sadminSmPropQueryDto.setPropName(CmisCommonConstants.SUCCESS_TOPPACC_CODE);
            String successToppAccCode = adminSmPropService.getPropValue(sadminSmPropQueryDto).getData().getPropValue();
            log.info("【查询正常的二代响应码" + successToppAccCode + "】");
            if(StringUtils.nonBlank(toppAccPayDetail.getReasonFirst()) && successToppAccCode.contains(toppAccPayDetail.getReasonFirst())) {
                // 调用贷记入账状态查询申请往帐接口
                toppAccPayDetail = djztcxCall(toppAccPayDetail);
            } else {
                // 调用大额往账列表查询
                toppAccPayDetail = hvpylsCall(toppAccPayDetail);
            }
            // 更新状态
            toppAccPayDetailMapper.updateByPrimaryKeySelective(toppAccPayDetail);
            return toppAccPayDetail;
        }else {
            throw BizException.error(null, "9999", "只有处理中的件，才可以进行查询二代受托支付结果，请确认！！");
        }
    }

    /**
     * 轮询查询任务-从二代支付系统中查询受托支付结果
     */
    public Integer updateToppAccPayDetailBatch() {
        log.info("轮询查询任务-从二代支付系统中查询受托支付结果开始");
        // 处理件数
        int count = 0;
        // 修改
        String updId = "";
        // 修改
        String updBrId = "";
        // 修改日期
        String updDate = "";
        // 修改时间
        Date updateTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
            // 申请时间
            updDate = DateUtils.getCurrDateStr();
        }
        QueryModel model = new QueryModel();
        // 查询处理中的数据
        model.addCondition("status","1");
        List<ToppAccPayDetail> detailList = selectAll(model);
        if(CollectionUtils.nonEmpty(detailList)) {
            for(ToppAccPayDetail toppAccPayDetail : detailList) {
                log.info("轮询查询任务-从二代支付系统中查询受托支付结果执行中，【交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用大额往账列表查询开始");
                // 获取二代支付正常的响应码   已清算
                AdminSmPropQueryDto sadminSmPropQueryDto = new AdminSmPropQueryDto();
                sadminSmPropQueryDto.setPropName(CmisCommonConstants.SUCCESS_TOPPACC_CODE);
                String successToppAccCode = adminSmPropService.getPropValue(sadminSmPropQueryDto).getData().getPropValue();
                log.info("【查询正常的二代响应码" + successToppAccCode + "】");
                if(StringUtils.nonBlank(toppAccPayDetail.getReasonFirst()) && successToppAccCode.contains(toppAccPayDetail.getReasonFirst())) {
                    // 调用贷记入账状态查询申请往帐接口
                    toppAccPayDetail = djztcxCall(toppAccPayDetail);
                } else {
                    // 调用大额往账列表查询
                    toppAccPayDetail = hvpylsCall(toppAccPayDetail);
                }
                // 最近修改人
                toppAccPayDetail.setUpdId(updId);
                // 最近修改机构
                toppAccPayDetail.setUpdBrId(updBrId);
                // 最近修改日期
                toppAccPayDetail.setUpdDate(updDate);
                // 修改时间
                toppAccPayDetail.setUpdateTime(updateTime);
                // 更新状态
                toppAccPayDetailMapper.updateByPrimaryKeySelective(toppAccPayDetail);
                log.info("轮询查询任务-从二代支付系统中查询受托支付结果执行中，【交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用大额往账列表查询结束");
            }
            count = detailList.size();
        }
        log.info("轮询查询任务-从二代支付系统中查询受托支付结果结束");
        return count;
    }

    /**
     * 调用大额往账列表查询
     * @param toppAccPayDetail
     * @return
     */
    public ToppAccPayDetail hvpylsCall(ToppAccPayDetail toppAccPayDetail) {
        log.info("从二代支付系统中查询受托支付结果开始（调用大额往账列表查询），受托支付账号：{}",toppAccPayDetail.getToppAccno());
        HvpylsReqDto reqDto = new HvpylsReqDto();
        // 大小额交易标志(大小额固定传hvtran)
        reqDto.setHvflag("hvtran");
        // 大额输入05 小额输入06，这里是05
        reqDto.setFunctp("05");
        // 钱箱号(不需要)
        reqDto.setCsbxno("");
        // 和esb交易码填写的一样的，hvpyls
        reqDto.setSubprc("hvpyls");
        // 交易日期(不需要)
        reqDto.setOperdt("");
        // 往来账标志
        reqDto.setSrflag("1");
        // 业务受理编号
        reqDto.setOpersq("");
        // 终端号
        reqDto.setTermid("");
        // 系统日期
        reqDto.setSystdt(DateUtils.getCurrentDate(DateFormatEnum.DATE_COMPACT));
        // 行号
        reqDto.setBankid("");
        // 系统日期
        String sysdate = toppAccPayDetail.getInputDate();
        if(StringUtils.nonBlank(sysdate)){
            sysdate = sysdate.replaceAll("-","");
        }
        reqDto.setXtrqdt(sysdate);
        // 交易金额
        reqDto.setTranam(toppAccPayDetail.getToppAmt());
        // 发起行行号(默认本行)
        reqDto.setSdcode("");
        // 接收行行号
        reqDto.setRdcode("");
        // 付款人账号
        reqDto.setPyerac("");
        // 收款人账号
        reqDto.setPyeeac("");
        // 交易状态
        reqDto.setTranst("%");
        // 业务受理编号
        reqDto.setHstrsq(toppAccPayDetail.getBusNo());
        // 来往帐标志
        reqDto.setCocain("1");
        // 起始笔数
        reqDto.setBegnum("1");
        // 查询笔数
        reqDto.setCountnum("10");
        // 渠道流水
        reqDto.setChannelseq("");
        // 账务机构
        String finalBrId = "";
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息");
        AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(toppAccPayDetail.getBillNo());
        if(accLoan != null && StringUtils.nonBlank(accLoan.getBillNo())){
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息成功,数据为【"+ JSON.toJSONString(accLoan) + "】");
            finalBrId = accLoan.getManagerBrId();
        }else{
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息");
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(toppAccPayDetail.getBillNo());
            if(accEntrustLoan != null && StringUtils.nonBlank(accEntrustLoan.getBillNo())){
                log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息成功,数据为【"+ JSON.toJSONString(accEntrustLoan) + "】");
                finalBrId = accEntrustLoan.getManagerBrId();
            }else{
                log.info("从二代支付系统中查询受托支付结果结束（调用大额往账列表查询），查询台账信息失败：【{}】",toppAccPayDetail.getToppAccno());
                return toppAccPayDetail;
            }
        }
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 获取管护机构为：" + finalBrId);
        if (finalBrId.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            finalBrId = "810100";
        }else if(finalBrId.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            finalBrId = "800100";
        }
        finalBrId = finalBrId.substring(0, finalBrId.length()-1) +"1";
        log.info("【前往二代支付系统进行调用大额往账列表查询开始【" + toppAccPayDetail.getBillNo() + "】, 账务机构为：" + finalBrId);
        // 账务机构
        reqDto.setBrchno(finalBrId);
        // 调用大额往账列表查询
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用大额往账列表查询开始-请求报文：" + JSON.toJSONString(reqDto));
        ResultDto<HvpylsRespDto> resultDto =  dscms2HvpylsClientService.hvpyls(reqDto);
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用大额往账列表查询结束-响应报文：" + JSON.toJSONString(resultDto));
        if(Objects.isNull(resultDto)) {
            throw BizException.error(null, "9999", "调用大额往账列表查询接口失败，请确认！！");
        }
        if(Objects.equals("0000",resultDto.getCode()) || Objects.equals("0",resultDto.getCode())) {
            java.util.List<cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.List> list = resultDto.getData().getList();
            if(CollectionUtils.nonEmpty(list)) {
                // 获取二代支付正常的响应码   已清算
                AdminSmPropQueryDto sadminSmPropQueryDto = new AdminSmPropQueryDto();
                sadminSmPropQueryDto.setPropName(CmisCommonConstants.SUCCESS_TOPPACC_CODE);
                String successToppAccCode = adminSmPropService.getPropValue(sadminSmPropQueryDto).getData().getPropValue();
                log.info("【查询正常的二代响应码" + successToppAccCode + "】");
                // 获取二代支付异常的响应码 已取消
                AdminSmPropQueryDto eadminSmPropQueryDto = new AdminSmPropQueryDto();
                eadminSmPropQueryDto.setPropName(CmisCommonConstants.ERROR_TOPPACC_CODE);
                String errorToppaccCode = adminSmPropService.getPropValue(eadminSmPropQueryDto).getData().getPropValue();
                log.info("【查询异常的二代响应码" + successToppAccCode + "】");
                if(StringUtils.nonBlank(list.get(0).getTranst()) && successToppAccCode.contains(list.get(0).getTranst())) { //二代处理码正常情况 已清算
                    // 原因一（已清算）
                    toppAccPayDetail.setReasonFirst(list.get(0).getTranst());
                    // 调用贷记入账状态查询申请往帐接口
                    toppAccPayDetail = djztcxCall(toppAccPayDetail);
                } else if(StringUtils.nonBlank(list.get(0).getTranst()) && errorToppaccCode.contains(list.get(0).getTranst())){ //二代处理码异常情况 已取消
                    // 1:待处理
                    toppAccPayDetail.setStatus("0");
                    // 原因一（已取消）
                    toppAccPayDetail.setReasonFirst(list.get(0).getTranst());
                } else {
                    // 1:继续处理
                    toppAccPayDetail.setStatus("1");
                    // 原因一
                    toppAccPayDetail.setReasonFirst(list.get(0).getTranst());
                }
            }
        } else {
            // 1:继续处理
            toppAccPayDetail.setStatus("1");
            // 原因一
            toppAccPayDetail.setReasonFirst(resultDto.getMessage());
        }
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用大额往账列表查询结束-请求报文：" + JSON.toJSONString(resultDto));
        log.info("从二代支付系统中查询受托支付结果结束（调用大额往账列表查询），受托支付账号：{}",toppAccPayDetail.getToppAccno());
        return toppAccPayDetail;
    }
    
    /**
     * 调用贷记入账状态查询申请往帐接口
     * @param toppAccPayDetail
     * @return
     */
    public ToppAccPayDetail djztcxCall(ToppAccPayDetail toppAccPayDetail) {
        log.info("轮询查询任务-从二代支付系统中查询受托支付结果开始（调用贷记入账状态查询申请往帐接口），受托支付账号：{}",toppAccPayDetail.getToppAccno());
        DjztcxReqDto djztcxReqDto = new DjztcxReqDto();
        // 大小额交易标志
        djztcxReqDto.setHvflag("hvtran");
        // 业务类型（05：大额支付系统(缺省)）
        djztcxReqDto.setFunctp("05");
        // 前台交易码
        djztcxReqDto.setSubprc("djztcx");
        // 原业务受理编号
        djztcxReqDto.setOribusinum(toppAccPayDetail.getBusNo());
        // 业务受理编号(空)
        djztcxReqDto.setBusinum("");
        // 账务机构
        String finalBrId = "";
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息");
        AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(toppAccPayDetail.getBillNo());
        if(accLoan != null && StringUtils.nonBlank(accLoan.getBillNo())){
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息成功,数据为【"+ JSON.toJSONString(accLoan) + "】");
            finalBrId = accLoan.getManagerBrId();
        }else{
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息");
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(toppAccPayDetail.getBillNo());
            if(accEntrustLoan != null && StringUtils.nonBlank(accEntrustLoan.getBillNo())){
                log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息成功,数据为【"+ JSON.toJSONString(accEntrustLoan) + "】");
                finalBrId = accEntrustLoan.getManagerBrId();
            }else{
                log.info("大额往帐一体化（二代大小额受托支付贷记往账->客户账到交易对手账）结束，查询台账信息失败：{}",toppAccPayDetail.getToppAccno());
                return toppAccPayDetail;
            }
        }
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 获取管护机构为：" + finalBrId);
        if (finalBrId.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            finalBrId = "810100";
        }else if(finalBrId.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            finalBrId = "800100";
        }
        finalBrId = finalBrId.substring(0, finalBrId.length()-1) +"1";
        log.info("【前往二代支付系统进行调用大额往账列表查询开始【" + toppAccPayDetail.getBillNo() + "】, 账务机构为：" + finalBrId);
        // 账务机构
        djztcxReqDto.setBrchno(finalBrId);
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询开始-请求报文：" + JSON.toJSONString(djztcxReqDto));
        ResultDto<DjztcxRespDto> djztcxResultDto = dscms2HvpylsClientService.djztcx(djztcxReqDto);
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询结束-请求报文：" + JSON.toJSONString(djztcxResultDto));
        if(Objects.isNull(djztcxResultDto)) {
            throw BizException.error(null, "9999", "调用贷记入账状态查询申请往帐接口失败，请确认！！");
        }
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询开始-响应码为：" + djztcxResultDto.getCode());
        // 成功code（0000或者0）
        if(Objects.equals("0000",djztcxResultDto.getCode()) || Objects.equals("0",djztcxResultDto.getCode())) {
            DjztcxRespDto djztcxRespDto = djztcxResultDto.getData();
            log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询开始-响应报文体：" + djztcxRespDto.toString());
            log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询开始-原因二为：" + djztcxRespDto.getTtfnlst());
            if(Objects.equals("TA00",djztcxRespDto.getTtfnlst())) {
                // 2:成功
                toppAccPayDetail.setStatus("2");
                // 原因二
                toppAccPayDetail.setReasonSecond("TA00-已入账");
            } else if(Objects.equals("TA05",djztcxRespDto.getTtfnlst()) || Objects.equals("TA06",djztcxRespDto.getTtfnlst())){
                // 3:失败
                toppAccPayDetail.setStatus("3");
                // 原因二
                toppAccPayDetail.setReasonSecond(Objects.nonNull(resultMap.get(djztcxRespDto.getTtfnlst())) ? resultMap.get(djztcxRespDto.getTtfnlst()) : djztcxRespDto.getTtfnlst());
                // 如果是已退汇业务，则表示需要变更受托账户信息
                if(Objects.equals("TA05",djztcxRespDto.getTtfnlst())) {
                    createIqpChgTrupayAcctApp(toppAccPayDetail);
                }
            } else {
                // 1:继续处理
                toppAccPayDetail.setStatus("1");
                // 原因二
                toppAccPayDetail.setReasonSecond(Objects.nonNull(resultMap.get(djztcxRespDto.getTtfnlst())) ? resultMap.get(djztcxRespDto.getTtfnlst()) : djztcxRespDto.getTtfnlst());
            }
        } else {
            log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行调用贷记入账状态查询开始-响应失败：" + djztcxResultDto.getCode());
            // 1:继续处理
            toppAccPayDetail.setStatus("1");
            // 原因一
            toppAccPayDetail.setReasonSecond(djztcxResultDto.getMessage());
        }
        log.info("轮询查询任务-从二代支付系统中查询受托支付结果结束（调用贷记入账状态查询申请往帐接口），受托支付账号：{}",toppAccPayDetail.getToppAccno());
        return toppAccPayDetail;
    }

    /**
     * 根据授权信息表创建受托支付登记簿名单
     * @param pvpAuthorize
     */
    public void createToppAccPayDetail(PvpAuthorize pvpAuthorize,List<Lstdkstzf> list) {
        log.info("根据授权信息表创建受托支付登记簿名单开始，放款流水号：{}",pvpAuthorize.getPvpSerno());
        // 初始化受托支付登记薄名单
        ToppAccPayDetail toppAccPayDetail = null;
        for(Lstdkstzf lstdkstzf : list) {
            // 判断对方账号种类 1 本行 2 他行   对方账号不为空 对方账号名称不为空
            if(Objects.equals("2",lstdkstzf.getDfzhhzhl()) && (StringUtils.nonBlank(lstdkstzf.getDfzhangh())
            && StringUtils.nonBlank(lstdkstzf.getDfzhhmch()))) {
                toppAccPayDetail =  new ToppAccPayDetail();
                // 放款流水号
                toppAccPayDetail.setPvpSerno(pvpAuthorize.getPvpSerno());
                // 合同编号
                toppAccPayDetail.setContNo(pvpAuthorize.getContNo());
                // 借据编号
                toppAccPayDetail.setBillNo(pvpAuthorize.getBillNo());
                // 客户编号
                toppAccPayDetail.setCusId(pvpAuthorize.getCusId());
                // 客户名称
                toppAccPayDetail.setCusName(pvpAuthorize.getCusName());
                // 支付对手账号
                toppAccPayDetail.setPayAccno(lstdkstzf.getZjzrzhao());
                // 支付对手名称
                toppAccPayDetail.setPayName(lstdkstzf.getZjzrzhmc());
                // 交易对手账户
                toppAccPayDetail.setToppAccno(lstdkstzf.getDfzhangh());
                // 交易对手名称
                toppAccPayDetail.setToppName(lstdkstzf.getDfzhhmch());
                // 交易对手金额
                toppAccPayDetail.setToppAmt(lstdkstzf.getStzfjine());
                // 交易对手开户行号
                toppAccPayDetail.setToppBankNo(lstdkstzf.getDfzhhkhh());
                // 交易对手开户行名称
                toppAccPayDetail.setToppBankName(lstdkstzf.getDfzhkhhm());
                // 冻结编号
                toppAccPayDetail.setFreezeNo(lstdkstzf.getDjiebhao());
                // 根据 stzfclzt 0未支付 /1已支付 受托支付处理状态 给状态
                // 状态(0、待处理，1、处理中，2、成功，3、失败)
                toppAccPayDetail.setStatus("0");
                // 原因一
                toppAccPayDetail.setReasonFirst("");
                // 原因二
                toppAccPayDetail.setReasonSecond("");

                String managerId = "";
                String managerBrId = "";
                log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息");
                AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(toppAccPayDetail.getBillNo());
                if(accLoan != null && StringUtils.nonBlank(accLoan.getBillNo())){
                    log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息成功,数据为【"+ JSON.toJSONString(accLoan) + "】");
                    managerId = accLoan.getManagerId();
                    managerBrId = accLoan.getManagerBrId();
                }else{
                    log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息");
                    AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(toppAccPayDetail.getBillNo());
                    if(accEntrustLoan != null && StringUtils.nonBlank(accEntrustLoan.getBillNo())){
                        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息成功,数据为【"+ JSON.toJSONString(accEntrustLoan) + "】");
                        managerId = accEntrustLoan.getManagerId();
                        managerBrId = accEntrustLoan.getManagerBrId();
                    }else{
                        log.info("大额往帐一体化（二代大小额受托支付贷记往账->客户账到交易对手账）结束，查询台账信息失败：借据号【{}】",toppAccPayDetail.getBillNo());
                        managerId = pvpAuthorize.getManagerId();
                        managerBrId = pvpAuthorize.getManagerBrId();
                    }
                }
                // 登记人
                toppAccPayDetail.setInputId(managerId);
                // 登记机构
                toppAccPayDetail.setInputBrId(managerBrId);
                // 登记日期
                toppAccPayDetail.setInputDate(pvpAuthorize.getInputDate());
                // 创建时间
                toppAccPayDetail.setCreateTime(DateUtils.getCurrDate());
                int count = insert(toppAccPayDetail);
                if (count <= 0) {
                    log.info("根据授权信息表创建受托支付登记簿名单失败");
                    throw new YuspException(EcbEnum.E_IQP_EXISTS.key, EcbEnum.E_IQP_EXISTS.value);
                } else {
                    log.info("根据授权信息表创建受托支付登记簿名单成功");
                    // 大额往帐一体化（发送二代支付系统进行受托）
                    toppAccPayDetail = hvpsmrCall(toppAccPayDetail);
                    toppAccPayDetailMapper.updateByPrimaryKey(toppAccPayDetail);
                }
            }
        }
        log.info("根据授权信息表创建受托支付登记簿名单结束，放款流水号：{}",pvpAuthorize.getPvpSerno());
    }

    /**
     * 大额往帐一体化（发送二代支付系统进行受托）
     * @param toppAccPayDetail
     * @return
     */
    public ToppAccPayDetail hvpsmrCall(ToppAccPayDetail toppAccPayDetail) {
        log.info("大额往帐一体化（二代大小额受托支付贷记往账->客户账到交易对手账）开始，放款流水号：{}",toppAccPayDetail.getToppAccno());
        HvpsmrReqDto hvpsmrReqDto = new HvpsmrReqDto();
        // 大小额交易标志
        hvpsmrReqDto.setHvflag("hvtran");
        // 业务类型
        hvpsmrReqDto.setFunctp("05");
        // 钱箱号
        hvpsmrReqDto.setCsbxno("");
        // 前台交易码
        hvpsmrReqDto.setSubprc("hvpsmr");
        // 交易日期
        hvpsmrReqDto.setOpersq("");
        // 往来账标志
        hvpsmrReqDto.setSrflag("");
        // 业务受理编号
        hvpsmrReqDto.setOpersq("");
        // 终端号
        hvpsmrReqDto.setTermid("");
        // 系统日期
        hvpsmrReqDto.setSystdt(DateUtils.getCurrentDate(DateFormatEnum.DATE_COMPACT));
        // 行号
        hvpsmrReqDto.setBankid("");
        // 跨境标志
        hvpsmrReqDto.setMesgtp("1");
        // 业务类型
        hvpsmrReqDto.setOpertp("A100");
        // 业务种类
        hvpsmrReqDto.setOpertr("02102");
        // 疑似重账标志
        hvpsmrReqDto.setFlagtp("0");
        // 币种
        hvpsmrReqDto.setCrcycd("01");
        // 票据种类
        hvpsmrReqDto.setBilltp("2");
        // 小额贷记手续费扣款标志 0月末扣款 1及时扣款
        hvpsmrReqDto.setDrawtp("1");
        // 手续费
        hvpsmrReqDto.setHandch(new BigDecimal("0.00"));
        // 邮电费
        hvpsmrReqDto.setPostch(new BigDecimal("0.00"));
        // 业务优先级
        hvpsmrReqDto.setPriotg("NORM");
        // 附言
        hvpsmrReqDto.setPscrtx("他行受托支付");
        // 实际收款账号
        hvpsmrReqDto.setPyeeac(toppAccPayDetail.getToppAccno());
        // 实际收款人名称
        hvpsmrReqDto.setPyeena(toppAccPayDetail.getToppName());
        // 账户账号
        hvpsmrReqDto.setPyerac(toppAccPayDetail.getPayAccno());
        // 账户户名
        hvpsmrReqDto.setPyerna(toppAccPayDetail.getPayName());
        // 接收行行号
        hvpsmrReqDto.setRdcode(toppAccPayDetail.getToppBankNo());
        // 接收行行名
        hvpsmrReqDto.setRdname(toppAccPayDetail.getToppBankName());
        // 交易金额
        hvpsmrReqDto.setTranam(toppAccPayDetail.getToppAmt());
        // 转账类型
        hvpsmrReqDto.setTrantp("TR");
        // 凭证组合
        hvpsmrReqDto.setDcmtgp("003");
        // 小额贷记手续费扣款标志
        hvpsmrReqDto.setDrawtp("1");
        // 电汇凭证类型
        hvpsmrReqDto.setTlmotp("999");
        // 支票签发日期
        hvpsmrReqDto.setOnofbz("1");
        //
        hvpsmrReqDto.setWaibjymc("汇兑受理");
        // 服务时间
        hvpsmrReqDto.setServti(DateUtils.getCurrentDate(DateFormatEnum.TIME_COMPACT));
        // 服务日期
        hvpsmrReqDto.setServti(DateUtils.getCurrentDate(DateFormatEnum.DATE_COMPACT));
        // 受托交易支付号
        hvpsmrReqDto.setStzfbill(toppAccPayDetail.getBillNo());

        Ln3123ReqDto ln3123ReqDto = new Ln3123ReqDto();
        ln3123ReqDto.setDkjiejuh(toppAccPayDetail.getBillNo());
        ln3123ReqDto.setDjiebhao(toppAccPayDetail.getFreezeNo());
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 冻结编号【"+ toppAccPayDetail.getFreezeNo() +"】查询受托支付查询, 请求报文：" + ln3123ReqDto.toString());
        ResultDto<Ln3123RespDto> ln3123RespDtoResultDto = dscms2CoreLnClientService.ln3123(ln3123ReqDto);
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 冻结编号【"+ toppAccPayDetail.getFreezeNo() +"】查询受托支付查询, 返回报文：" + ln3123RespDtoResultDto.toString());
        if(ln3123RespDtoResultDto != null && CollectionUtils.nonEmpty(ln3123RespDtoResultDto.getData().getLstdkstzf())){
            cn.com.yusys.yusp.dto.client.esb.core.ln3123.Lstdkstzf lstdkstzf = ln3123RespDtoResultDto.getData().getLstdkstzf().get(0);
            // 序列号
            hvpsmrReqDto.setSeqnno(String.valueOf(lstdkstzf.getXuhaoooo()));
        }else{
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 冻结编号【"+ toppAccPayDetail.getFreezeNo() +"】查询受托支付查询, 返回报文异常");
        }
        // 支票签发日期
        hvpsmrReqDto.setWkdate(DateUtils.getCurrentDate(DateFormatEnum.DATE_COMPACT));
        // waibclma
        hvpsmrReqDto.setWaibclma("530101");
        // 冻结编号
        hvpsmrReqDto.setDjbh(toppAccPayDetail.getFreezeNo());

        // 账务机构
        String finalBrId = "";
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息");
        AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(toppAccPayDetail.getBillNo());
        if(accLoan != null && StringUtils.nonBlank(accLoan.getBillNo())){
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询贷款台账信息成功,数据为【"+ JSON.toJSONString(accLoan) + "】");
            finalBrId = accLoan.getManagerBrId();
        }else{
            log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息");
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(toppAccPayDetail.getBillNo());
            if(accEntrustLoan != null && StringUtils.nonBlank(accEntrustLoan.getBillNo())){
                log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 查询委托贷款台账信息成功,数据为【"+ JSON.toJSONString(accEntrustLoan) + "】");
                finalBrId = accEntrustLoan.getManagerBrId();
            }else{
                log.info("大额往帐一体化（二代大小额受托支付贷记往账->客户账到交易对手账）结束，查询台账信息失败：{}",toppAccPayDetail.getToppAccno());
                return toppAccPayDetail;
            }
        }
        log.info("【根据借据编号【" + toppAccPayDetail.getBillNo() + "】, 获取管护机构为：" + finalBrId);
        if (finalBrId.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            finalBrId = "810100";
        }else if(finalBrId.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            finalBrId = "800100";
        }
        finalBrId = finalBrId.substring(0, finalBrId.length()-1) +"1";
        log.info("【前往二代支付系统进行调用大额往账列表查询开始【" + toppAccPayDetail.getBillNo() + "】, 账务机构为：" + finalBrId);
        // 账务机构
        hvpsmrReqDto.setBrchno(finalBrId);
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行受托-请求报文：" + JSON.toJSONString(hvpsmrReqDto));
        ResultDto<HvpsmrRespDto> resultDto = dscms2HvpylsClientService.hvpsmr(hvpsmrReqDto);
        log.info("【根据交易对手账户" + toppAccPayDetail.getToppAccno() + "】前往二代支付系统进行受托-响应报文：" + JSON.toJSONString(resultDto));
        if(Objects.isNull(resultDto)) {
            // 如果请求响应内容为空，则表示服务调用失败
            toppAccPayDetail.setStatus("0");
            toppAccPayDetail.setReasonFirst("服务调用失败");
        }
        if(!Objects.equals("0",resultDto.getCode()) && !Objects.equals("0000",resultDto.getCode())) {
            toppAccPayDetail.setStatus("0");
            toppAccPayDetail.setReasonFirst(resultDto.getMessage());
        } else {
            // 处理中
            toppAccPayDetail.setStatus("1");
            // 错误信息
            toppAccPayDetail.setReasonFirst("");
            // 业务受理编号
            toppAccPayDetail.setBusNo(Objects.nonNull(resultDto.getData()) ? resultDto.getData().getHstrsq1() : "");
            // 核心交易日期赋值
            toppAccPayDetail.setInputDate(Objects.nonNull(resultDto.getData()) ? resultDto.getData().getJiaoyirq() : DateUtils.getCurrentDate(DateFormatEnum.DATE_COMPACT));
        }
        log.info("大额往帐一体化（二代大小额受托支付贷记往账->客户账到交易对手账）结束，放款流水号：{}",toppAccPayDetail.getToppAccno());
        return toppAccPayDetail;
    }

    /**
     * 生成受托账号修改申请信息
     * @param toppAccPayDetail
     */
    @Transactional
    public void createIqpChgTrupayAcctApp(ToppAccPayDetail toppAccPayDetail) {
        log.info("二代支付系统进行调用贷记入账状态,生成受托支付修改任务开始");
        // 生成流水号
        String sqSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>());
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = new IqpChgTrupayAcctApp();
        iqpChgTrupayAcctApp.setPkId(sqSerno);
        // 放款流水号
        iqpChgTrupayAcctApp.setSerno(toppAccPayDetail.getPvpSerno());
        // 借据编号
        iqpChgTrupayAcctApp.setBillNo(toppAccPayDetail.getBillNo());
        // 客户号
        iqpChgTrupayAcctApp.setCusId(toppAccPayDetail.getCusId());
        // 客户名称
        iqpChgTrupayAcctApp.setCusName(toppAccPayDetail.getCusName());
        // 交易对手账户
        iqpChgTrupayAcctApp.setToppAccno("");
        // 交易对手名称
        iqpChgTrupayAcctApp.setToppName("");
        // 交易对手金额
        iqpChgTrupayAcctApp.setToppAmt(toppAccPayDetail.getToppAmt());
        // 交易对手开户行号
        iqpChgTrupayAcctApp.setToppBankNo("");
        // 交易对手开户行名称
        iqpChgTrupayAcctApp.setToppBankName("");
        // 原交易对手账户
        iqpChgTrupayAcctApp.setOrigiToppAccno(toppAccPayDetail.getToppAccno());
        // 原交易对手名称
        iqpChgTrupayAcctApp.setOrigiToppName(toppAccPayDetail.getToppName());
        // 原交易对手金额
        iqpChgTrupayAcctApp.setOrigiToppAmt(toppAccPayDetail.getToppAmt());
        // 原交易对手开户行号
        iqpChgTrupayAcctApp.setOrigiToppBankno(toppAccPayDetail.getToppBankNo());
        // 原交易对手账户
        iqpChgTrupayAcctApp.setOrigiToppBankname(toppAccPayDetail.getToppBankName());
        // 登记人
        iqpChgTrupayAcctApp.setInputId(toppAccPayDetail.getInputId());
        // 登记机构
        iqpChgTrupayAcctApp.setInputBrId(toppAccPayDetail.getInputBrId());
        // 主管客户经理
        iqpChgTrupayAcctApp.setManagerId(toppAccPayDetail.getInputId());
        // 主管机构
        iqpChgTrupayAcctApp.setManagerBrId(toppAccPayDetail.getInputBrId());
        // 冻结编号
        iqpChgTrupayAcctApp.setHxSerno(toppAccPayDetail.getFreezeNo());
        // 合同编号
        iqpChgTrupayAcctApp.setContNo(toppAccPayDetail.getContNo());
        // 操作类型
        iqpChgTrupayAcctApp.setOprType("01");
        // 审批状态
        iqpChgTrupayAcctApp.setApproveStatus("000");
        // 登记日期
        iqpChgTrupayAcctApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        // 是否客户修改
        iqpChgTrupayAcctApp.setIsOwner("0");
        int iqpChgTrupayAcctAppCnt = iqpChgTrupayAcctAppService.insert(iqpChgTrupayAcctApp);
        if (iqpChgTrupayAcctAppCnt < 1) {
            throw BizException.error(null, "9999", "生成受托账号修改申请表失败，请确认！！");
        }
        log.info("二代支付系统进行调用贷记入账状态,生成受托支付修改任务结束");
    }

    /**
     * 根据受托账号修改信息创建受托支付登记簿名单
     * @param iqpChgTrupayAcctApp
     */
    public void createToppAccPayDetailForChange(IqpChgTrupayAcctApp iqpChgTrupayAcctApp) {
        log.info("根据受托账号修改信息创建受托支付登记簿名单开始，冻结编号【{}】",iqpChgTrupayAcctApp.getHxSerno());
        QueryModel model = new QueryModel();
        // 查询处理中的数据
        model.addCondition("freezeNo",iqpChgTrupayAcctApp.getHxSerno());
        // 因为后续需求改动，手工发起的可能不是已退汇
        //model.addCondition("reasonSecond","TA05-已退汇");
        List<ToppAccPayDetail> toppAccPayDetailList = selectAll(model);
        if(CollectionUtils.nonEmpty(toppAccPayDetailList)) {
            ToppAccPayDetail toppAccPayDetailNew = new ToppAccPayDetail();
            ToppAccPayDetail toppAccPayDetailOld = toppAccPayDetailList.get(0);
            // 流水号
            toppAccPayDetailNew.setSerno(toppAccPayDetailOld.getSerno());
            // 放款流水号
            toppAccPayDetailNew.setPvpSerno(toppAccPayDetailOld.getPvpSerno());
            // 合同编号
            toppAccPayDetailNew.setContNo(toppAccPayDetailOld.getContNo());
            // 借据编号
            toppAccPayDetailNew.setBillNo(toppAccPayDetailOld.getBillNo());
            // 客户编号
            toppAccPayDetailNew.setCusId(toppAccPayDetailOld.getCusId());
            // 客户名称
            toppAccPayDetailNew.setCusName(toppAccPayDetailOld.getCusName());
            // 支付对手账号
            toppAccPayDetailNew.setPayAccno(toppAccPayDetailOld.getPayAccno());
            // 支付对手名称
            toppAccPayDetailNew.setPayName(toppAccPayDetailOld.getPayName());
            // 交易对手账户
            toppAccPayDetailNew.setToppAccno(iqpChgTrupayAcctApp.getToppAccno());
            // 交易对手名称
            toppAccPayDetailNew.setToppName(iqpChgTrupayAcctApp.getToppName());
            // 交易对手金额
            toppAccPayDetailNew.setToppAmt(iqpChgTrupayAcctApp.getToppAmt());
            // 交易对手开户行号
            toppAccPayDetailNew.setToppBankNo(iqpChgTrupayAcctApp.getToppBankNo());
            // 交易对手开户行名称
            toppAccPayDetailNew.setToppBankName(iqpChgTrupayAcctApp.getToppBankName());
            // 冻结编号
            toppAccPayDetailNew.setFreezeNo(iqpChgTrupayAcctApp.getHxSerno());
            // 状态(0、待处理，1、处理中，2、成功，3、失败)
            toppAccPayDetailNew.setStatus("0");
            // 原因一
            toppAccPayDetailNew.setReasonFirst("");
            // 原因二
            toppAccPayDetailNew.setReasonSecond("");
            // 登记人
            toppAccPayDetailNew.setInputId(toppAccPayDetailOld.getInputId());
            // 登记机构
            toppAccPayDetailNew.setInputBrId(toppAccPayDetailOld.getInputBrId());
            // 登记日期
            toppAccPayDetailNew.setInputDate(toppAccPayDetailOld.getInputDate());
            int count = update(toppAccPayDetailNew);
            if (count <= 0) {
                log.info("根据冻结编号修改受托支付登记簿名单失败");
                throw new YuspException(EcbEnum.E_IQP_EXISTS.key, EcbEnum.E_IQP_EXISTS.value);
            } else {
                log.info("根据冻结编号修改受托支付登记簿名单成功");
                // 大额往帐一体化（发送二代支付系统进行受托）
                toppAccPayDetailNew = hvpsmrCall(toppAccPayDetailNew);
                toppAccPayDetailMapper.updateByPrimaryKey(toppAccPayDetailNew);
            }
        }else{
            log.info("根据受托账号修改受托支付登记簿名单失败,未查询到冻结编号【{}】,TA05-已退汇的数据", iqpChgTrupayAcctApp.getHxSerno());
        }
        log.info("根据受托账号修改信息创建受托支付登记簿名单结束，冻结编号：{}",iqpChgTrupayAcctApp.getHxSerno());
    }

    /**
     * 发起受托支付信息变更
     * @param serno
     * @return
     */
    public ToppAccPayDetail toppChgAppAdd(String serno) {
        log.info("根据受托账号修改信息生成受托支付修改任务开始,流水号为{}", serno);
        ToppAccPayDetail toppAccPayDetail = selectByPrimaryKey(serno);
        if(Objects.isNull(toppAccPayDetail)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value);
        }
        // 3:失败
        toppAccPayDetail.setStatus("3");
        // 原因二
        toppAccPayDetail.setReasonSecond("手工发起受托支付信息变更");
        // 生成受托支付信息变更任务
        createIqpChgTrupayAcctApp(toppAccPayDetail);
        this.updateSelective(toppAccPayDetail);
        log.info("根据受托账号修改信息生成受托支付修改任务结束,流水号为{}", serno);
        return toppAccPayDetail;
    }

    /**
     * @方法名称: selecctByBusNo
     * @方法描述: 根据业务受理编号（二代主键）查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ToppAccPayDetail selecctByBusNo(String busNo) {
        return toppAccPayDetailMapper.selecctByBusNo(busNo);
    }
}
