package cn.com.yusys.yusp.web.server.xdcz0030;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.Xdcz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.resp.Xdcz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0030.Xdcz0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:校验额度是否足额，合同是否足额
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDCZ0030:校验额度是否足额，合同是否足额")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0030Resource.class);

    @Autowired
    private Xdcz0030Service xdcz0030Service;

    /**
     * 交易码：xdcz0030
     * 交易描述：校验额度是否足额，合同是否足额
     *
     * @param xdcz0030DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("校验额度是否足额，合同是否足额")
    @PostMapping("/xdcz0030")
    protected @ResponseBody
    ResultDto<Xdcz0030DataRespDto> xdcz0030(@Validated @RequestBody Xdcz0030DataReqDto xdcz0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataReqDto));
        Xdcz0030DataRespDto xdcz0030DataRespDto = new Xdcz0030DataRespDto();// 响应Dto:校验额度是否足额，合同是否足额
        ResultDto<Xdcz0030DataRespDto> xdcz0030DataResultDto = new ResultDto<>();

        try {
            // 从xdcz0030DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataReqDto));
            xdcz0030DataRespDto = xdcz0030Service.xdcz0030(xdcz0030DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataRespDto));
            // 封装xdcz0030DataResultDto中正确的返回码和返回信息
            xdcz0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, e.getMessage());
            // 封装xdcz0030DataResultDto中异常返回码和返回信息
            xdcz0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }
        // 封装xdcz0030DataRespDto到xdcz0030DataResultDto中
        xdcz0030DataResultDto.setData(xdcz0030DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataResultDto));
        return xdcz0030DataResultDto;
    }
}
