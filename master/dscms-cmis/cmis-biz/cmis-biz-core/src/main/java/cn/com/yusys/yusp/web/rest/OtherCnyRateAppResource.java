/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import cn.com.yusys.yusp.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateApp;
import cn.com.yusys.yusp.dto.OtherCnyRateAppDto;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 10:14:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othercnyrateapp")
public class OtherCnyRateAppResource {
    private static final Logger log = LoggerFactory.getLogger(OtherCnyRateAppResource.class);

    @Autowired
    private OtherCnyRateAppService otherCnyRateAppService;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherCnyRateApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherCnyRateApp> list = otherCnyRateAppService.selectAll(queryModel);
        return new ResultDto<List<OtherCnyRateApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherCnyRateApp>> index(QueryModel queryModel) {
        List<OtherCnyRateApp> list = otherCnyRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateApp>>(list);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<OtherCnyRateApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherCnyRateApp> list = otherCnyRateAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherCnyRateApp> show(@PathVariable("serno") String serno) {
        OtherCnyRateApp otherCnyRateApp = otherCnyRateAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherCnyRateApp>(otherCnyRateApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherCnyRateApp> create(@RequestBody OtherCnyRateApp otherCnyRateApp) throws URISyntaxException {
        otherCnyRateAppService.insert(otherCnyRateApp);
        return new ResultDto<OtherCnyRateApp>(otherCnyRateApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherCnyRateApp otherCnyRateApp) throws URISyntaxException {
        int result = otherCnyRateAppService.update(otherCnyRateApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherCnyRateAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherCnyRateAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getothercnyrateapp
     * @方法描述: 根据入参获取当前客户经理名下人民币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getothercnyrateapp")
    protected ResultDto<List<OtherCnyRateApp>> getOtherCnyRateApp(@RequestBody QueryModel model) {
        List<OtherCnyRateApp> list = otherCnyRateAppService.getOtherCnyRateAppByModel(model);
        return new ResultDto<List<OtherCnyRateApp>>(list);
    }

    /**
     * @方法名称: getothercnyrateappHis
     * @方法描述: 根据入参获取当前客户经理名下人民币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getothercnyrateapphis")
    protected ResultDto<List<OtherCnyRateApp>> getOtherCnyRateAppHis(@RequestBody QueryModel model) {
        List<OtherCnyRateApp> list = otherCnyRateAppService.getOtherCnyRateAppHis(model);
        return new ResultDto<List<OtherCnyRateApp>>(list);
    }
    
    /**
     * 插入人民币定价利率及所有关联表
     * @param dto
     * @return
     */
    @PostMapping("/insertAll")
    protected ResultDto<OtherCnyRateAppDto> insertAll(@RequestBody OtherCnyRateAppDto dto) {
    	otherCnyRateAppService.insertOtherCnyRateAppDto(dto);
    	return new ResultDto<OtherCnyRateAppDto>(dto);
    }
    
    /**
     * 逻辑删除人民币定价利率及所有关联表
     * @return
     */
    @PostMapping("/updateAll/{serno}")
    protected ResultDto<Integer> updateAll(@PathVariable String serno) {
    	int result = otherCnyRateAppService.updateAll(serno);
    	return new ResultDto<>(result);
    }

    /**
     * 授信申请场景下逻辑删除人民币定价利率及所有关联表
     * @return
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = otherCnyRateAppService.updateAll(serno);
        return new ResultDto<>(result);
    }

    /**
     * TODOCJD
     * @return
     */
    @PostMapping("/getAppInfo")
    protected ResultDto<List<Map>> getAppInfo(@RequestBody Map map) {
        return otherCnyRateAppService.getAppInfo(map);
    }


}
