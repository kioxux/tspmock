package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.LmtGrpReplyAccOperAppService;
import cn.com.yusys.yusp.service.LmtReplyAccOperAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class DGSX03BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGSX03BizService.class);//定义log

    @Autowired
    private LmtReplyAccOperAppService lmtReplyAccOperAppService;

    @Autowired
    private LmtGrpReplyAccOperAppService lmtGrpReplyAccOperAppService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_SX016.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX017.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX018.equals(bizType)){
            handleLmtReplyOperBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_SX019.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX020.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX021.equals(bizType)){
            handleLmtGrpReplyOperBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 单一客户处理
    private void handleLmtReplyOperBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "对公额度冻结、解冻、终止审批流程(单一)"+serno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto.toString());
                lmtReplyAccOperAppService.handleBusinessAfterStart(serno);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto.toString());
                lmtReplyAccOperAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtReplyAccOperAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtReplyAccOperAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                lmtReplyAccOperAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 集团客户处理
    private void handleLmtGrpReplyOperBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String grpSerno, String currentUserId, String currentOrgId) {
        String logPrefix = "对公额度冻结、解冻、终止审批流程(集团)"+grpSerno+"流程操作:";
        log.info(logPrefix + currentOpType+"后业务处理");
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpReplyAccOperAppService.handleBusinessAfterStart(grpSerno);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpReplyAccOperAppService.handleBusinessAfterEnd(grpSerno, currentUserId, currentOrgId);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpReplyAccOperAppService.handleBusinessAfterBack(grpSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数："+ resultInstanceDto.toString());
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtGrpReplyAccOperAppService.handleBusinessAfterBack(grpSerno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数："+ resultInstanceDto.toString());
                lmtGrpReplyAccOperAppService.handleBusinessAfterRefuse(grpSerno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGSX03.equals(flowCode);
    }
}
