package cn.com.yusys.yusp.web.server.xdht0045;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0045.req.Xdht0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0045.resp.Xdht0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0045.Xdht0045Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:房群客户查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0045:查询信贷客户名称合同状态")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0045Resource.class);
    @Autowired
    private Xdht0045Service xdht0045Service;

    /**
     * 交易码：xdht0045
     * 交易描述：查询信贷客户名称合同状态
     *
     * @param xdht0045DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdht0045:查询信贷客户名称合同状态")
    @PostMapping("/xdht0045")
    protected @ResponseBody
    ResultDto<Xdht0045DataRespDto> xdht0045(@Validated @RequestBody Xdht0045DataReqDto xdht0045DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045DataReqDto));
        Xdht0045DataRespDto xdht0045DataRespDto = new Xdht0045DataRespDto();// 响应Dto:房群客户查询
        ResultDto<Xdht0045DataRespDto> xdht0045DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0045DataReqDto));
            xdht0045DataRespDto = xdht0045Service.xdht0045(xdht0045DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xdht0045DataResultDto));

            // 封装xdht0045DataResultDto中正确的返回码和返回信息
            xdht0045DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0045DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, e.getMessage());
            // 封装xdht0045DataResultDto中异常返回码和返回信息
            xdht0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0045DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, e.getMessage());
            // 封装xdht0045DataResultDto中异常返回码和返回信息
            xdht0045DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0045DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0045DataRespDto到xdht0045DataResultDto中
        xdht0045DataResultDto.setData(xdht0045DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045DataResultDto));
        return xdht0045DataResultDto;
    }
}
