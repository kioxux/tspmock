/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BusiImageRelInfo
 * @类描述: busi_image_rel_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-08 01:58:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "busi_image_rel_info")
public class BusiImageRelInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 外接编码 **/
	@Column(name = "TOP_OUTSYSTEM_CODE", unique = false, nullable = true, length = 40)
	private String topOutsystemCode;
	
	/** 影像类型 **/
	@Column(name = "IMAGE_DESC", unique = false, nullable = true, length = 200)
	private String imageDesc;
	
	/** 关键字类别 **/
	@Column(name = "KEYWORD_TYPE", unique = false, nullable = true, length = 40)
	private String keywordType;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 80)
	private String imageNo;
	
	/** 权限 **/
	@Column(name = "AUTHORITY", unique = false, nullable = true, length = 100)
	private String authority;
	
	/** 影像顺序 **/
	@Column(name = "IMAGE_ORDER", unique = false, nullable = true, length = 10)
	private Integer imageOrder;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param topOutsystemCode
	 */
	public void setTopOutsystemCode(String topOutsystemCode) {
		this.topOutsystemCode = topOutsystemCode;
	}
	
    /**
     * @return topOutsystemCode
     */
	public String getTopOutsystemCode() {
		return this.topOutsystemCode;
	}
	
	/**
	 * @param imageDesc
	 */
	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}
	
    /**
     * @return imageDesc
     */
	public String getImageDesc() {
		return this.imageDesc;
	}
	
	/**
	 * @param keywordType
	 */
	public void setKeywordType(String keywordType) {
		this.keywordType = keywordType;
	}
	
    /**
     * @return keywordType
     */
	public String getKeywordType() {
		return this.keywordType;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param authority
	 */
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	
    /**
     * @return authority
     */
	public String getAuthority() {
		return this.authority;
	}
	
	/**
	 * @param imageOrder
	 */
	public void setImageOrder(Integer imageOrder) {
		this.imageOrder = imageOrder;
	}
	
    /**
     * @return imageOrder
     */
	public Integer getImageOrder() {
		return this.imageOrder;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}