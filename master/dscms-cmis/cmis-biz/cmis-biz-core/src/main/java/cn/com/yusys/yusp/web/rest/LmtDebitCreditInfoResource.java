/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtDebitCreditInfo;
import cn.com.yusys.yusp.service.LmtDebitCreditInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitCreditInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-15 22:24:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "借款人征信信息")
@RequestMapping("/api/lmtdebitcreditinfo")
public class LmtDebitCreditInfoResource {
    @Autowired
    private LmtDebitCreditInfoService lmtDebitCreditInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtDebitCreditInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtDebitCreditInfo> list = lmtDebitCreditInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtDebitCreditInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtDebitCreditInfo>> index(QueryModel queryModel) {
        List<LmtDebitCreditInfo> list = lmtDebitCreditInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtDebitCreditInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{surveyNo}")
    protected ResultDto<LmtDebitCreditInfo> show(@PathVariable("surveyNo") String surveyNo) {
        LmtDebitCreditInfo lmtDebitCreditInfo = lmtDebitCreditInfoService.selectByPrimaryKey(surveyNo);
        return new ResultDto<LmtDebitCreditInfo>(lmtDebitCreditInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtDebitCreditInfo> create(@RequestBody LmtDebitCreditInfo lmtDebitCreditInfo) throws URISyntaxException {
        lmtDebitCreditInfoService.insert(lmtDebitCreditInfo);
        return new ResultDto<LmtDebitCreditInfo>(lmtDebitCreditInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtDebitCreditInfo lmtDebitCreditInfo) throws URISyntaxException {
        int result = lmtDebitCreditInfoService.update(lmtDebitCreditInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{surveyNo}")
    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
        int result = lmtDebitCreditInfoService.deleteByPrimaryKey(surveyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtDebitCreditInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtdebitcreditinfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto>
     * @author hubp
     * @date 2021/5/24 15:46
     * @version 1.0.0
     * @desc    根据征信风险指标查询信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectdebit")
    protected ResultDto<CredzbRespDto> selectDebit(@RequestBody LmtDebitCreditInfo lmtdebitcreditinfo) {
        CredzbRespDto result = lmtDebitCreditInfoService.selectDebit(lmtdebitcreditinfo.getSurveySerno());
        return new ResultDto<CredzbRespDto>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/19 16:03
     * @注释 查询单挑数据
     */
    @PostMapping("/selectone")
    protected ResultDto selectone(@RequestBody LmtDebitCreditInfo lmtdebitcreditinfo) {
        return  lmtDebitCreditInfoService.selectDebitAndCom(lmtdebitcreditinfo.getSurveySerno());
    }
}
