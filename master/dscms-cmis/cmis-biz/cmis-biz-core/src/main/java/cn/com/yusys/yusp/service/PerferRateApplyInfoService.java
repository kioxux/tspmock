package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.PerferRateApplyInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PerferRateApplyInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: PerferRateApplyInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-13 10:17:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PerferRateApplyInfoService {

    private static final Logger logger = LoggerFactory.getLogger(PerferRateApplyInfo.class);

    @Autowired(required = false)
    private PerferRateApplyInfoMapper perferRateApplyInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public PerferRateApplyInfo selectByPrimaryKey(String surveyNo) {
        return perferRateApplyInfoMapper.selectByPrimaryKey(surveyNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<PerferRateApplyInfo> selectAll(QueryModel model) {
        List<PerferRateApplyInfo> records = (List<PerferRateApplyInfo>) perferRateApplyInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<PerferRateApplyInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PerferRateApplyInfo> list = perferRateApplyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(PerferRateApplyInfo record) {
        return perferRateApplyInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(PerferRateApplyInfo record) {
        return perferRateApplyInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(PerferRateApplyInfo record) {
        return perferRateApplyInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(PerferRateApplyInfo record) {
        return perferRateApplyInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveyNo) {
        return perferRateApplyInfoMapper.deleteByPrimaryKey(surveyNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return perferRateApplyInfoMapper.deleteByIds(ids);
    }

    /**
     * @param perferRateApplyInfo
     * @return cn.com.yusys.yusp.domain.PerferRateApplyInfo
     * @author 王玉坤
     * @date 2021/4/16 17:18
     * @version 1.0.0
     * @desc 根据调查流水号插入或更新优惠利率信息
     * @修改历史:
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Map> saveBySurveyNo(PerferRateApplyInfo perferRateApplyInfo) {
        // 更新、插入标识
        int nums = -1;
        // 返回码
        int code = 0;
        // 返回信息
        String msg = "";

        // 临时变量
        PerferRateApplyInfo perferRateApplyInfoTemp = null;
        try {
            // 根据流水号查询优惠利率申请信息，有则更新、无则插入
            perferRateApplyInfoTemp = selectByPrimaryKey(perferRateApplyInfo.getSurveyNo());
            // 根据调查流水号查询优惠利率申请信息
            if (perferRateApplyInfoTemp != null) {
                // 已处于审批中的流程不允许提交
                if (!CmisCommonConstants.WF_STATUS_000.equals(perferRateApplyInfoTemp.getApproveoveStatus())) {
                    throw new YuspException(EcbEnum.ECB010005.key, EcbEnum.ECB010005.value);
                }
//                perferRateApplyInfo.setUpdateTime(DateUtils.getCurrDate());
                //perferRateApplyInfoMapper.updateByPrimaryKeySelective

                int i = perferRateApplyInfoMapper.updateByPrimaryKeySelective(perferRateApplyInfo);
                logger.info("根据调查流水号{}更新成功，更新条数{}", perferRateApplyInfo.getSurveyNo(), nums);
            } else {
                perferRateApplyInfo.setApproveoveStatus(CmisCommonConstants.WF_STATUS_000);
                perferRateApplyInfo.setUpdateTime(DateUtils.getCurrDate());
                perferRateApplyInfo.setCreateTime(DateUtils.getCurrDate());
                nums = insert(perferRateApplyInfo);
                logger.info("根据调查流水号{}插入成功，插入条数{}", perferRateApplyInfo.getSurveyNo(), nums);
            }
            code = nums > 0 ? code : -1;
            msg = nums > 0 ? "处理成功" : "系统异常";
        } catch (YuspException e) {
            code = -1;
            msg = e.getMsg();
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            code = -1;
            msg = "系统异常";
            logger.error(e.getMessage(), e);
        }
        return new ResultDto<Map>(code, msg);
    }
}
