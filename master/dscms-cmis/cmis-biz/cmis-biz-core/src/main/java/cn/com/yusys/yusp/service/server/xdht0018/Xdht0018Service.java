package cn.com.yusys.yusp.service.server.xdht0018;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdht0018.req.Xdht0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0018.resp.Xdht0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0018Service
 * @类描述: #服务类
 * @功能描述: 担保合同文本生成pdf
 * @创建人:
 * @创建时间: 2021-06-15 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0018Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0018Service.class);

    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;
    @Autowired
    private CommonService commonService;


    /**
     * @param xdht0018DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0018DataRespDto xdht0018(Xdht0018DataReqDto xdht0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, "请求参数：" + JSON.toJSONString(xdht0018DataReqDto));
        Xdht0018DataRespDto xdht0018DataRespDto = new Xdht0018DataRespDto();
        try {
            /***************************************获取请求参数值***************************************/
            String cusId = xdht0018DataReqDto.getCusId();//签订人id
            String contNo = xdht0018DataReqDto.getContNo();//合同编号
            String grtContNo = xdht0018DataReqDto.getGrtContNo();//担保合同号
            String applyDate = xdht0018DataReqDto.getApplyDate();//申请日期
            //***************************************查询担保合同状态***************************************
            logger.info("根据担保合同编号【{}】查询担保合同信息开始", grtContNo);
            GrtGuarCont grtGuarContInfo = Optional.ofNullable(grtGuarContMapper.queryGrtGuarContByGuarContNo(grtContNo)).orElse(new GrtGuarCont());
            logger.info("***************根据担保合同编号【{}】查询担保合同信息结束,查询结果信息【{}】", grtContNo, JSON.toJSONString(grtGuarContInfo));
            String guarContState = (null == grtGuarContInfo.getGuarContState() ? "" : grtGuarContInfo.getGuarContState()); //担保合同状态
            String signDate = (null == grtGuarContInfo.getSignDate() ? "" : grtGuarContInfo.getSignDate()); //签订日期
            //如果GUAR_CONT_STATE为“101”，返回"您已在【" + SIGN_DATE + "】签约过本笔担保合同！"；
            if ("101".equals(guarContState)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "您已签约过本笔担保合同！");
            }


            // 判断当前保证人能否签订保证担保合同（按照担保编号顺序签订+未签约） 本逻辑参考XDHT0012接口oprType=01模块 ，有相同的处理（已经粘过来啦）
            logger.info("根据担保合同编号【{}】查询保证人信息开始", grtContNo);
            HashMap<String, Object> map = guarGuaranteeMapper.selectlListByDbContNo(grtContNo);
            logger.info("***************根据担保合同编号【{}】查询保证人信息结束,查询结果信息【{}】", grtContNo, JSON.toJSONString(map));
            if (map != null) {
                String cusid_list = map.get("cusidList").toString();
                String cusname_list = map.get("cusnameList").toString();
                if (!cusid_list.startsWith(cusId)) {
                    //throw new Exception("按照担保编号顺序签订规则【" + cusname_list + "】，当前客户签约序号未到！");
                    xdht0018DataRespDto.setPassword("0123456");
                    xdht0018DataRespDto.setUserName("按照担保编号顺序签订规则【" + cusname_list + "】，当前客户签约序号未到！");
                    return xdht0018DataRespDto;
                }
            }
            //查询担保合同信息，获得如下字段
            String guarContCnNo = grtGuarContInfo.getGuarContCnNo();//担保合同中文合同编号
            String guarContType = grtGuarContInfo.getGuarContType();//担保合同类型 STD_ZB_GRT_TYP
            String guarWay = grtGuarContInfo.getGuarWay();//担保合同担保方式 STD_ZB_GUAR_WAY
            String managerBrId = grtGuarContInfo.getManagerBrId();//主办机构
            String pldContType = grtGuarContInfo.getPldContType();//抵质押合同类型
            String type = StringUtils.EMPTY;//对公对私类型
            logger.info("****************根据客户号【{}】获取客户证件号开始", cusId);
            CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);
            logger.info("****************根据客户号【{}】获取客户证件号开始", JSON.toJSONString(cusBaseDto));
            if (cusBaseDto != null) {
                type = cusBaseDto.getCusCatalog();//客户大类 1-对私；2-对公
            } else {
                type = "1";//对私
            }
            //定义合同模板名称和生成后的文件名(下面这一大段逻辑直接复用)
            String TempleteName = "";// 合同模板名字
            String saveFileName = "";// 合同另保存名字 （模板名字+合同申请流水号）

            logger.info("#########################根据担保合同类型【{}】,担保方式【{}】判断对应模板开始#####################", grtContNo, guarWay);
            if ("A".equals(guarContType)) {
                if ("20".equals(guarWay)) {//质押担保合同
                    if ("01".equals(pldContType)) {//动产质押担保合同
                        TempleteName = "ybdczydbht.cpt";// 合同模板名字 xdybdczydbht.docx
                        saveFileName = "xdybdczydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    } else if ("02".equals(pldContType)) {//权利质押担保合同
                        TempleteName = "ybqlzydbht.cpt";// 合同模板名字 xdybqlzydbht.docx
                        saveFileName = "xdybqlzydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    } else if ("03".equals(pldContType)) {//股权质押担保合同
                        TempleteName = "ybqlzydbht.cpt";// 合同模板名字 gqzyhtqdyb.docx （权利质押担保合同）
                        saveFileName = "gqzyhtqdyb_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("810100".equals(managerBrId) && !"".equals(TempleteName)) {
                        TempleteName = "dh" + TempleteName;
                        saveFileName = "dh" + saveFileName;
                    } else if ("800100".equals(managerBrId) && !"".equals(TempleteName)) {
                        TempleteName = "sg" + TempleteName;
                        saveFileName = "sg" + saveFileName;
                    }
                } else if ("10".equals(guarWay)) {//抵押担保合同
                    // 个人抬头的担保合同
                    if ("1".equals(type)) {//对私抵押担保
                        String gcount = guarBaseInfoMapper.queryCountByContNo(grtContNo);// 车辆抵押
                        if (!"0".equals(gcount)) {
                            TempleteName = "zgedydbht.cpt";//xdybgrdydbhtcar.docx
                        } else {
                            TempleteName = "zgedydbht.cpt";// 合同模板名字 xdybgrdydbht.docx
                        }
                        saveFileName = "xdybgrdydbht_" + grtContNo + "";
                    } else {// 对公抵押担保
                        TempleteName = "dagl-dayjqd.cpt";// 合同模板名字 xdybdgdydbht.docx
                        saveFileName = "xdybdgdydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("810100".equals(managerBrId)) {//东海
                        TempleteName = "dh" + TempleteName;
                        saveFileName = "dh" + saveFileName;
                    } else if ("800100".equals(managerBrId)) {//寿光
                        TempleteName = "sg" + TempleteName;
                        saveFileName = "sg" + saveFileName;
                    }
                } else { //保证担保合同
                    if ("1".equals(type)) {//对私保证担保
                        TempleteName = "line_ybbzdbht.cpt";// 合同模板名字 xdybgrbzdbht.docx
                        saveFileName = "xdybgrbzdbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    } else {//对公保证担保
                        TempleteName = "ybbzdbht.cpt";// 合同模板名字 xdybdgbzdbht.docx
                        saveFileName = "xdybdgbzdbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("810100".equals(managerBrId)) {//东海
                        TempleteName = "dh" + TempleteName;
                        saveFileName = "dh" + saveFileName;
                    } else if ("800100".equals(managerBrId)) {//寿光
                        TempleteName = "sg" + TempleteName;
                        saveFileName = "sg" + saveFileName;
                    }
                }
            } else {
                // 2最高额担保合同
                if ("20".equals(guarWay)) {
                    if ("01".equals(pldContType)) {//动产质押
                        TempleteName = "zgedczydbht.cpt";// 合同模板名字 xdzgedczydbht.docx
                        saveFileName = "xdzgedczydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    } else if ("02".equals(pldContType)) {//权利质押
                        TempleteName = "zgeqlzydbht.cpt";// 合同模板名字 xdzgeqlzydbht.docx
                        saveFileName = "xdzgeqlzydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    } else if ("03".equals(pldContType)) {//股权质押
                        TempleteName = "zgeqlzydbht.cpt";// 合同模板名字 gqzyhtqdyb.docx (最高额权利质押担保合同)
                        saveFileName = "gqzyhtqdyb_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                    }
                    if ("810100".equals(managerBrId) && !"".equals(TempleteName)) {
                        TempleteName = "dh" + TempleteName;
                        saveFileName = "dh" + saveFileName;
                    } else if ("800100".equals(managerBrId) && !"".equals(TempleteName)) {
                        TempleteName = "sg" + TempleteName;
                        saveFileName = "sg" + saveFileName;
                    }
                } else {
                    if ("10".equals(guarWay)) {//抵押担保合同
                        TempleteName = "zgedydbht.cpt";// 合同模板名字 xdzgedydbht.docx
                        saveFileName = "xdzgedydbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                        if ("810100".equals(managerBrId)) {//东海
                            TempleteName = "dh" + TempleteName;
                            saveFileName = "dh" + saveFileName;
                        } else if ("800100".equals(managerBrId)) {//寿光
                            TempleteName = "sg" + TempleteName;
                            saveFileName = "sg" + saveFileName;
                        }
                    } else {                         //保证担保合同
                        if ("1".equals(type)) {//对私保证担保
                            TempleteName = "line_zgebzdbht.cpt";// 合同模板名字 xdzgegrbzdbht.docx
                            saveFileName = "xdzgegrbzdbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                        } else {//对公保证担保
                            TempleteName = "zgebzdbht.cpt";// 合同模板名字 xdzgedgbzdbht.docx
                            saveFileName = "xdzgedgbzdbht_" + grtContNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
                        }
                        if ("810100".equals(managerBrId)) {//东海
                            TempleteName = "dh" + TempleteName;
                            saveFileName = "dh" + saveFileName;
                        } else if ("800100".equals(managerBrId)) {//寿光
                            TempleteName = "sg" + TempleteName;
                            saveFileName = "sg" + saveFileName;
                        }
                    }
                }
            }
            logger.info("#########################根据担保合同类型【{}】,担保方式【{}】判断对应模板结束，模板类型为【{}】#####################", grtContNo, guarWay, TempleteName);

            /******************************************查询服务器配置*************************************************/
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            /******************************************查询存储地址信息**************************************************/
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            /******************************************传入帆软报表生成需要的参数*******************************/
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("guarContNo", grtContNo);
            parameterMap.put("contNo", contNo);
            parameterMap.put("cusId", cusId);
            /**************************************调用公共方法生成pdf******************************************/
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(grtContNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                //HttpEntity<HashMap> entity = new HttpEntity<HashMap>(parameterMap,headers);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
            /**************************************接口返回结果**************************************/
            //String pdfDepoAddr = saveFileName + "#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
            xdht0018DataRespDto.setPdfDepoAddr(path);//pdf存放地址
            xdht0018DataRespDto.setPdfFileName(saveFileName + ".pdf");//pdf文件名称
            xdht0018DataRespDto.setFtpAddr(ip);//ftp地址
            xdht0018DataRespDto.setPort(port);//登录端口
            xdht0018DataRespDto.setUserName(username);//登录用户名
            xdht0018DataRespDto.setPassword(password);//登录密码
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataRespDto));
        return xdht0018DataRespDto;
    }
}
