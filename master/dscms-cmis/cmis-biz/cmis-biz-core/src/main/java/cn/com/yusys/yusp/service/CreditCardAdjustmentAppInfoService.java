package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.R005;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto;
import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.CreditCardAdjustmentAppInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credxx.CredxxService;
import cn.com.yusys.yusp.util.CmisBizCiis2ndUtils;
import cn.com.yusys.yusp.vo.CreditCardAdjustmentListVo;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAdjustmentAppInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-24 15:54:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardAdjustmentAppInfoService {

    private static final Logger log = LoggerFactory.getLogger(CreditCardAdjustmentAppInfoService.class);

    @Autowired
    private CreditCardAdjustmentAppInfoMapper creditCardAdjustmentAppInfoMapper;
    @Autowired
    private CreditCardAdjustmentAppInfoService creditCardAdjustmentAppInfoService;
    @Autowired
    private CreditCardAdjustmentJudgInifoService creditCardAdjustmentJudgInifoService;
    @Autowired
    private CreditCardAdjustmentAmtInfoService creditCardAdjustmentAmtInfoService;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CredxxService credxxService;
    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;
    @Autowired
    private CreditQryBizRealService creditQryBizRealService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;
    @Value("${application.creditUrl.url}")
    private String creditUrl;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CreditCardAdjustmentAppInfo selectByPrimaryKey(String serno) {
        return creditCardAdjustmentAppInfoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CreditCardAdjustmentAppInfo> selectAll(QueryModel model) {
        List<CreditCardAdjustmentAppInfo> records = (List<CreditCardAdjustmentAppInfo>) creditCardAdjustmentAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardAdjustmentAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModelStatus
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardAdjustmentAppInfo> selectByModelStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoMapper.selectByModelStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: model
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardAdjustmentAppInfo> selectByNotStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAdjustmentAppInfo> list = creditCardAdjustmentAppInfoMapper.selectByNotStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CreditCardAdjustmentAppInfo record) {
        return creditCardAdjustmentAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int save(CreditCardAdjustmentAppInfo record) {
        D14020ReqDto reqDto = new D14020ReqDto();
        reqDto.setCardno(record.getCardNo());
        ResultDto<D14020RespDto> repDto = dscms2TonglianClientService.d14020(reqDto);
        if (!"0".equals(repDto.getCode())) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, repDto.getMessage());
        }
        if (repDto.getData() == null) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该卡号不存在！");
        }
        if ("" != repDto.getData().getBkcode() && null != repDto.getData().getBkcode()) {
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该卡状态不正确，不可以进行额度申请！");
        }
        log.info("更新审批中参数开始："+record.getSerno());
        List<CreditCardAdjustmentJudgInifo>  list  = creditCardAdjustmentJudgInifoService.selectAllBySerno(record.getSerno());
        if(list != null){
            if(list.size()>0){
                for(int i =0;i<list.size();i++){
                    CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = list.get(i);
                    creditCardAdjustmentJudgInifo.setApproveAmt(record.getNewCreditCardLmt());
                    creditCardAdjustmentJudgInifoService.updateSelective(creditCardAdjustmentJudgInifo);
                }
            }
        }
        log.info("更新审批中参数结束："+record.getSerno());
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = new CreditCardAdjustmentAppInfo();
        creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(record.getSerno());
        if (creditCardAdjustmentAppInfo != null) {
            return creditCardAdjustmentAppInfoMapper.updateByPrimaryKeySelective(record);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setAppDate(simpleDateFormat.format(new Date()));
            record.setOprType("01");
            record.setAdjustmentChnl("00");
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            return creditCardAdjustmentAppInfoMapper.insert(record);
        }

    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CreditCardAdjustmentAppInfo record) {
        return creditCardAdjustmentAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CreditCardAdjustmentAppInfo record) {
        return creditCardAdjustmentAppInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CreditCardAdjustmentAppInfo record) {
        return creditCardAdjustmentAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return creditCardAdjustmentAppInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: cutByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int cutByPrimaryKey(String serno) {
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = creditCardAdjustmentAppInfoService.selectByPrimaryKey(serno);
        if (creditCardAdjustmentAppInfo != null) {
            //如果需要删除的额度申请为渠道进件，不可以删除
            log.info("删除调额申请" + serno + "渠道进件不可删除");
            if ("02".equals(creditCardAdjustmentAppInfo.getAdjustmentChnl())) {
                throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, "渠道进件类型调额申请不可删除！");
            }
            log.info("删除调额申请" + serno + "非待发起以及自行退回不可删除");
            //如果需要删除的额度申请状态不是待发起，不可以删除
            if (!CmisCommonConstants.WF_STATUS_000.equals(creditCardAdjustmentAppInfo.getApproveStatus()) && !CmisCommonConstants.WF_STATUS_992.equals(creditCardAdjustmentAppInfo.getApproveStatus())) {
                throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, "该调额申请不可删除！");
            }
            //如果需要删除的额度申请状态是退回，将状态改为自行退出
            else if (CmisCommonConstants.WF_STATUS_992.equals(creditCardAdjustmentAppInfo.getApproveStatus())) {
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId："+serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
                return creditCardAdjustmentAppInfoMapper.changeApproveStatusAndOprType(serno, CmisCommonConstants.WF_STATUS_996, "02");
//                return creditCardAdjustmentAppInfoMapper.changeApproveStatus(serno,"5");
            } else {
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId：",serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
                return creditCardAdjustmentAppInfoMapper.deleteByPrimaryKey(serno);
            }
        } else {
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "未找到该调额申请！");
        }
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return creditCardAdjustmentAppInfoMapper.deleteByIds(ids);
    }

    /**
     * @param serno
     * @return
     * @author zsm
     * @date 2021/5/26 9:57
     * @version 1.0.0
     * @desc 额度申请流程处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void handleBusinessDataAfterEnd(String serno) {

        if (StringUtils.isBlank(serno)) {
//            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            throw BizException.error(null, EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }
        log.info("流程发起-获取调额申请" + serno + "调额申请信息");
        CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = this.selectByPrimaryKey(serno);
        log.info("流程发起-更新额度申请" + serno + "流程审批状态为【997】-已通过");
        creditCardAdjustmentAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        int result = this.updateSelective(creditCardAdjustmentAppInfo);
        if (result < 0) {
            throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, "更新调额申请失败！");
//                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        log.info("流程发起-新增调额申请金额表" + serno + "调额申请金额");
        CreditCardAdjustmentAmtInfo creditCardAdjustmentAmtInfo = new CreditCardAdjustmentAmtInfo();
        creditCardAdjustmentAmtInfo.setSerno(creditCardAdjustmentAppInfo.getSerno());
        creditCardAdjustmentAmtInfo.setApplyCardPrd(creditCardAdjustmentAppInfo.getCardPrd());
        creditCardAdjustmentAmtInfo.setCardNo(creditCardAdjustmentAppInfo.getCardNo());
        creditCardAdjustmentAmtInfo.setOrigCreditCardLmt(creditCardAdjustmentAppInfo.getOrigCreditCardLmt());
        CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifo = creditCardAdjustmentJudgInifoService.selectBySernoAndApprovePostFlag(serno, "01");
        BigDecimal approveAmt = BigDecimal.ZERO;
        if (creditCardAdjustmentJudgInifo != null) {
            approveAmt = creditCardAdjustmentJudgInifo.getApproveAmt();
            creditCardAdjustmentAmtInfo.setApproveAmt(creditCardAdjustmentJudgInifo.getApproveAmt());
        } else {
            CreditCardAdjustmentJudgInifo creditCardAdjustmentJudgInifoOne = creditCardAdjustmentJudgInifoService.selectBySernoAndApprovePostFlag(serno, "00");
            approveAmt = creditCardAdjustmentJudgInifoOne.getApproveAmt();
            creditCardAdjustmentAmtInfo.setApproveAmt(creditCardAdjustmentJudgInifoOne.getApproveAmt());
        }
        creditCardAdjustmentAmtInfo.setApproveAmt(approveAmt);
        creditCardAdjustmentAmtInfoService.insert(creditCardAdjustmentAmtInfo);
        D15011ReqDto reqDto = new D15011ReqDto();
        reqDto.setCardno(creditCardAdjustmentAppInfo.getCardNo());
        reqDto.setCrcycd("156");
        reqDto.setOpt("1");
        reqDto.setCdtlmt(approveAmt);
        //调用通联15011接口完成额度调整
        ResultDto<D15011RespDto> d15011RespDtoResultDto = dscms2TonglianClientService.d15011(reqDto);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        log.info("异步导出模板");
        ExportContext exportContext = ExportContext.of(CreditCardAdjustmentListVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param creditCardAdjustmentListVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<CreditCardAdjustmentListVo> creditCardAdjustmentListVoList) {
        log.info("批量插入");
        List<CreditCardAdjustmentAppInfo> creditCardAdjustmentAppInfoList = (List<CreditCardAdjustmentAppInfo>) BeanUtils.beansCopy(creditCardAdjustmentListVoList, CreditCardAdjustmentAppInfo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CreditCardAdjustmentAppInfoMapper creditCardAdjustmentAppInfoMapper = sqlSession.getMapper(CreditCardAdjustmentAppInfoMapper.class);
            for (CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo : creditCardAdjustmentAppInfoList) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String  serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
                User user = SessionUtils.getUserInformation();
                UserIdentity org = user.getOrg();
                creditCardAdjustmentAppInfo.setSerno(serno);
                creditCardAdjustmentAppInfo.setAppDate(simpleDateFormat.format(new Date()));
                creditCardAdjustmentAppInfo.setOprType("01");
                creditCardAdjustmentAppInfo.setAdjustmentChnl("01");
                creditCardAdjustmentAppInfo.setApproveStatus("000");
                creditCardAdjustmentAppInfo.setInputId(user.getLoginCode());
                creditCardAdjustmentAppInfo.setInputBrId(org.getCode());
                creditCardAdjustmentAppInfoMapper.insertSelective(creditCardAdjustmentAppInfo);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return creditCardAdjustmentListVoList.size();
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/26 16:42
     * @version 1.0.0
     * @desc 大额分期申请征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int getCreditReportInfo(CreditCardAdjustmentAppInfo reditCardAdjustmentAppInfo) {
        try {
            if (reditCardAdjustmentAppInfo == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            String serno = reditCardAdjustmentAppInfo.getSerno();
            log.info("征信查询开始 {}", serno);
            int updateCount = 0;
            String achieveDate = "";
            //取系统日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date nowDay = simpleDateFormat.parse(openDay);
            Date oldday = DateUtils.addYear(nowDay,-50);
            //查询通用指标接口
            CredzbReqDto reqDto = new CredzbReqDto();
            reqDto.setRuleCode("R003");
            reqDto.setReqId(serno);
            reqDto.setBrchno(reditCardAdjustmentAppInfo.getInputBrId());
            reqDto.setCertificateNum(reditCardAdjustmentAppInfo.getCertCode());
            reqDto.setCustomName(reditCardAdjustmentAppInfo.getCusName());
            reqDto.setStartDate(String.valueOf(DateUtils.formatDate(oldday, "yyyy-MM-dd")));
            reqDto.setEndDate(openDay);
            ResultDto<CredzbRespDto> repDto = dscms2Ciis2ndClientService.credzb(reqDto);
            String code = repDto.getCode();
            if ("0".equals(code)) {
                CredzbRespDto credzbRespDto = repDto.getData();
                if (credzbRespDto == null) {
                    throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, "该客户30天内无征信报告！请联系客户经理");
                }
            } else {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, repDto.getMessage());
            }
            //查询征信授权日期
            CredzbReqDto reqDtoDate = new CredzbReqDto();
            reqDtoDate.setRuleCode("R005");
            reqDtoDate.setCertificateNum(reditCardAdjustmentAppInfo.getCertCode());
            reqDtoDate.setBrchno(reditCardAdjustmentAppInfo.getInputBrId());
            reqDtoDate.setBorrowPersonRelation("");
            reqDtoDate.setAuditReason("");
            reqDtoDate.setStartDate(DateUtils.formatDate(oldday, "yyyy-MM-dd"));
            reqDtoDate.setEndDate(openDay);
            ResultDto<CredzbRespDto> repDtoDate = dscms2Ciis2ndClientService.credzb(reqDtoDate);
            if (repDtoDate.getCode().equals(code)) {
                CredzbRespDto credzbRespDto = repDtoDate.getData();
                if (credzbRespDto != null) {
                    R005 r005 = repDtoDate.getData().getR005();
                    if (r005 != null) {
                        achieveDate = r005.getARCHIVECREATEDATE();
                    } else {
                        throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, "未查询到征信授权签订日期!请联系客户经理");
                    }
                } else {
                    throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, repDtoDate.getMessage());
                }
            } else {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, repDtoDate.getMessage());
            }
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(reditCardAdjustmentAppInfo.getInputId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            CredxxReqDto credxxReqDto = new CredxxReqDto();
            credxxReqDto.setCreditType("0");//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(reditCardAdjustmentAppInfo.getCusName());//客户名称
            credxxReqDto.setCertificateType(toConverCurrency(reditCardAdjustmentAppInfo.getCertType()));//证件类型
            credxxReqDto.setCertificateNum(reditCardAdjustmentAppInfo.getCertCode());//证件号
            credxxReqDto.setQueryReason("01");//查询原因 贷后
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserId(reditCardAdjustmentAppInfo.getInputId());//客户经理工号
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("30");//信用报告复用策略
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            //总行：朱真尧 东海：邵雯
            if (reditCardAdjustmentAppInfo.getInputBrId().startsWith("81")) {
                credxxReqDto.setApprovalName("邵雯");//审批人姓名
                credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
            } else {
                credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
            }
            // credxxReqDto.setApprovalName(adminSmUserDto.getUserName());//审批人姓名
            // credxxReqDto.setApprovalIdCard(adminSmUserDto.getCertNo());//审批人身份证号
            credxxReqDto.setArchiveCreateDate(achieveDate);//授权书签订日期
            //生成征信流水号
            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            credxxReqDto.setCreditDocId(crqlSerno);//授权影像编号
            credxxReqDto.setBorrowPersonRelation("001");//与主借款人关系
            String expirDate = DateUtils.addYear(achieveDate,"yyyy-MM-dd",50);
            credxxReqDto.setArchiveExpireDate(expirDate);//授权结束日期
            credxxReqDto.setBorrowPersonRelationName(reditCardAdjustmentAppInfo.getCusName());//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber(reditCardAdjustmentAppInfo.getCertCode());//主借款人证件号
            credxxReqDto.setAuditReason("002;004");//授权书内容
            credxxReqDto.setBrchno(reditCardAdjustmentAppInfo.getInputBrId());
            log.info("发送征信接口" + credxxReqDto);
            CredxxRespDto credxxRespDto = credxxService.credxx(credxxReqDto);
            log.info("**************征信接口返回报文**********" + credxxRespDto);
            String zxSerno = credxxRespDto.getReportId();
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            creditReportQryLst.setAuthbookNo(authbookNoSeq);
            creditReportQryLst.setAuthbookDate(achieveDate);
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setReportNo(zxSerno);
            creditReportQryLst.setBorrowRel("001");//主借款人
            creditReportQryLst.setQryCls("0");
            creditReportQryLst.setApproveStatus("997");//审批通过
            creditReportQryLst.setQryFlag("04");
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setReportNo(zxSerno);
            if (!StringUtils.isEmpty(zxSerno)) {
                creditReportQryLst.setImageNo(crqlSerno);
                creditReportQryLst.setQryCls("0"); //线下
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setReportCreateTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                creditReportQryLst.setSendTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                creditReportQryLst.setQryStatus("003");
                CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
                callCiis2ndReqDto.setPrefixUrl(creditUrl);
                callCiis2ndReqDto.setReqId(credxxRespDto.getReqId());
                callCiis2ndReqDto.setOrgName("信贷");
                callCiis2ndReqDto.setUserCode(reditCardAdjustmentAppInfo.getInputId());
                callCiis2ndReqDto.setUserName(adminSmUserDto.getUserName());
                callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
                String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
                creditReportQryLst.setCreditUrl(url);
                creditReportQryLst.setCertType(reditCardAdjustmentAppInfo.getCertType());
                creditReportQryLst.setCertCode(reditCardAdjustmentAppInfo.getCertCode());
                creditReportQryLst.setCusId("");
                creditReportQryLst.setCusName(reditCardAdjustmentAppInfo.getCusName());
                creditReportQryLst.setBorrowerCusName(reditCardAdjustmentAppInfo.getCusName());
                creditReportQryLst.setBorrowerCertCode(reditCardAdjustmentAppInfo.getCertCode());
                creditReportQryLst.setManagerBrId(reditCardAdjustmentAppInfo.getInputBrId());
                creditReportQryLst.setManagerId(reditCardAdjustmentAppInfo.getInputId());
                creditReportQryLst.setQryResn("27");//征信查询原因 贷后
                creditReportQryLst.setAuthbookContent("002;007");//授权书内容
                //生成业务与征信关系表
                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                creditQryBizReal.setBizSerno(serno);
                creditQryBizReal.setCrqlSerno(crqlSerno);
                creditQryBizReal.setCertType(reditCardAdjustmentAppInfo.getCertType());
                creditQryBizReal.setCertCode(reditCardAdjustmentAppInfo.getCertCode());
                creditQryBizReal.setCusId("");
                creditQryBizReal.setCusName(reditCardAdjustmentAppInfo.getCusName());
                creditQryBizReal.setBorrowRel("001");
                creditQryBizReal.setScene("01");
                int result = creditQryBizRealService.insertSelective(creditQryBizReal);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成征信关联表数据异常！");
                }
                //生产征信报告
                result = creditReportQryLstMapper.insertSelective(creditReportQryLst);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                org.springframework.beans.BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                creditAuthbookInfo.setAuthMode("02");
                creditAuthbookInfo.setAuthbookContent("002;007");
                result = creditAuthbookInfoService.insert(creditAuthbookInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                return result;
            } else {
                throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "该客户为查询到征信信息！");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, e.getMessage());
        }
    }

    /*
     * 证件映射 信贷--->征信
     * @param xdbz 信贷证件码值
     * @return hxbz 征信证件码值
     */
    public static String toConverCurrency(String xdbz) {
        String hxbz = "";
        if ("C".equals(xdbz)) { //户口簿
            hxbz = "1";
        } else if ("B".equals(xdbz)) {//护照
            hxbz = "2";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            hxbz = "5";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            hxbz = "6";
        } else if ("12".equals(xdbz)) { // 外国人居留证
            hxbz = "8";
        } else if ("Y".equals(xdbz)) {//警官证
            hxbz = "9";
        } else if ("13".equals(xdbz)) {//香港身份证
            hxbz = "A";
        } else if ("14".equals(xdbz)) {//澳门身份证
            hxbz = "B";
        } else if ("15".equals(xdbz)) {//台湾身份证
            hxbz = "C";
        } else if ("16".equals(xdbz)) {//其他证件
            hxbz = "X";
        } else if ("A".equals(xdbz)) {//居民身份证及其他以公民身份证号为标识的证件
            hxbz = "10";
        } else if ("11".equals(xdbz)) {//军人身份证件
            hxbz = "20";
        } else if ("06".equals(xdbz)) {//工商注册号
            hxbz = "01";
        } else if ("01".equals(xdbz)) {//机关和事业单位登记号
            hxbz = "02";
        } else if ("02".equals(xdbz)) {//社会团体登记号
            hxbz = "03";
        } else if ("03".equals(xdbz)) {//民办非企业登记号
            hxbz = "04";
        } else if ("04".equals(xdbz)) {//基金会登记号
            hxbz = "05";
        } else if ("05".equals(xdbz)) {//宗教证书登记号
            hxbz = "06";
        } else if ("P2".equals(xdbz)) {//中征码
            hxbz = "10";
        } else if ("R".equals(xdbz)) {//统一社会信用代码
            hxbz = "20";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            hxbz = "30";
        } else if ("07".equals(xdbz)) {//纳税人识别号（国税）
            hxbz = "41";
        } else if ("08".equals(xdbz)) {//纳税人识别号（地税）
            hxbz = "42";
        } else {
            hxbz = xdbz;//未匹配到的证件类型
        }
        return hxbz;
    }
}
