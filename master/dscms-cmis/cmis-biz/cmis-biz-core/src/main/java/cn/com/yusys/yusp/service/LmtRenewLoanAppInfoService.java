/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.LmtSurveyEnums;
import cn.com.yusys.yusp.domain.LmtRenewLoanAppInfo;
import cn.com.yusys.yusp.domain.dto.LmtRenewLoanAppCustInfo;
import cn.com.yusys.yusp.repository.mapper.LmtRenewLoanAppInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRenewLoanAppInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-02 09:10:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRenewLoanAppInfoService {

    @Autowired
    private LmtRenewLoanAppInfoMapper lmtRenewLoanAppInfoMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRenewLoanAppInfo selectByPrimaryKey(String serno) {
        return lmtRenewLoanAppInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRenewLoanAppInfo> selectAll(QueryModel model) {
        List<LmtRenewLoanAppInfo> records = (List<LmtRenewLoanAppInfo>) lmtRenewLoanAppInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRenewLoanAppInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRenewLoanAppInfo> list = lmtRenewLoanAppInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRenewLoanAppInfo record) {
        return lmtRenewLoanAppInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRenewLoanAppInfo record) {
        return lmtRenewLoanAppInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtRenewLoanAppInfo record) {
        return lmtRenewLoanAppInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRenewLoanAppInfo record) {
        return lmtRenewLoanAppInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtRenewLoanAppInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtRenewLoanAppInfoMapper.deleteByIds(ids);
    }

    /**
     * 新增数据
     * @param lmtRenewLoanAppInfo
     * @return
     */
    public int save(LmtRenewLoanAppInfo lmtRenewLoanAppInfo) {
        // 主键UUID
        lmtRenewLoanAppInfo.setSerno(StringUtils.getUUID());
        // 登记日期
        lmtRenewLoanAppInfo.setInputDate(DateUtils.getCurrDateStr());
        // 申请日期
        lmtRenewLoanAppInfo.setAppDate(DateUtils.getCurrDateStr());
        // 创建时间
        lmtRenewLoanAppInfo.setCreateTime(DateUtils.getCurrDate());
        // 名单处理状态
        lmtRenewLoanAppInfo.setStatus(LmtSurveyEnums.STD_XD_LIST_STATUS_000.getValue());

        return lmtRenewLoanAppInfoMapper.insertSelective(lmtRenewLoanAppInfo);
    }

    /**
     * 分页查询相关客户信息
     * @param queryModel
     * @return
     */
    public List<LmtRenewLoanAppCustInfo> getRenewLoanCusInfo(QueryModel queryModel) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        queryModel.addCondition("openDay", openDay);
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtRenewLoanAppCustInfo> list = lmtRenewLoanAppInfoMapper.getRenewLoanCusInfo(queryModel);
        PageHelper.clearPage();
        return list;
    }
}
