package cn.com.yusys.yusp.service.server.xdzc0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.dto.server.xdzc0014.req.Xdzc0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.resp.Xdzc0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AccAccpDrftSubService;
import cn.com.yusys.yusp.service.AccAccpService;
import cn.com.yusys.yusp.service.AccLoanService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 接口处理类:资产池主动还款接口
 *
 * @Author xs
 * @Date 2021/6/15 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0014Service {

    @Autowired
    private AccAccpDrftSubService accAccpDrftSubService;


    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccLoanService accLoanService;

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0014Service.class);

    /**
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0014DataRespDto xdzc0014Service(Xdzc0014DataReqDto xdzc0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value);
        Xdzc0014DataRespDto xdzc0014DataRespDto = new Xdzc0014DataRespDto();

        String cusId = xdzc0014DataReqDto.getCusId(); // 客户号
        String loanType = xdzc0014DataReqDto.getLoanType(); // 贷款种类
        String loanStatus = xdzc0014DataReqDto.getLoanStatus(); // 贷款状态
        String pvpSerno = xdzc0014DataReqDto.getPvpSerno(); // 出账流水号
        String loanDateStartS = xdzc0014DataReqDto.getLoanDateStartS(); // 贷款起始日start
        String loanDateStartE = xdzc0014DataReqDto.getLoanDateStartE(); // 贷款起始日end
        String loanDateEndS = xdzc0014DataReqDto.getLoanDateEndS(); // 贷款到期日start
        String loanDateEndE = xdzc0014DataReqDto.getLoanDateEndE(); // 贷款到期日end
        String pageNum = xdzc0014DataReqDto.getStartPageNum(); // 开始页数
        String pageSize = xdzc0014DataReqDto.getPageSize();; // 条数
        if(StringUtils.isEmpty(pageNum)){
            pageNum = "1";
        }
        if(StringUtils.isEmpty(pageSize)){
            pageSize = "10";
        }
        long count =0;

        try {
            // 贷款种类
            QueryModel model = new QueryModel();
            model.addCondition("cusId", cusId);// 客户号
            model.addCondition("pvpSerno",pvpSerno);// 放款出账流水号
            /**
             * todo
             * 台账状态
             * STD_ACC_STATUS
             * 1未生效
             * 2正常
             * 3结清
             * 4付垫款
             * 5作废
             */
            model.addCondition("loanDateStartS", loanDateStartS);// 贷款起始日start
            model.addCondition("loanDateStartE", loanDateStartE);// 贷款起始日end
            model.addCondition("loanDateEndS", loanDateEndS);// 贷款到期日start
            model.addCondition("loanDateEndE", loanDateEndE);// 贷款到期日end
            // 银票
            List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> resultList = new ArrayList<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List>();

            PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
            if ("01".equals(loanType)) {
                // 01银票 （当前、历史）
                if("0".equals(loanStatus)){
                    // 当前(未结清)
                    model.addCondition("accStatus", "0,1,4,5");
                }else {
                    // 历史（结清）
                    model.addCondition("accStatus", "2,3,6,7");
                }
                // TODO  银票查询
                resultList = accAccpService.selectInfoByCusIdDate(model);
            }else if("02".equals(loanType)){
                // 02超短贷
                if("0".equals(loanStatus)){
                    model.addCondition("accStatus", "1,2,3,4,5,6,8");
                }else {
                    model.addCondition("accStatus", "0,7");
                }
                //  控制产品类型（超短贷  短期流动资金贷款）
                resultList = accLoanService.selectInfoByCusIdDate(model);
            }else if("03".equals(loanType)){
                // 03省心E付
                if("0".equals(loanStatus)){
                    model.addCondition("accStatus", "1,2,3,4,5,6,8");
                }else {
                    model.addCondition("accStatus", "0,7");
                }
                model.addCondition("isSxef", CommonConstance.STD_ZB_YES_NO_1);// 是否省心E付（STD_ZB_YES_NO）
                resultList = accLoanService.selectInfoByCusIdDate(model);
            }else if("04".equals(loanType)){
                // 04省心E票
                if("0".equals(loanStatus)){
                    model.addCondition("accStatus", "0,1,4,5");
                }else {
                    model.addCondition("accStatus", "2,3,6,7");
                }
                model.addCondition("isSxep", CommonConstance.STD_ZB_YES_NO_1);//  是否省心E付（STD_ZB_YES_NO）
                resultList = accAccpService.selectInfoByCusIdDate(model);
            }else{
                throw BizException.error(null, "9999", "查询类型【loanType】未定义");
            }
            PageHelper.clearPage();
            PageInfo<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> pageinfo = new PageInfo<>(resultList);
            count = pageinfo.getTotal();
            // 业务说 拿票据明细第一张票的到期日 作为台账到期日
            resultList = resultList.parallelStream()
                .map(e->{
                    List<AccAccpDrftSub> accAccpDrftSubs=  accAccpDrftSubService.selectDetailsByCoreBillNo(e.getBillNo());
                    if(CollectionUtils.nonEmpty(accAccpDrftSubs)){
                        e .setEndDate(accAccpDrftSubs.get(0).getEndDate());
                    }
                    return e;
                }
            ).collect(Collectors.toList());
            xdzc0014DataRespDto.setList(resultList);
            xdzc0014DataRespDto.setTotalQnt(count);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value);
        return xdzc0014DataRespDto;
    }
}







