/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestRelCusOutGuar;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRelCusOutGuarMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelCusOutGuarService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 14:47:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRelCusOutGuarService {

    @Autowired
    private LmtSigInvestRelCusOutGuarMapper lmtSigInvestRelCusOutGuarMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRelCusOutGuar selectByPrimaryKey(String pkId) {
        return lmtSigInvestRelCusOutGuarMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRelCusOutGuar> selectAll(QueryModel model) {
        List<LmtSigInvestRelCusOutGuar> records = (List<LmtSigInvestRelCusOutGuar>) lmtSigInvestRelCusOutGuarMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRelCusOutGuar> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRelCusOutGuar> list = lmtSigInvestRelCusOutGuarMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestRelCusOutGuar record) {
        return lmtSigInvestRelCusOutGuarMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRelCusOutGuar record) {
        return lmtSigInvestRelCusOutGuarMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRelCusOutGuar record) {
        return lmtSigInvestRelCusOutGuarMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRelCusOutGuar record) {
        return lmtSigInvestRelCusOutGuarMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRelCusOutGuarMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRelCusOutGuarMapper.deleteByIds(ids);
    }

    /**
     * 逻辑删除
     * @param pkId
     * @return
     */
    public int deleteLogicByPkId(String pkId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId",pkId);
        BizInvestCommonService.checkParamsIsNull("pkId",pkId);
        List<LmtSigInvestRelCusOutGuar> lmtSigInvestRelCusOutGuars = selectAll(queryModel);
        if (lmtSigInvestRelCusOutGuars!=null && lmtSigInvestRelCusOutGuars.size()>0){
            LmtSigInvestRelCusOutGuar lmtSigInvestRelCusOutGuar = lmtSigInvestRelCusOutGuars.get(0);
            lmtSigInvestRelCusOutGuar.setOprType(CmisBizConstants.OPR_TYPE_02);
            return update(lmtSigInvestRelCusOutGuar);
        }
        return 0;
    }
}
