package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplRateInfo
 * @类描述: iqp_appl_rate_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 15:08:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApplRateInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 固定利率期限（月) **/
	private Integer rateTerm;
	
	/** 固定利率% **/
	private java.math.BigDecimal ratePercent;
	
	/** 登记人机构 **/
	private String createOrg;
	
	/** 登记时间 **/
	private String createTime;
	
	/** 登记人 **/
	private String createUsr;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param rateTerm
	 */
	public void setRateTerm(Integer rateTerm) {
		this.rateTerm = rateTerm;
	}
	
    /**
     * @return RateTerm
     */	
	public Integer getRateTerm() {
		return this.rateTerm;
	}
	
	/**
	 * @param ratePercent
	 */
	public void setRatePercent(java.math.BigDecimal ratePercent) {
		this.ratePercent = ratePercent;
	}
	
    /**
     * @return RatePercent
     */	
	public java.math.BigDecimal getRatePercent() {
		return this.ratePercent;
	}
	
	/**
	 * @param createOrg
	 */
	public void setCreateOrg(String createOrg) {
		this.createOrg = createOrg == null ? null : createOrg.trim();
	}
	
    /**
     * @return CreateOrg
     */	
	public String getCreateOrg() {
		return this.createOrg;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}
	
    /**
     * @return CreateTime
     */	
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param createUsr
	 */
	public void setCreateUsr(String createUsr) {
		this.createUsr = createUsr == null ? null : createUsr.trim();
	}
	
    /**
     * @return CreateUsr
     */	
	public String getCreateUsr() {
		return this.createUsr;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}