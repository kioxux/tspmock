/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherAccpPerferFeeAppCusList
 * @类描述: other_accp_perfer_fee_app_cus_list数据实体类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-07 21:40:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_accp_perfer_fee_app_cus_list")
public class OtherAccpPerferFeeAppCusList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 信用等级 **/
	@Column(name = "CUS_CRD_GRADE", unique = false, nullable = true, length = 5)
	private String cusCrdGrade;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 40)
	private String guarMode;
	
	/** 对公活期存款日均 **/
	@Column(name = "CORP_AVERDAY_DEP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpAverdayDep;
	
	/** 关联个人存款日均 **/
	@Column(name = "REL_INDIV_AVERDAY_DEP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal relIndivAverdayDep;
	
	/** 对公贷款日均 **/
	@Column(name = "CORP_AVERDAY_LOAN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpAverdayLoan;
	
	/** 是否开通网银 **/
	@Column(name = "IS_OPN_EBANK", unique = false, nullable = true, length = 5)
	private String isOpnEbank;
	
	/** 是否代发工资 **/
	@Column(name = "IS_ISS_SAL", unique = false, nullable = true, length = 5)
	private String isIssSal;
	
	/** 代发工资户数 **/
	@Column(name = "ISS_SAL_TIMES", unique = false, nullable = true, length = 10)
	private Integer issSalTimes;
	
	/** 是否需要申请优惠 **/
	@Column(name = "IS_APP_PREFER", unique = false, nullable = true, length = 5)
	private String isAppPrefer;
	
	/** 月手续费率 **/
	@Column(name = "CHRG_RATE_M", unique = false, nullable = true, length = 100)
	private String chrgRateM;
	
	/** 此次申请手续费率 **/
	@Column(name = "CURT_CHRG_RATE", unique = false, nullable = true, length = 16)
	private String curtChrgRate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 客户名单状态 **/
	@Column(name = "CUS_LIST_STATUS", unique = false, nullable = true, length = 5)
	private String cusListStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;

	/** 审批状态（联合查询用） **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 业务类型（特殊需要） **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCrdGrade
	 */
	public void setCusCrdGrade(String cusCrdGrade) {
		this.cusCrdGrade = cusCrdGrade;
	}
	
    /**
     * @return cusCrdGrade
     */
	public String getCusCrdGrade() {
		return this.cusCrdGrade;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param corpAverdayDep
	 */
	public void setCorpAverdayDep(java.math.BigDecimal corpAverdayDep) {
		this.corpAverdayDep = corpAverdayDep;
	}
	
    /**
     * @return corpAverdayDep
     */
	public java.math.BigDecimal getCorpAverdayDep() {
		return this.corpAverdayDep;
	}
	
	/**
	 * @param relIndivAverdayDep
	 */
	public void setRelIndivAverdayDep(java.math.BigDecimal relIndivAverdayDep) {
		this.relIndivAverdayDep = relIndivAverdayDep;
	}
	
    /**
     * @return relIndivAverdayDep
     */
	public java.math.BigDecimal getRelIndivAverdayDep() {
		return this.relIndivAverdayDep;
	}
	
	/**
	 * @param corpAverdayLoan
	 */
	public void setCorpAverdayLoan(java.math.BigDecimal corpAverdayLoan) {
		this.corpAverdayLoan = corpAverdayLoan;
	}
	
    /**
     * @return corpAverdayLoan
     */
	public java.math.BigDecimal getCorpAverdayLoan() {
		return this.corpAverdayLoan;
	}
	
	/**
	 * @param isOpnEbank
	 */
	public void setIsOpnEbank(String isOpnEbank) {
		this.isOpnEbank = isOpnEbank;
	}
	
    /**
     * @return isOpnEbank
     */
	public String getIsOpnEbank() {
		return this.isOpnEbank;
	}
	
	/**
	 * @param isIssSal
	 */
	public void setIsIssSal(String isIssSal) {
		this.isIssSal = isIssSal;
	}
	
    /**
     * @return isIssSal
     */
	public String getIsIssSal() {
		return this.isIssSal;
	}
	
	/**
	 * @param issSalTimes
	 */
	public void setIssSalTimes(Integer issSalTimes) {
		this.issSalTimes = issSalTimes;
	}
	
    /**
     * @return issSalTimes
     */
	public Integer getIssSalTimes() {
		return this.issSalTimes;
	}
	
	/**
	 * @param isAppPrefer
	 */
	public void setIsAppPrefer(String isAppPrefer) {
		this.isAppPrefer = isAppPrefer;
	}
	
    /**
     * @return isAppPrefer
     */
	public String getIsAppPrefer() {
		return this.isAppPrefer;
	}
	
	/**
	 * @param chrgRateM
	 */
	public void setChrgRateM(String chrgRateM) {
		this.chrgRateM = chrgRateM;
	}
	
    /**
     * @return chrgRateM
     */
	public String getChrgRateM() {
		return this.chrgRateM;
	}
	
	/**
	 * @param curtChrgRate
	 */
	public void setCurtChrgRate(String curtChrgRate) {
		this.curtChrgRate = curtChrgRate;
	}
	
    /**
     * @return curtChrgRate
     */
	public String getCurtChrgRate() {
		return this.curtChrgRate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param cusListStatus
	 */
	public void setCusListStatus(String cusListStatus) {
		this.cusListStatus = cusListStatus;
	}
	
    /**
     * @return cusListStatus
     */
	public String getCusListStatus() {
		return this.cusListStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}

}