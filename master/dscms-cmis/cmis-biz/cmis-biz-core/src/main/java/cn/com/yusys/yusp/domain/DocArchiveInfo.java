/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveInfo
 * @类描述: doc_archive_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_archive_info")
public class DocArchiveInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 档案流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DOC_SERNO")
	private String docSerno;
	
	/** 档案编号 **/
	@Column(name = "DOC_NO", unique = false, nullable = true, length = 40)
	private String docNo;
	
	/** 归档模式 **/
	@Column(name = "ARCHIVE_MODE", unique = false, nullable = true, length = 5)
	private String archiveMode;
	
	/** 档案分类 **/
	@Column(name = "DOC_CLASS", unique = false, nullable = true, length = 5)
	private String docClass;
	
	/** 档案类型 **/
	@Column(name = "DOC_TYPE", unique = false, nullable = true, length = 5)
	private String docType;
	
	/** 关联业务编号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 任务标识 **/
	@Column(name = "TASK_FLAG", unique = false, nullable = true, length = 5)
	private String taskFlag;
	
	/** 档案状态 **/
	@Column(name = "DOC_STAUTS", unique = false, nullable = true, length = 5)
	private String docStauts;
	
	/** 生成日期 **/
	@Column(name = "CREATE_DATE", unique = false, nullable = true, length = 10)
	private String createDate;
	
	/** 完成日期 **/
	@Column(name = "FINISH_DATE", unique = false, nullable = true, length = 10)
	private String finishDate;
	
	/** 入库操作人 **/
	@Column(name = "OPT_USR", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="optUsrName")
	private String optUsr;
	
	/** 入库操作机构 **/
	@Column(name = "OPT_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="optOrgName" )
	private String optOrg;
	
	/** 入库操作时间 **/
	@Column(name = "STORAGE_DATE", unique = false, nullable = true, length = 20)
	private String storageDate;
	
	/** 删除原因 **/
	@Column(name = "DEL_REASON", unique = false, nullable = true, length = 2000)
	private String delReason;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 资料总页数 **/
	@Column(name = "TOTAL_PAGE", unique = false, nullable = true, length = 10)
	private String totalPage;

	/** 库存位置 **/
	@Column(name = "LOCATION", unique = false, nullable = true, length = 200)
	private String location;

	/** 贷款账号 **/
	@Column(name = "LOAN_NO", unique = false, nullable = true, length = 40)
	private String loanNo;

	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal loanAmt;

	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private BigDecimal loanBalance;

	/** 贷款期限(月) **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private String loanTerm;

	/** 账户状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 20)
	private String accStatus;

	/** 委托扣款账号 **/
	@Column(name = "ENTRUSTED_DEDUCT_NO", unique = false, nullable = true, length = 40)
	private String entrustedDeductNo;

	/** 放款日期 **/
	@Column(name = "ENCASH_DATE", unique = false, nullable = true, length = 20)
	private String encashDate;

	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;

	/** 联系电话 **/
	@Column(name = "TELEPHONE", unique = false, nullable = true, length = 20)
	private String telephone;

	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;

	/** 档案业务品种 **/
	@Column(name = "DOC_BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String docBizType;

	/** 起始时间 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 扫描人信息 **/
	@JsonProperty(value = "scanUserParam")
	private List<DocScanUserInfo> scanUserParam;

	/** 档案资料清单信息 **/
	@JsonProperty(value = "archiveParam")
	private List<DocArchiveMaterList> archiveParam;

	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno;
	}
	
    /**
     * @return docSerno
     */
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	
    /**
     * @return docNo
     */
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param archiveMode
	 */
	public void setArchiveMode(String archiveMode) {
		this.archiveMode = archiveMode;
	}
	
    /**
     * @return archiveMode
     */
	public String getArchiveMode() {
		return this.archiveMode;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass;
	}
	
    /**
     * @return docClass
     */
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
    /**
     * @return docType
     */
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param taskFlag
	 */
	public void setTaskFlag(String taskFlag) {
		this.taskFlag = taskFlag;
	}
	
    /**
     * @return taskFlag
     */
	public String getTaskFlag() {
		return this.taskFlag;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts;
	}
	
    /**
     * @return docStauts
     */
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param createDate
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
    /**
     * @return createDate
     */
	public String getCreateDate() {
		return this.createDate;
	}
	
	/**
	 * @param finishDate
	 */
	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}
	
    /**
     * @return finishDate
     */
	public String getFinishDate() {
		return this.finishDate;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr;
	}
	
    /**
     * @return optUsr
     */
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg;
	}
	
    /**
     * @return optOrg
     */
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param storageDate
	 */
	public void setStorageDate(String storageDate) {
		this.storageDate = storageDate;
	}
	
    /**
     * @return storageDate
     */
	public String getStorageDate() {
		return this.storageDate;
	}
	
	/**
	 * @param delReason
	 */
	public void setDelReason(String delReason) {
		this.delReason = delReason;
	}
	
    /**
     * @return delReason
     */
	public String getDelReason() {
		return this.delReason;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param totalPage
	 */
	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}

	/**
	 * @return totalPage
	 */
	public String getTotalPage() {
		return this.totalPage;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public BigDecimal getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}

	public String getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	public String getAccStatus() {
		return accStatus;
	}

	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}

	public String getEntrustedDeductNo() {
		return entrustedDeductNo;
	}

	public void setEntrustedDeductNo(String entrustedDeductNo) {
		this.entrustedDeductNo = entrustedDeductNo;
	}

	public String getEncashDate() {
		return encashDate;
	}

	public void setEncashDate(String encashDate) {
		this.encashDate = encashDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public String getDocBizType() {
		return docBizType;
	}

	public void setDocBizType(String docBizType) {
		this.docBizType = docBizType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public List<DocScanUserInfo> getScanUserParam() {
		return scanUserParam;
	}

	public void setScanUserParam(List<DocScanUserInfo> scanUserParam) {
		this.scanUserParam = scanUserParam;
	}

	public List<DocArchiveMaterList> getArchiveParam() {
		return archiveParam;
	}

	public void setArchiveParam(List<DocArchiveMaterList> archiveParam) {
		this.archiveParam = archiveParam;
	}
}