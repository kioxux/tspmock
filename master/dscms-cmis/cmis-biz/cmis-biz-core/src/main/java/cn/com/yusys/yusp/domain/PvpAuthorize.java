/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorize
 * @类描述: pvp_authorize数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 09:37:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_authorize")
public class PvpAuthorize extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 交易流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TRAN_SERNO")
	private String tranSerno;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;
	
	/** 授权编号 **/
	@Column(name = "AUTHORIZE_NO", unique = false, nullable = true, length = 40)
	private String authorizeNo;
	
	/** 授权类型 STD_ZB_AUTH_TYPE **/
	@Column(name = "AUTHORIZE_TYPE", unique = false, nullable = true, length = 5)
	private String authorizeType;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 未受托支付类型 **/
	@Column(name = "UNTRU_PAY_TYPE", unique = false, nullable = true, length = 5)
	private String untruPayType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 255)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 255)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 交易编号 **/
	@Column(name = "TRAN_ID", unique = false, nullable = true, length = 20)
	private String tranId;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账号名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 币种  STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 交易金额 **/
	@Column(name = "TRAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tranAmt;
	
	/** 交易日期 **/
	@Column(name = "TRAN_DATE", unique = false, nullable = true, length = 10)
	private String tranDate;
	
	/** 发送次数 **/
	@Column(name = "SEND_TIMES", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal sendTimes;
	
	/** 返回编码 **/
	@Column(name = "RETURN_CODE", unique = false, nullable = true, length = 15)
	private String returnCode;
	
	/** 返回说明 **/
	@Column(name = "RETURN_DESC", unique = false, nullable = true, length = 500)
	private String returnDesc;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = false, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 授权状态 **/
	@Column(name = "AUTH_STATUS", unique = false, nullable = true, length = 5)
		private String authStatus;

	/** 出账状态 **/
	@Column(name = "TRADE_STATUS", unique = false, nullable = true, length = 5)
	private String tradeStatus;
	
	/** 核心交易日期 **/
	@Column(name = "CORE_TRAN_DATE", unique = false, nullable = true, length = 10)
	private String coreTranDate;
	
	/** 核心交易流水 **/
	@Column(name = "CORE_TRAN_SERNO", unique = false, nullable = true, length = 40)
	private String coreTranSerno;
	
	/** FLDVALUE01 **/
	@Column(name = "FLDVALUE01", unique = false, nullable = true, length = 200)
	private String fldvalue01;
	
	/** FLDVALUE02 **/
	@Column(name = "FLDVALUE02", unique = false, nullable = true, length = 200)
	private String fldvalue02;
	
	/** FLDVALUE03 **/
	@Column(name = "FLDVALUE03", unique = false, nullable = true, length = 200)
	private String fldvalue03;
	
	/** FLDVALUE04 **/
	@Column(name = "FLDVALUE04", unique = false, nullable = true, length = 200)
	private String fldvalue04;
	
	/** FLDVALUE05 **/
	@Column(name = "FLDVALUE05", unique = false, nullable = true, length = 200)
	private String fldvalue05;
	
	/** FLDVALUE06 **/
	@Column(name = "FLDVALUE06", unique = false, nullable = true, length = 200)
	private String fldvalue06;
	
	/** FLDVALUE07 **/
	@Column(name = "FLDVALUE07", unique = false, nullable = true, length = 200)
	private String fldvalue07;
	
	/** FLDVALUE08 **/
	@Column(name = "FLDVALUE08", unique = false, nullable = true, length = 200)
	private String fldvalue08;
	
	/** FLDVALUE09 **/
	@Column(name = "FLDVALUE09", unique = false, nullable = true, length = 200)
	private String fldvalue09;
	
	/** FLDVALUE10 **/
	@Column(name = "FLDVALUE10", unique = false, nullable = true, length = 200)
	private String fldvalue10;
	
	/** FLDVALUE11 **/
	@Column(name = "FLDVALUE11", unique = false, nullable = true, length = 200)
	private String fldvalue11;
	
	/** FLDVALUE12 **/
	@Column(name = "FLDVALUE12", unique = false, nullable = true, length = 200)
	private String fldvalue12;
	
	/** FLDVALUE13 **/
	@Column(name = "FLDVALUE13", unique = false, nullable = true, length = 200)
	private String fldvalue13;
	
	/** FLDVALUE14 **/
	@Column(name = "FLDVALUE14", unique = false, nullable = true, length = 200)
	private String fldvalue14;
	
	/** FLDVALUE15 **/
	@Column(name = "FLDVALUE15", unique = false, nullable = true, length = 200)
	private String fldvalue15;
	
	/** FLDVALUE16 **/
	@Column(name = "FLDVALUE16", unique = false, nullable = true, length = 200)
	private String fldvalue16;
	
	/** FLDVALUE17 **/
	@Column(name = "FLDVALUE17", unique = false, nullable = true, length = 200)
	private String fldvalue17;
	
	/** FLDVALUE18 **/
	@Column(name = "FLDVALUE18", unique = false, nullable = true, length = 200)
	private String fldvalue18;
	
	/** FLDVALUE19 **/
	@Column(name = "FLDVALUE19", unique = false, nullable = true, length = 200)
	private String fldvalue19;
	
	/** FLDVALUE20 **/
	@Column(name = "FLDVALUE20", unique = false, nullable = true, length = 200)
	private String fldvalue20;
	
	/** FLDVALUE21 **/
	@Column(name = "FLDVALUE21", unique = false, nullable = true, length = 200)
	private String fldvalue21;
	
	/** FLDVALUE22 **/
	@Column(name = "FLDVALUE22", unique = false, nullable = true, length = 200)
	private String fldvalue22;
	
	/** FLDVALUE23 **/
	@Column(name = "FLDVALUE23", unique = false, nullable = true, length = 200)
	private String fldvalue23;
	
	/** FLDVALUE24 **/
	@Column(name = "FLDVALUE24", unique = false, nullable = true, length = 200)
	private String fldvalue24;
	
	/** FLDVALUE25 **/
	@Column(name = "FLDVALUE25", unique = false, nullable = true, length = 200)
	private String fldvalue25;
	
	/** FLDVALUE26 **/
	@Column(name = "FLDVALUE26", unique = false, nullable = true, length = 200)
	private String fldvalue26;
	
	/** FLDVALUE27 **/
	@Column(name = "FLDVALUE27", unique = false, nullable = true, length = 200)
	private String fldvalue27;
	
	/** FLDVALUE28 **/
	@Column(name = "FLDVALUE28", unique = false, nullable = true, length = 200)
	private String fldvalue28;
	
	/** FLDVALUE29 **/
	@Column(name = "FLDVALUE29", unique = false, nullable = true, length = 200)
	private String fldvalue29;
	
	/** FLDVALUE30 **/
	@Column(name = "FLDVALUE30", unique = false, nullable = true, length = 200)
	private String fldvalue30;
	
	/** FLDVALUE31 **/
	@Column(name = "FLDVALUE31", unique = false, nullable = true, length = 200)
	private String fldvalue31;
	
	/** FLDVALUE32 **/
	@Column(name = "FLDVALUE32", unique = false, nullable = true, length = 200)
	private String fldvalue32;
	
	/** FLDVALUE33 **/
	@Column(name = "FLDVALUE33", unique = false, nullable = true, length = 200)
	private String fldvalue33;
	
	/** FLDVALUE34 **/
	@Column(name = "FLDVALUE34", unique = false, nullable = true, length = 200)
	private String fldvalue34;
	
	/** FLDVALUE35 **/
	@Column(name = "FLDVALUE35", unique = false, nullable = true, length = 200)
	private String fldvalue35;
	
	/** FLDVALUE36 **/
	@Column(name = "FLDVALUE36", unique = false, nullable = true, length = 200)
	private String fldvalue36;
	
	/** FLDVALUE37 **/
	@Column(name = "FLDVALUE37", unique = false, nullable = true, length = 200)
	private String fldvalue37;
	
	/** FLDVALUE38 **/
	@Column(name = "FLDVALUE38", unique = false, nullable = true, length = 200)
	private String fldvalue38;
	
	/** FLDVALUE39 **/
	@Column(name = "FLDVALUE39", unique = false, nullable = true, length = 200)
	private String fldvalue39;
	
	/** FLDVALUE40 **/
	@Column(name = "FLDVALUE40", unique = false, nullable = true, length = 200)
	private String fldvalue40;
	
	/** FLDVALUE41 **/
	@Column(name = "FLDVALUE41", unique = false, nullable = true, length = 200)
	private String fldvalue41;
	
	/** FLDVALUE42 **/
	@Column(name = "FLDVALUE42", unique = false, nullable = true, length = 200)
	private String fldvalue42;
	
	/** FLDVALUE43 **/
	@Column(name = "FLDVALUE43", unique = false, nullable = true, length = 200)
	private String fldvalue43;
	
	/** FLDVALUE44 **/
	@Column(name = "FLDVALUE44", unique = false, nullable = true, length = 200)
	private String fldvalue44;
	
	/** FLDVALUE45 **/
	@Column(name = "FLDVALUE45", unique = false, nullable = true, length = 200)
	private String fldvalue45;
	
	/** FLDVALUE46 **/
	@Column(name = "FLDVALUE46", unique = false, nullable = true, length = 200)
	private String fldvalue46;
	
	/** FLDVALUE47 **/
	@Column(name = "FLDVALUE47", unique = false, nullable = true, length = 200)
	private String fldvalue47;
	
	/** FLDVALUE48 **/
	@Column(name = "FLDVALUE48", unique = false, nullable = true, length = 200)
	private String fldvalue48;
	
	/** FLDVALUE49 **/
	@Column(name = "FLDVALUE49", unique = false, nullable = true, length = 200)
	private String fldvalue49;
	
	/** FLDVALUE50 **/
	@Column(name = "FLDVALUE50", unique = false, nullable = true, length = 200)
	private String fldvalue50;
	
	/** FLDVALUE51 **/
	@Column(name = "FLDVALUE51", unique = false, nullable = true, length = 200)
	private String fldvalue51;
	
	/** FLDVALUE52 **/
	@Column(name = "FLDVALUE52", unique = false, nullable = true, length = 200)
	private String fldvalue52;
	
	/** FLDVALUE53 **/
	@Column(name = "FLDVALUE53", unique = false, nullable = true, length = 200)
	private String fldvalue53;
	
	/** FLDVALUE54 **/
	@Column(name = "FLDVALUE54", unique = false, nullable = true, length = 200)
	private String fldvalue54;
	
	/** FLDVALUE55 **/
	@Column(name = "FLDVALUE55", unique = false, nullable = true, length = 200)
	private String fldvalue55;
	
	/** FLDVALUE56 **/
	@Column(name = "FLDVALUE56", unique = false, nullable = true, length = 200)
	private String fldvalue56;
	
	/** FLDVALUE57 **/
	@Column(name = "FLDVALUE57", unique = false, nullable = true, length = 200)
	private String fldvalue57;
	
	/** FLDVALUE58 **/
	@Column(name = "FLDVALUE58", unique = false, nullable = true, length = 200)
	private String fldvalue58;
	
	/** FLDVALUE59 **/
	@Column(name = "FLDVALUE59", unique = false, nullable = true, length = 200)
	private String fldvalue59;
	
	/** FLDVALUE60 **/
	@Column(name = "FLDVALUE60", unique = false, nullable = true, length = 200)
	private String fldvalue60;
	
	/** FLDVALUE61 **/
	@Column(name = "FLDVALUE61", unique = false, nullable = true, length = 200)
	private String fldvalue61;
	
	/** FLDVALUE62 **/
	@Column(name = "FLDVALUE62", unique = false, nullable = true, length = 200)
	private String fldvalue62;
	
	/** FLDVALUE63 **/
	@Column(name = "FLDVALUE63", unique = false, nullable = true, length = 200)
	private String fldvalue63;
	
	/** FLDVALUE64 **/
	@Column(name = "FLDVALUE64", unique = false, nullable = true, length = 200)
	private String fldvalue64;
	
	/** FLDVALUE65 **/
	@Column(name = "FLDVALUE65", unique = false, nullable = true, length = 200)
	private String fldvalue65;
	
	/** FLDVALUE66 **/
	@Column(name = "FLDVALUE66", unique = false, nullable = true, length = 200)
	private String fldvalue66;
	
	/** FLDVALUE67 **/
	@Column(name = "FLDVALUE67", unique = false, nullable = true, length = 200)
	private String fldvalue67;
	
	/** FLDVALUE68 **/
	@Column(name = "FLDVALUE68", unique = false, nullable = true, length = 200)
	private String fldvalue68;
	
	/** FLDVALUE69 **/
	@Column(name = "FLDVALUE69", unique = false, nullable = true, length = 200)
	private String fldvalue69;
	
	/** FLDVALUE70 **/
	@Column(name = "FLDVALUE70", unique = false, nullable = true, length = 200)
	private String fldvalue70;
	
	/** FLDVALUE71 **/
	@Column(name = "FLDVALUE71", unique = false, nullable = true, length = 200)
	private String fldvalue71;
	
	/** FLDVALUE72 **/
	@Column(name = "FLDVALUE72", unique = false, nullable = true, length = 200)
	private String fldvalue72;
	
	/** FLDVALUE73 **/
	@Column(name = "FLDVALUE73", unique = false, nullable = true, length = 200)
	private String fldvalue73;
	
	/** FLDVALUE74 **/
	@Column(name = "FLDVALUE74", unique = false, nullable = true, length = 200)
	private String fldvalue74;
	
	/** FLDVALUE75 **/
	@Column(name = "FLDVALUE75", unique = false, nullable = true, length = 200)
	private String fldvalue75;
	
	/** FLDVALUE76 **/
	@Column(name = "FLDVALUE76", unique = false, nullable = true, length = 200)
	private String fldvalue76;
	
	/** FLDVALUE77 **/
	@Column(name = "FLDVALUE77", unique = false, nullable = true, length = 200)
	private String fldvalue77;
	
	/** FLDVALUE78 **/
	@Column(name = "FLDVALUE78", unique = false, nullable = true, length = 200)
	private String fldvalue78;
	
	/** FLDVALUE79 **/
	@Column(name = "FLDVALUE79", unique = false, nullable = true, length = 200)
	private String fldvalue79;
	
	/** FLDVALUE80 **/
	@Column(name = "FLDVALUE80", unique = false, nullable = true, length = 200)
	private String fldvalue80;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/** 放款机构编号 **/
	@Column(name = "DISB_ORG_NO", unique = false, nullable = true, length = 20)
	private String disbOrgNo;


	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 40)
	private String belgLine;

	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno;
	}
	
    /**
     * @return tranSerno
     */
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param authorizeNo
	 */
	public void setAuthorizeNo(String authorizeNo) {
		this.authorizeNo = authorizeNo;
	}
	
    /**
     * @return authorizeNo
     */
	public String getAuthorizeNo() {
		return this.authorizeNo;
	}
	
	/**
	 * @param authorizeType
	 */
	public void setAuthorizeType(String authorizeType) {
		this.authorizeType = authorizeType;
	}
	
    /**
     * @return authorizeType
     */
	public String getAuthorizeType() {
		return this.authorizeType;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param untruPayType
	 */
	public void setUntruPayType(String untruPayType) {
		this.untruPayType = untruPayType;
	}
	
    /**
     * @return untruPayType
     */
	public String getUntruPayType() {
		return this.untruPayType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param tranId
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	
    /**
     * @return tranId
     */
	public String getTranId() {
		return this.tranId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return tranAmt
     */
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	
    /**
     * @return tranDate
     */
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param sendTimes
	 */
	public void setSendTimes(java.math.BigDecimal sendTimes) {
		this.sendTimes = sendTimes;
	}
	
    /**
     * @return sendTimes
     */
	public java.math.BigDecimal getSendTimes() {
		return this.sendTimes;
	}
	
	/**
	 * @param returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
    /**
     * @return returnCode
     */
	public String getReturnCode() {
		return this.returnCode;
	}
	
	/**
	 * @param returnDesc
	 */
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}
	
    /**
     * @return returnDesc
     */
	public String getReturnDesc() {
		return this.returnDesc;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}
	
    /**
     * @return authStatus
     */
	public String getAuthStatus() {
		return this.authStatus;
	}

	/**
	 * @param tradeStatus
	 */
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

    /**
     * @return tradeStatus
     */
	public String getTradeStatus() {
		return this.tradeStatus;
	}
	
	/**
	 * @param coreTranDate
	 */
	public void setCoreTranDate(String coreTranDate) {
		this.coreTranDate = coreTranDate;
	}
	
    /**
     * @return coreTranDate
     */
	public String getCoreTranDate() {
		return this.coreTranDate;
	}
	
	/**
	 * @param coreTranSerno
	 */
	public void setCoreTranSerno(String coreTranSerno) {
		this.coreTranSerno = coreTranSerno;
	}
	
    /**
     * @return coreTranSerno
     */
	public String getCoreTranSerno() {
		return this.coreTranSerno;
	}
	
	/**
	 * @param fldvalue01
	 */
	public void setFldvalue01(String fldvalue01) {
		this.fldvalue01 = fldvalue01;
	}
	
    /**
     * @return fldvalue01
     */
	public String getFldvalue01() {
		return this.fldvalue01;
	}
	
	/**
	 * @param fldvalue02
	 */
	public void setFldvalue02(String fldvalue02) {
		this.fldvalue02 = fldvalue02;
	}
	
    /**
     * @return fldvalue02
     */
	public String getFldvalue02() {
		return this.fldvalue02;
	}
	
	/**
	 * @param fldvalue03
	 */
	public void setFldvalue03(String fldvalue03) {
		this.fldvalue03 = fldvalue03;
	}
	
    /**
     * @return fldvalue03
     */
	public String getFldvalue03() {
		return this.fldvalue03;
	}
	
	/**
	 * @param fldvalue04
	 */
	public void setFldvalue04(String fldvalue04) {
		this.fldvalue04 = fldvalue04;
	}
	
    /**
     * @return fldvalue04
     */
	public String getFldvalue04() {
		return this.fldvalue04;
	}
	
	/**
	 * @param fldvalue05
	 */
	public void setFldvalue05(String fldvalue05) {
		this.fldvalue05 = fldvalue05;
	}
	
    /**
     * @return fldvalue05
     */
	public String getFldvalue05() {
		return this.fldvalue05;
	}
	
	/**
	 * @param fldvalue06
	 */
	public void setFldvalue06(String fldvalue06) {
		this.fldvalue06 = fldvalue06;
	}
	
    /**
     * @return fldvalue06
     */
	public String getFldvalue06() {
		return this.fldvalue06;
	}
	
	/**
	 * @param fldvalue07
	 */
	public void setFldvalue07(String fldvalue07) {
		this.fldvalue07 = fldvalue07;
	}
	
    /**
     * @return fldvalue07
     */
	public String getFldvalue07() {
		return this.fldvalue07;
	}
	
	/**
	 * @param fldvalue08
	 */
	public void setFldvalue08(String fldvalue08) {
		this.fldvalue08 = fldvalue08;
	}
	
    /**
     * @return fldvalue08
     */
	public String getFldvalue08() {
		return this.fldvalue08;
	}
	
	/**
	 * @param fldvalue09
	 */
	public void setFldvalue09(String fldvalue09) {
		this.fldvalue09 = fldvalue09;
	}
	
    /**
     * @return fldvalue09
     */
	public String getFldvalue09() {
		return this.fldvalue09;
	}
	
	/**
	 * @param fldvalue10
	 */
	public void setFldvalue10(String fldvalue10) {
		this.fldvalue10 = fldvalue10;
	}
	
    /**
     * @return fldvalue10
     */
	public String getFldvalue10() {
		return this.fldvalue10;
	}
	
	/**
	 * @param fldvalue11
	 */
	public void setFldvalue11(String fldvalue11) {
		this.fldvalue11 = fldvalue11;
	}
	
    /**
     * @return fldvalue11
     */
	public String getFldvalue11() {
		return this.fldvalue11;
	}
	
	/**
	 * @param fldvalue12
	 */
	public void setFldvalue12(String fldvalue12) {
		this.fldvalue12 = fldvalue12;
	}
	
    /**
     * @return fldvalue12
     */
	public String getFldvalue12() {
		return this.fldvalue12;
	}
	
	/**
	 * @param fldvalue13
	 */
	public void setFldvalue13(String fldvalue13) {
		this.fldvalue13 = fldvalue13;
	}
	
    /**
     * @return fldvalue13
     */
	public String getFldvalue13() {
		return this.fldvalue13;
	}
	
	/**
	 * @param fldvalue14
	 */
	public void setFldvalue14(String fldvalue14) {
		this.fldvalue14 = fldvalue14;
	}
	
    /**
     * @return fldvalue14
     */
	public String getFldvalue14() {
		return this.fldvalue14;
	}
	
	/**
	 * @param fldvalue15
	 */
	public void setFldvalue15(String fldvalue15) {
		this.fldvalue15 = fldvalue15;
	}
	
    /**
     * @return fldvalue15
     */
	public String getFldvalue15() {
		return this.fldvalue15;
	}
	
	/**
	 * @param fldvalue16
	 */
	public void setFldvalue16(String fldvalue16) {
		this.fldvalue16 = fldvalue16;
	}
	
    /**
     * @return fldvalue16
     */
	public String getFldvalue16() {
		return this.fldvalue16;
	}
	
	/**
	 * @param fldvalue17
	 */
	public void setFldvalue17(String fldvalue17) {
		this.fldvalue17 = fldvalue17;
	}
	
    /**
     * @return fldvalue17
     */
	public String getFldvalue17() {
		return this.fldvalue17;
	}
	
	/**
	 * @param fldvalue18
	 */
	public void setFldvalue18(String fldvalue18) {
		this.fldvalue18 = fldvalue18;
	}
	
    /**
     * @return fldvalue18
     */
	public String getFldvalue18() {
		return this.fldvalue18;
	}
	
	/**
	 * @param fldvalue19
	 */
	public void setFldvalue19(String fldvalue19) {
		this.fldvalue19 = fldvalue19;
	}
	
    /**
     * @return fldvalue19
     */
	public String getFldvalue19() {
		return this.fldvalue19;
	}
	
	/**
	 * @param fldvalue20
	 */
	public void setFldvalue20(String fldvalue20) {
		this.fldvalue20 = fldvalue20;
	}
	
    /**
     * @return fldvalue20
     */
	public String getFldvalue20() {
		return this.fldvalue20;
	}
	
	/**
	 * @param fldvalue21
	 */
	public void setFldvalue21(String fldvalue21) {
		this.fldvalue21 = fldvalue21;
	}
	
    /**
     * @return fldvalue21
     */
	public String getFldvalue21() {
		return this.fldvalue21;
	}
	
	/**
	 * @param fldvalue22
	 */
	public void setFldvalue22(String fldvalue22) {
		this.fldvalue22 = fldvalue22;
	}
	
    /**
     * @return fldvalue22
     */
	public String getFldvalue22() {
		return this.fldvalue22;
	}
	
	/**
	 * @param fldvalue23
	 */
	public void setFldvalue23(String fldvalue23) {
		this.fldvalue23 = fldvalue23;
	}
	
    /**
     * @return fldvalue23
     */
	public String getFldvalue23() {
		return this.fldvalue23;
	}
	
	/**
	 * @param fldvalue24
	 */
	public void setFldvalue24(String fldvalue24) {
		this.fldvalue24 = fldvalue24;
	}
	
    /**
     * @return fldvalue24
     */
	public String getFldvalue24() {
		return this.fldvalue24;
	}
	
	/**
	 * @param fldvalue25
	 */
	public void setFldvalue25(String fldvalue25) {
		this.fldvalue25 = fldvalue25;
	}
	
    /**
     * @return fldvalue25
     */
	public String getFldvalue25() {
		return this.fldvalue25;
	}
	
	/**
	 * @param fldvalue26
	 */
	public void setFldvalue26(String fldvalue26) {
		this.fldvalue26 = fldvalue26;
	}
	
    /**
     * @return fldvalue26
     */
	public String getFldvalue26() {
		return this.fldvalue26;
	}
	
	/**
	 * @param fldvalue27
	 */
	public void setFldvalue27(String fldvalue27) {
		this.fldvalue27 = fldvalue27;
	}
	
    /**
     * @return fldvalue27
     */
	public String getFldvalue27() {
		return this.fldvalue27;
	}
	
	/**
	 * @param fldvalue28
	 */
	public void setFldvalue28(String fldvalue28) {
		this.fldvalue28 = fldvalue28;
	}
	
    /**
     * @return fldvalue28
     */
	public String getFldvalue28() {
		return this.fldvalue28;
	}
	
	/**
	 * @param fldvalue29
	 */
	public void setFldvalue29(String fldvalue29) {
		this.fldvalue29 = fldvalue29;
	}
	
    /**
     * @return fldvalue29
     */
	public String getFldvalue29() {
		return this.fldvalue29;
	}
	
	/**
	 * @param fldvalue30
	 */
	public void setFldvalue30(String fldvalue30) {
		this.fldvalue30 = fldvalue30;
	}
	
    /**
     * @return fldvalue30
     */
	public String getFldvalue30() {
		return this.fldvalue30;
	}
	
	/**
	 * @param fldvalue31
	 */
	public void setFldvalue31(String fldvalue31) {
		this.fldvalue31 = fldvalue31;
	}
	
    /**
     * @return fldvalue31
     */
	public String getFldvalue31() {
		return this.fldvalue31;
	}
	
	/**
	 * @param fldvalue32
	 */
	public void setFldvalue32(String fldvalue32) {
		this.fldvalue32 = fldvalue32;
	}
	
    /**
     * @return fldvalue32
     */
	public String getFldvalue32() {
		return this.fldvalue32;
	}
	
	/**
	 * @param fldvalue33
	 */
	public void setFldvalue33(String fldvalue33) {
		this.fldvalue33 = fldvalue33;
	}
	
    /**
     * @return fldvalue33
     */
	public String getFldvalue33() {
		return this.fldvalue33;
	}
	
	/**
	 * @param fldvalue34
	 */
	public void setFldvalue34(String fldvalue34) {
		this.fldvalue34 = fldvalue34;
	}
	
    /**
     * @return fldvalue34
     */
	public String getFldvalue34() {
		return this.fldvalue34;
	}
	
	/**
	 * @param fldvalue35
	 */
	public void setFldvalue35(String fldvalue35) {
		this.fldvalue35 = fldvalue35;
	}
	
    /**
     * @return fldvalue35
     */
	public String getFldvalue35() {
		return this.fldvalue35;
	}
	
	/**
	 * @param fldvalue36
	 */
	public void setFldvalue36(String fldvalue36) {
		this.fldvalue36 = fldvalue36;
	}
	
    /**
     * @return fldvalue36
     */
	public String getFldvalue36() {
		return this.fldvalue36;
	}
	
	/**
	 * @param fldvalue37
	 */
	public void setFldvalue37(String fldvalue37) {
		this.fldvalue37 = fldvalue37;
	}
	
    /**
     * @return fldvalue37
     */
	public String getFldvalue37() {
		return this.fldvalue37;
	}
	
	/**
	 * @param fldvalue38
	 */
	public void setFldvalue38(String fldvalue38) {
		this.fldvalue38 = fldvalue38;
	}
	
    /**
     * @return fldvalue38
     */
	public String getFldvalue38() {
		return this.fldvalue38;
	}
	
	/**
	 * @param fldvalue39
	 */
	public void setFldvalue39(String fldvalue39) {
		this.fldvalue39 = fldvalue39;
	}
	
    /**
     * @return fldvalue39
     */
	public String getFldvalue39() {
		return this.fldvalue39;
	}
	
	/**
	 * @param fldvalue40
	 */
	public void setFldvalue40(String fldvalue40) {
		this.fldvalue40 = fldvalue40;
	}
	
    /**
     * @return fldvalue40
     */
	public String getFldvalue40() {
		return this.fldvalue40;
	}
	
	/**
	 * @param fldvalue41
	 */
	public void setFldvalue41(String fldvalue41) {
		this.fldvalue41 = fldvalue41;
	}
	
    /**
     * @return fldvalue41
     */
	public String getFldvalue41() {
		return this.fldvalue41;
	}
	
	/**
	 * @param fldvalue42
	 */
	public void setFldvalue42(String fldvalue42) {
		this.fldvalue42 = fldvalue42;
	}
	
    /**
     * @return fldvalue42
     */
	public String getFldvalue42() {
		return this.fldvalue42;
	}
	
	/**
	 * @param fldvalue43
	 */
	public void setFldvalue43(String fldvalue43) {
		this.fldvalue43 = fldvalue43;
	}
	
    /**
     * @return fldvalue43
     */
	public String getFldvalue43() {
		return this.fldvalue43;
	}
	
	/**
	 * @param fldvalue44
	 */
	public void setFldvalue44(String fldvalue44) {
		this.fldvalue44 = fldvalue44;
	}
	
    /**
     * @return fldvalue44
     */
	public String getFldvalue44() {
		return this.fldvalue44;
	}
	
	/**
	 * @param fldvalue45
	 */
	public void setFldvalue45(String fldvalue45) {
		this.fldvalue45 = fldvalue45;
	}
	
    /**
     * @return fldvalue45
     */
	public String getFldvalue45() {
		return this.fldvalue45;
	}
	
	/**
	 * @param fldvalue46
	 */
	public void setFldvalue46(String fldvalue46) {
		this.fldvalue46 = fldvalue46;
	}
	
    /**
     * @return fldvalue46
     */
	public String getFldvalue46() {
		return this.fldvalue46;
	}
	
	/**
	 * @param fldvalue47
	 */
	public void setFldvalue47(String fldvalue47) {
		this.fldvalue47 = fldvalue47;
	}
	
    /**
     * @return fldvalue47
     */
	public String getFldvalue47() {
		return this.fldvalue47;
	}
	
	/**
	 * @param fldvalue48
	 */
	public void setFldvalue48(String fldvalue48) {
		this.fldvalue48 = fldvalue48;
	}
	
    /**
     * @return fldvalue48
     */
	public String getFldvalue48() {
		return this.fldvalue48;
	}
	
	/**
	 * @param fldvalue49
	 */
	public void setFldvalue49(String fldvalue49) {
		this.fldvalue49 = fldvalue49;
	}
	
    /**
     * @return fldvalue49
     */
	public String getFldvalue49() {
		return this.fldvalue49;
	}
	
	/**
	 * @param fldvalue50
	 */
	public void setFldvalue50(String fldvalue50) {
		this.fldvalue50 = fldvalue50;
	}
	
    /**
     * @return fldvalue50
     */
	public String getFldvalue50() {
		return this.fldvalue50;
	}
	
	/**
	 * @param fldvalue51
	 */
	public void setFldvalue51(String fldvalue51) {
		this.fldvalue51 = fldvalue51;
	}
	
    /**
     * @return fldvalue51
     */
	public String getFldvalue51() {
		return this.fldvalue51;
	}
	
	/**
	 * @param fldvalue52
	 */
	public void setFldvalue52(String fldvalue52) {
		this.fldvalue52 = fldvalue52;
	}
	
    /**
     * @return fldvalue52
     */
	public String getFldvalue52() {
		return this.fldvalue52;
	}
	
	/**
	 * @param fldvalue53
	 */
	public void setFldvalue53(String fldvalue53) {
		this.fldvalue53 = fldvalue53;
	}
	
    /**
     * @return fldvalue53
     */
	public String getFldvalue53() {
		return this.fldvalue53;
	}
	
	/**
	 * @param fldvalue54
	 */
	public void setFldvalue54(String fldvalue54) {
		this.fldvalue54 = fldvalue54;
	}
	
    /**
     * @return fldvalue54
     */
	public String getFldvalue54() {
		return this.fldvalue54;
	}
	
	/**
	 * @param fldvalue55
	 */
	public void setFldvalue55(String fldvalue55) {
		this.fldvalue55 = fldvalue55;
	}
	
    /**
     * @return fldvalue55
     */
	public String getFldvalue55() {
		return this.fldvalue55;
	}
	
	/**
	 * @param fldvalue56
	 */
	public void setFldvalue56(String fldvalue56) {
		this.fldvalue56 = fldvalue56;
	}
	
    /**
     * @return fldvalue56
     */
	public String getFldvalue56() {
		return this.fldvalue56;
	}
	
	/**
	 * @param fldvalue57
	 */
	public void setFldvalue57(String fldvalue57) {
		this.fldvalue57 = fldvalue57;
	}
	
    /**
     * @return fldvalue57
     */
	public String getFldvalue57() {
		return this.fldvalue57;
	}
	
	/**
	 * @param fldvalue58
	 */
	public void setFldvalue58(String fldvalue58) {
		this.fldvalue58 = fldvalue58;
	}
	
    /**
     * @return fldvalue58
     */
	public String getFldvalue58() {
		return this.fldvalue58;
	}
	
	/**
	 * @param fldvalue59
	 */
	public void setFldvalue59(String fldvalue59) {
		this.fldvalue59 = fldvalue59;
	}
	
    /**
     * @return fldvalue59
     */
	public String getFldvalue59() {
		return this.fldvalue59;
	}
	
	/**
	 * @param fldvalue60
	 */
	public void setFldvalue60(String fldvalue60) {
		this.fldvalue60 = fldvalue60;
	}
	
    /**
     * @return fldvalue60
     */
	public String getFldvalue60() {
		return this.fldvalue60;
	}
	
	/**
	 * @param fldvalue61
	 */
	public void setFldvalue61(String fldvalue61) {
		this.fldvalue61 = fldvalue61;
	}
	
    /**
     * @return fldvalue61
     */
	public String getFldvalue61() {
		return this.fldvalue61;
	}
	
	/**
	 * @param fldvalue62
	 */
	public void setFldvalue62(String fldvalue62) {
		this.fldvalue62 = fldvalue62;
	}
	
    /**
     * @return fldvalue62
     */
	public String getFldvalue62() {
		return this.fldvalue62;
	}
	
	/**
	 * @param fldvalue63
	 */
	public void setFldvalue63(String fldvalue63) {
		this.fldvalue63 = fldvalue63;
	}
	
    /**
     * @return fldvalue63
     */
	public String getFldvalue63() {
		return this.fldvalue63;
	}
	
	/**
	 * @param fldvalue64
	 */
	public void setFldvalue64(String fldvalue64) {
		this.fldvalue64 = fldvalue64;
	}
	
    /**
     * @return fldvalue64
     */
	public String getFldvalue64() {
		return this.fldvalue64;
	}
	
	/**
	 * @param fldvalue65
	 */
	public void setFldvalue65(String fldvalue65) {
		this.fldvalue65 = fldvalue65;
	}
	
    /**
     * @return fldvalue65
     */
	public String getFldvalue65() {
		return this.fldvalue65;
	}
	
	/**
	 * @param fldvalue66
	 */
	public void setFldvalue66(String fldvalue66) {
		this.fldvalue66 = fldvalue66;
	}
	
    /**
     * @return fldvalue66
     */
	public String getFldvalue66() {
		return this.fldvalue66;
	}
	
	/**
	 * @param fldvalue67
	 */
	public void setFldvalue67(String fldvalue67) {
		this.fldvalue67 = fldvalue67;
	}
	
    /**
     * @return fldvalue67
     */
	public String getFldvalue67() {
		return this.fldvalue67;
	}
	
	/**
	 * @param fldvalue68
	 */
	public void setFldvalue68(String fldvalue68) {
		this.fldvalue68 = fldvalue68;
	}
	
    /**
     * @return fldvalue68
     */
	public String getFldvalue68() {
		return this.fldvalue68;
	}
	
	/**
	 * @param fldvalue69
	 */
	public void setFldvalue69(String fldvalue69) {
		this.fldvalue69 = fldvalue69;
	}
	
    /**
     * @return fldvalue69
     */
	public String getFldvalue69() {
		return this.fldvalue69;
	}
	
	/**
	 * @param fldvalue70
	 */
	public void setFldvalue70(String fldvalue70) {
		this.fldvalue70 = fldvalue70;
	}
	
    /**
     * @return fldvalue70
     */
	public String getFldvalue70() {
		return this.fldvalue70;
	}
	
	/**
	 * @param fldvalue71
	 */
	public void setFldvalue71(String fldvalue71) {
		this.fldvalue71 = fldvalue71;
	}
	
    /**
     * @return fldvalue71
     */
	public String getFldvalue71() {
		return this.fldvalue71;
	}
	
	/**
	 * @param fldvalue72
	 */
	public void setFldvalue72(String fldvalue72) {
		this.fldvalue72 = fldvalue72;
	}
	
    /**
     * @return fldvalue72
     */
	public String getFldvalue72() {
		return this.fldvalue72;
	}
	
	/**
	 * @param fldvalue73
	 */
	public void setFldvalue73(String fldvalue73) {
		this.fldvalue73 = fldvalue73;
	}
	
    /**
     * @return fldvalue73
     */
	public String getFldvalue73() {
		return this.fldvalue73;
	}
	
	/**
	 * @param fldvalue74
	 */
	public void setFldvalue74(String fldvalue74) {
		this.fldvalue74 = fldvalue74;
	}
	
    /**
     * @return fldvalue74
     */
	public String getFldvalue74() {
		return this.fldvalue74;
	}
	
	/**
	 * @param fldvalue75
	 */
	public void setFldvalue75(String fldvalue75) {
		this.fldvalue75 = fldvalue75;
	}
	
    /**
     * @return fldvalue75
     */
	public String getFldvalue75() {
		return this.fldvalue75;
	}
	
	/**
	 * @param fldvalue76
	 */
	public void setFldvalue76(String fldvalue76) {
		this.fldvalue76 = fldvalue76;
	}
	
    /**
     * @return fldvalue76
     */
	public String getFldvalue76() {
		return this.fldvalue76;
	}
	
	/**
	 * @param fldvalue77
	 */
	public void setFldvalue77(String fldvalue77) {
		this.fldvalue77 = fldvalue77;
	}
	
    /**
     * @return fldvalue77
     */
	public String getFldvalue77() {
		return this.fldvalue77;
	}
	
	/**
	 * @param fldvalue78
	 */
	public void setFldvalue78(String fldvalue78) {
		this.fldvalue78 = fldvalue78;
	}
	
    /**
     * @return fldvalue78
     */
	public String getFldvalue78() {
		return this.fldvalue78;
	}
	
	/**
	 * @param fldvalue79
	 */
	public void setFldvalue79(String fldvalue79) {
		this.fldvalue79 = fldvalue79;
	}
	
    /**
     * @return fldvalue79
     */
	public String getFldvalue79() {
		return this.fldvalue79;
	}
	
	/**
	 * @param fldvalue80
	 */
	public void setFldvalue80(String fldvalue80) {
		this.fldvalue80 = fldvalue80;
	}
	
    /**
     * @return fldvalue80
     */
	public String getFldvalue80() {
		return this.fldvalue80;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getDisbOrgNo() {
		return disbOrgNo;
	}

	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo;
	}

	public String getFinaBrId() {
		return finaBrId;
	}

	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	public String getBelgLine() {
		return belgLine;
	}

	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}


}