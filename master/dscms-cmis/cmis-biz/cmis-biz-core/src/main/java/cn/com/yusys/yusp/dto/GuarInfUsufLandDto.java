package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfUsufLand
 * @类描述: guar_inf_usuf_land数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfUsufLandDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 不动产权证号 **/
	private String bdcCrno;
	
	/** 使用权权证号 **/
	private String useNo;
	
	/** 使用权抵押登记证号 **/
	private String useCertNo;
	
	/** 使用权登记机关 **/
	private String useCertOffice;
	
	/** 使用权取得日期 **/
	private String useCertBeginDate;
	
	/** 使用权到期日期 **/
	private String useCertEndDate;
	
	/** 使用权面积 **/
	private java.math.BigDecimal useCertArea;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 使用权位置 **/
	private String useCertLocation;
	
	/** 买卖合同编号 **/
	private String businessContractNo;
	
	/** 购买日期 **/
	private String purchaseDate;
	
	/** 购买价格（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 押品使用情况 **/
	private String guarUtilCase;
	
	/** 林地名称 **/
	private String forestName;
	
	/** 林种 **/
	private String forestVariet;
	
	/** 主要种树 **/
	private String forestMain;
	
	/** 取得方式 **/
	private String acquMode;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param bdcCrno
	 */
	public void setBdcCrno(String bdcCrno) {
		this.bdcCrno = bdcCrno == null ? null : bdcCrno.trim();
	}
	
    /**
     * @return BdcCrno
     */	
	public String getBdcCrno() {
		return this.bdcCrno;
	}
	
	/**
	 * @param useNo
	 */
	public void setUseNo(String useNo) {
		this.useNo = useNo == null ? null : useNo.trim();
	}
	
    /**
     * @return UseNo
     */	
	public String getUseNo() {
		return this.useNo;
	}
	
	/**
	 * @param useCertNo
	 */
	public void setUseCertNo(String useCertNo) {
		this.useCertNo = useCertNo == null ? null : useCertNo.trim();
	}
	
    /**
     * @return UseCertNo
     */	
	public String getUseCertNo() {
		return this.useCertNo;
	}
	
	/**
	 * @param useCertOffice
	 */
	public void setUseCertOffice(String useCertOffice) {
		this.useCertOffice = useCertOffice == null ? null : useCertOffice.trim();
	}
	
    /**
     * @return UseCertOffice
     */	
	public String getUseCertOffice() {
		return this.useCertOffice;
	}
	
	/**
	 * @param useCertBeginDate
	 */
	public void setUseCertBeginDate(String useCertBeginDate) {
		this.useCertBeginDate = useCertBeginDate == null ? null : useCertBeginDate.trim();
	}
	
    /**
     * @return UseCertBeginDate
     */	
	public String getUseCertBeginDate() {
		return this.useCertBeginDate;
	}
	
	/**
	 * @param useCertEndDate
	 */
	public void setUseCertEndDate(String useCertEndDate) {
		this.useCertEndDate = useCertEndDate == null ? null : useCertEndDate.trim();
	}
	
    /**
     * @return UseCertEndDate
     */	
	public String getUseCertEndDate() {
		return this.useCertEndDate;
	}
	
	/**
	 * @param useCertArea
	 */
	public void setUseCertArea(java.math.BigDecimal useCertArea) {
		this.useCertArea = useCertArea;
	}
	
    /**
     * @return UseCertArea
     */	
	public java.math.BigDecimal getUseCertArea() {
		return this.useCertArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param useCertLocation
	 */
	public void setUseCertLocation(String useCertLocation) {
		this.useCertLocation = useCertLocation == null ? null : useCertLocation.trim();
	}
	
    /**
     * @return UseCertLocation
     */	
	public String getUseCertLocation() {
		return this.useCertLocation;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo == null ? null : businessContractNo.trim();
	}
	
    /**
     * @return BusinessContractNo
     */	
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase == null ? null : guarUtilCase.trim();
	}
	
    /**
     * @return GuarUtilCase
     */	
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param forestName
	 */
	public void setForestName(String forestName) {
		this.forestName = forestName == null ? null : forestName.trim();
	}
	
    /**
     * @return ForestName
     */	
	public String getForestName() {
		return this.forestName;
	}
	
	/**
	 * @param forestVariet
	 */
	public void setForestVariet(String forestVariet) {
		this.forestVariet = forestVariet == null ? null : forestVariet.trim();
	}
	
    /**
     * @return ForestVariet
     */	
	public String getForestVariet() {
		return this.forestVariet;
	}
	
	/**
	 * @param forestMain
	 */
	public void setForestMain(String forestMain) {
		this.forestMain = forestMain == null ? null : forestMain.trim();
	}
	
    /**
     * @return ForestMain
     */	
	public String getForestMain() {
		return this.forestMain;
	}
	
	/**
	 * @param acquMode
	 */
	public void setAcquMode(String acquMode) {
		this.acquMode = acquMode == null ? null : acquMode.trim();
	}
	
    /**
     * @return AcquMode
     */	
	public String getAcquMode() {
		return this.acquMode;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}