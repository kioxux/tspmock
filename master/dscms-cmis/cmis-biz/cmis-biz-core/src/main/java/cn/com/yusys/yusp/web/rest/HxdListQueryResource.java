package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.DoubleViewDto;
import cn.com.yusys.yusp.dto.HxdxdInfoDto;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import cn.com.yusys.yusp.service.HxdListQueryServices;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DoubleViewResource
 * @类描述: #唤醒贷信息查询
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-30 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "唤醒贷分配")
@RequestMapping("/api/hxdmessage")
public class HxdListQueryResource {

    @Autowired
    private HxdListQueryServices hxdListQueryServices;

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 唤醒贷分配列表查询
     */
    @PostMapping("/hxlist")
    protected ResultDto<List<HxdxdInfoDto>> hxlist(@RequestBody HxdxdInfoDto hxdxdInfoDto) {
        List<HxdxdInfoDto> hxdxdInfoDtoList = hxdListQueryServices.hxlist(hxdxdInfoDto);
        return new ResultDto<List<HxdxdInfoDto>>(hxdxdInfoDtoList);
    }

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 唤醒贷客户信息查询
     */
    @PostMapping("/hxdeta")
    protected ResultDto<HxdxdInfoDto> hxdeta(@RequestBody HxdxdInfoDto hxdxdInfoDto) {
        HxdxdInfoDto hxdxdInfo = hxdListQueryServices.hxdeta(hxdxdInfoDto);
        return new ResultDto(hxdxdInfo);
    }

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 唤醒贷客户撤销操作
     */
    @PostMapping("/hxcxcz")
    protected ResultDto<Integer> hxcxcz(@RequestBody HxdxdInfoDto hxdxdInfoDto) {
        int result = hxdListQueryServices.hxcxcz(hxdxdInfoDto);
        return new ResultDto(result);
    }

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 唤醒贷客户分配操作
     */
    @PostMapping("/hxfpcz")
    protected ResultDto<Integer> hxfpcz(@RequestBody HxdxdInfoDto hxdxdInfoDto) {
        int result = hxdListQueryServices.hxfpcz(hxdxdInfoDto);
        return new ResultDto(result);
    }


}
