/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprLoanCond
 * @类描述: lmt_appr_loan_cond数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:21:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_appr_loan_cond")
public class LmtApprLoanCond extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 审批流水号 **/
	@Column(name = "APPROVE_SERNO", unique = false, nullable = true, length = 40)
	private String approveSerno;

	/** 条件类型 **/
	@Column(name = "COND_TYPE", unique = false, nullable = true, length = 5)
	private String condType;

	/** 条件说明 **/
	@Column(name = "COND_DESC", unique = false, nullable = true, length = 2000)
	private String condDesc;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}

	/**
	 * @return approveSerno
	 */
	public String getApproveSerno() {
		return this.approveSerno;
	}

	/**
	 * @param condType
	 */
	public void setCondType(String condType) {
		this.condType = condType;
	}

	/**
	 * @return condType
	 */
	public String getCondType() {
		return this.condType;
	}

	/**
	 * @param condDesc
	 */
	public void setCondDesc(String condDesc) {
		this.condDesc = condDesc;
	}

	/**
	 * @return condDesc
	 */
	public String getCondDesc() {
		return this.condDesc;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}