package cn.com.yusys.yusp.service.server.xdht0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GuarGuarantee;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdht0011.req.Xdht0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.*;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.bsp.core.dp2021.Dp2021Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0011Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-06-05 15:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0011Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0011Service.class);

    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Resource
    private LmtCobInfoMapper lmtCobInfoMapper;

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ICusClientService icusClientService;

    @Autowired
    private Dp2021Service dp2021Service;

    @Resource
    private AccLoanMapper accLoanMapper;

    @Resource
    private ToppAcctSubMapper toppAcctSubMapper;
    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Autowired
    private CommonService commonService;

    /**
     * 借款担保合同签订/放款查询
     * DzqyQdFkZyAction.java
     *
     * @param xdht0011DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0011DataRespDto queryCtrLoanContOrFk(Xdht0011DataReqDto xdht0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataReqDto));
        Xdht0011DataRespDto xdht0011DataRespDto = new Xdht0011DataRespDto();
        int result = 0;//执行结果
        Map queryMap = new HashMap();
        try {
            String certNo = xdht0011DataReqDto.getCertNo();//身份证号
            String cusType = xdht0011DataReqDto.getCusType();//客户类型
            String cusId = xdht0011DataReqDto.getCusId();//客户号
            String cusName = xdht0011DataReqDto.getCusName();//客户名称
            String managerId = xdht0011DataReqDto.getManagerId();//客户经理号
            String contNo = xdht0011DataReqDto.getContNo();//合同编号
            String signFlag = xdht0011DataReqDto.getSignFlag();//是否签约
            String chnlSour = xdht0011DataReqDto.getChnlSour();//来源渠道
            if (StringUtils.isBlank(cusType)) {
                throw new Exception("客户类型为空！");
            }
            if (StringUtils.isBlank(chnlSour)) {
                throw new Exception("来源渠道为空！");
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 01借款合同查询
            if ("01".equals(cusType)) {
                if (StringUtils.isBlank(certNo) && "01".equals(chnlSour)) {
                    throw new Exception("身份证号【手机端】必填！");
                }
                if (StringUtils.isBlank(managerId) && "02".equals(chnlSour)) {
                    throw new Exception("客户经理号【pad端】必填！");
                }

                CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
                String custId = cusBaseClientDto.getCusId();
                queryMap.put("custId", custId);
                queryMap.put("certNo", certNo);//身份证号
                queryMap.put("cusId", custId);//客户号
                queryMap.put("cusName", cusName);//客户姓名
                queryMap.put("managerId", managerId);//客户经理号
                queryMap.put("contNo", contNo);//合同号
                queryMap.put("signFlag", signFlag);//是否签约
                queryMap.put("cusType", cusType);//客户类型
                queryMap.put("chnlSour", chnlSour);//来源渠道
                List<LoanContList> loanContList = ctrLoanContMapper.selectLoanContListByCertNo(queryMap);
                if (loanContList != null && loanContList.size() > 0) {
                    List<String> ipqSernos = new ArrayList<String>();
                    List<String> contNos = new ArrayList<String>();
                    for (int i = 0; i < loanContList.size(); i++) {
                        LoanContList lcl = loanContList.get(i);
                        if (!StringUtils.isBlank(lcl.getContNo())) {
                            contNos.add(lcl.getContNo());
                        }
                        if (!StringUtils.isBlank(lcl.getIqpSerno())) {
                            ipqSernos.add(lcl.getIqpSerno());
                        }
                    }
                    if (contNos.size() > 0) {//拼接accloan表中统计的余额
                        List<LoanContList> subList = accLoanMapper.getLoanBalanceByContNos(contNos);
                        if (CollectionUtils.nonEmpty(subList)) {//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!此处底层实现可能有bug:subList结果集为空时，subList.size()=1,且所有元素都为null
                            for (int i = 0; i < loanContList.size(); i++) {
                                LoanContList lcl = loanContList.get(i);
                                String lclContNo = lcl.getContNo();
                                if (!StringUtils.isBlank(lclContNo)) {
                                    for (int j = 0; j < subList.size(); j++) {
                                        if (lclContNo.equals(subList.get(j).getContNo())) {
                                            lcl.setOutstndLmt(subList.get(j).getOutstndLmt());
                                            loanContList.set(i, lcl);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (ipqSernos.size() > 0) {//拼接topp_Acct_Sub表中字段
                        List<LoanContList> subList = toppAcctSubMapper.get3EleByIqpSernos(ipqSernos);
                        if (subList != null && subList.get(0) != null) {//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!此处底层实现可能有bug:subList结果集为空时，subList.size()=1,且所有元素都为null
                            for (int i = 0; i < loanContList.size(); i++) {
                                LoanContList lcl = loanContList.get(i);
                                String lclIqpSerno = lcl.getIqpSerno();
                                if (!StringUtils.isBlank(lclIqpSerno)) {
                                    for (int j = 0; j < subList.size(); j++) {
                                        if (lclIqpSerno.equals(subList.get(j).getIqpSerno())) {
                                            lcl.setEntruPayAcctNo(subList.get(j).getEntruPayAcctNo());
                                            lcl.setEntruPayAcctName(subList.get(j).getEntruPayAcctName());
                                            lcl.setEntruAcctb(subList.get(j).getEntruAcctb());
                                            loanContList.set(i, lcl);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                xdht0011DataRespDto.setLoanContList(loanContList);
            }

            // 担保合同查询条件
            if ("02".equals(cusType) || "02A".equals(cusType)) {
                // 02A：借款人根据合同号查询担保合同，借款人身份证号\借款合同号必填
                if ("02A".equals(cusType)) {
                    if (StringUtils.isBlank(certNo) || StringUtils.isBlank(contNo)) {
                        throw new Exception("借款人身份证号和借款合同号【当02A条件】必填！");
                    }
                } else {
                    if (StringUtils.isBlank(certNo) && "01".equals(chnlSour)) {
                        throw new Exception("身份证号【手机端】必填！");
                    }

                    if (StringUtils.isBlank(managerId) && "02".equals(chnlSour)) {
                        throw new Exception("客户经理号【pad端】必填！");
                    }
                }

                CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
                String custId = cusBaseClientDto.getCusId();
                queryMap.put("certNo", certNo);//身份证号
                queryMap.put("cusId", custId);//客户号
                queryMap.put("cusName", cusName);//客户姓名
                queryMap.put("managerId", managerId);//客户经理号
                queryMap.put("contNo", contNo);//合同号
                queryMap.put("signFlag", signFlag);//是否签约
                queryMap.put("cusType", cusType);//客户类型
                queryMap.put("chnlSour", chnlSour);//来源渠道
                queryMap.put("custId", custId);
                List<GuarContList> guarContList = ctrLoanContMapper.selectGuarLoanContByCertNo(queryMap);
                //拼接证件号方法开始
                List<GuarContList> guarContList2 = findCertCode(guarContList,certNo);
                if (guarContList2.size() > 0) {
                    guarContList = guarContList2;
                }
                //返回list
                xdht0011DataRespDto.setGuarContList(guarContList);
            }

            // 账户信息查询条件
            if ("03".equals(cusType)) {
                if (StringUtils.isBlank(cusId)) {
                    throw new Exception("客户号必填！");
                } else {
                    Dp2021ReqDto dp2021ReqDto = new Dp2021ReqDto();
                    dp2021ReqDto.setChxunbis(CmisBizConstants.NUM_TEN);
                    dp2021ReqDto.setKehuhaoo(cusId);
                    Dp2021RespDto dp2021RespDto = Optional.ofNullable(dp2021Service.dp2021(dp2021ReqDto)).orElse(new Dp2021RespDto());
                    List<Lstacctinfo> lstacctinfo = Optional.ofNullable(dp2021RespDto.getLstacctinfo()).orElse(new ArrayList<>());
                    List<AccountList> list = Optional.ofNullable(lstacctinfo.parallelStream().map(lst -> {
                        AccountList accountList = new AccountList();
                        accountList.setAcct(lst.getKehuzhao());
                        accountList.setAcctName(lst.getZhhuzwmc());
                        accountList.setSubAcctSeq(lst.getZhhaoxuh());
                        return accountList;
                    }).collect(Collectors.toList())).orElse(new ArrayList<>());
                    xdht0011DataRespDto.setAccountList(list);
                }

            }

            // 惠享贷共借人身份证查询主借款合同
            if ("04".equals(cusType)) {
                if (StringUtils.isBlank(certNo)) {
                    throw new Exception("身份证必填！");
                } else {
                    List<HxdLoanContList> hxdLoanContList = ctrLoanContMapper.selectLoanContByCertNo(certNo);
                    xdht0011DataRespDto.setHxdLoanContList(hxdLoanContList);
                }
            }

            // 惠享贷共借人签约查询
            if ("05".equals(cusType)) {
                if (StringUtils.isBlank(contNo)) {
                    throw new Exception("借款合同号为空！");
                } else {
                    String state_list = lmtCobInfoMapper.selectSignFalgByContNo(contNo);
                    if (StringUtil.isNotEmpty(state_list)) {
                        String[] stateList = state_list.split(",");
                        for (int i = 0; i < stateList.length; i++) {
                            if (!"2".equals(stateList[i])) {
                                throw new Exception("惠享贷共借人未签约！");
                            }
                        }
                    }

                    queryMap.put("contNo", contNo);//身份证号
                    queryMap.put("openDay", openDay);//客户号
                    String startdays = ctrLoanContMapper.selectStartdaysByContNo(queryMap);
                    if (startdays != null && Integer.parseInt(startdays) > 365) {
                        throw new Exception("已超过支用期限！可支用额度为0！");
                    }
                }
            }
        } catch (Exception e) {
//            xdht0011DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 成功失败标志
//            xdht0011DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value + ":" + e.getMessage());// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataRespDto));
        return xdht0011DataRespDto;
    }

    /*******
     * 跟据担保合同编号查询出担保人的证件号号
     * 并与担保合同号拼接
     * ********/
    public List<GuarContList> findCertCode(List<GuarContList> guarContList,String certNo) {
        logger.info("****************根据担保合同列表【{}】获取客户证件号开始", JSON.toJSONString(guarContList));
        List<GuarContList> guarContList2 = new ArrayList<>();
        try {
            if (CollectionUtils.nonEmpty(guarContList)) {
                int count = guarContList.size();
                for (int i = 0; i < count; i++) {
                    GuarContList guarCont = guarContList.get(i);
                    String guarContNo = guarCont.getGuarContNo();//获取担保合同号
                    String guarCertNo = "";//证件号
                    String guarCusNo = "";//担保人客户号
                    String guarCusName = "";//担保人名称
                    String dqCusId = "";//(当前登录人)的客户号
                    String certCode = "";//当前登录人证件号
                    String dbCertCodeList = "";//证件号集合
                    List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.queryCusIDByGuarContNo(guarContNo);
                    if (CollectionUtils.nonEmpty(guarGuaranteeList)) {
                        for (int j = 0; j < guarGuaranteeList.size(); j++) {
                            guarCusNo = guarGuaranteeList.get(j).getCusId();//获取保证人客户号
                            guarCertNo = guarGuaranteeList.get(j).getAssureCertCode();//保证人证
                            if(StringUtil.isEmpty(guarCertNo)){//保证人信息表中证件号为空,则去客户表查询
                                logger.info("****************根据客户号【{}】获取客户证件号开始", guarCusNo);
                                CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(guarCusNo);
                                logger.info("****************根据客户号【{}】获取客户证件号开始", JSON.toJSONString(cusBaseDto));
                                if (cusBaseDto != null) {
                                    guarCertNo = cusBaseDto.getCertCode();
                                    if(guarCertNo.equals(certNo)){
                                        certCode = guarCertNo;
                                        dqCusId = cusBaseDto.getCusId();
                                        guarCusName = cusBaseDto.getCusName();
                                    }
                                }
                            }
                            //拼接信息
                            if(StringUtil.isEmpty(dbCertCodeList)){
                                dbCertCodeList =  guarCertNo;
                            }else{
                                dbCertCodeList = dbCertCodeList + "/" + guarCertNo;
                            }
                        }
                    }
                    if (StringUtil.isNotEmpty(dbCertCodeList)) {
                        guarContNo = guarContNo + "#" + dbCertCodeList;
                        guarCont.setGuarContNo(guarContNo);
                        guarCont.setGuarCusNo(dqCusId);//担保人客户号
                        guarCont.setGuarCertNo(certCode);//担保人证件号
                        guarCont.setGuarCusName(guarCusName);
                        guarContList2.add(guarCont);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("****************根据担保合同列表【{}】获取客户证件号结束", JSON.toJSONString(guarContList2));
        return guarContList2;
    }

}
