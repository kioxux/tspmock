package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CfgDocParamsDetail;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsList
 * @类描述: cfg_doc_params_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgDocParamsListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 配置流水号 **/
	private String cdplSerno;
	
	/** 档案分类 **/
	private String docClass;
	
	/** 档案类型 **/
	private String docType;
	
	/** 是否启用 **/
	private String isBegin;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 档案归档关联档案资料清单表 **/
	private List<CfgDocParamsDetail> cfgDocParamsDetail;

	public List<CfgDocParamsDetail> getCfgDocParamsDetail() {
		return cfgDocParamsDetail;
	}

	public void setCfgDocParamsDetail(List<CfgDocParamsDetail> plaExpenseDetail) {
		this.cfgDocParamsDetail = plaExpenseDetail;
	}
	
	/**
	 * @param cdplSerno
	 */
	public void setCdplSerno(String cdplSerno) {
		this.cdplSerno = cdplSerno == null ? null : cdplSerno.trim();
	}
	
    /**
     * @return CdplSerno
     */	
	public String getCdplSerno() {
		return this.cdplSerno;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass == null ? null : docClass.trim();
	}
	
    /**
     * @return DocClass
     */	
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType == null ? null : docType.trim();
	}
	
    /**
     * @return DocType
     */	
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin == null ? null : isBegin.trim();
	}
	
    /**
     * @return IsBegin
     */	
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}