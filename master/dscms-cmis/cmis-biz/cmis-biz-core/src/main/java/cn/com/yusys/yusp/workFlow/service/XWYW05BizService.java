package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class XWYW05BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(XWYW05BizService.class);//定义log

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private OtherItemAppService otherItemAppService;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07BizService;
    @Autowired
    private CentralFileTaskService centralFileTaskService;
    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if (CmisFlowConstants.FLOW_TYPE_TYPE_XW011.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_XW012.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_XW013.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_XW014.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_XW015.equals(bizType)) {
            handleLmtAppBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }



    // 小微对公客户授信申请
    private void handleLmtAppBiz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        String logPrefix = "小微对公客户授信申请" + serno + "流程操作:";
        log.info(logPrefix + currentOpType + "后业务处理");
        try {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            // 获取当前业务申请路程类型
            String bizType = resultInstanceDto.getBizType();
            put2VarParam(resultInstanceDto,serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info(logPrefix + "流程发起操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info(logPrefix + "流程提交操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterStart(serno);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info(logPrefix + "跳转操作，流程参数：" + resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info(logPrefix + "流程同意操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterEnd(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info(logPrefix + "退回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info(logPrefix + "打回操作，流程参数：" + resultInstanceDto.toString());
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    lmtAppService.handleBusinessAfterBack(serno);
                }
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info(logPrefix + "否决操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterRefuse(serno, currentUserId, currentOrgId, resultInstanceDto.getFlowCode());
            } else if (OpType.RE_START.equals(currentOpType)) {
                log.info(logPrefix + "再议操作操作，流程参数：" + resultInstanceDto.toString());
                lmtAppService.handleBusinessAfterReStart(serno);
            } else {
                log.warn(logPrefix + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * put2VarParam
     *
     * @param resultInstanceDto
     * @param serno
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        Map guarModeDetail = lmtAppSubPrdService.getGuarModeDetail(serno);
        params.put("xyLmtAmt",guarModeDetail.get("xyLmtAmt"));
        params.put("bzLmtAmt",guarModeDetail.get("bzLmtAmt"));
        params.put("cdlcLmtAmt",guarModeDetail.get("cdlcLmtAmt"));
        params.put("pldLmtAmt",guarModeDetail.get("pldLmtAmt"));
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }
    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
//        return CmisFlowConstants.FLOW_ID_XWYW05.equals(flowCode);
       return CmisFlowConstants.FLOW_ID_XWYW05.equals(flowCode);
    }


}
