/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralCreditCardTask
 * @类描述: central_credit_card_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "central_credit_card_task")
public class CentralCreditCardTask extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 业务类型细分 **/
	@Column(name = "BIZ_SUB_TYPE", unique = false, nullable = true, length = 10)
	private String bizSubType;
	
	/** 任务类型 **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 5)
	private String taskType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 100)
	private String cusName;
	
	/** 证件号码 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 20)
	private String certType;
	
	/** 单位名称 **/
	@Column(name = "CPRT_NAME", unique = false, nullable = true, length = 100)
	private String cprtName;
	
	/** 申请卡产品 **/
	@Column(name = "CREDIT_CARD_TYPE", unique = false, nullable = true, length = 40)
	private String creditCardType;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 5)
	private String appChnl;
	
	/** 任务生成时间 **/
	@Column(name = "TASK_START_TIME", unique = false, nullable = true, length = 20)
	private String taskStartTime;
	
	/** 任务加急标识 **/
	@Column(name = "TASK_URGENT_FLAG", unique = false, nullable = true, length = 5)
	private String taskUrgentFlag;
	
	/** 接收人 **/
	@Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 100)
	private String receiverId;
	
	/** 接收机构 **/
	@Column(name = "RECEIVER_ORG", unique = false, nullable = true, length = 40)
	private String receiverOrg;
	
	/** 任务状态 **/
	@Column(name = "TASK_STATUS", unique = false, nullable = true, length = 5)
	private String taskStatus;
	
	/** 审批状态 **/
	@Column(name = "APPR_STATUS", unique = false, nullable = true, length = 5)
	private String apprStatus;
	
	/** 作废原因 **/
	@Column(name = "CANCEL_RESN", unique = false, nullable = true, length = 1000)
	private String cancelResn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizSubType
	 */
	public void setBizSubType(String bizSubType) {
		this.bizSubType = bizSubType;
	}
	
    /**
     * @return bizSubType
     */
	public String getBizSubType() {
		return this.bizSubType;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param cprtName
	 */
	public void setCprtName(String cprtName) {
		this.cprtName = cprtName;
	}
	
    /**
     * @return cprtName
     */
	public String getCprtName() {
		return this.cprtName;
	}
	
	/**
	 * @param creditCardType
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	
    /**
     * @return creditCardType
     */
	public String getCreditCardType() {
		return this.creditCardType;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param taskStartTime
	 */
	public void setTaskStartTime(String taskStartTime) {
		this.taskStartTime = taskStartTime;
	}
	
    /**
     * @return taskStartTime
     */
	public String getTaskStartTime() {
		return this.taskStartTime;
	}
	
	/**
	 * @param taskUrgentFlag
	 */
	public void setTaskUrgentFlag(String taskUrgentFlag) {
		this.taskUrgentFlag = taskUrgentFlag;
	}
	
    /**
     * @return taskUrgentFlag
     */
	public String getTaskUrgentFlag() {
		return this.taskUrgentFlag;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
    /**
     * @return receiverId
     */
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverOrg
	 */
	public void setReceiverOrg(String receiverOrg) {
		this.receiverOrg = receiverOrg;
	}
	
    /**
     * @return receiverOrg
     */
	public String getReceiverOrg() {
		return this.receiverOrg;
	}
	
	/**
	 * @param taskStatus
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
    /**
     * @return taskStatus
     */
	public String getTaskStatus() {
		return this.taskStatus;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param cancelResn
	 */
	public void setCancelResn(String cancelResn) {
		this.cancelResn = cancelResn;
	}
	
    /**
     * @return cancelResn
     */
	public String getCancelResn() {
		return this.cancelResn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}