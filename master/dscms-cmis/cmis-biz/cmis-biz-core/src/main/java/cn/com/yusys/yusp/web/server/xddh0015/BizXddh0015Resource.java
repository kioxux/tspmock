package cn.com.yusys.yusp.web.server.xddh0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0015.req.Xddh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0015.resp.List;
import cn.com.yusys.yusp.dto.server.xddh0015.resp.Xddh0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:优农贷贷后预警通知
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0015:优农贷贷后预警通知")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXddh0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0015Resource.class);

    /**
     * 交易码：xddh0015
     * 交易描述：优农贷贷后预警通知
     *
     * @param xddh0015DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷贷后预警通知")
    @PostMapping("/xddh0015")
    protected @ResponseBody
    ResultDto<Xddh0015DataRespDto> xddh0015(@Validated @RequestBody Xddh0015DataReqDto xddh0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015DataReqDto));
        Xddh0015DataRespDto xddh0015DataRespDto = new Xddh0015DataRespDto();// 响应Dto:优农贷贷后预警通知
        ResultDto<Xddh0015DataRespDto> xddh0015DataResultDto = new ResultDto<>();
        String certNo = xddh0015DataReqDto.getCertNo();//身份证号
        String billNo = xddh0015DataReqDto.getBillNo();//借据号
        String irAdjustType = xddh0015DataReqDto.getIrAdjustType();//利率调整方式
        String LPRInterzone = xddh0015DataReqDto.getLPRInterzone();//lpr区间
        BigDecimal irFloatNum = xddh0015DataReqDto.getIrFloatNum();//浮动点数
        String level = xddh0015DataReqDto.getLevel();//层级
        String debit = xddh0015DataReqDto.getDebit();//借款人
        String debitCertNo = xddh0015DataReqDto.getDebitCertNo();//借款人身份证号
        String contNo = xddh0015DataReqDto.getContNo();//合同号
        BigDecimal LPRRate = xddh0015DataReqDto.getLPRRate();//lpr利率
        String addDeclFlag = xddh0015DataReqDto.getAddDeclFlag();//加减标志
        String isHouseLoan = xddh0015DataReqDto.getIsHouseLoan();//是否为房贷
        BigDecimal realityIrY = xddh0015DataReqDto.getRealityIrY();//执行利率年
        try {
            // 从xddh0015DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0015DataRespDto对象开始
            List list = new List();
            xddh0015DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xddh0015DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            list.setCommonDebitCertNo(StringUtils.EMPTY);// 共同借款人证件号
            list.setCommonDebitName(StringUtils.EMPTY);// 共同借款人名称
            list.setSignFlag(StringUtils.EMPTY);// 是否签约
            list.setSubRela(StringUtils.EMPTY);// 主副关系
            xddh0015DataRespDto.setList(Arrays.asList(list));
            // TODO 封装xddh0015DataRespDto对象结束
            // 封装xddh0015DataResultDto中正确的返回码和返回信息
            xddh0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, e.getMessage());
            // 封装xddh0015DataResultDto中异常返回码和返回信息
            xddh0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0015DataRespDto到xddh0015DataResultDto中
        xddh0015DataResultDto.setData(xddh0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015DataResultDto));
        return xddh0015DataResultDto;
    }
}
