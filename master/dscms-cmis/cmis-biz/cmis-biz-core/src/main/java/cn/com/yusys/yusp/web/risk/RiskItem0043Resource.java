package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtAppSubService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: riskItem0042Resource
 * @类描述: 复审任务发起次数控制分项拦截
 * @功能描述:
 * @创建人: liuzz
 * @创建时间: 2021-07-13 14:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0042授信担保信息校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0043")
public class RiskItem0043Resource {

    @Autowired
    private LmtAppService lmtAppService;

    /**
     * @方法名称: riskItem0043
     * @方法描述: 复审任务发起次数控制分项拦截
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuzz
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "授信担保信息校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0043(@RequestBody QueryModel queryModel) {
        return ResultDto.success(lmtAppService.riskItem0043(queryModel.getCondition().get("bizId").toString()));
    }
}