/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDelayInfo
 * @类描述: doc_read_delay_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-19 09:36:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

public class DocReadDelayInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 延期申请流水号 **/

	private String drdiSerno;
	
	/** 调阅流水号 **/

	private String draiSerno;
	
	/** 延期归还日期 **/

	private String delayBackDate;
	/***调阅原因**/
	private String readReason;

	/** 归还日期 **/
	private String backDate;
	/** 延期申请人 **/
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="delayRqstrName")
	private String delayRqstrId;
	
	/** 延期申请机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName" ,refFieldName="delayRqstrOrgName")
	private String delayRqstrOrg;
	
	/** 延期申请日期 **/
	private String delayRqstrDate;
	
	/** 延期情况说明 **/

	private String delayCaseDesc;
	
	/** 审批状态 **/

	private String approveStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;

	/** 调阅类型 **/
	private String readType;
	
	/**
	 * @param drdiSerno
	 */
	public void setDrdiSerno(String drdiSerno) {
		this.drdiSerno = drdiSerno;
	}
	
    /**
     * @return drdiSerno
     */
	public String getDrdiSerno() {
		return this.drdiSerno;
	}
	
	/**
	 * @param draiSerno
	 */
	public void setDraiSerno(String draiSerno) {
		this.draiSerno = draiSerno;
	}
	
    /**
     * @return draiSerno
     */
	public String getDraiSerno() {
		return this.draiSerno;
	}
	
	/**
	 * @param delayBackDate
	 */
	public void setDelayBackDate(String delayBackDate) {
		this.delayBackDate = delayBackDate;
	}
	
    /**
     * @return delayBackDate
     */
	public String getDelayBackDate() {
		return this.delayBackDate;
	}
	
	/**
	 * @param delayRqstrId
	 */
	public void setDelayRqstrId(String delayRqstrId) {
		this.delayRqstrId = delayRqstrId;
	}
	
    /**
     * @return delayRqstrId
     */
	public String getDelayRqstrId() {
		return this.delayRqstrId;
	}
	
	/**
	 * @param delayRqstrOrg
	 */
	public void setDelayRqstrOrg(String delayRqstrOrg) {
		this.delayRqstrOrg = delayRqstrOrg;
	}
	
    /**
     * @return delayRqstrOrg
     */
	public String getDelayRqstrOrg() {
		return this.delayRqstrOrg;
	}
	
	/**
	 * @param delayRqstrDate
	 */
	public void setDelayRqstrDate(String delayRqstrDate) {
		this.delayRqstrDate = delayRqstrDate;
	}
	
    /**
     * @return delayRqstrDate
     */
	public String getDelayRqstrDate() {
		return this.delayRqstrDate;
	}
	
	/**
	 * @param delayCaseDesc
	 */
	public void setDelayCaseDesc(String delayCaseDesc) {
		this.delayCaseDesc = delayCaseDesc;
	}
	
    /**
     * @return delayCaseDesc
     */
	public String getDelayCaseDesc() {
		return this.delayCaseDesc;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


	public String getReadReason() {
		return readReason;
	}

	public void setReadReason(String readReason) {
		this.readReason = readReason;
	}

	public String getBackDate() {
		return backDate;
	}

	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}

	public String getReadType() {
		return readType;
	}

	public void setReadType(String readType) {
		this.readType = readType;
	}
}