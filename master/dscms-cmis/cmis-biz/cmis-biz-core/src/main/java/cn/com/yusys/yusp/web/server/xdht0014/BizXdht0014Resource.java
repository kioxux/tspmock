package cn.com.yusys.yusp.web.server.xdht0014;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0014.req.Xdht0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0014.Xdht0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:合同签订查询VX
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDHT0014:合同签订查询VX")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0014Resource.class);

    @Autowired
    private Xdht0014Service xdht0014Service;

    /**
     * 交易码：xdht0014
     * 交易描述：合同签订查询VX
     *
     * @param xdht0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订查询VX")
    @PostMapping("/xdht0014")
    protected @ResponseBody
    ResultDto<Xdht0014DataRespDto> xdht0014(@Validated @RequestBody Xdht0014DataReqDto xdht0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataReqDto));
        Xdht0014DataRespDto xdht0014DataRespDto = new Xdht0014DataRespDto();// 响应Dto:合同签订查询VX
        ResultDto<Xdht0014DataRespDto> xdht0014DataResultDto = new ResultDto<>();

        try {
            // 从xdht0014DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataReqDto));
            xdht0014DataRespDto = xdht0014Service.getContSignInfo(xdht0014DataReqDto.getIndgtSerno());
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataRespDto));
            // 封装xdht0014DataResultDto中正确的返回码和返回信息
            xdht0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, e.getMessage());
            // 封装xdht0014DataResultDto中异常返回码和返回信息
            xdht0014DataResultDto.setCode(Optional.ofNullable(e.getCode()).orElse(EpbEnum.EPB099999.key));
            xdht0014DataResultDto.setMessage(Optional.ofNullable(e.getMessage()).orElse(EpbEnum.EPB099999.value));
        }
        // 封装xdht0014DataRespDto到xdht0014DataResultDto中
        xdht0014DataResultDto.setData(xdht0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataResultDto));
        return xdht0014DataResultDto;
    }
}
