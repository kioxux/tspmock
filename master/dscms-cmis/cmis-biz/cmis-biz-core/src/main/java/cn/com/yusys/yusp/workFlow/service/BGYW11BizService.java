package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpPrePayment;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.req.Ln3042ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.resp.Ln3042RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import cn.com.yusys.yusp.util.BizUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:20
 * @desc 主动还款
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW11BizService implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW11BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IqpPrePaymentService iqpPrePaymentService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private   CmisLmtClientService  cmisLmtClientService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;






    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // BG022主动还款（对公）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG022.equals(bizType)) {
            iqpBillAcctChgBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    // 流程处理
    private void iqpBillAcctChgBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {

            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            IqpPrePayment iqpPrePayment = iqpPrePaymentService.selectByPrimaryKey(iqpSerno);
            log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
            if (iqpPrePayment == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("开始处理流程操作------");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步-- ----" + instanceInfo);
                // 改变标志 -> 审批中
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                //调用核心接口还款
                ResultDto<Ln3041RespDto> ln3041ResultDto = this.sendHxToRepay(iqpSerno);
                String ln3041Meesage = Optional.ofNullable(ln3041ResultDto.getMessage())
                        .orElse(SuccessEnum.SUCCESS.value);
                if (!Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3041ResultDto.getCode())) {
                    iqpPrePayment.setStatus("0"); // 还款失败
                    iqpPrePayment.setResn("核心返回："+ ln3041Meesage);
                } else {
                    iqpPrePayment.setStatus("1"); // 还款成功
                    iqpPrePayment.setResn("核心返回流水号："+ ln3041Meesage);
                   // 修改贷款余额：信贷发送ln3041交易成功后，esb会继续调用核心ln3100交易，将查询的内容返回给信贷（gxloan）xdtz0059，并且返回给国结
                    this.changeAccLoan(iqpSerno);
                }
                if(null != ln3041ResultDto && null != ln3041ResultDto.getData()){
                    iqpPrePayment.setHxSerno(ln3041ResultDto.getData().getJiaoyils());
                    iqpPrePayment.setHxDate(ln3041ResultDto.getData().getJiaoyirq());
                }
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
                // 推送用印系统
               /* try {
                    cmisBizXwCommonService.sendYk(currentUserId,iqpSerno,iqpPrePayment.getCusId());
                    log.info("推送印系统成功:【{}】", iqpSerno);
                } catch (Exception e) {
                    log.info("推送印系统异常:【{}】", e.getMessage());
                }*/
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 退回改变标志 审批中 111 -> 打回 992
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 打回改变标志 （若打回至初始节点）审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpPrePaymentService.updateSelective(iqpPrePayment);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                //项目拿回初始节点,状态改为追回 991
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                iqpPrePayment.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpPrePaymentService.updateSelective(iqpPrePayment);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    public ResultDto<Ln3041RespDto> sendHxToRepay(String iqpSerno) {
        ResultDto<Ln3041RespDto> ln3041ResultDto = null;
        //查询数据
        IqpPrePayment iqp = iqpPrePaymentService.selectByPrimaryKey(iqpSerno);
        String repayType = iqp.getRepayType();//贷款收回方式  02 保证金代偿
        String repayMode = iqp.getRepayMode();// 01 提前还款
        String isOtherAcct = iqp.getIsOtherAcct();//是否第三方 1
        String belgLine = "";
        String contCurType = "";
        String term = "";
        String loanEndDate = "";
        // 获取责任人
        String managerBrId = iqp.getManagerBrId();
        if(null == managerBrId ){
            throw BizException.error(null, EcbEnum.ECB010001.key, "责任机构为空请确认!");
        }
        // 账务机构
        String brchno = managerBrId.substring(0, managerBrId.length()-1) +"1";
        // 村镇银行账务机构处理
        if(brchno.startsWith("80")){ // 寿光村镇银行
            brchno = "800101";  // 寿光村镇银行营业部
        }
        if(brchno.startsWith("81")){ // 东海村镇银行
            brchno = "810101";  // 东海村镇银行营业部
        }

        // 是否委托贷款 台账表不一样
        String prdId = iqp.getPrdId();
        if("042175".equals(prdId)){
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(iqp.getBillNo());
            belgLine = "03"; // 委托贷款默认对公
            contCurType = accEntrustLoan.getContCurType();
            term = accEntrustLoan.getLoanTerm();
            loanEndDate = accEntrustLoan.getLoanEndDate();
        } else {
            AccLoan accLoan = accLoanService.selectByBillNo(iqp.getBillNo());
            belgLine = accLoan.getBelgLine();
            contCurType = accLoan.getContCurType();
            term = accLoan.getLoanTerm();
            loanEndDate = accLoan.getLoanEndDate();
        }
        Ln3041ReqDto reqDto = new Ln3041ReqDto();
        reqDto.setBrchno(brchno);
        // 3 直接还款  1 通知还款
        if ("01".equals(belgLine)) {//业务线是小微
            reqDto.setDaikczbz("3");
        } else {
            if ("01".equals(repayMode) || "1".equals(isOtherAcct) || "02".equals(repayType)) {
                reqDto.setDaikczbz("1");
            } else {
                reqDto.setDaikczbz("3");
            }
        }
        if(!"02".equals(iqp.getLoanAppChnl()) && !"G".equals(iqp.getLoanAppChnl()) && !"".equals(iqp.getLoanAppChnl()) && null != iqp.getLoanAppChnl()){// 非PC端直同  G/02	PC端
            reqDto.setDaikczbz("3");
        }

        reqDto.setDkjiejuh(iqp.getBillNo());		//贷款借据号
        reqDto.setDkzhangh("");		//贷款账号   核心说是可以不传
        reqDto.setHetongbh(iqp.getContNo());		//合同编号
        reqDto.setKehuhaoo(iqp.getCusId());		//客户号
        reqDto.setKehuzwmc(iqp.getCusName());		//客户名
        // 货币代号转换
        reqDto.setHuobdhao(BizUtils.changCurType(contCurType));
        // reqDto.setQixiriqi(iqp.getLoanStartDate().replaceAll("-",""));		//起息日期
        BigDecimal loanTerm = new BigDecimal(term);
        if ("1".equals(iqp.getIsPeriod())) {//缩期 才传期限
            String times = (loanTerm.add(iqp.getPeriodTimes())).stripTrailingZeros().toPlainString();
            reqDto.setDkqixian(times+"M");		//贷款期限(月)
            // 最终到期日
            String endDate = DateUtils.addMonth(loanEndDate, "yyyy-MM-dd",iqp.getPeriodTimes().intValue());
            reqDto.setDaoqriqi(endDate.replace("-",""));
        }
        reqDto.setZhchbjin(iqp.getZcbjAmt());		//正常本金
        reqDto.setYuqibjin(iqp.getYqbjAmt());		//逾期本金
        reqDto.setDzhibjin(iqp.getDzbjAmt());		//呆滞本金
        reqDto.setDaizbjin(iqp.getDaizbjAmt());		//呆账本金
        reqDto.setYsyjlixi(iqp.getYsyjlxXmt());		//应收应计利息
        reqDto.setCsyjlixi(iqp.getCsyjlxAmt());		//催收应计利息
        reqDto.setYsqianxi(iqp.getYsqxAmt());		//应收欠息
        reqDto.setCsqianxi(iqp.getCsqxAmt());		//催收欠息
        reqDto.setYsyjfaxi(iqp.getYsyjfxAmt());		//应收应计罚息
        reqDto.setCsyjfaxi(iqp.getCsyjfxAmt());		//催收应计罚息
        reqDto.setYshofaxi(iqp.getYsfxAmt());		//应收罚息
        reqDto.setCshofaxi(iqp.getCsfxAmt());		//催收罚息
        reqDto.setYingjifx(iqp.getYjfxAmt());		//应计复息
        reqDto.setFuxiiiii(iqp.getFxAmt());		//复息
        reqDto.setBenjheji(iqp.getTotalBjAmt());		//本金合计
        reqDto.setLixiheji(iqp.getTotalTqlxAmt());		//利息合计
//                reqDto.setQiankzee("");		//欠款总额
        /**
         * 核心             信贷
         * 1--结清贷款
         * 2--归还欠款      02	归还拖欠
         * 3--提前还款       01	提前还款
         */
        reqDto.setHuankzle("01".equals(iqp.getRepayMode()) ? "3" : "2" );		//还款种类
        BigDecimal paymAmt = "01".equals(iqp.getRepayMode()) ? iqp.getRepayPriAmt() : iqp.getRepayAmt();
        BigDecimal totalTqlxAmt = Optional.ofNullable(iqp.getTotalTqlxAmt()).orElse(new BigDecimal(0));
        // 还款种类等于3--提前还款时
        if ("3".equals(reqDto.getHuankzle())) {

            /*
              信贷码值:                                       核心
             01	提前归还本金及全部利息                      2--指定总额
             02	提前归还本金及本金对应利息                  1--指定本金
             03	灵活还款
             04	归还拖欠的本息                                 ""
             */
            /*
             * 信贷码值
             * 01--提前归还本金及全部利息
             * 02--提前归还本金及本金对应利息
             * 03--灵活还款
             * 04--归还拖欠的本息
             *
             * 核心码值
             * 0--不还息
             * 1--部分还息
             * 2--全部还息
             */
            if ("01".equals(iqp.getRepayModeDesc())) { // 提前还款全部利息
                paymAmt = paymAmt.add(totalTqlxAmt);
                reqDto.setDktqhkzl("2");		// 提前还款种类 2--指定总额
                reqDto.setTqhkhxfs("2");        // 提前还款还息方式 2--全部还息
            } else if ("02".equals(iqp.getRepayModeDesc())) {
                reqDto.setDktqhkzl("1");		// 提前还款种类 1--指定本金
                reqDto.setTqhkhxfs("1");        // 提前还款还息方式 1--部分还息
            }
            // 03--灵活还款 特殊处理

        }

//                reqDto.setTqhktysx("");		//退客户预收息金额
//                reqDto.setYshxhbje("");		//预收息还本金额
        /**
         * 0--不变
         * 1--摊薄
         * 2--缩期
         * 3--自动缩期
         */
        reqDto.setTqhktzfs( "1".equals(iqp.getIsPeriod()) ? "2" : "1");		//提前还款调整计划方式

//        reqDto.setHuankjee( "01".equals(iqp.getRepayMode()) ? iqp.getRepayPriAmt() : iqp.getRepayAmt());		//还款金额

        reqDto.setHuankjee(paymAmt);		//还款金额
        /**
         * 核心             信贷
         * 0--现金          01	转账
         * 1--转账          02	待销账
         * 2--待销账        03	内部资金账
         * 3--内部资金账    04	内部会计账
         * 4--内部会计账    05	质押存单
         */
        //资金来源
        if("01".equals(iqp.getAmtSource())){
            reqDto.setZijnlaiy("1");
        }else if("02".equals(iqp.getAmtSource())){
            reqDto.setZijnlaiy("2");
        }else if("03".equals(iqp.getAmtSource())){
            reqDto.setZijnlaiy("3");
        }else if("04".equals(iqp.getAmtSource())){
            reqDto.setZijnlaiy("4");
        }else{
            // 改用ln3047
            reqDto.setZijnlaiy("1");
        }

        reqDto.setHuankzhh(iqp.getRepayAcctNo());		//还款账号
        reqDto.setSfqzjieq("");		//强制结清标志
        reqDto.setHkzhhzxh(iqp.getRepaySubAccno());		//还款账号子序号
//                reqDto.setTqhkfjje("");		//提前还款罚金金额
        /**
         * 01--借款人还款
         * 02--担保人还款
         * 03--第三方还款
         * 04--以物抵债
         * 05--资产转让
         * 06--转让资产回购
         * 07--资产证券化
         * 08--证券化资产回购
         * 09--化债
         * 10--再化债
         * 11--借新还旧
         * 12--政策性还款
         * 13--资产重组
         * 14--核销
         * 15--债权减免
         * 16--其他
         */
        reqDto.setDaikghfs("1".equals(iqp.getIsOtherAcct()) ? "03" : "01");		//贷款归还方式
        reqDto.setHkbeizhu("");		//还款备注
//                reqDto.setFajiftbl("");		//罚金分摊比例
        reqDto.setFajirzzh("");		//罚金入账账号
        reqDto.setFajirzxh("");		//罚金入账子序号
        reqDto.setPingzhzl("");		//凭证种类
        reqDto.setPingzhma("");		//凭证批号
        reqDto.setPngzxhao("");		//凭证序号
        reqDto.setZhfutojn("");		//支付条件
        reqDto.setJiaoymma("");		//交易密码
        reqDto.setMimazlei("");		//密码种类
        reqDto.setZhjnzlei("");		//证件种类
        reqDto.setZhjhaoma("");   //还贷证明书
        if ("03".equals(iqp.getRepayModeDesc())) {
            Ln3042ReqDto ln3042ReqDto = new Ln3042ReqDto();
            BeanUtils.copyProperties(reqDto, ln3042ReqDto);
            // 提前还款的灵活还款 paymAmt是此次还的本金 归还欠款的灵活还款paymAmt是此次还的金额
            if ("01".equals(iqp.getRepayMode())) {
                paymAmt = paymAmt.add(totalTqlxAmt);
            }
            ln3042ReqDto.setZdhkjine(paymAmt);
            ResultDto<Ln3042RespDto> ln3042ResultDto = dscms2CoreLnClientService.ln3042(ln3042ReqDto);
            ln3041ResultDto = new ResultDto<Ln3041RespDto>();
            ln3041ResultDto.setCode(ln3042ResultDto.getCode());
            ln3041ResultDto.setMessage(Optional.ofNullable(ln3042ResultDto.getMessage())
                    .orElse(SuccessEnum.SUCCESS.value));
            if(null != ln3042ResultDto && null != ln3042ResultDto.getData()){
                iqp.setHxSerno(ln3042ResultDto.getData().getJiaoyils());
                iqp.setHxDate(ln3042ResultDto.getData().getJiaoyirq());
                iqpPrePaymentService.updateSelective(iqp);
            }
        } else if ("05".equals(iqp.getAmtSource())) {// 存单质押
            Ln3047ReqDto ln3047ReqDto = new Ln3047ReqDto();
            // 账务机构
            ln3047ReqDto.setBrchno(brchno);
            // 操作标志 daikczbz
            ln3047ReqDto.setDaikczbz(reqDto.getDaikczbz());
            // dkjiejuh 贷款借据号
            ln3047ReqDto.setDkjiejuh(reqDto.getDkjiejuh());
            // huobdhao 货币代号
            ln3047ReqDto.setHuobdhao(reqDto.getHuobdhao());
            //  huankjee 还款金额
            // 提前还款的灵活还款 paymAmt是此次还的本金 归还欠款的灵活还款paymAmt是此次还的金额
            if ("01".equals(iqp.getRepayMode()) && "03".equals(iqp.getRepayModeDesc())) {
                paymAmt = paymAmt.add(totalTqlxAmt);
            }
            ln3047ReqDto.setHuankjee(paymAmt);
            // 	zdzyzhbz 指定质押账号标志 1-是 2 否
            ln3047ReqDto.setZdzyzhbz("1");
            // djiebhao 冻结编号
            ln3047ReqDto.setDjiebhao("");
            //	huankzhh 质押账号
            ln3047ReqDto.setHuankzhh(reqDto.getHuankzhh());
            // hkzhhzxh 质押账号子序号
            ln3047ReqDto.setHkzhhzxh(reqDto.getHkzhhzxh());
           // zjzrzhao 资金转入账号
            ln3047ReqDto.setZjzrzhao("");
            // zjzrzzxh 资金转入账号子序号
            ln3047ReqDto.setZjzrzzxh("");
            // huandzms 还贷证明书
            ln3047ReqDto.setHuandzms("");
            ResultDto<Ln3047RespDto> ln3047ResultDto = dscms2CoreLnClientService.ln3047(ln3047ReqDto);
            ln3041ResultDto = new ResultDto<Ln3041RespDto>();
            ln3041ResultDto.setCode(ln3047ResultDto.getCode());
            ln3041ResultDto.setMessage(Optional.ofNullable(ln3047ResultDto.getMessage())
                    .orElse(SuccessEnum.SUCCESS.value));
            if(null != ln3047ResultDto && null != ln3047ResultDto.getData()){
                iqp.setHxSerno(ln3047ResultDto.getData().getJiaoyils());
                iqp.setHxDate(ln3047ResultDto.getData().getJiaoyirq());
                iqpPrePaymentService.updateSelective(iqp);
            }
        }else {
            ln3041ResultDto = dscms2CoreLnClientService.ln3041(reqDto);
        }
        return ln3041ResultDto;
    }

    public Integer changeAccLoan(String iqpSerno){
        IqpPrePayment iqp = iqpPrePaymentService.selectByPrimaryKey(iqpSerno);
        String bill_no = iqp.getBillNo();
        AccLoan accLoan = accLoanService.selectByBillNo(bill_no);
        BigDecimal loanTerm = new BigDecimal(accLoan.getLoanTerm());
        if("1".equals(iqp.getIsPeriod())){//缩期
            String times = (loanTerm.add(iqp.getPeriodTimes())).stripTrailingZeros().toPlainString();
            accLoan.setLoanTerm(times);
            // 最终到期日
            accLoan.setLoanEndDate(DateUtils.addMonth(accLoan.getLoanEndDate(), "yyyy-MM-dd",iqp.getPeriodTimes().intValue()));		//最终到期日
        }
        return accLoanService.update(accLoan);
    }
    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW11.equals(flowCode);
    }
}

