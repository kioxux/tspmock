package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfLivingRoom
 * @类描述: guar_inf_living_room数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfLivingRoomDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 现房/期房标识 STD_ZB_PERIOD_HOUSE **/
	private String readyOrPeriodHouse;
	
	/** 预购商品房预告登记证明号 **/
	private String purchseHouseNo;
	
	/** 预购商品房抵押权预告登记证明号 **/
	private String purchseHouseRegNo;
	
	/** 预售许可证编号 **/
	private String presellPermitNo;
	
	/** 预售许可证有效期 **/
	private String presellPermitValDate;
	
	/** 预计房屋交付时间 **/
	private String pGetDate;
	
	/** 预计预抵押凭证取得时间 **/
	private String pCreditGetDate;
	
	/** 预计产权证办理时间 **/
	private String pDoDate;
	
	/** 预计他项权证取得时间 **/
	private String pOtherDate;
	
	/** 一手/二手标识 STD_ZB_IS_USED **/
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	private String twocard2oneInd;
	
	/** 产权证号 **/
	private String houseLandNo;
	
	/** 销售许可证编号 **/
	private String marketPermitNo;
	
	/** 该产证是否全部抵押  STD_ZB_YES_NO **/
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	private String businessHouseNo;
	
	/** 合同签订日期 **/
	private String purchaseDate;
	
	/** 购买价格（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 销售备案  STD_ZB_YES_NO **/
	private String saleBak;
	
	/** 是否本次申请所购房产  STD_ZB_YES_NO **/
	private String applyForHouse;
	
	/** 抵押住房是否权属人唯一住所 STD_ZB_YES_NO **/
	private String houseOwnershipInd;
	
	/** 建筑面积 **/
	private Double buildArea;
	
	/** 建成年份 **/
	private String activateYears;
	
	/** 房屋产权期限信息（年） **/
	private String housePrDesc;
	
	/** 楼龄 **/
	private String floorAge;
	
	/** 套型 STD_ZB_F_STY **/
	private String fSty;
	
	/** 朝向 STD_ZB_ORIENTATIONS **/
	private String orientations;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUCTURE **/
	private String houseStructure;
	
	/** 地面构造 STD_ZB_GROUND_STRUCTURE **/
	private String groundStructure;
	
	/** 屋顶构造 STD_ZB_GROUND_STRUCTURE **/
	private String roofStructure;
	
	/** 房屋状态 STD_ZB_HOUSE_STA **/
	private String houseSta;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 门牌号/弄号 **/
	private String houseNo;
	
	/** 楼号 **/
	private String buildingRoomNum;
	
	/** 室号 **/
	private String roomNum;
	
	/** 产权地址 **/
	private String pocAddr;
	
	/** 楼盘（社区）名称 **/
	private String communityName;
	
	/** 层次（标的楼层） **/
	private String bdlc;
	
	/** 层数（标的楼高） **/
	private String bdgd;
	
	/** 房地产所在地段情况   STD_ZB_HOUSE_PLACE_INFO **/
	private String housePlaceInfo;
	
	/** 周边环境 STD_ZB_ARR_ENV **/
	private String arrEnv;
	
	/** 建筑物说明 **/
	private String buildDesc;
	
	/** 土地证号 **/
	private String landNo;
	
	/** 土地使用权性质  STD_ZB_LAND_USE_QUAL **/
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_USE_WAY **/
	private String landUseWay;
	
	/** 土地使用权面积 **/
	private String landUseArea;
	
	/** 土地使用权单位 **/
	private String landUseUnit;
	
	/** 土地使用权使用年限起始日期 **/
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LAND_PURP **/
	private String landPurp;
	
	/** 土地说明 **/
	private String landExplain;
	
	/** 竣工日期 **/
	private String completeDate;
	
	/** 公共配套 STD_ZB_PUBLIC_FACILITIES **/
	private String publicFacilities;
	
	/** 楼层情况 **/
	private String floor;
	
	/** 装修状况 STD_ZB_DECORATION **/
	private String decoration;
	
	/** 通风采光 STD_ZB_AND_LIGHTING **/
	private String ventilationAndLighting;
	
	/** 临街状况 STD_ZB_STREET_SITUATION **/
	private String streetSituation;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	private String houseUseType;
	
	/** 承租人名称 **/
	private String lesseeName;
	
	/** 市场租金 **/
	private java.math.BigDecimal marketRent;
	
	/** 租赁合同生效日 **/
	private String leaseConEftDt;
	
	/** 租赁合同到期日 **/
	private String leaseConEndDt;
	
	/** 年租金（元） **/
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	private String lease;
	
	/** 剩余租期（月） **/
	private String leftLease;
	
	/** 修改次数 **/
	private Integer modifyNum;
	
	/** 房产取得方式 STD_ZB_ESTATE_ACQUIRE_WAY **/
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	private String inUseYear;
	
	/** 居住用房类型 STD_ZB_HOUSE_TYPE **/
	private String houseType;
	
	/** 租赁形式 STD_ZB_LEASE_TYPE **/
	private String leaseType;
	
	/** 开发商名称 **/
	private String developersName;
	
	/** 预计不动产权证取得日期 **/
	private String fetchCertiDate;
	
	/** 发证日期 **/
	private String issueCertiDate;
	
	/** 分摊面积 **/
	private String apportArea;
	
	/** 平面布局 **/
	private String planeLayout;
	
	/** 物业情况 **/
	private String propertyCase;
	
	/** 房产使用情况 **/
	private String houseProperty;
	
	/** 是否有独立产权证 STD_ZB_YES_NO **/
	private String propertyPermitsInd;
	
	/** 车库/车位类型 STD_ZB_CARPORT_TYPE **/
	private String carportType;
	
	/** 车库具体位置 **/
	private String detailAdd;
	
	/** 所属地段 **/
	private String belongArea;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse == null ? null : readyOrPeriodHouse.trim();
	}
	
    /**
     * @return ReadyOrPeriodHouse
     */	
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param purchseHouseNo
	 */
	public void setPurchseHouseNo(String purchseHouseNo) {
		this.purchseHouseNo = purchseHouseNo == null ? null : purchseHouseNo.trim();
	}
	
    /**
     * @return PurchseHouseNo
     */	
	public String getPurchseHouseNo() {
		return this.purchseHouseNo;
	}
	
	/**
	 * @param purchseHouseRegNo
	 */
	public void setPurchseHouseRegNo(String purchseHouseRegNo) {
		this.purchseHouseRegNo = purchseHouseRegNo == null ? null : purchseHouseRegNo.trim();
	}
	
    /**
     * @return PurchseHouseRegNo
     */	
	public String getPurchseHouseRegNo() {
		return this.purchseHouseRegNo;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo == null ? null : presellPermitNo.trim();
	}
	
    /**
     * @return PresellPermitNo
     */	
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param presellPermitValDate
	 */
	public void setPresellPermitValDate(String presellPermitValDate) {
		this.presellPermitValDate = presellPermitValDate == null ? null : presellPermitValDate.trim();
	}
	
    /**
     * @return PresellPermitValDate
     */	
	public String getPresellPermitValDate() {
		return this.presellPermitValDate;
	}
	
	/**
	 * @param pGetDate
	 */
	public void setPGetDate(String pGetDate) {
		this.pGetDate = pGetDate == null ? null : pGetDate.trim();
	}
	
    /**
     * @return PGetDate
     */	
	public String getPGetDate() {
		return this.pGetDate;
	}
	
	/**
	 * @param pCreditGetDate
	 */
	public void setPCreditGetDate(String pCreditGetDate) {
		this.pCreditGetDate = pCreditGetDate == null ? null : pCreditGetDate.trim();
	}
	
    /**
     * @return PCreditGetDate
     */	
	public String getPCreditGetDate() {
		return this.pCreditGetDate;
	}
	
	/**
	 * @param pDoDate
	 */
	public void setPDoDate(String pDoDate) {
		this.pDoDate = pDoDate == null ? null : pDoDate.trim();
	}
	
    /**
     * @return PDoDate
     */	
	public String getPDoDate() {
		return this.pDoDate;
	}
	
	/**
	 * @param pOtherDate
	 */
	public void setPOtherDate(String pOtherDate) {
		this.pOtherDate = pOtherDate == null ? null : pOtherDate.trim();
	}
	
    /**
     * @return POtherDate
     */	
	public String getPOtherDate() {
		return this.pOtherDate;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed == null ? null : isUsed.trim();
	}
	
    /**
     * @return IsUsed
     */	
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd == null ? null : twocard2oneInd.trim();
	}
	
    /**
     * @return Twocard2oneInd
     */	
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo == null ? null : houseLandNo.trim();
	}
	
    /**
     * @return HouseLandNo
     */	
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param marketPermitNo
	 */
	public void setMarketPermitNo(String marketPermitNo) {
		this.marketPermitNo = marketPermitNo == null ? null : marketPermitNo.trim();
	}
	
    /**
     * @return MarketPermitNo
     */	
	public String getMarketPermitNo() {
		return this.marketPermitNo;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd == null ? null : houseAllPledgeInd.trim();
	}
	
    /**
     * @return HouseAllPledgeInd
     */	
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc == null ? null : partRegPositionDesc.trim();
	}
	
    /**
     * @return PartRegPositionDesc
     */	
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo == null ? null : businessHouseNo.trim();
	}
	
    /**
     * @return BusinessHouseNo
     */	
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param saleBak
	 */
	public void setSaleBak(String saleBak) {
		this.saleBak = saleBak == null ? null : saleBak.trim();
	}
	
    /**
     * @return SaleBak
     */	
	public String getSaleBak() {
		return this.saleBak;
	}
	
	/**
	 * @param applyForHouse
	 */
	public void setApplyForHouse(String applyForHouse) {
		this.applyForHouse = applyForHouse == null ? null : applyForHouse.trim();
	}
	
    /**
     * @return ApplyForHouse
     */	
	public String getApplyForHouse() {
		return this.applyForHouse;
	}
	
	/**
	 * @param houseOwnershipInd
	 */
	public void setHouseOwnershipInd(String houseOwnershipInd) {
		this.houseOwnershipInd = houseOwnershipInd == null ? null : houseOwnershipInd.trim();
	}
	
    /**
     * @return HouseOwnershipInd
     */	
	public String getHouseOwnershipInd() {
		return this.houseOwnershipInd;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(Double buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return BuildArea
     */	
	public Double getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears == null ? null : activateYears.trim();
	}
	
    /**
     * @return ActivateYears
     */	
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param housePrDesc
	 */
	public void setHousePrDesc(String housePrDesc) {
		this.housePrDesc = housePrDesc == null ? null : housePrDesc.trim();
	}
	
    /**
     * @return HousePrDesc
     */	
	public String getHousePrDesc() {
		return this.housePrDesc;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge == null ? null : floorAge.trim();
	}
	
    /**
     * @return FloorAge
     */	
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param fSty
	 */
	public void setFSty(String fSty) {
		this.fSty = fSty == null ? null : fSty.trim();
	}
	
    /**
     * @return FSty
     */	
	public String getFSty() {
		return this.fSty;
	}
	
	/**
	 * @param orientations
	 */
	public void setOrientations(String orientations) {
		this.orientations = orientations == null ? null : orientations.trim();
	}
	
    /**
     * @return Orientations
     */	
	public String getOrientations() {
		return this.orientations;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure == null ? null : houseStructure.trim();
	}
	
    /**
     * @return HouseStructure
     */	
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param groundStructure
	 */
	public void setGroundStructure(String groundStructure) {
		this.groundStructure = groundStructure == null ? null : groundStructure.trim();
	}
	
    /**
     * @return GroundStructure
     */	
	public String getGroundStructure() {
		return this.groundStructure;
	}
	
	/**
	 * @param roofStructure
	 */
	public void setRoofStructure(String roofStructure) {
		this.roofStructure = roofStructure == null ? null : roofStructure.trim();
	}
	
    /**
     * @return RoofStructure
     */	
	public String getRoofStructure() {
		return this.roofStructure;
	}
	
	/**
	 * @param houseSta
	 */
	public void setHouseSta(String houseSta) {
		this.houseSta = houseSta == null ? null : houseSta.trim();
	}
	
    /**
     * @return HouseSta
     */	
	public String getHouseSta() {
		return this.houseSta;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo == null ? null : houseNo.trim();
	}
	
    /**
     * @return HouseNo
     */	
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum == null ? null : buildingRoomNum.trim();
	}
	
    /**
     * @return BuildingRoomNum
     */	
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum == null ? null : roomNum.trim();
	}
	
    /**
     * @return RoomNum
     */	
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr == null ? null : pocAddr.trim();
	}
	
    /**
     * @return PocAddr
     */	
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName == null ? null : communityName.trim();
	}
	
    /**
     * @return CommunityName
     */	
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc == null ? null : bdlc.trim();
	}
	
    /**
     * @return Bdlc
     */	
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd == null ? null : bdgd.trim();
	}
	
    /**
     * @return Bdgd
     */	
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo == null ? null : housePlaceInfo.trim();
	}
	
    /**
     * @return HousePlaceInfo
     */	
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param arrEnv
	 */
	public void setArrEnv(String arrEnv) {
		this.arrEnv = arrEnv == null ? null : arrEnv.trim();
	}
	
    /**
     * @return ArrEnv
     */	
	public String getArrEnv() {
		return this.arrEnv;
	}
	
	/**
	 * @param buildDesc
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc == null ? null : buildDesc.trim();
	}
	
    /**
     * @return BuildDesc
     */	
	public String getBuildDesc() {
		return this.buildDesc;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo == null ? null : landNo.trim();
	}
	
    /**
     * @return LandNo
     */	
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual == null ? null : landUseQual.trim();
	}
	
    /**
     * @return LandUseQual
     */	
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea == null ? null : landUseArea.trim();
	}
	
    /**
     * @return LandUseArea
     */	
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setRightUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit == null ? null : landUseUnit.trim();
	}
	
    /**
     * @return RightUseUnit
     */	
	public String getRightUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate == null ? null : landUseBeginDate.trim();
	}
	
    /**
     * @return LandUseBeginDate
     */	
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate == null ? null : landUseEndDate.trim();
	}
	
    /**
     * @return LandUseEndDate
     */	
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears == null ? null : landUseYears.trim();
	}
	
    /**
     * @return LandUseYears
     */	
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp == null ? null : landPurp.trim();
	}
	
    /**
     * @return LandPurp
     */	
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain == null ? null : landExplain.trim();
	}
	
    /**
     * @return LandExplain
     */	
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param completeDate
	 */
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate == null ? null : completeDate.trim();
	}
	
    /**
     * @return CompleteDate
     */	
	public String getCompleteDate() {
		return this.completeDate;
	}
	
	/**
	 * @param publicFacilities
	 */
	public void setPublicFacilities(String publicFacilities) {
		this.publicFacilities = publicFacilities == null ? null : publicFacilities.trim();
	}
	
    /**
     * @return PublicFacilities
     */	
	public String getPublicFacilities() {
		return this.publicFacilities;
	}
	
	/**
	 * @param floor
	 */
	public void setFloor(String floor) {
		this.floor = floor == null ? null : floor.trim();
	}
	
    /**
     * @return Floor
     */	
	public String getFloor() {
		return this.floor;
	}
	
	/**
	 * @param decoration
	 */
	public void setDecoration(String decoration) {
		this.decoration = decoration == null ? null : decoration.trim();
	}
	
    /**
     * @return Decoration
     */	
	public String getDecoration() {
		return this.decoration;
	}
	
	/**
	 * @param ventilationAndLighting
	 */
	public void setVentilationAndLighting(String ventilationAndLighting) {
		this.ventilationAndLighting = ventilationAndLighting == null ? null : ventilationAndLighting.trim();
	}
	
    /**
     * @return VentilationAndLighting
     */	
	public String getVentilationAndLighting() {
		return this.ventilationAndLighting;
	}
	
	/**
	 * @param streetSituation
	 */
	public void setStreetSituation(String streetSituation) {
		this.streetSituation = streetSituation == null ? null : streetSituation.trim();
	}
	
    /**
     * @return StreetSituation
     */	
	public String getStreetSituation() {
		return this.streetSituation;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType == null ? null : houseUseType.trim();
	}
	
    /**
     * @return HouseUseType
     */	
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName == null ? null : lesseeName.trim();
	}
	
    /**
     * @return LesseeName
     */	
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param marketRent
	 */
	public void setMarketRent(java.math.BigDecimal marketRent) {
		this.marketRent = marketRent;
	}
	
    /**
     * @return MarketRent
     */	
	public java.math.BigDecimal getMarketRent() {
		return this.marketRent;
	}
	
	/**
	 * @param leaseConEftDt
	 */
	public void setLeaseConEftDt(String leaseConEftDt) {
		this.leaseConEftDt = leaseConEftDt == null ? null : leaseConEftDt.trim();
	}
	
    /**
     * @return LeaseConEftDt
     */	
	public String getLeaseConEftDt() {
		return this.leaseConEftDt;
	}
	
	/**
	 * @param leaseConEndDt
	 */
	public void setLeaseConEndDt(String leaseConEndDt) {
		this.leaseConEndDt = leaseConEndDt == null ? null : leaseConEndDt.trim();
	}
	
    /**
     * @return LeaseConEndDt
     */	
	public String getLeaseConEndDt() {
		return this.leaseConEndDt;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return AnnualRent
     */	
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease == null ? null : lease.trim();
	}
	
    /**
     * @return Lease
     */	
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease == null ? null : leftLease.trim();
	}
	
    /**
     * @return LeftLease
     */	
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param modifyNum
	 */
	public void setModifyNum(Integer modifyNum) {
		this.modifyNum = modifyNum;
	}
	
    /**
     * @return ModifyNum
     */	
	public Integer getModifyNum() {
		return this.modifyNum;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay == null ? null : estateAcquireWay.trim();
	}
	
    /**
     * @return EstateAcquireWay
     */	
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear == null ? null : inUseYear.trim();
	}
	
    /**
     * @return InUseYear
     */	
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param leaseType
	 */
	public void setLeaseType(String leaseType) {
		this.leaseType = leaseType == null ? null : leaseType.trim();
	}
	
    /**
     * @return LeaseType
     */	
	public String getLeaseType() {
		return this.leaseType;
	}
	
	/**
	 * @param developersName
	 */
	public void setDevelopersName(String developersName) {
		this.developersName = developersName == null ? null : developersName.trim();
	}
	
    /**
     * @return DevelopersName
     */	
	public String getDevelopersName() {
		return this.developersName;
	}
	
	/**
	 * @param fetchCertiDate
	 */
	public void setFetchCertiDate(String fetchCertiDate) {
		this.fetchCertiDate = fetchCertiDate == null ? null : fetchCertiDate.trim();
	}
	
    /**
     * @return FetchCertiDate
     */	
	public String getFetchCertiDate() {
		return this.fetchCertiDate;
	}
	
	/**
	 * @param issueCertiDate
	 */
	public void setIssueCertiDate(String issueCertiDate) {
		this.issueCertiDate = issueCertiDate == null ? null : issueCertiDate.trim();
	}
	
    /**
     * @return IssueCertiDate
     */	
	public String getIssueCertiDate() {
		return this.issueCertiDate;
	}
	
	/**
	 * @param apportArea
	 */
	public void setApportArea(String apportArea) {
		this.apportArea = apportArea == null ? null : apportArea.trim();
	}
	
    /**
     * @return ApportArea
     */	
	public String getApportArea() {
		return this.apportArea;
	}
	
	/**
	 * @param planeLayout
	 */
	public void setPlaneLayout(String planeLayout) {
		this.planeLayout = planeLayout == null ? null : planeLayout.trim();
	}
	
    /**
     * @return PlaneLayout
     */	
	public String getPlaneLayout() {
		return this.planeLayout;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase == null ? null : propertyCase.trim();
	}
	
    /**
     * @return PropertyCase
     */	
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty == null ? null : houseProperty.trim();
	}
	
    /**
     * @return HouseProperty
     */	
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param propertyPermitsInd
	 */
	public void setPropertyPermitsInd(String propertyPermitsInd) {
		this.propertyPermitsInd = propertyPermitsInd == null ? null : propertyPermitsInd.trim();
	}
	
    /**
     * @return PropertyPermitsInd
     */	
	public String getPropertyPermitsInd() {
		return this.propertyPermitsInd;
	}
	
	/**
	 * @param carportType
	 */
	public void setCarportType(String carportType) {
		this.carportType = carportType == null ? null : carportType.trim();
	}
	
    /**
     * @return CarportType
     */	
	public String getCarportType() {
		return this.carportType;
	}
	
	/**
	 * @param detailAdd
	 */
	public void setDetailAdd(String detailAdd) {
		this.detailAdd = detailAdd == null ? null : detailAdd.trim();
	}
	
    /**
     * @return DetailAdd
     */	
	public String getDetailAdd() {
		return this.detailAdd;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea == null ? null : belongArea.trim();
	}
	
    /**
     * @return BelongArea
     */	
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}