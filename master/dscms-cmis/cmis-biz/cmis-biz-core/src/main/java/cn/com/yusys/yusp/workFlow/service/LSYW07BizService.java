package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyChg;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyChgMapper;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.LmtCrdReplyChgService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @param
 * @return
 * @author wzy
 * @date 2021-6-29 21:7:27
 * @version 1.0.0
 * @desc 零售批复变更
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class LSYW07BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(LSYW07BizService.class);
    @Autowired
    private LmtCrdReplyChgService lmtCrdReplyChgService;
    @Autowired
    private LmtCrdReplyChgMapper lmtCrdReplyChgMapper;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + resultInstanceDto);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if("LS009".equals(bizType)){
            handlerLS009(resultInstanceDto);
        }
        log.info("进入业务类型【{}】流程处理逻辑结束！", resultInstanceDto);
    }


    //零售批复变更
    private void handlerLS009(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String replyNo = resultInstanceDto.getBizId();
        String pkId = (String) resultInstanceDto.getParam().get("pkId");
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                lmtCrdReplyChgService.handleBizEnd(pkId,replyNo);
                updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_997);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(replyNo,pkId, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.LSYW07.equals(flowCode);
    }

    /**
     * @创建人 wzy
     * @创建时间 2021-5-10 15:08:32
     * @注释 更新批复状态为审批中
     */
    public void updateStatus(String replyNo,String pkId,String status) {
        Map map = new HashMap();
        map.put("replyNo",replyNo);
        LmtCrdReplyChg lmtCrdReplyChg = lmtCrdReplyChgMapper.selectNew(map);
        if("000".equals(lmtCrdReplyChg.getApproveStatus())){
            lmtCrdReplyChg.setApproveStatus(status);
            lmtCrdReplyChgService.updateSelective(lmtCrdReplyChg);
        }
    }

}
