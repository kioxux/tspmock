/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperDevelopRealEstate;
import cn.com.yusys.yusp.service.RptOperDevelopRealEstateService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperDevelopRealEstateResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 19:38:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperdeveloprealestate")
public class RptOperDevelopRealEstateResource {
    @Autowired
    private RptOperDevelopRealEstateService rptOperDevelopRealEstateService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperDevelopRealEstate>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperDevelopRealEstate> list = rptOperDevelopRealEstateService.selectAll(queryModel);
        return new ResultDto<List<RptOperDevelopRealEstate>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperDevelopRealEstate>> index(QueryModel queryModel) {
        List<RptOperDevelopRealEstate> list = rptOperDevelopRealEstateService.selectByModel(queryModel);
        return new ResultDto<List<RptOperDevelopRealEstate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperDevelopRealEstate> show(@PathVariable("pkId") String pkId) {
        RptOperDevelopRealEstate rptOperDevelopRealEstate = rptOperDevelopRealEstateService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperDevelopRealEstate>(rptOperDevelopRealEstate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperDevelopRealEstate> create(@RequestBody RptOperDevelopRealEstate rptOperDevelopRealEstate) throws URISyntaxException {
        rptOperDevelopRealEstateService.insert(rptOperDevelopRealEstate);
        return new ResultDto<RptOperDevelopRealEstate>(rptOperDevelopRealEstate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperDevelopRealEstate rptOperDevelopRealEstate) throws URISyntaxException {
        int result = rptOperDevelopRealEstateService.update(rptOperDevelopRealEstate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperDevelopRealEstateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperDevelopRealEstateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperDevelopRealEstate>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptOperDevelopRealEstate>>(rptOperDevelopRealEstateService.selectByModel(model));
    }

    @PostMapping("/deleteDevelop")
    protected ResultDto<Integer> deleteDevelop(@RequestBody RptOperDevelopRealEstate rptOperDevelopRealEstate){
        String pkId = rptOperDevelopRealEstate.getPkId();
        return new ResultDto<Integer>(rptOperDevelopRealEstateService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/updateDevelop")
    protected ResultDto<Integer> updateDevelop(@RequestBody RptOperDevelopRealEstate rptOperDevelopRealEstate){
        return new ResultDto<Integer>(rptOperDevelopRealEstateService.updateSelective(rptOperDevelopRealEstate));
    }

    @PostMapping("/insertDevelop")
    protected ResultDto<Integer> insertDevelop(@RequestBody RptOperDevelopRealEstate rptOperDevelopRealEstate){
        rptOperDevelopRealEstate.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOperDevelopRealEstateService.insertSelective(rptOperDevelopRealEstate));
    }

}
