/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CtrEntrustLoanContShowDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrEntrustLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpEntrustLoanAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.codec.net.QCodec;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrEntrustLoanContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-15 14:31:51
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrEntrustLoanContService {

    @Autowired
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    private static final Logger log = LoggerFactory.getLogger(CtrEntrustLoanContService.class);

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private IqpEntrustLoanAppMapper iqpEntrustLoanAppMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrEntrustLoanCont selectByPrimaryKey(String pkId) {
        return ctrEntrustLoanContMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CtrEntrustLoanCont> selectAll(QueryModel model) {
        List<CtrEntrustLoanCont> records = (List<CtrEntrustLoanCont>) ctrEntrustLoanContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrEntrustLoanCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrEntrustLoanCont> list = ctrEntrustLoanContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CtrEntrustLoanCont record) {
        return ctrEntrustLoanContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insertSelective(CtrEntrustLoanCont record) {
        return ctrEntrustLoanContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CtrEntrustLoanCont record) {
        return ctrEntrustLoanContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(CtrEntrustLoanCont record) {
        return ctrEntrustLoanContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 根据合同号更新lmtAccNo
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateLmtAccNoByContNo(CtrEntrustLoanCont record) {
        return ctrEntrustLoanContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return ctrEntrustLoanContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrEntrustLoanContMapper.deleteByIds(ids);
    }

    /**
     * 合同注销保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateLogout(@RequestBody Map params) throws ParseException {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            String logPrefix = "合同注销" + serno;

            log.info(logPrefix + "获取申请数据");
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContMapper.selectByEntrustLoanSernoKey(serno);
            if (ctrEntrustLoanCont == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            // 先判断合同是否存在未结清的业务
            String responseResult = ctrLoanContService.isContUncleared(ctrEntrustLoanCont.getContNo());
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                rtnCode = EcbEnum.ECB020055.key;
                rtnMsg = EcbEnum.ECB020055.value;
                return result;
            }
            // 获取合同注销后的合同状态
            String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrEntrustLoanCont.getEndDate(),ctrEntrustLoanCont.getContStatus());
            log.info(logPrefix + "合同注销-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                ctrEntrustLoanCont.setInputId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setInputBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setManagerId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setManagerBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ctrEntrustLoanCont.setContStatus(finalContStatus);
            }

            log.info(logPrefix + "保存委托贷款合同数据");
            int updCount = ctrEntrustLoanContMapper.updateByPrimaryKeySelective(ctrEntrustLoanCont);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

            //注销合同，恢复占额
            String guarMode = ctrEntrustLoanCont.getGuarMode();
            //恢复敞口
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            //恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
                recoverSpacAmtCny = BigDecimal.ZERO;
                recoverAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
            }else{
                recoverSpacAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
                recoverAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
            }
            // 根据合是否存在业务，得到恢复类型
            String recoverType = "";
            List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByContNo(ctrEntrustLoanCont.getContNo());
            if(CollectionUtils.nonEmpty(accEntrustLoanList)){
                for (AccEntrustLoan accEntrustLoan : accEntrustLoanList) {
                    if(CmisCommonConstants.ACC_STATUS_7.equals(accEntrustLoan.getAccStatus())){
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                    }else{
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                        break;
                    }
                }
            }else {
                //未用注销
                recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
            }
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrEntrustLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrEntrustLoanCont.getContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
            cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
            cmisLmt0012ReqDto.setInputId(ctrEntrustLoanCont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrEntrustLoanCont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ResultDto<CmisLmt0012RespDto> resultDto=  cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            String code = resultDto.getData().getErrorCode();
            if(!"0".equals(resultDto.getCode())){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(code)){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,code, resultDto.getData().getErrorMsg());
            }
        } catch (Exception e) {
            log.error("保存委托贷款合同" + serno + "异常", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }

        return result;
    }

    /**
     * 合同注销保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateLogoutHis(@RequestBody Map params) throws ParseException {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        int pvpEntrustCount = 0;
        int accEntrustCount = 0;
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            String logPrefix = "合同签订" + serno;

            log.info(logPrefix + "获取申请数据");
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContMapper.selectByEntrustLoanSernoKey(serno);
            if (ctrEntrustLoanCont == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            // 先判断合同是否存在未结清的业务
            String responseResult = ctrLoanContService.isContUncleared(ctrEntrustLoanCont.getContNo());
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                rtnCode = EcbEnum.ECB020055.key;
                rtnMsg = EcbEnum.ECB020055.value;
                return result;
            }
            // 获取合同注销后的合同状态
            String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrEntrustLoanCont.getEndDate(),ctrEntrustLoanCont.getContStatus());
            log.info(logPrefix + "合同注销-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                ctrEntrustLoanCont.setInputId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setInputBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setManagerId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setManagerBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ctrEntrustLoanCont.setContStatus(finalContStatus);
            }

            log.info(logPrefix + "保存委托贷款合同数据");
            int updCount = ctrEntrustLoanContMapper.updateByPrimaryKeySelective(ctrEntrustLoanCont);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

            //合同注销后，恢复占额
            String guarMode = ctrEntrustLoanCont.getGuarMode();
            //恢复敞口
            BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
            //恢复总额
            BigDecimal recoverAmtCny = BigDecimal.ZERO;
            if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
                recoverSpacAmtCny = BigDecimal.ZERO;
                recoverAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
            }else{
                recoverSpacAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
                recoverAmtCny = ctrEntrustLoanCont.getContHighAvlAmt();
            }

            // 根据合是否存在业务，得到恢复类型
            String recoverType = "";
            List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByContNo(ctrEntrustLoanCont.getContNo());
            if(CollectionUtils.nonEmpty(accEntrustLoanList)){
                //结清注销
                for (AccEntrustLoan accEntrustLoan : accEntrustLoanList) {
                    if(CmisCommonConstants.ACC_STATUS_0.equals(accEntrustLoan.getAccStatus())||
                            CmisCommonConstants.ACC_STATUS_0.equals(accEntrustLoan.getAccStatus())){
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                    }else{
                        recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                        break;
                    }
                }
            }else {
                //未用注销
                recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
            }

            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrEntrustLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrEntrustLoanCont.getContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
            cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
            cmisLmt0012ReqDto.setInputId(ctrEntrustLoanCont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrEntrustLoanCont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            ResultDto<CmisLmt0012RespDto> resultDto=  cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            String code = resultDto.getData().getErrorCode();
            if(!"0".equals(resultDto.getCode())){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(code)){
                log.error("业务申请恢复额度异常！");
                throw BizException.error(null,code, resultDto.getData().getErrorMsg());
            }
        } catch (Exception e) {
            log.error("保存委托贷款额度申请" + serno + "异常", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }

        return result;
    }

    /**
     * 合同签订保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map updateSign(@RequestBody Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            String logPrefix = "合同签订" + serno;

            log.info(logPrefix + "获取申请数据");
            CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContMapper.selectByEntrustLoanSernoKey(serno);
            if (ctrEntrustLoanCont == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            QueryModel  queryModel = new QueryModel();
            queryModel.getCondition().put("serno",ctrEntrustLoanCont.getSerno());
            List<GrtGuarCont> list = grtGuarContService.selectGuarContByContNo(queryModel);
            //担保合同签订生效
            for(int i=0;i<list.size();i++){
                GrtGuarCont grtGuarCont = list.get(i);
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);
                HashMap map =  new HashMap();
                map.put("bizType","02");
                map.put("isFlag","1");
                map.put("bizSerno",ctrEntrustLoanCont.getSerno());
                map.put("guarContNo",grtGuarCont.getGuarContNo());
                guarBaseInfoService.buscon(map);
            }

            log.info(logPrefix + "合同签订-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                ctrEntrustLoanCont.setInputId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setInputBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setManagerId(userInfo.getLoginCode());
                ctrEntrustLoanCont.setManagerBrId(userInfo.getOrg().getCode());
                ctrEntrustLoanCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ctrEntrustLoanCont.setContStatus((String) params.get("contStatus"));
                ctrEntrustLoanCont.setPaperContSignDate((String)params.get("paperContSignDate"));
            }

            log.info(logPrefix + "保存委托贷款合同数据");
            int updCount = ctrEntrustLoanContMapper.updateByPrimaryKeySelective(ctrEntrustLoanCont);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

            // 判断是否为合同续签,若为续签合同则恢复原合同的占额
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrEntrustLoanCont.getIsRenew())){
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrEntrustLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(ctrEntrustLoanCont.getOrigiContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
                cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
                cmisLmt0012ReqDto.setInputId(ctrEntrustLoanCont.getInputId());//登记人
                cmisLmt0012ReqDto.setInputBrId(ctrEntrustLoanCont.getInputBrId());//登记机构
                cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                log.info("委托贷款合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrEntrustLoanCont.getContNo(), ctrEntrustLoanCont.getOrigiContNo(), JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("委托贷款合同【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrEntrustLoanCont.getContNo(), ctrEntrustLoanCont.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("委托贷款合同【{}】签订异常",ctrEntrustLoanCont.getContNo());
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                    rtnCode = code;
                    rtnMsg = resultDtoDto.getData().getErrorCode();
                    throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                }
            }
            guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_09, ctrEntrustLoanCont.getContNo());

            //合同申请选择续签时，新签订成功后，原合同作废
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrEntrustLoanCont.getIsRenew())){
                CtrEntrustLoanCont ctrEntrustLoanContData = this.selectDataByContNo(ctrEntrustLoanCont.getOrigiContNo());
                ctrEntrustLoanContData.setContStatus(CmisCommonConstants.CONT_STATUS_800);
                ctrEntrustLoanContMapper.updateByPrimaryKeySelective(ctrEntrustLoanContData);
            }
            log.info("原合同：" +ctrEntrustLoanCont.getOrigiContNo()+"作废成功");
        } catch (Exception e) {
            log.error("保存委托贷款额度申请" + serno + "异常", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }

        return result;
    }

    /**
     * 获取基本信息
     *
     * @param Serno
     * @return
     */
    public CtrEntrustLoanCont selectByCtrEntrustLoanSernoKey(String Serno) {
        return ctrEntrustLoanContMapper.selectByEntrustLoanSernoKey(Serno);
    }

    /**
     * @return
     * @方法名称: selectCtrCont
     * @方法描述: 通过查询合同选择列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrEntrustLoanContShowDto> selectCtrCont(QueryModel queryModel) {
        List<CtrEntrustLoanContShowDto> ctrList = ctrEntrustLoanContMapper.selectCtrContEntrustByParams(queryModel);
        return ctrList;
    }

    /**
     * 获取基本信息
     * zxz
     * @param queryMap
     * @return
     */
    public CtrEntrustLoanCont queryCtrEntrustLoanByDataParams(HashMap<String, String> queryMap) {
        return ctrEntrustLoanContMapper.queryCtrEntrustLoanByDataParams(queryMap);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrEntrustLoanCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrEntrustLoanContMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:14
     * @修改记录：修改时间 修改人员 修改时间
    */
    public CtrEntrustLoanCont selectByIqpSerno(String serno){
        return ctrEntrustLoanContMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:14
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrEntrustLoanCont selectByContNo(String contNo){
        return ctrEntrustLoanContMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrEntrustLoanCont> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        return ctrEntrustLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrEntrustLoanCont> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("contStatusOther",CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrEntrustLoanContMapper.selectByModel(model);
    }

    public List<CtrEntrustLoanCont> toSignlistForZhcx(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        return ctrEntrustLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectDataByContNo
     * @方法描述: 根据合同号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrEntrustLoanCont selectDataByContNo(String contNo) {
        return ctrEntrustLoanContMapper.selectDataByContNo(contNo);
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrEntrustLoanCont> selectByQuerymodel(QueryModel model) {
        return ctrEntrustLoanContMapper.selectByModel(model);
    }
}
