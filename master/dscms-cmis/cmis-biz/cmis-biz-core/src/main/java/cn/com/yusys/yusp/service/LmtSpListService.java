/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtFinSpApp;
import cn.com.yusys.yusp.domain.LmtSpList;
import cn.com.yusys.yusp.repository.mapper.LmtSpListMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSpListService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 15:27:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSpListService {

    @Autowired
    private LmtSpListMapper lmtSpListMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSpList selectByPrimaryKey(String pkId) {
        return lmtSpListMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSpList> selectAll(QueryModel model) {
        List<LmtSpList> records = (List<LmtSpList>) lmtSpListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSpList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSpList> list = lmtSpListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSpList record) {
        return lmtSpListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSpList record) {
        return lmtSpListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSpList record) {
        return lmtSpListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSpList record) {
        return lmtSpListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSpListMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSpListMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateOprTypeBySerno
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateOprTypeBySerno(LmtFinSpApp lmtFinSpApp) {
        return lmtSpListMapper.updateOprTypeBySerno(lmtFinSpApp);
    }

    /**
     * @函数名称:reCalCurtCurSubpayQntBySerno
     * @函数描述:重新计算代偿笔数
     * @参数与返回说明:
     * @算法描述:
     */

    public int calCurtCurSubpayQntBySerno(String serno) {
        return lmtSpListMapper.calCurtCurSubpayQntBySerno(serno);
    }
}
