package cn.com.yusys.yusp.service.server.xdht0040;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0040.req.Xdht0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0040.resp.Xdht0040DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpChgTrupayAcctAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0040Service
 * @类描述: #服务类
 * @功能描述:客户一直做受托，渠道端去信贷查询受托信息是否存在
 * 1、信贷更新受托支付账号修改表change_trustee_pay(审批状态更新成通过)
 * 2、将受托变更信息存入票据池受托支付账号变化记录表trustee_pay_pjc_his
 * @创建人: xll
 * @创建时间: 2021-05-03 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0040Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0040Service.class);

    @Resource
    private IqpChgTrupayAcctAppMapper iqpChgTrupayAcctAppMapper;

    /**
     * XDHT0040 修改受托信息
     *
     * @param xdht0040DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0040DataRespDto updateIqpChgTrupayAcctAppBySerno(Xdht0040DataReqDto xdht0040DataReqDto) throws Exception{
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value);
        Xdht0040DataRespDto xdht0040DataRespDto = new Xdht0040DataRespDto();
        int result = 0;
        try {
            //修改受托支付信息表
            result = iqpChgTrupayAcctAppMapper.updateIqpChgTrupayAcctAppBySerno(xdht0040DataReqDto);
            if (result > 0) {//修改受托信息成功
                xdht0040DataRespDto.setOpFlag(DscmsBizHtEnum.FALG_SUCCESS.key);
                xdht0040DataRespDto.setOpMsg(DscmsBizHtEnum.FALG_SUCCESS.value);
            } else {
                xdht0040DataRespDto.setOpFlag(DscmsBizHtEnum.FLAG_FAILD.key);
                xdht0040DataRespDto.setOpMsg(DscmsBizHtEnum.FLAG_FAILD.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {//修改受托信息异常
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value);
        return xdht0040DataRespDto;
    }
}
