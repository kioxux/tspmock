/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;

import java.text.ParseException;
import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AsplAccpDto;
import cn.com.yusys.yusp.dto.AsplAccpEntrustAccDto;
import cn.com.yusys.yusp.dto.AsplBailAcctDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.service.*;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAsplDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产池协议台账")
@RequestMapping("/api/ctraspldetails")
public class CtrAsplDetailsResource {
    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrAsplDetails> create(@RequestBody CtrAsplDetails ctrAsplDetails) throws URISyntaxException {
        ctrAsplDetailsService.insert(ctrAsplDetails);
        return new ResultDto<CtrAsplDetails>(ctrAsplDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrAsplDetails ctrAsplDetails) throws URISyntaxException {
        int result = ctrAsplDetailsService.updateSelective(ctrAsplDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrAsplDetailsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrAsplDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryCtrAsplPopList
     * @函数描述:资产池台账pop列表查询
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryctrasplpoplist")
    protected ResultDto<List<CtrAsplDetails>> queryCtrAsplPopList(@RequestBody QueryModel queryModel) {
        List<CtrAsplDetails> list = ctrAsplDetailsService.selectByModel(queryModel);
        return new ResultDto<List<CtrAsplDetails>>(list);
    }

    /**
     * @函数名称:queryCtrAsplDetailsDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过入参查询")
    @PostMapping("/queryctraspldetailsdatabyparams")
    protected ResultDto<CtrAsplDetails> queryCtrAsplDetailsDataByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("cusId",(String)map.get("cusId"));
        queryData.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        queryData.put("contStatus", CmisCommonConstants.CONT_STATUS_200);
        CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.queryCtrAsplDetailsDataByParams(queryData);
        return new ResultDto<CtrAsplDetails>(ctrAsplDetails);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:资产池协议申请待发起列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产池协议变更列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<CtrAsplDetails>> toSignlist(@RequestBody QueryModel model) throws ParseException {
        List<CtrAsplDetails> list = ctrAsplDetailsService.toSignlist(model);
        return new ResultDto<List<CtrAsplDetails>>(list);
    }

    /**
     * 资产池协议新增保存操作
     * @param ctrAsplDetails
     * @return
     */
    @ApiOperation("资产池协议新增保存")
    @PostMapping("/savectraspldetailsinfo")
    public ResultDto<Map> savectrAsplDetailsInfo(@RequestBody CtrAsplDetails ctrAsplDetails){
        Map result = ctrAsplDetailsService.savectrAsplDetailsInfo(ctrAsplDetails);
        return new ResultDto<>(result);
    }

    /**
     * 资产池协议通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("资产池协议通用的保存方法")
    @PostMapping("/commonsavectraspldetailsinfo")
    public ResultDto<Map> commonSaveCtrAsplDetailsInfo(@RequestBody Map params){
        Map rtnData = ctrAsplDetailsService.commonSaveCtrAsplDetailsInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:queryCtrAsplDetailsDataByParams
     * @函数描述:通过入参查询，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询数据")
    @PostMapping("/queryctraspldetailsdatabyserno")
    protected ResultDto<CtrAsplDetails> queryCtrAsplDetailsDataBySerno(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        queryData.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.queryCtrAsplDetailsDataByParams(queryData);
        return new ResultDto<CtrAsplDetails>(ctrAsplDetails);
    }


    /**
     * 资产池协议启用停用和手动注销
     * @param map
     * @return
     */
    @ApiOperation("资产池协议启用停用和手动注销")
    @PostMapping("/ctraspldetailsstartstoplogout")
    public ResultDto<Map> ctrAsplDetailsStartStopLogout(@RequestBody Map<String,Object> map){
        Map result = ctrAsplDetailsService.ctrAsplDetailsStartStopLogout(map);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称: ctrAspiDetailsAndBailAccInfoList
     * @函数描述: 保证金信息列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("保证金信息列表")
    @PostMapping("/ctraspidetailsandbailaccinfolist")
    protected ResultDto<List<AsplBailAcctDto>> ctrAspiDetailsAndBailAccInfoList(@RequestBody QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplBailAcctDto> list = ctrAsplDetailsService.ctrAspiDetailsAndBailAccInfoList(model);
        PageHelper.clearPage();
        return new ResultDto<List<AsplBailAcctDto>>(list);
    }

    /**
     * @函数名称: queryCtrAspiDetailsAndBailAccInfoByParams
     * @函数描述: 根据流水号得到资产池协议关联的保证金信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据流水号得到资产池协议关联的保证金信息")
    @PostMapping("/queryctraspidetailsandbailaccinfobyparams")
    protected ResultDto<AsplBailAcctDto> queryCtrAspiDetailsAndBailAccInfoByParams(@RequestBody Map map) {
        AsplBailAcctDto asplBailAcctDto = ctrAsplDetailsService.queryCtrAspiDetailsAndBailAccInfoByParams(map);
        return new ResultDto<AsplBailAcctDto>(asplBailAcctDto);
    }

    /**
     * 导出池额度台账列表
     */
    @ApiOperation("导出池额度台账列表")
    @PostMapping("/exportasplacc")
    public ResultDto<ProgressDto> exportAsplAcc(@RequestBody CtrAsplDetails ctrAsplDetails) {
        ProgressDto progressDto = ctrAsplDetailsService.exportAsplAcc(ctrAsplDetails);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称: inPoolAccList
     * @函数描述: 池内业务台账列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("池内业务台账列表")
    @PostMapping("/inpoolacclist")
    protected ResultDto<List<AsplAccpEntrustAccDto>> inPoolAccList(@RequestBody QueryModel queryModel) {
        List<AsplAccpEntrustAccDto> list = ctrAsplDetailsService.inPoolAccList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAccpEntrustAccDto>>(list);
    }
    /**
     * @函数名称: queryAsplAccpDtoInfoByParams
     * @函数描述: 根据入参查询贸易背景资料审核详情
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据入参查询贸易背景资料审核详情")
    @PostMapping("/queryasplaccpdtoinfobyparams")
    protected ResultDto<AsplAccpDto> queryAsplAccpDtoInfoByParams(@RequestBody Map map) {
        AsplAccpDto asplAccpDto = ctrAsplDetailsService.queryAsplAccpDtoInfoByParams(map);
        return new ResultDto<AsplAccpDto>(asplAccpDto);
    }

    /**
     * @函数名称:chCtrAsplDetail
     * @函数描述:根据资产池协议编号查询资产池详情
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @ApiOperation("根据资产池协议编号查询资产池详情")
    @PostMapping("/queryCtrAsplDetail")
    protected ResultDto<CtrAsplDetails> queryCtrAsplDetail(@RequestBody QueryModel queryModel) {
        String contNo = (String)queryModel.getCondition().get("contNo");
        if(Objects.isNull(contNo)){
            return new ResultDto().message("资产池协议编号【contNo】未传参").code("9999");
        }
        CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
        // 获取资产池协议台账信息
        return new ResultDto<CtrAsplDetails>(ctrAsplDetails);
    }
    /**
     * @函数名称:chCtrAsplDetail
     * @函数描述:根据资产池协议查询融资额度
     * @参数与返回说明:
     * @param contNo
     * @算法描述:
     */
    @ApiOperation("根据资产池协议编号查询融资额度")
    @PostMapping("/doComputePoolAmt/{contNo}")
    protected ResultDto<Map<String,Object>> doComputePoolAmt(@PathVariable String contNo) {
        ResultDto<Map<String,Object>> result = ctrAsplDetailsService.doComputePoolAmt(contNo);
        return result;
    }
    /**
     * @函数名称:asplAccLoanList
     * @函数描述:资产池贷款台账
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @ApiOperation("资产池贷款台账")
    @PostMapping("/asplaccloanlist")
    protected ResultDto<List<AccLoan>> asplAccLoanList(@RequestBody QueryModel queryModel) {
        // 是否资产池 1是 0否
        queryModel.addCondition("isPool",CmisCommonConstants.STD_ZB_YES_NO_1);
        List<AccLoan> result =  accLoanService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccLoan>>(result);
    }
    /**
     * @函数名称:asplAccAccpList
     * @函数描述:资产池银承台账
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @ApiOperation("资产池银承台账")
    @PostMapping("/asplaccaccplist")
    protected ResultDto<List<AccAccp>> asplAccAccpList(@RequestBody QueryModel queryModel) {
        // 是否资产池 1是 0否
        queryModel.addCondition("isPool",CmisCommonConstants.STD_ZB_YES_NO_1);
        List<AccAccp> result =  accAccpService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccAccp>>(result);
    }
    /**
     * @函数名称:asplAccLoanList
     * @函数描述:资产池贷款台账
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    @PostMapping(value = "/imageappr")
    public ResultDto<String> sendImage(@RequestBody Map map) {
        ImageApprDto imageApprDto = new ImageApprDto();
        imageApprDto.setDocId((String)map.get("pvpSerno"));
        imageApprDto.setIsApproved("1");
        imageApprDto.setApproval("同意");
        imageApprDto.setOutcode((String)map.get("temCode"));
        imageApprDto.setOpercode((String)map.get("cusId"));
        imageApprDto.setBatchNo((String)map.get("batchNo"));
        return cmisBizXwCommonService.sendImage(imageApprDto);
    }
}
