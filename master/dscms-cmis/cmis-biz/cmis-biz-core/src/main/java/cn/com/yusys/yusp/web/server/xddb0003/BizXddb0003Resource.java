package cn.com.yusys.yusp.web.server.xddb0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0003.req.Xddb0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0003.resp.Xddb0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0003.Xddb0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:押品信息同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0003:押品信息同步")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0003Resource.class);

    @Autowired
    private Xddb0003Service xddb0003Service;

    /**
     * 交易码：xddb0003
     * 交易描述：押品信息同步
     *
     * @param xddb0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0003:押品信息同步")
    @PostMapping("/xddb0003")
    protected @ResponseBody
    ResultDto<Xddb0003DataRespDto> xddb0003(@Validated @RequestBody Xddb0003DataReqDto xddb0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataReqDto));
        Xddb0003DataRespDto xddb0003DataRespDto = new Xddb0003DataRespDto();// 响应Dto:押品信息同步
        ResultDto<Xddb0003DataRespDto> xddb0003DataResultDto = new ResultDto<>();
        // 从xddb0003DataReqDto获取业务值进行业务逻辑处理
        try {
            // 从xddb0003DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataReqDto));
            xddb0003DataRespDto = xddb0003Service.xddb0003(xddb0003DataReqDto);
			logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataRespDto));

            // 封装xddb0003DataResultDto中正确的返回码和返回信息
            xddb0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, e.getMessage());
            // 封装xddb0003DataResultDto中异常返回码和返回信息
            xddb0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0003DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, e.getMessage());
            // 封装xddb0003DataResultDto中异常返回码和返回信息
            xddb0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0003DataRespDto到xddb0003DataResultDto中
        xddb0003DataResultDto.setData(xddb0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataResultDto));
        return xddb0003DataResultDto;
    }
}
