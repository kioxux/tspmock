/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CoopPlanEspecQuotaCtrlDto;
import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CoopReplyAppAllVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopReplyAppService {

    @Autowired
    private CoopReplyAppMapper coopReplyAppMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CoopReplyAppProMapper coopReplyAppProMapper;

    @Autowired
    private CoopReplyAppSubMapper coopReplyAppSubMapper;

    @Autowired
    private CoopPlanAppMapper coopPlanAppMapper;

    @Autowired
    private CoopPlanProInfoMapper coopPlanProInfoMapper;

    @Autowired
    private CoopPlanEspecQuotaCtrlMapper coopPlanEspecQuotaCtrlMapper;

    @Autowired
    private CoopReplyAppCondMapper coopReplyAppCondMapper;

    @Autowired
    private CoopReplyAppPspMapper coopReplyAppPspMapper;

    @Autowired
    private CoopReplyAccMapper coopReplyAccMapper;

    @Autowired
    private CoopReplyAccSubMapper coopReplyAccSubMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopReplyApp selectByPrimaryKey(String serno) {
        return coopReplyAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopReplyApp> selectAll(QueryModel model) {
        List<CoopReplyApp> records = (List<CoopReplyApp>) coopReplyAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopReplyApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopReplyApp> list = coopReplyAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopReplyApp record) {
        return coopReplyAppMapper.insert(record);
    }

    /**
     * @方法名称: insert
     * @方法描述: 批复申请表中插入数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void insertAll(CoopReplyAppAllVo coopReplyAppAllVo) {
        //批复申请基本表中插入数据
        CoopReplyApp coopReplyApp = coopReplyAppAllVo.getCoopReplyApp();
        String serno = coopReplyAppAllVo.getCoopReplyApp().getSerno();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        coopReplyAppMapper.updateByPrimaryKeySelective(coopReplyApp);
        //批复申请项目表中插入数据
        List<CoopReplyAppPro> coopReplyAppProList = coopReplyAppAllVo.getCoopReplyAppProList();
        if (!CollectionUtils.isEmpty(coopReplyAppProList)){
            for (CoopReplyAppPro coopReplyAppPro : coopReplyAppProList){
                coopReplyAppProMapper.updateByPrimaryKeySelective(coopReplyAppPro);
            }
        }
        //批复申请分项表中插入数据
        List<CoopReplyAppSub> coopReplyAppSubList = coopReplyAppAllVo.getCoopReplyAppSubList();
        if (!CollectionUtils.isEmpty(coopReplyAppSubList)){
            for (CoopReplyAppSub coopReplyAppSub : coopReplyAppSubList){
                coopReplyAppSubMapper.updateByPrimaryKeySelective(coopReplyAppSub);
            }
        }
        //批复申请条件表中插入数据
        List<CoopReplyAppCond> coopReplyAppCondList = coopReplyAppAllVo.getCoopReplyAppCondList();
        if (!CollectionUtils.isEmpty(coopReplyAppCondList)){
            List<CoopReplyAppCond> coopReplyAppCondList1 = coopReplyAppCondMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(coopReplyAppCondList1)){
                for (CoopReplyAppCond coopReplyAppCond : coopReplyAppCondList1){
                    coopReplyAppCondMapper.deleteByPrimaryKey(coopReplyAppCond.getPkId(),coopReplyAppCond.getSerno());
                }
            }
            int serial = 0;
            for (CoopReplyAppCond coopReplyAppCond : coopReplyAppCondList){
                serial = serial+1;
                coopReplyAppCond.setPkId(UUID.randomUUID().toString());
                coopReplyAppCond.setSerno(serno);
                coopReplyAppCond.setSerial(serial);
                coopReplyAppCond.setCreatTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                coopReplyAppCond.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                coopReplyAppCondMapper.insertSelective(coopReplyAppCond);
            }
        }
        //批复申请贷后管理要求表中插入数据
        List<CoopReplyAppPsp> coopReplyAppPspList = coopReplyAppAllVo.getCoopReplyAppPspList();
        if (!CollectionUtils.isEmpty(coopReplyAppCondList)){
            List<CoopReplyAppPsp> CoopReplyAppPspList1 = coopReplyAppPspMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(CoopReplyAppPspList1)){
                for (CoopReplyAppPsp coopReplyAppPsp : CoopReplyAppPspList1){
                    coopReplyAppPspMapper.deleteByPrimaryKey(coopReplyAppPsp.getPkId(),coopReplyAppPsp.getSerno());
                }
            }
            int serial = 0;
            for (CoopReplyAppPsp coopReplyAppPsp : coopReplyAppPspList){
                serial = serial+1;
                coopReplyAppPsp.setPkId(UUID.randomUUID().toString());
                coopReplyAppPsp.setSerno(serno);
                coopReplyAppPsp.setSerial(serial);
                coopReplyAppPsp.setCreatTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                coopReplyAppPsp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                coopReplyAppPspMapper.insertSelective(coopReplyAppPsp);
            }
        }
    }

    /**
     * @方法名称: insertReplyApp
     * @方法描述: 批复申请表中插入数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void insertReplyApp(CoopReplyApp record) {
        QueryModel queryModel = new QueryModel();
        CoopReplyApp coopReplyApp = new CoopReplyApp();
        queryModel.addCondition("serno", record.getSerno());
        List<CoopPlanApp> coopPlanAppList = coopPlanAppMapper.selectByModel(queryModel);
        CoopPlanApp coopPlanApp= null;
        if (!CollectionUtils.isEmpty(coopPlanAppList)){
            coopPlanApp = coopPlanAppList.get(0);
            BeanUtils.beanCopy(coopPlanApp, coopReplyApp);

        }
        List<CoopReplyApp> coopReplyAppList = coopReplyAppMapper.selectByModel(queryModel);
        // 批复台账只保留最新的一条，批复申请保留多条。如该合作方案存在旧的批复，则引用旧的批复编号与分项编号
        QueryModel queryModelAcc = new QueryModel();
        queryModelAcc.addCondition("partnerNo", coopPlanApp.getPartnerNo());
        queryModelAcc.addCondition("coopPlanNo", coopPlanApp.getCoopPlanNo());
        List<CoopReplyAcc> coopReplyAccList = coopReplyAccMapper.selectByModel(queryModelAcc);
        //生成批复编号，如果存在就批复编号则引用
        String replyNo = sequenceTemplateClient.getSequenceTemplate("COOP_REPLY_NO", new HashMap());;
        if(coopReplyAccList!=null && coopReplyAccList.size()>0){
            replyNo = coopReplyAccList.get(0).getReplyNo();
        }
        if (!CollectionUtils.isEmpty(coopReplyAppList)){
            coopReplyApp.setReplyNo(replyNo);
            coopReplyAppMapper.updateByPrimaryKey(coopReplyApp);
        }else {
            coopReplyApp.setReplyNo(replyNo);
            coopReplyAppMapper.insert(coopReplyApp);
        }
        //批复申请项目表中插入数据
        List<CoopPlanProInfoDto> coopPlanProInfoDtoList = coopPlanProInfoMapper.selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(coopPlanProInfoDtoList)){
            List<CoopReplyAppPro> coopReplyAppProList = coopReplyAppProMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(coopReplyAppProList)){
                for(CoopReplyAppPro coopReplyAppPro : coopReplyAppProList){
                    coopReplyAppProMapper.deleteByPrimaryKey(coopReplyAppPro.getPkId(),coopReplyAppPro.getSerno());
                }
            }
            CoopReplyAppPro coopReplyAppPro = new CoopReplyAppPro();
            for (CoopPlanProInfoDto coopPlanProInfoDto : coopPlanProInfoDtoList){
                BeanUtils.beanCopy(coopPlanProInfoDto, coopReplyAppPro);
                coopReplyAppProMapper.insertSelective(coopReplyAppPro);
            }
        }
        //批复申请分项表中插入数据
        List<CoopPlanEspecQuotaCtrlDto> coopPlanEspecQuotaCtrlDtoList = coopPlanEspecQuotaCtrlMapper.selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(coopPlanEspecQuotaCtrlDtoList)){
            List<CoopReplyAppSub> coopReplyAppSubList = coopReplyAppSubMapper.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(coopReplyAppSubList)){
                for (CoopReplyAppSub coopReplyAppSub : coopReplyAppSubList){
                    coopReplyAppSubMapper.deleteByPrimaryKey(coopReplyAppSub.getPkId(),coopReplyAppSub.getSerno());
                }
            }
            CoopReplyAppSub coopReplyAppSub = new CoopReplyAppSub();
            for (CoopPlanEspecQuotaCtrlDto coopPlanEspecQuotaCtrlDto : coopPlanEspecQuotaCtrlDtoList){
                String subNo = sequenceTemplateClient.getSequenceTemplate("COOP_REPLY_SUB_NO", new HashMap());
                //批复台账只保留最新的一条，批复申请保留多条。如该合作方案存在旧的批复，则引用旧的批复编号与分项编号
                QueryModel queryModelAccSub = new QueryModel();
                queryModelAccSub.addCondition("coopPlanNo", coopPlanEspecQuotaCtrlDto.getCoopPlanNo());
                queryModelAccSub.addCondition("prdTypeProp",coopPlanEspecQuotaCtrlDto.getPrdTypeProp());
                List<CoopReplyAccSub> coopReplyAccSubList= coopReplyAccSubMapper.selectByModel(queryModelAccSub);
                if(coopReplyAccSubList!=null && coopReplyAccSubList.size()>0){
                    subNo = coopReplyAccSubList.get(0).getSubNo();
                }
                BeanUtils.beanCopy(coopPlanEspecQuotaCtrlDto,coopReplyAppSub);
                coopReplyAppSub.setSubNo(subNo);
                coopReplyAppSubMapper.insert(coopReplyAppSub);
            }
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopReplyApp record) {
        return coopReplyAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopReplyApp record) {
        return coopReplyAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopReplyApp record) {
        return coopReplyAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return coopReplyAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopReplyAppMapper.deleteByIds(ids);
    }
}
