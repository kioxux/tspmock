package cn.com.yusys.yusp.web.server.xddh0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0006.resp.Xddh0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:提前还款试算查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0006:提前还款试算查询")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0006Resource.class);
    @Autowired
    private cn.com.yusys.yusp.service.server.xddh0006.Xddh0006Service xddh0006Service;

    /**
     * 交易码：xddh0006
     * 交易描述：提前还款试算查询
     *
     * @param xddh0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款试算查询")
    @PostMapping("/xddh0006")
    protected @ResponseBody
    ResultDto<Xddh0006DataRespDto> xddh0006(@Validated @RequestBody Xddh0006DataReqDto xddh0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006DataReqDto));
        Xddh0006DataRespDto xddh0006DataRespDto = new Xddh0006DataRespDto();// 响应Dto:提前还款试算查询
        ResultDto<Xddh0006DataRespDto> xddh0006DataResultDto = new ResultDto<>();
        try {
            // 从xddh0006DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddh0006DataReqDto));
            xddh0006DataRespDto = xddh0006Service.xddh0006(xddh0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddh0006DataRespDto));

            // 封装xddh0006DataResultDto中正确的返回码和返回信息
            xddh0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, e.getMessage());
            // 封装xddh0006DataResultDto中异常返回码和返回信息
            xddh0006DataResultDto.setCode(e.getErrorCode());
            xddh0006DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, e.getMessage());
            // 封装xddh0006DataResultDto中异常返回码和返回信息
            xddh0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0006DataRespDto到xddh0006DataResultDto中
        xddh0006DataResultDto.setData(xddh0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006DataResultDto));
        return xddh0006DataResultDto;
    }
}
