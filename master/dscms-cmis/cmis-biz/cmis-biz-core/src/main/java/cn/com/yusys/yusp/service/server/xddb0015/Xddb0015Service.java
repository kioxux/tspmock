package cn.com.yusys.yusp.service.server.xddb0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.req.GuarstReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.resp.GuarstRespDto;
import cn.com.yusys.yusp.dto.server.xddb0015.req.Xddb0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0015.resp.Xddb0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:押品状态变更推送
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xddb0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0015Service.class);

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * 押品状态变更推送，接口转发
     * @param xddb0015DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xddb0015DataRespDto getXddx0015(Xddb0015DataReqDto xddb0015DataReqDto)throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataReqDto));
        Xddb0015DataRespDto xddb0015DataRespDto = new Xddb0015DataRespDto();
        try {
            GuarstReqDto guarstReqDto = new GuarstReqDto();
            BeanUtils.copyProperties(xddb0015DataReqDto,guarstReqDto);
            ResultDto<GuarstRespDto> guarstRespDtoResultDto = dscms2YpxtClientService.guarst(guarstReqDto);
            if(ResultDto.success().getCode().equals(guarstRespDtoResultDto.getCode())){
                xddb0015DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
                xddb0015DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
            }else{
                xddb0015DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
                xddb0015DataRespDto.setOpMsg(guarstRespDtoResultDto.getMessage());
            }
        } catch (Exception e) {
            xddb0015DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xddb0015DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataRespDto));
        return xddb0015DataRespDto;
    }
}
