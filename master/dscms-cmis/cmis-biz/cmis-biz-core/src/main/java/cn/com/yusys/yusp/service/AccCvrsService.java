/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.dto.BailInfoDto;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.AccCvrsVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: AccCvrsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: user
 * @创建时间: 2021-04-27 21:54:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccCvrsService {

    @Autowired
    private AccCvrsMapper accCvrsMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccCvrs selectByPrimaryKey(String pkId) {
        return accCvrsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<AccCvrs> selectAll(QueryModel model) {
        List<AccCvrs> records = (List<AccCvrs>) accCvrsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccCvrs> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccCvrs> list = accCvrsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(AccCvrs record) {
        return accCvrsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(AccCvrs record) {
        return accCvrsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(AccCvrs record) {
        return accCvrsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(AccCvrs record) {
        return accCvrsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accCvrsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accCvrsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selectForAccCvrsInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:12
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccCvrs> selectForAccCvrsInfo(String cusId) {
        return accCvrsMapper.selectForAccCvrsInfo(cusId);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:59
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccCvrs> selectByContNo(String contNo) {
        return accCvrsMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称：updateAccStatusByBillNo
     * @方法描述：根据借据号更新保函台账表中台账状态
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:59
     * @修改记录：修改时间 修改人员 修改时间
     */
    public int updateAccStatusByBillNo(Map queryMap) {
        return accCvrsMapper.updateAccStatusByBillNo(queryMap);
    }

    /**
     * @param contNo
     * @return countAccCvrgLoanCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    public int countAccCvrgLoanCountByContNo(String contNo) {
        return accCvrsMapper.countAccCvrgLoanCountByContNo(contNo);
    }

    public AccCvrs selectByBillno(String billNo) {
        return accCvrsMapper.selectByBillno(billNo);
    }

    public void updateByBillNoForStatus(AccCvrs accCvrs) {
        accCvrsMapper.updateByBillNoForStatus(accCvrs);
    }


    public void deleteByBillNo(String billNo) {
        accCvrsMapper.deleteByBillNo(billNo);
    }

    public void updateByBillNoForamt(AccCvrs accCvrs) {
        accCvrsMapper.updateByBillNoForamt(accCvrs);
    }

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询保函台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccCvrs selectByBillNo(String billNo) {
        return accCvrsMapper.selectByBillNo(billNo);
    }


    /**
     * 异步导出保函台账列表数据
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccCvrs(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("inputDate desc");
            String apiUrl = "/api/acccvrs/exportAccCvrs";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return exportAccDiscData(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccCvrsVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }

    private List<AccCvrsVo> exportAccDiscData(QueryModel model) {
        List<AccCvrsVo> accCvrsVoList = new ArrayList<>();
        List<AccCvrs> accCvrsList = querymodelByCondition(model);
        for (AccCvrs accCvrs : accCvrsList) {
            AccCvrsVo accCvrsVo = new AccCvrsVo();
            BeanUtils.copyProperties(accCvrs, accCvrsVo);
            String managerIdName = OcaTranslatorUtils.getUserName(accCvrs.getManagerId());// 客户经理名称
            String managerBrIdName = OcaTranslatorUtils.getOrgName(accCvrs.getManagerBrId());// 责任机构名称
            accCvrsVo.setManagerIdName(managerIdName);
            accCvrsVo.setManagerBrIdName(managerBrIdName);
            accCvrsVoList.add(accCvrsVo);
        }
        return accCvrsVoList;
    }

    /**
     * @方法名称: querymodelByCondition
     * @方法描述: 根据查询条件查询台账信息并返回
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccCvrs> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccCvrs> list = accCvrsMapper.querymodelByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectAccCvrsBillNo
     * @方法描述: 根据申请流水号查询保函台账借据编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public AccCvrs selectAccCvrsBillNo(String pvpSerno) {
        return accCvrsMapper.selectAccCvrsBillNo(pvpSerno);
    }

    /**
     * 根据合同编号查询用信敞口余额
     * @param contNos
     * @return
     */
    public BigDecimal selectTotalSpacAmtByContNos(String contNos){
        return accCvrsMapper.selectTotalSpacAmtByContNos(contNos);
    }

    /**
     * 风险分类审批结束更新客户未结清保函台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:36
     **/
    public int updateLoanFiveAndTenClassByCusId(Map<String, String> map) {
        return accCvrsMapper.updateLoanFiveAndTenClassByCusId(map);
    }

    List<BailInfoDto> getBailInfoDto(String cusId){
        return accCvrsMapper.getBailInfoDto(cusId);
    }

}
