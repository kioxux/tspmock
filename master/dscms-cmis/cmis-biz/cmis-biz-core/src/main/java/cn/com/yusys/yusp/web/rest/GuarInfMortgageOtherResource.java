/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;
import cn.com.yusys.yusp.domain.GuarInfMortgageOther;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfMortgageOther;
import cn.com.yusys.yusp.service.GuarInfMortgageOtherService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMortgageOtherResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:17:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfmortgageother")
public class GuarInfMortgageOtherResource {
    @Autowired
    private GuarInfMortgageOtherService guarInfMortgageOtherService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfMortgageOther>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfMortgageOther> list = guarInfMortgageOtherService.selectAll(queryModel);
        return new ResultDto<List<GuarInfMortgageOther>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfMortgageOther>> index(QueryModel queryModel) {
        List<GuarInfMortgageOther> list = guarInfMortgageOtherService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfMortgageOther>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfMortgageOther> show(@PathVariable("serno") String serno) {
        GuarInfMortgageOther guarInfMortgageOther = guarInfMortgageOtherService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfMortgageOther>(guarInfMortgageOther);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfMortgageOther> create(@RequestBody GuarInfMortgageOther guarInfMortgageOther) throws URISyntaxException {
        guarInfMortgageOtherService.insert(guarInfMortgageOther);
        return new ResultDto<GuarInfMortgageOther>(guarInfMortgageOther);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfMortgageOther guarInfMortgageOther) throws URISyntaxException {
        int result = guarInfMortgageOtherService.update(guarInfMortgageOther);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfMortgageOtherService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfMortgageOtherService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfMortgageOther
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfMortgageOther")
    protected ResultDto<Integer> preserveGuarInfMortgageOther(@RequestBody GuarInfMortgageOther guarInfMortgageOther) {
        int result = guarInfMortgageOtherService.preserveGuarInfMortgageOther(guarInfMortgageOther);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfMortgageOther> queryBySerno(@RequestBody GuarInfMortgageOther record) {
        GuarInfMortgageOther guarInfMortgageOther = guarInfMortgageOtherService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfMortgageOther>(guarInfMortgageOther);
    }
}
