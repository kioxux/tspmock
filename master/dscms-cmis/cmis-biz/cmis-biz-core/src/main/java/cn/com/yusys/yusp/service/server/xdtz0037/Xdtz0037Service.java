package cn.com.yusys.yusp.service.server.xdtz0037;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0037.req.Xdtz0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.resp.Xdtz0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * 接口处理类:无还本续贷额度状态更新
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdtz0037Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0037Service.class);
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;

    /**
     * 无还本续贷额度状态更新
     *
     * @param xdtz0037DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0037DataRespDto queryLoanCont(Xdtz0037DataReqDto xdtz0037DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataReqDto));
        Xdtz0037DataRespDto xdtz0037DataRespDto = new Xdtz0037DataRespDto();
        String surveySerno = xdtz0037DataReqDto.getSurveySerno();//客户调查主键
        int zxSerno = 0;//放款笔数
        logger.info("**********Xddb0037Service查询放款笔数开始*START**************");
        if (StringUtil.isNotEmpty(surveySerno)) {//客户调查主键押空校验
            zxSerno = pvpLoanAppMapper.queryLoanContBysureySerno(surveySerno);
            xdtz0037DataRespDto.setZxSerno(String.valueOf(zxSerno));
        } else {
            //请求参数不存在
            xdtz0037DataRespDto.setZxSerno(StringUtils.EMPTY);// 放款笔数
            throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataRespDto));
        return xdtz0037DataRespDto;
    }
}
