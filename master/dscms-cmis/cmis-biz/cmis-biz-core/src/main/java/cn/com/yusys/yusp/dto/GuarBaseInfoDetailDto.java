package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBaseInfoDetailDto
 * @类描述: 统一押品详情,便于处理
 * @功能描述:
 * @创建人: zfq
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarBaseInfoDetailDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 业务流水号 **/
    private String serno;
    /** 押品统一编号 **/
    private String guarNo;
    /** 抵押住房是否权属人唯一住所 **/
    private String houseOwnershipInd;
    /** 房屋产权期限信息（年） **/
    private String housePrDesc;
    /** 楼盘（社区）名称 **/
    private String communityName;
    /** 所属地段 **/
    private String belongArea;
    /** 周边环境 STD_ZB_ARR_ENV **/
    private String arrEnv;
    /** 套型 STD_ZB_F_STY **/
    private String fSty;
    /** 房屋状态 STD_ZB_HOUSE_STA **/
    private String houseSta;
    /** 房屋结构 STD_ZB_HOUSE_STRUCTURE **/
    private String houseStructure;
    /** 地面构造 STD_ZB_GROUND_STRUCTURE **/
    private String groundStructure;
    /** 屋顶构造 STD_ZB_GROUND_STRUCTURE **/
    private String roofStructure;
    /** 公共配套 STD_ZB_PUBLIC_FACILITIES **/
    private String publicFacilities;
    /** 装修状况 STD_ZB_DECORATION **/
    private String decoration;
    /** 平面布局 **/
    private String planeLayout;
    /** 朝向 STD_ZB_ORIENTATIONS **/
    private String orientations;
    /** 通风采光 STD_ZB_AND_LIGHTING **/
    private String ventilationAndLighting;
    /** 临街状况 STD_ZB_STREET_SITUATION **/
    private String streetSituation;
    /** 街道/村镇/路名 **/
    private String street;
    /** 物业情况 **/
    private String propertyCase;
    /** 房产使用情况 **/
    private String houseProperty;
    /** 车库/车位类型 STD_ZB_CARPORT_TYPE **/
    private String carportType;
    /** 该产证是否全部抵押 STD_ZB_YES_NO **/
    private String houseAllPledgeInd;
    /** 工业房地产开发模式 STD_ZB_DECELOP_MODEL **/
    private String industryDecelopModel;
    /** 竣工日期 **/
    private String endDate;
    /** 房、地是否均已抵押我行 STD_ZB_YES_NO **/
    private String houseLandPledgeInd;
    /** 墙壁结构 **/
    private String wallStructure;
    /** 是否包含土地 STD_ZB_YES_NO **/
    private String fullLand;
    /** 所在/注册省份 **/
    private String provinceCd;
    /** 所在/注册市 **/
    private String cityCd;
    /** 所在县（区） **/
    private String countyCd;
    /** 房产类别 STD_ZB_INV_PTY_TYP **/
    private String realproCls;
    /** 土地说明 **/
    private String landExplain;
    /** 闲置土地类型STD_ZB_XZTDLX **/
    private String landNotinuseType;
    /** 土地所在地段情况 STD_ZB_FCDDQK **/
    private String landPInfo;
    /** 宗地号 **/
    private String parcelNo;
    /** 是否有地上定着物 **/
    private String landUp;
    /** 定着物种类 STD_ZB_DZWZL **/
    private String landUpType;
    /** 地上建筑物项数 **/
    private Integer landBuildAmount;
    /** 地上定着物总面积 **/
    private String landUpAllArea;
    /** 定着物所有权人名称 **/
    private String landUpOwnershipName;
    /** 定着物所有权人范围 STD_ZB_LAND_UP_SCOPE **/
    private String landUpOwnershipScope;
    /** 地上定着物说明 **/
    private String landUpExplain;
    /** 房地产买卖合同编号 **/
    private String businessHouseNo;
    /** 林地名称 **/
    private String forestName;
    /** 林种 **/
    private String forestVariet;
    /** 主要种树 **/
    private String forestMain;
    /** 取得方式 **/
    private String acquMode;
    /** 一手/二手标识 STD_ZB_IS_USED **/
    private String isUsed;
    /** 发票编号 **/
    private String invoiceNo;
    /** 发票日期 **/
    private String invoiceDate;
    /** 设备铭牌编号 **/
    private String equipNo;
    /** 设备类型 STD_ZB_SBLX **/
    private String machineType;
    /** 设备分类 STD_ZB_SBFL **/
    private String machineCode;
    /** 型号/规格 **/
    private String specModel;
    /** 品牌/生产厂商 **/
    private String vehicleBrand;
    /** 设备数量 **/
    private String equipQnt;
    /** 是否有产品合格证 **/
    private String indEligibleCerti;
    /** 出厂日期或报关日期 **/
    private String factoryDate;
    /** 设计使用到期日期 **/
    private String matureDate;
    /** 发票金额（元） **/
    private java.math.BigDecimal buyPrice;
    /** 土地用途 **/
    private String landPurp;
    /** 是否欠工程款 **/
    private String isDelayProjectAmt;
    /** 欠工程款金额 **/
    private String delayProjectAmt;
    /** 详细地址 **/
    private String detailsAddress;
    /** 供应链质押价值 **/
    private java.math.BigDecimal gylVal;
    /** 是否有监管公司 **/
    private String hasSupervision;
    /** 监管公司名称 **/
    private String supervisionCompanyName;
    /** 监管公司组织机构代码 **/
    private String supervisionOrgCode;
    /** 协议生效日 **/
    private String agreementBeginDate;
    /** 协议到期日 **/
    private String agreementEndDate;
    /** 金额 **/
    private java.math.BigDecimal amt;
    /** 保管人 **/
    private String keepId;
    /** 货物详细类型 **/
    private String coodsDetailType;
    /** 货物名称 **/
    private String cargoName;
    /** 货物数量 **/
    private java.math.BigDecimal cargoAmount;
    /** 最新核定单价 **/
    private java.math.BigDecimal latestApprovedPrice;
    /** 货物计量单位 STD_ZB_CARGO_MEASURE_UNIT **/
    private String cargoMeasureUnit;
    /** 货物型号 **/
    private String goodsModel;

    /** 建设工程规划许可证号 **/
    private String layoutLicence;

    public String getLayoutLicence() {
        return layoutLicence;
    }

    public void setLayoutLicence(String layoutLicence) {
        this.layoutLicence = layoutLicence;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getHouseOwnershipInd() {
        return houseOwnershipInd;
    }

    public void setHouseOwnershipInd(String houseOwnershipInd) {
        this.houseOwnershipInd = houseOwnershipInd;
    }

    public String getHousePrDesc() {
        return housePrDesc;
    }

    public void setHousePrDesc(String housePrDesc) {
        this.housePrDesc = housePrDesc;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getBelongArea() {
        return belongArea;
    }

    public void setBelongArea(String belongArea) {
        this.belongArea = belongArea;
    }

    public String getArrEnv() {
        return arrEnv;
    }

    public void setArrEnv(String arrEnv) {
        this.arrEnv = arrEnv;
    }

    public String getfSty() {
        return fSty;
    }

    public void setfSty(String fSty) {
        this.fSty = fSty;
    }

    public String getHouseSta() {
        return houseSta;
    }

    public void setHouseSta(String houseSta) {
        this.houseSta = houseSta;
    }

    public String getHouseStructure() {
        return houseStructure;
    }

    public void setHouseStructure(String houseStructure) {
        this.houseStructure = houseStructure;
    }

    public String getGroundStructure() {
        return groundStructure;
    }

    public void setGroundStructure(String groundStructure) {
        this.groundStructure = groundStructure;
    }

    public String getRoofStructure() {
        return roofStructure;
    }

    public void setRoofStructure(String roofStructure) {
        this.roofStructure = roofStructure;
    }

    public String getPublicFacilities() {
        return publicFacilities;
    }

    public void setPublicFacilities(String publicFacilities) {
        this.publicFacilities = publicFacilities;
    }

    public String getDecoration() {
        return decoration;
    }

    public void setDecoration(String decoration) {
        this.decoration = decoration;
    }

    public String getPlaneLayout() {
        return planeLayout;
    }

    public void setPlaneLayout(String planeLayout) {
        this.planeLayout = planeLayout;
    }

    public String getOrientations() {
        return orientations;
    }

    public void setOrientations(String orientations) {
        this.orientations = orientations;
    }

    public String getVentilationAndLighting() {
        return ventilationAndLighting;
    }

    public void setVentilationAndLighting(String ventilationAndLighting) {
        this.ventilationAndLighting = ventilationAndLighting;
    }

    public String getStreetSituation() {
        return streetSituation;
    }

    public void setStreetSituation(String streetSituation) {
        this.streetSituation = streetSituation;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPropertyCase() {
        return propertyCase;
    }

    public void setPropertyCase(String propertyCase) {
        this.propertyCase = propertyCase;
    }

    public String getHouseProperty() {
        return houseProperty;
    }

    public void setHouseProperty(String houseProperty) {
        this.houseProperty = houseProperty;
    }

    public String getCarportType() {
        return carportType;
    }

    public void setCarportType(String carportType) {
        this.carportType = carportType;
    }

    public String getHouseAllPledgeInd() {
        return houseAllPledgeInd;
    }

    public void setHouseAllPledgeInd(String houseAllPledgeInd) {
        this.houseAllPledgeInd = houseAllPledgeInd;
    }

    public String getIndustryDecelopModel() {
        return industryDecelopModel;
    }

    public void setIndustryDecelopModel(String industryDecelopModel) {
        this.industryDecelopModel = industryDecelopModel;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHouseLandPledgeInd() {
        return houseLandPledgeInd;
    }

    public void setHouseLandPledgeInd(String houseLandPledgeInd) {
        this.houseLandPledgeInd = houseLandPledgeInd;
    }

    public String getWallStructure() {
        return wallStructure;
    }

    public void setWallStructure(String wallStructure) {
        this.wallStructure = wallStructure;
    }

    public String getFullLand() {
        return fullLand;
    }

    public void setFullLand(String fullLand) {
        this.fullLand = fullLand;
    }

    public String getProvinceCd() {
        return provinceCd;
    }

    public void setProvinceCd(String provinceCd) {
        this.provinceCd = provinceCd;
    }

    public String getCityCd() {
        return cityCd;
    }

    public void setCityCd(String cityCd) {
        this.cityCd = cityCd;
    }

    public String getCountyCd() {
        return countyCd;
    }

    public void setCountyCd(String countyCd) {
        this.countyCd = countyCd;
    }

    public String getRealproCls() {
        return realproCls;
    }

    public void setRealproCls(String realproCls) {
        this.realproCls = realproCls;
    }

    public String getLandExplain() {
        return landExplain;
    }

    public void setLandExplain(String landExplain) {
        this.landExplain = landExplain;
    }

    public String getLandNotinuseType() {
        return landNotinuseType;
    }

    public void setLandNotinuseType(String landNotinuseType) {
        this.landNotinuseType = landNotinuseType;
    }

    public String getLandPInfo() {
        return landPInfo;
    }

    public void setLandPInfo(String landPInfo) {
        this.landPInfo = landPInfo;
    }

    public String getParcelNo() {
        return parcelNo;
    }

    public void setParcelNo(String parcelNo) {
        this.parcelNo = parcelNo;
    }

    public String getLandUp() {
        return landUp;
    }

    public void setLandUp(String landUp) {
        this.landUp = landUp;
    }

    public String getLandUpType() {
        return landUpType;
    }

    public void setLandUpType(String landUpType) {
        this.landUpType = landUpType;
    }

    public Integer getLandBuildAmount() {
        return landBuildAmount;
    }

    public void setLandBuildAmount(Integer landBuildAmount) {
        this.landBuildAmount = landBuildAmount;
    }

    public String getLandUpAllArea() {
        return landUpAllArea;
    }

    public void setLandUpAllArea(String landUpAllArea) {
        this.landUpAllArea = landUpAllArea;
    }

    public String getLandUpOwnershipName() {
        return landUpOwnershipName;
    }

    public void setLandUpOwnershipName(String landUpOwnershipName) {
        this.landUpOwnershipName = landUpOwnershipName;
    }

    public String getLandUpOwnershipScope() {
        return landUpOwnershipScope;
    }

    public void setLandUpOwnershipScope(String landUpOwnershipScope) {
        this.landUpOwnershipScope = landUpOwnershipScope;
    }

    public String getLandUpExplain() {
        return landUpExplain;
    }

    public void setLandUpExplain(String landUpExplain) {
        this.landUpExplain = landUpExplain;
    }

    public String getBusinessHouseNo() {
        return businessHouseNo;
    }

    public void setBusinessHouseNo(String businessHouseNo) {
        this.businessHouseNo = businessHouseNo;
    }

    public String getForestName() {
        return forestName;
    }

    public void setForestName(String forestName) {
        this.forestName = forestName;
    }

    public String getForestVariet() {
        return forestVariet;
    }

    public void setForestVariet(String forestVariet) {
        this.forestVariet = forestVariet;
    }

    public String getForestMain() {
        return forestMain;
    }

    public void setForestMain(String forestMain) {
        this.forestMain = forestMain;
    }

    public String getAcquMode() {
        return acquMode;
    }

    public void setAcquMode(String acquMode) {
        this.acquMode = acquMode;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getEquipNo() {
        return equipNo;
    }

    public void setEquipNo(String equipNo) {
        this.equipNo = equipNo;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getMachineCode() {
        return machineCode;
    }

    public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }

    public String getSpecModel() {
        return specModel;
    }

    public void setSpecModel(String specModel) {
        this.specModel = specModel;
    }

    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    public String getEquipQnt() {
        return equipQnt;
    }

    public void setEquipQnt(String equipQnt) {
        this.equipQnt = equipQnt;
    }

    public String getIndEligibleCerti() {
        return indEligibleCerti;
    }

    public void setIndEligibleCerti(String indEligibleCerti) {
        this.indEligibleCerti = indEligibleCerti;
    }

    public String getFactoryDate() {
        return factoryDate;
    }

    public void setFactoryDate(String factoryDate) {
        this.factoryDate = factoryDate;
    }

    public String getMatureDate() {
        return matureDate;
    }

    public void setMatureDate(String matureDate) {
        this.matureDate = matureDate;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getLandPurp() {
        return landPurp;
    }

    public void setLandPurp(String landPurp) {
        this.landPurp = landPurp;
    }

    public String getIsDelayProjectAmt() {
        return isDelayProjectAmt;
    }

    public void setIsDelayProjectAmt(String isDelayProjectAmt) {
        this.isDelayProjectAmt = isDelayProjectAmt;
    }

    public String getDelayProjectAmt() {
        return delayProjectAmt;
    }

    public void setDelayProjectAmt(String delayProjectAmt) {
        this.delayProjectAmt = delayProjectAmt;
    }

    public String getDetailsAddress() {
        return detailsAddress;
    }

    public void setDetailsAddress(String detailsAddress) {
        this.detailsAddress = detailsAddress;
    }

    public BigDecimal getGylVal() {
        return gylVal;
    }

    public void setGylVal(BigDecimal gylVal) {
        this.gylVal = gylVal;
    }

    public String getHasSupervision() {
        return hasSupervision;
    }

    public void setHasSupervision(String hasSupervision) {
        this.hasSupervision = hasSupervision;
    }

    public String getSupervisionCompanyName() {
        return supervisionCompanyName;
    }

    public void setSupervisionCompanyName(String supervisionCompanyName) {
        this.supervisionCompanyName = supervisionCompanyName;
    }

    public String getSupervisionOrgCode() {
        return supervisionOrgCode;
    }

    public void setSupervisionOrgCode(String supervisionOrgCode) {
        this.supervisionOrgCode = supervisionOrgCode;
    }

    public String getAgreementBeginDate() {
        return agreementBeginDate;
    }

    public void setAgreementBeginDate(String agreementBeginDate) {
        this.agreementBeginDate = agreementBeginDate;
    }

    public String getAgreementEndDate() {
        return agreementEndDate;
    }

    public void setAgreementEndDate(String agreementEndDate) {
        this.agreementEndDate = agreementEndDate;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getKeepId() {
        return keepId;
    }

    public void setKeepId(String keepId) {
        this.keepId = keepId;
    }

    public String getCoodsDetailType() {
        return coodsDetailType;
    }

    public void setCoodsDetailType(String coodsDetailType) {
        this.coodsDetailType = coodsDetailType;
    }

    public String getCargoName() {
        return cargoName;
    }

    public void setCargoName(String cargoName) {
        this.cargoName = cargoName;
    }

    public BigDecimal getCargoAmount() {
        return cargoAmount;
    }

    public void setCargoAmount(BigDecimal cargoAmount) {
        this.cargoAmount = cargoAmount;
    }

    public BigDecimal getLatestApprovedPrice() {
        return latestApprovedPrice;
    }

    public void setLatestApprovedPrice(BigDecimal latestApprovedPrice) {
        this.latestApprovedPrice = latestApprovedPrice;
    }

    public String getCargoMeasureUnit() {
        return cargoMeasureUnit;
    }

    public void setCargoMeasureUnit(String cargoMeasureUnit) {
        this.cargoMeasureUnit = cargoMeasureUnit;
    }

    public String getGoodsModel() {
        return goodsModel;
    }

    public void setGoodsModel(String goodsModel) {
        this.goodsModel = goodsModel;
    }
}
