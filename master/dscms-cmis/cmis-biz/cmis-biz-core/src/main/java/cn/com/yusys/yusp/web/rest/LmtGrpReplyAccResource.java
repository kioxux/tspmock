/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReplyAcc;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpReplyAcc;
import cn.com.yusys.yusp.service.LmtGrpReplyAccService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:40:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrpreplyacc")
public class LmtGrpReplyAccResource {
    @Autowired
    private LmtGrpReplyAccService lmtGrpReplyAccService;

	/**
     * 全表查询.该方法废弃
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpReplyAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpReplyAcc>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpReplyAcc>> index(QueryModel queryModel) {
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpReplyAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpReplyAcc> show(@PathVariable("pkId") String pkId) {
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpReplyAcc> create(@RequestBody LmtGrpReplyAcc lmtGrpReplyAcc) throws URISyntaxException {
        lmtGrpReplyAccService.insert(lmtGrpReplyAcc);
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpReplyAcc lmtGrpReplyAcc) throws URISyntaxException {
        int result = lmtGrpReplyAccService.update(lmtGrpReplyAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpReplyAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpReplyAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryDetail
     * @函数描述:根据集团授信台账编号查询集团台账信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querydetail")
    protected ResultDto<LmtGrpReplyAcc> queryDetail(@RequestBody HashMap<String, String> hashMap) {
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAccService.queryDetailByGrpAccNo(hashMap.get("grpAccNo")));
    }

    /**
     *  批量显示批复台账表的信息显示,备用
     * @函数名称:Applist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/applist")
    protected ResultDto<List<LmtGrpReplyAcc>> toSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.toSignlist(queryModel);
        ResultDto<List<LmtGrpReplyAcc>> resultDto = new ResultDto<List<LmtGrpReplyAcc>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<LmtGrpReplyAcc>>(list);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:集团客户批复查询列表
     * @参数与返回说明:
     * @param queryModel
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryall")
    protected ResultDto<List<LmtGrpReplyAcc>> queryAll(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.queryAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @方法名称: getReplyAccForRefine
     * @方法描述: 过滤得到可进行预授信变更的授信批复台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("过滤得到可进行预授信变更的授信批复台账")
    @PostMapping("/getreplyaccforrefine")
    protected ResultDto<List<LmtGrpReplyAcc>> getReplyAccForRefine(@RequestBody QueryModel params) {
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.getReplyAccForRefine(params);
        return new ResultDto<List<LmtGrpReplyAcc>>(list);
    }
    /**
     * @方法名称：queryLmtReplyDataByParams
     * @方法描述：获取当前登录人下的批复台账信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 10:07
     * @修改记录：修改时间   修改人员  修改原因
     */
    @PostMapping("/querybymanagerid")
    protected ResultDto<List<LmtGrpReplyAcc>> queryByManagerId() {
        List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccService.queryByManagerId();
        return new ResultDto<>(lmtGrpReplyAccList);
    }

    /**
     * @方法名称：queryByCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-06-22 上午 10:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/querybygrpcusid")
    protected ResultDto<List<LmtGrpReplyAcc>> queryByGrpCusId(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyAcc> lmtGrpReplyAccList = lmtGrpReplyAccService.queryByGrpCusId(queryModel);
        return new ResultDto<>(lmtGrpReplyAccList);
    }

    /**
     * @函数名称:queryLmtGrpReplyAccByGrpReplySerno
     * @函数描述:根据批复编号查询批复台账信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querylmtgrpreplyaccbygrpreplyserno")
    protected ResultDto<LmtGrpReplyAcc> queryLmtGrpReplyAccByGrpReplySerno(@RequestBody String grpReplySerno) throws URISyntaxException {
        LmtGrpReplyAcc lmtGrpReplyAcc = lmtGrpReplyAccService.queryLmtGrpReplyAccByGrpReplySerno(grpReplySerno);
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAcc);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:集团客户批复台账查询列表
     * @参数与返回说明:
     * @param queryModel
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/querylmtgrpacchis")
    protected ResultDto<List<LmtGrpReplyAcc>> queryLmtGrpAccListHis(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyAcc> list = lmtGrpReplyAccService.queryAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @方法名称：replyAccForManager
     * @方法描述：当前登录人下的批复台账信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-08-10 上午 9:19
     * @修改记录：修改时间   修改人员  修改原因
     */
    @PostMapping("/replyaccformanager")
    protected ResultDto<List<LmtGrpReplyAcc>> replyAccForManager(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyAcc> lmtGrpReplyAccs = lmtGrpReplyAccService.selectForManager(queryModel);
        return new ResultDto<>(lmtGrpReplyAccs);
    }

    @PostMapping("/getbysingleserno")
    protected ResultDto<LmtGrpReplyAcc> getBySingleSerno(@RequestBody String singleSerno) throws Exception {
        LmtGrpReplyAcc lmtGrpReplyAcc=lmtGrpReplyAccService.getBySingleSerno(singleSerno);
        return new ResultDto<LmtGrpReplyAcc>(lmtGrpReplyAcc);
    }
}
