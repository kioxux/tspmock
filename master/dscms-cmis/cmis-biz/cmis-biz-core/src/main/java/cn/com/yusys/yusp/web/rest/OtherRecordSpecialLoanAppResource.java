/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.OtherForRateApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.domain.OtherRecordSpecialLoanApp;
import cn.com.yusys.yusp.service.OtherRecordSpecialLoanAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordSpecialLoanAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 13:47:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otherrecordspecialloanapp")
public class OtherRecordSpecialLoanAppResource {
    @Autowired
    private OtherRecordSpecialLoanAppService otherRecordSpecialLoanAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherRecordSpecialLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppService.selectAll(queryModel);
        return new ResultDto<List<OtherRecordSpecialLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherRecordSpecialLoanApp>> index(QueryModel queryModel) {
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordSpecialLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<OtherRecordSpecialLoanApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherRecordSpecialLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherRecordSpecialLoanApp> show(@PathVariable("serno") String serno) {
        OtherRecordSpecialLoanApp otherRecordSpecialLoanApp = otherRecordSpecialLoanAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherRecordSpecialLoanApp>(otherRecordSpecialLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherRecordSpecialLoanApp> create(@RequestBody OtherRecordSpecialLoanApp otherRecordSpecialLoanApp) throws URISyntaxException {
        otherRecordSpecialLoanAppService.insert(otherRecordSpecialLoanApp);
        return new ResultDto<OtherRecordSpecialLoanApp>(otherRecordSpecialLoanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherRecordSpecialLoanApp otherRecordSpecialLoanApp) throws URISyntaxException {
        int result = otherRecordSpecialLoanAppService.update(otherRecordSpecialLoanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherRecordSpecialLoanAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherRecordSpecialLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:单个对象删除，将操作类型置为删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteInfo/{serno}")
    protected ResultDto<Integer> deleteInfo(@PathVariable("serno") String serno) {
        int result = otherRecordSpecialLoanAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteInfo
     * @函数描述:授信场景下 逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateoprtypeunderlmt")
    protected ResultDto<Integer> updateOprTypeUnderLmt(@RequestBody String serno) {
        int result = otherRecordSpecialLoanAppService.deleteInfo(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: getotherrecordspecialloanapp
     * @方法描述: 根据入参获取当前客户经理名下用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherrecordspecialloanapp")
    protected ResultDto<List<OtherRecordSpecialLoanApp>> getotherrecordspecialloanapp(@RequestBody QueryModel model) {
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppService.getOtherRecordSpecialLoanAppByModel(model);
        return new ResultDto<List<OtherRecordSpecialLoanApp>>(list);
    }

    /**
     * @方法名称: getotherrecordspecialloanapphis
     * @方法描述: 根据入参获取当前客户经理名下用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotherrecordspecialloanapphis")
    protected ResultDto<List<OtherRecordSpecialLoanApp>> getotherrecordspecialloanapphis(@RequestBody QueryModel model) {
        List<OtherRecordSpecialLoanApp> list = otherRecordSpecialLoanAppService.getOtherRecordSpecialLoanAppHis(model);
        return new ResultDto<List<OtherRecordSpecialLoanApp>>(list);
    }

    /**
     * @方法名称: addotherrecordspecialloanapp
     * @方法描述: 新增用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addotherrecordspecialloanapp")
    protected ResultDto addotherrecordspecialloanapp(@RequestBody OtherRecordSpecialLoanApp otherRecordSpecialLoanApp) {
        return otherRecordSpecialLoanAppService.addotherrecordspecialloanapp(otherRecordSpecialLoanApp);
    }

    /**
     * @方法名称: updateotherrecordspecialloanapp
     * @方法描述: 修改用信审核备案申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateotherrecordspecialloanapp")
    protected ResultDto updateotherrecordspecialloanapp(@RequestBody OtherRecordSpecialLoanApp otherRecordSpecialLoanApp) {
        return otherRecordSpecialLoanAppService.updateotherrecordspecialloanapp(otherRecordSpecialLoanApp);
    }
}
