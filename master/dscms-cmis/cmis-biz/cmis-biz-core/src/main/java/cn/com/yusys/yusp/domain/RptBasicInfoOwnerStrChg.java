/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoOwnerStrChg
 * @类描述: rpt_basic_info_owner_str_chg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:55:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_owner_str_chg")
public class RptBasicInfoOwnerStrChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 股东名称 **/
	@Column(name = "SHD_CUS_NAME", unique = false, nullable = true, length = 40)
	private String shdCusName;
	
	/** 变更日期 **/
	@Column(name = "CHANGE_DATE", unique = false, nullable = true, length = 20)
	private String changeDate;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 占比 **/
	@Column(name = "PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal perc;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCap;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param shdCusName
	 */
	public void setShdCusName(String shdCusName) {
		this.shdCusName = shdCusName;
	}
	
    /**
     * @return shdCusName
     */
	public String getShdCusName() {
		return this.shdCusName;
	}
	
	/**
	 * @param changeDate
	 */
	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}
	
    /**
     * @return changeDate
     */
	public String getChangeDate() {
		return this.changeDate;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param perc
	 */
	public void setPerc(java.math.BigDecimal perc) {
		this.perc = perc;
	}
	
    /**
     * @return perc
     */
	public java.math.BigDecimal getPerc() {
		return this.perc;
	}
	
	/**
	 * @param paidCap
	 */
	public void setPaidCap(java.math.BigDecimal paidCap) {
		this.paidCap = paidCap;
	}
	
    /**
     * @return paidCap
     */
	public java.math.BigDecimal getPaidCap() {
		return this.paidCap;
	}


}