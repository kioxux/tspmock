package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

@ExcelCsv(namePrefix = "金融机构准入名单导出", fileType = ExcelCsv.ExportFileType.XLS)
public class IntbankOrgAdmitAccEmportVo {

    /*
    申请流水号
     */
    @ExcelField(title = "批复台账编号", viewLength = 20)
    private String accNo;

    /*
    客户号
     */
    @ExcelField(title = "客户号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 40)
    private String cusName;

    /*
    主管客户经理
     */
    @ExcelField(title = "主管客户经理", viewLength = 20)
    private String managerIdName;

    /*
    主管机构
     */
    @ExcelField(title = "主管机构", viewLength = 20)
    private String managerBrIdName;

    /*
    申请时间
     */
    @ExcelField(title = "申请时间", viewLength = 20)
    private String inputDate;

    /*
    准入到期日
     */
    @ExcelField(title = "准入到期日", viewLength = 20)
    private String endDate;


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    @Override
    public String toString() {
        return "IntbankOrgAdmitAccEmportVo{" +
                "accNo='" + accNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", managerIdName='" + managerIdName + '\'' +
                ", managerBrIdName='" + managerBrIdName + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
