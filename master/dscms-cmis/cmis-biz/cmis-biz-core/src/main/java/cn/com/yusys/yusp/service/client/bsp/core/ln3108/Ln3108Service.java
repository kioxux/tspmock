package cn.com.yusys.yusp.service.client.bsp.core.ln3108;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.util.GenericBuilder;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款组合查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3108Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：贷款组合查询
     *
     * @param ln3108ReqDto
     * @return
     */
    @Transactional
    public Ln3108RespDto ln3108(Ln3108ReqDto ln3108ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value, JSON.toJSONString(ln3108ReqDto));
        ResultDto<Ln3108RespDto> ln3108ResultDto = dscms2CoreLnClientService.ln3108(ln3108ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value, JSON.toJSONString(ln3108ResultDto));
        String ln3108Code = Optional.ofNullable(ln3108ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3108Meesage = Optional.ofNullable(ln3108ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3108RespDto ln3108RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3108ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3108RespDto = ln3108ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(ln3108Code, ln3108Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value);
        return ln3108RespDto;
    }

    /**
     * 根据 [请求Data：提前还款 ]组装[请求Dto：贷款组合查询]
     *
     * @param xddh0005DataReqDto
     * @return
     */
    public Ln3108ReqDto buildLn3108ReqDto(Xddh0005DataReqDto xddh0005DataReqDto) {
        String billNo = xddh0005DataReqDto.getBillNo();//借据号
        String repayType = xddh0005DataReqDto.getRepayType();//还款模式
        BigDecimal repayAmt = xddh0005DataReqDto.getRepayAmt();//还款金额
        String repayAcctNo = xddh0005DataReqDto.getRepayAcctNo();//还款账号
        String curType = xddh0005DataReqDto.getCurType();//币种
        BigDecimal repayTotlCap = xddh0005DataReqDto.getRepayTotlCap();//还款总本金
        String tranSerno = xddh0005DataReqDto.getTranSerno();//交易流水号
        String yjhkms = xddh0005DataReqDto.getFstRepayMode();//一级还款模式 01-提前还款 02-归还拖欠
        String ejhkmx = xddh0005DataReqDto.getSedRepayMode();//二级还款模式 03-提前归还本金及全部利息 04-提前归还本金及本金对应利息 05-归还拖欠本息 08-灵活还款

        /******************还款模式 (1-结清贷款,2-归还欠款,3-提前还款)**********************/
        if("01".equals(yjhkms)){//提前还款
            repayType = "3";
        }else if("02".equals(yjhkms)){
            repayType = "2";
        }else{
            repayType = "1";
        }

        Ln3108ReqDto ln3108ReqDto = GenericBuilder.of(Ln3108ReqDto::new)//请求DTO:贷款展期查询
                .with(Ln3108ReqDto::setDkjiejuh, billNo) //贷款借据号
                .with(Ln3108ReqDto::setDkzhangh, StringUtils.EMPTY) //贷款账号
                .with(Ln3108ReqDto::setJiejuuje, null)//借据金额
                .with(Ln3108ReqDto::setHetongbh, StringUtils.EMPTY)//合同编号
                .with(Ln3108ReqDto::setKehuhaoo, StringUtils.EMPTY)//客户名
                .with(Ln3108ReqDto::setChaxfanw, StringUtils.EMPTY)//查询范围标志
                .with(Ln3108ReqDto::setYngyjigo, StringUtils.EMPTY)//营业机构
                .with(Ln3108ReqDto::setChanpdma, StringUtils.EMPTY)//产品代码
                .with(Ln3108ReqDto::setKaihriqi, StringUtils.EMPTY)//开户日期
                .with(Ln3108ReqDto::setZhzhriqi, StringUtils.EMPTY)//终止日期
                .with(Ln3108ReqDto::setQishriqi, StringUtils.EMPTY)//起始日期
                .with(Ln3108ReqDto::setDaoqriqi, StringUtils.EMPTY)//到期日期
                .with(Ln3108ReqDto::setXiaohurq, StringUtils.EMPTY)//销户日期
                .with(Ln3108ReqDto::setDjdqriqi, StringUtils.EMPTY)//冻结到期日期
                .with(Ln3108ReqDto::setZhcwjyrq, StringUtils.EMPTY)//最后财务交易日
                .with(Ln3108ReqDto::setScjyriqi, StringUtils.EMPTY)//上次交易日期
                .with(Ln3108ReqDto::setDaikxtai, StringUtils.EMPTY)//贷款形态
                .with(Ln3108ReqDto::setYjfyjzht, StringUtils.EMPTY)//应计非应计状态
                .with(Ln3108ReqDto::setDkzhhzht, StringUtils.EMPTY)//贷款账户状态
                .with(Ln3108ReqDto::setHuobdhao, StringUtils.EMPTY)//货币代号
                .with(Ln3108ReqDto::setHuankzhh, curType)//还款账号
                .with(Ln3108ReqDto::setKaihujig, StringUtils.EMPTY)//开户机构
                .with(Ln3108ReqDto::setQishibis, null)//起始笔数
                .with(Ln3108ReqDto::setChxunbis, null)//查询笔数
                .with(Ln3108ReqDto::setDaikduix, StringUtils.EMPTY)//贷款对象
                .with(Ln3108ReqDto::setXhdkqyzh, StringUtils.EMPTY)//循环贷款签约账号
                .with(Ln3108ReqDto::setDkrzhzhh, StringUtils.EMPTY)//贷款入账账号
                .with(Ln3108ReqDto::setCndkjjho, StringUtils.EMPTY)//承诺贷款借据号
                .with(Ln3108ReqDto::setKehmingc, StringUtils.EMPTY)//客户名称
                .with(Ln3108ReqDto::setHuankfsh, repayType)//还款方式
                .with(Ln3108ReqDto::setDzhhkjih, StringUtils.EMPTY)//定制还款计划
                .with(Ln3108ReqDto::setCxqkbizi, StringUtils.EMPTY)//查询欠款标志
                .with(Ln3108ReqDto::setKhjingli, StringUtils.EMPTY)//客户经理
                .build();
        return ln3108ReqDto;
    }
}
