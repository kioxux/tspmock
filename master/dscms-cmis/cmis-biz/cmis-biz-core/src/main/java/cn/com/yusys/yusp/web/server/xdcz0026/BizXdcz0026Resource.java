package cn.com.yusys.yusp.web.server.xdcz0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0026.req.Xdcz0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.resp.Xdcz0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0026.Xdcz0026Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:风控调用信贷放款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDCZ0026:风控调用信贷放款")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0026Resource.class);
    @Autowired
    private Xdcz0026Service xdcz0026Service;

    /**
     * 交易码：xdcz0026
     * 交易描述：风控调用信贷放款
     *
     * @param xdcz0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控调用信贷放款")
    @PostMapping("/xdcz0026")
    protected @ResponseBody
    ResultDto<Xdcz0026DataRespDto> xdcz0026(@Validated @RequestBody Xdcz0026DataReqDto xdcz0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataReqDto));
        Xdcz0026DataRespDto xdcz0026DataRespDto = new Xdcz0026DataRespDto();// 响应Dto:风控调用信贷放款
        ResultDto<Xdcz0026DataRespDto> xdcz0026DataResultDto = new ResultDto<>();

        try {
            // 从xdcz0026DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataReqDto));
            xdcz0026DataRespDto = xdcz0026Service.xdcz0026(xdcz0026DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataRespDto));
            // 封装xdcz0026DataResultDto中正确的返回码和返回信息
            String opFlag = xdcz0026DataRespDto.getOpFlag();
            String opMessage = xdcz0026DataRespDto.getOpMsg();
            //如果失败，返回9999
            if("F".equals(opFlag)){//出账失败
                xdcz0026DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdcz0026DataResultDto.setMessage(opMessage);
            }else{
                xdcz0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdcz0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, e.getMessage());
            // 封装xdcz0026DataResultDto中异常返回码和返回信息
            xdcz0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0026DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdcz0026DataRespDto到xdcz0026DataResultDto中
        xdcz0026DataResultDto.setData(xdcz0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataResultDto));
        return xdcz0026DataResultDto;
    }
}
