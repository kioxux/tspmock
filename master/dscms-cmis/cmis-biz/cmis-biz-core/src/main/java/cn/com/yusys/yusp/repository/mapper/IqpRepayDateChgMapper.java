package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRepayDateChg;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayDateChgMapper
 * @类描述: #服务类
 * @功能描述:
 * @创建人: fangzhen
 * @创建时间: 2020-01-14 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

@Mapper
public interface IqpRepayDateChgMapper{


    List<IqpRepayDateChg>  selectByBillNo(String billno );

    IqpRepayDateChg   selectByPrimaryKey(IqpRepayDateChg  iqpRepayDateChg);


    int insert(IqpRepayDateChg iqpRepayDateChg);


    int updateByPrimaryKey(IqpRepayDateChg iqpRepayDateChg);


    int updateOprTypeByPrimaryKey(IqpRepayDateChg iqpRepayDateChg);

    int checkIsExistChgBizByBillNo(HashMap hashMap);

    int delete(IqpRepayDateChg iqpRepayDateChg);

    int updateCommitByPrimaryKey(IqpRepayDateChg iqpRepayDateChg);

    int updateYtgByPrimaryKey(IqpRepayDateChg iqpRepayDateChg);

    int handleBusinessDataAfteCallBack(IqpRepayDateChg iqpRepayDateChg);

    int handleBusinessDataAfteTackBack(IqpRepayDateChg iqpRepayDateChg);

    int handleBusinessDataAfterRefuse(IqpRepayDateChg iqpRepayDateChg);

    List<IqpRepayDateChg> selectByModel(QueryModel model);
}
