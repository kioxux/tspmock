package cn.com.yusys.yusp.web.server.xdxw0034;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0034.req.Xdxw0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0034.resp.Xdxw0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0034.Xdxw0034Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据流水号查询无还本续贷基本信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0034:根据流水号查询无还本续贷基本信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0034Resource.class);

    @Autowired
    private Xdxw0034Service xdxw0034Service;
    /**
     * 交易码：xdxw0034
     * 交易描述：根据流水号查询无还本续贷基本信息
     *
     * @param xdxw0034DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询无还本续贷基本信息")
    @PostMapping("/xdxw0034")
    protected @ResponseBody
    ResultDto<Xdxw0034DataRespDto> xdxw0034(@Validated @RequestBody Xdxw0034DataReqDto xdxw0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataReqDto));
        Xdxw0034DataRespDto xdxw0034DataRespDto = new Xdxw0034DataRespDto();// 响应Dto:根据流水号查询无还本续贷基本信息
        ResultDto<Xdxw0034DataRespDto> xdxw0034DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0034DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
			String indgtSerno = xdxw0034DataReqDto.getIndgtSerno();//调查流水号
            xdxw0034DataRespDto = xdxw0034Service.xdxw0034(xdxw0034DataReqDto);
            // 封装xdxw0034DataResultDto中正确的返回码和返回信息
            xdxw0034DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0034DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, e.getMessage());
            // 封装xdxw0034DataResultDto中异常返回码和返回信息
            xdxw0034DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0034DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0034DataRespDto到xdxw0034DataResultDto中
        xdxw0034DataResultDto.setData(xdxw0034DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataResultDto));
        return xdxw0034DataResultDto;
    }
}
