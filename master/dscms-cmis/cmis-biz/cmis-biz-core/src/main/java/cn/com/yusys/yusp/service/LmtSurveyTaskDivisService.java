package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.LmtSurveyEnums;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyTaskDivisMapper;
import cn.com.yusys.yusp.service.client.bsp.rircp.fkyx01.Fkyx01Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyTaskDivisService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 13:48:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSurveyTaskDivisService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyTaskDivisService.class);

    @Autowired(required = false)
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private Fkyx01Service fkyx01Service;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSurveyTaskDivis selectByPrimaryKey(String pkId) {
        return lmtSurveyTaskDivisMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSurveyTaskDivis> selectAll(QueryModel model) {
        List<LmtSurveyTaskDivis> records = (List<LmtSurveyTaskDivis>) lmtSurveyTaskDivisMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyTaskDivis> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSurveyTaskDivis record) {
        return lmtSurveyTaskDivisMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSurveyTaskDivis record) {
        return lmtSurveyTaskDivisMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSurveyTaskDivis record) {
        return lmtSurveyTaskDivisMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSurveyTaskDivis record) {
        return lmtSurveyTaskDivisMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtSurveyTaskDivisMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSurveyTaskDivisMapper.deleteByIds(ids);
    }

    /**
     * @param lmtSurveyTaskDivis
     * @return int
     * @author WH
     * @date 2021/4/22 14:00
     * @version 1.0.0
     * @desc 调查任务分配
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto allocation(LmtSurveyTaskDivis lmtSurveyTaskDivis) throws Exception {
        logger.info("任务分配编号【{}】，进入手工分配处理逻辑开始！.....................", lmtSurveyTaskDivis.getPkId());
        // 返回结果标识
        int result = 0;
        // 临时对象
        LmtSurveyTaskDivis lmtSurveyTaskDivisTemp = null;
        // 调查主表信息
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = null;
        try {
            // 1.根据主键查询调查任务分配信息  查询到了目前任务分配的信息
            lmtSurveyTaskDivisTemp = selectByPrimaryKey(lmtSurveyTaskDivis.getPkId());
            Objects.requireNonNull(lmtSurveyTaskDivisTemp, "根据主键未查询到分配任务信息");

            // 2、不同产品，分配规则不同，需特殊处理
            logger.info("产品【{}】，分配逻辑处理开始！", lmtSurveyTaskDivis.getPrdName());
            if (Objects.equals(LmtSurveyEnums.PRD_ID_SC010008, lmtSurveyTaskDivis.getPrdId())) {// 优企贷

            }

            // 3、获取处理人相关信息
            AdminSmUserDto prcIdInfo = commonService.getByLoginCode(lmtSurveyTaskDivis.getPrcId());

            //进入分配逻辑
            if (LmtSurveyEnums.DIVIS_STATUS_101.getValue().equals(lmtSurveyTaskDivisTemp.getDivisStatus())) { // 正常分配处理逻辑
                logger.info("任务分配编号【{}】，第一次手工分配开始.....................", lmtSurveyTaskDivis.getPkId());
                // 状态更新为已分配
                lmtSurveyTaskDivisTemp.setDivisStatus(LmtSurveyEnums.DIVIS_STATUS_100.getValue());
                // 处理人更新为选择人
                lmtSurveyTaskDivisTemp.setPrcId(lmtSurveyTaskDivis.getPrcId());
                // 分配时间
                lmtSurveyTaskDivisTemp.setDivisTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                logger.info("开始查询分配人");
                //分配人=>操作人
                User user = SessionUtils.getUserInformation();
                if (user != null) {
                    lmtSurveyTaskDivisTemp.setDivisId(user.getLoginCode());
                    logger.info("分配人工号【{}】", user.getLoginCode());
                }
                logger.info("修改任务分配表【{}】 分配调查流水号", lmtSurveyTaskDivisTemp.getSurveySerno());
                if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && CmisBizConstants.CODE_YXD.equals(lmtSurveyTaskDivisTemp.getPrdId())) {
                    //优享贷 塞 未确认
                    //优享贷暂时用“001”，客户经理确认状态暂时“00”为待确认，”01“为确认
                    lmtSurveyTaskDivisTemp.setMarConfirmStatus(CmisBizConstants.STD_CONFIRM_STATUS_00);
                } else {
                    //塞 无需确认
                    //优享贷暂时用“001”，客户经理确认状态暂时“00”为待确认，”01“为确认 02 无需确认
                    lmtSurveyTaskDivisTemp.setMarConfirmStatus(CmisBizConstants.STD_CONFIRM_STATUS_02);
                }
                if (CmisBizConstants.CODE_YXD.equals(lmtSurveyTaskDivisTemp.getPrdId())) {//优享贷
                    logger.info("优享贷【{}】新客户更新客户基本信息表管户为【{}】", lmtSurveyTaskDivisTemp.getPrdId(), prcIdInfo.getLoginCode());
                    //获取客户基本信息
                    CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(lmtSurveyTaskDivisTemp.getCusId());
                    String managerId = cusBaseDto.getManagerId();
                    if(StringUtils.isBlank(managerId) || "0".equals(managerId)){
                        CusBaseClientDto cusBase = new CusBaseClientDto();
                        cusBase.setCusId(lmtSurveyTaskDivisTemp.getCusId());
                        cusBase.setManagerId(prcIdInfo.getLoginCode());
                        cusBase.setManagerBrId(prcIdInfo.getOrgId());
                        cusBase.setUpdId(prcIdInfo.getLoginCode());
                        cusBase.setUpdBrId(prcIdInfo.getOrgId());
                        ResultDto<Integer> update = iCusClientService.updateCusbase(cusBase);
                        if (Objects.isNull(update.getData())) {
                            logger.info("客户管户信息更新失败！");
                            throw new Exception("客户信息更新失败！");
                        }
                    }
                }
                // 2021年11月17日00:11:47 hubp 问题编号：20211116-00186 优企贷进件客户为虚拟客户，无管护经理，现进行更新
                if ("SC010008".equals(lmtSurveyTaskDivisTemp.getPrdId())) {//优企贷
                    logger.info("优企贷【{}】新客户更新客户基本信息表管户为【{}】", lmtSurveyTaskDivisTemp.getPrdId(), prcIdInfo.getLoginCode());
                    //获取客户基本信息
                    CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(lmtSurveyTaskDivisTemp.getCusId());
                    String managerId = cusBaseDto.getManagerId();
                    if(StringUtils.isBlank(managerId) ||"0".equals(managerId) || "xwd00001".equals(managerId)){
                        CusBaseClientDto cusBase = new CusBaseClientDto();
                        cusBase.setCusId(lmtSurveyTaskDivisTemp.getCusId());
                        cusBase.setManagerId(prcIdInfo.getLoginCode());
                        cusBase.setManagerBrId(prcIdInfo.getOrgId());
                        cusBase.setUpdId(prcIdInfo.getLoginCode());
                        cusBase.setUpdBrId(prcIdInfo.getOrgId());
                        ResultDto<Integer> update = iCusClientService.updateCusbase(cusBase);
                        if (Objects.isNull(update.getData())) {
                            logger.info("客户管户信息更新失败！");
                            throw new Exception("客户信息更新失败！");
                        }
                    }
                }
                // 2021年9月7日09:50:57 hubp 为分配数据添加数据
                lmtSurveyTaskDivisTemp.setManagerId(prcIdInfo.getLoginCode());
                lmtSurveyTaskDivisTemp.setManagerName(prcIdInfo.getUserName());
                ResultDto<String> resultDto = cmisBizXwCommonService.getmanagerAreaName(prcIdInfo.getOrgId());
                lmtSurveyTaskDivisTemp.setManagerArea(resultDto.getData());
                result = this.updateSelective(lmtSurveyTaskDivisTemp);
                logger.info("修改任务结束 修改成功【{}】条", result);
                // 分配完成后处理逻辑

                if (result == 1) {
                    // 2021年9月15日16:06:00 hubp 优农贷不存在主表信息，不予更新
                    // 直接在优农贷名单插入待发起的名单信息
                    if("SC020010".equals(lmtSurveyTaskDivisTemp.getPrdId())){
                        CusLstYndAppDto reqDto = new CusLstYndAppDto();
                        reqDto.setAppDate(stringRedisTemplate.opsForValue().get("openDay"));
                        reqDto.setCertCode(lmtSurveyTaskDivisTemp.getCertCode());
                        reqDto.setCusId(lmtSurveyTaskDivisTemp.getCusId());
                        reqDto.setCusName(lmtSurveyTaskDivisTemp.getCusName());
                        reqDto.setUpdateTime(DateUtils.getCurrDate());
                        reqDto.setApproveStatus("000");
                        reqDto.setCreateTime(DateUtils.getCurrDate());
                        reqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        reqDto.setInputId(prcIdInfo.getLoginCode());
                        reqDto.setHuser(prcIdInfo.getLoginCode());
                        reqDto.setInputBrId(prcIdInfo.getOrgId());
                        reqDto.setHandOrg(prcIdInfo.getOrgId());
                        reqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                        logger.info("插入优农贷待发起名单开始，请求报文：【{}】", JSON.toJSONString(reqDto));
                        ResultDto resultInt = cmisCusClientService.insertYndApp(reqDto);
                        logger.info("插入优农贷待发起名单结束，共插入：【{}】条", result);
                        if(Integer.parseInt(resultInt.getData().toString()) != 1){
                            throw new Exception("插入待发起优农贷申请数据失败！");
                        }
                    }else{
                        // 若产品不需线下调查，直接更新调查主表审批状态为通过
                        // 根据调查编号查询调查主表信息
                        lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyTaskDivisTemp.getSurveySerno());
                        if(Objects.isNull(lmtSurveyReportMainInfo)){
                            throw new Exception("调查主表信息不存在！请核查");
                        }
                        // 更新责任人、责任机构
                        lmtSurveyReportMainInfo.setManagerId(prcIdInfo.getLoginCode());
                        lmtSurveyReportMainInfo.setManagerBrId(prcIdInfo.getOrgId());
                        lmtSurveyReportMainInfo.setDataSource("01");
                        lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                        // 如果为优抵贷,优企贷，优享贷 ，发送消息提醒 2021年9月29日10:37:07 hubp
                        String prdId = lmtSurveyTaskDivisTemp.getPrdId();
                        if("SC020009".equals(prdId) || "SC010008".equals(prdId) || "PW010004".equals(prdId)){
                            this.sendMessage(lmtSurveyTaskDivisTemp);
                        }
                        logger.info("开始修改主表信息{}", result);
                        if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && !CmisBizConstants.CODE_YXD.equals(lmtSurveyTaskDivisTemp.getPrdId())) { // 不需要线下调查
                            logger.info("调查流水号{}不需要线下调查，且不为优享贷，调查任务直接更新为审批通过", lmtSurveyTaskDivisTemp.getSurveySerno());
                            // 更新审批状态
                            lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                        }
                        //TODO 优享贷 需要客户经理确认
                        else if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && CmisBizConstants.CODE_YXD.equals(lmtSurveyTaskDivisTemp.getPrdId())) {
                            logger.info("调查流水号{}不需要线下调查，且为优享贷，调查任务等待客户经理确认", lmtSurveyTaskDivisTemp.getSurveySerno());
                            lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);
                            return new ResultDto(0).message("操作成功,该产品为优享贷,需客户经理确认");

                        } else if ("1".equals(lmtSurveyTaskDivisTemp.getIsStopOffline())) {
                            logger.info("调查流水号{}需要线下调查，调查任务为待发起，数据来源为系统推送", lmtSurveyTaskDivisTemp.getSurveySerno());
                            lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        }
                        lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);

                    }
                } else {
                    logger.info("调查流水号{}调查任务分配更新失败", lmtSurveyTaskDivisTemp.getSurveySerno());
                    throw new YuspException(EpbEnum.EPB090001.key, EpbEnum.EPB090001.value);
                }
                logger.info("任务分配编号【{}】，第一次手工分配结束.....................", lmtSurveyTaskDivis.getPkId());
            } else { // 重新分配逻辑
                logger.info("任务分配编号【{}】，重新分配开始.....................", lmtSurveyTaskDivis.getPkId());
                //二次 多次分配 先去判断是否存在旗下存在待审批的单子
                //（3）拦截规则：任务项下存在流程中或审批通过的授信数据：
                if("SC020010".equals(lmtSurveyTaskDivisTemp.getPrdId())){
                    // 如果为优农贷
                    IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySurveyNo(lmtSurveyTaskDivisTemp.getSurveySerno());
                    if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) &&
                            (CmisCommonConstants.WF_STATUS_992.equals(iqpLoanApp.getApproveStatus()) ||
                                    CmisCommonConstants.WF_STATUS_111.equals(iqpLoanApp.getApproveStatus()))) {
                        return new ResultDto(9999).message("该任务项下存在在途的合同申请，请先结束流程");
                    }
                    if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && CmisCommonConstants.WF_STATUS_997.equals(iqpLoanApp.getApproveStatus())) {
                        return new ResultDto(9999).message("该任务项下存在审批通过的合同申请，请走客户移交功能");
                    }
                    lmtSurveyTaskDivisTemp.setDivisStatus(LmtSurveyEnums.DIVIS_STATUS_110.getValue());
                    // 处理人更新为选择人
                    lmtSurveyTaskDivisTemp.setPrcId(lmtSurveyTaskDivis.getPrcId());
                    // 分配时间
                    lmtSurveyTaskDivisTemp.setDivisTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtSurveyTaskDivisTemp.setUpdateTime(DateUtils.getCurrDate());
                    this.updateSelective(lmtSurveyTaskDivisTemp);
                }else if ("PW010004".equals(lmtSurveyTaskDivisTemp.getPrdId())) {
                    // 如果是优享贷就就查合同表
                    lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyTaskDivisTemp.getSurveySerno());
                    String apprStatus = lmtSurveyReportMainInfo.getApproveStatus();
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("surveySerno", lmtSurveyTaskDivisTemp.getSurveySerno());
                    List<CtrLoanCont> ctrLoanContList = ctrLoanContService.selectByModel(queryModel);
                    if (ctrLoanContList.size() == 0) {
                        if (CmisCommonConstants.WF_STATUS_992.equals(apprStatus) || CmisCommonConstants.WF_STATUS_111.equals(apprStatus)) {
                            // 若存在“审批状态=退回、审批中”的授信调查，系统提示“该任务项下已存在退回/审批中的授信调查，请先结束流程”；
                            return new ResultDto(9999).message("该任务项下已存在退回/审批中的授信调查，请先结束流程");
                        }
                        if (CmisCommonConstants.WF_STATUS_997.equals(apprStatus) && "1".equals(lmtSurveyTaskDivisTemp.getIsStopOffline())) {
                            // 该任务项下已存在审批通过的授信调查，请走客户移交功能
                            return new ResultDto(9999).message("该任务项下已存在审批通过的授信调查，请走客户移交功能");
                        }
                        // 更新分配任务信息
                        // 状态更新为已分配
                        lmtSurveyTaskDivisTemp.setDivisStatus(LmtSurveyEnums.DIVIS_STATUS_110.getValue());
                        // 处理人更新为选择人
                        lmtSurveyTaskDivisTemp.setPrcId(lmtSurveyTaskDivis.getPrcId());
                        // 分配时间
                        lmtSurveyTaskDivisTemp.setDivisTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtSurveyTaskDivisTemp.setUpdateTime(DateUtils.getCurrDate());
                        //分配人=>操作人
                        User user = SessionUtils.getUserInformation();
                        if (user != null) {
                            lmtSurveyTaskDivisTemp.setDivisId(user.getLoginCode());
                        }
                        result = this.updateSelective(lmtSurveyTaskDivisTemp);

                        // 更新责任人、责任机构
                        lmtSurveyReportMainInfo.setManagerId(prcIdInfo.getLoginCode());
                        lmtSurveyReportMainInfo.setManagerBrId(prcIdInfo.getOrgId());
                        lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                        lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);
                        // 如果为优抵贷,优企贷，优享贷 ，发送消息提醒 2021年9月29日10:37:07 hubp
                        String prdId = lmtSurveyTaskDivisTemp.getPrdId();
                        if("SC020009".equals(prdId) || "SC010008".equals(prdId) || "PW010004".equals(prdId)){
                            this.sendMessage(lmtSurveyTaskDivisTemp);
                        }
                        return new ResultDto(1).message("分配成功");
                    } else {
                        for (CtrLoanCont ctrLoanCont : ctrLoanContList) {
                            if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && (CmisCommonConstants.CONT_STATUS_100.equals(ctrLoanCont.getContStatus()))) {
                                return new ResultDto(9999).message("该任务项下存在未生效的合同，请先结束流程");
                            }
                            if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && CmisCommonConstants.CONT_STATUS_200.equals(ctrLoanCont.getContStatus())) {
                                return new ResultDto(9999).message("该任务项下存在生效的合同，请走客户移交功能");
                            }
                        }
                    }
                } else {
                    // 不为优享贷查业务申请表
                    lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(lmtSurveyTaskDivisTemp.getSurveySerno());
                    String apprStatus = lmtSurveyReportMainInfo.getApproveStatus();
                    IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySurveyNo(lmtSurveyTaskDivisTemp.getSurveySerno());
                    if (iqpLoanApp == null) { //若无合同信息
                        if (CmisCommonConstants.WF_STATUS_992.equals(apprStatus) || CmisCommonConstants.WF_STATUS_111.equals(apprStatus)) {
                            // 若存在“审批状态=退回、审批中”的授信调查，系统提示“该任务项下已存在退回/审批中的授信调查，请先结束流程”；
                            return new ResultDto(9999).message("该任务项下已存在退回/审批中的授信调查，请先结束流程");
                        }
                        if (CmisCommonConstants.WF_STATUS_997.equals(apprStatus) && "1".equals(lmtSurveyTaskDivisTemp.getIsStopOffline())) {
                            // 该任务项下已存在审批通过的授信调查，请走客户移交功能
                            return new ResultDto(9999).message("该任务项下已存在审批通过的授信调查，请走客户移交功能");
                        }
                        // 更新分配任务信息
                        // 状态更新为已分配
                        lmtSurveyTaskDivisTemp.setDivisStatus(LmtSurveyEnums.DIVIS_STATUS_110.getValue());
                        // 处理人更新为选择人
                        lmtSurveyTaskDivisTemp.setPrcId(lmtSurveyTaskDivis.getPrcId());
                        // 分配时间
                        lmtSurveyTaskDivisTemp.setDivisTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtSurveyTaskDivisTemp.setUpdateTime(DateUtils.getCurrDate());
                        //分配人=>操作人
                        User user = SessionUtils.getUserInformation();
                        if (user != null) {
                            lmtSurveyTaskDivisTemp.setDivisId(user.getLoginCode());
                        }
                        result = this.updateSelective(lmtSurveyTaskDivisTemp);

                        // 更新责任人、责任机构
                        lmtSurveyReportMainInfo.setManagerId(prcIdInfo.getLoginCode());
                        lmtSurveyReportMainInfo.setManagerBrId(prcIdInfo.getOrgId());
                        lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                        lmtSurveyReportMainInfoService.update(lmtSurveyReportMainInfo);
                        // 如果为优抵贷,优企贷，优享贷 ，发送消息提醒 2021年9月29日10:37:07 hubp
                        String prdId = lmtSurveyTaskDivisTemp.getPrdId();
                        if("SC020009".equals(prdId) || "SC010008".equals(prdId) || "PW010004".equals(prdId)){
                            this.sendMessage(lmtSurveyTaskDivisTemp);
                        }
                        return new ResultDto(1).message("分配成功");
                    } else { // 若有合同信息
                        if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) &&
                                (CmisCommonConstants.WF_STATUS_992.equals(iqpLoanApp.getApproveStatus()) ||
                                        CmisCommonConstants.WF_STATUS_111.equals(iqpLoanApp.getApproveStatus()))) {
                            return new ResultDto(9999).message("该任务项下存在在途的合同申请，请先结束流程");
                        }
                        if ("0".equals(lmtSurveyTaskDivisTemp.getIsStopOffline()) && CmisCommonConstants.WF_STATUS_997.equals(iqpLoanApp.getApproveStatus())) {
                            return new ResultDto(9999).message("该任务项下存在审批通过的合同申请，请走客户移交功能");
                        }
                    }
                }
                //查询合同申请信息 iqpLoanAppService

                logger.info("任务分配编号【{}】，重新分配结束.....................", lmtSurveyTaskDivis.getPkId());
            }

        } catch (YuspException e) {
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("任务分配编号【{}】，进入手工分配处理逻辑结束！.....................", lmtSurveyTaskDivis.getPkId());
        }
        return new ResultDto(result);
    }


    /**
     * @param lmtSurveyTaskDivis
     * @return void
     * @author hubp
     * @date 2021/9/29 10:18
     * @version 1.0.0
     * @desc  消息提醒
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendMessage(LmtSurveyTaskDivis lmtSurveyTaskDivis) {
        String surveySerno = lmtSurveyTaskDivis.getSurveySerno();
        logger.info("*************小微调查分配任务短信通知开始，调查流水号：【{}】",surveySerno);
        String managerId = lmtSurveyTaskDivis.getPrcId();
        String mgrTel = StringUtils.EMPTY;
        String prdId = StringUtils.EMPTY; // 产品编号
        String messageType = StringUtils.EMPTY; // 短信编号
        String receivedUserType = StringUtils.EMPTY; //接收人员类型
        try {
            prdId = lmtSurveyTaskDivis.getPrdId();
            logger.info("***********【{}】调用AdminSmUserService用户信息查询服务开始*START**************" , prdId);
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            /** 优企贷-SC010008 ；优抵贷-SC020009 ；优享贷-PW010004 */
            if("SC010008".equals(prdId)){
                messageType = "MSG_XW_M_0003";// 优企贷
                receivedUserType = "1";//1--客户经理 2--借款人
            } else if ("SC020009".equals(prdId)) {
                messageType = "MSG_XW_M_0011";// 优抵贷
                receivedUserType = "1";// 1--客户经理 2--借款人
            } else if ("PW010004".equals(prdId)) {
                messageType = "MSG_XW_M_0014";// 优享贷
                receivedUserType = "1";// 1--客户经理 2--借款人
            }
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName",lmtSurveyTaskDivis.getCusName());
            paramMap.put("prdName", lmtSurveyTaskDivis.getPrdName());
            //执行发送借款人操作
            logger.info("*************小微调查分配任务短信通知调用开始，调查流水号：【{}】，发送报文：【{}】",surveySerno,JSON.toJSONString(paramMap));
            ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
            logger.info("*************小微调查分配任务短信通知调用结束，调查流水号：【{}】，返回报文：【{}】",surveySerno,JSON.toJSONString(resultMessageDto));
        } catch (Exception e) {
            logger.error("*************小微调查分配任务短信通知异常，调查流水号：【{}】，异常信息：【{}】",surveySerno,JSON.toJSONString(e));
        }
        logger.info("*************小微调查分配任务短信通知结束，调查流水号：【{}】",surveySerno);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-28 19:10
     * @注释 分页查询参数自建
     */
    public List<LmtSurveyTaskDivis> findlistbymodel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisMapper.findlistbymodel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.domain.LmtSurveyTaskDivis
     * @author hubp
     * @date 2021/5/20 11:14
     * @version 1.0.0
     * @desc 根据调查流水号查看分配表信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtSurveyTaskDivis selectBySurveySerno(String surveySerno) {
        return lmtSurveyTaskDivisMapper.selectBySurveySerno(surveySerno);
    }

    /**
     * @param model
     * @return java.util.List<cn.com.yusys.yusp.domain.LmtSurveyTaskDivis>
     * @author hubp
     * @date 2021/6/16 11:01
     * @version 1.0.0
     * @desc 优享贷客户经理确认查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto findlistByYxd(QueryModel model) {
        Object prcId = model.getCondition().get("prcId");
        if (null == prcId || "".equals((String) prcId)) {
            return new ResultDto(null).code(9999).message("处理人工号不可为空！");
        }
        logger.info("优享贷客户经理确认查询开始！————————————当前登录人：{}", prcId);
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("prdId", CmisBizConstants.CODE_YXD); // 优享贷产品编号
        model.getCondition().put("marConfirmStatus", CmisBizConstants.STD_CONFIRM_STATUS_00); // 客户经理确认状态
        List<LmtSurveyTaskDivis> list = lmtSurveyTaskDivisMapper.findlistbymodel(model);
        PageHelper.clearPage();
        logger.info("优享贷客户经理确认查询结束！————————————当前登录人：{}", prcId);
        return new ResultDto(list).code(0);
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/16 11:22
     * @version 1.0.0
     * @desc 优享贷客户经理确认
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto marConfirm(String surveySerno) {
        //通过调查编号开始查询分配信息
        logger.info("***************优享贷客户经理确认开始***************{}", surveySerno);
        int result = 0;
        String mobileNo = StringUtils.EMPTY;
        try {
            if (StringUtils.isBlank(surveySerno)) {
                return new ResultDto(null).code(0).message("调查流水号为空");
            }
            LmtSurveyTaskDivis lmtSurveyTaskDivis = lmtSurveyTaskDivisMapper.selectBySurveySerno(surveySerno);
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(surveySerno);
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(surveySerno);
            // 2021年11月4日15:40:19 hubp BUG15818 更新basic表管护信息，更新联系方式
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
            CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(lmtSurveyTaskDivis.getCusId());
            if(Objects.nonNull(cusIndivContactDto)){
                mobileNo = cusIndivContactDto.getMobileNo();
            }
            // 调查分配表更改为已确认
            if (null != lmtSurveyTaskDivis) {
                lmtSurveyTaskDivis.setMarConfirmStatus(CmisBizConstants.STD_CONFIRM_STATUS_01);
                lmtSurveyTaskDivis.setPhone(mobileNo);
                result = lmtSurveyTaskDivisMapper.updateByPrimaryKeySelective(lmtSurveyTaskDivis);
            } else {
                throw new YuspException("9999", "调查分配表数据查询为空");
            }
            if (result != 1) {
                throw new YuspException("9999", "数据操作异常");
            }
            // 调查主表更新为已通过
            if (null != lmtSurveyReportMainInfo) {
                lmtSurveyReportMainInfo.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_997);
                result = lmtSurveyReportMainInfoService.updateSelective(lmtSurveyReportMainInfo);
            } else {
                throw new YuspException("9999", "调查主表数据查询为空");
            }
            if (result != 1) {
                throw new YuspException("9999", "数据操作异常");
            }
            // 获取批复信息，2021年9月10日10:09:39 hubp 将批复状态更新为通过 "key":"01","value":"生效"},{"key":"02","value":"失效"},{"key":"03","value":"重新办理"
            if (null != lmtCrdReplyInfo) {
                lmtCrdReplyInfo.setReplyStatus("01");
                lmtCrdReplyInfo.setManagerId(lmtSurveyReportMainInfo.getManagerId());
                lmtCrdReplyInfo.setManagerBrId(lmtSurveyReportMainInfo.getManagerBrId());
                result = lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
            } else {
                throw new YuspException("9999", "调查批复表数据查询为空");
            }
            if (result != 1) {
                throw new YuspException("9999", "数据操作异常");
            }
            if (Objects.nonNull(lmtSurveyReportBasicInfo)) {
                lmtSurveyReportBasicInfo.setManagerId(lmtSurveyReportMainInfo.getManagerId());
                lmtSurveyReportBasicInfo.setManagerBrId(lmtSurveyReportMainInfo.getManagerBrId());
                lmtSurveyReportBasicInfo.setPhone(mobileNo);
                result = lmtSurveyReportBasicInfoService.updateSelective(lmtSurveyReportBasicInfo);
            } else {
                throw new YuspException("9999", "调查基础信息表查询为空");
            }
            if (result != 1) {
                throw new YuspException("9999", "数据操作异常");
            }

            ResultDto resultDto = lmtSurveyReportMainInfoService.handleBizEnd(lmtCrdReplyInfo);
            if (resultDto.getCode().equals("9999")) {
                throw new YuspException("9999", "数据操作异常");
            } else {
                Fkyx01ReqDto fkyx01ReqDto = new Fkyx01ReqDto();
                logger.info("***************查询客户经理信息*******【{}】********", lmtSurveyReportMainInfo.getManagerId());
                AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(lmtSurveyReportMainInfo.getManagerId());
                logger.info("***************查询客户经理信息结果*******【{}】********", adminSmUserDto);
                fkyx01ReqDto.setApp_no(lmtSurveyReportMainInfo.getSurveySerno()); // 批复编号
                fkyx01ReqDto.setManager_id(adminSmUserDto.getLoginCode());
                fkyx01ReqDto.setManager_name(adminSmUserDto.getUserName());
                fkyx01ReqDto.setOrg_id(adminSmUserDto.getOrgId());
                logger.info("***************查询机构名称*******【{}】********", adminSmUserDto.getOrgId());
                String orgName = OcaTranslatorUtils.getOrgName(adminSmUserDto.getOrgId());
                logger.info("***************查询机构名称结果*******【{}】********", orgName);
                fkyx01ReqDto.setOrg_name(orgName);
                String phone = adminSmUserDto.getUserMobilephone();
                fkyx01ReqDto.setManager_phone(phone);
                logger.info("***************调用fkyx01*******【{}】********", fkyx01ReqDto);
                Fkyx01RespDto fkyx01RespDto = fkyx01Service.fkyx01(fkyx01ReqDto);
                logger.info("***************调用fkyx01结束*******【{}】********", fkyx01RespDto);
                logger.info("***************调用znsp04*******【{}】********", lmtCrdReplyInfo);
                Znsp04RespDto znsp04RespDto = lmtSurveyReportMainInfoService.znsp04Yxd(lmtCrdReplyInfo);
                logger.info("***************调用znsp04结束*******【{}】********", znsp04RespDto);
            }
            sendMessageYxd(lmtSurveyTaskDivis);
        } catch (Exception e) {
            logger.info("***************优享贷客户经理确认异常***************", e);
            throw new YuspException("9999", e.getMessage());
        }
        return new ResultDto(result).code(0);
    }

    /**
     * @param lmtSurveyTaskDivis
     * @return void
     * @author hubp
     * @date 2021/10/22 22:41
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendMessageYxd(LmtSurveyTaskDivis lmtSurveyTaskDivis) {
        logger.info("*************优享贷确认任务短信通知开始，流水号：【{}】", lmtSurveyTaskDivis.getSurveySerno());
        String surveySerno = lmtSurveyTaskDivis.getSurveySerno();
        String managerId = lmtSurveyTaskDivis.getPrcId();
        String mgrTel = lmtSurveyTaskDivis.getPhone();
        // 短信发送对象
        try {
            String messageTypeC = "MSG_XW_C_0014";// 优享贷
            String receivedUserTypeC = "2";// 1--客户经理 2--借款人
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", lmtSurveyTaskDivis.getCusName());
            paramMap.put("prdName", lmtSurveyTaskDivis.getPrdName());
            //执行发送借款人操作
            logger.info("*************优享贷确认任务短信通知客户经理调用开始，调查流水号：【{}】，发送报文：【{}】",surveySerno, JSON.toJSONString(paramMap));
            ResultDto<Integer> resultMessageDtoC = messageCommonService.sendMessage(messageTypeC, paramMap, receivedUserTypeC, managerId, mgrTel);
            logger.info("*************优享贷确认任务短信通知客户经理调用结束，调查流水号：【{}】，返回报文：【{}】",surveySerno,JSON.toJSONString(resultMessageDtoC));
        } catch (Exception e) {
            logger.error("*************优享贷确认任务短信通知异常，调查流水号：【{}】，异常信息：【{}】", surveySerno, JSON.toJSONString(e));
        }
        logger.info("*************小优享贷确认任务短信通知结束，调查流水号：【{}】", surveySerno);
    }
}
