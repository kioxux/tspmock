package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCoopApp
 * @类描述: lmt_coop_app数据实体类
 * @功能描述: 
 * @创建人: liqichao
 * @创建时间: 2021-01-12 19:42:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCoopAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 原业务流水号 **/
	private String oldSerno;

	/** 协议名称 **/
	private String lmtCtrName;
	
	/** 合作方类型 STD_ZB_COOP_TYP **/
	private String coopType;
	
	/** 合作方客户编号 **/
	private String coopCusId;
	
	/** 合作方客户名称 **/
	private String coopCusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 授信额度 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 单户限额 **/
	private java.math.BigDecimal sigAmt;
	
	/** 授信期限类型 STD_ZB_TERM_TYP **/
	private String lmtTermType;
	
	/** 授信期限 **/
	private String lmtTerm;
	
	/** 共享范围 STD_ZB_SHR_SCOPE **/
	private String sharedScope;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改时间 **/
	private String updDate;
	
	/** 操作类型 STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno == null ? null : oldSerno.trim();
	}
	
    /**
     * @return OldSerno
     */	
	public String getOldSerno() {
		return this.oldSerno;
	}

	/**
	 * @param lmtCtrName
	 */
	public void setLmtCtrName(String lmtCtrName) {
		this.lmtCtrName = lmtCtrName == null ? null : lmtCtrName.trim();
	}

	/**
	 * @return lmtCtrName
	 */
	public String getLmtCtrName() {
		return this.lmtCtrName;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType == null ? null : coopType.trim();
	}
	
    /**
     * @return CoopType
     */	
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId == null ? null : coopCusId.trim();
	}
	
    /**
     * @return CoopCusId
     */	
	public String getCoopCusId() {
		return this.coopCusId;
	}
	
	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName == null ? null : coopCusName.trim();
	}
	
    /**
     * @return CoopCusName
     */	
	public String getCoopCusName() {
		return this.coopCusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return SigAmt
     */	
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType == null ? null : lmtTermType.trim();
	}
	
    /**
     * @return LmtTermType
     */	
	public String getLmtTermType() {
		return this.lmtTermType;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(String lmtTerm) {
		this.lmtTerm = lmtTerm == null ? null : lmtTerm.trim();
	}
	
    /**
     * @return LmtTerm
     */	
	public String getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param sharedScope
	 */
	public void setSharedScope(String sharedScope) {
		this.sharedScope = sharedScope == null ? null : sharedScope.trim();
	}
	
    /**
     * @return SharedScope
     */	
	public String getSharedScope() {
		return this.sharedScope;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}