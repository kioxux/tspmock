package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class LmtReplyAccSubPrdDto {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 分项额度品种编号 **/
    private String accSubPrdNo;

    /** 分项额度号 **/
    private String accSubNo;

    /** 批复分项流水号 **/
    private String replySubSerno;

    /** 授信批复分项品种流水号 **/
    private String replySubPrdSerno;

    /** 分项流水号 **/
    private String subSerno;

    /** 分项品种流水号 **/
    private String subPrdSerno;

    /** 客户编号 **/
    private String cusId;

    /** 客户类型 **/
    private String cusType;

    /** 客户名称 **/
    private String cusName;

    /** 授信品种编号 **/
    private String lmtBizType;

    /** 授信品种名称 **/
    private String lmtBizTypeName;

    /** 授信品种类型属性 **/
    private String lmtBizTypeProp;

    /** 是否循环额度 **/
    private String isRevolvLimit;

    /** 担保方式 **/
    private String guarMode;

    /** 币种 **/
    private String curType;

    /** 授信额度 **/
    private java.math.BigDecimal lmtAmt;

    /** 起始日期 **/
    private String startDate;

    /** 到期日期 **/
    private String endDate;

    /** 额度期限 **/
    private Integer lmtTerm;

    /** 保证金预留比例 **/
    private java.math.BigDecimal bailPreRate;

    /** 授信宽限期 **/
    private Integer lmtGraperTerm;

    /** 年利率 **/
    private java.math.BigDecimal rateYear;

    /** 还款方式 **/
    private String repayMode;

    /** 结息方式 **/
    private String eiMode;

    /** 是否借新还旧 **/
    private String isRefinance;

    /** 是否无还本续贷 **/
    private String isRwrop;

    /** 手续费率 **/
    private java.math.BigDecimal chrgRate;

    /** 手续费收取方式 **/
    private String chrgCollectMode;

    /** 委托人类型 **/
    private String consignorType;

    /** 委托人客户编号 **/
    private String consignorCusId;

    /** 委托人客户名称 **/
    private String consignorCusName;

    /** 委托人证件号码 **/
    private String consignorCertCode;

    /** 委托人证件类型 **/
    private String consignorCertType;

    /** 是否预授信额度 **/
    private String isPreLmt;

    /** 台账状态 **/
    private String accStatus;

    /** 还款计划描述 **/
    private String repayPlanDesc;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private Date createTime;

    /** 修改时间 **/
    private Date updateTime;

    /**
     * 页面用的授信分项额度编号
     **/
    private String lmtDrawNo;

    /**
     * 页面用的授信品种
     **/
    private String lmtDrawType;

    public String getLmtDrawNo() {
        return lmtDrawNo;
    }

    public void setLmtDrawNo(String lmtDrawNo) {
        this.lmtDrawNo = lmtDrawNo;
    }

    public String getLmtDrawType() {
        return lmtDrawType;
    }

    public void setLmtDrawType(String lmtDrawType) {
        this.lmtDrawType = lmtDrawType;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getAccSubPrdNo() {
        return accSubPrdNo;
    }

    public void setAccSubPrdNo(String accSubPrdNo) {
        this.accSubPrdNo = accSubPrdNo;
    }

    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getReplySubSerno() {
        return replySubSerno;
    }

    public void setReplySubSerno(String replySubSerno) {
        this.replySubSerno = replySubSerno;
    }

    public String getReplySubPrdSerno() {
        return replySubPrdSerno;
    }

    public void setReplySubPrdSerno(String replySubPrdSerno) {
        this.replySubPrdSerno = replySubPrdSerno;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public String getSubPrdSerno() {
        return subPrdSerno;
    }

    public void setSubPrdSerno(String subPrdSerno) {
        this.subPrdSerno = subPrdSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLmtBizType() {
        return lmtBizType;
    }

    public void setLmtBizType(String lmtBizType) {
        this.lmtBizType = lmtBizType;
    }

    public String getLmtBizTypeName() {
        return lmtBizTypeName;
    }

    public void setLmtBizTypeName(String lmtBizTypeName) {
        this.lmtBizTypeName = lmtBizTypeName;
    }

    public String getLmtBizTypeProp() {
        return lmtBizTypeProp;
    }

    public void setLmtBizTypeProp(String lmtBizTypeProp) {
        this.lmtBizTypeProp = lmtBizTypeProp;
    }

    public String getIsRevolvLimit() {
        return isRevolvLimit;
    }

    public void setIsRevolvLimit(String isRevolvLimit) {
        this.isRevolvLimit = isRevolvLimit;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public Integer getLmtGraperTerm() {
        return lmtGraperTerm;
    }

    public void setLmtGraperTerm(Integer lmtGraperTerm) {
        this.lmtGraperTerm = lmtGraperTerm;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getIsRefinance() {
        return isRefinance;
    }

    public void setIsRefinance(String isRefinance) {
        this.isRefinance = isRefinance;
    }

    public String getIsRwrop() {
        return isRwrop;
    }

    public void setIsRwrop(String isRwrop) {
        this.isRwrop = isRwrop;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public String getChrgCollectMode() {
        return chrgCollectMode;
    }

    public void setChrgCollectMode(String chrgCollectMode) {
        this.chrgCollectMode = chrgCollectMode;
    }

    public String getConsignorType() {
        return consignorType;
    }

    public void setConsignorType(String consignorType) {
        this.consignorType = consignorType;
    }

    public String getConsignorCusId() {
        return consignorCusId;
    }

    public void setConsignorCusId(String consignorCusId) {
        this.consignorCusId = consignorCusId;
    }

    public String getConsignorCusName() {
        return consignorCusName;
    }

    public void setConsignorCusName(String consignorCusName) {
        this.consignorCusName = consignorCusName;
    }

    public String getConsignorCertCode() {
        return consignorCertCode;
    }

    public void setConsignorCertCode(String consignorCertCode) {
        this.consignorCertCode = consignorCertCode;
    }

    public String getConsignorCertType() {
        return consignorCertType;
    }

    public void setConsignorCertType(String consignorCertType) {
        this.consignorCertType = consignorCertType;
    }

    public String getIsPreLmt() {
        return isPreLmt;
    }

    public void setIsPreLmt(String isPreLmt) {
        this.isPreLmt = isPreLmt;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getRepayPlanDesc() {
        return repayPlanDesc;
    }

    public void setRepayPlanDesc(String repayPlanDesc) {
        this.repayPlanDesc = repayPlanDesc;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
