package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.BailDepositInfo;
import cn.com.yusys.yusp.domain.CtrAccpCont;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccAccpMapper;
import cn.com.yusys.yusp.repository.mapper.BailDepositInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: BailDepositInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-06-09 21:06:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BailDepositInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BailDepositInfoService.class);

    @Autowired
    private BailDepositInfoMapper bailDepositInfoMapper;

    @Autowired
    private AccAccpMapper accAccpMapper;

    @Autowired
    private PvpAccpAppMapper pvpAccpAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BailDepositInfo selectByPrimaryKey(String serno) {
        return bailDepositInfoMapper.selectByPrimaryKey(serno);
    }

    //得到合同可出账金额
    public double difAmt(String cont_no) throws Exception {
        double difAmt = 0;
        Map map = new HashMap<>();
        //授信可出账金额
        BigDecimal sxkczAmt = BigDecimal.ZERO;
        try {
            String sql = "select cont_amt,limit_acc_no,is_util_lmt,manager_br_id from  Ctr_Accp_Cont where cont_no='" + cont_no + "'" +
                    " and cont_state in('200','800')";
            CtrAccpCont ctrAccpCont = bailDepositInfoMapper.selectSxedInfo(cont_no);
            //合同金额
            double applyAmount = ctrAccpCont.getContAmt().doubleValue();
            //授信台帐账号
            String item_id = ctrAccpCont.getLmtAccNo();
            // 是否使用授信额度
            String is_util_lmt = ctrAccpCont.getIsUtilLmt();
            //主管机构
            String manager_br_id = ctrAccpCont.getManagerBrId();

            if (!"1".equals(is_util_lmt)) {//存在授信,拿到授信可出账金额
                CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
                cmisLmt0026ReqDto.setInstuCde(manager_br_id);
                cmisLmt0026ReqDto.setSubSerno(item_id);
                cmisLmt0026ReqDto.setQueryType("01");
                ResultDto<CmisLmt0026RespDto> cmisLmt0026ResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);

                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0026ResultDto.getCode())) {
                    sxkczAmt = cmisLmt0026ResultDto.getData().getValPvpAmt();
                }
            }
            double pvpAmt = 0;//敞口额度
            //【台账中已出账票面金额（本合同下所有借据余额包括纸、电票）-台账中的保证金金额】
            List<AccAccp> list1 = accAccpMapper.getpvpAmtInfo(cont_no);

            for (int i = 0; i < list1.size(); i++) {
                String loanTotlAmount = list1.get(i).getDrftTotalAmt().toString();//借据金额
                String securityTotlAmount = list1.get(i).getBailAmt().toString();//保证金金额
                Double loanBalance = Double.parseDouble(loanTotlAmount) - Double.parseDouble(securityTotlAmount);
                pvpAmt = pvpAmt + loanBalance;
            }

            //【台账中已出账批次在保证金登记表中新的补交保证金记录】
            Date curDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strCurDate = sdf.format(curDate);
            map.put("contno", cont_no);
            map.put("strCurDate", strCurDate);
            List<BailDepositInfo> list2 = bailDepositInfoMapper.selectNewBailamt(map);

            for (int n = 0; n < list2.size(); n++) {
                double othAmt3 = list2.get(n).getBailAmt().doubleValue();
                pvpAmt = pvpAmt - othAmt3;
            }

            //【查询在途状态账票面金额-sum（相应批次的保证金余额）】
            //String sql4 = "select yp_flag,apply_amount,security_money_amt from pvp_accp_app where cont_no='" + cont_no + "' and approve_status in('000','001','111')";
            List<PvpAccpApp> list3 = pvpAccpAppMapper.selectPmAmtInfo(cont_no);

            for (int i = 0; i < list3.size(); i++) {
                String yp_flag = list3.get(i).getIsEDrft();//01纸票 02电票
                double othAmt1 = convertValue(list3.get(i).getAppAmt());
                double othAmt2 = 0;
                othAmt2 = convertValue(list3.get(i).getBailAmt());
                double othAmt = othAmt1 - othAmt2;
                pvpAmt = pvpAmt + othAmt;
            }

            //当日出账申请通过，未生成台账【当日已出账票面金额-sum（相应批次的保证金余额）】
            //select p.PK_ID,p.IS_E_DRFT,p.APP_AMT,p.BAIL_AMT,a.CORE_TRAN_SERNO
            //from pvp_accp_app p left join pvp_authorize a on p.PK_ID=a.TRAN_SERNO
            //where p.cont_no='cont_no'and p.approve_status='997'
            //and a.CORE_TRAN_SERNO not in
            //(select distinct CORE_BILL_NO from acc_accp where cont_no='cont_no');
            List<PvpAccpApp> list4 = pvpAccpAppMapper.seletWsctzInfo(cont_no);
            for (int i = 0; i < list4.size(); i++) {

                String pc_serno = list4.get(i).getPvpAuthorize().getContNo(); //电票批次号
                String yp_flag = list4.get(i).getIsEDrft();//01纸票 02电票
                double othAmt1 = convertValue(list4.get(i).getAppAmt());//票面金额
                double security_money_amt = convertValue(list4.get(i).getBailAmt());//保证金申请金额
                double othAmt2 = 0;

                String sum = bailDepositInfoMapper.selectSumAmt(cont_no);
                double othAmt4 = convertValue(sum);
                //othAmt2 = othAmt4;
                othAmt2 = security_money_amt + othAmt4;
                double othAmt = othAmt1 - othAmt2;
                pvpAmt = pvpAmt + othAmt;
            }
            //合同可出账金额
            //BigDecimal difAmt = applyAmount.subtract(pvpAmt);
            difAmt = applyAmount - pvpAmt;

            if (difAmt - sxkczAmt.doubleValue() > 0) {
                difAmt = sxkczAmt.doubleValue();
            }
            DecimalFormat df = new DecimalFormat("0.00");
            df.setRoundingMode(RoundingMode.HALF_UP);
            difAmt = Double.parseDouble(df.format(difAmt));
            return difAmt;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {

        }
        return 0;
    }

    private static double convertValue(Object value) {
        String strValue = StringUtils.isEmpty((String) value) ? 0 + "" : String.valueOf(value);
        double convertedValue = Double.parseDouble(strValue);
        return convertedValue;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(BailDepositInfo record) {
        return bailDepositInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(BailDepositInfo record) {
        return bailDepositInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(BailDepositInfo record) {
        return bailDepositInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return bailDepositInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return bailDepositInfoMapper.deleteByIds(ids);
    }

    /**
     * 有效台账下当天补交保证金总额
     *
     * @param bdiMap
     * @return
     */
    public BigDecimal queryDtbjBailAmtByBdiMap(Map bdiMap) {
        return bailDepositInfoMapper.queryDtbjBailAmtByBdiMap(bdiMap);
    }

    /**
     * 已缴存保证金总额
     *
     * @param bdiMap
     * @return
     */
    public BigDecimal queryYjcBailAmtByBdiMap(Map bdiMap) {
        return bailDepositInfoMapper.queryYjcBailAmtByBdiMap(bdiMap);
    }
}
