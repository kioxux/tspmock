package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetFinan
 * @类描述: iqp_dis_asset_finan数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDisAssetFinanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 金融资产表主键 **/
	private String finanPk;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	private String apptCode;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 金融产品类型 STD_ZB_FIN_PRD_TYP **/
	private String finPrdType;
	
	/** 金融产品名称 **/
	private String finPrdName;
	
	/** 是否满足起息日在三个月以上  STD_ZB_YES_NO **/
	private String depositType;
	
	/** 时点余额 **/
	private java.math.BigDecimal timePointBalance;
	
	/** 活期存款（含第三方存管证券保证金）近3个月日均余额 **/
	private java.math.BigDecimal averDailyBalance;
	
	/** 发行方 STD_ZB_LSSUER_TYPE **/
	private String lssuerType;
	
	/** 风险等级 STD_ZB_RISK_LEVEL **/
	private String riskLevel;
	
	/** 认购金额 **/
	private java.math.BigDecimal bayAmount;
	
	/** 是否保本 STD_ZB_YES_NO **/
	private String capitalType;
	
	/** 基金类型 STD_ZB_FUND_TYPE **/
	private String fundType;
	
	/** 近1月以内某时点的基金市值 **/
	private java.math.BigDecimal marketValue;
	
	/** 股票属性 STD_ZB_SHARES_TYPE **/
	private String sharesType;
	
	/** 近1个月内某时点的股票账户市值 **/
	private java.math.BigDecimal sharesAccountValue;
	
	/** 近1个月内某时点或近一个财务年度的每股净资产 **/
	private java.math.BigDecimal netAssets;
	
	/** 股份数 **/
	private String sharesNumber;
	
	/** 其他金融资产（折算后）金额 **/
	private java.math.BigDecimal otherFinanAsset;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 客户经理认定收入金额 **/
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	private java.math.BigDecimal headExIncome;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param finanPk
	 */
	public void setFinanPk(String finanPk) {
		this.finanPk = finanPk == null ? null : finanPk.trim();
	}
	
    /**
     * @return FinanPk
     */	
	public String getFinanPk() {
		return this.finanPk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param finPrdType
	 */
	public void setFinPrdType(String finPrdType) {
		this.finPrdType = finPrdType == null ? null : finPrdType.trim();
	}
	
    /**
     * @return FinPrdType
     */	
	public String getFinPrdType() {
		return this.finPrdType;
	}
	
	/**
	 * @param finPrdName
	 */
	public void setFinPrdName(String finPrdName) {
		this.finPrdName = finPrdName == null ? null : finPrdName.trim();
	}
	
    /**
     * @return FinPrdName
     */	
	public String getFinPrdName() {
		return this.finPrdName;
	}
	
	/**
	 * @param depositType
	 */
	public void setDepositType(String depositType) {
		this.depositType = depositType == null ? null : depositType.trim();
	}
	
    /**
     * @return DepositType
     */	
	public String getDepositType() {
		return this.depositType;
	}
	
	/**
	 * @param timePointBalance
	 */
	public void setTimePointBalance(java.math.BigDecimal timePointBalance) {
		this.timePointBalance = timePointBalance;
	}
	
    /**
     * @return TimePointBalance
     */	
	public java.math.BigDecimal getTimePointBalance() {
		return this.timePointBalance;
	}
	
	/**
	 * @param averDailyBalance
	 */
	public void setAverDailyBalance(java.math.BigDecimal averDailyBalance) {
		this.averDailyBalance = averDailyBalance;
	}
	
    /**
     * @return AverDailyBalance
     */	
	public java.math.BigDecimal getAverDailyBalance() {
		return this.averDailyBalance;
	}
	
	/**
	 * @param lssuerType
	 */
	public void setLssuerType(String lssuerType) {
		this.lssuerType = lssuerType == null ? null : lssuerType.trim();
	}
	
    /**
     * @return LssuerType
     */	
	public String getLssuerType() {
		return this.lssuerType;
	}
	
	/**
	 * @param riskLevel
	 */
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel == null ? null : riskLevel.trim();
	}
	
    /**
     * @return RiskLevel
     */	
	public String getRiskLevel() {
		return this.riskLevel;
	}
	
	/**
	 * @param bayAmount
	 */
	public void setBayAmount(java.math.BigDecimal bayAmount) {
		this.bayAmount = bayAmount;
	}
	
    /**
     * @return BayAmount
     */	
	public java.math.BigDecimal getBayAmount() {
		return this.bayAmount;
	}
	
	/**
	 * @param capitalType
	 */
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType == null ? null : capitalType.trim();
	}
	
    /**
     * @return CapitalType
     */	
	public String getCapitalType() {
		return this.capitalType;
	}
	
	/**
	 * @param fundType
	 */
	public void setFundType(String fundType) {
		this.fundType = fundType == null ? null : fundType.trim();
	}
	
    /**
     * @return FundType
     */	
	public String getFundType() {
		return this.fundType;
	}
	
	/**
	 * @param marketValue
	 */
	public void setMarketValue(java.math.BigDecimal marketValue) {
		this.marketValue = marketValue;
	}
	
    /**
     * @return MarketValue
     */	
	public java.math.BigDecimal getMarketValue() {
		return this.marketValue;
	}
	
	/**
	 * @param sharesType
	 */
	public void setSharesType(String sharesType) {
		this.sharesType = sharesType == null ? null : sharesType.trim();
	}
	
    /**
     * @return SharesType
     */	
	public String getSharesType() {
		return this.sharesType;
	}
	
	/**
	 * @param sharesAccountValue
	 */
	public void setSharesAccountValue(java.math.BigDecimal sharesAccountValue) {
		this.sharesAccountValue = sharesAccountValue;
	}
	
    /**
     * @return SharesAccountValue
     */	
	public java.math.BigDecimal getSharesAccountValue() {
		return this.sharesAccountValue;
	}
	
	/**
	 * @param netAssets
	 */
	public void setNetAssets(java.math.BigDecimal netAssets) {
		this.netAssets = netAssets;
	}
	
    /**
     * @return NetAssets
     */	
	public java.math.BigDecimal getNetAssets() {
		return this.netAssets;
	}
	
	/**
	 * @param sharesNumber
	 */
	public void setSharesNumber(String sharesNumber) {
		this.sharesNumber = sharesNumber == null ? null : sharesNumber.trim();
	}
	
    /**
     * @return SharesNumber
     */	
	public String getSharesNumber() {
		return this.sharesNumber;
	}
	
	/**
	 * @param otherFinanAsset
	 */
	public void setOtherFinanAsset(java.math.BigDecimal otherFinanAsset) {
		this.otherFinanAsset = otherFinanAsset;
	}
	
    /**
     * @return OtherFinanAsset
     */	
	public java.math.BigDecimal getOtherFinanAsset() {
		return this.otherFinanAsset;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return ManagerIncome
     */	
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return BranchExIncome
     */	
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return HeadExIncome
     */	
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}