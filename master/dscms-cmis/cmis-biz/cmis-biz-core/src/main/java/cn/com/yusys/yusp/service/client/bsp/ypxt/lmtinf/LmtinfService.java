package cn.com.yusys.yusp.service.client.bsp.ypxt.lmtinf;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/28 14:21
 * @desc 信贷授信协议信息同步接口
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class LmtinfService {
    private static final Logger logger = LoggerFactory.getLogger(LmtinfService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * @param lmtinfReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfRespDto
     * @author 王玉坤
     * @date 2021/6/28 22:12
     * @version 1.0.0
     * @desc 信贷授信协议信息同步
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtinfRespDto lmtinf(LmtinfReqDto lmtinfReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(lmtinfReqDto));
        ResultDto<LmtinfRespDto> lmtinf2ResultDto = dscms2YpxtClientService.lmtinf(lmtinfReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(lmtinf2ResultDto));

        String lmtinfCode = Optional.ofNullable(lmtinf2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String lmtinfMeesage = Optional.ofNullable(lmtinf2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        LmtinfRespDto lmtinfRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, lmtinf2ResultDto.getCode())) {
            //  获取相关的值并解析
            lmtinfRespDto = lmtinf2ResultDto.getData();
        } else {//未查询到相关信息
            lmtinf2ResultDto.setCode(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value);
        return lmtinfRespDto;
    }
}
