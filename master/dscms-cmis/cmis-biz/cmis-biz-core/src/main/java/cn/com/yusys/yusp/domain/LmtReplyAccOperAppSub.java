/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppSub
 * @类描述: lmt_reply_acc_oper_app_sub数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:27:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_reply_acc_oper_app_sub")
public class LmtReplyAccOperAppSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;

	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
	private String subSerno;

	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 分项额度号 **/
	@Column(name = "ACC_SUB_NO", unique = false, nullable = true, length = 40)
	private String accSubNo;

	/** 分项额度名称 **/
	@Column(name = "ACC_SUB_NAME", unique = false, nullable = true, length = 255)
	private String accSubName;

	/** 是否预授信额度 **/
	@Column(name = "IS_PRE_LMT", unique = false, nullable = true, length = 5)
	private String isPreLmt;

	/** 是否循环额度 **/
	@Column(name = "IS_REVOLV_LIMIT", unique = false, nullable = true, length = 5)
	private String isRevolvLimit;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}

	/**
	 * @return subSerno
	 */
	public String getSubSerno() {
		return this.subSerno;
	}

	/**
	 * @param accSubNo
	 */
	public void setAccSubNo(String accSubNo) {
		this.accSubNo = accSubNo;
	}

	/**
	 * @return accSubNo
	 */
	public String getAccSubNo() {
		return this.accSubNo;
	}

	public String getLmtAccNo() {
		return lmtAccNo;
	}

	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @param accSubName
	 */
	public void setAccSubName(String accSubName) {
		this.accSubName = accSubName;
	}

	/**
	 * @return accSubName
	 */
	public String getAccSubName() {
		return this.accSubName;
	}

	/**
	 * @param isPreLmt
	 */
	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt;
	}

	/**
	 * @return isPreLmt
	 */
	public String getIsPreLmt() {
		return this.isPreLmt;
	}

	/**
	 * @param isRevolvLimit
	 */
	public void setIsRevolvLimit(String isRevolvLimit) {
		this.isRevolvLimit = isRevolvLimit;
	}

	/**
	 * @return isRevolvLimit
	 */
	public String getIsRevolvLimit() {
		return this.isRevolvLimit;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	/**
	 * @return lmtAmt
	 */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}