/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.ypxt.callguar.CallGuarReqDto;
import cn.com.yusys.yusp.service.Dscms2ImageClientService;
import cn.com.yusys.yusp.vo.*;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenRespDto;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-12-01 21:10:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "抵质押基本信息")
@RequestMapping("/api/guarbaseinfo")
public class GuarBaseInfoResource {
    private static final Logger log = LoggerFactory.getLogger(GuarBaseInfoResource.class);

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    private Dscms2ImageClientService dscms2ImageClientService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<GuarBaseInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarBaseInfo> list = guarBaseInfoService.selectAll(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/all")
    protected ResultDto<List<GuarBaseInfo>> indexAll(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:guarinfomanagequery
     * @函数描述:押品信息管理查询
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/guarinfomanagequery")
    protected ResultDto<List<GuarBaseInfo>> guarInfoManageQuery(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:guarpreval
     * @函数描述:押品价值初估
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/guarpreval")
    protected ResultDto<List<GuarBaseInfo>> guarPreVal(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:guarreval
     * @函数描述:押品价值重估
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/guarreval")
    protected ResultDto<List<GuarBaseInfo>> guarReVal(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     *
     * 查询所有的押品
     * @param queryModel
     * @return
     */
    @PostMapping("/allGuar")
    protected ResultDto<List<GuarBaseInfo>> allGuar(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querybymanagerid")
    protected ResultDto<List<GuarBaseInfo>> querybymanagerid(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querybymanagerbrid")
    protected ResultDto<List<GuarBaseInfo>> querybymanagerbrid(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<GuarBaseInfo>> index(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }


    @PostMapping("/selectByModel")
    protected ResultDto<List<GuarBaseInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.selectByModel(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<GuarBaseInfo> show(@PathVariable("serno") String serno) {
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectByPrimaryKey(serno);
        return new ResultDto<GuarBaseInfo>(guarBaseInfo);
    }


    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:根据业务流水号查询抵质押信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<GuarBaseInfo> selectByPrimaryKey(@RequestBody GuarBaseInfo guarbaseinfo) {
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectByPrimaryKey(guarbaseinfo.getSerno());
        return new ResultDto<GuarBaseInfo>(guarBaseInfo);
    }

    /**
     * @函数名称:selectByCusId
     * @函数描述:根据客户编号查询抵质押信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByCusId")
    protected ResultDto<List<GuarBaseInfo>> selectByCusId(@RequestBody QueryModel model) {
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.selectByCusId(model);
        return new ResultDto<List<GuarBaseInfo>>(guarBaseInfoList);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<GuarBaseInfo> create(@RequestBody GuarBaseInfo guarBaseInfo) throws URISyntaxException {
        guarBaseInfoService.insert(guarBaseInfo);
        return new ResultDto<GuarBaseInfo>(guarBaseInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarBaseInfo guarBaseInfo) throws URISyntaxException {
        int result = guarBaseInfoService.update(guarBaseInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarBaseInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarBaseInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取押品信息
     * @param guarBaseInfoDto
     * @return
     */
    @PostMapping("/getGuarBaseInfo")
    protected ResultDto<List<GuarBaseInfoDto>> getGuarBaseInfo(@RequestBody GuarBaseInfoDto guarBaseInfoDto) {
        log.info("押品信息【{}】", JSONObject.toJSON(guarBaseInfoDto));
        List<GuarBaseInfoDto> rsGuarBaseInfoDto = guarBaseInfoService.getGuarBaseInfo(guarBaseInfoDto);
        return new ResultDto<List<GuarBaseInfoDto>>(rsGuarBaseInfoDto);
    }

    /**
     * 押品确认匹配
     * @param bizGuarExchangeDto
     * @return
     */
    @PostMapping("/guarConfirMatching")
    protected ResultDto<BizGuarExchangeDto> guarConfirMatching(@RequestBody BizGuarExchangeDto bizGuarExchangeDto) {
        log.info("押品确认匹配请求信息【{}】", JSONObject.toJSON(bizGuarExchangeDto));
        BizGuarExchangeDto rsBizGuarExchangeDto = guarBaseInfoService.guarConfirMatching(bizGuarExchangeDto);
        log.info("押品确认匹配响应信息【{}】", JSONObject.toJSON(rsBizGuarExchangeDto));
        return new ResultDto<BizGuarExchangeDto>(rsBizGuarExchangeDto);
    }

    /**
     * 删除押品信息
     * @param bizGuarExchangeDto
     * @return
     */
    @PostMapping("/deleteGuar")
    protected ResultDto<Integer> deleteGuar(@RequestBody BizGuarExchangeDto bizGuarExchangeDto) {
        log.info("押品删除请求信息【{}】", JSONObject.toJSON(bizGuarExchangeDto));
        return new ResultDto<Integer>(guarBaseInfoService.deleteGuar(bizGuarExchangeDto));
    }
    
    /**
     * 获取满足当前查询条件的押品信息(权证出入库申请)
     * @param map
     * @return
     */
    @PostMapping(value = "/getGuarBaseInfoByParams")
    protected GuarClientRsDto getGuarBaseInfoByParams(@RequestBody Map map){
        return guarBaseInfoService.getGuarBaseInfoByParams(map);
    }
    
    /**
     * 接口调用更新押品所在业务阶段
     * @param guarBaseInfoClientDto
     * @return
     */
    @PostMapping("/updateGuarBusistate")
    protected GuarClientRsDto updateGuarBusistate(@RequestBody GuarBaseInfoClientDto guarBaseInfoClientDto){
        log.info("押品更新所在业务阶段请求信息【{}】", JSONObject.toJSON(guarBaseInfoClientDto));
        return guarBaseInfoService.updateGuarBusistate(guarBaseInfoClientDto);
    }
    /**
     * 更新押品所在业务阶段
     * @param guarBaseInfoClientDto
     * @return
     */
    @PostMapping("/updateGuarBusistateInfo")
    protected GuarClientRsDto updateGuarBusistateInfo(@RequestBody GuarBaseInfoClientDto guarBaseInfoClientDto){
        log.info("押品更新所在业务阶段请求信息【{}】", JSONObject.toJSON(guarBaseInfoClientDto));
        return guarBaseInfoService.updateGuarBusistateInfo(guarBaseInfoClientDto);
    }

/*    *//**
     * @函数名称:getGuarInfo
     * @算法描述:
     *//*
    @PostMapping("/getGuarRelotInfo")
    protected List<GuarEvalRelotResultDto> getGuarRelotInfo(@RequestBody GuarRelotInfoClientDto guarRelotInfoClientDto) {
        List<GuarEvalRelotResultDto> result = guarBaseInfoService.getGuarRelotInfo(guarRelotInfoClientDto);
        return result;
    }*/

    /**
     * @函数名称:delete
     * @函数描述:单个对象逻辑删除删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteOnLogic/{serno}")
    protected ResultDto<Integer> deleteOnLogic(@PathVariable("serno") String serno) {
        int result = guarBaseInfoService.deleteOnLogic(serno);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:preserveGuarInfo
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfo")
    protected ResultDto<Integer> preserveGuarBaseInfo(@RequestBody GuarBaseInfo guarBaseInfo) {
        int result = guarBaseInfoService.preserveGuarBaseInfo(guarBaseInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getGuarInfoUrl
     * @函数描述:获取押品系统url
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getGuarInfoUrl")
    protected ResultDto<String> getGuarInfoUrl(@RequestBody GuarBaseInfoDto guarBaseInfoDto) throws UnsupportedEncodingException {
        String guarInfoUrl = guarBaseInfoService.getGuarInfoUrl(guarBaseInfoDto);
        return new ResultDto<String>(guarInfoUrl);
    }


    /**
     * @函数名称:getImageSysToken
     * @函数描述:获取影像系统token
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getImageSysToken/{useName}/{passWord}")
    protected ResultDto<ImageTokenRespDto> getImageSysToken(@PathVariable("useName") String useName,@PathVariable String passWord){
        ImageTokenReqDto imageTokenReqDto = new ImageTokenReqDto();
        imageTokenReqDto.setUsername(useName);
        imageTokenReqDto.setPassword(passWord);
        return dscms2ImageClientService.imagetoken(imageTokenReqDto);
    }
//    /**
//     * @函数名称:getImageSysUrl
//     * @函数描述:获取影像系统url
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/getImageSysUrl")
//    protected ResultDto<String> getImageSysUrl(@RequestBody CallImageReqDto callGuarReqDto){
//        String imageSysUrl = guarBaseInfoService.getImageSysUrl(callGuarReqDto);
//        return new ResultDto<String>(imageSysUrl);
//    }

    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkGuarInfoIsExist/{serno}")
    protected ResultDto<Integer> checkGuarInfoIsExist(@PathVariable("serno") String serno) {
        int result = guarBaseInfoService.checkGuarInfoIsExist(serno);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:querFddGuarinforel
     * @函数描述:房抵e点贷根据业务流水号查询抵质押物信息
     * @算法描述: 获取业务与押品关系表当前业务流水号下optype为01,02的抵押物信息
     * @创建者： zhangliang15
     * @参数与返回说明:
     */
    @ApiOperation("抵质押信息表")
    @PostMapping("/querFddGuarinforel")
    protected ResultDto<List<GuarBizRelGuarBaseDto>> querFddGuarinforel(@RequestBody QueryModel queryModel) {
        List<GuarBizRelGuarBaseDto> list = (List<GuarBizRelGuarBaseDto>) guarBaseInfoService.querFddGuarinforel(queryModel);
        return new ResultDto<List<GuarBizRelGuarBaseDto>>(list);
    }


    /**
     * @函数名称:queryGuarInfoSell
     * @函数描述:根据业务流水号查询抵质押物信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("抵质押信息表")
    @PostMapping("/querguarinforel")
    protected ResultDto<List<GuarBizRelGuarBaseDto>> queryGuarInfoSell(@RequestBody QueryModel queryModel) {
        List<GuarBizRelGuarBaseDto> list = (List<GuarBizRelGuarBaseDto>) guarBaseInfoService.queryGuarInfoSell(queryModel);
        return new ResultDto<List<GuarBizRelGuarBaseDto>>(list);
    }

    @PostMapping("/queryGuarBaseInfoRelDtoByParams")
    protected ResultDto<List<GuarBaseInfoRelDto>> queryGuarBaseInfoRelDtoByParams(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfoRelDto> list = (List<GuarBaseInfoRelDto>) guarBaseInfoService.queryGuarBaseInfoRelDtoByParams(queryModel);
        return new ResultDto<List<GuarBaseInfoRelDto>>(list);
    }

    /**
     * @param queryModel
     * @return Integer
     * @author 王祝远
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    @GetMapping("/saveguarinforel/")
    protected ResultDto<List<GuarBizRelGuarBaseDto>> saveGuarInfoRel(QueryModel queryModel) {
        List<GuarBizRelGuarBaseDto> list = (List<GuarBizRelGuarBaseDto>) guarBaseInfoService.queryGuarInfoSell(queryModel);
        return new ResultDto<List<GuarBizRelGuarBaseDto>>(list);
    }
    /**根据担保合同编号查询担保合同下所有押品信息列表**/

    /**
     * 全表查询.
     * @函数名称:queryAllInfo
     * @函数描述:查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query/allInfo/{guarContNo}")
    protected ResultDto<List<GuarBaseInfo>> queryAllInfo(@PathVariable("guarContNo") String guarContNo) {
        List<GuarBaseInfo> list = guarBaseInfoService.selectByGuarContNoModel(guarContNo,null);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:saveByGuarBasicBuildingVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicBuildingVo")
    protected ResultDto saveByGuarBasicBuildingVo(@RequestBody GuarBasicBuildingVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicBuildingVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicCargoPledgeVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicCargoPledgeVo")
    protected ResultDto saveByGuarBasicCargoPledgeVo(@RequestBody GuarBasicCargoPledgeVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicCargoPledgeVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicGuarInfBuildUseVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicGuarInfBuildUseVo")
    protected ResultDto saveByGuarBasicGuarInfBuildUseVo(@RequestBody GuarBasicGuarInfBuildUseVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicGuarInfBuildUseVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByInfBusinessIndustryHousrVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByInfBusinessIndustryHousrVo")
    protected ResultDto saveByInfBusinessIndustryHousrVo(@RequestBody GuarBasicGuarInfBusinessIndustryHousrVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByInfBusinessIndustryHousrVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicGuarInfLivingRoomVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicGuarInfLivingRoomVo")
    protected ResultDto saveByGuarBasicGuarInfLivingRoomVo(@RequestBody GuarBasicGuarInfLivingRoomVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicGuarInfLivingRoomVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicGuarInfMachEquiVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicGuarInfMachEquiVo")
    protected ResultDto saveByGuarBasicGuarInfMachEquiVo(@RequestBody GuarBasicGuarInfMachEquiVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicGuarInfMachEquiVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicGuarInfOtherHouseVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicGuarInfOtherHouseVo")
    protected ResultDto saveByGuarBasicGuarInfOtherHouseVo(@RequestBody GuarBasicGuarInfOtherHouseVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicGuarInfOtherHouseVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:saveByGuarBasicGuarInfUsufLandVo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveByGuarBasicGuarInfUsufLandVo")
    protected ResultDto saveByGuarBasicGuarInfUsufLandVo(@RequestBody GuarBasicGuarInfUsufLandVo record) throws URISyntaxException {
        ResultDto result = guarBaseInfoService.saveByGuarBasicGuarInfUsufLandVo(record);
        return new ResultDto(result);
    }

    /**
     * @函数名称:selectByGuarContNoModel
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryUndo")
    protected ResultDto<List<GuarBaseInfo>> queryUndo(@RequestBody GuarBasicInfoVo guarBasicInfoVo) {
        List<GuarBaseInfo> list = guarBaseInfoService.selectByGuarContNoModel(guarBasicInfoVo);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:getGuarBase
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getGuarBase")
    protected ResultDto<GuarBaseInfo> getGuarBase(@RequestBody GuarBaseInfo record) {
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarBaseInfo>(guarBaseInfo);
    }

    /**
     * @函数名称:getGuarBaseInfoByGuarNo
     * @函数描述:根据押品编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getGuarBaseInfoByGuarNo")
    @ApiOperation("根据押品编号获取押品信息")
    protected ResultDto<GuarBaseInfo> getGuarBaseInfoByGuarNo(@RequestBody String guarNo) {
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.getGuarBaseInfoByGuarNo(guarNo);
        return new ResultDto<GuarBaseInfo>(guarBaseInfo);
    }

    /**
     * @函数名称:selectGuarBaseInfoByGuarContNo
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectGuarBaseInfoByGuarContNo")
    @ApiOperation("根据担保合同编号获取押品信息")
    protected ResultDto<List<GuarBaseInfoDto2> > selectGuarBaseInfoByGuarContNo(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfoDto2>  GuarBaseInfoDto2 = guarBaseInfoService.selectGrtGuarContRelByGuarContNo(queryModel);

        if (CollectionUtils.isNotEmpty(GuarBaseInfoDto2)){
            if (GuarBaseInfoDto2.get(0)==null || StringUtils.isEmpty(GuarBaseInfoDto2.get(0).getGuarNo())){
                GuarBaseInfoDto2 = null;
            }
        }

        return new ResultDto<>(GuarBaseInfoDto2);
    }

    /**
     * @函数名称:selectGuarBaseInfoByGuarContNo
     * @函数描述:根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectguarbaseinfoformortgagelogout")
    @ApiOperation("根据担保合同编号获取押品信息(针对抵押注销)")
    protected ResultDto<List<GuarBaseInfoDto> > selectGuarBaseInfoForMortgageLogOut(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfoDto>  guarBaseInfoDtoList = guarBaseInfoService.selectGuarBaseInfoForMortgageLogOut(queryModel);

        if (CollectionUtils.isNotEmpty(guarBaseInfoDtoList)){
            if (guarBaseInfoDtoList.get(0)==null || StringUtils.isEmpty(guarBaseInfoDtoList.get(0).getGuarNo())){
                guarBaseInfoDtoList = null;
            }
        }

        return new ResultDto<>(guarBaseInfoDtoList);
    }

    /**
     * @函数名称:queryGuarInfoIsUnderLmt
     * @函数描述:根据额度编号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据业务流水号查询授信项下的押品信息")
    @PostMapping("/queryGuarInfoIsUnderLmt")
    protected ResultDto<List<GuarBaseInfo>> queryGuarInfoIsUnderLmt(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.queryGuarInfoIsUnderLmt(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByParams
     * @函数描述:根据额度编号查询授信项下的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据业务流水号查询授信项下的押品信息")
    @PostMapping("/queryguarbaseinfodatabyparams")
    protected ResultDto<List<GuarBaseInfo>> queryGuarBaseInfoDataByParams(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.queryGuarBaseInfoDataByParams(queryModel);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:queryGuarBaseInfoByWarrantInSerno
     * @函数描述:根据权证入库流水号查询权证入库里的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据权证入库流水号查询权证入库里的押品信息")
    @PostMapping("/queryguarbaseinfobywarrantinserno")
    protected ResultDto<List<GuarBaseInfo>> queryGuarBaseInfoByWarrantInSerno(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.queryGuarBaseInfoByWarrantInSerno(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:queryGuarBaseInfoByCoreGuarantyNo
     * @函数描述:根据核心担保编号查询押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据核心担保编号查询押品信息")
    @PostMapping("/queryguarbaseinfobycoreguarantyno")
    protected ResultDto<List<GuarBaseInfo>> queryGuarBaseInfoByCoreGuarantyNo(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.queryGuarBaseInfoByCoreGuarantyNo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:queryGuarBaseInfoDataByGuarContNo
     * @函数描述:根据担保合同编号查询关联的押品信息
     * @算法描述:
     * @参数与返回说明:
     */
    @ApiOperation("根据担保合同编号查询关联的押品信息")
    @PostMapping("/queryguarbaseinfodatabyguarcontno")
    protected ResultDto<List<GuarBaseInfo>> queryGuarBaseInfoDataByGuarContNo(@RequestBody QueryModel queryModel) {
        List<GuarBaseInfo> list = guarBaseInfoService.queryGuarBaseInfoDataByGuarContNo(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:selectByGuarNo
     * @函数描述:根据押品编号查询抵质押信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBaseInfoByGuarNo")
    protected ResultDto<List<GuarBaseInfo>> selectBaseInfoByGuarNo(@RequestBody QueryModel model) {
        List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.selectBaseInfoByGuarNo(model);
        return new ResultDto<List<GuarBaseInfo>>(guarBaseInfoList);
    }

    /**
     * @函数名称:selectByGuarBaseInfo
     * @函数描述:根据借款合同号查询担保合同
     * @参数与返回说明:
     * @算法描述:
     * @创建人：liuquan
     */
    @PostMapping("/selectByGuarBaseInfo")
    @ApiOperation("通过担保合同编号查询抵质押基本信息")
    protected ResultDto<List<GuarBaseInfoClientDto>> selectByGuarBaseInfo(@RequestBody String guarContNo) {
        List<GuarBaseInfoClientDto> list= guarBaseInfoService.selectByGuarBaseInfo(guarContNo);
        return new ResultDto<List<GuarBaseInfoClientDto>>(list);
    }
}
