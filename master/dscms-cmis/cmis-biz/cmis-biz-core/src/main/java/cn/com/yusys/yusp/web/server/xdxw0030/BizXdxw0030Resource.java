package cn.com.yusys.yusp.web.server.xdxw0030;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0030.req.Xdxw0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0030.resp.Xdxw0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0030.Xdxw0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据云估计流水号查询房屋信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0030:根据云估计流水号查询房屋信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0030Resource.class);

    @Autowired
    private Xdxw0030Service xdxw0030Service;
    /**
     * 交易码：xdxw0030
     * 交易描述：根据云估计流水号查询房屋信息
     *
     * @param xdxw0030DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据云估计流水号查询房屋信息")
    @PostMapping("/xdxw0030")
    protected @ResponseBody
    ResultDto<Xdxw0030DataRespDto> xdxw0030(@Validated @RequestBody Xdxw0030DataReqDto xdxw0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataReqDto));
        Xdxw0030DataRespDto xdxw0030DataRespDto = new Xdxw0030DataRespDto();// 响应Dto:根据云估计流水号查询房屋信息
        ResultDto<Xdxw0030DataRespDto> xdxw0030DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataReqDto));
            xdxw0030DataRespDto = xdxw0030Service.getXdxw0030(xdxw0030DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataRespDto));
            // 封装xdxw0030DataResultDto中正确的返回码和返回信息
            xdxw0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            xdxw0030DataResultDto.setCode(e.getErrorCode());
            xdxw0030DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, e.getMessage());
            // 封装xdxw0030DataResultDto中异常返回码和返回信息
            xdxw0030DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0030DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0030DataRespDto到xdxw0030DataResultDto中
        xdxw0030DataResultDto.setData(xdxw0030DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataResultDto));
        return xdxw0030DataResultDto;
    }
}
