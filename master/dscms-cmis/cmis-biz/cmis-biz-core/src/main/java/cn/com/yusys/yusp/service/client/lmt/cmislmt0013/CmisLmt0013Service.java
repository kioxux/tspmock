package cn.com.yusys.yusp.service.client.lmt.cmislmt0013;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0013.req.CmisLmt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class cmisLmt0013Service
 * @date 2021/8/25 19:10
 * @desc 占用台账额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0013Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0013Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0013ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0013.resp.cmisLmt0013RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 占用台账额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0013RespDto cmisLmt0013(CmisLmt0013ReqDto cmisLmt0013ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, JSON.toJSONString(cmisLmt0013ReqDto));
        ResultDto<CmisLmt0013RespDto> cmisLmt0013ResultDto = cmisLmtClientService.cmisLmt0013(cmisLmt0013ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value, JSON.toJSONString(cmisLmt0013ResultDto));

        String cmisLmt0013Code = Optional.ofNullable(cmisLmt0013ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0013Meesage = Optional.ofNullable(cmisLmt0013ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0013RespDto cmisLmt0013RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0013ResultDto.getCode()) && cmisLmt0013ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0013ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0013RespDto = cmisLmt0013ResultDto.getData();
        } else {
            if (cmisLmt0013ResultDto.getData() != null) {
                cmisLmt0013Code = cmisLmt0013ResultDto.getData().getErrorCode();
                cmisLmt0013Meesage = cmisLmt0013ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0013Code = EpbEnum.EPB099999.key;
                cmisLmt0013Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0013Code, cmisLmt0013Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0013.value);
        return cmisLmt0013RespDto;
    }
}
