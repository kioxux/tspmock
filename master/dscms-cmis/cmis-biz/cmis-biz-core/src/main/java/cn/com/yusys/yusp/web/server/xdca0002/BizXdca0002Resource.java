package cn.com.yusys.yusp.web.server.xdca0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0002.req.Xdca0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0002.resp.Xdca0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdca0002.Xdca0002Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:大额分期申请（试算）接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0002:大额分期申请（试算）接口")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0002Resource.class);

    @Autowired
    private Xdca0002Service xdca0002Service;

    /**
     * 交易码：xdca0002
     * 交易描述：大额分期申请（试算）接口
     *
     * @param xdca0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期申请（试算）接口")
    @PostMapping("/xdca0002")
    protected @ResponseBody
    ResultDto<Xdca0002DataRespDto> xdca0002(@Validated @RequestBody Xdca0002DataReqDto xdca0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataReqDto));
        Xdca0002DataRespDto xdca0002DataRespDto = new Xdca0002DataRespDto();// 响应Dto:大额分期申请（试算）接口
        ResultDto<Xdca0002DataRespDto> xdca0002DataResultDto = new ResultDto<>();
        try {
            // 从xdca0002DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataReqDto));
            xdca0002DataRespDto = xdca0002Service.xdca0002(xdca0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataRespDto));
            // 封装xdca0002DataResultDto中正确的返回码和返回信息
            xdca0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdca0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, e.getMessage());
            // 封装xdca0002DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdca0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdca0002DataRespDto到xdca0002DataResultDto中
        xdca0002DataResultDto.setData(xdca0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataRespDto));
        return xdca0002DataResultDto;
    }
}
