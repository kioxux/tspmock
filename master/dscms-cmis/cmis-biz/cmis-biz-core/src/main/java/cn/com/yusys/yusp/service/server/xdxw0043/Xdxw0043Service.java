package cn.com.yusys.yusp.service.server.xdxw0043;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req.Xwd013ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.AssetsLiabilitieList;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.IncomeStatementList;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0016.req.Cmiscus0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0016.resp.Cmiscus0016RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0043.req.Xdxw0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0043.resp.Xdxw0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.xwd.xwd013.Xwd013Service;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.web.rest.CfgXdFinReportResource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 接口处理类:无还本续贷待办接收
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdxw0043Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0043Service.class);
    @Autowired
    private AccLoanService accLoanService;//贷款台账

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;// 客户授信调查表

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;// 调查报告基本信息

    @Autowired
    private LmtSurveyReportOtherInfoService lmtSurveyReportOtherInfoService;// 调查报告其他信息

    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;// 调查结论信息

    @Autowired
    private CusLstMclWhbxdService cusLstMclWhbxdService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CfgXdFinReportService cfgXdFinReportService;

    @Autowired
    private CfgXdFinReportPrdService cfgXdFinReportPrdService;

    @Autowired
    private LmtBsInfoService lmtBsInfoService;// 资产负债信息

    @Autowired
    private LmtPlListInfoService lmtPlListInfoService;// 损益明细信息

    @Autowired
    private Xwd013Service xwd013Service;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * 无还本续贷待办接收
     *
     * @param xdxw0043DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0043DataRespDto xdcz0043(Xdxw0043DataReqDto xdxw0043DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value);

        // 声明变量
        // 接口返回体
        Xdxw0043DataRespDto xdcz0043DataRespDto = new Xdxw0043DataRespDto();
        // 无还本续贷实体
        CusLstMclWhbxd cusLstMclWhbxd = null;

        String opFlag = StringUtils.EMPTY;// 操作成功标志位
        String opMsg = StringUtils.EMPTY;// 描述信息
        try {
            // 1、校验数据非空
            // 从xdxw0043DataReqDto获取业务值进行业务逻辑处理
            // 流水号
            String apply_no = xdxw0043DataReqDto.getApply_no();
            // 分层标识 01--优转 02--普转 99--正常规则拒绝 98--异常规则拒绝
            String flag = xdxw0043DataReqDto.getFlag();
            // 客户经理
            String zb_manager_id = xdxw0043DataReqDto.getZb_manager_id();

            if(!StringUtils.nonBlank(apply_no)){
                throw BizException.error(null, null, "业务流水【apply_no】不能为空！");
            }
            if(!StringUtils.nonBlank(flag)){
                throw BizException.error(null, null, "客户分层【flag】不能为空！");
            }
            if(!StringUtils.nonBlank(zb_manager_id)){
                throw BizException.error(null, null, "客户经理号【zb_manager_id】不能为空！");
            }

            // 2、根据风控传回来的流水号查询无还本续贷名单信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", apply_no);
            // 查询无还本续贷名单信息
            logger.info("根据流水号【{}】，获取无还本续贷名单信息开始！", apply_no);
            List<CusLstMclWhbxd> cusLstMclWhbxds= cusLstMclWhbxdService.selectByModel(queryModel);
            logger.info("根据流水号【{}】，获取无还本续贷名单信息结束！", apply_no);

            if (!CollectionUtils.nonEmpty(cusLstMclWhbxds)) {
                throw BizException.error(null, null, "未查询到名单申请信息！");
            }
            cusLstMclWhbxd = cusLstMclWhbxds.get(0);

                if (Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_99.key, flag)) {
                logger.info("无还本续贷名单流水号【{}】,预授信已被拒绝！", apply_no);
                // 更新办理状态为处理失败
                cusLstMclWhbxd.setApplyStatus("6");
                cusLstMclWhbxdService.update(cusLstMclWhbxd);

                // 返回处理信息
                xdcz0043DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 操作成功标志位
                xdcz0043DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
                return xdcz0043DataRespDto;
            }

            // 3、正常处理逻辑
            QueryModel model = new QueryModel();
            model.addCondition("listSerno", cusLstMclWhbxd.getSerno());
            List<LmtSurveyReportMainInfo> lmtSurveyReportMainInfoList = lmtSurveyReportMainInfoService.selectByModel(model);

            if (CollectionUtils.nonEmpty(lmtSurveyReportMainInfoList)) {
                throw BizException.error(null, null, "已生成调查报告信息，请勿重复推送！");
            }

            // 生成调查流水号信息
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());

            // 根据原始借据号查询台账信息
            AccLoan accLoan = accLoanService.selectByBillNo(cusLstMclWhbxd.getBillNo());

            if (Objects.isNull(accLoan)) {
                throw BizException.error(null, null, "未查询到原借据信息！");
            }
            // 新老表映射如下：
            // 老【授信调查主表】XdCusSurvey，新【新信贷调查主表】 LMT_SURVEY_REPORT_MAIN_INFO LMT_SURVEY_REPORT_BASIC_INFO
            // 老【无还本续贷调查主表】WhbxdCusSurvey，新【新信贷调查从表】LMT_SURVEY_REPORT_OTHER_INFO
            // 老【无还本续贷资产负债表】XdWhbxdBalancesheet，新【资产负债信息】LMT_BS_INFO
            // 老【无还本续贷（损益表明细）】XD_WHBXD_LOSS_DETAIL，新【损益明细信息】LMT_PL_LIST_INFO
            if (Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_01.key, flag) || Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_02.key, flag)) {
                // 老信贷处理逻辑：flag=“01”时只生成授信调查主表和无还本续贷调查表
                // 新信贷处理逻辑：flag=“01”时只生成 调查报告基本信息 和 调查报告其他信息
                logger.info("*****XDXW0043:新信贷处理逻辑：flag=“01”时只生成 调查报告基本信息 和 调查报告其他信息");

                // 生成主表信息
                LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();// 客户授信调查表
                lmtSurveyReportMainInfo.setSurveySerno(serno);// 调查流水号
                lmtSurveyReportMainInfo.setBizSerno(""); // 第三方流水号信息
                lmtSurveyReportMainInfo.setListSerno(cusLstMclWhbxd.getSerno()); // 名单流水号
                lmtSurveyReportMainInfo.setCusId(cusLstMclWhbxd.getCusId());
                lmtSurveyReportMainInfo.setCusName(cusLstMclWhbxd.getCusName());
                lmtSurveyReportMainInfo.setCertCode(cusLstMclWhbxd.getCertCode());
                lmtSurveyReportMainInfo.setCertType(cusLstMclWhbxd.getCertType());
                lmtSurveyReportMainInfo.setBizSerno(serno);//第三方业务流水号
                lmtSurveyReportMainInfo.setPrdName("房抵循环贷");//产品名称
                lmtSurveyReportMainInfo.setPrdId("SC010010");//产品编号
                lmtSurveyReportMainInfo.setSurveyType(Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_01.key, flag) ? DscmsBizXwEnum.SURVEY_TYPE_14.key
                        : DscmsBizXwEnum.SURVEY_TYPE_7.key);//小微调查报告类型,默认无还本续贷
                lmtSurveyReportMainInfo.setAppAmt(accLoan.getLoanBalance());//申请金额
                lmtSurveyReportMainInfo.setApproveStatus("000");//审批状态，默认待发起
                lmtSurveyReportMainInfo.setDataSource("01");//数据来源，默认系统推送
                String inserttime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                lmtSurveyReportMainInfo.setIntoTime(inserttime);//进件时间，默认系统当前时间
                lmtSurveyReportMainInfo.setIsAutodivis("0");//是否自动分配，默认否
                lmtSurveyReportMainInfo.setIsStopOffline("1");//是否线下调查，默认是
                lmtSurveyReportMainInfo.setOprType("01");//操作类型，默认新增
                lmtSurveyReportMainInfo.setMarId("");//营销人工号
                lmtSurveyReportMainInfo.setManagerId(zb_manager_id);// 主办客户经理
                // 跟据客户经理查询其所属机构信息
                // 调用oca接口查询机构
                AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(zb_manager_id);
                // 校验非空
                if (Objects.isNull(adminSmUserDto)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户信息！");
                }
                lmtSurveyReportMainInfo.setManagerBrId(adminSmUserDto.getOrgId());
                // 2021年11月16日10:41:21 给基本表塞入值 hubp
                lmtSurveyReportMainInfo.setInputId(zb_manager_id);
                lmtSurveyReportMainInfo.setInputBrId(adminSmUserDto.getOrgId());

                lmtSurveyReportMainInfoService.insertSelective(lmtSurveyReportMainInfo);

                // 生成基本信息
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();// 调查报告基本信息
                BeanUtils.beanCopy(lmtSurveyReportMainInfo, lmtSurveyReportBasicInfo);
                /** 电话号码 **/
                lmtSurveyReportBasicInfo.setPhone(cusLstMclWhbxd.getLinkMode());
                /** 婚姻状况 **/
                lmtSurveyReportBasicInfo.setMarStatus(cusLstMclWhbxd.getMarStatus());
                /** 配偶姓名 **/
                lmtSurveyReportBasicInfo.setSpouseName(cusLstMclWhbxd.getSpouseName());
                /** 配偶证件号码 **/
                lmtSurveyReportBasicInfo.setSpouseCertCode(cusLstMclWhbxd.getSpouseCertCode());
                /** 配偶电话 **/
                lmtSurveyReportBasicInfo.setSpousePhone(cusLstMclWhbxd.getSpousePhone());
                /** 是否线上抵押 **/
                lmtSurveyReportBasicInfo.setIsOnlinePld("0");
                /** 担保方式 10--抵押 **/
                lmtSurveyReportBasicInfo.setGuarMode("10");
                /** 还款方式 A001 **/
                lmtSurveyReportBasicInfo.setRepayMode("A001");
                /** 原借据号 **/
                lmtSurveyReportBasicInfo.setOldBillNo(cusLstMclWhbxd.getBillNo());
                /** 原借据金额 **/
                lmtSurveyReportBasicInfo.setOldBillAmt(accLoan.getLoanAmt());
                /** 申请金额 **/
                lmtSurveyReportBasicInfo.setAppAmt(accLoan.getLoanBalance());
                /** 原借据余额 **/
                lmtSurveyReportBasicInfo.setOldBillBalance(accLoan.getLoanBalance());
                /** 原借据利率 **/
                lmtSurveyReportBasicInfo.setOldBillLoanRate(accLoan.getExecRateYear());
                /** 借款人负债较上期增加是否超50% **/
                lmtSurveyReportBasicInfo.setDebtFlag("");
                /** 借款人对外是否提供过多担保或大量资产被抵押 **/
                lmtSurveyReportBasicInfo.setGuarFlag("");
                /** 借款人及其家庭是否发生意外 **/
                lmtSurveyReportBasicInfo.setAccidentFlag("");
                /** 抵/质押物是否异常 **/
                lmtSurveyReportBasicInfo.setGuaranteeFlag("");
                /** 借款人经营活动是否正常 **/
                lmtSurveyReportBasicInfo.setActivityFlag("");
                /** 借款人经营所有权是否发生重大变化 **/
                lmtSurveyReportBasicInfo.setManagementBelongFlag("");
                logger.info("**********XDXW0043**插入lmtSurveyReportBasicInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                int t0 = lmtSurveyReportBasicInfoService.insertSelective(lmtSurveyReportBasicInfo);
                logger.info("**********XDXW0043**插入lmtSurveyReportBasicInfo结束,插入条数:{}", t0);

                LmtSurveyReportOtherInfo lmtSurveyReportOtherInfo = new LmtSurveyReportOtherInfo();// 调查报告其他信息
                BeanUtils.beanCopy(lmtSurveyReportMainInfo, lmtSurveyReportOtherInfo);
                /** 个人征信状态 **/
                lmtSurveyReportOtherInfo.setIndivCreditStatus("");
                /** 个人征信备注 **/
                lmtSurveyReportOtherInfo.setIndivCreditRemark("");
                /** 企业征信状态 **/
                lmtSurveyReportOtherInfo.setCorpCreditStatus("");
                /** 企业征信备注 **/
                lmtSurveyReportOtherInfo.setCorpCreditRemark("");
                /** 营业额检验情况说明一 **/
                lmtSurveyReportOtherInfo.setTurnoverChkDesc1("");
                /** 营业额检验情况说明二 **/
                lmtSurveyReportOtherInfo.setTurnoverChkDesc2("");

                logger.info("**********XDXW0043**插入lmtSurveyReportOtherInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyReportOtherInfo));
                int t1 = lmtSurveyReportOtherInfoService.insertSelective(lmtSurveyReportOtherInfo);
                logger.info("**********XDXW0043**插入lmtSurveyReportOtherInfo结束,插入条数:{}", t1);

                // 生成调查结论信息
                LmtSurveyConInfo lmtSurveyConInfo = new LmtSurveyConInfo();
                BeanUtils.beanCopy(lmtSurveyReportBasicInfo, lmtSurveyConInfo);
                /** 建议金额 **/
                lmtSurveyConInfo.setAdviceAmt(accLoan.getLoanBalance());
                /** 建议利率 **/
                lmtSurveyConInfo.setAdviceRate(accLoan.getExchangeRate());
                /** 建议期限 **/
                lmtSurveyConInfo.setAdviceTerm(accLoan.getLoanTerm());
                /** 担保方式 **/
                lmtSurveyConInfo.setGuarMode("10");
                /** 还款方式 **/
                lmtSurveyConInfo.setRepayMode(lmtSurveyReportBasicInfo.getRepayMode());

                logger.info("**********XDXW0043**插入lmtSurveyConInfo开始,插入参数为:{}", JSON.toJSONString(lmtSurveyConInfoService));
                int t2 = lmtSurveyConInfoService.insert(lmtSurveyConInfo);
                logger.info("**********XDXW0043**插入lmtSurveyConInfo结束,插入条数:{}", t2);


                // 如果是普转，须填充损益明细信息及资产负债
                if (Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_02.key, flag)) {
                    //资产负债表和损益明细
                    List<CfgXdFinReport> cfgXdFinReportList = cfgXdFinReportService.selectByPrd("SC010010");
                    if (!CollectionUtils.nonEmpty(cfgXdFinReportList)) {
                        logger.info("根据产品代码【{}】未查询到资产负债信息！", "SC010010");
                    } else {
                        // 2021年10月14日21:24:17 hubp 查询原调查编号
                        logger.info("*****xdxw0043--开始通过原借据编号查询【{}】到的放款流水号【{}】查询调查流水号************", cusLstMclWhbxd.getBillNo(),accLoan.getPvpSerno());
                        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(accLoan.getPvpSerno());
                        logger.info("*****xdxw0043--开始通过原借据编号查询【{}】到的放款流水号【{}】查询调查流水号【{}】************", cusLstMclWhbxd.getBillNo(),accLoan.getPvpSerno(),pvpLoanApp.getSurveySerno());
                        /** 2021年10月20日16:01:43 hubp 因为无还本续贷可以做两次，因此资产负债和损益表明细数据两边都要可能存在，先插新信贷本地，无数据再去查新微贷 **/
                        //1、先查新信贷
                        String surveySerno = pvpLoanApp.getSurveySerno();
                        Xwd013RespDto xwd013RespDto = null;
                        List<LmtBsInfo> bsInfoList = lmtBsInfoService.selectBySurveySerno(surveySerno);
                        List<LmtPlListInfo> plListInfoList = lmtPlListInfoService.selectBySerno(surveySerno);
                        logger.info("根据原调查编号【{}】查询到资产负债【{}】，损益表【{}】！", surveySerno , bsInfoList.size() , plListInfoList.size());
                        if(bsInfoList.size() > 0 || plListInfoList.size() > 0){
                            xwd013RespDto = getPreAmt2(bsInfoList,plListInfoList);
                        }else {
                            Xwd013ReqDto xwd013ReqDto = new Xwd013ReqDto();
                            xwd013ReqDto.setReqId(surveySerno);
                            xwd013ReqDto.setReqType("0");
                            xwd013RespDto = xwd013Service.xwd013(xwd013ReqDto);
                            logger.info("*****根据产品代码【{}】查询到损益明细和资产负债信息【{}】************", "SC010010",JSON.toJSONString(xwd013RespDto));
                        }
                        for(CfgXdFinReport s : cfgXdFinReportList){
                            if (s.getTypeChild().equals("1")) {
                                String pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                                //插入资产负债表
                                LmtBsInfo lmtBsInfo=  new LmtBsInfo();
                                lmtBsInfo.setPkId(pkId);
                                lmtBsInfo.setSurveySerno(serno);
                                lmtBsInfo.setSubject(s.getCodeText());
                                if(!"9999".equals(xwd013RespDto.getCode())){
                                    lmtBsInfo.setPreAmt(getPreAmt(s.getCodeValue(),xwd013RespDto));
                                }
                                lmtBsInfo.setSubjectValue(s.getCodeValue());
                                lmtBsInfoService.insert(lmtBsInfo);
                                logger.info("*****XDXW0043:初始资产负债表************"+lmtBsInfo);
                            }else if (s.getTypeChild().equals("2")){
                                String pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                                LmtPlListInfo lmtPlListInfo = new LmtPlListInfo();
                                lmtPlListInfo.setPkId(pkId);
                                lmtPlListInfo.setSurveySerno(serno);
                                lmtPlListInfo.setSubject(s.getCodeText());
                                if(!"9999".equals(xwd013RespDto.getCode())){
                                    lmtPlListInfo.setPreAmt(getPreAmt1(s.getCodeValue(),xwd013RespDto));
                                }
                                lmtPlListInfo.setSubjectValue(s.getCodeValue());
                                lmtPlListInfoService.insert(lmtPlListInfo);
                                logger.info("*****XDXW0043:初始化损益明细表************"+lmtPlListInfo);
                            }else {
                                logger.info("*****XDXW0043:初始化损益明细表和资产负债表存在脏数据************");
                            }
                        };
                    }
                }
            } else if (Objects.equals(DscmsBizXwEnum.XDXW0043_FLAG_98.key, flag)) {
                logger.info("*****XDXW0043:新信贷处理逻辑： flag=“98”时，将名单处理办理状态更新为待处理****");

                // 更新办理状态为处理失败 5--待处理
                cusLstMclWhbxd.setApplyStatus("5");
                cusLstMclWhbxdService.update(cusLstMclWhbxd);
            }
            xdcz0043DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 操作成功标志位
            xdcz0043DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value);
        return xdcz0043DataRespDto;
    }

    /**
     * @param subjectValue, list
     * @return java.math.BigDecimal
     * @author hubp
     * @date 2021/10/14 20:50
     * @version 1.0.0
     * @desc  根据科目号value 获取上期金额 --资产负债
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private BigDecimal getPreAmt (String subjectValue ,Xwd013RespDto list) {
        String preAmt = StringUtils.EMPTY;
        switch (subjectValue){
            case "001":
                preAmt = list.getTotalCurrentAmount();
                break;
            case "002":
                preAmt = list.getCurrFloatAmount();
                break;
            case "003":
                preAmt = list.getBankcashCurAmount();
                break;
            case "004":
                preAmt = list.getCurrentOfReceivables();
                break;
            case "005":
                preAmt = list.getCurrentOfInventory();
                break;
            case "006":
                preAmt = list.getPrepayRentCurAmount();
                break;
            case "007":
                preAmt = list.getFixassCurrent();
                break;
            case "008":
                preAmt = list.getIndebtedCurrentAmount();
                break;
            case "009":
                preAmt = list.getBankCurrentAmount();
                break;
            case "0010":
                preAmt = list.getAccountsCurrentAmount();
                break;
            case "011":
                preAmt = list.getOtherloanCurrentAmount();
                break;
            case "012":
                preAmt = list.getOwnersCurrAmount();
                break;
        }
        return StringUtils.nonBlank(preAmt) ? new BigDecimal(preAmt) : BigDecimal.ZERO;
    }

    /**
     * @param subjectValue, list
     * @return java.math.BigDecimal
     * @author hubp
     * @date 2021/10/14 20:50
     * @version 1.0.0
     * @desc  根据科目号value 获取上期金额 --损益表明细
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private BigDecimal getPreAmt1 (String subjectValue , Xwd013RespDto list) {
        String preAmt = StringUtils.EMPTY;
        switch (subjectValue){
            case "001":
                preAmt = list.getTotalsalFullYear();
                break;
            case "002":
                preAmt = list.getVariableMatYear();
                break;
            case "003":
                preAmt = list.getGrossYearRecently();
                break;
            case "004":
                preAmt = list.getFixedManagecostFullYear();
                break;
            case "005":
                preAmt = list.getRecentlyNetProfit();
                break;
        }
        return StringUtils.nonBlank(preAmt) ? new BigDecimal(preAmt) : BigDecimal.ZERO;
    }

    /**
     * @param bsInfoList, plListInfoList
     * @return cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto
     * @author hubp
     * @date 2021/10/20 16:30
     * @version 1.0.0
     * @desc  新信贷资产负债和损益表赋值
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private  Xwd013RespDto getPreAmt2 (List<LmtBsInfo> bsInfoList , List<LmtPlListInfo> plListInfoList) {
        Xwd013RespDto xwd013RespDto = new Xwd013RespDto();
        xwd013RespDto.setCode("0000");
        String zero = StringUtils.STRING_ZERROR;
        // 只是为了循环赋值
        for(LmtBsInfo lmtBsInfo : bsInfoList){
            switch (lmtBsInfo.getSubjectValue()){
                case "001":
                    xwd013RespDto.setTotalCurrentAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "002":
                    xwd013RespDto.setCurrFloatAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "003":
                    xwd013RespDto.setBankcashCurAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "004":
                    xwd013RespDto.setCurrentOfReceivables(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "005":
                    xwd013RespDto.setCurrentOfInventory(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "006":
                    xwd013RespDto.setPrepayRentCurAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "007":
                    xwd013RespDto.setFixassCurrent(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "008":
                    xwd013RespDto.setIndebtedCurrentAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "009":
                    xwd013RespDto.setBankCurrentAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "0010":
                    xwd013RespDto.setAccountsCurrentAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "011":
                    xwd013RespDto.setOtherloanCurrentAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
                case "012":
                    xwd013RespDto.setOwnersCurrAmount(Objects.isNull(lmtBsInfo.getCurtAmt()) ? zero:lmtBsInfo.getCurtAmt().toPlainString());
                    break;
            }
        }
        for(LmtPlListInfo lmtPlListInfo : plListInfoList){
            switch (lmtPlListInfo.getSubjectValue()){
                case "001":
                    xwd013RespDto.setTotalsalFullYear(Objects.isNull(lmtPlListInfo.getNearYearAmt()) ? zero:lmtPlListInfo.getNearYearAmt().toPlainString());
                    break;
                case "002":
                    xwd013RespDto.setVariableMatYear(Objects.isNull(lmtPlListInfo.getNearYearAmt()) ? zero:lmtPlListInfo.getNearYearAmt().toPlainString());
                    break;
                case "003":
                    xwd013RespDto.setGrossYearRecently(Objects.isNull(lmtPlListInfo.getNearYearAmt()) ? zero:lmtPlListInfo.getNearYearAmt().toPlainString());
                    break;
                case "004":
                    xwd013RespDto.setFixedManagecostFullYear(Objects.isNull(lmtPlListInfo.getNearYearAmt()) ? zero:lmtPlListInfo.getNearYearAmt().toPlainString());
                    break;
                case "005":
                    xwd013RespDto.setRecentlyNetProfit(Objects.isNull(lmtPlListInfo.getNearYearAmt()) ? zero:lmtPlListInfo.getNearYearAmt().toPlainString());
                    break;
            }
        }
        return xwd013RespDto;
    }
}
