package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppSub
 * @类描述: lmt_intbank_app_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 16:09:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtIntbankAppSubDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 分项流水号 **/
	private String subSerno;
	
	/** 授信品种编号 **/
	private String lmtBizType;
	
	/** 授信品种名称 **/
	private String lmtBizTypeName;
	
	/** 变更标志 **/
	private String chgFlag;
	
	/** 原额度台账分项编号 **/
	private String origiLmtAccSubNo;
	
	/** 原额度台账分项授信金额 **/
	private java.math.BigDecimal origiLmtAccSubAmt;
	
	/** 原额度台账分项期限 **/
	private Integer origiLmtAccSubTerm;
	
	/** 授信金额 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 币种 **/
	private String curType;
	
	/** 是否循环 **/
	private String isRevolv;
	
	/** 是否涉及货币基金 **/
	private String isIvlMf;
	
	/** 货币基金总授信额度 **/
	private java.math.BigDecimal lmtMfAmt;
	
	/** 单只货币基金授信额度 **/
	private java.math.BigDecimal lmtSingleMfAmt;
	
	/** 期限 **/
	private Integer term;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno == null ? null : subSerno.trim();
	}
	
    /**
     * @return SubSerno
     */	
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType == null ? null : lmtBizType.trim();
	}
	
    /**
     * @return LmtBizType
     */	
	public String getLmtBizType() {
		return this.lmtBizType;
	}
	
	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName == null ? null : lmtBizTypeName.trim();
	}
	
    /**
     * @return LmtBizTypeName
     */	
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}
	
	/**
	 * @param chgFlag
	 */
	public void setChgFlag(String chgFlag) {
		this.chgFlag = chgFlag == null ? null : chgFlag.trim();
	}
	
    /**
     * @return ChgFlag
     */	
	public String getChgFlag() {
		return this.chgFlag;
	}
	
	/**
	 * @param origiLmtAccSubNo
	 */
	public void setOrigiLmtAccSubNo(String origiLmtAccSubNo) {
		this.origiLmtAccSubNo = origiLmtAccSubNo == null ? null : origiLmtAccSubNo.trim();
	}
	
    /**
     * @return OrigiLmtAccSubNo
     */	
	public String getOrigiLmtAccSubNo() {
		return this.origiLmtAccSubNo;
	}
	
	/**
	 * @param origiLmtAccSubAmt
	 */
	public void setOrigiLmtAccSubAmt(java.math.BigDecimal origiLmtAccSubAmt) {
		this.origiLmtAccSubAmt = origiLmtAccSubAmt;
	}
	
    /**
     * @return OrigiLmtAccSubAmt
     */	
	public java.math.BigDecimal getOrigiLmtAccSubAmt() {
		return this.origiLmtAccSubAmt;
	}
	
	/**
	 * @param origiLmtAccSubTerm
	 */
	public void setOrigiLmtAccSubTerm(Integer origiLmtAccSubTerm) {
		this.origiLmtAccSubTerm = origiLmtAccSubTerm;
	}
	
    /**
     * @return OrigiLmtAccSubTerm
     */	
	public Integer getOrigiLmtAccSubTerm() {
		return this.origiLmtAccSubTerm;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv == null ? null : isRevolv.trim();
	}
	
    /**
     * @return IsRevolv
     */	
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param isIvlMf
	 */
	public void setIsIvlMf(String isIvlMf) {
		this.isIvlMf = isIvlMf == null ? null : isIvlMf.trim();
	}
	
    /**
     * @return IsIvlMf
     */	
	public String getIsIvlMf() {
		return this.isIvlMf;
	}
	
	/**
	 * @param lmtMfAmt
	 */
	public void setLmtMfAmt(java.math.BigDecimal lmtMfAmt) {
		this.lmtMfAmt = lmtMfAmt;
	}
	
    /**
     * @return LmtMfAmt
     */	
	public java.math.BigDecimal getLmtMfAmt() {
		return this.lmtMfAmt;
	}
	
	/**
	 * @param lmtSingleMfAmt
	 */
	public void setLmtSingleMfAmt(java.math.BigDecimal lmtSingleMfAmt) {
		this.lmtSingleMfAmt = lmtSingleMfAmt;
	}
	
    /**
     * @return LmtSingleMfAmt
     */	
	public java.math.BigDecimal getLmtSingleMfAmt() {
		return this.lmtSingleMfAmt;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return Term
     */	
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}