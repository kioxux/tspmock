/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0059.req.CmisLmt0059ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0059.resp.CmisLmt0059RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplySubPrdMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.RptLastLmtAppSubPrdMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLastLmtAppSubPrdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-25 20:19:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLastLmtAppSubPrdService {
    private static final Logger log = LoggerFactory.getLogger(RptLastLmtAppSubPrdService.class);
    @Autowired
    private RptLastLmtAppSubPrdMapper rptLastLmtAppSubPrdMapper;
    @Autowired
    private LmtReplyService lmtReplyService;
    @Autowired
    private LmtReplySubService lmtReplySubService;
    @Autowired
    private LmtReplySubPrdMapper lmtReplySubPrdMapper;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;
    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;
    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;
    @Autowired
    private LmtAppService lmtAppService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLastLmtAppSubPrd selectByPrimaryKey(String pkId, String serno) {
        return rptLastLmtAppSubPrdMapper.selectByPrimaryKey(pkId, serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLastLmtAppSubPrd> selectAll(QueryModel model) {
        List<RptLastLmtAppSubPrd> records = (List<RptLastLmtAppSubPrd>) rptLastLmtAppSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLastLmtAppSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLastLmtAppSubPrd> list = rptLastLmtAppSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLastLmtAppSubPrd record) {
        return rptLastLmtAppSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLastLmtAppSubPrd record) {
        return rptLastLmtAppSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLastLmtAppSubPrd record) {
        return rptLastLmtAppSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLastLmtAppSubPrd record) {
        return rptLastLmtAppSubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return rptLastLmtAppSubPrdMapper.deleteByPrimaryKey(pkId, serno);
    }

    public List<Map> initLastLmt(QueryModel model) {
        if (model.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String type = model.getCondition().get("type").toString();
        String serno1 = model.getCondition().get("serno").toString();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno1);
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtApp.getOrigiLmtReplySerno());
        String serno = lmtReply.getSerno();
        List<RptLastLmtAppSubPrd> rptLastLmtAppSubPrds = selectBySerno(serno1);
        if (CollectionUtils.isEmpty(rptLastLmtAppSubPrds)) {
            List<LmtReplySub> lmtReplySubList = null;
            if ("normal".equals(type)) {
                lmtReplySubList = lmtReplySubService.getNormal(serno);
            } else if ("low".equals(type)) {
                lmtReplySubList = lmtReplySubService.getLow(serno);
            }
            if (CollectionUtils.nonEmpty(lmtReplySubList)) {
                for (LmtReplySub lmtReplySub : lmtReplySubList) {
                    String replySubSerno = lmtReplySub.getReplySubSerno();
                    List<LmtReplySubPrd> lmtReplySubPrdList = new ArrayList<>();
                    if ("normal".equals(type)) {
                        lmtReplySubPrdList = lmtReplySubPrdMapper.selectByReplySubSerno(replySubSerno);
                    } else if ("low".equals(type)) {
                        lmtReplySubPrdList = lmtReplySubPrdMapper.selectLastByReplySubSerno(replySubSerno);
                    }
                    List<RptLastLmtAppSubPrd> children = new ArrayList<>();
                    BigDecimal subLoanBalance = BigDecimal.ZERO;
                    if (CollectionUtils.nonEmpty(lmtReplySubPrdList)) {
                        User userInfo = SessionUtils.getUserInformation();
                        String instuCode = CmisCommonUtils.getInstucde(userInfo.getOrg().getCode());
                        for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                            HashMap<String, String> queryMap = new HashMap<String, String>();
                            queryMap.put("replySubPrdSerno", lmtReplySubPrd.getReplySubPrdSerno());
                            List<LmtReplyAccSubPrd> lmtReplyAccSubPrds = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(queryMap);
                            String accSubPrdNo = "";
                            if (CollectionUtils.nonEmpty(lmtReplyAccSubPrds)) {
                                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrds.get(0);
                                accSubPrdNo = lmtReplyAccSubPrd.getAccSubPrdNo();
                            }

                            RptLastLmtAppSubPrd rptLastLmtAppSubPrd = new RptLastLmtAppSubPrd();
                            // 目前用信净额 contAmt
                            if (StringUtils.nonBlank(accSubPrdNo)) {
                                CmisLmt0059ReqDto cmisLmt0059ReqDto = new CmisLmt0059ReqDto();
                                cmisLmt0059ReqDto.setInstuCde(instuCode);
                                cmisLmt0059ReqDto.setApprSubSerno(accSubPrdNo);
                                String guarMode = lmtReplySubPrd.getGuarMode();
                                log.info("获取分项品种编号对应的用信净额,请求报文:" + JSON.toJSONString(cmisLmt0059ReqDto));
                                CmisLmt0059RespDto cmisLmt0059RespDto = cmisLmtClientService.cmislmt0059(cmisLmt0059ReqDto).getData();
                                log.info("获取分项品种编号对应的用信净额,响应报文:" + JSON.toJSONString(cmisLmt0059RespDto));
                                BigDecimal loanBalance = BigDecimal.ZERO;
                                if (cmisLmt0059RespDto.getErrorCode().equals(EcbEnum.ECB010000.key)) {
                                    //低风险取用信余额
                                    if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode)){
                                        loanBalance = cmisLmt0059RespDto.getSubLoanBal();
                                    }else {
                                        //一般额度取用信敞口余额
                                        loanBalance = cmisLmt0059RespDto.getSubSpacLoanBal();
                                    }
                                }
                                subLoanBalance = subLoanBalance.add(loanBalance);
                                rptLastLmtAppSubPrd.setLoanBalance(loanBalance);
                            }else {
                                rptLastLmtAppSubPrd.setLoanBalance(BigDecimal.ZERO);
                            }
                            rptLastLmtAppSubPrd.setPkId(StringUtils.getUUID());
                            rptLastLmtAppSubPrd.setSerno(serno1);
                            rptLastLmtAppSubPrd.setSubSerno(lmtReplySub.getReplySubSerno());
                            rptLastLmtAppSubPrd.setSubPrdSerno(lmtReplySubPrd.getReplySubPrdSerno());
                            rptLastLmtAppSubPrd.setLmtBizType(lmtReplySubPrd.getLmtBizType());
                            rptLastLmtAppSubPrd.setLmtBizTypeName(lmtReplySubPrd.getLmtBizTypeName());
                            rptLastLmtAppSubPrd.setLmtBizTypeProp(lmtReplySubPrd.getLmtBizTypeProp());
                            rptLastLmtAppSubPrd.setLmtAmt(lmtReplySubPrd.getLmtAmt());
                            rptLastLmtAppSubPrd.setLmtTerm(lmtReplySubPrd.getLmtTerm());
                            rptLastLmtAppSubPrd.setRateYear(lmtReplySubPrd.getRateYear());
                            rptLastLmtAppSubPrd.setBailPreRate(lmtReplySubPrd.getBailPreRate());
                            rptLastLmtAppSubPrd.setGuarMode(lmtReplySubPrd.getGuarMode());
                            insert(rptLastLmtAppSubPrd);
                            children.add(rptLastLmtAppSubPrd);
                        }
                    }
                    tempMap = new HashMap();
                    String pkId = StringUtils.getUUID();
                    RptLastLmtAppSubPrd lastSub = new RptLastLmtAppSubPrd();
                    lastSub.setPkId(pkId);
                    lastSub.setSerno(serno1);
                    lastSub.setSubSerno(lmtReplySub.getReplySubSerno());
                    lastSub.setLmtBizTypeName(lmtReplySub.getSubName());
                    lastSub.setLmtTerm(lmtReplySub.getLmtTerm());
                    lastSub.setLmtAmt(lmtReplySub.getLmtAmt());
                    lastSub.setGuarMode(lmtReplySub.getGuarMode());
                    lastSub.setLoanBalance(subLoanBalance);
                    insert(lastSub);
                    tempMap.put("pkId", pkId);
                    tempMap.put("serno", serno);
                    tempMap.put("subPrdSerno", lmtReplySub.getReplySubSerno());
                    tempMap.put("lmtBizTypeName", lmtReplySub.getSubName());
                    tempMap.put("lmtAmt", lmtReplySub.getLmtAmt());
                    tempMap.put("lmtTerm", lmtReplySub.getLmtTerm());
                    tempMap.put("guarMode", lmtReplySub.getGuarMode());
                    tempMap.put("loanBalance", subLoanBalance);
                    tempMap.put("children", children);
                    list.add(tempMap);

                }
            }
            return list;
        } else {
            for (RptLastLmtAppSubPrd rptLastLmtAppSubPrd : rptLastLmtAppSubPrds) {
                if (StringUtils.nonBlank(rptLastLmtAppSubPrd.getSubSerno()) && StringUtils.isBlank(rptLastLmtAppSubPrd.getSubPrdSerno())) {
                    QueryModel model2 = new QueryModel();
                    model2.addCondition("serno", serno1);
                    model2.addCondition("subSerno", rptLastLmtAppSubPrd.getSubSerno());
                    List<RptLastLmtAppSubPrd> children = new ArrayList<>();
                    List<RptLastLmtAppSubPrd> tempList = selectByModel(model2);
                    if (CollectionUtils.nonEmpty(tempList)) {
                        for (RptLastLmtAppSubPrd lastLmtAppSubPrd : tempList) {
                            if (StringUtils.nonBlank(lastLmtAppSubPrd.getSubPrdSerno())) {
                                children.add(lastLmtAppSubPrd);
                            }
                        }
                    }
                    if ("normal".equals(type)) {
                        if (CommonConstance.GUAR_MODE_60.equals(rptLastLmtAppSubPrd.getGuarMode())) {
                            continue;
                        }
                    } else if ("low".equals(type)) {
                        if (!CommonConstance.GUAR_MODE_60.equals(rptLastLmtAppSubPrd.getGuarMode())) {
                            continue;
                        }
                    }
                    tempMap = new HashMap();
                    tempMap.put("pkId", rptLastLmtAppSubPrd.getPkId());
                    tempMap.put("serno", serno1);
                    tempMap.put("subPrdSerno", rptLastLmtAppSubPrd.getSubSerno());
                    tempMap.put("lmtBizTypeName", rptLastLmtAppSubPrd.getLmtBizTypeName());
                    tempMap.put("lmtAmt", rptLastLmtAppSubPrd.getLmtAmt());
                    tempMap.put("lmtTerm", rptLastLmtAppSubPrd.getLmtTerm());
                    tempMap.put("guarMode", rptLastLmtAppSubPrd.getGuarMode());
                    tempMap.put("loanBalance", rptLastLmtAppSubPrd.getLoanBalance());
                    tempMap.put("children", children);
                    list.add(tempMap);
                }
            }
            return list;
        }
    }

    public List<Map> initGrpLastLmt(QueryModel model) {
        if (model.getCondition() == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String type = model.getCondition().get("type").toString();
        String grpSerno1 = model.getCondition().get("grpSerno").toString();
        List<Map> list = new ArrayList<>();
        Map tempMap = null;
        List<LmtApp> lmtAppByGrpSernoIsDeclare = lmtAppService.getLmtAppByGrpSernoIsDeclare(grpSerno1);
        if (CollectionUtils.nonEmpty(lmtAppByGrpSernoIsDeclare)) {
            for (LmtApp lmtApp : lmtAppByGrpSernoIsDeclare) {
                String serno = lmtApp.getSerno();
                List<RptLastLmtAppSubPrd> rptLastLmtAppSubPrds = selectBySerno(serno);
                if (CollectionUtils.isEmpty(rptLastLmtAppSubPrds)) {
                    LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtApp.getOrigiLmtReplySerno());
                    String serno1 = lmtReply.getSerno();
                    List<LmtReplySub> lmtReplySubList = null;
                    if ("normal".equals(type)) {
                        lmtReplySubList = lmtReplySubService.getNormal(serno1);
                    } else if ("low".equals(type)) {
                        lmtReplySubList = lmtReplySubService.getLow(serno1);
                    }
                    if (CollectionUtils.nonEmpty(lmtReplySubList)) {
                        for (LmtReplySub lmtReplySub : lmtReplySubList) {
                            String replySubSerno = lmtReplySub.getReplySubSerno();
                            List<LmtReplySubPrd> lmtReplySubPrdList = new ArrayList<>();
                            if ("normal".equals(type)) {
                                lmtReplySubPrdList = lmtReplySubPrdMapper.selectByReplySubSerno(replySubSerno);
                            } else if ("low".equals(type)) {
                                lmtReplySubPrdList = lmtReplySubPrdMapper.selectLastByReplySubSerno(replySubSerno);
                            }
                            List<RptLastLmtAppSubPrd> children = new ArrayList<>();
                            BigDecimal subLoanBalance = BigDecimal.ZERO;
                            if (CollectionUtils.nonEmpty(lmtReplySubPrdList)) {
                                User userInfo = SessionUtils.getUserInformation();
                                String instuCode = CmisCommonUtils.getInstucde(userInfo.getOrg().getCode());
                                for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                                    HashMap<String, String> queryMap = new HashMap<String, String>();
                                    queryMap.put("replySubPrdSerno", lmtReplySubPrd.getReplySubPrdSerno());
                                    List<LmtReplyAccSubPrd> lmtReplyAccSubPrds = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(queryMap);
                                    String accSubPrdNo = "";
                                    if (CollectionUtils.nonEmpty(lmtReplyAccSubPrds)) {
                                        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrds.get(0);
                                        accSubPrdNo = lmtReplyAccSubPrd.getAccSubPrdNo();
                                    }

                                    RptLastLmtAppSubPrd rptLastLmtAppSubPrd = new RptLastLmtAppSubPrd();
                                    // 目前用信净额 contAmt
                                    if (StringUtils.nonBlank(accSubPrdNo)) {
                                        CmisLmt0059ReqDto cmisLmt0059ReqDto = new CmisLmt0059ReqDto();
                                        cmisLmt0059ReqDto.setInstuCde(instuCode);
                                        cmisLmt0059ReqDto.setApprSubSerno(lmtReplySubPrd.getOrigiLmtAccSubPrdNo());
                                        log.info("获取分项品种编号对应的用信净额,请求报文:" + JSON.toJSONString(cmisLmt0059ReqDto));
                                        CmisLmt0059RespDto cmisLmt0059RespDto = cmisLmtClientService.cmislmt0059(cmisLmt0059ReqDto).getData();
                                        log.info("获取分项品种编号对应的用信净额,响应报文:" + JSON.toJSONString(cmisLmt0059RespDto));
                                        BigDecimal loanBalance = BigDecimal.ZERO;
                                        if (cmisLmt0059RespDto.getErrorCode().equals(EcbEnum.ECB010000.key)) {
                                            loanBalance = cmisLmt0059RespDto.getSubLoanBal();
                                        }
                                        subLoanBalance = subLoanBalance.add(loanBalance);
                                        rptLastLmtAppSubPrd.setLoanBalance(loanBalance);
                                    }
                                    rptLastLmtAppSubPrd.setPkId(StringUtils.getUUID());
                                    rptLastLmtAppSubPrd.setSerno(serno);
                                    rptLastLmtAppSubPrd.setSubSerno(lmtReplySub.getReplySubSerno());
                                    rptLastLmtAppSubPrd.setSubPrdSerno(lmtReplySubPrd.getReplySubPrdSerno());
                                    rptLastLmtAppSubPrd.setLmtBizType(lmtReplySubPrd.getLmtBizType());
                                    rptLastLmtAppSubPrd.setLmtBizTypeName(lmtReplySubPrd.getLmtBizTypeName());
                                    rptLastLmtAppSubPrd.setLmtBizTypeProp(lmtReplySubPrd.getLmtBizTypeProp());
                                    rptLastLmtAppSubPrd.setLmtAmt(lmtReplySubPrd.getLmtAmt());
                                    rptLastLmtAppSubPrd.setLmtTerm(lmtReplySubPrd.getLmtTerm());
                                    rptLastLmtAppSubPrd.setRateYear(lmtReplySubPrd.getRateYear());
                                    rptLastLmtAppSubPrd.setBailPreRate(lmtReplySubPrd.getBailPreRate());
                                    rptLastLmtAppSubPrd.setGuarMode(lmtReplySubPrd.getGuarMode());
                                    insert(rptLastLmtAppSubPrd);
                                    children.add(rptLastLmtAppSubPrd);
                                }
                            }
                            tempMap = new HashMap();
                            String pkId = StringUtils.getUUID();
                            RptLastLmtAppSubPrd lastSub = new RptLastLmtAppSubPrd();
                            lastSub.setPkId(pkId);
                            lastSub.setSerno(serno);
                            lastSub.setSubSerno(lmtReplySub.getReplySubSerno());
                            lastSub.setLmtBizTypeName(lmtReplySub.getSubName());
                            lastSub.setLmtTerm(lmtReplySub.getLmtTerm());
                            lastSub.setLmtAmt(lmtReplySub.getLmtAmt());
                            lastSub.setGuarMode(lmtReplySub.getGuarMode());
                            lastSub.setLoanBalance(subLoanBalance);
                            insert(lastSub);
                            tempMap.put("pkId", pkId);
                            tempMap.put("serno", serno);
                            tempMap.put("cusName", lmtReplySub.getCusName());
                            tempMap.put("subPrdSerno", lmtReplySub.getReplySubSerno());
                            tempMap.put("lmtBizTypeName", lmtReplySub.getSubName());
                            tempMap.put("lmtAmt", lmtReplySub.getLmtAmt());
                            tempMap.put("lmtTerm", lmtReplySub.getLmtTerm());
                            tempMap.put("guarMode", lmtReplySub.getGuarMode());
                            tempMap.put("loanBalance", subLoanBalance);
                            tempMap.put("children", children);
                            list.add(tempMap);

                        }
                    }
                    return list;
                } else {
                    for (RptLastLmtAppSubPrd rptLastLmtAppSubPrd : rptLastLmtAppSubPrds) {
                        if (StringUtils.nonBlank(rptLastLmtAppSubPrd.getSubSerno()) && StringUtils.isBlank(rptLastLmtAppSubPrd.getSubPrdSerno())) {
                            QueryModel model2 = new QueryModel();
                            model2.addCondition("serno", serno);
                            model2.addCondition("subSerno", rptLastLmtAppSubPrd.getSubSerno());
                            List<RptLastLmtAppSubPrd> children = new ArrayList<>();
                            List<RptLastLmtAppSubPrd> tempList = selectByModel(model2);
                            if (CollectionUtils.nonEmpty(tempList)) {
                                for (RptLastLmtAppSubPrd lastLmtAppSubPrd : tempList) {
                                    if (StringUtils.nonBlank(lastLmtAppSubPrd.getSubPrdSerno())) {
                                        children.add(lastLmtAppSubPrd);
                                    }
                                }
                            }
                            if ("normal".equals(type)) {
                                if (CommonConstance.GUAR_MODE_60.equals(rptLastLmtAppSubPrd.getGuarMode())) {
                                    continue;
                                }
                            } else if ("low".equals(type)) {
                                if (!CommonConstance.GUAR_MODE_60.equals(rptLastLmtAppSubPrd.getGuarMode())) {
                                    continue;
                                }
                            }
                            tempMap = new HashMap();
                            tempMap.put("pkId", rptLastLmtAppSubPrd.getPkId());
                            tempMap.put("serno", serno);
                            tempMap.put("cusName", lmtApp.getCusName());
                            tempMap.put("subPrdSerno", rptLastLmtAppSubPrd.getSubSerno());
                            tempMap.put("lmtBizTypeName", rptLastLmtAppSubPrd.getLmtBizTypeName());
                            tempMap.put("lmtAmt", rptLastLmtAppSubPrd.getLmtAmt());
                            tempMap.put("lmtTerm", rptLastLmtAppSubPrd.getLmtTerm());
                            tempMap.put("guarMode", rptLastLmtAppSubPrd.getGuarMode());
                            tempMap.put("loanBalance", rptLastLmtAppSubPrd.getLoanBalance());
                            tempMap.put("children", children);
                            list.add(tempMap);
                        }
                    }
                }
            }
        }
        return list;
    }

    public List<RptLastLmtAppSubPrd> selectBySerno(String serno) {
        return rptLastLmtAppSubPrdMapper.selectBySerno(serno);
    }
}
