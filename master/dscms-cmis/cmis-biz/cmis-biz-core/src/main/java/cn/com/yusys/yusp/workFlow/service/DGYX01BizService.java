package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;


@Service
public class DGYX01BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX01BizService.class);//定义log

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;

    @Autowired
    private IqpHighAmtAgrAppMapper iqpHighAmtAgrAppMapper;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;

    @Autowired
    private IqpTfLocAppMapper iqpTfLocAppMapper;

    @Autowired
    private IqpAccpAppMapper iqpAccpAppMapper;

    @Autowired
    private IqpCvrgAppMapper iqpCvrgAppMapper;

    @Autowired
    private IqpEntrustLoanAppMapper iqpEntrustLoanAppMapper;

    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX001.equals(bizType)) {
            handleYX001Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType)) {
            handleYX002Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX003.equals(bizType)) {
            handleYX003Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX004.equals(bizType)) {
            handleYX004Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX005.equals(bizType)) {
            handleYX005Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX006.equals(bizType)) {
            handleYX006Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX007.equals(bizType)) {
            handleYX007Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)) {
            handleYX008Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_BG009.equals(bizType)) {
            //担保变更
            bgyw03Bizservice.bizOp(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    // 最高额授信协议申请
    private void handleYX001Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("最高额授信协议申请" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpHighAmtAgrAppService.put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                log.info("最高额授信协议申请" + serno + "判断是否是低风险业务传递参数");
                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpHighAmtAgrAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpHighAmtAgrApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX001, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpHighAmtAgrApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX001, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpHighAmtAgrAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
                String managerId = iqpHighAmtAgrApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpHighAmtAgrApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }

                // 首页消息提醒
                iqpHighAmtAgrAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpHighAmtAgrAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    // 流程参数传递
                    iqpHighAmtAgrAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpHighAmtAgrAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpHighAmtAgrAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
                    String managerId = iqpHighAmtAgrApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpHighAmtAgrApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpHighAmtAgrAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpHighAmtAgrAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("最高额授信协议申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpHighAmtAgrAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                iqpHighAmtAgrAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn("最高额授信协议申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 普通贷款业务申请
    private void handleYX002Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("普通贷款业务申请" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpLoanAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX002, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX002, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpLoanAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                String managerId = iqpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpLoanApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpLoanAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                    String managerId = iqpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpLoanApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("普通贷款业务申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpLoanAppService.handleBusinessAfterRefuse(serno);
//                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("普通贷款业务申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("普通贷款业务申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 贸易融资贷款申请流程
    private void handleYX003Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("贸易融资贷款申请流程" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpLoanAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX003, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX003, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpLoanAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                String managerId = iqpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpLoanApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpLoanAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                    String managerId = iqpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpLoanApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("贸易融资贷款申请流程" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("贸易融资贷款申请流程" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("贸易融资贷款申请流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 福费廷贷款申请流程
    private void handleYX004Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("福费廷贷款申请流程" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();
                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpLoanAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX004, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX004, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpLoanAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                String managerId = iqpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpLoanApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpLoanAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
                    String managerId = iqpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpLoanApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                    // 流程参数传递
                    iqpLoanAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("福费廷贷款申请流程" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("福费廷贷款申请流程" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("福费廷贷款申请流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 开证申请流程
    private void handleYX005Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("开证申请流程" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpTfLocAppService.put2VarParam(resultInstanceDto, serno);
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpTfLocAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX005, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpTfLocApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX005, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                iqpTfLocAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                IqpTfLocApp iqpTfLocApp1 = iqpTfLocAppService.selectByTfLocSernoKey(serno);
                String managerId = iqpTfLocApp1.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpTfLocApp1.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpTfLocAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpTfLocAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    // 流程参数传递
                    iqpTfLocAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpTfLocAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpTfLocAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    IqpTfLocApp iqpTfLocApp1 = iqpTfLocAppService.selectByTfLocSernoKey(serno);
                    String managerId = iqpTfLocApp1.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpTfLocApp1.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpTfLocAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpTfLocAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("开证申请流程" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpTfLocApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
//                iqpTfLocAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                iqpTfLocAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn("开证申请流程" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("开证申请流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 银承贷款申请流程
    private void handleYX006Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("银承贷款申请流程" + serno + "流程操作");
        try {
            // 流程参数传递
            iqpAccpAppService.put2VarParam(resultInstanceDto, serno);
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByIqpSerno(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpAccpAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX006, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpAccpApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX006, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpAccpAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                String managerId = iqpAccpApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpAccpApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpAccpAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpAccpAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    // 流程参数传递
                    iqpAccpAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAccpAppService.handleBusinessAfterCallBack(serno);
                    // 流程参数传递
                    iqpAccpAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    String managerId = iqpAccpApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpAccpApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType,managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpAccpAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpAccpAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("银承业务申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
//                iqpAccpAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                iqpAccpAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn("银承业务申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("银承业务申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 保函贷款申请流程
    private void handleYX007Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("保函贷款申请流程" + serno + "流程操作");
        try {
            iqpCvrgAppService.put2VarParam(resultInstanceDto, serno);
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpCvrgAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX007, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpCvrgApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX007, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpCvrgAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                String managerId = iqpCvrgApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpCvrgApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpCvrgAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpCvrgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpCvrgAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    iqpCvrgAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpCvrgAppService.handleBusinessAfterCallBack(serno);
                    iqpCvrgAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    String managerId = iqpCvrgApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpCvrgApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpCvrgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpCvrgAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpCvrgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpCvrgAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("保函业务申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpCvrgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
//                iqpCvrgAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                iqpCvrgAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn("保函业务申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("保函业务申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 委托贷款申请流程
    private void handleYX008Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("委托贷款申请流程" + serno + "流程操作");
        try {
            iqpEntrustLoanAppService.put2VarParam(resultInstanceDto, serno);
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "流程发起操作，流程参数" + resultInstanceDto);

            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                String currNodeId = resultInstanceDto.getCurrentNodeId();

                if (CmisBizConstants.DGYX01_START.equals(currNodeId)) {
                    //发起节点执行下面的逻辑
                    iqpEntrustLoanAppService.handleBusinessDataAfterStart(serno);
                }
                // 如果提交节点是首个节点
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)) {
                    createUrgentTask4IqpLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX008, resultInstanceDto.getInstanceId(), resultInstanceDto);
                }
                if ("237_8".equals(currNodeId)) { // 信贷管理部用信审核岗
                    createCentralFileTask4IqpEntrustLoanApp(serno, CmisFlowConstants.FLOW_TYPE_TYPE_YX008, resultInstanceDto);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("委托贷款务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpEntrustLoanAppService.handleBusinessDataAfterEnd(serno);
                //微信通知
                String managerId = iqpEntrustLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpEntrustLoanApp.getCusName());
                        paramMap.put("prdName", "一般合同申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                // 首页消息提醒
                iqpEntrustLoanAppService.sendOnlinePldRemind(resultInstanceDto, serno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpEntrustLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
                    iqpEntrustLoanAppService.put2VarParam(resultInstanceDto, serno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "打回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpEntrustLoanAppService.handleBusinessAfterCallBack(serno);
                    iqpEntrustLoanAppService.put2VarParam(resultInstanceDto, serno);
                    //微信通知
                    String managerId = iqpEntrustLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpEntrustLoanApp.getCusName());
                            paramMap.put("prdName", "一般合同申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }

            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "拿回操作，流程参数：" + resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpEntrustLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpEntrustLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("委托贷款业务申请" + serno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
//                iqpEntrustLoanAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
                iqpEntrustLoanAppService.handleBusinessAfterRefuse(serno);
            } else {
                log.warn("委托贷款业务申请" + serno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("委托贷款申请流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGYX01.equals(flowCode);
    }

    public void createCentralFileTask4IqpHighAmtAgrApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
        createCentralFileTask(serno, iqpHighAmtAgrApp.getCusId(), iqpHighAmtAgrApp.getCusName(),
                iqpHighAmtAgrApp.getInputId(), iqpHighAmtAgrApp.getInputBrId(), bizType, iqpHighAmtAgrApp.getContNo(), resultInstanceDto);
    }

    public void createCentralFileTask4IqpLoanApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(serno);
        createCentralFileTask(serno, iqpLoanApp.getCusId(), iqpLoanApp.getCusName(),
                iqpLoanApp.getInputId(), iqpLoanApp.getInputBrId(), bizType, iqpLoanApp.getContNo(), resultInstanceDto);
    }

    public void createCentralFileTask4IqpTfLocApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(serno);
        createCentralFileTask(serno, iqpTfLocApp.getCusId(), iqpTfLocApp.getCusName(),
                iqpTfLocApp.getInputId(), iqpTfLocApp.getInputBrId(), bizType, iqpTfLocApp.getContNo(), resultInstanceDto);
    }

    public void createCentralFileTask4IqpAccpApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(serno);
        createCentralFileTask(serno, iqpAccpApp.getCusId(), iqpAccpApp.getCusName(),
                iqpAccpApp.getInputId(), iqpAccpApp.getInputBrId(), bizType, iqpAccpApp.getContNo(), resultInstanceDto);
    }

    public void createCentralFileTask4IqpCvrgApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpCvrgApp iqpCrvgApp = iqpCvrgAppMapper.selectByCvrgSernoKey(serno);        //3、新增临时档案任务
        createCentralFileTask(serno, iqpCrvgApp.getCusId(), iqpCrvgApp.getCusName(),
                iqpCrvgApp.getInputId(), iqpCrvgApp.getInputBrId(), bizType, iqpCrvgApp.getContNo(), resultInstanceDto);
    }

    public void createCentralFileTask4IqpEntrustLoanApp(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(serno);
        createCentralFileTask(serno, iqpEntrustLoanApp.getCusId(), iqpEntrustLoanApp.getCusName(),
                iqpEntrustLoanApp.getInputId(), iqpEntrustLoanApp.getInputBrId(), bizType, iqpEntrustLoanApp.getContNo(), resultInstanceDto);
    }


    public void createUrgentTask4IqpHighAmtAgrApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(serno);
        if (Objects.nonNull(iqpHighAmtAgrApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpHighAmtAgrApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpHighAmtAgrApp.getGuarMode()))) {
            createUrgentTask(serno, iqpHighAmtAgrApp.getCusId(), iqpHighAmtAgrApp.getCusName(), bizType, instanceId,iqpHighAmtAgrApp.getInputId(),iqpHighAmtAgrApp.getInputBrId());
        }
    }

    public void createUrgentTask4IqpLoanApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(serno);
        if (Objects.nonNull(iqpLoanApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanApp.getGuarWay()))) {
            createUrgentTask(serno, iqpLoanApp.getCusId(), iqpLoanApp.getCusName(), bizType, instanceId,iqpLoanApp.getInputId(), iqpLoanApp.getInputBrId());
        }
    }

    public void createUrgentTask4IqpTfLocApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpTfLocApp iqpTfLocApp = iqpTfLocAppMapper.selectByTfLocSernoKey(serno);
        if (Objects.nonNull(iqpTfLocApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpTfLocApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpTfLocApp.getGuarMode()))) {
            createUrgentTask(serno, iqpTfLocApp.getCusId(), iqpTfLocApp.getCusName(), bizType, instanceId,iqpTfLocApp.getInputId(), iqpTfLocApp.getInputBrId());
        }
    }

    public void createUrgentTask4IqpAccpApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(serno);
        if (Objects.nonNull(iqpAccpApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpAccpApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpAccpApp.getGuarMode()))) {
            createUrgentTask(serno, iqpAccpApp.getCusId(), iqpAccpApp.getCusName(), bizType, instanceId,iqpAccpApp.getInputId(), iqpAccpApp.getInputBrId());
        }
    }

    public void createUrgentTask4IqpCvrgApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpCvrgApp iqpCrvgApp = iqpCvrgAppMapper.selectByCvrgSernoKey(serno);        //3、新增临时档案任务
        if (Objects.nonNull(iqpCrvgApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpCrvgApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpCrvgApp.getGuarMode()))) {
            createUrgentTask(serno, iqpCrvgApp.getCusId(), iqpCrvgApp.getCusName(), bizType, instanceId,iqpCrvgApp.getInputId(), iqpCrvgApp.getInputBrId());
        }
    }

    public void createUrgentTask4IqpEntrustLoanApp(String serno, String bizType, String instanceId, ResultInstanceDto resultInstanceDto) {
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppMapper.selectByEntrustLoanSernoKey(serno);
        if (Objects.nonNull(iqpEntrustLoanApp)
                && (CmisCommonConstants.GUAR_MODE_21.equals(iqpEntrustLoanApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpEntrustLoanApp.getGuarMode()))) {
            createUrgentTask(serno, iqpEntrustLoanApp.getCusId(), iqpEntrustLoanApp.getCusName(), bizType, instanceId,iqpEntrustLoanApp.getInputId(), iqpEntrustLoanApp.getInputBrId());
        }
    }

    public void createCentralFileTask(String serno, String cusId, String cusName, String inputId, String inputBrId, String bizType, String contNo, ResultInstanceDto resultInstanceDto) {
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(serno);
        centralFileTaskdto.setTraceId(contNo);
        centralFileTaskdto.setCusId(cusId);
        centralFileTaskdto.setCusName(cusName);
        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
        centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
        centralFileTaskdto.setInputId(inputId);
        centralFileTaskdto.setInputBrId(inputBrId);
        centralFileTaskdto.setOptType("01"); // 纯指令
        centralFileTaskdto.setTaskType("01"); // 档案接收
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    public void createUrgentTask(String serno, String cusId, String cusName, String bizType, String instanceId,String inputId, String inputBrId) {
        TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
        taskUrgentAppDto.setBizType(bizType);
        taskUrgentAppDto.setCusId(cusId);
        taskUrgentAppDto.setCusName(cusName);
        taskUrgentAppDto.setSerno(serno);
        taskUrgentAppDto.setPwbrSerno(instanceId);
        taskUrgentAppDto.setUrgentType("3");
        taskUrgentAppDto.setUrgentResn("系统加急");
        taskUrgentAppDto.setManagerBrId(inputBrId);
        taskUrgentAppDto.setManagerId(inputId);
        taskUrgentAppDto.setInputBrId(inputBrId);
        taskUrgentAppDto.setInputId(inputId);
        taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
        taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
        ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
        if (!"0".equals(result.getCode()) || 1 != result.getData()) {
            throw BizException.error(null, "999999", "新增系统加急记录失败！");
        }
    }
}
