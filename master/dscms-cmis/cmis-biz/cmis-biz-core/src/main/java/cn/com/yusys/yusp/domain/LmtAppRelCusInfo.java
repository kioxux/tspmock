/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusInfo
 * @类描述: lmt_app_rel_cus_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 16:35:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_app_rel_cus_info")
public class LmtAppRelCusInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户大类 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String cusCatalog;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 关联客户性质 **/
	@Column(name = "CUS_CHA", unique = false, nullable = true, length = 200)
	private String cusCha;
	
	/** 同业机构类型 **/
	@Column(name = "INTBANK_ORG_TYPE", unique = false, nullable = true, length = 10)
	private String intbankOrgType;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 金融业务许可证 **/
	@Column(name = "BUSI_LIC", unique = false, nullable = true, length = 80)
	private String busiLic;
	
	/** 是否上市 **/
	@Column(name = "IS_STOCK", unique = false, nullable = true, length = 5)
	private String isStock;
	
	/** 注册资金币种 **/
	@Column(name = "REGI_CAP_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String regiCapCurType;
	
	/** 注册资金 **/
	@Column(name = "REGI_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCap;
	
	/** 登记注册类型 **/
	@Column(name = "REGI_TYPE", unique = false, nullable = true, length = 5)
	private String regiType;
	
	/** 注册地行政区划 **/
	@Column(name = "REGI_AREA_CODE", unique = false, nullable = true, length = 12)
	private String regiAreaCode;
	
	/** 注册地行政区划名称 **/
	@Column(name = "REGI_AREA_CODE_NAME", unique = false, nullable = true, length = 200)
	private String regiAreaCodeName;
	
	/** 实际经营地址 **/
	@Column(name = "OPER_ADDR_ACT", unique = false, nullable = true, length = 200)
	private String operAddrAct;
	
	/** 经营范围 **/
	@Column(name = "OPER_RANGE", unique = false, nullable = true, length = 200)
	private String operRange;
	
	/** 实际控制人客户号 **/
	@Column(name = "REAL_OPER_CUS_ID", unique = false, nullable = true, length = 20)
	private String realOperCusId;
	
	/** 实际控制人客户名称 **/
	@Column(name = "REAL_OPER_CUS_NAME", unique = false, nullable = true, length = 80)
	private String realOperCusName;
	
	/** 是否城投 **/
	@Column(name = "IS_CTINVE", unique = false, nullable = true, length = 5)
	private String isCtinve;
	
	/** 评级结果(外部) **/
	@Column(name = "EVAL_RESULT_OUTER", unique = false, nullable = true, length = 5)
	private String evalResultOuter;
	
	/** 评级时间(外部) **/
	@Column(name = "EVAL_TIM_OUTER", unique = false, nullable = true, length = 20)
	private String evalTimOuter;
	
	/** 评级机构(外部) **/
	@Column(name = "EVAL_ORG_OUTER", unique = false, nullable = true, length = 200)
	private String evalOrgOuter;
	
	/** 评级结果(内部) **/
	@Column(name = "EVAL_RESULT_INNER", unique = false, nullable = true, length = 5)
	private String evalResultInner;
	
	/** 评级时间(内部) **/
	@Column(name = "EVAL_TIME_INNER", unique = false, nullable = true, length = 20)
	private String evalTimeInner;
	
	/** 评级结果(城投) **/
	@Column(name = "EVAL_RESULT_CTINVE", unique = false, nullable = true, length = 10)
	private String evalResultCtinve;
	
	/** 评级时间(城投) **/
	@Column(name = "EVAL_TIME_CTINVE", unique = false, nullable = true, length = 20)
	private String evalTimeCtinve;
	
	/** 监管评级 **/
	@Column(name = "SUPE_EVAL", unique = false, nullable = true, length = 5)
	private String supeEval;
	
	/** 资产规模 **/
	@Column(name = "ASSET_SIZE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetSize;
	
	/** 控股类型 **/
	@Column(name = "HOLD_TYPE", unique = false, nullable = true, length = 5)
	private String holdType;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusCha
	 */
	public void setCusCha(String cusCha) {
		this.cusCha = cusCha;
	}
	
    /**
     * @return cusCha
     */
	public String getCusCha() {
		return this.cusCha;
	}
	
	/**
	 * @param intbankOrgType
	 */
	public void setIntbankOrgType(String intbankOrgType) {
		this.intbankOrgType = intbankOrgType;
	}
	
    /**
     * @return intbankOrgType
     */
	public String getIntbankOrgType() {
		return this.intbankOrgType;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param busiLic
	 */
	public void setBusiLic(String busiLic) {
		this.busiLic = busiLic;
	}
	
    /**
     * @return busiLic
     */
	public String getBusiLic() {
		return this.busiLic;
	}
	
	/**
	 * @param isStock
	 */
	public void setIsStock(String isStock) {
		this.isStock = isStock;
	}
	
    /**
     * @return isStock
     */
	public String getIsStock() {
		return this.isStock;
	}
	
	/**
	 * @param regiCapCurType
	 */
	public void setRegiCapCurType(String regiCapCurType) {
		this.regiCapCurType = regiCapCurType;
	}
	
    /**
     * @return regiCapCurType
     */
	public String getRegiCapCurType() {
		return this.regiCapCurType;
	}
	
	/**
	 * @param regiCap
	 */
	public void setRegiCap(java.math.BigDecimal regiCap) {
		this.regiCap = regiCap;
	}
	
    /**
     * @return regiCap
     */
	public java.math.BigDecimal getRegiCap() {
		return this.regiCap;
	}
	
	/**
	 * @param regiType
	 */
	public void setRegiType(String regiType) {
		this.regiType = regiType;
	}
	
    /**
     * @return regiType
     */
	public String getRegiType() {
		return this.regiType;
	}
	
	/**
	 * @param regiAreaCode
	 */
	public void setRegiAreaCode(String regiAreaCode) {
		this.regiAreaCode = regiAreaCode;
	}
	
    /**
     * @return regiAreaCode
     */
	public String getRegiAreaCode() {
		return this.regiAreaCode;
	}
	
	/**
	 * @param regiAreaCodeName
	 */
	public void setRegiAreaCodeName(String regiAreaCodeName) {
		this.regiAreaCodeName = regiAreaCodeName;
	}
	
    /**
     * @return regiAreaCodeName
     */
	public String getRegiAreaCodeName() {
		return this.regiAreaCodeName;
	}
	
	/**
	 * @param operAddrAct
	 */
	public void setOperAddrAct(String operAddrAct) {
		this.operAddrAct = operAddrAct;
	}
	
    /**
     * @return operAddrAct
     */
	public String getOperAddrAct() {
		return this.operAddrAct;
	}
	
	/**
	 * @param operRange
	 */
	public void setOperRange(String operRange) {
		this.operRange = operRange;
	}
	
    /**
     * @return operRange
     */
	public String getOperRange() {
		return this.operRange;
	}
	
	/**
	 * @param realOperCusId
	 */
	public void setRealOperCusId(String realOperCusId) {
		this.realOperCusId = realOperCusId;
	}
	
    /**
     * @return realOperCusId
     */
	public String getRealOperCusId() {
		return this.realOperCusId;
	}
	
	/**
	 * @param realOperCusName
	 */
	public void setRealOperCusName(String realOperCusName) {
		this.realOperCusName = realOperCusName;
	}
	
    /**
     * @return realOperCusName
     */
	public String getRealOperCusName() {
		return this.realOperCusName;
	}
	
	/**
	 * @param isCtinve
	 */
	public void setIsCtinve(String isCtinve) {
		this.isCtinve = isCtinve;
	}
	
    /**
     * @return isCtinve
     */
	public String getIsCtinve() {
		return this.isCtinve;
	}
	
	/**
	 * @param evalResultOuter
	 */
	public void setEvalResultOuter(String evalResultOuter) {
		this.evalResultOuter = evalResultOuter;
	}
	
    /**
     * @return evalResultOuter
     */
	public String getEvalResultOuter() {
		return this.evalResultOuter;
	}
	
	/**
	 * @param evalTimOuter
	 */
	public void setEvalTimOuter(String evalTimOuter) {
		this.evalTimOuter = evalTimOuter;
	}
	
    /**
     * @return evalTimOuter
     */
	public String getEvalTimOuter() {
		return this.evalTimOuter;
	}
	
	/**
	 * @param evalOrgOuter
	 */
	public void setEvalOrgOuter(String evalOrgOuter) {
		this.evalOrgOuter = evalOrgOuter;
	}
	
    /**
     * @return evalOrgOuter
     */
	public String getEvalOrgOuter() {
		return this.evalOrgOuter;
	}
	
	/**
	 * @param evalResultInner
	 */
	public void setEvalResultInner(String evalResultInner) {
		this.evalResultInner = evalResultInner;
	}
	
    /**
     * @return evalResultInner
     */
	public String getEvalResultInner() {
		return this.evalResultInner;
	}
	
	/**
	 * @param evalTimeInner
	 */
	public void setEvalTimeInner(String evalTimeInner) {
		this.evalTimeInner = evalTimeInner;
	}
	
    /**
     * @return evalTimeInner
     */
	public String getEvalTimeInner() {
		return this.evalTimeInner;
	}
	
	/**
	 * @param evalResultCtinve
	 */
	public void setEvalResultCtinve(String evalResultCtinve) {
		this.evalResultCtinve = evalResultCtinve;
	}
	
    /**
     * @return evalResultCtinve
     */
	public String getEvalResultCtinve() {
		return this.evalResultCtinve;
	}
	
	/**
	 * @param evalTimeCtinve
	 */
	public void setEvalTimeCtinve(String evalTimeCtinve) {
		this.evalTimeCtinve = evalTimeCtinve;
	}
	
    /**
     * @return evalTimeCtinve
     */
	public String getEvalTimeCtinve() {
		return this.evalTimeCtinve;
	}
	
	/**
	 * @param supeEval
	 */
	public void setSupeEval(String supeEval) {
		this.supeEval = supeEval;
	}
	
    /**
     * @return supeEval
     */
	public String getSupeEval() {
		return this.supeEval;
	}
	
	/**
	 * @param assetSize
	 */
	public void setAssetSize(java.math.BigDecimal assetSize) {
		this.assetSize = assetSize;
	}
	
    /**
     * @return assetSize
     */
	public java.math.BigDecimal getAssetSize() {
		return this.assetSize;
	}
	
	/**
	 * @param holdType
	 */
	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}
	
    /**
     * @return holdType
     */
	public String getHoldType() {
		return this.holdType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}