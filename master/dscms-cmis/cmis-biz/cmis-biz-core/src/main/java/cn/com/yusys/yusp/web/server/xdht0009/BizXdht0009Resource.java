package cn.com.yusys.yusp.web.server.xdht0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0009.req.Xdht0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.Xdht0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0009.Xdht0009Servcie;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:银承合同信息列表查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0009:银承合同信息列表查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0009Resource.class);

    @Autowired
    private Xdht0009Servcie xdht0009Servcie;

    /**
     * 交易码：xdht0009
     * 交易描述：银承合同信息列表查询
     *
     * @param xdht0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("银承合同信息列表查询")
    @PostMapping("/xdht0009")
    protected @ResponseBody
    ResultDto<Xdht0009DataRespDto> xdht0009(@Validated @RequestBody Xdht0009DataReqDto xdht0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataReqDto));
        Xdht0009DataRespDto xdht0009DataRespDto = new Xdht0009DataRespDto();// 响应Dto:银承合同信息列表查询
        ResultDto<Xdht0009DataRespDto> xdht0009DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataReqDto));
            xdht0009DataRespDto = xdht0009Servcie.getXdht0009(xdht0009DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataRespDto));
            // 封装xdht0009DataResultDto中正确的返回码和返回信息
            xdht0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, e.getMessage());
            // 封装xdht0009DataResultDto中异常返回码和返回信息
            xdht0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0009DataRespDto到xdht0009DataResultDto中
        xdht0009DataResultDto.setData(xdht0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataRespDto));
        return xdht0009DataResultDto;
    }
}
