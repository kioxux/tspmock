/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.repository.mapper.RptBasicInfoPersFamilyAssetsMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoPersFamilyAssetsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 16:54:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicInfoPersFamilyAssetsService {

    @Autowired
    private RptBasicInfoPersFamilyAssetsMapper rptBasicInfoPersFamilyAssetsMapper;




    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptBasicInfoPersFamilyAssets selectByPrimaryKey(String pkId) {
        return rptBasicInfoPersFamilyAssetsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectByPkId
     * @方法描述: 根据pkId查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptBasicInfoPersFamilyAssets selectByPkId(String pkId) {
        return rptBasicInfoPersFamilyAssetsMapper.selectByPkId(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptBasicInfoPersFamilyAssets> selectAll(QueryModel model) {
        List<RptBasicInfoPersFamilyAssets> records = (List<RptBasicInfoPersFamilyAssets>) rptBasicInfoPersFamilyAssetsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptBasicInfoPersFamilyAssets> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasicInfoPersFamilyAssets> list = rptBasicInfoPersFamilyAssetsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptBasicInfoPersFamilyAssets record) {
        return rptBasicInfoPersFamilyAssetsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptBasicInfoPersFamilyAssets record) {
        return rptBasicInfoPersFamilyAssetsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptBasicInfoPersFamilyAssets record) {
        return rptBasicInfoPersFamilyAssetsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptBasicInfoPersFamilyAssets record) {
        return rptBasicInfoPersFamilyAssetsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptBasicInfoPersFamilyAssetsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicInfoPersFamilyAssetsMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    public  List<RptBasicInfoPersFamilyAssets> queryGuarByLmtSerno(String serno) {
        return rptBasicInfoPersFamilyAssetsMapper.queryGuarByLmtSerno(serno);
    }

    public int save(RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets){
        String pkId = rptBasicInfoPersFamilyAssets.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return rptBasicInfoPersFamilyAssetsMapper.updateByPrimaryKeySelective(rptBasicInfoPersFamilyAssets);
        }else {
            rptBasicInfoPersFamilyAssets.setPkId(StringUtils.getUUID());
            return rptBasicInfoPersFamilyAssetsMapper.insertSelective(rptBasicInfoPersFamilyAssets);
        }
    }

    public List<RptBasicInfoPersFamilyAssets> selectBySerno(String serno){
        return rptBasicInfoPersFamilyAssetsMapper.selectBySerno(serno);
    }
}
