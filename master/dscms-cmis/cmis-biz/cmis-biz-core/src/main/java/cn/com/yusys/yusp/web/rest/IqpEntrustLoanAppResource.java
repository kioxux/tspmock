package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.AccountDto;
import cn.com.yusys.yusp.dto.CfgAccpOrgRelDto;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.dto.CfgSorgLoanManyDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.service.Dscms2CoreDpClientService;
import cn.com.yusys.yusp.service.IqpEntrustLoanAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpEntrustLoanAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-15 14:31:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "委托贷款合同申请")
@RequestMapping("/api/iqpentrustloanapp")
public class IqpEntrustLoanAppResource {
    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

	/**
     * 全表查询.
     * 
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpEntrustLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpEntrustLoanApp> list = iqpEntrustLoanAppService.selectAll(queryModel);
        return new ResultDto<List<IqpEntrustLoanApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpEntrustLoanApp>> index(QueryModel queryModel) {
        List<IqpEntrustLoanApp> list = iqpEntrustLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpEntrustLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpEntrustLoanApp> show(@PathVariable("pkId") String pkId) {
        IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpEntrustLoanApp>(iqpEntrustLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpEntrustLoanApp> create(@RequestBody IqpEntrustLoanApp iqpEntrustLoanApp) throws URISyntaxException {
        iqpEntrustLoanAppService.insert(iqpEntrustLoanApp);
        return new ResultDto<IqpEntrustLoanApp>(iqpEntrustLoanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpEntrustLoanApp iqpEntrustLoanApp) throws URISyntaxException {
        int result = iqpEntrustLoanAppService.update(iqpEntrustLoanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpEntrustLoanAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpEntrustLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 委托贷款申请保存操作
     * @param iqpEntrustLoanApp
     * @return
     */
    @ApiOperation("委托贷款申请保存新增操作")
    @PostMapping("/saveiqpEntrustLoanAppinfo")
    public ResultDto<Map> saveIqpEntrustLoanAppInfo(@RequestBody IqpEntrustLoanApp iqpEntrustLoanApp) {
        Map result = iqpEntrustLoanAppService.saveIqpEntrustLoanAppInfo(iqpEntrustLoanApp);
        return new ResultDto<>(result);
    }
    /**
     * @函数名称:deleteIqpEntrustLoanAppinfo
     * @函数描述:通过主键对核销申请进行逻辑删除
     * @参数与返回说明: params
     * @算法描述: 对数据类型进行更新，并修改明细列表中的数据类型
     */
    @ApiOperation("委托贷款申请删除操作")
    @PostMapping("/deleteIqpEntrustLoanAppinfo")
    protected ResultDto<Map> deleteIqpEntrustLoanAppinfo(@RequestBody Map params) {
        Map map = iqpEntrustLoanAppService.deleteIqpEntrustLoanAppinfo((String)params.get("serno"));
        return new ResultDto<Map>(map);
    }

    /**
     * 委托贷款申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("委托贷款申请通用的保存方法")
    @PostMapping("/commonsaveiqpentrustappinfo")
    public ResultDto<Map> commonSaveIqpEntrustAppInfo(@RequestBody Map params){
        Map rtnData = iqpEntrustLoanAppService.commonSaveIqpEntrustAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请待处理列表查询")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpEntrustLoanApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpEntrustLoanApp> list = iqpEntrustLoanAppService.toSignlist(queryModel);
        ResultDto<List<IqpEntrustLoanApp>> resultDto = new ResultDto<List<IqpEntrustLoanApp>>();// 方法返回对象
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpEntrustLoanApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请已处理列表查询")
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpEntrustLoanApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpEntrustLoanApp> list = iqpEntrustLoanAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<IqpEntrustLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请明细展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpEntrustLoanApp temp = new IqpEntrustLoanApp();
        IqpEntrustLoanApp studyDemo = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey((String)map.get("serno"));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:getAccNoInfo
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("委托贷款申请账户获取")
    @PostMapping("/getaccnoinfo")
    protected ResultDto<Ib1253RespDto> getAccNoInfo(@RequestBody  Map map) {
        ResultDto<Ib1253RespDto> resultDto = iqpEntrustLoanAppService.getAccNoInfo(map);
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpEntrustLoanAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * @方法名称：selectSorgFina
     * @方法描述：获取入账机构信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("获取入账机构方法")
    @PostMapping("/selectsorgfina")
    protected  ResultDto<List<CfgSorgFinaDto>> selectSorgFina(@RequestBody QueryModel queryModel) {
        ResultDto<List<CfgSorgFinaDto>> RestltList = new ResultDto<>();
        RestltList = iqpEntrustLoanAppService.selectSorgFina(queryModel);
        return RestltList;
    }

    /**
     * @方法名称：selectSorgFina
     * @方法描述：获取贷款机构信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("获取贷款机构机构方法")
    @PostMapping("/selectsorgloanm")
    protected  ResultDto<List<CfgSorgLoanManyDto>> selecSorgLoanMany(@RequestBody QueryModel queryModel) {
        ResultDto<List<CfgSorgLoanManyDto>> RestltList = new ResultDto<>();
        RestltList = iqpEntrustLoanAppService.selecSorgLoanMany(queryModel);
        return RestltList;
    }

    /**
     * @方法名称：selectSorgFina
     * @方法描述：获取承兑机构信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("获取承兑机构方法")
    @PostMapping("/selectaccporg")
    protected  ResultDto<List<CfgAccpOrgRelDto>> selectAccpOrg(@RequestBody QueryModel queryModel) {
        ResultDto<List<CfgAccpOrgRelDto>> RestltList = new ResultDto<>();
        RestltList = iqpEntrustLoanAppService.selectAccpOrg(queryModel);
        return RestltList;
    }

    /**
     * @函数名称:iqpEntrustLoanSubmitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/iqpEntrustLoanSubmitNoFlow")
    protected ResultDto<Map> iqpEntrustLoanSubmitNoFlow(@RequestBody IqpEntrustLoanApp iqpEntrustLoanApp) throws URISyntaxException {
        Map rtnData = iqpEntrustLoanAppService.iqpEntrustLoanSubmitNoFlow(iqpEntrustLoanApp.getSerno());
        return new ResultDto<>(rtnData);
    }


    /**
     * @方法名称：selectaccount
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("账户获取方法")
    @PostMapping("/selectaccount")
    protected  ResultDto<List<AccountDto>> selectaccount(@RequestBody QueryModel queryModel) {
        ResultDto<Object> result = new ResultDto<>();
        List<AccountDto> list = new LinkedList<>();

        String  accountno = (String)queryModel.getCondition().get("loanPayoutAccno");
        Dp2021ReqDto Dp2021ReqDto = new Dp2021ReqDto();
        Dp2021ReqDto.setChaxleix("0");
        Dp2021ReqDto.setKehuhaoo("");//客户号
        Dp2021ReqDto.setChaxmima("");
        // TODO 翻页支持
        Dp2021ReqDto.setQishibis(1);
        Dp2021ReqDto.setChxunbis(100);
        Dp2021ReqDto.setChaxfanw("");
        Dp2021ReqDto.setZhanghao("");
        Dp2021ReqDto.setKehuzhao(accountno);
        Dp2021ReqDto.setDinhuobz("");
        Dp2021ReqDto.setZhhaoxuh("");
        Dp2021ReqDto.setCunkzlei("");
        Dp2021ReqDto.setHuobdaih("");
        Dp2021ReqDto.setChaohubz("");
        Dp2021ReqDto.setZhcxzhzt("");
        Dp2021ReqDto.setShifoudy("");
        Dp2021ReqDto.setJigouhao("");
        Dp2021ReqDto.setMimazlei("");
        Dp2021ReqDto.setSfcxglzh("");
        Dp2021ReqDto.setZhjhaoma("");
        //Dp2021ReqDto.setKehumich("");
        Dp2021ReqDto.setKehuzhmc("");
        Dp2021ReqDto.setZhjnzlei("");
        Dp2021ReqDto.setBeiyzd01("");
        Dp2021ReqDto.setBeiyzd02("");
        ResultDto<Dp2021RespDto> resultDp2021 = dscms2CoreDpClientService.dp2021(Dp2021ReqDto);
        Dp2021RespDto dp2021RespDto = resultDp2021.getData();
        List<Lstacctinfo> lstacctinfolist = dp2021RespDto.getLstacctinfo();
        if(resultDp2021.getCode().equals("0")){
            for(int i=0;i<lstacctinfolist.size();i++){
                AccountDto accountDto = new AccountDto();
                Lstacctinfo lstacctinfo = lstacctinfolist.get(i);
                accountDto.setAiostype(lstacctinfo.getAiostype());//组合账户形态
                accountDto.setBeiyye01(lstacctinfo.getBeiyye01());//备用余额01
                accountDto.setBeiyye02(lstacctinfo.getBeiyye02());//备用余额02
                accountDto.setBeiyye03(lstacctinfo.getBeiyye03());//备用余额03
                accountDto.setBeiyzd01(lstacctinfo.getBeiyzd01());//备用字段01
                accountDto.setBeiyzd02(lstacctinfo.getBeiyzd02());//备用字段02
                accountDto.setBeiyzd03(lstacctinfo.getBeiyzd03());//备用字段03
                accountDto.setCenjleix(lstacctinfo.getCenjleix());//层级类型
                accountDto.setChanpshm(lstacctinfo.getChanpshm());//产品说明
                accountDto.setChaohubz(lstacctinfo.getChaohubz());//账户钞汇标志
                accountDto.setChapbhao(lstacctinfo.getChapbhao());//产品编号
                accountDto.setCunkzlei(lstacctinfo.getCunkzlei());//存款种类
                accountDto.setCunqiiii(lstacctinfo.getCunqiiii());//存期
                accountDto.setDgdsbzhi(lstacctinfo.getDgdsbzhi());//对公对私标志
                accountDto.setDinhuobz(lstacctinfo.getDinhuobz());//产品定活标志
                accountDto.setDmhsbioz(lstacctinfo.getDmhsbioz());//当面核实标志
                accountDto.setDoqiriqi(lstacctinfo.getDoqiriqi());//到期日期
                accountDto.setDsdzzhfl(lstacctinfo.getDsdzzhfl());//个人电子账户分类
                accountDto.setDspzsyzt(lstacctinfo.getDspzsyzt());//凭证状态
                accountDto.setGlpinzbz(lstacctinfo.getGlpinzbz());//关联凭证标志
                accountDto.setHuobdaih(changeCurrency(lstacctinfo.getHuobdaih()));//货币代号
                accountDto.setKachapbh(lstacctinfo.getKachapbh());//卡产品编号
                accountDto.setKaihguiy(lstacctinfo.getKaihguiy());//账户开户柜员
                accountDto.setKaihjigo(lstacctinfo.getKaihjigo());//账户开户机构
//                accountDto.setKaihjigo(OcaTranslatorUtils.getOrgName(lstacctinfo.getKaihjigo()));
                accountDto.setKaihriqi(lstacctinfo.getKaihriqi());////开户日期
                accountDto.setKaihuqud(lstacctinfo.getKaihuqud());//开户渠道
                accountDto.setKehuhaoo(lstacctinfo.getKehuhaoo());//客户号
                accountDto.setKehuywmc(lstacctinfo.getKehuywmc());//客户英文名
                accountDto.setKehuzhao(lstacctinfo.getKehuzhao());//客户账号
                accountDto.setKehuzhlx(lstacctinfo.getKehuzhlx());//客户账号类型
                accountDto.setKehuzhwm(lstacctinfo.getKehuzhwm());//客户中文名
                accountDto.setKeyonedu(lstacctinfo.getKeyonedu());//可用额度
                accountDto.setKeyongye(lstacctinfo.getKeyongye());//可用余额
                accountDto.setKhakjine(lstacctinfo.getKhakjine());//可扣划金额
                accountDto.setKongzhzt(lstacctinfo.getKongzhzt());//账户控制状态
                accountDto.setKzfbdjbz(lstacctinfo.getKzfbdjbz());//客户账户封闭冻结标志
                accountDto.setKzhuztai(changezhuangtai(lstacctinfo.getKzhuztai()));//客户账户状态
                accountDto.setKzjedjbz(lstacctinfo.getKzjedjbz());//客户账户金额冻结标志
                accountDto.setKzzfbsbz(lstacctinfo.getKzzfbsbz());//客户账户只付不收标志
                accountDto.setKzzsbfbz(lstacctinfo.getKzzsbfbz());//客户账户只收不付标志
                accountDto.setPingzhma(lstacctinfo.getPingzhma());//凭证号码
                accountDto.setPngzzlei(lstacctinfo.getPngzzlei());//凭证种类
                accountDto.setPnzqiyrq(lstacctinfo.getPnzqiyrq());//凭证启用日期
                accountDto.setQixiriqi(lstacctinfo.getQixiriqi());//起息日期
                accountDto.setQuerenbz(lstacctinfo.getQuerenbz());//确认标志
                accountDto.setQukuanrq(lstacctinfo.getQukuanrq());//取款日期
                accountDto.setSbkbiaoz(lstacctinfo.getSbkbiaoz());//社保卡标志
                accountDto.setShifzhbz(lstacctinfo.getShifzhbz());//是否保证金组合产品
                accountDto.setSuoshudx(lstacctinfo.getSuoshudx());//产品所属对象
                accountDto.setTduibzhi(lstacctinfo.getTduibzhi());//通兑标志
                accountDto.setTonzbhao(lstacctinfo.getTonzbhao());//通知编号
                accountDto.setTonzjine(lstacctinfo.getTonzjine());//通知金额
                accountDto.setTonzriqi(lstacctinfo.getTonzriqi());//通知日期
                accountDto.setWeidzhes(lstacctinfo.getWeidzhes());//未登折数
                accountDto.setXiohriqi(lstacctinfo.getXiohriqi());//销户日期
                accountDto.setXushibzh(lstacctinfo.getXushibzh());//账户虚实标志
                accountDto.setYegxriqi(lstacctinfo.getYegxriqi());//余额最近更新日期
                accountDto.setYezztbbz(lstacctinfo.getYezztbbz());//余额与总账同步标志
                accountDto.setYouwkbiz(lstacctinfo.getYouwkbiz());//有无卡标志
                accountDto.setYoxqjine(lstacctinfo.getYoxqjine());//优先权金额
                accountDto.setZhanghao(lstacctinfo.getZhanghao());//负债账号
                accountDto.setZhanghye(lstacctinfo.getZhanghye());//账户余额
                accountDto.setZhcunfsh(lstacctinfo.getZhcunfsh());//转存方式
                accountDto.setZhfbdjbz(lstacctinfo.getZhfbdjbz());//账户封闭冻结标志
                accountDto.setZhfutojn(lstacctinfo.getZhfutojn());//支付条件
                accountDto.setZhhaoxuh(lstacctinfo.getZhhaoxuh());//子账户序号
                accountDto.setZhhufenl(lstacctinfo.getZhhufenl());//账户分类
                accountDto.setZhhuztai(lstacctinfo.getZhhuztai());//账户状态
                accountDto.setZhhuzwmc(lstacctinfo.getZhhuzwmc());//账户名称
                accountDto.setZhjedjbz(lstacctinfo.getZhjedjbz());//账户金额冻结标志
                accountDto.setZhshuxin(lstacctinfo.getZhshuxin());;//账户属性
                accountDto.setZhujigoh(lstacctinfo.getZhujigoh());//账户所属机构
                accountDto.setZhxililv(lstacctinfo.getZhxililv());//当前执行利率
                accountDto.setZhzfbsbz(lstacctinfo.getZhzfbsbz());;//账户只付不收标志
                accountDto.setZhzsbfbz(lstacctinfo.getZhzsbfbz());//账户只收不付标志
                accountDto.setZtmiaosu(lstacctinfo.getZtmiaosu());//状态描述
                list.add(accountDto);
            }
        }

        return new ResultDto<List<AccountDto>>(list);
    }
    //币种转换
    public String changeCurrency(String cur){
        if("01".equals(cur)){
            cur="CNY";
        }else if("12".equals(cur)){
            cur="GBP";
        }else if("13".equals(cur)){
            cur="HKD";
        }else if("14".equals(cur)){
            cur="USD";
        }else if("15".equals(cur)){
            cur="CHF";
        }else if("21".equals(cur)){
            cur="SEK";
        }else if("27".equals(cur)){
            cur="JPY";
        }else if("29".equals(cur)){
            cur="AUD";
        }else if("38".equals(cur)){
            cur="EUR";
        }
        return cur;
    }

    //账户状态
    public String changezhuangtai(String zt){
        if("A".equals(zt)){
            zt="正常";
        }else if("J".equals(zt)){
            zt="控制";
        }else if("C".equals(zt)){
            zt="销户";
        }else if("D".equals(zt)){
            zt="久悬户";
        }else if("I".equals(zt)){
            zt="转营业外收入";
        }else if("H".equals(zt)){
            zt="待启用";
        }else if("G".equals(zt)){
            zt="未启用";
        }else if("Y".equals(zt)){
            zt="预销户";
        }
        return zt;
    }

}
