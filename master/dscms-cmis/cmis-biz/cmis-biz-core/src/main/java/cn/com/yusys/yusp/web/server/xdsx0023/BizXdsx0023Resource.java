package cn.com.yusys.yusp.web.server.xdsx0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0023.req.Xdsx0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.resp.Xdsx0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0023.Xdsx0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:保函协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0023:保函协议查询")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0023Resource.class);

    @Autowired
    private Xdsx0023Service xdsx0023Service;

    /**
     * 交易码：xdsx0023
     * 交易描述：保函协议查询
     *
     * @param xdsx0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保函协议查询")
    @PostMapping("/xdsx0023")
    protected @ResponseBody
    ResultDto<Xdsx0023DataRespDto> xdsx0023(@Validated @RequestBody Xdsx0023DataReqDto xdsx0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataReqDto));
        ResultDto<Xdsx0023DataRespDto> xdsx0023DataResultDto = new ResultDto<>();
        try {
            // 调用xdsx0023Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataReqDto));
            xdsx0023DataResultDto = xdsx0023Service.getCvrgContDetail(xdsx0023DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataResultDto));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, e.getMessage());
            // 封装xdsx0023DataResultDto中异常返回码和返回信息
            xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataResultDto));
        return xdsx0023DataResultDto;
    }
}
