package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.template.FileSystemTemplate;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.BizDomain;
import cn.com.yusys.yusp.domain.BizMustCheckDetails;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * 主体授信通用service
 */
@Component
public class BizInvestCommonService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(BizInvestCommonService.class);

    private static final String FILE_SEPARATOR = "_SPLIT_";

    @Resource(name="fanruanFileTemplate")
    public FileSystemTemplate fanruanFileTemplate;

    @Resource(name="sftpFileSystemTemplate")
    public FileSystemTemplate sftpFileSystemTemplate;

    @Resource(name = "sharedFileTemplate")
    public FileSystemTemplate sharedFileTemplate;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    public WorkflowCoreClient workflowCoreClient;

    // 系统参数配置服务
    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

    /**
     * 生成主键id
     * @return
     */
    public String generatePkId(){
        String sequenceTemplate = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, new HashMap<>());
        if (StringUtils.isBlank(sequenceTemplate)){
            sequenceTemplate = sequenceTemplateClient.getSequenceTemplate("PK_VALUE", new HashMap<>());
        }
        return sequenceTemplate;
    }

    /**
     * 生成序列号
     * @param CodeName(序列号生成规则) 默认 CmisBizConstants.BIZ_SERNO
     * @return
     */
    public String generateSerno(String CodeName){
        if (StringUtils.isBlank(CodeName)){
            CodeName = CmisBizConstants.BIZ_SERNO;
        }
        return sequenceTemplateClient.getSequenceTemplate(CodeName, new HashMap<>());
    }

    /**
     * 获取当前登陆人员
     * @return
     */
    public User getCurrentUser() {
        return SessionUtils.getUserInformation();
    }


    /**
     * 通用当前是时间
     * @return
     */
    public Date getCurrrentDate(){
        return DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
    }

    /**
     * 通用当前时间（yyyy-MM-dd） form openDay
     * @return
     */
    public String getCurrrentDateStr(){
        return stringRedisTemplate.opsForValue().get("openDay");
    }

    /**
     * 修改map中的对应字段类型为Date类型
     * @param map
     * @param key
     */
    public void changeValueToDate(Map map, String key) {
        if (map!=null && map.get(key)!=null){
            try {
                String val = (String) map.get(key);
                Date date = DateUtils.parseDate(val, "yyyy-MM-dd HH:mm:ss");
                map.put(key,date);
            } catch (Exception e) {
                log.error("时间格式转化失败===》",e);
                throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
            }
        }
    }

    /**
     * 修改map中的对应字段类型为Decimal类型<br/>
     * （null、”“、"null"）默认值为BigDecimal.ZERO
     * @param map
     * @param key
     */
    public Object changeValueToBigDecimal(Map map, String key) {
        BigDecimal resultVal = BigDecimal.ZERO;
        if (map!=null && map.containsKey(key)){
            try {
                if (map.get(key)==null || "".equals(map.get(key)) || "null".equals(map.get(key))){
                    map.put(key,resultVal);
                }else{
                    String val = String.valueOf(map.get(key));
                    resultVal = new BigDecimal(val);
                    map.put(key,resultVal);
                }
            } catch (Exception e) {
                log.error("BigDecimal格式转化失败===》",e);
                throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
            }
        }
        return resultVal;
    }

    /**
     * 转换对象为BigDecimal （null、”“、"null"）默认值为BigDecimal.ZERO
     * @param target 需要转换的值
     * @return
     */
    public BigDecimal changeValueToBigDecimal(Object target){
        try {
            if (target ==null ||"".equals(target) || "null".equals(target)){
                return BigDecimal.ZERO;
            }else{
                String val = String.valueOf(target);
                return new BigDecimal(val);
            }
        } catch (Exception e) {
            log.error("BigDecimal格式转化失败===》",e);
            throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
        }
    }

    public Integer changeValueToInteger(Map map,String key){
        Integer resultVal = 0;
        if (map!=null && map.containsKey(key)){
            try {
                if (map.get(key)==null || "".equals(map.get(key)) || "null".equals(map.get(key))){
                    map.put(key,resultVal);
                }else{
                    resultVal = Integer.parseInt(String.valueOf(map.get(key)));
                    map.put(key,resultVal);
                }
            } catch (Exception e) {
                log.error("Integer格式转化失败===》",e);
                throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key,EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
            }
        }
        return resultVal;
    }

    public Integer changeValueToInteger(Object target) {
        try {
            if (target == null || "".equals(target) || "null".equals(target)) {
                return 0;
            } else {
                String val = String.valueOf(target);
                return Integer.parseInt(String.valueOf(val));
            }
        } catch (Exception e) {
            log.error("Integer格式转化失败===》",e);
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.key, EclEnum.LMT_SIG_INVESTAPP_UPDATE_FAILD.value);
        }
    }

    /**
     * 复制对象属性值--增加空值copy判断（org.springframework.beans.BeanUtils）
     * @param source
     * @param target
     * @param ingoreNullValue 是否忽略空值（针对整个source中的所有key对应value为空的keys忽略）
     * @param ignoreProperties 忽略属性
     */
    public void copyProperties(Object source, Object target, boolean ingoreNullValue, String... ignoreProperties) {
        //是否忽略空值
        if (ingoreNullValue) {
            //获取对象中的空值属性
            String[] nullPropertyNames = BizInvestCommonService.getNullPropertyNames(source);
            if (nullPropertyNames != null && nullPropertyNames.length > 0) {
                //属性忽略为空，采用对象nullPropertyNames
                if (ignoreProperties == null || ignoreProperties.length == 0) {
                    ignoreProperties = nullPropertyNames;
                } else {
                    //拼接需要忽略的属性
                    int oldLength = ignoreProperties.length;
                    int newLength = oldLength + nullPropertyNames.length;
                    ignoreProperties = Arrays.copyOf(ignoreProperties, newLength);
                    //Object src,  int  srcPos,Object dest, int destPos, int length
                    System.arraycopy(nullPropertyNames, 0, ignoreProperties, oldLength, nullPropertyNames.length);
                }
            }
        }
        BeanUtils.copyProperties(source, target, ignoreProperties);
    }

    /**
     * 复制对象属性值（org.springframework.beans.BeanUtils）
     * @param source
     * @param target
     * @param ignoreProperties 忽略属性值
     */
    public void copyProperties(Object source,Object target,String... ignoreProperties){
        //默认不忽略空值属性
        copyProperties(source,target,false,ignoreProperties);
    }

    /**
     * 获取对象中值为null的属性名
     * @param source
     * @return
     */
    public static String[] getNullPropertyNames(Object source){
        final BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] propertyDescriptors = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            Object srcValue = src.getPropertyValue(propertyDescriptor.getName());
            if (srcValue == null){
                emptyNames.add(propertyDescriptor.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    /**
     * 初始化(新增)通用domain信息<br/>
     * 包含（input、upd、pkId、oprType）相关信息<br/>
     * 默认生成（pkId,oprType=CmisBizConstants.OPR_TYPE_01）
     * @param target
     */
    public void initInsertDomainProperties(Object target){
        initInsertDomainProperties(target,null,null);
    }

    /**
     * 初始化(新增)通用domain信息(不包含inputId、InputBrId)<br/>
     * 包含（input、upd、pkId、oprType）相关信息<br/>
     * 默认生成（pkId,oprType=CmisBizConstants.OPR_TYPE_01）
     * @param target
     */
    public void initInsertDomainPropertiesForWF(Object target){
        initInsertDomainPropertiesForWF(target,null,null);
    }

    /**
     * 初始化(新增)通用domain信息<br/>
     * 包含（input、upd、pkId、oprType）相关信息<br/>
     * 默认生成（pkId,oprType=CmisBizConstants.OPR_TYPE_01）
     * @param target
     * @param userId
     * @param orgId
     */
    public void initInsertDomainProperties(Object target,String userId,String orgId){
        copyProperties(initBizDomain(false,userId,orgId,true),target,true);
    }
    /**
     * 初始化(新增)通用domain信息(不包含inputId、InputBrId)<br/>
     * 包含（upd、pkId、oprType）相关信息(不包含inputId、InputBrId)<br/>
     * 默认生成（pkId,oprType=CmisBizConstants.OPR_TYPE_01）
     * @param target
     * @param userId
     * @param orgId
     */
    public void initInsertDomainPropertiesForWF(Object target,String userId,String orgId){
        copyProperties(initBizDomain(false,userId,orgId,false),target,true);
    }

    /**
     * 初始化（更新）通用domain信息<br/>
     * 包含（upd）相关信息
     * @param target
     */
    public void initUpdateDomainProperties(Object target){
        initUpdateDomainProperties(target,null,null);
    }

    /**
     * 初始化（更新）通用domain信息<br/>
     * 包含（upd）相关信息
     * @param target
     * @param userId
     * @param orgId
     */
    public void initUpdateDomainProperties(Object target,String userId,String orgId){
        copyProperties(initBizDomain(true,userId,orgId,true),target,true);
    }

    /**
     * 初始化通用domain信息<br/>
     * 初始化 upd和input相关信息
     * @param currentUserId
     * @param currentOrgId
     * @param isUpdate false:(默认规则生成pkId,oprType=CmisBizConstants.OPR_TYPE_01)
     * @return
     */
    private BizDomain initBizDomain(boolean isUpdate,String currentUserId,String currentOrgId,boolean isSetInputInfo) {
        if (StringUtils.isBlank(currentOrgId)){
            currentOrgId = getCurrentUser() != null ? getCurrentUser().getOrg().getCode() : "";
        }
        if (StringUtils.isBlank(currentUserId)){
            currentUserId = getCurrentUser() != null ? getCurrentUser().getLoginCode() : "";
        }
        BizDomain bizDomain = new BizDomain();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
        bizDomain.setUpdateTime(DateUtils.getCurrTimestamp());
        bizDomain.setUpdDate(openDay);
        bizDomain.setUpdId(currentUserId);
        bizDomain.setUpdBrId(currentOrgId);
        if (!isUpdate) {
            bizDomain.setPkId(generatePkId());
            bizDomain.setInputDate(openDay);
            if (isSetInputInfo){
                bizDomain.setInputBrId(currentOrgId);
                bizDomain.setInputId(currentUserId);
            }
            bizDomain.setCreateTime(DateUtils.getCurrTimestamp());
            bizDomain.setOprType(CmisBizConstants.OPR_TYPE_01);
        }
        return bizDomain;
    }


    public String getFileAbsolutePath(String fileId, String serverPath, String relativePath, FileSystemTemplate fileSystemTemplate) {
        return getFileAbsolutePath(fileId,serverPath,relativePath,fileSystemTemplate,true);
    }

    /**
     * 获取文件绝对路径
     * @param fileId
     * @param serverPath
     * @param relativePath
     * @param fileSystemTemplate
     * @return
     */
    public String getFileAbsolutePath(String fileId, String serverPath, String relativePath, FileSystemTemplate fileSystemTemplate, boolean isFanruan) {
        String absolutePath = "";
        try {
            //获取图片绝对路径
            FileInfo oldFileInfo = FileInfoUtils.fromIdentity(fileId);
            if (oldFileInfo != null) {
                String fileName = oldFileInfo.getFileName();
                //路径缓存文件长度信息
                StringBuilder sb = new StringBuilder(fileName);
                sb.insert(sb.lastIndexOf("."), "_" + oldFileInfo.getFileSize());
                fileName = sb.toString();

                log.info("====fileName====>" + fileName);
                //保存1
                FileInfo newFileInfo = FileInfoUtils.createFileInfo(fileSystemTemplate, fileName, relativePath);
                FileInfoUtils.copy(oldFileInfo, newFileInfo, true);
                if (isFanruan){
                    //帆软保存两个位置  ip:46
                    FileInfo fanruan2 = FileInfoUtils.createFileInfo(sharedFileTemplate, fileName, relativePath);
                    FileInfoUtils.copy(oldFileInfo, fanruan2, true);
                    Path path = Paths.get(serverPath, fanruan2.getFilePath(), fileName);
                    log.info("fanruan2 ===>absolutePath{}",path);
                }
                Path path = Paths.get(serverPath, newFileInfo.getFilePath(), fileName);
                absolutePath = path.toString();
                String newFileId = FileInfoUtils.toIdentity(newFileInfo);
                log.info("newFileName===>{}  newFileID===》{} absolutePath{}", fileName, newFileId,absolutePath);
            }
        } catch (Exception e) {
            log.error("获取文件绝对路径失败===》", e);
        }
        return absolutePath;
    }

    /**
     * 获取文件名称
     * @param filePath -文件绝对路径
     * @return
     */
    public TempFileInfo getTempFileInfo(@NotNull String filePath) {
        try {
            Path path = Paths.get(filePath);
            if (path != null) {
                FileSystemTemplate fileSystemTemplate;

                File file = path.toFile();
                //获取文件名称 （包含 文件大小信息）
                String fileName = file.getName();
                //获取文件大小
                String[] s = fileName.substring(0, fileName.lastIndexOf(".")).split("_");
                Long fileSize = Long.parseLong(s[s.length - 1]);

                String defaultPath = "";
                String convertFilePath = filePath.replaceAll("\\\\", FILE_SEPARATOR).replaceAll("/",FILE_SEPARATOR);
                String convertFanruanPath = fanruanFileTemplate.getDefaultPath().replaceAll("/", FILE_SEPARATOR);
                String convertSftpFileSystemPath = sftpFileSystemTemplate.getDefaultPath().replaceAll("/", FILE_SEPARATOR);

                if (convertFilePath.contains(convertFanruanPath)) {
                    fileSystemTemplate = fanruanFileTemplate;
                    defaultPath = convertFanruanPath;
                } else {
                    fileSystemTemplate = sftpFileSystemTemplate;
                    defaultPath = convertSftpFileSystemPath;
                }
                String relativePath = convertFilePath.split(defaultPath)[1].replace(fileName, "").replaceAll(FILE_SEPARATOR, "/");
                FileInfo fileInfo1 = FileInfoUtils.createFileInfo(fileSystemTemplate, fileName, relativePath);
                String fileId = FileInfoUtils.toIdentity(fileInfo1);

                log.info("fileInfo: fileName==>{} filePath==>{} relativePath===>{} defaultPath===>{} fileId==>{}", fileName, filePath, relativePath, defaultPath, fileId);

                TempFileInfo tempFileInfo = new TempFileInfo();
                tempFileInfo.setFileLength(BizInvestCommonService.getFileSize(fileSize));
                tempFileInfo.setFileName(fileName);
                tempFileInfo.setFilePath(filePath);
                tempFileInfo.setFileId(fileId);

                return tempFileInfo;
            }
        } catch (Exception e) {
            log.error("文件获取失败===》", e);
        }
        return null;
    }

    /**
     *
     * @param obj
     * @return
     */
    public static Map<String,Object> BeanToMap(Object obj){
        if (obj != null){
            Map<String,Object> newMap = new HashMap<>();
            Map<String, Object> map = (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(obj);
            map.entrySet().forEach(a->{
                newMap.put(a.getKey(),a.getValue());
            });
            return newMap;
        }
        return null;
    }

    /**
     * 获取文件大小
     * @param fileLength
     * @return
     */
    private static String getFileSize(Long fileLength){
        if (fileLength != -1L){
            return new BigDecimal(fileLength).divide(new BigDecimal(1024L),3, RoundingMode.HALF_UP).toString();
//            if (fileLength < 1024L * 1024L ) {
//
//            }
//            return new BigDecimal(fileLength).divide(new BigDecimal(1024L*1024L),2, RoundingMode.HALF_UP)+"MB";
        }
        return "";
    }

    /**
     * 设置目标对象对应属性设置值
     * @param target  目标转换
     * @param key  属性名称
     * @param val  属性值
     */
    public void setValByKey(Object target, String key, Object val) {
        if (target == null){
            log.error("target不能为null");
            return;
        }
        boolean isSuccess = false;
        BeanWrapper src = new BeanWrapperImpl(target);
        PropertyDescriptor[] propertyDescriptors = src.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            String name = propertyDescriptor.getName();

            if (name.equals(key)){
                String valTypeName = "";
                if (val != null){
                    valTypeName = val.getClass().getTypeName();
                }
                String typeName = propertyDescriptor.getPropertyType().getTypeName();
                //BigDecimal单独进行转换
                if(typeName.equals(BigDecimal.class.getTypeName())){
                    BigDecimal bigDecimal = changeValueToBigDecimal(val);
                    src.setPropertyValue(name,bigDecimal);
                }else if(typeName.equals(Integer.class.getTypeName())){
                    //Integer单独进行转换
                    Integer integer = changeValueToInteger(val);
                    src.setPropertyValue(name,integer);
                }else if(typeName.equals(Date.class.getTypeName()) && valTypeName.equals(Long.class.getTypeName())){
                    Date date = new Date((Long) val);
                    src.setPropertyValue(name,date);
                }else if(valTypeName.equals(typeName)){
                    //类型匹配直接赋值
                    src.setPropertyValue(name,val);
                }else{
                    src.setPropertyValue(name,null);
                    //类型无法匹配
                    //log.error("当前copy属性值：{}(目标所需类型：{} --- 提供属性类型：{} )不匹配，该属性copy失败，默认为null",name,typeName,valTypeName);
                }
                isSuccess = true;
            }
        }
        if (!isSuccess){
            //log.info("目标类（{}）不存在该属性（{}）。。",target.getClass().getSimpleName(),key);
        }
    }

    public static void main(String[] args) {
        String[] ingoreProperties = new String[]{"reviewResult","reviewResultZh","restDesc","restDescZh"};
        System.out.println(ingoreProperties.length);

    }

    /**
     * 根据属性名称拷贝map中的属性值到目标对象中的属性，
     * 增加对（BigDecimal、Integer、Date.Long 判断）
     * @param source
     * @param target
     */
    public void mapToBean(Map source,Object target){
        source.forEach((key,val)->{
            setValByKey(target, (String) key,val);
        });
    }


    /**
     * 获取下一步操作人
     * @param resultInstanceDto
     * @return
     */
    public List<String> getNextNodeUserIds(ResultInstanceDto resultInstanceDto) {
        List<String> nextNodeUserIds = null;
        try {
            nextNodeUserIds = resultInstanceDto.getNextNodeInfos().get(0).getNextNodeUserIds();
        } catch (Exception e) {
            log.error("获取下一步操作人失败===》",e);
        }
        return nextNodeUserIds;
    }

    /**
     * 获取系统参数
     * @param configName
     * @return
     */
    public String getSysParameterByName(String configName){
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(configName);
            return adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        } catch (Exception e) {
            log.error("获取系统参数{}失败{}",configName,e);
            return "";
        }
    }

    /**
     * 判断是否为流程第一步
     * @param resultInstanceDto
     * @return
     */
    public boolean checkIsFirstWF_Node(ResultInstanceDto resultInstanceDto) {
        String currentNodeId = resultInstanceDto.getCurrentNodeId();
        ResultDto<Boolean> retVal = workflowCoreClient.isFirstNode(currentNodeId);
        if (retVal != null) {
            return retVal.getData();
        }
        return false;
    }

    /**
     *
     * @param serno
     * @param pageId
     * @param pageName
     * @param appType
     * @param status
     */
    public void insertMustCheckItem(String serno, String pageId, String pageName, String appType, String status){
        BizMustCheckDetails bizMustCheckDetails = new BizMustCheckDetails();
        bizMustCheckDetails.setBizType(appType);
        bizMustCheckDetails.setFinFlag(status);
        bizMustCheckDetails.setSerno(serno);
        bizMustCheckDetails.setPageId(pageId);
        bizMustCheckDetails.setPageName(pageName);
        initInsertDomainProperties(bizMustCheckDetails);
        bizMustCheckDetails.setPkId(UUID.randomUUID().toString());
        bizMustCheckDetailsService.insert(bizMustCheckDetails);
    }

    /**
     * 更新必填页面校验为已完成
     * @param serno
     * @param pageId
     */
    public void updateMustCheckStatus(String serno,String pageId){
        log.info("[updateMustCheckStatus] serno===>"+serno+"  pageId===>"+pageId);
        BizMustCheckDetails bizMustCheckDetails = bizMustCheckDetailsService.selectBySernoAndPageId(serno, pageId);
        if (bizMustCheckDetails != null){
            bizMustCheckDetails.setFinFlag(CmisBizConstants.STD_ZB_YES_NO_Y);
            bizMustCheckDetailsService.update(bizMustCheckDetails);
        }
    }


    /**
     * 获取审批记录拷贝忽略的属性
     * 默认忽略 "reviewResult","reviewResultZh","restDesc","restDescZh"
     * 若 下一节点 或者 当前审批节点是出具核查报告岗位，则用信条件列表不进行copy关联至下一流水号，且INTE_ANALY综合分析 和  REVIEW_RESULT评审结论 和  REST_DESC结论性描述不copy至下一流水号；
     * 若当前审批节点是信贷管理部风险派驻岗节点，则用信条件不进行copy至下一流水号，且INTE_ANALY综合分析 和  REVIEW_RESULT评审结论 和  REST_DESC结论性描述不copy至下一流水号；
     * @param cur_next_id
     * @return
     */
    public String[] getApprIngoreProperties(String cur_next_id) {
        //默认忽略 "reviewResult","reviewResultZh","restDesc","restDescZh"
        String[] ingoreProperties = new String[]{"reviewResult","reviewResultZh","restDesc","restDescZh"};
        String curNodeId = cur_next_id.split(";")[0];
        String nextNodeId = cur_next_id.split(";")[1];
        if (CmisBizConstants.Xdglbfxpzg_NodeIds.contains(curNodeId+",") || CmisBizConstants.TYSX04_02.contains(curNodeId+",") || CmisBizConstants.TYSX04_02.contains(nextNodeId+",")){
            ingoreProperties = new String[]{"inteAnaly","reviewResult","restDesc","reviewResult","reviewResultZh","restDesc","restDescZh"};
        }
        return ingoreProperties;
    }


    /**
     * 检测参数是否为空
     * @param fieldName
     * @param val
     */
    public static void checkParamsIsNull(String fieldName, Object val){
        boolean isEmpty = false;
        if (Objects.isNull(val)){
            isEmpty = true;

        }
        if (val instanceof String && StringUtils.isBlank(val.toString())){
            isEmpty = true;
        }

        if (val instanceof java.util.ArrayList && CollectionUtils.isEmpty((ArrayList)val)){
            isEmpty = true;
        }

        if (isEmpty){
            throw BizException.error(null,"9999",fieldName+"字段不能为空！");
        }
    }

    /**
     * 文件基本信息
     */
    public class TempFileInfo {
        private String fileName;
        private String fileLength;
        private String filePath;
        private String fileId;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFileLength() {
            return fileLength;
        }

        public void setFileLength(String fileLength) {
            this.fileLength = fileLength;
        }

        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(String filePath) {
            this.filePath = filePath;
        }

        public String getFileId() {
            return fileId;
        }

        public void setFileId(String fileId) {
            this.fileId = fileId;
        }

        @Override
        public String toString() {
            return "TempFileInfo{" +
                    "fileName='" + fileName + '\'' +
                    ", fileLength='" + fileLength + '\'' +
                    ", filePath='" + filePath + '\'' +
                    ", fileId='" + fileId + '\'' +
                    '}';
        }
    }
}
