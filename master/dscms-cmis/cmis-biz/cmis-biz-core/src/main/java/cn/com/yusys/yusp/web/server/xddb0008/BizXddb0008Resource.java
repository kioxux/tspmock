package cn.com.yusys.yusp.web.server.xddb0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0008.req.Xddb0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0008.resp.Xddb0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0008.Xddb0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:查询在线抵押信息
 *
 * @author zhangpeng
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDDB0008:查询在线抵押信息")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0008Resource.class);

    @Autowired
    private Xddb0008Service xddb0008Service;

    /**
     * 交易码：xddb0008
     * 交易描述：查询在线抵押信息
     *
     * @param xddb0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询在线抵押信息")
    @PostMapping("/xddb0008")
    protected @ResponseBody
    ResultDto<Xddb0008DataRespDto> xddb0008(@Validated @RequestBody Xddb0008DataReqDto xddb0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008DataReqDto));
        Xddb0008DataRespDto xddb0008DataRespDto = new Xddb0008DataRespDto();// 响应Dto:查询在线抵押信息
        ResultDto<Xddb0008DataRespDto> xddb0008DataResultDto = new ResultDto<>();
        // 从xddb0008DataReqDto获取业务值进行业务逻辑处理
        String pldManName = xddb0008DataReqDto.getPldManName();//抵押人名称
        String managerId = xddb0008DataReqDto.getManagerId();//客户经理号
        String startPageNum = xddb0008DataReqDto.getStartPageNum();//开始页码
        String pageSize = xddb0008DataReqDto.getPageSize();//每页记录数

        try {
            if (StringUtil.isNotEmpty(managerId) && StringUtil.isNotEmpty(startPageNum) && StringUtil.isNotEmpty(pageSize)) {//请求字段非空校验
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008DataReqDto));
                xddb0008DataRespDto = xddb0008Service.selcetGuarBaseInfoByManagerId(xddb0008DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008DataResultDto));
                // 封装xddb0008DataResultDto中正确的返回码和返回信息
                xddb0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                //请求必输字段存在空值
                logger.info("**************************调用xddb0016Service结束*END*************************");
                xddb0008DataResultDto.setCode(EcbEnum.ECB010001.key);
                xddb0008DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008DataResultDto));
            // 封装xddb0008DataResultDto中异常返回码和返回信息
            xddb0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0008DataRespDto到xddb0008DataResultDto中
        xddb0008DataResultDto.setData(xddb0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008DataResultDto));
        return xddb0008DataResultDto;
    }
}
