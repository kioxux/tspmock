package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorize
 * @类描述: pvp_authorize数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-26 21:43:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpAuthorizeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 交易流水号 **/
	private String tranSerno;
	
	/** 放款流水号 **/
	private String pvpSerno;
	
	/** 授权编号 **/
	private String authorizeNo;
	
	/** 授权类型 STD_ZB_AUTH_TYPE **/
	private String authorizeType;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 未受托支付类型 **/
	private String untruPayType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 交易编号 **/
	private String tranId;
	
	/** 账号 **/
	private String acctNo;
	
	/** 账号名称 **/
	private String acctName;
	
	/** 币种  STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 交易金额 **/
	private java.math.BigDecimal tranAmt;
	
	/** 交易日期 **/
	private String tranDate;
	
	/** 发送次数 **/
	private java.math.BigDecimal sendTimes;
	
	/** 返回编码 **/
	private String returnCode;
	
	/** 返回说明 **/
	private String returnDesc;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 放款机构 **/
	private String acctBrId;
	
	/** 授权状态 STD_ZB_AUTH_ST **/
	private String authStatus;
	
	/** 出账状态 STD_JXHJCZ_TYPE **/
	private String tradeStatus;
	
	/** FLDVALUE01 **/
	private String fldvalue01;
	
	/** FLDVALUE02 **/
	private String fldvalue02;
	
	/** FLDVALUE03 **/
	private String fldvalue03;
	
	/** FLDVALUE04 **/
	private String fldvalue04;
	
	/** FLDVALUE05 **/
	private String fldvalue05;
	
	/** FLDVALUE06 **/
	private String fldvalue06;
	
	/** FLDVALUE07 **/
	private String fldvalue07;
	
	/** FLDVALUE08 **/
	private String fldvalue08;
	
	/** FLDVALUE09 **/
	private String fldvalue09;
	
	/** FLDVALUE10 **/
	private String fldvalue10;
	
	/** FLDVALUE11 **/
	private String fldvalue11;
	
	/** FLDVALUE12 **/
	private String fldvalue12;
	
	/** FLDVALUE13 **/
	private String fldvalue13;
	
	/** FLDVALUE14 **/
	private String fldvalue14;
	
	/** FLDVALUE15 **/
	private String fldvalue15;
	
	/** FLDVALUE16 **/
	private String fldvalue16;
	
	/** FLDVALUE17 **/
	private String fldvalue17;
	
	/** FLDVALUE18 **/
	private String fldvalue18;
	
	/** FLDVALUE19 **/
	private String fldvalue19;
	
	/** FLDVALUE20 **/
	private String fldvalue20;
	
	/** FLDVALUE21 **/
	private String fldvalue21;
	
	/** FLDVALUE22 **/
	private String fldvalue22;
	
	/** FLDVALUE23 **/
	private String fldvalue23;
	
	/** FLDVALUE24 **/
	private String fldvalue24;
	
	/** FLDVALUE25 **/
	private String fldvalue25;
	
	/** FLDVALUE26 **/
	private String fldvalue26;
	
	/** FLDVALUE27 **/
	private String fldvalue27;
	
	/** FLDVALUE28 **/
	private String fldvalue28;
	
	/** FLDVALUE29 **/
	private String fldvalue29;
	
	/** FLDVALUE30 **/
	private String fldvalue30;
	
	/** FLDVALUE31 **/
	private String fldvalue31;
	
	/** FLDVALUE32 **/
	private String fldvalue32;
	
	/** FLDVALUE33 **/
	private String fldvalue33;
	
	/** FLDVALUE34 **/
	private String fldvalue34;
	
	/** FLDVALUE35 **/
	private String fldvalue35;
	
	/** FLDVALUE36 **/
	private String fldvalue36;
	
	/** FLDVALUE37 **/
	private String fldvalue37;
	
	/** FLDVALUE38 **/
	private String fldvalue38;
	
	/** FLDVALUE39 **/
	private String fldvalue39;
	
	/** FLDVALUE40 **/
	private String fldvalue40;
	
	/** FLDVALUE41 **/
	private String fldvalue41;
	
	/** FLDVALUE42 **/
	private String fldvalue42;
	
	/** FLDVALUE43 **/
	private String fldvalue43;
	
	/** FLDVALUE44 **/
	private String fldvalue44;
	
	/** FLDVALUE45 **/
	private String fldvalue45;
	
	/** FLDVALUE46 **/
	private String fldvalue46;
	
	/** FLDVALUE47 **/
	private String fldvalue47;
	
	/** FLDVALUE48 **/
	private String fldvalue48;
	
	/** FLDVALUE49 **/
	private String fldvalue49;
	
	/** FLDVALUE50 **/
	private String fldvalue50;
	
	/** FLDVALUE51 **/
	private String fldvalue51;
	
	/** FLDVALUE52 **/
	private String fldvalue52;
	
	/** FLDVALUE53 **/
	private String fldvalue53;
	
	/** FLDVALUE54 **/
	private String fldvalue54;
	
	/** FLDVALUE55 **/
	private String fldvalue55;
	
	/** FLDVALUE56 **/
	private String fldvalue56;
	
	/** FLDVALUE57 **/
	private String fldvalue57;
	
	/** FLDVALUE58 **/
	private String fldvalue58;
	
	/** FLDVALUE59 **/
	private String fldvalue59;
	
	/** FLDVALUE60 **/
	private String fldvalue60;
	
	/** FLDVALUE61 **/
	private String fldvalue61;
	
	/** FLDVALUE62 **/
	private String fldvalue62;
	
	/** FLDVALUE63 **/
	private String fldvalue63;
	
	/** FLDVALUE64 **/
	private String fldvalue64;
	
	/** FLDVALUE65 **/
	private String fldvalue65;
	
	/** FLDVALUE66 **/
	private String fldvalue66;
	
	/** FLDVALUE67 **/
	private String fldvalue67;
	
	/** FLDVALUE68 **/
	private String fldvalue68;
	
	/** FLDVALUE69 **/
	private String fldvalue69;
	
	/** FLDVALUE70 **/
	private String fldvalue70;
	
	/** FLDVALUE71 **/
	private String fldvalue71;
	
	/** FLDVALUE72 **/
	private String fldvalue72;
	
	/** FLDVALUE73 **/
	private String fldvalue73;
	
	/** FLDVALUE74 **/
	private String fldvalue74;
	
	/** FLDVALUE75 **/
	private String fldvalue75;
	
	/** FLDVALUE76 **/
	private String fldvalue76;
	
	/** FLDVALUE77 **/
	private String fldvalue77;
	
	/** FLDVALUE78 **/
	private String fldvalue78;
	
	/** FLDVALUE79 **/
	private String fldvalue79;
	
	/** FLDVALUE80 **/
	private String fldvalue80;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno == null ? null : tranSerno.trim();
	}
	
    /**
     * @return TranSerno
     */	
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param authorizeNo
	 */
	public void setAuthorizeNo(String authorizeNo) {
		this.authorizeNo = authorizeNo == null ? null : authorizeNo.trim();
	}
	
    /**
     * @return AuthorizeNo
     */	
	public String getAuthorizeNo() {
		return this.authorizeNo;
	}
	
	/**
	 * @param authorizeType
	 */
	public void setAuthorizeType(String authorizeType) {
		this.authorizeType = authorizeType == null ? null : authorizeType.trim();
	}
	
    /**
     * @return AuthorizeType
     */	
	public String getAuthorizeType() {
		return this.authorizeType;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param untruPayType
	 */
	public void setUntruPayType(String untruPayType) {
		this.untruPayType = untruPayType == null ? null : untruPayType.trim();
	}
	
    /**
     * @return UntruPayType
     */	
	public String getUntruPayType() {
		return this.untruPayType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param tranId
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId == null ? null : tranId.trim();
	}
	
    /**
     * @return TranId
     */	
	public String getTranId() {
		return this.tranId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}
	
    /**
     * @return AcctName
     */	
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return TranAmt
     */	
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate == null ? null : tranDate.trim();
	}
	
    /**
     * @return TranDate
     */	
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param sendTimes
	 */
	public void setSendTimes(java.math.BigDecimal sendTimes) {
		this.sendTimes = sendTimes;
	}
	
    /**
     * @return SendTimes
     */	
	public java.math.BigDecimal getSendTimes() {
		return this.sendTimes;
	}
	
	/**
	 * @param returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode == null ? null : returnCode.trim();
	}
	
    /**
     * @return ReturnCode
     */	
	public String getReturnCode() {
		return this.returnCode;
	}
	
	/**
	 * @param returnDesc
	 */
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc == null ? null : returnDesc.trim();
	}
	
    /**
     * @return ReturnDesc
     */	
	public String getReturnDesc() {
		return this.returnDesc;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param acctBrId
	 */
	public void setAcctBrId(String acctBrId) {
		this.acctBrId = acctBrId == null ? null : acctBrId.trim();
	}
	
    /**
     * @return AcctBrId
     */	
	public String getAcctBrId() {
		return this.acctBrId;
	}
	
	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus == null ? null : authStatus.trim();
	}
	
    /**
     * @return AuthStatus
     */	
	public String getAuthStatus() {
		return this.authStatus;
	}
	
	/**
	 * @param tradeStatus
	 */
	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus == null ? null : tradeStatus.trim();
	}
	
    /**
     * @return TradeStatus
     */	
	public String getTradeStatus() {
		return this.tradeStatus;
	}
	
	/**
	 * @param fldvalue01
	 */
	public void setFldvalue01(String fldvalue01) {
		this.fldvalue01 = fldvalue01 == null ? null : fldvalue01.trim();
	}
	
    /**
     * @return Fldvalue01
     */	
	public String getFldvalue01() {
		return this.fldvalue01;
	}
	
	/**
	 * @param fldvalue02
	 */
	public void setFldvalue02(String fldvalue02) {
		this.fldvalue02 = fldvalue02 == null ? null : fldvalue02.trim();
	}
	
    /**
     * @return Fldvalue02
     */	
	public String getFldvalue02() {
		return this.fldvalue02;
	}
	
	/**
	 * @param fldvalue03
	 */
	public void setFldvalue03(String fldvalue03) {
		this.fldvalue03 = fldvalue03 == null ? null : fldvalue03.trim();
	}
	
    /**
     * @return Fldvalue03
     */	
	public String getFldvalue03() {
		return this.fldvalue03;
	}
	
	/**
	 * @param fldvalue04
	 */
	public void setFldvalue04(String fldvalue04) {
		this.fldvalue04 = fldvalue04 == null ? null : fldvalue04.trim();
	}
	
    /**
     * @return Fldvalue04
     */	
	public String getFldvalue04() {
		return this.fldvalue04;
	}
	
	/**
	 * @param fldvalue05
	 */
	public void setFldvalue05(String fldvalue05) {
		this.fldvalue05 = fldvalue05 == null ? null : fldvalue05.trim();
	}
	
    /**
     * @return Fldvalue05
     */	
	public String getFldvalue05() {
		return this.fldvalue05;
	}
	
	/**
	 * @param fldvalue06
	 */
	public void setFldvalue06(String fldvalue06) {
		this.fldvalue06 = fldvalue06 == null ? null : fldvalue06.trim();
	}
	
    /**
     * @return Fldvalue06
     */	
	public String getFldvalue06() {
		return this.fldvalue06;
	}
	
	/**
	 * @param fldvalue07
	 */
	public void setFldvalue07(String fldvalue07) {
		this.fldvalue07 = fldvalue07 == null ? null : fldvalue07.trim();
	}
	
    /**
     * @return Fldvalue07
     */	
	public String getFldvalue07() {
		return this.fldvalue07;
	}
	
	/**
	 * @param fldvalue08
	 */
	public void setFldvalue08(String fldvalue08) {
		this.fldvalue08 = fldvalue08 == null ? null : fldvalue08.trim();
	}
	
    /**
     * @return Fldvalue08
     */	
	public String getFldvalue08() {
		return this.fldvalue08;
	}
	
	/**
	 * @param fldvalue09
	 */
	public void setFldvalue09(String fldvalue09) {
		this.fldvalue09 = fldvalue09 == null ? null : fldvalue09.trim();
	}
	
    /**
     * @return Fldvalue09
     */	
	public String getFldvalue09() {
		return this.fldvalue09;
	}
	
	/**
	 * @param fldvalue10
	 */
	public void setFldvalue10(String fldvalue10) {
		this.fldvalue10 = fldvalue10 == null ? null : fldvalue10.trim();
	}
	
    /**
     * @return Fldvalue10
     */	
	public String getFldvalue10() {
		return this.fldvalue10;
	}
	
	/**
	 * @param fldvalue11
	 */
	public void setFldvalue11(String fldvalue11) {
		this.fldvalue11 = fldvalue11 == null ? null : fldvalue11.trim();
	}
	
    /**
     * @return Fldvalue11
     */	
	public String getFldvalue11() {
		return this.fldvalue11;
	}
	
	/**
	 * @param fldvalue12
	 */
	public void setFldvalue12(String fldvalue12) {
		this.fldvalue12 = fldvalue12 == null ? null : fldvalue12.trim();
	}
	
    /**
     * @return Fldvalue12
     */	
	public String getFldvalue12() {
		return this.fldvalue12;
	}
	
	/**
	 * @param fldvalue13
	 */
	public void setFldvalue13(String fldvalue13) {
		this.fldvalue13 = fldvalue13 == null ? null : fldvalue13.trim();
	}
	
    /**
     * @return Fldvalue13
     */	
	public String getFldvalue13() {
		return this.fldvalue13;
	}
	
	/**
	 * @param fldvalue14
	 */
	public void setFldvalue14(String fldvalue14) {
		this.fldvalue14 = fldvalue14 == null ? null : fldvalue14.trim();
	}
	
    /**
     * @return Fldvalue14
     */	
	public String getFldvalue14() {
		return this.fldvalue14;
	}
	
	/**
	 * @param fldvalue15
	 */
	public void setFldvalue15(String fldvalue15) {
		this.fldvalue15 = fldvalue15 == null ? null : fldvalue15.trim();
	}
	
    /**
     * @return Fldvalue15
     */	
	public String getFldvalue15() {
		return this.fldvalue15;
	}
	
	/**
	 * @param fldvalue16
	 */
	public void setFldvalue16(String fldvalue16) {
		this.fldvalue16 = fldvalue16 == null ? null : fldvalue16.trim();
	}
	
    /**
     * @return Fldvalue16
     */	
	public String getFldvalue16() {
		return this.fldvalue16;
	}
	
	/**
	 * @param fldvalue17
	 */
	public void setFldvalue17(String fldvalue17) {
		this.fldvalue17 = fldvalue17 == null ? null : fldvalue17.trim();
	}
	
    /**
     * @return Fldvalue17
     */	
	public String getFldvalue17() {
		return this.fldvalue17;
	}
	
	/**
	 * @param fldvalue18
	 */
	public void setFldvalue18(String fldvalue18) {
		this.fldvalue18 = fldvalue18 == null ? null : fldvalue18.trim();
	}
	
    /**
     * @return Fldvalue18
     */	
	public String getFldvalue18() {
		return this.fldvalue18;
	}
	
	/**
	 * @param fldvalue19
	 */
	public void setFldvalue19(String fldvalue19) {
		this.fldvalue19 = fldvalue19 == null ? null : fldvalue19.trim();
	}
	
    /**
     * @return Fldvalue19
     */	
	public String getFldvalue19() {
		return this.fldvalue19;
	}
	
	/**
	 * @param fldvalue20
	 */
	public void setFldvalue20(String fldvalue20) {
		this.fldvalue20 = fldvalue20 == null ? null : fldvalue20.trim();
	}
	
    /**
     * @return Fldvalue20
     */	
	public String getFldvalue20() {
		return this.fldvalue20;
	}
	
	/**
	 * @param fldvalue21
	 */
	public void setFldvalue21(String fldvalue21) {
		this.fldvalue21 = fldvalue21 == null ? null : fldvalue21.trim();
	}
	
    /**
     * @return Fldvalue21
     */	
	public String getFldvalue21() {
		return this.fldvalue21;
	}
	
	/**
	 * @param fldvalue22
	 */
	public void setFldvalue22(String fldvalue22) {
		this.fldvalue22 = fldvalue22 == null ? null : fldvalue22.trim();
	}
	
    /**
     * @return Fldvalue22
     */	
	public String getFldvalue22() {
		return this.fldvalue22;
	}
	
	/**
	 * @param fldvalue23
	 */
	public void setFldvalue23(String fldvalue23) {
		this.fldvalue23 = fldvalue23 == null ? null : fldvalue23.trim();
	}
	
    /**
     * @return Fldvalue23
     */	
	public String getFldvalue23() {
		return this.fldvalue23;
	}
	
	/**
	 * @param fldvalue24
	 */
	public void setFldvalue24(String fldvalue24) {
		this.fldvalue24 = fldvalue24 == null ? null : fldvalue24.trim();
	}
	
    /**
     * @return Fldvalue24
     */	
	public String getFldvalue24() {
		return this.fldvalue24;
	}
	
	/**
	 * @param fldvalue25
	 */
	public void setFldvalue25(String fldvalue25) {
		this.fldvalue25 = fldvalue25 == null ? null : fldvalue25.trim();
	}
	
    /**
     * @return Fldvalue25
     */	
	public String getFldvalue25() {
		return this.fldvalue25;
	}
	
	/**
	 * @param fldvalue26
	 */
	public void setFldvalue26(String fldvalue26) {
		this.fldvalue26 = fldvalue26 == null ? null : fldvalue26.trim();
	}
	
    /**
     * @return Fldvalue26
     */	
	public String getFldvalue26() {
		return this.fldvalue26;
	}
	
	/**
	 * @param fldvalue27
	 */
	public void setFldvalue27(String fldvalue27) {
		this.fldvalue27 = fldvalue27 == null ? null : fldvalue27.trim();
	}
	
    /**
     * @return Fldvalue27
     */	
	public String getFldvalue27() {
		return this.fldvalue27;
	}
	
	/**
	 * @param fldvalue28
	 */
	public void setFldvalue28(String fldvalue28) {
		this.fldvalue28 = fldvalue28 == null ? null : fldvalue28.trim();
	}
	
    /**
     * @return Fldvalue28
     */	
	public String getFldvalue28() {
		return this.fldvalue28;
	}
	
	/**
	 * @param fldvalue29
	 */
	public void setFldvalue29(String fldvalue29) {
		this.fldvalue29 = fldvalue29 == null ? null : fldvalue29.trim();
	}
	
    /**
     * @return Fldvalue29
     */	
	public String getFldvalue29() {
		return this.fldvalue29;
	}
	
	/**
	 * @param fldvalue30
	 */
	public void setFldvalue30(String fldvalue30) {
		this.fldvalue30 = fldvalue30 == null ? null : fldvalue30.trim();
	}
	
    /**
     * @return Fldvalue30
     */	
	public String getFldvalue30() {
		return this.fldvalue30;
	}
	
	/**
	 * @param fldvalue31
	 */
	public void setFldvalue31(String fldvalue31) {
		this.fldvalue31 = fldvalue31 == null ? null : fldvalue31.trim();
	}
	
    /**
     * @return Fldvalue31
     */	
	public String getFldvalue31() {
		return this.fldvalue31;
	}
	
	/**
	 * @param fldvalue32
	 */
	public void setFldvalue32(String fldvalue32) {
		this.fldvalue32 = fldvalue32 == null ? null : fldvalue32.trim();
	}
	
    /**
     * @return Fldvalue32
     */	
	public String getFldvalue32() {
		return this.fldvalue32;
	}
	
	/**
	 * @param fldvalue33
	 */
	public void setFldvalue33(String fldvalue33) {
		this.fldvalue33 = fldvalue33 == null ? null : fldvalue33.trim();
	}
	
    /**
     * @return Fldvalue33
     */	
	public String getFldvalue33() {
		return this.fldvalue33;
	}
	
	/**
	 * @param fldvalue34
	 */
	public void setFldvalue34(String fldvalue34) {
		this.fldvalue34 = fldvalue34 == null ? null : fldvalue34.trim();
	}
	
    /**
     * @return Fldvalue34
     */	
	public String getFldvalue34() {
		return this.fldvalue34;
	}
	
	/**
	 * @param fldvalue35
	 */
	public void setFldvalue35(String fldvalue35) {
		this.fldvalue35 = fldvalue35 == null ? null : fldvalue35.trim();
	}
	
    /**
     * @return Fldvalue35
     */	
	public String getFldvalue35() {
		return this.fldvalue35;
	}
	
	/**
	 * @param fldvalue36
	 */
	public void setFldvalue36(String fldvalue36) {
		this.fldvalue36 = fldvalue36 == null ? null : fldvalue36.trim();
	}
	
    /**
     * @return Fldvalue36
     */	
	public String getFldvalue36() {
		return this.fldvalue36;
	}
	
	/**
	 * @param fldvalue37
	 */
	public void setFldvalue37(String fldvalue37) {
		this.fldvalue37 = fldvalue37 == null ? null : fldvalue37.trim();
	}
	
    /**
     * @return Fldvalue37
     */	
	public String getFldvalue37() {
		return this.fldvalue37;
	}
	
	/**
	 * @param fldvalue38
	 */
	public void setFldvalue38(String fldvalue38) {
		this.fldvalue38 = fldvalue38 == null ? null : fldvalue38.trim();
	}
	
    /**
     * @return Fldvalue38
     */	
	public String getFldvalue38() {
		return this.fldvalue38;
	}
	
	/**
	 * @param fldvalue39
	 */
	public void setFldvalue39(String fldvalue39) {
		this.fldvalue39 = fldvalue39 == null ? null : fldvalue39.trim();
	}
	
    /**
     * @return Fldvalue39
     */	
	public String getFldvalue39() {
		return this.fldvalue39;
	}
	
	/**
	 * @param fldvalue40
	 */
	public void setFldvalue40(String fldvalue40) {
		this.fldvalue40 = fldvalue40 == null ? null : fldvalue40.trim();
	}
	
    /**
     * @return Fldvalue40
     */	
	public String getFldvalue40() {
		return this.fldvalue40;
	}
	
	/**
	 * @param fldvalue41
	 */
	public void setFldvalue41(String fldvalue41) {
		this.fldvalue41 = fldvalue41 == null ? null : fldvalue41.trim();
	}
	
    /**
     * @return Fldvalue41
     */	
	public String getFldvalue41() {
		return this.fldvalue41;
	}
	
	/**
	 * @param fldvalue42
	 */
	public void setFldvalue42(String fldvalue42) {
		this.fldvalue42 = fldvalue42 == null ? null : fldvalue42.trim();
	}
	
    /**
     * @return Fldvalue42
     */	
	public String getFldvalue42() {
		return this.fldvalue42;
	}
	
	/**
	 * @param fldvalue43
	 */
	public void setFldvalue43(String fldvalue43) {
		this.fldvalue43 = fldvalue43 == null ? null : fldvalue43.trim();
	}
	
    /**
     * @return Fldvalue43
     */	
	public String getFldvalue43() {
		return this.fldvalue43;
	}
	
	/**
	 * @param fldvalue44
	 */
	public void setFldvalue44(String fldvalue44) {
		this.fldvalue44 = fldvalue44 == null ? null : fldvalue44.trim();
	}
	
    /**
     * @return Fldvalue44
     */	
	public String getFldvalue44() {
		return this.fldvalue44;
	}
	
	/**
	 * @param fldvalue45
	 */
	public void setFldvalue45(String fldvalue45) {
		this.fldvalue45 = fldvalue45 == null ? null : fldvalue45.trim();
	}
	
    /**
     * @return Fldvalue45
     */	
	public String getFldvalue45() {
		return this.fldvalue45;
	}
	
	/**
	 * @param fldvalue46
	 */
	public void setFldvalue46(String fldvalue46) {
		this.fldvalue46 = fldvalue46 == null ? null : fldvalue46.trim();
	}
	
    /**
     * @return Fldvalue46
     */	
	public String getFldvalue46() {
		return this.fldvalue46;
	}
	
	/**
	 * @param fldvalue47
	 */
	public void setFldvalue47(String fldvalue47) {
		this.fldvalue47 = fldvalue47 == null ? null : fldvalue47.trim();
	}
	
    /**
     * @return Fldvalue47
     */	
	public String getFldvalue47() {
		return this.fldvalue47;
	}
	
	/**
	 * @param fldvalue48
	 */
	public void setFldvalue48(String fldvalue48) {
		this.fldvalue48 = fldvalue48 == null ? null : fldvalue48.trim();
	}
	
    /**
     * @return Fldvalue48
     */	
	public String getFldvalue48() {
		return this.fldvalue48;
	}
	
	/**
	 * @param fldvalue49
	 */
	public void setFldvalue49(String fldvalue49) {
		this.fldvalue49 = fldvalue49 == null ? null : fldvalue49.trim();
	}
	
    /**
     * @return Fldvalue49
     */	
	public String getFldvalue49() {
		return this.fldvalue49;
	}
	
	/**
	 * @param fldvalue50
	 */
	public void setFldvalue50(String fldvalue50) {
		this.fldvalue50 = fldvalue50 == null ? null : fldvalue50.trim();
	}
	
    /**
     * @return Fldvalue50
     */	
	public String getFldvalue50() {
		return this.fldvalue50;
	}
	
	/**
	 * @param fldvalue51
	 */
	public void setFldvalue51(String fldvalue51) {
		this.fldvalue51 = fldvalue51 == null ? null : fldvalue51.trim();
	}
	
    /**
     * @return Fldvalue51
     */	
	public String getFldvalue51() {
		return this.fldvalue51;
	}
	
	/**
	 * @param fldvalue52
	 */
	public void setFldvalue52(String fldvalue52) {
		this.fldvalue52 = fldvalue52 == null ? null : fldvalue52.trim();
	}
	
    /**
     * @return Fldvalue52
     */	
	public String getFldvalue52() {
		return this.fldvalue52;
	}
	
	/**
	 * @param fldvalue53
	 */
	public void setFldvalue53(String fldvalue53) {
		this.fldvalue53 = fldvalue53 == null ? null : fldvalue53.trim();
	}
	
    /**
     * @return Fldvalue53
     */	
	public String getFldvalue53() {
		return this.fldvalue53;
	}
	
	/**
	 * @param fldvalue54
	 */
	public void setFldvalue54(String fldvalue54) {
		this.fldvalue54 = fldvalue54 == null ? null : fldvalue54.trim();
	}
	
    /**
     * @return Fldvalue54
     */	
	public String getFldvalue54() {
		return this.fldvalue54;
	}
	
	/**
	 * @param fldvalue55
	 */
	public void setFldvalue55(String fldvalue55) {
		this.fldvalue55 = fldvalue55 == null ? null : fldvalue55.trim();
	}
	
    /**
     * @return Fldvalue55
     */	
	public String getFldvalue55() {
		return this.fldvalue55;
	}
	
	/**
	 * @param fldvalue56
	 */
	public void setFldvalue56(String fldvalue56) {
		this.fldvalue56 = fldvalue56 == null ? null : fldvalue56.trim();
	}
	
    /**
     * @return Fldvalue56
     */	
	public String getFldvalue56() {
		return this.fldvalue56;
	}
	
	/**
	 * @param fldvalue57
	 */
	public void setFldvalue57(String fldvalue57) {
		this.fldvalue57 = fldvalue57 == null ? null : fldvalue57.trim();
	}
	
    /**
     * @return Fldvalue57
     */	
	public String getFldvalue57() {
		return this.fldvalue57;
	}
	
	/**
	 * @param fldvalue58
	 */
	public void setFldvalue58(String fldvalue58) {
		this.fldvalue58 = fldvalue58 == null ? null : fldvalue58.trim();
	}
	
    /**
     * @return Fldvalue58
     */	
	public String getFldvalue58() {
		return this.fldvalue58;
	}
	
	/**
	 * @param fldvalue59
	 */
	public void setFldvalue59(String fldvalue59) {
		this.fldvalue59 = fldvalue59 == null ? null : fldvalue59.trim();
	}
	
    /**
     * @return Fldvalue59
     */	
	public String getFldvalue59() {
		return this.fldvalue59;
	}
	
	/**
	 * @param fldvalue60
	 */
	public void setFldvalue60(String fldvalue60) {
		this.fldvalue60 = fldvalue60 == null ? null : fldvalue60.trim();
	}
	
    /**
     * @return Fldvalue60
     */	
	public String getFldvalue60() {
		return this.fldvalue60;
	}
	
	/**
	 * @param fldvalue61
	 */
	public void setFldvalue61(String fldvalue61) {
		this.fldvalue61 = fldvalue61 == null ? null : fldvalue61.trim();
	}
	
    /**
     * @return Fldvalue61
     */	
	public String getFldvalue61() {
		return this.fldvalue61;
	}
	
	/**
	 * @param fldvalue62
	 */
	public void setFldvalue62(String fldvalue62) {
		this.fldvalue62 = fldvalue62 == null ? null : fldvalue62.trim();
	}
	
    /**
     * @return Fldvalue62
     */	
	public String getFldvalue62() {
		return this.fldvalue62;
	}
	
	/**
	 * @param fldvalue63
	 */
	public void setFldvalue63(String fldvalue63) {
		this.fldvalue63 = fldvalue63 == null ? null : fldvalue63.trim();
	}
	
    /**
     * @return Fldvalue63
     */	
	public String getFldvalue63() {
		return this.fldvalue63;
	}
	
	/**
	 * @param fldvalue64
	 */
	public void setFldvalue64(String fldvalue64) {
		this.fldvalue64 = fldvalue64 == null ? null : fldvalue64.trim();
	}
	
    /**
     * @return Fldvalue64
     */	
	public String getFldvalue64() {
		return this.fldvalue64;
	}
	
	/**
	 * @param fldvalue65
	 */
	public void setFldvalue65(String fldvalue65) {
		this.fldvalue65 = fldvalue65 == null ? null : fldvalue65.trim();
	}
	
    /**
     * @return Fldvalue65
     */	
	public String getFldvalue65() {
		return this.fldvalue65;
	}
	
	/**
	 * @param fldvalue66
	 */
	public void setFldvalue66(String fldvalue66) {
		this.fldvalue66 = fldvalue66 == null ? null : fldvalue66.trim();
	}
	
    /**
     * @return Fldvalue66
     */	
	public String getFldvalue66() {
		return this.fldvalue66;
	}
	
	/**
	 * @param fldvalue67
	 */
	public void setFldvalue67(String fldvalue67) {
		this.fldvalue67 = fldvalue67 == null ? null : fldvalue67.trim();
	}
	
    /**
     * @return Fldvalue67
     */	
	public String getFldvalue67() {
		return this.fldvalue67;
	}
	
	/**
	 * @param fldvalue68
	 */
	public void setFldvalue68(String fldvalue68) {
		this.fldvalue68 = fldvalue68 == null ? null : fldvalue68.trim();
	}
	
    /**
     * @return Fldvalue68
     */	
	public String getFldvalue68() {
		return this.fldvalue68;
	}
	
	/**
	 * @param fldvalue69
	 */
	public void setFldvalue69(String fldvalue69) {
		this.fldvalue69 = fldvalue69 == null ? null : fldvalue69.trim();
	}
	
    /**
     * @return Fldvalue69
     */	
	public String getFldvalue69() {
		return this.fldvalue69;
	}
	
	/**
	 * @param fldvalue70
	 */
	public void setFldvalue70(String fldvalue70) {
		this.fldvalue70 = fldvalue70 == null ? null : fldvalue70.trim();
	}
	
    /**
     * @return Fldvalue70
     */	
	public String getFldvalue70() {
		return this.fldvalue70;
	}
	
	/**
	 * @param fldvalue71
	 */
	public void setFldvalue71(String fldvalue71) {
		this.fldvalue71 = fldvalue71 == null ? null : fldvalue71.trim();
	}
	
    /**
     * @return Fldvalue71
     */	
	public String getFldvalue71() {
		return this.fldvalue71;
	}
	
	/**
	 * @param fldvalue72
	 */
	public void setFldvalue72(String fldvalue72) {
		this.fldvalue72 = fldvalue72 == null ? null : fldvalue72.trim();
	}
	
    /**
     * @return Fldvalue72
     */	
	public String getFldvalue72() {
		return this.fldvalue72;
	}
	
	/**
	 * @param fldvalue73
	 */
	public void setFldvalue73(String fldvalue73) {
		this.fldvalue73 = fldvalue73 == null ? null : fldvalue73.trim();
	}
	
    /**
     * @return Fldvalue73
     */	
	public String getFldvalue73() {
		return this.fldvalue73;
	}
	
	/**
	 * @param fldvalue74
	 */
	public void setFldvalue74(String fldvalue74) {
		this.fldvalue74 = fldvalue74 == null ? null : fldvalue74.trim();
	}
	
    /**
     * @return Fldvalue74
     */	
	public String getFldvalue74() {
		return this.fldvalue74;
	}
	
	/**
	 * @param fldvalue75
	 */
	public void setFldvalue75(String fldvalue75) {
		this.fldvalue75 = fldvalue75 == null ? null : fldvalue75.trim();
	}
	
    /**
     * @return Fldvalue75
     */	
	public String getFldvalue75() {
		return this.fldvalue75;
	}
	
	/**
	 * @param fldvalue76
	 */
	public void setFldvalue76(String fldvalue76) {
		this.fldvalue76 = fldvalue76 == null ? null : fldvalue76.trim();
	}
	
    /**
     * @return Fldvalue76
     */	
	public String getFldvalue76() {
		return this.fldvalue76;
	}
	
	/**
	 * @param fldvalue77
	 */
	public void setFldvalue77(String fldvalue77) {
		this.fldvalue77 = fldvalue77 == null ? null : fldvalue77.trim();
	}
	
    /**
     * @return Fldvalue77
     */	
	public String getFldvalue77() {
		return this.fldvalue77;
	}
	
	/**
	 * @param fldvalue78
	 */
	public void setFldvalue78(String fldvalue78) {
		this.fldvalue78 = fldvalue78 == null ? null : fldvalue78.trim();
	}
	
    /**
     * @return Fldvalue78
     */	
	public String getFldvalue78() {
		return this.fldvalue78;
	}
	
	/**
	 * @param fldvalue79
	 */
	public void setFldvalue79(String fldvalue79) {
		this.fldvalue79 = fldvalue79 == null ? null : fldvalue79.trim();
	}
	
    /**
     * @return Fldvalue79
     */	
	public String getFldvalue79() {
		return this.fldvalue79;
	}
	
	/**
	 * @param fldvalue80
	 */
	public void setFldvalue80(String fldvalue80) {
		this.fldvalue80 = fldvalue80 == null ? null : fldvalue80.trim();
	}
	
    /**
     * @return Fldvalue80
     */	
	public String getFldvalue80() {
		return this.fldvalue80;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}