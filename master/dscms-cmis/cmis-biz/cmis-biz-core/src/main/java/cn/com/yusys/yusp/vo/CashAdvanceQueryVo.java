package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CashAdvance
 * @类描述: cash_advance数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-11-15 15:43:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CashAdvanceQueryVo extends PageQuery{
	
	/** 放款流水号 **/
	private String pvpSerno;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 垫款借据编号 **/
	private String cashBillNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 调查编号 **/
	private String surveySerno;
	
	/** 是否使用授信额度 **/
	private String isUtilLmt;
	
	/** 授信额度编号 **/
	private String lmtAccNo;
	
	/** 批复编号 **/
	private String replyNo;
	
	/** 中文合同编号 **/
	private String contCnNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品类型属性 **/
	private String prdTypeProp;
	
	/** 贷款形式 **/
	private String loanModal;
	
	/** 借新还旧类型 **/
	private String refinancingType;
	
	/** 借新还旧标识 **/
	private String refinancingFlag;
	
	/** FTP剔除考核 **/
	private String ftp;
	
	/** 是否为疫情相关企业 **/
	private String isEpidemicCorreCon;
	
	/** 出账模式 **/
	private String pvpMode;
	
	/** 合同影像是否审核 **/
	private String isContImageAudit;
	
	/** 合同币种 **/
	private String contCurType;
	
	/** 折算人民币金额 **/
	private java.math.BigDecimal cvtCnyAmt;
	
	/** 汇率 **/
	private java.math.BigDecimal exchangeRate;
	
	/** 合同最高可放金额 **/
	private java.math.BigDecimal contHighDisb;
	
	/** 起始日 **/
	private String startDate;
	
	/** 到期日 **/
	private String endDate;
	
	/** 是否用印 **/
	private String isSeal;
	
	/** 贷款起始日 **/
	private String loanStartDate;
	
	/** 贷款到期日 **/
	private String loanEndDate;
	
	/** 贷款期限 **/
	private String loanTerm;
	
	/** 贷款期限单位 **/
	private String loanTermUnit;
	
	/** 利率调整方式 **/
	private String rateAdjMode;
	
	/** 借款利率调整日 **/
	private String loanRateAdjDay;
	
	/** 是否分段计息 **/
	private String isSegInterest;
	
	/** 是否自动审批中 **/
	private String isAutoAppr;
	
	/** LPR授信利率区间 **/
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	private java.math.BigDecimal curtLprRate;
	
	/** 正常利率浮动方式 **/
	private String irFloatType;
	
	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;
	
	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;
	
	/** 执行年利率 **/
	private java.math.BigDecimal execRateYear;
	
	/** 逾期利率浮动比 **/
	private java.math.BigDecimal overdueRatePefloat;
	
	/** 逾期执行利率(年利率) **/
	private java.math.BigDecimal overdueExecRate;
	
	/** 复息利率浮动比 **/
	private java.math.BigDecimal ciRatePefloat;
	
	/** 复息执行利率(年利率) **/
	private java.math.BigDecimal ciExecRate;
	
	/** 违约利率浮动百分比 **/
	private java.math.BigDecimal defaultRate;
	
	/** 违约利率（年） **/
	private java.math.BigDecimal defaultRateY;
	
	/** 利率调整选项 **/
	private String rateAdjType;
	
	/** 下一次利率调整间隔 **/
	private String nextRateAdjInterval;
	
	/** 下一次利率调整间隔单位 **/
	private String nextRateAdjUnit;
	
	/** 第一次调整日 **/
	private String firstAdjDate;
	
	/** 还款方式 **/
	private String repayMode;
	
	/** 结息间隔周期 **/
	private String eiIntervalCycle;
	
	/** 结息间隔周期单位 **/
	private String eiIntervalUnit;
	
	/** 扣款方式 **/
	private String deductType;
	
	/** 扣款日 **/
	private String deductDay;
	
	/** 贷款发放账号 **/
	private String loanPayoutAccno;
	
	/** 贷款发放账号子序号 **/
	private String loanPayoutSubNo;
	
	/** 发放账号名称 **/
	private String payoutAcctName;
	
	/** 是否受托支付 **/
	private String isBeEntrustedPay;
	
	/** 贷款还款账号 **/
	private String repayAccno;
	
	/** 贷款还款账户子序号 **/
	private String repaySubAccno;
	
	/** 还款账户名称 **/
	private String repayAcctName;
	
	/** 贷款承诺标志 **/
	private String loanPromiseFlag;
	
	/** 贷款承诺类型 **/
	private String loanPromiseType;
	
	/** 贴息人存款账号 **/
	private String sbsyDepAccno;
	
	/** 贴息比例 **/
	private java.math.BigDecimal sbsyPerc;
	
	/** 贴息到期日 **/
	private String sbysEnddate;
	
	/** 贷款类别细分 **/
	private String loanTypeDetail;
	
	/** 是否落实贷款 **/
	private String isPactLoan;
	
	/** 是否绿色产业 **/
	private String isGreenIndustry;
	
	/** 是否钢贸行业贷款 **/
	private String isSteelLoan;
	
	/** 是否不锈钢行业贷款 **/
	private String isStainlessLoan;
	
	/** 是否扶贫贴息贷款 **/
	private String isPovertyReliefLoan;
	
	/** 是否劳动密集型小企业贴息贷款 **/
	private String isLaborIntenSbsyLoan;
	
	/** 保障性安居工程贷款 **/
	private String goverSubszHouseLoan;
	
	/** 项目贷款节能环保 **/
	private String engyEnviProteLoan;
	
	/** 是否农村综合开发贷款标志 **/
	private String isCphsRurDelpLoan;
	
	/** 房地产贷款 **/
	private String realproLoan;
	
	/** 房产开发贷款资本金比例 **/
	private java.math.BigDecimal realproLoanRate;
	
	/** 账务机构编号 **/
	private String finaBrId;
	
	/** 账务机构名称 **/
	private String finaBrIdName;
	
	/** 放款机构编号 **/
	private String disbOrgNo;
	
	/** 放款机构名称 **/
	private String disbOrgName;
	
	/** 贷款担保方式 **/
	private String guarMode;
	
	/** 农户类型 **/
	private String agriType;
	
	/** 涉农贷款投向 **/
	private String agriLoanTer;
	
	/** 是否资料补充 **/
	private String isMaterComp;
	
	/** 担保方式明细 **/
	private String guarDetailMode;
	
	/** 是否经营性物业贷款 **/
	private String isOperPropertyLoan;
	
	/** 贷款投向 **/
	private String loanTer;
	
	/** 贷款科目号 **/
	private String loanSubjectNo;
	
	/** 借款用途类型 **/
	private String loanUseType;
	
	/** 其他借款用途描述 **/
	private String loanUseTypeDesc;
	
	/** 产业结构类型 **/
	private String estateType;
	
	/** 工业结构转型升级标识 **/
	private String indtUpFlag;
	
	/** 战略新兴产业类型 **/
	private String strategyNewLoan;
	
	/** 文化产业标识 **/
	private String culIndustryFlag;
	
	/** 本金自动归还标志 **/
	private String capAutobackFlag;
	
	/** 是否贴息 **/
	private String isSbsy;
	
	/** 节假日是否顺延 **/
	private String isHolidayDelay;
	
	/** 是否计复息 **/
	private String isMeterCi;
	
	/** 利息自动归还标志 **/
	private String intAutobackFlag;
	
	/** 币种 **/
	private String curType;
	
	/** 合同金额 **/
	private java.math.BigDecimal contAmt;
	
	/** 垫款金额 **/
	private java.math.BigDecimal cashAmt;
	
	/** 支付方式 **/
	private String payMode;
	
	/** 是否省心E付 **/
	private String isSxef;
	
	/** 是否立即发起受托支付 **/
	private String isCfirmPay;
	
	/** 期限类型 **/
	private String termType;
	
	/** 申请期限 **/
	private String appTerm;
	
	/** 客户评级系数 **/
	private java.math.BigDecimal customerRatingFactor;
	
	/** 其他借款用途 **/
	private String otherLoanPurp;
	
	/** 授权状态 **/
	private String authStatus;
	
	/** 渠道来源 **/
	private String chnlSour;
	
	/** 申请状态 **/
	private String approveStatus;
	
	/** 是否先放款后抵押 **/
	private String beforehandInd;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 所属条线 **/
	private String belgLine;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 是否我行监管账户 **/
	private String isLocalManag;
	
	/** 是否资产池 **/
	private String isPool;
	
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cashBillNo
	 */
	public void setCashBillNo(String cashBillNo) {
		this.cashBillNo = cashBillNo == null ? null : cashBillNo.trim();
	}
	
    /**
     * @return CashBillNo
     */	
	public String getCashBillNo() {
		return this.cashBillNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt == null ? null : isUtilLmt.trim();
	}
	
    /**
     * @return IsUtilLmt
     */	
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo == null ? null : lmtAccNo.trim();
	}
	
    /**
     * @return LmtAccNo
     */	
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo == null ? null : replyNo.trim();
	}
	
    /**
     * @return ReplyNo
     */	
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo == null ? null : contCnNo.trim();
	}
	
    /**
     * @return ContCnNo
     */	
	public String getContCnNo() {
		return this.contCnNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp == null ? null : prdTypeProp.trim();
	}
	
    /**
     * @return PrdTypeProp
     */	
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal == null ? null : loanModal.trim();
	}
	
    /**
     * @return LoanModal
     */	
	public String getLoanModal() {
		return this.loanModal;
	}
	
	/**
	 * @param refinancingType
	 */
	public void setRefinancingType(String refinancingType) {
		this.refinancingType = refinancingType == null ? null : refinancingType.trim();
	}
	
    /**
     * @return RefinancingType
     */	
	public String getRefinancingType() {
		return this.refinancingType;
	}
	
	/**
	 * @param refinancingFlag
	 */
	public void setRefinancingFlag(String refinancingFlag) {
		this.refinancingFlag = refinancingFlag == null ? null : refinancingFlag.trim();
	}
	
    /**
     * @return RefinancingFlag
     */	
	public String getRefinancingFlag() {
		return this.refinancingFlag;
	}
	
	/**
	 * @param ftp
	 */
	public void setFtp(String ftp) {
		this.ftp = ftp == null ? null : ftp.trim();
	}
	
    /**
     * @return Ftp
     */	
	public String getFtp() {
		return this.ftp;
	}
	
	/**
	 * @param isEpidemicCorreCon
	 */
	public void setIsEpidemicCorreCon(String isEpidemicCorreCon) {
		this.isEpidemicCorreCon = isEpidemicCorreCon == null ? null : isEpidemicCorreCon.trim();
	}
	
    /**
     * @return IsEpidemicCorreCon
     */	
	public String getIsEpidemicCorreCon() {
		return this.isEpidemicCorreCon;
	}
	
	/**
	 * @param pvpMode
	 */
	public void setPvpMode(String pvpMode) {
		this.pvpMode = pvpMode == null ? null : pvpMode.trim();
	}
	
    /**
     * @return PvpMode
     */	
	public String getPvpMode() {
		return this.pvpMode;
	}
	
	/**
	 * @param isContImageAudit
	 */
	public void setIsContImageAudit(String isContImageAudit) {
		this.isContImageAudit = isContImageAudit == null ? null : isContImageAudit.trim();
	}
	
    /**
     * @return IsContImageAudit
     */	
	public String getIsContImageAudit() {
		return this.isContImageAudit;
	}
	
	/**
	 * @param contCurType
	 */
	public void setContCurType(String contCurType) {
		this.contCurType = contCurType == null ? null : contCurType.trim();
	}
	
    /**
     * @return ContCurType
     */	
	public String getContCurType() {
		return this.contCurType;
	}
	
	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}
	
    /**
     * @return CvtCnyAmt
     */	
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return ExchangeRate
     */	
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param contHighDisb
	 */
	public void setContHighDisb(java.math.BigDecimal contHighDisb) {
		this.contHighDisb = contHighDisb;
	}
	
    /**
     * @return ContHighDisb
     */	
	public java.math.BigDecimal getContHighDisb() {
		return this.contHighDisb;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param isSeal
	 */
	public void setIsSeal(String isSeal) {
		this.isSeal = isSeal == null ? null : isSeal.trim();
	}
	
    /**
     * @return IsSeal
     */	
	public String getIsSeal() {
		return this.isSeal;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm == null ? null : loanTerm.trim();
	}
	
    /**
     * @return LoanTerm
     */	
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param loanTermUnit
	 */
	public void setLoanTermUnit(String loanTermUnit) {
		this.loanTermUnit = loanTermUnit == null ? null : loanTermUnit.trim();
	}
	
    /**
     * @return LoanTermUnit
     */	
	public String getLoanTermUnit() {
		return this.loanTermUnit;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode == null ? null : rateAdjMode.trim();
	}
	
    /**
     * @return RateAdjMode
     */	
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay == null ? null : loanRateAdjDay.trim();
	}
	
    /**
     * @return LoanRateAdjDay
     */	
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}
	
	/**
	 * @param isSegInterest
	 */
	public void setIsSegInterest(String isSegInterest) {
		this.isSegInterest = isSegInterest == null ? null : isSegInterest.trim();
	}
	
    /**
     * @return IsSegInterest
     */	
	public String getIsSegInterest() {
		return this.isSegInterest;
	}
	
	/**
	 * @param isAutoAppr
	 */
	public void setIsAutoAppr(String isAutoAppr) {
		this.isAutoAppr = isAutoAppr == null ? null : isAutoAppr.trim();
	}
	
    /**
     * @return IsAutoAppr
     */	
	public String getIsAutoAppr() {
		return this.isAutoAppr;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval == null ? null : lprRateIntval.trim();
	}
	
    /**
     * @return LprRateIntval
     */	
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return CurtLprRate
     */	
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType == null ? null : irFloatType.trim();
	}
	
    /**
     * @return IrFloatType
     */	
	public String getIrFloatType() {
		return this.irFloatType;
	}
	
	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}
	
    /**
     * @return IrFloatRate
     */	
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return RateFloatPoint
     */	
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return ExecRateYear
     */	
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}
	
    /**
     * @return OverdueRatePefloat
     */	
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}
	
	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}
	
    /**
     * @return OverdueExecRate
     */	
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}
	
	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}
	
    /**
     * @return CiRatePefloat
     */	
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}
	
	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}
	
    /**
     * @return CiExecRate
     */	
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}
	
	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}
	
    /**
     * @return DefaultRate
     */	
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}
	
	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}
	
    /**
     * @return DefaultRateY
     */	
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType == null ? null : rateAdjType.trim();
	}
	
    /**
     * @return RateAdjType
     */	
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval == null ? null : nextRateAdjInterval.trim();
	}
	
    /**
     * @return NextRateAdjInterval
     */	
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit == null ? null : nextRateAdjUnit.trim();
	}
	
    /**
     * @return NextRateAdjUnit
     */	
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate == null ? null : firstAdjDate.trim();
	}
	
    /**
     * @return FirstAdjDate
     */	
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle == null ? null : eiIntervalCycle.trim();
	}
	
    /**
     * @return EiIntervalCycle
     */	
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}
	
	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit == null ? null : eiIntervalUnit.trim();
	}
	
    /**
     * @return EiIntervalUnit
     */	
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}
	
	/**
	 * @param deductType
	 */
	public void setDeductType(String deductType) {
		this.deductType = deductType == null ? null : deductType.trim();
	}
	
    /**
     * @return DeductType
     */	
	public String getDeductType() {
		return this.deductType;
	}
	
	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay == null ? null : deductDay.trim();
	}
	
    /**
     * @return DeductDay
     */	
	public String getDeductDay() {
		return this.deductDay;
	}
	
	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno == null ? null : loanPayoutAccno.trim();
	}
	
    /**
     * @return LoanPayoutAccno
     */	
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}
	
	/**
	 * @param loanPayoutSubNo
	 */
	public void setLoanPayoutSubNo(String loanPayoutSubNo) {
		this.loanPayoutSubNo = loanPayoutSubNo == null ? null : loanPayoutSubNo.trim();
	}
	
    /**
     * @return LoanPayoutSubNo
     */	
	public String getLoanPayoutSubNo() {
		return this.loanPayoutSubNo;
	}
	
	/**
	 * @param payoutAcctName
	 */
	public void setPayoutAcctName(String payoutAcctName) {
		this.payoutAcctName = payoutAcctName == null ? null : payoutAcctName.trim();
	}
	
    /**
     * @return PayoutAcctName
     */	
	public String getPayoutAcctName() {
		return this.payoutAcctName;
	}
	
	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay == null ? null : isBeEntrustedPay.trim();
	}
	
    /**
     * @return IsBeEntrustedPay
     */	
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}
	
	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno == null ? null : repayAccno.trim();
	}
	
    /**
     * @return RepayAccno
     */	
	public String getRepayAccno() {
		return this.repayAccno;
	}
	
	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno == null ? null : repaySubAccno.trim();
	}
	
    /**
     * @return RepaySubAccno
     */	
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName == null ? null : repayAcctName.trim();
	}
	
    /**
     * @return RepayAcctName
     */	
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param loanPromiseFlag
	 */
	public void setLoanPromiseFlag(String loanPromiseFlag) {
		this.loanPromiseFlag = loanPromiseFlag == null ? null : loanPromiseFlag.trim();
	}
	
    /**
     * @return LoanPromiseFlag
     */	
	public String getLoanPromiseFlag() {
		return this.loanPromiseFlag;
	}
	
	/**
	 * @param loanPromiseType
	 */
	public void setLoanPromiseType(String loanPromiseType) {
		this.loanPromiseType = loanPromiseType == null ? null : loanPromiseType.trim();
	}
	
    /**
     * @return LoanPromiseType
     */	
	public String getLoanPromiseType() {
		return this.loanPromiseType;
	}
	
	/**
	 * @param sbsyDepAccno
	 */
	public void setSbsyDepAccno(String sbsyDepAccno) {
		this.sbsyDepAccno = sbsyDepAccno == null ? null : sbsyDepAccno.trim();
	}
	
    /**
     * @return SbsyDepAccno
     */	
	public String getSbsyDepAccno() {
		return this.sbsyDepAccno;
	}
	
	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}
	
    /**
     * @return SbsyPerc
     */	
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}
	
	/**
	 * @param sbysEnddate
	 */
	public void setSbysEnddate(String sbysEnddate) {
		this.sbysEnddate = sbysEnddate == null ? null : sbysEnddate.trim();
	}
	
    /**
     * @return SbysEnddate
     */	
	public String getSbysEnddate() {
		return this.sbysEnddate;
	}
	
	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail == null ? null : loanTypeDetail.trim();
	}
	
    /**
     * @return LoanTypeDetail
     */	
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}
	
	/**
	 * @param isPactLoan
	 */
	public void setIsPactLoan(String isPactLoan) {
		this.isPactLoan = isPactLoan == null ? null : isPactLoan.trim();
	}
	
    /**
     * @return IsPactLoan
     */	
	public String getIsPactLoan() {
		return this.isPactLoan;
	}
	
	/**
	 * @param isGreenIndustry
	 */
	public void setIsGreenIndustry(String isGreenIndustry) {
		this.isGreenIndustry = isGreenIndustry == null ? null : isGreenIndustry.trim();
	}
	
    /**
     * @return IsGreenIndustry
     */	
	public String getIsGreenIndustry() {
		return this.isGreenIndustry;
	}
	
	/**
	 * @param isSteelLoan
	 */
	public void setIsSteelLoan(String isSteelLoan) {
		this.isSteelLoan = isSteelLoan == null ? null : isSteelLoan.trim();
	}
	
    /**
     * @return IsSteelLoan
     */	
	public String getIsSteelLoan() {
		return this.isSteelLoan;
	}
	
	/**
	 * @param isStainlessLoan
	 */
	public void setIsStainlessLoan(String isStainlessLoan) {
		this.isStainlessLoan = isStainlessLoan == null ? null : isStainlessLoan.trim();
	}
	
    /**
     * @return IsStainlessLoan
     */	
	public String getIsStainlessLoan() {
		return this.isStainlessLoan;
	}
	
	/**
	 * @param isPovertyReliefLoan
	 */
	public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
		this.isPovertyReliefLoan = isPovertyReliefLoan == null ? null : isPovertyReliefLoan.trim();
	}
	
    /**
     * @return IsPovertyReliefLoan
     */	
	public String getIsPovertyReliefLoan() {
		return this.isPovertyReliefLoan;
	}
	
	/**
	 * @param isLaborIntenSbsyLoan
	 */
	public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
		this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan == null ? null : isLaborIntenSbsyLoan.trim();
	}
	
    /**
     * @return IsLaborIntenSbsyLoan
     */	
	public String getIsLaborIntenSbsyLoan() {
		return this.isLaborIntenSbsyLoan;
	}
	
	/**
	 * @param goverSubszHouseLoan
	 */
	public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
		this.goverSubszHouseLoan = goverSubszHouseLoan == null ? null : goverSubszHouseLoan.trim();
	}
	
    /**
     * @return GoverSubszHouseLoan
     */	
	public String getGoverSubszHouseLoan() {
		return this.goverSubszHouseLoan;
	}
	
	/**
	 * @param engyEnviProteLoan
	 */
	public void setEngyEnviProteLoan(String engyEnviProteLoan) {
		this.engyEnviProteLoan = engyEnviProteLoan == null ? null : engyEnviProteLoan.trim();
	}
	
    /**
     * @return EngyEnviProteLoan
     */	
	public String getEngyEnviProteLoan() {
		return this.engyEnviProteLoan;
	}
	
	/**
	 * @param isCphsRurDelpLoan
	 */
	public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
		this.isCphsRurDelpLoan = isCphsRurDelpLoan == null ? null : isCphsRurDelpLoan.trim();
	}
	
    /**
     * @return IsCphsRurDelpLoan
     */	
	public String getIsCphsRurDelpLoan() {
		return this.isCphsRurDelpLoan;
	}
	
	/**
	 * @param realproLoan
	 */
	public void setRealproLoan(String realproLoan) {
		this.realproLoan = realproLoan == null ? null : realproLoan.trim();
	}
	
    /**
     * @return RealproLoan
     */	
	public String getRealproLoan() {
		return this.realproLoan;
	}
	
	/**
	 * @param realproLoanRate
	 */
	public void setRealproLoanRate(java.math.BigDecimal realproLoanRate) {
		this.realproLoanRate = realproLoanRate;
	}
	
    /**
     * @return RealproLoanRate
     */	
	public java.math.BigDecimal getRealproLoanRate() {
		return this.realproLoanRate;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName == null ? null : finaBrIdName.trim();
	}
	
    /**
     * @return FinaBrIdName
     */	
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo == null ? null : disbOrgNo.trim();
	}
	
    /**
     * @return DisbOrgNo
     */	
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}
	
	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName == null ? null : disbOrgName.trim();
	}
	
    /**
     * @return DisbOrgName
     */	
	public String getDisbOrgName() {
		return this.disbOrgName;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param agriType
	 */
	public void setAgriType(String agriType) {
		this.agriType = agriType == null ? null : agriType.trim();
	}
	
    /**
     * @return AgriType
     */	
	public String getAgriType() {
		return this.agriType;
	}
	
	/**
	 * @param agriLoanTer
	 */
	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer == null ? null : agriLoanTer.trim();
	}
	
    /**
     * @return AgriLoanTer
     */	
	public String getAgriLoanTer() {
		return this.agriLoanTer;
	}
	
	/**
	 * @param isMaterComp
	 */
	public void setIsMaterComp(String isMaterComp) {
		this.isMaterComp = isMaterComp == null ? null : isMaterComp.trim();
	}
	
    /**
     * @return IsMaterComp
     */	
	public String getIsMaterComp() {
		return this.isMaterComp;
	}
	
	/**
	 * @param guarDetailMode
	 */
	public void setGuarDetailMode(String guarDetailMode) {
		this.guarDetailMode = guarDetailMode == null ? null : guarDetailMode.trim();
	}
	
    /**
     * @return GuarDetailMode
     */	
	public String getGuarDetailMode() {
		return this.guarDetailMode;
	}
	
	/**
	 * @param isOperPropertyLoan
	 */
	public void setIsOperPropertyLoan(String isOperPropertyLoan) {
		this.isOperPropertyLoan = isOperPropertyLoan == null ? null : isOperPropertyLoan.trim();
	}
	
    /**
     * @return IsOperPropertyLoan
     */	
	public String getIsOperPropertyLoan() {
		return this.isOperPropertyLoan;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer == null ? null : loanTer.trim();
	}
	
    /**
     * @return LoanTer
     */	
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param loanSubjectNo
	 */
	public void setLoanSubjectNo(String loanSubjectNo) {
		this.loanSubjectNo = loanSubjectNo == null ? null : loanSubjectNo.trim();
	}
	
    /**
     * @return LoanSubjectNo
     */	
	public String getLoanSubjectNo() {
		return this.loanSubjectNo;
	}
	
	/**
	 * @param loanUseType
	 */
	public void setLoanUseType(String loanUseType) {
		this.loanUseType = loanUseType == null ? null : loanUseType.trim();
	}
	
    /**
     * @return LoanUseType
     */	
	public String getLoanUseType() {
		return this.loanUseType;
	}
	
	/**
	 * @param loanUseTypeDesc
	 */
	public void setLoanUseTypeDesc(String loanUseTypeDesc) {
		this.loanUseTypeDesc = loanUseTypeDesc == null ? null : loanUseTypeDesc.trim();
	}
	
    /**
     * @return LoanUseTypeDesc
     */	
	public String getLoanUseTypeDesc() {
		return this.loanUseTypeDesc;
	}
	
	/**
	 * @param estateType
	 */
	public void setEstateType(String estateType) {
		this.estateType = estateType == null ? null : estateType.trim();
	}
	
    /**
     * @return EstateType
     */	
	public String getEstateType() {
		return this.estateType;
	}
	
	/**
	 * @param indtUpFlag
	 */
	public void setIndtUpFlag(String indtUpFlag) {
		this.indtUpFlag = indtUpFlag == null ? null : indtUpFlag.trim();
	}
	
    /**
     * @return IndtUpFlag
     */	
	public String getIndtUpFlag() {
		return this.indtUpFlag;
	}
	
	/**
	 * @param strategyNewLoan
	 */
	public void setStrategyNewLoan(String strategyNewLoan) {
		this.strategyNewLoan = strategyNewLoan == null ? null : strategyNewLoan.trim();
	}
	
    /**
     * @return StrategyNewLoan
     */	
	public String getStrategyNewLoan() {
		return this.strategyNewLoan;
	}
	
	/**
	 * @param culIndustryFlag
	 */
	public void setCulIndustryFlag(String culIndustryFlag) {
		this.culIndustryFlag = culIndustryFlag == null ? null : culIndustryFlag.trim();
	}
	
    /**
     * @return CulIndustryFlag
     */	
	public String getCulIndustryFlag() {
		return this.culIndustryFlag;
	}
	
	/**
	 * @param capAutobackFlag
	 */
	public void setCapAutobackFlag(String capAutobackFlag) {
		this.capAutobackFlag = capAutobackFlag == null ? null : capAutobackFlag.trim();
	}
	
    /**
     * @return CapAutobackFlag
     */	
	public String getCapAutobackFlag() {
		return this.capAutobackFlag;
	}
	
	/**
	 * @param isSbsy
	 */
	public void setIsSbsy(String isSbsy) {
		this.isSbsy = isSbsy == null ? null : isSbsy.trim();
	}
	
    /**
     * @return IsSbsy
     */	
	public String getIsSbsy() {
		return this.isSbsy;
	}
	
	/**
	 * @param isHolidayDelay
	 */
	public void setIsHolidayDelay(String isHolidayDelay) {
		this.isHolidayDelay = isHolidayDelay == null ? null : isHolidayDelay.trim();
	}
	
    /**
     * @return IsHolidayDelay
     */	
	public String getIsHolidayDelay() {
		return this.isHolidayDelay;
	}
	
	/**
	 * @param isMeterCi
	 */
	public void setIsMeterCi(String isMeterCi) {
		this.isMeterCi = isMeterCi == null ? null : isMeterCi.trim();
	}
	
    /**
     * @return IsMeterCi
     */	
	public String getIsMeterCi() {
		return this.isMeterCi;
	}
	
	/**
	 * @param intAutobackFlag
	 */
	public void setIntAutobackFlag(String intAutobackFlag) {
		this.intAutobackFlag = intAutobackFlag == null ? null : intAutobackFlag.trim();
	}
	
    /**
     * @return IntAutobackFlag
     */	
	public String getIntAutobackFlag() {
		return this.intAutobackFlag;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return ContAmt
     */	
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param cashAmt
	 */
	public void setCashAmt(java.math.BigDecimal cashAmt) {
		this.cashAmt = cashAmt;
	}
	
    /**
     * @return CashAmt
     */	
	public java.math.BigDecimal getCashAmt() {
		return this.cashAmt;
	}
	
	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode == null ? null : payMode.trim();
	}
	
    /**
     * @return PayMode
     */	
	public String getPayMode() {
		return this.payMode;
	}
	
	/**
	 * @param isSxef
	 */
	public void setIsSxef(String isSxef) {
		this.isSxef = isSxef == null ? null : isSxef.trim();
	}
	
    /**
     * @return IsSxef
     */	
	public String getIsSxef() {
		return this.isSxef;
	}
	
	/**
	 * @param isCfirmPay
	 */
	public void setIsCfirmPay(String isCfirmPay) {
		this.isCfirmPay = isCfirmPay == null ? null : isCfirmPay.trim();
	}
	
    /**
     * @return IsCfirmPay
     */	
	public String getIsCfirmPay() {
		return this.isCfirmPay;
	}
	
	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType == null ? null : termType.trim();
	}
	
    /**
     * @return TermType
     */	
	public String getTermType() {
		return this.termType;
	}
	
	/**
	 * @param appTerm
	 */
	public void setAppTerm(String appTerm) {
		this.appTerm = appTerm == null ? null : appTerm.trim();
	}
	
    /**
     * @return AppTerm
     */	
	public String getAppTerm() {
		return this.appTerm;
	}
	
	/**
	 * @param customerRatingFactor
	 */
	public void setCustomerRatingFactor(java.math.BigDecimal customerRatingFactor) {
		this.customerRatingFactor = customerRatingFactor;
	}
	
    /**
     * @return CustomerRatingFactor
     */	
	public java.math.BigDecimal getCustomerRatingFactor() {
		return this.customerRatingFactor;
	}
	
	/**
	 * @param otherLoanPurp
	 */
	public void setOtherLoanPurp(String otherLoanPurp) {
		this.otherLoanPurp = otherLoanPurp == null ? null : otherLoanPurp.trim();
	}
	
    /**
     * @return OtherLoanPurp
     */	
	public String getOtherLoanPurp() {
		return this.otherLoanPurp;
	}
	
	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus == null ? null : authStatus.trim();
	}
	
    /**
     * @return AuthStatus
     */	
	public String getAuthStatus() {
		return this.authStatus;
	}
	
	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour == null ? null : chnlSour.trim();
	}
	
    /**
     * @return ChnlSour
     */	
	public String getChnlSour() {
		return this.chnlSour;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd == null ? null : beforehandInd.trim();
	}
	
    /**
     * @return BeforehandInd
     */	
	public String getBeforehandInd() {
		return this.beforehandInd;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}
	
    /**
     * @return BelgLine
     */	
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param isLocalManag
	 */
	public void setIsLocalManag(String isLocalManag) {
		this.isLocalManag = isLocalManag == null ? null : isLocalManag.trim();
	}
	
    /**
     * @return IsLocalManag
     */	
	public String getIsLocalManag() {
		return this.isLocalManag;
	}
	
	/**
	 * @param isPool
	 */
	public void setIsPool(String isPool) {
		this.isPool = isPool == null ? null : isPool.trim();
	}
	
    /**
     * @return IsPool
     */	
	public String getIsPool() {
		return this.isPool;
	}


}