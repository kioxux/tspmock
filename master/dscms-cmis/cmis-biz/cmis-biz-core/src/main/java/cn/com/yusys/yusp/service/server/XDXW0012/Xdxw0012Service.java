package cn.com.yusys.yusp.service.server.XDXW0012;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdxw0012.req.Xdxw0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0012.resp.Xdxw0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.web.server.xdxw0012.BizXdxw0012Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

/**
 * 小贷授信申请文本生成pdf
 *
 * @author wangqing
 * @version 1.0
 */
@Service
public class Xdxw0012Service {

    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0012Resource.class);

    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 交易描述：优企贷共借人签订
     *
     * @param xdxw0012DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdxw0012DataRespDto updateSignStatByCommonCertNo(Xdxw0012DataReqDto xdxw0012DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataReqDto));
        //返回对象
        Xdxw0012DataRespDto xdxw0012DataRespDto = new Xdxw0012DataRespDto();
        int result = 0;//执行结果
        try {
            String cusName = xdxw0012DataReqDto.getCusName();//客户名称
            String certNo = xdxw0012DataReqDto.getCertNo();//身份证号码
            String certType = xdxw0012DataReqDto.getCertType();//身份证件类型
            String applyDate = xdxw0012DataReqDto.getApplyDate();//申请日期
            String lmtLmtType = xdxw0012DataReqDto.getLmtLmtType();//授信文件类型
            String cus_type = xdxw0012DataReqDto.getCus_type();////查询类型 01企业；02个人
            String cusname_type = xdxw0012DataReqDto.getCusname_type();//授权人类型:01主借款人，02共同借款人，03其他关系人，04担保人，05法人代表、出资人及关联人等，06关系人
            String prdType = xdxw0012DataReqDto.getPrdType();//产品类型 ：优享贷 YXD   优抵贷YDD  优企贷YQD  优农贷 YND
            String company = xdxw0012DataReqDto.getCompany();

            String certCodeNo = certNo.substring(certNo.length() - 4);

            //1、第一步：确认要生成prd的模板名称
            String cptModelName = "";//帆软模板名称
            String pdfFileName = ""; //生成的pdf文件名称

            // 暂定docx版本，等cpt版本画好后统一调整
            if ("01".equals(lmtLmtType)) {
                cptModelName = "xdgrxyxxcxsysqs.cpt";// 信贷个人信息查询书
                pdfFileName = "xdgrxyxxcxsysqs_" + certCodeNo + "";
            } else if ("02".equals(lmtLmtType)) {
                if ("01".equals(cus_type)) {//01-企业
                    cptModelName = "xdqyzxcxsq.cpt";// 信贷企业征信查询书
                    pdfFileName = "xdqyzxcxsq_" + certCodeNo + "";
                } else {
                    cptModelName = "xdgrzxcxsq.cpt";// 信贷个人征信查询书
                    pdfFileName = "xdgrzxcxsq_" + certCodeNo;
                }
            } else if ("03".equals(lmtLmtType)) {
                cptModelName = "xdytsms.cpt";// 信贷用途申明书
                pdfFileName = "xdytsms_" + certCodeNo + "";
            }

            // 待调试
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);
            //查询存储地址信息
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String filePath = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //调用帆软的生成pdf的方法
            //2、传入帆软报表生成需要的参数
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("cert_no", certNo);//证件号
            parameterMap.put("cusName", cusName);//客户名
            parameterMap.put("prdType", prdType);//产品类型
            // String typeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_CERT_TYP", certType);
            String typeName = changeCertType(certType);
            parameterMap.put("certType", typeName);//证件类型
            parameterMap.put("applyDate", applyDate);//申请日期
            parameterMap.put("cusnameType", cusname_type);//授权人类型
            parameterMap.put("company", company);//企业客户
            //传入公共参数
            parameterMap.put("TempleteName", cptModelName);//模板名称（附带路径）
            parameterMap.put("saveFileName", pdfFileName);//待生成的PDF文件名称
            parameterMap.put("path", path);

            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(cptModelName);//模板名称
                frptPdfArgsDto.setNewFileName(pdfFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(certNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
            //返回值pdfDepoAddr=PdfFilename#ip#port#username#password#path
            xdxw0012DataRespDto.setPdfDepoAddr(path);
            xdxw0012DataRespDto.setPdfFileName(pdfFileName + ".pdf");
            xdxw0012DataRespDto.setFtpAddr(ip);
            xdxw0012DataRespDto.setPort(port);
            xdxw0012DataRespDto.setUserName(username);
            xdxw0012DataRespDto.setPassword(password);
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value);
        return xdxw0012DataRespDto;
    }

    public String changeCertType(String certType) {
        String certTypeName = "";
        if ("10".equals(certType)) {
            certTypeName = "身份证";
        } else if ("11".equals(certType)) {
            certTypeName = "户口簿";
        } else if ("12".equals(certType)) {
            certTypeName = "护照";
        } else if ("13".equals(certType)) {
            certTypeName = "军官证";
        } else if ("14".equals(certType)) {
            certTypeName = "士兵证";
        } else if ("15".equals(certType)) {
            certTypeName = "港澳居民来往内地通行证";
        } else if ("16".equals(certType)) {
            certTypeName = "台湾居民来往内地通行证";
        } else if ("17".equals(certType)) {
            certTypeName = "临时身份证";
        } else if ("18".equals(certType)) {
            certTypeName = "外国人居留证";
        } else if ("19".equals(certType)) {
            certTypeName = "警官证";
        } else if ("1X".equals(certType)) {
            certTypeName = "其他证件";
        } else if ("20".equals(certType)) {
            certTypeName = "组织机构代码";
        } else if ("21".equals(certType)) {
            certTypeName = "港澳台身份证";
        } else if ("22".equals(certType)) {
            certTypeName = "境外企业代码";
        } else if ("23".equals(certType)) {
            certTypeName = "回乡证";
        } else if ("24".equals(certType)) {
            certTypeName = "营业执照";
        }
        return certTypeName;
    }

}
