/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.service.IqpHighAmtAgrAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHighAmtAgrAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-12 13:58:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "最高额授信协议申请")
@RequestMapping("/api/iqphighamtagrapp")
public class IqpHighAmtAgrAppResource {
    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpHighAmtAgrApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpHighAmtAgrApp> list = iqpHighAmtAgrAppService.selectAll(queryModel);
        return new ResultDto<List<IqpHighAmtAgrApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpHighAmtAgrApp>> index(QueryModel queryModel) {
        List<IqpHighAmtAgrApp> list = iqpHighAmtAgrAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpHighAmtAgrApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpHighAmtAgrApp> show(@PathVariable("pkId") String pkId) {
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpHighAmtAgrApp>(iqpHighAmtAgrApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpHighAmtAgrApp> create(@RequestBody IqpHighAmtAgrApp iqpHighAmtAgrApp) throws URISyntaxException {
        iqpHighAmtAgrAppService.insert(iqpHighAmtAgrApp);
        return new ResultDto<IqpHighAmtAgrApp>(iqpHighAmtAgrApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpHighAmtAgrApp iqpHighAmtAgrApp) throws URISyntaxException {
        int result = iqpHighAmtAgrAppService.update(iqpHighAmtAgrApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpHighAmtAgrAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpHighAmtAgrAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 最高额授信协议申请保存操作
     * @param iqpHighAmtAgrApp
     * @return
     */
    @ApiOperation("新增保存")
    @PostMapping("/savehighamtagrappinfo")
    public ResultDto<Map> saveIqpHighAmtAgrAppInfo(@RequestBody IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        Map result = iqpHighAmtAgrAppService.saveIqpHighAmtAgrAppInfo(iqpHighAmtAgrApp);
        return new ResultDto<>(result);
    }

    /**
     * 最高额授信协议申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("修改保存")
    @PostMapping("/commonsaveiqphighamtagrappinfo")
    public ResultDto<Map> commonSaveIqpHighAmtAgrAppInfo(@RequestBody Map params){
        Map rtnData = iqpHighAmtAgrAppService.commonSaveIqpHighAmtAgrAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpHighAmtAgrApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpHighAmtAgrApp> list = iqpHighAmtAgrAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpHighAmtAgrApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpHighAmtAgrApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpHighAmtAgrApp> list = iqpHighAmtAgrAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpHighAmtAgrApp>>(list);
    }

    /**
     * @函数名称:logicDelete
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicdelete")
    public ResultDto<Map> logicDelete(@RequestBody IqpHighAmtAgrApp iqpHighAmtAgrApp) {
        Map rtnData = iqpHighAmtAgrAppService.logicDelete(iqpHighAmtAgrApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:updateStatus
     * @函数描述:修改审批状态
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatestatus")
    public ResultDto<Integer> updateStatus(@RequestBody IqpHighAmtAgrApp params) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByPrimaryKey(params.getPkId());
        if (iqpHighAmtAgrApp != null) {
            int delete = iqpHighAmtAgrAppService.updateStatus(iqpHighAmtAgrApp);
            if (delete > 0) {
                resultDto.setCode(200);
                resultDto.setData(1);
                resultDto.setMessage("删除成功！");
            } else {
                resultDto.setCode(300);
                resultDto.setData(0);
                resultDto.setMessage("删除失败！");
            }
        } else {
            resultDto.setCode(400);
            resultDto.setData(0);
            resultDto.setMessage("数据不存在！");
        }
        return resultDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("最高额授信协议申请详情展示")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.queryIqpHighAmtAgrAppDataBySerno((String)map.get("serno"));
        if (iqpHighAmtAgrApp != null) {
            resultDto.setCode(0);
            resultDto.setData(iqpHighAmtAgrApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @方法名称：sendctrcont
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendctrcont")
    protected  ResultDto<Object> sendCtrcont(@RequestBody Map map) throws Exception {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        iqpHighAmtAgrAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * @函数名称:iqpHighAmtSubmitNoFlow
     * @函数描述:无流程提交后业务处理
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("无流程提交后业务处理")
    @PostMapping("/iqpHighAmtSubmitNoFlow")
    protected ResultDto<Map> iqpHighAmtSubmitNoFlow(@RequestBody String serno) throws URISyntaxException {
        Map rtnData = iqpHighAmtAgrAppService.iqpHighAmtSubmitNoFlow(serno);
        return new ResultDto<>(rtnData);
    }
}
