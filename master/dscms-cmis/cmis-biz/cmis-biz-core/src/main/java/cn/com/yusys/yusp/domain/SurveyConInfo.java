/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyConInfo
 * @类描述: survey_con_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-12 19:53:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "survey_con_info")
public class SurveyConInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_NO")
	private String surveyNo;
	
	/** 建议金额 **/
	@Column(name = "ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	@Column(name = "ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceRate;
	
	/** 参考利率 **/
	@Column(name = "REF_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal refRate;
	
	/** 建议期限 **/
	@Column(name = "ADVICE_TERM", unique = false, nullable = true, length = 20)
	private String adviceTerm;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 20)
	private String loanTerm;
	
	/** 担保方式 **/
	@Column(name = "GRT_MODE", unique = false, nullable = true, length = 5)
	private String grtMode;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 是否原抵押物 **/
	@Column(name = "IS_OLD_COLL", unique = false, nullable = true, length = 1)
	private String isOldColl;
	
	/** 周转额度 **/
	@Column(name = "TURNOV_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal turnovLmt;
	
	/** 新增额度 **/
	@Column(name = "NEW_ADD_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal newAddLmt;
	
	/** 是否存在协办客户经理 **/
	@Column(name = "IS_ASS_MANAGER_ID", unique = false, nullable = true, length = 1)
	private String isAssManagerId;
	
	/** 协办客户经理工号 **/
	@Column(name = "ASS_MANAGER_ID_JOB_NO", unique = false, nullable = true, length = 20)
	private String assManagerIdJobNo;
	
	/** 客户是否实际经营人 **/
	@Column(name = "CUS_IS_REAL_OPER_PER", unique = false, nullable = true, length = 1)
	private String cusIsRealOperPer;
	
	/** 客户经营是否正常 **/
	@Column(name = "CUS_OPER_IS_NORMAL", unique = false, nullable = true, length = 1)
	private String cusOperIsNormal;
	
	/** 拒绝类型 **/
	@Column(name = "RFU_TYPE", unique = false, nullable = true, length = 50)
	private String rfuType;
	
	/** 办理建议 **/
	@Column(name = "PRC_ADVICE", unique = false, nullable = true, length = 5)
	private String prcAdvice;
	
	/** 情况说明 **/
	@Column(name = "CASE_MEMO", unique = false, nullable = true, length = 500)
	private String caseMemo;
	
	/** 模型金额 **/
	@Column(name = "MODEL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAmt;
	
	/** 模型利率 **/
	@Column(name = "MODEL_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelRate;
	
	/** 模型期限 **/
	@Column(name = "MODEL_TERM", unique = false, nullable = true, length = 20)
	private String modelTerm;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 审批状态 **/
	@Column(name = "approve_status", unique = false, nullable = true, length = 200)
	private String approveStatus;
	
	/** 原借据金额 **/
	@Column(name = "bill_amt", unique = false, nullable = true, length = 200)
	private String billAmt;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	
    /**
     * @return surveyNo
     */
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return adviceAmt
     */
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return adviceRate
     */
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return refRate
     */
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm;
	}
	
    /**
     * @return adviceTerm
     */
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public String getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param grtMode
	 */
	public void setGrtMode(String grtMode) {
		this.grtMode = grtMode;
	}
	
    /**
     * @return grtMode
     */
	public String getGrtMode() {
		return this.grtMode;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param isOldColl
	 */
	public void setIsOldColl(String isOldColl) {
		this.isOldColl = isOldColl;
	}
	
    /**
     * @return isOldColl
     */
	public String getIsOldColl() {
		return this.isOldColl;
	}
	
	/**
	 * @param turnovLmt
	 */
	public void setTurnovLmt(java.math.BigDecimal turnovLmt) {
		this.turnovLmt = turnovLmt;
	}
	
    /**
     * @return turnovLmt
     */
	public java.math.BigDecimal getTurnovLmt() {
		return this.turnovLmt;
	}
	
	/**
	 * @param newAddLmt
	 */
	public void setNewAddLmt(java.math.BigDecimal newAddLmt) {
		this.newAddLmt = newAddLmt;
	}
	
    /**
     * @return newAddLmt
     */
	public java.math.BigDecimal getNewAddLmt() {
		return this.newAddLmt;
	}
	
	/**
	 * @param isAssManagerId
	 */
	public void setIsAssManagerId(String isAssManagerId) {
		this.isAssManagerId = isAssManagerId;
	}
	
    /**
     * @return isAssManagerId
     */
	public String getIsAssManagerId() {
		return this.isAssManagerId;
	}
	
	/**
	 * @param assManagerIdJobNo
	 */
	public void setAssManagerIdJobNo(String assManagerIdJobNo) {
		this.assManagerIdJobNo = assManagerIdJobNo;
	}
	
    /**
     * @return assManagerIdJobNo
     */
	public String getAssManagerIdJobNo() {
		return this.assManagerIdJobNo;
	}
	
	/**
	 * @param cusIsRealOperPer
	 */
	public void setCusIsRealOperPer(String cusIsRealOperPer) {
		this.cusIsRealOperPer = cusIsRealOperPer;
	}
	
    /**
     * @return cusIsRealOperPer
     */
	public String getCusIsRealOperPer() {
		return this.cusIsRealOperPer;
	}
	
	/**
	 * @param cusOperIsNormal
	 */
	public void setCusOperIsNormal(String cusOperIsNormal) {
		this.cusOperIsNormal = cusOperIsNormal;
	}
	
    /**
     * @return cusOperIsNormal
     */
	public String getCusOperIsNormal() {
		return this.cusOperIsNormal;
	}
	
	/**
	 * @param rfuType
	 */
	public void setRfuType(String rfuType) {
		this.rfuType = rfuType;
	}
	
    /**
     * @return rfuType
     */
	public String getRfuType() {
		return this.rfuType;
	}
	
	/**
	 * @param prcAdvice
	 */
	public void setPrcAdvice(String prcAdvice) {
		this.prcAdvice = prcAdvice;
	}
	
    /**
     * @return prcAdvice
     */
	public String getPrcAdvice() {
		return this.prcAdvice;
	}
	
	/**
	 * @param caseMemo
	 */
	public void setCaseMemo(String caseMemo) {
		this.caseMemo = caseMemo;
	}
	
    /**
     * @return caseMemo
     */
	public String getCaseMemo() {
		return this.caseMemo;
	}
	
	/**
	 * @param modelAmt
	 */
	public void setModelAmt(java.math.BigDecimal modelAmt) {
		this.modelAmt = modelAmt;
	}
	
    /**
     * @return modelAmt
     */
	public java.math.BigDecimal getModelAmt() {
		return this.modelAmt;
	}
	
	/**
	 * @param modelRate
	 */
	public void setModelRate(java.math.BigDecimal modelRate) {
		this.modelRate = modelRate;
	}
	
    /**
     * @return modelRate
     */
	public java.math.BigDecimal getModelRate() {
		return this.modelRate;
	}
	
	/**
	 * @param modelTerm
	 */
	public void setModelTerm(String modelTerm) {
		this.modelTerm = modelTerm;
	}
	
    /**
     * @return modelTerm
     */
	public String getModelTerm() {
		return this.modelTerm;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(String billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return billAmt
     */
	public String getBillAmt() {
		return this.billAmt;
	}


}