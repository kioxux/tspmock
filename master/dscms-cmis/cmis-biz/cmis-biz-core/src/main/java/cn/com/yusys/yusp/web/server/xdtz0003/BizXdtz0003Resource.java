package cn.com.yusys.yusp.web.server.xdtz0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.Xdtz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.resp.Xdtz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0003.Xdtz0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:查询小微借据余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0003:查询小微借据余额")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0003Resource.class);

    @Resource
    private Xdtz0003Service xdtz0003Service;

    /**
     * 交易码：xdtz0003
     * 交易描述：查询小微借据余额
     *
     * @param xdtz0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询小微借据余额")
    @PostMapping("/xdtz0003")
    protected @ResponseBody
    ResultDto<Xdtz0003DataRespDto> xdtz0003(@Validated @RequestBody Xdtz0003DataReqDto xdtz0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataReqDto));
        Xdtz0003DataRespDto xdtz0003DataRespDto = new Xdtz0003DataRespDto();// 响应Dto:查询小微借据余额
        ResultDto<Xdtz0003DataRespDto> xdtz0003DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataReqDto));
            xdtz0003DataRespDto = xdtz0003Service.getXdtz0003(xdtz0003DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataReqDto));
            // 封装xdtz0003DataResultDto中正确的返回码和返回信息
            xdtz0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdtz0003DataResultDto中异常返回码和返回信息
            xdtz0003DataResultDto.setCode(e.getErrorCode());
            xdtz0003DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, e.getMessage());
            // 封装xdtz0003DataResultDto中异常返回码和返回信息
            xdtz0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0003DataRespDto到xdtz0003DataResultDto中
        xdtz0003DataResultDto.setData(xdtz0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataRespDto));
        return xdtz0003DataResultDto;
    }
}
