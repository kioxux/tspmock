package cn.com.yusys.yusp.web.server.xdca0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdca0005.Xdca0005Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:大额分期合同签订接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0005:大额分期合同签订接口")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0005Resource.class);

    @Autowired
    private Xdca0005Service xdca0005Service;

    /**
     * 交易码：xdca0005
     * 交易描述：大额分期合同签订接口
     *
     * @param xdca0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同签订接口")
    @PostMapping("/xdca0005")
    protected @ResponseBody
    ResultDto<Xdca0005DataRespDto> xdca0005(@Validated @RequestBody Xdca0005DataReqDto xdca0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataReqDto));
        Xdca0005DataRespDto xdca0005DataRespDto = new Xdca0005DataRespDto();// 响应Dto:大额分期合同签订接口
        ResultDto<Xdca0005DataRespDto> xdca0005DataResultDto = new ResultDto<>();
        try {
            //请求报文
            String contNo = xdca0005DataReqDto.getContNo();//合同编号
            if (StringUtil.isNotEmpty(contNo)) {
                // 从xdca0005DataReqDto获取业务值进行业务逻辑处理
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataReqDto));
                xdca0005DataRespDto = xdca0005Service.xdca0005(xdca0005DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataRespDto));
                // 封装xdca0005DataResultDto中正确的返回码和返回信息
                xdca0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdca0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                // 请求字段为空
                xdca0005DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdca0005DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, e.getMessage());
            // 封装xdca0005DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdca0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdca0005DataRespDto到xdca0005DataResultDto中
        xdca0005DataResultDto.setData(xdca0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataRespDto));
        return xdca0005DataResultDto;
    }
}
