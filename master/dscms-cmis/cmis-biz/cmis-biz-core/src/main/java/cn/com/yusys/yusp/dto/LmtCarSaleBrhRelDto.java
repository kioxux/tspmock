package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCarSaleBrhRel
 * @类描述: lmt_car_sale_brh_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 11:45:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtCarSaleBrhRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 项目编号 **/
	private String proNo;
	
	/** 专卖店地址 **/
	private String agencyAddr;
	
	/** 街道 **/
	private String street;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo == null ? null : proNo.trim();
	}
	
    /**
     * @return ProNo
     */	
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param agencyAddr
	 */
	public void setAgencyAddr(String agencyAddr) {
		this.agencyAddr = agencyAddr == null ? null : agencyAddr.trim();
	}
	
    /**
     * @return AgencyAddr
     */	
	public String getAgencyAddr() {
		return this.agencyAddr;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}


}