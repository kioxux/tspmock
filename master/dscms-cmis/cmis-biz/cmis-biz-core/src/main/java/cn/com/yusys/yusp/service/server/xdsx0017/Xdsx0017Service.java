package cn.com.yusys.yusp.service.server.xdsx0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.req.Xdsx0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.resp.Xdsx0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0021.CmisLmt0021Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdsx0017Service
 * @类描述: #服务类
 * @功能描述:通过证件类型、证件号码，查询此客户的授信协议信息，授信协议分项信息
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdsx0017Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0017Service.class);

    @Autowired
    private CommonService commonService;
    @Autowired
    private CmisLmt0021Service cmisLmt0021Service;
    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 查询对公客户授信信息
     *
     * @param xdsx0017DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0017DataRespDto xdsx0017(Xdsx0017DataReqDto xdsx0017DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value);
        //返回对象
        Xdsx0017DataRespDto xdsx0017DataRespDto = new Xdsx0017DataRespDto();
        //请求参数
        String certType = xdsx0017DataReqDto.getCer_type();
        String certCode = xdsx0017DataReqDto.getLocal_no();

        try {
            List<CusBaseDto> cusBaseDtoList = commonService.getCusBaseListByCertCode(certCode);
            List<cn.com.yusys.yusp.dto.server.xdsx0017.resp.List> lists = new ArrayList<>();

            logger.info("*********XDSX0017**调用客户管理服务接口****START*********:");
            List<CusBaseDto> cusBaseList = cusBaseDtoList.parallelStream()
                    .filter(cusBaseDto -> Objects.equals(certType, cusBaseDto.getCertType()))//判断和请求参数中的开户证件类型一致
                    .collect(Collectors.toList());// 转换成List
            //得到客户基本信息
            CusBaseDto cusBaseDto = null;
            if (cusBaseList.size() > 0) {//存在客户记录
                for (int i = 0; i < cusBaseList.size(); i++) {
                    cusBaseDto = cusBaseList.get(i);
                    String cusId = cusBaseDto.getCusId();//获取客户号
                    String cusName = cusBaseDto.getCusName();//获取客户名称

                    //调用额度管理服务接口
                    logger.info("*********XDSX0017**调用额度管理服务接口****START*********:");
                    CmisLmt0021ReqDto cmisLmt0021ReqDto = new CmisLmt0021ReqDto();
                    cmisLmt0021ReqDto.setCusId(cusId);
                    CmisLmt0021RespDto cmisLmt0021RespDto = cmisLmt0021Service.cmisLmt0021(cmisLmt0021ReqDto);
                    //获取额度信息
                    String lmtStatus = cmisLmt0021RespDto.getLmtStatus();
                    BigDecimal sumCrd = cmisLmt0021RespDto.getSumCrd();
                    String startDtae = cmisLmt0021RespDto.getStartDate();
                    String overDtae = cmisLmt0021RespDto.getOverDate();
                    String managerId = cmisLmt0021RespDto.getManagerId();

                    //客户经理信息查询
                    logger.info("*********XDSX0017**客户经理信息查询****START*********:");
                    AdminSmUserDto adminSmUserDto = managerIdMessageFind(managerId);
                    String managerIdName = adminSmUserDto.getUserName();

                    //组装返回信息
                    cn.com.yusys.yusp.dto.server.xdsx0017.resp.List list = new cn.com.yusys.yusp.dto.server.xdsx0017.resp.List();
                    list.setCus_id(cusId);
                    list.setCus_name(cusName);
                    list.setSum_crd(sumCrd.toString());// 总额度
                    list.setLmt_status(lmtStatus);// 授信状态
                    list.setManager_id(managerId);// 管护客户经理编号
                    list.setManager_name(managerIdName);// 管护客户经理名称
                    list.setStart_date(startDtae);// 额度起始日
                    list.setOver_date(overDtae);// 额度到期日
                    lists.add(list);
                }
            } else {//查询不到对应的客户信息
                cn.com.yusys.yusp.dto.server.xdsx0017.resp.List list = new cn.com.yusys.yusp.dto.server.xdsx0017.resp.List();
                list.setCus_id(StringUtils.EMPTY);
                list.setCus_name(StringUtils.EMPTY);
                list.setSum_crd(StringUtils.EMPTY);// 总额度
                list.setLmt_status(StringUtils.EMPTY);// 授信状态
                list.setManager_id(StringUtils.EMPTY);// 管护客户经理编号
                list.setManager_name(StringUtils.EMPTY);// 管护客户经理名称
                list.setStart_date(StringUtils.EMPTY);// 额度起始日
                list.setOver_date(StringUtils.EMPTY);// 额度到期日
                lists.add(list);
            }
            xdsx0017DataRespDto.setList(lists);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value);
        return xdsx0017DataRespDto;
    }

    //客户经理信息查询
    public AdminSmUserDto managerIdMessageFind(String managerId) {
        AdminSmUserDto adminSmUserDto = new AdminSmUserDto();
        try {
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, e.getMessage());
        }
        return adminSmUserDto;
    }
}
