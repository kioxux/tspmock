package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021年8月4日22:15:23
 * @desc 出账申请中交易对手信息校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0073Service {

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private ToppAcctSubService toppAcctSubService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年8月4日22:15:23
     * @version 1.0.0
     * @desc    出账申请中交易对手信息校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0073(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(pvpSerno);
        if(Objects.isNull(pvpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        //判断是否为受托支付
        String isBeEntrustedPay = pvpLoanApp.getIsBeEntrustedPay();
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isBeEntrustedPay)){
            QueryModel queryModelData = new QueryModel();
            queryModelData.addCondition("bizSerno",pvpSerno);
            queryModelData.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
            List<ToppAcctSub> toppAcctSubList = toppAcctSubService.selectByModel(queryModelData);
            if(toppAcctSubList.size()<=0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07301); //未完善交易对手信息
                return riskResultDto;
            }else{
                BigDecimal totalToppAmt = BigDecimal.ZERO;
                for (ToppAcctSub toppAcctSub : toppAcctSubList) {
                    BigDecimal toppAmt = toppAcctSub.getToppAmt();
                    totalToppAmt = totalToppAmt.add(toppAmt);
                }
                if(totalToppAmt.compareTo(pvpLoanApp.getPvpAmt()) != 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07302); //交易对手总额与出账金额不一致
                    return riskResultDto;
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
