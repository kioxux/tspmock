package cn.com.yusys.yusp.service.server.xdcz0029;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.SxkdDrawCheck;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdcz0029.req.Xdcz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.resp.Xdcz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.PvpAuthorizeService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@Transactional
public class Xdcz0029Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0029Service.class);
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Resource
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Resource
    private ToppAcctSubMapper toppAcctSubMapper;
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private SenddxService senddxService;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private SxkdDrawCheckMapper sxkdDrawCheckMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 网银推送省心快贷审核任务至信贷
     *
     * @param xdcz0029DataReqDto
     * @return
     */
    @Transactional
    public Xdcz0029DataRespDto getSxkdInfo(Xdcz0029DataReqDto xdcz0029DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key,
                DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdcz0029DataReqDto));
        Map map = new HashMap<>();
        Xdcz0029DataRespDto xdcz0029DataRespDto = new Xdcz0029DataRespDto();
        String cont_no = xdcz0029DataReqDto.getContNo();
        String isBankFlag = xdcz0029DataReqDto.getIsBankFlag();
        String billno = xdcz0029DataReqDto.getBillNo();
//        1、根据合同号查找相关信息
//        2、有合同号，判断是否存在借据号，如果存在只新增受托支付相关信息，落表trustee_pay 映射新信贷（出账授权主表PVP_AUTHORIZE）；
//          如果不存在，插入省心快贷提款表sxkd_Draw_Check （出账授权主表PVP_AUTHORIZE）。
//        3、  标志为1的时候发短信给客户经理提示尽快处理
        try {
            if (StringUtils.isEmpty(cont_no)) {
                //判断是否有合同号
                xdcz0029DataRespDto.setOpFlag("F");
                xdcz0029DataRespDto.setOpMsg("传入合同号为空！");
            }

            if (!StringUtils.isEmpty(xdcz0029DataReqDto.getBillNo())) {
                //根据合同号查找相关信息
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(cont_no);
                if (Objects.isNull(ctrLoanCont)) {
                    xdcz0029DataRespDto.setOpFlag("F");
                    xdcz0029DataRespDto.setOpMsg("根据合同号未查找相关信息！");
                    return xdcz0029DataRespDto;
                }
                //中文合同号
                String cont_no_cny = ctrLoanCont.getContCnNo();
                //客户经理号
                String manager_id = ctrLoanCont.getManagerId();
                //管户机构
                String manager_br_id = ctrLoanCont.getManagerBrId();
                //账务机构
                String fina_br_id = manager_br_id.substring(0, manager_br_id.length() - 1) + "1";

                // 获取营业日期
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                //判断借据号是否存在 ，若存在借据号
                String SERNO = "";
                map.put("billNo", xdcz0029DataReqDto.getBillNo());

                /**
                 * 此接口企业网银一笔出账信息可能会多次调用，因为企业网银端受托支付信息可能存在多笔，但是每次仅发送一笔受托支付信息，根据受托支付信息数量循环调用此接口(别问，我也不知道为啥不发list一起推送过来)。
                 * 所以先根据借据号查询省心快贷提款审核表是否存在数据，若存在，仅落受托支付信息。否则，先落省心快贷提款审核表，在落受托支付信息。
                 */
                int count = sxkdDrawCheckMapper.getJjhCount(map);
                String PVP_SERNO = sxkdDrawCheckMapper.getJjhSernoInfo(map);
                //新增受托支付相关信息 TOPP_ACCT_SUB
                SERNO = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
                map.put("PK_ID", SERNO);
                map.put("BIZ_SERNO", PVP_SERNO); //业务主键号
                map.put("TOPP_ACCT_NO", xdcz0029DataReqDto.getEntruPayAcctNo()); //交易对手账号
                map.put("TOPP_NAME", xdcz0029DataReqDto.getEntruPayAcctName()); //交易对手名称
                map.put("TOPP_AMT", xdcz0029DataReqDto.getAmt()); //交易对手金额
                map.put("ACCTSVCR_NO", xdcz0029DataReqDto.getAcctsvcrAcctNo());//开户行行号
                map.put("ACCTSVCR_NAME", xdcz0029DataReqDto.getAcctsvcrName()); //开户行名称
                map.put("IS_ONLINE", "1"); // 是否线上
                map.put("IS_BANK_ACCT", isBankFlag); //是否本行账户

                if (count > 0) {
                    //新增受托支付相关信息 TOPP_ACCT_SUB
                    int i = pvpAuthorizeMapper.insertJjhStzfInfo(map);
                    if (i > 0) {
                        xdcz0029DataRespDto.setOpFlag("S");
                        xdcz0029DataRespDto.setOpMsg("新增受托支付相关信息成功！");
                    } else {
                        xdcz0029DataRespDto = new Xdcz0029DataRespDto();
                    }
                } else {
                    //借据号不存在，插入省心快贷提款表=> pvp_loan_app
                    SxkdDrawCheck sxkdDrawCheck = new SxkdDrawCheck();
                    String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
                    String iqp_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
                    sxkdDrawCheck.setPvpSerno(serno);
                    sxkdDrawCheck.setSerno(iqp_serno);
                    sxkdDrawCheck.setCusId(xdcz0029DataReqDto.getCusId());
                    sxkdDrawCheck.setCusName(xdcz0029DataReqDto.getCusName());
                    sxkdDrawCheck.setBillNo(xdcz0029DataReqDto.getBillNo());
                    sxkdDrawCheck.setContNo(xdcz0029DataReqDto.getContNo());
                    sxkdDrawCheck.setContCnNo(cont_no_cny);
                    sxkdDrawCheck.setPvpDate(xdcz0029DataReqDto.getLoanStartDate());
                    sxkdDrawCheck.setBillEndDate(xdcz0029DataReqDto.getLoanEndDate());
                    sxkdDrawCheck.setPvpAmt(xdcz0029DataReqDto.getLoanAmt());
                    sxkdDrawCheck.setImgSerno(iqp_serno);
                    //客户经理号
                    sxkdDrawCheck.setManagerId(manager_id);
                    //账务机构
                    sxkdDrawCheck.setFinaBrId(fina_br_id);
                    //管户机构
                    sxkdDrawCheck.setManagerBrId(manager_br_id);
                    //中文合同号
                    sxkdDrawCheck.setContCnNo(cont_no_cny);
                    //还款方式
                    sxkdDrawCheck.setRepayMode("A0001");
                    //还款账号
                    sxkdDrawCheck.setRepayAccno(xdcz0029DataReqDto.getLoanAcctNo());
                    //放款账号
                    sxkdDrawCheck.setDisbAcct(xdcz0029DataReqDto.getLoanAcctNo());
                    //申请状态
                    sxkdDrawCheck.setApproveStatus("000");
                    //插入新表
                    sxkdDrawCheckMapper.insert(sxkdDrawCheck);
                    map.put("BIZ_SERNO", serno);
                    pvpAuthorizeMapper.insertJjhStzfInfo(map);
                    xdcz0029DataRespDto.setOpFlag("S");
                    xdcz0029DataRespDto.setOpMsg("信贷登记成功！");
                }
            } else {
                xdcz0029DataRespDto.setOpFlag("F");
                xdcz0029DataRespDto.setOpMsg("传入借据号为空！");
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdcz0029DataRespDto));
        return xdcz0029DataRespDto;
    }
}
