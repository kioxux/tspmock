/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.Doc000ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.List_index;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.resp.Doc000RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.req.Doc002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.resp.Doc002RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.DocArchiveInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocArchiveInfoService {

    private static final Logger logger = LoggerFactory.getLogger(DocArchiveInfoService.class);

    private static Map<String, String> docStatusMap = new HashMap<String, String>();

    static {
        docStatusMap.put("3(入库在途)", "02");
        docStatusMap.put("6(已入库)", "03");
        docStatusMap.put("30(退回申请)", "04");
        docStatusMap.put("8(已出库)", "09");
        docStatusMap.put("40(退回确认)", "04");
    }

    @Autowired
    private DocArchiveInfoMapper docArchiveInfoMapper;
    @Autowired
    private DocArchiveMaterListService docArchiveMaterListService;
    @Autowired
    private DocAccListService docAccListService;
    @Autowired
    private Dscms2DocClientService dscms2DocClientService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private CtrDiscContService ctrDiscContService;
    @Autowired
    private CtrTfLocContService ctrTfLocContService;
    @Autowired
    private CtrCvrgContService ctrCvrgContService;
    @Autowired
    private PvpAccpAppService pvpAccpAppService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CreditCardAppInfoService creditCardAppInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocArchiveInfo selectByPrimaryKey(String docSerno) {
        return docArchiveInfoMapper.selectByPrimaryKey(docSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocArchiveInfo> selectAll(QueryModel model) {
        List<DocArchiveInfo> records = (List<DocArchiveInfo>) docArchiveInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocArchiveInfo> selectByModel(QueryModel model) {
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                // 集中作业档案岗 R1034；集中作业管理人员 R1036
                if ("R1034".equals(roles.get(i).getCode()) || "R1036".equals(roles.get(i).getCode())) {
                    model.addCondition("localFlag", "true");
                    break;
                } else if ("R1054".equals(roles.get(i).getCode())) {
                    // R1054 信贷管理部档案管理人员
                    model.addCondition("daFlag", "true");
                    break;
                }
            }
        }
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("createDate desc");
        List<DocArchiveInfo> list = docArchiveInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocArchiveInfo record) {
        DocArchiveInfo docArchiveInfo = docArchiveInfoMapper.selectByPrimaryKey(record.getDocSerno());
        if (docArchiveInfo != null) {
            // 已生成业务数据
            return 1;
        }
        // 根据客户编号以及关联业务编号去查询是否已存在对应的档案（排除作废的）
        QueryModel model = new QueryModel();
        // 关联业务编号
        model.addCondition("bizSerno", record.getBizSerno());
        // 客户编号
        model.addCondition("cusId", record.getCusId());
        List<DocArchiveInfo> docList = docArchiveInfoMapper.selectByModel(model);
        if (Objects.nonNull(docList) && docList.size() > 0) {
            for (DocArchiveInfo doc : docList) {
                if (!Objects.equals("04", doc.getDocStauts())) {
                    throw BizException.error(null, "9999", "已存在关联业务编号与客户编号相同的档案信息，请确认！！");
                }
            }
        }

        /** 根据档案类型生成资料清单 **/
        docArchiveMaterListService.insertMaterList(record);
        /** 生成档案台账 **/
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        DocAccList docAccList = new DocAccList();
        docAccList.setDocSerno(record.getDocSerno());
        docAccList.setDocNo(record.getDocNo());
        docAccList.setArchiveMode(record.getArchiveMode());
        docAccList.setDocClass(record.getDocClass());
        docAccList.setDocType(record.getDocType());
        docAccList.setBizSerno(record.getBizSerno());
        docAccList.setCusId(record.getCusId());
        docAccList.setCusName(record.getCusName());
        docAccList.setManagerId(record.getManagerId());
        docAccList.setManagerBrId(record.getManagerBrId());
        docAccList.setDocStauts(record.getDocStauts());
        docAccList.setInputId(record.getInputId());
        docAccList.setInputBrId(record.getInputBrId());
        docAccList.setInputDate(record.getInputDate());
        docAccList.setCreateTime(DateUtils.getCurrDate());
        docAccList.setContNo(record.getContNo());
        docAccList.setBillNo(record.getBillNo());
        docAccListService.insertSelective(docAccList);

        return docArchiveInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocArchiveInfo record) {
        return docArchiveInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocArchiveInfo docArchiveInfo) {

        // 更新档案资料清单
        List<DocArchiveMaterList> docArchiveMaterListList = docArchiveInfo.getArchiveParam();
        if (docArchiveMaterListList != null) {
            for (DocArchiveMaterList docArchiveMater : docArchiveMaterListList) {
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo != null) {
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    docArchiveMater.setUpdId(userInfo.getLoginCode());// 更新人
                    docArchiveMater.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docArchiveMater.setUpdDate(openDay);// 更新日期
                }
                docArchiveMaterListService.updateSelective(docArchiveMater);
            }
        }

        return docArchiveInfoMapper.updateByPrimaryKey(docArchiveInfo);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocArchiveInfo record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docArchiveInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String docSerno) {
        return docArchiveInfoMapper.deleteByPrimaryKey(docSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docArchiveInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据档案类型和客户号获取业务流水号
     *
     * @author jijian_yx
     * @date 2021/6/21 16:35
     **/
    public List<DocArchiveInfo> selectSernoByDocType(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocArchiveInfo> docArchiveInfoList = docArchiveInfoMapper.selectSernoByDocType(model);
        if (docArchiveInfoList != null && docArchiveInfoList.size() > 0) {
            for (DocArchiveInfo docArchiveInfo : docArchiveInfoList) {
                String cusId = docArchiveInfo.getCusId();
                if (!StringUtils.isEmpty(cusId)) {
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                    if (cusBaseClientDto != null) {
                        docArchiveInfo.setCertCode(cusBaseClientDto.getCertCode());
                        docArchiveInfo.setCertType(cusBaseClientDto.getCertType());
                    }
                }
            }
        }
        PageHelper.clearPage();
        return docArchiveInfoList;
    }

    /**
     * 提交入库
     *
     * @author jijian_yx
     * @date 2021/6/21 16:38
     **/
    public Map<String, String> archiveOpt(DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = new HashMap<>();
        String rtnCode = "";
        String rtnMsg = "";
        // 业务数据json类型封转格式list_index record
        List<List_index> dataList = new ArrayList<>();
        try {
            /** 提交入库 **/
            String docStatus = docArchiveInfo.getDocStauts();
            // 档案状态： 01：等待入库、05：已退回
            if (!"01".equals(docStatus) && !"05".equals(docStatus)) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "档案状态不为待入库或已退回！";
                return map;
            }

            int totalPage = Integer.parseInt(Optional.ofNullable(docArchiveInfo.getTotalPage()).orElse("0"));
            if (totalPage < 1) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "资料总页数为0，请先修改档案资料清单！";
                return map;
            }

            // 信用卡业务审批未结束拦截提交入库
            if (Objects.equals(docArchiveInfo.getDocType(), "20")) {
                CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(docArchiveInfo.getBizSerno());
                if (null != creditCardAppInfo) {
                    if (!Objects.equals(creditCardAppInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_996)
                            && !Objects.equals(creditCardAppInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_997)
                            && !Objects.equals(creditCardAppInfo.getApproveStatus(), CmisCommonConstants.WF_STATUS_998)) {
                        rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                        rtnMsg = "关联的信用卡业务尚未审批结束,无法提交入库！";
                        return map;
                    }
                }
            }

            // 校验合同启用状态（09贴现额度协议(最高额)、16白领易贷通、15贸易融资业务、房抵e点贷(小企业/对公)、省心快贷、信用证开立、保函、福费廷）
            String contNo = Optional.ofNullable(docArchiveInfo.getContNo()).orElse("");
            String docType = Optional.ofNullable(docArchiveInfo.getDocType()).orElse("");
            String docBizType = Optional.ofNullable(docArchiveInfo.getDocBizType()).orElse("");
            if (StringUtils.nonEmpty(contNo)) {
                if (Objects.equals("09", docType)) {
                    // 09贴现额度协议
                    CtrDiscCont ctrDiscCont = ctrDiscContService.selectByContNo(contNo);
                    if (null == ctrDiscCont || !Objects.equals("1", ctrDiscCont.getCtrBeginFlag())) {
                        rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                        rtnMsg = "未经过合同影像审核，合同未启用！";
                        return map;
                    }
                } else if (Objects.equals("15", docType)) {
                    // 15贸易融资
                    if (Objects.equals("06", docBizType)) {
                        // 06开证
                        CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByContNo(contNo);
                        if (null == ctrTfLocCont || !Objects.equals("1", ctrTfLocCont.getCtrBeginFlag())) {
                            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                            rtnMsg = "未经过合同影像审核，合同未启用！";
                            return map;
                        }
                    } else {
                        // 05福费廷 07贸易融资
                        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                        if (null == ctrLoanCont || !Objects.equals("1", ctrLoanCont.getCtrBeginFlag())) {
                            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                            rtnMsg = "未经过合同影像审核，合同未启用！";
                            return map;
                        }
                    }
                } else if (Objects.equals("05", docType)) {
                    // 05对公及个人经营性贷款 16白领易贷通
                    CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                    if (null != ctrLoanCont) {
                        String prdTypeProp = ctrLoanCont.getPrdTypeProp();// 产品类型属性 STD_PRD_TYPE_PROP
                        // P011省心快贷 P034房抵e点贷
                        if ((Objects.equals("P011", prdTypeProp) || Objects.equals("P034", prdTypeProp))
                                && !Objects.equals("1", ctrLoanCont.getCtrBeginFlag())) {
                            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                            rtnMsg = "未经过合同影像审核，合同未启用！";
                            return map;
                        }
                    }
                } else if (Objects.equals("13", docType)) {
                    // 13保函
                    CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByContNo(contNo);
                    if (null == ctrCvrgCont || !Objects.equals("1", ctrCvrgCont.getCtrBeginFlag())) {
                        rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                        rtnMsg = "未经过合同影像审核，合同未启用！";
                        return map;
                    }
                } else if (Objects.equals("16", docType)) {
                    // 16白领易贷通
                    CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                    if (null == ctrLoanCont || !Objects.equals("1", ctrLoanCont.getCtrBeginFlag())) {
                        rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                        rtnMsg = "未经过线上提款启用，合同未启用！";
                        return map;
                    }
                }
            }

            // 未完成的关联影像补扫任务数
            String bizSerno = docArchiveInfo.getBizSerno();
            Map<String, String> spplMap = new HashMap<>();
            spplMap.put("bizSerno", bizSerno);// 业务流水号
            spplMap.put("existsAppFlag", "true");// 标识位 在途
            int count = docImageSpplInfoService.selectByBizSerno(spplMap);
            if (count > 0) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "入库失败，关联业务编号【" + bizSerno + "】存在未完成的影像补扫任务！";
                return map;
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            String opeateUserId = "";// 操作员号
            String orgName = "";// 机构名
            User user = SessionUtils.getUserInformation();
            if (user != null) {
                opeateUserId = user.getUserId();
            }
            AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(docArchiveInfo.getManagerBrId());
            if (Objects.nonNull(adminSmOrgDto)) {
                orgName = adminSmOrgDto.getOrgName();
            }
            String fina_br_id = Optional.ofNullable(docArchiveInfo.getManagerBrId()).orElse("");// 账务机构号
            String real_br_id = Optional.ofNullable(docArchiveInfo.getManagerBrId()).orElse("");// 真实机构号
            // 公积金账务机构特殊处理
            if (Objects.equals("18", docArchiveInfo.getDocType())) {
                fina_br_id = "999999";
            } else if (Objects.equals("21", docArchiveInfo.getDocType()) || Objects.equals("22", docArchiveInfo.getDocType())) {
                // 除新浦支行067000、即墨支行047000，其他小贷送016000
                if (!Objects.equals(fina_br_id, "047000") && !Objects.equals(fina_br_id, "067000")) {
                    fina_br_id = "016000";
                }
            } else if (Objects.equals("23", docArchiveInfo.getDocType()) || Objects.equals("24", docArchiveInfo.getDocType())) {
                String belgLine = docArchiveInfoMapper.selectBelgLineForYWBG(docArchiveInfo.getBizSerno(), docArchiveInfo.getDocType());
                if (Objects.equals(belgLine, "01")) {
                    // 除新浦支行067000、即墨支行047000，其他小贷送016000
                    if (!Objects.equals(fina_br_id, "047000") && !Objects.equals(fina_br_id, "067000")) {
                        fina_br_id = "016000";
                    }
                }
            }
            // 东海、寿光机构号特殊处理
            if (fina_br_id.startsWith("81")) {
                fina_br_id = "810100";// 东海村镇银行营业部
                real_br_id = "810100";// 东海村镇银行营业部
            } else if (fina_br_id.startsWith("80")) {
                fina_br_id = "800100";// 寿光村镇银行营业部
                real_br_id = "800100";// 东海村镇银行营业部
            }

            // 档案入库接口必传字段
            Doc000ReqDto doc000ReqDto = new Doc000ReqDto();
            // 机构号
            doc000ReqDto.setOrgId(fina_br_id);
            // 机构名称
            doc000ReqDto.setOrgName(orgName);
            // 档案类型 信贷档案:0100050006
            doc000ReqDto.setTypeCode("0100050006");
            // 档案代码 2001：放款资料A，2002：放款资料B
            // 是否按揭
            doc000ReqDto.setIs_aj("0");
            // 档案代码
            // 2001：放款资料
            // 2006：客户资料
            // 2007：授信资料
            // XYKZL：信用卡资料
            // XD：小贷
            if (Objects.equals("01", docArchiveInfo.getDocType()) || Objects.equals("02", docArchiveInfo.getDocType())) {
                doc000ReqDto.setFileCode("2006");
            } else if (Objects.equals("03", docArchiveInfo.getDocType()) || Objects.equals("04", docArchiveInfo.getDocType())) {
                doc000ReqDto.setFileCode("2007");
            } else if (Objects.equals("20", docArchiveInfo.getDocType())) {
                doc000ReqDto.setFileCode("XYKZL");
            } else if (Objects.equals("21", docArchiveInfo.getDocType()) || Objects.equals("22", docArchiveInfo.getDocType())) {
                doc000ReqDto.setFileCode("XD");
            } else if (Objects.equals("23", docArchiveInfo.getDocType()) || Objects.equals("24", docArchiveInfo.getDocType())) {
                String belgLine = docArchiveInfoMapper.selectBelgLineForYWBG(docArchiveInfo.getBizSerno(), docArchiveInfo.getDocType());
                if (Objects.equals(belgLine, "01")) {
                    // 小微
                    doc000ReqDto.setFileCode("XD");
                } else {
                    doc000ReqDto.setFileCode("2001");
                }
            } else {
                doc000ReqDto.setFileCode("2001");
                if (Objects.equals("17", docArchiveInfo.getDocType())) {
                    doc000ReqDto.setIs_aj("1");
                }
            }
            // 档案名称（这里可以传：客户号）
            doc000ReqDto.setFileName(docArchiveInfo.getCusName());
            // 备注
            doc000ReqDto.setFileDesc("档案入库");
            // 交易类型；001:档案登记,002:档案登记撤销
            doc000ReqDto.setType("001");
            // 默认 100001：信贷档案借口
            doc000ReqDto.setRequestType("100001");
            // 操作员号
            doc000ReqDto.setOpeateUserId(opeateUserId);
            // 操作日期
            doc000ReqDto.setOpeateDate(openDay);
            // 档案编号：交易类型为002必传，默认交易类型为：001
            doc000ReqDto.setFileNum("");
            // 对收到的信息进行处理的结果的反馈
            doc000ReqDto.setResult("");
            // 归档模式(01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档)
            if (Objects.equals("01", docArchiveInfo.getArchiveMode())) {
                //是否异地
                doc000ReqDto.setIs_yd("1");
                //库房（本地A 异地B 村镇C）
                doc000ReqDto.setKfbh("B");
            } else if (Objects.equals("02", docArchiveInfo.getArchiveMode()) || Objects.equals("03", docArchiveInfo.getArchiveMode())) {
                //是否异地
                doc000ReqDto.setIs_yd("0");
                //库房（本地A 异地B 村镇C）
                doc000ReqDto.setKfbh("A");
            } else {
                //是否异地
                doc000ReqDto.setIs_yd("0");
                //库房（本地A 异地B 村镇C）
                doc000ReqDto.setKfbh("C");
            }
            // 公积金特殊处理
            if (Objects.equals("18", docArchiveInfo.getDocType())) {
                //库房（本地A 异地B 村镇C）
                doc000ReqDto.setKfbh("A");
            }
            // 真实机构号
            doc000ReqDto.setReal_mng_br_id(real_br_id);
            List_index index = new List_index();
            // 合同号
            if (StringUtils.nonEmpty(docArchiveInfo.getContNo())) {
                index.setContid(docArchiveInfo.getContNo());
            } else {
                index.setContid(docArchiveInfo.getDocSerno());
            }
            // 合同时间(可以用：（档案归档时间）)
            index.setContdate(docArchiveInfo.getStorageDate());
            // 客户名称
            index.setCustname(docArchiveInfo.getCusName());
            // 业务编号(关联业务编号)
            index.setBusinessid(docArchiveInfo.getBizSerno());
            // 录入影像的外接编码
            index.setOutsystem_code("");
            // 客户号
            index.setCustid(docArchiveInfo.getCusId());
            // 借据号
            index.setBillno(docArchiveInfo.getDocSerno());
            // 客户经理号
            index.setCustconduct(docArchiveInfo.getManagerId());
            // 客户类型
            index.setCusttype("");
            dataList.add(index);
            // 业务数据json类型封转格式 list_index record
            doc000ReqDto.setList_index(dataList);
            // 调用档案系统接口，进行提交入库
            ResultDto<Doc000RespDto> doc000RespDto = dscms2DocClientService.doc000(doc000ReqDto);
            String doc000Code = Optional.ofNullable(doc000RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String doc000Meesage = Optional.ofNullable(doc000RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            logger.info("档案归档入库返回信息[{}],交易码[{}],交易信息[{}]", doc000RespDto, doc000Code, doc000Meesage);
            if (Objects.equals(doc000Code, SuccessEnum.CMIS_SUCCSESS.key) && Objects.nonNull(doc000RespDto.getData())
                    && StringUtils.nonEmpty(doc000RespDto.getData().getFileNum())) {
                // 接口发送成功，变更档案归档和台账状态已入库，更新返回的档案编号
                String docNo = doc000RespDto.getData().getFileNum();
                User userInfo = SessionUtils.getUserInformation();
                // 更新档案归档状态
                docArchiveInfo.setDocNo(docNo);
                docArchiveInfo.setDocStauts("02");// 入库状态：02入库在途
                docArchiveInfo.setStorageDate(openDay + " " + DateUtils.getCurrTime());// 入库操作时间
                docArchiveInfo.setOptUsr(userInfo.getUserId());// 入库操作人
                docArchiveInfo.setOptOrg(userInfo.getOrg().getCode());// 入库操作机构
                docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                docArchiveInfo.setUpdDate(openDay);// 更新日期
                int docNum = updateSelective(docArchiveInfo);
                logger.info("档案归档入库提交成功,归档流水号[{}],档案编号[{}],更新结果[{}]", docArchiveInfo.getDocSerno(), docNo, docNum);
                // 更新档案台账状态
                DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
                if (docAccList != null) {
                    docAccList.setDocNo(docNo);
                    docAccList.setDocStauts("02");// 入库在途
                    docAccList.setStorageOptDate(docArchiveInfo.getStorageDate());
                    docAccList.setOptUsr(userInfo.getUserId());// 入库操作人
                    docAccList.setOptOrg(userInfo.getOrg().getCode());// 入库操作机构
                    docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                    docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docArchiveInfo.setUpdDate(openDay);// 更新日期
                    int accNum = docAccListService.updateSelective(docAccList);
                    logger.info("档案归档入库提交成功,台账流水号[{}],档案编号[{}],更新结果[{}]", docAccList.getDocSerno(), docNo, accNum);
                }
                rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
                rtnMsg = "操作成功";
            } else if (doc000Meesage.contains("该档案已申请入库，请勿重复提交")) {
                // 提交入库有未知事务问题,重复提交入库时再次更新
                String docNo = doc000RespDto.getData().getFileNum();
                if (Objects.nonNull(docNo)) {
                    User userInfo = SessionUtils.getUserInformation();
                    // 更新档案归档状态
                    docArchiveInfo.setDocNo(docNo);
                    docArchiveInfo.setDocStauts("02");// 入库状态：02入库在途
                    docArchiveInfo.setStorageDate(openDay + " " + DateUtils.getCurrTime());// 入库操作时间
                    docArchiveInfo.setOptUsr(userInfo.getUserId());// 入库操作人
                    docArchiveInfo.setOptOrg(userInfo.getOrg().getCode());// 入库操作机构
                    docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                    docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docArchiveInfo.setUpdDate(openDay);// 更新日期
                    int docNum = updateSelective(docArchiveInfo);
                    logger.info("档案归档入库更新成功,归档流水号[{}],档案编号[{}],更新结果[{}]", docArchiveInfo.getDocSerno(), docNo, docNum);
                    // 更新档案台账状态
                    DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
                    if (docAccList != null) {
                        docAccList.setDocNo(docNo);
                        docAccList.setDocStauts("02");// 入库在途
                        docAccList.setStorageOptDate(docArchiveInfo.getStorageDate());
                        docAccList.setOptUsr(userInfo.getUserId());// 入库操作人
                        docAccList.setOptOrg(userInfo.getOrg().getCode());// 入库操作机构
                        docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                        docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                        docArchiveInfo.setUpdDate(openDay);// 更新日期
                        int accNum = docAccListService.updateSelective(docAccList);
                        logger.info("档案归档入库更新成功,台账流水号[{}],档案编号[{}],更新结果[{}]", docAccList.getDocSerno(), docNo, accNum);
                    }
                }
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = doc000Meesage;
            } else {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = doc000Meesage;
            }

        } catch (
                Exception e) {
            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
            rtnMsg = e.getMessage();
            logger.error("档案管理提交入库异常，原因：" + e.getMessage(), e);
        } finally {
            map.put("rtnCode", rtnCode);
            map.put("rtnMsg", rtnMsg);
        }
        return map;
    }

    /**
     * 作废档案归档信息
     *
     * @author jijian_yx
     * @date 2021/6/21 20:07
     **/
    public Map<String, String> invalid(DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = new HashMap<>();
        String rtnCode = "";
        String rtnMsg = "";

        try {
            /**
             * 分支机构档案管理岗、集中作业档案岗，只能删除人工新增的档案归档任务；
             * 系统管理员可删除系统生成、人工新增的档案归档任务。
             * 删除时，更新档案状态为作废。
             **/
            String docStatus = docArchiveInfo.getDocStauts();
            if ("06".equals(docStatus)) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "档案状态已作废！";
                return map;
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                docArchiveInfo.setUpdDate(openDay);// 更新日期
            }
            docArchiveInfo.setDocStauts("06");// 档案状态(STD_DOC_STAUTS:06作废)
            docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);

            // 更新档案台账状态
            DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
            if (docAccList != null) {
                if (userInfo != null) {
                    docAccList.setUpdId(userInfo.getLoginCode());// 更新人
                    docAccList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docAccList.setUpdDate(openDay);// 更新日期
                }
                docAccList.setDocStauts("06");// 作废
                docAccListService.updateSelective(docAccList);
            }

            rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
            rtnMsg = "作废成功";
        } catch (Exception e) {
            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
            rtnMsg = e.getMessage();
        } finally {
            map.put("rtnCode", rtnCode);
            map.put("rtnMsg", rtnMsg);
        }
        return map;
    }

    /**
     * 档案接收
     *
     * @author jijian_yx
     * @date 2021/6/22 22:33
     **/
    public Map<String, String> receive(DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = new HashMap<>();
        String rtnCode = "";
        String rtnMsg = "";
        try {
            String docStatus = docArchiveInfo.getDocStauts();
            if ("05".equals(docStatus)) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "档案状态已退回,请勿重复操作！";
                return map;
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo != null) {
                docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                docArchiveInfo.setUpdDate(openDay);// 更新日期
            }
            docArchiveInfo.setDocStauts("05");// 档案状态(STD_DOC_STAUTS:05已退回)
            // docArchiveInfo.setFinishDate(DateUtils.getCurrDateStr());
            docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);

            // 更新档案台账状态
            DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
            if (docAccList != null) {
                if (userInfo != null) {
                    docAccList.setUpdId(userInfo.getLoginCode());// 更新人
                    docAccList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docAccList.setUpdDate(openDay);// 更新日期
                }
                docAccList.setDocStauts("05");// 已退回
                docAccListService.updateSelective(docAccList);
            }

            rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
            rtnMsg = "接收成功";
        } catch (Exception e) {
            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
            rtnMsg = e.getMessage();
        } finally {
            map.put("rtnCode", rtnCode);
            map.put("rtnMsg", rtnMsg);
        }
        return map;
    }

    /**
     * 去档案系统查询档案状态
     *
     * @author jijian_yx
     * @date 2021/6/25 14:06
     **/
    public Map<String, String> searchStatus(DocArchiveInfo docArchiveInfo) {
        Map<String, String> map = new HashMap<>();
        String rtnCode = "";
        String rtnMsg = "";
        try {
            String docStatus = docArchiveInfo.getDocStauts();
            if (!"02".equals(docStatus)) {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = "非入库在途的档案不可操作！";
                return map;
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 档案入库查询接口必传字段
            Doc002ReqDto doc002ReqDto = new Doc002ReqDto();
            // 档案号
            doc002ReqDto.setFilenum(docArchiveInfo.getDocNo());
            // 状态
            doc002ReqDto.setFilestatus("");

            // 调用档案系统接口，进行提交入库
            ResultDto<Doc002RespDto> doc002RespDto = dscms2DocClientService.doc002(doc002ReqDto);
            String doc002Code = Optional.ofNullable(doc002RespDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String doc002Meesage = Optional.ofNullable(doc002RespDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(doc002Code, SuccessEnum.CMIS_SUCCSESS.key) && !StringUtils.isEmpty(doc002Meesage)) {
                for (Map.Entry<String, String> entry : docStatusMap.entrySet()) {
                    if (doc002Meesage.contains(entry.getKey())) {
                        // 档案状态(STD_DOC_STAUTS:03已入库)
                        docArchiveInfo.setFinishDate(openDay);
                        docArchiveInfo.setDocStauts(entry.getValue());
                        User userInfo = SessionUtils.getUserInformation();
                        if (userInfo != null) {
                            docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                            docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                            docArchiveInfo.setUpdDate(openDay);// 更新日期
                        }
                        docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);
                        // 更新档案台账状态
                        DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
                        if (docAccList != null) {
                            docAccList.setDocStauts(entry.getValue());// 已入库
                            docAccList.setStorageCashDate(DateUtils.getCurrDateTimeStr());// 档案入现金库时间
                            if (userInfo != null) {
                                docAccList.setUpdId(userInfo.getLoginCode());// 更新人
                                docAccList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                                docAccList.setUpdDate(openDay);// 更新日期
                            }
                            docAccListService.updateSelective(docAccList);
                        }
                        rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
                        rtnMsg = doc002Meesage;
                        return map;
                    }
                }

                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = doc002Meesage;
            } else {
                rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
                rtnMsg = doc002Meesage;
            }

        } catch (Exception e) {
            rtnCode = EcbEnum.COMMON_EXCEPTION_DEF.key;
            rtnMsg = e.getMessage();
        } finally {
            map.put("rtnCode", rtnCode);
            map.put("rtnMsg", rtnMsg);
        }
        return map;
    }

    /**
     * 系统生成档案归档信息
     *
     * @author jijian_yx
     * @date 2021/6/29 20:24
     **/
    public int createDocArchiveBySys(DocArchiveClientDto docArchiveClientDto) {
        int num = 0;
        try {
            if (StringUtils.isEmpty(docArchiveClientDto.getBizSerno())) {
                // 未传入业务流水号
                logger.info("档案归档任务无业务流水号,入参[{}]", docArchiveClientDto);
                return 0;
            }

            // 房抵e点贷为线上授信,不生成归档任务
            if (Objects.equals(docArchiveClientDto.getDocClass(), "02")) {
                int lmtCount = docArchiveInfoMapper.getP034LmtCount(docArchiveClientDto.getBizSerno());
                if (lmtCount > 0) {
                    logger.info("授信流水号[{}]为房抵e点贷授信业务,不生成归档任务", docArchiveClientDto.getBizSerno());
                    return 1;
                }
            }

            if ("04".equals(docArchiveClientDto.getDocType())) {
                /**
                 * 1、授信申报审批通过或者否决都生成档案归档任务；
                 * 2、授信复议和再议通过或否决后，归档任务生成规则；
                 * 若状态为03已入库、02入库在途、06作废，重新生成一笔新的归档任务；
                 * 若状态01等待入库、04退回在途、05已退回，合并成一条归档任务；
                 **/
                LmtApp lmtApp = lmtAppService.selectBySerno(docArchiveClientDto.getBizSerno());
                if (null != lmtApp && ("05".equals(lmtApp.getLmtType()) || "06".equals(lmtApp.getLmtType()))) {
                    // 单户/集团授信 05授信复议 06授信再议
                    String ogrigiLmtSerno = lmtApp.getOgrigiLmtSerno();// 原授信流水号
                    // 判断该笔业务流水号是否已生成在途的归档任务
                    Map<String, String> map = new HashMap<>();
                    map.put("cusId", docArchiveClientDto.getCusId());// 客户号
                    map.put("bizSerno", ogrigiLmtSerno);// 业务流水号
                    map.put("docBizType", docArchiveClientDto.getDocBizType());//档案业务品种
                    map.put("taskFlag", "01");// 任务标识 01系统发起;02人工发起
                    map.put("lmtFlag", "todo");// 状态标识 01等待入库、04退回在途、05已退回
                    int count = docArchiveInfoMapper.selectByBizSerno(map);
                    if (count > 0) {
                        return 1;
                    }
                }
            }


            // 判断该笔业务流水号是否已生成等待入库的归档任务
            Map<String, String> map = new HashMap<>();
            map.put("cusId", docArchiveClientDto.getCusId());// 客户号
            map.put("bizSerno", docArchiveClientDto.getBizSerno());// 业务流水号
            map.put("docBizType", docArchiveClientDto.getDocBizType());//档案业务品种
            map.put("taskFlag", "01");// 任务标识 01系统发起;02人工发起
            map.put("docStauts", "01");// 档案状态 STD_DOC_STAUTS 01:等待入库
            int count = docArchiveInfoMapper.selectByBizSerno(map);
            if (count > 0) {
                // 已生成,防止重复生成
                logger.info("业务流水号[{}]已生成待归档的档案归档任务" + docArchiveClientDto.getBizSerno());
                return 1;
            } else {
                // 最高额授信协议档案归档任务中的普通合同在放款出账会再次生产归档任务,此处根据合同号判断该笔合同的最高额授信协议的归档任务是否存在,并且是否为等待入库状态
                // 如果存在并且为等待入库状态,作废该笔合同对应的最高授信协议档案归档任务
                if (Objects.equals(docArchiveClientDto.getDocType(), "05") && Objects.equals(docArchiveClientDto.getDocBizType(), "08")) {
                    DocArchiveInfo highDocArchiveInfo = docArchiveInfoMapper.queryHighAmtAgrCount(docArchiveClientDto.getContNo());
                    if (null != highDocArchiveInfo) {
                        highDocArchiveInfo.setDocStauts("06");//作废
                        docArchiveInfoMapper.updateByPrimaryKeySelective(highDocArchiveInfo);
                    }
                }

                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(docArchiveClientDto.getManagerBrId());
                String archiveMode = "03";
                if (null != resultDto && null != resultDto.getData()) {
                    String orgType = resultDto.getData().getOrgType();
                    if ("1".equals(orgType) || "2".equals(orgType) || "3".equals(orgType) || "A".equals(orgType)) {
                        archiveMode = "01";
                    } else {
                        List<String> nodeNames = commonService.getInstanceNodeNameByBizId(docArchiveClientDto.getBizSerno());
                        if (null != nodeNames && nodeNames.size() > 0) {
                            for (String nodeName : nodeNames) {
                                if (nodeName.contains("集中作业")) {
                                    archiveMode = "02";// 本地集中归档
                                    break;
                                }
                            }
                        }
                    }
                    // 合作方协议(10)/总行部室(0) 不生成档案任务
                    if ("0".equals(orgType) && "10".equals(docArchiveClientDto.getDocType())) {
                        logger.info("合作方协议总行部室不生成档案任务,关联流水号[{}]", docArchiveClientDto.getBizSerno());
                        return 1;
                    } else if ("10".equals(docArchiveClientDto.getDocType())) {
                        // 合作方协议已判断是否走集中作业
                        archiveMode = docArchiveClientDto.getArchiveMode();
                    }
                }
                // 信用卡归档任务
                if ("20".equals(docArchiveClientDto.getDocType())) {
                    archiveMode = "03";// 本地分支机构归档
                }
                // 本地支行基础资料待归档任务生成规则调整为按自然年度（1.1-12.31）生成（一个自然年度只生成一次待归档任务）
                // 异地归档(公司客户创建/转正、个人客户第一次财报新增)只生成一次归档任务
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                Date nowDay = DateUtils.parseDate(openDay, "yyyy-MM-dd");
                int year = DateUtils.getYear(nowDay);
//                int year = DateUtils.getCurrYear();
                String docClass = docArchiveClientDto.getDocClass();
                if ("01".equals(docClass) && ("02".equals(archiveMode) || "03".equals(archiveMode))) {
                    Map<String, String> baseMap = new HashMap<>();
                    baseMap.put("baseBDFlag", "true");
                    baseMap.put("baseCusId", docArchiveClientDto.getCusId());
                    baseMap.put("baseYear", String.valueOf(year));
                    int baseCount = docArchiveInfoMapper.selectByBizSerno(baseMap);
                    if (baseCount > 0) {
                        // 基本资料归档任务本自然年度已生成
                        logger.info("本地归档客户[{}]自然年度[{}]已生成档案归档任务" + docArchiveClientDto.getCusId(), year);
                        return 1;
                    }
                } else if ("01".equals(docClass) && "01".equals(archiveMode)) {
                    Map<String, String> baseMap = new HashMap<>();
                    baseMap.put("baseYDFlag", "true");
                    baseMap.put("baseCusId", docArchiveClientDto.getCusId());
                    int baseCount = docArchiveInfoMapper.selectByBizSerno(baseMap);
                    if (baseCount > 0) {
                        // 基本资料归档任务已生成
                        logger.info("异地归档客户[{}]已生成档案归档任务" + docArchiveClientDto.getCusId());
                        return 1;
                    }
                }

                // 20信用卡业务零售支行转换为对应的中心支行
                if (Objects.equals(docArchiveClientDto.getDocType(), "20")) {
                    String managerBrId = docArchiveClientDto.getManagerBrId();
                    if (Objects.equals(managerBrId, "016600") || Objects.equals(managerBrId, "016800") ||
                            Objects.equals(managerBrId, "016900") || Objects.equals(managerBrId, "017600") ||
                            Objects.equals(managerBrId, "018400") || Objects.equals(managerBrId, "018500") ||
                            Objects.equals(managerBrId, "018600") || Objects.equals(managerBrId, "018700")) {
                        docArchiveClientDto.setManagerBrId("017000");
                    } else if (Objects.equals(managerBrId, "018100") || Objects.equals(managerBrId, "018300")) {
                        docArchiveClientDto.setManagerBrId("019500");
                    } else if (Objects.equals(managerBrId, "017200") || Objects.equals(managerBrId, "017500")) {
                        docArchiveClientDto.setManagerBrId("017700");
                    } else if (Objects.equals(managerBrId, "018900") || Objects.equals(managerBrId, "019200")) {
                        docArchiveClientDto.setManagerBrId("018800");
                    } else if (Objects.equals(managerBrId, "019100") || Objects.equals(managerBrId, "019300")) {
                        docArchiveClientDto.setManagerBrId("019000");
                    } else if (Objects.equals(managerBrId, "017300")) {
                        docArchiveClientDto.setManagerBrId("017100");
                    } else if (Objects.equals(managerBrId, "018000") || Objects.equals(managerBrId, "017800")) {
                        docArchiveClientDto.setManagerBrId("017900");
                    }
                }

                // 档案归档信息
                DocArchiveInfo docArchiveInfo = new DocArchiveInfo();
                BeanUtils.copyProperties(docArchiveClientDto, docArchiveInfo);
                docArchiveInfo.setDocSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_GD_SEQ, new HashMap()));// 档案流水号
                docArchiveInfo.setArchiveMode(archiveMode);// 归档模式 01异地分支机构归档;02本地集中归档;03本地分支机构归档
                docArchiveInfo.setTaskFlag("01");// 任务标识 01系统发起;02人工发起
                docArchiveInfo.setDocStauts("01");// 档案状态 STD_DOC_STAUTS 01:等待入库
                docArchiveInfo.setCreateDate(openDay);// 生成日期
                docArchiveInfo.setInputDate(openDay);// 登记日期
                docArchiveInfo.setCreateTime(DateUtils.getCurrDate());// 创建时间
                // 根据档案类型生成资料清单
                docArchiveMaterListService.insertMaterList(docArchiveInfo);
                // 生成档案台账
                DocAccList docAccList = new DocAccList();
                BeanUtils.copyProperties(docArchiveInfo, docAccList);
                docAccListService.insertSelective(docAccList);
                num = docArchiveInfoMapper.insertSelective(docArchiveInfo);

                // 出账阶段(房抵e点贷、省心快贷)作废等待入库的合同归档任务和台账
                PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(docArchiveInfo.getBizSerno());
                if (null != pvpLoanApp) {
                    String prdTypeProp = pvpLoanApp.getPrdTypeProp();
                    if (Objects.equals("19", docArchiveInfo.getDocType()) || Objects.equals("P034", prdTypeProp)) {
                        // 19省心快贷 P034房抵e点贷
                        Map<String, String> docMap = new HashMap<>();
                        docMap.put("contNo", docArchiveInfo.getContNo());
                        docMap.put("bizSerno", docArchiveInfo.getBizSerno());
                        docArchiveInfoMapper.invalidContDocInfo(docMap);
                        docArchiveInfoMapper.invalidContDocAcc(docMap);
                    }
                }
                // 银承
                if (Objects.equals(docArchiveInfo.getDocType(), "14")) {
                    PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(docArchiveInfo.getBizSerno());
                    if (null != pvpAccpApp && !Objects.equals(pvpAccpApp.getIsSxep(), "1")) {
                        // 1、出账申请通过后，生成归档任务；
                        // 2、若合同归档任务未完成，则删除合同类归档任务，并生成放款类资料归档任务，若已经完成合同类归档任务，生成放款类资料归档任务。
                        Map<String, String> docMap = new HashMap<>();
                        docMap.put("contNo", docArchiveInfo.getContNo());
                        docMap.put("bizSerno", docArchiveInfo.getBizSerno());
                        docArchiveInfoMapper.invalidContDocInfo(docMap);
                        docArchiveInfoMapper.invalidContDocAcc(docMap);
                    }
                }
            }
        } catch (Exception e) {
            num = 0;
            logger.error("档案归档任务系统生成异常，业务流水号：[{}]，异常原因：[{}]", docArchiveClientDto.getBizSerno(), e.getMessage());
        }
        return num;
    }

    /**
     * @方法名称: selectByDocNo
     * @方法描述: 条件查询 - 根据档案编号号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<DocArchiveInfo> selectByDocNo(String docNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("docNo", docNo);
        List<DocArchiveInfo> list = docArchiveInfoMapper.selectByModel(queryModel);
        return list;
    }

    /**
     * 同步档案状态
     *
     * @param docNo
     */
    public void synStatus(String docNo, String status) {
        // 申请人，申请机构，创建时间
        String updId = "", updBrId = "", updDate = "";
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            // 申请人
            updId = userInfo.getLoginCode();
            // 申请机构
            updBrId = userInfo.getOrg().getCode();
            // 申请时间
            updDate = openDay;
        }
        // 更新档案归档状态
        List<DocArchiveInfo> docArchiveInfos = selectByDocNo(docNo);
        if (Objects.nonNull(docArchiveInfos) && !docArchiveInfos.isEmpty()) {
            DocArchiveInfo docArchiveInfo = docArchiveInfos.get(0);
            docArchiveInfo.setDocStauts(status);
            docArchiveInfo.setUpdId(updId);
            docArchiveInfo.setUpdBrId(updBrId);
            docArchiveInfo.setUpdDate(updDate);
            if (Objects.equals("03", status)) {
                docArchiveInfo.setFinishDate(openDay);
            }
            docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);
        }
        // 更新档案台账状态
        List<DocAccList> docAccLists = docAccListService.selectByDocNo(docNo);
        if (Objects.nonNull(docAccLists) && !docAccLists.isEmpty()) {

            DocAccList docAccList = docAccLists.get(0);
            docAccList.setDocStauts(status);
            docAccList.setUpdId(updId);
            docAccList.setUpdBrId(updBrId);
            docAccList.setUpdDate(updDate);
            if (Objects.equals("03", status) && StringUtils.isEmpty(docAccList.getStorageOptDate())) {
                // 入库操作时间
                docAccList.setStorageOptDate(DateUtils.formatDate("yyyy-MM-dd"));
            }
            if (Objects.equals("03", status) && StringUtils.isEmpty(docAccList.getStorageCashDate())) {
                // 档案入现金库时间
                docAccList.setStorageCashDate(DateUtils.formatDate("yyyy-MM-dd"));
            }
            docAccListService.updateSelective(docAccList);
        }
    }

    /**
     * 作废"等待入库"状态的档案归档任务
     *
     * @author jijian_yx
     * @date 2021/7/22 16:03
     **/
    @Transactional(rollbackFor = Exception.class)
    public int invalidByBizSerno(String serno, String billNo) {
        int result = 0;
        QueryModel model = new QueryModel();
        try {
            // 关联业务编号
            model.addCondition("bizSerno", serno);// 关联流水号
            if (StringUtils.nonEmpty(billNo)) {
                model.addCondition("billNo", billNo);// 借据号
            }
            model.addCondition("docStauts", "01");// 等待入库
            List<DocArchiveInfo> docList = docArchiveInfoMapper.selectByModel(model);
            if (Objects.nonNull(docList) && docList.size() > 0) {
                DocArchiveInfo docArchiveInfo = docList.get(0);
                // 作废档案归档台账
                DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
                docAccList.setDocStauts("06");// 作废
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo != null) {
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    docAccList.setUpdId(userInfo.getLoginCode());// 更新人
                    docAccList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docAccList.setUpdDate(openDay);// 更新日期
                    docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                    docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docArchiveInfo.setUpdDate(openDay);// 更新日期
                }
                docAccListService.updateSelective(docAccList);
                // 作废档案归档任务
                docArchiveInfo.setDocStauts("06");// 作废
                result = docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);
                logger.info("等待入库状态档案归档任务作废成功,业务流水号[{}]", serno);
            }
        } catch (Exception e) {
            logger.error("等待入库状态档案归档任务作废异常,流水号[{}],原因:[{}]", serno, e.getMessage());
        }
        return result;
    }

    /**
     * 作废最高授信协议对应的合同影像审核生成的等待入库的归档任务
     *
     * @author jijian_yx
     * @date 2021/10/25 15:30
     **/
    public int invalidByContNo(String contNo) {
        int result = 0;
        QueryModel model = new QueryModel();
        try {
            // 关联业务编号
            model.addCondition("contNo", contNo);// 关联流水号
            model.addCondition("docStauts", "01");// 等待入库
            model.addCondition("docType", "05");// 对公及个人经营性贷款
            model.addCondition("docBizType", "09");// 最高额授信协议
            List<DocArchiveInfo> docList = docArchiveInfoMapper.selectByModel(model);
            if (Objects.nonNull(docList) && docList.size() > 0) {
                DocArchiveInfo docArchiveInfo = docList.get(0);
                // 作废档案归档台账
                DocAccList docAccList = docAccListService.selectByPrimaryKey(docArchiveInfo.getDocSerno());
                docAccList.setDocStauts("06");// 作废
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo != null) {
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    docAccList.setUpdId(userInfo.getLoginCode());// 更新人
                    docAccList.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docAccList.setUpdDate(openDay);// 更新日期
                    docArchiveInfo.setUpdId(userInfo.getLoginCode());// 更新人
                    docArchiveInfo.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                    docArchiveInfo.setUpdDate(openDay);// 更新日期
                }
                docAccListService.updateSelective(docAccList);
                // 作废档案归档任务
                docArchiveInfo.setDocStauts("06");// 作废
                result = docArchiveInfoMapper.updateByPrimaryKeySelective(docArchiveInfo);
            }
        } catch (Exception e) {
            logger.info("等待入库状态档案归档任务作废异常,原因:[{}]", e.getMessage());
        }
        return result;
    }

    /**
     * 根据业务流水号获取在途归档任务数
     *
     * @author jijian_yx
     * @date 2021/8/30 10:42
     **/
    public int queryDocTaskInWayCount(String bizSerno) {
        return docArchiveInfoMapper.queryDocTaskInWayCount(bizSerno);
    }

    /**
     * 获取档案池关联零售业务申请流水号
     *
     * @author jijian_yx
     * @date 2021/9/8 16:50
     **/
    public String getLSIqpserno(String bizSerno) {
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(bizSerno);
        return Optional.ofNullable(pvpLoanApp.getIqpSerno()).orElse(bizSerno);
    }

    /**
     * 根据入参获取档案归档任务数
     *
     * @author jijian_yx
     * @date 2021/9/11 0:09
     **/
    public Integer selectByBizSerno(Map<String, String> map) {
        return docArchiveInfoMapper.selectByBizSerno(map);
    }

    /**
     * 提交入库有未知事务问题,暂时在提交入库回调中处理更新失败的情况
     *
     * @author jijian_yx
     * @date 2021/9/27 20:00
     **/
    public int updateDocAfterOpt(String docSerno) {
        int i = 0;
        DocArchiveInfo docArchiveInfo = docArchiveInfoMapper.selectByPrimaryKey(docSerno);
        if (null != docArchiveInfo && Objects.equals(docArchiveInfo.getDocStauts(), "01") && StringUtils.isEmpty(docArchiveInfo.getDocNo())) {
            DocAccList docAccList = docAccListService.selectByPrimaryKey(docSerno);
            if (null != docAccList && Objects.equals(docAccList.getDocStauts(), "02") && StringUtils.nonEmpty(docAccList.getDocNo())) {
                docArchiveInfo.setDocNo(docAccList.getDocNo());
                docArchiveInfo.setDocStauts(docAccList.getDocStauts());// 入库状态：02入库在途
                docArchiveInfo.setStorageDate(docAccList.getStorageOptDate());// 入库操作时间
                docArchiveInfo.setOptUsr(docAccList.getOptUsr());// 入库操作人
                docArchiveInfo.setOptOrg(docAccList.getOptOrg());// 入库操作机构
                docArchiveInfo.setUpdId(docAccList.getUpdId());// 更新人
                docArchiveInfo.setUpdBrId(docAccList.getUpdBrId());// 更新机构
                docArchiveInfo.setUpdDate(docAccList.getUpdDate());// 更新日期
                i = updateSelective(docArchiveInfo);
                logger.info("档案归档入库提交归档数据更新异常,档案流水号[{}],重新更新结果[{}]", docSerno, i);
            }
        }
        return i;
    }
}
