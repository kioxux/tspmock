///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.web.rest;
//
//import java.net.URISyntaxException;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.SurveyConInfo;
//import cn.com.yusys.yusp.service.SurveyConInfoService;
//
///**
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyConInfoResource
// * @类描述: #资源类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 19:53:28
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/surveyconinfo")
//public class SurveyConInfoResource {
//    @Autowired
//    private SurveyConInfoService surveyConInfoService;
//
//	/**
//     * 全表查询.
//     *
//     * @return
//     */
//    @GetMapping("/query/all")
//    protected ResultDto<List<SurveyConInfo>> query() {
//        QueryModel queryModel = new QueryModel();
//        List<SurveyConInfo> list = surveyConInfoService.selectAll(queryModel);
//        return new ResultDto<List<SurveyConInfo>>(list);
//    }
//
//    /**
//     * @函数名称:index
//     * @函数描述:查询对象列表，公共API接口
//     * @参数与返回说明:
//     * @param queryModel
//     *            分页查询类
//     * @算法描述:
//     */
//    @GetMapping("/")
//    protected ResultDto<List<SurveyConInfo>> index(QueryModel queryModel) {
//        List<SurveyConInfo> list = surveyConInfoService.selectByModel(queryModel);
//        return new ResultDto<List<SurveyConInfo>>(list);
//    }
//
//    /**
//     * @函数名称:show
//     * @函数描述:查询单个对象，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @GetMapping("/{surveyNo}")
//    protected ResultDto<SurveyConInfo> show(@PathVariable("surveyNo") String surveyNo) {
//        SurveyConInfo surveyConInfo = surveyConInfoService.selectByPrimaryKey(surveyNo);
//        return new ResultDto<SurveyConInfo>(surveyConInfo);
//    }
//
//    /**
//     * @函数名称:create
//     * @函数描述:实体类创建，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/")
//    protected ResultDto<Integer> create(@RequestBody SurveyConInfo surveyConInfo) throws URISyntaxException {
//        int result = surveyConInfoService.insert(surveyConInfo);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:update
//     * @函数描述:对象修改，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/update")
//    protected ResultDto<Integer> update(@RequestBody SurveyConInfo surveyConInfo) throws URISyntaxException {
//        int result = surveyConInfoService.update(surveyConInfo);
//        return new ResultDto<Integer>(result);
//    }
//    /**
//     * @param surveyConInfo 调查结论表
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
//     * @author hubp
//     * @date 2021/4/16 14:22
//     * @version 1.0.0
//     * @desc 更新非空字段
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @PostMapping("/updateselective")
//    protected ResultDto<Integer> updateSelective(@RequestBody SurveyConInfo surveyConInfo) throws URISyntaxException {
//        int result = surveyConInfoService.updateSelective(surveyConInfo);
//        return new ResultDto<Integer>(result);
//    }
//
//
//    /**
//     * @函数名称:delete
//     * @函数描述:单个对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/delete/{surveyNo}")
//    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
//        int result = surveyConInfoService.deleteByPrimaryKey(surveyNo);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:batchdelete
//     * @函数描述:批量对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/batchdelete/{ids}")
//    protected ResultDto<Integer> deletes(@PathVariable String ids) {
//        int result = surveyConInfoService.deleteByIds(ids);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:batchdelete
//     * @函数描述:批量对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/saveSurveyConInfo/")
//    protected ResultDto<Integer> addSurveyConInfo(@RequestBody SurveyConInfo surveyConInfo) {
//        int result = surveyConInfoService.saveSurveyConInfo(surveyConInfo);
//        return new ResultDto<Integer>(result);
//    }
//}
