/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CoopPlanEspecQuotaCtrlDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanEspecQuotaCtrl;
import cn.com.yusys.yusp.service.CoopPlanEspecQuotaCtrlService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanEspecQuotaCtrlResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:08:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplanespecquotactrl")
public class CoopPlanEspecQuotaCtrlResource {
    @Autowired
    private CoopPlanEspecQuotaCtrlService coopPlanEspecQuotaCtrlService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanEspecQuotaCtrlDto>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanEspecQuotaCtrlDto> list = coopPlanEspecQuotaCtrlService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanEspecQuotaCtrlDto>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanEspecQuotaCtrlDto>> index(QueryModel queryModel) {
        List<CoopPlanEspecQuotaCtrlDto> list = coopPlanEspecQuotaCtrlService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanEspecQuotaCtrlDto>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPlanEspecQuotaCtrlDto>> query(@RequestBody QueryModel queryModel) {
        List<CoopPlanEspecQuotaCtrlDto> list = coopPlanEspecQuotaCtrlService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanEspecQuotaCtrlDto>>(list);
    }



    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CoopPlanEspecQuotaCtrl> show(@PathVariable("pkId") String pkId) {
        CoopPlanEspecQuotaCtrl coopPlanEspecQuotaCtrl = coopPlanEspecQuotaCtrlService.selectByPrimaryKey(pkId);
        return new ResultDto<CoopPlanEspecQuotaCtrl>(coopPlanEspecQuotaCtrl);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPlanEspecQuotaCtrl> create(@RequestBody CoopPlanEspecQuotaCtrl coopPlanEspecQuotaCtrl) throws URISyntaxException {
        coopPlanEspecQuotaCtrlService.insert(coopPlanEspecQuotaCtrl);
        return new ResultDto<CoopPlanEspecQuotaCtrl>(coopPlanEspecQuotaCtrl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPlanEspecQuotaCtrl coopPlanEspecQuotaCtrl) throws URISyntaxException {
        int result = coopPlanEspecQuotaCtrlService.update(coopPlanEspecQuotaCtrl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = coopPlanEspecQuotaCtrlService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanEspecQuotaCtrlService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody CoopPlanEspecQuotaCtrl coopPlanEspecQuotaCtrl) {
        int result = coopPlanEspecQuotaCtrlService.deleteBySerno(coopPlanEspecQuotaCtrl);
        return ResultDto.success(result);
    }

}
