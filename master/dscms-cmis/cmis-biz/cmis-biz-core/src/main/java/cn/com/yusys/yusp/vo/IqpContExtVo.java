package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.domain.IqpContExt;
import cn.com.yusys.yusp.domain.IqpContExtBill;

import java.util.List;

public class IqpContExtVo {

    private IqpContExt iqpContExt;

    private List<IqpContExtBill> billList;

    public IqpContExt getIqpContExt() {
        return iqpContExt;
    }

    public void setIqpContExt(IqpContExt iqpContExt) {
        this.iqpContExt = iqpContExt;
    }

    public List<IqpContExtBill> getBillList() {
        return billList;
    }

    public void setBillList(List<IqpContExtBill> billList) {
        this.billList = billList;
    }
}
