/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtSigInvestRiskAnly;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRiskAnlyMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRiskAnlyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 18:59:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRiskAnlyService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestRiskAnlyMapper lmtSigInvestRiskAnlyMapper;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRiskAnly selectByPrimaryKey(String pkId) {
        return lmtSigInvestRiskAnlyMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRiskAnly> selectAll(QueryModel model) {
        List<LmtSigInvestRiskAnly> records = (List<LmtSigInvestRiskAnly>) lmtSigInvestRiskAnlyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRiskAnly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRiskAnly> list = lmtSigInvestRiskAnlyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestRiskAnly record) {
        return lmtSigInvestRiskAnlyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRiskAnly record) {
        return lmtSigInvestRiskAnlyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRiskAnly record) {
        return lmtSigInvestRiskAnlyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRiskAnly record) {
        return lmtSigInvestRiskAnlyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRiskAnlyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRiskAnlyMapper.deleteByIds(ids);
    }

    /**
     * 根据serno获取对象
     * @param serno
     * @return
     */
    public LmtSigInvestRiskAnly selectBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.setSize(1);
        queryModel.setPage(1);
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestRiskAnly> lmtSigInvestRiskAnlies = selectByModel(queryModel);
        if (lmtSigInvestRiskAnlies!=null && lmtSigInvestRiskAnlies.size()>0){
            return lmtSigInvestRiskAnlies.get(0);
        }
        return null;
    }

    /**
     * 根据流水号新增对象
     * @param serno
     * @return
     */
    public int insertRiskAnly(String serno) {
        LmtSigInvestRiskAnly riskAnly = new LmtSigInvestRiskAnly();

        riskAnly.setSerno(serno);
        //初始化(新增)通用domain信息
        initInsertDomainProperties(riskAnly);
        return insert(riskAnly);
    }

    /**
     * 更新图片
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        LmtSigInvestRiskAnly lmtSigInvestRiskAnly = new LmtSigInvestRiskAnly();
        lmtSigInvestRiskAnly.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/"+getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)){
            picAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,fanruanFileTemplate);
        }
        setValByKey(lmtSigInvestRiskAnly,key,picAbsolutePath);
        updateSelective(lmtSigInvestRiskAnly);
        return picAbsolutePath;
    }

    /**
     * 保存主体分析
     * @param lmtSigInvestRiskAnly
     * @return
     */
    public int updateZtfx(LmtSigInvestRiskAnly lmtSigInvestRiskAnly) {
        String serno = lmtSigInvestRiskAnly.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"ztfx");
        return update(lmtSigInvestRiskAnly);
    }
}
