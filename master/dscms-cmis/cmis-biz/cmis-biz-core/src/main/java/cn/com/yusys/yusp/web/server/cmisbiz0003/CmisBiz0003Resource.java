package cn.com.yusys.yusp.web.server.cmisbiz0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.cmisbiz0003.CmisBiz0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:集团客户授信复审
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "CMISBIZ0003:根据机构号和业务条线获取额度")
@RestController
@RequestMapping("/api/biz4inner")
public class CmisBiz0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0003Resource.class);

    @Autowired
    private CmisBiz0003Service cmisBiz0003Service;

    /**
     * 交易码：cmisbiz0003
     * 交易描述：根据机构号和业务条线获取额度
     *
     * @param cmisbiz0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据机构号和业务条线获取额度")
    @PostMapping("/cmisbiz0003")
    protected @ResponseBody
    ResultDto<CmisBiz0003RespDto> cmisbiz0003(@Validated @RequestBody CmisBiz0003ReqDto cmisbiz0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value, JSON.toJSONString(cmisbiz0003ReqDto));
        CmisBiz0003RespDto cmisbiz0003RespDto = new CmisBiz0003RespDto();// 响应Dto:集团客户授信复审
        ResultDto<CmisBiz0003RespDto> cmisbiz0003ResultDto = new ResultDto<>();
        try {
            LmtGrpApp lmtGrpApp = new LmtGrpApp();
            BeanUtils.copyProperties(cmisbiz0003ReqDto, lmtGrpApp);

            // 从cmisbiz0003ReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value, JSON.toJSONString(cmisbiz0003ReqDto));
            cmisbiz0003RespDto = cmisBiz0003Service.execute(cmisbiz0003ReqDto);
            cmisbiz0003ResultDto.setData(cmisbiz0003RespDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value, JSON.toJSONString(cmisbiz0003RespDto));
            // 封装cmisbiz0003ResultDto中正确的返回码和返回信息
            cmisbiz0003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisbiz0003ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value, e.getMessage());
            // 封装cmisbiz0003ResultDto中异常返回码和返回信息
            cmisbiz0003ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisbiz0003ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisbiz0003RespDto到cmisbiz0003ResultDto中
        cmisbiz0003ResultDto.setData(cmisbiz0003RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value, JSON.toJSONString(cmisbiz0003ResultDto));
        return cmisbiz0003ResultDto;
    }
}