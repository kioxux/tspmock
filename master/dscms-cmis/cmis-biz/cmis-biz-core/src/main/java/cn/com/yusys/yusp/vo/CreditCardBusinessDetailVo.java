package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;

import java.math.BigDecimal;

public class CreditCardBusinessDetailVo extends PageQuery {

    /** 账号 **/
    private String cardNo;

    /** 账户类型 **/
    private String cardType;

    /** 交易日期 **/
    private String tndate;

    /** 受理机构名称地址 **/
    private String aqnead;

    /** 交易码 **/
    private String tncode;

    /** 交易币种代码 **/
    private String tncrcd;

    /** 借贷标志 **/
    private String dbcrid;

    /** 交易金额 **/
    private BigDecimal txnamt;

    /** 账单交易描述 **/
    private String tnstds;

    /** 入账日期 **/
    private String ptdate;

    /** 入账币种 **/
    private String ptcrcd;

    /** 入账币种金额 **/
    private BigDecimal pstamt;

    /** 资金来源 **/
    private String cupsou;

    /** 付款方账号 **/
    private String cuptno;

    /** 付款方卡号 **/
    private String cupdno;

    /** 付款方名称 **/
    private String cupame;

    /** 附言 **/
    private String cupent;

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getTndate() {
        return tndate;
    }

    public void setTndate(String tndate) {
        this.tndate = tndate;
    }

    public String getAqnead() {
        return aqnead;
    }

    public void setAqnead(String aqnead) {
        this.aqnead = aqnead;
    }

    public String getTncode() {
        return tncode;
    }

    public void setTncode(String tncode) {
        this.tncode = tncode;
    }

    public String getTncrcd() {
        return tncrcd;
    }

    public void setTncrcd(String tncrcd) {
        this.tncrcd = tncrcd;
    }

    public String getDbcrid() {
        return dbcrid;
    }

    public void setDbcrid(String dbcrid) {
        this.dbcrid = dbcrid;
    }

    public BigDecimal getTxnamt() {
        return txnamt;
    }

    public void setTxnamt(BigDecimal txnamt) {
        this.txnamt = txnamt;
    }

    public String getTnstds() {
        return tnstds;
    }

    public void setTnstds(String tnstds) {
        this.tnstds = tnstds;
    }

    public String getPtdate() {
        return ptdate;
    }

    public void setPtdate(String ptdate) {
        this.ptdate = ptdate;
    }

    public String getPtcrcd() {
        return ptcrcd;
    }

    public void setPtcrcd(String ptcrcd) {
        this.ptcrcd = ptcrcd;
    }

    public BigDecimal getPstamt() {
        return pstamt;
    }

    public void setPstamt(BigDecimal pstamt) {
        this.pstamt = pstamt;
    }

    public String getCupsou() {
        return cupsou;
    }

    public void setCupsou(String cupsou) {
        this.cupsou = cupsou;
    }

    public String getCuptno() {
        return cuptno;
    }

    public void setCuptno(String cuptno) {
        this.cuptno = cuptno;
    }

    public String getCupdno() {
        return cupdno;
    }

    public void setCupdno(String cupdno) {
        this.cupdno = cupdno;
    }

    public String getCupame() {
        return cupame;
    }

    public void setCupame(String cupame) {
        this.cupame = cupame;
    }

    public String getCupent() {
        return cupent;
    }

    public void setCupent(String cupent) {
        this.cupent = cupent;
    }
}
