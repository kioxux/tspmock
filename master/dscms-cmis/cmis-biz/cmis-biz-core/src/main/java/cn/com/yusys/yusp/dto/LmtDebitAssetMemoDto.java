package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitAssetMemo
 * @类描述: lmt_debit_asset_memo数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-26 14:27:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtDebitAssetMemoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String serno;
	
	/** 调查编号 **/
	private String surveySerno;
	
	/** 资产性质 **/
	private String assetCha;
	
	/** 资产说明 **/
	private String assetExpl;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param assetCha
	 */
	public void setAssetCha(String assetCha) {
		this.assetCha = assetCha == null ? null : assetCha.trim();
	}
	
    /**
     * @return AssetCha
     */	
	public String getAssetCha() {
		return this.assetCha;
	}
	
	/**
	 * @param assetExpl
	 */
	public void setAssetExpl(String assetExpl) {
		this.assetExpl = assetExpl == null ? null : assetExpl.trim();
	}
	
    /**
     * @return AssetExpl
     */	
	public String getAssetExpl() {
		return this.assetExpl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}