/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydHkPlanTmp
 * @类描述: tmp_wyd_hk_plan数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_hk_plan_tmp")
public class TmpWydHkPlanTmp {
	
	/** 借据号 **/
	@Id
	@Column(name = "LENDING_REF")
	private String lendingRef;
	
	/** 当前期数 **/
	@Id
	@Column(name = "TERM")
	private String term;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 当期本金到期日期 **/
	@Column(name = "P_MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String pMaturityDate;
	
	/** 当期应还本金 **/
	@Column(name = "P_REPAY", unique = false, nullable = true, length = 20)
	private String pRepay;
	
	/** 当期实还本金 **/
	@Column(name = "P_REPAY_ACT", unique = false, nullable = true, length = 20)
	private String pRepayAct;
	
	/** 当期利息到期日期 **/
	@Column(name = "I_MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String iMaturityDate;
	
	/** 当期应还总利息 **/
	@Column(name = "I_REPAY", unique = false, nullable = true, length = 20)
	private String iRepay;
	
	/** 当期实还利息 **/
	@Column(name = "I_REPAY_ACT", unique = false, nullable = true, length = 20)
	private String iRepayAct;
	
	/** 逾期金额 **/
	@Column(name = "P_OVERDUE_AMT", unique = false, nullable = true, length = 20)
	private String pOverdueAmt;
	
	/** 剩余期限-月 **/
	@Column(name = "REMAINING_MATURITY_M", unique = false, nullable = true, length = 5)
	private String remainingMaturityM;
	
	/** 剩余期限_日 **/
	@Column(name = "REMAINING_MATURITY_D", unique = false, nullable = true, length = 5)
	private String remainingMaturityD;
	
	/** 下一利息收付剩余期限_月 **/
	@Column(name = "REMAINING_MATURITY_MI", unique = false, nullable = true, length = 5)
	private String remainingMaturityMi;
	
	/** 下一利息收付剩余期限_日 **/
	@Column(name = "REMAINING_MATURITY_DI", unique = false, nullable = true, length = 5)
	private String remainingMaturityDi;
	
	/** 还款计划操作动作 **/
	@Column(name = "SCHEDULE_ACTION", unique = false, nullable = true, length = 10)
	private String scheduleAction;

	/** 保险代偿标志(y-是空为否) **/
	@Column(name = "INSURANCE_PAYMENT_FLAG", unique = false, nullable = true, length = 1)
	private String insurancePaymentFlag;
	
	/** 保险代偿日期 **/
	@Column(name = "INSURANCE_PAYMENT_DATE", unique = false, nullable = true, length = 10)
	private String insurancePaymentDate;
	
	/** 顺延日期 **/
	@Column(name = "INTEDATE", unique = false, nullable = true, length = 10)
	private String intedate;
	
	/** 本金提前还款金额 **/
	@Column(name = "P_REPAY_ADV", unique = false, nullable = true, length = 15)
	private String pRepayAdv;
	
	/** 递延利息 **/
	@Column(name = "DELAY_INTEREST", unique = false, nullable = true, length = 20)
	private String delayInterest;
	
	/** 应还利息 **/
	@Column(name = "PAYINTERESTAMT", unique = false, nullable = true, length = 20)
	private String payinterestamt;
	
	/** 应还本金罚息 **/
	@Column(name = "PAYPRINCIPALPENALTYAMT", unique = false, nullable = true, length = 20)
	private String payprincipalpenaltyamt;
	
	/** 应还利息罚息 **/
	@Column(name = "PAYINTERESTPENALTYAMT", unique = false, nullable = true, length = 20)
	private String payinterestpenaltyamt;
	
	/** 实还利息 **/
	@Column(name = "ACTUALPAYINTERESTAMT", unique = false, nullable = true, length = 20)
	private String actualpayinterestamt;
	
	/** 实还本金罚息 **/
	@Column(name = "ACTUALPAYPRINCIPALPENALTYAMT", unique = false, nullable = true, length = 20)
	private String actualpayprincipalpenaltyamt;
	
	/** 实还利息罚息 **/
	@Column(name = "ACTUALPAYINTERESTPENALTYAMT", unique = false, nullable = true, length = 20)
	private String actualpayinterestpenaltyamt;

	/** 本金状态 **/
	@Column(name = "P_STATUS", unique = false, nullable = true, length = 2)
	private String pStatus;

	/** 本期状态 **/
	@Column(name = "D_STATUS", unique = false, nullable = true, length = 2)
	private String dStatus;

	/** 当期结清日期 **/
	@Column(name = "FINISHDATE", unique = false, nullable = true, length = 10)
	private String finishdate;

	/** 减免本金 **/
	@Column(name = "WAIVEPRINCIPALAMT", unique = false, nullable = true, length = 20)
	private String waiveprincipalamt;

	/** 减免利息 **/
	@Column(name = "WAIVEINTERESTAMT", unique = false, nullable = true, length = 20)
	private String waiveinterestamt;

	/** 减免罚息 **/
	@Column(name = "WAIVEPENALTYAMT", unique = false, nullable = true, length = 20)
	private String waivepenaltyamt;
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(String term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public String getTerm() {
		return this.term;
	}
	
	/**
	 * @param pMaturityDate
	 */
	public void setPMaturityDate(String pMaturityDate) {
		this.pMaturityDate = pMaturityDate;
	}
	
    /**
     * @return pMaturityDate
     */
	public String getPMaturityDate() {
		return this.pMaturityDate;
	}
	
	/**
	 * @param pRepay
	 */
	public void setPRepay(String pRepay) {
		this.pRepay = pRepay;
	}
	
    /**
     * @return pRepay
     */
	public String getPRepay() {
		return this.pRepay;
	}
	
	/**
	 * @param pRepayAct
	 */
	public void setPRepayAct(String pRepayAct) {
		this.pRepayAct = pRepayAct;
	}
	
    /**
     * @return pRepayAct
     */
	public String getPRepayAct() {
		return this.pRepayAct;
	}
	
	/**
	 * @param iMaturityDate
	 */
	public void setIMaturityDate(String iMaturityDate) {
		this.iMaturityDate = iMaturityDate;
	}
	
    /**
     * @return iMaturityDate
     */
	public String getIMaturityDate() {
		return this.iMaturityDate;
	}
	
	/**
	 * @param iRepay
	 */
	public void setIRepay(String iRepay) {
		this.iRepay = iRepay;
	}
	
    /**
     * @return iRepay
     */
	public String getIRepay() {
		return this.iRepay;
	}
	
	/**
	 * @param iRepayAct
	 */
	public void setIRepayAct(String iRepayAct) {
		this.iRepayAct = iRepayAct;
	}
	
    /**
     * @return iRepayAct
     */
	public String getIRepayAct() {
		return this.iRepayAct;
	}
	
	/**
	 * @param pOverdueAmt
	 */
	public void setPOverdueAmt(String pOverdueAmt) {
		this.pOverdueAmt = pOverdueAmt;
	}
	
    /**
     * @return pOverdueAmt
     */
	public String getPOverdueAmt() {
		return this.pOverdueAmt;
	}
	
	/**
	 * @param remainingMaturityM
	 */
	public void setRemainingMaturityM(String remainingMaturityM) {
		this.remainingMaturityM = remainingMaturityM;
	}
	
    /**
     * @return remainingMaturityM
     */
	public String getRemainingMaturityM() {
		return this.remainingMaturityM;
	}
	
	/**
	 * @param remainingMaturityD
	 */
	public void setRemainingMaturityD(String remainingMaturityD) {
		this.remainingMaturityD = remainingMaturityD;
	}
	
    /**
     * @return remainingMaturityD
     */
	public String getRemainingMaturityD() {
		return this.remainingMaturityD;
	}
	
	/**
	 * @param remainingMaturityMi
	 */
	public void setRemainingMaturityMi(String remainingMaturityMi) {
		this.remainingMaturityMi = remainingMaturityMi;
	}
	
    /**
     * @return remainingMaturityMi
     */
	public String getRemainingMaturityMi() {
		return this.remainingMaturityMi;
	}
	
	/**
	 * @param remainingMaturityDi
	 */
	public void setRemainingMaturityDi(String remainingMaturityDi) {
		this.remainingMaturityDi = remainingMaturityDi;
	}
	
    /**
     * @return remainingMaturityDi
     */
	public String getRemainingMaturityDi() {
		return this.remainingMaturityDi;
	}
	
	/**
	 * @param scheduleAction
	 */
	public void setScheduleAction(String scheduleAction) {
		this.scheduleAction = scheduleAction;
	}
	
    /**
     * @return scheduleAction
     */
	public String getScheduleAction() {
		return this.scheduleAction;
	}
	
	/**
	 * @param insurancePaymentFlag
	 */
	public void setInsurancePaymentFlag(String insurancePaymentFlag) {
		this.insurancePaymentFlag = insurancePaymentFlag;
	}
	
    /**
     * @return insurancePaymentFlag
     */
	public String getInsurancePaymentFlag() {
		return this.insurancePaymentFlag;
	}
	
	/**
	 * @param insurancePaymentDate
	 */
	public void setInsurancePaymentDate(String insurancePaymentDate) {
		this.insurancePaymentDate = insurancePaymentDate;
	}
	
    /**
     * @return insurancePaymentDate
     */
	public String getInsurancePaymentDate() {
		return this.insurancePaymentDate;
	}
	
	/**
	 * @param intedate
	 */
	public void setIntedate(String intedate) {
		this.intedate = intedate;
	}
	
    /**
     * @return intedate
     */
	public String getIntedate() {
		return this.intedate;
	}
	
	/**
	 * @param pRepayAdv
	 */
	public void setPRepayAdv(String pRepayAdv) {
		this.pRepayAdv = pRepayAdv;
	}
	
    /**
     * @return pRepayAdv
     */
	public String getPRepayAdv() {
		return this.pRepayAdv;
	}
	
	/**
	 * @param delayInterest
	 */
	public void setDelayInterest(String delayInterest) {
		this.delayInterest = delayInterest;
	}
	
    /**
     * @return delayInterest
     */
	public String getDelayInterest() {
		return this.delayInterest;
	}
	
	/**
	 * @param payinterestamt
	 */
	public void setPayinterestamt(String payinterestamt) {
		this.payinterestamt = payinterestamt;
	}
	
    /**
     * @return payinterestamt
     */
	public String getPayinterestamt() {
		return this.payinterestamt;
	}
	
	/**
	 * @param payprincipalpenaltyamt
	 */
	public void setPayprincipalpenaltyamt(String payprincipalpenaltyamt) {
		this.payprincipalpenaltyamt = payprincipalpenaltyamt;
	}
	
    /**
     * @return payprincipalpenaltyamt
     */
	public String getPayprincipalpenaltyamt() {
		return this.payprincipalpenaltyamt;
	}
	
	/**
	 * @param payinterestpenaltyamt
	 */
	public void setPayinterestpenaltyamt(String payinterestpenaltyamt) {
		this.payinterestpenaltyamt = payinterestpenaltyamt;
	}
	
    /**
     * @return payinterestpenaltyamt
     */
	public String getPayinterestpenaltyamt() {
		return this.payinterestpenaltyamt;
	}
	
	/**
	 * @param actualpayinterestamt
	 */
	public void setActualpayinterestamt(String actualpayinterestamt) {
		this.actualpayinterestamt = actualpayinterestamt;
	}
	
    /**
     * @return actualpayinterestamt
     */
	public String getActualpayinterestamt() {
		return this.actualpayinterestamt;
	}
	
	/**
	 * @param actualpayprincipalpenaltyamt
	 */
	public void setActualpayprincipalpenaltyamt(String actualpayprincipalpenaltyamt) {
		this.actualpayprincipalpenaltyamt = actualpayprincipalpenaltyamt;
	}
	
    /**
     * @return actualpayprincipalpenaltyamt
     */
	public String getActualpayprincipalpenaltyamt() {
		return this.actualpayprincipalpenaltyamt;
	}
	
	/**
	 * @param actualpayinterestpenaltyamt
	 */
	public void setActualpayinterestpenaltyamt(String actualpayinterestpenaltyamt) {
		this.actualpayinterestpenaltyamt = actualpayinterestpenaltyamt;
	}
	
    /**
     * @return actualpayinterestpenaltyamt
     */
	public String getActualpayinterestpenaltyamt() {
		return this.actualpayinterestpenaltyamt;
	}

	public String getpStatus() {
		return pStatus;
	}

	public void setpStatus(String pStatus) {
		this.pStatus = pStatus;
	}

	public String getdStatus() {
		return dStatus;
	}

	public void setdStatus(String dStatus) {
		this.dStatus = dStatus;
	}

	public String getFinishdate() {
		return finishdate;
	}

	public void setFinishdate(String finishdate) {
		this.finishdate = finishdate;
	}

	public String getWaiveprincipalamt() {
		return waiveprincipalamt;
	}

	public void setWaiveprincipalamt(String waiveprincipalamt) {
		this.waiveprincipalamt = waiveprincipalamt;
	}

	public String getWaiveinterestamt() {
		return waiveinterestamt;
	}

	public void setWaiveinterestamt(String waiveinterestamt) {
		this.waiveinterestamt = waiveinterestamt;
	}

	public String getWaivepenaltyamt() {
		return waivepenaltyamt;
	}

	public void setWaivepenaltyamt(String waivepenaltyamt) {
		this.waivepenaltyamt = waivepenaltyamt;
	}
}