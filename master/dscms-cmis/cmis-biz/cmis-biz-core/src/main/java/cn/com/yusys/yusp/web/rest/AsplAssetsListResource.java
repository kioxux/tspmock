/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AsplIoPool;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.service.AsplAssetsListService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAssetsListResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:45:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "客户资产清单")
@RequestMapping("/api/asplassetslist")
public class AsplAssetsListResource {
    @Autowired
    private AsplAssetsListService asplAssetsListService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplAssetsList>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplAssetsList> list = asplAssetsListService.selectAll(queryModel);
        return new ResultDto<List<AsplAssetsList>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplAssetsList>> index(QueryModel queryModel) {
        List<AsplAssetsList> list = asplAssetsListService.selectByModel(queryModel);
        return new ResultDto<List<AsplAssetsList>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplAssetsList> show(@PathVariable("pkId") String pkId) {
        AsplAssetsList asplAssetsList = asplAssetsListService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplAssetsList>(asplAssetsList);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplAssetsList> create(@RequestBody AsplAssetsList asplAssetsList) throws URISyntaxException {
        asplAssetsListService.insert(asplAssetsList);
        return new ResultDto<AsplAssetsList>(asplAssetsList);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplAssetsList asplAssetsList) throws URISyntaxException {
        int result = asplAssetsListService.update(asplAssetsList);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplAssetsListService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplAssetsListService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:asplList
     * @函数描述:资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产清单")
    @PostMapping("/asplassetslist")
    protected ResultDto<List<AsplAssetsList>> asplAssetsList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<AsplAssetsList> list = asplAssetsListService.asplAssetsList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAssetsList>>(list);
    }

    /**
     * @函数名称:inPoolAssetsList
     * @函数描述:池内资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("池内资产清单")
    @PostMapping("/inpoolassetslist")
    protected ResultDto<List<AsplAssetsList>> inPoolAssetsList(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 是否入池
        queryModel.addCondition("isPool", CmisCommonConstants.STD_ZB_YES_NO_1);
        // 是否质押
        queryModel.addCondition("isPledge", CmisCommonConstants.STD_ZB_YES_NO_1);
        List<AsplAssetsList> list = asplAssetsListService.asplAssetsList(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAssetsList>>(list);
    }
    /**
     * @函数名称:outPoolAssetsList
     * @函数描述:可出池资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("可出池资产清单")
    @PostMapping("/outPoolAssetsList")
    protected ResultDto<List<AsplAssetsList>> outPoolAssetsList(@RequestBody QueryModel queryModel) {
        return asplAssetsListService.outPoolAssetsList(queryModel);
    }

    /**
     * @函数名称:inPoolAssetsList
     * @函数描述:池内资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("池内资产清单台账")
    @PostMapping("/inpoolassetslistall")
    protected ResultDto<List<AsplAssetsList>> inPoolAssetsListAll(@RequestBody QueryModel queryModel) {
        queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
//        queryModel.addCondition("isPool", CmisCommonConstants.STD_ZB_YES_NO_1);// 是否入池
//        queryModel.addCondition("isPledge", CmisCommonConstants.STD_ZB_YES_NO_1);// 是否质押
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<AsplAssetsList> list = asplAssetsListService.inPoolAssetsListAll(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<AsplAssetsList>>(list);
    }
    /**
     * @函数名称:doSynchronize
     * @函数描述:可出池资产清单
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("可出池资产清单")
    @PostMapping("/doSynchronize")
    protected ResultDto<Map<String,Object>> doSynchronize(@RequestBody AsplAssetsList asplAssetsList ) {
        return asplAssetsListService.doSynchronize(asplAssetsList);
    }
}
