/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.dto.AsplAccpDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccpTaskMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:19:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AsplAccpTaskMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AsplAccpTask selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AsplAccpTask> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AsplAccpTask record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AsplAccpTask record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AsplAccpTask record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AsplAccpTask record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByParam
     * @方法描述: 根据参数查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    AsplAccpTask selectByParam(HashMap map);

    /**
     * @方法名称: selectInfoByCondition
     * @方法描述: 根据灵活参数查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AsplAccpTask> selectInfoByCondition(QueryModel model);

    /**
     * @方法名称: updateInfoByTContNo
     * @方法描述: 根据贸易合同编号更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    // TODO 有问题
    int updateEmptyByTContNo(@Param("tcontNo")String tContNO);

    /**
     * @方法名称: updateBillImgId
     * @方法描述: 根据银承流水更新补录状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateIsAddBill(@Param("pvpSerno")String pvpSerno);

    /**
     * @方法名称: xdzc0024
     * @方法描述: 根据银承编号并联查询 资产池贸易背景收集任务表 和 银承台账票据明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.xdzc0024.resp.List> xdzc0024(QueryModel model);

    /**
     * @函数名称: asplaccpchecklist
     * @函数描述: 贸易背景资料审核
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<AsplAccpTask> asplAccpTasklist(QueryModel queryModel);

    /**
     * 根据任务编号删除
     * @param taskId
     * @return
     */
    int deletebytaskid(@Param("taskId")String taskId);

    /**
     * 根据任务编号查询
     * @param taskId
     * @return
     */
    AsplAccpTask selectByTaskId(@Param("taskId")String taskId);

    int updateByTaskId(@Param("approveStatus")String wfStatus,@Param("taskId") String taskId);

    /**
     *  根据出账流水号查询
     * @param pvpSerno
     * @return
     */
    List<AsplAccpTask> selectByPvpserno(@Param("pvpSerno") String pvpSerno);
}