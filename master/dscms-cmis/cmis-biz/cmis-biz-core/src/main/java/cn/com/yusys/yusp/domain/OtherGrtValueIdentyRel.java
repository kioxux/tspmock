/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueIdentyRel
 * @类描述: other_grt_value_identy_rel数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-10 20:39:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_grt_value_identy_rel")
public class OtherGrtValueIdentyRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;
	
	/** 房屋坐落位置 **/
	@Column(name = "HOUSE_PLACE", unique = false, nullable = true, length = 500)
	private String housePlace;
	
	/** 房屋建筑面积 **/
	@Column(name = "HOUSE_BUILD_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseBuildSqu;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_UTIL_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landUtilSqu;
	
	/** 房产类型 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 10)
	private String houseType;
	
	/** 土地用途 **/
	@Column(name = "LAND_USE", unique = false, nullable = true, length = 5)
	private String landUse;
	
	/** 土地使用权性质 **/
	@Column(name = "LAND_USE_CHA", unique = false, nullable = true, length = 5)
	private String landUseCha;
	
	/** 评估来源 **/
	@Column(name = "EVAL_SOURCE", unique = false, nullable = true, length = 5)
	private String evalSource;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param housePlace
	 */
	public void setHousePlace(String housePlace) {
		this.housePlace = housePlace;
	}
	
    /**
     * @return housePlace
     */
	public String getHousePlace() {
		return this.housePlace;
	}
	
	/**
	 * @param houseBuildSqu
	 */
	public void setHouseBuildSqu(java.math.BigDecimal houseBuildSqu) {
		this.houseBuildSqu = houseBuildSqu;
	}
	
    /**
     * @return houseBuildSqu
     */
	public java.math.BigDecimal getHouseBuildSqu() {
		return this.houseBuildSqu;
	}
	
	/**
	 * @param landUtilSqu
	 */
	public void setLandUtilSqu(java.math.BigDecimal landUtilSqu) {
		this.landUtilSqu = landUtilSqu;
	}
	
    /**
     * @return landUtilSqu
     */
	public java.math.BigDecimal getLandUtilSqu() {
		return this.landUtilSqu;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param landUse
	 */
	public void setLandUse(String landUse) {
		this.landUse = landUse;
	}
	
    /**
     * @return landUse
     */
	public String getLandUse() {
		return this.landUse;
	}
	
	/**
	 * @param landUseCha
	 */
	public void setLandUseCha(String landUseCha) {
		this.landUseCha = landUseCha;
	}
	
    /**
     * @return landUseCha
     */
	public String getLandUseCha() {
		return this.landUseCha;
	}
	
	/**
	 * @param evalSource
	 */
	public void setEvalSource(String evalSource) {
		this.evalSource = evalSource;
	}
	
    /**
     * @return evalSource
     */
	public String getEvalSource() {
		return this.evalSource;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}