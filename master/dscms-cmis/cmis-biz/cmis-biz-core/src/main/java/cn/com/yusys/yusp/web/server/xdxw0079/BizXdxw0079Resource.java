package cn.com.yusys.yusp.web.server.xdxw0079;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0079.req.Xdxw0079DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0079.resp.Xdxw0079DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0079.Xdxw0079Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

/**
 * 接口处理类:勘验任务状态同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0079:勘验任务状态同步")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0079Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0079Resource.class);

    @Autowired
    private Xdxw0079Service xdxw0079Service;

    /**
     * 交易码：xdxw0079
     * 交易描述：勘验任务状态同步
     *
     * @param xdxw0079DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("勘验任务状态同步")
    @PostMapping("/xdxw0079")
    protected @ResponseBody
    ResultDto<Xdxw0079DataRespDto> xdxw0079(@Validated @RequestBody Xdxw0079DataReqDto xdxw0079DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataReqDto));
        Xdxw0079DataRespDto xdxw0079DataRespDto = new Xdxw0079DataRespDto();// 响应Dto:勘验任务状态同步
        ResultDto<Xdxw0079DataRespDto> xdxw0079DataResultDto = new ResultDto<>();
        try {
            //获取请求参数
            String type = xdxw0079DataReqDto.getType();//操作类型
            String certCode = xdxw0079DataReqDto.getCertCode();//证件号码
            List list = xdxw0079DataReqDto.getList();

            if (StringUtil.isEmpty(type)) {
                xdxw0079DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0079DataResultDto.setMessage("操作类型【type】不能为空！");
                return xdxw0079DataResultDto;
            } else if (("0".equals(type) || "1".equals(type))) {//0:校验白名单查询
                if (StringUtil.isEmpty(certCode)) {
                    xdxw0079DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdxw0079DataResultDto.setMessage("证件号码【certCode】不能为空！");
                    return xdxw0079DataResultDto;
                }
            } else if ("2".equals(type)) {
                if (CollectionUtils.isEmpty(list)) {
                    xdxw0079DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdxw0079DataResultDto.setMessage("参数【list】不能为空！");
                    return xdxw0079DataResultDto;
                }
            }

            // 从xdxw0079DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0079DataReqDto));
            xdxw0079DataRespDto = xdxw0079Service.xdxw0079(xdxw0079DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0079DataRespDto));

            if ("0".equals(type)) {//查询
                List<cn.com.yusys.yusp.dto.server.xdxw0079.resp.List> lmtRenewLoanAppInfoList = xdxw0079DataRespDto.getList();
                if (lmtRenewLoanAppInfoList.size() == 0) {
                    // 封装xdxw0079DataResultDto中正确的返回码和返回信息
                    xdxw0079DataResultDto.setCode("0001");
                    xdxw0079DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
                } else {
                    // 封装xdxw0079DataResultDto中正确的返回码和返回信息
                    xdxw0079DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                    xdxw0079DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
                }
            } else {
                // 封装xdxw0079DataResultDto中正确的返回码和返回信息
                xdxw0079DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0079DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, e.getMessage());
            // 封装xdxw0079DataResultDto中异常返回码和返回信息
            xdxw0079DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0079DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0079DataRespDto到xdxw0079DataResultDto中
        xdxw0079DataResultDto.setData(xdxw0079DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataResultDto));
        return xdxw0079DataResultDto;
    }
}
