package cn.com.yusys.yusp.web.server.xddb0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0013.req.Xddb0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0013.resp.Xddb0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0013.Xddb0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:抵押登记不动产登记证明入库
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0013:抵押登记不动产登记证明入库")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0013Resource.class);
	 @Autowired
	 private Xddb0013Service xddb0013Service;
    /**
     * 交易码：xddb0013
     * 交易描述：抵押登记不动产登记证明入库
     *
     * @param xddb0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记不动产登记证明入库")
    @PostMapping("/xddb0013")
    protected @ResponseBody
    ResultDto<Xddb0013DataRespDto> xddb0013(@Validated @RequestBody Xddb0013DataReqDto xddb0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataReqDto));
        Xddb0013DataRespDto xddb0013DataRespDto = new Xddb0013DataRespDto();// 响应Dto:抵押登记不动产登记证明入库
        ResultDto<Xddb0013DataRespDto> xddb0013DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataReqDto));
			xddb0013DataRespDto = xddb0013Service.xddb0013(xddb0013DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataRespDto));
            // 封装xddb0013DataResultDto中正确的返回码和返回信息
            xddb0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, e.getMessage());
            // 封装xddb0013DataResultDto中异常返回码和返回信息
            xddb0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0013DataRespDto到xddb0013DataResultDto中
        xddb0013DataResultDto.setData(xddb0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataRespDto));
        return xddb0013DataResultDto;
    }
}
