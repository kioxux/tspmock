package cn.com.yusys.yusp.constant;

/**
 * 合作方类型枚举
 */
public enum PartnerTypeEnums {
    PARTNER_TYPE_1("1", "房地产开发商"),
    PARTNER_TYPE_2("2", "专业担保公司"),
    PARTNER_TYPE_3("3", "保险公司"),
    PARTNER_TYPE_4("4", "集群贷市场方"),
    PARTNER_TYPE_5("5", "光伏设备生产企业"),
    PARTNER_TYPE_6("6", "旅游公司"),
    PARTNER_TYPE_7("7", "物联网动产贷交易平台归属方"),
    PARTNER_TYPE_8("8", "核心企业"),
    PARTNER_TYPE_9("9", "监管公司"),
    PARTNER_TYPE_10("10", "仓储公司"),
    PARTNER_TYPE_11("11", "押品评估机构"),
    PARTNER_TYPE_12("12", "网金业务合作方"),

    PARTNER_TYPE_13("13", "律师事务所"),
    PARTNER_TYPE_14("14", "其他合作方"),
    PARTNER_TYPE_15("15", "教育机构");

    private String value;
    private String desc;

    PartnerTypeEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

    public static String getDescByValue(String value){
        PartnerTypeEnums[]  partnerTypeEnums = PartnerTypeEnums.values();
        for (PartnerTypeEnums partnerTypeEnum : partnerTypeEnums) {
            if(partnerTypeEnum.getValue().equals(value)){
                return partnerTypeEnum.getDesc();
            }
        }
        return null;
    }
}
