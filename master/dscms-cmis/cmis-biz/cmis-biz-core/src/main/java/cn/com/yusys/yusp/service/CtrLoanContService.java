package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.req.CmisLmt0045ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0045.resp.CmisLmt0045RespDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.GuarContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.HxdLoanContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import cn.com.yusys.yusp.enums.common.MessageNoEnums;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.ypxt.buscon.BusconService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-09 16:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CtrLoanContService {

    private static final Logger log = LoggerFactory.getLogger(CtrLoanContService.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CtrLmtRelService ctrLmtRelService;
    @Autowired
    private CtrContNotarService ctrContNotarService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private IqpBillRelService iqpBillRelService;
    @Autowired
    private IqpLoanAppApprModeService iqpLoanAppApprModeService;
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;
    @Autowired
    private GrtGuarBizRstRelService getGrtGuarBizRstRelService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private IqpRepayPlanService iqpRepayPlanService;
    @Autowired
    private IqpLoanAppDramPlanSubService iqpLoanAppDramPlanSubService;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;//业务主表
    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private GrtGuarContService grtGuarContService;
    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private IqpAppReplyService iqpAppReplyService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private IqpDiscAppService iqpDiscAppService;
    @Autowired
    private CtrDiscContService ctrDiscContService;
    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;
    @Autowired
    private CtrCvrgContService ctrCvrgContService;
    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;
    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;
    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;
    @Autowired
    private CtrTfLocContService ctrTfLocContService;
    @Autowired
    private IqpAccpAppService iqpAccpAppService;
    @Autowired
    private CtrAccpContService ctrAccpContService;
    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;
    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;
    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private AreaAdminUserService areaAdminUserService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private GuarBusinessRelService guarBusinessRelService;
    @Autowired
    private BusconService busconService;
    @Autowired
    private GuarBizRelService guarBizRelService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RepayCapPlanService repayCapPlanService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;
    @Resource
    private ICusClientService icusClientService;
    @Autowired
    private LmtCobInfoService lmtCobInfoService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrLoanCont selectByPrimaryKey(String contNo) {
        return ctrLoanContMapper.selectByPrimaryKey(contNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CtrLoanCont> selectAll(QueryModel model) {
        List<CtrLoanCont> records = (List<CtrLoanCont>) ctrLoanContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrLoanCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanCont> list = null;
        if (null != model.getCondition().get("inputDate") && model.getCondition().get("inputDate") instanceof List) {
            List<String> datelist = (List<String>) model.getCondition().get("inputDate");
            model.getCondition().put("inputStartDate", datelist.get(0));
            model.getCondition().put("inputEndDate", datelist.get(1));
            list = ctrLoanContMapper.selectByModelCtr(model);
        } else {
            list = ctrLoanContMapper.selectByModel(model);
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param model
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author hubp
     * @date 2021/11/8 21:14
     * @version 1.0.0
     * @desc  小微专用-只查小微条线生效的合同
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanCont> selectByModelXw(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanCont> list = null ;
        // 查询当前登入人信息
        User userInfo = SessionUtils.getUserInformation();
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        String managerId = userInfo.getLoginCode();
        model.addCondition("managerId",managerId);
        model.addCondition("belgLine","01");
        model.setSort("contStartDate desc");
        userAndDutyReqDto.setManagerId(managerId);
        List<UserAndDutyRespDto> userAndDutyRespDtos = commonService.getUserAndDuty(userAndDutyReqDto);
        if (userAndDutyRespDtos.size() == 0) {
            throw BizException.error(null, EcbEnum.ECB010004.key, "该登陆用户未查询到岗位信息");
        }
        //查询当前登录人是否有小微内勤岗 岗位 dutyNo=  XWB13
        long xwb13 = userAndDutyRespDtos.stream().filter(s -> "XWB13".equals(s.getDutyNo())).count();
        if (xwb13 == 1) {
            log.info("***************小微内勤岗 查询所有同机构小微客户经理的数据*************");
            //TODO 子机构如何查询暂时不知道暂时根据登录人机构去查同机构的 那么就查当前登录机构下小微客户经理数据（先获取当前机构下用户，再根据角色筛选）
            model.getCondition().put("managerId", "");
            model.getCondition().put("managerBrId", "");
            // 2021年10月28日19:53:19 hubp 如果为小微内勤岗，，查inputBrId
            model.getCondition().put("inputBrId", userInfo.getOrg().getCode());
            //查询所有拥有小微客户经理角色的人
            PageHelper.startPage(model.getPage(), model.getSize());
            list = ctrLoanContMapper.selectByModel(model);
            PageHelper.clearPage();
            return list;
        }
        log.info("***************非小微内勤岗 查询当前登录人的批复数据*************");
        model.getCondition().put("managerBrId", "");
        list = ctrLoanContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectCtrLoanContListData
     * @方法描述: 重写列表查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrLoanCont> selectCtrLoanContListData(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("inputDate desc");
        model.addCondition("belgLine", CmisCommonConstants.STD_BELG_LINE_03);
        List<CtrLoanCont> list = ctrLoanContMapper.selectCtrLoanContListData(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectCtrLoanContListDataForZhcx
     * @方法描述: 普通贷款合同查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrLoanContForZhcxDto> selectCtrLoanContListDataForZhcx(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanContForZhcxDto> list = ctrLoanContMapper.selectCtrLoanContListDataForZhcx(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectCtrLoanContHisListData
     * @方法描述: 重写历史列表查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrLoanCont> selectCtrLoanContHisListData(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("inputDate desc");
        model.addCondition("belgLine", CmisCommonConstants.STD_BELG_LINE_03);
        List<CtrLoanCont> list = ctrLoanContMapper.selectCtrLoanContHisListData(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectWorryFreeLoanContinfo
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 根据合同号查询省心快贷查封查验中符合条件的合同
     */

    public List<CtrLoanCont> selectWorryFreeLoanContinfo(QueryModel model) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        model.addCondition("openday", openday);
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanCont> list = ctrLoanContMapper.selectWorryFreeLoanContinfo(model);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param params 合同号
     * @return cn.com.yusys.yusp.domain.CtrLoanCont
     * @author hubp
     * @date 2021/4/23 15:55
     * @version 1.0.0
     * @desc 通过合同号查找合同信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CtrLoanContEdit queryBycontNo(Map params) {
        String contNo = (String) params.get("contNo");
        CtrLoanContEdit ctrLoanContEdit = new CtrLoanContEdit();
        if (contNo == null || "".equals(contNo)) {
            return ctrLoanContEdit;
        } else {
            ctrLoanContEdit.setCtrLoanCont(ctrLoanContMapper.selectByPrimaryKey(contNo));
            ctrLoanContEdit.setIqpLoanAppDramPlanSubList(iqpLoanAppDramPlanSubService.selectByContNo(contNo));
            ctrLoanContEdit.setRepayCapPlanList(repayCapPlanService.selectBySerno(ctrLoanContEdit.getCtrLoanCont().getIqpSerno()));
        }
        return ctrLoanContEdit;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CtrLoanCont record) {
        return ctrLoanContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insertSelective(CtrLoanCont record) {
        return ctrLoanContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CtrLoanCont record) {
        return ctrLoanContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: 签订合同
     * @方法描述: 根据合同号更新签订状态和时间, 包含合同项下的担保合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map updateContStatus(CtrLoanCont ctrLoanCont, List<GrtGuarCont> list) throws Exception {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            // 获取当前合同以及担保合同信息
            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            if (Objects.nonNull(userInfo)) {
                ctrLoanCont.setUpdId(userInfo.getLoginCode());
                ctrLoanCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrLoanCont.setUpdDate(nowDate);
            }

            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(ctrLoanCont.getIqpSerno());
            // 是否占用第三方额度
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsOutstndTrdLmtAmt())) {
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(iqpLoanApp.getIsSeajnt());//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(iqpLoanApp.getIsRenew());//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(iqpLoanApp.getOrigiContNo());//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus("");//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType("");//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr("");//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getContHighAvlAmt());//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpLoanApp.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

                //是否占第三方额度（产品为集群贷或专业担保公司时不进行占额）
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_03);//额度类型
                cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo(iqpLoanApp.getTdpAgrNo());//额度分项编号
                cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp(iqpLoanApp.getPrdTypeProp());//授信品种类型
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(iqpLoanApp.getContHighAvlAmt());//占用敞口(原币种)
                cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(iqpLoanApp.getCvtCnyAmt());//占用总额(折人民币)
                cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(iqpLoanApp.getCvtCnyAmt());//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + " 前往额度系统进行占用第三方额度,请求报文为：" + cmisLmt0011ReqDto.toString());
                ResultDto<CmisLmt0011RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + "前往额度系统进行占用第三方额度,返回报文为：" + resultDtoDto.getData().toString());
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("普通贷款【{}】签订异常");
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("普通贷款业务占用第三方额度异常！" + resultDtoDto.getData().getErrorMsg());
                    rtnCode = code;
                    rtnMsg = resultDtoDto.getData().getErrorCode();
                    throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                }
            }

            // 判断是否为合同续签,若为续签合同则恢复原合同的占额
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsRenew())){
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getOrigiContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
                cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + "前往额度系统恢复原合同额度及第三方额度开始,请求报文为：" + JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + "前往额度系统恢复原合同额度及第三方额度结束,返回报文为：" + JSON.toJSONString(resultDtoDto));
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("普通贷款【{}】签订异常");
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("恢复原合同额度及第三方额度异常！" + resultDtoDto.getData().getErrorMsg());
                    rtnCode = code;
                    rtnMsg = resultDtoDto.getData().getErrorCode();
                    throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                }
            }

            // 更新 合同信息
            ctrLoanCont.setContStatus(CommonConstance.STD_ZB_CONT_TYP_200);
            updateSelective(ctrLoanCont);
            // 更新担保合同最后更新人信息
            for (GrtGuarCont grtGuarCont : list) {
                grtGuarCont.setUpdId(userInfo.getLoginCode());
                grtGuarCont.setUpdBrId(userInfo.getOrg().getCode());
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);// 担保合同生效
                grtGuarCont.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                if (StringUtils.isEmpty(grtGuarCont.getBizLine())) {
                    log.error("担保合同【" + grtGuarCont.getGuarContNo() + "】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);

                /**
                 * 判断是否为专业担保公司业务,是专业担保公司会去占担保公司的额度
                 * 若出现异常，更新主合同以及担保合同的信息进行事务回滚
                 */
                this.isProfessionalGuarCom(grtGuarCont, iqpLoanApp);

                // 信贷业务与押品关联关系信息同步
                HashMap queryMap = new HashMap();
                queryMap.put("bizType", CmisCommonConstants.STD_BUSI_TYPE_02);
                queryMap.put("isFlag", "1");
                queryMap.put("bizSerno", ctrLoanCont.getIqpSerno());
                queryMap.put("guarContNo", grtGuarCont.getGuarContNo());
                guarBaseInfoService.buscon(queryMap);
            }

            guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_02, ctrLoanCont.getContNo());

            //产品类型属性 STD_PRD_TYPE_PROP P011:省心快贷;P034:房抵e点贷
            String prdTypeProp = ctrLoanCont.getPrdTypeProp();
            if ("P011".equals(prdTypeProp) || "P034".equals(prdTypeProp)) {
                // 生成归档任务
                log.info("开始系统生成档案归档信息");
                String cusId = ctrLoanCont.getCusId();
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setDocClass("03");
                docArchiveClientDto.setDocType("19");// 省心快贷合同
                docArchiveClientDto.setBizSerno(ctrLoanCont.getIqpSerno());
                docArchiveClientDto.setCusId(cusId);
                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                docArchiveClientDto.setManagerId(ctrLoanCont.getManagerId());
                docArchiveClientDto.setManagerBrId(ctrLoanCont.getManagerBrId());
                docArchiveClientDto.setInputId(ctrLoanCont.getInputId());
                docArchiveClientDto.setInputBrId(ctrLoanCont.getInputBrId());
                docArchiveClientDto.setContNo(ctrLoanCont.getContNo());
                docArchiveClientDto.setLoanAmt(ctrLoanCont.getContAmt());
                docArchiveClientDto.setStartDate(ctrLoanCont.getContStartDate());
                docArchiveClientDto.setEndDate(ctrLoanCont.getContEndDate());
                docArchiveClientDto.setPrdId(ctrLoanCont.getPrdId());
                docArchiveClientDto.setPrdName(ctrLoanCont.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成普通贷款合同档案归档信息失败,业务流水号[{}],产品类型:[{}]", ctrLoanCont.getIqpSerno(), prdTypeProp);
                }
            }
        } catch (BizException e) {
            log.error("普通贷款合同申请签订异常", e.getMessage(), e);
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
        } catch (Exception e) {
            log.error("普通贷款合同申请签订异常", e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
            throw new Exception(e.getMessage());
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: isProfessionalGuarCom
     * @方法描述: 判断担保合同关联的保证人是否为专业担保公司，并占额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void isProfessionalGuarCom(GrtGuarCont grtGuarCont, IqpLoanApp iqpLoanApp) {
        if (CmisCommonConstants.GUAR_MODE_30.equals(grtGuarCont.getGuarWay())) {
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarContNo", grtGuarCont.getGuarContNo());
            List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(queryModel);
            if (CollectionUtils.isEmpty(guarGuaranteeList)) {
                log.error("根据担保合同编号【{}】未获取到保证人信息", grtGuarCont.getGuarContNo());
                throw BizException.error(null, EcbEnum.ECB020048.key, EcbEnum.ECB020048 + grtGuarCont.getGuarContNo());
            }
            // 判断保证人是否属于专业担保公司
            for (GuarGuarantee guarGuarantee : guarGuaranteeList) {
                if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(guarGuarantee.getIsGuarCom())) {
                    log.info("通过专业担保公司客户编号：" + guarGuarantee.getCusId() + " 调客户接口查询客户信息开始," + "请求报文：" + guarGuarantee.getCusId());
                    CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(guarGuarantee.getCusId());
                    log.info("通过专业担保公司客户编号：" + guarGuarantee.getCusId() + " 调客户接口查询客户信息结束," + "响应报文：" + cusBaseDtoResultDto.toString());
                    CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                    cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getContNo());//合同编号
                    cmisLmt0011ReqDto.setCusId(cusBaseDtoResultDto.getCusId());//客户编号
                    cmisLmt0011ReqDto.setCusName(cusBaseDtoResultDto.getCusName());//客户名称
                    cmisLmt0011ReqDto.setDealBizType(grtGuarCont.getGuarContType());//交易业务类型
                    cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                    cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                    cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                    cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
                    cmisLmt0011ReqDto.setIsFollowBiz(iqpLoanApp.getIsSeajnt());//是否无缝衔接
                    cmisLmt0011ReqDto.setIsBizRev(iqpLoanApp.getIsRenew());//是否合同重签
                    cmisLmt0011ReqDto.setOrigiDealBizNo(iqpLoanApp.getOrigiContNo());//原交易业务编号
                    cmisLmt0011ReqDto.setOrigiDealBizStatus("");//原交易业务状态
                    cmisLmt0011ReqDto.setOrigiRecoverType("");//原交易业务恢复类型
                    cmisLmt0011ReqDto.setOrigiBizAttr("");//原交易属性D
                    cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getCvtCnyAmt());//交易业务金额
                    cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                    cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                    cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                    cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                    cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                    cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                    cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                    cmisLmt0011ReqDto.setInputDate(iqpLoanApp.getInputDate());//登记日期

                    List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

                    //是否占第三方额度（产品为集群贷或专业担保公司时不进行占额）
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDtoOtherLmt = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_03);//额度类型
                    cmisLmt0011OccRelListDtoOtherLmt.setLmtSubNo("");//额度分项编号
                    cmisLmt0011OccRelListDtoOtherLmt.setPrdTypeProp(iqpLoanApp.getPrdTypeProp());//授信品种类型
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmt(iqpLoanApp.getContHighAvlAmt());//占用敞口(原币种)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizTotalAmtCny(iqpLoanApp.getCvtCnyAmt());//占用总额(折人民币)
                    cmisLmt0011OccRelListDtoOtherLmt.setBizSpacAmtCny(iqpLoanApp.getCvtCnyAmt());//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDtoOtherLmt);
                    cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                    log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + " 前往额度系统进行占用专业担保公司额度,请求报文为：" + cmisLmt0011ReqDto.toString());
                    ResultDto<CmisLmt0011RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                    log.info("普通贷款业务" + iqpLoanApp.getIqpSerno() + " 前往额度系统进行占用专业担保公司额度,返回报文为：" + resultDtoDto.getData().toString());
                    String code = resultDtoDto.getData().getErrorCode();
                    if (!"0".equals(resultDtoDto.getCode())) {
                        log.error("普通贷款业务占用专业担保公司额度异常！");
                        throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                    if (!"0000".equals(code)) {
                        log.error("普通贷款业务占用专业担保公司额度异常！" + resultDtoDto.getData().getErrorMsg());
                        throw BizException.error(null, resultDtoDto.getData().getErrorCode(), resultDtoDto.getData().getErrorMsg());
                    }
                }
            }
        }
    }

    /**
     * @方法名称: 普通贷款合同注销
     * @方法描述: 根据合同号更新签订状态和时间, 包含合同项下的担保合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map onLogOut(@RequestBody Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        try {

            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            contNo = (String) params.get("contNo");//主键合同编号
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            // 先判断合同是否存在未结清的业务
            String responseResult = this.isContUncleared(ctrLoanCont.getContNo());
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)) {
                rtnCode = EcbEnum.ECB020055.key;
                rtnMsg = EcbEnum.ECB020055.value;
                return result;
            } else {
                // 获取合同注销后的合同状态
                String finalContStatus = this.getContStatusAfterLogout(ctrLoanCont.getContEndDate(), ctrLoanCont.getContStatus());
                ctrLoanCont.setUpdId(userInfo.getLoginCode());
                ctrLoanCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrLoanCont.setUpdDate(nowDate);
                ctrLoanCont.setContStatus(finalContStatus);
                int updateCount = updateSelective(ctrLoanCont);
                if (updateCount <= 0) {
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }

                //合同注销后，释放额度
                String guarMode = ctrLoanCont.getGuarWay();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.STD_BUSI_TYPE_05.equals(ctrLoanCont.getBizType())) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = ctrLoanCont.getCvtCnyAmt();
                } else {
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                            || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                        recoverSpacAmtCny = BigDecimal.ZERO;
                        recoverAmtCny = ctrLoanCont.getCvtCnyAmt();
                    } else {
                        recoverSpacAmtCny = ctrLoanCont.getCvtCnyAmt();
                        recoverAmtCny = ctrLoanCont.getCvtCnyAmt();
                    }
                }
                // 根据合是否存在业务，得到恢复类型
                String recoverType = "";
                Map map = new HashMap();
                map.put("contNo", ctrLoanCont.getContNo());
                List<AccLoan> accLoanList = accLoanService.queryAccLoanByContNo(map);
                if (CollectionUtils.nonEmpty(accLoanList)) {
                    //结清注销
                    for (AccLoan accLoan : accLoanList) {
                        if (CmisCommonConstants.ACC_STATUS_7.equals(accLoan.getAccStatus())) {
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                        } else {
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                            break;
                        }
                    }
                } else {
                    //未用注销
                    recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(ctrLoanCont.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(ctrLoanCont.getInputId());
                cmisLmt0012ReqDto.setInputBrId(ctrLoanCont.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                if (!"0".equals(resultDto.getCode())) {
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("合同注销恢复额度异常！");
                    throw BizException.error(null, resultDto.getData().getErrorCode(), resultDto.getData().getErrorMsg());
                }
                //不存在电子用印的情况下，需要作废档案归档任务
                if (!"1".equals(ctrLoanCont.getIsESeal()) && CmisCommonConstants.CUR_TYPE_CNY.equals(ctrLoanCont.getCurType())) {
                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务开始");
                    try {
                        docArchiveInfoService.invalidByBizSerno(ctrLoanCont.getIqpSerno(),"");
                    } catch (Exception e) {
                        log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                    }
                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务结束");
                }
            }
        } catch (BizException e) {
            log.error("普通贷款合同申请注销异常", e.getMessage(), e);
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
        } catch (Exception e) {
            log.error("普通贷款合同申请注销异常", e.getMessage(), e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = e.getMessage();
        } finally {
            //成功则无需处理
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: isContUncleared
     * @方法描述:判断合同项下是否存在未结清的业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String isContUncleared(String contNo) {
        String result = "";
        // 调额度接口，查询合同项下是否存在未结清的业务
        CmisLmt0045ReqDto cmisLmt0045ReqDto = new CmisLmt0045ReqDto();
        cmisLmt0045ReqDto.setDealBizNo(contNo);
        log.info("根据合同编号【{}】，前往额度系统查询合同项下是否存在未结清的业务开始,请求报文为:【{}】", contNo, cmisLmt0045ReqDto.toString());
        ResultDto<CmisLmt0045RespDto> cmisLmt0045RespDtoResultDto = cmisLmtClientService.cmislmt0045(cmisLmt0045ReqDto);
        log.info("根据合同编号【{}】，前往额度系统查询合同项下是否存在未结清的业务结束,响应报文为:【{}】", contNo, cmisLmt0045RespDtoResultDto.toString());
        if (!"0".equals(cmisLmt0045RespDtoResultDto.getCode())) {
            log.error("接口调用失败！");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        if (!"0000".equals(cmisLmt0045RespDtoResultDto.getData().getErrorCode())) {
            log.error("查询合同项下是否存在未结清的业务失败！");
            throw BizException.error(null, cmisLmt0045RespDtoResultDto.getData().getErrorCode(), cmisLmt0045RespDtoResultDto.getData().getErrorMsg());
        }
        result = cmisLmt0045RespDtoResultDto.getData().getFlag();
        return result;
    }

    /**
     * @方法名称: getContStatusAfterLogout
     * @方法描述: 获取合同注销后的合同状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String getContStatusAfterLogout(String stringEndDate, String contStatus) throws ParseException {
        /*
         * 合同注销规则
         * 1.中止--合同未到期，项下无业务或已结清，提前注销
         * 2.注销--合同到期，且项下无未结清业务，到期注销
         * 3.撤回--不使用(不清楚使用场景)
         * 4.作废--合同未生效，注销则为作废
         */
        String finalContStatus = "";// 注销后的合同状态
        if (CmisCommonConstants.CONT_STATUS_100.equals(contStatus)) {
            finalContStatus = CmisCommonConstants.CONT_STATUS_800;
        } else {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//营业日期
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date endDate = sdf.parse(stringEndDate);
            Date openDayDate = sdf.parse(openDay);
            if (endDate.compareTo(openDayDate) >= 0) {
                // 注销--合同到期，且项下无未结清业务，到期注销
                finalContStatus = CmisCommonConstants.CONT_STATUS_600;
            } else {
                // 中止--合同未到期，项下无业务或已结清，提前注销
                finalContStatus = CmisCommonConstants.CONT_STATUS_500;
            }
        }
        return finalContStatus;
    }

    /**
     * @方法名称: 普通贷款合同重签
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map contReSign(@RequestBody Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        int pvpLoanAppCount = 0;
        int accLoanCount = 0;
        try {
            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            contNo = (String) params.get("contNo");//主键合同编号
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpLoanAppCount = pvpLoanAppMapper.countPvpLoanAppCountByContNo(pvpQueryMap);
            accLoanCount = accLoanMapper.countAccLoanCountByContNo(contNo);
            if (pvpLoanAppCount > 0) {
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_PVP.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_PVP.value;
            } else if (accLoanCount > 0) {
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_ACC.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_ACC.value;
            } else {
                //合同重签需要恢复第三方占额
                IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(ctrLoanCont.getIqpSerno());
                // 是否占用第三方额度
                if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsOutstndTrdLmtAmt())) {

                }

                ctrLoanCont.setUpdId(userInfo.getLoginCode());
                ctrLoanCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrLoanCont.setUpdDate(nowDate);
                ctrLoanCont.setPaperContSignDate("");//纸质合同签订日期为空
                ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
                updateSelective(ctrLoanCont);
            }
        } catch (Exception e) {
            log.error("普通贷款合同申请重签异常", e.getMessage(), e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
        } finally {
            //成功则无需处理
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: updateByContNoKey
     * @方法描述: 根据入参更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateByContNoKey(Map params) {
        return ctrLoanContMapper.updateByContNoKey(params);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(CtrLoanCont record) {
        return ctrLoanContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contNo) {
        return ctrLoanContMapper.deleteByPrimaryKey(contNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrLoanContMapper.deleteByIds(ids);
    }

    /**
     * 通过入参查询合同信息
     *
     * @param params
     * @return
     */
    public List<CtrLoanCont> selectCtrLoanContByParams(Map params) {
        return ctrLoanContMapper.selectCtrLoanContByParams(params);
    }


    /**
     * 查询引入入参的第三方额度的生效、待生效的合同数据
     *
     * @param params
     * @return
     */
    public List<CtrLoanCont> selectCtrLoanContByThLimitId(Map params) {
        return ctrLoanContMapper.selectCtrLoanContByThLimitId(params);
    }


    /*    *//**
     * @param ctrLoanCont
     * @方法名称: updateSelectiveByKey
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     *//*

    public int updateSelectiveByKey(CtrLoanCont ctrLoanCont) throws Exception {
        int rtnData = 0;
        try {
            String contNo = ctrLoanCont.getContNo();
            String bizType = ctrLoanCont.getBizType();
            String optType = ctrLoanCont.getOptType();
            if (StringUtils.isBlank(contNo)) {
                throw new YuspException(EcbEnum.UPDATE_NULL_EXCEPTION.key, EcbEnum.UPDATE_NULL_EXCEPTION.value);
            }//合同打印
            if (CmisBizConstants.OPT_TYPE_ONPRINT.equals(optType)) {
                rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
            }//合同签订
            if (CmisBizConstants.OPT_TYPE_SIGN.equals(optType)) {
                ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_200);
                BigDecimal contPrintNum = ctrLoanCont.getContPrintNum();
                //打印合同次数大于0次
                if (contPrintNum.compareTo(BigDecimal.ZERO) == 0) {
                    throw new YuspException(EcbEnum.GRT_CONT_PRINT_EXCEPTION.key, EcbEnum.GRT_CONT_PRINT_EXCEPTION.value);
                } else if (CmisBizConstants.BIZ_TYPE_01.equals(bizType)) {
                    String iqpSerno = ctrLoanCont.getIqpSerno();
                    IqpLoanAppApprMode iqpLoanAppApprMode = iqpLoanAppApprModeService.selectByPrimaryKey(iqpSerno);
                    if (iqpLoanAppApprMode == null) {
                        throw new YuspException(EcbEnum.AFTEREND_IQPLOANAPPAPPRMODE_EXCEPTION.key, EcbEnum.AFTEREND_IQPLOANAPPAPPRMODE_EXCEPTION.value);
                    }
                    String wfStatus = iqpLoanAppApprMode.getWfStatus();
                    if (CmisBizConstants.WF_STATUS_02.equals(wfStatus)) {
                        iqpLoanAppApprMode.setWfStatus(CmisBizConstants.WF_STATUS_04);
                        iqpLoanAppApprModeService.updateSelective(iqpLoanAppApprMode);
                    }
                }
                rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
            }
            //合同作废
            if (CmisBizConstants.OPT_TYPE_ONINVALID.equals(optType)) {
                updategrtloancontrel(ctrLoanCont);
                ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_800);
                rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
            }
            String accStatus = "";
            boolean unconfirmed = false;
            boolean Verification = false;
            boolean clofe = false;
            //合同撤回、中止、注销
            if (CmisBizConstants.OPT_TYPE_ONLOGOUT.equals(optType) || CmisBizConstants.OPT_TYPE_ONRECALL.equals(optType) || CmisBizConstants.OPT_TYPE_ONSUSPEND.equals(optType)) {
                Map params = new HashMap();
                params.put("contNo", contNo);
                List<AccLoan> accLoanList = accLoanService.selectAccLoanByContNo(params);
                if (CollectionUtils.isEmpty(accLoanList) || accLoanList.size() == 0) {
                    throw new YuspException(EcbEnum.ACC_LOAN_QUERT_EXCEPTION.key, EcbEnum.ACC_LOAN_QUERT_EXCEPTION.value);
                }
                for (AccLoan accLoan : accLoanList) {
                    accStatus = accStatus + accLoan.getAccStatus() + CmisCommonConstants.COMMON_SPLIT_COMMA;
                }
                if (StringUtils.nonBlank(accStatus)) {
                    accStatus = accStatus.substring(0, accStatus.lastIndexOf(CmisCommonConstants.COMMON_SPLIT_COMMA));
                }
                unconfirmed = accStatus.contains(CmisBizConstants.IQP_ACC_STATUS_0);
                Verification = accStatus.contains(CmisBizConstants.IQP_ACC_STATUS_1);
                clofe = accStatus.contains(CmisBizConstants.IQP_ACC_STATUS_9);
            }
            //合同撤回
            if (CmisBizConstants.OPT_TYPE_ONRECALL.equals(optType)) {
                if (unconfirmed == true || CmisBizConstants.IQP_ACC_STATUS_0.equals(accStatus)) {
                    updategrtloancontrel(ctrLoanCont);
                    ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_700);
                    rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                } else {
                    throw new YuspException(EcbEnum.GRT_CONT_RECALL_EXCEPTION.key, EcbEnum.GRT_CONT_RECALL_EXCEPTION.value);
                }
            }
            //合同中止
            if (CmisBizConstants.OPT_TYPE_ONSUSPEND.equals(optType)) {
                if (Verification == true || CmisBizConstants.IQP_ACC_STATUS_1.equals(accStatus)) {
                    updategrtloancontrel(ctrLoanCont);
                    ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                } else {
                    throw new YuspException(EcbEnum.GRT_CONT_LOGOUT_EXCEPTION.key, EcbEnum.GRT_CONT_LOGOUT_EXCEPTION.value);
                }
            }
            //合同注销
            if (CmisBizConstants.OPT_TYPE_ONLOGOUT.equals(optType)) {
                if (clofe == true || CmisBizConstants.IQP_ACC_STATUS_9.equals(accStatus)) {
                    updategrtloancontrel(ctrLoanCont);
                    ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_600);
                    rtnData = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                } else {
                    throw new YuspException(EcbEnum.GRT_CONT_SUSPEND_EXCEPTION.key, EcbEnum.GRT_CONT_SUSPEND_EXCEPTION.value);
                }
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtnData;
    }*/

    /**
     * @param ctrLoanCont
     * @方法名称: updategrtloancontrel
     * @方法描述: 合同作废撤回注销更新合同子表信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updategrtloancontrel(CtrLoanCont ctrLoanCont) throws Exception {
        int rtnData = 0;
        Map params = new HashMap();
        params.put("contNo", ctrLoanCont.getContNo());
        params.put("iqpSerno", ctrLoanCont.getIqpSerno());
        params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        try {
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectByParams(params);
            if (CollectionUtils.isEmpty(iqpGuarBizRelAppList)) {
                throw new YuspException(EcbEnum.GRT_NULL_EXCEPTION.key, EcbEnum.GRT_NULL_EXCEPTION.value);
            }
            for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
                iqpGuarBizRelApp.setCorreRel(CmisBizConstants.CORRE_REL_3);
                iqpGuarBizRelAppService.updateSelective(iqpGuarBizRelApp);
            }
            params.remove("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelService.selectByParams(params);
            if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                throw new YuspException(EcbEnum.GRT_NULL_EXCEPTION.key, EcbEnum.GRT_NULL_EXCEPTION.value);
            }
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                grtGuarBizRstRel.setCorreRel(CmisBizConstants.CORRE_REL_3);
                grtGuarBizRstRelService.updateSelective(grtGuarBizRstRel);
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return rtnData;
    }

    /**
     * 通过合同号获取授信额度信息
     *
     * @param params
     * @return
     */
    public Map getLmtAcctInfoByContNo(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.CTR_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.CTR_SUCCESS_DEF.value;
        try {
            String contNo = (String) params.get("contNo");//合同编号
            if (StringUtils.isBlank(contNo)) {
                rtnCode = EcbEnum.E_CTR_QUERY_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.E_CTR_QUERY_PARAMS_EXCEPTION.value;
                return rtnData;
            }

            log.info("通过合同" + contNo + "获取业务申请额度数据-获取合同与授信、第三方额度关系数据");
            List<CtrLmtRel> ctrLmtRelList = ctrLmtRelService.selectCtrLmtRelByContNo(contNo);

            if (CollectionUtils.isEmpty(ctrLmtRelList) || ctrLmtRelList.size() == 0) {
                //针对单笔单批的场景，若是未选取第三方额度，则无关系数据，因此该场景不能认为是异常
                rtnCode = EcbEnum.CTR_QUERY_LMTRELNULL_MSG.key;
                rtnMsg = EcbEnum.CTR_QUERY_LMTRELNULL_MSG.value;
                return rtnData;
            }

            //定义交互的dto，进行信息查询
            log.info("通过合同" + contNo + "获取业务申请额度数据-获取业务申请额度数据-封装交互dto");


            //调用接口进行数据查询操作
            log.info("通过合同" + contNo + "获取业务申请额度数据-获取业务申请额度数据-调用接口开始");
            try {
                //ctrLmtQueryDto = iLmtClientService.getLmtAndThLmtInfo(ctrLmtQueryDto);
            } catch (Exception e) {
                log.error("通过合同" + contNo + "获取业务申请额度数据-获取业务申请额度数据--调用接口异常", e);
                rtnCode = EcbEnum.E_CTR_QUERY_CLIENTCALL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_CTR_QUERY_CLIENTCALL_EXCEPTION.value;
                return rtnData;
            }
            log.info("通过合同" + contNo + "获取业务申请额度数据-获取业务申请额度数据-调用接口结束");

            log.info("通过合同" + contNo + "获取业务申请额度数据-获取接口返回数据，处理数据并返回");


        } catch (Exception e) {
            log.error("合同查询额度信息异常！", e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value + "：" + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 获取待签合同信息
     *
     * @param userId
     * @return
     */
    public List<BizCtrLoanContDto> selectCtrLoanContByCorreManagerId(String userId) {
        return ctrLoanContMapper.selectCtrLoanContByCorreManagerId(userId);
    }

    /**
     * 业务统计-笔数
     *
     * @param map
     * @return
     */
    public List<Map> getBusRows(Map map) {
        return ctrLoanContMapper.selectBusRows(map);
    }

    /**
     * 业务统计-金额（万元）
     *
     * @param map
     * @return
     */
    public List<Map> getBusAmt(Map map) {
        return ctrLoanContMapper.selectBusAmt(map);
    }


    /**
     * 先查询 放款申请表 -> 有数据 返回，无数据 ->   再查询 合同主表
     * 业务流水号 获取 合同号  和 放款申请 号
     *
     * @param iqpSerno
     * @return
     */
    public Map<String, String> singleBatchGetContAndPvpInfo(String iqpSerno) {
        Map<String, String> map = new HashMap<>();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("iqpSerno", iqpSerno);
        List<PvpLoanApp> pvpLoanApps = pvpLoanAppService.selectByModel(queryModel);
        if (CollectionUtils.nonEmpty(pvpLoanApps)) {
            PvpLoanApp pvpLoanApp = pvpLoanApps.get(0);
            map.put("pvp_serno", pvpLoanApp.getPvpSerno());
            map.put("cont_no", pvpLoanApp.getContNo());
            map.put("cont_status", CmisBizConstants.IQP_CONT_STS_200);//已签订
        } else {
            List<CtrLoanCont> ctrLoanConts = ctrLoanContMapper.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(ctrLoanConts)) {
                CtrLoanCont ctrLoanCont = ctrLoanConts.get(0);
                map.put("cont_no", ctrLoanCont.getContNo());
                map.put("cont_status", ctrLoanCont.getContStatus());//未签订
                map.put("pvp_serno", null);
            }
        }
        return map;
    }

    /**
     * @param contNo
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author shenli
     * @date 2021/4/24 0024 14:06
     * @version 1.0.0
     * @desc 根据合同编号，查询合同基本信息，关联担保合同列表，客户信息，批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> queryContInfoXw(String contNo) {

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //合同信息
            returnMap.put("CtrLoanCont", ctrloancont);

            //担保合同列表
            List<GrtGuarCont> grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            returnMap.put("GrtGuarContList", grtGuarContList);

        } catch (Exception e) {
            log.error("小微-合同管理-详细信息查询异常：", e);
        }
        return returnMap;
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author shenli
     * @date 2021/4/24 0024 17:33
     * @version 1.0.0
     * @desc 小微合同列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanCont> queryCtrLoanContListXw(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanCont> list = ctrLoanContMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @param signCtrLoanContDto
     * @return int
     * @author shenli
     * @date 2021/4/25 0025 20:56
     * @version 1.0.0
     * @desc 小微-贷款合同: 合同签订
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int signContXw(SignCtrLoanContDto signCtrLoanContDto) throws Exception {

        CtrLoanCont ctrLoanCont = signCtrLoanContDto.getCtrLoanCont();
        String contNo = ctrLoanCont.getContNo();
        log.info("******************小微-合同管理： 签订合同开始 ******************", contNo);
        //查询申请表信息
        String iqpSerno = ctrLoanCont.getIqpSerno();
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);

        if (!"00".equals(iqpLoanApp.getGuarWay())) {

            try {
                log.info("调用信贷业务合同信息同步开始");
                guarBusinessRelService.sendBusinf("01", "02", ctrLoanCont.getContNo());
                log.info("调用信贷业务合同信息同步结束");
            } catch (Exception e) {
                log.info("调用信贷业务合同信息同步失败" + e);

            }

            try {
                log.info("调用根据业务流水号同步押品与业务关联信息开始");
                guarBusinessRelService.sendBusconXw(iqpLoanApp.getIqpSerno(), "02");
                log.info("调用根据业务流水号同步押品与业务关联信息结束");
            } catch (Exception e) {
                log.info("调用根据业务流水号同步押品与业务关联信息失败" + e);

            }

            //将关联的担保合同的合同状态更新为“已生效”
            List<GrtGuarCont> grtGuarContList = signCtrLoanContDto.getGrtGuarContList();
            if (grtGuarContList != null && grtGuarContList.size() > 0) {
                for (int i = 0; i < grtGuarContList.size(); i++) {
                    GrtGuarCont grtGuarCont = grtGuarContList.get(i);

                    try {
                        log.info("调用信贷担保合同信息同步开始");
                        guarBusinessRelService.sendConinf("01", grtGuarCont.getGuarContNo());
                        log.info("调用信贷担保合同信息同步结束");
                    } catch (Exception e) {
                        log.info("调用信贷担保合同信息同步失败" + e);
                    }
                    try {
                        log.info("调用根据担保合同编号同步担保合同与押品关系开始");
                        guarBusinessRelService.sendContra(grtGuarCont.getGuarContNo());
                        log.info("调用根据担保合同编号同步担保合同与押品关系结束");
                    } catch (Exception e) {
                        log.info("调用根据担保合同编号同步担保合同与押品关系失败" + e);
                    }

                }
            }
        }


        try {
            //将合同状态更新为“已生效”
            // 2021年10月8日14:54:45 hubp 获取当前日期给签订日期赋值
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            ctrLoanCont.setContStatus("200");
            ctrLoanCont.setSignDate(openday);
            ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
            Map<String, String> map = new HashMap<>();
            // * bizType 业务类型 01授信 02业务
            //  * isFlag  是否有效 1有效 0无效 默认1
            // * bizSerno  业务编号
            //  * guarContNo 担保合同编号  02业务 必填
            // * serno  业务与押品关联流水号 01授信 必填
            map.put("bizType", "02");
            map.put("isFlag", "1");
            map.put("bizSerno", ctrLoanCont.getContNo());
            //将关联的担保合同的合同状态更新为“已生效”
            List<GrtGuarCont> grtGuarContList = signCtrLoanContDto.getGrtGuarContList();
            if (grtGuarContList != null && grtGuarContList.size() > 0) {
                for (int i = 0; i < grtGuarContList.size(); i++) {
                    GrtGuarCont grtGuarCont = grtGuarContList.get(i);
                    grtGuarCont.setGuarContState("101");//生效,码值没有确定最终版

                    if (StringUtils.isEmpty(grtGuarCont.getBizLine())) {
                        log.error("担保合同【" + grtGuarCont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtGuarCont);
                    map.put("guarContNo", grtGuarCont.getGuarContNo());
                    guarBaseInfoService.buscon(map);
                }
            }
            log.info("******************小微-合同创建成功： 前往额度系统占用额度开始 ******************【{}】", ctrLoanCont.getContNo());
            ResultDto resultDto = cmisBizXwCommonService.contLmtForXw(ctrLoanCont);
            log.info("******************小微-合同创建成功： 前往额度系统占用额度结束 ******************【{}】", ctrLoanCont.getContNo());
            if ("9999".equals(resultDto.getCode())) {
                throw BizException.error(null, resultDto.getCode(), resultDto.getMessage());
            }

            // 发送合同签订成功短息
            log.info("******************发送合同签订短信开始******************");
            // 发送客户经理
            String receivedUserType = "1";// 接收人员类型 1--客户经理 2--借款人
            Map paramMap = new HashMap();// 短信填充参数
            paramMap.put("cusName", ctrLoanCont.getCusName());
            paramMap.put("prdName", ctrLoanCont.getPrdName());
            //执行发送客户经理操作
            messageCommonService.sendMessage(MessageNoEnums.MSG_XW_M_0001.key, paramMap, receivedUserType, ctrLoanCont.getManagerId(), "");

            // 发送借款人
            String phone = ""; // 电话号码，优先取客户信息中的
            try{
                CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(iqpLoanApp.getCusId());
                phone = cusIndivContactDto.getMobileNo();
            }catch (Exception e){
                log.info("获取手机号异常，获取合同表中的"+ e);
                phone = ctrLoanCont.getPhone();
            }
            receivedUserType = "2";// 接收人员类型 1--客户经理 2--借款人
            // 执行发送借款人信息操作
            messageCommonService.sendMessage(MessageNoEnums.MSG_XW_C_0001.key, paramMap, receivedUserType, ctrLoanCont.getManagerId(), phone);
            log.info("******************发送合同签订短信结束******************");
        } catch (BizException e) {
            log.error("小微-合同管理： 签订合同异常：", e);
            throw BizException.error(null, "9999", e.getMessage());
        } catch (Exception e) {
            log.error("小微-合同管理： 签订合同异常：", e);
            throw BizException.error(null, "9999", e.getMessage());
        } finally {
            log.info("******************小微-合同管理： 签订合同结束 ******************【{}】", contNo);
        }


        return 0;
    }

    /**
     * @param contNo
     * @return int
     * @author shenli
     * @date 2021/4/25 0025 20:56
     * @version 1.0.0
     * @desc 小微-贷款合同: 作废签章审批
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map cancelSignXw(String contNo) {

        log.info("******************小微-合同管理： 作废签章审批开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            String manager_id = ctrloancont.getManagerId();
            String sign_approve_status = ctrloancont.getSignApproveStatus();

            //将签章审批状态更新为“自行退出”
            ctrloancont.setSignApproveStatus("996");
            ctrLoanContMapper.updateByPrimaryKey(ctrloancont);


        } catch (Exception e) {
            returnMap.put("code", "9999");
            returnMap.put("msg", "作废签章审批异常！");
            log.error("小微-合同管理-作废签章审批异常：", e);
        }

        log.info("******************小微-合同管理： 作废签章审批结束 ******************", contNo);

        return returnMap;

    }

    /**
     * @param iqpLoanApp
     * @return int
     * @author shenli
     * @date 2021/4/25 0025 20:56
     * @version 1.0.0
     * @desc 小微-贷款申请：提交
     * 生成合同信息
     * @修改历史: 修改时间：2021年6月30日09:32:00    修改人员：hubp    修改原因：修改合同参数
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto createCtrLoanContXw(IqpLoanApp iqpLoanApp) {

        log.info("******************小微-合同申请： 提交开始 ******************", iqpLoanApp.getIqpSerno());
        try {
            //相关校验待确定iqploanapp
            // 校验重复提交，导致重复生成合同
            CtrLoanCont cont = ctrLoanContMapper.selectContByIqpSerno(iqpLoanApp.getIqpSerno());
            if (null != cont) {
                // 已经存在合同
                return new ResultDto(null).code(9999).message("重复生成合同");
            }
            //根据流水号获取合同申请信息
            iqpLoanApp.setApproveStatus("997");//更新审批状态为通过
            iqpLoanApp.setCvtCnyAmt(iqpLoanApp.getContHighAvlAmt());
            iqpLoanAppMapper.updateByPrimaryKey(iqpLoanApp);
            // 获取所属团队
            AreaAdminUser areaAdminUser = areaAdminUserService.selectByPrimaryKey(iqpLoanApp.getManagerId());
            //1. 新增合同
            CtrLoanCont ctrLoanCont = new CtrLoanCont();
            if (null != areaAdminUser && !StringUtils.isBlank(areaAdminUser.getTeamType())) {
                ctrLoanCont.setTeam(areaAdminUser.getTeamType());
            }
            BeanUtils.beanCopy(iqpLoanApp, ctrLoanCont);
            // 是否使用授信额度 默认是
            ctrLoanCont.setIsUtilLmt("1");
            // 币种 人民币
            ctrLoanCont.setCurType("CNY");
            ctrLoanCont.setContNo(iqpLoanApp.getContNo());
            ctrLoanCont.setIqpSerno(iqpLoanApp.getIqpSerno());
            ctrLoanCont.setCusId(iqpLoanApp.getCusId());
            ctrLoanCont.setCusName(iqpLoanApp.getCusName());
            ctrLoanCont.setPrdId(iqpLoanApp.getPrdId());
            ctrLoanCont.setPrdName(iqpLoanApp.getPrdName());
            ctrLoanCont.setCertCode(iqpLoanApp.getCertCode());
            ctrLoanCont.setSurveySerno(iqpLoanApp.getSurveySerno());
            ctrLoanCont.setPhone(iqpLoanApp.getPhone());
            ctrLoanCont.setGuarWay(iqpLoanApp.getGuarWay());
            ctrLoanCont.setLoanCha(iqpLoanApp.getLoanCha());
            ctrLoanCont.setCurType(iqpLoanApp.getCurType());
            ctrLoanCont.setIsESeal(CmisCommonConstants.YES_NO_1); // 是否电子用印
            ctrLoanCont.setContAmt(iqpLoanApp.getContAmt());
            ctrLoanCont.setContBalance(iqpLoanApp.getContAmt());
            ctrLoanCont.setContType(iqpLoanApp.getContType());
            ctrLoanCont.setAppTerm(new BigDecimal(iqpLoanApp.getContTerm()));// 申请期限
            ctrLoanCont.setHighAvlAmt(iqpLoanApp.getContHighAvlAmt());// 合同最高可用金额
            ctrLoanCont.setCvtCnyAmt(iqpLoanApp.getCvtCnyAmt());// 折算人民币金额
            //根据期限放入LPR利率相关字段 CURT_LPR_RATE
            //最高额合同无需赋值，一般合同页面赋值即可，后台无需赋值
             ctrLoanCont.setCurtLprRate(iqpLoanApp.getCurtLprRate());
            ctrLoanCont.setLprRateIntval(iqpLoanApp.getLprRateIntval());// 定价区间
            //执行年利率
            ctrLoanCont.setExecRateYear(iqpLoanApp.getExecRateYear());
            ctrLoanCont.setRateFloatPoint(iqpLoanApp.getRateFloatPoint());//LPR_BP 浮动点数
            ctrLoanCont.setContStatus("100");
            ctrLoanCont.setSignApproveStatus("000"); // 待发起

            ctrLoanCont.setSignChannel("PC端");
            ctrLoanCont.setTermType("M");//设置默认期限类型->月
            ctrLoanCont.setContStartDate(iqpLoanApp.getStartDate());//合同起始日期
            ctrLoanCont.setContEndDate(iqpLoanApp.getEndDate());//合同到期日
            ctrLoanCont.setSignMode(iqpLoanApp.getSignMode());
            ctrLoanCont.setBelgLine(iqpLoanApp.getBelgLine());
            ctrLoanCont.setManagerBrId(iqpLoanApp.getManagerBrId());
            ctrLoanCont.setManagerId(iqpLoanApp.getManagerId());
            ctrLoanCont.setInputId(iqpLoanApp.getInputId());
            ctrLoanCont.setInputBrId(iqpLoanApp.getInputBrId());
            ctrLoanCont.setInputDate(iqpLoanApp.getInputDate());
            ctrLoanCont.setOprType("01");
            ctrLoanCont.setChnlSour("02"); // 2021年9月25日14:22:56 Hubp 塞入渠道来源字段，02为PC端
            ctrLoanCont.setQq(iqpLoanApp.getQq());
            ctrLoanCont.setFax(iqpLoanApp.getFax());
            ctrLoanCont.setLinkman(iqpLoanApp.getLinkman());
            ctrLoanCont.setEmail(iqpLoanApp.getEmail());
            ctrLoanCont.setWechat(iqpLoanApp.getWechat());
            ctrLoanCont.setIrAdjustType(iqpLoanApp.getIrAdjustType());
            ctrLoanContMapper.insert(ctrLoanCont);

            //2. “申请贷款类型=一般合同”时，均需要同步更新项下担保合同的“担保合同起始日期、担保合同到期日期、担保合同期限
            if ("1".equals(ctrLoanCont.getContType())) {
                //根据合同申请流水号查询担保合同
                List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(iqpLoanApp.getIqpSerno());
                for (int i = 0; i < grtGuarContList.size(); i++) {
                    GrtGuarCont grtguarcont = grtGuarContList.get(i);
                    grtguarcont.setGuarStartDate(iqpLoanApp.getStartDate());
                    grtguarcont.setGuarEndDate(iqpLoanApp.getEndDate());
                    grtguarcont.setGuarTerm(iqpLoanApp.getContTerm());

                    if (StringUtils.isEmpty(grtguarcont.getBizLine())) {
                        log.error("担保合同【" + grtguarcont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtguarcont);
                }
            }

            //3. 根据流水号查询借款合同与担保合同关系，更新合同编号
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", iqpLoanApp.getIqpSerno());
            List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByModel(queryModel);
            for (int i = 0; i < grtGuarBizRstRelList.size(); i++) {
                GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelList.get(i);
                grtGuarBizRstRel.setContNo(ctrLoanCont.getContNo());
                grtGuarBizRstRelMapper.updateByPrimaryKey(grtGuarBizRstRel);
            }

            if (!"00".equals(iqpLoanApp.getGuarWay())) {
                try {
                    log.info("调用信贷授信协议信息同步开始");
                    guarBusinessRelService.sendLmtinf("01", iqpLoanApp.getIqpSerno());
                    log.info("调用信贷授信协议信息同步结束");
                } catch (Exception e) {
                    log.error("调用信贷授信协议信息同步失败" + e);
                }

                try {
                    log.info("调用根据业务流水号同步押品与业务关联信息开始");
                    guarBusinessRelService.sendBusconXw(iqpLoanApp.getIqpSerno(), "01");
                    log.info("调用根据业务流水号同步押品与业务关联信息结束");
                } catch (Exception e) {
                    log.error("调用根据业务流水号同步押品与业务关联信息失败" + e);
                }
            }
        } catch (Exception e) {
            log.error("******************小微-合同创建异常：【{}】", e.getMessage());
            throw BizException.error(null, "9999", e.getMessage());
        }
        log.info("******************小微-合同申请： 提交结束 ******************", iqpLoanApp.getIqpSerno());
        return new ResultDto(1);
    }


    /**
     * @param ctrLoanCont
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/29 21:09
     * @version 1.0.0
     * @desc 合同生成-前往额度执行额度占用
     * @修改历史: 修改时间: 2021年6月16日21:51:28    修改人员：hubp    修改原因：修改部分参数
     */
    public ResultDto retail(CtrLoanCont ctrLoanCont) {
        ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = null;
        try {
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(ctrLoanCont.getIqpSerno());
            //1. 向额度系统发送接口：额度校验
            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0009ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0009ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0009ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0009ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0009ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0009ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0009ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
            cmisLmt0009ReqDto.setOrigiDealBizNo(ctrLoanCont.getIqpSerno());//原交易业务编号
            cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
            cmisLmt0009ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
//                cmisLmt0009ReqDto.setCurType(ctrLoanCont.getCurType());//币种
//                cmisLmt0009ReqDto.setExchgRate(new BigDecimal(0));//汇率
            cmisLmt0009ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0009ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0009OccRelListReqDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getContHighAvlAmt());//占用敞口(折人民币)


            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
            log.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
            ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
            log.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());

            String code = cmisLmt0009RespDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                return new ResultDto(null).code(9999).message(cmisLmt0009RespDto.getData().getErrorMsg());
            }
            //2. 向额度系统发送接口：占用额度
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
            //cmisLmt0011ReqDto.setOrigiDealBizNo(mainInfo.getSurveySerno());//原交易业务编号
            //cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            //cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            //cmisLmt0011ReqDto.setOrigiRecoverType("1");//原交易属性
            //cmisLmt0011ReqDto.setDealBizAmt(ctrLoanCont.getContAmt());//交易业务金额
            //cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            //cmisLmt0011ReqDto.setCurType(ctrLoanCont.getCurType());//币种
            //cmisLmt0011ReqDto.setExchgRate(new BigDecimal(0));//汇率
            //cmisLmt0011ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            //cmisLmt0011ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态  TODO 王浩 2021年8月2日16:42:22 这里必须传200  不然会发生后续其他问题 谨记
            cmisLmt0011ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(ctrLoanCont.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmt(iqpLoanApp.getContHighAvlAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpLoanApp.getContHighAvlAmt());//占用敞口(折人民币)


            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
            log.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011ReqDto);
            cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("根据业务申请编号【" + ctrLoanCont.getContNo() + "】前往额度系统-额度占用返回报文：" + cmisLmt0011RespDto.toString());
        } catch (Exception e) {
            log.error("零售-合同申请签订异常：", e);
            return new ResultDto().code(9999).message(e.getMessage());
        }
        return cmisLmt0011RespDto;
    }

    /**
     * @param contNo
     * @return java.util.Maps
     * @author shenli
     * @date 2021/4/26 0026 21:23
     * @version 1.0.0
     * @desc 小微-合同管理：合同重签
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map contReSignXw(String contNo) {

        log.info("******************小微-合同申请： 合同重签开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //1.存在审批中，审批通过的放款申请，拦截
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("flag1", true);//是否存在审批中，审批通过标志位
            int pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            if (pvpLoanAppCount > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在审批中与审批通过的放款申请，不可重签");
                return returnMap;

            }

            //2. 将主合同与担保合同（最高额除外）状态刷新为，“未生效”
            ctrloancont.setContStatus("100");//100:为生效
            ctrLoanContMapper.updateByPrimaryKey(ctrloancont);//更新合同
            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            for (int i = 0; i < grtGuarContList.size(); i++) {
                GrtGuarCont grtguarcont = grtGuarContList.get(i);
                if ("00".equals(grtguarcont.getGuarContType())) {//担保合同类型 为一般担保合同 00
                    grtguarcont.setGuarContState("100");//未生效，码值没有确定最终版
                    if (StringUtils.isEmpty(grtguarcont.getBizLine())) {
                        log.error("担保合同【" + grtguarcont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtguarcont);
                }
            }

        } catch (BizException e) {
            returnMap.put("code", "9999");
            log.error("小微-合同申请-合同重签异常：", e);
            throw BizException.error(null, "9999", e.getMessage());
        }
        log.info("******************小微-合同申请： 合同重签结束 ******************", contNo);
        return returnMap;
    }

    /**
     * @param contNo
     * @return java.util.Map
     * @author shenli
     * @date 2021/4/26 0026 22:12
     * @version 1.0.0
     * @desc 小微-合同管理：合同注销
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map cancelContXw(String contNo) {
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        log.info("******************小微-合同申请： 合同注销开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //1. 不准存在待发起，审批中,退回的放款申请
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("oprType","01");//新增
            pvpQueryMap.put("flag2", true);//是否存在审批中，审批通过标志位
            int pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            if (pvpLoanAppCount > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在待发起，审批中的放款申请，不可注销");
                return returnMap;

            }

            //2. 不准审批通过的放款申请但未出账的
            pvpQueryMap.clear();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("flag3", true);//是否存在审批通过标志位
            pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            int accLoanCount = accLoanMapper.selectAccLoanCountByContNobyxw(contNo);
            if (pvpLoanAppCount > 0 && accLoanCount > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在在途的放款申请及合同下存在未结清的借据，不可注销");
                return returnMap;

            }


            //3. 根据合同编号查询贷款余额大于0的台账笔数
            if (accLoanMapper.selectAccLoanBalanceByContNo(contNo) > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在贷款余额大于0的台账，不可注销");
                return returnMap;

            }
            //4. 解除额度占用
            CmisLmt0012ReqDto req = new CmisLmt0012ReqDto();
            req.setSysId(EsbEnum.SERVTP_XDG.key);// 系统编号
            req.setInstuCde(CmisCommonUtils.getInstucde(ctrloancont.getManagerBrId()));// 金融机构代码
            req.setBizNo(contNo);// 合同编号
            req.setRecoverType("06");// 恢复类型 01-结清注销 02-未用注销 05-押品出库 06-撤销占用
            req.setRecoverSpacAmtCny(ctrloancont.getContAmt());// 恢复敞口金额(人民币)
            req.setRecoverAmtCny(ctrloancont.getContAmt());// 恢复总额(人民币)
            req.setInputId(ctrloancont.getInputId());// 登记人
            req.setInputBrId(ctrloancont.getInputBrId());// 登记机构
            req.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 登记日期

            log.info("根据合同编号【" + ctrloancont.getContNo() + "】前往额度系统-额度恢复请求报文：" + req.toString());
            ResultDto<CmisLmt0012RespDto> resp = cmisLmtClientService.cmisLmt0012(req);
            log.info("根据合同编号【" + ctrloancont.getContNo() + "】前往额度系统-额度恢复返回报文：" + resp.toString());

            String code = resp.getData().getErrorCode();
            if (!"0000".equals(code)) {

                log.info("根据合同编号【{}】,前往额度系统-额度恢复失败！", ctrloancont.getContNo());
                returnMap.put("code", "9999");
                returnMap.put("msg", "零售-合同申请签订占用额度异常：" + resp.getData().getErrorMsg());
                return returnMap;

            }

            //5. 将主合同与担保合同（最高额除外）状态刷新为，“注销”
            ctrloancont.setContStatus("600");// STD_ZB_CONT_TYP 600:注销
            ctrLoanContMapper.updateByPrimaryKey(ctrloancont);//更新合同
            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            for (int i = 0; i < grtGuarContList.size(); i++) {
                GrtGuarCont grtguarcont = grtGuarContList.get(i);
                if ("00".equals(grtguarcont.getGuarContType())) {//担保合同类型 为一般担保合同 00
                    grtguarcont.setGuarContState("104");//注销，码值没有确定最终版
                    if (StringUtils.isEmpty(grtguarcont.getBizLine())) {
                        log.error("担保合同【" + grtguarcont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtguarcont);
                }
            }

            // 6、惠享贷、优企贷注销共同借款人签约信息 SC010008--优企贷 SC060001--惠享贷 1-未签约 2-已签约
            if (Objects.equals("SC010008", ctrloancont.getPrdId()) || Objects.equals("SC060001", ctrloancont.getPrdId())) {
                // 查询共借人信息
                log.info("根据调查流水号【{}】查询共同借款人信息开始！", ctrloancont.getSurveySerno());
                List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(ctrloancont.getSurveySerno());
                log.info("根据调查流水号【{}】查询共同借款人信息结束！", ctrloancont.getSurveySerno());

                log.info("根据调查流水号【{}】更新共同借款人信息为【未签约】开始！", ctrloancont.getSurveySerno());
                if (CollectionUtils.nonEmpty(lmtCobInfoList)) {
                    lmtCobInfoList.stream().forEach(lmtCobInfo -> {
                        // 签约方式由已签约更新为未签约
                        lmtCobInfo.setSignState("1");
                        lmtCobInfo.setUpdateTime(new Date());
                        lmtCobInfoService.update(lmtCobInfo);
                    });
                }
                log.info("根据调查流水号【{}】更新共同借款人信息为【未签约】结束！", ctrloancont.getSurveySerno());
            }


        } catch (Exception e) {
            returnMap.put("code", "9999");
            returnMap.put("msg", "合同注销异常！" + e.getMessage());
            log.error("小微-合同申请-合同注销异常：", e);
        }

        log.info("******************小微-合同申请： 合同注销结束 ******************", contNo);

        return returnMap;

    }


    /**
     * @param contNo
     * @return java.util.Map
     * @author shenli
     * @date 2021/4/26 0026 22:12
     * @version 1.0.0
     * @desc 小微-合同管理：合同打印（电子骑缝章）
     * @修改历史: 修改时间:2021年6月17日20:43:44    修改人员：hubp    修改原因:修改挡板参数
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto contdoPrintingSeal1Xw(String contNo) {
        log.info("******************小微-合同申请： 合同打印（电子骑缝章）开始 ******************", contNo);
        ResultDto resultDto = null;
        try {
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            if (null == ctrLoanCont) {
                return new ResultDto(null).message("合同编号异常");
            }
            String cs = ctrLoanCont.getContStatus(); // 合同状态
            if (StringUtils.isBlank(cs) || "200".equals(cs) || "600".equals(cs)) {
                return new ResultDto(null).message("合同状态为生效、注销、重新签订禁止打印");
            }
            resultDto = new ResultDto(1);
        } catch (Exception e) {
            log.error("******************小微-合同申请： 合同打印（电子骑缝章）异常 ******************", contNo, e.getMessage());
            return new ResultDto(null).code(-1).message(e.getMessage());
        } finally {
            log.info("******************小微-合同申请： 合同打印（电子骑缝章）结束 ******************", contNo);
        }
        return resultDto;
    }


    /**
     * @param contNo
     * @return java.util.Map
     * @author shenli
     * @date 2021/4/26 0026 22:12
     * @version 1.0.0
     * @desc 小微-合同管理：合同打印（银行电子公章）
     * @修改历史: 修改时间:2021年6月17日15:32:42    修改人员：hubp    修改原因:修改挡板参数
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto contdoPrintingSeal2Xw(String contNo) {
        log.info("******************小微-合同申请： 合同打印（银行电子公章）开始 ******************", contNo);
        try {
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            if (null == ctrLoanCont) {
                return new ResultDto(null).code(9999).message("合同编号异常");
            }
            String cs = ctrLoanCont.getContStatus(); // 合同状态
            String sas = ctrLoanCont.getSignApproveStatus(); //签章审批状态
            if (StringUtils.isBlank(cs) || "200".equals(cs) || "600".equals(cs)) {
                return new ResultDto(null).code(9999).message("合同状态为生效、注销、重新签订禁止打印");
            }

            if (StringUtils.isBlank(sas) || "998".equals(sas) || "000".equals(sas)) { // 否决 或空
                return new ResultDto(1).message("进入流程提交页面，发起签章审批申请");
            } else if ("992".equals(sas)) { // 打回
                return new ResultDto(2).message("进入流程提交页面，提交签章审批申请");
            } else if ("111".equals(sas)) { // 审批中
                return new ResultDto(3).message("签章审批中禁止打印");
            } else if ("997".equals(sas)) { // 审批通过
                return new ResultDto(0);
            }
        } catch (Exception e) {
            log.error("******************小微-合同申请： 合同打印（银行电子公章）异常 ******************", contNo, e.getMessage());
            return new ResultDto(null).code(9999).message(e.getMessage());
        } finally {
            log.info("******************小微-合同申请： 合同打印（银行电子公章）结束 ******************", contNo);
        }
        return new ResultDto(4).message("签章审批状态异常");
    }


    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author shenli
     * @date 2021/5/4 0004 16:17
     * @version 1.0.0
     * @desc 零售-合同申请列表 flag：1-待完善。2-待签订，3-已签订
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanCont> qyeryCtrLoanContListRetail(QueryModel queryModel) {

        List<CtrLoanCont> list = null;
        try {
            String flag = (String) queryModel.getCondition().get("flag");
            PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
            queryModel.addCondition("belgLine", "02");//02 零售业务
            queryModel.addCondition("oprType", "01");//01 新增
            if ("1".equals(flag)) {//待完善
                queryModel.addCondition("contStatus", "100");
                queryModel.addCondition("applyExistsStatus", "000,992");
            } else if ("2".equals(flag)) {//待签订-未生效
                queryModel.addCondition("contStatus", "100");
                queryModel.addCondition("approveStatus", "997");
            } else if ("3".equals(flag)) {//已签订-已生效
                queryModel.addCondition("contStatus", "200");
                queryModel.addCondition("approveStatus", "997");
            }

            if (null != queryModel.getCondition().get("inputDate") && queryModel.getCondition().get("inputDate") instanceof List) {
                List<String> datelist = (List<String>) queryModel.getCondition().get("inputDate");
                queryModel.getCondition().put("inputStartDate", datelist.get(0));
                queryModel.getCondition().put("inputEndDate", datelist.get(1));
                list = ctrLoanContMapper.selectByModelCtr(queryModel);
            } else {
                list = ctrLoanContMapper.selectByModelCtr(queryModel);
            }
            PageHelper.clearPage();

        } catch (Exception e) {
            log.error("零售-合同申请列表异常：", e);
        }

        return list;
    }

    /**
     * @return
     * @方法名称: selectCtrCont
     * @方法描述: 贷款出账申请新增弹框中通过查询合同选择列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrLoanContDto> selectCtrCont(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanContDto> ctrList = ctrLoanContMapper.selectCtrLoanContWithPvpLoanApp(model);
        PageHelper.clearPage();
        return ctrList;
    }
    /**
     * @return
     * @方法名称: selectCtrLoanContAll
     * @方法描述: 贷款出账申请关联合同信息 (保函对公,零售,小微)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrLoanContDto> selectCtrLoanContAll(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanContDto> ctrList = ctrLoanContMapper.selectCtrLoanContAll(model);
        PageHelper.clearPage();
        return ctrList;
    }

    /**
     * @return
     * @方法名称: getContInfobyPvpContNo
     * @方法描述: 根据贷款出账表合同号获取合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrLoanContEdit getContInfobyPvpContNo(Map params) {
        String contNo = (String) params.get("contNo");
        CtrLoanContEdit ctrLoanContEdit = new CtrLoanContEdit();
        if (contNo == null || "".equals(contNo)) {
            return ctrLoanContEdit;
        } else {
            ctrLoanContEdit.setCtrLoanCont(ctrLoanContMapper.getContInfobyPvpContNo(contNo));
        }
        return ctrLoanContEdit;
    }

    /**
     * @param ctrLoanContRetailDto
     * @return Map
     * @author shenli
     * @date 2021/5/5 0005 9:58
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-待完善： 保存功能
     * 更新合同信息，并且根据是否存在担保合同，将合同状态更新为“未生效”
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map updateCtrLoanContRetail(CtrLoanContRetailDto ctrLoanContRetailDto) {

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            CtrLoanCont ctrLoanCont = new CtrLoanCont();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrLoanContRetailDto, ctrLoanCont);
            String contMode = ctrLoanCont.getContMode();//合同模式

            String orgType = ctrLoanContRetailDto.getOrgType();
            String isOlPld = ctrLoanContRetailDto.getIsOlPld();

            if ("01".equals(contMode)) {//合同模式为"生成打印模式"，异地
                if ("1".equals(orgType) || "2".equals(orgType) || "3".equals(orgType)) {
                    ctrLoanCont.setContStatus("100");//更新合同状为“未生效”
                    ctrLoanCont.setApproveStatus("997");

                }

                if (!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) && !"1".equals(isOlPld)) {
                    ctrLoanCont.setContStatus("100");//更新合同状为“未生效”
                    ctrLoanCont.setApproveStatus("997");

                }

            } else if ("02".equals(contMode)) {//线上签订模式
                ctrLoanCont.setContStatus("100");//更新合同状为“未生效”
                ctrLoanCont.setApproveStatus("997");
            }
            ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
            //发送至额度系统跟新额度视图
            changelmt(ctrLoanCont.getContNo());
        } catch (Exception e) {
            log.error("零售-合同申请保存异常：", e);
            returnMap.put("code", "9999");
            returnMap.put("msg", "零售-合同申请保存异常！");
        }
        return returnMap;
    }

    /**
     * @param signCtrLoanContDto
     * @return Map
     * @author shenli
     * @date 2021/5/5 0005 9:58
     * @version 1.0.0
     * @desc 零售-合同申请管理-合同申请列表-待签约：签订
     * 合同状态更新为“已生效”
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map signCtrLoanContRetail(SignCtrLoanContDto signCtrLoanContDto) {

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {
            CtrLoanCont ctrLoanCont = signCtrLoanContDto.getCtrLoanCont();
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(ctrLoanCont.getIqpSerno());


            //1. 向额度系统发送接口：额度校验
            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0009ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0009ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
            cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
            cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
            cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
            cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
            cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
            cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
            cmisLmt0009ReqDto.setOrigiDealBizNo(iqpLoanApp.getIqpSerno());//原交易业务编号
            cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
            cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
            cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
            cmisLmt0009ReqDto.setDealBizAmt(ctrLoanCont.getCvtCnyAmt());//交易业务金额
            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
            cmisLmt0009ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
            cmisLmt0009ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
            cmisLmt0009ReqDto.setBelgLine(CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_04);//零售
            cmisLmt0009ReqDto.setInputId(iqpLoanApp.getInputId());
            cmisLmt0009ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
            cmisLmt0009ReqDto.setInputDate(iqpLoanApp.getInputDate());
            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
            cmisLmt0009OccRelListReqDto.setLmtType("01");//额度类型
            cmisLmt0009OccRelListReqDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
            cmisLmt0009OccRelListReqDto.setBizTotalAmt(ctrLoanCont.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0009OccRelListReqDto.setBizSpacAmt(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)
            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
            cmisLmt0009ReqDto.setBussStageType("02");//合同阶段


            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
            ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());

            String code = cmisLmt0009RespDto.getData().getErrorCode();

            if ("0000".equals(code)) {
                //1. 向额度系统发送接口：占用额度
                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(ctrLoanCont.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(ctrLoanCont.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(ctrLoanCont.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(ctrLoanCont.getContType());//交易业务类型
                cmisLmt0011ReqDto.setBizAttr("1");//交易属性
                cmisLmt0011ReqDto.setPrdId(ctrLoanCont.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(ctrLoanCont.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(ctrLoanCont.getIqpSerno());//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiRecoverType("1");//原交易属性
                cmisLmt0011ReqDto.setDealBizAmt(ctrLoanCont.getCvtCnyAmt());//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                cmisLmt0011ReqDto.setStartDate(ctrLoanCont.getContStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(ctrLoanCont.getContEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
                cmisLmt0011ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
                cmisLmt0011ReqDto.setBelgLine("04");//零售条线
                cmisLmt0011ReqDto.setBussStageType("02");//合同阶段


                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType("01");//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getReplyNo());//额度分项编号
                cmisLmt0011OccRelListDto.setBizTotalAmt(ctrLoanCont.getCvtCnyAmt());//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmt(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(ctrLoanCont.getCvtCnyAmt());//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011ReqDto.toString());
                ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011RespDto.toString());

                code = cmisLmt0011RespDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.info("根据业务申请编号【{}】,前往额度系统-额度占用失败！", ctrLoanCont.getIqpSerno());
                    returnMap.put("code", "9999");
                    returnMap.put("msg", "零售-合同申请签订占用额度异常：" + cmisLmt0011RespDto.getData().getErrorMsg());
                    return returnMap;

                }
            } else if (!"0000".equals(code) && !"70125".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-额度校验失败！", ctrLoanCont.getIqpSerno());
                returnMap.put("code", "9999");
                returnMap.put("msg", "零售-合同申请签订占用额度异常：" + cmisLmt0009RespDto.getData().getErrorMsg());
                return returnMap;
            }

            //2. 额度占用成功后。更新合同状态为“已生效”
            ctrLoanCont.setContStatus("200");//更新合同状为“已生效”
            ctrLoanCont.setSignDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);

            //将关联的担保合同的合同状态更新为“已生效”
            List<GrtGuarCont> grtGuarContList = signCtrLoanContDto.getGrtGuarContList();
            if (grtGuarContList != null && grtGuarContList.size() > 0) {
                for (int i = 0; i < grtGuarContList.size(); i++) {
                    GrtGuarCont grtGuarCont = grtGuarContList.get(i);
                    grtGuarCont.setGuarContState("101");//生效,码值没有确定最终版
                    if (StringUtils.isEmpty(grtGuarCont.getBizLine())) {
                        log.error("担保合同【" + grtGuarCont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtGuarCont);
                    Map<String, String> map = new HashMap<>();
                    map.put("bizType", "02");
                    map.put("isFlag", "1");
                    map.put("bizSerno", ctrLoanCont.getIqpSerno());
                    map.put("guarContNo", grtGuarCont.getGuarContNo());
                    guarBaseInfoService.buscon(map);
                }
            }

            if ("022028".equals(iqpLoanApp.getPrdId())) {
                try {
                    // 生成归档任务
                    log.info("开始系统生成档案归档信息");
                    String cusId = ctrLoanCont.getCusId();
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                    docArchiveClientDto.setDocType("16");//16:白领易贷通业务
                    docArchiveClientDto.setBizSerno(ctrLoanCont.getIqpSerno());
                    docArchiveClientDto.setCusId(cusId);
                    docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                    docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                    docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                    docArchiveClientDto.setManagerId(ctrLoanCont.getManagerId());
                    docArchiveClientDto.setManagerBrId(ctrLoanCont.getManagerBrId());
                    docArchiveClientDto.setInputId(ctrLoanCont.getInputId());
                    docArchiveClientDto.setInputBrId(ctrLoanCont.getInputBrId());
                    docArchiveClientDto.setContNo(ctrLoanCont.getContNo());
                    docArchiveClientDto.setLoanAmt(ctrLoanCont.getContAmt());
                    docArchiveClientDto.setStartDate(ctrLoanCont.getContStartDate());
                    docArchiveClientDto.setEndDate(ctrLoanCont.getContEndDate());
                    docArchiveClientDto.setPrdId(ctrLoanCont.getPrdId());
                    docArchiveClientDto.setPrdName(ctrLoanCont.getPrdName());
                    int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                    if (num < 1) {
                        log.info("系统生成白领易贷通业务档案归档信息失败,业务流水号[{}]", ctrLoanCont.getIqpSerno());
                    }

                } catch (Exception e) {
                    log.info("系统生成档案归档信息失败" + e);
                }
            }


            if (!"00".equals(iqpLoanApp.getGuarWay())) {

                try {
                    log.info("调用信贷业务合同信息同步开始");
                    guarBusinessRelService.sendBusinf("02", "02", ctrLoanCont.getContNo());
                    log.info("调用信贷业务合同信息同步结束");
                } catch (Exception e) {
                    log.info("调用信贷业务合同信息同步失败" + e);
                }

                try {
                    log.info("调用根据业务流水号同步押品与业务关联信息开始");
                    // 请求对象信息
                    BusconReqDto busconReqDto = new BusconReqDto();
                    // 循环体
                    List list = new ArrayList();
                    // 根据业务流水号查询押品列表信息
                    QueryModel queryModel = new QueryModel();
                    queryModel.getCondition().put("serno", ctrLoanCont.getIqpSerno());
                    List<GuarBizRel> guarBizRelList = guarBizRelService.selectAll(queryModel);

                    if (CollectionUtils.isEmpty(guarBizRelList)) {
                        log.info("根据业务流水号【{}】未查询到押品信息，所属业务类型【{}】", ctrLoanCont.getIqpSerno(), "02");
                    } else {
                        // 拼装请求报文
                        guarBizRelList.stream().forEach(s -> {
                            // 单循环体
                            cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List busconList = new cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List();
                            // 业务编号
                            busconList.setYwbhyp(ctrLoanCont.getContNo());
                            // 业务类型
                            busconList.setYwlxyp("02");
                            // 押品统一编号
                            busconList.setYptybh(s.getGuarNo());
                            // 是否有效 1-有效；0-无效
                            busconList.setIsflag("1");
                            list.add(busconList);
                        });
                        busconReqDto.setList(list);
                        // 请求押品系统
                        busconService.buscon(busconReqDto);
                    }
                    log.info("调用根据业务流水号同步押品与业务关联信息结束");
                } catch (Exception e) {
                    log.info("调用根据业务流水号同步押品与业务关联信息失败" + e);
                }


                if (grtGuarContList != null && grtGuarContList.size() > 0) {
                    for (int i = 0; i < grtGuarContList.size(); i++) {
                        GrtGuarCont grtGuarCont = grtGuarContList.get(i);

                        try {
                            log.info("调用信贷担保合同信息同步开始");
                            guarBusinessRelService.sendConinf("02", grtGuarCont.getGuarContNo());
                            log.info("调用信贷担保合同信息同步结束");
                        } catch (Exception e) {
                            log.info("调用信贷担保合同信息同步失败" + e);
                        }


                        try {
                            log.info("调用根据担保合同编号同步担保合同与押品关系开始");
                            guarBusinessRelService.sendContra(grtGuarCont.getGuarContNo());
                            log.info("调用根据担保合同编号同步担保合同与押品关系结束");
                        } catch (Exception e) {
                            log.info("调用根据担保合同编号同步担保合同与押品关系失败" + e);
                        }

                    }
                }
            }

        } catch (Exception e) {
            log.error("零售-合同申请签订异常：", e);
            returnMap.put("code", "9999");
            returnMap.put("msg", "零售-合同申请签订异常！");
        }
        return returnMap;
    }


    /**
     * @param contNo
     * @return java.util.Map
     * @author shenli
     * @date 2021-5-5 11:15:44
     * @version 1.0.0
     * @desc 零售-合同申请管理：合同打印
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map contPrintingRetail(String contNo) {
        log.info("******************零售-合同申请管理： 合同打印 开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {
            returnMap.put("code", "9999");
            returnMap.put("msg", "尚在开发中");

        } catch (Exception e) {
            returnMap.put("code", "9999");
            returnMap.put("msg", "零售-合同申请管理-合同打印异常！");
            log.error("零售-合同申请管理-合同打印异常：", e);
        }

        log.info("******************零售-合同申请管理： 合同打印 结束 ******************", contNo);

        return returnMap;

    }


    /**
     * @param contNo
     * @return java.util.Maps
     * @author shenli
     * @date 2021-5-5 14:03:57
     * @version 1.0.0
     * @desc 零售-合同申请管理-已签订 ：撤销
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map contReSignRetail(String contNo) {

        log.info("******************零售-合同申请管理-已签订：撤销 开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //1.存在审批中，审批通过的放款申请，拦截
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("oprType", "01");
            pvpQueryMap.put("flag1", true);//是否存在审批中，审批通过标志位
            int pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            if (pvpLoanAppCount > 0) {

                log.info("根据业务申请编号【{}】,前往额度系统-额度恢复失败！", ctrloancont.getIqpSerno());
                returnMap.put("code", "9999");
                returnMap.put("msg", "存在审批中与审批通过的放款申请，不可重签");
                return returnMap;

            }

            //2. 向额度系统发送接口：合同恢复占用
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrloancont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrloancont.getContNo());//业务编号
            cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(ctrloancont.getCvtCnyAmt());// 恢复敞口金额(人民币)
            cmisLmt0012ReqDto.setRecoverAmtCny(ctrloancont.getCvtCnyAmt());//恢复总额(人民币)
            cmisLmt0012ReqDto.setInputId(ctrloancont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrloancont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            cmisLmt0012ReqDto.setIsRecoverCoop("0");

            log.info("根据业务申请编号【" + ctrloancont.getIqpSerno() + "】前往额度系统-额度恢复请求报文：" + cmisLmt0012ReqDto.toString());
            ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("根据业务申请编号【" + ctrloancont.getIqpSerno() + "】前往额度系统-额度恢复返回报文：" + cmisLmt0012RespDto.toString());

            String code = cmisLmt0012RespDto.getData().getErrorCode();
            if (!"0000".equals(code)) {

                log.info("根据业务申请编号【{}】,前往额度系统-额度恢复失败！", ctrloancont.getIqpSerno());
                returnMap.put("code", "9999");
                returnMap.put("msg", "零售-合同申请签订占用额度异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
                return returnMap;

            }

            //3. 将主合同与担保合同（最高额除外）状态刷新为，“未生效”
            ctrloancont.setContStatus("100");//100:未生效
            ctrLoanContMapper.updateByPrimaryKey(ctrloancont);//更新合同
            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(ctrloancont.getIqpSerno());
            if (grtGuarContList == null || grtGuarContList.size() == 0) {
                grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            }
            for (int i = 0; i < grtGuarContList.size(); i++) {
                GrtGuarCont grtguarcont = grtGuarContList.get(i);
                if ("A".equals(grtguarcont.getGuarContType())) {//担保合同类型 为一般担保合同 A
                    grtguarcont.setGuarContState("100");//未生效，码值没有确定最终版
                    if (StringUtils.isEmpty(grtguarcont.getBizLine())) {
                        log.error("担保合同【" + grtguarcont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtguarcont);
                }
            }

        } catch (Exception e) {
            returnMap.put("code", "9999");
            log.error("零售-合同申请管理-已签订：撤销异常：", e);
        }

        log.info("******************零售-合同申请管理-已签订：撤销 结束 ******************", contNo);

        return returnMap;

    }


    /**
     * @param contNo
     * @return java.util.Map
     * @author shenli
     * @date 2021-5-5 14:18:06
     * @version 1.0.0
     * @desc 零售-合同申请管理-已签订 ：作废
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Map cancelContRetail(String contNo) {
        log.info("******************零售-合同申请管理-已签订 ：作废 开始 ******************", contNo);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //1. 不准存在待发起，审批中的放款申请
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("oprType", "01");
            pvpQueryMap.put("flag2", true);//是否存在审批中，审批通过标志位
            int pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            if (pvpLoanAppCount > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在待发起，审批中的放款申请，不可注销");
                return returnMap;

            }

            //2. 不准审批通过的放款申请但未出账的
            pvpQueryMap.clear();
            pvpQueryMap.put("contNo", contNo);
            pvpQueryMap.put("oprType", "01");
            pvpQueryMap.put("flag3", true);//是否存在审批通过标志位
            pvpLoanAppCount = pvpLoanAppMapper.selectPvpLoanAppCountByContNo(pvpQueryMap);
            int accLoanCount = accLoanMapper.selectAccLoanCountByContNo(contNo);
            if (pvpLoanAppCount > 0 && accLoanCount > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在审批通过的放款申请但未出账的，不可注销");
                return returnMap;

            }


            //3. 根据合同编号查询贷款余额大于0的台账笔数
            if (accLoanMapper.selectAccLoanBalanceByContNo(contNo) > 0) {

                returnMap.put("code", "9999");
                returnMap.put("msg", "存在贷款余额大于0的台账，不可注销");
                return returnMap;

            }

            //4. 向额度系统发送接口：合同恢复占用
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(ctrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrLoanCont.getContNo());//业务编号
            cmisLmt0012ReqDto.setRecoverType("02");//恢复类型
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(ctrLoanCont.getCvtCnyAmt());// 恢复敞口金额(人民币)
            cmisLmt0012ReqDto.setRecoverAmtCny(ctrLoanCont.getCvtCnyAmt());//恢复总额(人民币)
            cmisLmt0012ReqDto.setInputId(ctrLoanCont.getInputId());
            cmisLmt0012ReqDto.setInputBrId(ctrLoanCont.getInputBrId());
            cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            cmisLmt0012ReqDto.setIsRecoverCoop("0");

            log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度注销请求报文：" + cmisLmt0012ReqDto.toString());
            ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度注销返回报文：" + cmisLmt0012RespDto.toString());

            if (!"0000".equals(cmisLmt0012RespDto.getData().getErrorCode())) {
                log.info("根据业务申请编号【{}】,前往额度系统-额度注销失败！", ctrLoanCont.getIqpSerno());

                returnMap.put("code", "9999");
                returnMap.put("msg", "零售-合同作废,额度恢复异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
                return returnMap;
            }


            //5. 向额度系统发送接口：第三方恢复占用
//            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(ctrLoanCont.getIqpSerno());
//            String thirdPartyFlag = iqpLoanApp.getThirdPartyFlag();//第三方标识
//            if ("1".equalsIgnoreCase(thirdPartyFlag)) {
//                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
//                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerBrId));//金融机构代码
//                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getIqpSerno());//业务编号
//                cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
//                cmisLmt0012ReqDto.setRecoverSpacAmtCny(iqpLoanApp.getAppAmt());// 恢复敞口金额(人民币)
//                cmisLmt0012ReqDto.setRecoverAmtCny(iqpLoanApp.getAppAmt());//恢复总额(人民币)
//                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
//                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
//                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
//
//                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用请求报文：" + cmisLmt0012ReqDto.toString());
//                cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
//                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用返回报文：" + cmisLmt0012RespDto.toString());
//
//                if (!"0000".equals(cmisLmt0012RespDto.getData().getErrorCode())) {
//                    returnMap.put("code", "9999");
//                    returnMap.put("msg", "零售-合同作废,第三方恢复占用异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
//                    return returnMap;
//                }
//            }

            //6. 将主合同与担保合同（最高额除外）状态刷新为，“注销”
            ctrLoanCont.setContStatus("600");// STD_ZB_CONT_TYP 600:注销
            ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);//更新合同


            //不存在电子用印的情况下，需要作废档案归档任务
            if (!"1".equals(ctrLoanCont.getIsESeal())) {
                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务开始");
                try {
                    docArchiveInfoService.invalidByBizSerno(ctrLoanCont.getIqpSerno(),"");
                } catch (Exception e) {
                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                }
                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】需要作废档案归档任务结束");
            }


            //7. 通知押品系统
            if (!"00".equals(ctrLoanCont.getGuarWay())) {
                try {
                    log.info("调用信贷业务合同信息同步开始-作废");
                    guarBusinessRelService.sendBusinf("02", "02", ctrLoanCont.getContNo());
                    log.info("调用信贷业务合同信息同步结束-作废");
                } catch (Exception e) {
                    log.info("调用信贷业务合同信息同步失败-作废" + e);
                }
            }

            //根据合同编号查询担保合同
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(ctrLoanCont.getIqpSerno());
            if (grtGuarContList == null || grtGuarContList.size() == 0) {
                grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            }
            for (int i = 0; i < grtGuarContList.size(); i++) {
                GrtGuarCont grtGuarCont = grtGuarContList.get(i);
                if ("A".equals(grtGuarCont.getGuarContType())) {//担保合同类型 为一般担保合同 00
                    grtGuarCont.setGuarContState("104");//注销，码值没有确定最终版
                    if (StringUtils.isEmpty(grtGuarCont.getBizLine())) {
                        log.error("担保合同【" + grtGuarCont.getGuarContNo() + "】的业务条线为空");
                    }
                    grtGuarContMapper.updateByPrimaryKey(grtGuarCont);

                    try {
                        log.info("调用信贷担保合同信息同步开始-生效");
                        guarBusinessRelService.sendConinf("02", grtGuarCont.getGuarContNo());
                        log.info("调用信贷担保合同信息同步结束-生效");
                    } catch (Exception e) {
                        log.info("调用信贷担保合同信息同步失败" + e);
                    }

                }
            }

        } catch (Exception e) {
            returnMap.put("code", "9999");
            returnMap.put("msg", "合同作废异常！");
            log.error("零售-合同申请管理-已签订 ：作废异常：", e);
        }

        log.info("******************零售-合同申请管理-已签订 ：作废 结束 ******************", contNo);

        return returnMap;

    }


    /**
     * @param contNo
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author shenli
     * @date 2021-5-6
     * @version 1.0.0
     * @desc 零售-合同申请管理-签订   根据合同编号，查询合同基本信息，关联担保合同列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map<String, Object> queryContInfoRetail(String contNo) {

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {

            //根据合同编号查询合同信息
            CtrLoanCont ctrloancont = ctrLoanContMapper.selectByPrimaryKey(contNo);

            //合同信息
            returnMap.put("CtrLoanCont", ctrloancont);

            //担保合同列表
            List<GrtGuarCont> grtGuarContList = grtGuarContService.selectDataBySerno(ctrloancont.getIqpSerno());
            if (grtGuarContList == null || grtGuarContList.size() == 0) {
                grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
            }
            returnMap.put("GrtGuarContList", grtGuarContList);

        } catch (Exception e) {
            log.error("零售-合同管理-详细信息查询异常：", e);
        }
        return returnMap;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.domain.CtrLoanCont
     * @author 王玉坤
     * @date 2021/5/5 19:05
     * @version 1.0.0
     * @desc 零售生成主合同信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public CtrLoanCont insertCtrLoanContInfo(IqpLoanApp iqpLoanApp) throws Exception {
        // 主合同信息
        CtrLoanCont ctrLoanCont = null;
        // 合同编号
        String contNo = null;
        int nums = 0;
        try {
            ctrLoanCont = new CtrLoanCont();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanApp, ctrLoanCont);
            // 生成合同流水号
            contNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, new HashMap<>());
            ctrLoanCont.setContNo(contNo);
            ctrLoanCont.setUpdateTime(DateUtils.getCurrDate());
            ctrLoanCont.setCreateTime(DateUtils.getCurrDate());
            nums = this.insert(ctrLoanCont);
            Asserts.isTrue(nums > 0, "插入合同主表信息出错！");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return ctrLoanCont;
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 17:28
     * @注释 线上提款取用保存
     */
    public ResultDto updatabyloandirection(CtrLoanCont ctrLoanCont) {
        CtrLoanCont data = ctrLoanContMapper.selectByPrimaryKey(ctrLoanCont.getContNo());

        if (data != null) {
            data.setLoanTer(ctrLoanCont.getLoanTer());
            int i = ctrLoanContMapper.updateByPrimaryKeySelective(data);
            if (i == 1) {
                return new ResultDto(data).message("操作成功");
            }
        }
        return new ResultDto(null).message("没获取到合同信息");
    }


    /**
     * @param
     * @return void
     * @author shenli
     * @date 2021/5/8 0008 13:52
     * @version 1.0.0
     * @desc 零售-合同申请（空白合同）审批结束，更新合同状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void blankContractHandleBizEnd(CtrLoanCont ctrloancont) throws Exception {
        log.info("零售-空白合同：审批结束后业务处理逻辑开始【{}】", ctrloancont.getContNo());
        try {
            ctrloancont.setContStatus("100");//未生效
            ctrLoanContMapper.updateByPrimaryKey(ctrloancont);//更新合同
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        log.info("零售-空白合同：审批结束后业务处理逻辑结束【{}】", ctrloancont.getContNo());
    }

    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author zxz
     * @date 2021/5/11 0024 17:33
     * @version 1.0.0
     * @desc 影像审核选择合同列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanContDto> selectImageCtrLoan(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanContDto> list = ctrLoanContMapper.queryAllImageCont(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param lmtAccNo
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author zxz
     * @date 2021/5/11 0024 17:33
     * @version 1.0.0
     * @desc 授信续作合同查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanContDto> queryAllContByLmtAccNo(String lmtAccNo) {
        List<CtrLoanContDto> list = ctrLoanContMapper.queryAllContByLmtAccNo(lmtAccNo);
        return list;
    }

    /**
     * @param oldLmtAccNo 旧额度分项编号
     * @param lmtAccNo 新额度分项编号
     * @param replyNo 新批复编号
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 根据合同号更新lmtAccNo
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateLmtAccNoByOldLmtAccNo(String oldLmtAccNo, String lmtAccNo, String replyNo) {
        List<CtrLoanContDto> list = ctrLoanContMapper.queryAllContByLmtAccNo(oldLmtAccNo);
        int count = 0;
        if (CollectionUtils.nonEmpty(list)) {
            for (CtrLoanContDto ctrLoanContDto : list) {
                String bizType = ctrLoanContDto.getBizType();
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_LOAN_CONT, bizType)) {
                    // 普通贷款合同
                    CtrLoanCont ctrLoanCont = new CtrLoanCont();
                    ctrLoanCont.setContNo(ctrLoanContDto.getContNo());
                    ctrLoanCont.setLmtAccNo(lmtAccNo);
                    ctrLoanCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrLoanCont开始，参数为：{}", JSON.toJSONString(ctrLoanCont));
                    int i = ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                    log.info("根据合同号更新ctrLoanCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_HIGH_AMT_AGR_CONT, bizType)) {
                    // 最高额授信协议
                    CtrHighAmtAgrCont ctrHighAmtAgrCont = new CtrHighAmtAgrCont();
                    ctrHighAmtAgrCont.setContNo(ctrLoanContDto.getContNo());
                    ctrHighAmtAgrCont.setLmtAccNo(lmtAccNo);
                    ctrHighAmtAgrCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrHighAmtAgrCont开始，参数为：{}", JSON.toJSONString(ctrHighAmtAgrCont));
                    int i = ctrHighAmtAgrContService.updateLmtAccNoByContNo(ctrHighAmtAgrCont);
                    log.info("根据合同号更新ctrHighAmtAgrCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_ACCP_CONT, bizType)) {
                    // 银承合同
                    CtrAccpCont ctrAccpCont = new CtrAccpCont();
                    ctrAccpCont.setContNo(ctrLoanContDto.getContNo());
                    ctrAccpCont.setLmtAccNo(lmtAccNo);
                    ctrAccpCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrAccpCont开始，参数为：{}", JSON.toJSONString(ctrAccpCont));
                    int i = ctrAccpContService.updateLmtAccNoByContNo(ctrAccpCont);
                    log.info("根据合同号更新ctrAccpCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_TF_LOC_CONT, bizType)) {
                    // 开证合同
                    CtrTfLocCont ctrTfLocCont = new CtrTfLocCont();
                    ctrTfLocCont.setLmtAccNo(lmtAccNo);
                    ctrTfLocCont.setContNo(ctrLoanContDto.getContNo());
                    ctrTfLocCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrTfLocCont开始，参数为：{}", JSON.toJSONString(ctrTfLocCont));
                    int i = ctrTfLocContService.updateLmtAccNoByContNo(ctrTfLocCont);
                    log.info("根据合同号更新ctrTfLocCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_CVRG_CONT, bizType)) {
                    // 保函合同
                    CtrCvrgCont ctrCvrgCont = new CtrCvrgCont();
                    ctrCvrgCont.setLmtAccNo(lmtAccNo);
                    ctrCvrgCont.setContNo(ctrLoanContDto.getContNo());
                    ctrCvrgCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrCvrgCont开始，参数为：{}", JSON.toJSONString(ctrCvrgCont));
                    int i = ctrCvrgContService.updateLmtAccNoByContNo(ctrCvrgCont);
                    log.info("根据合同号更新ctrCvrgCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_ENTRUST_LOAN_CONT, bizType)) {
                    // 委托贷款合同
                    CtrEntrustLoanCont ctrEntrustLoanCont = new CtrEntrustLoanCont();
                    ctrEntrustLoanCont.setLmtAccNo(lmtAccNo);
                    ctrEntrustLoanCont.setContNo(ctrLoanContDto.getContNo());
                    ctrEntrustLoanCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrEntrustLoanCont开始，参数为：{}", JSON.toJSONString(ctrEntrustLoanCont));
                    int i = ctrEntrustLoanContService.updateLmtAccNoByContNo(ctrEntrustLoanCont);
                    log.info("根据合同号更新ctrEntrustLoanCont结束，参数为：{}", i);
                    count += i;
                }
                if (Objects.equals(CmisBizConstants.STD_ZB_CTR_DISC_CONT, bizType)) {
                    // 贴现合同
                    CtrDiscCont ctrDiscCont = new CtrDiscCont();
                    ctrDiscCont.setLmtAccNo(lmtAccNo);
                    ctrDiscCont.setContNo(ctrLoanContDto.getContNo());
                    ctrDiscCont.setReplyNo(replyNo);
                    log.info("根据合同号更新ctrDiscCont开始，参数为：{}", JSON.toJSONString(ctrDiscCont));
                    int i = ctrDiscContService.updateLmtAccNoByContNo(ctrDiscCont);
                    log.info("根据合同号更新ctrDiscCont结束，参数为：{}", i);
                    count += i;
                }
            }
        }
        List<CtrAsplDetails> ctrAsplDetailsList =  ctrAsplDetailsService.selectByLmtAccNo(oldLmtAccNo);
        for(CtrAsplDetails ctrAsplDetails : ctrAsplDetailsList){
            ctrAsplDetails.setLmtAccNo(lmtAccNo);
            ctrAsplDetails.setReplySerno(replyNo);
            int i = ctrAsplDetailsService.updateSelective(ctrAsplDetails);
            count += i;
        }
        return count;
    }


    /**
     * @param queryModel
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont>
     * @author zxz
     * @date 2021/5/11 0024 17:33
     * @version 1.0.0
     * @desc 诚易融选择合同列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CtrLoanContDto> selectctrloanCyrRel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CtrLoanContDto> list = ctrLoanContMapper.queryAllImageCont(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: toSignListForftin
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> toSignListForftin(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_05);
        return ctrLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignListForftin
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> doneSignListForftin(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contExistsStatus", CmisCommonConstants.CONT_STATUS_OTHER);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_05);
        return ctrLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: logoutForftin
     * @方法描述: 根据合同号注销
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logoutForftin(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_600);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }

    /**
     * @方法名称: againSignForftin
     * @方法描述: 根据合同号重签
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int againSignForftin(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }

    /**
     * 获取基本信息
     *
     * @param params
     * @return
     */
    public CtrLoanCont queryCtrForftinContDataByParams(HashMap<String, String> params) {
        return ctrLoanContMapper.queryCtrForftinContDataByParams(params);
    }

    /**
     * @方法名称: contSign
     * @方法描述: 合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int contSign(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_200);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }


    /**
     * @方法名称: toSignListTCont
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> toSignListTCont(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_04);
        return ctrLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignListTCont
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> doneSignListTCont(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contExistsStatus", CmisCommonConstants.CONT_STATUS_OTHER);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_04);
        return ctrLoanContMapper.selectByModel(model);
    }

    /**
     * @方法名称: logoutTCont
     * @方法描述: 根据合同号注销
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logoutTCont(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_600);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }

    /**
     * @方法名称: againSignTCont
     * @方法描述: 根据合同号重签
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int againSignTCont(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }

    /**
     * @方法名称: contSignTCont
     * @方法描述: 合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int contSignTCont(CtrLoanCont ctrLoanCont) {
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_200);
        return ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
    }

    /**
     * @方法名称: selectContByIqpSerno
     * @方法描述: 根据业务申请流水号查询合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrLoanCont selectContByIqpSerno(String iqpSerno) {

        return ctrLoanContMapper.selectContByIqpSerno(iqpSerno);
    }

    /**
     * @方法名称: selectContByIqpSerno
     * @方法描述: 检查该客户下是否存在有效的合同
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Integer queryNormalContByCusId(HashMap<String, String> params) {
        return ctrLoanContMapper.queryNormalContByCusId(params);
    }

    /**
     * @方法名称: queryUnuseContByCusId
     * @方法描述: 检查该客户下是否存在失效的合同
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrLoanCont> queryUnuseContByCusId(HashMap<String, String> params) {
        return ctrLoanContMapper.queryUnuseContByCusId(params);
    }

    /**
     * @方法名称: updateContStatus
     * @方法描述: 根据业务流水号逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateContStatus(String iqpSerno) {
        return ctrLoanContMapper.updateContStatu(iqpSerno);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrLoanContMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称：stockContract
     * @方法描述：存量合同
     * @创建人：zhangming12
     * @创建时间：2021/5/20 9:56
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AllContDto> stockContract(QueryModel queryModel) {
        Map<String, Object> condition = queryModel.getCondition();
        if (condition == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String lmtAccNo = (String) condition.get("lmtAccNo");
        List<AllContDto> allContDtoList = new ArrayList<>();

        // 最高额授信协议
        List<CtrHighAmtAgrCont> ctrHighAmtAgrConts = ctrHighAmtAgrContService.selectByLmtAccNo(lmtAccNo);
        for (CtrHighAmtAgrCont ctrHighAmtAgrCont : ctrHighAmtAgrConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrHighAmtAgrCont, AllContDto.class);
            allContDto.setBusiType(ctrHighAmtAgrCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 普通贷款合同 贸融 福费廷
        List<CtrLoanCont> ctrLoanConts = this.selectByLmtAccNo(lmtAccNo);
        for (CtrLoanCont ctrLoanCont : ctrLoanConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrLoanCont, AllContDto.class);
            allContDto.setBusiType(ctrLoanCont.getBizType());
            allContDtoList.add(allContDto);
        }

        // 贴现合同
        List<CtrDiscCont> ctrDiscConts = ctrDiscContService.selectByLmtAccNo(lmtAccNo);
        for (CtrDiscCont ctrDiscCont : ctrDiscConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrDiscCont, AllContDto.class);
            allContDto.setBusiType(ctrDiscCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 保函协议详情
        List<CtrCvrgCont> ctrCvrgConts = ctrCvrgContService.selectByLmtAccNo(lmtAccNo);
        for (CtrCvrgCont ctrCvrgCont : ctrCvrgConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrCvrgCont, AllContDto.class);
            allContDto.setBusiType(ctrCvrgCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 开证合同详情
        List<CtrTfLocCont> ctrTfLocConts = ctrTfLocContService.selectByLmtAccNo(lmtAccNo);
        for (CtrTfLocCont ctrTfLocCont : ctrTfLocConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrTfLocCont, AllContDto.class);
            allContDto.setBusiType(ctrTfLocCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 银承合同详情
        List<CtrAccpCont> ctrAccpConts = ctrAccpContService.selectByLmtAccNo(lmtAccNo);
        for (CtrAccpCont ctrAccpCont : ctrAccpConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrAccpCont, AllContDto.class);
            allContDto.setBusiType(ctrAccpCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 委托贷款合同详情
        List<CtrEntrustLoanCont> ctrEntrustLoanConts = ctrEntrustLoanContService.selectByLmtAccNo(lmtAccNo);
        for (CtrEntrustLoanCont ctrEntrustLoanCont : ctrEntrustLoanConts) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrEntrustLoanCont, AllContDto.class);
            allContDto.setBusiType(ctrEntrustLoanCont.getBusiType());
            allContDtoList.add(allContDto);
        }

        // 资产池协议
        List<CtrAsplDetails> ctrAsplDetails = ctrAsplDetailsService.selectByLmtAccNo(lmtAccNo);
        for (CtrAsplDetails ctrAsplDetail : ctrAsplDetails) {
            AllContDto allContDto = BeanUtils.beanCopy(ctrAsplDetail, AllContDto.class);
            // TODO 表中无字段，建议加字段使用字段
            allContDto.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_10);
            allContDtoList.add(allContDto);
        }

        return allContDtoList;
    }


    /**
     * @author zlf
     * @date 2021/5/21 10:50
     * @version 1.0.0
     * @desc 根据账号查询帐户信息
     * @修改历史 修改时间 修改人员 修改原因
     */
    public ResultDto getOpanOrgName(String loanPayoutAccNoData) {
        Ib1253ReqDto reqDto = null;
        Ib1253RespDto data = new Ib1253RespDto();
        try {
            reqDto = new Ib1253ReqDto();
            if (!"".equals(loanPayoutAccNoData)) {
                //客户账户
                reqDto.setKehuzhao(loanPayoutAccNoData);
                reqDto.setShifoubz("0");
            } else {
                return new ResultDto(null).message("请输入账号");
            }
            log.info("根据账号查询帐户信息开始：账号为：" + loanPayoutAccNoData);
            //拼接报文后发送
            ResultDto<Ib1253RespDto> respDto = dscms2CoreIbClientService.ib1253(reqDto);
            log.info("根据账号查询帐户信息返回：" + respDto.toString());
            if ("0".equals(respDto.getCode())) {
                if (respDto.getData() != null) {
                    Ib1253RespDto data1 = respDto.getData();
                    String acctst =  data1.getZhhuztai();
                    if(!"A".equals(acctst)){
                        if("J".equals(acctst)){
                            //修改：覃明贵说 状态是控制情况下，不判断余额是否大于0，而判断 zhfbdjbz、zhzsbfbz、zhzfbsbz、kzfbdjbz、kzzsbfbz、kzzfbsbz 只要有1个不为0，就拦截
                            String zhfbdjbz = data1.getZhfbdjbz();
                            String zhzsbfbz = data1.getZhzsbfbz();
                            String zhzfbsbz = data1.getZhzfbsbz();
                            String kzfbdjbz = data1.getKzfbdjbz();
                            String kzzsbfbz = data1.getKzzsbfbz();
                            String kzzfbsbz = data1.getKzzfbsbz();
                            if((!"0".equals(zhfbdjbz))||(!"0".equals(zhzsbfbz))||(!"0".equals(zhzfbsbz))
                                    ||(!"0".equals(kzfbdjbz))||(!"0".equals(kzzsbfbz))||(!"0".equals(kzzfbsbz))){
                                return new ResultDto(null).message("状态不正常,状态为【" + this.kzhuztaiDicConversion(acctst) + "】,不可使用");
                            }
                        }else{
                            return new ResultDto(null).message("状态不正常,状态为【" + this.kzhuztaiDicConversion(acctst) + "】");
                        }
                    }

                    data.setZhhuzwmc(data1.getZhhuzwmc());
                    data.setKaihriqi(data1.getKaihriqi());
                    data.setZhaoxhao(data1.getZhaoxhao());
                    data.setZhhuztai(data1.getZhhuztai());
                    data.setKehuhaoo(data1.getKehuhaoo());
                    data.setZhufldm2(data1.getZhufldm2());
                    data.setZhhuztai(data1.getZhhuztai());
                    data.setZhjhaoma(data1.getZhjhaoma());
                    data.setZhjnzlei(data1.getZhjnzlei());
                    String zhkhjigo = data1.getZhkhjigo();
                    String orgName = "张家港农村商业银行";
                    if (zhkhjigo.startsWith("80")) {//寿光
                        orgName = "寿光村镇银行";
                    } else if (zhkhjigo.startsWith("81")) {//东海
                        orgName = "东海村镇银行";
                    }
                    data.setZhkhjigo(orgName);//2021-9-7 21:48:494  与王建佩和陈瑜确认

                }
            } else {
                log.error("核心接口异常！", respDto.getMessage());
                //return new ResultDto<Map>(map).message(respDto.getMessage());
                return new ResultDto(null).message("未查询到账户信息！");
            }

        } catch (Exception e) {
            log.error("核心接口异常！", e.getMessage());
            return new ResultDto(null).message("接口异常！");
        } finally {
        }
        return new ResultDto(data);
    }

    /**
     * @方法名称：stockContractAcc
     * @方法描述：存量合同
     * @创建人：yangwl
     * @创建时间：2021/5/24 20:56
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AllContDto> stockContractAcc(QueryModel queryModel) {
        Map<String, Object> condition = queryModel.getCondition();
        if (condition == null) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        String subSerno = (String) condition.get("subSerno");

        List<AllContDto> allContDtoList = new ArrayList<>();
        AllContDto allContDto;

        List<LmtReplyAccSub> lmtReplyAccSubs = lmtReplyAccSubService.selectBySubserno(subSerno);
        if (lmtReplyAccSubs != null) {
            for (LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubs) {
                String accNo = lmtReplyAccSub.getAccNo();
                //普通贷款合同申请
                List<IqpLoanApp> iqpLoanAppList = iqpLoanAppService.selectForLmtAccNo(accNo);
                if (iqpLoanAppList != null) {
                    for (IqpLoanApp iqpLoanApp : iqpLoanAppList) {
                        CtrLoanCont ctrLoanCont = this.selectContByIqpSerno(iqpLoanApp.getIqpSerno());
                        allContDto = BeanUtils.beanCopy(ctrLoanCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }
                //贴现合同
                List<IqpDiscApp> iqpDiscAppList = iqpDiscAppService.selectForLmtAccNo(accNo);
                if (iqpDiscAppList != null) {
                    for (IqpDiscApp iqpDiscApp : iqpDiscAppList) {
                        CtrDiscCont ctrDiscCont = ctrDiscContService.selectByIqpSerno(iqpDiscApp.getSerno());
                        allContDto = BeanUtils.beanCopy(ctrDiscCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }
                //保函合同
                List<IqpCvrgApp> iqpCvrgAppList = iqpCvrgAppService.selectForLmtAccNo(accNo);
                if (iqpCvrgAppList != null) {
                    for (IqpCvrgApp iqpCvrgApp : iqpCvrgAppList) {
                        CtrCvrgCont ctrCvrgCont = ctrCvrgContService.selectByIqpSerno(iqpCvrgApp.getSerno());
                        allContDto = BeanUtils.beanCopy(ctrCvrgCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }

                //最高额授信协议
                List<IqpHighAmtAgrApp> iqpHighAmtAgrAppList = iqpHighAmtAgrAppService.selectForLmtAccNo(accNo);
                if (iqpHighAmtAgrAppList != null) {
                    for (IqpHighAmtAgrApp iqpHighAmtAgrApp : iqpHighAmtAgrAppList) {
                        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectByIqpSerno(iqpHighAmtAgrApp.getSerno());
                        allContDto = BeanUtils.beanCopy(ctrHighAmtAgrCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }

                //开证合同
                List<IqpTfLocApp> iqpTfLocAppList = iqpTfLocAppService.selectForLmtAccNo(accNo);
                if (iqpTfLocAppList != null) {
                    for (IqpTfLocApp iqpTfLocApp : iqpTfLocAppList) {
                        CtrTfLocCont ctrTfLocCont = ctrTfLocContService.selectByIqpSerno(iqpTfLocApp.getSerno());
                        allContDto = BeanUtils.beanCopy(ctrTfLocCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }

                //银承合同
                List<IqpAccpApp> iqpAccpAppList = iqpAccpAppService.selectForLmtAccNo(accNo);
                if (iqpAccpAppList != null) {
                    for (IqpAccpApp iqpAccpApp : iqpAccpAppList) {
                        CtrAccpCont ctrAccpCont = ctrAccpContService.selectByIqpSerno(iqpAccpApp.getSerno());
                        allContDto = cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrAccpCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }

                //委托贷款合同
                List<IqpEntrustLoanApp> iqpEntrustLoanAppList = iqpEntrustLoanAppService.selectForLmtAccNo(accNo);
                if (iqpEntrustLoanAppList != null) {
                    for (IqpEntrustLoanApp iqpEntrustLoanApp : iqpEntrustLoanAppList) {
                        CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByIqpSerno(iqpEntrustLoanApp.getSerno());
                        allContDto = BeanUtils.beanCopy(ctrEntrustLoanCont, AllContDto.class);
                        allContDtoList.add(allContDto);
                    }
                }
            }
        }
        return allContDtoList;
    }

    //普通贷款合同注销
    public int logout(CtrLoanCont record) {
        return ctrLoanContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: selectContByContno
     * @方法描述: 根据借款合同号查询合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrLoanCont selectContByContno(String loanContNo) {

        return ctrLoanContMapper.selectContByContno(loanContNo);
    }

    /**
     * @方法名称: updateContStatusByContNo
     * @方法描述: 根据合同号更新借款合同或担保合同签约状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateContStatusByContNo(Map queryMap) {
        return ctrLoanContMapper.updateContStatusByContNo(queryMap);
    }

    /**
     * @方法名称: queryContStatusByContNo
     * @方法描述: 根据合同号查询合同状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String queryContStatusByContNo(String contNo) {
        return ctrLoanContMapper.queryContStatusByContNo(contNo);
    }

    /**
     * @方法名称: selectStartdaysByContNo
     * @方法描述: 查询合同起始日开始日
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public String selectStartdaysByContNo(Map queryMap) {
        return ctrLoanContMapper.selectStartdaysByContNo(queryMap);
    }

    /**
     * @方法名称: selectLoanContByCertNo
     * @方法描述: 惠享贷共借人身份证查询主借款合同
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<HxdLoanContList> selectLoanContByCertNo(String certCode) {
        return ctrLoanContMapper.selectLoanContByCertNo(certCode);
    }

    /**
     * @方法名称: selectLoanContByCertNo
     * @方法描述: 惠享贷共借人身份证查询主借款合同
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GuarContList> selectGuarLoanContByCertNo(Map queryMap) {
        return ctrLoanContMapper.selectGuarLoanContByCertNo(queryMap);
    }

    /**
     * @方法名称: selectLoanContListByCertNo
     * @方法描述: 借款合同查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<LoanContList> selectLoanContListByCertNo(Map queryMap) {
        return ctrLoanContMapper.selectLoanContListByCertNo(queryMap);
    }

    /**
     * 验证该合同是否被其他借款人占用
     *
     * @param ctrLoanContMap
     * @return
     */
    public int countCtrLoanContByMap(Map ctrLoanContMap) {
        return ctrLoanContMapper.countCtrLoanContByMap(ctrLoanContMap);
    }

    /**
     * @方法名称: querySurveySernoByContNo
     * @方法描述: 根据借款合同号查询调查流水号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String querySurveySernoByContNo(String contNo) {
        return ctrLoanContMapper.querySurveySernoByContNo(contNo);
    }


    /**
     * 根据日期查询合同数量
     * 根据日期查询未签订合同数量
     * 根据日期查询未签订合同数量(关联授信批复)
     *
     * @param map
     * @return
     */
    public int quertContSum(Map map) {
        return ctrLoanContMapper.quertContSum(map);
    }

    public int quertUnSignContSum(Map map) {
        return ctrLoanContMapper.quertUnSignContSum(map);
    }

    public int quertUnSignContSumByPf(Map map) {
        return ctrLoanContMapper.quertUnSignContSumByPf(map);
    }


    /**
     * 根据日期查询生效合同信息
     * 根据日期查询生效合同信息(关联授信批复)
     *
     * @param map
     * @return
     */
    public HashMap quertContByDate(Map map) {
        return ctrLoanContMapper.quertContByDate(map);
    }

    public HashMap quertContByDateByPf(Map map) {
        return ctrLoanContMapper.quertContByDateByPf(map);
    }


    /**
     * 根据日期查询未签订合同数量(关联授信批复)
     *
     * @param map
     * @return
     */
    public int quertOtherPrdIdContSum(Map map) {
        return ctrLoanContMapper.quertOtherPrdIdContSum(map);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> selectNumByInputId(QueryModel queryModel) {

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<>();
        int view200num = ctrLoanContMapper.selectView200num(queryModel);
        map.put("view200num", view200num);
        int view201num = ctrLoanContMapper.selectView201num(queryModel);
        map.put("view201num", view201num);
        int view202num = ctrLoanContMapper.selectView202num(queryModel);
        map.put("view202num", view202num);
        int view203num = ctrLoanContMapper.selectView203num(queryModel);
        map.put("view203num", view203num);
        map.put("num1", view200num + view201num + view202num + view203num);

        int view300num = ctrLoanContMapper.selectView300num(queryModel);
        map.put("view300num", view300num);
        int view301num = ctrLoanContMapper.selectView301num(queryModel);
        map.put("view301num", view301num);
        int view302num = ctrLoanContMapper.selectView302num(queryModel);
        map.put("view302num", view302num);
        int view303num = ctrLoanContMapper.selectView303num(queryModel);
        map.put("view303num", view303num);
        int view304num = ctrLoanContMapper.selectView304num(queryModel);
        map.put("view304num", view304num);
        int view305num = ctrLoanContMapper.selectView305num(queryModel);
        map.put("view305num", view305num);
        int view306num = ctrLoanContMapper.selectView306num(queryModel);
        map.put("view306num", view306num);
        int view307num = ctrLoanContMapper.selectView307num(queryModel);
        map.put("view307num", view307num);
        int view308num = ctrLoanContMapper.selectView308num(queryModel);
        map.put("view308num", view308num);
        int view309num = ctrLoanContMapper.selectView309num(queryModel);
        map.put("view309num", view309num);
        map.put("num2", view300num + view301num + view302num + view303num + view304num + view305num + view306num + view307num + view308num + view309num);

        int view400num = ctrLoanContMapper.selectView400num(queryModel);
        map.put("view400num", view400num);
        int view401num = ctrLoanContMapper.selectView401num(queryModel);
        map.put("view401num", view401num);
        int view402num = ctrLoanContMapper.selectView402num(queryModel);
        map.put("view402num", view402num);
        map.put("num3", view400num + view401num + view402num);

        list.add(map);
        return list;
    }

    /**
     * @函数名称:getAllContByInputId
     * @函数描述:根据客户经理工号查询合同待签订数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> getAllContByInputId(QueryModel queryModel) {
        return ctrLoanContMapper.getAllContByInputId(queryModel);
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrLoanCont> selectByQuerymodel(QueryModel model) {
        return ctrLoanContMapper.selectByModel(model);
    }

    /**
     * 查询押品对应的核销合同记录数
     *
     * @param guarNo
     * @return
     */
    public int countHxCtrLoanContRecords(String guarNo) {
        return ctrLoanContMapper.countHxCtrLoanContRecords(guarNo);
    }

    /**
     * 查询押品对应的合同状态
     *
     * @param guarNo
     * @return
     */
    public String selectContStatusByGuarNo(String guarNo) {
        return ctrLoanContMapper.selectContStatusByGuarNo(guarNo);
    }


    /**
     * @方法名称: updateisOnlineDraw
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateisOnlineDraw(String guarContNo) {

        try {
            List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByGuarContNo(guarContNo);
            if (ctrLoanContList != null && ctrLoanContList.size() > 0) {
                CtrLoanCont ctrLoanCont = ctrLoanContList.get(0);
                ctrLoanCont.setIsOnlineDraw("1");
                ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
            } else {
                throw BizException.error(null, "9999", "不存在担保合同关联的白领贷主合同！");
            }

        } catch (Exception e) {
            throw BizException.error(null, "9999", e.getMessage());
        }
        return 0;
    }

    /**
     * @param IqpSerno
     * @return int
     * @author shenli
     * @date 2021-8-13
     * @version 1.0.0
     * @desc 零售-业务申请历史-作废
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int cancelIqpRetail(String IqpSerno) {
        log.info("******************零售-业务申请历史-作废 开始 ******************", IqpSerno);
        try {

            //根据合同编号查询合同信息
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(IqpSerno);
            //第三方额度占用
            String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectContByIqpSerno(IqpSerno);
            //合同状态
            String contStatus = ctrLoanCont.getContStatus();
            String approveStatus = ctrLoanCont.getApproveStatus();
            String contNo = ctrLoanCont.getContNo();

            QueryModel QueryModel1 = new QueryModel();
            QueryModel1.addCondition("contNo",contNo);
            List<AccLoan> accLoanList =  accLoanMapper.selectByModel(QueryModel1);

            //未生效且审批状态不在流程中的
            if (!"200".equals(contStatus) && "000,997,996,998".indexOf(approveStatus) > -1 && accLoanList.size() == 0) {
                //额度提前终止
                CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
                cmisLmt0008ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0008ReqDto.setLmtType("01");//授信类型
                cmisLmt0008ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
                cmisLmt0008ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
                cmisLmt0008ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期

                List<CmisLmt0008ApprListReqDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0008ApprListReqDto>();
                CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
                cmisLmt0008ApprListReqDto.setCusId(ctrLoanCont.getCusId());
                cmisLmt0008ApprListReqDto.setOptType("02");
                cmisLmt0008ApprListReqDto.setApprSerno(iqpLoanApp.getIqpSerno());
                cmisLmt0011OccRelListDtoList.add(cmisLmt0008ApprListReqDto);
                cmisLmt0008ReqDto.setApprList(cmisLmt0011OccRelListDtoList);

                List<CmisLmt0008ApprSubListReqDto> cmisLmt0008ApprSubListReqDtoList = new ArrayList<CmisLmt0008ApprSubListReqDto>();
                CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
                cmisLmt0008ApprSubListReqDto.setCusId(ctrLoanCont.getCusId());
                cmisLmt0008ApprSubListReqDto.setApprSubSerno(iqpLoanApp.getReplyNo());
                cmisLmt0008ApprSubListReqDtoList.add(cmisLmt0008ApprSubListReqDto);
                cmisLmt0008ReqDto.setApprSubList(cmisLmt0008ApprSubListReqDtoList);

                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度提前终止请求报文：" + cmisLmt0008ReqDto.toString());
                ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
                log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度提前终止返回报文：" + cmisLmt0008RespDto.toString());

                if (!"0000".equals(cmisLmt0008RespDto.getData().getErrorCode())) {
                    log.info("零售-额度提前终止异常：" + cmisLmt0008RespDto.getData().getErrorMsg());
                    throw BizException.error(null, "9999", "零售额度提前终止异常");
                }

                //第三方额度恢复
                if ("1".equalsIgnoreCase(thirdPartyFlag)) {
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(ctrLoanCont.getContNo());//业务编号
                    cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(iqpLoanApp.getCvtCnyAmt());// 恢复敞口金额(人民币)
                    cmisLmt0012ReqDto.setRecoverAmtCny(iqpLoanApp.getCvtCnyAmt());//恢复总额(人民币)
                    cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                    cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                    cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    cmisLmt0012ReqDto.setIsRecoverCoop("1");


                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用请求报文：" + cmisLmt0012ReqDto.toString());
                    ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用返回报文：" + cmisLmt0012RespDto.toString());

                    if (!"0000".equals(cmisLmt0012RespDto.getData().getErrorCode())) {
                        log.info("零售-第三方恢复占用异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
                        throw BizException.error(null, "9999", "零售-第三方恢复占用异常");
                    }
                }

                ctrLoanCont.setContStatus("600");//更新为注销
                ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);

                iqpLoanApp.setApproveStatus("996");//自行退出
                iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);

            } else {
                throw BizException.error(null, "9999", "已生效的合同，审批中的合同，已放款的合同，不可注销申请!");
            }
        } catch (Exception e) {
            throw BizException.error(null, "9999", e.getMessage());
        }
        log.info("******************零售-业务申请历史-作废 结束 ******************", IqpSerno);
        return 0;
    }

    /**
     * @方法名称: queryCtrLoanListByCusId
     * @方法描述: 根据客户ID查询合同列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrLoanCont> queryCtrLoanListByCusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanCont> list = ctrLoanContMapper.queryCtrLoanListByCusId(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectSxkdContList
     * @方法描述: 获取省心快贷合同信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zl
     */
    public List<CtrLoanCont> selectSxkdContList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrLoanCont> list = ctrLoanContMapper.selectSxkdContList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param contNo
     * @return BigDecimal
     * @author qw
     * @date 2021/8/20 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可出账金额
     * @修改历史:
     */
    public BigDecimal getCanOutAccountAmt(String contNo) {
        //可出账金额
        BigDecimal canOutAccountAmt = BigDecimal.ZERO;
        CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
        if (Objects.isNull(ctrLoanCont)) {
            throw BizException.error(null, EcbEnum.ECB020022.key, EcbEnum.ECB020022.value);
        }
        //合同最高可用信金额折算人民币金额
        BigDecimal contHighAmt = ctrLoanCont.getCvtCnyAmt();
        //贷款总金额
        BigDecimal totalLoanAmt = BigDecimal.ZERO;
        //贷款总余额
        BigDecimal totalLoanBalance = BigDecimal.ZERO;
        Map map = new HashMap();
        map.put("contNo", ctrLoanCont.getContNo());
        List<AccLoan> accLoanList = accLoanService.selectAccLoanByContNo(map);
        if (CollectionUtils.nonEmpty(accLoanList)) {
            for (AccLoan accLoan : accLoanList) {
                totalLoanAmt = totalLoanAmt.add(accLoan.getExchangeRmbAmt());//贷款总金额
                totalLoanBalance = totalLoanBalance.add(accLoan.getExchangeRmbBal());//贷款总余额
            }
            if (CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrLoanCont.getContType())) {
                //一般合同
                canOutAccountAmt = contHighAmt.subtract(totalLoanAmt);
            } else {
                //最高额合同
                canOutAccountAmt = contHighAmt.subtract(totalLoanBalance);
            }
        } else {
            canOutAccountAmt = contHighAmt;
        }
        return canOutAccountAmt;
    }

    /**
     * 根据担保合同编号查询对应的借款合同的所属条线
     *
     * @param guarContNo
     * @return
     */
    public String selectBelgLineByGuarContNo(String guarContNo) {
        return ctrLoanContMapper.selectBelgLineByGuarContNo(guarContNo);
    }

    public String judgeQFXXSer(String iqpSerno) {
        return ctrLoanContMapper.judgeQFBySerno(iqpSerno);
    }


    public List<CtrLoanCont> selectByModelCtr(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        if (null != model.getCondition().get("inputDate") && model.getCondition().get("inputDate") instanceof List) {
            List<String> datelist = (List<String>) model.getCondition().get("inputDate");
            model.getCondition().put("inputStartDate", datelist.get(0));
            model.getCondition().put("inputEndDate", datelist.get(1));
        }
        List<CtrLoanCont> list = ctrLoanContMapper.selectByModelCtr(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/9/8 16:52
     * @version 1.0.0
     * @desc 合同签定后更新额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void changelmt(String contNo) {
        CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectContByContno(contNo);
        CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
        cmisLmt0029ReqDto.setDealBizNo(ctrLoanCont.getIqpSerno());
        cmisLmt0029ReqDto.setOriginAccNo(ctrLoanCont.getIqpSerno());
        cmisLmt0029ReqDto.setNewAccNo(contNo);
        cmisLmt0029ReqDto.setStartDate(ctrLoanCont.getContStartDate());
        cmisLmt0029ReqDto.setEndDate(ctrLoanCont.getContEndDate());
        log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-更新第三方起始到期日请求报文：" + cmisLmt0029ReqDto.toString());
        ResultDto<CmisLmt0029RespDto> cmisLmt0029RespDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
        log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-更新第三方起始到期日返回报文：" + cmisLmt0029RespDto.toString());
        if (cmisLmt0029RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0029RespDto.getData().getErrorCode())) {
            log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日成功！", ctrLoanCont.getIqpSerno());
        } else {
            log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日失败！", ctrLoanCont.getIqpSerno());
        }

    }

    /**
     * 查询借款人名下未结清的合同信息（最高额授信协议、普通贷款合同、贸易融资合同、开证合同、保函合同、银承合同）
     *
     * @param cusId
     * @return
     */
    public List<CtrLoanCont> selectUnClearContByCusId(String cusId) {
        return ctrLoanContMapper.selectUnClearContByCusId(cusId);
    }

    /**
     * 根据合同编号列表查询合同信息（最高额授信协议、普通贷款合同、贸易融资合同、开证合同、保函合同、银承合同）
     *
     * @param contNos
     * @return
     */
    public List<CtrLoanCont> selectByContNos(String contNos) {
        return ctrLoanContMapper.selectByContNos(contNos);
    }


    /**
     * @创建人 css
     * @创建时间 2021-09-14 15:27
     * @注释 其他事项申报合同选择
     */
    public List<Map> selectOtherThingsAppLoan(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map> list = ctrLoanContMapper.selectOtherThingsAppLoan(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据授信额度编号查询关联的所有非否决非自行退出的合同编号
     *
     * @param lmtAccNo
     * @return
     */
    public String selectContNosByLmtAccNo(String lmtAccNo) {
        return ctrLoanContMapper.selectContNosByLmtAccNo(lmtAccNo);
    }

    /**
     * 根据合同编号查询产品编号
     *
     * @param contNo
     * @return
     */
    public String selectPrdIdByContNo(String contNo) {
        return ctrLoanContMapper.selectPrdIdByContNo(contNo);
    }

    /**
     * @方法名称: getSumContAmt
     * @方法描述: 根据额度编号查询合同金额总和
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal getSumContAmt(String lmtAccNo) {
        return ctrLoanContMapper.getSumContAmt(lmtAccNo);
    }

    /**
     * @param map
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont
            * @ author qw
            * @ date 2 0 2 1 / 1 0 / 1 2 1 7 : 3 3
            * @ version 1 . 0 . 0
            * @ desc 合同信息
            * @ 修改历史: 修改时间 修改人员 修改原因
     */
    public CtrLoanContDto queryAllCont(Map map) {
        return ctrLoanContMapper.queryAllCont(map);
    }

    /**
     * @param map
     * @return java.util.List<cn.com.yusys.yusp.domain.CtrLoanCont
            * @ author qw
            * @ date 2 0 2 1 / 1 0 / 1 2 1 7 : 3 3
            * @ version 1 . 0 . 0
            * @ desc 合同信息
            * @ 修改历史: 修改时间 修改人员 修改原因
     */
    public IqpLoanAppDto queryAllContApp(Map map) {
        return ctrLoanContMapper.queryAllContApp(map);
    }

    /**
     * 根据客户号产品编号查询固定资产信息
     * @param model
     * @return
     */
    public List<CtrLoanCont> selectCtrLoanContByCusId(QueryModel model) {
        List<CtrLoanCont> list = ctrLoanContMapper.selectCtrLoanContByCusId(model);
        return list;
    }

    public Map<String, String> selectMapByContNo(String contNo) {
        return ctrLoanContMapper.selectMapByContNo(contNo);
    }

    //核心账户状态字典项翻译
    public String kzhuztaiDicConversion(String kzhuztai) {
        String msg = "未知";
        if ("B".equals(kzhuztai)) {
            msg = "不动户";
        } else if ("C".equals(kzhuztai)) {
            msg = "销户 ";
        } else if ("D".equals(kzhuztai)) {
            msg = "久悬户";
        } else if ("H".equals(kzhuztai)) {
            msg = "待启用";
        } else if ("I".equals(kzhuztai)) {
            msg = "转营业外收入";
        } else if ("G".equals(kzhuztai)) {
            msg = "未启用";
        } else if ("Y".equals(kzhuztai)) {
            msg = "预销户";
        } else if ("J".equals(kzhuztai)) {
            msg = "控制";
        }
        return msg;
    }


    /**
     * @方法名称: updatePaperContSignDate
     * @方法描述: 根据合同号更新纸质签订日期
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updatePaperContSignDate(CtrLoanCont ctrLoanCont) {
        CtrLoanCont ctrLoanContTemp = ctrLoanContMapper.selectByPrimaryKey(ctrLoanCont.getContNo());
        ctrLoanContTemp.setPaperContSignDate(ctrLoanCont.getPaperContSignDate());
        return ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanContTemp);
    }


}