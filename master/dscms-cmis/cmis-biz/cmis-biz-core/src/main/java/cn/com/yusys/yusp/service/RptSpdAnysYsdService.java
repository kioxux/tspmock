/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysYsdMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysYsdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-24 15:11:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysYsdService {

    @Autowired
    private RptSpdAnysYsdMapper rptSpdAnysYsdMapper;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;
    @Autowired
    private RptCptlSituLegalRepreLoanService rptCptlSituLegalRepreLoanService;
    @Autowired
    private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private RptOperationService rptOperationService;
    @Autowired
    private RptBasicInfoRealEstateService rptBasicInfoRealEstateService;
    @Autowired
    private RptBasicInfoAssoService rptBasicInfoAssoService;
    @Autowired
    private RptFncSituBsAnysService rptFncSituBsAnysService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private RptCptlSituExtGuarService rptCptlSituExtGuarService;
    @Autowired
    private RptFncSituBsService rptFncSituBsService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private RptOperEnergySalesService rptOperEnergySalesService;
    @Autowired
    private RptOperProfitSituationService rptOperProfitSituationService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysYsd selectByPrimaryKey(String serno) {
        return rptSpdAnysYsdMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptSpdAnysYsd> selectAll(QueryModel model) {
        List<RptSpdAnysYsd> records = (List<RptSpdAnysYsd>) rptSpdAnysYsdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysYsd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysYsd> list = rptSpdAnysYsdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysYsd record) {
        return rptSpdAnysYsdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysYsd record) {
        return rptSpdAnysYsdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysYsd record) {
        return rptSpdAnysYsdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysYsd record) {
        return rptSpdAnysYsdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptSpdAnysYsdMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysYsdMapper.deleteByIds(ids);
    }

    /**
     * 评分卡自动打分
     *
     * @param params
     * @return
     */
    public int autoValue(Map params) {
        String serno = params.get("serno").toString();
        int count = 0;
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        String rptType = params.get("rptType").toString();
        //客户经理类别: 01 主办客户经理，02协办客户经理
        String managerType = params.get("managerType").toString();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        BigDecimal curAmt = BigDecimal.ZERO;
        BigDecimal corpAmt = BigDecimal.ZERO;
        List<String> bankList = new ArrayList<>();
        List<RptCptlSituCorp> rptCptlSituCorpList = rptCptlSituCorpService.selectByModel(model);
        int totalTimes = 0;
        int cptlCount = 0;
        if (CollectionUtils.nonEmpty(rptCptlSituCorpList)) {
            for (RptCptlSituCorp rptCptlSituCorp : rptCptlSituCorpList) {
                bankList.add(rptCptlSituCorp.getBelongBank());
                int debtTimes = rptCptlSituCorp.getDebtTimes();
                int overdueTimes = rptCptlSituCorp.getOverdueTimes();
                totalTimes += debtTimes + overdueTimes;
                curAmt = curAmt.add(rptCptlSituCorp.getCurMonthAmt());
                corpAmt = corpAmt.add(rptCptlSituCorp.getCurMonthAmt());
            }
        }
        //第2项打分
        int value2 = 0;
        List<RptCptlSituLegalRepreLoan> rptCptlSituLegalRepreLoanList = rptCptlSituLegalRepreLoanService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptCptlSituLegalRepreLoanList)) {
            for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : rptCptlSituLegalRepreLoanList) {
                //实际控制人
                if (rptCptlSituLegalRepreLoan.getEnterprisesRelation().equals("02")) {
                    curAmt = curAmt.add(rptCptlSituLegalRepreLoan.getCptlBalance());
                    if (rptCptlSituLegalRepreLoan.getCptlType().equals("4")) {
                        bankList.add(rptCptlSituLegalRepreLoan.getCptlBankName());
                    }
                }
                //实际控制人配偶
                if (rptCptlSituLegalRepreLoan.getEnterprisesRelation().equals("04")) {
                    curAmt = curAmt.add(rptCptlSituLegalRepreLoan.getCptlBalance());
                }

            }
        }
        if (CollectionUtils.nonEmpty(bankList)) {
            //去重
            List<String> bankNewList = new ArrayList<>();
            for (String bank : bankList) {
                if (!bankNewList.contains(bank)) {
                    bankNewList.add(bank);
                }
            }
            cptlCount = bankNewList.size();
        }
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssetsList = rptBasicInfoPersFamilyAssetsService.selectByModel(model);
        BigDecimal zyValue = BigDecimal.ZERO;
        BigDecimal hxValue = BigDecimal.ZERO;
        BigDecimal zyjzValue = BigDecimal.ZERO;
        BigDecimal qyhxValue = BigDecimal.ZERO;
        List<RptBasicInfoRealEstate> rptBasicInfoRealEstates = rptBasicInfoRealEstateService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptBasicInfoRealEstates)) {
            for (RptBasicInfoRealEstate rptBasicInfoRealEstate : rptBasicInfoRealEstates) {
                if (Objects.nonNull(rptBasicInfoRealEstate.getEquityCertPaperValue())) {
                    hxValue = hxValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue());
                    qyhxValue = qyhxValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue());
                }
                if (Objects.nonNull(rptBasicInfoRealEstate.getCurrGuarCaseLoanAmt())) {
                    zyjzValue = zyjzValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue().subtract(rptBasicInfoRealEstate.getCurrGuarCaseLoanAmt()));
                }
                if (Objects.nonNull(rptBasicInfoRealEstate.getEquityPaperValue())) {
                    zyValue = zyValue.add(rptBasicInfoRealEstate.getEquityPaperValue());
                    zyjzValue = zyjzValue.add(rptBasicInfoRealEstate.getEquityPaperValue());
                }
            }
        }
        if (CollectionUtils.nonEmpty(rptBasicInfoPersFamilyAssetsList)) {
            for (RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets : rptBasicInfoPersFamilyAssetsList) {
                //自有资产价值
                if (rptBasicInfoPersFamilyAssets.getAssetRange().equals("1")) {
                    if ("02".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "04".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "05".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation())) {
                        zyValue = zyValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                        zyjzValue = zyjzValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                    }
                }//核心资产价值
                else if (rptBasicInfoPersFamilyAssets.getAssetRange().equals("2")) {
                    if ("02".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "04".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation()) || "05".equals(rptBasicInfoPersFamilyAssets.getEnterprisesRelation())) {
                        hxValue = hxValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                        zyjzValue = zyjzValue.add(rptBasicInfoPersFamilyAssets.getMarketValue().subtract(rptBasicInfoPersFamilyAssets.getHasGuarAmt()));
                    }
                    if (rptBasicInfoPersFamilyAssets.getEnterprisesRelation().equals("05")) {
                        qyhxValue = qyhxValue.add(rptBasicInfoPersFamilyAssets.getMarketValue());
                    }
                }
            }
        }
        if (hxValue.compareTo(BigDecimal.ZERO) > 0 && curAmt.compareTo(BigDecimal.ZERO) > 0) {
            if ((curAmt.divide(hxValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(0.3)) <= 0) {
                value2 = 12;
            } else if ((curAmt.divide(hxValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(0.4)) <= 0) {
                value2 = 10;
            } else if ((curAmt.divide(hxValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(0.6)) <= 0) {
                value2 = 7;
            } else if ((curAmt.divide(hxValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(1)) <= 0) {
                value2 = 3;
            } else if ((curAmt.divide(hxValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(1)) > 0) {
                value2 = 0;
            }
        }
        if (curAmt.compareTo(BigDecimal.ZERO) == 0) {
            value2 = 12;
        }
        //第3项自动打分
        int value3 = 0;
        if (zyValue.compareTo(BigDecimal.ZERO) > 0 && curAmt.compareTo(BigDecimal.ZERO) > 0) {
            if ((curAmt.divide(zyValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                value3 = 12;
            } else if ((curAmt.divide(zyValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(0.8)) <= 0) {
                value3 = 8;
            } else if ((curAmt.divide(zyValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(1)) <= 0) {
                value3 = 6;
            } else if ((curAmt.divide(zyValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(1.5)) <= 0) {
                value3 = 4;
            } else if ((curAmt.divide(zyValue, 6, RoundingMode.HALF_UP)).compareTo(BigDecimal.valueOf(1.5)) > 0) {
                value3 = 0;
            }
        }
        if (curAmt.compareTo(BigDecimal.ZERO) == 0) {
            value3 = 12;
        }
        //第4项打分
        int value4 = 0;
        if (cptlCount == 0) {
            value4 = 5;
        } else if (cptlCount == 1) {
            value4 = 4;
        } else if (cptlCount == 2) {
            value4 = 3;
        } else if (cptlCount == 3) {
            value4 = 1;
        } else if (cptlCount > 3) {
            value4 = 0;
        }

        //第5项打分
        int value5 = 0;
        BigDecimal balance = BigDecimal.ZERO;
        QueryModel model4 = new QueryModel();
        model4.addCondition("serno", serno);
        model4.addCondition("guarCha", CmisBizConstants.STD_ZB_GUAR_CHA_10);
        List<RptCptlSituExtGuar> rptCptlSituExtGuars = rptCptlSituExtGuarService.selectByModel(model4);
        if (CollectionUtils.nonEmpty(rptCptlSituExtGuars)) {
            for (RptCptlSituExtGuar rptCptlSituExtGuar : rptCptlSituExtGuars) {
                balance = rptCptlSituExtGuar.getBalance();
            }
        }
        BigDecimal subtract = qyhxValue.subtract(corpAmt);
        if (balance.compareTo(BigDecimal.ZERO) == 0) {
            value5 = 4;
        } else if (balance.compareTo(BigDecimal.ZERO) > 0) {
            if (subtract.compareTo(BigDecimal.ZERO) <= 0) {
                value5 = 0;
            } else {
                BigDecimal divide = NumberUtils.divide(balance, subtract);
                if (balance.compareTo(subtract) <= 0) {
                    value5 = 4;
                } else if (divide.compareTo(BigDecimal.valueOf(5)) <= 0) {
                    value5 = 3;
                } else if (divide.compareTo(BigDecimal.valueOf(10)) <= 0) {
                    value5 = 2;
                } else if (divide.compareTo(BigDecimal.valueOf(10)) > 0) {
                    value5 = 0;
                }
            }
        }
        //第6项打分
        int value6 = 0;
        String cusId = lmtApp.getCusId();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
            String fjobDate = "";
            Map<String, Object> map1 = new HashMap<>();
            map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
            map1.put("cusIdRel", lmtApp.getCusId());
            ResultDto<CusCorpMgrDto> cusCorpMgrByParams = cmisCusClientService.getCusCorpMgrByParams(map1);
            //获取实际控制人从业日期
            if (cusCorpMgrByParams != null && cusCorpMgrByParams.getData() != null) {
                fjobDate = cusCorpMgrByParams.getData().getFjobDate();
            }
            if (StringUtils.nonBlank(fjobDate)) {
                int month = DateUtils.getMonthsByTwoDatesDef(fjobDate, DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                double year = month / 12;
                if (year >= 5) {
                    value6 = 6;
                } else if (year >= 1 && year < 5) {
                    value6 = 3;
                } else if (year < 1) {
                    value6 = 0;
                }
            }
        } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
            ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
            if (cusIndivUnitDtoResultDto != null && cusIndivUnitDtoResultDto.getData() != null) {
                CusIndivUnitDto data = cusIndivUnitDtoResultDto.getData();
                String fjobDate = data.getWorkDate();
                String currentDate = DateUtils.getCurrentDate(DateFormatEnum.YEAR);
                if (StringUtils.nonBlank(fjobDate)) {
                    int year = Integer.parseInt(currentDate) - Integer.parseInt(fjobDate);
                    if (year >= 5) {
                        value6 = 6;
                    } else if (year >= 1 && year < 5) {
                        value6 = 3;
                    } else if (year < 1) {
                        value6 = 0;
                    }
                }
            }
        }
        //第7项自动打分
        int value7 = 0;
        String tradeClass = "";
        if (Objects.nonNull(cusBaseClientDto)) {
            if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                RptBasicInfoAsso rptBasicInfoAsso = rptBasicInfoAssoService.selectByPrimaryKey(serno);
                tradeClass = rptBasicInfoAsso.getTradeClass();
            } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
                if (cusIndivUnitDtoResultDto != null && cusIndivUnitDtoResultDto.getData() != null) {
                    CusIndivUnitDto data = cusIndivUnitDtoResultDto.getData();
                    String indivComTrade = data.getIndivComTrade();
                    tradeClass = indivComTrade;
                }
            }
            if (StringUtils.nonBlank(tradeClass)) {
                if (tradeClass.startsWith("A")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("B")) {
                    value7 = 0;
                } else if (tradeClass.startsWith("C")) {
                    if ("C".equals(tradeClass)) {
                        value7 = 3;
                    } else if (tradeClass.startsWith("C13") || tradeClass.startsWith("C14") || tradeClass.startsWith("C15") || tradeClass.startsWith("C16")) {
                        value7 = 6;
                    } else if (tradeClass.startsWith("C17") || tradeClass.startsWith("C18") || tradeClass.startsWith("C19") || tradeClass.startsWith("C20") || tradeClass.startsWith("C21")) {
                        value7 = 3;
                    } else if (tradeClass.startsWith("C22") || tradeClass.startsWith("C23")) {
                        value7 = 0;
                    } else if (tradeClass.startsWith("C24")) {
                        value7 = 6;
                    } else if (tradeClass.startsWith("C25")) {
                        value7 = 0;
                    } else if (tradeClass.startsWith("C26")) {
                        value7 = 3;
                    } else if (tradeClass.startsWith("C27")) {
                        value7 = 6;
                    } else if (tradeClass.startsWith("C28") || tradeClass.startsWith("C29") || tradeClass.startsWith("C30")) {
                        value7 = 3;
                    } else if (tradeClass.startsWith("C31") | tradeClass.startsWith("C32")) {
                        value7 = 0;
                    } else if (tradeClass.startsWith("C33") || tradeClass.startsWith("C34") || tradeClass.startsWith("C35") || tradeClass.startsWith("C36")) {
                        value7 = 3;
                    } else if (tradeClass.startsWith("C37")) {
                        if ("C373".equals(tradeClass)) {
                            value7 = 0;
                        } else {
                            value7 = 3;
                        }
                    } else {
                        value7 = 3;
                    }
                } else if (tradeClass.startsWith("D")) {
                    value7 = 3;
                } else if (tradeClass.startsWith("E")) {
                    value7 = 0;
                } else if (tradeClass.startsWith("F")) {
                    value7 = 3;
                } else if (tradeClass.startsWith("G")) {
                    value7 = 3;
                } else if (tradeClass.startsWith("H")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("I")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("J")) {
                    value7 = 3;
                } else if (tradeClass.startsWith("K")) {
                    value7 = 0;
                } else if (tradeClass.startsWith("L")) {
                    value7 = 3;
                } else if (tradeClass.startsWith("M")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("N")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("O")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("P")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("Q")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("R")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("S")) {
                    value7 = 6;
                } else if (tradeClass.startsWith("T")) {
                    value7 = 6;
                }
            }
        }

        //第8项打分
        int value8 = this.value8(serno, rptType);

        //第12项打分
        int value12 = 0;
        //业务申请金额
        BigDecimal lmtAmt = BigDecimal.ZERO;
        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectSubPrdBySerno(serno);
        if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
            String subSerno = "";
            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                if (StringUtils.nonBlank(lmtBizTypeProp)) {
                    if ("P014".equals(lmtBizTypeProp)) {
                        lmtAmt = lmtAppSubPrd.getLmtAmt();
                        break;
                    }
                }
            }
        }
        if (zyjzValue.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal divide = lmtAmt.divide(zyjzValue, 6, RoundingMode.HALF_UP);
            if (divide.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                value12 = 6;
            } else if (divide.compareTo(BigDecimal.valueOf(0.6)) <= 0) {
                value12 = 5;
            } else if (divide.compareTo(BigDecimal.valueOf(0.7)) <= 0) {
                value12 = 4;
            } else if (divide.compareTo(BigDecimal.valueOf(1)) <= 0) {
                value12 = 2;
            } else if (divide.compareTo(BigDecimal.valueOf(1)) > 0) {
                value12 = 0;
            }
        }
        RptSpdAnysYsd rptSpdAnysYsd = new RptSpdAnysYsd();
        rptSpdAnysYsd.setSerno(serno);
        User userInfo = SessionUtils.getUserInformation();
        if (managerType.equals("01")) {
            rptSpdAnysYsd.setPfkYsd2Grade1(value2);
            rptSpdAnysYsd.setPfkYsd3Grade1(value3);
            rptSpdAnysYsd.setPfkYsd4Grade1(value4);
            rptSpdAnysYsd.setPfkYsd5Grade1(value5);
            rptSpdAnysYsd.setPfkYsd6Grade1(value6);
            rptSpdAnysYsd.setPfkYsd7Grade1(value7);
            rptSpdAnysYsd.setPfkYsd8Grade1(value8);
            rptSpdAnysYsd.setPfkYsd12Grade1(value12);
            rptSpdAnysYsd.setMainManagerId(userInfo.getLoginCode());
            count = rptSpdAnysYsdMapper.updateByPrimaryKeySelective(rptSpdAnysYsd);
        } else if (managerType.equals("02")) {
            RptSpdAnysYsd temp = selectByPrimaryKey(serno);
            if (Objects.nonNull(temp)) {
                rptSpdAnysYsd.setPfkYsd1Grade2(temp.getPfkYsd1Grade1());
                rptSpdAnysYsd.setPfkYsd9Grade2(temp.getPfkYsd9Grade1());
                rptSpdAnysYsd.setPfkYsd10Grade2(temp.getPfkYsd10Grade1());
                rptSpdAnysYsd.setPfkYsd11Grade2(temp.getPfkYsd11Grade1());
                rptSpdAnysYsd.setPfkYsd13Grade2(temp.getPfkYsd13Grade1());
                rptSpdAnysYsd.setPfkYsd14Grade2(temp.getPfkYsd14Grade1());
            }
            rptSpdAnysYsd.setPfkYsd2Grade2(value2);
            rptSpdAnysYsd.setPfkYsd3Grade2(value3);
            rptSpdAnysYsd.setPfkYsd4Grade2(value4);
            rptSpdAnysYsd.setPfkYsd5Grade2(value5);
            rptSpdAnysYsd.setPfkYsd6Grade2(value6);
            rptSpdAnysYsd.setPfkYsd7Grade2(value7);
            rptSpdAnysYsd.setPfkYsd8Grade2(value8);
            rptSpdAnysYsd.setPfkYsd12Grade2(value12);
            rptSpdAnysYsd.setAssistManagerId(userInfo.getLoginCode());
            count = rptSpdAnysYsdMapper.updateByPrimaryKeySelective(rptSpdAnysYsd);
        }

        return count;
    }

    public int value8(String serno, String rptType) {
        BigDecimal lrzeCurr = BigDecimal.ZERO;
        BigDecimal lrzeLastYear = BigDecimal.ZERO;
        BigDecimal lrzeLastSecond = BigDecimal.ZERO;
        BigDecimal xssrCurr = BigDecimal.ZERO;
        BigDecimal xssrLastYear = BigDecimal.ZERO;
        BigDecimal xssrLastSecond = BigDecimal.ZERO;
        List<RptFncSituBsAnys> rptFncSituBsAnysList = new ArrayList<>();
        if (CmisBizConstants.STD_RPT_TYPE_C11.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C14.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_4);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_I04.equals(rptType) || CmisBizConstants.STD_RPT_TYPE_I05.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
            RptOperEnergySales rptOperEnergySales = rptOperEnergySalesService.selectByPrimaryKey(serno);
            RptOperProfitSituation rptOperProfitSituation = rptOperProfitSituationService.selectByPrimaryKey(serno);
            if (Objects.nonNull(rptOperEnergySales)) {
                if (Objects.nonNull(rptOperEnergySales.getNearSecondAppSaleIncome())) {
                    xssrLastSecond = rptOperEnergySales.getNearSecondAppSaleIncome();
                }
                if (Objects.nonNull(rptOperEnergySales.getNearFirstAppSaleIncome())) {
                    xssrLastYear = rptOperEnergySales.getNearFirstAppSaleIncome();
                }
                if (Objects.nonNull(rptOperEnergySales.getCurrAppSaleIncome())) {
                    xssrCurr = rptOperEnergySales.getCurrAppSaleIncome();
                }
            }
            if (Objects.nonNull(rptOperProfitSituation)) {
                lrzeCurr = rptOperProfitSituation.getCurrTotalProfit();
                lrzeLastYear = rptOperProfitSituation.getNearFirstTotalProfit();
                lrzeLastSecond = rptOperProfitSituation.getNearSecondTotalProfit();
            }
        }
        RptFncSituBs rptFncSituBs = rptFncSituBsService.selectByPrimaryKey(serno);
        String inputYear = rptFncSituBs.getInputYear();
        if (CollectionUtils.nonEmpty(rptFncSituBsAnysList)) {
            for (RptFncSituBsAnys rptFncSituBsAnys : rptFncSituBsAnysList) {
                if ("利润总额".equals(rptFncSituBsAnys.getSubjectName())) {
                    lrzeCurr = rptFncSituBsAnys.getCurYearLastMonth();
                    lrzeLastYear = rptFncSituBsAnys.getLastYear();
                    lrzeLastSecond = rptFncSituBsAnys.getLastTwoYear();
                } else if ("销售收入".equals(rptFncSituBsAnys.getSubjectName())) {
                    xssrCurr = rptFncSituBsAnys.getCurYearLastMonth();
                    xssrLastYear = rptFncSituBsAnys.getLastYear();
                    xssrLastSecond = rptFncSituBsAnys.getLastTwoYear();
                }
            }
        }
        int month = Integer.parseInt(inputYear.substring(4, 6));
        BigDecimal xssrCurrYear = BigDecimal.ZERO;
        BigDecimal lrzeCurrYear = BigDecimal.ZERO;
        if (month < 12) {
            xssrCurrYear = (xssrCurr.divide(BigDecimal.valueOf(month), 6, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(12));
            lrzeCurrYear = (lrzeCurr.divide(BigDecimal.valueOf(month), 6, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(12));
        } else if (month == 12) {
            xssrCurrYear = xssrCurr;
            lrzeCurrYear = lrzeCurr;
        }
        BigDecimal lastYearXssrDiff = xssrLastYear.subtract(xssrLastSecond);
        BigDecimal curXssrDiff = xssrCurrYear.subtract(xssrLastYear);
        if (lrzeCurrYear.compareTo(BigDecimal.ZERO) > 0 && lrzeLastYear.compareTo(BigDecimal.ZERO) > 0 && lrzeLastSecond.compareTo(BigDecimal.ZERO) > 0) {
            //利润总额上一年增长率
            if (lrzeLastYear.compareTo(lrzeLastSecond.add(lrzeLastSecond.multiply(BigDecimal.valueOf(0.1)))) >= 0 && lrzeCurrYear.compareTo(lrzeLastYear.add(lrzeLastYear.multiply(BigDecimal.valueOf(0.1)))) >= 0) {
                if (xssrCurr.compareTo(BigDecimal.ZERO) > 0 && xssrLastYear.compareTo(BigDecimal.ZERO) > 0 && xssrLastSecond.compareTo(BigDecimal.ZERO) > 0) {
                    if (xssrLastYear.compareTo(xssrLastSecond.add(xssrLastSecond.multiply(BigDecimal.valueOf(0.1)))) >= 0 && xssrCurrYear.compareTo(xssrLastYear.add(xssrLastYear.multiply(BigDecimal.valueOf(0.1)))) >= 0) {
                        return 12;
                    }
                }
            }

            if (lastYearXssrDiff.compareTo(BigDecimal.ZERO) > 0 && curXssrDiff.compareTo(BigDecimal.ZERO) > 0) {
                return 10;
            }
        }
        if (lrzeCurr.compareTo(BigDecimal.ZERO) > 0) {
            if (curXssrDiff.compareTo(BigDecimal.ZERO) > 0) {
                return 8;
            } else {
                return 6;
            }
        }
        if (lrzeCurr.compareTo(lrzeLastYear.subtract(lrzeLastYear.multiply(BigDecimal.valueOf(0.3)))) >= 0 &&
                xssrCurrYear.compareTo(xssrLastYear.subtract(xssrLastYear.multiply(BigDecimal.valueOf(0.3)))) >= 0) {
            return 2;
        }
        return 0;

    }
}
