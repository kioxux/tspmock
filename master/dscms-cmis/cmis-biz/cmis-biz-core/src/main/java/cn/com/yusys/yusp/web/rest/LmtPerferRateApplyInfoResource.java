/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtPerferRateApplyInfo;
import cn.com.yusys.yusp.service.LmtPerferRateApplyInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPerferRateApplyInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-05-11 19:31:50
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "优惠利率申请")
@RequestMapping("/api/lmtperferrateapplyinfo")
public class LmtPerferRateApplyInfoResource {
    @Autowired
    private LmtPerferRateApplyInfoService lmtPerferRateApplyInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtPerferRateApplyInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtPerferRateApplyInfo> list = lmtPerferRateApplyInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtPerferRateApplyInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtPerferRateApplyInfo>> index(QueryModel queryModel) {
        List<LmtPerferRateApplyInfo> list = lmtPerferRateApplyInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtPerferRateApplyInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtPerferRateApplyInfo> show(@PathVariable("pkId") String pkId) {
        LmtPerferRateApplyInfo lmtPerferRateApplyInfo = lmtPerferRateApplyInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtPerferRateApplyInfo>(lmtPerferRateApplyInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtPerferRateApplyInfo> create(@RequestBody LmtPerferRateApplyInfo lmtPerferRateApplyInfo) throws URISyntaxException {
        lmtPerferRateApplyInfoService.insert(lmtPerferRateApplyInfo);
        return new ResultDto<LmtPerferRateApplyInfo>(lmtPerferRateApplyInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtPerferRateApplyInfo lmtPerferRateApplyInfo) throws URISyntaxException {
        int result = lmtPerferRateApplyInfoService.update(lmtPerferRateApplyInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtPerferRateApplyInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtPerferRateApplyInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @param perferRateApplyInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.PerferRateApplyInfo>
     * @author 王玉坤
     * @date 2021/4/16 17:17
     * @version 1.0.0
     * @desc 根据调查流水号插入或更新优惠利率信息
     * @修改历史:
     */
    @ApiOperation("根据调查流水号插入或更新优惠利率信息")
    @PostMapping("/savebysurveyserno")
    protected ResultDto saveBySurveyNo(@RequestBody LmtPerferRateApplyInfo perferRateApplyInfo) {
        return lmtPerferRateApplyInfoService.saveBySurveyNo(perferRateApplyInfo);
    }

    /**
     * @return 废弃优惠利率审批接口  updateSelective
     * @创建人 WH
     * @创建时间 14:27 2021-04-13
     **/
    @ApiOperation("废弃优惠利率审批接口")
    @PostMapping("/updateSelective/{surveyNo}")
    protected ResultDto<Integer> updateSelective(@PathVariable String surveyNo) {
        LmtPerferRateApplyInfo lmtperferRateApplyInfo = new LmtPerferRateApplyInfo();
        lmtperferRateApplyInfo.setSurveySerno(surveyNo);
        lmtperferRateApplyInfo.setApproveStatus("200");
        int result = lmtPerferRateApplyInfoService.updateSelective(lmtperferRateApplyInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @return 根据流水号查询唯一的一条审批记录
     * @创建人 WH
     * @创建时间 14:27 2021-04-13
     **/
    @ApiOperation("根据流水号查询唯一的一条审批记录")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto selectbysurveyserno(@RequestBody LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        return lmtPerferRateApplyInfoService.selectBySurveySerNo(lmtPerferRateApplyInfo.getSurveySerno());
    }

    /**
     * @return 优惠利率申请流程
     * @创建人 李帅
     * @创建时间 2021-08-04 21:43
     **/
    @ApiOperation("优惠利率申请流程")
    @PostMapping("/rateApp")
    protected ResultDto<String> rateApp(@RequestBody LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        return lmtPerferRateApplyInfoService.rateApp(lmtPerferRateApplyInfo);
    }

    /**
     * @param lmtPerferRateApplyInfo
     * @return 优惠利率撤销流程
     * @创建人 尚智勇
     * @创建时间 2021-09-10 15:45
     **/
    @ApiOperation("优惠利率撤销流程")
    @PostMapping("/rateRev")
    protected ResultDto<String> rateRev(@RequestBody LmtPerferRateApplyInfo lmtPerferRateApplyInfo) {
        return lmtPerferRateApplyInfoService.rateRev(lmtPerferRateApplyInfo);
    }

}
