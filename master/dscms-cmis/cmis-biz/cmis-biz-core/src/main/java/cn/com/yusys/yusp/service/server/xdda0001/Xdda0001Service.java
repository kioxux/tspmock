package cn.com.yusys.yusp.service.server.xdda0001;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.DocArchiveInfo;
import cn.com.yusys.yusp.domain.DocScanUserInfo;
import cn.com.yusys.yusp.dto.server.xdda0001.req.Xdda0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdda0001.resp.Xdda0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.DocImageSpplInfoMapper;
import cn.com.yusys.yusp.service.DocAccListService;
import cn.com.yusys.yusp.service.DocArchiveInfoService;
import cn.com.yusys.yusp.service.DocReadDetailInfoService;
import cn.com.yusys.yusp.service.DocScanUserInfoService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class Xdda0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdda0001Service.class);

    @Resource
    private DocImageSpplInfoMapper docImageSpplInfoMapper;

    @Resource
    private DocArchiveInfoService docArchiveInfoService;

    @Resource
    private DocAccListService docAccListService;

    @Resource
    private DocReadDetailInfoService docReadDetailInfoService;

    @Resource
    private DocScanUserInfoService docScanUserInfoService;

    public Xdda0001DataRespDto insertDocImageSppi(Xdda0001DataReqDto xdda0001DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataReqDto));
        Xdda0001DataRespDto xdda0001DataRespDto = new Xdda0001DataRespDto();
        xdda0001DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
        xdda0001DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
        try{
            List<DocArchiveInfo> list = docArchiveInfoService.selectByDocNo(xdda0001DataReqDto.getDocNo());
            if(Objects.nonNull(list) && list.size() > 0) {
                DocScanUserInfo docScanUserInfo = new DocScanUserInfo();
                // 扫描信息流水号
                docScanUserInfo.setDsuiSerno(StringUtils.getUUID());
                // 档案编号
                docScanUserInfo.setDocSerno(list.get(0).getDocSerno());
                // 扫描人编号
                docScanUserInfo.setScanUserId(xdda0001DataReqDto.getScanId());
                // 扫描人
                docScanUserInfo.setScanUserName(xdda0001DataReqDto.getScanName());
                // 扫描时间
                docScanUserInfo.setScanTime(xdda0001DataReqDto.getScanTime());
                // 登记日期
                docScanUserInfo.setInputDate(DateUtils.formatDate("yyyy-MM-dd"));
                docScanUserInfoService.insertSelective(docScanUserInfo);

                // 更新库存位置
                DocArchiveInfo archiveInfo = new DocArchiveInfo();
                archiveInfo.setDocSerno(list.get(0).getDocSerno());
                archiveInfo.setLocation(xdda0001DataReqDto.getLocation());
                docArchiveInfoService.updateSelective(archiveInfo);

                xdda0001DataRespDto.setOpFlag(CmisBizConstants.YES);
                xdda0001DataRespDto.setOpMsg("扫描人信息登记成功");
            } else {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, "扫描人信息登记失败，未找到对应的档案信息。");
                xdda0001DataRespDto.setOpFlag(CmisBizConstants.NO);
                xdda0001DataRespDto.setOpMsg("扫描人信息登记失败，未找到对应的档案信息。");
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, e.getMessage());
            xdda0001DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdda0001DataRespDto.setOpMsg("扫描人信息登记失败");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataRespDto));
        return xdda0001DataRespDto;
    }

    public Xdda0001DataRespDto updateStatus(Xdda0001DataReqDto xdda0001DataReqDto,String status) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataReqDto));
        Xdda0001DataRespDto xdda0001DataRespDto = new Xdda0001DataRespDto();
        xdda0001DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
        xdda0001DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
        try{
            // 退回操作的时候，同步档案状态为04：退回在途
            docArchiveInfoService.synStatus(xdda0001DataReqDto.getDocNo(),status);
            xdda0001DataRespDto.setOpFlag(CmisBizConstants.YES);
            xdda0001DataRespDto.setOpMsg("档案状态同步成功");
        }catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, e.getMessage());
            xdda0001DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdda0001DataRespDto.setOpMsg("档案状态同步失败");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0002.key, DscmsEnum.TRADE_CODE_XDDA0002.value, JSON.toJSONString(xdda0001DataRespDto));
        return xdda0001DataRespDto;
    }

    public Xdda0001DataRespDto updateDocStatus(Xdda0001DataReqDto xdda0001DataReqDto,String status) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0003.key, DscmsEnum.TRADE_CODE_XDDA0003.value, JSON.toJSONString(xdda0001DataReqDto));
        Xdda0001DataRespDto xdda0001DataRespDto = new Xdda0001DataRespDto();
        xdda0001DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
        xdda0001DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
        try{
            // 同步档案台账状态为03：档案已入库/08：出库在途
            docAccListService.synDocAccStatus(xdda0001DataReqDto.getDocNo(),status);
            // 同步档案借阅明细状态为03：档案已入库/08：出库在途
            docReadDetailInfoService.synReadDetailStatus(xdda0001DataReqDto.getDocNo(),status);
            xdda0001DataRespDto.setOpFlag(CmisBizConstants.YES);
            xdda0001DataRespDto.setOpMsg("同步档案台账以及借阅明细状态");
        }catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0003.key, DscmsEnum.TRADE_CODE_XDDA0003.value, e.getMessage());
            xdda0001DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdda0001DataRespDto.setOpMsg("同步档案台账以及借阅明细状态");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0003.key, DscmsEnum.TRADE_CODE_XDDA0003.value, JSON.toJSONString(xdda0001DataRespDto));
        return xdda0001DataRespDto;
    }
}
