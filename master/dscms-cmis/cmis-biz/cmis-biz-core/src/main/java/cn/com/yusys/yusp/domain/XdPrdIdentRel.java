/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: XdPrdIdentRel
 * @类描述: xd_prd_ident_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-10-13 22:47:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "xd_prd_ident_rel")
public class XdPrdIdentRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = false, length = 10)
	private String bizType;
	
	/** 贷款品种 **/
	@Column(name = "LOAN_TYPE_DETAIL", unique = false, nullable = true, length = 5)
	private String loanTypeDetail;
	
	/** 是否经营性物业贷款 **/
	@Column(name = "IS_JYXWYDK", unique = false, nullable = true, length = 5)
	private String isJyxwydk;
	
	/** 是否钢贸行业贷款 **/
	@Column(name = "IS_GMHY", unique = false, nullable = true, length = 5)
	private String isGmhy;
	
	/** 是否不锈钢行业贷款 **/
	@Column(name = "IS_BXGHY", unique = false, nullable = true, length = 5)
	private String isBxghy;
	
	/** 是否扶贫贴息贷款 **/
	@Column(name = "IS_FPTXDK", unique = false, nullable = true, length = 5)
	private String isFptxdk;
	
	/** 是否保障性安居工程贷款 **/
	@Column(name = "IS_BZXAJGC", unique = false, nullable = true, length = 5)
	private String isBzxajgc;
	
	/** 创业担保贷款类型 **/
	@Column(name = "IS_XGSYRYXEDB", unique = false, nullable = true, length = 10)
	private String isXgsyryxedb;
	
	/** 贷款用途类型 **/
	@Column(name = "LOAN_USE_TYPE", unique = false, nullable = true, length = 10)
	private String loanUseType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail;
	}
	
    /**
     * @return loanTypeDetail
     */
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}
	
	/**
	 * @param isJyxwydk
	 */
	public void setIsJyxwydk(String isJyxwydk) {
		this.isJyxwydk = isJyxwydk;
	}
	
    /**
     * @return isJyxwydk
     */
	public String getIsJyxwydk() {
		return this.isJyxwydk;
	}
	
	/**
	 * @param isGmhy
	 */
	public void setIsGmhy(String isGmhy) {
		this.isGmhy = isGmhy;
	}
	
    /**
     * @return isGmhy
     */
	public String getIsGmhy() {
		return this.isGmhy;
	}
	
	/**
	 * @param isBxghy
	 */
	public void setIsBxghy(String isBxghy) {
		this.isBxghy = isBxghy;
	}
	
    /**
     * @return isBxghy
     */
	public String getIsBxghy() {
		return this.isBxghy;
	}
	
	/**
	 * @param isFptxdk
	 */
	public void setIsFptxdk(String isFptxdk) {
		this.isFptxdk = isFptxdk;
	}
	
    /**
     * @return isFptxdk
     */
	public String getIsFptxdk() {
		return this.isFptxdk;
	}
	
	/**
	 * @param isBzxajgc
	 */
	public void setIsBzxajgc(String isBzxajgc) {
		this.isBzxajgc = isBzxajgc;
	}
	
    /**
     * @return isBzxajgc
     */
	public String getIsBzxajgc() {
		return this.isBzxajgc;
	}
	
	/**
	 * @param isXgsyryxedb
	 */
	public void setIsXgsyryxedb(String isXgsyryxedb) {
		this.isXgsyryxedb = isXgsyryxedb;
	}
	
    /**
     * @return isXgsyryxedb
     */
	public String getIsXgsyryxedb() {
		return this.isXgsyryxedb;
	}
	
	/**
	 * @param loanUseType
	 */
	public void setLoanUseType(String loanUseType) {
		this.loanUseType = loanUseType;
	}
	
    /**
     * @return loanUseType
     */
	public String getLoanUseType() {
		return this.loanUseType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}