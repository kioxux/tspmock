/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.SequenceDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardAppInfo;
import cn.com.yusys.yusp.service.CreditCardAppInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAppInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 11:06:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "信用卡申请信息")
@RequestMapping("/api/creditcardappinfo")
public class CreditCardAppInfoResource {
    @Autowired
    private CreditCardAppInfoService creditCardAppInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardAppInfo> list = creditCardAppInfoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardAppInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardAppInfo>> index(QueryModel queryModel) {
        List<CreditCardAppInfo> list = creditCardAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CreditCardAppInfo> show(@PathVariable("serno") String serno) {
        CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CreditCardAppInfo>(creditCardAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CreditCardAppInfo> create(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        creditCardAppInfoService.insert(creditCardAppInfo);
        return new ResultDto<CreditCardAppInfo>(creditCardAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请信息预录入修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        int result = creditCardAppInfoService.update(creditCardAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请信息预录入根据主键删除")
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        int result = creditCardAppInfoService.deleteByPrimaryKey(creditCardAppInfo.getSerno());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请信息预录入批量对象删除")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 14:44
            * @version 1.0.0
            * @desc 信用卡申请信息列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡申请信息列表查询")
    @PostMapping("/querybycondition")
    protected ResultDto<List<CreditCardAppInfo>> queryByCondition(@RequestBody QueryModel queryModel) {
        List<CreditCardAppInfo> list = creditCardAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAppInfo>>(list);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 14:46
     * @version 1.0.0
     * @desc 根据主键查询信用卡申请信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据主键查询信用卡申请信息")
    @PostMapping("/querybyserno")
    protected ResultDto<CreditCardAppInfo> queryBySerno(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        CreditCardAppInfo creditCardApp = creditCardAppInfoService.selectByPrimaryKey(creditCardAppInfo.getSerno());
        return new ResultDto<CreditCardAppInfo>(creditCardApp);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  信用卡申请新增向导
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡申请信息预录入")
    @PostMapping("/addcreditappinfo")
    protected ResultDto<CreditCardAppInfo> addCreditAppInfo(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        CreditCardAppInfo creditCardApp = creditCardAppInfoService.addCreditAppInfo(creditCardAppInfo);
        return new ResultDto<CreditCardAppInfo>(creditCardApp);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  信用卡申请新增向导
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡申请信息主副同申查看")
    @PostMapping("/querycreditappinfo")
    protected ResultDto<CreditCardAppInfo> updateCreditAppInfo(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        CreditCardAppInfo creditCardApp = creditCardAppInfoService.addCreditAppInfo(creditCardAppInfo);
        return new ResultDto<CreditCardAppInfo>(creditCardApp);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  信用卡申请新增向导
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡零售内评接口")
    @PostMapping("/xyklsnp")
    protected ResultDto<Map> hand(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map result = creditCardAppInfoService.handleBusinessDataAfterRun(creditCardAppInfo.getSerno());
       return  new ResultDto<Map>(result);
    }
    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  主卡卡号：校验卡号的有效性及状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("主卡卡号：校验卡号的有效性及状态")
    @PostMapping("/confirmcard")
    protected ResultDto<Map> confirmCard(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map result = creditCardAppInfoService.confirmCard(creditCardAppInfo);
        return  new ResultDto<Map>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  校验自选卡号是否已存在
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("校验自选卡号是否已存在")
    @PostMapping("/confirmcardalive")
    protected ResultDto<Map> confirmCardAlive(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map result = creditCardAppInfoService.confirmCardAlive(creditCardAppInfo);
        return  new ResultDto<Map>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  约定还款扣款账号校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("约定还款扣款账号校验")
    @PostMapping("/confirmrepaycard")
    protected ResultDto<Map> confirmRepayCard(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map result = creditCardAppInfoService.confirmRepayCard(creditCardAppInfo.getAgreedRepayCardNo());
        return  new ResultDto<Map>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  约定还款扣款账号校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("流程结束处理方法")
    @PostMapping("/processend")
    protected ResultDto<Map> processend(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        creditCardAppInfoService.handleBusinessDataAfterEnd(creditCardAppInfo.getSerno(),"1","242_12");
        return  null;
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  约定还款扣款账号校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡复议")
    @PostMapping("/reconsider")
    protected ResultDto<Map> reconsider(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map map = creditCardAppInfoService.reconsider(creditCardAppInfo.getSerno());
        return new ResultDto<Map>(map);
    }

    /**
     * @param creditCardAppInfo
     * @return ResultDto
     * @author wzy
     * @date 2021/5/24 15:06
     * @version 1.0.0
     * @desc  约定还款扣款账号校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("获取额度标志")
    @PostMapping("/querylsnp")
    protected ResultDto<Map> querylsnp(@RequestBody CreditCardAppInfo creditCardAppInfo) {
        Map map = creditCardAppInfoService.queryLsnp(creditCardAppInfo.getSerno());
        return new ResultDto<Map>(map);
    }
    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  信用卡申请信息补充录入退回
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡申请信息补充录入退回")
    @PostMapping("/returnmainsup")
    protected ResultDto<Integer> returnMain(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        int result = creditCardAppInfoService.returnMain(creditCardAppInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  信用卡征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡征信查询")
    @PostMapping("/getcreditreportinfo")
    protected ResultDto<Integer> getCreditReportInfo(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        int result = creditCardAppInfoService.getCreditReportInfo(creditCardAppInfo.getSerno());
        return new ResultDto<Integer>(result);
    }
    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  查询信用卡征信地址
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查询信用卡征信地址")
    @PostMapping("/selectreporturlbySerno")
    protected ResultDto<CreditReportQryLst> selectReportUrlBySerno(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        CreditReportQryLst result = creditCardAppInfoService.selectReportUrlBySerno(creditCardAppInfo);
        return new ResultDto<CreditReportQryLst>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  归档任务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("归档任务")
    @PostMapping("/getdocarchiveclient")
    protected ResultDto<CreditCardAppInfo> getDocArchiveClient(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        CreditCardAppInfo result = creditCardAppInfoService.getDocArchiveClient(creditCardAppInfo);
        return new ResultDto<CreditCardAppInfo>(result);
    }

    /**
     * @param sequenceDto
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  流水号生成
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("流水号生成")
    @PostMapping("/getserno")
    protected ResultDto<SequenceDto> getSerno(@RequestBody SequenceDto sequenceDto) throws URISyntaxException {
        SequenceDto result = creditCardAppInfoService.getSerno(sequenceDto);
        return new ResultDto<SequenceDto>(result);
    }

    /**
     * @param adminSmUserDto
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  获取客户经理信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("获取客户经理信息")
    @PostMapping("/getmanagername")
    protected ResultDto<AdminSmUserDto> getManagerName(@RequestBody AdminSmUserDto adminSmUserDto) throws URISyntaxException {
        AdminSmUserDto result = creditCardAppInfoService.getManagerName(adminSmUserDto);
        return new ResultDto<AdminSmUserDto>(result);
    }

    /**
     * @param creditCardAppInfo
     * @return
     * @author wzy
     * @date 2021/6/9 16:06
     * @version 1.0.0
     * @desc  获取客户经理信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("app端申请信息处理")
    @PostMapping("/handapptogether")
    protected ResultDto<Map> handAppTogether(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
        Map result = creditCardAppInfoService.handAppTogether(creditCardAppInfo);
        return new ResultDto<>(result);
    }

    @ApiOperation("额度生成")
    @PostMapping("/handleBusinessDatFirst")
    protected ResultDto<Map> handleBusinessDatFirst(@RequestBody CreditCardAppInfo creditCardAppInfo) throws URISyntaxException {
         creditCardAppInfoService.handleBusinessDatFirst(creditCardAppInfo.getSerno(),"242_13");
        return new ResultDto<>(null);
    }
}
