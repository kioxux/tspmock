/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperFinLeasCompanyRisk
 * @类描述: rpt_oper_fin_leas_company_risk数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 21:11:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_fin_leas_company_risk")
public class RptOperFinLeasCompanyRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 发生年度 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 80)
	private String inputYear;
	
	/** 发生额 **/
	@Column(name = "HAPPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal happenAmt;
	
	/** 已收回金额 **/
	@Column(name = "RECEIVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal receiveAmt;
	
	/** 未收回金额 **/
	@Column(name = "NO_RECEIVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal noReceiveAmt;
	
	/** 进度 **/
	@Column(name = "PROGRESS", unique = false, nullable = true, length = 200)
	private String progress;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param happenAmt
	 */
	public void setHappenAmt(java.math.BigDecimal happenAmt) {
		this.happenAmt = happenAmt;
	}
	
    /**
     * @return happenAmt
     */
	public java.math.BigDecimal getHappenAmt() {
		return this.happenAmt;
	}
	
	/**
	 * @param receiveAmt
	 */
	public void setReceiveAmt(java.math.BigDecimal receiveAmt) {
		this.receiveAmt = receiveAmt;
	}
	
    /**
     * @return receiveAmt
     */
	public java.math.BigDecimal getReceiveAmt() {
		return this.receiveAmt;
	}
	
	/**
	 * @param noReceiveAmt
	 */
	public void setNoReceiveAmt(java.math.BigDecimal noReceiveAmt) {
		this.noReceiveAmt = noReceiveAmt;
	}
	
    /**
     * @return noReceiveAmt
     */
	public java.math.BigDecimal getNoReceiveAmt() {
		return this.noReceiveAmt;
	}
	
	/**
	 * @param progress
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}
	
    /**
     * @return progress
     */
	public String getProgress() {
		return this.progress;
	}


}