/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import org.apache.poi.hpsf.Decimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAsplTcont
 * @类描述: doc_aspl_tcont数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:15:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_aspl_tcont")
public class DocAsplTcont extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 贸易合同影像流水号 **/
	@Column(name = "TCONT_IMG_ID", unique = false, nullable = false, length = 40)
	private String tcontImgId;
	
	/** 贸易合同编号 **/
	@Column(name = "TCONT_NO", unique = false, nullable = false, length = 40)
	private String tcontNo;
	
	/** 贸易合同名称 **/
	@Column(name = "TCONT_CN_NAME", unique = false, nullable = true, length = 500)
	private String tcontCnName;

	/** 资产池协议编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 合同起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;

	/** 合同到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 供货方 **/
	@Column(name = "SUPPLIER_NAME", unique = false, nullable = true, length = 500)
	private String supplierName;
	
	/** 消费方 **/
	@Column(name = "CONSUME_NAME", unique = false, nullable = true, length = 500)
	private String consumeName;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 500)
	private String prdName;
	
	/** 产品数量 **/
	@Column(name = "PRD_QNT", unique = false, nullable = true, length = 20)
	private String prdQnt;
	
	/** 产品单价 **/
	@Column(name = "PRD_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal prdPrice;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 签约日期 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 20)
	private String signDate;
	
	/** 交付日期 **/
	@Column(name = "DELIVER_DATE", unique = false, nullable = true, length = 20)
	private String deliverDate;
	
	/** 交付方式 **/
	@Column(name = "DELIVER_TYPE", unique = false, nullable = true, length = 5)
	private String deliverType;

	/** 交易对手名称 **/
	@Column(name = "TOPP_NAME", unique = false, nullable = true, length = 40)
	private String toppName;

	/** 交易对手账号 **/
	@Column(name = "TOPP_ACCT_NO", unique = false, nullable = true, length = 40)
	private String toppAcctNo;

	/** 交易对手是否本行账户STD_ZB_YES_NO **/
	@Column(name = "IS_BANK_ACCT", unique = false, nullable = true, length = 5)
	private String isBankAcct;

	/** 开户行行号 **/
	@Column(name = "OPAN_ORG_NO", unique = false, nullable = true, length = 40)
	private String opanOrgNo;

	/** 开户行行名 **/
	@Column(name = "OPAN_ORG_NAME", unique = false, nullable = true, length = 40)
	private String opanOrgName;

	/** 可用信总金额 **/
	@Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal contHighAvlAmt;

	/** 结算方式STD_ZB_SETTLE_METH **/
	@Column(name = "SETTLEMENT_METHOD", unique = false, nullable = true, length = 5)
	private String settlementMethod;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_Name", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 40)
	private String loanTer;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getTcontImgId() {
		return tcontImgId;
	}

	public void setTcontImgId(String tcontImgId) {
		this.tcontImgId = tcontImgId;
	}

	public String getTcontNo() {
		return tcontNo;
	}

	public void setTcontNo(String tcontNo) {
		this.tcontNo = tcontNo;
	}

	public String getTcontCnName() {
		return tcontCnName;
	}

	public void setTcontCnName(String tcontCnName) {
		this.tcontCnName = tcontCnName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getConsumeName() {
		return consumeName;
	}

	public void setConsumeName(String consumeName) {
		this.consumeName = consumeName;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getPrdQnt() {
		return prdQnt;
	}

	public void setPrdQnt(String prdQnt) {
		this.prdQnt = prdQnt;
	}

	public BigDecimal getPrdPrice() {
		return prdPrice;
	}

	public void setPrdPrice(BigDecimal prdPrice) {
		this.prdPrice = prdPrice;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getDeliverDate() {
		return deliverDate;
	}

	public void setDeliverDate(String deliverDate) {
		this.deliverDate = deliverDate;
	}

	public String getDeliverType() {
		return deliverType;
	}

	public void setDeliverType(String deliverType) {
		this.deliverType = deliverType;
	}

	public String getToppName() {
		return toppName;
	}

	public void setToppName(String toppName) {
		this.toppName = toppName;
	}

	public String getToppAcctNo() {
		return toppAcctNo;
	}

	public void setToppAcctNo(String toppAcctNo) {
		this.toppAcctNo = toppAcctNo;
	}

	public String getIsBankAcct() {
		return isBankAcct;
	}

	public void setIsBankAcct(String isBankAcct) {
		this.isBankAcct = isBankAcct;
	}

	public String getOpanOrgNo() {
		return opanOrgNo;
	}

	public void setOpanOrgNo(String opanOrgNo) {
		this.opanOrgNo = opanOrgNo;
	}

	public String getOpanOrgName() {
		return opanOrgName;
	}

	public void setOpanOrgName(String opanOrgName) {
		this.opanOrgName = opanOrgName;
	}

	public BigDecimal getContHighAvlAmt() {
		return contHighAvlAmt;
	}

	public void setContHighAvlAmt(BigDecimal contHighAvlAmt) {
		this.contHighAvlAmt = contHighAvlAmt;
	}

	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getLoanTer() {
		return loanTer;
	}

	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}
}