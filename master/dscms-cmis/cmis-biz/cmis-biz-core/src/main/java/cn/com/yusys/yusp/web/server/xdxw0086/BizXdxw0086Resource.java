package cn.com.yusys.yusp.web.server.xdxw0086;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0086.req.Xdxw0086DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0086.resp.Xdxw0086DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0086.Xdxw0086Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:涉税保密信息查询委托授权书文本生产PDF
 *
 * @author zrcb
 * @version 1.0
 */
@Api(tags = "XDXW0086:涉税保密信息查询委托授权书文本生产PDF")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0086Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0086Resource.class);

    @Autowired
    private Xdxw0086Service xdxw0086Service;

    /**
     * 交易码：xdxw0086
     * 交易描述：涉税保密信息查询委托授权书文本生产PDF
     *
     * @param xdxw0086DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("涉税保密信息查询委托授权书文本生产PDF")
    @PostMapping("/xdxw0086")
    protected @ResponseBody
    ResultDto<Xdxw0086DataRespDto> xdxw0086(@Validated @RequestBody Xdxw0086DataReqDto xdxw0086DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataReqDto));
        Xdxw0086DataRespDto xdxw0086DataRespDto = new Xdxw0086DataRespDto();// 响应Dto:优企贷还款账号变更
        ResultDto<Xdxw0086DataRespDto> xdxw0086DataResultDto = new ResultDto<>();
        try {
            String companyName = xdxw0086DataReqDto.getCompanyName();
            String signDate = xdxw0086DataReqDto.getSignDate();

            if(StringUtil.isEmpty(companyName)||StringUtil.isEmpty(signDate)){
                xdxw0086DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0086DataResultDto.setMessage("参数不能为空");
                return xdxw0086DataResultDto;
            }
            // 从xdxw0086DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataReqDto));
            xdxw0086DataRespDto = xdxw0086Service.xdxw0086(xdxw0086DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataRespDto));
            // 封装xdxw0086DataResultDto中正确的返回码和返回信息
            xdxw0086DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0086DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch(BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, e.getMessage());
            // 封装xdxw0086DataResultDto中异常返回码和返回信息
            xdxw0086DataResultDto.setCode(e.getErrorCode());
            xdxw0086DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, e.getMessage());
            // 封装xdxw0086DataResultDto中异常返回码和返回信息
            xdxw0086DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0086DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0086DataRespDto到xdxw0086DataResultDto中
        xdxw0086DataResultDto.setData(xdxw0086DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataResultDto));
        return xdxw0086DataResultDto;
    }
}
