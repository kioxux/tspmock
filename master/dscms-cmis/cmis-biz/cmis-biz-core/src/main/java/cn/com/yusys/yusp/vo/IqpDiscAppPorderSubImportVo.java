package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.time.LocalDate;

@ExcelCsv(namePrefix = "贴现汇票明细导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class IqpDiscAppPorderSubImportVo {

    /*
    票据号 DRFT_NO
     */
    @ExcelField(title = "票据号", viewLength = 40)
    private String drftNo;

    /*
    票面金额 DRFT_AMT 16,2
     */
    @ExcelField(title = "票面金额", viewLength = 16)
    private BigDecimal drftAmt;

    /*
    到期日期 END_DATE
     */
    @ExcelField(title = "到期日", viewLength = 20)
    private String endDate;

    /*
    出票人 DRWR
   */
    @ExcelField(title = "出票人", viewLength = 20)
    private String drwr;

    /*
    收款人 PYEE
     */
    @ExcelField(title = "收款人", viewLength = 20)
    private String pyee;

    /*
    承兑人 ACCPTR
     */
    @ExcelField(title = "承兑人", viewLength = 80)
    private String accptr;

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public BigDecimal getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(BigDecimal drftAmt) {
        this.drftAmt = drftAmt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDrwr() {
        return drwr;
    }

    public void setDrwr(String drwr) {
        this.drwr = drwr;
    }

    public String getPyee() {
        return pyee;
    }

    public void setPyee(String pyee) {
        this.pyee = pyee;
    }

    public String getAccptr() {
        return accptr;
    }

    public void setAccptr(String accptr) {
        this.accptr = accptr;
    }
}

