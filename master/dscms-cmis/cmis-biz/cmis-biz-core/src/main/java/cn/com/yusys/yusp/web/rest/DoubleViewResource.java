package cn.com.yusys.yusp.web.rest;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.DoubleViewDto;
import cn.com.yusys.yusp.service.DoubleViewServices;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DoubleViewResource
 * @类描述: #双录信息查询
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-30 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "双录信息查询")
@RequestMapping("/api/doubleview")
public class DoubleViewResource {
    @Autowired
    private DoubleViewServices doubleViewServices;

    /**
     * @创建人 XLL
     * @创建时间 2021/8/30 14:48
     * @注释 双录信息查询
     */
    @PostMapping("/querymessage")
    protected ResultDto<List<DoubleViewDto>> querymessage(@RequestBody DoubleViewDto doubleViewDto) {
        List<DoubleViewDto> doubleViewDtolist = doubleViewServices.querymessage(doubleViewDto);
        return new ResultDto<List<DoubleViewDto>>(doubleViewDtolist);
    }

}
