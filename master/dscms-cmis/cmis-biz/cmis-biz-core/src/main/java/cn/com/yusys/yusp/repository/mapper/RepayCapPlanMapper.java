/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RepayCapPlan;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RepayCapPlanMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 72908
 * @创建时间: 2021-05-06 17:42:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface RepayCapPlanMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    RepayCapPlan selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<RepayCapPlan> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(RepayCapPlan record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(RepayCapPlan record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(RepayCapPlan record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(RepayCapPlan record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据集团申请流水号查询还款计划
     * @param grpSerno
     * @return
     */
    ArrayList<RepayCapPlan> queryRepayCapPlanByGrpSerno(String grpSerno);

    /**
     * @方法名称: selectByParams
     * @方法描述: 根据入参条件查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<RepayCapPlan> selectByParams(Map map);

    /**
     * @方法名称: updateByPkId
     * @方法描述: 根据主键实现逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPkId(RepayCapPlan repayCapPlan);

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<RepayCapPlan> selectByIqpSerno( QueryModel queryModel);

    /**
     * @方法名称: selectRepayCapPlanListByApproveSerno
     * @方法描述: 根据授信申请流水号初始化查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<Map> selectRepayCapPlanListByApproveSerno(String approveSerno);

    /**
     * @方法名称: deleteBySerno
     * @方法描述: 根据流水号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteBySerno(@Param("serno") String serno);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据流水号 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySerno(QueryModel model);

    List<Map> selectByLmtSerno(String serno);

    List<Map> selectByGrpSerno(String grpSerno);
}