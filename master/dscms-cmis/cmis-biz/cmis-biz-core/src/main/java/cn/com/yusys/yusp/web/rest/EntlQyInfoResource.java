/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.EntlQyInfo;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.service.EntlQyInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EntlQyInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/entlqyinfo")
public class EntlQyInfoResource {
    @Autowired
    private EntlQyInfoService entlQyInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<EntlQyInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<EntlQyInfo> list = entlQyInfoService.selectAll(queryModel);
        return new ResultDto<List<EntlQyInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<EntlQyInfo>> index(QueryModel queryModel) {
        List<EntlQyInfo> list = entlQyInfoService.selectByModel(queryModel);
        return new ResultDto<List<EntlQyInfo>>(list);
    }

    /**
     * @函数名称:queryEntlQyInfo
     * @函数描述:房抵e点贷企业经营信息分页查询
     * @参数与返回说明:queryModel
     * @创建人:zhangliang15
     */
    @ApiOperation("房抵e点贷企业经营信息分页查询")
    @PostMapping("/queryEntlQyInfo")
    protected ResultDto<List<EntlQyInfo>> queryEntlQyInfo(@RequestBody QueryModel queryModel) {
        List<EntlQyInfo> list = entlQyInfoService.queryEntlQyInfo(queryModel);
        return new ResultDto<List<EntlQyInfo>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过面签流水号查询企业详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过面签流水号查询企业详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        EntlQyInfo entlQyInfo = entlQyInfoService.selectBySerno((String) params.get("serno"));
        if (entlQyInfo != null) {
            resultDto.setCode(200);
            resultDto.setData(entlQyInfo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<EntlQyInfo> show(@PathVariable("serno") String serno) {
        EntlQyInfo entlQyInfo = entlQyInfoService.selectByPrimaryKey(serno);
        return new ResultDto<EntlQyInfo>(entlQyInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<EntlQyInfo> create(@RequestBody EntlQyInfo entlQyInfo) throws URISyntaxException {
        entlQyInfoService.insert(entlQyInfo);
        return new ResultDto<EntlQyInfo>(entlQyInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody EntlQyInfo entlQyInfo) throws URISyntaxException {
        int result = entlQyInfoService.update(entlQyInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = entlQyInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = entlQyInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateEntlQyInfo
     * @函数描述:企业，房产信息修改将信息同步发送风控 调用fb1214
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("企业，房产信息修改将信息同步发送风控")
    @PostMapping("/updateEntlQyInfo")
    protected ResultDto<String> updateEntlQyInfo(@RequestBody EntlQyInfo entlQyInfo) {
        return  entlQyInfoService.updateEntlQyInfo(entlQyInfo);
    }
}
