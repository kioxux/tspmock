package cn.com.yusys.yusp.service.server.xdcz0014;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0014.req.Xdcz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.resp.Xdcz0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.GroupMapList;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpAccpAppMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cz模块
 * @类名称: Xdcz0014Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-11 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0014Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0014Service.class);

    @Resource
    private PvpAccpAppMapper pvpAccpAppMapper;
   // @Resource
    //private CusIntbankMapper cusIntbankMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 企业网银查询影像补录批次
     *
     * @param xdcz0014DataReqDto
     * @return
     */
    @Transactional
    public Xdcz0014DataRespDto xdcz0014(Xdcz0014DataReqDto xdcz0014DataReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataReqDto));
        Xdcz0014DataRespDto xdcz0014DataRespDto = new Xdcz0014DataRespDto();
        String cusId = xdcz0014DataReqDto.getCusId();//客户编号
        Integer startPageNum = Integer.parseInt(xdcz0014DataReqDto.getStartPageNum());//起始页数
        Integer pageSize =  Integer.parseInt(xdcz0014DataReqDto.getPageSize());//分页大小
        String batchNo = xdcz0014DataReqDto.getBatchNo();//批次号

        try {
            //企业网银查询需要影像补录的出账批次，信贷先判断是否存在批次号，存在则查询pvp_accp_app中批次编号和影像流水号；
            // 不存在则查询该客户项下所有需要影像补录的出账批次号和影像流水号
            //分页查询设置
            PageHelper.startPage(startPageNum, pageSize);
            //存在批次号查询
            if(StringUtils.nonEmpty(batchNo)){
                //mapper调用，查询数据
                //String sql = "select pc_serno as hx_cont_no, case when yxserno is not null then yxserno else serno end as yx_serno, cont_no from pvp_accp_app where pc_serno = '"+pc_serno+"'";
                //根据批次号查询影像流水号
                List<cn.com.yusys.yusp.dto.server.xdcz0014.resp.List > list = pvpAccpAppMapper.selectVideoNoByPcSerno(xdcz0014DataReqDto);
                xdcz0014DataRespDto.setTotalQnt(list.size()+"");
                xdcz0014DataRespDto.setList(list);
            }else{

                //不存在批次号
                List<cn.com.yusys.yusp.dto.server.xdcz0014.resp.List > list2 = pvpAccpAppMapper.selectVideoNoByPcSerno(xdcz0014DataReqDto);
                xdcz0014DataRespDto.setTotalQnt(list2.size()+"");
                xdcz0014DataRespDto.setList(list2);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, e.getMessage());
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataReqDto));
        return xdcz0014DataRespDto;
    }
}
