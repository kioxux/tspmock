package cn.com.yusys.yusp.domain.bat;

import cn.com.yusys.yusp.enums.batch.BatEnums;

public class BatBizWydPojo {
    /******原表******/
    private String tableA;
    /******临时表******/
    private String tableB;
    /******表字段******/
    private String columns;
    /******表主键******/
    private String primary;
    /******对应数据文件******/
    private String dataFileName;

    public String getTableA() {
        return tableA;
    }

    public void setTableA(String tableA) {
        this.tableA = tableA;
    }

    public String getTableB() {
        return tableB;
    }

    public void setTableB(String tableB) {
        this.tableB = tableB;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }
}
