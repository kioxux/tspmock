package cn.com.yusys.yusp.service.server.xdqt0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LoanBookingRegister;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdqt0004.req.Xdqt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.resp.Xdqt0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LoanBookingRegisterMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 接口处理类:贷款申请预约（个人客户）
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdqt0004Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdqt0004Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Resource
    private AdminSmUserService adminSmUserService;

    @Resource
    private AdminSmOrgService adminSmOrgService;

    @Resource
    private CommonService commonService;

    @Autowired
    private SenddxService senddxService;

    @Resource
    private LoanBookingRegisterMapper loanBookingRegisterMapper;

    /**
     * 企业网银推送预约信息
     *
     * @param xdqt0004DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdqt0004DataRespDto WYtoCmisAction(Xdqt0004DataReqDto xdqt0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value);
        //返回对象
        Xdqt0004DataRespDto xdqt0004DataRespDto = new Xdqt0004DataRespDto();

        String loanServerBankNo = xdqt0004DataReqDto.getLoanBank();//贷款服务行
        String loanServerBankAddr = xdqt0004DataReqDto.getAddr();//地址
        String loanServerBankPost = xdqt0004DataReqDto.getProcde();//邮编
        String loanServerBankPhone = xdqt0004DataReqDto.getPhone();//电话
        String loanPurp = xdqt0004DataReqDto.getLoanUse();//贷款用途
        BigDecimal guarAmt = xdqt0004DataReqDto.getLoanBal();//贷款金额
        String guarTerm = xdqt0004DataReqDto.getLoanTerm();//贷款期限
        String guarMode = xdqt0004DataReqDto.getGuarType();//担保方式
        String cusName = xdqt0004DataReqDto.getCusName();//姓名
        String sex = xdqt0004DataReqDto.getSex();//性别
        String certType = xdqt0004DataReqDto.getCertType();//证件类型
        String certCode = xdqt0004DataReqDto.getCertNo();//证件号
        String phone = xdqt0004DataReqDto.getMobile();//手机号
        String edu = xdqt0004DataReqDto.getEdu();//学历
        String marStatus = xdqt0004DataReqDto.getMarStatus();//婚姻状况
        String unitName = xdqt0004DataReqDto.getIndivRelComName();//单位名称
        String indivComTyp = xdqt0004DataReqDto.getIndivComTyp();//单位性质
        String isLocalRegist = xdqt0004DataReqDto.getIsLocal();//是否本地户口
        BigDecimal indivYearn = xdqt0004DataReqDto.getYearn();//年收入
        String pawnType = xdqt0004DataReqDto.getPldType();//抵押物类型
        String pledType = xdqt0004DataReqDto.getCollType();//质押物类型
        String duty = xdqt0004DataReqDto.getDuty();//职务
        String resiAddr = xdqt0004DataReqDto.getIndivRsdAddr();//居住地址
        String infoSource = xdqt0004DataReqDto.getInfoSour();//信息来源
        String loanType = xdqt0004DataReqDto.getLoanType();//经营或消费性贷款
        String serno = "";
        try {
            if (loanServerBankNo == null || "".equals(loanServerBankNo)) {
                throw new Exception("贷款服务行为空!");
            }
            if (loanServerBankAddr == null || "".equals(loanServerBankAddr)) {
                throw new Exception("地址为空!");
            }
            if (infoSource == null || "".equals(infoSource)) {
                throw new Exception("信息来源为空!");
            }
            if (loanType == null || "".equals(loanType)) {
                throw new Exception("经营或消费性贷款为空!");
            }
            if (resiAddr == null || "".equals(resiAddr)) {
                throw new Exception("居住地址为空!");
            }
            if (loanServerBankPost == null || "".equals(loanServerBankPost)) {
                throw new Exception("邮编为空!");
            }
            if (phone == null || "".equals(phone)) {
                throw new Exception("电话为空!");
            }
            if (loanPurp == null || "".equals(loanPurp)) {
                throw new Exception("贷款用途为空!");
            }
            if (guarAmt == null) {
                throw new Exception("贷款金额为空!");
            }
            if (guarTerm == null || "".equals(guarTerm)) {
                throw new Exception("贷款期限为空!");
            }
            if (guarMode == null || "".equals(guarMode)) {
                throw new Exception("担保方式为空!");
            }
            if (cusName == null || "".equals(cusName)) {
                throw new Exception("姓名为空!");
            }
            if (sex == null || "".equals(sex)) {
                throw new Exception("性别为空!");
            }
            if (certType == null || "".equals(certType)) {
                throw new Exception("证件类型为空!");
            }
            if (certCode == null || "".equals(certCode)) {
                throw new Exception("证件号为空!");
            }
            if (edu == null || "".equals(edu)) {
                throw new Exception("学历为空!");
            }
            if (marStatus == null || "".equals(marStatus)) {
                throw new Exception("婚姻状况为空!");
            }
            if (unitName == null || "".equals(unitName)) {
                throw new Exception("单位名称为空!");
            }
            if (indivComTyp == null || "".equals(indivComTyp)) {
                throw new Exception("单位性质为空!");
            }
            if (isLocalRegist == null || "".equals(isLocalRegist)) {
                throw new Exception("是否本地户口为空!");
            }
            if (indivYearn == null) {
                throw new Exception("年收入为空!");
            }
            if (pawnType == null || "".equals(pawnType)) {
                throw new Exception("抵押物类型为空!");
            }
            if (pledType == null || "".equals(pledType)) {
                throw new Exception("质押物类型为空!");
            }
            if (duty == null || "".equals(duty)) {
                throw new Exception("职务为空!");
            }

            String upOrgId = "";//上级机构
            /* 根据贷款服务行查询对应的上级机构 */
            ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(loanServerBankNo);
            if (ResultDto.success().getCode().equals(resultOrgDto.getCode())) {
                AdminSmOrgDto adminSmOrgrDto = resultOrgDto.getData();
                if (!Objects.isNull(adminSmOrgrDto)) {
                    upOrgId = adminSmOrgrDto.getUpOrgId();
                }
            }
            String actorno = "";
            String telNum = "";
            /* 根据上级机构找到关联的支行客户总经理 */
            GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
            UserAndDutyRespDto userAndDutyRespDto = new UserAndDutyRespDto();
            //支行总经理 角色编号待确认
//            getUserInfoByRoleCodeDto.setRoleCode("1004");
//            getUserInfoByRoleCodeDto.setPageNum(1);
//            getUserInfoByRoleCodeDto.setPageSize(999999);
//            List<AdminSmUserDto> adminSmUserDtos = commonService.getUserInfoByRoleCode(getUserInfoByRoleCodeDto);

            //根据机构信息、岗位信息查询出对应的人
            UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
            userAndDutyReqDto.setDutyNo("FZH12");//支行行长
            userAndDutyReqDto.setOrgId(upOrgId);//机构
            ResultDto<List<UserAndDutyRespDto>> userAndDutyRespDtos = adminSmUserService.getUserAndDuty(userAndDutyReqDto);
            if (CollectionUtils.nonNull(userAndDutyRespDtos)) {
                List<UserAndDutyRespDto> userAndDutyRespDtolist = userAndDutyRespDtos.getData();
                if (CollectionUtils.nonNull(userAndDutyRespDtolist)) {
                    userAndDutyRespDto = userAndDutyRespDtolist.get(0);
                    actorno = userAndDutyRespDto.getActorNo();
                    telNum = userAndDutyRespDto.getPhone();
                }
            }


            // 判断是否是小贷中心进行受理，若是，则不需要关联客户总经理
            if ("01".equals(loanType) && guarAmt.intValue() <= 1000000) {
                actorno = "01";
                // 查询贷款服务行对应的客户经理的电话号码
                //  --------- 需通过调用参数表接口 -----------
                //select b.telnum from wy_to_cmis_manager a,s_user b
                // where a.manager_id=b.actorno and a.oragan_zh=?
            }
            if (actorno == null) {
                throw new Exception("查不到对应的支行");
            } else {
                // 查询是否已经存在贷款信息
                Map<String, String> map = new HashMap<>();
                map.put("cusName", cusName);
                map.put("certCode", certCode);
                String status = loanBookingRegisterMapper.queryIsExistLoanInfo(map);
                if (!"997".equals(status)) {
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());
                    //插入表数据
                    LoanBookingRegister loanBookingRegister = new LoanBookingRegister();
                    loanBookingRegister.setSerno(serno);
                    loanBookingRegister.setLoanServerBankNo(loanServerBankNo);
                    loanBookingRegister.setLoanServerBankAddr(loanServerBankAddr);
                    loanBookingRegister.setLoanServerBankPost(loanServerBankPost);
                    loanBookingRegister.setLoanServerBankPhone(loanServerBankPhone);
                    loanBookingRegister.setLoanPurp(loanPurp);
                    loanBookingRegister.setLoanType(loanType);
                    loanBookingRegister.setGuarMode(guarMode);
                    loanBookingRegister.setGuarAmt(guarAmt);
                    loanBookingRegister.setGuarTerm(Integer.parseInt(guarTerm));
                    loanBookingRegister.setCusName(cusName);
                    loanBookingRegister.setSex(sex);
                    loanBookingRegister.setCertType(certType);
                    loanBookingRegister.setCertCode(certCode);
                    loanBookingRegister.setPhone(phone);
                    loanBookingRegister.setEdu(edu);
                    loanBookingRegister.setMarStatus(marStatus);
                    loanBookingRegister.setUnitName(unitName);
                    loanBookingRegister.setIndivComTyp(indivComTyp);
                    loanBookingRegister.setIsLocalRegist(isLocalRegist);
                    loanBookingRegister.setIndivYearn(indivYearn);
                    loanBookingRegister.setPawnType(pawnType);
                    loanBookingRegister.setPledType(pledType);
                    loanBookingRegister.setDuty(duty);
                    loanBookingRegister.setResiAddr(resiAddr);
                    loanBookingRegister.setInfoSource(infoSource);
                    loanBookingRegister.setLoanStartDate(openDay);
                    loanBookingRegisterMapper.insertSelective(loanBookingRegister);

                    if (StringUtils.isNotBlank(telNum)) {//如果有号码就给客户发短信
                        SenddxReqDto senddxReqDto = new SenddxReqDto();
                        senddxReqDto.setInfopt("dx");
                        SenddxReqList senddxReqList = new SenddxReqList();
                        senddxReqList.setMobile(telNum);
                        senddxReqList.setSmstxt("客户" + cusName + "申请了一笔贷款直通车业务，请在规定的时间内及时处理！");
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key,
                                EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
                        senddxService.senddx(senddxReqDto);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
                    }
                    xdqt0004DataRespDto.setSerno(serno);
                } else {
                    throw new Exception("已存在未审核贷款信息，请不要重复提交！");
                }
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value);
        return xdqt0004DataRespDto;
    }
}
