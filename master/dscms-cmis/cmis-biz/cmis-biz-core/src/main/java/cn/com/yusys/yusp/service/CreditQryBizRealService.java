/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CreditQryBizReal;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.CusInfoQueryDto;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// import cn.com.yusys.yusp.dto.CreditQryBizRealDto;
// import cn.com.yusys.yusp.dto.CusBaseDto;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditQryBizRealService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditQryBizRealService {

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    // @Autowired
    // private CusBaseService cusBaseService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditQryBizReal selectByPrimaryKey(String cqbrSerno) {
        return creditQryBizRealMapper.selectByPrimaryKey(cqbrSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CreditQryBizReal> selectAll(QueryModel model) {
        List<CreditQryBizReal> records = (List<CreditQryBizReal>) creditQryBizRealMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

     public List<CreditQryBizReal> selectByModel(QueryModel model) {
         PageHelper.startPage(model.getPage(), model.getSize());
         List<CreditQryBizReal> list = creditQryBizRealMapper.selectByModel(model);
         PageHelper.clearPage();
         return list;
     }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CreditQryBizReal record) {
        record.setBizSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>()));
        User userInfo = SessionUtils.getUserInformation();
        return creditQryBizRealMapper.insert(record);
    }

    /**
     * @方法名称: createBySerno
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int createBySerno(CreditQryBizReal record) {
        return creditQryBizRealMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CreditQryBizReal record) {
        return creditQryBizRealMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CreditQryBizReal record) {
        return creditQryBizRealMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CreditQryBizReal record) {
        return creditQryBizRealMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cqbrSerno) {
        return creditQryBizRealMapper.deleteByPrimaryKey(cqbrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditQryBizRealMapper.deleteByIds(ids);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/29 19:47
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public List<CreditQryBizReal> selectBySerno(String serno) {
        return creditQryBizRealMapper.selectBySerno(serno);
    }
}
