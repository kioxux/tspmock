/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrintRecord
 * @类描述: print_record数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-13 13:50:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "print_record")
public class PrintRecord extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 批次号 **/
	@Id
	@Column(name = "BATCH_NO")
	private String batchNo;
	
	/** 业务编号 **/
	@Id
	@Column(name = "BIZ_NO")
	private String bizNo;
	
	/** 打印人 **/
	@Column(name = "PRINT_ID", unique = false, nullable = true, length = 20)
	private String printId;
	
	/** 打印时间 **/
	@Column(name = "PRINT_TIME", unique = false, nullable = true, length = 20)
	private String printTime;
	
	/** 打印机构 **/
	@Column(name = "PRINT_BR_ID", unique = false, nullable = true, length = 20)
	private String printBrId;
	
	/** 创建时间 **/
	@Column(name = "CREAT_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date creatTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param batchNo
	 */
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	
    /**
     * @return batchNo
     */
	public String getBatchNo() {
		return this.batchNo;
	}
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	
    /**
     * @return bizNo
     */
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param printId
	 */
	public void setPrintId(String printId) {
		this.printId = printId;
	}
	
    /**
     * @return printId
     */
	public String getPrintId() {
		return this.printId;
	}
	
	/**
	 * @param printTime
	 */
	public void setPrintTime(String printTime) {
		this.printTime = printTime;
	}
	
    /**
     * @return printTime
     */
	public String getPrintTime() {
		return this.printTime;
	}
	
	/**
	 * @param printBrId
	 */
	public void setPrintBrId(String printBrId) {
		this.printBrId = printBrId;
	}
	
    /**
     * @return printBrId
     */
	public String getPrintBrId() {
		return this.printBrId;
	}
	
	/**
	 * @param creatTime
	 */
	public void setCreatTime(java.util.Date creatTime) {
		this.creatTime = creatTime;
	}
	
    /**
     * @return creatTime
     */
	public java.util.Date getCreatTime() {
		return this.creatTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}