/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtCobInfo;
import cn.com.yusys.yusp.service.LmtCobInfoService;
import tk.mybatis.mapper.util.StringUtil;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCobInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-25 19:49:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "共同借款人信息")
@RequestMapping("/api/lmtcobinfo")
public class LmtCobInfoResource {
    @Autowired
    private LmtCobInfoService lmtCobInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtCobInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtCobInfo> list = lmtCobInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtCobInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtCobInfo>> index(QueryModel queryModel) {
        List<LmtCobInfo> list = lmtCobInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCobInfo>>(list);
    }
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.LmtCobInfo>>
     * @author hubp
     * @date 2021/6/2 19:01
     * @version 1.0.0
     * @desc    post查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查询对象列表")
    @PostMapping("/select")
    protected ResultDto<List<LmtCobInfo>> select(@RequestBody QueryModel queryModel) {
        List<LmtCobInfo> list = lmtCobInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCobInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{pkId}")
    protected ResultDto<LmtCobInfo> show(@PathVariable("pkId") String pkId) {
        LmtCobInfo lmtCobInfo = lmtCobInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtCobInfo>(lmtCobInfo);
    }

    /**
     * @函数名称:queryByiqpPkId
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByiqpPkId")
    protected ResultDto<LmtCobInfo> queryByiqpPkId(@RequestBody LmtCobInfo lmtCobInfo) {
        return new ResultDto<LmtCobInfo>(lmtCobInfoService.selectByPrimaryKey(lmtCobInfo.getPkId()));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtCobInfo> create(@RequestBody LmtCobInfo lmtCobInfo) throws URISyntaxException {
        lmtCobInfoService.insert(lmtCobInfo);
        return new ResultDto<LmtCobInfo>(lmtCobInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtCobInfo lmtCobInfo) throws URISyntaxException {
        int result = lmtCobInfoService.updateSelective(lmtCobInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtCobInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtCobInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param
     * @return ResultDto
     * @author wzy
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 共同借款人列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("共同借款人列表查询")
    @PostMapping("/querybycondition")
    protected ResultDto<List<LmtCobInfo>> queryByCondition(@RequestBody QueryModel queryModel) {
        List<LmtCobInfo> list = lmtCobInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCobInfo>>(list);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增共同借款人")
    @PostMapping("/addlmtcobinfo")
    protected ResultDto<Integer> addLmtCobInfo(@RequestBody LmtCobInfo lmtCobInfo) {
        return lmtCobInfoService.addLmtCobInfo(lmtCobInfo);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("删除共同借款人与征信信息")
    @PostMapping("/deletecobandcrql")
    protected ResultDto<Integer> deleteCobAndCrql(@RequestBody LmtCobInfo lmtCobInfo) {
        int result = lmtCobInfoService.deleteCobAndCrql(lmtCobInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @创建人 WH
     * @创建时间 2021/5/18 19:51
     * @注释 列表条件查询 POST
     */
    @ApiOperation("查询对象列表，POST请求")
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtCobInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtCobInfo> list = lmtCobInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtCobInfo>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/19 10:50
     * @注释 saveandupdate
     */
    @ApiOperation("新增或保存")
    @PostMapping("/saveandupdate")
    protected ResultDto saveandupdate(@RequestBody LmtCobInfo lmtCobInfo) {
        return lmtCobInfoService.saveandupdate(lmtCobInfo);
    }
    /**
     * @创建人 WH
     * @创建时间 2021/6/26 16:19
     * @注释 因为惠享贷新增共借人需要去增加征信信息，所以现在单拉出来一个方法 不用于区分其他的
     */
    @PostMapping("/saveandupdatehxd")
    protected ResultDto saveandupdatehxd(@RequestBody LmtCobInfo lmtCobInfo) {
        return lmtCobInfoService.saveandupdatehxd(lmtCobInfo);
    }
    /**
     * @param lmtCobInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/29 14:18
     * @version 1.0.0
     * @desc  应wh要求单拉出一个删除方法
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletehxd")
    protected ResultDto deleteHxd(@RequestBody LmtCobInfo lmtCobInfo) {
        return lmtCobInfoService.deleteHxd(lmtCobInfo);
    }
}
