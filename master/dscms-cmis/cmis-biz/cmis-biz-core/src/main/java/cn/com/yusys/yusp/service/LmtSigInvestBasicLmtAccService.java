/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLmtAcc;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLmtAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtAccService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:24:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicLmtAccService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicLmtAccMapper lmtSigInvestBasicLmtAccMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAcc selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtAccMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicLmtAcc> selectAll(QueryModel model) {
        List<LmtSigInvestBasicLmtAcc> records = (List<LmtSigInvestBasicLmtAcc>) lmtSigInvestBasicLmtAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicLmtAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicLmtAcc> list = lmtSigInvestBasicLmtAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicLmtAcc record) {
        return lmtSigInvestBasicLmtAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicLmtAcc record) {
        return lmtSigInvestBasicLmtAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicLmtAcc record) {
        return lmtSigInvestBasicLmtAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicLmtAcc record) {
        return lmtSigInvestBasicLmtAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicLmtAccMapper.deleteByIds(ids);
    }



    /**
     * @方法名称: initLmtSigInvestBasicLmtAccInfo
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAcc initLmtSigInvestBasicLmtAccInfo(Object lmtSigInvestBasicLmtRst) {
        //初始化对象
        LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc = new LmtSigInvestBasicLmtAcc() ;
        //主键
        String pkValue = generatePkId();
        //拷贝数据
        BeanUtils.copyProperties(lmtSigInvestBasicLmtRst, lmtSigInvestBasicLmtAcc);
        //生成主键
        lmtSigInvestBasicLmtAcc.setPkId(pkValue);
        //状态
        lmtSigInvestBasicLmtAcc.setAccStatus(CmisBizConstants.STD_REPLY_STATUS_01);
        //最新更新日期
        lmtSigInvestBasicLmtAcc.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicLmtAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicLmtAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //底层批复流水号
        //lmtSigInvestBasicLmtAcc.setBasicReplySerno(basicReplySerno);
        return lmtSigInvestBasicLmtAcc;
    }

    /**
     * 根据批复流水号获取
     * @param replySerno
     * @return
     */
    public List<LmtSigInvestBasicLmtAcc> selectByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectAll(queryModel);
    }

    /**
     * 批复变更-生成台账信息
     * @param lmtSigInvestBasicLimitApp
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     * @param accSerno
     */
    public void insertBasicLmtAcc(LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp, String oldSerno, String replySerno, String currentOrgId, String currentUserId, String accSerno) {
        LmtSigInvestBasicLmtAcc lmtSigInvestBasicLmtAcc = new LmtSigInvestBasicLmtAcc();
        org.springframework.beans.BeanUtils.copyProperties(lmtSigInvestBasicLimitApp,lmtSigInvestBasicLmtAcc);

        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期

        lmtSigInvestBasicLmtAcc.setPkId(generatePkId());
        lmtSigInvestBasicLmtAcc.setSerno(oldSerno);
        lmtSigInvestBasicLmtAcc.setAccNo(accSerno);
        lmtSigInvestBasicLmtAcc.setReplySerno(replySerno);
        lmtSigInvestBasicLmtAcc.setCreateTime(getCurrrentDate());
        lmtSigInvestBasicLmtAcc.setInputBrId(currentOrgId);
        lmtSigInvestBasicLmtAcc.setInputId(currentUserId);
        lmtSigInvestBasicLmtAcc.setInputDate(openDay);
        lmtSigInvestBasicLmtAcc.setUpdateTime(getCurrrentDate());
        lmtSigInvestBasicLmtAcc.setUpdBrId(currentOrgId);
        lmtSigInvestBasicLmtAcc.setUpdId(currentUserId);
        lmtSigInvestBasicLmtAcc.setUpdDate(openDay);
        insert(lmtSigInvestBasicLmtAcc);
    }

    public int updateLmtSigInvestBasicAccStatus(HashMap<String, String > paramMap){
        return lmtSigInvestBasicLmtAccMapper.updateLmtSigInvestBasicAccStatus(paramMap) ;
    }


    /**
     * @方法名称: deleteByAccNo
     * @方法描述: 根据批复编号，删除数据信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByAccNo(String accNo) {
        return lmtSigInvestBasicLmtAccMapper.deleteByAccNo(accNo);
    }

    /**
     * @方法名称: selectBasicAccNo
     * @方法描述: 根据底层台账查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtAcc selectBasicAccNo(String basicAccNo) {
        return lmtSigInvestBasicLmtAccMapper.selectBasicAccNo(basicAccNo);
    }
}
