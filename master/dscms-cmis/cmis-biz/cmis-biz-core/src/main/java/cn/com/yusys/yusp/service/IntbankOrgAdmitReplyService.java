/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitApp;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAppr;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitReply;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.IntbankOrgAdmitReplyMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

//import cn.com.yusys.yusp.commons.validation.constraints.In;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitReplyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:26:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IntbankOrgAdmitReplyService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(IntbankOrgAdmitReplyService.class);

    @Autowired
    private IntbankOrgAdmitReplyMapper intbankOrgAdmitReplyMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private IntbankOrgAdmitAccService accService;

    @Autowired
    private IntbankOrgAdmitAppService intbankOrgAdmitAppService ;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IntbankOrgAdmitReply selectByPrimaryKey(String pkId) {
        return intbankOrgAdmitReplyMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IntbankOrgAdmitReply> selectAll(QueryModel model) {
        List<IntbankOrgAdmitReply> records = (List<IntbankOrgAdmitReply>) intbankOrgAdmitReplyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IntbankOrgAdmitReply> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IntbankOrgAdmitReply> list = intbankOrgAdmitReplyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IntbankOrgAdmitReply record) {
        return intbankOrgAdmitReplyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IntbankOrgAdmitReply record) {
        return intbankOrgAdmitReplyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IntbankOrgAdmitReply record) {
        return intbankOrgAdmitReplyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IntbankOrgAdmitReply record) {
        return intbankOrgAdmitReplyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return intbankOrgAdmitReplyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return intbankOrgAdmitReplyMapper.deleteByIds(ids);
    }

    /**
     * 获取当前机构最新批复信息
     * @param queryModel
     * @return
     */
    public List<IntbankOrgAdmitReply> lastReplayByApp(QueryModel queryModel) {
        PageHelper.startPage(1, 1);
        queryModel.setSort(" startdate desc ");
        List<IntbankOrgAdmitReply> records = (List<IntbankOrgAdmitReply>) intbankOrgAdmitReplyMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return records;
    }

    /**
     * 根据最新的同业机构准入申请审批表中的数据生成批复数据
     * @param object
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    public String generateAdmitReplyHandleByAppr(Object object, String currentUserId, String currentOrgId,String approveStatus) {
        //终止以前的批复（暂不）

        // 生成新的批复
        return generateAdmitReplyInfoByAdmitAppr(object, currentUserId, currentOrgId,approveStatus);
    }

    /**
     * 生成新的批复
     * @param object
     *   当审批数据（IntbankOrgAdmitAppr）不存在时使用 申请表中数据（IntbankOrgAdmitApp）
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     * @return 批复流水号
     */
    private String generateAdmitReplyInfoByAdmitAppr(Object object, String currentUserId, String currentOrgId, String approveStatus) {
        IntbankOrgAdmitAppr intbankOrgAdmitAppr = null;
        IntbankOrgAdmitApp intbankOrgAdmitApp = null;
        String cusId = "";//客户编号
        String replaySerno = null;
        try {
            //判断是否为审批表中数据
            intbankOrgAdmitAppr = (IntbankOrgAdmitAppr) object;
            cusId = intbankOrgAdmitAppr.getCusId();
            //判断是否为申请表中数据
            intbankOrgAdmitApp = intbankOrgAdmitAppService.selectBySerno(intbankOrgAdmitAppr.getSerno()) ;
            //获取批复状态 TODO  采用审批END和否决
            String replayStatus = "";
            if (CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)) {
                replayStatus = CmisBizConstants.STD_REPLY_STATUS_01;
            }
            if (CmisBizConstants.APPLY_STATE_REFUSE.equals(approveStatus)) {
                replayStatus = CmisBizConstants.STD_REPLY_STATUS_02;
            }
            IntbankOrgAdmitReply reply = new IntbankOrgAdmitReply();
            reply.setReplyStatus(replayStatus);

            //当存在审批记录时，使用审批记录生成批复
            //accService.lastAccByCusId(intbankOrgAdmitApp.getCusId());
            BeanUtils.copyProperties(intbankOrgAdmitAppr, reply);
            //审批通过日期
            reply.setStartDate(getCurrrentDateStr());
            //准入到期日=审批通过日期+期限
            reply.setEndDate(getEndate(getCurrrentDateStr(), intbankOrgAdmitAppr.getTerm()));
            reply.setInputBrId(intbankOrgAdmitAppr.getInputBrId());
            reply.setInputId(intbankOrgAdmitAppr.getInputId());
            reply.setInputDate(intbankOrgAdmitAppr.getInputDate());

            //主管客户经理，主管客户机构生成
            if(intbankOrgAdmitApp!=null){
                reply.setManagerBrId(intbankOrgAdmitApp.getManagerBrId());
                reply.setManagerId(intbankOrgAdmitApp.getManagerId());
            }
            // 根据同业机构准入审批的记录生成并组装同业机构准入批复数据
            replaySerno = generateSerno(SeqConstant.INTBANK_ADMIT_REPLY_SEQ);
            log.info("生成新的同业机构准入申请批复流水号:" + replaySerno);
            reply.setReplySerno(replaySerno);
            //初始化(新增)通用domain信息(不包含input)
            initInsertDomainPropertiesForWF(reply,currentUserId,currentOrgId);
            // 批复结论为 流程审批结果（通过 997，否决 998）
            reply.setApprResult(approveStatus);
            this.insert(reply);
            log.info("同业机构准入审批业务逻辑处理完成" + replaySerno);
        } catch (Exception e) {
            log.error("同业机构准入批复信息生成失败！==》",e.getMessage());
            throw BizException.error(null, EclEnum.INTBANK_ORG_APP_REPLY_FAILED.key,EclEnum.INTBANK_ORG_APP_REPLY_FAILED.value);
        }
        return replaySerno;
    }


    /**
     * 获取结束时间
     * @param startdatestr
     * @param term
     * @return
     */
    private String getEndate(String startdatestr,Integer term){
        String enddate = "";
        try {
            Date startdate = DateUtils.parseDate(startdatestr, "yyyy-MM-dd");
            enddate = DateUtils.addMonth(startdate, "yyyy-MM-dd", term);
        } catch (Exception e) {
            log.error("时间计算错误！",e);
        }
        return enddate;
    }

    /**
     * 根据批复流水号获取批复信息
     * @param replySerno
     * @return
     */
    public IntbankOrgAdmitReply selectByReplayNo(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSize(1);
        queryModel.setPage(1);
        List<IntbankOrgAdmitReply> intbankOrgAdmitReplies = selectByModel(queryModel);
        if (intbankOrgAdmitReplies!=null && intbankOrgAdmitReplies.size()>0){
            return intbankOrgAdmitReplies.get(0);
        }
        return null;
    }

    /**
     * @方法名称: updateRestBySerno
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestBySerno(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<IntbankOrgAdmitReply> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            IntbankOrgAdmitReply intbankOrgAdmitReply = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            intbankOrgAdmitReply.setApprResult((String)model.getCondition().get("rapprResult"));
            //获取前端传入参数restDesc-审批意见
            //intbankOrgAdmitReply.setRestDesc((String)model.getCondition().get("restDesc"));
            //更新 更新数据时间
            intbankOrgAdmitReply.setUpdateTime(getCurrrentDate());
            return intbankOrgAdmitReplyMapper.updateByPrimaryKey(intbankOrgAdmitReply);
        }
        return 0;
    }

}
