package cn.com.yusys.yusp.service.server.xdxw0065;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0065.req.Xdxw0065DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0065.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:调查报告审批结果信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Service
public class Xdxw0065Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0065Service.class);

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    /**
     * 调查报告审批结果信息查询
     * @param xdxw0065DataReqDto
     * @return
     */
    @Transactional
    public java.util.List<List> getLmtReplyInfoList(Xdxw0065DataReqDto xdxw0065DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataReqDto));
        java.util.List<List> result = lmtCrdReplyInfoMapper.getLmtReplyInfoList(xdxw0065DataReqDto);
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(result));
        return result;
    }
}
