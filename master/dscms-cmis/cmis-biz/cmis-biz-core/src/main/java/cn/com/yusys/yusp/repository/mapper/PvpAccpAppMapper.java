/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.dto.server.xdcz0014.req.Xdcz0014DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-21 09:13:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PvpAccpAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PvpAccpApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PvpAccpApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(PvpAccpApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PvpAccpApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PvpAccpApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PvpAccpApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectPvpAccpAppInfoByParams
     * @方法描述: 根据查询待发起/已处理列表数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAccpApp> selectPvpAccpAppInfoByParams(Map pvpAccpAppQueryMap);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PvpAccpApp selectBySerno(String pvpSerno);

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 根据流水号更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApprStatus(@Param("pvpSerno") String pvpSerno, @Param("approveStatus") String approveStatus);

    /**
     * @方法名称: updateAppBySerno
     * @方法描述: 逻辑删除 查询待发起、退回、生效的  电子银行承兑汇票出账申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateAppBySerno(@Param("pvpSerno") String pvpSerno);

    /**
     * @方法名称: countPvpAccpAppCountByContNo
     * @方法描述: 根据普通合同编号查询出账申请数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    int countPvpAccpAppCountByContNo(Map pvpQueryMap);


    int UpdatePvpAccpAppByserno(String serno);

    String selectPvpAccpAppByPcSerno(String serno);

    /**
     * 出账申请审批中的敞口总额
     *
     * @param pvpAccpAppMap
     * @return
     */
    BigDecimal queryApprovingSpacAmtByMap(Map pvpAccpAppMap);

    /**
     * 出账审批通过尚未生成银承台账的敞口总额
     *
     * @param pvpAccpAppMap
     * @return
     */
    BigDecimal queryApprovedSpacAmtByMap(Map pvpAccpAppMap);
    /**
     * @方法名称: selectVideoNoByPcSerno
     * @方法描述: 根据批次号查询影像流水号
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.xdcz0014.resp.List> selectVideoNoByPcSerno(Xdcz0014DataReqDto xdcz0014DataReqDto);

    /**
     * @方法名称: selectVideoNoByPcSerno
     * @方法描述: 根据客户号查询其全部补扫影像流水号
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.xdcz0014.resp.List> selectVideoNosByPcSerno(Xdcz0014DataReqDto xdcz0014DataReqDto);

    /**
     * @方法名称: selectAppAmt
     * @方法描述: 查询申请金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal selectAppAmt(String contNo);

    /**
     * @方法名称: updateAppAmt
     * @方法描述: 修改申请金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApplyAmount(Map map);

    /**
     * @方法名称: selectManInfo
     * @方法描述: 查询客户经理部分信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    PvpAccpApp selectManInfo(String contNo);

    //【查询在途状态账票面金额-sum（相应批次的保证金余额）】
    List<PvpAccpApp> selectPmAmtInfo(String contNo);

    //当日出账申请通过，未生成台账【当日已出账票面金额-sum（相应批次的保证金余额）】
    List<PvpAccpApp> seletWsctzInfo(String contNo);

    /**
     * @方法名称: toSelectAspl
     * @方法描述: 查询资产池协议 银承出账
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAccpApp> toSelectAspl(QueryModel queryModel);
}