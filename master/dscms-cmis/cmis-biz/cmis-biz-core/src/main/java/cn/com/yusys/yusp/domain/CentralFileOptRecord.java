/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileOptRecord
 * @类描述: central_file_opt_record数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 23:24:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "central_file_opt_record")
public class CentralFileOptRecord extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 档案编号 **/
	@Column(name = "FILE_NO", unique = false, nullable = true, length = 40)
	private String fileNo;
	
	/** 操作类型   **/
	@Column(name = "FILE_OPT_TYPE", unique = false, nullable = true, length = 5)
	private String fileOptType;
	
	/** 接收人 **/
	@Column(name = "OPT_USR", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName",refFieldName="optUsrName" )
	private String optUsr;
	
	/** 接收机构 **/
	@Column(name = "OPT_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="optOrgName" )
	private String optOrg;
	
	/** 操作原因 **/
	@Column(name = "OPT_REASON", unique = false, nullable = true, length = 1000)
	private String optReason;
	
	/** 操作时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "OPT_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date optTime;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param fileNo
	 */
	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}
	
    /**
     * @return fileNo
     */
	public String getFileNo() {
		return this.fileNo;
	}
	
	/**
	 * @param fileOptType
	 */
	public void setFileOptType(String fileOptType) {
		this.fileOptType = fileOptType;
	}
	
    /**
     * @return fileOptType
     */
	public String getFileOptType() {
		return this.fileOptType;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr;
	}
	
    /**
     * @return optUsr
     */
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg;
	}
	
    /**
     * @return optOrg
     */
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param optReason
	 */
	public void setOptReason(String optReason) {
		this.optReason = optReason;
	}
	
    /**
     * @return optReason
     */
	public String getOptReason() {
		return this.optReason;
	}
	
	/**
	 * @param optTime
	 */
	public void setOptTime(java.util.Date optTime) {
		this.optTime = optTime;
	}
	
    /**
     * @return optTime
     */
	public java.util.Date getOptTime() {
		return this.optTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}