package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: LmtIntbankAccAndLmtSigDto
 * @类描述: appr_str_org_info数据实体类
 * @功能描述: 
 * @创建人: 周茂伟
 * @创建时间: 2021-05-03 10:30:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtIntbankAccAndLmtSigDto implements Serializable{
	private static final long serialVersionUID = 1L;


	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 主管机构 **/
	private String managerBrId;

	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;

	public String getCusId() {
		return cusId;
	}

	public String getCusName() {
		return cusName;
	}


	public String getInputId() {
		return inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}


	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
}