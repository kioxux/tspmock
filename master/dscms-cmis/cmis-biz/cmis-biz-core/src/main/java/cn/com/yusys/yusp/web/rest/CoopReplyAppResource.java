/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.vo.CoopReplyAppAllVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopReplyApp;
import cn.com.yusys.yusp.service.CoopReplyAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopreplyapp")
public class CoopReplyAppResource {
    @Autowired
    private CoopReplyAppService coopReplyAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopReplyApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopReplyApp> list = coopReplyAppService.selectAll(queryModel);
        return new ResultDto<List<CoopReplyApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopReplyApp>> index(QueryModel queryModel) {
        List<CoopReplyApp> list = coopReplyAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopReplyApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CoopReplyApp> show(@PathVariable("serno") String serno) {
        CoopReplyApp coopReplyApp = coopReplyAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopReplyApp>(coopReplyApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopReplyApp> create(@RequestBody CoopReplyApp coopReplyApp) throws URISyntaxException {
        coopReplyAppService.insert(coopReplyApp);
        return new ResultDto<CoopReplyApp>(coopReplyApp);
    }

    /**
     * @函数名称:贷审会秘书岗提交保存批复信息
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertAll")
    protected ResultDto<CoopReplyAppAllVo> insertAll(@RequestBody CoopReplyAppAllVo coopReplyAppAllVo) throws URISyntaxException {
        coopReplyAppService.insertAll(coopReplyAppAllVo);
        return new ResultDto<CoopReplyAppAllVo>(coopReplyAppAllVo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertReplyApp")
    protected ResultDto<CoopReplyApp> insertReplyApp(@RequestBody CoopReplyApp coopReplyApp) throws URISyntaxException {
        coopReplyAppService.insertReplyApp(coopReplyApp);
        return new ResultDto<CoopReplyApp>(coopReplyApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopReplyApp coopReplyApp) throws URISyntaxException {
        int result = coopReplyAppService.update(coopReplyApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopReplyAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopReplyAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
