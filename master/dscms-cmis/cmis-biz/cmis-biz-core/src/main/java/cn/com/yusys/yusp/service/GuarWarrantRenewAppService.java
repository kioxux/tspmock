/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.GuarWarrantRenewApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantRenewAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarWarrantRenewAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-14 16:47:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarWarrantRenewAppService {
    private final Logger log = LoggerFactory.getLogger(GuarWarrantRenewAppService.class);

    @Autowired
    private GuarWarrantRenewAppMapper guarWarrantRenewAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarWarrantRenewApp selectByPrimaryKey(String serno) {
        return guarWarrantRenewAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarWarrantRenewApp> selectAll(QueryModel model) {
        List<GuarWarrantRenewApp> records = (List<GuarWarrantRenewApp>) guarWarrantRenewAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarWarrantRenewApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarWarrantRenewApp> list = guarWarrantRenewAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarWarrantRenewApp record) {
        return guarWarrantRenewAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarWarrantRenewApp record) {
        return guarWarrantRenewAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarWarrantRenewApp record) {
        return guarWarrantRenewAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarWarrantRenewApp record) {
        return guarWarrantRenewAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarWarrantRenewAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarWarrantRenewAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: vilaIsInWayRenewApp
     * @方法描述: 根据押品编号，判断该押品是否存在在途的
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int  vilaIsInWayRenewApp(Map paramMap){
        int resultCount = guarWarrantRenewAppMapper.selectGuarWarrantRenewInWay(paramMap) ;
        return resultCount ;
    }


    /**
     * @方法名称: vilaIsInWayAndInsertApp
     * @方法描述: 校验在途申请，并且插入校验通过，插入续借申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertGuarWarrantReNewApp(Map paramMap){

        //String serno =  sequenceTemplateClient.getSequenceTemplate("IQP_ACCT_SERNO", new HashMap<>());
        if(paramMap == null){
            throw new YuspException(EcbEnum.E_GUAR_PARAM_EXPCETION.key, EcbEnum.E_GUAR_PARAM_EXPCETION.value);
        }
        //传入参数转化为实体对象
        GuarWarrantRenewApp guarWarrantRenewApp = JSONObject.parseObject(JSON.toJSONString(paramMap),GuarWarrantRenewApp.class);
        //生成流水号
        String serno = (String)paramMap.get("newSerno") ;
        //重置申请流水号
        guarWarrantRenewApp.setSerno(serno) ;
        //获取当前日期
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        //更新登记日期
        guarWarrantRenewApp.setInputDate(nowDate);
        //更新最近更新日期
        guarWarrantRenewApp.setUpdDate(nowDate);
        //TODO:软需默认：权证出库类型默认为:权证借阅（诉讼）/权证借阅（非诉讼） 此处默认：权证借阅（非诉讼）
        guarWarrantRenewApp.setWarrantOutType(CmisBizConstants.STD_WARRANT_OUT_TYPE_05);
        //默认待发起状态
        guarWarrantRenewApp.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_000);
        //出库类型 默认：非结清出库
        guarWarrantRenewApp.setOutType(CmisBizConstants.STD_OUT_TYPE_02);
        return guarWarrantRenewAppMapper.insert(guarWarrantRenewApp) ;
    }

    /**
     * @方法名称: vilaIsInWayAndInsertApp
     * @方法描述: 校验在途申请，并且插入校验通过，插入续借申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map vilaIsInWayAndInsertApp(Map paramMap) {
        log.info("集团变更，解散校验是否存在在途【{}】", JSONObject.toJSON(paramMap));
        HashMap<String, String> resultMap = new HashMap();
        String rtnCode = EcbEnum.E_GUAR_SUCCESS_EXPCETION.key;
        String rtnMsg = EcbEnum.E_GUAR_SUCCESS_EXPCETION.value;
        int result;
        try {
            //参数不允许为空校验
            if (paramMap == null) {
                throw new YuspException(EcbEnum.E_GUAR_PARAM_EXPCETION.key, EcbEnum.E_GUAR_PARAM_EXPCETION.value);
            }

            result = vilaIsInWayRenewApp(paramMap);
            if (result > 0) {
                throw new YuspException(EcbEnum.E_GUAR_WARRANT_INWAY_EXPCETION.key, EcbEnum.E_GUAR_WARRANT_INWAY_EXPCETION.value);
            }

            //申请数据生成
            result = insertGuarWarrantReNewApp(paramMap);
            //申请信息生成
            if (result <= 0) {
                throw new YuspException(EcbEnum.E_GUAR_INSERT_EXPCETION.key, EcbEnum.E_GUAR_INSERT_EXPCETION.value);
            }

        } catch (YuspException e) {
            log.info("押品续借校验生成申请数据失败： ", e);
            rtnCode = e.getCode() ;
            rtnMsg = e.getMsg() ;
        } finally {
            resultMap.put("rtnCode", rtnCode) ;
            resultMap.put("rtnMsg", rtnMsg) ;
        }
        return resultMap ;
    }

}
