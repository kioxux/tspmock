/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.IqpDisAssetIncomeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpDisAssetIncome;
import cn.com.yusys.yusp.service.IqpDisAssetIncomeService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncomeResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "还款能力分析-月收入")
@RequestMapping("/api/iqpdisassetincome")
public class IqpDisAssetIncomeResource {
    private static final Logger log = LoggerFactory.getLogger(IqpDisAssetIncomeResource.class);
    @Autowired
    private IqpDisAssetIncomeService iqpDisAssetIncomeService;

	/**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpDisAssetIncome>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeService.selectAll(queryModel);
        return new ResultDto<List<IqpDisAssetIncome>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpDisAssetIncome>> index(QueryModel queryModel) {
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeService.selectByModel(queryModel);
        return new ResultDto<List<IqpDisAssetIncome>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{incomePk}")
    protected ResultDto<IqpDisAssetIncome> show(@PathVariable("incomePk") String incomePk) {
        IqpDisAssetIncome iqpDisAssetIncome = iqpDisAssetIncomeService.selectByPrimaryKey(incomePk);
        return new ResultDto<IqpDisAssetIncome>(iqpDisAssetIncome);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<Integer> create(@RequestBody IqpDisAssetIncome iqpDisAssetIncome) throws URISyntaxException {
        int result =iqpDisAssetIncomeService.insert(iqpDisAssetIncome);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/createone")
    protected ResultDto<Integer> createOne(@RequestBody IqpDisAssetIncome iqpDisAssetIncome) throws URISyntaxException {
        iqpDisAssetIncome.setDiscountAssetType("1");
        int result =iqpDisAssetIncomeService.insert(iqpDisAssetIncome);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param iqpDisAssetIncome
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/8/25 16:57
     * @version 1.0.0
     * @desc  为移动OA新增
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("移动OA专用新增")
    @PostMapping("/createoneoa")
    protected ResultDto<Integer> createOneOa(@RequestBody IqpDisAssetIncome iqpDisAssetIncome) throws URISyntaxException {
        iqpDisAssetIncome.setDiscountAssetType("1");
        int result =iqpDisAssetIncomeService.insertSelective(iqpDisAssetIncome);
        return new ResultDto<Integer>(result);
    }

   /** 
    * @author zlf
    * @date 2021/5/5 15:09
    * @version 1.0.0
    * @desc     修改数据
    * @修改历史: 修改时间    修改人员    修改原因
   */
    @ApiOperation("更新单表数据")
    @PostMapping("/update")
    protected ResultDto<IqpDisAssetIncome> update(@RequestBody IqpDisAssetIncome iqpDisAssetIncome) throws URISyntaxException {
        IqpDisAssetIncome result = iqpDisAssetIncomeService.update(iqpDisAssetIncome);
        return new ResultDto<IqpDisAssetIncome>(iqpDisAssetIncome);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{incomePk}")
    protected ResultDto<Integer> delete(@PathVariable("incomePk") String incomePk) {
        int result = iqpDisAssetIncomeService.deleteByPrimaryKey(incomePk);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteByPrimaryKey
     * @函数描述:根据主键删除资产折算-月收入表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByPrimaryKey")
    protected ResultDto<Integer> deleteByPrimaryKey(@RequestBody IqpDisAssetIncome iqpDisAssetIncome) {
        int result = iqpDisAssetIncomeService.deleteByPrimaryKey(iqpDisAssetIncome.getIncomePk());
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpDisAssetIncomeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getSumIncome
     * @函数描述:根据业务流水查询月收入汇总信息入参
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据业务流水查询月收入汇总信息入参")
    @GetMapping("getSumIncome/{iqpSerno}")
    protected ResultDto<String> getSumIncome(@PathVariable("iqpSerno") String iqpSerno){
        log.info("根据业务流水查询月收入汇总信息入参{}"+iqpSerno);
        return new ResultDto<>(iqpDisAssetIncomeService.getSumIncome(iqpSerno));
    }
    /**
     * @author zlf
     * @date 2021/5/5 11:09
     * @version 1.0.0
     * @desc 根据业务流水号查单条
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("根据业务流水号查单条")
    @PostMapping("/selectbyiqpserno/{iqpSerno}")
    protected ResultDto<IqpDisAssetIncome> selectByIqpSerno(@PathVariable("iqpSerno") String iqpSerno) {
        IqpDisAssetIncome iqpDisAssetIncome = iqpDisAssetIncomeService.selectIqpSerno(iqpSerno);
        return new ResultDto<IqpDisAssetIncome>(iqpDisAssetIncome);
    }
    /**
     * @author zlf
     * @date 2021/5/5 16:59
     * @version 1.0.0
     * @desc    根据业务流水号查经营性收入
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("根据业务流水号查经营性收入")
    @PostMapping("/selectmore")
    protected ResultDto<List<IqpDisAssetIncome>> selectMore(@RequestBody IqpDisAssetIncome iqpDisAssetIncome){
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeService.selectList(iqpDisAssetIncome.getIqpSerno());
        return new ResultDto<List<IqpDisAssetIncome>>(list);
    }
    /**
     * @author zlf
     * @date 2021/5/5 16:59
     * @version 1.0.0
     * @desc    查经营性收入 3
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("查经营性收入")
    @PostMapping("/selectmorereplaybyiqpserno")
    protected ResultDto selectMoreReplayByIqpSerno(@RequestBody IqpDisAssetIncome iqpDisAssetIncome){
        IqpDisAssetIncomeDto iqpDisAssetIncomeDtos = iqpDisAssetIncomeService.selectMore(iqpDisAssetIncome.getIqpSerno());
        return new ResultDto(iqpDisAssetIncomeDtos);
    }



    /**
     * @author zlf
     * @date 2021/5/5 16:59
     * @version 1.0.0
     * @desc    根据业务流水号查其他 2
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号查其他")
    @PostMapping("/selectmorebyiqpserno/{iqpSerno}")
    protected ResultDto selectMoreByIqpSerno(@PathVariable("iqpSerno") String iqpSerno){
        IqpDisAssetIncomeDto iqpDisAssetIncomeDtos = iqpDisAssetIncomeService.selectMoreOther(iqpSerno);
        return new ResultDto(iqpDisAssetIncomeDtos);
    }

    /**
     * @author zlf
     * @date 2021/5/5 16:59
     * @version 1.0.0
     * @desc    根据业务流水号查其他收入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据业务流水号查其他收入")
    @PostMapping("/selectmoreother")
    protected ResultDto<List<IqpDisAssetIncome>> selectMoreOther(@RequestBody IqpDisAssetIncome iqpDisAssetIncome){
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeService.selectListOther(iqpDisAssetIncome.getIqpSerno());
        return new ResultDto<List<IqpDisAssetIncome>>(list);
    }

    /**
     * @param queryModel
     * @return ResultDto
     * @author wzy
     * @date 2021/6/17 10:33
     * @version 1.0.0
     * @desc  分页查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("分页查询")
    @PostMapping("/querybycondition")
    protected ResultDto<List<IqpDisAssetIncome>> queryByCondition(@RequestBody QueryModel queryModel) {
        List<IqpDisAssetIncome> list = iqpDisAssetIncomeService.selectByModel(queryModel);
        return new ResultDto<List<IqpDisAssetIncome>>(list);
    }

    /**
     * @author shenli
     * @date 2021-6-24 20:28:12
     * @version 1.0.0
     * @desc 根据流水号查询经营与其他收入总计
     * @修改历史 修改时间 修改人员 修改原因
     */
    @ApiOperation("根据流水号查询经营与其他收入总计")
    @PostMapping("/selectSubtotalSumByAssetType")
    protected ResultDto<BigDecimal> selectSubtotalSumByAssetType(@RequestBody String  iqpSerno) {
        return new ResultDto<BigDecimal>(iqpDisAssetIncomeService.selectSubtotalSumByAssetType(iqpSerno));
    }

}
