package cn.com.yusys.yusp.web.server.xdxw0065;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0065.req.Xdxw0065DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0065.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0065.resp.Xdxw0065DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0065.Xdxw0065Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

/**
 * 接口处理类:调查报告审批结果信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0065:调查报告审批结果信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0065Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0065Resource.class);

    @Autowired
    private Xdxw0065Service xdxw0065Service;

    /**
     * 交易码：xdxw0065
     * 交易描述：调查报告审批结果信息查询
     *
     * @param xdxw0065DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查报告审批结果信息查询")
    @PostMapping("/xdxw0065")
    protected @ResponseBody
    ResultDto<Xdxw0065DataRespDto> xdxw0065(@Validated @RequestBody Xdxw0065DataReqDto xdxw0065DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataReqDto));
        Xdxw0065DataRespDto xdxw0065DataRespDto = new Xdxw0065DataRespDto();// 响应Dto:调查报告审批结果信息查询
        ResultDto<Xdxw0065DataRespDto> xdxw0065DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0065DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0065Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataReqDto));
            java.util.List<List> result = Optional.ofNullable(xdxw0065Service.getLmtReplyInfoList(xdxw0065DataReqDto)).orElseGet(() -> {
                List list = new List();
                list.setSerno(StringUtils.EMPTY);// 主键
                list.setSurvey_serno(StringUtils.EMPTY);// 调查报告主表主键
                list.setCus_id(StringUtils.EMPTY);// 客户代码
                list.setCus_name(StringUtils.EMPTY);// 客户名称
                list.setCont_type(StringUtils.EMPTY);// 合同类型
                list.setGuar_ways(StringUtils.EMPTY);// 担保方式
                list.setApply_amount(new BigDecimal(0L));// 合同金额
                list.setTerm_time_type(StringUtils.EMPTY);// 期限类型
                list.setApply_term(StringUtils.EMPTY);// 申请期限
                list.setLoan_start_date(StringUtils.EMPTY);// 合同起始日
                list.setLoan_end_date(StringUtils.EMPTY);// 合同到期日
                list.setReality_ir_y(new BigDecimal(0L));// 利率
                list.setRepay_type(StringUtils.EMPTY);// 还款方式
                list.setLimit_type(StringUtils.EMPTY);// 额度类型
                list.setInsert_time(StringUtils.EMPTY);// 插入时间
                list.setCert_code(StringUtils.EMPTY);// 证件号
                list.setCur_use_amt(new BigDecimal(0L));// 本次用信金额
                list.setIs_trustee_pay(StringUtils.EMPTY);// 是否受托支付
                list.setIs_credit_condition(StringUtils.EMPTY);// 是否有用信条件
                return Arrays.asList(list);
            });
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(result));
            xdxw0065DataRespDto.setList(result);
            // 封装xdxw0065DataResultDto中正确的返回码和返回信息
            xdxw0065DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0065DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, e.getMessage());
            // 封装xdxw0065DataResultDto中异常返回码和返回信息
            xdxw0065DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0065DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0065DataRespDto到xdxw0065DataResultDto中
        xdxw0065DataResultDto.setData(xdxw0065DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataResultDto));
        return xdxw0065DataResultDto;
    }
}
