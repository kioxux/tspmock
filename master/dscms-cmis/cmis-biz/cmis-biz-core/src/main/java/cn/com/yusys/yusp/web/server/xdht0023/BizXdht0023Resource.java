package cn.com.yusys.yusp.web.server.xdht0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0023.req.Xdht0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0023.resp.Xdht0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:信贷系统信贷合同生成接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0023:信贷系统信贷合同生成接口")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0023Resource.class);

    /**
     * 交易码：xdht0023
     * 交易描述：信贷系统信贷合同生成接口
     *
     * @param xdht0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷系统信贷合同生成接口")
    @PostMapping("/xdht0023")
    protected @ResponseBody
    ResultDto<Xdht0023DataRespDto> xdht0023(@Validated @RequestBody Xdht0023DataReqDto xdht0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023DataReqDto));
        Xdht0023DataRespDto xdht0023DataRespDto = new Xdht0023DataRespDto();// 响应Dto:信贷系统信贷合同生成接口
        ResultDto<Xdht0023DataRespDto> xdht0023DataResultDto = new ResultDto<>();
        try {
            // 从xdht0023DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdht0023DataRespDto对象开始
            // TODO 封装xdht0023DataRespDto对象结束
            // 封装xdht0023DataResultDto中正确的返回码和返回信息
            xdht0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, e.getMessage());
            // 封装xdht0023DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0023DataRespDto到xdht0023DataResultDto中
        xdht0023DataResultDto.setData(xdht0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023DataRespDto));
        return xdht0023DataResultDto;
    }
}
