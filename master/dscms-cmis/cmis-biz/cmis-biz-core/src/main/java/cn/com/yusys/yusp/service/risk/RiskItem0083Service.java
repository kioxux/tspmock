package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.ToppAcctSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0078Service
 * @类描述: 受托账号校验
 * @功能描述: 受托账号校验
 * @创建人: hubp
 * @创建时间: 2021年8月11日08:38:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0083Service {

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private ToppAcctSubService toppAcctSubService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    /**
     * @方法名称: riskItem0083
     * @方法描述: 受托账号校验
     * @参数与返回说明:
     * @算法描述:  1,若支付方式为受托支付，交易对手账号与发放贷款账号不能重复
     *            2,受托金额必须等于出账金额
     * @创建人: hubp
     * @创建时间: 2021年8月11日08:38:26
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0083(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        if("LS004".equals(bizType) || "LS008".equals(bizType)){
            // 零售合同申请进入，使用合同编号查
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(pvpSerno);
            if (Objects.isNull(ctrLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                return riskResultDto;
            }
            if (!StringUtils.isBlank(ctrLoanCont.getPayMode()) && "1".equals(ctrLoanCont.getPayMode()) && !"022028".equals(ctrLoanCont.getPrdId())) {
                // 如果支付方式为受托支付，则开始去查找交易对手账号
                QueryModel model = new QueryModel();
                model.addCondition("serno", ctrLoanCont.getIqpSerno());
                List<ToppAcctSub> list = toppAcctSubService.selectBySerno(model);
                if (list.size() == 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08301);
                    return riskResultDto;
                } else {
                    // 受托账号金额之和不等于放款金额，则拦截
                    // 受托账号存在相同账号，则拦截
                    BigDecimal toppAmt = BigDecimal.ZERO;
                    HashSet<String> set = new HashSet();
                    for(ToppAcctSub toppAcctSub:list){
                        if (toppAcctSub.getToppAcctNo().equals(ctrLoanCont.getLoanPayoutAccno())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08302);
                            return riskResultDto;
                        }
                        set.add(toppAcctSub.getToppAcctNo());
                        toppAmt = toppAmt.add(toppAcctSub.getToppAmt());

                    }
                    if(toppAmt.compareTo(ctrLoanCont.getContAmt()) != 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08305);
                        return riskResultDto;
                    }
                    if(list.size() !=set.size() ){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08304);
                        return riskResultDto;
                    }
                }
            }
        }else if ("LS005".equals(bizType) || "LS006".equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            // 零售放款申请进入,使用放款编号查
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            if (!StringUtils.isBlank(pvpLoanApp.getIsBeEntrustedPay()) && CmisCommonConstants.YES_NO_1.equals(pvpLoanApp.getIsBeEntrustedPay())) {
                // 如果支付方式为受托支付，则开始去查找交易对手账号
                QueryModel model = new QueryModel();
                model.addCondition("serno", pvpSerno);
                List<ToppAcctSub> list = toppAcctSubService.selectBySerno(model);
                if (list.size() == 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08301);
                    return riskResultDto;
                } else {
                    // 受托账号金额之和不等于放款金额，则拦截
                    // 受托账号存在相同账号，则拦截
                    BigDecimal toppAmt = BigDecimal.ZERO;
                    HashSet<String> set = new HashSet();
                    for(ToppAcctSub toppAcctSub:list){
                        if (toppAcctSub.getToppAcctNo().equals(pvpLoanApp.getLoanPayoutAccno())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08302);
                            return riskResultDto;
                        }
                        set.add(toppAcctSub.getToppAcctNo());
                        toppAmt = toppAmt.add(toppAcctSub.getToppAmt());

                    }
                    if(toppAmt.compareTo(pvpLoanApp.getPvpAmt()) != 0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08303);
                        return riskResultDto;
                    }
                    if(list.size() !=set.size() ){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08304);
                        return riskResultDto;
                    }
                }
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
