package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import java.util.Date;

@ExcelCsv(namePrefix = "征信授权书台账信息", fileType = ExcelCsv.ExportFileType.XLS)
public class CreditAuthbookInfoVo {
    /** 授权业务编号 **/
    @ExcelField(title = "授权业务编号", viewLength = 20)
    private String caiSerno;

    /** 证件类型 **/
    @ExcelField(title = "证件类型", dictCode = "STD_ZB_CERT_TYP", viewLength = 15)
    private String certType;

    /** 证件号码 **/
    @ExcelField(title = "证件号码", viewLength = 20)
    private String certCode;

    /** 客户名称 **/
    @ExcelField(title = "客户名称", viewLength = 15)
    private String cusName;

    /** 征信查询类别 **/
    @ExcelField(title = "征信查询类别", dictCode = "STD_QRY_CLS", viewLength = 20)
    private String qryCls;

    /** 授权书编号 **/
    @ExcelField(title = "授权书编号", viewLength = 20)
    private String authbookNo;

    /** 授权书日期 **/
    @ExcelField(title = "授权书日期", viewLength = 20)
    private String authbookDate;

    /** 授权书内容 **/
    @ExcelField(title = "授权书内容", dictCode = "STD_AUTHBOOK_CONTENT", viewLength = 40)
    private String authbookContent;

    /** 其他授权书内容 **/
    @ExcelField(title = "其他授权书内容", viewLength = 40)
    private String otherAuthbookContent;

    /** 授权方式 **/
    @ExcelField(title = "授权方式", dictCode = "STD_AUTH_MODE", viewLength = 15)
    private String authMode;

    /** 影像编号 **/
    @ExcelField(title = "影像编号", viewLength = 20)
    private String imageNo;

    /** 创建时间 **/
    @ExcelField(title = "创建时间", viewLength = 20)
    private java.util.Date createTime;

    /** 修改时间 **/
    @ExcelField(title = "修改时间", viewLength = 20)
    private java.util.Date updateTime;

    public String getCaiSerno() {
        return caiSerno;
    }

    public void setCaiSerno(String caiSerno) {
        this.caiSerno = caiSerno;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getQryCls() {
        return qryCls;
    }

    public void setQryCls(String qryCls) {
        this.qryCls = qryCls;
    }

    public String getAuthbookNo() {
        return authbookNo;
    }

    public void setAuthbookNo(String authbookNo) {
        this.authbookNo = authbookNo;
    }

    public String getAuthbookDate() {
        return authbookDate;
    }

    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    public String getAuthbookContent() {
        return authbookContent;
    }

    public void setAuthbookContent(String authbookContent) {
        this.authbookContent = authbookContent;
    }

    public String getOtherAuthbookContent() {
        return otherAuthbookContent;
    }

    public void setOtherAuthbookContent(String otherAuthbookContent) {
        this.otherAuthbookContent = otherAuthbookContent;
    }

    public String getAuthMode() {
        return authMode;
    }

    public void setAuthMode(String authMode) {
        this.authMode = authMode;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
