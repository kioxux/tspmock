package cn.com.yusys.yusp.service.server.xdht0027;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0027.req.Xdht0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0027.resp.Xdht0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0027Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-25 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0027Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0027Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 根据客户调查表编号取得贷款合同主表的合同状态
     *
     * @param xdht0027DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0027DataRespDto queryContStatusByCertCode(Xdht0027DataReqDto xdht0027DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value);
        Xdht0027DataRespDto xdht0027DataRespDto = new Xdht0027DataRespDto();

        try {
            //请求字段
            String certCode = xdht0027DataReqDto.getCert_code();
            //返回结果
            String contStatus = StringUtils.EMPTY;
            BigDecimal realityIrY = BigDecimal.ZERO;
            //根据客户证件号关联查询合同状态
            HashMap<String, Object> map = ctrLoanContMapper.selectContStatuByCode(certCode);
            if (map != null) {
                contStatus = (String) map.get("contstatus");
                realityIrY = (BigDecimal) map.get("realityiry");
            }

            if (StringUtil.isNotEmpty(contStatus)) {//查询结果不为空
                xdht0027DataRespDto.setContStatus(contStatus);
                xdht0027DataRespDto.setRealityIrY(realityIrY);
            } else {
                xdht0027DataRespDto.setContStatus(StringUtils.EMPTY);
                xdht0027DataRespDto.setRealityIrY(null);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value);
        return xdht0027DataRespDto;
    }
}
