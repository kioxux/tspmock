/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyReportBasicInfo
 * @类描述: survey_report_basic_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-12 13:55:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "survey_report_basic_info")
public class SurveyReportBasicInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_NO")
	private String surveyNo;
	
	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 证件号码 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 40)
	private String certNo;
	
	/** 电话号码 **/
	@Column(name = "PHONE_NO", unique = false, nullable = true, length = 11)
	private String phoneNo;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
		private String marStatus;
	
	/** 配偶姓名 **/
	@Column(name = "SPOUSE_NAME", unique = false, nullable = true, length = 35)
	private String spouseName;
	
	/** 配偶证件号码 **/
	@Column(name = "SPOUSE_CERT_NO", unique = false, nullable = true, length = 40)
	private String spouseCertNo;
	
	/** 配偶电话 **/
	@Column(name = "SPOUSE_PHONE", unique = false, nullable = true, length = 30)
	private String spousePhone;
	
	/** 有无子女 **/
	@Column(name = "HAVE_CHILDREN", unique = false, nullable = true, length = 2)
	private String haveChildren;
	
	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = true, length = 10)
	private String edu;
	
	/** 居住年限 **/
	@Column(name = "RESI_YEARS", unique = false, nullable = true, length = 20)
	private String resiYears;
	
	/** 居住地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 500)
	private String resiAddr;
	
	/** 是否线上抵押 **/
	@Column(name = "IS_ONLINE_PLD", unique = false, nullable = true, length = 1)
	private String isOnlinePld;
	
	/** 担保方式 **/
	@Column(name = "GRT_MODE", unique = false, nullable = true, length = 5)
	private String grtMode;
	
	/** 模型建议金额 **/
	@Column(name = "MODEL_ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceAmt;
	
	/** 模型建议利率 **/
	@Column(name = "MODEL_ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceRate;
	
	/** 参考利率 **/
	@Column(name = "REF_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal refRate;
	
	/** 建议金额 **/
	@Column(name = "ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	@Column(name = "ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceRate;
	
	/** 建议期限 **/
	@Column(name = "ADVICE_TERM", unique = false, nullable = true, length = 20)
	private String adviceTerm;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_USE", unique = false, nullable = true, length = 5)
	private String loanUse;
	
	/** 是否新员工 **/
	@Column(name = "IS_NEW_EMPLOYEE", unique = false, nullable = true, length = 1)
	private String isNewEmployee;
	
	/** 新员工名称 **/
	@Column(name = "NEW_EMPLOYEE_NAME", unique = false, nullable = true, length = 80)
	private String newEmployeeName;
	
	/** 新员工电话 **/
	@Column(name = "NEW_EMPLOYEE_PHONE", unique = false, nullable = true, length = 11)
	private String newEmployeePhone;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 工作单位 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 200)
	private String workUnit;
	
	/** 配偶客户编号 **/
	@Column(name = "SPOUSE_CUS_ID", unique = false, nullable = true, length = 20)
	private String spouseCusId;
	
	/** 营销人工号 **/
	@Column(name = "MARKETING_ID", unique = false, nullable = true, length = 20)
	private String marketingId;
	
	/** CERT_TYPE **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 20)
	private String certType;
	
	/** 是否农户 **/
	@Column(name = "IS_AGRI", unique = false, nullable = true, length = 1)
	private String isAgri;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	
    /**
     * @return surveyNo
     */
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param phoneNo
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
    /**
     * @return phoneNo
     */
	public String getPhoneNo() {
		return this.phoneNo;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	
    /**
     * @return spouseName
     */
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseCertNo
	 */
	public void setSpouseCertNo(String spouseCertNo) {
		this.spouseCertNo = spouseCertNo;
	}
	
    /**
     * @return spouseCertNo
     */
	public String getSpouseCertNo() {
		return this.spouseCertNo;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone;
	}
	
    /**
     * @return spousePhone
     */
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param haveChildren
	 */
	public void setHaveChildren(String haveChildren) {
		this.haveChildren = haveChildren;
	}
	
    /**
     * @return haveChildren
     */
	public String getHaveChildren() {
		return this.haveChildren;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}
	
    /**
     * @return edu
     */
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param resiYears
	 */
	public void setResiYears(String resiYears) {
		this.resiYears = resiYears;
	}
	
    /**
     * @return resiYears
     */
	public String getResiYears() {
		return this.resiYears;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	
    /**
     * @return resiAddr
     */
	public String getResiAddr() {
		return this.resiAddr;
	}
	
	/**
	 * @param isOnlinePld
	 */
	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld;
	}
	
    /**
     * @return isOnlinePld
     */
	public String getIsOnlinePld() {
		return this.isOnlinePld;
	}
	
	/**
	 * @param grtMode
	 */
	public void setGrtMode(String grtMode) {
		this.grtMode = grtMode;
	}
	
    /**
     * @return grtMode
     */
	public String getGrtMode() {
		return this.grtMode;
	}
	
	/**
	 * @param modelAdviceAmt
	 */
	public void setModelAdviceAmt(java.math.BigDecimal modelAdviceAmt) {
		this.modelAdviceAmt = modelAdviceAmt;
	}
	
    /**
     * @return modelAdviceAmt
     */
	public java.math.BigDecimal getModelAdviceAmt() {
		return this.modelAdviceAmt;
	}
	
	/**
	 * @param modelAdviceRate
	 */
	public void setModelAdviceRate(java.math.BigDecimal modelAdviceRate) {
		this.modelAdviceRate = modelAdviceRate;
	}
	
    /**
     * @return modelAdviceRate
     */
	public java.math.BigDecimal getModelAdviceRate() {
		return this.modelAdviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return refRate
     */
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return adviceAmt
     */
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return adviceRate
     */
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm;
	}
	
    /**
     * @return adviceTerm
     */
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param loanUse
	 */
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}
	
    /**
     * @return loanUse
     */
	public String getLoanUse() {
		return this.loanUse;
	}
	
	/**
	 * @param isNewEmployee
	 */
	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee;
	}
	
    /**
     * @return isNewEmployee
     */
	public String getIsNewEmployee() {
		return this.isNewEmployee;
	}
	
	/**
	 * @param newEmployeeName
	 */
	public void setNewEmployeeName(String newEmployeeName) {
		this.newEmployeeName = newEmployeeName;
	}
	
    /**
     * @return newEmployeeName
     */
	public String getNewEmployeeName() {
		return this.newEmployeeName;
	}
	
	/**
	 * @param newEmployeePhone
	 */
	public void setNewEmployeePhone(String newEmployeePhone) {
		this.newEmployeePhone = newEmployeePhone;
	}
	
    /**
     * @return newEmployeePhone
     */
	public String getNewEmployeePhone() {
		return this.newEmployeePhone;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId;
	}
	
    /**
     * @return spouseCusId
     */
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param marketingId
	 */
	public void setMarketingId(String marketingId) {
		this.marketingId = marketingId;
	}
	
    /**
     * @return marketingId
     */
	public String getMarketingId() {
		return this.marketingId;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param isAgri
	 */
	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri;
	}
	
    /**
     * @return isAgri
     */
	public String getIsAgri() {
		return this.isAgri;
	}


}