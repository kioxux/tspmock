/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydAccountFlow
 * @类描述: tmp_wyd_account_flow数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_account_flow_tmp")
public class TmpWydAccountFlowTmp {
	
	/** 交易流水号 **/
	@Id
	@Column(name = "REF_NO")
	private String refNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 合作机构号 **/
	@Column(name = "OPERORG", unique = false, nullable = true, length = 10)
	private String operorg;
	
	/** 交易类型 **/
	@Column(name = "TRANSTYPE", unique = false, nullable = true, length = 10)
	private String transtype;
	
	/** 放还本金额 **/
	@Column(name = "P_PAY", unique = false, nullable = true, length = 16)
	private String pPay;

	/** 还利息金额 **/
	@Column(name = "I_PAY", unique = false, nullable = true, length = 16)
	private String iPay;
	
	/** 还罚息 **/
	@Column(name = "PP_PAY", unique = false, nullable = true, length = 16)
	private String ppPay;
	
	/** 还费用 **/
	@Column(name = "FEE_REPAY", unique = false, nullable = true, length = 16)
	private String feeRepay;
	
	/** 交易日期 **/
	@Column(name = "TRANS_DATE", unique = false, nullable = true, length = 8)
	private String transDate;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 255)
	private String remark;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param operorg
	 */
	public void setOperorg(String operorg) {
		this.operorg = operorg;
	}
	
    /**
     * @return operorg
     */
	public String getOperorg() {
		return this.operorg;
	}
	
	/**
	 * @param refNo
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
    /**
     * @return refNo
     */
	public String getRefNo() {
		return this.refNo;
	}
	
	/**
	 * @param transtype
	 */
	public void setTranstype(String transtype) {
		this.transtype = transtype;
	}
	
    /**
     * @return transtype
     */
	public String getTranstype() {
		return this.transtype;
	}
	
	/**
	 * @param pPay
	 */
	public void setPPay(String pPay) {
		this.pPay = pPay;
	}
	
    /**
     * @return pPay
     */
	public String getPPay() {
		return this.pPay;
	}
	
	/**
	 * @param iPay
	 */
	public void setIPay(String iPay) {
		this.iPay = iPay;
	}
	
    /**
     * @return iPay
     */
	public String getIPay() {
		return this.iPay;
	}
	
	/**
	 * @param ppPay
	 */
	public void setPpPay(String ppPay) {
		this.ppPay = ppPay;
	}
	
    /**
     * @return ppPay
     */
	public String getPpPay() {
		return this.ppPay;
	}
	
	/**
	 * @param feeRepay
	 */
	public void setFeeRepay(String feeRepay) {
		this.feeRepay = feeRepay;
	}
	
    /**
     * @return feeRepay
     */
	public String getFeeRepay() {
		return this.feeRepay;
	}
	
	/**
	 * @param transDate
	 */
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	
    /**
     * @return transDate
     */
	public String getTransDate() {
		return this.transDate;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}


}