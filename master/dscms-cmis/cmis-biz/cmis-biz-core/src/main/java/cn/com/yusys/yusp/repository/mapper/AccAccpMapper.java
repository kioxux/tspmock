/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.AccAccpDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:30:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccAccpMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccAccp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccAccp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccAccp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccAccp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccAccp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccAccp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCountNumByDrftNo
     * @方法描述: 根据票号查询是否存在该笔票据台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectCountNumByDrftNo(Map QueryMap);

    /**
     * @方法名称: updateCountNumByDrftNo
     * @方法描述: 更新银承台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateCountNumByDrftNo(Map QueryMap);

    /**
     * @方法名称：selectForAccAccpInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:16
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccAccp> selectForAccAccpInfo(@Param("cusId") String cusId);


    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:55
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccAccp> selectByContNo(@Param("contNo") String contNo);

    /**
     * @param contNo
     * @return countAccAccpCountByContNo
     * @desc 根据普通合同编号查询台账数量
     * @修改历史:
     */
    int countAccAccpCountByContNo(@Param("contNo") String contNo);
    /**
     * 汇总有效银承台账敞口金额
     * @param accAccpMap
     * @return
     */
    BigDecimal querySpacAmtByAccAccpMap(Map accAccpMap);

    int selectAccAccpByBullNo(String serno);

    int updateAccAccpByserno(String serno);

    List<AccAccp> getDqAccAccpByCondition(Map map);

    BigDecimal getTotalAmount(Map map);

    BigDecimal getTotalSecurityMoneyAmount(Map map);

    int updateAccAccpBysernoAndOpenday(Map map);

    int updateAccAccpBysernoAndOpenday(String billNo);

    int updateAccAccpByParms(String billNo, BigDecimal addSecurityMoneyAmt, BigDecimal riskOpenMoneyAmt, BigDecimal newSecurityRate, BigDecimal acc_status, BigDecimal riskOpenRate);

    int updateAccAccpByParms2(String billNo, BigDecimal addSecurityMoneyAmt, BigDecimal riskOpenMoneyAmt, BigDecimal newSecurityRate, BigDecimal riskOpenRate);

    List<AccAccp> selectAccAccpByContNo(String hxcontno);

    int updateAccAccpByhxcontno(String hxcontno);

    int updateAccAccpSucceed(String hxcontno, String openday);
    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId")String cusId);

    /**
     * 未用退回
     *
     * @param sBillNo
     * @return
     */
    String getWythInfo(String sBillNo);

    /**
     * 未用退回更新
     *
     * @param sBillNo
     * @return
     */
    String updateWythInfo(String sBillNo);

    /**
     * 保证金金额查询
     *
     * @param sBillNo
     * @return
     */
    List<AccAccp> getBzjjeInfo(String sBillNo);

    /**
     * 保证金金额更新
     *
     * @param map
     * @return
     */
    int updateBzjjeInfo(Map map);

    /**
     * 保证金金额查询
     *
     * @param cont_no
     * @return
     */
    List<AccAccp> getpvpAmtInfo(String cont_no);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据根据核心银承编号回显银承台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccAccpDto queryByCoreBillNo(AccAccp accAccp);

    /**
     * 根据查询条件查询台账信息并返回
     * @param model
     * @return
     */
    List<AccAccp> querymodelByCondition(QueryModel model);

    /**
     * 根据查询条件查询台账信息并返回
     * @param tContNo
     * @return
     */
    BigDecimal selectSumLoanByTContNo(@Param("tContNo")String tContNo);

    /**
     * 根据出账流水号查询台账信息
     * @param pvpSerno
     * @return
     */
    AccAccp selectByPvpSerno(@Param("pvpSerno")String pvpSerno);

    /**
     * 根据合同编号查询银票总和
     * @param contNo
     * @return
     */
    BigDecimal selectSumAmtByContNo(@Param("contNo")String contNo);

    /**
     * @方法名称：selectInfoByCusIdDate
     * @方法描述：根据客户号灵活查询
     * @创建人：xs
     * @创建时间：2021/6/7 16:39
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model);

    /**
     * 根据合同编号查询用信敞口余额
     * @param contNos
     * @return
     */
    BigDecimal selectTotalSpacAmtByContNos(@Param("contNos") String contNos);

    /**
     * 风险分类审批结束更新客户未结清银承台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:28
     **/
    int updateLoanFiveAndTenClassByCusId(Map<String, String> map);

    int updateAccStatusByPvpSerno(String serno);

    AccAccp queryDomainByCoreBillNo(String seqNo);


    AccAccp selectByCoreBillNo(String pcSerno);
}