package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "票据明细台账导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccAccpDrftSubVo {

    /*
  合同编号
   */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
   银承核心编号
    */
    @ExcelField(title = "银承核心编号", viewLength = 20)
    private String coreBillNo;

    /*
   借据编号
    */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
   票据号码
    */
    @ExcelField(title = "票据号码", viewLength = 20)
    private String porderNo;

    /*
   票面金额
    */
    @ExcelField(title = "票面金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal draftAmt;

    /*
    币种
    */
    @ExcelField(title = "币种", viewLength = 20, dictCode = "STD_ZB_CUR_TYP")
    private String curType;

    /*
   出票日期
    */
    @ExcelField(title = "出票日期", viewLength = 20)
    private String isseDate;

    /*
   到期日期
    */
    @ExcelField(title = "到期日期", viewLength = 20)
    private String endDate;

    /*
    保证金金额
    */
    @ExcelField(title = "保证金金额", viewLength = 20, format = "#0.00")
    private java.math.BigDecimal bailAmt;

    /*
    收款人名称
    */
    @ExcelField(title = "收款人名称", viewLength = 40 )
    private String pyeeName;

    /*
    收款人账号
    */
    @ExcelField(title = "收款人账号", viewLength = 20 )
    private String pyeeAccno;

    /*
    承兑行行号
    */
    @ExcelField(title = "承兑行行号", viewLength = 20)
    private String aorgNo;

    /*
    承兑行名称
    */
    @ExcelField(title = "承兑行名称", viewLength = 20 )
    private String aorgName;

    /*
    台账状态
    */
    @ExcelField(title = "台账状态", viewLength = 20 ,dictCode = "STD_ACC_ACCP_STATUS")
    private String accStatus;

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCoreBillNo() {
        return coreBillNo;
    }

    public void setCoreBillNo(String coreBillNo) {
        this.coreBillNo = coreBillNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public BigDecimal getDraftAmt() {
        return draftAmt;
    }

    public void setDraftAmt(BigDecimal draftAmt) {
        this.draftAmt = draftAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAccno() {
        return pyeeAccno;
    }

    public void setPyeeAccno(String pyeeAccno) {
        this.pyeeAccno = pyeeAccno;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
