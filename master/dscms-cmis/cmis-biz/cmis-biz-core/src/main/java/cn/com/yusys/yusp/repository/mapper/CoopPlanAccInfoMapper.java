/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.domain.CoopProAccInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAccInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 16:26:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Component
public interface CoopPlanAccInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CoopPlanAccInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CoopPlanAccInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CoopPlanAccInfo> selectByModel4Widget(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CoopPlanAccInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CoopPlanAccInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CoopPlanAccInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CoopPlanAccInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByCoopPlanNO
     * @方法描述: 根据合作方申请流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CoopPlanAccInfo selectCoopPlanAccInfoBySerno(@Param("serno") String serno);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: queryListByPartnerNo
     * @方法描述: 根据合作方编号查询特色关联对公授信
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> queryListByPartnerNo(QueryModel queryModel);

    /**
     * @方法名称: queryLmtAppSubGuar
     * @方法描述: 根据合作方编号查询一般担保关联对公授信
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> queryLmtAppSubGuar(QueryModel queryModel);

    /**
     * @方法名称: byProNo
     * @方法描述: 根据分项编号查询关联零售授信
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> byProNo(QueryModel queryModel);

    /**
     * @方法名称: getSumByLmtAmt
     * @方法描述: 根据合作方编号计算特色关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getSumByLmtAmt(QueryModel queryModel);

    /**
     * @方法名称: getSumByLmtAmtGuar
     * @方法描述: 根据合作方编号计算一般担保关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getSumByLmtAmtGuar(QueryModel queryModel);

    /**
     * @方法名称: getSumByAppAmt
     * @方法描述: 计算申请额度之和
     * @参数与返回说明:
     * @算法描述: 无
     */
    String getSumByAppAmt(QueryModel queryModel);

    /**
     * @param coopPlanNo
     * @return cn.com.yusys.yusp.domain.CoopProAccInfo
     * @author hubp
     * @date 2021/9/30 16:45
     * @version 1.0.0
     * @desc    根据合作方案编号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    CoopPlanAccInfo selectByCoopPlanNo(@Param("coopPlanNo") String coopPlanNo);

    /**
     * 根据合作方编号查询
     * @param partnerNo
     * @return
     */
    CoopPlanAccInfo selectByPartnerNo(String partnerNo);
}