package cn.com.yusys.yusp.constant;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/7/723:52
 * @desc 零售产品属性配置
 * @修改历史: 修改时间    修改人员    修改原因
 */
public enum RetailPrdAttributionEnums {
    // 个人一手住房按揭贷款--异地---空白
    PRD_ID_022001_01_00("022001", "01", "00"),
    // 个人一手住房按揭贷款--异地---打印
    PRD_ID_022001_01_01("022001", "01", "01"),
    // 个人一手住房按揭贷款--本地---打印
    PRD_ID_022001_02_01("022001", "02", "01"),
    // 个人二手住房按揭贷款--异地---空白
    PRD_ID_022002_01_00("022002", "01", "00"),
    // 个人二手住房按揭贷款--异地---打印
    PRD_ID_022002_01_01("022002", "01", "01"),
    // 个人一手商用房按揭贷款--异地---空白
    PRD_ID_022020_01_00("022020", "01", "00"),
    // 个人一手商用房按揭贷款--异地---打印
    PRD_ID_022020_01_01("022020", "01", "01"),
    // 个人一手商用房按揭贷款--本地---打印
    PRD_ID_022020_02_01("022020", "02", "01"),
    // 个人二手商用房按揭贷款--异地---空白
    PRD_ID_022021_01_00("022021", "01", "00"),
    // 个人二手商用房按揭贷款--异地---打印
    PRD_ID_022021_01_01("022021", "01", "01"),
    // 接力贷--异地---空白
    PRD_ID_022024_01_00("022024", "01", "00"),
    // 个接力贷--异地---打印
    PRD_ID_022024_01_01("022024", "01", "01"),
    // 接力贷--本地---空白
    PRD_ID_022024_02_00("022024", "02", "00"),
    // 接力贷--本地---打印
    PRD_ID_022024_02_01("022024", "02", "01"),
    // 拍卖贷--异地---空白
    PRD_ID_022031_01_00("022031", "01", "00"),
    // 拍卖贷--异地---打印
    PRD_ID_022031_01_01("022031", "01", "01"),
    // 拍卖贷--本地---空白
    PRD_ID_022031_02_00("022031", "02", "00"),
    // 拍卖贷--本地---打印
    PRD_ID_022031_02_01("022031", "02", "01"),
    // 个人二手住房按揭贷款（资金托管）--本地---空白
    PRD_ID_022040_02_00("022040", "02", "00"),
    // 个人二手商用房按揭贷款（资金托管）--本地---空白
    PRD_ID_022051_02_00("022051", "02", "00"),
    // 个人一手住房按揭贷款（常熟资金监管）--异地---打印
    PRD_ID_022052_01_01("022052", "01", "01"),
    // 个人二手住房按揭贷款（连云港资金托管）--异地---打印
    PRD_ID_022053_01_01("022053", "01", "01"),
    // 个人二手商用房按揭贷款（连云港资金托管）--异地---打印
    PRD_ID_022054_01_01("022054", "01", "01"),
    // 个人一手住房按揭贷款（宿迁资金监管）--异地---打印
    PRD_ID_022055_01_01("022055", "01", "01"),
    // 个人一手住房按揭贷款（宿迁资金监管）--异地---空白
    PRD_ID_022055_01_00("022055", "01", "00"),
    // 个人一手商用房按揭贷款（宿迁资金监管）--异地---打印
    PRD_ID_022056_01_01("022056", "01", "01"),
    // 个人一手商用房按揭贷款（宿迁资金监管）--异地---空白
    PRD_ID_022056_01_00("022056", "01", "00"),
    // 个人综合消费贷款--本地---打印
    PRD_ID_022005_02_01("022005", "02", "01"),
    // 个人综合消费贷款--异地---打印
    PRD_ID_022005_01_01("022005", "01", "01"),
    // 个人汽车消费贷款--异地---打印
    PRD_ID_022019_01_01("022019", "01", "01"),
    // 个人汽车消费贷款--本地---打印
    PRD_ID_022019_02_01("022019", "02", "01"),
    // 白领易贷通--本地---打印
    PRD_ID_022028_02_01("022028", "02", "01"),
    // 白领易贷通--异地---打印
    PRD_ID_022028_01_01("022028", "01", "01"),
    // 装修贷--异地---打印
    PRD_ID_022006_01_01("022006", "01", "01"),
    // 装修贷--本地---打印
    PRD_ID_022006_02_01("022006", "02", "01"),
    // 理财通--本地---打印
    PRD_ID_022017_02_01("022017", "02", "01"),
    // 理财通--异地---打印
    PRD_ID_022017_01_01("022017", "01", "01"),
    // 迁居贷--异地---打印
    PRD_ID_022023_01_01("022023", "01", "01"),
    // 迁居贷--本地---打印
    PRD_ID_022023_02_01("022023", "02", "01"),
    // 美丽家园贷--本地---打印
    PRD_ID_022090_02_01("022090", "02", "01"),
    // 美丽家园贷--异地---打印
    PRD_ID_022090_01_01("022090", "01", "01"),
    // 个人二手住房按揭贷款--本地---空白
    PRD_ID_022002_02_00("022002", "02", "00"),
    // 个人二手住房按揭贷款--本地---打印
    PRD_ID_022002_02_01("022002", "02", "01"),
    // 个人二手商用房按揭贷款--本地---空白
    PRD_ID_022021_02_00("022021", "02", "00"),
    // 个人二手商用房按揭贷款--本地---打印
    PRD_ID_022021_02_01("022021", "02", "01");



    // 产品代码
    private String prdId;
    // 机构类型 01--异地 02--本地
    private String orgType;
    // 合同模式 00--空白合同模式 01--生成打印模式 02--线上签订模式
    private String contMode;

    public static Map<String, String> keyValue;

    private RetailPrdAttributionEnums(String prdId, String orgType, String contMode) {
        this.prdId = prdId;
        this.orgType = orgType;
        this.contMode = contMode;
    };

    // 初始化数据 key:枚举名，value:prdId
    static {
        keyValue = new TreeMap<>();
        for (RetailPrdAttributionEnums enumData : EnumSet.allOf(RetailPrdAttributionEnums.class)) {
            keyValue.put(enumData.name(), enumData.prdId);
        }
    }

    public static String lookup(String key) {
        if (keyValue.containsKey(key)) {
            return (String) keyValue.get(key);
        } else {
            return "";
        }
    }
}
