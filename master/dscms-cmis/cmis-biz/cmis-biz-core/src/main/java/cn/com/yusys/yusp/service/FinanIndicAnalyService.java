/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.dto.FncConfTemplateDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.req.QuotaReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.QuotaRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.node.BigIntegerNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.FinanIndicAnalyMapper;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.Data;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FinanIndicAnalyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 16:12:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FinanIndicAnalyService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(FinanIndicAnalyService.class);

    @Resource
    private FinanIndicAnalyMapper finanIndicAnalyMapper;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private RptOperEnergySalesService rptOperEnergySalesService;

    @Autowired
    private RptFncSituBsService rptFncSituBsService;

    @Autowired
    private RptFncSituService rptFncSituService;

    @Autowired
    private RptOperEnergyConsService rptOperEnergyConsService;

    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private Dscms2SjztClientService dscms2SjztClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public FinanIndicAnaly selectByPrimaryKey(String pkId) {
        return finanIndicAnalyMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<FinanIndicAnaly> selectAll(QueryModel model) {
        List<FinanIndicAnaly> records = (List<FinanIndicAnaly>) finanIndicAnalyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<FinanIndicAnaly> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FinanIndicAnaly> list = finanIndicAnalyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(FinanIndicAnaly record) {
        return finanIndicAnalyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(FinanIndicAnaly record) {
        return finanIndicAnalyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(FinanIndicAnaly record) {
        return finanIndicAnalyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(FinanIndicAnaly record) {
        return finanIndicAnalyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return finanIndicAnalyMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return finanIndicAnalyMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:index
     * @函数描述:通用列表初始化
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    public List<FinanIndicAnaly> selectFinanIndicAnalyList(Map map) {
        List<FinanIndicAnaly> list = new ArrayList<>();
        try{
            log.info("通用列表查询,参数{}", JSON.toJSONString(map));
            list = this.selectFinanIndicAnalyListBySerno((String) map.get("serno"));
            if(!list.isEmpty() && list.size()>0){
                log.info("通用列表查询数据成功", JSON.toJSONString(list));
            }else{
                log.info("通用列表查询数据为空,手动插入数据------start-------");
                this.insertIntoFinanIndicAnalyDataBySerno((String) map.get("serno"));
                log.info("通用列表查询数据,手动插入数据------end-------");
                log.info("手动插入数据,重新根据参数查询");
                list = finanIndicAnalyMapper.selectFinanIndicAnalyList(map);
            }
        }catch (Exception e){
            throw BizException.error(null,String.valueOf(e.hashCode()),e.getMessage());
        }finally {
            log.info("通用列表查询返回数据集{}", JSON.toJSONString(list));
        }
        return list;
    }

    /**
     * @函数名称:selectFinanIndicAnalyListByParam
     * @函数描述:通用列表查询
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */
    public List<FinanIndicAnaly> selectFinanIndicAnalyListByParam(Map map) {
        List<FinanIndicAnaly> list = new ArrayList<>();
        list = finanIndicAnalyMapper.selectFinanIndicAnalyList(map);
        return list;
    }

    /**
     * @函数名称:insertIntoFinanIndicAnalyDataBySerno
     * @函数描述:根据流水号查询数据
     * @参数与返回说明:
     * @param serno
     * @算法描述:
     */

    private List<FinanIndicAnaly> selectFinanIndicAnalyListBySerno(String serno) {
        return finanIndicAnalyMapper.selectFinanIndicAnalyListBySerno(serno);
    }

    /**
     * @函数名称:insertIntoFinanIndicAnalyDataBySerno
     * @函数描述:自动生成数据
     * @参数与返回说明:
     * @param serno
     * @算法描述:
     */

    public List<FinanIndicAnaly> insertIntoFinanIndicAnalyDataBySerno(String serno){
        List<FinanIndicAnaly> returnList = new ArrayList<>();
        try{
            Calendar cal = Calendar.getInstance();
            int curYear = cal.get(Calendar.YEAR);
            int curMonth = cal.get(Calendar.MONTH)+1;
            log.info("根据授信流水号{}查询当前授信信息中的调查报告类型数据",serno);
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            BigDecimal curAmt = new BigDecimal(0); // 当年月 银行融资总额
            BigDecimal lastOneAmt = new BigDecimal(0); // 去年月 银行融资总额
            BigDecimal lastTwoAmt = new BigDecimal(0); // 前年月 银行融资总额
            if(!StringUtils.isBlank(lmtApp.getRptType())){
                log.info("根据授信流水号{}查询调查报告信息---------start--------------",serno);
                QueryModel model = new QueryModel();
                model.addCondition("serno",serno);
                log.info("根据流水号查询调查报告经营情况销售情况--rpt_oper_energy_sales");
                List<RptOperEnergySales> rptOperEnergySalesList = rptOperEnergySalesService.selectByModel(model);
                if(!rptOperEnergySalesList.isEmpty() && rptOperEnergySalesList.size() > 0){
                        log.info("处理开票销售数据");
                        FinanIndicAnaly finanIndicAnaly = new FinanIndicAnaly();
                        finanIndicAnaly.setPkId(UUID.randomUUID().toString());
                        finanIndicAnaly.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_01);
                        finanIndicAnaly.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_01);
                        finanIndicAnaly.setSerno(serno);
                        finanIndicAnaly.setInputYear(rptOperEnergySalesList.get(0).getInputYear());
                        finanIndicAnaly.setCurYmValue(rptOperEnergySalesList.get(0).getCurrAppSaleIncome());
                        finanIndicAnaly.setNearFirstValue(rptOperEnergySalesList.get(0).getNearFirstAppSaleIncome());
                        finanIndicAnaly.setNearSecondValue(rptOperEnergySalesList.get(0).getNearSecondAppSaleIncome());
                        finanIndicAnaly.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                        finanIndicAnaly.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        finanIndicAnaly.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                        this.insert(finanIndicAnaly);
                        returnList.add(finanIndicAnaly);
                }
                log.info("根据流水号查询调查报告财务情况--rpt_fnc_situ");
                RptFncSitu rptFncSitu = rptFncSituService.selectByPrimaryKey(serno);
                if(rptFncSitu != null){
                    log.info("处理缴纳增值税数据");
                    FinanIndicAnaly finanIndicAnaly = new FinanIndicAnaly();
                    finanIndicAnaly.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_02);
                    finanIndicAnaly.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_02);
                    finanIndicAnaly.setSerno(serno);
                    finanIndicAnaly.setInputYear(rptFncSitu.getAcquisitionDate());
                    finanIndicAnaly.setCurYmValue(rptFncSitu.getCurPaidVat());
                    finanIndicAnaly.setNearFirstValue(rptFncSitu.getLastYearPaidVat());
                    finanIndicAnaly.setNearSecondValue(rptFncSitu.getLastTwoYearPaidVat());
                    finanIndicAnaly.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly);
                    returnList.add(finanIndicAnaly);
                    log.info("处理缴纳所得税数据");
                    FinanIndicAnaly finanIndicAnaly1 = new FinanIndicAnaly();
                    finanIndicAnaly1.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly1.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_03);
                    finanIndicAnaly1.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_03);
                    finanIndicAnaly1.setSerno(serno);
                    finanIndicAnaly1.setInputYear(rptFncSitu.getAcquisitionDate());
                    finanIndicAnaly1.setCurYmValue(rptFncSitu.getCurPaidIt());
                    finanIndicAnaly1.setNearFirstValue(rptFncSitu.getLastYearPaidIt());
                    finanIndicAnaly1.setNearSecondValue(rptFncSitu.getLastTwoYearPaidIt());
                    finanIndicAnaly1.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly1.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly1.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly1);
                    returnList.add(finanIndicAnaly1);
                }
                log.info("根据流水号查询调查报告经营情况能源用量--rpt_oper_energy_cons");
                RptOperEnergyCons rptOperEnergyCons = rptOperEnergyConsService.selectByPrimaryKey(serno);
                if(rptOperEnergyCons != null){
                    log.info("处理用电量数据");
                    FinanIndicAnaly finanIndicAnaly = new FinanIndicAnaly();
                    finanIndicAnaly.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_07);
                    finanIndicAnaly.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_07);
                    finanIndicAnaly.setSerno(serno);
                    finanIndicAnaly.setInputYear(rptOperEnergyCons.getInputYear());
                    finanIndicAnaly.setCurYmValue(new BigDecimal(rptOperEnergyCons.getCurrWaterAmount()==null || rptOperEnergyCons.getCurrWaterAmount().equals("")? "0" :rptOperEnergyCons.getCurrWaterAmount()));
                    finanIndicAnaly.setNearFirstValue(new BigDecimal(rptOperEnergyCons.getNearFirstWaterAmount()==null || rptOperEnergyCons.getNearFirstWaterAmount().equals("")? "0" :rptOperEnergyCons.getNearFirstWaterAmount()));
                    finanIndicAnaly.setNearSecondValue(new BigDecimal(rptOperEnergyCons.getNearSecondWaterAmount()==null || rptOperEnergyCons.getNearSecondWaterAmount().equals("")? "0" :rptOperEnergyCons.getNearSecondWaterAmount()));
                    finanIndicAnaly.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly);
                    returnList.add(finanIndicAnaly);
                    log.info("处理工人人数数据");
                    FinanIndicAnaly finanIndicAnaly1 = new FinanIndicAnaly();
                    finanIndicAnaly1.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly1.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_08);
                    finanIndicAnaly1.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_08);
                    finanIndicAnaly1.setSerno(serno);
                    finanIndicAnaly1.setInputYear(rptOperEnergyCons.getInputYear());
                    finanIndicAnaly1.setCurYmValue(new BigDecimal(rptOperEnergyCons.getCurrWorkerNum()==null? 0 : rptOperEnergyCons.getCurrWorkerNum()));
                    finanIndicAnaly1.setNearFirstValue(new BigDecimal(rptOperEnergyCons.getNearFirstWorkerNum()==null? 0 : rptOperEnergyCons.getNearFirstWorkerNum()));
                    finanIndicAnaly1.setNearSecondValue(new BigDecimal(rptOperEnergyCons.getNearSecondWorkerNum()==null? 0 : rptOperEnergyCons.getNearSecondWorkerNum()));
                    finanIndicAnaly1.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly1.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly1.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly1);
                    returnList.add(finanIndicAnaly1);
                }
                log.info("根据流水号查询调查报告融资情况公司名下--rpt_cptl_situ_corp");
                List<RptCptlSituCorp> rptCptlSituCorpList = rptCptlSituCorpService.selectByModel(model);
                int curNum = 0; // 当年月 融资银行数量
                int lastOneNum = 0; // 去年月 融资银行数量
                int lastTwoNum = 0; // 前年月 融资银行数量
                if(!rptCptlSituCorpList.isEmpty() && rptCptlSituCorpList.size() > 0){
                    for(RptCptlSituCorp rptCptlSituCorp : rptCptlSituCorpList){
                        log.info("循环遍历当前调查报告融资情况公司名下数据列表,计算出融资银行数量、银行融资总额，当前遍历对象数据："+JSON.toJSONString(rptCptlSituCorp));
                        if(!"".equals(rptCptlSituCorp.getCusId())){
                            curAmt = curAmt.add(rptCptlSituCorp.getCurMonthAmt()==null? curAmt : rptCptlSituCorp.getCurMonthAmt());
                            lastOneAmt = lastOneAmt.add(rptCptlSituCorp.getLastYearAmt()==null? lastOneAmt : rptCptlSituCorp.getLastYearAmt());
                            lastTwoAmt = lastTwoAmt.add(rptCptlSituCorp.getLastTwoYearAmt()==null? lastTwoAmt : rptCptlSituCorp.getLastTwoYearAmt());
                            if(rptCptlSituCorp.getCurMonthAmt()!=null){
                                curNum++;
                            }
                            if(rptCptlSituCorp.getLastYearAmt()!=null){
                                lastOneNum++;
                            }
                            if(rptCptlSituCorp.getLastTwoYearAmt()!=null){
                                lastTwoNum++;
                            }
                        }
                    }
                    log.info("处理银行融资总额数据");
                    FinanIndicAnaly finanIndicAnaly = new FinanIndicAnaly();
                    finanIndicAnaly.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_11);
                    finanIndicAnaly.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_11);
                    finanIndicAnaly.setSerno(serno);
                    finanIndicAnaly.setInputYear(rptOperEnergyCons.getInputYear());
                    finanIndicAnaly.setCurYmValue(curAmt);
                    finanIndicAnaly.setNearFirstValue(lastOneAmt);
                    finanIndicAnaly.setNearSecondValue(lastTwoAmt);
                    finanIndicAnaly.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly);
                    returnList.add(finanIndicAnaly);

                    log.info("处理融资银行数量数据");
                    FinanIndicAnaly finanIndicAnaly1 = new FinanIndicAnaly();
                    finanIndicAnaly1.setPkId(UUID.randomUUID().toString());
                    finanIndicAnaly1.setItemId(CmisCommonConstants.STD_CW_ITEM_ID_12);
                    finanIndicAnaly1.setItemName(CmisCommonConstants.STD_CW_ITEM_NAME_12);
                    finanIndicAnaly1.setSerno(serno);
                    finanIndicAnaly1.setInputYear(rptOperEnergyCons.getInputYear());
                    finanIndicAnaly1.setCurYmValue(new BigDecimal(curNum));
                    finanIndicAnaly1.setNearFirstValue(new BigDecimal(lastOneNum));
                    finanIndicAnaly1.setNearSecondValue(new BigDecimal(lastTwoNum));
                    finanIndicAnaly1.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_01);
                    finanIndicAnaly1.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    finanIndicAnaly1.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    this.insert(finanIndicAnaly1);
                    returnList.add(finanIndicAnaly1);
                }
                log.info("查询调查报告信息---------end--------------打印当前列表信息:"+JSON.toJSONString(returnList));
                }else{
                    log.info("当前授信调查报告信息不完善,请完善后在查看!");
                }
                log.info("根据客户编号{}查询财务报表信息---------start--------------",lmtApp.getCusId());
                /* 入参：客户号
                1、根据当前客户号去查询客户信息中的财务报表类型
                2、根据财报类型查询项下的资产负债表编号、损益表编号、财务指标表编号
                3、1）资产负债表数据：根据客户编号、当前年月（前一年年报。前两年年报。注：取前几年数据时，只需以当前年份报表类型中的字段为基准去找前几年的同名字段）、项目编号（注：根据资产负债表编号去确定项目编号）
                   2）损益表同上
                   3）财务指标表同上
                 */
               List<FinanIndicAnalyDto> fncConfTemplateDtoListFromCusClient = iCusClientService.selectFinanindicAnalysFncItemDataByCusId(lmtApp.getCusId()).getData();
               log.info("财报取数成功:"+JSON.toJSONString(fncConfTemplateDtoListFromCusClient));
               if(!fncConfTemplateDtoListFromCusClient.isEmpty() && fncConfTemplateDtoListFromCusClient.size() != 0){
                   for(FinanIndicAnalyDto finanIndicAnalyDto : fncConfTemplateDtoListFromCusClient){
                       FinanIndicAnaly finanIndicAnaly = new FinanIndicAnaly();
                       BeanUtils.copyProperties(finanIndicAnalyDto,finanIndicAnaly);
                       finanIndicAnaly.setPkId(UUID.randomUUID().toString());
                       finanIndicAnaly.setSerno(serno);
                       finanIndicAnaly.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                       finanIndicAnaly.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                       this.insert(finanIndicAnaly);
                       if(finanIndicAnalyDto.getItemId().equals(CmisCommonConstants.STD_IS_ITEM_ID_01)){
                           log.info("根据营业收入计算销贷比----------start----------");
                           FinanIndicAnaly finanIndicAnaly1 = new FinanIndicAnaly();
                           finanIndicAnaly1.setPkId(UUID.randomUUID().toString());
                           finanIndicAnaly1.setSerno(serno);
                           finanIndicAnaly1.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_16);
                           finanIndicAnaly1.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_16);
                           finanIndicAnaly1.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
                           finanIndicAnaly1.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
                           finanIndicAnaly1.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                           finanIndicAnaly1.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                           // 当前年
                           if(curAmt == null || curAmt.compareTo(new BigDecimal(0)) == 0){
                               finanIndicAnaly1.setCurYmValue(new BigDecimal(0));
                           }else{
                               try{
                                   finanIndicAnaly1.setCurYmValue(finanIndicAnalyDto.getCurYmValue().divide(curAmt,2,BigDecimal.ROUND_HALF_UP));
                               }catch(Exception e ){
                                   log.info("根据营业收入计算销贷比报错,财报中营业收入相关数据:"+JSON.toJSONString(finanIndicAnalyDto)+"--------当年月银行融资总额:"+curAmt);
                                   finanIndicAnaly1.setCurYmValue(new BigDecimal(0));
                               }

                           }
                           // 前一年
                           if(lastOneAmt == null || lastOneAmt.compareTo(new BigDecimal(0)) == 0){
                               finanIndicAnaly1.setNearFirstValue(new BigDecimal(0));
                           }else{
                               try{
                                   finanIndicAnaly1.setNearFirstValue(finanIndicAnalyDto.getNearFirstValue().divide(lastOneAmt,2,BigDecimal.ROUND_HALF_UP));
                               }catch(Exception e ){
                                   log.info("根据营业收入计算销贷比报错,财报中营业收入相关数据:"+JSON.toJSONString(finanIndicAnalyDto)+"--------上年月银行融资总额:"+lastOneAmt);
                                   finanIndicAnaly1.setNearFirstValue(new BigDecimal(0));
                               }

                           }
                           // 前两年
                           if(lastTwoAmt == null || lastTwoAmt.compareTo(new BigDecimal(0)) == 0){
                               finanIndicAnaly1.setNearSecondValue(new BigDecimal(0));
                           }else{
                               try{
                                   finanIndicAnaly1.setNearSecondValue(finanIndicAnalyDto.getNearSecondValue().divide(lastTwoAmt,2,BigDecimal.ROUND_HALF_UP));
                               }catch(Exception e ){
                                   log.info("根据营业收入计算销贷比报错,财报中营业收入相关数据:"+JSON.toJSONString(finanIndicAnalyDto)+"--------上两年月银行融资总额:"+lastTwoAmt);
                                   finanIndicAnaly1.setNearSecondValue(new BigDecimal(0));
                               }

                           }
                           this.insert(finanIndicAnaly1);
                           log.info("根据营业收入计算销贷比------------end----------");
                       }

                   }
               }else{
                   log.info("当前客户财报信息获取失败,请完善后在查看!");
               }

        }catch (Exception e){
            throw BizException.error(null, EcbEnum.ECB010100.key,EcbEnum.ECB010100.value);
        }
        return returnList;
    }

    /**
     * @函数名称:selectFinanIndicAnalyForOption
     * @函数描述:财务报表表格取数
     * @参数与返回说明:
     * @param map
     * @算法描述:
     */

    public Map selectFinanIndicAnalyForOption(Map map) {
        Map resMap = new HashMap();
        try{
            // 查询 折线信息 -- 财务报表数据
            map.put("finanIndicGroup","02");
            List<FinanIndicAnaly> list1 = finanIndicAnalyMapper.selectFinanIndicAnalyList(map);
            // 财务报表折线图 折线
            String option1LegendData[] = new String[list1.size()];
            // 财务报表折线图数据
            List<Map> option1Series = new ArrayList<>();
            for(int i = 0 ; i < list1.size() ; i++){
                option1LegendData[i] = list1.get(i).getItemName();
                Map optionSeriesMap = new HashMap();
                optionSeriesMap.put("name",list1.get(i).getItemName());
                BigDecimal itemValueData[] = {list1.get(i).getCurYmValue(),list1.get(i).getNearFirstValue(),list1.get(i).getNearSecondValue()};
                optionSeriesMap.put("data",itemValueData);
                option1Series.add(optionSeriesMap);
            }
            log.info("财务报表折线图数据加载:"+JSON.toJSONString(option1Series));
            resMap.put("option1LegendData",option1LegendData);
            resMap.put("option1Series",option1Series);
            // 查询调查报告 折线信息
            map.put("finanIndicGroup","01");
            List<FinanIndicAnaly> list2 = finanIndicAnalyMapper.selectFinanIndicAnalyList(map);
            // 调查报告折线图 折线
            String option2LegendData[] = new String[list2.size()];
            // 调查报告折线图数据
            List<Map> option2Series = new ArrayList<>();
            for(int i = 0 ; i < list2.size() ; i++){
                option2LegendData[i] = list2.get(i).getItemName();
                Map optionSeriesMap = new HashMap();
                optionSeriesMap.put("name",list2.get(i).getItemName());
                BigDecimal itemValueData[] = {list2.get(i).getCurYmValue(),list2.get(i).getNearFirstValue(),list2.get(i).getNearSecondValue()};
                optionSeriesMap.put("data",itemValueData);
                option2Series.add(optionSeriesMap);
            }
            log.info("调查报告折线图折线图数据加载:"+JSON.toJSONString(option2Series));
            resMap.put("option2LegendData",option2LegendData);
            resMap.put("option2Series",option2Series);
            // 查询授用信信息
            LmtApp lmtApp = lmtAppService.selectBySerno((String) map.get("serno"));
            QuotaReqDto quotaReqDto = new QuotaReqDto();
            quotaReqDto.setCus_id(lmtApp.getCusId());
            log.info("查询授用信接口请求参数:"+JSON.toJSONString(quotaReqDto));
            List<Data> dataDtoList = dscms2SjztClientService.quota(quotaReqDto).getData().getData();
            log.info("查询授用信接口返回参数:"+JSON.toJSONString(dataDtoList));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
            Date inureDate = new Date();
            if(CollectionUtils.nonEmpty(dataDtoList)){
                Collections.sort(dataDtoList, new Comparator<Data>() {
                            @Override
                            public int compare(Data o1, Data o2) {
                                int diff = 0;
                                try {
                                    diff = simpleDateFormat.parse(o1.getAcctdt()).compareTo(simpleDateFormat.parse(o2.getAcctdt()));
                                } catch (ParseException e) {
                                    log.info("根据日期升序处理授用信列表报错当前比对的的两条数据为:"+JSON.toJSONString(o1)+"--------"+JSON.toJSONString(o2));
                                    log.info("报错信息:"+JSON.toJSONString(e));
                                }
                                if(diff > 0){
                                    return 1;
                                }else{
                                    return -1;
                                }
                            }
                        }
                );
                log.info("查询授用信接口返回参数根据日期升序处理:"+JSON.toJSONString(dataDtoList));
                // 授用信折线图
                String xValueData[] = new String[dataDtoList.size()];// X轴年月数据
                BigDecimal sxValueData[] = new BigDecimal[dataDtoList.size()];// 授信
                BigDecimal yxValueData[] = new BigDecimal[dataDtoList.size()];// 用信
                for(int i = 0 ; i < dataDtoList.size() ; i++){
                    xValueData[i] = dataDtoList.get(i).getAcctdt();
                    sxValueData[i] = new BigDecimal(dataDtoList.get(i).getCredit_quota());
                    yxValueData[i] = new BigDecimal(dataDtoList.get(i).getQuota_used());
                }
                resMap.put("xValueData",xValueData);
                resMap.put("yxValueData",yxValueData);
                resMap.put("sxValueData",sxValueData);
                log.info("客户我行授用信分布数据加载,授信:"+JSON.toJSONString(sxValueData)+",用信:"+JSON.toJSONString(yxValueData));
            }
        }catch(Exception e){
            log.info("折线图数据加载报错:"+e.getMessage());
            throw BizException.error(null,null,String.valueOf(e.getMessage()));
        }
        return resMap;
    }

    /**
     * @函数名称:resetfinanindicanalylist
     * @函数描述:财务指标分析数据重置操作
     * @参数与返回说明:map
     * @算法描述:
     */

    public int reSetFinanIndicAnalyBySerno(Map map) {
        return finanIndicAnalyMapper.reSetFinanIndicAnalyBySerno(map);
    }
}
