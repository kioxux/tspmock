/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplTotlIncome
 * @类描述: aspl_totl_income数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "aspl_totl_income")
public class AsplTotlIncome extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 资产池类型 **/
	@Column(name = "ASSET_POOL_TYPE", unique = false, nullable = true, length = 5)
	private String assetPoolType;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 入池资产价值 **/
	@Column(name = "INP_ASSET_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetValue;
	
	/** 入池资产增加的客户贡献度 **/
	@Column(name = "INP_ASSET_BEFOR_CTBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetBeforCtbt;
	
	/** 入池前资产原始收益基准 **/
	@Column(name = "INP_ASSET_BEFOR_GROW", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetBeforGrow;
	
	/** 入池后资产收入 **/
	@Column(name = "INP_ASSET_AFTER_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetAfterIncome;
	
	/** 资产入池后收益增长情况 **/
	@Column(name = "INP_ASSET_AFTER_INCOME_GROW", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetAfterIncomeGrow;
	
	/** 入池资产质押换开票据本息及费用合计 **/
	@Column(name = "INP_ASSET_GUAR_CHG_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal inpAssetGuarChgTotalAmt;
	
	/** 预测入池资产逐笔贴现金额合计 **/
	@Column(name = "PREDICT_EVERY_DISC_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal predictEveryDiscTotalAmt;
	
	/** 融资成本节约金额 **/
	@Column(name = "FIN_COST_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finCostAmt;
	
	/** 融资成本节约比例 **/
	@Column(name = "FIN_COST_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal finCostPerc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param assetPoolType
	 */
	public void setAssetPoolType(String assetPoolType) {
		this.assetPoolType = assetPoolType;
	}
	
    /**
     * @return assetPoolType
     */
	public String getAssetPoolType() {
		return this.assetPoolType;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param inpAssetValue
	 */
	public void setInpAssetValue(java.math.BigDecimal inpAssetValue) {
		this.inpAssetValue = inpAssetValue;
	}
	
    /**
     * @return inpAssetValue
     */
	public java.math.BigDecimal getInpAssetValue() {
		return this.inpAssetValue;
	}
	
	/**
	 * @param inpAssetBeforCtbt
	 */
	public void setInpAssetBeforCtbt(java.math.BigDecimal inpAssetBeforCtbt) {
		this.inpAssetBeforCtbt = inpAssetBeforCtbt;
	}
	
    /**
     * @return inpAssetBeforCtbt
     */
	public java.math.BigDecimal getInpAssetBeforCtbt() {
		return this.inpAssetBeforCtbt;
	}
	
	/**
	 * @param inpAssetBeforGrow
	 */
	public void setInpAssetBeforGrow(java.math.BigDecimal inpAssetBeforGrow) {
		this.inpAssetBeforGrow = inpAssetBeforGrow;
	}
	
    /**
     * @return inpAssetBeforGrow
     */
	public java.math.BigDecimal getInpAssetBeforGrow() {
		return this.inpAssetBeforGrow;
	}
	
	/**
	 * @param inpAssetAfterIncome
	 */
	public void setInpAssetAfterIncome(java.math.BigDecimal inpAssetAfterIncome) {
		this.inpAssetAfterIncome = inpAssetAfterIncome;
	}
	
    /**
     * @return inpAssetAfterIncome
     */
	public java.math.BigDecimal getInpAssetAfterIncome() {
		return this.inpAssetAfterIncome;
	}
	
	/**
	 * @param inpAssetAfterIncomeGrow
	 */
	public void setInpAssetAfterIncomeGrow(java.math.BigDecimal inpAssetAfterIncomeGrow) {
		this.inpAssetAfterIncomeGrow = inpAssetAfterIncomeGrow;
	}
	
    /**
     * @return inpAssetAfterIncomeGrow
     */
	public java.math.BigDecimal getInpAssetAfterIncomeGrow() {
		return this.inpAssetAfterIncomeGrow;
	}
	
	/**
	 * @param inpAssetGuarChgTotalAmt
	 */
	public void setInpAssetGuarChgTotalAmt(java.math.BigDecimal inpAssetGuarChgTotalAmt) {
		this.inpAssetGuarChgTotalAmt = inpAssetGuarChgTotalAmt;
	}
	
    /**
     * @return inpAssetGuarChgTotalAmt
     */
	public java.math.BigDecimal getInpAssetGuarChgTotalAmt() {
		return this.inpAssetGuarChgTotalAmt;
	}
	
	/**
	 * @param predictEveryDiscTotalAmt
	 */
	public void setPredictEveryDiscTotalAmt(java.math.BigDecimal predictEveryDiscTotalAmt) {
		this.predictEveryDiscTotalAmt = predictEveryDiscTotalAmt;
	}
	
    /**
     * @return predictEveryDiscTotalAmt
     */
	public java.math.BigDecimal getPredictEveryDiscTotalAmt() {
		return this.predictEveryDiscTotalAmt;
	}
	
	/**
	 * @param finCostAmt
	 */
	public void setFinCostAmt(java.math.BigDecimal finCostAmt) {
		this.finCostAmt = finCostAmt;
	}
	
    /**
     * @return finCostAmt
     */
	public java.math.BigDecimal getFinCostAmt() {
		return this.finCostAmt;
	}
	
	/**
	 * @param finCostPerc
	 */
	public void setFinCostPerc(java.math.BigDecimal finCostPerc) {
		this.finCostPerc = finCostPerc;
	}
	
    /**
     * @return finCostPerc
     */
	public java.math.BigDecimal getFinCostPerc() {
		return this.finCostPerc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}