package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContInsur
 * @类描述: ctr_cont_insur数据实体类
 * @功能描述: 
 * @创建人: zsm
 * @创建时间: 2021-05-27 16:30:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CardDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private String cardPrd; //卡产品

	private String certType; //证件类型

	private String certCode; //证件号码

	private String cusName; //持卡人姓名

	private java.math.BigDecimal origCreditCardLmt; //信用额度

	public String getCardPrd() {
		return cardPrd;
	}

	public void setCardPrd(String cardPrd) {
		this.cardPrd = cardPrd;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public BigDecimal getOrigCreditCardLmt() {
		return origCreditCardLmt;
	}

	public void setOrigCreditCardLmt(BigDecimal origCreditCardLmt) {
		this.origCreditCardLmt = origCreditCardLmt;
	}
}