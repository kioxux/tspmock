package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import cn.com.yusys.yusp.dto.server.cmislmt0004.resp.CmisLmt0004RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerLstInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerStatusChgAppMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanProInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerStatusChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-13 15:39:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerStatusChgAppService {

    private static final Logger log = LoggerFactory.getLogger(CoopPartnerAgrAccInfoService.class);
    @Autowired
    private CoopPartnerStatusChgAppMapper coopPartnerStatusChgAppMapper;

    @Autowired
    private CoopProAccInfoService coopProAccInfoService;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;

    @Autowired
    private CoopPartnerAgrAccInfoService coopPartnerAgrAccInfoService;

    @Autowired
    private CoopPartnerLstInfoMapper coopPartnerLstInfoMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CoopPlanProInfoMapper coopPlanProInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CoopPartnerStatusChgApp selectByPrimaryKey(String serno) {
        return coopPartnerStatusChgAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CoopPartnerStatusChgApp> selectAll(QueryModel model) {
        List<CoopPartnerStatusChgApp> records = (List<CoopPartnerStatusChgApp>) coopPartnerStatusChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CoopPartnerStatusChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerStatusChgApp> list = coopPartnerStatusChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CoopPartnerStatusChgApp record) {
//        QueryModel queryModel = new QueryModel();
//        queryModel.addCondition("partnerNo",record.getPartnerNo());
//        List<CoopPartnerStatusChgApp> coopPartnerStatusChgAppList = coopPartnerStatusChgAppMapper.selectByModel(queryModel);
//        if(coopPartnerStatusChgAppList.size() > 0){
//            throw BizException.error(null, null, "该合作方已存在在途合作方退出申请！");
//        }
        return coopPartnerStatusChgAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CoopPartnerStatusChgApp record) {
        return coopPartnerStatusChgAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CoopPartnerStatusChgApp record) {
        return coopPartnerStatusChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CoopPartnerStatusChgApp record) {
        return coopPartnerStatusChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return coopPartnerStatusChgAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return coopPartnerStatusChgAppMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: coopPartnerQuit
     * @方法描述: 合作方退出
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public void coopPartnerQuit(String serno) {
        CoopPartnerStatusChgApp domain = this.selectByPrimaryKey(serno);
        // 1、更新申请表审批状态为审批通过
        domain.setApprStatus(CmisBizConstants.APPLY_STATE_PASS);
        this.update(domain);

        //合作方名单信息  状态变更为退出
        CoopPartnerLstInfo cooppartnerlstinfo = new CoopPartnerLstInfo();
        cooppartnerlstinfo.setPartnerNo(domain.getPartnerNo());
        cooppartnerlstinfo.setPartnerStatus("3");
        coopPartnerLstInfoMapper.updateByPrimaryKeySelective(cooppartnerlstinfo);
        // 2、更新合作方项下所有合作方案的合作方状态为“退出”   与3重复

        // 3、更新合作方项下所有合作方案的状态为“失效”
        QueryModel model = new QueryModel();
        model.addCondition("partnerNo", domain.getPartnerNo());
        //额度系统-合作方额度台账信息
        List<CmisLmt0008ApprListReqDto> apprList = new ArrayList<>();
        CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = null;
        List<CoopPlanAccInfo> coopPlanAccInfoList = coopPlanAccInfoService.selectAll(model);
        if (CollectionUtils.nonEmpty(coopPlanAccInfoList)) {
            for (CoopPlanAccInfo coopPlanAccInfo : coopPlanAccInfoList) {
                coopPlanAccInfo.setCoopPlanStatus("2"); // STD_COOP_PRO_STATUS  [{"key":"1","value":"正常"},{"key":"2","value":"失效"},{"key":"3","value":"冻结"}]
                coopPlanAccInfoService.updateSelective(coopPlanAccInfo);
                cmisLmt0008ApprListReqDto= new CmisLmt0008ApprListReqDto();
                cmisLmt0008ApprListReqDto.setApprSerno(coopPlanAccInfo.getCoopPlanNo());
                cmisLmt0008ApprListReqDto.setCusId(coopPlanAccInfo.getPartnerNo());
                cmisLmt0008ApprListReqDto.setOptType("02");//全部
                apprList.add(cmisLmt0008ApprListReqDto);
            }
        }

        // 4、更新合作方项下所有项目状态为“失效”
        List<CoopProAccInfo> coopProAccInfoList = coopProAccInfoService.selectAll(model);
        //额度系统-合作方额度明细信息
        List<CmisLmt0008ApprSubListReqDto> apprSubList = new ArrayList<>();
        CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto =null;
        if (CollectionUtils.nonEmpty(coopProAccInfoList)) {
            for (CoopProAccInfo coopProAccInfo : coopProAccInfoList) {
                coopProAccInfo.setProStatus("2"); // STD_COOP_PRO_STATUS字典  [{"key":"0","value":"未生效"},{"key":"1","value":"正常"},{"key":"2","value":"失效"},{"key":"3","value":"冻结"}]
                coopProAccInfoService.updateSelective(coopProAccInfo);
                cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
                cmisLmt0008ApprSubListReqDto.setApprSubSerno(coopProAccInfo.getProNo());
                cmisLmt0008ApprSubListReqDto.setCusId(coopProAccInfo.getPartnerNo());
                apprSubList.add(cmisLmt0008ApprSubListReqDto);
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("proNo",coopProAccInfo.getProNo());
                List<CoopPlanProInfoDto> coopPlanProInfoDtos = coopPlanProInfoMapper.selectByModel(queryModel);
                CoopPlanProInfoDto coopPlanProInfoDto  =null;
                if (CollectionUtils.nonEmpty(coopPlanProInfoDtos)) {
                    coopPlanProInfoDto = coopPlanProInfoDtos.get(0);
                    CoopPlanProInfo coopPlanProInfo = new CoopPlanProInfo();
                    coopPlanProInfo.setPkId(coopPlanProInfoDto.getPkId());
                    coopPlanProInfo.setProStatus("2");
                    coopPlanProInfoMapper.updateByPrimaryKeySelective(coopPlanProInfo);
                }
            }
        }
        // 5、更新合作方项下所有“合作协议状态=生效”状态的协议为失效
        List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfoList = coopPartnerAgrAccInfoService.selectAll(model);
        if (CollectionUtils.nonEmpty(coopPartnerAgrAccInfoList)) {
            for (CoopPartnerAgrAccInfo coopPartnerAgrAccInfo : coopPartnerAgrAccInfoList) {
                coopPartnerAgrAccInfo.setAgrStatus("02"); // COOP_PARTNER_AGR_STS   [{"key":"01","value":"生效"},{"key":"02","value":"作废"}]
                coopPartnerAgrAccInfoService.updateSelective(coopPartnerAgrAccInfo);
            }
        }
        //额度系统同步
        // 2.2 组装请求数据
        // 2.2.1 基本信息
        CmisLmt0008ReqDto reqDto = new CmisLmt0008ReqDto();
        reqDto.setSysId("cmis"); //系统编号
        reqDto.setLmtType("03");//合作方额度
        reqDto.setInputId(domain.getInputId()); // 登记人
        reqDto.setInputBrId(domain.getInputBrId()); // 登记机构
        reqDto.setInputDate(domain.getInputDate()); // 登记日期
        reqDto.setApprList(apprList);
        reqDto.setApprSubList(apprSubList);
        // 2.3 发送请求
        log.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(reqDto));
        // 3、判断调用结果，若失败直接抛异常回滚事务
        ResultDto<CmisLmt0008RespDto> result = cmisLmtClientService.cmisLmt0008(reqDto);
        log.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(result));
        // 3、判断调用结果，若失败直接抛异常回滚事务
        String resCode = result.getData().getErrorCode();
        String resMsg = result.getData().getErrorMsg();
        if ("0000".equals(resCode)) {
            log.info("合作方退出，申请流水号【{}】；后处理调用额度接口【cmisLmt0008】同步信息成功！", domain.getSerno());
        } else {
            log.error("合作方退出，申请流水号【{}】；后处理调用额度接口【cmisLmt0008】同步信息失败！失败原因【{}】", domain.getSerno(), resMsg);
            throw BizException.error(null, resCode, resMsg);
        }
    }

}
