/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageRel
 * @类描述: guar_mortgage_manage_rel数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_mortgage_manage_rel")
public class GuarMortgageManageRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 押品名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;
	
	/** 押品类型 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 押品所有人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 押品所有人名称 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;
	
	/** 押品所有人证件类型 **/
	@Column(name = "GUAR_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String guarCertType;
	
	/** 押品所有人证件号码 **/
	@Column(name = "GUAR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String guarCertCode;
	
	/** 押品评估价值 **/
	@Column(name = "EVAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal evalAmt;
	
	/** 押品认定价值 **/
	@Column(name = "CONFIRM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal confirmAmt;

	/** 押品标识STD_GRT_FLAG **/
	@Column(name = "GRT_FLAG", unique = false, nullable = true, length = 5)
	private String grtFlag;

	/** 担保分类代码 **/
	@Column(name = "GUAR_TYPE_CD", unique = false, nullable = true, length = 40)
	private String guarTypeCd;

	/** 担保分类代码名称**/
	@Column(name = "GUAR_TYPE_CD_NAME", unique = false, nullable = true, length = 100)
	private String guarTypeCdName;

	/** 权证编号 **/
	@Column(name = "WARRANT_NO", unique = false, nullable = true, length = 200)
	private String warrantNo;

	/** 核心担保编号 **/
	@Column(name = "CORE_GUARANTY_NO", unique = false, nullable = true, length = 40)
	private String coreGuarantyNo;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guarCertType
	 */
	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType;
	}
	
    /**
     * @return guarCertType
     */
	public String getGuarCertType() {
		return this.guarCertType;
	}
	
	/**
	 * @param guarCertCode
	 */
	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode;
	}
	
    /**
     * @return guarCertCode
     */
	public String getGuarCertCode() {
		return this.guarCertCode;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return evalAmt
     */
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return confirmAmt
     */
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}

	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}

	/**
	 * @return grtFlag
	 */
	public String getGrtFlag() {
		return grtFlag;
	}

	/**
	 * @param guarTypeCd
	 */
	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	/**
	 * @return guarTypeCd
	 */
	public String getGuarTypeCd() {
		return guarTypeCd;
	}

	/**
	 * @param guarTypeCdName
	 */
	public void setGuarTypeCdName(String guarTypeCdName) {
		this.guarTypeCdName = guarTypeCdName;
	}

	/**
	 * @return guarTypeCdName
	 */
	public String getGuarTypeCdName() {
		return guarTypeCdName;
	}

	public String getWarrantNo() {
		return warrantNo;
	}

	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo;
	}

	public String getCoreGuarantyNo() {
		return coreGuarantyNo;
	}

	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}
}