/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtBuildingPro;
import cn.com.yusys.yusp.repository.mapper.LmtBuildingProMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtBuildingProService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtBuildingProService {

    @Autowired
    private LmtBuildingProMapper lmtBuildingProMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtBuildingPro selectByPrimaryKey(String serno) {
        return lmtBuildingProMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtBuildingPro> selectAll(QueryModel model) {
        List<LmtBuildingPro> records = (List<LmtBuildingPro>) lmtBuildingProMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtBuildingPro> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtBuildingPro> list = lmtBuildingProMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtBuildingPro record) {
        return lmtBuildingProMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtBuildingPro record) {
        return lmtBuildingProMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtBuildingPro record) {
        return lmtBuildingProMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtBuildingPro record) {
        return lmtBuildingProMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtBuildingProMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtBuildingProMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateByParams
     * @方法描述: 逻辑删除--更新opr_type字段由01-新增变为02-删除
     * @参数: delMap
     * */

    public int updateByParams(Map delMap) {
        return lmtBuildingProMapper.updateByParams(delMap);
    }

    /**
     * @方法名称: selectCountByModel
     * @方法描述: 根据条件参数查询记录条数
     * @参数: model
     * */

    public int selectCountByModel(QueryModel model) {
        return lmtBuildingProMapper.selectCountByModel(model);
    }
}
