/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfOtherHouse;
import cn.com.yusys.yusp.domain.GuarInfUsufLand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfUsufLand;
import cn.com.yusys.yusp.service.GuarInfUsufLandService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfUsufLandResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfusufland")
public class GuarInfUsufLandResource {
    @Autowired
    private GuarInfUsufLandService guarInfUsufLandService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfUsufLand>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfUsufLand> list = guarInfUsufLandService.selectAll(queryModel);
        return new ResultDto<List<GuarInfUsufLand>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfUsufLand>> index(QueryModel queryModel) {
        List<GuarInfUsufLand> list = guarInfUsufLandService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfUsufLand>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfUsufLand> show(@PathVariable("serno") String serno) {
        GuarInfUsufLand guarInfUsufLand = guarInfUsufLandService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfUsufLand>(guarInfUsufLand);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfUsufLand> create(@RequestBody GuarInfUsufLand guarInfUsufLand) throws URISyntaxException {
        guarInfUsufLandService.insert(guarInfUsufLand);
        return new ResultDto<GuarInfUsufLand>(guarInfUsufLand);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfUsufLand guarInfUsufLand) throws URISyntaxException {
        int result = guarInfUsufLandService.update(guarInfUsufLand);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfUsufLandService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfUsufLandService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:preserveGuarInfUsufLand
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfUsufLand")
    protected ResultDto<Integer> preserveGuarInfUsufLand(@RequestBody GuarInfUsufLand guarInfUsufLand) {
        int result = guarInfUsufLandService.preserveGuarInfUsufLand(guarInfUsufLand);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfUsufLand> queryBySerno(@RequestBody GuarInfUsufLand record) {
        GuarInfUsufLand guarInfUsufLand = guarInfUsufLandService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfUsufLand>(guarInfUsufLand);
    }
}
