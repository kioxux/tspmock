package cn.com.yusys.yusp.web.server.xdzx0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0003.req.Xdzx0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.resp.Xdzx0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzx0003.Xdzx0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:征信查询授权状态同步
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0003:征信查询授权状态同步")
@RestController
@RequestMapping("/api/bizzx4bsp")
public class BizXdzx0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzx0003Resource.class);

    @Autowired
    private Xdzx0003Service xdzx0003Service;

    /**
     * 交易码：xdzx0003
     * 交易描述：征信查询授权状态同步
     *
     * @param xdzx0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("征信查询授权状态同步")
    @PostMapping("/xdzx0003")
    protected @ResponseBody
    ResultDto<Xdzx0003DataRespDto> xdzx0003(@Validated @RequestBody Xdzx0003DataReqDto xdzx0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataReqDto));
        Xdzx0003DataRespDto xdzx0003DataRespDto = new Xdzx0003DataRespDto();// 响应Dto:征信查询授权状态同步
        ResultDto<Xdzx0003DataRespDto> xdzx0003DataResultDto = new ResultDto<>();
        String approveStatus = xdzx0003DataReqDto.getApproveStatus();//状态
        String iamgeNo = xdzx0003DataReqDto.getIamgeNo();//影像流水号
        String crqlSerno = xdzx0003DataReqDto.getCrqlSerno();//征信查询申请流水号
        try {
            // 从xdzx0003DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataReqDto));
            xdzx0003DataRespDto = xdzx0003Service.syncApproveStatus(xdzx0003DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataRespDto));
            // 封装xdzx0003DataResultDto中正确的返回码和返回信息
            xdzx0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzx0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, e.getMessage());
            xdzx0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzx0003DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, e.getMessage());
            // 封装xdzx0003DataResultDto中异常返回码和返回信息
            xdzx0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzx0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzx0003DataRespDto到xdzx0003DataResultDto中
        xdzx0003DataResultDto.setData(xdzx0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataResultDto));
        return xdzx0003DataResultDto;
    }
}
