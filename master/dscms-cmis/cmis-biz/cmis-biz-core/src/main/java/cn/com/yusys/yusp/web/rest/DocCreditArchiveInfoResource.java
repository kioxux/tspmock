/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.domain.DocCreditArchiveInfo;
import cn.com.yusys.yusp.dto.DocCreditArchiveClientDto;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import cn.com.yusys.yusp.service.DocCreditArchiveInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocCreditArchiveInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:17:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/doccreditarchiveinfo")
public class DocCreditArchiveInfoResource {
    @Autowired
    private DocCreditArchiveInfoService docCreditArchiveInfoService;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<DocCreditArchiveInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<DocCreditArchiveInfo> list = docCreditArchiveInfoService.selectAll(queryModel);
        return new ResultDto<List<DocCreditArchiveInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<DocCreditArchiveInfo>> index(QueryModel queryModel) {
        List<DocCreditArchiveInfo> list = docCreditArchiveInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocCreditArchiveInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{dcaiSerno}")
    protected ResultDto<DocCreditArchiveInfo> show(@PathVariable("dcaiSerno") String dcaiSerno) {
        DocCreditArchiveInfo docCreditArchiveInfo = docCreditArchiveInfoService.selectByPrimaryKey(dcaiSerno);
        return new ResultDto<DocCreditArchiveInfo>(docCreditArchiveInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<DocCreditArchiveInfo> create(@RequestBody DocCreditArchiveInfo docCreditArchiveInfo) throws URISyntaxException {
        docCreditArchiveInfoService.insert(docCreditArchiveInfo);
        return new ResultDto<DocCreditArchiveInfo>(docCreditArchiveInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody DocCreditArchiveInfo docCreditArchiveInfo) throws URISyntaxException {
        int result = docCreditArchiveInfoService.update(docCreditArchiveInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{dcaiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("dcaiSerno") String dcaiSerno) {
        int result = docCreditArchiveInfoService.deleteByPrimaryKey(dcaiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = docCreditArchiveInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByCondition
     * @函数描述:根据条件查询人行征信档案归档列表
     * @参数与返回说明:
     * @算法描述:
     * @author cainingbo_yx
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<DocCreditArchiveInfo>> selectByCondition(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())) {
            queryModel.setSort(" dcai_serno desc");
        }
        List<DocCreditArchiveInfo> list = docCreditArchiveInfoService.selectByModel(queryModel);
        return new ResultDto<List<DocCreditArchiveInfo>>(list);
    }

    /**
     * @函数名称:updateByReceive
     * @函数描述:点击接受更新人行征信档案归档数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateByReceive")
    protected ResultDto<Map> updateByReceive(@RequestBody List<DocCreditArchiveInfo> docCreditArchiveInfoList) throws URISyntaxException {
        Map result = docCreditArchiveInfoService.updateByReceive(docCreditArchiveInfoList);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:updateByReceive
     * @函数描述:点击核对更新人行征信档案归档数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateByCheck")
    protected ResultDto<Map> updateByCheck(@RequestBody List<DocCreditArchiveInfo> docCreditArchiveInfoList) throws URISyntaxException {
        Map result = docCreditArchiveInfoService.updateByCheck(docCreditArchiveInfoList);
        return new ResultDto<>(result);
    }

    /**
     * 系统生成人行征信档案归档任务
     *
     * @author jijian_yx
     * @date 2021/7/1 10:02
     **/
    @PostMapping("/createDocCreateArchiveBySys")
    protected ResultDto<Integer> createDocCreateArchiveBySys(@RequestBody DocCreditArchiveClientDto docCreditArchiveClientDto) {
        int result = docCreditArchiveInfoService.createDocCreateArchiveBySys(docCreditArchiveClientDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据业务流水号获取人行征信查询信息
     *
     * @author jijian_yx
     * @date 2021/7/26 23:14
     **/
    @PostMapping("/queryQryCls")
    protected ResultDto<CreditReportQryLst> queryQryCls(@RequestBody String crqlSerno) {
        CreditReportQryLst creditReportQryLst = creditReportQryLstService.selectByPrimaryKey(crqlSerno);
        return new ResultDto<CreditReportQryLst>(creditReportQryLst);
    }

    /**
     * 导出
     *
     * @author jijian_yx
     * @date 2021/9/15 0:26
     **/
    @PostMapping("/export")
    protected ResultDto<ProgressDto> export(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = docCreditArchiveInfoService.export(queryModel);
        return ResultDto.success(progressDto);
    }
}
