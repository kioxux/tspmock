package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanApp
 * @类描述: coop_plan_app数据实体类
 * @功能描述: 
 * @创建人: pc
 * @创建时间: 2021-04-15 21:26:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 合作方类型 **/
	private String partnerType;
	
	/** 合作方编号 **/
	private String partnerNo;
	
	/** 合作方名称 **/
	private String partnerName;
	
	/** 合作类型 **/
	private String coopType;
	
	/** 是否全行适用 **/
	private String isWholeBankSuit;
	
	/** 总合作额度（元） **/
	private java.math.BigDecimal totlCoopLmtAmt;
	
	/** 一般担保额度（元） **/
	private java.math.BigDecimal commonGrtLmtAmt;
	
	/** 合作期限(月) **/
	private Integer coopTerm;
	
	/** 单户合作限度（元） **/
	private java.math.BigDecimal singleCoopQuota;
	
	/** 单笔业务合作限额（元） **/
	private java.math.BigDecimal sigBusiCoopQuota;
	
	/** 合作起始日 **/
	private String coopStartDate;
	
	/** 合作到期日 **/
	private String coopEndDate;
	
	/** 是否白名单控制 **/
	private String isWhiteListCtrl;
	
	/** 代偿宽限期(天) **/
	private Integer subpayGraper;
	
	/** 代偿比例 **/
	private java.math.BigDecimal subpayPerc;
	
	/** 对外担保放大倍数 **/
	private java.math.BigDecimal outguarMultiple;
	
	/** 其他相关说明 **/
	private String otherCorreDesc;
	
	/** 保证金比例 **/
	private java.math.BigDecimal bailPerc;
	
	/** 保证金账户最低金额(元) **/
	private java.math.BigDecimal bailAccLowAmt;
	
	/** 单笔最低缴存金额(元) **/
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金透支上限(元) **/
	private java.math.BigDecimal bailOverdraftMax;
	
	/** 保证金缴存方式 **/
	private String bailDepositMode;
	
	/** 保证金账号 **/
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	private String bailAccNoSubSeq;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 调查意见 **/
	private String indgtAdvice;
	
	/** 审批状态 **/
	private String apprStatus;
	
	/** 影像编号 **/
	private String imageNo;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 统一社会信用代码 **/
	private String unifyCreditCode;
	
	/** 企业性质 **/
	private String corpCha;
	
	/** 成立日期 **/
	private String buildDate;
	
	/** 注册资本 **/
	private java.math.BigDecimal regiCapAmt;
	
	/** 营业期限（月） **/
	private Integer bsinsTerm;
	
	/** 经营范围 **/
	private String operRange;
	
	/** 企业规模 **/
	private String corpScale;
	
	/** 行业分类 **/
	private String tradeClass;
	
	/** 法人代表 **/
	private String legal;
	
	/** 是否集团型客户 **/
	private String isGrpCus;
	
	/** 办公地址 **/
	private String officeAddr;
	
	/** 注册地址 **/
	private String regiAddr;
	
	/** 是否我行关联方 **/
	private String isBankCorre;
	
	/** 是否我行风险预警客户 **/
	private String isBankRiskAltCus;
	
	/** 合作说明 **/
	private String coopDesc;
	
	/** 调查报告类型 **/
	private String indgtReportType;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType == null ? null : partnerType.trim();
	}
	
    /**
     * @return PartnerType
     */	
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo == null ? null : partnerNo.trim();
	}
	
    /**
     * @return PartnerNo
     */	
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName == null ? null : partnerName.trim();
	}
	
    /**
     * @return PartnerName
     */	
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType == null ? null : coopType.trim();
	}
	
    /**
     * @return CoopType
     */	
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param isWholeBankSuit
	 */
	public void setIsWholeBankSuit(String isWholeBankSuit) {
		this.isWholeBankSuit = isWholeBankSuit == null ? null : isWholeBankSuit.trim();
	}
	
    /**
     * @return IsWholeBankSuit
     */	
	public String getIsWholeBankSuit() {
		return this.isWholeBankSuit;
	}
	
	/**
	 * @param totlCoopLmtAmt
	 */
	public void setTotlCoopLmtAmt(java.math.BigDecimal totlCoopLmtAmt) {
		this.totlCoopLmtAmt = totlCoopLmtAmt;
	}
	
    /**
     * @return TotlCoopLmtAmt
     */	
	public java.math.BigDecimal getTotlCoopLmtAmt() {
		return this.totlCoopLmtAmt;
	}
	
	/**
	 * @param commonGrtLmtAmt
	 */
	public void setCommonGrtLmtAmt(java.math.BigDecimal commonGrtLmtAmt) {
		this.commonGrtLmtAmt = commonGrtLmtAmt;
	}
	
    /**
     * @return CommonGrtLmtAmt
     */	
	public java.math.BigDecimal getCommonGrtLmtAmt() {
		return this.commonGrtLmtAmt;
	}
	
	/**
	 * @param coopTerm
	 */
	public void setCoopTerm(Integer coopTerm) {
		this.coopTerm = coopTerm;
	}
	
    /**
     * @return CoopTerm
     */	
	public Integer getCoopTerm() {
		return this.coopTerm;
	}
	
	/**
	 * @param singleCoopQuota
	 */
	public void setSingleCoopQuota(java.math.BigDecimal singleCoopQuota) {
		this.singleCoopQuota = singleCoopQuota;
	}
	
    /**
     * @return SingleCoopQuota
     */	
	public java.math.BigDecimal getSingleCoopQuota() {
		return this.singleCoopQuota;
	}
	
	/**
	 * @param sigBusiCoopQuota
	 */
	public void setSigBusiCoopQuota(java.math.BigDecimal sigBusiCoopQuota) {
		this.sigBusiCoopQuota = sigBusiCoopQuota;
	}
	
    /**
     * @return SigBusiCoopQuota
     */	
	public java.math.BigDecimal getSigBusiCoopQuota() {
		return this.sigBusiCoopQuota;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate == null ? null : coopStartDate.trim();
	}
	
    /**
     * @return CoopStartDate
     */	
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate == null ? null : coopEndDate.trim();
	}
	
    /**
     * @return CoopEndDate
     */	
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param isWhiteListCtrl
	 */
	public void setIsWhiteListCtrl(String isWhiteListCtrl) {
		this.isWhiteListCtrl = isWhiteListCtrl == null ? null : isWhiteListCtrl.trim();
	}
	
    /**
     * @return IsWhiteListCtrl
     */	
	public String getIsWhiteListCtrl() {
		return this.isWhiteListCtrl;
	}
	
	/**
	 * @param subpayGraper
	 */
	public void setSubpayGraper(Integer subpayGraper) {
		this.subpayGraper = subpayGraper;
	}
	
    /**
     * @return SubpayGraper
     */	
	public Integer getSubpayGraper() {
		return this.subpayGraper;
	}
	
	/**
	 * @param subpayPerc
	 */
	public void setSubpayPerc(java.math.BigDecimal subpayPerc) {
		this.subpayPerc = subpayPerc;
	}
	
    /**
     * @return SubpayPerc
     */	
	public java.math.BigDecimal getSubpayPerc() {
		return this.subpayPerc;
	}
	
	/**
	 * @param outguarMultiple
	 */
	public void setOutguarMultiple(java.math.BigDecimal outguarMultiple) {
		this.outguarMultiple = outguarMultiple;
	}
	
    /**
     * @return OutguarMultiple
     */	
	public java.math.BigDecimal getOutguarMultiple() {
		return this.outguarMultiple;
	}
	
	/**
	 * @param otherCorreDesc
	 */
	public void setOtherCorreDesc(String otherCorreDesc) {
		this.otherCorreDesc = otherCorreDesc == null ? null : otherCorreDesc.trim();
	}
	
    /**
     * @return OtherCorreDesc
     */	
	public String getOtherCorreDesc() {
		return this.otherCorreDesc;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return BailPerc
     */	
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAccLowAmt
	 */
	public void setBailAccLowAmt(java.math.BigDecimal bailAccLowAmt) {
		this.bailAccLowAmt = bailAccLowAmt;
	}
	
    /**
     * @return BailAccLowAmt
     */	
	public java.math.BigDecimal getBailAccLowAmt() {
		return this.bailAccLowAmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return SigLowDepositAmt
     */	
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailOverdraftMax
	 */
	public void setBailOverdraftMax(java.math.BigDecimal bailOverdraftMax) {
		this.bailOverdraftMax = bailOverdraftMax;
	}
	
    /**
     * @return BailOverdraftMax
     */	
	public java.math.BigDecimal getBailOverdraftMax() {
		return this.bailOverdraftMax;
	}
	
	/**
	 * @param bailDepositMode
	 */
	public void setBailDepositMode(String bailDepositMode) {
		this.bailDepositMode = bailDepositMode == null ? null : bailDepositMode.trim();
	}
	
    /**
     * @return BailDepositMode
     */	
	public String getBailDepositMode() {
		return this.bailDepositMode;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo == null ? null : bailAccNo.trim();
	}
	
    /**
     * @return BailAccNo
     */	
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSubSeq
	 */
	public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
		this.bailAccNoSubSeq = bailAccNoSubSeq == null ? null : bailAccNoSubSeq.trim();
	}
	
    /**
     * @return BailAccNoSubSeq
     */	
	public String getBailAccNoSubSeq() {
		return this.bailAccNoSubSeq;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param indgtAdvice
	 */
	public void setIndgtAdvice(String indgtAdvice) {
		this.indgtAdvice = indgtAdvice == null ? null : indgtAdvice.trim();
	}
	
    /**
     * @return IndgtAdvice
     */	
	public String getIndgtAdvice() {
		return this.indgtAdvice;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo == null ? null : imageNo.trim();
	}
	
    /**
     * @return ImageNo
     */	
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode == null ? null : unifyCreditCode.trim();
	}
	
    /**
     * @return UnifyCreditCode
     */	
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param corpCha
	 */
	public void setCorpCha(String corpCha) {
		this.corpCha = corpCha == null ? null : corpCha.trim();
	}
	
    /**
     * @return CorpCha
     */	
	public String getCorpCha() {
		return this.corpCha;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate == null ? null : buildDate.trim();
	}
	
    /**
     * @return BuildDate
     */	
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return RegiCapAmt
     */	
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param bsinsTerm
	 */
	public void setBsinsTerm(Integer bsinsTerm) {
		this.bsinsTerm = bsinsTerm;
	}
	
    /**
     * @return BsinsTerm
     */	
	public Integer getBsinsTerm() {
		return this.bsinsTerm;
	}
	
	/**
	 * @param operRange
	 */
	public void setOperRange(String operRange) {
		this.operRange = operRange == null ? null : operRange.trim();
	}
	
    /**
     * @return OperRange
     */	
	public String getOperRange() {
		return this.operRange;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale == null ? null : corpScale.trim();
	}
	
    /**
     * @return CorpScale
     */	
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass == null ? null : tradeClass.trim();
	}
	
    /**
     * @return TradeClass
     */	
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal == null ? null : legal.trim();
	}
	
    /**
     * @return Legal
     */	
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param isGrpCus
	 */
	public void setIsGrpCus(String isGrpCus) {
		this.isGrpCus = isGrpCus == null ? null : isGrpCus.trim();
	}
	
    /**
     * @return IsGrpCus
     */	
	public String getIsGrpCus() {
		return this.isGrpCus;
	}
	
	/**
	 * @param officeAddr
	 */
	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr == null ? null : officeAddr.trim();
	}
	
    /**
     * @return OfficeAddr
     */	
	public String getOfficeAddr() {
		return this.officeAddr;
	}
	
	/**
	 * @param regiAddr
	 */
	public void setRegiAddr(String regiAddr) {
		this.regiAddr = regiAddr == null ? null : regiAddr.trim();
	}
	
    /**
     * @return RegiAddr
     */	
	public String getRegiAddr() {
		return this.regiAddr;
	}
	
	/**
	 * @param isBankCorre
	 */
	public void setIsBankCorre(String isBankCorre) {
		this.isBankCorre = isBankCorre == null ? null : isBankCorre.trim();
	}
	
    /**
     * @return IsBankCorre
     */	
	public String getIsBankCorre() {
		return this.isBankCorre;
	}
	
	/**
	 * @param isBankRiskAltCus
	 */
	public void setIsBankRiskAltCus(String isBankRiskAltCus) {
		this.isBankRiskAltCus = isBankRiskAltCus == null ? null : isBankRiskAltCus.trim();
	}
	
    /**
     * @return IsBankRiskAltCus
     */	
	public String getIsBankRiskAltCus() {
		return this.isBankRiskAltCus;
	}
	
	/**
	 * @param coopDesc
	 */
	public void setCoopDesc(String coopDesc) {
		this.coopDesc = coopDesc == null ? null : coopDesc.trim();
	}
	
    /**
     * @return CoopDesc
     */	
	public String getCoopDesc() {
		return this.coopDesc;
	}
	
	/**
	 * @param indgtReportType
	 */
	public void setIndgtReportType(String indgtReportType) {
		this.indgtReportType = indgtReportType == null ? null : indgtReportType.trim();
	}
	
    /**
     * @return IndgtReportType
     */	
	public String getIndgtReportType() {
		return this.indgtReportType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}