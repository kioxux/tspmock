package cn.com.yusys.yusp.service.server.xdht0038;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0038.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:乐悠金根据核心客户号查询房贷首付款比例进行额度计算
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdht0038Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0038Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 乐悠金根据核心客户号查询房贷首付款比例进行额度计算
     * @param xdht0038DataReqDto
     * @return
     */
    @Transactional
    public java.util.List<List> getFirstpayByCusId(Xdht0038DataReqDto xdht0038DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataReqDto));
        java.util.List<List> result = ctrLoanContMapper.getFirstpayByCusId(xdht0038DataReqDto);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(result));
        return result;
    }
}
