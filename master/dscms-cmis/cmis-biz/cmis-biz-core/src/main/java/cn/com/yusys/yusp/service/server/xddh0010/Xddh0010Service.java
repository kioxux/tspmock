package cn.com.yusys.yusp.service.server.xddh0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.server.xddh0010.req.Xddh0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.server.xdtz0050.Xdtz0050Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：FRUIT
 * @创建时间：2021/6/15 23:51
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xddh0010Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0050Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * @param xddh0010DataReqDto:
     * @Description:校验参数信息，通过后生成预警数据
     * @Author: YX-WJ
     * @Date: 2021/6/16 0:17
     * @return: cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto
     **/
    public Xddh0010DataRespDto xddh0010(Xddh0010DataReqDto xddh0010DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value);
        Xddh0010DataRespDto xddh0010DataRespDto = new Xddh0010DataRespDto();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, xddh0010DataReqDto.toString());
            AccLoan accLoan = accLoanMapper.selectByBillNo(xddh0010DataReqDto.getBillNo());
            //如果根据借据号无法查询到台账信息，则查询失败，直接返回
            if (null == accLoan) {
                writeReturnMessage(xddh0010DataRespDto, "F", "信贷缺失此借据的借据信息!");
                // 可以查询到值，且取出的合同号不为空，继续校验,根据合同号查询合同信息
            } else if (StringUtils.isEmpty(accLoan.getContNo())) {
                writeReturnMessage(xddh0010DataRespDto, "F", "借据表中的合同号为空!");
            } else {
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(accLoan.getContNo());
                //查询不到合同信息，校验失败
                if (null == ctrLoanCont) {
                    writeReturnMessage(xddh0010DataRespDto, "F", "信贷缺失此借据的合同信息!");
                } else {
                    // 可以查到合同信息，校验结束，开始生成预警数据
                    makeRiskInfo(xddh0010DataRespDto);
                }
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, xddh0010DataReqDto.toString());
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value);
        return xddh0010DataRespDto;
    }

    /**
     * @param flag:         成功标志，S成功  F失败
     * @param errorMessage: 具体内容
     * @Description:写入校验结果信息
     * @Author: YX-WJ
     * @Date: 2021/6/16 0:22
     * @return: void
     **/
    private void writeReturnMessage(Xddh0010DataRespDto xddh0010DataRespDto, String flag, String errorMessage) {
        xddh0010DataRespDto.setOpFlag(flag);
        xddh0010DataRespDto.setOpMsg(errorMessage);
    }

    /**
     * @Description:校验通过，生成预警信息
     * @Author: YX-WJ
     * @Date: 2021/6/16 0:15
     * @return: void
     **/
    private void makeRiskInfo(Xddh0010DataRespDto xddh0010DataRespDto) {
        //    todo 生成预警信息
        writeReturnMessage(xddh0010DataRespDto, "S", "交易成功");
    }

}
