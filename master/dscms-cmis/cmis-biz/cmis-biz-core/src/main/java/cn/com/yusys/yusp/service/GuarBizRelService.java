package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CreditReportQryLstAndRealDto;
import cn.com.yusys.yusp.dto.GuarBaseInfoRelDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBizRelMapper;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarBizRelService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private GuarBizRelMapper guarBizRelMapper;
    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private CreditQryBizRealService creditQryBizRealService;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;
    @Autowired
    private CoopColonyWhiteLstService coopColonyWhiteLstService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private CoopPlanAccInfoService coopPlanAccInfoService;
    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarBizRel selectByPrimaryKey(String pkId) {
        return guarBizRelMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectGuarBizRelGuarBaseDto
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarBizRelGuarBaseDto selectGuarBizRelGuarBaseDto(String pkId) {
        return guarBizRelMapper.selectGuarBizRelGuarBaseDto(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<GuarBizRel> selectAll(QueryModel model) {
        List<GuarBizRel> records = (List<GuarBizRel>) guarBizRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarBizRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBizRel> list = guarBizRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(GuarBizRel record) {
        return guarBizRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(GuarBizRel record) {
        return guarBizRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(GuarBizRel record) {
        return guarBizRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据业务流水更新
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateBySerno(Map map) {
        return guarBizRelMapper.updateBySerno(map);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(GuarBizRel record) {
        return guarBizRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return guarBizRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return guarBizRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据主键删除业务与保证人关系表，删除征信关系表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteGuarInfoRelGuarantee(GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        int result;
        result = guarBizRelMapper.deleteByIds(guarBizRelGuaranteeDto.getPkId());
        if (result != 1) {
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "删除业务关系表异常！");
        }
        Map params = new HashMap();
        params.put("iqpSerno", guarBizRelGuaranteeDto.getSerno());
        params.put("certCode", guarBizRelGuaranteeDto.getAssureCertCode());
        result = creditQryBizRealMapper.deleteByCondition(params);
        return result;
    }

    /**
     * @param guarBizRelGuaranteeDto
     * @return Integer
     * @author 王祝远
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存担保人关联表
     * @修改历史: V1.0
     */
    public int insertGuarBizRel(GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        int result;
        try {
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
            BigDecimal GuaranteeAmt = new BigDecimal(guarBizRelGuaranteeDto.getGuarAmt());
            String cusId = guarBizRelGuaranteeDto.getCusId();
            String cusName = guarBizRelGuaranteeDto.getAssureName();
            String CertCode = guarBizRelGuaranteeDto.getAssureCertCode();
            String certType = guarBizRelGuaranteeDto.getCerType();
            String workSpace = guarBizRelGuaranteeDto.getBusiness();
            String marry = guarBizRelGuaranteeDto.getMarry();
            String cusTyp = guarBizRelGuaranteeDto.getCusTyp();
            String iqp_serno = guarBizRelGuaranteeDto.getSerno();
            String guarantyId = guarBizRelGuaranteeDto.getGuarantyId();
            String guarantyType = guarBizRelGuaranteeDto.getGuarantyType();
            String isAddGuar = guarBizRelGuaranteeDto.getIsAddGuar();
            GuarBizRel guarBizRel = new GuarBizRel();
            guarBizRel.setPkId(serno);
            //业务流水号
            guarBizRel.setSerno(iqp_serno);
            //押品编号
            guarBizRel.setGuarNo(guarBizRelGuaranteeDto.getGuarantyId());
            guarBizRel.setIsAddGuar(isAddGuar);
            guarBizRel.setOprType("01");
            //查询是否引入相同保证人
            Map params = new HashMap();
            params.put("iqpSerno", guarBizRelGuaranteeDto.getSerno());
            params.put("certCode", guarBizRelGuaranteeDto.getAssureCertCode());
            result = guarBizRelMapper.selectGuarantee(params);
            if (result > 0) {
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该业务下已存在相同保证人！");
            }
            result = guarBizRelMapper.insertSelective(guarBizRel);
            GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarantyId);
            guarGuarantee.setGuarantyType(guarantyType);
            guarGuaranteeService.updateSelective(guarGuarantee);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return result;
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author wzy
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    public int insertGuarBizRelGuarBase(GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        int result = 1;
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        String serno_pk = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
        //抵押物流水号
        String serno = guarBizRelGuarBaseDto.getSerno();
        //业务流水号
        String iqpSerno = guarBizRelGuarBaseDto.getIqpSerno();
        //抵质押编号
        String guarNo = guarBizRelGuarBaseDto.getGuarNo();
        //担保分类名称
        String guarTypeCd = guarBizRelGuarBaseDto.getGuarTypeCd();
//        抵质押品所有人
        String guarCusName = guarBizRelGuarBaseDto.getGuarCusName();
//        评估价(元）
        String evalAmt = guarBizRelGuarBaseDto.getEvalAmt();
//        抵/质押物总价（元）
        String maxMortagageAmt = guarBizRelGuarBaseDto.getMaxMortagageAmt();
//        抵/质押率（%）
        String mortagageRate = guarBizRelGuarBaseDto.getMortagageRate();
//        登记时间
        String inputDate = guarBizRelGuarBaseDto.getInputDate();
//        更新时间
        String isAddGuar = guarBizRelGuarBaseDto.getIsAddGuar();
        java.math.BigDecimal correFinAmt = guarBizRelGuarBaseDto.getCorreFinAmt();
        try {

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarNo", guarNo);
            queryModel.addCondition("serno", iqpSerno);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

            List<GuarBizRel> guarBizRels = guarBizRelService.selectAll(queryModel);

            if (CollectionUtils.nonEmpty(guarBizRels)) {
                throw new BizException(null, "", null, "押品不能被重复引入！");
            }

//            GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
//            guarBaseInfo.setSerno(serno);
//            guarBaseInfo.setGuarNo(guarNo);
//            result = guarBaseInfoService.updateSelective(guarBaseInfo);
//            Asserts.isTrue(result > 0, "更新关联押品信息表");
//            logger.info("更新关联押品信息表");

            String updDate = guarBizRelGuarBaseDto.getUpdDate();
            GuarBizRel guarBizRel = new GuarBizRel();

            guarBizRel.setPkId(serno_pk);
            guarBizRel.setGuarNo(guarNo);
            guarBizRel.setSerno(iqpSerno);
            guarBizRel.setInputDate(inputDate);
            guarBizRel.setUpdDate(updDate);
            guarBizRel.setIsAddGuar(isAddGuar);
            guarBizRel.setCorreFinAmt(correFinAmt);
            guarBizRel.setOprType(CmisBizConstants.OPR_TYPE_01);
            guarBizRel.setMortagageRate(new BigDecimal(mortagageRate));
            guarBizRel.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
            result = guarBizRelMapper.insertSelective(guarBizRel);
            Asserts.isTrue(result > 0, "新增业务关联押品信息表");
            logger.info("新增业务关联押品信息表");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(null, "", null, e.getMessage());
        } finally {
            logger.info("新增业务关联押品信息表结束..............");
        }
        return result;
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author zhangliang15
     * @date 2021/09/27 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     *       用于房抵e点贷授信押品关联，房抵贷授信后抵押品补录，先引入关系表为oprType:02,审批通过后更新为01
     * @修改历史: V1.0
     */
    public int ffdSaveGuarInfoRel(GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        int result = 1;
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        String serno_pk = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
        //抵押物流水号
        String serno = guarBizRelGuarBaseDto.getSerno();
        //业务流水号
        String iqpSerno = guarBizRelGuarBaseDto.getIqpSerno();
        //抵质押编号
        String guarNo = guarBizRelGuarBaseDto.getGuarNo();
        //担保分类名称
        String guarTypeCd = guarBizRelGuarBaseDto.getGuarTypeCd();
//        抵质押品所有人
        String guarCusName = guarBizRelGuarBaseDto.getGuarCusName();
//        评估价(元）
        String evalAmt = guarBizRelGuarBaseDto.getEvalAmt();
//        抵/质押物总价（元）
        String maxMortagageAmt = guarBizRelGuarBaseDto.getMaxMortagageAmt();
//        抵/质押率（%）
        String mortagageRate = guarBizRelGuarBaseDto.getMortagageRate();
//        登记时间
        String inputDate = guarBizRelGuarBaseDto.getInputDate();
//        更新时间
        String isAddGuar = guarBizRelGuarBaseDto.getIsAddGuar();
        java.math.BigDecimal correFinAmt = guarBizRelGuarBaseDto.getCorreFinAmt();
        try {

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarNo", guarNo);
            queryModel.addCondition("serno", iqpSerno);

            List<GuarBizRel> guarBizRels = guarBizRelService.selectAll(queryModel);

            if (CollectionUtils.nonEmpty(guarBizRels)) {
                throw new BizException(null, "", null, "押品不能被重复引入！");
            }

            GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
            guarBaseInfo.setSerno(serno);
            guarBaseInfo.setGuarNo(guarNo);
            guarBaseInfo.setMortagageRate(new BigDecimal(mortagageRate));
            guarBaseInfo.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
            result = guarBaseInfoService.updateSelective(guarBaseInfo);
            Asserts.isTrue(result > 0, "更新关联押品信息表");
            logger.info("更新关联押品信息表");

            String updDate = guarBizRelGuarBaseDto.getUpdDate();
            GuarBizRel guarBizRel = new GuarBizRel();
            guarBizRel.setPkId(serno_pk);
            guarBizRel.setGuarNo(guarNo);
            guarBizRel.setSerno(iqpSerno);
            guarBizRel.setInputDate(inputDate);
            guarBizRel.setUpdDate(updDate);
            guarBizRel.setIsAddGuar(isAddGuar);
            guarBizRel.setCorreFinAmt(correFinAmt);
            guarBizRel.setOprType(CmisBizConstants.OPR_TYPE_02);
            guarBizRel.setMortagageRate(new BigDecimal(mortagageRate));
            guarBizRel.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
            result = guarBizRelMapper.insertSelective(guarBizRel);
            logger.info("新增业务关联押品信息表");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(null, "", null, e.getMessage());
        } finally {
            logger.info("新增业务关联押品信息表结束");
        }
        return result;
    }

    /**
     * 更新押品信息及业务关联押品信息表
     *
     * @param guarBizRelGuarBaseDto
     * @return
     */
    public int updateGuarBaseInfo(GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        int result;
        //抵押物流水号
        String serno = guarBizRelGuarBaseDto.getSerno();
        //业务流水号
        String iqpSerno = guarBizRelGuarBaseDto.getIqpSerno();
        //抵质押编号
        String guarNo = guarBizRelGuarBaseDto.getGuarNo();
        //我行可用价值
        String maxMortagageAmt = guarBizRelGuarBaseDto.getMaxMortagageAmt();
        //抵/质押率（%）
        String mortagageRate = guarBizRelGuarBaseDto.getMortagageRate();
        //对应融资金额
        BigDecimal correFinAmt = guarBizRelGuarBaseDto.getCorreFinAmt();
        //担保标识
        String isAddGuar = guarBizRelGuarBaseDto.getIsAddGuar();
        //更新日期
        String updDate = guarBizRelGuarBaseDto.getUpdDate();

        try {
            logger.info("更新业务关联押品信息表 开始");
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarNo", guarNo);
            queryModel.addCondition("serno", iqpSerno);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

            GuarBizRel guarBizRel = guarBizRelService.selectAll(queryModel).get(0);
            guarBizRel.setIsAddGuar(isAddGuar);
            guarBizRel.setCorreFinAmt(correFinAmt);
            guarBizRel.setUpdDate(updDate);
            guarBizRel.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
            guarBizRel.setMortagageRate(new BigDecimal(mortagageRate));
            result = guarBizRelMapper.updateByPrimaryKey(guarBizRel);
            logger.info("更新业务关联押品信息表 结束");

//            logger.info("更新押品信息表 开始");
//            GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectByPrimaryKey(serno);
//            guarBaseInfo.setMortagageRate(new BigDecimal(mortagageRate));
//            guarBaseInfo.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
//            guarBaseInfo.setEvalAmt(new BigDecimal(evalAmt));
//            result = guarBaseInfoService.updateSelective(guarBaseInfo);
//            logger.info("更新押品信息表 结束");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new BizException(null, "", null, e.getMessage());
        }
        return result;
    }

    public int updateGuarBizRel(GuarBizRelGuaranteeDto guarBizRelGuaranteeDto) {
        int result;
        try {
            String serno = guarBizRelGuaranteeDto.getPkId();
            BigDecimal GuaranteeAmt = new BigDecimal(guarBizRelGuaranteeDto.getGuarAmt());
            String cusId = guarBizRelGuaranteeDto.getCusId();
            String cusName = guarBizRelGuaranteeDto.getAssureName();
            String CertCode = guarBizRelGuaranteeDto.getAssureCertCode();
            String certType = guarBizRelGuaranteeDto.getCerType();
            String workSpace = guarBizRelGuaranteeDto.getBusiness();
            String marry = guarBizRelGuaranteeDto.getMarry();
            String cusTyp = guarBizRelGuaranteeDto.getCusTyp();
            String iqp_serno = guarBizRelGuaranteeDto.getSerno();
            String guarantyId = guarBizRelGuaranteeDto.getGuarantyId();
            String guarantyType = guarBizRelGuaranteeDto.getGuarantyType();
            String isAddGuar = guarBizRelGuaranteeDto.getIsAddGuar();
            GuarBizRel guarBizRel = new GuarBizRel();
            guarBizRel.setPkId(serno);
            //业务流水号
            guarBizRel.setSerno(iqp_serno);
            //押品编号
            guarBizRel.setGuarNo(guarBizRelGuaranteeDto.getGuarantyId());
            guarBizRel.setIsAddGuar(isAddGuar);
            guarBizRel.setOprType("01");
            result = guarBizRelMapper.updateByPrimaryKeySelective(guarBizRel);
            GuarGuarantee guarGuarantee = guarGuaranteeService.selectByPrimaryKey(guarantyId);
            guarGuarantee.setGuarantyType(guarantyType);
            guarGuaranteeService.updateSelective(guarGuarantee);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return result;
    }

    /**
     * @param guarBizRelGuarBaseDto
     * @return Integer
     * @author wzy
     * @date 2021/4/29 16:42
     * @version 1.0.0
     * @desc 根据业务流水号保存抵质押物关联表
     * @修改历史: V1.0
     */
    public int updateGuarBizRelGuarBase(GuarBizRelGuarBaseDto guarBizRelGuarBaseDto) {
        int result = 1;
        //抵押物流水号
        String serno = guarBizRelGuarBaseDto.getSerno();
        //抵质押编号
        String guarNo = guarBizRelGuarBaseDto.getGuarNo();
        //抵/质押物总价（元）
        String maxMortagageAmt = guarBizRelGuarBaseDto.getMaxMortagageAmt();
        //抵/质押率（%）
        String mortagageRate = guarBizRelGuarBaseDto.getMortagageRate();
        try {
            String updDate = guarBizRelGuarBaseDto.getUpdDate();
            GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
            guarBaseInfo.setSerno(serno);
            guarBaseInfo.setGuarNo(guarNo);
            //guarBaseInfo.setMortagageRate(new BigDecimal(mortagageRate));
            //guarBaseInfo.setMaxMortagageAmt(new BigDecimal(maxMortagageAmt));
            result = guarBaseInfoService.updateSelective(guarBaseInfo);
            Asserts.isTrue(result > 0, "更新关联押品信息表");
            logger.info("更新关联押品信息表");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("新增业务关联押品信息表结束..............");
        }
        return result;
    }

    /**
     * @方法名称: riskItem0018
     * @方法描述: 集群名单校验
     * @参数与返回说明:
     * @算法描述: 在授信的时候，如果选择产品属性为集群贷的时候，并选择合作方时，判断该合作方是否存在白名单管控，如果存在则校验是否客户在该合作方的白名单内
     * @创建人: cainingbo
     * @创建时间: 2021-06-23 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0018(String serno, String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        Map<String, String> map = new HashMap<>();
        map.put("serno", serno);
        map.put("oprType", "01");
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByParams(map);
        if (Objects.isNull(lmtAppSubList) || lmtAppSubList.size() == 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        QueryModel model = new QueryModel();
        for (LmtAppSub lmtAppSub : lmtAppSubList) {
            List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
            if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                    // P019 : 集群贷
                    if (Objects.equals("P019", lmtAppSubPrd.getLmtBizTypeProp())) {
                        if (StringUtils.isBlank(lmtAppSubPrd.getPartnerNo())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1803);
                            return riskResultDto;
                        }
                        model.addCondition("partnerNo", lmtAppSubPrd.getPartnerNo());
                        List<CoopPlanAccInfo> coopPlanAccInfoList = coopPlanAccInfoService.selectByModel(model);
                        if (CollectionUtils.isEmpty(coopPlanAccInfoList)) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1804);
                            return riskResultDto;
                        }
                        // 是否白名单控制
                        if (Objects.equals(CmisCommonConstants.YES_NO_1, coopPlanAccInfoList.get(0).getIsWhiteListCtrl())) {
                            //查询此客户是否在集群合作方白名单中
                            model.addCondition("cusId", lmtAppSubPrd.getCusId());
                            model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                            List<CoopColonyWhiteLst> coopColonyWhiteLstList = coopColonyWhiteLstService.selectByModel(model);
                            if (CollectionUtils.isEmpty(coopColonyWhiteLstList)) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1801);
                                return riskResultDto;
                            }
                        }
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0005);
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: queryGuarBizRelDataBySerno
     * @方法描述: 根据业务流水号查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarBizRel> queryGuarBizRelDataBySerno(String serno) {
        return guarBizRelMapper.queryGuarBizRelDataBySerno(serno);
    }

    /**
     * @方法名称: queryMainGuarBizRelDataBySerno
     * @方法描述: 根据业务流水号查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarBizRel> queryMainGuarBizRelDataBySerno(Map map) {
        return guarBizRelMapper.queryMainGuarBizRelDataBySerno(map);
    }

    /**
     * @方法名称: copyLmtAppGuarRel
     * @方法描述: 复议，复审，变更时将原担保关系挂靠在新的授信分项流水号下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyLmtAppGuarRel(String originSubSerno, String newSubSerno) {
        GuarBizRel guarBizRel = null;
        QueryModel model = new QueryModel();
        model.addCondition("serno", originSubSerno);
        if (!StringUtils.isBlank(originSubSerno)) {
            List<GuarBizRel> list = guarBizRelMapper.selectByModel(model);
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    guarBizRel = list.get(i);
                    guarBizRel.setPkId(UUID.randomUUID().toString());
                    guarBizRel.setSerno(newSubSerno);
                    int count = guarBizRelMapper.insert(guarBizRel);
                    if (count != 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * @方法名称: queryGuarBizRelDataBySernoAndGuarNo
     * @方法描述: 根据传入的流水号以及押品编号查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarBizRel queryGuarBizRelDataBySernoAndGuarNo(Map map) {
        return guarBizRelMapper.queryGuarBizRelDataBySernoAndGuarNo(map);
    }

    /**
     * 根据押品编号查询用信时该押品是否被授信项下的担保合同所引用
     * @param model
     * @return
     */
    public String checkGuarNoIsExist(QueryModel model) {
        //授信额度编号
        String lmtAccNo = (String) model.getCondition().get("lmtAccNo");
        //担保方式
        String guarWay = (String) model.getCondition().get("guarWay");
        //押品编号
        String guarNo = (String) model.getCondition().get("guarNo");
        //是否追加担保
        String isAddGuar = (String) model.getCondition().get("isAddGuar");

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("lmtAccNo", lmtAccNo);
        queryModel.addCondition("isSuperaddGuar", isAddGuar);

        if ("10".equals(guarWay) || "20".equals(guarWay) || "21".equals(guarWay)) {
            String guarNos = "";
            queryModel.addCondition("guarWay", guarWay);
            //根据授信额度编号查询关联的所有非否决非自行退出的合同编号
            String contNos = ctrLoanContService.selectContNosByLmtAccNo(lmtAccNo);

            if (!StringUtils.isEmpty(contNos)) {
                //查询合同编号对应的担保合同编号
                String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNos(contNos);

                if (!StringUtils.isEmpty(guarContNos)) {
                    //查询授信额度分项关联担保合同下的押品编号
                    queryModel.addCondition("guarContNos", guarContNos);
                    guarNos = guarBaseInfoService.selectGuarNosByLmtAccNo(queryModel);
                }
            }

            if (StringUtils.isEmpty(guarNos) || !guarNos.contains(guarNo)) {
                return "notExist";
            }
        } else {
            //保证人id
            String guarantyIds = "";
            //根据授信额度编号查询关联的所有非否决非自行退出的合同编号
            String contNos = ctrLoanContService.selectContNosByLmtAccNo(lmtAccNo);

            if (!StringUtils.isEmpty(contNos)) {
                //查询合同编号对应的担保合同编号
                String guarContNos = grtGuarBizRstRelService.selectGuarContNosByContNos(contNos);

                if (!StringUtils.isEmpty(guarContNos)) {
                    //查询授信额度分项关联担保合同下的押品编号
                    queryModel.addCondition("guarContNos", guarContNos);
                    //根据授信分项编号查询担保合同关联的保证人id
                    guarantyIds = guarGuaranteeService.selectGuarantyIdsByLmtAccNo(queryModel);
                }
            }

            if (StringUtils.isEmpty(guarantyIds) || !guarantyIds.contains(guarNo)) {
                return "notExist";
            }
        }
        return "exist";
    }

    /**
     * 查询押品编号
     * @param queryModel
     * @return
     */
    public String selectGuarNosByModel(QueryModel queryModel){
        return guarBizRelMapper.selectGuarNosByModel(queryModel);
    }

    /**
     * 根据流水号和押品编号查询对应融资金额总和
     * @param queryModel
     * @return
     */
    public BigDecimal selectTotalCorreFinAmtByModel(QueryModel queryModel){
        return guarBizRelMapper.selectTotalCorreFinAmtByModel(queryModel);
    }
}