/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtAppCorreShd;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014StockHolderListRespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppCorreShdMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppCorreShdService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-27 16:36:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppCorreShdService extends BizInvestCommonService{

    @Autowired
    private LmtAppCorreShdMapper lmtAppCorreShdMapper;

    @Autowired
    private ICusClientService cusClientService;

    @Autowired
    private SequenceTemplateService sequenceTemplateService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppCorreShd selectByPrimaryKey(String pkId) {
        return lmtAppCorreShdMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppCorreShd> selectAll(QueryModel model) {
        List<LmtAppCorreShd> records = (List<LmtAppCorreShd>) lmtAppCorreShdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppCorreShd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppCorreShd> list = lmtAppCorreShdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppCorreShd record) {
        return lmtAppCorreShdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppCorreShd record) {
        return lmtAppCorreShdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAppCorreShd record) {
        return lmtAppCorreShdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppCorreShd record) {
        return lmtAppCorreShdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppCorreShdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppCorreShdMapper.deleteByIds(ids);
    }

    /**
     * 保存同业机构准入申请（企业基本信息-申请）
     * @param appSerno
     * @param cusId
     */
    public void insertLmtAppCorreShdByIntbankOrgAdmitAppApply(String appSerno, String cusId) {
        CusBaseClientDto cusBaseClientDto = cusClientService.queryCus(cusId);
        if (cusBaseClientDto != null){
            LmtAppCorreShd lmtAppCorreShd = new LmtAppCorreShd();
            lmtAppCorreShd.setPkId(generatePkId());
            lmtAppCorreShd.setShdCusId(cusId);
            lmtAppCorreShd.setShdCusName(cusBaseClientDto.getCusName());
        }
    }

    /**
     * 逻辑删除股东
     * @param pkId
     * @return
     */
    public int deleteLogicByPkId(String pkId) {
        LmtAppCorreShd lmtAppCorreShd = selectByPrimaryKey(pkId);
        if (lmtAppCorreShd!=null){
            lmtAppCorreShd.setOprType(CmisBizConstants.OPR_TYPE_02);
            return update(lmtAppCorreShd);
        }else{
            throw BizException.error(null, EclEnum.LMT_SIG_INVESTAPP_CORRE_SHD_DEL_FAILED.key,EclEnum.LMT_SIG_INVESTAPP_CORRE_SHD_DEL_FAILED.value);
        }
    }

    /**
     * 法人客户股东信息添加
     * @param serno
     * @param cusId
     * @param respDto
     * @return
     */
    public int insertLmtAppCorreShdBy0014(String serno, String cusId, CmisCus0014StockHolderListRespDto respDto) {
        LmtAppCorreShd lmtAppCorreShd = new LmtAppCorreShd();
        lmtAppCorreShd.setPkId(generatePkId());
        lmtAppCorreShd.setSerno(serno);
        lmtAppCorreShd.setCusId(cusId);
        lmtAppCorreShd.setShdCusId(respDto.getCertCode());
        lmtAppCorreShd.setShdCusName(respDto.getInvtName());
        //出资金额
        lmtAppCorreShd.setShdAmt(respDto.getInvtAmt());
        //出资比例
        lmtAppCorreShd.setShdPerc(respDto.getInvtPerc());
        //备注
        lmtAppCorreShd.setRemark(respDto.getRemark());
        //出资人性质
        lmtAppCorreShd.setCorpCha(respDto.getInvtTyp());
        lmtAppCorreShd.setInputBrId(getCurrentUser().getOrg().getCode());
        lmtAppCorreShd.setInputDate(getCurrrentDateStr());
        lmtAppCorreShd.setInputId(getCurrentUser().getLoginCode());
        lmtAppCorreShd.setCreateTime(getCurrrentDate());
        lmtAppCorreShd.setUpdateTime(getCurrrentDate());
        lmtAppCorreShd.setUpdBrId(getCurrentUser().getOrg().getCode());
        lmtAppCorreShd.setUpdId(getCurrentUser().getLoginCode());
        lmtAppCorreShd.setUpdDate(getCurrrentDateStr());
        lmtAppCorreShd.setOprType(CmisBizConstants.OPR_TYPE_01);
        return insert(lmtAppCorreShd);
    }

    /**
     * 添加客户股东信息（同业）
     * @param serno
     * @param cusId
     * @param cusCorpApitalDto
     * @return
     */
    public int insertLmtAppCorreShdBy0010(String serno, String cusId, CusCorpApitalDto cusCorpApitalDto) {
        LmtAppCorreShd lmtAppCorreShd = new LmtAppCorreShd();
        lmtAppCorreShd.setPkId(generatePkId());
        lmtAppCorreShd.setSerno(serno);
        lmtAppCorreShd.setCusId(cusId);
        //关联股东客户编号
        lmtAppCorreShd.setShdCusId(cusCorpApitalDto.getCusId());
        //关联股东客户名称
        lmtAppCorreShd.setShdCusName(cusCorpApitalDto.getInvtName());
        //出资金额
        lmtAppCorreShd.setShdAmt(cusCorpApitalDto.getInvtAmt());
        //出资比例
        lmtAppCorreShd.setShdPerc(cusCorpApitalDto.getInvtPerc());
        //出资人性质 STD_ZB_INVY_TYP
        lmtAppCorreShd.setCorpCha(cusCorpApitalDto.getInvtTyp());
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtAppCorreShd);
        return insert(lmtAppCorreShd);
    }
}
