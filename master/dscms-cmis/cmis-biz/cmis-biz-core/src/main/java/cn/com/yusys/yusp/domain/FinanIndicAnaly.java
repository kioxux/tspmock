/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FinanIndicAnaly
 * @类描述: finan_indic_analy数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 16:12:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "finan_indic_analy")
public class FinanIndicAnaly extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 项目编号 **/
	@Column(name = "ITEM_ID", unique = false, nullable = false, length = 40)
	private String itemId;
	
	/** 项目名称 **/
	@Column(name = "ITEM_NAME", unique = false, nullable = false, length = 200)
	private String itemName;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 40)
	private String inputYear;
	
	/** 当前年月数值 **/
	@Column(name = "CUR_YM_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYmValue;
	
	/** 前一年数值 **/
	@Column(name = "NEAR_FIRST_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstValue;
	
	/** 前二年数值 **/
	@Column(name = "NEAR_SECOND_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondValue;
	
	/** 财务指标分组序列 **/
	@Column(name = "FINAN_INDIC_GROUP", unique = false, nullable = true, length = 5)
	private String finanIndicGroup;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
    /**
     * @return itemId
     */
	public String getItemId() {
		return this.itemId;
	}
	
	/**
	 * @param itemName
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
    /**
     * @return itemName
     */
	public String getItemName() {
		return this.itemName;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param curYmValue
	 */
	public void setCurYmValue(java.math.BigDecimal curYmValue) {
		this.curYmValue = curYmValue;
	}
	
    /**
     * @return curYmValue
     */
	public java.math.BigDecimal getCurYmValue() {
		return this.curYmValue;
	}
	
	/**
	 * @param nearFirstValue
	 */
	public void setNearFirstValue(java.math.BigDecimal nearFirstValue) {
		this.nearFirstValue = nearFirstValue;
	}
	
    /**
     * @return nearFirstValue
     */
	public java.math.BigDecimal getNearFirstValue() {
		return this.nearFirstValue;
	}
	
	/**
	 * @param nearSecondValue
	 */
	public void setNearSecondValue(java.math.BigDecimal nearSecondValue) {
		this.nearSecondValue = nearSecondValue;
	}
	
    /**
     * @return nearSecondValue
     */
	public java.math.BigDecimal getNearSecondValue() {
		return this.nearSecondValue;
	}
	
	/**
	 * @param finanIndicGroup
	 */
	public void setFinanIndicGroup(String finanIndicGroup) {
		this.finanIndicGroup = finanIndicGroup;
	}
	
    /**
     * @return finanIndicGroup
     */
	public String getFinanIndicGroup() {
		return this.finanIndicGroup;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}