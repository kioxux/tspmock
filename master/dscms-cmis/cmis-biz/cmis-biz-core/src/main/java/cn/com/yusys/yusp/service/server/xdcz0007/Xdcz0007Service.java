package cn.com.yusys.yusp.service.server.xdcz0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.domain.CtrCvrgCont;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.CtrTfLocCont;
import cn.com.yusys.yusp.dto.CfgTfRateDto;
import cn.com.yusys.yusp.dto.CfgTfRateQueryDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.req.Xdcz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.resp.Xdcz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.AccTfLocMapper;
import cn.com.yusys.yusp.repository.mapper.BailDepositInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrTfLocContMapper;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Service
public class Xdcz0007Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0007Service.class);

    @Autowired
    private AccTfLocMapper accTfLocMapper;
    @Autowired
    private CtrTfLocContMapper ctrTfLocContMapper;
    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AccCvrsMapper accCvrsMapper;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private BailDepositInfoMapper bailDepositInfoMapper;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 交易码：Xdcz0007
     * 交易描述：查询敞口额度及保证金校验
     *
     * @param xdcz0007DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0007DataRespDto xdcz0007(Xdcz0007DataReqDto xdcz0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key,
                DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataReqDto));
        //定义返回信息
        Xdcz0007DataRespDto xdcz0007DataRespDto = new Xdcz0007DataRespDto();
        //得到请求字段
        String bizType = xdcz0007DataReqDto.getBizType();//业务类型
        String oprtype = xdcz0007DataReqDto.getOprtype();//操作类型
        String contNo = xdcz0007DataReqDto.getContNo();//合同号
        String busiClass = xdcz0007DataReqDto.getBusiClass();// 业务细分
        BigDecimal loanAmt = Optional.ofNullable(xdcz0007DataReqDto.getLoanAmt()).orElse(BigDecimal.ZERO);//放款金额
        BigDecimal loanAmtExchgRat = Optional.ofNullable(xdcz0007DataReqDto.getLoanAmtExchgRat()).orElse(BigDecimal.ZERO);//放款金额汇率
        List<cn.com.yusys.yusp.dto.server.xdcz0007.req.List> lists = xdcz0007DataReqDto.getList();
        Map param = new HashMap<>();
        if (!Objects.isNull(busiClass)) {
            param.put("prdId", busiClass);
            if (Objects.equals(busiClass, "412265")) {
                // 国际信用证开立
                param.put("bizType", "11010101");
            } else if (Objects.equals(busiClass, "410002")) {
                // 国内信用证开立
                param.put("bizType", "11020101");
            } else if (Objects.equals(busiClass, "042062")) {
                // 非融资性保函（国内）
                param.put("bizType", "13010101");
            } else if (Objects.equals(busiClass, "410003")) {
                // 非融资性保函（国际）
                param.put("bizType", "13020101");
            } else if (Objects.equals(busiClass, "610002")) {
                // 融资性保函（国内）
                param.put("bizType", "13010201");
            } else if (Objects.equals(busiClass, "610003")) {
                // 融资性保函（国际）
                param.put("bizType", "13020201");
            }
        }
        param.put("contNo", contNo);
        //业务处理开始
        try {
            //信用证
            if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_01, bizType)) {
                BigDecimal kyAmt = BigDecimal.ZERO;//可用额度
                //判断国结传过来的合同是否存在
                List<CtrTfLocCont> ctrTfLocContList = ctrTfLocContMapper.getContInfoByParam(param);
                CtrTfLocCont ctrTfLocCont = null;
                if (CollectionUtils.nonEmpty(ctrTfLocContList)) {
                    ctrTfLocCont = ctrTfLocContList.get(0);
                }
                if (ctrTfLocCont == null) {
                    throw new BizException(null, "", null, "不存在相关合同信息");
                } else if ("01".equals(oprtype)) {
                    xdcz0007DataRespDto.setOrderBal("0");
                } else if ("02".equals(oprtype)) {
                    String contType = ctrTfLocCont.getContType();//合同类型
                    BigDecimal cvtCnyAmt = ctrTfLocCont.getCvtCnyAmt();
                    String curType = ctrTfLocCont.getCurType();//币种

                    BigDecimal bail = BigDecimal.ZERO;//保证金金额
                    String bailCurType = "";//币种
                    BigDecimal bailExchgRate = BigDecimal.ZERO;
                    BigDecimal sumBailAmt = BigDecimal.ZERO;//保证金金额之和
                    BigDecimal sumBailAmtOld = BigDecimal.ZERO;//原保证金金额之和
                    //获取保证金相关信息
                    if (lists != null && !lists.isEmpty()) {
                        for (int i = 0; i < lists.size(); i++) {
                            cn.com.yusys.yusp.dto.server.xdcz0007.req.List list = lists.get(i);
                            bail = list.getBail();
                            bailCurType = list.getCurType();
                            bailExchgRate = list.getBailExchgRate();//保证金汇率
                            if ("1".equals(contType) && !"".equals(curType) && curType.equals(bailCurType)) {
                                //如果合同为一般合同并且保证金的币种和合同的申请币种一致，则保证金的汇率为合同申请时的汇率
                                bailExchgRate = loanAmtExchgRat;
                            }
                            BigDecimal bailAmt = bail.multiply(bailExchgRate);//保证金金额
                            sumBailAmt = sumBailAmt.add(bailAmt);
                            sumBailAmtOld = sumBailAmtOld.add(bail);
                        }
                    }
                    if (loanAmt.compareTo(BigDecimal.ZERO) > 0 && bail.compareTo(BigDecimal.ZERO) > 0 && bailExchgRate.compareTo(BigDecimal.ZERO) > 0) {
                        //判断出账金额是否大于合同可用金额
                        if (loanAmtExchgRat.compareTo(BigDecimal.ZERO) < 1) {
                            loanAmtExchgRat = BigDecimal.valueOf(1);
                        }
                        //首先根据授信的保证金比例以及申请金额计算出应该交多少保证金
                        //调用额度系统成功,返回信息
                        BigDecimal security_rate = ctrTfLocCont.getBailPerc();
                        if (security_rate == null) {
                            String assureMeansMain = ctrTfLocCont.getGuarMode();
                            if ("40".equals(assureMeansMain)) {
                                security_rate = new BigDecimal(1);
                            } else {
                                security_rate = BigDecimal.ZERO;
                            }
                        }
                        BigDecimal shouldBzj = loanAmt.multiply(security_rate);//应交保证金
                        //首先根据合同号查找出对应的借据信息
                        BigDecimal accSecAmt = BigDecimal.ZERO;//借据中的保证金之和
                        BigDecimal sumbzj = BigDecimal.ZERO;//保证金台帐表保证金之和
                        BigDecimal accbalance = BigDecimal.ZERO;//借据中的贷款余额之和
                        java.util.List<AccTfLoc> accTfLocList = accTfLocMapper.getAccTfInfoByContNo(contNo);
                        if (accTfLocList != null && !accTfLocList.isEmpty()) {
                            for (AccTfLoc it : accTfLocList) {
                                BigDecimal cvtCnySpac = it.getCvtCnySpac();//折算人民币敞口
                                accbalance = accbalance.add(Optional.ofNullable(cvtCnySpac).orElse(BigDecimal.ZERO));
                            }
                        }
                        //信用证出账校验
                        //用申请放款金额减去保证金总额
                        BigDecimal cha = loanAmt.multiply(loanAmtExchgRat).subtract(sumBailAmt);
                        if (accbalance.compareTo(BigDecimal.ZERO) < 0) {
                            kyAmt = cvtCnyAmt;
                        } else {
                            kyAmt = cvtCnyAmt.subtract(accbalance);
                        }
                        //查找出合同关联的授信分项下的合同可用额度
                        BigDecimal crdLmt = ctrTfLocCont.getCvtCnyAmt();//cmisLmt0026RespDto.getSpacAmt();
                        String msg = "";
                        //如果保证金交的足够，判断申请的敞口是否小于合同的余额
                        if (shouldBzj.compareTo(sumBailAmtOld) == 0) {
                            if (cha.compareTo(kyAmt) > 0) {
                                throw new BizException(null, "", null, "合同敞口余额不足！");
                            }
                        } else if (crdLmt.compareTo(BigDecimal.ZERO) > 0) {
                            if (cha.compareTo(crdLmt) > 0) {
                                throw new BizException(null, "", null, "申请金额大于授信分项的总额度");
                            }
                        }
                        //当放款金额大于合同可用余额时，需校验保证金是否足额
                        if (cha.compareTo(kyAmt) > 0) {
                            BigDecimal mustBzjAmt = loanAmt.multiply(loanAmtExchgRat).subtract(kyAmt);
                            if (sumBailAmt.compareTo(mustBzjAmt) < 0) {
                                throw new BizException(null, "", null, "申请的保证金金额不足");
                            }
                        }
                    }
                }
                //保函
            } else if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_02, bizType)) {
                BigDecimal kyAmt = BigDecimal.ZERO;//可用额度
                //判断国结传过来的合同是否存在
                CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.getCvrgContByParam(param);
                if (ctrCvrgCont == null) {
                    throw new BizException(null, "", null, "不存在相关合同信息");
                } else if ("01".equals(oprtype)) {
                    xdcz0007DataRespDto.setOrderBal("0");
                } else if ("02".equals(oprtype)) {
                    String contType = ctrCvrgCont.getContType();//合同类型
                    BigDecimal cvtCnyAmt = ctrCvrgCont.getCvtCnyAmt();
                    //折算人民币金额
                    //如果是一般合同，后续汇率已合同汇率来计算
                    if (!"2".equals(contType)) {
                        loanAmtExchgRat = StringUtils.isBlank(ctrCvrgCont.getExchangeRate()) ? new BigDecimal("1") : new BigDecimal(ctrCvrgCont.getExchangeRate());//汇率
                    }
                    String curType = ctrCvrgCont.getCurType();//币种

                    BigDecimal bail = BigDecimal.ZERO;//保证金金额
                    String bailCurType = "";//币种
                    BigDecimal bailExchgRate = BigDecimal.ZERO;
                    BigDecimal sumBailAmt = BigDecimal.ZERO;//保证金金额之和
                    BigDecimal sumBailAmtOld = BigDecimal.ZERO;//原保证金金额之和
                    //获取保证金相关信息
                    if (lists != null && !lists.isEmpty()) {
                        for (int i = 0; i < lists.size(); i++) {
                            cn.com.yusys.yusp.dto.server.xdcz0007.req.List list = lists.get(i);
                            bail = list.getBail();
                            bailCurType = list.getCurType();
                            bailExchgRate = list.getBailExchgRate();//保证金汇率
                            if ("1".equals(contType) && !"".equals(curType) && curType.equals(bailCurType)) {
                                //如果合同为一般合同并且保证金的币种和合同的申请币种一致，则保证金的汇率为合同申请时的汇率
                                bailExchgRate = loanAmtExchgRat;
                            }
                            BigDecimal bailAmt = bail.multiply(bailExchgRate);//保证金金额
                            sumBailAmt = sumBailAmt.add(bailAmt);
                            sumBailAmtOld = sumBailAmtOld.add(bail);
                        }
                    }
                    if (loanAmt.compareTo(BigDecimal.ZERO) > 0 && bail.compareTo(BigDecimal.ZERO) > 0 && bailExchgRate.compareTo(BigDecimal.ZERO) > 0) {
                        //判断出账金额是否大于合同可用金额
                        if (loanAmtExchgRat.compareTo(BigDecimal.ZERO) < 1) {
                            loanAmtExchgRat = BigDecimal.valueOf(1);
                        }
                        //首先根据授信的保证金比例以及申请金额计算出应该交多少保证金
                        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
                        cmisLmt0026ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
                        cmisLmt0026ReqDto.setSubSerno(ctrCvrgCont.getLmtAccNo());
                        cmisLmt0026ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
                        ResultDto<CmisLmt0026RespDto> cmisLmt0026ResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
                        //调用额度系统成功,返回信息
                        BigDecimal securityRate = BigDecimal.ZERO;
                        CmisLmt0026RespDto cmisLmt0026RespDto = new CmisLmt0026RespDto();
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0026ResultDto.getCode())) {
                            cmisLmt0026RespDto = cmisLmt0026ResultDto.getData();
                            securityRate = cmisLmt0026RespDto.getBailPreRate();
                        }
                        if (securityRate == null) {
                            String assureMeansMain = ctrCvrgCont.getGuarMode();
                            if ("40".equals(assureMeansMain)) {
                                securityRate = new BigDecimal(1);
                            } else {
                                securityRate = BigDecimal.ZERO;
                            }
                        }
                        BigDecimal shouldBzj = loanAmt.multiply(securityRate);//应交保证金
                        //首先根据合同号查找出对应的借据信息
                        BigDecimal accSecAmt = BigDecimal.ZERO;//借据中的保证金之和
                        BigDecimal sumbzj = BigDecimal.ZERO;//保证金台帐表保证金之和
                        BigDecimal accbalance = BigDecimal.ZERO;//借据中的贷款余额之和
                        List<AccCvrs> accCvrsList = accCvrsMapper.getAccCvrsInfoByContNo(contNo);
                        if (accCvrsList != null && !accCvrsList.isEmpty()) {
                            for (AccCvrs it : accCvrsList) {
                                BigDecimal exchangeRmbSpac = Optional.ofNullable(it.getExchangeRmbSpac()).orElse(BigDecimal.ZERO);
                                accbalance = accbalance.add(exchangeRmbSpac);
                            }
                        }
                        //保函出账校验
                        //用申请放款金额减去保证金总额
                        BigDecimal cha = loanAmt.multiply(loanAmtExchgRat).subtract(sumBailAmt);
                        if (accbalance.compareTo(BigDecimal.ZERO) < 0) {
                            kyAmt = cvtCnyAmt;
                        } else {
                            kyAmt = cvtCnyAmt.subtract(accbalance);
                        }
                        //查找出合同关联的授信分项下的合同可用额度
                        BigDecimal crdLmt = Optional.ofNullable(cmisLmt0026RespDto.getSpacAmt()).orElse(BigDecimal.ZERO);
                        String msg = "";
                        //如果保证金交的足够，判断申请的敞口是否小于合同的余额
                        if (shouldBzj.compareTo(sumBailAmtOld) == 0) {
                            if (cha.compareTo(kyAmt) > 0) {
                                throw new BizException(null, "", null, "合同敞口余额不足！");
                            }
                        } else if (crdLmt.compareTo(BigDecimal.ZERO) > 0) {
                            if (cha.compareTo(crdLmt) > 0) {
                                throw new BizException(null, "", null, "申请金额大于授信分项的总额度！");
                            }
                        }
                        //当放款金额大于合同可用余额时，需校验保证金是否足额
                        if (cha.compareTo(kyAmt) > 0) {
                            BigDecimal mustBzjAmt = loanAmt.multiply(loanAmtExchgRat).subtract(kyAmt);
                            if (sumBailAmt.compareTo(mustBzjAmt) < 0) {
                                throw new BizException(null, "", null, "申请的保证金金额不足！");
                            }
                        }
                    }
                }

                //贸易融资、福费廷
            } else if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_03, bizType) || Objects.equals(CommonConstance.CONT_QUERY_TYPE_04, bizType)) {
                BigDecimal kyAmt = BigDecimal.ZERO;//可用额度
                //判断国结传过来的合同是否存在
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.getCtrContByParam(param);
                if (ctrLoanCont == null) {
                    throw new BizException(null, "", null, "不存在相关合同信息");
                } else if ("01".equals(oprtype)) {
                    xdcz0007DataRespDto.setOrderBal("0");
                } else if ("02".equals(oprtype)) {
                    String contType = ctrLoanCont.getContType();//合同类型
                    BigDecimal cvtCnyAmt = ctrLoanCont.getContAmt();
                    //合同金额
                    //如果是一般合同，后续汇率已合同汇率来计算
                    if (!"2".equals(contType)) {
                        loanAmtExchgRat = ctrLoanCont.getContRate();//汇率
                    }
                    BigDecimal bail = BigDecimal.ZERO;//保证金金额
                    BigDecimal bailExchgRate = BigDecimal.ZERO;
                    BigDecimal sumBailAmt = BigDecimal.ZERO;//保证金金额之和
                    BigDecimal sumBailAmtOld = BigDecimal.ZERO;//原保证金金额之和
                    //获取保证金相关信息
                    if (lists != null && !lists.isEmpty()) {
                        for (int i = 0; i < lists.size(); i++) {
                            cn.com.yusys.yusp.dto.server.xdcz0007.req.List list = lists.get(i);
                            bail = list.getBail();
                            bailExchgRate = list.getBailExchgRate();//保证金汇率
                            BigDecimal bailAmt = bail.multiply(bailExchgRate);//保证金金额
                            sumBailAmt = sumBailAmt.add(bailAmt);
                            sumBailAmtOld = sumBailAmtOld.add(bail);
                        }
                    }
                    if (loanAmt.compareTo(BigDecimal.ZERO) > 0 && bail.compareTo(BigDecimal.ZERO) > 0 && bailExchgRate.compareTo(BigDecimal.ZERO) > 0) {
                        //判断出账金额是否大于合同可用金额
                        if (loanAmtExchgRat.compareTo(BigDecimal.ZERO) < 1) {
                            loanAmtExchgRat = BigDecimal.valueOf(1);
                        }
                        //首先根据授信的保证金比例以及申请金额计算出应该交多少保证金
                        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
                        cmisLmt0026ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);
                        cmisLmt0026ReqDto.setSubSerno(ctrLoanCont.getLmtAccNo());
                        ResultDto<CmisLmt0026RespDto> cmisLmt0026ResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
                        //调用额度系统成功,返回信息
                        BigDecimal securityRate = BigDecimal.ZERO;
                        CmisLmt0026RespDto cmisLmt0026RespDto = new CmisLmt0026RespDto();
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0026ResultDto.getCode())) {
                            cmisLmt0026RespDto = cmisLmt0026ResultDto.getData();
                            securityRate = cmisLmt0026RespDto.getBailPreRate();
                        }
                        if (securityRate == null) {
                            String assure_means_main = ctrLoanCont.getGuarWay();
                            if ("40".equals(assure_means_main)) {
                                securityRate = new BigDecimal(1);
                            } else {
                                securityRate = BigDecimal.ZERO;
                            }
                        }
                        //判断申请金额是否大于合同余额
                        //获取合同下所有借据的余额
                        BigDecimal balance = BigDecimal.ZERO;
                        BigDecimal pvpbalance = BigDecimal.ZERO;
                        List<AccLoan> accLoanList = accLoanMapper.selectRateAccLoanByContNo(contNo);
                        if (accLoanList != null && !accLoanList.isEmpty()) {
                            for (AccLoan accLoan : accLoanList) {
                                String billNo = accLoan.getBillNo();
                                BigDecimal accrate = accLoan.getExchangeRate();
                                String curType = accLoan.getContCurType();
                                if (accrate == null && accrate.compareTo(BigDecimal.ZERO) < 1) {

                                    CfgTfRateQueryDto cfgTfRateQueryDto = new CfgTfRateQueryDto();
                                    cfgTfRateQueryDto.setCurType(curType);
                                    ResultDto<CfgTfRateDto> cfgTfRateDtos = iCmisCfgClientService.queryCfgTfRate(cfgTfRateQueryDto);
                                    //调用额度系统成功,返回信息
                                    BigDecimal rate = BigDecimal.ZERO;
                                    CfgTfRateDto cfgTfRateDto = new CfgTfRateDto();
                                    if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0026ResultDto.getCode())) {
                                        cfgTfRateDto = cfgTfRateDtos.getData();
                                        rate = cfgTfRateDto.getRate();
                                    }

                                    if (rate == null) {
                                        rate = BigDecimal.ZERO;
                                    }
                                    BigDecimal accpvpbalance = accLoan.getLoanBalance().multiply(rate);
                                    pvpbalance = pvpbalance.add(accpvpbalance);
                                } else {
                                    BigDecimal loanbalance = accLoanMapper.selectAccLoanAmtByBillNo(billNo);
                                    balance = balance.add(loanbalance);
                                }
                            }
                        }
                        String msg = "";
                        BigDecimal cha = cvtCnyAmt.subtract(balance).subtract(pvpbalance);
                        BigDecimal applyAmt = loanAmt.multiply(loanAmtExchgRat);
                        if (cha.compareTo(BigDecimal.ZERO) < 0) {
                            throw new BizException(null, "", null, "合同可用余额不足！");
                        } else if (applyAmt.compareTo(cha) > 0) {
                            throw new BizException(null, "", null, "申请金额大于合同可用余额！");
                        }
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key,
                DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataRespDto));
        return xdcz0007DataRespDto;
    }
}