/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdInfo
 * @类描述: lmt_sig_invest_prod_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-16 14:48:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_prod_info")
public class LmtSigInvestProdInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 运作方式 **/
	@Column(name = "OPER_MODE", unique = false, nullable = true, length = 200)
	private String operMode;
	
	/** 投资范围 **/
	@Column(name = "INVEST_SCP", unique = false, nullable = true, length = 2000)
	private String investScp;
	
	/** 投资限制 **/
	@Column(name = "INVEST_LIMIT", unique = false, nullable = true, length = 2000)
	private String investLimit;
	
	/** 投资策略 **/
	@Column(name = "INVEST_STRATY", unique = false, nullable = true, length = 2000)
	private String investStraty;
	
	/** 其他说明事项 **/
	@Column(name = "OTHER_DESC_CONTENT", unique = false, nullable = true, length = 65535)
	private String otherDescContent;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String otherDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param operMode
	 */
	public void setOperMode(String operMode) {
		this.operMode = operMode;
	}
	
    /**
     * @return operMode
     */
	public String getOperMode() {
		return this.operMode;
	}
	
	/**
	 * @param investScp
	 */
	public void setInvestScp(String investScp) {
		this.investScp = investScp;
	}
	
    /**
     * @return investScp
     */
	public String getInvestScp() {
		return this.investScp;
	}
	
	/**
	 * @param investLimit
	 */
	public void setInvestLimit(String investLimit) {
		this.investLimit = investLimit;
	}
	
    /**
     * @return investLimit
     */
	public String getInvestLimit() {
		return this.investLimit;
	}
	
	/**
	 * @param investStraty
	 */
	public void setInvestStraty(String investStraty) {
		this.investStraty = investStraty;
	}
	
    /**
     * @return investStraty
     */
	public String getInvestStraty() {
		return this.investStraty;
	}
	
	/**
	 * @param otherDescContent
	 */
	public void setOtherDescContent(String otherDescContent) {
		this.otherDescContent = otherDescContent;
	}
	
    /**
     * @return otherDescContent
     */
	public String getOtherDescContent() {
		return this.otherDescContent;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}