package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtModelApprResultInfo
 * @类描述: lmt_model_appr_result_info数据实体类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-19 10:41:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtModelApprResultInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 模型审批时间 **/
	private String modelApprTime;
	
	/** 模型评级 **/
	private String modelGrade;
	
	/** 模型得分 **/
	private String modelScore;
	
	/** 模型金额 **/
	private java.math.BigDecimal modelAmt;
	
	/** 模型利率 **/
	private java.math.BigDecimal modelRate;
	
	/** 司法案件判断 **/
	private String jojc;
	
	/** 模型结果状态 **/
	private String modelRstStatus;
	
	/** 业务唯一编号 **/
	private String bizUniqueNo;
	
	/** 模型意见 **/
	private String modelAdvice;
	
	/** 模型初始额度 **/
	private java.math.BigDecimal modelFstLmt;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param modelApprTime
	 */
	public void setModelApprTime(String modelApprTime) {
		this.modelApprTime = modelApprTime == null ? null : modelApprTime.trim();
	}
	
    /**
     * @return ModelApprTime
     */	
	public String getModelApprTime() {
		return this.modelApprTime;
	}
	
	/**
	 * @param modelGrade
	 */
	public void setModelGrade(String modelGrade) {
		this.modelGrade = modelGrade == null ? null : modelGrade.trim();
	}
	
    /**
     * @return ModelGrade
     */	
	public String getModelGrade() {
		return this.modelGrade;
	}
	
	/**
	 * @param modelScore
	 */
	public void setModelScore(String modelScore) {
		this.modelScore = modelScore == null ? null : modelScore.trim();
	}
	
    /**
     * @return ModelScore
     */	
	public String getModelScore() {
		return this.modelScore;
	}
	
	/**
	 * @param modelAmt
	 */
	public void setModelAmt(java.math.BigDecimal modelAmt) {
		this.modelAmt = modelAmt;
	}
	
    /**
     * @return ModelAmt
     */	
	public java.math.BigDecimal getModelAmt() {
		return this.modelAmt;
	}
	
	/**
	 * @param modelRate
	 */
	public void setModelRate(java.math.BigDecimal modelRate) {
		this.modelRate = modelRate;
	}
	
    /**
     * @return ModelRate
     */	
	public java.math.BigDecimal getModelRate() {
		return this.modelRate;
	}
	
	/**
	 * @param jojc
	 */
	public void setJojc(String jojc) {
		this.jojc = jojc == null ? null : jojc.trim();
	}
	
    /**
     * @return Jojc
     */	
	public String getJojc() {
		return this.jojc;
	}
	
	/**
	 * @param modelRstStatus
	 */
	public void setModelRstStatus(String modelRstStatus) {
		this.modelRstStatus = modelRstStatus == null ? null : modelRstStatus.trim();
	}
	
    /**
     * @return ModelRstStatus
     */	
	public String getModelRstStatus() {
		return this.modelRstStatus;
	}
	
	/**
	 * @param bizUniqueNo
	 */
	public void setBizUniqueNo(String bizUniqueNo) {
		this.bizUniqueNo = bizUniqueNo == null ? null : bizUniqueNo.trim();
	}
	
    /**
     * @return BizUniqueNo
     */	
	public String getBizUniqueNo() {
		return this.bizUniqueNo;
	}
	
	/**
	 * @param modelAdvice
	 */
	public void setModelAdvice(String modelAdvice) {
		this.modelAdvice = modelAdvice == null ? null : modelAdvice.trim();
	}
	
    /**
     * @return ModelAdvice
     */	
	public String getModelAdvice() {
		return this.modelAdvice;
	}
	
	/**
	 * @param modelFstLmt
	 */
	public void setModelFstLmt(java.math.BigDecimal modelFstLmt) {
		this.modelFstLmt = modelFstLmt;
	}
	
    /**
     * @return ModelFstLmt
     */	
	public java.math.BigDecimal getModelFstLmt() {
		return this.modelFstLmt;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}