/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSitu
 * @类描述: rpt_fnc_situ数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-18 18:54:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_fnc_situ")
public class RptFncSitu extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 采集年月 **/
	@Column(name = "ACQUISITION_DATE", unique = false, nullable = true, length = 10)
	private String acquisitionDate;
	
	/** 资产负债类型 **/
	@Column(name = "FNC_RPT_TYPE", unique = false, nullable = true, length = 5)
	private String fncRptType;
	
	/** 最近两年末经营活动现金净流量 **/
	@Column(name = "LAST_TWO_YEAR_NCFO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearNcfo;
	
	/** 最近两年末投资活动现金净流量 **/
	@Column(name = "LAST_TWO_YEAR_NCFIA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearNcfia;
	
	/** 最近两年末筹资活动现金净流量 **/
	@Column(name = "LAST_TWO_YEAR_NCFFA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearNcffa;
	
	/** 最近一年末经营活动现金净流量 **/
	@Column(name = "LAST_YEAR_NCFO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearNcfo;
	
	/** 最近一年末投资活动现金净流量 **/
	@Column(name = "LAST_YEAR_NCFIA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearNcfia;
	
	/** 最近一年末筹资活动现金净流量 **/
	@Column(name = "LAST_YEAR_NCFFA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearNcffa;
	
	/** 当前年经营活动现金净流量 **/
	@Column(name = "CUR_YEAR_NCFO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearNcfo;
	
	/** 当前年投资活动现金净流量 **/
	@Column(name = "CUR_YEAR_NCFIA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearNcfia;
	
	/** 当前年筹资活动现金净流量 **/
	@Column(name = "CUR_YEAR_NCFFA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearNcffa;
	
	/** 经营活动现金净流量备注 **/
	@Column(name = "NCFO_REMARK", unique = false, nullable = true, length = 65535)
	private String ncfoRemark;
	
	/** 投资活动现金净流量备注 **/
	@Column(name = "NCFIA_REMARK", unique = false, nullable = true, length = 65535)
	private String ncfiaRemark;
	
	/** 筹资活动现金净流量备注 **/
	@Column(name = "NCFFA_REMARK", unique = false, nullable = true, length = 65535)
	private String ncffaRemark;
	
	/** 应纳增值税率 **/
	@Column(name = "PAYABLE_VAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payableVatRate;
	
	/** 已缴增值税率 **/
	@Column(name = "PAID_VAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidVatRate;
	
	/** 出口退税增值税率 **/
	@Column(name = "EXPORT_TAX_VAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exportTaxVatRate;
	
	/** 应纳所得税率 **/
	@Column(name = "PAYABLE_IT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payableItRate;
	
	/** 已缴所得税率 **/
	@Column(name = "PAID_IT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidItRate;
	
	/** 即征即退所得税率 **/
	@Column(name = "EXPORT_TAX_IT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exportTaxItRate;
	
	/** 最近第二年应纳增值税 **/
	@Column(name = "LAST_TWO_YEAR_PAYABLE_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearPayableVat;
	
	/** 最近第二年已缴增值税 **/
	@Column(name = "LAST_TWO_YEAR_PAID_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearPaidVat;
	
	/** 最近第二年出口退税增值税 **/
	@Column(name = "LAST_TWO_YEAR_EXPORT_TAX_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearExportTaxVat;
	
	/** 最近第二年应纳所得税 **/
	@Column(name = "LAST_TWO_YEAR_PAYABLE_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearPayableIt;
	
	/** 最近第二年已缴所得税 **/
	@Column(name = "LAST_TWO_YEAR_PAID_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearPaidIt;
	
	/** 最近第二年即征即退所得税 **/
	@Column(name = "LAST_TWO_YEAR_EXPORT_TAX_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearExportTaxIt;
	
	/** 最近第一年应纳增值税 **/
	@Column(name = "LAST_YEAR_PAYABLE_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPayableVat;
	
	/** 最近第一年已缴增值税 **/
	@Column(name = "LAST_YEAR_PAID_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPaidVat;
	
	/** 最近第一年出口退税增值税 **/
	@Column(name = "LAST_YEAR_EXPORT_TAX_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearExportTaxVat;
	
	/** 最近第一年应纳所得税 **/
	@Column(name = "LAST_YEAR_PAYABLE_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPayableIt;
	
	/** 最近第一年已缴所得税 **/
	@Column(name = "LAST_YEAR_PAID_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPaidIt;
	
	/** 最近第一年即征即退所得税 **/
	@Column(name = "LAST_YEAR_EXPORT_TAX_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearExportTaxIt;
	
	/** 当前年应纳增值税 **/
	@Column(name = "CUR_PAYABLE_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curPayableVat;
	
	/** 当前年已缴增值税 **/
	@Column(name = "CUR_PAID_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curPaidVat;
	
	/** 当前年出口退税增值税 **/
	@Column(name = "CUR_EXPORT_TAX_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curExportTaxVat;
	
	/** 当前年应纳所得税 **/
	@Column(name = "CUR_PAYABLE_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curPayableIt;
	
	/** 当前年已缴所得税 **/
	@Column(name = "CUR_PAID_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curPaidIt;
	
	/** 当前年即征即退所得税 **/
	@Column(name = "CUR_EXPORT_TAX_IT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curExportTaxIt;
	
	/** 应纳增值税备注 **/
	@Column(name = "PAYABLE_VAT_REMARK", unique = false, nullable = true, length = 65535)
	private String payableVatRemark;
	
	/** 已缴增值税备注 **/
	@Column(name = "PAID_VAT_REMARK", unique = false, nullable = true, length = 65535)
	private String paidVatRemark;
	
	/** 出口退税增值税备注 **/
	@Column(name = "EXPORT_TAX_VAT_REMARK", unique = false, nullable = true, length = 65535)
	private String exportTaxVatRemark;
	
	/** 应纳所得税备注 **/
	@Column(name = "PAYABLE_IT_REMARK", unique = false, nullable = true, length = 65535)
	private String payableItRemark;
	
	/** 已缴所得税备注 **/
	@Column(name = "PAID_IT_REMARK", unique = false, nullable = true, length = 65535)
	private String paidItRemark;
	
	/** 即征即退所得税备注 **/
	@Column(name = "EXPORT_TAX_IT_REMARK", unique = false, nullable = true, length = 65535)
	private String exportTaxItRemark;
	
	/** 最近两年末资产负债率 **/
	@Column(name = "LAST_TWO_YEAR_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearDebtRate;
	
	/** 最近两年末流动比率 **/
	@Column(name = "LAST_TWO_YEAR_CURRENT_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearCurrentRatio;
	
	/** 最近两年末速动比率 **/
	@Column(name = "LAST_TWO_YEAR_QUICK_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearQuickRatio;
	
	/** 最近两年末营运资金 **/
	@Column(name = "LAST_TWO_YEAR_WORK_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearWorkCap;
	
	/** 最近两年末应收账款周转率 **/
	@Column(name = "LAST_TWO_YEAR_ARVTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearArvto;
	
	/** 最近两年末存货周转率 **/
	@Column(name = "LAST_TWO_YEAR_ITTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearItto;
	
	/** 最近两年末总资产报酬率 **/
	@Column(name = "LAST_TWO_YEAR_ROTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearRota;
	
	/** 最近两年末净资产收益率 **/
	@Column(name = "LAST_TWO_YEAR_RONA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearRona;
	
	/** 最近两年末营业收入增长率 **/
	@Column(name = "LAST_TWO_YEAR_REVINR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearRevinr;
	
	/** 最近一年末资产负债率 **/
	@Column(name = "LAST_YEAR_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearDebtRate;
	
	/** 最近一年末流动比率 **/
	@Column(name = "LAST_YEAR_CURRENT_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearCurrentRatio;
	
	/** 最近一年末速动比率 **/
	@Column(name = "LAST_YEAR_QUICK_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearQuickRatio;
	
	/** 最近一年末营运资金 **/
	@Column(name = "LAST_YEAR_WORK_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearWorkCap;
	
	/** 最近一年末应收账款周转率 **/
	@Column(name = "LAST_YEAR_ARVTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearArvto;
	
	/** 最近一年末存货周转率 **/
	@Column(name = "LAST_YEAR_ITTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearItto;
	
	/** 最近一年末总资产报酬率 **/
	@Column(name = "LAST_YEAR_ROTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearRota;
	
	/** 最近一年末净资产收益率 **/
	@Column(name = "LAST_YEAR_RONA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearRona;
	
	/** 最近一年末营业收入增长率 **/
	@Column(name = "LAST_YEAR_REVINR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearRevinr;
	
	/** 当前年月资产负债率 **/
	@Column(name = "CUR_YEAR_DEBT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearDebtRate;
	
	/** 当前年月流动比率 **/
	@Column(name = "CUR_YEAR_CURRENT_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearCurrentRatio;
	
	/** 当前年月速动比率 **/
	@Column(name = "CUR_YEAR_QUICK_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearQuickRatio;
	
	/** 当前年月营运资金 **/
	@Column(name = "CUR_YEAR_WORK_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearWorkCap;
	
	/** 当前年月应收账款周转率 **/
	@Column(name = "CUR_YEAR_ARVTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearArvto;
	
	/** 当前年月存货周转率 **/
	@Column(name = "CUR_YEAR_ITTO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearItto;
	
	/** 当前年月总资产报酬率 **/
	@Column(name = "CUR_YEAR_ROTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearRota;
	
	/** 当前年月净资产收益率 **/
	@Column(name = "CUR_YEAR_RONA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearRona;
	
	/** 当前年月营业收入增长率 **/
	@Column(name = "CUR_YEAR_REVINR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curYearRevinr;
	
	/** 财务状况作总体陈述 **/
	@Column(name = "FNC_DECLARE", unique = false, nullable = true, length = 65535)
	private String fncDeclare;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param acquisitionDate
	 */
	public void setAcquisitionDate(String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}
	
    /**
     * @return acquisitionDate
     */
	public String getAcquisitionDate() {
		return this.acquisitionDate;
	}
	
	/**
	 * @param fncRptType
	 */
	public void setFncRptType(String fncRptType) {
		this.fncRptType = fncRptType;
	}
	
    /**
     * @return fncRptType
     */
	public String getFncRptType() {
		return this.fncRptType;
	}
	
	/**
	 * @param lastTwoYearNcfo
	 */
	public void setLastTwoYearNcfo(java.math.BigDecimal lastTwoYearNcfo) {
		this.lastTwoYearNcfo = lastTwoYearNcfo;
	}
	
    /**
     * @return lastTwoYearNcfo
     */
	public java.math.BigDecimal getLastTwoYearNcfo() {
		return this.lastTwoYearNcfo;
	}
	
	/**
	 * @param lastTwoYearNcfia
	 */
	public void setLastTwoYearNcfia(java.math.BigDecimal lastTwoYearNcfia) {
		this.lastTwoYearNcfia = lastTwoYearNcfia;
	}
	
    /**
     * @return lastTwoYearNcfia
     */
	public java.math.BigDecimal getLastTwoYearNcfia() {
		return this.lastTwoYearNcfia;
	}
	
	/**
	 * @param lastTwoYearNcffa
	 */
	public void setLastTwoYearNcffa(java.math.BigDecimal lastTwoYearNcffa) {
		this.lastTwoYearNcffa = lastTwoYearNcffa;
	}
	
    /**
     * @return lastTwoYearNcffa
     */
	public java.math.BigDecimal getLastTwoYearNcffa() {
		return this.lastTwoYearNcffa;
	}
	
	/**
	 * @param lastYearNcfo
	 */
	public void setLastYearNcfo(java.math.BigDecimal lastYearNcfo) {
		this.lastYearNcfo = lastYearNcfo;
	}
	
    /**
     * @return lastYearNcfo
     */
	public java.math.BigDecimal getLastYearNcfo() {
		return this.lastYearNcfo;
	}
	
	/**
	 * @param lastYearNcfia
	 */
	public void setLastYearNcfia(java.math.BigDecimal lastYearNcfia) {
		this.lastYearNcfia = lastYearNcfia;
	}
	
    /**
     * @return lastYearNcfia
     */
	public java.math.BigDecimal getLastYearNcfia() {
		return this.lastYearNcfia;
	}
	
	/**
	 * @param lastYearNcffa
	 */
	public void setLastYearNcffa(java.math.BigDecimal lastYearNcffa) {
		this.lastYearNcffa = lastYearNcffa;
	}
	
    /**
     * @return lastYearNcffa
     */
	public java.math.BigDecimal getLastYearNcffa() {
		return this.lastYearNcffa;
	}
	
	/**
	 * @param curYearNcfo
	 */
	public void setCurYearNcfo(java.math.BigDecimal curYearNcfo) {
		this.curYearNcfo = curYearNcfo;
	}
	
    /**
     * @return curYearNcfo
     */
	public java.math.BigDecimal getCurYearNcfo() {
		return this.curYearNcfo;
	}
	
	/**
	 * @param curYearNcfia
	 */
	public void setCurYearNcfia(java.math.BigDecimal curYearNcfia) {
		this.curYearNcfia = curYearNcfia;
	}
	
    /**
     * @return curYearNcfia
     */
	public java.math.BigDecimal getCurYearNcfia() {
		return this.curYearNcfia;
	}
	
	/**
	 * @param curYearNcffa
	 */
	public void setCurYearNcffa(java.math.BigDecimal curYearNcffa) {
		this.curYearNcffa = curYearNcffa;
	}
	
    /**
     * @return curYearNcffa
     */
	public java.math.BigDecimal getCurYearNcffa() {
		return this.curYearNcffa;
	}
	
	/**
	 * @param ncfoRemark
	 */
	public void setNcfoRemark(String ncfoRemark) {
		this.ncfoRemark = ncfoRemark;
	}
	
    /**
     * @return ncfoRemark
     */
	public String getNcfoRemark() {
		return this.ncfoRemark;
	}
	
	/**
	 * @param ncfiaRemark
	 */
	public void setNcfiaRemark(String ncfiaRemark) {
		this.ncfiaRemark = ncfiaRemark;
	}
	
    /**
     * @return ncfiaRemark
     */
	public String getNcfiaRemark() {
		return this.ncfiaRemark;
	}
	
	/**
	 * @param ncffaRemark
	 */
	public void setNcffaRemark(String ncffaRemark) {
		this.ncffaRemark = ncffaRemark;
	}
	
    /**
     * @return ncffaRemark
     */
	public String getNcffaRemark() {
		return this.ncffaRemark;
	}
	
	/**
	 * @param payableVatRate
	 */
	public void setPayableVatRate(java.math.BigDecimal payableVatRate) {
		this.payableVatRate = payableVatRate;
	}
	
    /**
     * @return payableVatRate
     */
	public java.math.BigDecimal getPayableVatRate() {
		return this.payableVatRate;
	}
	
	/**
	 * @param paidVatRate
	 */
	public void setPaidVatRate(java.math.BigDecimal paidVatRate) {
		this.paidVatRate = paidVatRate;
	}
	
    /**
     * @return paidVatRate
     */
	public java.math.BigDecimal getPaidVatRate() {
		return this.paidVatRate;
	}
	
	/**
	 * @param exportTaxVatRate
	 */
	public void setExportTaxVatRate(java.math.BigDecimal exportTaxVatRate) {
		this.exportTaxVatRate = exportTaxVatRate;
	}
	
    /**
     * @return exportTaxVatRate
     */
	public java.math.BigDecimal getExportTaxVatRate() {
		return this.exportTaxVatRate;
	}
	
	/**
	 * @param payableItRate
	 */
	public void setPayableItRate(java.math.BigDecimal payableItRate) {
		this.payableItRate = payableItRate;
	}
	
    /**
     * @return payableItRate
     */
	public java.math.BigDecimal getPayableItRate() {
		return this.payableItRate;
	}
	
	/**
	 * @param paidItRate
	 */
	public void setPaidItRate(java.math.BigDecimal paidItRate) {
		this.paidItRate = paidItRate;
	}
	
    /**
     * @return paidItRate
     */
	public java.math.BigDecimal getPaidItRate() {
		return this.paidItRate;
	}
	
	/**
	 * @param exportTaxItRate
	 */
	public void setExportTaxItRate(java.math.BigDecimal exportTaxItRate) {
		this.exportTaxItRate = exportTaxItRate;
	}
	
    /**
     * @return exportTaxItRate
     */
	public java.math.BigDecimal getExportTaxItRate() {
		return this.exportTaxItRate;
	}
	
	/**
	 * @param lastTwoYearPayableVat
	 */
	public void setLastTwoYearPayableVat(java.math.BigDecimal lastTwoYearPayableVat) {
		this.lastTwoYearPayableVat = lastTwoYearPayableVat;
	}
	
    /**
     * @return lastTwoYearPayableVat
     */
	public java.math.BigDecimal getLastTwoYearPayableVat() {
		return this.lastTwoYearPayableVat;
	}
	
	/**
	 * @param lastTwoYearPaidVat
	 */
	public void setLastTwoYearPaidVat(java.math.BigDecimal lastTwoYearPaidVat) {
		this.lastTwoYearPaidVat = lastTwoYearPaidVat;
	}
	
    /**
     * @return lastTwoYearPaidVat
     */
	public java.math.BigDecimal getLastTwoYearPaidVat() {
		return this.lastTwoYearPaidVat;
	}
	
	/**
	 * @param lastTwoYearExportTaxVat
	 */
	public void setLastTwoYearExportTaxVat(java.math.BigDecimal lastTwoYearExportTaxVat) {
		this.lastTwoYearExportTaxVat = lastTwoYearExportTaxVat;
	}
	
    /**
     * @return lastTwoYearExportTaxVat
     */
	public java.math.BigDecimal getLastTwoYearExportTaxVat() {
		return this.lastTwoYearExportTaxVat;
	}
	
	/**
	 * @param lastTwoYearPayableIt
	 */
	public void setLastTwoYearPayableIt(java.math.BigDecimal lastTwoYearPayableIt) {
		this.lastTwoYearPayableIt = lastTwoYearPayableIt;
	}
	
    /**
     * @return lastTwoYearPayableIt
     */
	public java.math.BigDecimal getLastTwoYearPayableIt() {
		return this.lastTwoYearPayableIt;
	}
	
	/**
	 * @param lastTwoYearPaidIt
	 */
	public void setLastTwoYearPaidIt(java.math.BigDecimal lastTwoYearPaidIt) {
		this.lastTwoYearPaidIt = lastTwoYearPaidIt;
	}
	
    /**
     * @return lastTwoYearPaidIt
     */
	public java.math.BigDecimal getLastTwoYearPaidIt() {
		return this.lastTwoYearPaidIt;
	}
	
	/**
	 * @param lastTwoYearExportTaxIt
	 */
	public void setLastTwoYearExportTaxIt(java.math.BigDecimal lastTwoYearExportTaxIt) {
		this.lastTwoYearExportTaxIt = lastTwoYearExportTaxIt;
	}
	
    /**
     * @return lastTwoYearExportTaxIt
     */
	public java.math.BigDecimal getLastTwoYearExportTaxIt() {
		return this.lastTwoYearExportTaxIt;
	}
	
	/**
	 * @param lastYearPayableVat
	 */
	public void setLastYearPayableVat(java.math.BigDecimal lastYearPayableVat) {
		this.lastYearPayableVat = lastYearPayableVat;
	}
	
    /**
     * @return lastYearPayableVat
     */
	public java.math.BigDecimal getLastYearPayableVat() {
		return this.lastYearPayableVat;
	}
	
	/**
	 * @param lastYearPaidVat
	 */
	public void setLastYearPaidVat(java.math.BigDecimal lastYearPaidVat) {
		this.lastYearPaidVat = lastYearPaidVat;
	}
	
    /**
     * @return lastYearPaidVat
     */
	public java.math.BigDecimal getLastYearPaidVat() {
		return this.lastYearPaidVat;
	}
	
	/**
	 * @param lastYearExportTaxVat
	 */
	public void setLastYearExportTaxVat(java.math.BigDecimal lastYearExportTaxVat) {
		this.lastYearExportTaxVat = lastYearExportTaxVat;
	}
	
    /**
     * @return lastYearExportTaxVat
     */
	public java.math.BigDecimal getLastYearExportTaxVat() {
		return this.lastYearExportTaxVat;
	}
	
	/**
	 * @param lastYearPayableIt
	 */
	public void setLastYearPayableIt(java.math.BigDecimal lastYearPayableIt) {
		this.lastYearPayableIt = lastYearPayableIt;
	}
	
    /**
     * @return lastYearPayableIt
     */
	public java.math.BigDecimal getLastYearPayableIt() {
		return this.lastYearPayableIt;
	}
	
	/**
	 * @param lastYearPaidIt
	 */
	public void setLastYearPaidIt(java.math.BigDecimal lastYearPaidIt) {
		this.lastYearPaidIt = lastYearPaidIt;
	}
	
    /**
     * @return lastYearPaidIt
     */
	public java.math.BigDecimal getLastYearPaidIt() {
		return this.lastYearPaidIt;
	}
	
	/**
	 * @param lastYearExportTaxIt
	 */
	public void setLastYearExportTaxIt(java.math.BigDecimal lastYearExportTaxIt) {
		this.lastYearExportTaxIt = lastYearExportTaxIt;
	}
	
    /**
     * @return lastYearExportTaxIt
     */
	public java.math.BigDecimal getLastYearExportTaxIt() {
		return this.lastYearExportTaxIt;
	}
	
	/**
	 * @param curPayableVat
	 */
	public void setCurPayableVat(java.math.BigDecimal curPayableVat) {
		this.curPayableVat = curPayableVat;
	}
	
    /**
     * @return curPayableVat
     */
	public java.math.BigDecimal getCurPayableVat() {
		return this.curPayableVat;
	}
	
	/**
	 * @param curPaidVat
	 */
	public void setCurPaidVat(java.math.BigDecimal curPaidVat) {
		this.curPaidVat = curPaidVat;
	}
	
    /**
     * @return curPaidVat
     */
	public java.math.BigDecimal getCurPaidVat() {
		return this.curPaidVat;
	}
	
	/**
	 * @param curExportTaxVat
	 */
	public void setCurExportTaxVat(java.math.BigDecimal curExportTaxVat) {
		this.curExportTaxVat = curExportTaxVat;
	}
	
    /**
     * @return curExportTaxVat
     */
	public java.math.BigDecimal getCurExportTaxVat() {
		return this.curExportTaxVat;
	}
	
	/**
	 * @param curPayableIt
	 */
	public void setCurPayableIt(java.math.BigDecimal curPayableIt) {
		this.curPayableIt = curPayableIt;
	}
	
    /**
     * @return curPayableIt
     */
	public java.math.BigDecimal getCurPayableIt() {
		return this.curPayableIt;
	}
	
	/**
	 * @param curPaidIt
	 */
	public void setCurPaidIt(java.math.BigDecimal curPaidIt) {
		this.curPaidIt = curPaidIt;
	}
	
    /**
     * @return curPaidIt
     */
	public java.math.BigDecimal getCurPaidIt() {
		return this.curPaidIt;
	}
	
	/**
	 * @param curExportTaxIt
	 */
	public void setCurExportTaxIt(java.math.BigDecimal curExportTaxIt) {
		this.curExportTaxIt = curExportTaxIt;
	}
	
    /**
     * @return curExportTaxIt
     */
	public java.math.BigDecimal getCurExportTaxIt() {
		return this.curExportTaxIt;
	}
	
	/**
	 * @param payableVatRemark
	 */
	public void setPayableVatRemark(String payableVatRemark) {
		this.payableVatRemark = payableVatRemark;
	}
	
    /**
     * @return payableVatRemark
     */
	public String getPayableVatRemark() {
		return this.payableVatRemark;
	}
	
	/**
	 * @param paidVatRemark
	 */
	public void setPaidVatRemark(String paidVatRemark) {
		this.paidVatRemark = paidVatRemark;
	}
	
    /**
     * @return paidVatRemark
     */
	public String getPaidVatRemark() {
		return this.paidVatRemark;
	}
	
	/**
	 * @param exportTaxVatRemark
	 */
	public void setExportTaxVatRemark(String exportTaxVatRemark) {
		this.exportTaxVatRemark = exportTaxVatRemark;
	}
	
    /**
     * @return exportTaxVatRemark
     */
	public String getExportTaxVatRemark() {
		return this.exportTaxVatRemark;
	}
	
	/**
	 * @param payableItRemark
	 */
	public void setPayableItRemark(String payableItRemark) {
		this.payableItRemark = payableItRemark;
	}
	
    /**
     * @return payableItRemark
     */
	public String getPayableItRemark() {
		return this.payableItRemark;
	}
	
	/**
	 * @param paidItRemark
	 */
	public void setPaidItRemark(String paidItRemark) {
		this.paidItRemark = paidItRemark;
	}
	
    /**
     * @return paidItRemark
     */
	public String getPaidItRemark() {
		return this.paidItRemark;
	}
	
	/**
	 * @param exportTaxItRemark
	 */
	public void setExportTaxItRemark(String exportTaxItRemark) {
		this.exportTaxItRemark = exportTaxItRemark;
	}
	
    /**
     * @return exportTaxItRemark
     */
	public String getExportTaxItRemark() {
		return this.exportTaxItRemark;
	}
	
	/**
	 * @param lastTwoYearDebtRate
	 */
	public void setLastTwoYearDebtRate(java.math.BigDecimal lastTwoYearDebtRate) {
		this.lastTwoYearDebtRate = lastTwoYearDebtRate;
	}
	
    /**
     * @return lastTwoYearDebtRate
     */
	public java.math.BigDecimal getLastTwoYearDebtRate() {
		return this.lastTwoYearDebtRate;
	}
	
	/**
	 * @param lastTwoYearCurrentRatio
	 */
	public void setLastTwoYearCurrentRatio(java.math.BigDecimal lastTwoYearCurrentRatio) {
		this.lastTwoYearCurrentRatio = lastTwoYearCurrentRatio;
	}
	
    /**
     * @return lastTwoYearCurrentRatio
     */
	public java.math.BigDecimal getLastTwoYearCurrentRatio() {
		return this.lastTwoYearCurrentRatio;
	}
	
	/**
	 * @param lastTwoYearQuickRatio
	 */
	public void setLastTwoYearQuickRatio(java.math.BigDecimal lastTwoYearQuickRatio) {
		this.lastTwoYearQuickRatio = lastTwoYearQuickRatio;
	}
	
    /**
     * @return lastTwoYearQuickRatio
     */
	public java.math.BigDecimal getLastTwoYearQuickRatio() {
		return this.lastTwoYearQuickRatio;
	}
	
	/**
	 * @param lastTwoYearWorkCap
	 */
	public void setLastTwoYearWorkCap(java.math.BigDecimal lastTwoYearWorkCap) {
		this.lastTwoYearWorkCap = lastTwoYearWorkCap;
	}
	
    /**
     * @return lastTwoYearWorkCap
     */
	public java.math.BigDecimal getLastTwoYearWorkCap() {
		return this.lastTwoYearWorkCap;
	}
	
	/**
	 * @param lastTwoYearArvto
	 */
	public void setLastTwoYearArvto(java.math.BigDecimal lastTwoYearArvto) {
		this.lastTwoYearArvto = lastTwoYearArvto;
	}
	
    /**
     * @return lastTwoYearArvto
     */
	public java.math.BigDecimal getLastTwoYearArvto() {
		return this.lastTwoYearArvto;
	}
	
	/**
	 * @param lastTwoYearItto
	 */
	public void setLastTwoYearItto(java.math.BigDecimal lastTwoYearItto) {
		this.lastTwoYearItto = lastTwoYearItto;
	}
	
    /**
     * @return lastTwoYearItto
     */
	public java.math.BigDecimal getLastTwoYearItto() {
		return this.lastTwoYearItto;
	}
	
	/**
	 * @param lastTwoYearRota
	 */
	public void setLastTwoYearRota(java.math.BigDecimal lastTwoYearRota) {
		this.lastTwoYearRota = lastTwoYearRota;
	}
	
    /**
     * @return lastTwoYearRota
     */
	public java.math.BigDecimal getLastTwoYearRota() {
		return this.lastTwoYearRota;
	}
	
	/**
	 * @param lastTwoYearRona
	 */
	public void setLastTwoYearRona(java.math.BigDecimal lastTwoYearRona) {
		this.lastTwoYearRona = lastTwoYearRona;
	}
	
    /**
     * @return lastTwoYearRona
     */
	public java.math.BigDecimal getLastTwoYearRona() {
		return this.lastTwoYearRona;
	}
	
	/**
	 * @param lastTwoYearRevinr
	 */
	public void setLastTwoYearRevinr(java.math.BigDecimal lastTwoYearRevinr) {
		this.lastTwoYearRevinr = lastTwoYearRevinr;
	}
	
    /**
     * @return lastTwoYearRevinr
     */
	public java.math.BigDecimal getLastTwoYearRevinr() {
		return this.lastTwoYearRevinr;
	}
	
	/**
	 * @param lastYearDebtRate
	 */
	public void setLastYearDebtRate(java.math.BigDecimal lastYearDebtRate) {
		this.lastYearDebtRate = lastYearDebtRate;
	}
	
    /**
     * @return lastYearDebtRate
     */
	public java.math.BigDecimal getLastYearDebtRate() {
		return this.lastYearDebtRate;
	}
	
	/**
	 * @param lastYearCurrentRatio
	 */
	public void setLastYearCurrentRatio(java.math.BigDecimal lastYearCurrentRatio) {
		this.lastYearCurrentRatio = lastYearCurrentRatio;
	}
	
    /**
     * @return lastYearCurrentRatio
     */
	public java.math.BigDecimal getLastYearCurrentRatio() {
		return this.lastYearCurrentRatio;
	}
	
	/**
	 * @param lastYearQuickRatio
	 */
	public void setLastYearQuickRatio(java.math.BigDecimal lastYearQuickRatio) {
		this.lastYearQuickRatio = lastYearQuickRatio;
	}
	
    /**
     * @return lastYearQuickRatio
     */
	public java.math.BigDecimal getLastYearQuickRatio() {
		return this.lastYearQuickRatio;
	}
	
	/**
	 * @param lastYearWorkCap
	 */
	public void setLastYearWorkCap(java.math.BigDecimal lastYearWorkCap) {
		this.lastYearWorkCap = lastYearWorkCap;
	}
	
    /**
     * @return lastYearWorkCap
     */
	public java.math.BigDecimal getLastYearWorkCap() {
		return this.lastYearWorkCap;
	}
	
	/**
	 * @param lastYearArvto
	 */
	public void setLastYearArvto(java.math.BigDecimal lastYearArvto) {
		this.lastYearArvto = lastYearArvto;
	}
	
    /**
     * @return lastYearArvto
     */
	public java.math.BigDecimal getLastYearArvto() {
		return this.lastYearArvto;
	}
	
	/**
	 * @param lastYearItto
	 */
	public void setLastYearItto(java.math.BigDecimal lastYearItto) {
		this.lastYearItto = lastYearItto;
	}
	
    /**
     * @return lastYearItto
     */
	public java.math.BigDecimal getLastYearItto() {
		return this.lastYearItto;
	}
	
	/**
	 * @param lastYearRota
	 */
	public void setLastYearRota(java.math.BigDecimal lastYearRota) {
		this.lastYearRota = lastYearRota;
	}
	
    /**
     * @return lastYearRota
     */
	public java.math.BigDecimal getLastYearRota() {
		return this.lastYearRota;
	}
	
	/**
	 * @param lastYearRona
	 */
	public void setLastYearRona(java.math.BigDecimal lastYearRona) {
		this.lastYearRona = lastYearRona;
	}
	
    /**
     * @return lastYearRona
     */
	public java.math.BigDecimal getLastYearRona() {
		return this.lastYearRona;
	}
	
	/**
	 * @param lastYearRevinr
	 */
	public void setLastYearRevinr(java.math.BigDecimal lastYearRevinr) {
		this.lastYearRevinr = lastYearRevinr;
	}
	
    /**
     * @return lastYearRevinr
     */
	public java.math.BigDecimal getLastYearRevinr() {
		return this.lastYearRevinr;
	}
	
	/**
	 * @param curYearDebtRate
	 */
	public void setCurYearDebtRate(java.math.BigDecimal curYearDebtRate) {
		this.curYearDebtRate = curYearDebtRate;
	}
	
    /**
     * @return curYearDebtRate
     */
	public java.math.BigDecimal getCurYearDebtRate() {
		return this.curYearDebtRate;
	}
	
	/**
	 * @param curYearCurrentRatio
	 */
	public void setCurYearCurrentRatio(java.math.BigDecimal curYearCurrentRatio) {
		this.curYearCurrentRatio = curYearCurrentRatio;
	}
	
    /**
     * @return curYearCurrentRatio
     */
	public java.math.BigDecimal getCurYearCurrentRatio() {
		return this.curYearCurrentRatio;
	}
	
	/**
	 * @param curYearQuickRatio
	 */
	public void setCurYearQuickRatio(java.math.BigDecimal curYearQuickRatio) {
		this.curYearQuickRatio = curYearQuickRatio;
	}
	
    /**
     * @return curYearQuickRatio
     */
	public java.math.BigDecimal getCurYearQuickRatio() {
		return this.curYearQuickRatio;
	}
	
	/**
	 * @param curYearWorkCap
	 */
	public void setCurYearWorkCap(java.math.BigDecimal curYearWorkCap) {
		this.curYearWorkCap = curYearWorkCap;
	}
	
    /**
     * @return curYearWorkCap
     */
	public java.math.BigDecimal getCurYearWorkCap() {
		return this.curYearWorkCap;
	}
	
	/**
	 * @param curYearArvto
	 */
	public void setCurYearArvto(java.math.BigDecimal curYearArvto) {
		this.curYearArvto = curYearArvto;
	}
	
    /**
     * @return curYearArvto
     */
	public java.math.BigDecimal getCurYearArvto() {
		return this.curYearArvto;
	}
	
	/**
	 * @param curYearItto
	 */
	public void setCurYearItto(java.math.BigDecimal curYearItto) {
		this.curYearItto = curYearItto;
	}
	
    /**
     * @return curYearItto
     */
	public java.math.BigDecimal getCurYearItto() {
		return this.curYearItto;
	}
	
	/**
	 * @param curYearRota
	 */
	public void setCurYearRota(java.math.BigDecimal curYearRota) {
		this.curYearRota = curYearRota;
	}
	
    /**
     * @return curYearRota
     */
	public java.math.BigDecimal getCurYearRota() {
		return this.curYearRota;
	}
	
	/**
	 * @param curYearRona
	 */
	public void setCurYearRona(java.math.BigDecimal curYearRona) {
		this.curYearRona = curYearRona;
	}
	
    /**
     * @return curYearRona
     */
	public java.math.BigDecimal getCurYearRona() {
		return this.curYearRona;
	}
	
	/**
	 * @param curYearRevinr
	 */
	public void setCurYearRevinr(java.math.BigDecimal curYearRevinr) {
		this.curYearRevinr = curYearRevinr;
	}
	
    /**
     * @return curYearRevinr
     */
	public java.math.BigDecimal getCurYearRevinr() {
		return this.curYearRevinr;
	}
	
	/**
	 * @param fncDeclare
	 */
	public void setFncDeclare(String fncDeclare) {
		this.fncDeclare = fncDeclare;
	}
	
    /**
     * @return fncDeclare
     */
	public String getFncDeclare() {
		return this.fncDeclare;
	}


}