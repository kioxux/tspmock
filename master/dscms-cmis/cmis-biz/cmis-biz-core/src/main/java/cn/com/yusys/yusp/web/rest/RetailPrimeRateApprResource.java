/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import cn.com.yusys.yusp.service.RetailPrimeRateApprService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:20:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/retailprimerateappr")
public class RetailPrimeRateApprResource {
    @Autowired
    private RetailPrimeRateApprService retailPrimeRateApprService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RetailPrimeRateAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<RetailPrimeRateAppr> list = retailPrimeRateApprService.selectAll(queryModel);
        return new ResultDto<List<RetailPrimeRateAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RetailPrimeRateAppr>> index(QueryModel queryModel) {
        List<RetailPrimeRateAppr> list = retailPrimeRateApprService.selectByModel(queryModel);
        return new ResultDto<List<RetailPrimeRateAppr>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RetailPrimeRateAppr> create(@RequestBody RetailPrimeRateAppr retailPrimeRateAppr) throws URISyntaxException {
        retailPrimeRateApprService.insert(retailPrimeRateAppr);
        return new ResultDto<RetailPrimeRateAppr>(retailPrimeRateAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RetailPrimeRateAppr retailPrimeRateAppr) throws URISyntaxException {
        int result = retailPrimeRateApprService.update(retailPrimeRateAppr);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = retailPrimeRateApprService.deleteByPrimaryKey(pkId, serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param  retailPrimeRateAppr
     * @return
     * @author wzy
     * @date 2021/9/11 17:32
     * @version 1.0.0
     * @desc  查询最新一条审批数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbysernonew")
    protected ResultDto<RetailPrimeRateAppr> selectbysernonew(@RequestBody RetailPrimeRateAppr retailPrimeRateAppr) {
        RetailPrimeRateAppr record = retailPrimeRateApprService.selectBySernoNew(retailPrimeRateAppr);
        return new ResultDto<RetailPrimeRateAppr>(record);
    }

    /**
     * @param  retailPrimeRateAppr
     * @return
     * @author wzy
     * @date 2021/9/11 17:32
     * @version 1.0.0
     * @desc  查询最新一条审批数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/saveinfo")
    protected ResultDto<RetailPrimeRateAppr> saveInfo(@RequestBody RetailPrimeRateAppr retailPrimeRateAppr) {
        RetailPrimeRateAppr record = retailPrimeRateApprService.saveInfo(retailPrimeRateAppr);
        return new ResultDto<RetailPrimeRateAppr>(record);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<RetailPrimeRateAppr>> selectByModel(@RequestBody QueryModel queryModel) {
        List<RetailPrimeRateAppr> list = retailPrimeRateApprService.selectByModel(queryModel);
        return new ResultDto<List<RetailPrimeRateAppr>>(list);
    }

    /**
     * @param  retailPrimeRateAppr
     * @return
     * @author wzy
     * @date 2021/9/11 17:32
     * @version 1.0.0
     * @desc 根据节点查询批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbysernoandnode")
    protected ResultDto<RetailPrimeRateAppr> selectbySernoAndNode(@RequestBody RetailPrimeRateAppr retailPrimeRateAppr) {
        RetailPrimeRateAppr record = retailPrimeRateApprService.selectbySernoAndNode(retailPrimeRateAppr);
        return new ResultDto<RetailPrimeRateAppr>(record);
    }

}
