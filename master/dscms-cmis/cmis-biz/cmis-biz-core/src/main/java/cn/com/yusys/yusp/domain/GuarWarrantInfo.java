/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantInfo
 * @类描述: guar_warrant_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 17:13:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_warrant_info")
public class GuarWarrantInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 抵质押分类 **/
	@Column(name = "GRT_FLAG", unique = false, nullable = true, length = 5)
	private String grtFlag;
	
	/** 权证编号 **/
	@Column(name = "WARRANT_NO", unique = false, nullable = true, length = 200)
	private String warrantNo;

	/** 核心担保编号 **/
	@Column(name = "CORE_GUARANTY_NO", unique = false, nullable = true, length = 40)
	private String coreGuarantyNo;
	
	/** 核心担保品序号 **/
	@Column(name = "CORE_GUARANTY_SEQ", unique = false, nullable = true, length = 40)
	private String coreGuarantySeq;
	
	/** 权利凭证号 **/
	@Column(name = "CERTI_RECORD_ID", unique = false, nullable = true, length = 40)
	private String certiRecordId;
	
	/** 权证类别 **/
	@Column(name = "CERTI_CATALOG", unique = false, nullable = true, length = 5)
	private String certiCatalog;
	
	/** 权证类型 **/
	@Column(name = "CERTI_TYPE_CD", unique = false, nullable = true, length = 5)
	private String certiTypeCd;
	
	/** 是否电子权证 **/
	@Column(name = "IS_E_WARRANT", unique = false, nullable = true, length = 5)
	private String isEWarrant;
	
	/** 权证名称 **/
	@Column(name = "WARRANT_NAME", unique = false, nullable = true, length = 100)
	private String warrantName;
	
	/** 权证发证机关名称 **/
	@Column(name = "CERTI_ORG_NAME", unique = false, nullable = true, length = 100)
	private String certiOrgName;
	
	/** 权证发证日期 **/
	@Column(name = "CERTI_START_DATE", unique = false, nullable = true, length = 10)
	private String certiStartDate;
	
	/** 权证到期日期 **/
	@Column(name = "CERTI_END_DATE", unique = false, nullable = true, length = 10)
	private String certiEndDate;
	
	/** 权证登记日期 **/
	@Column(name = "WARRANT_INPUT_DATE", unique = false, nullable = true, length = 10)
	private String warrantInputDate;
	
	/** 权证入库日期 **/
	@Column(name = "IN_DATE", unique = false, nullable = true, length = 10)
	private String inDate;
	
	/** 权证正常出库日期 **/
	@Column(name = "OUT_DATE", unique = false, nullable = true, length = 10)
	private String outDate;
	
	/** 权利金额(权利价值) **/
	@Column(name = "CERTI_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal certiAmt;

	/** 押品顺位标识 **/
	@Column(name = "MORT_ORDER_FLAG", unique = false, nullable = true, length = 10)
	private String mortOrderFlag;
	
	/** 管存机关 **/
	@Column(name = "DEPOSIT_ORG_NO", unique = false, nullable = true, length = 100)
	private String depositOrgNo;
	
	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;
	
	/** 权证备注信息 **/
	@Column(name = "CERTI_COMMENT", unique = false, nullable = true, length = 1000)
	private String certiComment;
	
	/** 权证临时借用人名称 **/
	@Column(name = "TEMP_BORROWER_NAME", unique = false, nullable = true, length = 100)
	private String tempBorrowerName;
	
	/** 权证临时出库日期 **/
	@Column(name = "TEMP_OUT_DATE", unique = false, nullable = true, length = 10)
	private String tempOutDate;
	
	/** 权证预计归还时间 **/
	@Column(name = "PRE_BACK_DATE", unique = false, nullable = true, length = 10)
	private String preBackDate;
	
	/** 权证实际归还日期 **/
	@Column(name = "REAL_BACK_DATE", unique = false, nullable = true, length = 10)
	private String realBackDate;
	
	/** 权证状态 **/
	@Column(name = "CERTI_STATE", unique = false, nullable = true, length = 10)
	private String certiState;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private java.util.Date updateTime;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 证明权利或事项 **/
	@Column(name = "PRO_CERTI_ITEM", unique = false, nullable = true, length = 2)
	private String proCertiItem;

	/** 是否集中作业打印 **/
	@Column(name = "IS_FOCUS_WORK_PRINT", unique = false, nullable = true, length = 2)
	private String isFocusWorkPrint;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}
	
    /**
     * @return grtFlag
     */
	public String getGrtFlag() {
		return this.grtFlag;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo;
	}
	
    /**
     * @return warrantNo
     */
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}
	
    /**
     * @return coreGuarantyNo
     */
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param coreGuarantySeq
	 */
	public void setCoreGuarantySeq(String coreGuarantySeq) {
		this.coreGuarantySeq = coreGuarantySeq;
	}
	
    /**
     * @return coreGuarantySeq
     */
	public String getCoreGuarantySeq() {
		return this.coreGuarantySeq;
	}
	
	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId;
	}
	
    /**
     * @return certiRecordId
     */
	public String getCertiRecordId() {
		return this.certiRecordId;
	}
	
	/**
	 * @param certiCatalog
	 */
	public void setCertiCatalog(String certiCatalog) {
		this.certiCatalog = certiCatalog;
	}
	
    /**
     * @return certiCatalog
     */
	public String getCertiCatalog() {
		return this.certiCatalog;
	}
	
	/**
	 * @param certiTypeCd
	 */
	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd;
	}
	
    /**
     * @return certiTypeCd
     */
	public String getCertiTypeCd() {
		return this.certiTypeCd;
	}
	
	/**
	 * @param isEWarrant
	 */
	public void setIsEWarrant(String isEWarrant) {
		this.isEWarrant = isEWarrant;
	}
	
    /**
     * @return isEWarrant
     */
	public String getIsEWarrant() {
		return this.isEWarrant;
	}
	
	/**
	 * @param warrantName
	 */
	public void setWarrantName(String warrantName) {
		this.warrantName = warrantName;
	}
	
    /**
     * @return warrantName
     */
	public String getWarrantName() {
		return this.warrantName;
	}
	
	/**
	 * @param certiOrgName
	 */
	public void setCertiOrgName(String certiOrgName) {
		this.certiOrgName = certiOrgName;
	}
	
    /**
     * @return certiOrgName
     */
	public String getCertiOrgName() {
		return this.certiOrgName;
	}
	
	/**
	 * @param certiStartDate
	 */
	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate;
	}
	
    /**
     * @return certiStartDate
     */
	public String getCertiStartDate() {
		return this.certiStartDate;
	}
	
	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate;
	}
	
    /**
     * @return certiEndDate
     */
	public String getCertiEndDate() {
		return this.certiEndDate;
	}
	
	/**
	 * @param warrantInputDate
	 */
	public void setWarrantInputDate(String warrantInputDate) {
		this.warrantInputDate = warrantInputDate;
	}
	
    /**
     * @return warrantInputDate
     */
	public String getWarrantInputDate() {
		return this.warrantInputDate;
	}
	
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}
	
    /**
     * @return inDate
     */
	public String getInDate() {
		return this.inDate;
	}
	
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	
    /**
     * @return outDate
     */
	public String getOutDate() {
		return this.outDate;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return certiAmt
     */
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}

	/**
	 * @param mortOrderFlag
	 */
	public void setMortOrderFlag(String mortOrderFlag) {
		this.mortOrderFlag = mortOrderFlag;
	}
	
    /**
     * @return mortOrderFlag
     */
	public String getMortOrderFlag() {
		return this.mortOrderFlag;
	}
	
	/**
	 * @param depositOrgNo
	 */
	public void setDepositOrgNo(String depositOrgNo) {
		this.depositOrgNo = depositOrgNo;
	}
	
    /**
     * @return depositOrgNo
     */
	public String getDepositOrgNo() {
		return this.depositOrgNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param certiComment
	 */
	public void setCertiComment(String certiComment) {
		this.certiComment = certiComment;
	}
	
    /**
     * @return certiComment
     */
	public String getCertiComment() {
		return this.certiComment;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName;
	}
	
    /**
     * @return tempBorrowerName
     */
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate;
	}
	
    /**
     * @return tempOutDate
     */
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate;
	}
	
    /**
     * @return preBackDate
     */
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate;
	}
	
    /**
     * @return realBackDate
     */
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param certiState
	 */
	public void setCertiState(String certiState) {
		this.certiState = certiState;
	}
	
    /**
     * @return certiState
     */
	public String getCertiState() {
		return this.certiState;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param proCertiItem
	 */
	public void setProCertiItem(String proCertiItem) {
		this.proCertiItem = proCertiItem;
	}
	
    /**
     * @return proCertiItem
     */
	public String getProCertiItem() {
		return this.proCertiItem;
	}

	/**
	 * @param isFocusWorkPrint
	 */
	public void setIsFocusWorkPrint(String isFocusWorkPrint) {
		this.isFocusWorkPrint = isFocusWorkPrint;
	}

	/**
	 * @return isFocusWorkPrint
	 */
	public String getIsFocusWorkPrint() {
		return this.isFocusWorkPrint;
	}


}