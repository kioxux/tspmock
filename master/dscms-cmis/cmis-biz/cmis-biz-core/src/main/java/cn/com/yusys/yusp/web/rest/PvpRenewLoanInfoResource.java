/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpRenewLoanInfo;
import cn.com.yusys.yusp.service.PvpRenewLoanInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRenewLoanInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/pvprenewloaninfo")
public class PvpRenewLoanInfoResource {
    @Autowired
    private PvpRenewLoanInfoService pvpRenewLoanInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpRenewLoanInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoService.selectAll(queryModel);
        return new ResultDto<List<PvpRenewLoanInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpRenewLoanInfo>> index(QueryModel queryModel) {
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoService.selectByModel(queryModel);
        return new ResultDto<List<PvpRenewLoanInfo>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpRenewLoanInfo> create(@RequestBody PvpRenewLoanInfo pvpRenewLoanInfo) throws URISyntaxException {
        pvpRenewLoanInfoService.insert(pvpRenewLoanInfo);
        return new ResultDto<PvpRenewLoanInfo>(pvpRenewLoanInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpRenewLoanInfo pvpRenewLoanInfo) throws URISyntaxException {
        int result = pvpRenewLoanInfoService.update(pvpRenewLoanInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String oldBillNo) {
        int result = pvpRenewLoanInfoService.deleteByPrimaryKey(pkId, oldBillNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<PvpRenewLoanInfo>> selectByModel(@RequestBody QueryModel queryModel) {
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoService.selectByModel(queryModel);
        return new ResultDto<List<PvpRenewLoanInfo>>(list);
    }

    /**
     * @函数名称:selectByParams
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbyparams")
    protected ResultDto<List<PvpRenewLoanInfo>> selectByParams(@RequestBody QueryModel queryModel) {
        List<PvpRenewLoanInfo> list = pvpRenewLoanInfoService.selectByParams(queryModel);
        return new ResultDto<List<PvpRenewLoanInfo>>(list);
    }
}
