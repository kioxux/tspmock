/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperAgricultureBreedSituation
 * @类描述: rpt_oper_agriculture_breed_situation数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 10:36:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_agriculture_breed_situation")
public class RptOperAgricultureBreedSituation extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 项目 **/
	@Column(name = "PROJECT", unique = false, nullable = true, length = 65535)
	private String project;
	
	/** 种植或养殖开始时间 **/
	@Column(name = "PLANT_START_DATE", unique = false, nullable = true, length = 20)
	private String plantStartDate;
	
	/** 生长周期 **/
	@Column(name = "GROWTH_CYCLE", unique = false, nullable = true, length = 10)
	private String growthCycle;
	
	/** 现有规模 **/
	@Column(name = "CURRENT_SCALE", unique = false, nullable = true, length = 80)
	private String currentScale;
	
	/** 种植工艺（栽培技术） **/
	@Column(name = "PLANT_SKILL", unique = false, nullable = true, length = 80)
	private String plantSkill;
	
	/** 应收账款 **/
	@Column(name = "DEBT_RECEVIABLE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtReceviable;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param project
	 */
	public void setProject(String project) {
		this.project = project;
	}
	
    /**
     * @return project
     */
	public String getProject() {
		return this.project;
	}
	
	/**
	 * @param plantStartDate
	 */
	public void setPlantStartDate(String plantStartDate) {
		this.plantStartDate = plantStartDate;
	}
	
    /**
     * @return plantStartDate
     */
	public String getPlantStartDate() {
		return this.plantStartDate;
	}
	
	/**
	 * @param growthCycle
	 */
	public void setGrowthCycle(String growthCycle) {
		this.growthCycle = growthCycle;
	}
	
    /**
     * @return growthCycle
     */
	public String getGrowthCycle() {
		return this.growthCycle;
	}
	
	/**
	 * @param currentScale
	 */
	public void setCurrentScale(String currentScale) {
		this.currentScale = currentScale;
	}
	
    /**
     * @return currentScale
     */
	public String getCurrentScale() {
		return this.currentScale;
	}
	
	/**
	 * @param plantSkill
	 */
	public void setPlantSkill(String plantSkill) {
		this.plantSkill = plantSkill;
	}
	
    /**
     * @return plantSkill
     */
	public String getPlantSkill() {
		return this.plantSkill;
	}
	
	/**
	 * @param debtReceviable
	 */
	public void setDebtReceviable(java.math.BigDecimal debtReceviable) {
		this.debtReceviable = debtReceviable;
	}
	
    /**
     * @return debtReceviable
     */
	public java.math.BigDecimal getDebtReceviable() {
		return this.debtReceviable;
	}


}