/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReconsideDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtChgDetail;
import cn.com.yusys.yusp.service.LmtChgDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtChgDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:18:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtchgdetail")
public class LmtChgDetailResource {
    @Autowired
    private LmtChgDetailService lmtChgDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtChgDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtChgDetail> list = lmtChgDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtChgDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtChgDetail>> index(QueryModel queryModel) {
        List<LmtChgDetail> list = lmtChgDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtChgDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtChgDetail> show(@PathVariable("pkId") String pkId) {
        LmtChgDetail lmtChgDetail = lmtChgDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtChgDetail>(lmtChgDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtChgDetail> create(@RequestBody LmtChgDetail lmtChgDetail) throws URISyntaxException {
        lmtChgDetailService.insert(lmtChgDetail);
        return new ResultDto<LmtChgDetail>(lmtChgDetail);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtChgDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtChgDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号获取对象
     * @param condition
     * @return
     */
    @PostMapping("/selectByLmtSerno")
    protected ResultDto<LmtChgDetail> selectByLmtSerno(@RequestBody Map condition){
        String lmtSerno = (String) condition.get("lmtSerno");
        LmtChgDetail lmtChgDetail = lmtChgDetailService.selectByLmtSerno(lmtSerno);
        return new ResultDto<>(lmtChgDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtChgDetail lmtChgDetail) throws URISyntaxException {
        int result = lmtChgDetailService.update(lmtChgDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtChgDetail> save(@RequestBody LmtChgDetail lmtChgDetail) throws URISyntaxException {
        LmtChgDetail lmtChgDetail1 = lmtChgDetailService.selectByLmtSerno1(lmtChgDetail.getLmtSerno());
        lmtChgDetail.setOprType("01");
        if(lmtChgDetail1!=null){
            lmtChgDetailService.update(lmtChgDetail);
        }else{
            lmtChgDetailService.insert(lmtChgDetail);
        }
        return ResultDto.success(lmtChgDetail);
    }

    /**
     * @函数名称:show
     * @函数描述:进入页面查询数据返回前台
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtChgDetail> selectBySerno(@RequestBody String lmtSerno) {
        LmtChgDetail lmtChgDetail = lmtChgDetailService.selectByLmtSerno1(lmtSerno);
        return  ResultDto.success(lmtChgDetail);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetailByLmtSerno")
    protected ResultDto<List<Map<String,String>>> queryDetailByLmtSerno(@RequestBody Map<String,String> map) {
        List<Map<String,String>> lmtChgDetails = lmtChgDetailService.queryDetailByLmtSerno(map.get("lmtSerno"));
        return  ResultDto.success(lmtChgDetails);
    }


    /**
     * @函数名称:保存变更申请表
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateBgsqb")
    protected ResultDto<Integer> updateBgsqb(@RequestBody LmtChgDetail lmtChgDetail) throws URISyntaxException {
        int result = lmtChgDetailService.updateBgsqb(lmtChgDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveBgsqb")
    protected ResultDto<LmtChgDetail> saveBgsqb(@RequestBody LmtChgDetail lmtChgDetail) throws URISyntaxException {
        LmtChgDetail lmtChgDetail1 = lmtChgDetailService.selectByLmtSerno1(lmtChgDetail.getLmtSerno());
        lmtChgDetail.setOprType("01");
        if(lmtChgDetail1!=null){
            lmtChgDetailService.update(lmtChgDetail);
        }else{
            lmtChgDetailService.insert(lmtChgDetail);
        }
        lmtChgDetailService.updateMustCheckStatus(lmtChgDetail.getLmtSerno(),"bgsqb");
        return ResultDto.success(lmtChgDetail);
    }

}
