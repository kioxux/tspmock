/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpTruPayInfo
 * @类描述: pvp_tru_pay_info数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-08 20:46:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_tru_pay_info")
public class PvpTruPayInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 交易流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TRAN_SERNO")
	private String tranSerno;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = false, length = 40)
	private String pvpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 未受托支付类型 **/
	@Column(name = "UNTRU_PAY_TYPE", unique = false, nullable = true, length = 5)
	private String untruPayType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账号名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 币种  STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 交易金额 **/
	@Column(name = "TRAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tranAmt;
	
	/** 交易日期 **/
	@Column(name = "TRAN_DATE", unique = false, nullable = true, length = 10)
	private String tranDate;
	
	/** 返回编码 **/
	@Column(name = "RETURN_CODE", unique = false, nullable = true, length = 15)
	private String returnCode;
	
	/** 返回说明 **/
	@Column(name = "RETURN_DESC", unique = false, nullable = true, length = 2000)
	private String returnDesc;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 放款机构 **/
	@Column(name = "ACCT_BR_ID", unique = false, nullable = true, length = 20)
	private String acctBrId;
	
	/** 支付状态 STD_ZB_PAY_ST **/
	@Column(name = "PAY_STATUS", unique = false, nullable = true, length = 5)
	private String payStatus;
	
	/** 是否退汇标识 STD_ZB_YES_NO **/
	@Column(name = "IS_RETURN_FLAG", unique = false, nullable = true, length = 5)
	private String isReturnFlag;
	
	/** 退汇原因 **/
	@Column(name = "RRU_RESN", unique = false, nullable = true, length = 200)
	private String rruResn;
	
	
	/**
	 * @param tranSerno
	 */
	public void setTranSerno(String tranSerno) {
		this.tranSerno = tranSerno;
	}
	
    /**
     * @return tranSerno
     */
	public String getTranSerno() {
		return this.tranSerno;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param untruPayType
	 */
	public void setUntruPayType(String untruPayType) {
		this.untruPayType = untruPayType;
	}
	
    /**
     * @return untruPayType
     */
	public String getUntruPayType() {
		return this.untruPayType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param tranAmt
	 */
	public void setTranAmt(java.math.BigDecimal tranAmt) {
		this.tranAmt = tranAmt;
	}
	
    /**
     * @return tranAmt
     */
	public java.math.BigDecimal getTranAmt() {
		return this.tranAmt;
	}
	
	/**
	 * @param tranDate
	 */
	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}
	
    /**
     * @return tranDate
     */
	public String getTranDate() {
		return this.tranDate;
	}
	
	/**
	 * @param returnCode
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	
    /**
     * @return returnCode
     */
	public String getReturnCode() {
		return this.returnCode;
	}
	
	/**
	 * @param returnDesc
	 */
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}
	
    /**
     * @return returnDesc
     */
	public String getReturnDesc() {
		return this.returnDesc;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param acctBrId
	 */
	public void setAcctBrId(String acctBrId) {
		this.acctBrId = acctBrId;
	}
	
    /**
     * @return acctBrId
     */
	public String getAcctBrId() {
		return this.acctBrId;
	}
	
	/**
	 * @param payStatus
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	
    /**
     * @return payStatus
     */
	public String getPayStatus() {
		return this.payStatus;
	}
	
	/**
	 * @param isReturnFlag
	 */
	public void setIsReturnFlag(String isReturnFlag) {
		this.isReturnFlag = isReturnFlag;
	}
	
    /**
     * @return isReturnFlag
     */
	public String getIsReturnFlag() {
		return this.isReturnFlag;
	}
	
	/**
	 * @param rruResn
	 */
	public void setRruResn(String rruResn) {
		this.rruResn = rruResn;
	}
	
    /**
     * @return rruResn
     */
	public String getRruResn() {
		return this.rruResn;
	}


}