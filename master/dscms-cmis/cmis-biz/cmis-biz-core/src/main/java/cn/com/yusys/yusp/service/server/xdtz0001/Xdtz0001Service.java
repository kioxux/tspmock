package cn.com.yusys.yusp.service.server.xdtz0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0001.req.Xdtz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.resp.Xdtz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdtz0001Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class Xdtz0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0001Service.class);
    @Resource
    private AccLoanMapper accLoanMapper;

    /**
     * 客户信息查询(贷款信息)
     *
     * @param xdtz0001DataReqDto
     * @return
     * @throws Exception
     */
    public Xdtz0001DataRespDto getXdtz0001(Xdtz0001DataReqDto xdtz0001DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataReqDto));
        Xdtz0001DataRespDto xdtz0001DataRespDto = new Xdtz0001DataRespDto();
        try {
            //查询类型
            //01-根据核心客户号查询客户在信贷系统中是否存在逾期；
            //02-申请人在我行对公存在存量余额
            //03-根据核心客户号查询在行内是否存在经营性贷款
            //04-根据核心客户号查询惠享贷累计我行贷款次数
            //05-根据核心客户号查询申请人在本行信用类经营性贷款借据余额汇总
            String queryType = "";
            if (StringUtils.isEmpty(xdtz0001DataReqDto.getQueryType())) {
                throw BizException.error(null, EcbEnum.ECB010013.key, EcbEnum.ECB010013.value);
            } else {
                queryType = xdtz0001DataReqDto.getQueryType();
            }
            if (CmisCommonConstants.QUERY_TYPE_01.equals(queryType)) {
                //查询是否存在逾期
                int num = accLoanMapper.queryOverDueFromCmis(xdtz0001DataReqDto.getCusId());
                if (num > 0) {
                    xdtz0001DataRespDto.setIsOverdue(CmisCommonConstants.STD_ZB_YES);
                } else {
                    xdtz0001DataRespDto.setIsOverdue(CmisCommonConstants.STD_ZB_NO);
                }
            } else if (CmisCommonConstants.QUERY_TYPE_02.equals(queryType)) {
                //申请人在我行对公存在存量余额
                BigDecimal billBal = accLoanMapper.queryCunlye(xdtz0001DataReqDto.getCusId());
                xdtz0001DataRespDto.setBillBal(billBal);
            } else if (CmisCommonConstants.QUERY_TYPE_03.equals(queryType)) {
                //根据核心客户号查询在行内是否存在经营性贷款
                int num = accLoanMapper.queryPojyxdk(xdtz0001DataReqDto.getCusId());
                if (num > 0) {
                    xdtz0001DataRespDto.setIsOperLoan(CmisCommonConstants.STD_ZB_YES);
                } else {
                    xdtz0001DataRespDto.setIsOperLoan(CmisCommonConstants.STD_ZB_NO);
                }
            } else if (CmisCommonConstants.QUERY_TYPE_04.equals(queryType)) {
                //根据核心客户号查询惠享贷累计我行贷款次数
                int num = accLoanMapper.getLoanNumByCusId(xdtz0001DataReqDto.getCusId());
                xdtz0001DataRespDto.setLoanTimes(String.valueOf(num));
            } else if (CmisCommonConstants.QUERY_TYPE_05.equals(queryType)) {
                //根据核心客户号查询申请人在本行信用类经营性贷款借据余额汇总
                BigDecimal sumBalance = accLoanMapper.getSumBalanceByCusId(xdtz0001DataReqDto.getCusId());
                xdtz0001DataRespDto.setOperLoanBal(sumBalance);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataReqDto));
        return xdtz0001DataRespDto;
    }
}
