package cn.com.yusys.yusp.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.callbacks.Callback;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class Yqd001req implements Serializable {

    /**
     * 业务申请编号
     **/
    @JsonProperty(value = "businessId")
    String serno;

    /**
     * 申请人姓名
     **/
    @JsonProperty(value = "indname")
    String rqstrName;

    /**
     * 申请人证件号
     **/
    @JsonProperty(value = "indcertid")
    String rqstrCertNo;

    /**
     * 企业名称
     **/
    @JsonProperty(value = "entname")
    String conName;

    /**
     * 申请人手机号
     **/
    @JsonProperty(value = "indphoneid")
    String rqstrMobile;

    /**
     * 生产经营地址
     **/
    @JsonProperty(value = "address")
    String produceOperAddr;

    /**
     * 行业代码
     **/
    @JsonProperty(value = "hydm")
    String tradeCode;

    /**
     * 是否农户
     **/
    @JsonProperty(value = "sfnh")
    String isAgri;

    /**
     * 企业证件类型
     **/
    @JsonProperty(value = "ent_cert_type")
    String conCertType;

    /**
     * 企业证件号
     **/
    @JsonProperty(value = "ent_cert_id")
    String conCertNo;

    /**
     * 营销客户经理编号
     **/
    @JsonProperty(value = "cus_mgr_id")
    String saleManagerId;

    /**
     * 是否现场核验
     **/
    @JsonProperty(value = "is_scene_check")
    String isIotspot;

    /**
     * 核验原因
     **/
    @JsonProperty(value = "check_reason")
    String chekResn;

    /**
     * 居住地址
     **/
    @JsonProperty(value = "live_addr")
    String indivRsdAddr;

    /**
     * 终审额度
     **/
    @JsonProperty(value = "final_lmt")
    String finalApprLmt;

    /**
     * 利率
     **/
    @JsonProperty(value = "interest_rate")
    String realityIrY;

    /**
     * 累计逾期次数
     **/
    @JsonProperty(value = "indcredit005")
    String overdueTimesCumu;

    /**
     * 未结清贷款笔数
     **/
    @JsonProperty(value = "mdzrcb_icr0007")
    String unSettlLoanQnt;

    /**
     * 未销户信用卡授信总额
     **/
    @JsonProperty(value = "mdzrcb_icr0010")
    String unCloseCreditCardCrdLmt;

    /**
     * 未结清贷款余额
     **/
    @JsonProperty(value = "mdzrcb_icr0011")
    String unSettlLoanBal;

    /**
     * 近6个月信用卡平均支用比例
     **/
    @JsonProperty(value = "mdzrcb_icr0015")
    String latestSixMonCreditCardAvgDefrayPerc;

    /**
     * 最近2年贷款审批查询机构数
     **/
    @JsonProperty(value = "mdzrcb_icr0017")
    String latestTwoYearLoanApprQryOrgQnt;

    /**
     * 未结清消费金融公司贷款笔数
     **/
    @JsonProperty(value = "mdzrcb_icr0019")
    String unSettlBusiCprtLoanQnt;

    /**
     * 未结清对外贷款担保笔数
     **/
    @JsonProperty(value = "mdzrcb_icr0020")
    String unSettlOuterLoanGuarQnt;

    /**
     * 企业未结清业务机构数
     **/
    @JsonProperty(value = "mdzrcb_ecr0005")
    String conUnSettlBusiOrgQnt;

    /**
     * 企业未结清信贷余额
     **/
    @JsonProperty(value = "mdzrcb_ecr0004")
    String conUnSettlCretBal;

    /**
     * 完整纳税月份数
     **/
    @JsonProperty(value = "tax003")
    String fullTaxMonQnt;

    /**
     * 当前税务信用评级
     **/
    @JsonProperty(value = "mdzrcb_tax0008")
    String curtTaxCdtEval;

    /**
     * 近1年销售收入
     **/
    @JsonProperty(value = "mdzrcb_tax0005")
    String latestOneYearSaleIncome;

    /**
     * 近1年综合应纳税额
     **/
    @JsonProperty(value = "mdzrcb_tax0007")
    String latestOneYearInteTax;

    /**
     * 是否续贷
     **/
    @JsonProperty(value = "continuedloan")
    String isContinuLoan;

    /**
     * 模型初步结果
     **/
    @JsonProperty(value = "model_preli_result")
    String modelFstRst;

    /**
     * 申请期限
     **/
    @JsonProperty(value = "applyterm")
    String applyTerm;


    public String getRqstrName() {
        return rqstrName;
    }

    public void setRqstrName(String rqstrName) {
        this.rqstrName = rqstrName;
    }

    public String getRqstrCertNo() {
        return rqstrCertNo;
    }

    public void setRqstrCertNo(String rqstrCertNo) {
        this.rqstrCertNo = rqstrCertNo;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getRqstrMobile() {
        return rqstrMobile;
    }

    public void setRqstrMobile(String rqstrMobile) {
        this.rqstrMobile = rqstrMobile;
    }

    public String getProduceOperAddr() {
        return produceOperAddr;
    }

    public void setProduceOperAddr(String produceOperAddr) {
        this.produceOperAddr = produceOperAddr;
    }

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getConCertType() {
        return conCertType;
    }

    public void setConCertType(String conCertType) {
        this.conCertType = conCertType;
    }

    public String getConCertNo() {
        return conCertNo;
    }

    public void setConCertNo(String conCertNo) {
        this.conCertNo = conCertNo;
    }

    public String getSaleManagerId() {
        return saleManagerId;
    }

    public void setSaleManagerId(String saleManagerId) {
        this.saleManagerId = saleManagerId;
    }

    public String getIsIotspot() {
        return isIotspot;
    }

    public void setIsIotspot(String isIotspot) {
        this.isIotspot = isIotspot;
    }

    public String getChekResn() {
        return chekResn;
    }

    public void setChekResn(String chekResn) {
        this.chekResn = chekResn;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getFinalApprLmt() {
        return finalApprLmt;
    }

    public void setFinalApprLmt(String finalApprLmt) {
        this.finalApprLmt = finalApprLmt;
    }

    public String getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(String realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getOverdueTimesCumu() {
        return overdueTimesCumu;
    }

    public void setOverdueTimesCumu(String overdueTimesCumu) {
        this.overdueTimesCumu = overdueTimesCumu;
    }

    public String getUnSettlLoanQnt() {
        return unSettlLoanQnt;
    }

    public void setUnSettlLoanQnt(String unSettlLoanQnt) {
        this.unSettlLoanQnt = unSettlLoanQnt;
    }

    public String getUnCloseCreditCardCrdLmt() {
        return unCloseCreditCardCrdLmt;
    }

    public void setUnCloseCreditCardCrdLmt(String unCloseCreditCardCrdLmt) {
        this.unCloseCreditCardCrdLmt = unCloseCreditCardCrdLmt;
    }

    public String getUnSettlLoanBal() {
        return unSettlLoanBal;
    }

    public void setUnSettlLoanBal(String unSettlLoanBal) {
        this.unSettlLoanBal = unSettlLoanBal;
    }

    public String getLatestSixMonCreditCardAvgDefrayPerc() {
        return latestSixMonCreditCardAvgDefrayPerc;
    }

    public void setLatestSixMonCreditCardAvgDefrayPerc(String latestSixMonCreditCardAvgDefrayPerc) {
        this.latestSixMonCreditCardAvgDefrayPerc = latestSixMonCreditCardAvgDefrayPerc;
    }

    public String getLatestTwoYearLoanApprQryOrgQnt() {
        return latestTwoYearLoanApprQryOrgQnt;
    }

    public void setLatestTwoYearLoanApprQryOrgQnt(String latestTwoYearLoanApprQryOrgQnt) {
        this.latestTwoYearLoanApprQryOrgQnt = latestTwoYearLoanApprQryOrgQnt;
    }

    public String getUnSettlBusiCprtLoanQnt() {
        return unSettlBusiCprtLoanQnt;
    }

    public void setUnSettlBusiCprtLoanQnt(String unSettlBusiCprtLoanQnt) {
        this.unSettlBusiCprtLoanQnt = unSettlBusiCprtLoanQnt;
    }

    public String getUnSettlOuterLoanGuarQnt() {
        return unSettlOuterLoanGuarQnt;
    }

    public void setUnSettlOuterLoanGuarQnt(String unSettlOuterLoanGuarQnt) {
        this.unSettlOuterLoanGuarQnt = unSettlOuterLoanGuarQnt;
    }

    public String getConUnSettlBusiOrgQnt() {
        return conUnSettlBusiOrgQnt;
    }

    public void setConUnSettlBusiOrgQnt(String conUnSettlBusiOrgQnt) {
        this.conUnSettlBusiOrgQnt = conUnSettlBusiOrgQnt;
    }

    public String getConUnSettlCretBal() {
        return conUnSettlCretBal;
    }

    public void setConUnSettlCretBal(String conUnSettlCretBal) {
        this.conUnSettlCretBal = conUnSettlCretBal;
    }

    public String getFullTaxMonQnt() {
        return fullTaxMonQnt;
    }

    public void setFullTaxMonQnt(String fullTaxMonQnt) {
        this.fullTaxMonQnt = fullTaxMonQnt;
    }

    public String getCurtTaxCdtEval() {
        return curtTaxCdtEval;
    }

    public void setCurtTaxCdtEval(String curtTaxCdtEval) {
        this.curtTaxCdtEval = curtTaxCdtEval;
    }

    public String getLatestOneYearSaleIncome() {
        return latestOneYearSaleIncome;
    }

    public void setLatestOneYearSaleIncome(String latestOneYearSaleIncome) {
        this.latestOneYearSaleIncome = latestOneYearSaleIncome;
    }

    public String getLatestOneYearInteTax() {
        return latestOneYearInteTax;
    }

    public void setLatestOneYearInteTax(String latestOneYearInteTax) {
        this.latestOneYearInteTax = latestOneYearInteTax;
    }

    public String getIsContinuLoan() {
        return isContinuLoan;
    }

    public void setIsContinuLoan(String isContinuLoan) {
        this.isContinuLoan = isContinuLoan;
    }

    public String getModelFstRst() {
        return modelFstRst;
    }

    public void setModelFstRst(String modelFstRst) {
        this.modelFstRst = modelFstRst;
    }

    public String getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(String applyTerm) {
        this.applyTerm = applyTerm;
    }


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }


    @Override
    public String toString() {
        return "Yqd001req{" +
                "serno='" + serno + '\'' +
                '}';
    }
}
