/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSxkdPlusFkspDetails
 * @类描述: lmt_sxkd_plus_fksp_details数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-15 19:56:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sxkd_plus_fksp_details")
public class LmtSxkdPlusFkspDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 自动审批结果的主键 **/
	@Column(name = "APPROVE_RESULT_PK_ID", unique = false, nullable = true, length = 40)
	private String approveResultPkId;
	
	/** 规则编号 **/
	@Column(name = "RULE_ID", unique = false, nullable = true, length = 40)
	private String ruleId;
	
	/** 规则名称 **/
	@Column(name = "RULE_NAME", unique = false, nullable = true, length = 200)
	private String ruleName;
	
	/** 规则执行结果代码 **/
	@Column(name = "RULE_EXECUTE_CODE", unique = false, nullable = true, length = 40)
	private String ruleExecuteCode;
	
	/** 规则执行结果名称 **/
	@Column(name = "RULE_EXECUTE_NAME", unique = false, nullable = true, length = 40)
	private String ruleExecuteName;
	
	/** 规则执行结果名称 **/
	@Column(name = "RULE_EXECUTE_DESC", unique = false, nullable = true, length = 200)
	private String ruleExecuteDesc;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveResultPkId
	 */
	public void setApproveResultPkId(String approveResultPkId) {
		this.approveResultPkId = approveResultPkId;
	}
	
    /**
     * @return approveResultPkId
     */
	public String getApproveResultPkId() {
		return this.approveResultPkId;
	}
	
	/**
	 * @param ruleId
	 */
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	
    /**
     * @return ruleId
     */
	public String getRuleId() {
		return this.ruleId;
	}
	
	/**
	 * @param ruleName
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
    /**
     * @return ruleName
     */
	public String getRuleName() {
		return this.ruleName;
	}
	
	/**
	 * @param ruleExecuteCode
	 */
	public void setRuleExecuteCode(String ruleExecuteCode) {
		this.ruleExecuteCode = ruleExecuteCode;
	}
	
    /**
     * @return ruleExecuteCode
     */
	public String getRuleExecuteCode() {
		return this.ruleExecuteCode;
	}
	
	/**
	 * @param ruleExecuteName
	 */
	public void setRuleExecuteName(String ruleExecuteName) {
		this.ruleExecuteName = ruleExecuteName;
	}
	
    /**
     * @return ruleExecuteName
     */
	public String getRuleExecuteName() {
		return this.ruleExecuteName;
	}
	
	/**
	 * @param ruleExecuteDesc
	 */
	public void setRuleExecuteDesc(String ruleExecuteDesc) {
		this.ruleExecuteDesc = ruleExecuteDesc;
	}
	
    /**
     * @return ruleExecuteDesc
     */
	public String getRuleExecuteDesc() {
		return this.ruleExecuteDesc;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}