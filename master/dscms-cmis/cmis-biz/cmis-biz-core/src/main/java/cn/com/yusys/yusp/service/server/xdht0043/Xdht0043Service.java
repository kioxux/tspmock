package cn.com.yusys.yusp.service.server.xdht0043;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.xdht0043.req.Xdht0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0043.resp.Xdht0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0043Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: wangqing
 * @创建时间: 2021-06-15 19:32:46
 * @修改备注:小贷借款合同PDF生成
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0043Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0043Service.class);

    @Resource
    private ICusClientService icusClientService;

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;

    /**
     * 小贷借款合同文本生成pdf
     *
     * @param xdht0043DataReqDto
     * @return
     */
    public Xdht0043DataRespDto getXdJkhtInfo(Xdht0043DataReqDto xdht0043DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value,JSON.toJSONString(xdht0043DataReqDto));
        Xdht0043DataRespDto xdht0043DataRespDto = new Xdht0043DataRespDto();

        String cusName = xdht0043DataReqDto.getCusName();//客户名称
        String contNo = xdht0043DataReqDto.getContNo();//合同编号
        String certNo = xdht0043DataReqDto.getCertNo();//身份证号码
        String certType = xdht0043DataReqDto.getCertType();//身份证件类型
        String applyDate =xdht0043DataReqDto.getApplyDate();//申请日期
        String isdzqy = xdht0043DataReqDto.getIsdzqy();//是否电子签约:1-是 0-否

        try {
            if (StringUtils.isEmpty(contNo)) {
                throw new Exception("合同编号【contNo】不能为空！");
            }
            if (StringUtils.isEmpty(certNo)) {
                throw new Exception("证件号码【cert_code】不能为空！");
            }

            //根据证件号码查询cus服务
            CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
            String cusId = cusBaseClientDto.getCusId();
            CusIndivContactDto cusIndivContactDto = icusClientService.queryCusIndivByCusId(cusId);
            String postAddr = cusIndivContactDto.getSendAddr();//通讯地址
            String email = cusIndivContactDto.getEmail();//邮箱
            String qq = cusIndivContactDto.getQq();//qq号
            String wechat = cusIndivContactDto.getWechatNo();//微信号
            String agriFlg = cusIndivContactDto.getIsAgri();//是否农户

            String flag = "0";
            String message = "";
            if ("".equals(postAddr)) {
                flag = "2";
                message = "通讯地址不能为空";
            } else if ("".equals(qq)&&"".equals(email)&&"".equals(wechat)) {
                flag = "2";
                message = "邮箱、QQ、微信不能同时为空";
            } else if ("".equals(agriFlg)) {
                flag = "2";
                message = "是否农户不能为空";
            }
            if ("2".equals(flag)) {
                xdht0043DataRespDto.setFtpAddr(message);
                xdht0043DataRespDto.setPassword(flag);
                return xdht0043DataRespDto;
            }

            //根据contNo查询：SELECT cont_type, manager_br_id from ctr_loan_cont WHERE cont_no = '"+contNo+"'
            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            String contType = ctrLoanCont.getContType();
            String managerBrId = ctrLoanCont.getManagerBrId();

            String TempleteName = "";// 合同模板名字
            String saveFileName = "";// 合同另保存名字 （模板名字+合同申请流水号）

            if("1".equals(contType)){//一般借款合同
                TempleteName = "line_xwybjkht.cpt";// 合同模板名字 xdybjkht.docx
                saveFileName = "xdybjkht_" + contNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
            }
            if("2".equals(contType)){//最高额借款合同
                TempleteName = "line_xwzgejkht.cpt";// 合同模板名字 xdzgejkht.docx
                saveFileName = "xdzgejkht_" + contNo + "";// 合同另保存名字 （模板名字+合同申请流水号）
            }
            if("810100".equals(managerBrId) && !"".equals(TempleteName)){
                TempleteName = "dh" + TempleteName;
                saveFileName = "dh" + saveFileName;
            }else if("800100".equals(managerBrId) && !"".equals(TempleteName)){
                TempleteName = "sg" + TempleteName;
                saveFileName = "sg" + saveFileName;
            }

            // 查询参数
            QueryModel queryModel=new QueryModel();
            queryModel.addCondition("pkId","00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto=dtoList.get(0);
            //查询存储地址信息
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //调用帆软的生成pdf的方法
            //1、传入帆软报表生成需要的参数
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("cus_name", cusName);
            parameterMap.put("contNo", contNo);
            parameterMap.put("cert_no", certNo);
            parameterMap.put("cert_type", certType);
            parameterMap.put("apply_date", applyDate);
            parameterMap.put("post_addr", postAddr);
            parameterMap.put("qq", qq);
            parameterMap.put("email", email);
            parameterMap.put("wechat", wechat);
            parameterMap.put("cont_type", contType);
            parameterMap.put("manager_br_id", managerBrId);
            parameterMap.put("isdzqy", isdzqy);//是否电子签约:1-是 0-否

            //2、调用公共方法生成pdf(暂时先现在这里啦，后面转到工具类里面去)
            //2、调用公共方法生成pdf(暂时先现在这里啦，后面转到工具类里面去)
            //传入公共参数
            parameterMap.put("TempleteName",TempleteName);//模板名称（附带路径）
            parameterMap.put("saveFileName",saveFileName);//待生成的PDF文件名称
            parameterMap.put("path",path);
            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if(StringUtils.isEmpty(url)){
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try{
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//生成文件名称
                frptPdfArgsDto.setSerno(contNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                //HttpEntity<HashMap> entity = new HttpEntity<HashMap>(parameterMap,headers);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity= restTemplate.postForEntity(url,entity,String.class);
                String code = responseEntity.getBody();
            }catch (Exception e){
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }

            //3、返回参数
//            String pdfDepoAddr = saveFileName + "#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
            xdht0043DataRespDto.setPdfDepoAddr(path);
            xdht0043DataRespDto.setPdfFileName(saveFileName+".pdf");
            xdht0043DataRespDto.setFtpAddr(ip);
            xdht0043DataRespDto.setPort(port);
            xdht0043DataRespDto.setUserName(username);
            xdht0043DataRespDto.setPassword(password);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value);
        return xdht0043DataRespDto;
    }
}
