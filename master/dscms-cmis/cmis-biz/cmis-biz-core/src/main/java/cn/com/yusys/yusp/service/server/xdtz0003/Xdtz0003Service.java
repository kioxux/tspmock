package cn.com.yusys.yusp.service.server.xdtz0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.BizList;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.GuarList;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.Xdtz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.resp.Xdtz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdtz0001Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0003Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0003Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;

    /**
     * 查询小微借据余额
     *
     * @param xdtz0003DataReqDto
     * @return xdtz0003DataRespDto
     * @author xuchao
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0003DataRespDto getXdtz0003(Xdtz0003DataReqDto xdtz0003DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataReqDto));
        Xdtz0003DataRespDto xdtz0003DataRespDto = new Xdtz0003DataRespDto();
        try {
            Map param = new HashMap<>();
            //客户编号
            String cusId = xdtz0003DataReqDto.getCusNo();
            param.put("cusId", cusId);
            //业务类型列表,逗号分隔
            String bizList = xdtz0003DataReqDto.getBizType();
            List<String> bizTypeList = new ArrayList<>();
            if (bizList.contains(",")) {
                String[] bizTypeAry = bizList.split(",");
                for (int i = 0; i < bizTypeAry.length; i++) {
                    String guarCusName = bizTypeAry[i];
                    bizTypeList.add(guarCusName);
                }
            } else {
                bizTypeList.add(bizList);
            }
            param.put("bizList", bizTypeList);
            //担保方式列表,逗号分隔
            String guarList = xdtz0003DataReqDto.getAssureMeans();
            List<String> guarwayList = new ArrayList<>();
            if (guarList.contains(",")) {
                String[] guarWayAry = guarList.split(",");
                for (int i = 0; i < guarWayAry.length; i++) {
                    String guarCusName = guarWayAry[i];
                    guarwayList.add(guarCusName);
                }
            } else {
                guarwayList.add(guarList);
            }
            param.put("guarList", guarwayList);
            xdtz0003DataRespDto = accLoanMapper.getXWLoanBal(param);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataRespDto));
        return xdtz0003DataRespDto;
    }
}
