/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarZy;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysGuarZyMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarZyService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-02 23:10:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysGuarZyService {

    @Autowired
    private RptLmtRepayAnysGuarZyMapper rptLmtRepayAnysGuarZyMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLmtRepayAnysGuarZy selectByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarZyMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLmtRepayAnysGuarZy> selectAll(QueryModel model) {
        List<RptLmtRepayAnysGuarZy> records = (List<RptLmtRepayAnysGuarZy>) rptLmtRepayAnysGuarZyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLmtRepayAnysGuarZy> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnysGuarZy> list = rptLmtRepayAnysGuarZyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnysGuarZy record) {
        return rptLmtRepayAnysGuarZyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnysGuarZy record) {
        return rptLmtRepayAnysGuarZyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnysGuarZy record) {
        return rptLmtRepayAnysGuarZyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnysGuarZy record) {
        return rptLmtRepayAnysGuarZyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarZyMapper.deleteByPrimaryKey(pkId);
    }

    public int deleteBySerno(String serno) {
        return rptLmtRepayAnysGuarZyMapper.deleteBySerno(serno);
    }

    public List<RptLmtRepayAnysGuarZy> selectBySerno(String serno){
        return rptLmtRepayAnysGuarZyMapper.selectBySerno(serno);
    }

}
