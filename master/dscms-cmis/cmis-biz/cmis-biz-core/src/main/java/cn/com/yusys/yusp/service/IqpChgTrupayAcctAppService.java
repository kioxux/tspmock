/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.Ln3032ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.LstStzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.Ln3032RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpChgTrupayAcctAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.BizUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpChgTrupayAcctAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 屈文
 * @创建时间: 2021-04-14 20:06:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpChgTrupayAcctAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpHighAmtAgrAppService.class);
    @Autowired
    private IqpChgTrupayAcctAppMapper iqpChgTrupayAcctAppMapper;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private ToppAcctSubService toppAcctSubService;

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private ToppAccPayDetailService toppAccPayDetailService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpChgTrupayAcctApp selectByPrimaryKey(String pkId) {
        return iqpChgTrupayAcctAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpChgTrupayAcctApp> selectAll(QueryModel model) {
        List<IqpChgTrupayAcctApp> records = (List<IqpChgTrupayAcctApp>) iqpChgTrupayAcctAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpChgTrupayAcctApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpChgTrupayAcctApp> list = iqpChgTrupayAcctAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpChgTrupayAcctApp record) {
        return iqpChgTrupayAcctAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpChgTrupayAcctApp record) {
        return iqpChgTrupayAcctAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpChgTrupayAcctApp record) {
        return iqpChgTrupayAcctAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpChgTrupayAcctApp record) {
        return iqpChgTrupayAcctAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpChgTrupayAcctAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpChgTrupayAcctAppMapper.deleteByIds(ids);
    }

    /**
     * 修改受托账号申请新增页面点击下一步
     * @param map
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpChgTrupayAcctAppInfo(Map map) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (map == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }

            IqpChgTrupayAcctApp   iqpChgTrupayAcctApp = new IqpChgTrupayAcctApp();

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpChgTrupayAcctApp.setInputId(userInfo.getLoginCode());
                iqpChgTrupayAcctApp.setInputBrId(userInfo.getOrg().getCode());
                iqpChgTrupayAcctApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }


            Map seqMap = new HashMap();

            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, seqMap);

            iqpChgTrupayAcctApp.setCusId((String)map.get("cusId"));
            iqpChgTrupayAcctApp.setCusName((String)map.get("cusName"));
            iqpChgTrupayAcctApp.setContNo((String)map.get("contNo"));
            iqpChgTrupayAcctApp.setBillNo((String)map.get("billNo"));
            iqpChgTrupayAcctApp.setOrigiToppAccno((String)map.get("toppAcctNo"));//原交易对手账户
            iqpChgTrupayAcctApp.setOrigiToppName((String)map.get("toppName"));//原交易对手名称
            iqpChgTrupayAcctApp.setOrigiToppAmt(new BigDecimal((String)map.get("toppAmt")));//原交易对手金额
            iqpChgTrupayAcctApp.setOrigiToppBankno((String)map.get("acctsvcrNo"));//原交易对手开户行号
            iqpChgTrupayAcctApp.setOrigiToppBankname((String)map.get("acctsvcrName"));//原交易对手开户行名称
            iqpChgTrupayAcctApp.setToppAmt(new BigDecimal((String)map.get("toppAmt")));


            iqpChgTrupayAcctApp.setSerno(serno);
            iqpChgTrupayAcctApp.setPkId(StringUtils.uuid(true));
            iqpChgTrupayAcctApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpChgTrupayAcctApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            int insertCount = iqpChgTrupayAcctAppMapper.insertSelective(iqpChgTrupayAcctApp);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno",serno);
            log.info("修改受托账号申请"+serno+"-保存成功！");
        }catch(YuspException e){
            log.error("修改受托账号申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("保存修改受托账号申请异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 修改受托账号申请申请提交保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpChgTrupayAcctAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "修改受托账号申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpChgTrupayAcctApp iqpChgTrupayAcctApp = JSONObject.parseObject(JSON.toJSONString(params), IqpChgTrupayAcctApp.class);
            if (iqpChgTrupayAcctApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存修改受托账号申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpChgTrupayAcctApp.setUpdId(userInfo.getLoginCode());
                iqpChgTrupayAcctApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpChgTrupayAcctApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存修改受托账号申请数据");
            int updCount = iqpChgTrupayAcctAppMapper.updateByPrimaryKeySelective(iqpChgTrupayAcctApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存修改受托账号申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpChgTrupayAcctApp> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpChgTrupayAcctAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpChgTrupayAcctApp> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpChgTrupayAcctAppMapper.selectByModel(model);
    }


    /**
     * 受托支付信息修改发送核心系统
     * @param iqpChgTrupayAcctApp
     * @return
     */
    public Map sendCore(IqpChgTrupayAcctApp iqpChgTrupayAcctApp) {
        AccLoan queryDataFromAccLoan = accLoanService.queryAccLoanDataByBillNo(iqpChgTrupayAcctApp.getBillNo());
        ToppAcctSub queryDataFromToppAcctSub = toppAcctSubService.queryToppAcctSubDataByKey(queryDataFromAccLoan.getPvpSerno());
        Map result = new HashMap();
        List lstStzfs = new ArrayList();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try{
            Ln3032ReqDto reqDto = new Ln3032ReqDto();
            List<LstStzf> lstStzfList = new ArrayList<>();
            LstStzf lstStzf = new LstStzf();
            //业务操作方式 1--录入 2--修改 3--复核 4--直通
            reqDto.setDkkhczbz("4");
            //贷款借据号
            reqDto.setDkjiejuh(iqpChgTrupayAcctApp.getBillNo());
            //出账号
            reqDto.setChuzhhao("");
            //贷款账号
            reqDto.setDkzhangh("");
            //复核机构（字段不明确，暂以放款机构替代）
            reqDto.setFuhejgou("");
            //todo 暂无是否线上字段  老信贷系统表ChangeTrusteePay
            //渠道号 (XDG信贷管理系统渠道号)
            lstStzf.setQudaohao("NET");
            //序号（字段不明确，暂以业务主键号替代）
            lstStzf.setBeizhuxx("");
            //冻结编号（字段不明确，暂以贷款账号替代）
            lstStzf.setDjiebhao("");
            //受托金额（字段不明确，暂以贷款金额替代）
            lstStzf.setStzfjine(iqpChgTrupayAcctApp.getToppAmt());
            //对方账号种类（字段不明确，暂以贷款种类替代）
            //todo 是否本行账户
            lstStzf.setDfzhhzhl("2");//对方账号种类：1--本行账号、2--他行账号
            //对方账号开户行
            lstStzf.setDfzhhkhh(iqpChgTrupayAcctApp.getToppBankNo());
            //对方账号开户行名
            lstStzf.setDfzhkhhm(iqpChgTrupayAcctApp.getToppBankName());
            //对方账号
            lstStzf.setDfzhangh(iqpChgTrupayAcctApp.getToppAccno());
            //对方账号子序号（字段不明确，暂以发放账号子序号替代）
            lstStzf.setDfzhhzxh("");
            //对方账号名称
            lstStzf.setDfzhhmch(iqpChgTrupayAcctApp.getToppName());
            //受托支付方式（字段不明确，暂以还款方式替代） // 受托支付方式:1--手工支付、2--自动支付、3--立即支付
            //todo 暂无受托支付方式
            lstStzf.setStzffshi("1");
            //受托支付日期（字段不明确，暂以结清日期代替）
            lstStzf.setStzfriqi("");
            //受托支付处理状态（字段不明确，暂以台账状态）// 受托支付处理状态:0--未支付、1--已支付
            lstStzf.setStzfclzt("0");
            //备注
            lstStzf.setBeizhuxx("备注");
            lstStzfList.add(lstStzf);
            reqDto.setLstStzf(lstStzfList);

            ResultDto<Ln3032RespDto> respDto = dscms2CoreLnClientService.ln3032(reqDto);
            lstStzfs =respDto.getData().getLstStzf();
            queryDataFromToppAcctSub.setToppAcctNo(iqpChgTrupayAcctApp.getToppAccno());

            result.put("LstStzf",lstStzfs);
            String code = respDto.getCode();
            if("0".equals(respDto.getCode())){
                String jiaoyils = respDto.getData().getJiaoyils();
                iqpChgTrupayAcctApp.setHxSerno(jiaoyils);
                iqpChgTrupayAcctAppMapper.updateByPrimaryKey(iqpChgTrupayAcctApp);
            }else{
                rtnCode = respDto.getCode();
                rtnMsg = respDto.getMessage();
            }


        }catch(YuspException e){
            log.error("修改异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("修改异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
            result.put("LstStzf",lstStzfs);
        }
        return result;
    }

    @Transactional
    public Map deleteIqpChgtruPayAcctapp(String iqpSerno) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            log.info(String.format("根据主键%s对修改受托账号进行逻辑删除", iqpSerno));
            IqpChgTrupayAcctApp iqpChgTrupayAcctApp = iqpChgTrupayAcctAppMapper.selectByIqpChgTrupayAcctAppSerno(iqpSerno);
            iqpChgTrupayAcctApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);

            log.info(String.format("根据主键%s对修改受托账号进行逻辑删除,获取用户登录信息", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpChgTrupayAcctApp.setUpdId(userInfo.getLoginCode());
                iqpChgTrupayAcctApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpChgTrupayAcctApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对修改受托账号进行逻辑删除", iqpSerno));
            //iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            iqpChgTrupayAcctAppMapper.deleteByPrimaryKey(iqpChgTrupayAcctApp.getPkId());
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除修改受托账号出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateApproveStatus(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("审批通过-修改受托账号申请" + iqpSerno + "申请主表信息");
            IqpChgTrupayAcctApp iqpChgTrupayAcctApp = iqpChgTrupayAcctAppMapper.selectBySerno(iqpSerno);
            if (iqpChgTrupayAcctApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            int updateCount = 0;
            log.info("审批通过-修改受托账号申请" + iqpSerno + "流程审批状态为【997】-通过");
            updateCount = iqpChgTrupayAcctAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }

        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("修改受托账号申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpChgTrupayAcctApp selectByiqpChgTrupayAcctSernoKey(String iqpSerno) {
        return iqpChgTrupayAcctAppMapper.selectBySerno(iqpSerno);
    }


    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 受托支付账号变更审批流程发起逻辑处理
     * @参数与返回说明
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String pkId) throws Exception {
        // 1.将审批状态更新为111
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = selectByPrimaryKey(pkId);
        iqpChgTrupayAcctApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        this.update(iqpChgTrupayAcctApp);
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 受托支付账号变更审批流程拒绝逻辑处理
     * @参数与返回说明
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterRefuse(String pkId) throws Exception {
        // 1.将审批状态更新为998
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = selectByPrimaryKey(pkId);
        iqpChgTrupayAcctApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        this.update(iqpChgTrupayAcctApp);
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 受托支付账号变更审批流程打回逻辑处理
     * @参数与返回说明
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterBack(String pkId) throws Exception {
        // 1.将审批状态更新为998
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = selectByPrimaryKey(pkId);
        iqpChgTrupayAcctApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        this.update(iqpChgTrupayAcctApp);
    }


    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 受托支付账号变更审批流程通过逻辑处理
     * @参数与返回说明
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterEnd(String pkId, String currentUserId, String currentOrgId) throws Exception {
        // 1.将审批状态更新为997 通过
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = selectByPrimaryKey(pkId);
        iqpChgTrupayAcctApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        int i = this.update(iqpChgTrupayAcctApp);
        if(i > 0) {
            toppAccPayDetailService.createToppAccPayDetailForChange(iqpChgTrupayAcctApp);
        }
    }

}
