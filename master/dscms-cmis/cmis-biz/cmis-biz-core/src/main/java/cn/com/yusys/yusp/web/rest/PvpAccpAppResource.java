/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.service.PvpAccpAppService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAccpAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-21 09:13:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "银承出账申请")
@RequestMapping("/api/pvpaccpapp")
public class PvpAccpAppResource {
    @Autowired
    private PvpAccpAppService pvpAccpAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpAccpApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpAccpApp> list = pvpAccpAppService.selectAll(queryModel);
        return new ResultDto<List<PvpAccpApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpAccpApp>> index(QueryModel queryModel) {
        List<PvpAccpApp> list = pvpAccpAppService.selectByModel(queryModel);
        return new ResultDto<List<PvpAccpApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpAccpApp> show(@PathVariable("pkId") String pkId) {
        PvpAccpApp pvpAccpApp = pvpAccpAppService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpAccpApp>(pvpAccpApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpAccpApp> create(@RequestBody PvpAccpApp pvpAccpApp) throws URISyntaxException {
        pvpAccpAppService.insert(pvpAccpApp);
        return new ResultDto<PvpAccpApp>(pvpAccpApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpAccpApp pvpAccpApp) throws URISyntaxException {
        int result = pvpAccpAppService.update(pvpAccpApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpAccpAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpAccpAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:handInvaild
     * @函数描述:手工作废
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("手工作废")
    @PostMapping("/handinvaild")
    public ResultDto<Integer> handInvaild(@RequestBody String pvpSerno) {
        return new ResultDto<Integer>(pvpAccpAppService.handInvaild(pvpSerno));
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:待发起申请列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("待发起申请列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<PvpAccpApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAccpApp> list = pvpAccpAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpAccpApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:已处理申请列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("已处理申请列表")
    @PostMapping("/donesignlist")
    protected ResultDto<List<PvpAccpApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAccpApp> list = pvpAccpAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpAccpApp>>(list);
    }
    /**
     * @函数名称:doneSignlist
     * @函数描述:已处理申请列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("POP框查询资产池银承台账列表")
    @PostMapping("/toSelectAspl")
    protected ResultDto<List<PvpAccpApp>> toSelectAspl(@RequestBody QueryModel queryModel) {
        queryModel.setSort("pvpSerno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<PvpAccpApp> list = pvpAccpAppService.toSelectAspl(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<PvpAccpApp>>(list);
    }
    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno((String) params.get("pvpSerno"));
        if (pvpAccpApp != null) {
            resultDto.setCode(200);
            resultDto.setData(pvpAccpApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * 通用的保存方法
     * @param pvpAccpApp
     * @return
     */
    @ApiOperation("保存方法")
    @PostMapping("/commonupdatepvpaccpapp")
    public ResultDto<Map> commonUpdatePvpAccpApp(@RequestBody PvpAccpApp pvpAccpApp){
        Map rtnData = pvpAccpAppService.commonUpdatePvpAccpApp(pvpAccpApp);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称：sendpvpauth
     * @方法描述：获取合同生成信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zxz
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("调用生成方法")
    @PostMapping("/sendpvpauth")
    protected  ResultDto<Object> sendPvpAuth(@RequestBody Map map) {
        ResultDto<Object> result = new ResultDto<>();
        String serno = (String)map.get("serno");
        pvpAccpAppService.handleBusinessDataAfterEnd(serno);
        return  result;
    }

    /**
     * @方法名称：sendpvpauth
     * @方法描述：根据合同编号查询合同信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：xs
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("根据合同编号查询合同信息")
    @PostMapping("/selectContByContNo")
    protected  ResultDto<Map<String,String>> selectContByContNo(@RequestBody Map map) {
        String contNo = (String)map.get("contNo");
        return pvpAccpAppService.selectContByContNo(contNo);
    }

    /**
     * @方法名称：synAccpDetail
     * @方法描述：同步票据明细
     * @参数与返回说明：
     * @算法描述：
     * @创建人：lihh
     * @创建时间：2021-04-28 下午 6:52
     * @修改记录：修改时间 修改人员  修改原因
     */
    @ApiOperation("同步票据明细")
    @PostMapping("/synAccpDetail")
    protected  ResultDto synAccpDetail(@RequestBody String serno) {
        return pvpAccpAppService.synAccpDetail(serno);
    }
}
