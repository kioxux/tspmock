/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarCont
 * @类描述: grt_guar_cont数据实体类
 * @功能描述:
 * @创建人: 刘权
 * @创建时间: 2021-05-05 18:50:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "grt_guar_cont")
public class GrtGuarCont extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 担保合同流水号  **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GUAR_PK_ID")
	private String guarPkId;

	/** 担保合同编号  **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;

	/** 担保合同中文合同编号 **/
	@Column(name = "GUAR_CONT_CN_NO", unique = false, nullable = true, length = 80)
	private String guarContCnNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 是否追加担保  STD_ZB_YES_NO **/
	@Column(name = "IS_SUPERADD_GUAR", unique = false, nullable = true, length = 5)
	private String isSuperaddGuar;

	/** 担保合同类型 STD_ZB_GRT_TYP **/
	@Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String guarContType;

	/** 担保合同担保方式 STD_ZB_GUAR_WAY **/
	@Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
	private String guarWay;

	/** 保证形式 STD_ZB_ASSURE_MODAL **/
	@Column(name = "ASSURE_MODAL", unique = false, nullable = true, length = 5)
	private String assureModal;

	/** 保证方式 **/
	@Column(name = "ASSURE_WAY", unique = false, nullable = true, length = 5)
	private String assureWay;

	/** 纸质合同编号 **/
	@Column(name = "PAPER_CONT_NO", unique = false, nullable = true, length = 40)
	private String paperContNo;

	/** 是否在线抵押 **/
	@Column(name = "IS_ONLINE_PLD", unique = false, nullable = true, length = 5)
	private String isOnlinePld;

	/** 抵质押合同类型 **/
	@Column(name = "PLD_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String pldContType;

	/** 是否授信项下 STD_ZB_YES_NO **/
	@Column(name = "IS_UNDER_LMT", unique = false, nullable = true, length = 5)
	private String isUnderLmt;

	/** 抵押顺位 **/
	@Column(name = "PLD_ORDER", unique = false, nullable = true, length = 5)
	private String pldOrder;

	/** 是否浮动抵押 **/
	@Column(name = "IS_FLOAT_PLD", unique = false, nullable = true, length = 5)
	private String isFloatPld;

	/** 借款人编号 **/
	@Column(name = "BORROWER_ID", unique = false, nullable = true, length = 30)
	private String borrowerId;

	/** 贷款卡号 **/
	@Column(name = "LN_CARD_NO", unique = false, nullable = true, length = 40)
	private String lnCardNo;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 是否合格担保 STD_ZB_YES_NO **/
	@Column(name = "IS_QUALIFIED_GUAR", unique = false, nullable = true, length = 5)
	private String isQualifiedGuar;

	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;

	/** 期限类型 STD_ZB_TERM_TYP **/
	@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
	private String termType;

	/** 担保期限 **/
	@Column(name = "GUAR_TERM", unique = false, nullable = true, length = 10)
	private Integer guarTerm;

	/** 担保起始日 **/
	@Column(name = "GUAR_START_DATE", unique = false, nullable = true, length = 20)
	private String guarStartDate;

	/** 担保终止日 **/
	@Column(name = "GUAR_END_DATE", unique = false, nullable = true, length = 20)
	private String guarEndDate;

	/** 签订日期 **/
	@Column(name = "SIGN_DATE", unique = false, nullable = true, length = 20)
	private String signDate;

	/** 质押类型 STD_ZB_IMN_TYPE **/
	@Column(name = "IMN_TYPE", unique = false, nullable = true, length = 5)
	private String imnType;

	/** 担保合同状态 STD_ZB_GRT_ST **/
	@Column(name = "GUAR_CONT_STATE", unique = false, nullable = true, length = 5)
	private String guarContState;

	/** 担保双录编号 **/
	@Column(name = "GUAR_ISERCH_NO", unique = false, nullable = true, length = 40)
	private String guarIserchNo;

	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;

	/** 争议借据方式选项 **/
	@Column(name = "BILL_DISPUPE_OPT", unique = false, nullable = true, length = 5)
	private String billDispupeOpt;

	/** 法院所在地 **/
	@Column(name = "COURT_ADDR", unique = false, nullable = true, length = 200)
	private String courtAddr;

	/** 仲裁委员会地点 **/
	@Column(name = "ARBITRATE_ADDR", unique = false, nullable = true, length = 200)
	private String arbitrateAddr;

	/** 其他方式 **/
	@Column(name = "OTHER_OPT", unique = false, nullable = true, length = 200)
	private String otherOpt;

	/** 仲裁机构 **/
	@Column(name = "ARBITRATE_BCH", unique = false, nullable = true, length = 400)
	private String arbitrateBch;

	/** 合同份数 **/
	@Column(name = "CONT_QNT", unique = false, nullable = true, length = 10)
	private Integer contQnt;

	/** 甲方执合同份数 **/
	@Column(name = "CONT_QNT_OWNER", unique = false, nullable = true, length = 10)
	private Integer contQntOwner;

	/** 乙方执合同份数 **/
	@Column(name = "CONT_QNT_PARTY_B", unique = false, nullable = true, length = 10)
	private Integer contQntPartyB;

	/** 丙方执合同份数 **/
	@Column(name = "CONT_QNT_PARTY_C", unique = false, nullable = true, length = 10)
	private Integer contQntPartyC;

	/** 丁方执合同份数 **/
	@Column(name = "CONT_QNT_PARTY_D", unique = false, nullable = true, length = 10)
	private Integer contQntPartyD;

	/** 戊方执合同份数 **/
	@Column(name = "CONT_QNT_PARTY_E", unique = false, nullable = true, length = 10)
	private Integer contQntPartyE;

	/** 其他主合同 **/
	@Column(name = "OTHER_MAIN_CONT", unique = false, nullable = true, length = 40)
	private String otherMainCont;

	/** 确认最高债权额方式 STD_ZB_MAX_CLAIM_TP **/
	@Column(name = "MAX_CLAIM_TP", unique = false, nullable = true, length = 5)
	private String maxClaimTp;

	/** 最高债权额 **/
	@Column(name = "MAX_CLAIM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maxClaimAmt;

	/** 其他约定事项 **/
	@Column(name = "OTHER_AGREED_EVENT", unique = false, nullable = true, length = 4000)
	private String otherAgreedEvent;

	/** 合同打印次数 **/
	@Column(name = "CONT_PRINT_NUM", unique = false, nullable = true, length = 10)
	private Integer contPrintNum;

	/** 申请类型 STD_ZB_GRT_APP_TYP **/
	@Column(name = "APPROVE_TYPE", unique = false, nullable = true, length = 5)
	private String approveType;

	/** 注销日期 **/
	@Column(name = "LOGOUT_DATE", unique = false, nullable = true, length = 20)
	private String logoutDate;

	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private String updDate;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 担保人编号 **/
	@Column(name = "GUARANTOR_ID", unique = false, nullable = true, length = 30)
	private String guarantorId;

	/** 担保人名称 **/
	@Column(name = "ASSURE_NAME", unique = false, nullable = true, length = 80)
	private String assureName;

	/** 担保人证件类型 **/
	@Column(name = "ASSURE_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String assureCertType;

	/** 业务条线 **/
	@Column(name = "BIZ_LINE", unique = false, nullable = true, length = 40)
	private String bizLine;

	/** 担保人证件号码 **/
	@Column(name = "ASSURE_CERT_CODE", unique = false, nullable = true, length = 40)
	private String assureCertCode;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;

	/** 是否将存量债务纳入担保范围 **/
	@Column(name = "IS_DEBT_GUAR", unique = false, nullable = true, length = 5)
	private String isDebtGuar;

	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId;
	}

	/**
	 * @return guarPkId
	 */
	public String getGuarPkId() {
		return this.guarPkId;
	}

	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}

	/**
	 * @return guarContNo
	 */
	public String getGuarContNo() {
		return this.guarContNo;
	}

	/**
	 * @param guarContCnNo
	 */
	public void setGuarContCnNo(String guarContCnNo) {
		this.guarContCnNo = guarContCnNo;
	}

	/**
	 * @return guarContCnNo
	 */
	public String getGuarContCnNo() {
		return this.guarContCnNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param isSuperaddGuar
	 */
	public void setIsSuperaddGuar(String isSuperaddGuar) {
		this.isSuperaddGuar = isSuperaddGuar;
	}

	/**
	 * @return isSuperaddGuar
	 */
	public String getIsSuperaddGuar() {
		return this.isSuperaddGuar;
	}

	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}

	/**
	 * @return guarContType
	 */
	public String getGuarContType() {
		return this.guarContType;
	}

	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}

	/**
	 * @return guarWay
	 */
	public String getGuarWay() {
		return this.guarWay;
	}

	/**
	 * @param assureModal
	 */
	public void setAssureModal(String assureModal) {
		this.assureModal = assureModal;
	}

	/**
	 * @return assureModal
	 */
	public String getAssureModal() {
		return this.assureModal;
	}

	/**
	 * @param assureWay
	 */
	public void setAssureWay(String assureWay) {
		this.assureWay = assureWay;
	}

	/**
	 * @return assureWay
	 */
	public String getAssureWay() {
		return this.assureWay;
	}

	/**
	 * @param paperContNo
	 */
	public void setPaperContNo(String paperContNo) {
		this.paperContNo = paperContNo;
	}

	/**
	 * @return paperContNo
	 */
	public String getPaperContNo() {
		return this.paperContNo;
	}

	/**
	 * @param isOnlinePld
	 */
	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld;
	}

	/**
	 * @return isOnlinePld
	 */
	public String getIsOnlinePld() {
		return this.isOnlinePld;
	}

	/**
	 * @param pldContType
	 */
	public void setPldContType(String pldContType) {
		this.pldContType = pldContType;
	}

	/**
	 * @return pldContType
	 */
	public String getPldContType() {
		return this.pldContType;
	}

	/**
	 * @param isUnderLmt
	 */
	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt;
	}

	/**
	 * @return isUnderLmt
	 */
	public String getIsUnderLmt() {
		return this.isUnderLmt;
	}

	/**
	 * @param pldOrder
	 */
	public void setPldOrder(String pldOrder) {
		this.pldOrder = pldOrder;
	}

	/**
	 * @return pldOrder
	 */
	public String getPldOrder() {
		return this.pldOrder;
	}

	/**
	 * @param isFloatPld
	 */
	public void setIsFloatPld(String isFloatPld) {
		this.isFloatPld = isFloatPld;
	}

	/**
	 * @return isFloatPld
	 */
	public String getIsFloatPld() {
		return this.isFloatPld;
	}

	/**
	 * @param borrowerId
	 */
	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId;
	}

	/**
	 * @return borrowerId
	 */
	public String getBorrowerId() {
		return this.borrowerId;
	}

	/**
	 * @param lnCardNo
	 */
	public void setLnCardNo(String lnCardNo) {
		this.lnCardNo = lnCardNo;
	}

	/**
	 * @return lnCardNo
	 */
	public String getLnCardNo() {
		return this.lnCardNo;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param isQualifiedGuar
	 */
	public void setIsQualifiedGuar(String isQualifiedGuar) {
		this.isQualifiedGuar = isQualifiedGuar;
	}

	/**
	 * @return isQualifiedGuar
	 */
	public String getIsQualifiedGuar() {
		return this.isQualifiedGuar;
	}

	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}

	/**
	 * @return guarAmt
	 */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}

	/**
	 * @return termType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param guarTerm
	 */
	public void setGuarTerm(Integer guarTerm) {
		this.guarTerm = guarTerm;
	}

	/**
	 * @return guarTerm
	 */
	public Integer getGuarTerm() {
		return this.guarTerm;
	}

	/**
	 * @param guarStartDate
	 */
	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate;
	}

	/**
	 * @return guarStartDate
	 */
	public String getGuarStartDate() {
		return this.guarStartDate;
	}

	/**
	 * @param guarEndDate
	 */
	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate;
	}

	/**
	 * @return guarEndDate
	 */
	public String getGuarEndDate() {
		return this.guarEndDate;
	}

	/**
	 * @param signDate
	 */
	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	/**
	 * @return signDate
	 */
	public String getSignDate() {
		return this.signDate;
	}

	/**
	 * @param imnType
	 */
	public void setImnType(String imnType) {
		this.imnType = imnType;
	}

	/**
	 * @return imnType
	 */
	public String getImnType() {
		return this.imnType;
	}

	/**
	 * @param guarContState
	 */
	public void setGuarContState(String guarContState) {
		this.guarContState = guarContState;
	}

	/**
	 * @return guarContState
	 */
	public String getGuarContState() {
		return this.guarContState;
	}

	/**
	 * @param guarIserchNo
	 */
	public void setGuarIserchNo(String guarIserchNo) {
		this.guarIserchNo = guarIserchNo;
	}

	/**
	 * @return guarIserchNo
	 */
	public String getGuarIserchNo() {
		return this.guarIserchNo;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return remark
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param billDispupeOpt
	 */
	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt;
	}

	/**
	 * @return billDispupeOpt
	 */
	public String getBillDispupeOpt() {
		return this.billDispupeOpt;
	}

	/**
	 * @param courtAddr
	 */
	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr;
	}

	/**
	 * @return courtAddr
	 */
	public String getCourtAddr() {
		return this.courtAddr;
	}

	/**
	 * @param arbitrateAddr
	 */
	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr;
	}

	/**
	 * @return arbitrateAddr
	 */
	public String getArbitrateAddr() {
		return this.arbitrateAddr;
	}

	/**
	 * @param otherOpt
	 */
	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt;
	}

	/**
	 * @return otherOpt
	 */
	public String getOtherOpt() {
		return this.otherOpt;
	}

	/**
	 * @param arbitrateBch
	 */
	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch;
	}

	/**
	 * @return arbitrateBch
	 */
	public String getArbitrateBch() {
		return this.arbitrateBch;
	}

	/**
	 * @param contQnt
	 */
	public void setContQnt(Integer contQnt) {
		this.contQnt = contQnt;
	}

	/**
	 * @return contQnt
	 */
	public Integer getContQnt() {
		return this.contQnt;
	}

	/**
	 * @param contQntOwner
	 */
	public void setContQntOwner(Integer contQntOwner) {
		this.contQntOwner = contQntOwner;
	}

	/**
	 * @return contQntOwner
	 */
	public Integer getContQntOwner() {
		return this.contQntOwner;
	}

	/**
	 * @param contQntPartyB
	 */
	public void setContQntPartyB(Integer contQntPartyB) {
		this.contQntPartyB = contQntPartyB;
	}

	/**
	 * @return contQntPartyB
	 */
	public Integer getContQntPartyB() {
		return this.contQntPartyB;
	}

	/**
	 * @param contQntPartyC
	 */
	public void setContQntPartyC(Integer contQntPartyC) {
		this.contQntPartyC = contQntPartyC;
	}

	/**
	 * @return contQntPartyC
	 */
	public Integer getContQntPartyC() {
		return this.contQntPartyC;
	}

	/**
	 * @param contQntPartyD
	 */
	public void setContQntPartyD(Integer contQntPartyD) {
		this.contQntPartyD = contQntPartyD;
	}

	/**
	 * @return contQntPartyD
	 */
	public Integer getContQntPartyD() {
		return this.contQntPartyD;
	}

	/**
	 * @param contQntPartyE
	 */
	public void setContQntPartyE(Integer contQntPartyE) {
		this.contQntPartyE = contQntPartyE;
	}

	/**
	 * @return contQntPartyE
	 */
	public Integer getContQntPartyE() {
		return this.contQntPartyE;
	}

	/**
	 * @param otherMainCont
	 */
	public void setOtherMainCont(String otherMainCont) {
		this.otherMainCont = otherMainCont;
	}

	/**
	 * @return otherMainCont
	 */
	public String getOtherMainCont() {
		return this.otherMainCont;
	}

	/**
	 * @param maxClaimTp
	 */
	public void setMaxClaimTp(String maxClaimTp) {
		this.maxClaimTp = maxClaimTp;
	}

	/**
	 * @return maxClaimTp
	 */
	public String getMaxClaimTp() {
		return this.maxClaimTp;
	}

	/**
	 * @param maxClaimAmt
	 */
	public void setMaxClaimAmt(java.math.BigDecimal maxClaimAmt) {
		this.maxClaimAmt = maxClaimAmt;
	}

	/**
	 * @return maxClaimAmt
	 */
	public java.math.BigDecimal getMaxClaimAmt() {
		return this.maxClaimAmt;
	}

	/**
	 * @param otherAgreedEvent
	 */
	public void setOtherAgreedEvent(String otherAgreedEvent) {
		this.otherAgreedEvent = otherAgreedEvent;
	}

	/**
	 * @return otherAgreedEvent
	 */
	public String getOtherAgreedEvent() {
		return this.otherAgreedEvent;
	}

	/**
	 * @param contPrintNum
	 */
	public void setContPrintNum(Integer contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	/**
	 * @return contPrintNum
	 */
	public Integer getContPrintNum() {
		return this.contPrintNum;
	}

	/**
	 * @param approveType
	 */
	public void setApproveType(String approveType) {
		this.approveType = approveType;
	}

	/**
	 * @return approveType
	 */
	public String getApproveType() {
		return this.approveType;
	}

	/**
	 * @param logoutDate
	 */
	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	/**
	 * @return logoutDate
	 */
	public String getLogoutDate() {
		return this.logoutDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param guarantorId
	 */
	public void setGuarantorId(String guarantorId) {
		this.guarantorId = guarantorId;
	}

	/**
	 * @return guarantorId
	 */
	public String getGuarantorId() {
		return this.guarantorId;
	}

	/**
	 * @param assureName
	 */
	public void setAssureName(String assureName) {
		this.assureName = assureName;
	}

	/**
	 * @return assureName
	 */
	public String getAssureName() {
		return this.assureName;
	}

	/**
	 * @param assureCertType
	 */
	public void setAssureCertType(String assureCertType) {
		this.assureCertType = assureCertType;
	}

	/**
	 * @return assureCertType
	 */
	public String getAssureCertType() {
		return this.assureCertType;
	}

	/**
	 * @param bizLine
	 */
	public void setBizLine(String bizLine) {
		this.bizLine = bizLine;
	}

	/**
	 * @return bizLine
	 */
	public String getBizLine() {
		return this.bizLine;
	}

	/**
	 * @param assureCertCode
	 */
	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode;
	}

	/**
	 * @return assureCertCode
	 */
	public String getAssureCertCode() {
		return this.assureCertCode;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param isDebtGuar
	 */
	public void setIsDebtGuar(String isDebtGuar) {
		this.isDebtGuar = isDebtGuar;
	}

	/**
	 * @return isDebtGuar
	 */
	public String getIsDebtGuar() {
		return this.isDebtGuar;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "GrtGuarCont{" +
				"guarPkId='" + guarPkId + '\'' +
				", guarContNo='" + guarContNo + '\'' +
				", guarContCnNo='" + guarContCnNo + '\'' +
				", cusId='" + cusId + '\'' +
				", isSuperaddGuar='" + isSuperaddGuar + '\'' +
				", guarContType='" + guarContType + '\'' +
				", guarWay='" + guarWay + '\'' +
				", assureModal='" + assureModal + '\'' +
				", assureWay='" + assureWay + '\'' +
				", paperContNo='" + paperContNo + '\'' +
				", isOnlinePld='" + isOnlinePld + '\'' +
				", pldContType='" + pldContType + '\'' +
				", isUnderLmt='" + isUnderLmt + '\'' +
				", pldOrder='" + pldOrder + '\'' +
				", isFloatPld='" + isFloatPld + '\'' +
				", borrowerId='" + borrowerId + '\'' +
				", lnCardNo='" + lnCardNo + '\'' +
				", curType='" + curType + '\'' +
				", isQualifiedGuar='" + isQualifiedGuar + '\'' +
				", guarAmt=" + guarAmt +
				", termType='" + termType + '\'' +
				", guarTerm=" + guarTerm +
				", guarStartDate='" + guarStartDate + '\'' +
				", guarEndDate='" + guarEndDate + '\'' +
				", signDate='" + signDate + '\'' +
				", imnType='" + imnType + '\'' +
				", guarContState='" + guarContState + '\'' +
				", guarIserchNo='" + guarIserchNo + '\'' +
				", remark='" + remark + '\'' +
				", billDispupeOpt='" + billDispupeOpt + '\'' +
				", courtAddr='" + courtAddr + '\'' +
				", arbitrateAddr='" + arbitrateAddr + '\'' +
				", otherOpt='" + otherOpt + '\'' +
				", arbitrateBch='" + arbitrateBch + '\'' +
				", contQnt=" + contQnt +
				", contQntOwner=" + contQntOwner +
				", contQntPartyB=" + contQntPartyB +
				", contQntPartyC=" + contQntPartyC +
				", contQntPartyD=" + contQntPartyD +
				", contQntPartyE=" + contQntPartyE +
				", otherMainCont='" + otherMainCont + '\'' +
				", maxClaimTp='" + maxClaimTp + '\'' +
				", maxClaimAmt=" + maxClaimAmt +
				", otherAgreedEvent='" + otherAgreedEvent + '\'' +
				", contPrintNum=" + contPrintNum +
				", approveType='" + approveType + '\'' +
				", logoutDate='" + logoutDate + '\'' +
				", managerId='" + managerId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate='" + inputDate + '\'' +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate='" + updDate + '\'' +
				", oprType='" + oprType + '\'' +
				", guarantorId='" + guarantorId + '\'' +
				", assureName='" + assureName + '\'' +
				", assureCertType='" + assureCertType + '\'' +
				", bizLine='" + bizLine + '\'' +
				", assureCertCode='" + assureCertCode + '\'' +
				", cusName='" + cusName + '\'' +
				", isDebtGuar='" + isDebtGuar + '\'' +
				", lmtAccNo='" + lmtAccNo + '\'' +
				", replyNo='" + replyNo + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
