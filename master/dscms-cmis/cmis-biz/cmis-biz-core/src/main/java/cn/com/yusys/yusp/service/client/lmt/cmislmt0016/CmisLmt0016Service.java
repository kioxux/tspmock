package cn.com.yusys.yusp.service.client.lmt.cmislmt0016;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.resp.CmisLmt0016RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class CmisLmt0016Service
 * @date 2021/8/25 19:10
 * @desc 校验合同额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0016Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0016Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param CmisLmt0016ReqDto
     * @return cn.com.yusys.yusp.dto.server.CmisLmt0016.resp.CmisLmt0016RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 校验合同额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0016RespDto cmisLmt0016(CmisLmt0016ReqDto CmisLmt0016ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, JSON.toJSONString(CmisLmt0016ReqDto));
        ResultDto<CmisLmt0016RespDto> CmisLmt0016ResultDto = cmisLmtClientService.cmisLmt0016(CmisLmt0016ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value, JSON.toJSONString(CmisLmt0016ResultDto));

        String CmisLmt0016Code = Optional.ofNullable(CmisLmt0016ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String CmisLmt0016Meesage = Optional.ofNullable(CmisLmt0016ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0016RespDto CmisLmt0016RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, CmisLmt0016ResultDto.getCode()) && CmisLmt0016ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, CmisLmt0016ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            CmisLmt0016RespDto = CmisLmt0016ResultDto.getData();
        } else {
            if (CmisLmt0016ResultDto.getData() != null) {
                CmisLmt0016Code = CmisLmt0016ResultDto.getData().getErrorCode();
                CmisLmt0016Meesage = CmisLmt0016ResultDto.getData().getErrorMsg();
            } else {
                CmisLmt0016Code = EpbEnum.EPB099999.key;
                CmisLmt0016Code = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(CmisLmt0016Code, CmisLmt0016Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0016.value);
        return CmisLmt0016RespDto;
    }
}
