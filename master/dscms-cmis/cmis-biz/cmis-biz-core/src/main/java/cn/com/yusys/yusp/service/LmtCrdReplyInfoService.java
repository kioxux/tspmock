/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.IqpAppReply;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.UserAndDutyReqDto;
import cn.com.yusys.yusp.dto.UserAndDutyRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.impl.Dscms2ImageClientServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-20 14:05:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtCrdReplyInfoService {

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CommonService commonService;

    private static final Logger logger = LoggerFactory.getLogger(Dscms2ImageClientServiceImpl.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtCrdReplyInfo selectByPrimaryKey(String replyNo) {
        return lmtCrdReplyInfoMapper.selectByPrimaryKey(replyNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtCrdReplyInfo> selectAll(QueryModel model) {
        List<LmtCrdReplyInfo> records = (List<LmtCrdReplyInfo>) lmtCrdReplyInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/29 15:40
     * @注释 批复的公共查询方法
     */
    public List<LmtCrdReplyInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 给小微查批复用的
     * @算法描述: 无
     */

    public List<LmtCrdReplyInfo> selectByModelxw(QueryModel model) {
        UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
        String managerId = model.getCondition().get("managerId").toString();
        userAndDutyReqDto.setManagerId(managerId);
        List<UserAndDutyRespDto> userAndDutyRespDtos = commonService.getUserAndDuty(userAndDutyReqDto);
        if (userAndDutyRespDtos.size() == 0) {
            throw BizException.error(null, EcbEnum.ECB010004.key, "该登陆用户未查询到岗位信息");
        }
        //查询当前登录人是否有小微内勤岗 岗位 dutyNo=  XWB13
        long xwb13 = userAndDutyRespDtos.stream().filter(s -> "XWB13".equals(s.getDutyNo())).count();
        if (xwb13 == 1) {
            logger.info("***************小微内勤岗 查询所有同机构小微客户经理的数据*************");
            //TODO 子机构如何查询暂时不知道暂时根据登录人机构去查同机构的 那么就查当前登录机构下小微客户经理数据（先获取当前机构下用户，再根据角色筛选）
            model.getCondition().put("managerId", "");
            model.getCondition().put("managerBrId", "");
            // 2021年10月28日19:53:19 hubp 如果为小微内勤岗，，查inputBrId
            User userInfo = SessionUtils.getUserInformation();
            model.getCondition().put("inputBrId", userInfo.getOrg().getCode());
            //查询所有拥有小微客户经理角色的人
            PageHelper.startPage(model.getPage(), model.getSize());
            List<LmtCrdReplyInfo> list = lmtCrdReplyInfoMapper.selectByModel(model);
            PageHelper.clearPage();
            return list;
        }
        logger.info("***************非小微内勤岗 查询当前登录人的批复数据*************");
        model.getCondition().put("managerBrId", "");
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtCrdReplyInfo> list = lmtCrdReplyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(LmtCrdReplyInfo record) {
        return lmtCrdReplyInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtCrdReplyInfo record) {
        return lmtCrdReplyInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtCrdReplyInfo record) {
        return lmtCrdReplyInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtCrdReplyInfo record) {
        return lmtCrdReplyInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String replyNo) {
        return lmtCrdReplyInfoMapper.deleteByPrimaryKey(replyNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtCrdReplyInfoMapper.deleteByIds(ids);
    }




    /**
     * @方法名称: selectReplyList
     * @方法描述: 查询批复信息列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, String>> selectReplyList() {

        List<Map<String, String>> list = new ArrayList<>();
        Map <String, String> parame1 = new HashMap<String, String>();
        parame1.put("replyNo","R00000000001");
        parame1.put("cusId","KH00123456711");
        parame1.put("cusName","张家港银行测试33");
        parame1.put("prdId","0001");
        parame1.put("prdName","个人住房按揭贷款");
        parame1.put("limitType","002");
        parame1.put("contAmt","1000000");
        parame1.put("replyTerm","12");
        parame1.put("phone","18678761234");
        parame1.put("certCode","3209989212320912");
        parame1.put("loanTypes","1");
        parame1.put("loanCha","1");
        parame1.put("certType","100");
        parame1.put("surveyNo","SN00000000001");
        parame1.put("guarWay","10");
        parame1.put("isOlPld","Y");
        parame1.put("defrayMode","01");
        parame1.put("repaymentType","4");

        list.add(parame1);

        return list;
    }


    /**
     * @author shenli
     * @date 2021-6-5 11:21:33
     * @version 1.0.0
     * @desc 零售授信批复信息插入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtCrdReplyInfo insertLmtCrdReplyRetail(IqpLoanApp iqpLoanApp){
        logger.info("根据申请流水号【{}】生成批复信息开始", iqpLoanApp.getIqpSerno());
        // 对象声明
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        String replyNo = null;
        int nums = 0;
        try {
            lmtCrdReplyInfo = new LmtCrdReplyInfo();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanApp, lmtCrdReplyInfo);
            // 生成批复序列号
            replyNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
            logger.info("根据申请流水号【{}】生成批复编号【{}】", iqpLoanApp.getIqpSerno(), replyNo);

            lmtCrdReplyInfo.setReplySerno(replyNo);//批复流水号
            lmtCrdReplyInfo.setSurveySerno(iqpLoanApp.getIqpSerno());//申请流水号
            lmtCrdReplyInfo.setCertType("A");//证件类型
            lmtCrdReplyInfo.setGuarMode(iqpLoanApp.getGuarWay());//担保方式
            lmtCrdReplyInfo.setReplyAmt(iqpLoanApp.getCvtCnyAmt());//批复金额
            lmtCrdReplyInfo.setLmtGraper(Integer.parseInt(iqpLoanApp.getGraper()));//额度宽限期（月）
            lmtCrdReplyInfo.setReplyStartDate(iqpLoanApp.getStartDate());//批复起始日
            lmtCrdReplyInfo.setReplyEndDate(iqpLoanApp.getEndDate());//批复到期日
            if("022028".equals(lmtCrdReplyInfo.getPrdId())){
                lmtCrdReplyInfo.setLimitType("02");//循环额度
            }else{
                lmtCrdReplyInfo.setLimitType("01");//零时额度
            }
            lmtCrdReplyInfo.setCurtLoanAmt(iqpLoanApp.getAppAmt());//额度类型
            lmtCrdReplyInfo.setCurtLoanAmt(iqpLoanApp.getAppAmt());//额度类型
            lmtCrdReplyInfo.setAgriFlag("");//是否涉农
            lmtCrdReplyInfo.setApprMode("");//审批模式
            lmtCrdReplyInfo.setPrdId(iqpLoanApp.getPrdId());
            lmtCrdReplyInfo.setPrdName(iqpLoanApp.getPrdName());
            lmtCrdReplyInfo.setAppTerm(iqpLoanApp.getAppTerm().intValue());
            lmtCrdReplyInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            lmtCrdReplyInfo.setUpdateTime(DateUtils.getCurrDate());
            lmtCrdReplyInfo.setCreateTime(DateUtils.getCurrDate());

            nums = this.insertSelective(lmtCrdReplyInfo);
            Asserts.isTrue(nums > 0,"插入批复信息出错！");
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            logger.info("根据申请流水号【{}】生成批复信息结束", iqpLoanApp.getIqpSerno());
        }
        return lmtCrdReplyInfo;
    }

    /**
     * 根据证件号和日期查询最近的一笔授信决议
     *
     * @param map
     * @return
     */
    public LmtCrdReplyInfo selectLastApprovalResult(Map map) {
        return lmtCrdReplyInfoMapper.selectLastApprovalResult(map);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/3 15:35
     * @注释 通过调查流水号查批复信息
     */
    public LmtCrdReplyInfo selectBySurveySerno(String surveySerno){
        return lmtCrdReplyInfoMapper.selectBySurveySerno(surveySerno);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/31 20:33
     * @注释 通过授信流水号修改批复数据
     */
    public int updateSurveySerno(String surveySerno) {
        return lmtCrdReplyInfoMapper.updateSurveySerno(surveySerno);
    }

}
