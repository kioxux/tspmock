package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.GuarContRelWarrantService;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskItem0102Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0102Service.class);

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;
    /**
     * @方法名称: riskItem0102
     * @方法描述: 权证出库押品关联借款人贷款合同校验
     * @参数与返回说明:
     * @算法描述:
     * 1、合作方存在预警信号的，提交合作方准入申请时，作出提示拦截
     * @创建人: dumingdi
     * @创建时间: 2021-08-27 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0102(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("权证出库押品关联借款人贷款合同校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();

        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();

        //根据核心担保编号查询对应的抵押物的借款人客户项下存在有效的“诚易融”贷款合同的押品编号
        String guarNos = guarContRelWarrantService.selectGuarNoByCoreGuarantyNo(coreGuarantyNo);

        if(StringUtils.nonEmpty(guarNos)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("该笔权证出库下的押品编号为【"+guarNos+"】的借款人客户项下存在有效的“诚易融”贷款合同！");
            return riskResultDto;
        }

        log.info("权证出库押品关联借款人贷款合同校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}