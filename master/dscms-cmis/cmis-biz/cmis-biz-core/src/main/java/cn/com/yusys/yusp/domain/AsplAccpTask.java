/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccpTask
 * @类描述: aspl_accp_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:19:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "aspl_accp_task")
public class AsplAccpTask extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 任务编号 **/
	@Column(name = "TASK_ID", unique = false, nullable = false, length = 40)
	private String taskId;

	/** 影像流水号 **/
	@Column(name = "BILL_IMG_ID", unique = false, nullable = false, length = 40)
	private String billImgId;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 中文合同号 **/
	@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 80)
	private String contCnNo;
	
	/** 出账流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 80)
	private String pvpSerno;
	
	/** 核心合同号 **/
	@Column(name = "CORE_CONT_NO", unique = false, nullable = true, length = 40)
	private String coreContNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 任务生成日期 **/
	@Column(name = "TASK_CREATE_DATE", unique = false, nullable = true, length = 20)
	private String taskCreateDate;
	
	/** 要求完成日期 **/
	@Column(name = "NEED_FINISH_DATE", unique = false, nullable = true, length = 20)
	private String needFinishDate;
	
	/** 批次票面金额 **/
	@Column(name = "BATCH_DRFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal batchDrftAmt;
	
	/** 批次条数 **/
	@Column(name = "BATCH_QNT", unique = false, nullable = true, length = 10)
	private Integer batchQnt;
	
	/** 收集日期 **/
	@Column(name = "COLLECT_DATE", unique = false, nullable = true, length = 20)
	private String collectDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 备注描述 **/
	@Column(name = "REMARK", unique = false, nullable = false, length = 500)
	private String remark;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 是否完成补扫 **/
	@Column(name = "IS_ADD_BIll", unique = false, nullable = true, length = 5)
	private String isAddBill;


	public String getBillImgId() {
		return billImgId;
	}

	public void setBillImgId(String billImgId) {
		this.billImgId = billImgId;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param taskId
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
    /**
     * @return taskId
     */
	public String getTaskId() {
		return this.taskId;
	}


	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}
	
    /**
     * @return contCnNo
     */
	public String getContCnNo() {
		return this.contCnNo;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param coreContNo
	 */
	public void setCoreContNo(String coreContNo) {
		this.coreContNo = coreContNo;
	}
	
    /**
     * @return coreContNo
     */
	public String getCoreContNo() {
		return this.coreContNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param taskCreateDate
	 */
	public void setTaskCreateDate(String taskCreateDate) {
		this.taskCreateDate = taskCreateDate;
	}
	
    /**
     * @return taskCreateDate
     */
	public String getTaskCreateDate() {
		return this.taskCreateDate;
	}
	
	/**
	 * @param needFinishDate
	 */
	public void setNeedFinishDate(String needFinishDate) {
		this.needFinishDate = needFinishDate;
	}
	
    /**
     * @return needFinishDate
     */
	public String getNeedFinishDate() {
		return this.needFinishDate;
	}
	
	/**
	 * @param batchDrftAmt
	 */
	public void setBatchDrftAmt(java.math.BigDecimal batchDrftAmt) {
		this.batchDrftAmt = batchDrftAmt;
	}
	
    /**
     * @return batchDrftAmt
     */
	public java.math.BigDecimal getBatchDrftAmt() {
		return this.batchDrftAmt;
	}
	
	/**
	 * @param batchQnt
	 */
	public void setBatchQnt(Integer batchQnt) {
		this.batchQnt = batchQnt;
	}
	
    /**
     * @return batchQnt
     */
	public Integer getBatchQnt() {
		return this.batchQnt;
	}
	
	/**
	 * @param collectDate
	 */
	public void setCollectDate(String collectDate) {
		this.collectDate = collectDate;
	}
	
    /**
     * @return collectDate
     */
	public String getCollectDate() {
		return this.collectDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsAddBill() {
		return isAddBill;
	}

	public void setIsAddBill(String isAddBill) {
		this.isAddBill = isAddBill;
	}
}