/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAssetsListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 19:45:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AsplAssetsListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AsplAssetsList selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AsplAssetsList> selectByModel(QueryModel queryModel);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AsplAssetsList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AsplAssetsList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AsplAssetsList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AsplAssetsList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: selectByModelDate
     * @方法描述: 灵活查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AsplAssetsList> selectByModelDate(QueryModel queryModel);

    /**
     * @方法名称: insertAsplAssetsList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertAsplAssetsList(List<AsplAssetsList> asplAssetsListList);

    /**
     * @方法名称: isAssetNoList
     * @方法描述: 查询已存在的资产清单编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<String> isAssetNoList(@Param("list") List<String> assetNoList);

    /**
     * @方法名称: updateAsplAssets
     * @方法描述: 更新资产信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateAsplAssets(AsplAssetsList record);

    /**
     * @方法名称: updateAsplAssetsDataAfterOutPoolFlow
     * @方法描述: 更新 客户资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateAsplAssetsDataAfterOutPoolFlow();

    /**
     * @方法名称: selectAsplAssestsByAssestsNos
     * @方法描述: 根据资产编号集合查询资产集合
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AsplAssetsList> selectAsplAssestsByAssestsNos(@Param("list")List<String> assetNoList);

    /**
     * @方法名称: selectByAssetNo
     * @方法描述: 根据资产编号查询资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */
    AsplAssetsList selectByAssetNo(@Param("contNo")String contNo, @Param("assetNo")String assetNo);

    /**
     * 更新出池状态
     * @param assetNo
     * @param outpTime
     * @param contNo
     * @return
     */
    int updateOutPollAsplAssets(@Param("assetNo")String assetNo,@Param("outpTime") String outpTime,@Param("contNo")String contNo);

    /**
     * 查询池内资产清单台账
     * @param queryModel
     * @return
     */
    List<AsplAssetsList> inPoolAssetsListAll(QueryModel queryModel);

    /**
     * 更新出池状态
     * @param assetNo
     * @param inpTime
     * @param contNo
     * @return
     */
    int updateInpPollAsplAssets(@Param("assetNo")String assetNo,@Param("inpTime") String inpTime,@Param("contNo")String contNo);


    /**
     * 更新出池状态
     * @param contNo
     * @return
     */
    int deleteByContNo(@Param("contNo")String contNo);
}