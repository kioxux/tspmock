/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: RptOperAgriculture
 * @类描述: rpt_oper_agriculture数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-11-08 15:25:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_agriculture")
public class RptOperAgriculture extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;

	/** 最近第二年蔬菜产量 **/
	@Column(name = "NEAR_SECOND_VEGE_PRODUCRT", unique = false, nullable = true, length = 20)
	private String nearSecondVegeProducrt;

	/** 最近第二年蔬菜销量 **/
	@Column(name = "NEAR_SECOND_VEGE_SALE", unique = false, nullable = true, length = 20)
	private String nearSecondVegeSale;

	/** 最近第二年蔬菜实现销售收入 **/
	@Column(name = "NEAR_SECOND_VEGE_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondVegeSaleRevenrt;

	/** 最近第二年蔬菜实现毛利润 **/
	@Column(name = "NEAR_SECOND_VEGE_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondVegeGrossProfit;

	/** 最近第二年粮食产量 **/
	@Column(name = "NEAR_SECOND_FOOD_PRODUCRT", unique = false, nullable = true, length = 20)
	private String nearSecondFoodProducrt;

	/** 最近第二年粮食销量 **/
	@Column(name = "NEAR_SECOND_FOOD_SALE", unique = false, nullable = true, length = 20)
	private String nearSecondFoodSale;

	/** 最近第二年粮食实现销售收入 **/
	@Column(name = "NEAR_SECOND_FOOD_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondFoodSaleRevenrt;

	/** 最近第二年粮食实现毛利润 **/
	@Column(name = "NEAR_SECOND_FOOD_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondFoodGrossProfit;

	/** 最近第二年苗木种植产量 **/
	@Column(name = "NEAR_SECOND_SEED_PRODUCRT", unique = false, nullable = true, length = 20)
	private String nearSecondSeedProducrt;

	/** 最近第二年苗木种植销量 **/
	@Column(name = "NEAR_SECOND_SEED_SALE", unique = false, nullable = true, length = 20)
	private String nearSecondSeedSale;

	/** 最近第二年苗木种植实现销售收入 **/
	@Column(name = "NEAR_SECOND_SEED_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondSeedSaleRevenrt;

	/** 最近第二年苗木种植实现毛利润 **/
	@Column(name = "NEAR_SECOND_SEED_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondSeedGrossProfit;

	/** 最近第一年蔬菜产量 **/
	@Column(name = "NEAR_FIRST_VEGE_PRODUCRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstVegeProducrt;

	/** 最近第一年蔬菜销量 **/
	@Column(name = "NEAR_FIRST_VEGE_SALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstVegeSale;

	/** 最近第一年蔬菜实现销售收入 **/
	@Column(name = "NEAR_FIRST_VEGE_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstVegeSaleRevenrt;

	/** 最近第一年蔬菜实现毛利润 **/
	@Column(name = "NEAR_FIRST_VEGE_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstVegeGrossProfit;

	/** 最近第一年粮食产量 **/
	@Column(name = "NEAR_FIRST_FOOD_PRODUCRT", unique = false, nullable = true, length = 20)
	private String nearFirstFoodProducrt;

	/** 最近第一年粮食销量 **/
	@Column(name = "NEAR_FIRST_FOOD_SALE", unique = false, nullable = true, length = 20)
	private String nearFirstFoodSale;

	/** 最近第一年粮食实现销售收入 **/
	@Column(name = "NEAR_FIRST_FOOD_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstFoodSaleRevenrt;

	/** 最近第一年粮食实现毛利润 **/
	@Column(name = "NEAR_FIRST_FOOD_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstFoodGrossProfit;

	/** 最近第一年苗木种植产量 **/
	@Column(name = "NEAR_FIRST_SEED_PRODUCRT", unique = false, nullable = true, length = 20)
	private String nearFirstSeedProducrt;

	/** 最近第一年苗木种植销量 **/
	@Column(name = "NEAR_FIRST_SEED_SALE", unique = false, nullable = true, length = 20)
	private String nearFirstSeedSale;

	/** 最近第一年苗木种植实现销售收入 **/
	@Column(name = "NEAR_FIRST_SEED_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstSeedSaleRevenrt;

	/** 最近第一年苗木种植实现毛利润 **/
	@Column(name = "NEAR_FIRST_SEED_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstSeedGrossProfit;

	/** 当前年蔬菜产量 **/
	@Column(name = "CURR_VEGE_PRODUCRT", unique = false, nullable = true, length = 20)
	private String currVegeProducrt;

	/** 当前年蔬菜销量 **/
	@Column(name = "CURR_VEGE_SALE", unique = false, nullable = true, length = 20)
	private String currVegeSale;

	/** 当前年蔬菜实现销售收入 **/
	@Column(name = "CURR_VEGE_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currVegeSaleRevenrt;

	/** 当前年蔬菜实现毛利润 **/
	@Column(name = "CURR_VEGE_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currVegeGrossProfit;

	/** 当前年粮食产量 **/
	@Column(name = "CURR_FOOD_PRODUCRT", unique = false, nullable = true, length = 20)
	private String currFoodProducrt;

	/** 当前年粮食销量 **/
	@Column(name = "CURR_FOOD_SALE", unique = false, nullable = true, length = 20)
	private String currFoodSale;

	/** 当前年粮食实现销售收入 **/
	@Column(name = "CURR_FOOD_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currFoodSaleRevenrt;

	/** 当前年粮食实现毛利润 **/
	@Column(name = "CURR_FOOD_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currFoodGrossProfit;

	/** 当前年苗木种植产量 **/
	@Column(name = "CURR_SEED_PRODUCRT", unique = false, nullable = true, length = 20)
	private String currSeedProducrt;

	/** 当前年苗木种植销量 **/
	@Column(name = "CURR_SEED_SALE", unique = false, nullable = true, length = 20)
	private String currSeedSale;

	/** 当前年苗木种植实现销售收入 **/
	@Column(name = "CURR_SEED_SALE_REVENRT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currSeedSaleRevenrt;

	/** 当前年苗木种植实现毛利润 **/
	@Column(name = "CURR_SEED_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currSeedGrossProfit;

	/** 今年产销利波动原因 **/
	@Column(name = "CURR_SALE_WAVE_REASON", unique = false, nullable = true, length = 65535)
	private String currSaleWaveReason;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}

	/**
	 * @return inputYear
	 */
	public String getInputYear() {
		return this.inputYear;
	}

	/**
	 * @param nearSecondVegeProducrt
	 */
	public void setNearSecondVegeProducrt(String nearSecondVegeProducrt) {
		this.nearSecondVegeProducrt = nearSecondVegeProducrt;
	}

	/**
	 * @return nearSecondVegeProducrt
	 */
	public String getNearSecondVegeProducrt() {
		return this.nearSecondVegeProducrt;
	}

	/**
	 * @param nearSecondVegeSale
	 */
	public void setNearSecondVegeSale(String nearSecondVegeSale) {
		this.nearSecondVegeSale = nearSecondVegeSale;
	}

	/**
	 * @return nearSecondVegeSale
	 */
	public String getNearSecondVegeSale() {
		return this.nearSecondVegeSale;
	}

	/**
	 * @param nearSecondVegeSaleRevenrt
	 */
	public void setNearSecondVegeSaleRevenrt(java.math.BigDecimal nearSecondVegeSaleRevenrt) {
		this.nearSecondVegeSaleRevenrt = nearSecondVegeSaleRevenrt;
	}

	/**
	 * @return nearSecondVegeSaleRevenrt
	 */
	public java.math.BigDecimal getNearSecondVegeSaleRevenrt() {
		return this.nearSecondVegeSaleRevenrt;
	}

	/**
	 * @param nearSecondVegeGrossProfit
	 */
	public void setNearSecondVegeGrossProfit(java.math.BigDecimal nearSecondVegeGrossProfit) {
		this.nearSecondVegeGrossProfit = nearSecondVegeGrossProfit;
	}

	/**
	 * @return nearSecondVegeGrossProfit
	 */
	public java.math.BigDecimal getNearSecondVegeGrossProfit() {
		return this.nearSecondVegeGrossProfit;
	}

	/**
	 * @param nearSecondFoodProducrt
	 */
	public void setNearSecondFoodProducrt(String nearSecondFoodProducrt) {
		this.nearSecondFoodProducrt = nearSecondFoodProducrt;
	}

	/**
	 * @return nearSecondFoodProducrt
	 */
	public String getNearSecondFoodProducrt() {
		return this.nearSecondFoodProducrt;
	}

	/**
	 * @param nearSecondFoodSale
	 */
	public void setNearSecondFoodSale(String nearSecondFoodSale) {
		this.nearSecondFoodSale = nearSecondFoodSale;
	}

	/**
	 * @return nearSecondFoodSale
	 */
	public String getNearSecondFoodSale() {
		return this.nearSecondFoodSale;
	}

	/**
	 * @param nearSecondFoodSaleRevenrt
	 */
	public void setNearSecondFoodSaleRevenrt(java.math.BigDecimal nearSecondFoodSaleRevenrt) {
		this.nearSecondFoodSaleRevenrt = nearSecondFoodSaleRevenrt;
	}

	/**
	 * @return nearSecondFoodSaleRevenrt
	 */
	public java.math.BigDecimal getNearSecondFoodSaleRevenrt() {
		return this.nearSecondFoodSaleRevenrt;
	}

	/**
	 * @param nearSecondFoodGrossProfit
	 */
	public void setNearSecondFoodGrossProfit(java.math.BigDecimal nearSecondFoodGrossProfit) {
		this.nearSecondFoodGrossProfit = nearSecondFoodGrossProfit;
	}

	/**
	 * @return nearSecondFoodGrossProfit
	 */
	public java.math.BigDecimal getNearSecondFoodGrossProfit() {
		return this.nearSecondFoodGrossProfit;
	}

	/**
	 * @param nearSecondSeedProducrt
	 */
	public void setNearSecondSeedProducrt(String nearSecondSeedProducrt) {
		this.nearSecondSeedProducrt = nearSecondSeedProducrt;
	}

	/**
	 * @return nearSecondSeedProducrt
	 */
	public String getNearSecondSeedProducrt() {
		return this.nearSecondSeedProducrt;
	}

	/**
	 * @param nearSecondSeedSale
	 */
	public void setNearSecondSeedSale(String nearSecondSeedSale) {
		this.nearSecondSeedSale = nearSecondSeedSale;
	}

	/**
	 * @return nearSecondSeedSale
	 */
	public String getNearSecondSeedSale() {
		return this.nearSecondSeedSale;
	}

	/**
	 * @param nearSecondSeedSaleRevenrt
	 */
	public void setNearSecondSeedSaleRevenrt(java.math.BigDecimal nearSecondSeedSaleRevenrt) {
		this.nearSecondSeedSaleRevenrt = nearSecondSeedSaleRevenrt;
	}

	/**
	 * @return nearSecondSeedSaleRevenrt
	 */
	public java.math.BigDecimal getNearSecondSeedSaleRevenrt() {
		return this.nearSecondSeedSaleRevenrt;
	}

	/**
	 * @param nearSecondSeedGrossProfit
	 */
	public void setNearSecondSeedGrossProfit(java.math.BigDecimal nearSecondSeedGrossProfit) {
		this.nearSecondSeedGrossProfit = nearSecondSeedGrossProfit;
	}

	/**
	 * @return nearSecondSeedGrossProfit
	 */
	public java.math.BigDecimal getNearSecondSeedGrossProfit() {
		return this.nearSecondSeedGrossProfit;
	}

	/**
	 * @param nearFirstVegeProducrt
	 */
	public void setNearFirstVegeProducrt(java.math.BigDecimal nearFirstVegeProducrt) {
		this.nearFirstVegeProducrt = nearFirstVegeProducrt;
	}

	/**
	 * @return nearFirstVegeProducrt
	 */
	public java.math.BigDecimal getNearFirstVegeProducrt() {
		return this.nearFirstVegeProducrt;
	}

	/**
	 * @param nearFirstVegeSale
	 */
	public void setNearFirstVegeSale(java.math.BigDecimal nearFirstVegeSale) {
		this.nearFirstVegeSale = nearFirstVegeSale;
	}

	/**
	 * @return nearFirstVegeSale
	 */
	public java.math.BigDecimal getNearFirstVegeSale() {
		return this.nearFirstVegeSale;
	}

	/**
	 * @param nearFirstVegeSaleRevenrt
	 */
	public void setNearFirstVegeSaleRevenrt(java.math.BigDecimal nearFirstVegeSaleRevenrt) {
		this.nearFirstVegeSaleRevenrt = nearFirstVegeSaleRevenrt;
	}

	/**
	 * @return nearFirstVegeSaleRevenrt
	 */
	public java.math.BigDecimal getNearFirstVegeSaleRevenrt() {
		return this.nearFirstVegeSaleRevenrt;
	}

	/**
	 * @param nearFirstVegeGrossProfit
	 */
	public void setNearFirstVegeGrossProfit(java.math.BigDecimal nearFirstVegeGrossProfit) {
		this.nearFirstVegeGrossProfit = nearFirstVegeGrossProfit;
	}

	/**
	 * @return nearFirstVegeGrossProfit
	 */
	public java.math.BigDecimal getNearFirstVegeGrossProfit() {
		return this.nearFirstVegeGrossProfit;
	}

	/**
	 * @param nearFirstFoodProducrt
	 */
	public void setNearFirstFoodProducrt(String nearFirstFoodProducrt) {
		this.nearFirstFoodProducrt = nearFirstFoodProducrt;
	}

	/**
	 * @return nearFirstFoodProducrt
	 */
	public String getNearFirstFoodProducrt() {
		return this.nearFirstFoodProducrt;
	}

	/**
	 * @param nearFirstFoodSale
	 */
	public void setNearFirstFoodSale(String nearFirstFoodSale) {
		this.nearFirstFoodSale = nearFirstFoodSale;
	}

	/**
	 * @return nearFirstFoodSale
	 */
	public String getNearFirstFoodSale() {
		return this.nearFirstFoodSale;
	}

	/**
	 * @param nearFirstFoodSaleRevenrt
	 */
	public void setNearFirstFoodSaleRevenrt(java.math.BigDecimal nearFirstFoodSaleRevenrt) {
		this.nearFirstFoodSaleRevenrt = nearFirstFoodSaleRevenrt;
	}

	/**
	 * @return nearFirstFoodSaleRevenrt
	 */
	public java.math.BigDecimal getNearFirstFoodSaleRevenrt() {
		return this.nearFirstFoodSaleRevenrt;
	}

	/**
	 * @param nearFirstFoodGrossProfit
	 */
	public void setNearFirstFoodGrossProfit(java.math.BigDecimal nearFirstFoodGrossProfit) {
		this.nearFirstFoodGrossProfit = nearFirstFoodGrossProfit;
	}

	/**
	 * @return nearFirstFoodGrossProfit
	 */
	public java.math.BigDecimal getNearFirstFoodGrossProfit() {
		return this.nearFirstFoodGrossProfit;
	}

	/**
	 * @param nearFirstSeedProducrt
	 */
	public void setNearFirstSeedProducrt(String nearFirstSeedProducrt) {
		this.nearFirstSeedProducrt = nearFirstSeedProducrt;
	}

	/**
	 * @return nearFirstSeedProducrt
	 */
	public String getNearFirstSeedProducrt() {
		return this.nearFirstSeedProducrt;
	}

	/**
	 * @param nearFirstSeedSale
	 */
	public void setNearFirstSeedSale(String nearFirstSeedSale) {
		this.nearFirstSeedSale = nearFirstSeedSale;
	}

	/**
	 * @return nearFirstSeedSale
	 */
	public String getNearFirstSeedSale() {
		return this.nearFirstSeedSale;
	}

	/**
	 * @param nearFirstSeedSaleRevenrt
	 */
	public void setNearFirstSeedSaleRevenrt(java.math.BigDecimal nearFirstSeedSaleRevenrt) {
		this.nearFirstSeedSaleRevenrt = nearFirstSeedSaleRevenrt;
	}

	/**
	 * @return nearFirstSeedSaleRevenrt
	 */
	public java.math.BigDecimal getNearFirstSeedSaleRevenrt() {
		return this.nearFirstSeedSaleRevenrt;
	}

	/**
	 * @param nearFirstSeedGrossProfit
	 */
	public void setNearFirstSeedGrossProfit(java.math.BigDecimal nearFirstSeedGrossProfit) {
		this.nearFirstSeedGrossProfit = nearFirstSeedGrossProfit;
	}

	/**
	 * @return nearFirstSeedGrossProfit
	 */
	public java.math.BigDecimal getNearFirstSeedGrossProfit() {
		return this.nearFirstSeedGrossProfit;
	}

	/**
	 * @param currVegeProducrt
	 */
	public void setCurrVegeProducrt(String currVegeProducrt) {
		this.currVegeProducrt = currVegeProducrt;
	}

	/**
	 * @return currVegeProducrt
	 */
	public String getCurrVegeProducrt() {
		return this.currVegeProducrt;
	}

	/**
	 * @param currVegeSale
	 */
	public void setCurrVegeSale(String currVegeSale) {
		this.currVegeSale = currVegeSale;
	}

	/**
	 * @return currVegeSale
	 */
	public String getCurrVegeSale() {
		return this.currVegeSale;
	}

	/**
	 * @param currVegeSaleRevenrt
	 */
	public void setCurrVegeSaleRevenrt(java.math.BigDecimal currVegeSaleRevenrt) {
		this.currVegeSaleRevenrt = currVegeSaleRevenrt;
	}

	/**
	 * @return currVegeSaleRevenrt
	 */
	public java.math.BigDecimal getCurrVegeSaleRevenrt() {
		return this.currVegeSaleRevenrt;
	}

	/**
	 * @param currVegeGrossProfit
	 */
	public void setCurrVegeGrossProfit(java.math.BigDecimal currVegeGrossProfit) {
		this.currVegeGrossProfit = currVegeGrossProfit;
	}

	/**
	 * @return currVegeGrossProfit
	 */
	public java.math.BigDecimal getCurrVegeGrossProfit() {
		return this.currVegeGrossProfit;
	}

	/**
	 * @param currFoodProducrt
	 */
	public void setCurrFoodProducrt(String currFoodProducrt) {
		this.currFoodProducrt = currFoodProducrt;
	}

	/**
	 * @return currFoodProducrt
	 */
	public String getCurrFoodProducrt() {
		return this.currFoodProducrt;
	}

	/**
	 * @param currFoodSale
	 */
	public void setCurrFoodSale(String currFoodSale) {
		this.currFoodSale = currFoodSale;
	}

	/**
	 * @return currFoodSale
	 */
	public String getCurrFoodSale() {
		return this.currFoodSale;
	}

	/**
	 * @param currFoodSaleRevenrt
	 */
	public void setCurrFoodSaleRevenrt(java.math.BigDecimal currFoodSaleRevenrt) {
		this.currFoodSaleRevenrt = currFoodSaleRevenrt;
	}

	/**
	 * @return currFoodSaleRevenrt
	 */
	public java.math.BigDecimal getCurrFoodSaleRevenrt() {
		return this.currFoodSaleRevenrt;
	}

	/**
	 * @param currFoodGrossProfit
	 */
	public void setCurrFoodGrossProfit(java.math.BigDecimal currFoodGrossProfit) {
		this.currFoodGrossProfit = currFoodGrossProfit;
	}

	/**
	 * @return currFoodGrossProfit
	 */
	public java.math.BigDecimal getCurrFoodGrossProfit() {
		return this.currFoodGrossProfit;
	}

	/**
	 * @param currSeedProducrt
	 */
	public void setCurrSeedProducrt(String currSeedProducrt) {
		this.currSeedProducrt = currSeedProducrt;
	}

	/**
	 * @return currSeedProducrt
	 */
	public String getCurrSeedProducrt() {
		return this.currSeedProducrt;
	}

	/**
	 * @param currSeedSale
	 */
	public void setCurrSeedSale(String currSeedSale) {
		this.currSeedSale = currSeedSale;
	}

	/**
	 * @return currSeedSale
	 */
	public String getCurrSeedSale() {
		return this.currSeedSale;
	}

	/**
	 * @param currSeedSaleRevenrt
	 */
	public void setCurrSeedSaleRevenrt(java.math.BigDecimal currSeedSaleRevenrt) {
		this.currSeedSaleRevenrt = currSeedSaleRevenrt;
	}

	/**
	 * @return currSeedSaleRevenrt
	 */
	public java.math.BigDecimal getCurrSeedSaleRevenrt() {
		return this.currSeedSaleRevenrt;
	}

	/**
	 * @param currSeedGrossProfit
	 */
	public void setCurrSeedGrossProfit(java.math.BigDecimal currSeedGrossProfit) {
		this.currSeedGrossProfit = currSeedGrossProfit;
	}

	/**
	 * @return currSeedGrossProfit
	 */
	public java.math.BigDecimal getCurrSeedGrossProfit() {
		return this.currSeedGrossProfit;
	}

	/**
	 * @param currSaleWaveReason
	 */
	public void setCurrSaleWaveReason(String currSaleWaveReason) {
		this.currSaleWaveReason = currSaleWaveReason;
	}

	/**
	 * @return currSaleWaveReason
	 */
	public String getCurrSaleWaveReason() {
		return this.currSaleWaveReason;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}