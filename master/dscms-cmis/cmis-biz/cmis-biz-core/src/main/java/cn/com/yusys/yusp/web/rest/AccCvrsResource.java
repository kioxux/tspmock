/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.service.AccCvrsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccCvrsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:54:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/acccvrs")
public class AccCvrsResource {
    @Autowired
    private AccCvrsService accCvrsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccCvrs>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccCvrs> list = accCvrsService.selectAll(queryModel);
        return new ResultDto<List<AccCvrs>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccCvrs>> index(QueryModel queryModel) {
        List<AccCvrs> list = accCvrsService.selectByModel(queryModel);
        return new ResultDto<List<AccCvrs>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccCvrs> show(@PathVariable("pkId") String pkId) {
        AccCvrs accCvrs = accCvrsService.selectByPrimaryKey(pkId);
        return new ResultDto<AccCvrs>(accCvrs);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccCvrs> create(@RequestBody AccCvrs accCvrs) throws URISyntaxException {
        accCvrsService.insert(accCvrs);
        return new ResultDto<AccCvrs>(accCvrs);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccCvrs accCvrs) throws URISyntaxException {
        int result = accCvrsService.update(accCvrs);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accCvrsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accCvrsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccCvrs>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccCvrs> list = accCvrsService.selectByModel(queryModel);
        return new ResultDto<List<AccCvrs>>(list);
    }

    /**
     * @函数名称: selectByBillNo
     * @函数描述: 根据借据编号查询保函台账列表信息
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @PostMapping("/selectByBillNo")
    protected ResultDto<AccCvrs> selectByBillNo(@RequestBody String billNo) {
        AccCvrs accCvrs = accCvrsService.selectByBillNo(billNo);
        return new ResultDto<AccCvrs>(accCvrs);
    }

    /**
     * 异步下载保函台账列表
     */
    @PostMapping("/exportAccCvrs")
    public ResultDto<ProgressDto> asyncExportAccCvrs(@RequestBody QueryModel model) {
        ProgressDto progressDto = accCvrsService.asyncExportAccCvrs(model);
        return ResultDto.success(progressDto);
    }


    /**
     * @函数名称: querymodelByCondition
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            根据查询条件查询台账信息并返回
     * @算法描述:
     */
    @PostMapping("/querymodelByCondition")
    protected ResultDto<List<AccCvrs>> querymodelByCondition(@RequestBody QueryModel queryModel) {
        List<AccCvrs> list = accCvrsService.querymodelByCondition(queryModel);
        return new ResultDto<List<AccCvrs>>(list);
    }

    /**
     * @函数名称: selectAccCvrsBillNo
     * @函数描述: 根据申请流水号查询保函台账借据编号
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @PostMapping("/selectacccvrsbillno")
    protected String selectAccCvrsBillNo(@RequestBody String pvpSerno) {
        AccCvrs accCvrs = accCvrsService.selectAccCvrsBillNo(pvpSerno);
        if(Objects.nonNull(accCvrs)){
            return accCvrs.getBillNo();
        }
        return null;
    }
}
