package cn.com.yusys.yusp.service.server.xdht0004;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusAccountInfoDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfDto;
import cn.com.yusys.yusp.dto.LmtSubPrdMappConfListDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.dto.server.xdht0004.req.Xdht0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0004.resp.AccList;
import cn.com.yusys.yusp.dto.server.xdht0004.resp.Xdht0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.Dscms2CoreDpClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0004Service
 * @类描述: #服务类
 * @功能描述: * 场景：国结系统查询信贷合同信息，信贷返回该客户下对应的有效信用证合同/保函合同/贷款合同信息
 * * 接口说明：
 * * 1).根据请求参数【type-业务类型】、【cus_id-客户号】查询对应合同信息表
 * * 	type='01'查询【信用证合同ctr_tf_loc】
 * * 	type='02'查询【保函合同ctr_cvrg_cont】
 * * 	type='03'||type='04'查询【贷款合同ctr_loan_cont】
 * * 2).列表形式返回合同信息
 * *注：
 * *合同信息中涉及的部分字段需要加工获取：
 * *	"保证金账号scurity_acc"，从【结算账户cus_com_acc】获取；
 * *	"保证金比例security_money_rt"，从【授信台账lmt_cont_details】；
 * *	"汇率exchange_rate"，从对应台账【ACC_XXX】、【汇率TF_RATE】获取
 * *	"合同可用余额loan_balance",等于"合同金额"-已用信额度（需根据台账汇总）
 * *一次性合同，只返回未出账合同，如已存在出账记录则不返回
 * *
 * *老信贷代码参照XdGj01Action.java
 * @创建人: 徐超
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0004Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0004Service.class);

    @Resource
    private CtrTfLocContMapper ctrTfLocContMapper;
    @Resource
    private CtrCvrgContMapper ctrCvrgContMapper;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Resource
    private AccTfLocMapper accTfLocMapper;
    @Resource
    private BailAccInfoMapper bailAccInfoMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BailDepositInfoMapper bailDepositInfoMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    @Autowired
    private IqpLoanAppMapper iqpLoanAppMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;


    /**
     * 合同信息列表查询
     *
     * @param xdht0004DataReqDto
     * @return xdht0004DataRespDto
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0004DataRespDto getXdht0004(Xdht0004DataReqDto xdht0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataReqDto));
        Xdht0004DataRespDto xdht0004DataRespDto = new Xdht0004DataRespDto();

        try {
            //类型
            //01：信用证
            //02：保函
            //03：贸易融资
            //04：福费廷
            String type = xdht0004DataReqDto.getType();
            //业务细分
            String busiClass = xdht0004DataReqDto.getBusiClass();
            //客户号
            String cusId = xdht0004DataReqDto.getCusId();
            ResultDto<List<CusAccountInfoDto>> cusDto = cmisCusClientService.queryAccount(cusId);
            if (Objects.nonNull(cusDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cusDto.getCode()) && CollectionUtils.nonEmpty(cusDto.getData())) {
                String s = JSON.toJSONString(cusDto.getData().get(0));
                CusAccountInfoDto cusAccountInfoDto = JSON.parseObject(s, CusAccountInfoDto.class);
                BeanUtils.copyProperties(cusAccountInfoDto, xdht0004DataRespDto);
            }

            Map param = new HashMap<>();
            // 系统日期获取
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            param.put("cusId", cusId);
            if (!Objects.isNull(busiClass)) {
                param.put("prdId", busiClass);
                if (Objects.equals(busiClass, "412265")) {
                    // 国际信用证开立
                    param.put("bizType", "11010101");
                } else if (Objects.equals(busiClass, "410002")) {
                    // 国内信用证开立
                    param.put("bizType", "11020101");
                } else if (Objects.equals(busiClass, "042062")) {
                    // 非融资性保函（国内）
                    param.put("bizType", "13010101");
                } else if (Objects.equals(busiClass, "410003")) {
                    // 非融资性保函（国际）
                    param.put("bizType", "13020101");
                } else if (Objects.equals(busiClass, "610002")) {
                    // 融资性保函（国内）
                    param.put("bizType", "13010201");
                } else if (Objects.equals(busiClass, "610003")) {
                    // 融资性保函（国际）
                    param.put("bizType", "13020201");
                }
            }

            param.put("endDate", openday);
            param.put("contStatus", CmisBizConstants.IQP_CONT_STS_200);
            //信用证
            if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_01, type)) {
                //查询信用证合同列表
                List<CtrTfLocCont> ctrTfLocContlist = ctrTfLocContMapper.getContInfoByCusIdAndBizType(param);
                if (ctrTfLocContlist == null || ctrTfLocContlist.size() == 0) {
                    throw new BizException(null, "", null, "不存在该客户下所选业务类型的合同");
                }

                //计算可用余额
                List list = new ArrayList();
                for (CtrTfLocCont ctrTfLocCont : ctrTfLocContlist) {
                    String contNo = ctrTfLocCont.getContNo();//合同编号
                    String serno = ctrTfLocCont.getSerno();//申请流水号
                    String contCnNo = ctrTfLocCont.getContCnNo();//中文合同编号
                    String contType = ctrTfLocCont.getContType();//合同类型
                    String prdName = ctrTfLocCont.getPrdName();//产品名称
                    String cusName = ctrTfLocCont.getCusName();//客户名称
                    BigDecimal CvtCnyAmt = ctrTfLocCont.getCvtCnyAmt();//折算人民币金额
                    BigDecimal ContAmt = ctrTfLocCont.getContAmt();//合同金额
                    String curType = ctrTfLocCont.getCurType();//合同币种
                    String guarMode = ctrTfLocCont.getGuarMode();//担保方式
                    String exchangeRate = ctrTfLocCont.getExchangeRate();//汇率
                    String startDate = ctrTfLocCont.getStartDate();//起始日
                    startDate = startDate.replaceAll("-", "");
                    String endDate = ctrTfLocCont.getEndDate();//到期日
                    endDate = endDate.replaceAll("-", "");
                    String inputId = ctrTfLocCont.getInputId();//登记人
                    String managerId = ctrTfLocCont.getManagerId();//管户经理
                    BigDecimal bailPerc = Optional.ofNullable(ctrTfLocCont.getBailPerc()).orElse(BigDecimal.ZERO);
                    // 获取合同已用金额
                    CmisLmt0060ReqDto cmisLmt0060ReqDto = new CmisLmt0060ReqDto();
                    cmisLmt0060ReqDto.setDealBizNo(contNo);
                    logger.info("调用cmisLmt0060开始，参数：{}", JSON.toJSONString(cmisLmt0060ReqDto));
                    ResultDto<CmisLmt0060RespDto> cmisLmt0060RespDtoResultDto = cmisLmtClientService.cmislmt0060(cmisLmt0060ReqDto);
                    logger.info("调用cmisLmt0060开始，返回：{}", JSON.toJSONString(cmisLmt0060RespDtoResultDto));
                    BigDecimal avlBal = BigDecimal.ZERO;
                    if (Objects.nonNull(cmisLmt0060RespDtoResultDto) && Objects.equals(cmisLmt0060RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)
                            && Objects.nonNull(cmisLmt0060RespDtoResultDto.getData())
                            && Objects.equals(cmisLmt0060RespDtoResultDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
                        CmisLmt0060RespDto data = cmisLmt0060RespDtoResultDto.getData();
                        avlBal = ContAmt.subtract(data.getSpacBalanceCny());
                    }
                    //根据合同类型判断如果是一般额合同则判断是否已经用该合同进行开证如果已开证则不将该合同发送给国结
                    Map<String, Object> map = new HashMap<>();
                    map.put("contNo", contNo);
                    map.put("serno", serno);
//                    map.put("bailAcct", accNo);
                    map.put("cnContNo", contCnNo);
                    map.put("contType", contType);
                    map.put("bizType", "5");
                    map.put("prdName", prdName);
                    map.put("cusName", cusName);
                    map.put("cusId", cusId);
                    map.put("curType", curType);
                    map.put("contAmt", ContAmt);
                    map.put("avlBal", avlBal);
                    map.put("bailRate", bailPerc.doubleValue());
                    map.put("assureMeans", guarMode);
                    map.put("exchgRate", exchangeRate);
                    map.put("contStartDate", startDate);
                    map.put("contEndDate", endDate);
                    map.put("inputId", inputId);
                    map.put("managerId", managerId);
                    map.put("loanForm", "");
                    list.add(map);

                    xdht0004DataRespDto.setList(list);
                }
            } else if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_02, type)) {
                //02：保函
                //查询保函合同列表
                List<CtrCvrgCont> ctrCvrgContList = ctrCvrgContMapper.getCvrgContByCusIdAndBizType(param);
                if (ctrCvrgContList == null || ctrCvrgContList.size() == 0) {
                    throw new BizException(null, "", null, "不存在该客户下所选业务类型的合同");
                }
                List list = new ArrayList();
                for (CtrCvrgCont ctrCvrgCont : ctrCvrgContList) {
                    String contNo = ctrCvrgCont.getContNo();//合同编号
                    String serno = ctrCvrgCont.getSerno();//申请流水号
                    String contCnNo = ctrCvrgCont.getContCnNo();//中文合同编号
                    String contType = ctrCvrgCont.getContType();//合同类型
                    String prdName = ctrCvrgCont.getPrdName();//产品名称
                    String cusName = ctrCvrgCont.getCusName();//客户名称
                    BigDecimal CvtCnyAmt = ctrCvrgCont.getCvtCnyAmt();//折算人民币金额
                    BigDecimal ContAmt = ctrCvrgCont.getContAmt();//合同金额
                    String curType = ctrCvrgCont.getCurType();//合同币种
                    String guarMode = ctrCvrgCont.getGuarMode();//担保方式
                    String exchangeRate = ctrCvrgCont.getExchangeRate();//汇率
                    String startDate = ctrCvrgCont.getStartDate();//起始日
                    startDate = startDate.replaceAll("-", "");
                    String endDate = ctrCvrgCont.getEndDate();//到期日
                    endDate = endDate.replaceAll("-", "");
                    String inputId = ctrCvrgCont.getInputId();//登记人
                    String managerId = ctrCvrgCont.getManagerId();//管户经理

                    //保证金比例
                    BigDecimal bailRate = Optional.ofNullable(ctrCvrgCont.getBailPerc()).orElse(BigDecimal.ZERO);
                    // 获取合同已用金额
                    CmisLmt0060ReqDto cmisLmt0060ReqDto = new CmisLmt0060ReqDto();
                    cmisLmt0060ReqDto.setDealBizNo(contNo);
                    logger.info("调用cmisLmt0060开始，参数：{}", JSON.toJSONString(cmisLmt0060ReqDto));
                    ResultDto<CmisLmt0060RespDto> cmisLmt0060RespDtoResultDto = cmisLmtClientService.cmislmt0060(cmisLmt0060ReqDto);
                    logger.info("调用cmisLmt0060开始，返回：{}", JSON.toJSONString(cmisLmt0060RespDtoResultDto));
                    BigDecimal avlBal = BigDecimal.ZERO;
                    if (Objects.nonNull(cmisLmt0060RespDtoResultDto) && Objects.equals(cmisLmt0060RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)
                            && Objects.nonNull(cmisLmt0060RespDtoResultDto.getData())
                            && Objects.equals(cmisLmt0060RespDtoResultDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
                        CmisLmt0060RespDto data = cmisLmt0060RespDtoResultDto.getData();
                        avlBal = ContAmt.subtract(data.getSpacBalanceCny());
                    }
                    //根据合同类型判断如果是一般额合同则判断是否已经用该合同进行开证如果已开证则不将该合同发送给国结
                    Map<String, Object> map = new HashMap<>();
                    map.put("contNo", contNo);
                    map.put("serno", serno);
//                    map.put("bailAcct", accNo);
                    map.put("cnContNo", contCnNo);
                    map.put("contType", contType);
                    map.put("bizType", "4");
                    map.put("prdName", prdName);
                    map.put("cusName", cusName);
                    map.put("cusId", cusId);
                    map.put("curType", curType);
                    map.put("contAmt", ContAmt);
                    map.put("avlBal", avlBal);
                    map.put("bailRate", bailRate.doubleValue());
                    map.put("assureMeans", guarMode);
                    map.put("exchgRate", exchangeRate);
                    map.put("contStartDate", startDate);
                    map.put("contEndDate", endDate);
                    map.put("inputId", inputId);
                    map.put("managerId", managerId);
                    map.put("loanForm", "");
                    list.add(map);
                    xdht0004DataRespDto.setList(list);
                }

            } else if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_03, type) || Objects.equals(CommonConstance.CONT_QUERY_TYPE_04, type)) {
                //03：贸易融资,04：福费廷
                //查询贸易融资/福费廷合同列表
                if (Objects.equals(CommonConstance.CONT_QUERY_TYPE_03, type)) {
                    param.put("contType", CmisCommonConstants.STD_BUSI_TYPE_04);
                    ResultDto<List<LmtSubPrdMappConfDto>> listResultDto = cmisLmtClientService.selectLimitSubNoByPrdId(busiClass);
                    if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode()) && CollectionUtils.nonEmpty(listResultDto.getData())) {
                        param.put("bizType", listResultDto.getData().get(0).getLimitSubNo());
                    }
                } else {
                    param.put("contType", CmisCommonConstants.STD_BUSI_TYPE_05);
                    param.put("bizType", "12010102");
                    param.remove("prdId");
                }
                java.util.List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.getCtrContByCusIdAndBizType(param);
                if (ctrLoanContList == null || ctrLoanContList.size() == 0) {
                    throw new BizException(null, "", null, "不存在该客户下所选业务类型的合同");
                }
                List list = new ArrayList();
                for (CtrLoanCont ctrLoanCont : ctrLoanContList) {
                    String contNo = ctrLoanCont.getContNo();//合同编号
                    String serno = ctrLoanCont.getIqpSerno();//申请流水号
                    String contCnNo = ctrLoanCont.getContCnNo();//中文合同编号
                    String contType = ctrLoanCont.getContType();//合同类型
                    String prdName = ctrLoanCont.getPrdName();//产品名称
                    String cusName = ctrLoanCont.getCusName();//客户名称
                    BigDecimal ContAmt = Optional.ofNullable(ctrLoanCont.getContAmt()).orElse(BigDecimal.ZERO);//合同金额
                    String curType = ctrLoanCont.getCurType();//合同币种
                    String guarMode = ctrLoanCont.getGuarWay();//担保方式
                    BigDecimal exchangeRate = ctrLoanCont.getContRate();//汇率
                    String startDate = ctrLoanCont.getContStartDate();//起始日
                    startDate = startDate.replaceAll("-", "");
                    String endDate = ctrLoanCont.getContEndDate();//到期日
                    endDate = endDate.replaceAll("-", "");
                    String inputId = ctrLoanCont.getInputId();//登记人
                    String managerId = ctrLoanCont.getManagerId();//管户经理
                    String seajnt = ctrLoanCont.getIsSeajnt();// 是否无缝对接
                    BigDecimal bailPerc = Optional.ofNullable(ctrLoanCont.getBailPerc()).orElse(BigDecimal.ZERO);//保证金比例

                    // 获取合同已用金额
                    CmisLmt0060ReqDto cmisLmt0060ReqDto = new CmisLmt0060ReqDto();
                    cmisLmt0060ReqDto.setDealBizNo(contNo);
                    logger.info("调用cmisLmt0060开始，参数：{}", JSON.toJSONString(cmisLmt0060ReqDto));
                    ResultDto<CmisLmt0060RespDto> cmisLmt0060RespDtoResultDto = cmisLmtClientService.cmislmt0060(cmisLmt0060ReqDto);
                    logger.info("调用cmisLmt0060开始，返回：{}", JSON.toJSONString(cmisLmt0060RespDtoResultDto));
                    BigDecimal loanBalance = BigDecimal.ZERO;
                    if (Objects.nonNull(cmisLmt0060RespDtoResultDto) && Objects.equals(cmisLmt0060RespDtoResultDto.getCode(), SuccessEnum.CMIS_SUCCSESS.key)
                            && Objects.nonNull(cmisLmt0060RespDtoResultDto.getData())
                            && Objects.equals(cmisLmt0060RespDtoResultDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
                        CmisLmt0060RespDto data = cmisLmt0060RespDtoResultDto.getData();
                        loanBalance = ContAmt.subtract(data.getSpacBalanceCny());
                    }

                    Map<String, Object> map = new HashMap<>();
                    map.put("contNo", contNo);
                    map.put("serno", serno);
                    map.put("bailAcct", "");
                    map.put("cnContNo", contCnNo);
                    map.put("contType", contType);
                    map.put("bizType", "1");
                    map.put("prdName", prdName);
                    map.put("cusName", cusName);
                    map.put("cusId", cusId);
                    map.put("curType", curType);
                    map.put("contAmt", ContAmt);
                    map.put("avlBal", loanBalance);
                    map.put("bailRate", bailPerc.doubleValue());
                    map.put("assureMeans", guarMode);
                    map.put("exchgRate", exchangeRate);
                    map.put("contStartDate", startDate);
                    map.put("contEndDate", endDate);
                    map.put("inputId", inputId);
                    map.put("managerId", managerId);
                    if (Objects.equals(seajnt, "1")) {
                        map.put("loanForm", "4");//是否无缝对接 无缝对接传值4 国结系统 其他传0
                    } else {
                        map.put("loanForm", "0");//是否无缝对接
                    }

                    list.add(map);
                    xdht0004DataRespDto.setList(list);
                }
            } else {
                throw new BizException(null, "", null, "类型传入值错误！");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            throw new BizException(null, "", null, e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            throw new Exception(e.getMessage());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataRespDto));
        return xdht0004DataRespDto;
    }

}
