package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.LmtGrpMemRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0064Service
 * @类描述: 成员客户审批是否通过校验
 * @功能描述: 成员客户审批是否通过校验
 * @创建人: css
 * @创建时间: 2021年7月29日21:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0064Service {

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    /**
     * @方法名称: riskItem0064
     * @方法描述: 成员客户审批是否通过校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021年7月29日21:32:20
     * @修改记录: 废弃  -- 2021-8-23  by马顺
     */

     @Deprecated
    public RiskResultDto riskItem0064(QueryModel queryModel) {
        // 执行结果集
        RiskResultDto riskResultDto = new RiskResultDto();
        // 业务流水号
        String grpSerno = queryModel.getCondition().get("bizId").toString();
        // 执行结果 默认通过
        String riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_0;
        // 执行结果描述
        String riskResultDesc ="集团项下成员客户申报审批校验通过！";
        LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(grpSerno);
        if(lmtGrpApp == null){
            riskResultDesc = "当前集团授信申报["+grpSerno+"]不存在!";
            riskResultDto.setRiskResultType(riskResultType);
            riskResultDto.setRiskResultDesc(riskResultDesc);
            return riskResultDto;
        }
        // 授信类型
        String lmtType = lmtGrpApp.getLmtType();
        //  根据集团授信申请流水号获取成员客户信息
        List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        if(list.isEmpty() || list.size() == 0){
            riskResultDesc = "当前集团授信申报["+grpSerno+"]成员客户信息不存在!";
            riskResultDto.setRiskResultType(riskResultType);
            riskResultDto.setRiskResultDesc(riskResultDesc);
            return riskResultDto;
        }
        for (int i = 0; i < list.size() ; i++) {
            // 获取成员客户审批结果信息
            LmtGrpMemRel lmtGrpMemRel = list.get(i);
            if(CmisCommonConstants.LMT_TYPE_02.equals(lmtType)){ // 变更
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtChg())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        riskResultDesc += "成员客户["+lmtGrpMemRel.getCusId()+"]审批未通过!";
                        riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_1;
                        break;
                    }
                }
            }else if(CmisCommonConstants.LMT_TYPE_07.equals(lmtType)){ // 预授信细化
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtRefine())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        riskResultDesc += "成员客户["+lmtGrpMemRel.getCusId()+"]审批未通过!";
                        riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_1;
                        break;
                    }
                }
            }else if(CmisCommonConstants.LMT_TYPE_08.equals(lmtType)){ // 额度调剂
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsCurtAdjust())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        riskResultDesc += "成员客户["+lmtGrpMemRel.getCusId()+"]审批未通过!";
                        riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_1;
                        break;
                    }
                }
            }else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                if(CmisCommonConstants.YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())){
                    // 不填报时,不校验当前成员客户
                    continue;
                }else{
                    if(!CmisCommonConstants.WF_STATUS_997.equals(lmtGrpMemRel.getManagerIdSubmitStatus())){
                        riskResultDesc += "成员客户["+lmtGrpMemRel.getCusId()+"]审批未通过!";
                        riskResultType = CmisRiskConstants.RISK_RESULT_TYPE_1;
                        break;
                    }
                }
            }

        }
        riskResultDto.setRiskResultType(riskResultType);
        riskResultDto.setRiskResultDesc(riskResultDesc);
        return riskResultDto;
    }
}
