package cn.com.yusys.yusp.web.server.xdht0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0001.req.Xdht0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0001.resp.Xdht0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询省心E付合同信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0001:查询省心E付合同信息")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0001Resource.class);

    /**
     * 交易码：xdht0001
     * 交易描述：查询省心E付合同信息
     *
     * @param xdht0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询省心E付合同信息")
    @PostMapping("/xdht0001")
    protected @ResponseBody
    ResultDto<Xdht0001DataRespDto> xdht0001(@Validated @RequestBody Xdht0001DataReqDto xdht0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001DataReqDto));
        Xdht0001DataRespDto xdht0001DataRespDto = new Xdht0001DataRespDto();// 响应Dto:查询省心E付合同信息
        ResultDto<Xdht0001DataRespDto> xdht0001DataResultDto = new ResultDto<>();
        try {
            // 从xdht0001DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdht0001DataRespDto对象开始
            // TODO 封装xdht0001DataRespDto对象结束
            // 封装xdht0001DataResultDto中正确的返回码和返回信息
            xdht0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, e.getMessage());
            // 封装xdht0001DataResultDto中异常返回码和返回信息
            xdht0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0001DataRespDto到xdht0001DataResultDto中
        xdht0001DataResultDto.setData(xdht0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001DataRespDto));
        return xdht0001DataResultDto;
    }
}
