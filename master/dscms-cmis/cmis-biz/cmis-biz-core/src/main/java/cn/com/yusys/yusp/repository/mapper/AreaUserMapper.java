/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.AreaUser;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaUserMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-13 20:05:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AreaUserMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AreaUser selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AreaUser> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AreaUser record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AreaUser record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AreaUser record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AreaUser record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    
    /** 
     * @author zlf
     * @date 2021/4/26 15:04
     * @version 1.0.0
     * @desc    根据区域编号查询表信息
     * @修改历史: 修改时间    修改人员    修改原因
    */
    List<AreaUser> selectByAreaNo(@Param("areaNo") String areaNo);
    /**
    * @author zlf
    * @date 2021/6/3 9:46
    * @version 1.0.0
    * @desc    区域删除时配置的区域信息全部删除
    * @修改历史  修改时间 修改人员 修改原因
    */
    int deleteByPrimaryAreaNo(@Param("areaNo") String areaNo);

    /**
     * 分中心负责人工号
     * @param userNo
     * @return
     */
    List<String> getOrgIdsByUserNo(@Param("userNo") String userNo);

    /**
     * 通过区域名称查询“用户号列表”
     * @param areaName
     * @return
     * @desc    根据
     * @修改历史  修改时间 修改人员 修改原因
     */
    List<String> getUserNoByAreaName(@Param("areaName") String areaName);
}