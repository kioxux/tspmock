/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherRecordAccpSignOrAllPldApp
 * @类描述: other_record_accp_sign_or_all_pld_app数据实体类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-07 21:39:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_record_accp_sign_or_all_pld_app")
public class OtherRecordAccpSignOrAllPldApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户评级 **/
	@Column(name = "CUS_CRD_GRADE", unique = false, nullable = true, length = 5)
	private String cusCrdGrade;
	
	/** 拟签发起始日 **/
	@Column(name = "PLAN_ISS_START_DATE", unique = false, nullable = true, length = 20)
	private String planIssStartDate;
	
	/** 票据期限 **/
	@Column(name = "DRFT_TERM", unique = false, nullable = true, length = 10)
	private Integer drftTerm;
	
	/** 备案票据类型 **/
	@Column(name = "PLD_DRFT_TYPE", unique = false, nullable = true, length = 5)
	private String pldDrftType;
	
	/** 申请签发金额 **/
	@Column(name = "APP_ISS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appIssAmt;
	
	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 全资质押业务类型 **/
	@Column(name = "BIZ_QZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizQzType;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailRate;
	
	/** 利率 **/
	@Column(name = "RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rate;

	/** 全资质押利率 **/
	@Column(name = "QZ_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal qzRate;
	
	/** 上浮比例 **/
	@Column(name = "UP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal upRate;

	/** 全资质押上浮比例 **/
	@Column(name = "QZ_UP_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal qzUpRate;
	
	/** 担保方式或质押类型 **/
	@Column(name = "GUAR_IMN_TYPE", unique = false, nullable = true, length = 5)
	private String guarImnType;
	
	/** 质押类型 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款利率 **/
	@Column(name = "LOAN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanRate;
	
	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 10)
	private Integer loanTerm;
	
	/** 提供质押存单、结构性存款金额 **/
	@Column(name = "IMN_STR_DEP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal imnStrDepAmt;
	
	/** 存单、结构性存款期限 **/
	@Column(name = "DEPOSIT_STR_DEP_TERM", unique = false, nullable = true, length = 10)
	private Integer depositStrDepTerm;
	
	/** 分支行申请理由 **/
	@Column(name = "BRANCH_APP_RESN", unique = false, nullable = true, length = 4000)
	private String branchAppResn;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 是否上调审批权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = false, length = 5)
	private String isUpperApprAuth;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCrdGrade
	 */
	public void setCusCrdGrade(String cusCrdGrade) {
		this.cusCrdGrade = cusCrdGrade;
	}
	
    /**
     * @return cusCrdGrade
     */
	public String getCusCrdGrade() {
		return this.cusCrdGrade;
	}
	
	/**
	 * @param planIssStartDate
	 */
	public void setPlanIssStartDate(String planIssStartDate) {
		this.planIssStartDate = planIssStartDate;
	}
	
    /**
     * @return planIssStartDate
     */
	public String getPlanIssStartDate() {
		return this.planIssStartDate;
	}
	
	/**
	 * @param drftTerm
	 */
	public void setDrftTerm(Integer drftTerm) {
		this.drftTerm = drftTerm;
	}
	
    /**
     * @return drftTerm
     */
	public Integer getDrftTerm() {
		return this.drftTerm;
	}
	
	/**
	 * @param pldDrftType
	 */
	public void setPldDrftType(String pldDrftType) {
		this.pldDrftType = pldDrftType;
	}
	
    /**
     * @return pldDrftType
     */
	public String getPldDrftType() {
		return this.pldDrftType;
	}
	
	/**
	 * @param appIssAmt
	 */
	public void setAppIssAmt(java.math.BigDecimal appIssAmt) {
		this.appIssAmt = appIssAmt;
	}
	
    /**
     * @return appIssAmt
     */
	public java.math.BigDecimal getAppIssAmt() {
		return this.appIssAmt;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizQzType
	 */
	public void setBizQzType(String bizQzType) {
		this.bizQzType = bizQzType;
	}
	
    /**
     * @return bizQzType
     */
	public String getBizQzType() {
		return this.bizQzType;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return bailRate
     */
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(java.math.BigDecimal rate) {
		this.rate = rate;
	}
	
    /**
     * @return rate
     */
	public java.math.BigDecimal getRate() {
		return this.rate;
	}

	/**
	 * @param qzRate
	 */
	public void setQzRate(java.math.BigDecimal qzRate) {
		this.qzRate = qzRate;
	}

	/**
	 * @return qzRate
	 */
	public java.math.BigDecimal getQzRate() {
		return this.qzRate;
	}
	
	/**
	 * @param upRate
	 */
	public void setUpRate(java.math.BigDecimal upRate) {
		this.upRate = upRate;
	}
	
    /**
     * @return upRate
     */
	public java.math.BigDecimal getUpRate() {
		return this.upRate;
	}

	/**
	 * @param qzUpRate
	 */
	public void setQzUpRate(java.math.BigDecimal qzUpRate) {
		this.qzUpRate = qzUpRate;
	}

	/**
	 * @return qzUpRate
	 */
	public java.math.BigDecimal getQzUpRate() {
		return this.qzUpRate;
	}
	
	/**
	 * @param guarImnType
	 */
	public void setGuarImnType(String guarImnType) {
		this.guarImnType = guarImnType;
	}
	
    /**
     * @return guarImnType
     */
	public String getGuarImnType() {
		return this.guarImnType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanRate
	 */
	public void setLoanRate(java.math.BigDecimal loanRate) {
		this.loanRate = loanRate;
	}
	
    /**
     * @return loanRate
     */
	public java.math.BigDecimal getLoanRate() {
		return this.loanRate;
	}
	
	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	
    /**
     * @return loanTerm
     */
	public Integer getLoanTerm() {
		return this.loanTerm;
	}
	
	/**
	 * @param imnStrDepAmt
	 */
	public void setImnStrDepAmt(java.math.BigDecimal imnStrDepAmt) {
		this.imnStrDepAmt = imnStrDepAmt;
	}
	
    /**
     * @return imnStrDepAmt
     */
	public java.math.BigDecimal getImnStrDepAmt() {
		return this.imnStrDepAmt;
	}
	
	/**
	 * @param depositStrDepTerm
	 */
	public void setDepositStrDepTerm(Integer depositStrDepTerm) {
		this.depositStrDepTerm = depositStrDepTerm;
	}
	
    /**
     * @return depositStrDepTerm
     */
	public Integer getDepositStrDepTerm() {
		return this.depositStrDepTerm;
	}
	
	/**
	 * @param branchAppResn
	 */
	public void setBranchAppResn(String branchAppResn) {
		this.branchAppResn = branchAppResn;
	}
	
    /**
     * @return branchAppResn
     */
	public String getBranchAppResn() {
		return this.branchAppResn;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) {
		this.isUpperApprAuth = isUpperApprAuth;
	}

	/**
	 * @return isUpperApprAuth
	 */
	public String getIsUpperApprAuth() {
		return this.isUpperApprAuth;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}