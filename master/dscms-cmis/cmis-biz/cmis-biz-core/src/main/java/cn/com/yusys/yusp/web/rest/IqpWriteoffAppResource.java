/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpWriteoffApp;
import cn.com.yusys.yusp.service.IqpWriteoffAppService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpWriteoffAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:31:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpwriteoffapp")
public class IqpWriteoffAppResource {
    @Autowired
    private IqpWriteoffAppService iqpWriteoffAppService;

	/**
     * 全表查询.
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpWriteoffApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpWriteoffApp> list = iqpWriteoffAppService.selectAll(queryModel);
        return new ResultDto<List<IqpWriteoffApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpWriteoffApp>> index(QueryModel queryModel) {
        List<IqpWriteoffApp> list = iqpWriteoffAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpWriteoffApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{iqpSerno}")
    protected ResultDto<IqpWriteoffApp> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpWriteoffApp iqpWriteoffApp = iqpWriteoffAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<IqpWriteoffApp>(iqpWriteoffApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpWriteoffApp> create(@RequestBody IqpWriteoffApp iqpWriteoffApp) throws URISyntaxException {
        iqpWriteoffAppService.insert(iqpWriteoffApp);
        return new ResultDto<IqpWriteoffApp>(iqpWriteoffApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpWriteoffApp iqpWriteoffApp) throws URISyntaxException {
        int result = iqpWriteoffAppService.update(iqpWriteoffApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpWriteoffAppService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpWriteoffAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveIqpWriteOffAppLeadInfo
     * @函数描述:通过页面引导信息保存核销申请信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveIqpWriteOffAppLeadInfo")
    protected ResultDto<Map> saveIqpWriteOffAppLeadInfo(@RequestBody IqpWriteoffApp iqpWriteoffApp) throws URISyntaxException {
        Map map = iqpWriteoffAppService.saveIqpWriteOffAppLeadInfo(iqpWriteoffApp);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:deleteIqpWriteOffDetailLogic
     * @函数描述:通过主键对核销申请进行逻辑删除
     * @参数与返回说明: iqpSerno
     * @算法描述: 对数据类型进行更新，并修改明细列表中的数据类型
     */
    @GetMapping("/deleteIqpWriteOffAppLogic/{iqpSerno}")
    protected ResultDto<Map> deleteIqpWriteOffAppLogic(@PathVariable("iqpSerno") String iqpSerno) {
        Map map = iqpWriteoffAppService.deleteIqpWriteOffAppLogic(iqpSerno);
        return new ResultDto<Map>(map);
    }
}
