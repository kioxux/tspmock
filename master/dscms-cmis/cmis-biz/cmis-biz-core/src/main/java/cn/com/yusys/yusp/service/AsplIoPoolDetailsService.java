/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.InPoolAssetListDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AsplIoPoolDetailsMapper;
import cn.com.yusys.yusp.vo.AccAccpVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.aspectj.apache.bcel.classfile.Module;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplIoPoolDetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 11:15:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplIoPoolDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AsplIoPoolDetailsService.class);

    @Autowired
    private AsplIoPoolService asplIoPoolservice;

    @Autowired
    private AsplIoPoolDetailsMapper asplIoPoolDetailsMapper;

    @Autowired
    private AsplAssetsListService asplAssetsListService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplIoPoolDetails selectByPrimaryKey(String pkId) {
        return asplIoPoolDetailsMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplIoPoolDetails> selectAll(QueryModel model) {
        List<AsplIoPoolDetails> records = (List<AsplIoPoolDetails>) asplIoPoolDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplIoPoolDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplIoPoolDetails> list = asplIoPoolDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplIoPoolDetails record) {
        return asplIoPoolDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplIoPoolDetails record) {
        return asplIoPoolDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplIoPoolDetails record) {
        return asplIoPoolDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplIoPoolDetails record) {
        return asplIoPoolDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplIoPoolDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplIoPoolDetailsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertAsplIoPoolDetailsList(List<AsplIoPoolDetails> asplIoPoolDetailsList){
        return asplIoPoolDetailsMapper.insertAsplIoPoolDetailsList(asplIoPoolDetailsList);
    }

    /**
     * @方法名称: inPoolAssetList
     * @方法描述: 入池资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<InPoolAssetListDto> inPoolAssetList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("isPool", CmisLmtConstants.YES_NO_Y);
        return asplIoPoolDetailsMapper.inPoolAssetList(model);
    }

    /**
     * @方法名称: exportInPoolAssetList
     * @方法描述: 导出入池资产清单
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ProgressDto exportInPoolAssetList(Map<String,String> map) {
        try {
            QueryModel model = new QueryModel();
            for(Map.Entry<String,String> entry : map.entrySet()){
                model.addCondition(entry.getKey(),entry.getValue());
            }
            model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
//            model.addCondition("isPool", CmisCommonConstants.STD_ZB_YES_NO_1);// 是否入池
//            model.addCondition("isPledge", CmisCommonConstants.STD_ZB_YES_NO_1);// 是否质押
            DataAcquisition dataAcquisition = (size, page, Object) -> {
                QueryModel queryModeTemp = (QueryModel)Object;
                // 把model拷贝到 queryModeTemp
                BeanUtils.copyProperties(model,queryModeTemp);
                queryModeTemp.setSize(page);
                queryModeTemp.setPage(size);
                for(Map.Entry<String,String> entry : map.entrySet()){
                    queryModeTemp.addCondition(entry.getKey(),entry.getValue());
                }
                return exportInPoolAssetsListAll(queryModeTemp);
            };
            // excel的domain暂无
            ExportContext exportContext = ExportContext.of(InPoolAssetListExport.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
            return ExcelUtils.asyncExport(exportContext);
        } catch (Exception e) {
            throw new YuspException(EcbEnum.E_EXPORTINPOOLACCEXCEL_EXCEPTION_01.key,EcbEnum.E_EXPORTINPOOLACCEXCEL_EXCEPTION_01.value);
        }

    }
    public List<InPoolAssetListExport> exportInPoolAssetsListAll(QueryModel model) {
        List<InPoolAssetListExport> inPoolAssetListExportList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplAssetsList> list = asplAssetsListService.inPoolAssetsListAll(model);
        PageHelper.clearPage();

        for (AsplAssetsList asplAssetsList : list) {
            InPoolAssetListExport inPoolAssetListExport = new InPoolAssetListExport();
            org.springframework.beans.BeanUtils.copyProperties(asplAssetsList, inPoolAssetListExport);
            String orgName = OcaTranslatorUtils.getOrgName(inPoolAssetListExport.getManagerBrId());//责任机构
            String userName = OcaTranslatorUtils.getUserName(inPoolAssetListExport.getManagerId());   //责任人
            String updOrg = OcaTranslatorUtils.getOrgName(inPoolAssetListExport.getUpdBrId());   //修改机构
            String updName = OcaTranslatorUtils.getUserName(inPoolAssetListExport.getUpdId());   //修改人
            String inpOrg = OcaTranslatorUtils.getOrgName(inPoolAssetListExport.getInputBrId());   //登记机构
            String inpName = OcaTranslatorUtils.getUserName(inPoolAssetListExport.getInputId());   //登记人
            inPoolAssetListExport.setInputBrId(inpOrg);
            inPoolAssetListExport.setInputId(inpName);
            inPoolAssetListExport.setUpdBrId(updOrg);
            inPoolAssetListExport.setUpdId(updName);
            inPoolAssetListExport.setManagerBrId(orgName);
            inPoolAssetListExport.setManagerId(userName);
            inPoolAssetListExportList.add(inPoolAssetListExport);
        }
        return inPoolAssetListExportList;
    }


    /**
     * @方法名称: selectBySelfModel
     * @方法描述: 灵活查询
     * @参数与返回说明:
     *  cusId 客户编号
     *  contNo资产池协议编号
     *  assetType 资产类型
     * @算法描述: 无
     */
    public List<AsplIoPoolDetails> selectBySelfModel(QueryModel queryModel) {
        return asplIoPoolDetailsMapper.selectBySelfModel(queryModel);
    }

    /**
     * @方法名称: selectAssetIoInfoBySerno
     * @方法描述: 出入池详情查询
     * @参数与返回说明:
     *  cusId 客户编号
     *  contNo资产池协议编号
     *  assetType 资产类型
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.xdzc0020.resp.List>  selectAssetIoInfoBySerno(String serno) {
        return asplIoPoolDetailsMapper.selectAssetIoInfoBySerno(serno);
    }

    public void selectAssetIoInfoByAssetNo() {
    }

    /**
     * @方法名称: queryAsplIoPoolDetailsDataByParams
     * @方法描述: 根据入参查询入池资产信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<AsplIoPoolDetails> queryAsplIoPoolDetailsDataByParams(Map map) {
        return asplIoPoolDetailsMapper.queryAsplIoPoolDetailsDataByParams(map);
    }

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    public List<AsplIoPoolDetails> selectBySerno(String serno) {
        return asplIoPoolDetailsMapper.selectBySerno(serno);
    }

    /**
     * 根据流水号删除
     * @param serno
     * @return
     */
    public int deleteOutpoolDetailsBySerno(String serno) {
        return asplIoPoolDetailsMapper.deleteOutpoolDetailsBySerno(serno);
    }

    /**
     * @方法名称: addOutPoolDetails
     * @方法描述: 新增出池明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<AsplIoPoolDetails>  addOutPoolDetails(Map map) {
        String returnCode = "0";
        String returnMsg = "出池明细新增成功";
        AsplIoPool asplIoPool = new AsplIoPool();
        AsplIoPoolDetails asplIoPoolDetails = new AsplIoPoolDetails();
        try{
            String assetNo = (String)map.get("assetNo");
            String serno = (String)map.get("serno");
            String contNo = (String)map.get("contNo");
            // 校验新增出池资产是否已经被选中过
            QueryModel model = new QueryModel();
            model.addCondition("assetNo",assetNo);
            model.addCondition("serno",serno);
            List<AsplIoPoolDetails> list = asplIoPoolDetailsMapper.selectByModel(model);
            if(CollectionUtils.isEmpty(list)){
                // 获取用户信息
                User userInfo = SessionUtils.getUserInformation();
                // 根据资产编号获取资产信息
                AsplAssetsList asplAssetsList = asplAssetsListService.selectByAssetNo(contNo,assetNo);
                asplIoPoolDetails.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
                asplIoPoolDetails.setSerno(serno);//业务流水号
                asplIoPoolDetails.setContNo(contNo);//合同编号
                asplIoPoolDetails.setAssetNo(asplAssetsList.getAssetNo());//资产编号
                asplIoPoolDetails.setAssetType(asplAssetsList.getAssetType());//资产类型
                asplIoPoolDetails.setAssetValue(asplAssetsList.getAssetValue());//资产价值
                asplIoPoolDetails.setAssetEndDate(asplAssetsList.getAssetEndDate());//资产到期日
                asplIoPoolDetails.setAssetStatus(asplAssetsList.getAssetStatus());//资产状态
                asplIoPoolDetails.setIsPool(CmisCommonConstants.INOUT_TYPE_0);//是否入池（出池）
                asplIoPoolDetails.setIsPledge(CmisCommonConstants.YES_NO_0);// 是否解质押（0 是 1 否）
                asplIoPoolDetails.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                asplIoPoolDetails.setInputId(userInfo.getLoginCode());//登记人
                asplIoPoolDetails.setInputBrId(userInfo.getOrg().getCode());//登记机构
                asplIoPoolDetails.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
                asplIoPoolDetails.setUpdId(userInfo.getLoginCode());//最近修改人
                asplIoPoolDetails.setUpdBrId(userInfo.getOrg().getCode());//最近修改机构
                asplIoPoolDetails.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//最近修改日期
                asplIoPoolDetails.setCreateTime(DateUtils.getCurrDate());//创建时间
                asplIoPoolDetails.setUpdateTime(DateUtils.getCurrDate());//修改时间
                // 新增
                insertSelective(asplIoPoolDetails);
                // 更新出池批次总金额
                updateLmtAmt(serno);
            }else{
                throw BizException.error(null,"9999","资产【"+assetNo+"】已存在待出池列表中");
            }
        }catch (Exception e){
            logger.error("出池明细新增失败！",e);
            returnCode = "9999";
            returnMsg = e.getMessage();
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }finally{
            return new ResultDto<AsplIoPoolDetails>().code(returnCode).message(returnMsg);
        }
    }

    /**
     * @方法名称: delOutPoolDetails
     * @方法描述: 删除出池明细
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<AsplIoPoolDetails> delOutPoolDetails(Map map) {
        String returnCode = "0";
        String returnMsg = "出池明细删除成功";
        try{
            String assetNo = (String)map.get("assetNo");
            String serno = (String)map.get("serno");
            String contNo = (String)map.get("contNo");
            asplIoPoolDetailsMapper.delOutPoolDetails(serno,assetNo);
            // 更新出池批次总金额
            updateLmtAmt(serno);
        }catch (Exception e){
            logger.error("出池明细删除失败！",e);
            returnCode = "9999";
            returnMsg = e.getMessage();
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }finally{
            return new ResultDto<AsplIoPoolDetails>().code(returnCode).message(returnMsg);
        }
    }

    /**
     * 更新出入池批次总金额
     * @return
     */
    public int updateLmtAmt(String serno) {
        // 计算该笔批次明细的总价值
        BigDecimal lmtAmt = computeSumAmt(serno);
        return asplIoPoolservice.updateLmtAmtBySerno(serno,lmtAmt);
    }
    /**
     * 计算批次总价值
     * @param serno
     * @return
     */
    public BigDecimal computeSumAmt(String serno) {
        return asplIoPoolDetailsMapper.computeSumAmt(serno);
    }
}
