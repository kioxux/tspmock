/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-doc模块
 * @类名称: CreditQryBizReal
 * @类描述: credit_qry_biz_real数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-06 17:07:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "credit_qry_biz_real")
public class CreditQryBizReal extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 征信与业务关联流水号 **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "CQBR_SERNO")
    private String cqbrSerno;

    /** 业务流水号 **/
    @Column(name = "BIZ_SERNO", unique = false, nullable = false, length = 40)
    private String bizSerno;

    /** 查询对象证件类型 **/
    @Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
    private String certType;

    /** 查询对象证件号 **/
    @Column(name = "CERT_CODE", unique = false, nullable = false, length = 30)
    private String certCode;

    /** 征信查询对象号 **/
    @Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
    private String cusId;

    /** 征信查询对象名 **/
    @Column(name = "CUS_NAME", unique = false, nullable = false, length = 300)
    private String cusName;

    /** 与主借款人关系 **/
    @Column(name = "BORROW_REL", unique = false, nullable = false, length = 5)
    private String borrowRel;

    /** 征信查询场景 **/
    @Column(name = "SCENE", unique = false, nullable = false, length = 5)
    private String scene;

    /** 征信查询流水号 **/
    @Column(name = "CRQL_SERNO", unique = false, nullable = false, length = 40)
    private String crqlSerno;

    /** 创建时间 **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private Date createTime;

    /** 修改时间 **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private Date updateTime;

    /** 征信查询类别 **/
    @Column(name = "QRY_CLS", unique = false, nullable = true, length = 5)
    private String qryCls;

    /**
     * @return qryCls
     */
    public String getQryCls() {
        return this.qryCls;
    }

    /**
     * @param qryCls
     */
    public void setQryCls(String qryCls) {
        this.qryCls = qryCls;
    }

    /**
     * @return cqbrSerno
     */
    public String getCqbrSerno() {
        return this.cqbrSerno;
    }

    /**
     * @param cqbrSerno
     */
    public void setCqbrSerno(String cqbrSerno) {
        this.cqbrSerno = cqbrSerno;
    }

    /**
     * @return bizSerno
     */
    public String getBizSerno() {
        return this.bizSerno;
    }

    /**
     * @param bizSerno
     */
    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    /**
     * @return certType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType;
    }

    /**
     * @return certCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return borrowRel
     */
    public String getBorrowRel() {
        return this.borrowRel;
    }

    /**
     * @param borrowRel
     */
    public void setBorrowRel(String borrowRel) {
        this.borrowRel = borrowRel;
    }

    /**
     * @return scene
     */
    public String getScene() {
        return this.scene;
    }

    /**
     * @param scene
     */
    public void setScene(String scene) {
        this.scene = scene;
    }

    /**
     * @return crqlSerno
     */
    public String getCrqlSerno() {
        return this.crqlSerno;
    }

    /**
     * @param crqlSerno
     */
    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    /**
     * @return createTime
     */
    public Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return updateTime
     */
    public Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


}