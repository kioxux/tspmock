package cn.com.yusys.yusp.service.server.xdcz0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.req.Xdcz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.resp.Xdcz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:未中标业务注销
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0025Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0025Service.class);

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    public Xdcz0025DataRespDto accSettle(Xdcz0025DataReqDto xdcz0025DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataReqDto));
        Xdcz0025DataRespDto xdcz0025DataRespDto = new Xdcz0025DataRespDto();
        String billno = xdcz0025DataReqDto.getBillno();
        try {
            // 根据请求参数bil_no查询是否存在保函台帐acc_cvrg记录，不存在则接口报错并提示"未查询到该笔借据号";
            AccCvrs accCvrs = accCvrsMapper.selectByBillno(billno);
            if (Objects.isNull(accCvrs)) {
                xdcz0025DataRespDto.setOpFlag(CmisBizConstants.NO);
                xdcz0025DataRespDto.setOpMsg("未查询到该笔借据号");
                return xdcz0025DataRespDto;
            } else {
                // 存在保函记录时，检查保函台帐状态，如acc_cvrg.account_status='0'则 接口报错并提示"当前保函为未生效状态，不允许结清";
                String accStatus = accCvrs.getAccStatus();
                if (Objects.equals(accStatus, "0")) {
                    xdcz0025DataRespDto.setOpFlag(CmisBizConstants.NO);
                    xdcz0025DataRespDto.setOpMsg("当前保函为未生效状态，不允许结清");
                    return xdcz0025DataRespDto;
                }
            }
            // 组装结清通知报文
            // TODO 1).使用序列码生成器生成结清流水号GEN_GL_NO

            //2).获取系统时间FSETL_DT
            LocalDate fsetlDt = LocalDate.now();
            // 根据cont_no查询保函台帐acc_cvrg获取台帐责任人号manager_id、借据编号bill_no、合同编号cont_no、账务机构号fina_br_id
            String managerId = accCvrs.getManagerId();
            String billNo = accCvrs.getBillNo();
            String contNo = accCvrs.getContNo();
            String finaBrId = accCvrs.getFinaBrId();
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
            ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
            AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());
            String managerName = adminSmUserDto.getUserName();
            // TODO 发送serviceId（serv10000000037-保函结清）以及上述1-4信息给核心系统

            xdcz0025DataRespDto.setOpFlag(CmisBizConstants.YES);
            xdcz0025DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, e.getMessage());
            xdcz0025DataRespDto.setOpFlag(CmisBizConstants.NO);
            xdcz0025DataRespDto.setOpMsg("当前保函为未生效状态，不允许结清");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataRespDto));
        return xdcz0025DataRespDto;
    }


}
