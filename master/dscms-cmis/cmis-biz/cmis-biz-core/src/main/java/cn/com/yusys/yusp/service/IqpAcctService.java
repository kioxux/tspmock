/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpAcct;
import cn.com.yusys.yusp.domain.PvpTruPayInfo;
import cn.com.yusys.yusp.repository.mapper.IqpAcctMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 11:00:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpAcctService {

    @Autowired
    private IqpAcctMapper iqpAcctMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAcct selectByPrimaryKey(String pkId) {
        return iqpAcctMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAcct> selectAll(QueryModel model) {
        List<IqpAcct> records = (List<IqpAcct>) iqpAcctMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpAcct> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAcct> list = iqpAcctMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpAcct record) {
        return iqpAcctMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpAcct record) {
        return iqpAcctMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpAcct record) {
        return iqpAcctMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpAcct record) {
        return iqpAcctMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAcctMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAcctMapper.deleteByIds(ids);
    }

    /**
     * 通过入参更新数据
     * @param delMap
     * @return
     */
    public int updateByParams(Map delMap) {
        return iqpAcctMapper.updateByParams(delMap);
    }

    public List<IqpAcct> selectByIqpSerno(String iqpSernoOld) {
        return iqpAcctMapper.selectByIqpSerno(iqpSernoOld);
    }

    /**
     * 通过业务申请流水号查询账户信息数据
     * @param iqpSerno
     * @return
     */
    public List<IqpAcct> selectAffectInfoByIqpSerno(String iqpSerno) {
        return iqpAcctMapper.selectAffectInfoByIqpSerno(iqpSerno);
    }

    /**
     * 通过入参集合查询数据信息
     * @param param
     * @return
     */
    public List<IqpAcct> selectIqpAcctInfoByParam(Map param) {
        return iqpAcctMapper.selectAffectInfoByParam(param);
    }


    /**
     * @方法名称: selectByKey
     * @方法描述: 根据acct_no查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int selectCountByKey(String acctNo) {
        return iqpAcctMapper.selectCountByKey(acctNo);
    }

    /**
     * @方法名称: updateAcctStatusByContNoSelective
     * @方法描述: 审批通过后 更新账户状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateAcctStatusByContNoSelective(Map paramMap) {
        return iqpAcctMapper.updateAcctStatusByContNoSelective(paramMap);
    }

    /**
     * @方法名称: insertByContNoAfterApp
     * @方法描述: 审批通过后 新增账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertByContNoAfterApp(Map paramMap) {
        return iqpAcctMapper.insertByContNoAfterApp(paramMap);
    }
    /**
     * 根据借据标号查询借据信息
     * @param acctNo
     * @return
     */
    public IqpAcct getIqpAcct(String acctNo) {
        return this.iqpAcctMapper.getIqpAcct(acctNo);
    }
}
