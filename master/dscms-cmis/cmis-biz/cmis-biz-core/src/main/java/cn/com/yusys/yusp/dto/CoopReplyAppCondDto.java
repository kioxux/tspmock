package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAppCond
 * @类描述: coop_reply_app_cond数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopReplyAppCondDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 申请流水号 **/
	private String serno;
	
	/** 序号 **/
	private Integer serial;
	
	/** 条件具体内容 **/
	private String condContent;
	
	/** 创建时间 **/
	private java.util.Date creatTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param serial
	 */
	public void setSerial(Integer serial) {
		this.serial = serial;
	}
	
    /**
     * @return Serial
     */	
	public Integer getSerial() {
		return this.serial;
	}
	
	/**
	 * @param condContent
	 */
	public void setCondContent(String condContent) {
		this.condContent = condContent == null ? null : condContent.trim();
	}
	
    /**
     * @return CondContent
     */	
	public String getCondContent() {
		return this.condContent;
	}
	
	/**
	 * @param creatTime
	 */
	public void setCreatTime(java.util.Date creatTime) {
		this.creatTime = creatTime;
	}
	
    /**
     * @return CreatTime
     */	
	public java.util.Date getCreatTime() {
		return this.creatTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}