/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.domain.dto.IqpContExtDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.IqpContExtVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.IqpContExtMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpContExtService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-21 15:34:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpContExtService {

    private final Logger log = LoggerFactory.getLogger(IqpContExtService.class);//定义log

    @Autowired
    private IqpContExtMapper iqpContExtMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private IqpContExtBillService iqpContExtBillService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private MessageSendService messageSendService;
    @Autowired
    private AccLoanService accLoanService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpContExt selectByPrimaryKey(String iqpSerno) {
        return iqpContExtMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpContExt> selectAll(QueryModel model) {
        List<IqpContExt> records = (List<IqpContExt>) iqpContExtMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpContExt> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpContExt> list = iqpContExtMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpContExt record) {
        return iqpContExtMapper.insert(record);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int
    appExtInit(IqpContExt record) {
        QueryModel model1 = new QueryModel();
        model1.addCondition("contNo", record.getContNo());
        model1.addCondition("apply", "1");
        model1.addCondition("oprType", "01");
        List<IqpContExt> list1 = this.selectByModel(model1);
        if (list1.size() > 0) {
            throw BizException.error(null, "999999", "\"该合同存在在途展期业务申请，请勿重复发起！\"" + "保存失败！");
        }


        // 1、根据合同编号查询合同详细信息
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(record.getContNo());
        record.setContAmt(ctrLoanCont.getContAmt());
        record.setContBalance(ctrLoanCont.getContBalance());
        record.setContNo(ctrLoanCont.getContNo());
        record.setContStartDate(ctrLoanCont.getContStartDate());
        record.setContEndDate(ctrLoanCont.getContEndDate());
        record.setContStatus(ctrLoanCont.getContStatus());
        record.setContType(ctrLoanCont.getContType());
        record.setCusId(ctrLoanCont.getCusId());
        record.setCusName(ctrLoanCont.getCusName());
        record.setLmtAccNo(ctrLoanCont.getLmtAccNo());
        record.setBelgLine(ctrLoanCont.getBelgLine());
        record.setIsUtilLmt(ctrLoanCont.getIsUtilLmt());
        record.setLmtAccNo(ctrLoanCont.getLmtAccNo());
        record.setReplyNo(ctrLoanCont.getReplyNo());
        // 2、设置申请流水号
        record.setArgNo(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP, new HashMap<>()));
        record.setIqpSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP, new HashMap<>()));
        record.setOprType(CommonConstance.OPR_TYPE_ADD);
        record.setApproveStatus(CoopPlanAppConstant.APPR_STATUS_START);
        record.setContApproveStatus(BizFlowConstant.WF_STATUS_000);
        record.setUpdDate(DateUtils.getCurrDateStr());
        record.setStatus("0"); // 展期协议状态状态 0-未处理
        return iqpContExtMapper.insert(record);
    }


    public int updateextapp(IqpContExtVo iqpContExtVo) {
        // 1、更新展期主表
        int result = this.update(iqpContExtVo.getIqpContExt());
        // 2、清空展期字表关联数据
        String contNo = iqpContExtVo.getIqpContExt().getContNo();
        String iqpSerno = iqpContExtVo.getIqpContExt().getIqpSerno();
        iqpContExtBillService.deleteByContNoAndIqpSerno(contNo, iqpSerno);
        // 3、批量插入字表数据
        List<IqpContExtBill> billList = iqpContExtVo.getBillList();
        if (!CollectionUtils.isEmpty(billList)) {
            User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
            String inputId = userInfo.getLoginCode(); // 当前用户号
            String inputIBrId = userInfo.getOrg().getCode(); // 当前用户机构
            String currDateStr = DateUtils.getCurrDateStr(); // 当前日期
            for (int i = 0; i < billList.size(); i++) {
                IqpContExtBill iqpContExtBill = billList.get(i);
                // 获取台账信息
                AccLoan accloan = accLoanService.queryAccLoanDataByBillNo(iqpContExtBill.getBillNo());
                if (null != accloan) {
                    iqpContExtBill.setExtRate(accloan.getExecRateYear());
                }
                iqpContExtBill.setSerno(StringUtils.getUUID());
                iqpContExtBill.setIqpSerno(iqpSerno);
                iqpContExtBill.setContNo(contNo);
                iqpContExtBill.setCreateTime(DateUtils.getCurrTimestamp());
                iqpContExtBill.setInputId(inputId);
                iqpContExtBill.setInputDate(currDateStr);
                iqpContExtBill.setInputBrId(inputIBrId);
                iqpContExtBill.setManagerId(iqpContExtVo.getIqpContExt().getManagerId());
                iqpContExtBill.setManagerBrId(iqpContExtVo.getIqpContExt().getManagerBrId());
                iqpContExtBill.setUpdateTime(DateUtils.getCurrTimestamp());
                iqpContExtBill.setUpdBrId(inputIBrId);
                iqpContExtBill.setUpdDate(DateUtils.getCurrDateStr());
                iqpContExtBill.setUpdId(inputId);
                iqpContExtBill.setOprType(CommonConstance.OPR_TYPE_ADD);
                iqpContExtBillService.insert(iqpContExtBill);
            }
        }
        return result;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpContExt record) {
        return iqpContExtMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpContExt record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpContExtMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpContExt record) {
        return iqpContExtMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpContExtMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpContExtMapper.deleteByIds(ids);
    }

    /**
     * 根据查询条件查询综合查询需要的展期信息返回
     *
     * @param model
     * @return
     */
    public List<IqpContExtDto> selectByModelForZhcx(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpContExtDto> list = iqpContExtMapper.selectByModelForZhcx(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 推送首页提醒事项
     *
     * @param iqpContExt,messageType,comment,inputId,inputBrId add by zhangjw 20210630
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendWbMsgNotice(IqpContExt iqpContExt, String messageType, String comment, String inputId, String inputBrId) throws Exception {
        log.info("展期申请申报审批流程申请启用授信申报审批流程【{}】，流程打回或退回操作，推送首页提升事项", iqpContExt.getIqpSerno());
        MessageSendDto messageSendDto = new MessageSendDto();
        Map<String, String> map = new HashMap<>();
        map.put("cusName", iqpContExt.getCusName());
        map.put("prdName", "展期申请");
        map.put("result", "打回");
        List<ReceivedUserDto> receivedUserList = new ArrayList<>();
        ReceivedUserDto receivedUserDto = new ReceivedUserDto();

        // 1--客户经理 2--借款人  借款人必输
        receivedUserDto.setReceivedUserType("1");// 发送人员类型
        receivedUserDto.setUserId(inputId);// 客户经理工号
        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
        receivedUserList.add(receivedUserDto);
        messageSendDto.setMessageType(CmisCommonConstants.MSG_CF_M_0016);
        messageSendDto.setParams(map);
        messageSendDto.setReceivedUserList(receivedUserList);
        messageSendService.sendMessage(messageSendDto);
    }

    /**
     * 授信申请(通过、否决 录入批复表中)
     *
     * @param iqpContExt
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleAfterEnd(IqpContExt iqpContExt, String currentUserId, String currentOrgId, String approveStatus, ResultInstanceDto resultInstanceDto) throws Exception {
        if (CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)) {
            //推送首页提醒事项
            this.sendWbMsgNotice(iqpContExt, CmisBizConstants.STD_WB_NOTICE_TYPE_2,
                    resultInstanceDto.getComment().getUserComment(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId());
        } else {
            //推送首页提醒事项
            this.sendWbMsgNotice(iqpContExt, CmisBizConstants.STD_WB_NOTICE_TYPE_3,
                    null, resultInstanceDto.getCurrentUserId(), resultInstanceDto.getCurrentOrgId());
        }
        //更新审批状态
        iqpContExt.setApproveStatus(approveStatus);
        this.update(iqpContExt);

    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 21:04
     **/
    public void createImageSpplInfo(String serno, String bizType, String instanceId,String iqpName,String spplType) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(serno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            IqpContExt iqpContExt = iqpContExtMapper.selectByPrimaryKey(serno);
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(serno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(spplType);// 影像补扫类型 07:展期申请影像补扫 09:展期协议影像补扫
            docImageSpplClientDto.setCusId(iqpContExt.getCusId());// 客户号
            docImageSpplClientDto.setCusName(iqpContExt.getCusName());// 客户名称
            docImageSpplClientDto.setContNo(iqpContExt.getContNo());// 合同编号
            docImageSpplClientDto.setInputId(iqpContExt.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(iqpContExt.getManagerBrId());// 登记机构
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), iqpContExt.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", iqpContExt.getCusName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }
}
