/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpReplyChg;
import cn.com.yusys.yusp.service.LmtGrpReplyChgService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyChgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-21 14:02:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrpreplychg")
public class LmtGrpReplyChgResource {
    @Autowired
    private LmtGrpReplyChgService lmtGrpReplyChgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpReplyChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpReplyChg> list = lmtGrpReplyChgService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpReplyChg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpReplyChg>> index(QueryModel queryModel) {
        List<LmtGrpReplyChg> list = lmtGrpReplyChgService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpReplyChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpReplyChg> show(@PathVariable("pkId") String pkId) {
        LmtGrpReplyChg lmtGrpReplyChg = lmtGrpReplyChgService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpReplyChg>(lmtGrpReplyChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpReplyChg> create(@RequestBody LmtGrpReplyChg lmtGrpReplyChg) throws URISyntaxException {
        lmtGrpReplyChgService.insert(lmtGrpReplyChg);
        return new ResultDto<LmtGrpReplyChg>(lmtGrpReplyChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpReplyChg lmtGrpReplyChg) throws URISyntaxException {
        int result = lmtGrpReplyChgService.update(lmtGrpReplyChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpReplyChgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpReplyChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:查询所有
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<LmtGrpReplyChg>> queryAll(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyChg> list = lmtGrpReplyChgService.queryAll(queryModel);
        return new ResultDto<List<LmtGrpReplyChg>>(list);
    }

    /**
     * @函数名称:queryAll
     * @函数描述:查询变更历史
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/queryHis")
    protected ResultDto<List<LmtGrpReplyChg>> queryHis(@RequestBody QueryModel queryModel) {
        List<LmtGrpReplyChg> list = lmtGrpReplyChgService.queryHis(queryModel);
        return new ResultDto<List<LmtGrpReplyChg>>(list);
    }

    /**
     * @函数名称:update
     * @函数描述: 集团批复变更申请列表逻辑删除
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtGrpReplyChg lmtGrpReplyChg) throws URISyntaxException {
        int result = lmtGrpReplyChgService.updateSelective(lmtGrpReplyChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateLigicDelete
     * @函数描述: 集团批复变更申请列表逻辑删除
     * @参数与返回说明:
     * @创建人：ywl
     * @算法描述:
     */
    @PostMapping("/updateligicdelete")
    protected ResultDto<Integer> updateLigicDelete(@RequestBody LmtGrpReplyChg lmtGrpReplyChg) throws URISyntaxException {
        int result = lmtGrpReplyChgService.updateLigicDelete(lmtGrpReplyChg);

        return new ResultDto<Integer>(result);
    }


    /**
     * @方法名称：insertReplyChg
     * @方法描述：批复变更时，将批复表落到批复变更表里
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-05-04 上午 9:53
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/insertlmtgrpreplychg")
    protected ResultDto<LmtGrpReplyChg> insertLmtGrpReplyChg(@RequestBody LmtGrpReplyChg lmtGrpReplyChg) {
        return ResultDto.success(lmtGrpReplyChgService.insertLmtGrpReplyChg(lmtGrpReplyChg));
    }

    /**
     * @方法名称：insertReplyChg
     * @方法描述：查询数据
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-05-04 上午 9:53
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/querybygrpserno")
    public ResultDto<Object> queryByGrpserno(@RequestBody String grpReplySerno) throws Exception {
        HashMap<String, String> reqMap = new HashMap<>();
        reqMap.put("grpReplySerno", grpReplySerno);
        LmtGrpReplyChg lmtGrpReplyChg = lmtGrpReplyChgService.queryByGrpserno(reqMap);
        return new ResultDto<>(lmtGrpReplyChg);
    }

    /**
     * @方法名称：queryLmtGrpReplyChgDataByGrpSerno
     * @方法描述：根据流水号查询批复变更
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-05-04 上午 9:53
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/queryLmtgrpreplychgdatabygrpserno")
    public ResultDto<LmtGrpReplyChg> queryLmtGrpReplyChgDataByGrpSerno(@RequestBody String grpSerno) throws Exception {
        HashMap<String, String> reqMap = new HashMap<>();
        reqMap.put("grpSerno", grpSerno);
        LmtGrpReplyChg lmtGrpReplyChg = lmtGrpReplyChgService.queryLmtGrpReplyChgDataByGrpSerno(reqMap);
        return ResultDto.success(lmtGrpReplyChg);
    }
}
