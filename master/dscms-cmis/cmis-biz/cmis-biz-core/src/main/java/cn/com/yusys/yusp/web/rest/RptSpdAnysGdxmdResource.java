/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtRecheckDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysGdxmd;
import cn.com.yusys.yusp.service.RptSpdAnysGdxmdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysGdxmdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-19 10:35:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysgdxmd")
public class RptSpdAnysGdxmdResource {
    @Autowired
    private RptSpdAnysGdxmdService rptSpdAnysGdxmdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysGdxmd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysGdxmd> list = rptSpdAnysGdxmdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysGdxmd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysGdxmd>> index(QueryModel queryModel) {
        List<RptSpdAnysGdxmd> list = rptSpdAnysGdxmdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysGdxmd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysGdxmd> show(@PathVariable("serno") String serno) {
        RptSpdAnysGdxmd rptSpdAnysGdxmd = rptSpdAnysGdxmdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysGdxmd>(rptSpdAnysGdxmd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysGdxmd> create(@RequestBody RptSpdAnysGdxmd rptSpdAnysGdxmd) throws URISyntaxException {
        rptSpdAnysGdxmdService.insert(rptSpdAnysGdxmd);
        return new ResultDto<RptSpdAnysGdxmd>(rptSpdAnysGdxmd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysGdxmd rptSpdAnysGdxmd) throws URISyntaxException {
        int result = rptSpdAnysGdxmdService.update(rptSpdAnysGdxmd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysGdxmdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysGdxmdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据申请流水号查询数据
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysGdxmd> selectBySerno(@RequestBody String serno) {
        RptSpdAnysGdxmd rptSpdAnysGdxmd = rptSpdAnysGdxmdService.selectByPrimaryKey(serno);
        return  ResultDto.success(rptSpdAnysGdxmd);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<RptSpdAnysGdxmd> save(@RequestBody RptSpdAnysGdxmd rptSpdAnysGdxmd) throws URISyntaxException {
        RptSpdAnysGdxmd rptSpdAnysGdxmd1 = rptSpdAnysGdxmdService.selectByPrimaryKey(rptSpdAnysGdxmd.getSerno());
        if(rptSpdAnysGdxmd1!=null){
            rptSpdAnysGdxmdService.update(rptSpdAnysGdxmd);
        }else{
            rptSpdAnysGdxmdService.insert(rptSpdAnysGdxmd);
        }
        return ResultDto.success(rptSpdAnysGdxmd);
    }
}
