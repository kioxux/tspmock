/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtLadEval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.MajorGradeInfo;
import cn.com.yusys.yusp.service.MajorGradeInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-30 10:43:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/majorgradeinfo")
public class MajorGradeInfoResource {
    @Autowired
    private MajorGradeInfoService majorGradeInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<MajorGradeInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<MajorGradeInfo> list = majorGradeInfoService.selectAll(queryModel);
        return new ResultDto<List<MajorGradeInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<MajorGradeInfo>> index(QueryModel queryModel) {
        List<MajorGradeInfo> list = majorGradeInfoService.selectByModel(queryModel);
        return new ResultDto<List<MajorGradeInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<MajorGradeInfo> show(@PathVariable("pkId") String pkId) {
        MajorGradeInfo majorGradeInfo = majorGradeInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<MajorGradeInfo>(majorGradeInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<MajorGradeInfo> create(@RequestBody MajorGradeInfo majorGradeInfo) throws URISyntaxException {
        majorGradeInfoService.insert(majorGradeInfo);
        return new ResultDto<MajorGradeInfo>(majorGradeInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody MajorGradeInfo majorGradeInfo) throws URISyntaxException {
        int result = majorGradeInfoService.update(majorGradeInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = majorGradeInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = majorGradeInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: selectByLmtSerno
     * @方法描述: 根据多授信申请流水号查询专业贷款评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectbylmtserno")
    protected ResultDto<List<MajorGradeInfo>> selectByLmtSerno(@RequestBody QueryModel queryModel){
        String lmtSerno = (String)queryModel.getCondition().get("lmtSerno");
        List<MajorGradeInfo> majorGradeInfoList = majorGradeInfoService.selectBySerno(lmtSerno);
        return new ResultDto<>(majorGradeInfoList);
    }
}
