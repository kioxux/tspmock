package cn.com.yusys.yusp.web.server.xdxw0042;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0042.req.Xdxw0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0042.resp.Xdxw0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0042.Xdxw0042Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:惠享贷模型结果推送给信贷
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0042:惠享贷模型结果推送给信贷")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0042Resource.class);

    @Autowired
    private Xdxw0042Service xdxw0042Service;

    /**
     * 交易码：xdxw0042
     * 交易描述：惠享贷模型结果推送给信贷
     *
     * @param xdxw0042DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("惠享贷模型结果推送给信贷")
    @PostMapping("/xdxw0042")
    protected @ResponseBody
    ResultDto<Xdxw0042DataRespDto> xdxw0042(@Validated @RequestBody Xdxw0042DataReqDto xdxw0042DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042DataReqDto));
        Xdxw0042DataRespDto xdxw0042DataRespDto = new Xdxw0042DataRespDto();// 响应Dto:惠享贷模型结果推送给信贷
        ResultDto<Xdxw0042DataRespDto> xdxw0042DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0042DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0042DataReqDto));
            xdxw0042DataRespDto = xdxw0042Service.xdxw0042(xdxw0042DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0042DataRespDto));
            // 封装xdxw0042DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0042DataRespDto.getOpFlag();
            String opMessage = xdxw0042DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0042DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0042DataResultDto.setMessage(opMessage);
            } else {
                xdxw0042DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0042DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0042DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0042DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0042DataResultDto.setCode(e.getErrorCode());
            xdxw0042DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0042DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0042DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0042DataResultDto中异常返回码和返回信息
            xdxw0042DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0042DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0042DataRespDto到xdxw0042DataResultDto中
        xdxw0042DataResultDto.setData(xdxw0042DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042DataResultDto));
        return xdxw0042DataResultDto;
    }
}
