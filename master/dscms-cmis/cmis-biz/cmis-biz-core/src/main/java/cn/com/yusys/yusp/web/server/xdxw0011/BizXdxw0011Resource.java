package cn.com.yusys.yusp.web.server.xdxw0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0011.req.Xdxw0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0011.resp.Xdxw0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0011.Xdxw0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:提交勘验信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0011:提交勘验信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0011Resource.class);

    @Autowired
    private Xdxw0011Service xdxw0011Service;

    /**
     * 交易码：xdxw0011
     * 交易描述：提交勘验信息
     *
     * @param xdxw0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提交勘验信息")
    @PostMapping("/xdxw0011")
    protected @ResponseBody
    ResultDto<Xdxw0011DataRespDto> xdxw0011(@Validated @RequestBody Xdxw0011DataReqDto xdxw0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataReqDto));
        Xdxw0011DataRespDto xdxw0011DataRespDto = new Xdxw0011DataRespDto();// 响应Dto:提交勘验信息
        ResultDto<Xdxw0011DataRespDto> xdxw0011DataResultDto = new ResultDto<>();
        String serno = xdxw0011DataReqDto.getSerno();//业务编号
        String status = xdxw0011DataReqDto.getStatus();//状态
        String videoNo = xdxw0011DataReqDto.getVideoNo();//视频编号
        if (!CmisBizConstants.XDXW0011_STATUS.contains(status)) {
            xdxw0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0011DataResultDto.setMessage(CmisBizConstants.XDXW0011_STATUS_ERROR_MESSAGE);
            logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataResultDto));
            return xdxw0011DataResultDto;
        }
        try {
            // 从xdxw0011DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0011Service层开始
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataReqDto));
            int result = xdxw0011Service.updateLmtInspectInfo(xdxw0011DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, result);
            if (result >= CmisBizConstants.INT_ONE) {
                xdxw0011DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 操作成功标志位
                xdxw0011DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
            } else {
                xdxw0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 操作成功标志位
                xdxw0011DataRespDto.setOpMsg(CmisBizConstants.XDXW0011_FAIL_MESSAGE);// 描述信息
            }
            // 封装xdxw0011DataResultDto中正确的返回码和返回信息
            xdxw0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, e.getMessage());
            // 封装xdxw0011DataResultDto中异常返回码和返回信息
            xdxw0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0011DataRespDto到xdxw0011DataResultDto中
        xdxw0011DataResultDto.setData(xdxw0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataResultDto));
        return xdxw0011DataResultDto;
    }
}
