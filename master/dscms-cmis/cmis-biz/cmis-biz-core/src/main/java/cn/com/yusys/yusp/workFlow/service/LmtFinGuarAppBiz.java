package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtFinGuarApp;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtFinGuarAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 担保公司授信申请流程处理
 * @author liqichao
 * @date 2021-02-02
 * */
@Service
public class LmtFinGuarAppBiz implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(LmtFinGuarAppBiz.class);

    @Autowired
    private AmqpTemplate amqpTemplate;
    
    @Autowired
    private LmtFinGuarAppService lmtFinGuarAppService;


    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        log.info("担保公司授信申请:"+serno+"流程操作:"+currentOpType+"业务处理");
        try{
            LmtFinGuarApp lmtFinGuarApp = lmtFinGuarAppService.selectByPrimaryKey(serno);//根据流水号获取到申请记录数据
            log.info("担保公司授信申请:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
            if(OpType.STRAT.equals(currentOpType)){//流程发起 --- 不做处理
            }else if(OpType.RUN.equals(currentOpType)){//流程流转
                log.info("担保公司授信申请:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.JUMP.equals(currentOpType)){//流程跳转
                log.info("担保公司授信申请:"+serno+"流程跳转,参数:"+instanceInfo.toString());
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("担保公司授信申请:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("担保公司授信申请:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("担保公司授信申请:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("担保公司授信申请:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("担保公司授信申请:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_997);
                //审批通过业务处理
                lmtFinGuarAppService.lmtFinGuarAppAgree(lmtFinGuarApp);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("担保公司授信申请:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(lmtFinGuarApp, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("担保公司授信申请:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("担保公司授信申请:"+serno+"流程操作:"+currentOpType+"业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * 流程审批状态更新
     * lmtCoopApp 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (LmtFinGuarApp lmtFinGuarApp, String approveStatus){
        lmtFinGuarApp.setApproveStatus(approveStatus);
        lmtFinGuarAppService.updateSelective(lmtFinGuarApp);
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.BIZ_TYPE_LMT_FIN_GUAR_APP.equals(flowCode);
    }
}
