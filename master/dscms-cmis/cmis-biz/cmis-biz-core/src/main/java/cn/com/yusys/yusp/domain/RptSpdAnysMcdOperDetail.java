/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcdOperDetail
 * @类描述: rpt_spd_anys_mcd_oper_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-29 15:59:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_mcd_oper_detail")
public class RptSpdAnysMcdOperDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 项目名称 **/
	@Column(name = "SUBJECT_NAME", unique = false, nullable = true, length = 80)
	private String subjectName;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 最近第二年金额 **/
	@Column(name = "LAST_SECOND_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastSecondYear;
	
	/** 最近第一年金额 **/
	@Column(name = "LAST_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYear;
	
	/** 当年金额 **/
	@Column(name = "CURR_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currYear;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param subjectName
	 */
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
    /**
     * @return subjectName
     */
	public String getSubjectName() {
		return this.subjectName;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param lastSecondYear
	 */
	public void setLastSecondYear(java.math.BigDecimal lastSecondYear) {
		this.lastSecondYear = lastSecondYear;
	}
	
    /**
     * @return lastSecondYear
     */
	public java.math.BigDecimal getLastSecondYear() {
		return this.lastSecondYear;
	}
	
	/**
	 * @param lastYear
	 */
	public void setLastYear(java.math.BigDecimal lastYear) {
		this.lastYear = lastYear;
	}
	
    /**
     * @return lastYear
     */
	public java.math.BigDecimal getLastYear() {
		return this.lastYear;
	}
	
	/**
	 * @param currYear
	 */
	public void setCurrYear(java.math.BigDecimal currYear) {
		this.currYear = currYear;
	}
	
    /**
     * @return currYear
     */
	public java.math.BigDecimal getCurrYear() {
		return this.currYear;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}