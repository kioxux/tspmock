/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarBaseInfo
 * @类描述: guar_base_info数据实体类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-12-01 21:10:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarEvalApplyInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	private String guarNo;
	private String guarCusName;
	private String guarTypeCdCnname;
	private String certiRecordId;
	private String certiCatalog;
	private String certiTypeCd;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getGuarCusName() {
		return guarCusName;
	}

	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}

	public String getGuarTypeCdCnname() {
		return guarTypeCdCnname;
	}

	public void setGuarTypeCdCnname(String guarTypeCdCnname) {
		this.guarTypeCdCnname = guarTypeCdCnname;
	}

	public String getCertiRecordId() {
		return certiRecordId;
	}

	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId;
	}

	public String getCertiCatalog() {
		return certiCatalog;
	}

	public void setCertiCatalog(String certiCatalog) {
		this.certiCatalog = certiCatalog;
	}

	public String getCertiTypeCd() {
		return certiTypeCd;
	}

	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd;
	}
}