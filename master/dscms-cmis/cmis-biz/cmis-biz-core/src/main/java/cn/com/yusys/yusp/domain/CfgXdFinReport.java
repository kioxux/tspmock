/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgXdFinReport
 * @类描述: cfg_xd_fin_report数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cfg_xd_fin_report")
public class CfgXdFinReport extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 代码编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CODE_NO")
	private String codeNo;
	
	/** 中文值 **/
	@Column(name = "CODE_TEXT", unique = false, nullable = true, length = 100)
	private String codeText;
	
	/** 代码值 **/
	@Column(name = "CODE_VALUE", unique = false, nullable = true, length = 40)
	private String codeValue;
	
	/** 业务类型子类 **/
	@Column(name = "TYPE_CHILD", unique = false, nullable = true, length = 5)
	private String typeChild;
	
	/** 序号 **/
	@Column(name = "ORDER_ID", unique = false, nullable = true, length = 10)
	private String orderId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param codeNo
	 */
	public void setCodeNo(String codeNo) {
		this.codeNo = codeNo;
	}
	
    /**
     * @return codeNo
     */
	public String getCodeNo() {
		return this.codeNo;
	}
	
	/**
	 * @param codeText
	 */
	public void setCodeText(String codeText) {
		this.codeText = codeText;
	}
	
    /**
     * @return codeText
     */
	public String getCodeText() {
		return this.codeText;
	}
	
	/**
	 * @param codeValue
	 */
	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}
	
    /**
     * @return codeValue
     */
	public String getCodeValue() {
		return this.codeValue;
	}
	
	/**
	 * @param typeChild
	 */
	public void setTypeChild(String typeChild) {
		this.typeChild = typeChild;
	}
	
    /**
     * @return typeChild
     */
	public String getTypeChild() {
		return this.typeChild;
	}
	
	/**
	 * @param orderId
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
    /**
     * @return orderId
     */
	public String getOrderId() {
		return this.orderId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}