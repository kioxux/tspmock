package cn.com.yusys.yusp.service.server.xdsx0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgSxkdGuarDiscountDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.req.ListMx;
import cn.com.yusys.yusp.dto.server.xdsx0024.req.Xdsx0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.resp.Xdsx0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:省心快贷plus授信，风控自动审批结果推送信贷
 *
 * @author admin
 * @version 1.0
 */
@Service
public class Xdsx0024Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0024Service.class);
    @Autowired
    private LmtSxkdPlusFkspMapper lmtSxkdPlusFkspMapper;
    @Autowired
    private LmtSxkdPlusFkspDetailsMapper lmtSxkdPlusFkspDetailsMapper;
    @Autowired
    private LmtAppMapper lmtAppMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private SenddxService senddxService;
    @Autowired
    private LmtApprSubPrdMapper lmtApprSubPrdMapper;
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private LmtAppSubMapper lmtAppSubMapper;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private LmtAppSubMapper lmtAppsubMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private LmtApprService lmtApprService;
    @Autowired
    private LmtApprSubMapper lmtApprSubMapper;
    @Autowired
    private LmtApprMapper lmtApprMapper;
    @Autowired
    private LmtApprLoanCondMapper lmtApprLoanCondMapper;
    @Autowired
    private MessageCommonService messageCommonService;
    /**
     * 省心快贷plus授信，风控自动审批结果推送信贷
     *
     * @param xdsx0024DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0024DataRespDto xdsx0024(Xdsx0024DataReqDto xdsx0024DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024DataReqDto));
        //返回对象
        Xdsx0024DataRespDto xdsx0024DataRespDto = new Xdsx0024DataRespDto();
        try {
            if("".equals(xdsx0024DataReqDto.getLmt_serno())){
                xdsx0024DataRespDto.setOpFlag("F");
                xdsx0024DataRespDto.setOpMsg("授信流水号不能为空");
                return xdsx0024DataRespDto;
            }
            if("".equals(xdsx0024DataReqDto.getSp_result_code())){
                xdsx0024DataRespDto.setOpFlag("F");
                xdsx0024DataRespDto.setOpMsg("自动审批结果代码不能为空");
                return xdsx0024DataRespDto;
            }
            String lmtSerno = xdsx0024DataReqDto.getLmt_serno();
            //删除原省心快贷plus授信风控自动审批信息
            //1删除明细表
            lmtSxkdPlusFkspDetailsMapper.deleteByLmtSerno(lmtSerno);
            //2删除主表
            lmtSxkdPlusFkspMapper.deleteByLmtSerno(lmtSerno);

            //风控的审批结果(pass:通过, reject:否决)
            String spResultCode = xdsx0024DataReqDto.getSp_result_code();
            String spResultName = "pass".equals(spResultCode) ? "审批通过" : "否决";

            //插入主表
            Map dataMap = new HashMap();
            String pkValue = UUID.randomUUID().toString();
            dataMap.put("pkId",pkValue);
            dataMap.put("serno",lmtSerno);
            dataMap.put("approveResult",xdsx0024DataReqDto.getSp_result_code());
            dataMap.put("approveResultName",spResultName);
            lmtSxkdPlusFkspMapper.insertByMap(dataMap);
            //1插入明细表
            List<ListMx> listMx = xdsx0024DataReqDto.getList();
            Map dataMapDetail = new HashMap();
            for(int i=0;i<listMx.size();i++){
                dataMapDetail.clear();
                dataMapDetail.put("pkId",UUID.randomUUID().toString());
                dataMapDetail.put("approveResultPkId",pkValue);
                dataMapDetail.put("ruleId",listMx.get(i).getRule_code());
                dataMapDetail.put("ruleName",listMx.get(i).getRule_name());
                dataMapDetail.put("ruleExecuteCode",listMx.get(i).getExecute_result_code());
                dataMapDetail.put("ruleExecuteName","pass".equals(listMx.get(i).getExecute_result_code()) ? "通过" : "否决");
                dataMapDetail.put("ruleExecuteDesc",listMx.get(i).getExecute_result_remark());
                lmtSxkdPlusFkspDetailsMapper.insertByMap(dataMapDetail);
            }
            //查询授信申请信息
            LmtApp lmtApp = new LmtApp();
            QueryModel model = new QueryModel();
            model.addCondition("serno",lmtSerno);
            List<LmtApp> listLmt= lmtAppMapper.selectByModel(model);
            if(listLmt!=null && listLmt.size()>0){
                lmtApp = listLmt.get(0);
            } else {
                throw new BizException(null, "", null, "XDSX0024:信贷系统未查询到授信申请信息！");
            }
            //风控通过
            if("pass".equals(spResultCode)){
                //1处理授信审批额度信息 批复额度 = MIN（∑[抵押物我行认定价值*折率] , 自制报表年销售收入/2，500万，上报的授信分项的申请额度）
                processLmtAmtByFkPass(lmtApp,xdsx0024DataReqDto);
                //2发送短信
                String managerId = lmtApp.getManagerId();
                String mgrTel = "";
                String managerName = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                        managerName = adminSmUserDto.getUserName();
                    }
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_XW_M_0017";//短信编号
                    Map paramMap = new HashMap();//短信填充参数
                    paramMap.put("managerName", managerName);
                    paramMap.put("cusId", lmtApp.getCusId());
                    paramMap.put("cusName", lmtApp.getCusName());
                    //执行发送客户经理操作
                    messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    logger.info("XDSX0024：发送短信通知结束：");
                }
            }
            xdsx0024DataRespDto.setOpFlag("S");
            xdsx0024DataRespDto.setOpMsg("交易成功");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, e.getMessage());
            xdsx0024DataRespDto.setOpFlag("F");
            xdsx0024DataRespDto.setOpMsg(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, e.getMessage());
            xdsx0024DataRespDto.setOpFlag("F");
            xdsx0024DataRespDto.setOpMsg(e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value);
        return xdsx0024DataRespDto;
    }
    //风控模型通过，处理授信额度信息 批复额度 = MIN（∑[抵押物我行认定价值*折率] , 自制报表年销售收入/2，500万，上报的授信分项的申请额度）
    private void processLmtAmtByFkPass(LmtApp lmtApp,Xdsx0024DataReqDto xdsx0024DataReqDto) {
        logger.info("XDSX0024:风控自动审批结果返回成功,更新授信审批额度开始!" + lmtApp.getSerno());
        //销售收入一半
        BigDecimal halfSaleIncome = new BigDecimal(0);
        //申请金额
        BigDecimal sxkdPlusAppAmt = new BigDecimal(0);
        //押品折扣金额
        BigDecimal discountAmt = new BigDecimal(0);
        //押品折扣金额总和
        BigDecimal sumDiscountAmt = new BigDecimal(0);

        // 获取上报的授信分项的申请额度
        String subSerno = "";
        // 获取省心快贷授信分项信息
        logger.info("XDSX0024:获取省心快贷授信分项信息开始,授信流水{}!" + lmtApp.getSerno());
        // 查询条件过滤后应该只有一条符合条件的省心快贷授信分项，不应该会存在两笔授信金额大于0的省心快贷，除非数据有问题。
        List<LmtAppSub> subList = lmtAppsubMapper.selectSxkdLmtAppSubBySubSerno(lmtApp.getSerno());
        LmtAppSub lmtappsub = subList.get(0);
        if(Objects.nonNull(lmtappsub)){
            sxkdPlusAppAmt = lmtappsub.getLmtAmt();
            logger.info("XDSX0024:上报的授信分项的申请额度:" + sxkdPlusAppAmt);
            subSerno = lmtappsub.getSubSerno();
            logger.info("XDSX0024:获取省心快贷授信分项信息结束,授信分项流水{}!" + subSerno);
        } else {
            logger.info("XDSX0024:获取省心快贷授信分项信息异常!");
            throw new BizException(null, "", null, "获取省心快贷授信分项信息异常");
        }
        // 根据分项流水获取押品信息
        logger.info("XDSX0024:根据分项流水获取押品信息表!" + subSerno);
        List<GuarBaseInfo> list = guarBaseInfoMapper.selectGuarBaseInfoBySubSerno(subSerno);
        if(list != null && list.size() >0){
            for (GuarBaseInfo guarBaseInfo : list) {
                //我行认定价值
                BigDecimal MaxMortagageAmt = new BigDecimal(0);
                //抵押物折率
                BigDecimal discountRate = new BigDecimal(0);
                //获取我行认定金额
                MaxMortagageAmt = guarBaseInfo.getMaxMortagageAmt();
                // 引入抵押物类型在准入范围内
                String guarTypeCd = guarBaseInfo.getGuarTypeCd();
                // 根据抵押物担保分类代码判断抵押物类型是否在准入范围内
                ResultDto<CfgSxkdGuarDiscountDto> cfgSxkdGuarDiscountDto = iCmisCfgClientService.selectCfgSxkdGuarDiscountByGuarTypeCd(guarTypeCd);
                // 若查询结果为空，则该抵押物类型不在准入范围内
                if (Objects.nonNull(cfgSxkdGuarDiscountDto) || Objects.nonNull(cfgSxkdGuarDiscountDto.getData())) {
                    //获取折率
                    discountRate = cfgSxkdGuarDiscountDto.getData().getDiscountRate();
                    //抵押物我行认定价值*折率
                    discountAmt = MaxMortagageAmt.multiply(discountRate);
                }
                //押品折扣金额总和 抵押物我行认定价值*折率
                sumDiscountAmt = sumDiscountAmt.add(discountAmt);
                logger.info("XDSX0024:抵押物我行认定价值*折率计算结果!" + sumDiscountAmt);
            }
        }
        logger.info("XDSX0024:根据分项流水获取押品信息表结束!" + subSerno);

        // 获取自制报表年销售收入，如果说满1年的取上一年度年报中的年销售收入；不满1年的，取当期最新一期的数据
        BigDecimal SaleIncome = new BigDecimal(0);
        //查询对公客户基本信息
        ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(lmtApp.getCusId());
        if (Objects.nonNull(cusCorpDtoResultDto) || Objects.nonNull(cusCorpDtoResultDto.getData())) {
            // 获取公司成立日期
            CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
            // 判断公司成立日期是否为空
            if (StringUtils.nonBlank(cusCorpDto.getBuildDate())) {
                Date buildDate = DateUtils.parseDate(cusCorpDto.getBuildDate(), "yyyy-MM-dd");
                String cusId = lmtApp.getCusId();
                // 获取自制财报信息
                ResultDto<Map<String, FinanIndicAnalyDto>> finanIndicAnalyDto =  iCusClientService.getFinRepRetProAndRatOfLia(cusId);
                Map<String, FinanIndicAnalyDto> resultMap = finanIndicAnalyDto.getData();
                logger.info("XDSX0024:当前客户{【"+cusId+"】}获取的财报信息{【"+ JSON.toJSONString(finanIndicAnalyDto)+"】}");
                // 若成未满1年的，取近一期的报表数据的值判断；
                if (DateUtils.compare(DateUtils.addMonth(buildDate, 12), DateUtils.getCurrDate()) >= 0) {
                    SaleIncome = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("salesIncome")).get().getCurYmValue());
                } else if (DateUtils.compare(DateUtils.addMonth(buildDate, 12), DateUtils.getCurrDate()) < 0) {
                    SaleIncome = NumberUtils.nullDefaultZero(Optional.ofNullable(resultMap.get("lastYearSalesIncome")).get().getCurYmValue());
                }
                // 自制报表年销售收入/2
                halfSaleIncome = SaleIncome.divide(new BigDecimal(2));
                logger.info("XDSX0024:自制报表年销售收入/2计算结果【{}】",halfSaleIncome);
            }
        }
        //计算批复金额(从500W、销售收入的一半、申请金额、押品按折率计算后的金额，四个值中取最小的)
        BigDecimal pifuAmt = min(new BigDecimal(5000000), halfSaleIncome, sxkdPlusAppAmt, discountAmt).setScale(2,BigDecimal.ROUND_DOWN);

        //最终的批复金额，取整到万(不超过1W时，不再取整)
        String finalPifuAmt = pifuAmt.compareTo(new BigDecimal("10000")) > 0 ? (pifuAmt+"").substring(0, (pifuAmt+"").length() - 7) + "0000.00" : (pifuAmt+"");
        logger.info("XDSX0024:最终的批复金额计算结束!" + pifuAmt);

        // 1.通过授信申请流水号查询最新的一笔授信审批
        LmtAppr lmtAppr = lmtApprService.queryFinalLmtApprBySerno(lmtApp.getSerno());
        logger.info("XDSX0024:根据授信流水号获取授信审批信息" + lmtApp.getSerno());
        if(Objects.nonNull(lmtAppr)){
            logger.info("XDSX0024:更新最新一笔授信审批表授信金额开始");
            //更新授信审批分项明细表金额 一笔省心快贷分项下只能有一笔产品分项明细
            LmtApprSubPrd lmtApprSubPrd = new LmtApprSubPrd();
            lmtApprSubPrd.setSubSerno(subSerno);
            lmtApprSubPrd.setLmtAmt(new BigDecimal(finalPifuAmt));
            lmtApprSubPrdMapper.updateSxkdPrdAmtBySubSerno(lmtApprSubPrd);

            //更新授信申请分项适用品种明细表
            LmtApprSub lmtApprSub = new LmtApprSub();
            lmtApprSub.setSubSerno(subSerno);
            lmtApprSub.setLmtAmt(new BigDecimal(finalPifuAmt));
            lmtApprSubMapper.updateSxkdAmtBySubSerno(lmtApprSub);

            //更新授信申请总额
            lmtAppr.setOpenTotalLmtAmt(new BigDecimal(finalPifuAmt));
            lmtAppr.setSerno(lmtApp.getSerno());
            lmtApprMapper.updateSxkdAmtBySerno(lmtAppr);
            logger.info("XDSX0024:更新最新一笔授信申请表授信金额结束");

            //2更新授信审批用信条件表用信条件 批复模板

            // 拼装用信条件
            String condDesc = "(1).追加法人代表以及法人代表配偶（如有）个人担保，追加公司占股股东担保70%以上（含）；省心快贷授信批复额度为" + finalPifuAmt + "元，循环授信"+ lmtApp.getLmtTerm() + "个月，无宽限期，单笔用信不超一年，" +
                    "业务不支持第三方抵押（可准入所有权人范围包括，1、法定代表人及其配偶、直系亲属【成年子女、父母、岳父母、儿媳、女婿】；2、实际控制人及其配偶、直系亲属【成年子女、父母、岳父母、儿媳、女婿】；" +
                    "3、股份占比20%以上（含）的股东；4、企业名下所有）。(2).用信前需结清法人代表、法人代表配偶、出资人在我行的经营性和流动资金贷款业务。";
            LmtApprLoanCond lmtApprLoanCond = new LmtApprLoanCond();
            Date date = new Date();
            String approveSerno = lmtAppr.getApproveSerno();
            lmtApprLoanCond.setPkId(UUID.randomUUID().toString());
            lmtApprLoanCond.setCreateTime(date);
            lmtApprLoanCond.setCondDesc(condDesc);
            lmtApprLoanCond.setApproveSerno(approveSerno);
            lmtApprLoanCond.setCondType("01"); //STD_COND_TYPE 01用信条件
            lmtApprLoanCondMapper.insert(lmtApprLoanCond);
        } else {
            logger.info("XDSX0024:获取省心快贷授信审批信息异常!");
            throw new BizException(null, "", null, "获取省心快贷授信审批信息异常");
        }

    }

    private BigDecimal min(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d) {
        BigDecimal min1;
        BigDecimal min2;
        if(a.compareTo(b)>0){
            min1=b;
        }else {
            min1=a;
        }
        if(c.compareTo(d)>0){
            min2=d;
        }else {
            min2=c;
        }
        if(min1.compareTo(min2)>0){
            return min2;
        }else {
            return min1;
        }
    }

}
