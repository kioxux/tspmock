/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.DocAccList;
import cn.com.yusys.yusp.dto.DocAccSearchDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAccListMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 17:06:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocAccListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    DocAccList selectByPrimaryKey(@Param("docSerno") String docSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<DocAccList> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(DocAccList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(DocAccList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(DocAccList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(DocAccList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("docSerno") String docSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * 台账状态：更新为已销毁
     *
     * @param list
     * @param status
     * @return
     */
    int updatedocAccStatus(@Param("list") List<String> list, @Param("status") String status);

    /**
     * 获取各类型档案目录年份数据
     *
     * @author jijian_yx
     * @date 2021/7/9 20:07
     **/
    List<String> getAccTimeList(@Param("type") String type,@Param("cusId")String cusId);

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     *
     * @author jijian_yx
     * @date 2021/7/10 14:17
     **/
    List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel);
}