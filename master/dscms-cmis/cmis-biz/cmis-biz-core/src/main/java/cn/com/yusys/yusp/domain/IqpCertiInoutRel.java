/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCertiInoutRel
 * @类描述: iqp_certi_inout_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 10:21:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_certi_inout_rel")
public class IqpCertiInoutRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 押品出入库申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 担保合同流水号  **/
	@Column(name = "GUAR_PK_ID", unique = false, nullable = true, length = 40)
	private String guarPkId;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = false, length = 40)
	private String guarContNo;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 30)
	private String guarNo;
	
	/** 权证主键 **/
	@Column(name = "CERTI_PK_ID", unique = false, nullable = false, length = 40)
	private String certiPkId;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId;
	}
	
    /**
     * @return guarPkId
     */
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param certiPkId
	 */
	public void setCertiPkId(String certiPkId) {
		this.certiPkId = certiPkId;
	}
	
    /**
     * @return certiPkId
     */
	public String getCertiPkId() {
		return this.certiPkId;
	}
	
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}
}