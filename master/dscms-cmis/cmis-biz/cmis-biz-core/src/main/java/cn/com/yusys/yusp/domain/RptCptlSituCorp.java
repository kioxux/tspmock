/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituCorp
 * @类描述: rpt_cptl_situ_corp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-09 09:22:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_cptl_situ_corp")
public class RptCptlSituCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 所属行 **/
	@Column(name = "BELONG_BANK", unique = false, nullable = true, length = 80)
	private String belongBank;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 信贷品种 **/
	@Column(name = "CREDIT_TYPE", unique = false, nullable = true, length = 80)
	private String creditType;
	
	/** 采集日期 **/
	@Column(name = "ACQUISITION_DATE", unique = false, nullable = true, length = 10)
	private String acquisitionDate;
	
	/** 最近二年末金额 **/
	@Column(name = "LAST_TWO_YEAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearAmt;
	
	/** 最近二年末担保方式 **/
	@Column(name = "LAST_TWO_YEAR_GUAR_MODE", unique = false, nullable = true, length = 5)
	private String lastTwoYearGuarMode;
	
	/** 最近一年末金额 **/
	@Column(name = "LAST_YEAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearAmt;
	
	/** 最近一年末担保方式 **/
	@Column(name = "LAST_YEAR_GUAR_MODE", unique = false, nullable = true, length = 5)
	private String lastYearGuarMode;
	
	/** 当前月末金额 **/
	@Column(name = "CUR_MONTH_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curMonthAmt;
	
	/** 当前月末担保方式 **/
	@Column(name = "CUR_MONTH_GUAR_MODE", unique = false, nullable = true, length = 5)
	private String curMonthGuarMode;
	
	/** 当前月末五级分类 **/
	@Column(name = "CUR_MONTH_FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String curMonthFiveClass;
	
	/** 当前月末备注 **/
	@Column(name = "CUR_MONTH_REMARK", unique = false, nullable = true, length = 65535)
	private String curMonthRemark;
	
	/** 逾期次数 **/
	@Column(name = "OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private Integer overdueTimes;
	
	/** 欠息次数 **/
	@Column(name = "DEBT_TIMES", unique = false, nullable = true, length = 10)
	private Integer debtTimes;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param belongBank
	 */
	public void setBelongBank(String belongBank) {
		this.belongBank = belongBank;
	}
	
    /**
     * @return belongBank
     */
	public String getBelongBank() {
		return this.belongBank;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param creditType
	 */
	public void setCreditType(String creditType) {
		this.creditType = creditType;
	}
	
    /**
     * @return creditType
     */
	public String getCreditType() {
		return this.creditType;
	}
	
	/**
	 * @param acquisitionDate
	 */
	public void setAcquisitionDate(String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}
	
    /**
     * @return acquisitionDate
     */
	public String getAcquisitionDate() {
		return this.acquisitionDate;
	}
	
	/**
	 * @param lastTwoYearAmt
	 */
	public void setLastTwoYearAmt(java.math.BigDecimal lastTwoYearAmt) {
		this.lastTwoYearAmt = lastTwoYearAmt;
	}
	
    /**
     * @return lastTwoYearAmt
     */
	public java.math.BigDecimal getLastTwoYearAmt() {
		return this.lastTwoYearAmt;
	}
	
	/**
	 * @param lastTwoYearGuarMode
	 */
	public void setLastTwoYearGuarMode(String lastTwoYearGuarMode) {
		this.lastTwoYearGuarMode = lastTwoYearGuarMode;
	}
	
    /**
     * @return lastTwoYearGuarMode
     */
	public String getLastTwoYearGuarMode() {
		return this.lastTwoYearGuarMode;
	}
	
	/**
	 * @param lastYearAmt
	 */
	public void setLastYearAmt(java.math.BigDecimal lastYearAmt) {
		this.lastYearAmt = lastYearAmt;
	}
	
    /**
     * @return lastYearAmt
     */
	public java.math.BigDecimal getLastYearAmt() {
		return this.lastYearAmt;
	}
	
	/**
	 * @param lastYearGuarMode
	 */
	public void setLastYearGuarMode(String lastYearGuarMode) {
		this.lastYearGuarMode = lastYearGuarMode;
	}
	
    /**
     * @return lastYearGuarMode
     */
	public String getLastYearGuarMode() {
		return this.lastYearGuarMode;
	}
	
	/**
	 * @param curMonthAmt
	 */
	public void setCurMonthAmt(java.math.BigDecimal curMonthAmt) {
		this.curMonthAmt = curMonthAmt;
	}
	
    /**
     * @return curMonthAmt
     */
	public java.math.BigDecimal getCurMonthAmt() {
		return this.curMonthAmt;
	}
	
	/**
	 * @param curMonthGuarMode
	 */
	public void setCurMonthGuarMode(String curMonthGuarMode) {
		this.curMonthGuarMode = curMonthGuarMode;
	}
	
    /**
     * @return curMonthGuarMode
     */
	public String getCurMonthGuarMode() {
		return this.curMonthGuarMode;
	}
	
	/**
	 * @param curMonthFiveClass
	 */
	public void setCurMonthFiveClass(String curMonthFiveClass) {
		this.curMonthFiveClass = curMonthFiveClass;
	}
	
    /**
     * @return curMonthFiveClass
     */
	public String getCurMonthFiveClass() {
		return this.curMonthFiveClass;
	}
	
	/**
	 * @param curMonthRemark
	 */
	public void setCurMonthRemark(String curMonthRemark) {
		this.curMonthRemark = curMonthRemark;
	}
	
    /**
     * @return curMonthRemark
     */
	public String getCurMonthRemark() {
		return this.curMonthRemark;
	}
	
	/**
	 * @param overdueTimes
	 */
	public void setOverdueTimes(Integer overdueTimes) {
		this.overdueTimes = overdueTimes;
	}
	
    /**
     * @return overdueTimes
     */
	public Integer getOverdueTimes() {
		return this.overdueTimes;
	}
	
	/**
	 * @param debtTimes
	 */
	public void setDebtTimes(Integer debtTimes) {
		this.debtTimes = debtTimes;
	}
	
    /**
     * @return debtTimes
     */
	public Integer getDebtTimes() {
		return this.debtTimes;
	}


}