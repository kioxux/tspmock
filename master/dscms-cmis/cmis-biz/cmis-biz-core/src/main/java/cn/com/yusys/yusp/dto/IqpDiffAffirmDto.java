package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiffAffirm
 * @类描述: iqp_diff_affirm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:17:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDiffAffirmDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 借款人户籍类型
STD_ZB_APPLYER_REGIST_KIND **/
	private String aplyerRegistKind;
	
	/** 目前家庭已有房屋套数(套) STD_ZB_HAS_HOUSE_NUM **/
	private String hasHouse;
	
	/** 家庭已有商贷记录  STD_ZB_HAS_LOAN_RECORD **/
	private String hasCLoan;
	
	/** 本笔贷款执行政策 STD_ZB_CURR_POLICY_TYPE **/
	private String currLoanPolicy;
	
	/** 所购房屋类型 STD_ZB_BUY_KIND_TYPE **/
	private String buyHouseKind;
	
	/** 本地房产情况 **/
	private String localHouse;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param aplyerRegistKind
	 */
	public void setAplyerRegistKind(String aplyerRegistKind) {
		this.aplyerRegistKind = aplyerRegistKind == null ? null : aplyerRegistKind.trim();
	}
	
    /**
     * @return AplyerRegistKind
     */	
	public String getAplyerRegistKind() {
		return this.aplyerRegistKind;
	}
	
	/**
	 * @param hasHouse
	 */
	public void setHasHouse(String hasHouse) {
		this.hasHouse = hasHouse == null ? null : hasHouse.trim();
	}
	
    /**
     * @return HasHouse
     */	
	public String getHasHouse() {
		return this.hasHouse;
	}
	
	/**
	 * @param hasCLoan
	 */
	public void setHasCLoan(String hasCLoan) {
		this.hasCLoan = hasCLoan == null ? null : hasCLoan.trim();
	}
	
    /**
     * @return HasCLoan
     */	
	public String getHasCLoan() {
		return this.hasCLoan;
	}
	
	/**
	 * @param currLoanPolicy
	 */
	public void setCurrLoanPolicy(String currLoanPolicy) {
		this.currLoanPolicy = currLoanPolicy == null ? null : currLoanPolicy.trim();
	}
	
    /**
     * @return CurrLoanPolicy
     */	
	public String getCurrLoanPolicy() {
		return this.currLoanPolicy;
	}
	
	/**
	 * @param buyHouseKind
	 */
	public void setBuyHouseKind(String buyHouseKind) {
		this.buyHouseKind = buyHouseKind == null ? null : buyHouseKind.trim();
	}
	
    /**
     * @return BuyHouseKind
     */	
	public String getBuyHouseKind() {
		return this.buyHouseKind;
	}
	
	/**
	 * @param localHouse
	 */
	public void setLocalHouse(String localHouse) {
		this.localHouse = localHouse == null ? null : localHouse.trim();
	}
	
    /**
     * @return LocalHouse
     */	
	public String getLocalHouse() {
		return this.localHouse;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}