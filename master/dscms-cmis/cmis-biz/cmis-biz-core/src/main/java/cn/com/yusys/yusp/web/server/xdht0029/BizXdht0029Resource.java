package cn.com.yusys.yusp.web.server.xdht0029;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0029.req.Xdht0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0029.resp.Xdht0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0029.Xdht0029Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户非小微业务经营性贷款总金额查询
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0029:客户非小微业务经营性贷款总金额查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0029Resource.class);

    @Autowired
    private Xdht0029Service xdht0029Service;
    
    /**
     * 交易码：xdht0029
     * 交易描述：客户非小微业务经营性贷款总金额查询
     *
     * @param xdht0029DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("客户非小微业务经营性贷款总金额查询")
    @PostMapping("/xdht0029")
    protected @ResponseBody
    ResultDto<Xdht0029DataRespDto>    xdht0029(@Validated @RequestBody Xdht0029DataReqDto xdht0029DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029DataReqDto));
        Xdht0029DataRespDto  xdht0029DataRespDto  = new Xdht0029DataRespDto();// 响应Dto:客户非小微业务经营性贷款总金额查询
        ResultDto<Xdht0029DataRespDto>xdht0029DataResultDto = new ResultDto<>();
        try {
            // 身份证号码
            String certNo = xdht0029DataReqDto.getCertNo();
            if (StringUtils.isEmpty(certNo)) {
                throw BizException.error(null, EcbEnum.ECB010048.key, EcbEnum.ECB010048.value);
            }
            xdht0029DataRespDto = xdht0029Service.xdht0029Execute(xdht0029DataReqDto);
            xdht0029DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0029DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, e.getMessage());
            // 封装xdht0029DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdht0029DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0029DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdht0029DataRespDto到xdht0029DataResultDto中
        xdht0029DataResultDto.setData(xdht0029DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029DataRespDto));
        return xdht0029DataResultDto;
    }
}