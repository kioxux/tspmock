package cn.com.yusys.yusp.web.server.xddb0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0011.req.Xddb0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0011.resp.Xddb0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0011.Xddb0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:提前出库押品信息维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0011:提前出库押品信息维护")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0011Resource.class);

    @Autowired
    private Xddb0011Service xddb0011Service;

    /**
     * 交易码：xddb0011
     * 交易描述：提前出库押品信息维护
     *
     * @param xddb0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前出库押品信息维护")
    @PostMapping("/xddb0011")
    protected @ResponseBody
    ResultDto<Xddb0011DataRespDto> xddb0011(@Validated @RequestBody Xddb0011DataReqDto xddb0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataReqDto));
        Xddb0011DataRespDto xddb0011DataRespDto = new Xddb0011DataRespDto();// 响应Dto:提前出库押品信息维护
        ResultDto<Xddb0011DataRespDto> xddb0011DataResultDto = new ResultDto<>();
        try {

            // 从xddb0011DataReqDto获取业务值进行业务逻辑处理
            // 调用xddb0011Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataReqDto));
            int result = xddb0011Service.saveGrarWarrantOutApps(xddb0011DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, result);
            if (result < CmisBizConstants.INT_ONE) {
                xddb0011DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 操作成功标志位
                xddb0011DataRespDto.setOpMsg(CmisBizConstants.NO_MESSAGE);// 描述信息
            } else {
                xddb0011DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 操作成功标志位
                xddb0011DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
            }
            // 封装xddb0011DataResultDto中正确的返回码和返回信息
            xddb0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, e.getMessage());
            // 封装xddb0011DataResultDto中异常返回码和返回信息
            xddb0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0011DataRespDto到xddb0011DataResultDto中
        xddb0011DataResultDto.setData(xddb0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataRespDto));
        return xddb0011DataResultDto;
    }
}
