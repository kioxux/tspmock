/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestAppr;
import cn.com.yusys.yusp.service.LmtSigInvestApprService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-17 15:28:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestappr")
public class LmtSigInvestApprResource {
    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestAppr> list = lmtSigInvestApprService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestAppr>> index(QueryModel queryModel) {
        List<LmtSigInvestAppr> list = lmtSigInvestApprService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestAppr> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestAppr>(lmtSigInvestAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestAppr> create(@RequestBody LmtSigInvestAppr lmtSigInvestAppr) throws URISyntaxException {
        lmtSigInvestApprService.insert(lmtSigInvestAppr);
        return new ResultDto<LmtSigInvestAppr>(lmtSigInvestAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestAppr lmtSigInvestAppr) throws URISyntaxException {
        int result = lmtSigInvestApprService.update(lmtSigInvestAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<Map<String, Object>> selectBySerno(@RequestBody QueryModel model){
        return  new ResultDto<Map<String, Object>>(lmtSigInvestApprService.selectBySerno(model));
    }

    @PostMapping("/updateAppr")
    protected ResultDto<Integer> updateAppr(@RequestBody LmtSigInvestAppr lmtSigInvestAppr){
        return new ResultDto<Integer>(lmtSigInvestApprService.updateSelective(lmtSigInvestAppr));
    }

    /**
     * 修改审批表信息
     * @param lmtSigInvestApprMap
     * @return
     */
    @PostMapping("/updateUpApprAuth")
    protected  ResultDto<Integer> updateUpApprAuth(@RequestBody Map lmtSigInvestApprMap){
        return new ResultDto<Integer>(lmtSigInvestApprService.updateUpApprAuth(lmtSigInvestApprMap));
    }

    /**
     * @函数名称:updateRestByPkId
     * @函数描述:根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkId")
    protected int updateRestByPkId(@RequestBody QueryModel model){
        return lmtSigInvestApprService.updateRestByPkId(model);
    }

    /**
     * @函数名称:updateRestByPkIdZH
     * @函数描述:根据Serno更新信贷管理部风险派驻岗审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkIdZH")
    protected int updateRestByPkIdZH(@RequestBody QueryModel model){
        return lmtSigInvestApprService.updateRestByPkIdZH(model);
    }

    /**
     * @函数名称:updateRestBySerno
     * @函数描述:根据Serno更新核查结论、核查意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestBySerno")
    protected int updateRestBySerno(@RequestBody QueryModel model){
        return lmtSigInvestApprService.updateRestBySerno(model);
    }

    /**
     * 更新核查报告文件路径
     * @return
     */
    @PostMapping("/updateFilePath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtSigInvestApprService.updateFilePath(condition);
        return new ResultDto<>(result);
    }

}
