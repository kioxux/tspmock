package cn.com.yusys.yusp.service.server.xddh0013;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0013.req.Xddh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.IqpPrePaymentMapper;
import cn.com.yusys.yusp.service.server.xdtz0055.Xdtz0055Service;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/10 23:03
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xddh0013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0013Service.class);
    @Autowired
    private IqpPrePaymentMapper iqpPrePaymentMapper;


    /**
     * @Description:取得记账日期为当天的提前还款授权表信息一览
     * @Author: YX-WJ
     * @Date: 2021/6/10 23:04
     * @param xddh0013DataReqDto:
     * @return: cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto
     **/
    public Xddh0013DataRespDto xddh0013(Xddh0013DataReqDto xddh0013DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value);
        Xddh0013DataRespDto xddh0013DataRespDto = new Xddh0013DataRespDto();
        try {
            String pageSize = xddh0013DataReqDto.getPageSize();
            String startPageNum = xddh0013DataReqDto.getStartPageNum();
            String queryDate = xddh0013DataReqDto.getQueryDate();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, pageSize,startPageNum,queryDate);
            if (StringUtils.isBlank(queryDate)){
                queryDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            }
            int pageSize1 = Integer.parseInt(xddh0013DataReqDto.getPageSize()) ;
            int startNum =Integer.parseInt(xddh0013DataReqDto.getStartPageNum())-1;
            int count =  iqpPrePaymentMapper.queryCoountByQueryDate(queryDate);
            List<cn.com.yusys.yusp.dto.server.xddh0013.resp.List> resultList =  iqpPrePaymentMapper.queryByQueryDate(queryDate,pageSize1,startNum);
            xddh0013DataRespDto.setTotalQnt(String.valueOf(count));
            xddh0013DataRespDto.setList(resultList);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, pageSize,startPageNum,queryDate);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value);
        return xddh0013DataRespDto;
    }
}
