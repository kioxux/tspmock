/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.RptLmtRepayAnysGuarPldDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPldDetail;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarPldDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPldDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-02 10:17:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarplddetail")
public class RptLmtRepayAnysGuarPldDetailResource {
    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarPldDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarPldDetail> list = rptLmtRepayAnysGuarPldDetailService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPldDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarPldDetail>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarPldDetail> list = rptLmtRepayAnysGuarPldDetailService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPldDetail>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarPldDetail> create(@RequestBody RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail) throws URISyntaxException {
        rptLmtRepayAnysGuarPldDetailService.insert(rptLmtRepayAnysGuarPldDetail);
        return new ResultDto<RptLmtRepayAnysGuarPldDetail>(rptLmtRepayAnysGuarPldDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarPldDetailService.update(rptLmtRepayAnysGuarPldDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId) {
        int result = rptLmtRepayAnysGuarPldDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptLmtRepayAnysGuarPldDetail>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptLmtRepayAnysGuarPldDetail>>(rptLmtRepayAnysGuarPldDetailService.selectByModel(model));
    }

    /**
     * 初始化信息
     * @param model
     * @return
     */
    @PostMapping("/initPldDetail")
    protected ResultDto<RptLmtRepayAnysGuarPldDto> initPldDetail(@RequestBody QueryModel model){
        return new ResultDto<RptLmtRepayAnysGuarPldDto>(rptLmtRepayAnysGuarPldDetailService.initPldDetail(model));
    }
    @PostMapping("/updatePldDetail")
    protected ResultDto<RptLmtRepayAnysGuarPldDto> updatePldDetail(@RequestBody QueryModel model){
        return  new ResultDto<RptLmtRepayAnysGuarPldDto>(rptLmtRepayAnysGuarPldDetailService.updatePldDetail(model));
    }
    @PostMapping("/deletePldDetail")
    protected ResultDto<Integer> deletePldDetail(@RequestBody  RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail){
        return  new ResultDto<Integer>(rptLmtRepayAnysGuarPldDetailService.deleteByPrimaryKey(rptLmtRepayAnysGuarPldDetail.getPkId()));
    }

    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPldDetailService.update(rptLmtRepayAnysGuarPldDetail));
    }
}
