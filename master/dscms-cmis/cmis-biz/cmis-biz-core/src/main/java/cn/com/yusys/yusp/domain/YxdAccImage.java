/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: YxdAccImage
 * @类描述: yxd_acc_image数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-09 13:50:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "yxd_acc_image")
public class YxdAccImage extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "BILL_NO")
	private String billNo;
	
	/** 流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 借据影像 **/
	@Column(name = "ACC_IMAGENO", unique = false, nullable = true, length = 100)
	private String accImageno;
	
	/** 额度支用授权书 **/
	@Column(name = "ACC_AUTHNO", unique = false, nullable = true, length = 100)
	private String accAuthno;
	
	/** 贷款用途声明书 **/
	@Column(name = "ACC_LOAN_DIRECT", unique = false, nullable = true, length = 100)
	private String accLoanDirect;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param accImageno
	 */
	public void setAccImageno(String accImageno) {
		this.accImageno = accImageno;
	}
	
    /**
     * @return accImageno
     */
	public String getAccImageno() {
		return this.accImageno;
	}
	
	/**
	 * @param accAuthno
	 */
	public void setAccAuthno(String accAuthno) {
		this.accAuthno = accAuthno;
	}
	
    /**
     * @return accAuthno
     */
	public String getAccAuthno() {
		return this.accAuthno;
	}
	
	/**
	 * @param accLoanDirect
	 */
	public void setAccLoanDirect(String accLoanDirect) {
		this.accLoanDirect = accLoanDirect;
	}
	
    /**
     * @return accLoanDirect
     */
	public String getAccLoanDirect() {
		return this.accLoanDirect;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}