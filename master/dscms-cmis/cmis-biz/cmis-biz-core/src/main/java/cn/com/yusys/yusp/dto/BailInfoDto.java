package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class BailInfoDto {
    private String serialno;
    private String billNo;
    private String curType;
    private BigDecimal securityMoneyAmt;

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getSecurityMoneyAmt() {
        return securityMoneyAmt;
    }

    public void setSecurityMoneyAmt(BigDecimal securityMoneyAmt) {
        this.securityMoneyAmt = securityMoneyAmt;
    }
}
