package cn.com.yusys.yusp.service.client.lmt.cmislmt0009;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @class CmisLmt0009Service
 * @date 2021/8/25 19:10
 * @desc 校验合同额度交易
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CmisLmt0009Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0009Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * @param cmisLmt0009ReqDto
     * @return cn.com.yusys.yusp.dto.server.cmisLmt0009.resp.CmisLmt0009RespDto
     * @author 王玉坤
     * @date 2021/8/25 19:11
     * @version 1.0.0
     * @desc 校验合同额度交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CmisLmt0009RespDto cmisLmt0009(CmisLmt0009ReqDto cmisLmt0009ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, JSON.toJSONString(cmisLmt0009ReqDto));
        ResultDto<CmisLmt0009RespDto> cmisLmt0009ResultDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value, JSON.toJSONString(cmisLmt0009ResultDto));

        String cmisLmt0009Code = Optional.ofNullable(cmisLmt0009ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0009Meesage = Optional.ofNullable(cmisLmt0009ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0009RespDto cmisLmt0009RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0009ResultDto.getCode()) && cmisLmt0009ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0009ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0009RespDto = cmisLmt0009ResultDto.getData();
        } else {
            if (cmisLmt0009ResultDto.getData() != null) {
                cmisLmt0009Code = cmisLmt0009ResultDto.getData().getErrorCode();
                cmisLmt0009Meesage = cmisLmt0009ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0009Code = EpbEnum.EPB099999.key;
                cmisLmt0009Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw new YuspException(cmisLmt0009Code, cmisLmt0009Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0009.value);
        return cmisLmt0009RespDto;
    }
}
