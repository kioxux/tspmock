/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtMachineEquip
 * @类描述: lmt_machine_equip数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_machine_equip")
public class LmtMachineEquip extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 授信协议编号 **/
	@Column(name = "lmt_ctr_no", unique = false, nullable = true, length = 40)
	private String lmtCtrNo;
	
	/** 项目编号 **/
	@Column(name = "pro_no", unique = false, nullable = true, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "pro_name", unique = false, nullable = true, length = 80)
	private String proName;
	
	/** 责任人姓名 **/
	@Column(name = "manager_namen", unique = false, nullable = true, length = 80)
	private String managerNamen;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 32)
	private String certCode;
	
	/** 联系方式 **/
	@Column(name = "link_mode", unique = false, nullable = true, length = 3)
	private String linkMode;
	
	/** 回购担保总额 **/
	@Column(name = "guar_amt_rebuy", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmtRebuy;
	
	/** 保证金比例 **/
	@Column(name = "bail_perc", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 单户按揭限额 **/
	@Column(name = "single_quota_sched", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleQuotaSched;
	
	/** 单户按揭期限（月） **/
	@Column(name = "single_term_sched", unique = false, nullable = true, length = 8)
	private String singleTermSched;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param managerNamen
	 */
	public void setManagerNamen(String managerNamen) {
		this.managerNamen = managerNamen;
	}
	
    /**
     * @return managerNamen
     */
	public String getManagerNamen() {
		return this.managerNamen;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkMode
	 */
	public void setLinkMode(String linkMode) {
		this.linkMode = linkMode;
	}
	
    /**
     * @return linkMode
     */
	public String getLinkMode() {
		return this.linkMode;
	}
	
	/**
	 * @param guarAmtRebuy
	 */
	public void setGuarAmtRebuy(java.math.BigDecimal guarAmtRebuy) {
		this.guarAmtRebuy = guarAmtRebuy;
	}
	
    /**
     * @return guarAmtRebuy
     */
	public java.math.BigDecimal getGuarAmtRebuy() {
		return this.guarAmtRebuy;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param singleQuotaSched
	 */
	public void setSingleQuotaSched(java.math.BigDecimal singleQuotaSched) {
		this.singleQuotaSched = singleQuotaSched;
	}
	
    /**
     * @return singleQuotaSched
     */
	public java.math.BigDecimal getSingleQuotaSched() {
		return this.singleQuotaSched;
	}
	
	/**
	 * @param singleTermSched
	 */
	public void setSingleTermSched(String singleTermSched) {
		this.singleTermSched = singleTermSched;
	}
	
    /**
     * @return singleTermSched
     */
	public String getSingleTermSched() {
		return this.singleTermSched;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}