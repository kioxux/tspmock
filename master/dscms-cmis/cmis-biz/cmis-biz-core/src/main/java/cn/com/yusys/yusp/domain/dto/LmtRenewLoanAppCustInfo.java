package cn.com.yusys.yusp.domain.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import java.math.BigDecimal;

public class LmtRenewLoanAppCustInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 客户号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 客户证件号 **/
    private String certCode;

    /** 原合同编号 **/
    private String oldContNo;

    /** 合同类型 **/
    private String contType;

    /** 合同金额 **/
    private BigDecimal contAmt;

    /** 借据余额 **/
    private BigDecimal billBalance;

    /** 手机号码 **/
    private String appPhone;

    /** 企业名称 **/
    private String conName;

    /** 统一社会信用代码 **/
    private String unifyCreditCode;

    /** 管护客户经理 **/
    private String managerId;

    /** 管护机构 **/
    private String managerBrId;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

    /** 签订日期 **/
    private String signDate;

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getOldContNo() {
        return oldContNo;
    }

    public void setOldContNo(String oldContNo) {
        this.oldContNo = oldContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getBillBalance() {
        return billBalance;
    }

    public void setBillBalance(BigDecimal billBalance) {
        this.billBalance = billBalance;
    }

    public String getAppPhone() {
        return appPhone;
    }

    public void setAppPhone(String appPhone) {
        this.appPhone = appPhone;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getUnifyCreditCode() {
        return unifyCreditCode;
    }

    public void setUnifyCreditCode(String unifyCreditCode) {
        this.unifyCreditCode = unifyCreditCode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }
}
