package cn.com.yusys.yusp.service.server.xdcz0018;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.domain.LmtSurveyTaskDivis;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.req.Xdcz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.resp.Xdcz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AreaUserMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyTaskDivisMapper;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.server.xdxw0008.Xdxw0008Service;
import com.alibaba.fastjson.JSON;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0018Service
 * @类描述: #服务类
 * @功能描述: 家速贷、极速快贷贷款申请
 * @创建人: sunzhen
 * @创建时间: 2021-06-13 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0018Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0018Service.class);

    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private Xdxw0008Service xdxw0008Service;

    @Autowired
    private AreaUserMapper areaUserMapper; //小微区域管理

    @Autowired
    private AdminSmUserService adminSmUserService; //用户信息模块

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper; //征信报告查询信息

    /**
     * 家速贷、极速快贷贷款申请
     * <p>
     * 根据传入的客户证件号码和客户名称等信息，对零售内评系统发送znsp05交易，校验客户信息是否正常，
     * 如果客户信息不正常，则返回接口；
     * 如果客户信息正常，则对征信系统发送credi11交易，查询征信信息，插入征信报告落地表，最后补全小程序线上申贷任务表。
     * 适用产品范围: 家速贷、极速快贷
     * <p>
     * 参考老接口代码：SaveXdOnlineAppInfo.java
     *
     * @param xdcz0018DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0018DataRespDto xdcz0018(Xdcz0018DataReqDto xdcz0018DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataReqDto));
        Xdcz0018DataRespDto xdcz0018DataRespDto = new Xdcz0018DataRespDto();
        try {
            String cusName = xdcz0018DataReqDto.getCusName(); //客户名称
            String certNo = xdcz0018DataReqDto.getCertNo(); //证件号码
            String cusMobile = xdcz0018DataReqDto.getCusMobile(); //客户手机号码
            BigDecimal applyAmt = xdcz0018DataReqDto.getApplyAmt(); //申请金额
            String applyTerm = xdcz0018DataReqDto.getApplyTerm(); //申请期限
            String loanUseType = xdcz0018DataReqDto.getLoanUseType(); //借款用途
            String assureMeans = xdcz0018DataReqDto.getAssureMeans(); //担保方式
            String appChnlCode = xdcz0018DataReqDto.getAppChnlCode(); //申请渠道码
            String appChnlName = xdcz0018DataReqDto.getAppChnlName(); //申请渠道名
            String managerId = xdcz0018DataReqDto.getManagerId(); //客户经理工号
            String prdCode = xdcz0018DataReqDto.getPrdCode(); //产品代码
            String indivRsdAddr = xdcz0018DataReqDto.getIndivRsdAddr(); //居住地址
            String detailAddr = xdcz0018DataReqDto.getDetailAddr(); //详细地址
            String isRepr = xdcz0018DataReqDto.getIsRepr(); //是否法人
            String businessRegiNo = xdcz0018DataReqDto.getBusinessRegiNo(); //工商注册码
            String houseSqu = xdcz0018DataReqDto.getHouseSqu(); //房屋面积
            String house = xdcz0018DataReqDto.getHouse(); //房屋预估价值
            String houseAddr = xdcz0018DataReqDto.getHouseAddr(); //房产地址
            String conOperAddr = xdcz0018DataReqDto.getConOperAddr(); //企业经营地址
            String houseOwner = xdcz0018DataReqDto.getHouseOwner(); //房产所有人
            String conName = xdcz0018DataReqDto.getConName(); //企业名称
            String conDetailAddr = xdcz0018DataReqDto.getConDetailAddr(); //企业详细地址
            String estateName = xdcz0018DataReqDto.getEstateName(); //小区名称
            String buildingInfo = xdcz0018DataReqDto.getBuildingInfo(); //楼栋信息
            String authbookContent = xdcz0018DataReqDto.getAuthbookContent(); //授权书内容
            String authSignDate = xdcz0018DataReqDto.getAuthSignDate(); //授权签订日期
            String videoNo = xdcz0018DataReqDto.getVideoNo(); //影像编号
            Date currDate = DateUtils.getCurrDate(); //当前系统时间
            String currDateTimeStr = DateUtils.formatDate(currDate, DateFormatEnum.DEFAULT.getValue()); //当前系统时间(yyyy-MM-dd HH:mm:ss)
            String currDateStr = DateUtils.formatDate(currDate, DateFormatEnum.DEFAULT.getValue()); //当前系统时间(yyyy-MM-dd)
            String serno = StringUtils.uuid(false); //流水号,根据方法自动生成
            String distributionId = ""; //分配人ID
            String distributionName = ""; //分配人名称

            if (StringUtils.isBlank(cusName)) {
                throw new Exception("客户名称为空！");
            }
            if (StringUtils.isBlank(certNo)) {
                throw new Exception("证件号码为空！");
            }
            if (StringUtils.isBlank(cusMobile)) {
                throw new Exception("客户手机号码为空！");
            }
            if (Objects.isNull(applyAmt)) {
                throw new Exception("申请金额为空！");
            }
            if (StringUtils.isBlank(applyTerm)) {
                throw new Exception("申请期限为空！");
            }
            if (StringUtils.isBlank(loanUseType)) {
                throw new Exception("借款用途为空！");
            }
            if (StringUtils.isBlank(assureMeans)) {
                throw new Exception("担保方式为空！");
            }
            if (StringUtils.isBlank(prdCode)) {
                throw new Exception("产品代码为空！");
            }
            if (StringUtils.isBlank(indivRsdAddr)) {
                throw new Exception("居住地址为空！");
            }
            if (StringUtils.isBlank(detailAddr)) {
                throw new Exception("详细地址为空！");
            }
            if (StringUtils.isBlank(isRepr)) {
                throw new Exception("是否法人为空！");
            }
            if (StringUtils.isBlank(authbookContent)) {
                throw new Exception("授权书内容为空！");
            }
            if (StringUtils.isBlank(authSignDate)) {
                throw new Exception("授权签订日期为空！");
            }
            if (StringUtils.isBlank(videoNo)) {
                throw new Exception("影像编号为空！");
            }

            //根据证件号查询客户信息
            CusBaseClientDto cusInfoDto = cmisCusClientService.queryCusByCertCode(certNo);
            String cusId = cusInfoDto.getCusId(); //客户号
            String certType = cusInfoDto.getCertType(); //客户证件类型

            //LMT_SURVEY_TASK_DIVIS 调查任务分配表
            LmtSurveyTaskDivis lmtSurveyTaskDivis = new LmtSurveyTaskDivis();
            lmtSurveyTaskDivis.setPkId(serno); //主键
            lmtSurveyTaskDivis.setSurveySerno(serno); //调查流水号
            lmtSurveyTaskDivis.setBizSerno(serno); //第三方业务流水号
            lmtSurveyTaskDivis.setPrdName(prdCode); //产品名称  TODO 暂时放prd_id
            lmtSurveyTaskDivis.setPrdType(prdCode); //产品类型
            lmtSurveyTaskDivis.setPrdId(prdCode); //产品编号
            lmtSurveyTaskDivis.setAppChnl(appChnlCode); //申请渠道
            lmtSurveyTaskDivis.setCusId(cusId); //客户编号
            lmtSurveyTaskDivis.setCertType(certType); //客户名称
            lmtSurveyTaskDivis.setCertCode(certNo); //证件号码
            lmtSurveyTaskDivis.setCusName(cusName); //客户名称
            lmtSurveyTaskDivis.setAppAmt(applyAmt); //申请金额
            lmtSurveyTaskDivis.setPhone(cusMobile); //手机号码
            lmtSurveyTaskDivis.setWork(""); //工作
            lmtSurveyTaskDivis.setLoanPurp(loanUseType); //贷款用途
            lmtSurveyTaskDivis.setIntoTime(currDateTimeStr); //进件时间
            lmtSurveyTaskDivis.setIsStopOffline("1"); //是否线下调查
            lmtSurveyTaskDivis.setBelgLine("01"); //所属条线,默认小微条线
            lmtSurveyTaskDivis.setCreateTime(currDate); //创建时间
            lmtSurveyTaskDivis.setUpdateTime(currDate); //修改时间
            lmtSurveyTaskDivis.setMarConfirmStatus("00"); //客户经理确认状态 默认待确认
            lmtSurveyTaskDivis.setMarId(managerId);
            lmtSurveyTaskDivis.setPrcId(""); //处理人
            lmtSurveyTaskDivis.setDivisTime(""); //分配时间
            lmtSurveyTaskDivis.setDivisId(""); //分配人
            lmtSurveyTaskDivis.setManagerName(""); //客户经理名称
            lmtSurveyTaskDivis.setManagerId(""); //客户经理编号
            lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
            lmtSurveyTaskDivis.setDivisStatus("101"); //调查分配状态

            String suggestId = "";
            //家速贷 推荐人不为空，分配给推荐人
            if ("PW030012".equals(prdCode) && !"".equals(managerId)) {
                //根据cus_mgr_no查询推荐人信息
                suggestId = managerId;
                String suggest_name = ""; //客户经理名称
                String suggest_manager_area = ""; //客户经理片区

                lmtSurveyTaskDivis.setDivisTime(currDateTimeStr); //分配时间
                lmtSurveyTaskDivis.setDivisId("admin"); //分配人
                lmtSurveyTaskDivis.setManagerName(suggest_name); //客户经理名称
                lmtSurveyTaskDivis.setManagerId(managerId); //客户经理编号
                lmtSurveyTaskDivis.setManagerArea(suggest_manager_area); //客户经理片区
                lmtSurveyTaskDivis.setDivisStatus("100"); //调查分配状态,已分配
                int insertCount = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                if (insertCount < 1) {
                    throw new Exception("调查任务分配表插入失败!");
                }
            }

            //家速贷 推荐人为空,根据居住地址判断分配人ID,人工分配
            if ("PW030012".equals(prdCode) && "".equals(managerId)) {

                indivRsdAddr = indivRsdAddr.trim();
                String areaName = xdxw0008Service.adderCheck(indivRsdAddr); //根据居住地址获得所在区域名称
                if ("".equals(areaName)) {
                    throw new Exception("您的经营地址不在我行业务范围内！请联系0512-56968262处理！");
                }

                //通过角色编号和区域名称查询 用户号用户名称和手机号 此段逻辑参考XDXW0008接口
                String actorno = ""; //分配人id
                String actorname = ""; //分配人姓名
                String telnum = ""; //分配人电话
                //第一步：在cmis_biz执行通过 区域名称查询“用户号列表” 放到个list里面
                List<String> userNoList = areaUserMapper.getUserNoByAreaName(areaName);
                //第二步：把list组装成一个查询条件字符串 类似  'A','B','C'
                if (userNoList.size() > 0) {
                    String condition = xdxw0008Service.getStr(userNoList);
                    //第三步：在yusp_admin库执行通过角色号和“用户号列表” 查询用户号列表信息
                    //TODO 需要研发提供以下SQL的方法来调用:
                    // (1) ( 'A', 'B', 'C' ) 为上方 condition 变量传入
                    // (2) ROLE_CODE 角色代码 是否是sql写死还是传入待定
                    /*
                        SELECT
                            A.login_code,-- t.actorno
                            A.user_name,-- t.actorname
                            A.user_mobilephone -- t.telnum
                        FROM
                            admin_sm_user A
                            INNER JOIN admin_sm_user_role_rel B ON B.USER_ID = A.user_id
                            INNER JOIN admin_sm_role C ON C.role_id = B.role_id
                        WHERE
                            C.ROLE_CODE = '1105'
                            AND A.login_code IN ( 'A', 'B', 'C' )
                        ORDER BY
                            rand( )
                        LIMIT 1
                     */
                    //............
                    distributionId = ""; //分配人id = login_code

                    lmtSurveyTaskDivis.setDivisTime(""); //分配时间
                    lmtSurveyTaskDivis.setDivisId(distributionId); //分配人
                    lmtSurveyTaskDivis.setManagerName(""); //客户经理名称
                    lmtSurveyTaskDivis.setManagerId(""); //客户经理编号
                    lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
                    lmtSurveyTaskDivis.setDivisStatus("101"); //调查分配状态,未分配
                    int insertCount = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                    if (insertCount < 1) {
                        throw new Exception("调查任务分配表插入失败!");
                    }
                } else {
                    throw new Exception("未查询到该区域下的用户!区域: " + areaName);
                }
            }

            //业务细分为极速快贷且渠道码不为空的情况下根据渠道对应的接单人确定分配人ID
            if (!"PW030012".equals(prdCode) && !StringUtils.isBlank(appChnlCode)) {
                //TODO 老信贷渠道准入表xd_channel_info缺失，新信贷的表没确定，等处理好了再来改
                //select order_rece_id,channel_name from Xd_Channel_Info t where t.serno='"+channel_id+"'"
                //先写死两个字段吧
                distributionId = "00010001";
                appChnlName = "暂时写死的分配人";
                if (!StringUtils.isBlank(distributionId)) {
                    lmtSurveyTaskDivis.setDivisTime(""); //分配时间
                    lmtSurveyTaskDivis.setDivisId(distributionId); //分配时间
                    lmtSurveyTaskDivis.setManagerName(""); //客户经理名称
                    lmtSurveyTaskDivis.setManagerId(""); //客户经理编号
                    lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
                    lmtSurveyTaskDivis.setDivisStatus("101"); //调查分配状态,未分配
                    int insertCount = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                    if (insertCount < 1) {
                        throw new Exception("调查任务分配表插入失败!");
                    }
                } else {
                    throw new Exception("渠道商接单人为空或者渠道商接单人中文名为空！");
                }
            } else {
                throw new Exception("请仔细核实相关信息，业务细分或渠道码有误!");
            }

            String userId = "";//客户经理号
            if (!"".equals(distributionId)) {
                userId = distributionId;
            } else {
                userId = suggestId;
            }
            //根据userid查询客户经理/分配人信息
            ResultDto<AdminSmUserDto> userResultDto = adminSmUserService.getByLoginCode(userId);
            AdminSmUserDto adminSmUserDto = Optional.ofNullable(userResultDto.getData()).orElse(new AdminSmUserDto());
            String createUser = (null == adminSmUserDto.getUserName() ? "" : adminSmUserDto.getUserName()); //客户经理名称
            String createUserCode = (null == adminSmUserDto.getUserCode() ? "" : adminSmUserDto.getUserCode()); //客户经理证件号
            String createUserPhone = (null == adminSmUserDto.getUserMobilephone() ? "" : adminSmUserDto.getUserMobilephone()); //客户经理手机号

            //调用智能风控znsp05查询黑名单信息
            //TradeComponent tradeComponent = (TradeComponent) CMISComponentFactory.getComponentFactoryInstance()
            //        .getComponentInstance(TradePubConstant.tradeName, context,conn);
            //context.addDataField("currentuserid", userid);
            //context.addDataField("S_organno", "016000");
            //KeyedCollection znsp05reqPkg = new KeyedCollection();
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"prcscd","znsp05");
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"servtp",PUBUtilTools.getServtp());
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"servsq",PUBUtilTools.getServsq());
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"userid","016000");
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"brchno",userid);
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"cus_name",cus_name);//客户名称
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"cert_no",cert_no);//证件号码
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"cus_mgr_no",userid);//客户经理编号
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"cus_mgr_name",createUser);//客户经理名称
            //KeyedCollectionUtil.setValue(znsp05reqPkg,"check_type","01");//黑名单
            //KeyedCollection znsp05repPkg = tradeComponent.call8HeadFormate("znsp05", znsp05reqPkg, "esbServiceId");
            //String znsp05rorcd = (String)KeyedCollectionUtil.getDataValue(znsp05repPkg, "erorcd");
            //String znsp05result = (String)KeyedCollectionUtil.getDataValue(znsp05repPkg, "result");
            //if(!"0000".equals(znsp05rorcd)){
            //    erorcd = "9999";
            //    erortx="查询黑名单异常";
            //}else{
            //    try{
            //        if("false".equals(znsp05result)){
            //            erorcd = "9999";
            //            erortx = "该用户为信贷黑名单用户";
            //        }
            //    }catch(Exception e){
            //        erorcd = "9999";
            //        erortx ="信贷系统查询黑名单异常";
            //        e.printStackTrace();
            //    }
            //}

            //调用智能风控znsp05查询失信前科
            //KeyedCollection znsp05reqPkg2 = new KeyedCollection();
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"prcscd","znsp05");
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"servtp",PUBUtilTools.getServtp());
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"servsq",PUBUtilTools.getServsq());
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"userid","016000");
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"brchno",userid);
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"cus_name",cus_name);//客户名称
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"cert_no",cert_no);//证件号码
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"cus_mgr_no",userid);//客户经理编号
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"cus_mgr_name",createUser);//客户经理名称
            //KeyedCollectionUtil.setValue(znsp05reqPkg2,"check_type","02");//失信前科
            //KeyedCollection znsp05repPkg2 = tradeComponent.call8HeadFormate("znsp05", znsp05reqPkg2, "esbServiceId");
            //String znsp05rorcd2 = (String)KeyedCollectionUtil.getDataValue(znsp05repPkg, "erorcd");
            //String znsp05result2 = (String)KeyedCollectionUtil.getDataValue(znsp05repPkg, "result");
            //if(!"0000".equals(znsp05rorcd2)){
            //    erorcd = "9999";
            //    erortx="查询失信被执行人异常";
            //}else{
            //    try{
            //        if("false".equals(znsp05result2)){
            //            erorcd = "9999";
            //            erortx = "该用户为失信被执行人或有前科人员";
            //        }
            //    }catch(Exception e){
            //        erorcd = "9999";
            //        erortx ="信贷系统查询失信被执行人异常";
            //        e.printStackTrace();
            //    }
            //}

            //调用征信系统 credi11接口查询失信前科
            String[] scanIdArg = videoNo.split(","); //影像id
            if (scanIdArg.length < 2) {
                throw new Exception("影像编号输入错误");
            } else {
                String scanId = scanIdArg[0];
                String scanIdDs = scanIdArg[1];
                String borrowPersonRelationName = "";
                String borrowPersonRelationNumber = "";
                String brchno = "016000"; //客户经理所属机构
                String serReason = "02"; //查询原因
                String borrowpersonrelation = "001"; //与主借款人关系
                //    KeyedCollection reqPkg = new KeyedCollection("credi11");
                //    KeyedCollection repPkg = new KeyedCollection();
                //    reqPkg.addDataField("prcscd", "credi11");
                //    reqPkg.addDataField("servtp", PUBUtilTools.getServtp());
                //    reqPkg.addDataField("servsq", PUBUtilTools.getServsq());
                //    reqPkg.addDataField("userid", userid);
                //    reqPkg.addDataField("brchno", brchno);
                //    reqPkg.addDataField("datasq", "");
                //    reqPkg.addDataField("creditType","0");
                //    reqPkg.addDataField("customName",cus_name);//客户名称
                //    reqPkg.addDataField("scanId",scanId);//影像编号
                //    reqPkg.addDataField("certificateType","10");//证件类型
                //    reqPkg.addDataField("certificateNum",cert_no);//证件号码
                //    reqPkg.addDataField("createUserCode",createUserCode);//客户经理身份证号
                //    reqPkg.addDataField("createUser",createUser);//客户经理名称
                //    reqPkg.addDataField("applySrc","");
                //    reqPkg.addDataField("createDate",createservdt);//授权签订日期
                //    reqPkg.addDataField("auditReason",auditReason);//授权书内容
                //    reqPkg.addDataField("tranCode","");
                //    reqPkg.addDataField("queryType","");
                //    reqPkg.addDataField("queryStartDate","");
                //    reqPkg.addDataField("areaId","");
                //    reqPkg.addDataField("areaName","");
                //    reqPkg.addDataField("queryReason",serReason);//查询原因
                //    reqPkg.addDataField("borrowPersonRelation",borrowpersonrelation);
                //    reqPkg.addDataField("borrowPersonRelationName",borrowPersonRelationName);
                //    reqPkg.addDataField("borrowPersonRelationNumber",borrowPersonRelationNumber);
                //    reqPkg.addDataField("isAutoCheckPass","true");//是否自动审批通过
                //    reqPkg.addDataField("createUserPhone",createUserPhone);//客户经理手机号
                //    reqPkg.addDataField("areaQuery","0");//是否跨区域
                //    repPkg = tradeComponent.call8HeadFormate("credi11", reqPkg, "esbServiceId");
                //    String credierorcd = (String)KeyedCollectionUtil.getDataValue(repPkg, "erorcd");
                //    if(!"0000".equals(credierorcd)){
                //        erorcd = "9999";
                //        erortx="查询征信异常";
                //    }else{
                //        try{
                //            String zxSerno = (String)KeyedCollectionUtil.getDataValue(repPkg, "reqId");

                //老信贷保存的是这个表：ZxReportFromXcx
                //新信贷保存的是这个表：CREDIT_REPORT_QRY_LST	征信报告查询信息表
                CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
                creditReportQryLst.setCrqlSerno(serno); //征信查询流水号
                creditReportQryLst.setCertType("10"); //查询对象证件类型
                creditReportQryLst.setCertCode(certNo); //查询对象证件号码
                creditReportQryLst.setCusId(cusId); //征信查询对象客户号
                creditReportQryLst.setCusName(cusName); //征信查询对象名称
                creditReportQryLst.setBorrowerCusId(cusId); //主借款人客户号
                creditReportQryLst.setBorrowerCertCode(certNo); //主借款人证件号码
                creditReportQryLst.setBorrowerCusName(cusName); //主借款人名称
                creditReportQryLst.setBorrowRel("001"); //与主借款人关系,默认001-主借款人
                creditReportQryLst.setQryCls("0"); //征信查询类别,默认0-个人
                creditReportQryLst.setQryResn(serReason); //征信查询原因,默认02-贷款审批
                creditReportQryLst.setQryResnDec(""); //查询原因描述
                creditReportQryLst.setAuthbookNo(""); //授权书编号
                creditReportQryLst.setAuthbookContent(authbookContent); //授权书内容
                creditReportQryLst.setOtherAuthbookExt(""); //其他授权书内容
                creditReportQryLst.setAuthbookDate(authSignDate); //授权书日期
                creditReportQryLst.setManagerId(userId); //主管客户经理
                creditReportQryLst.setManagerBrId("016000"); //主管机构
                creditReportQryLst.setSendTime(currDateTimeStr); //成功发起时间
                creditReportQryLst.setApproveStatus("997"); //审批状态,默认997-审批通过
                creditReportQryLst.setIsSuccssInit("1"); //是否成功发起,默认1是
                creditReportQryLst.setReportNo(""); //TODO zxSerno 征信报告编号
                creditReportQryLst.setQryStatus("003"); //征信查询状态,默认003-已查询
                creditReportQryLst.setCreditUrl(""); //征信返回地址
                creditReportQryLst.setReportCreateTime(currDateTimeStr); //报告生成时间
                creditReportQryLst.setQryFlag("02"); //征信查询标识,默认02-业务发起
                creditReportQryLst.setImageNo(scanId); //影像编号 TODO 接口里还有一个大数据影像编号scanIdDs 不知道干哈用的
                creditReportQryLst.setCreateTime(currDate); //创建时间
                creditReportQryLst.setUpdateTime(currDate); //修改时间
                int insertCount = creditReportQryLstMapper.insert(creditReportQryLst);
                if (insertCount < 1) {
                    throw new Exception("征信报告查询信息表插入失败!");
                }

                //        }catch(Exception e){
                //            erorcd = "9999";
                //            erortx ="信贷系统查询征信异常";
                //            e.printStackTrace();
                //        }
                //    }
            }

            xdcz0018DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xdcz0018DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, e.getMessage());
            xdcz0018DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdcz0018DataRespDto.setOpMsg(CommonConstance.OP_MSG_F + e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataRespDto));
        return xdcz0018DataRespDto;
    }
}
