/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSub;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubAcc;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoSubAppr;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoSubAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoSubAccService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:24:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoSubAccService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoSubAccMapper lmtSigInvestBasicInfoSubAccMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    @Autowired
    private LmtSigInvestBasicInfoSubAccService lmtSigInvestBasicInfoSubAccService ;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSubAcc selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubAccMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoSubAcc> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoSubAcc> records = (List<LmtSigInvestBasicInfoSubAcc>) lmtSigInvestBasicInfoSubAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoSubAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoSubAcc> list = lmtSigInvestBasicInfoSubAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoSubAcc record) {
        return lmtSigInvestBasicInfoSubAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoSubAcc record) {
        return lmtSigInvestBasicInfoSubAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoSubAcc record) {
        return lmtSigInvestBasicInfoSubAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoSubAcc record) {
        return lmtSigInvestBasicInfoSubAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoSubAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoSubAccMapper.deleteByIds(ids);
    }



    /**
     * @方法名称: initLmtSigInvestBasicInfoSubAccInfo
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoSubAcc initLmtSigInvestBasicInfoSubAccInfo(LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubAppr, boolean chgFlag) {
        String origiBasicAccNo = sigInvestBasicInfoSubAppr.getOrigiBasicAccNo() ;
        LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc  ;
        //初始化对象 原底层台账编号不为空，变更
        String pkId = "" ;
        String basicAccNo = "" ;
        if(StringUtils.nonBlank(origiBasicAccNo) && chgFlag){
            lmtSigInvestBasicInfoSubAcc = lmtSigInvestBasicInfoSubAccService.selectByBasicAccNo(origiBasicAccNo) ;
            if(lmtSigInvestBasicInfoSubAcc!=null){
                pkId = lmtSigInvestBasicInfoSubAcc.getPkId() ;
                basicAccNo = lmtSigInvestBasicInfoSubAcc.getBasicAccNo() ;
            }
        }else{
            lmtSigInvestBasicInfoSubAcc = new LmtSigInvestBasicInfoSubAcc() ;
        }
        if(StringUtils.isBlank(pkId)) pkId = generatePkId() ;
        if(StringUtils.isBlank(basicAccNo)) basicAccNo = generateSerno(SeqConstant.INVEST_LMT_ACC_SEQ) ;
        //拷贝数据
        BeanUtils.copyProperties(sigInvestBasicInfoSubAppr, lmtSigInvestBasicInfoSubAcc);
        //设置主键
        lmtSigInvestBasicInfoSubAcc.setPkId(pkId);
        //设置底层授信台账号
        lmtSigInvestBasicInfoSubAcc.setBasicAccNo(basicAccNo);
        //最新更新日期
        lmtSigInvestBasicInfoSubAcc.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicInfoSubAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicInfoSubAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestBasicInfoSubAcc;
    }

    /**
     * 根据申请流水号查询单笔投资关联底层明细信息
     * @param serno
     * @return
     */
    public List<Map<String,Object>> selectBySerno(String serno){
        return lmtSigInvestBasicInfoSubAccMapper.selectBySerno(serno);
    }

    /**
     * 根据申请流水号查询单笔投资关联底层明细信息
     * @param basicAccNo
     * @return
     */
    public LmtSigInvestBasicInfoSubAcc selectByBasicAccNo(String basicAccNo){
        return lmtSigInvestBasicInfoSubAccMapper.selectByBasicAccNo(basicAccNo);
    }

    /**
     * 根据批复流水号查询单笔投资关联底层明细信息
     * @param replySerno
     * @return
     */
    public List<LmtSigInvestBasicInfoSubAcc> selectByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        return selectAll(queryModel);
    }

    public void initLmtSigInvestBasicSubInfo(LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc, String serno) {

    }

    /**
     * 批复变更流程-生成台账信息
     * @param lmtSigInvestBasicInfoSub
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     * @param accSerno
     */
    public void insertBasicInfoSubAcc(LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub, String oldSerno, String replySerno, String currentOrgId, String currentUserId, String accSerno) {
        LmtSigInvestBasicInfoSubAcc lmtSigInvestBasicInfoSubAcc = new LmtSigInvestBasicInfoSubAcc();
        BeanUtils.copyProperties(lmtSigInvestBasicInfoSub,lmtSigInvestBasicInfoSubAcc);
        lmtSigInvestBasicInfoSubAcc.setSerno(oldSerno);
        lmtSigInvestBasicInfoSubAcc.setAccNo(accSerno);
        lmtSigInvestBasicInfoSubAcc.setReplySerno(replySerno);
        //初始化通用新增属性信息
        initInsertDomainProperties(lmtSigInvestBasicInfoSubAcc,currentUserId,currentOrgId);
        insert(lmtSigInvestBasicInfoSubAcc);
    }

    /**
     * @方法名称: deleteByAccNO
     * @方法描述: 根据台账编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByAccNo(String accNo) {
        return lmtSigInvestBasicInfoSubAccMapper.deleteByaccNo(accNo);
    }

}
