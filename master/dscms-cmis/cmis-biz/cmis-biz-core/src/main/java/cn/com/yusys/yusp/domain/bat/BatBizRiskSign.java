/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain.bat;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizRiskSign
 * @类描述: bat_biz_risk_sign数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-29 21:32:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "bat_biz_risk_sign")
public class BatBizRiskSign extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 预警单编号 **/
	@Id
	@Column(name = "RISK_ID")
	private String riskId;
	
	/** 客户编号 **/
	@Column(name = "CUST_ID", unique = false, nullable = true, length = 32)
	private String custId;
	
	/** 客户名称 **/
	@Column(name = "CUST_NAME", unique = false, nullable = true, length = 80)
	private String custName;
	
	/** 客户经理 **/
	@Column(name = "MAIN_MGR", unique = false, nullable = true, length = 20)
	private String mainMgr;
	
	/** 所属机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 预警信号来源 **/
	@Column(name = "SIGN_SOURCE", unique = false, nullable = true, length = 4)
	private String signSource;
	
	/** 预警等级 **/
	@Column(name = "RISK_LVL", unique = false, nullable = true, length = 4)
	private String riskLvl;
	
	/** 预警日期 **/
	@Column(name = "RISK_DATE", unique = false, nullable = true, length = 12)
	private String riskDate;
	
	/** 预警状态 **/
	@Column(name = "RISK_STATUS", unique = false, nullable = true, length = 20)
	private String riskStatus;
	
	/** 规则编号 **/
	@Column(name = "RISK_NUM", unique = false, nullable = true, length = 20)
	private String riskNum;
	
	/** 预警大类 **/
	@Column(name = "RISK_BIG", unique = false, nullable = true, length = 32)
	private String riskBig;
	
	/** 预警子项 **/
	@Column(name = "RISK_SUB", unique = false, nullable = true, length = 2000)
	private String riskSub;
	
	/** 预警条件 **/
	@Column(name = "RISK_REASION", unique = false, nullable = true, length = 2000)
	private String riskReasion;
	
	/** 预警输出描述 **/
	@Column(name = "RISK_OUT_REASION", unique = false, nullable = true, length = 4000)
	private String riskOutReasion;

	/** 预警类型 **/
	@Column(name = "RISK_TYPE", unique = false, nullable = true, length = 8)
	private String riskType;

	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param riskId
	 */
	public void setRiskId(String riskId) {
		this.riskId = riskId;
	}
	
    /**
     * @return riskId
     */
	public String getRiskId() {
		return this.riskId;
	}
	
	/**
	 * @param custId
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
    /**
     * @return custId
     */
	public String getCustId() {
		return this.custId;
	}
	
	/**
	 * @param custName
	 */
	public void setCustName(String custName) {
		this.custName = custName;
	}
	
    /**
     * @return custName
     */
	public String getCustName() {
		return this.custName;
	}
	
	/**
	 * @param mainMgr
	 */
	public void setMainMgr(String mainMgr) {
		this.mainMgr = mainMgr;
	}
	
    /**
     * @return mainMgr
     */
	public String getMainMgr() {
		return this.mainMgr;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param signSource
	 */
	public void setSignSource(String signSource) {
		this.signSource = signSource;
	}
	
    /**
     * @return signSource
     */
	public String getSignSource() {
		return this.signSource;
	}
	
	/**
	 * @param riskLvl
	 */
	public void setRiskLvl(String riskLvl) {
		this.riskLvl = riskLvl;
	}
	
    /**
     * @return riskLvl
     */
	public String getRiskLvl() {
		return this.riskLvl;
	}
	
	/**
	 * @param riskDate
	 */
	public void setRiskDate(String riskDate) {
		this.riskDate = riskDate;
	}
	
    /**
     * @return riskDate
     */
	public String getRiskDate() {
		return this.riskDate;
	}
	
	/**
	 * @param riskStatus
	 */
	public void setRiskStatus(String riskStatus) {
		this.riskStatus = riskStatus;
	}
	
    /**
     * @return riskStatus
     */
	public String getRiskStatus() {
		return this.riskStatus;
	}
	
	/**
	 * @param riskNum
	 */
	public void setRiskNum(String riskNum) {
		this.riskNum = riskNum;
	}
	
    /**
     * @return riskNum
     */
	public String getRiskNum() {
		return this.riskNum;
	}
	
	/**
	 * @param riskBig
	 */
	public void setRiskBig(String riskBig) {
		this.riskBig = riskBig;
	}
	
    /**
     * @return riskBig
     */
	public String getRiskBig() {
		return this.riskBig;
	}
	
	/**
	 * @param riskSub
	 */
	public void setRiskSub(String riskSub) {
		this.riskSub = riskSub;
	}
	
    /**
     * @return riskSub
     */
	public String getRiskSub() {
		return this.riskSub;
	}
	
	/**
	 * @param riskReasion
	 */
	public void setRiskReasion(String riskReasion) {
		this.riskReasion = riskReasion;
	}
	
    /**
     * @return riskReasion
     */
	public String getRiskReasion() {
		return this.riskReasion;
	}
	
	/**
	 * @param riskOutReasion
	 */
	public void setRiskOutReasion(String riskOutReasion) {
		this.riskOutReasion = riskOutReasion;
	}
	
    /**
     * @return riskOutReasion
     */
	public String getRiskOutReasion() {
		return this.riskOutReasion;
	}


	/**
	 * @param riskType
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

    /**
     * @return riskType
     */
	public String getRiskType() {
		return this.riskType;
	}


}