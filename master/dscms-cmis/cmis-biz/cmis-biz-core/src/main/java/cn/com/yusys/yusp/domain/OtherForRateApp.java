/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherForRateApp
 * @类描述: other_for_rate_app数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-07 15:58:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_for_rate_app")
public class OtherForRateApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 优惠理由 **/
	@Column(name = "PREFER_RESN", unique = false, nullable = true, length = 4000)
	private String preferResn;
	
	/** 客户性质 **/
	@Column(name = "CUS_NATURE", unique = false, nullable = true, length = 5)
	private String cusNature;
	
	/** 信用等级 **/
	@Column(name = "CUS_CRD_GRADE", unique = false, nullable = true, length = 5)
	private String cusCrdGrade;
	
	/** 上年我行存款日均人民币 **/
	@Column(name = "LYEAR_AVERDAY_DEP_CNY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lyearAverdayDepCny;
	
	/** 上年我行存款日均外币 **/
	@Column(name = "LYEAR_AVERDAY_DEP_FOR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lyearAverdayDepFor;
	
	/** 上年我行国际结算量 **/
	@Column(name = "LYEAR_INTER_SETTL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lyearInterSettl;
	
	/** 上年我行结售汇量 **/
	@Column(name = "LYEAR_SALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lyearSale;
	
	/** 其他贡献情况 **/
	@Column(name = "OTHER_CONTRITUTIONS", unique = false, nullable = true, length = 4000)
	private String otherContritutions;
	
	/** 其他合作意向 **/
	@Column(name = "OTHER_INTEN_FOR_COOP", unique = false, nullable = true, length = 4000)
	private String otherIntenForCoop;
	
	/** 优惠项目 **/
	@Column(name = "PREFER_PRO", unique = false, nullable = true, length = 4000)
	private String preferPro;
	
	/** 基本收费标准 **/
	@Column(name = "STD_BASE_CHRG", unique = false, nullable = true, length = 80)
	private String stdBaseChrg;
	
	/** 上年优惠情况 **/
	@Column(name = "LYEAR_OFFER_RATE", unique = false, nullable = true, length = 80)
	private String lyearOfferRate;
	
	/** 本次申请优惠价格 **/
	@Column(name = "CURT_APP_OFFER_RATE", unique = false, nullable = true, length = 80)
	private String curtAppOfferRate;
	
	/** 优惠期限起始日 **/
	@Column(name = "PREFER_TERM_START_DATE", unique = false, nullable = true, length = 10)
	private String preferTermStartDate;
	
	/** 优惠期限到期日 **/
	@Column(name = "PREFER_TERM_END_DATE", unique = false, nullable = true, length = 10)
	private String preferTermEndDate;
	
	/** 他行优惠条件 **/
	@Column(name = "OTHER_BANK_PREFER_COND", unique = false, nullable = true, length = 4000)
	private String otherBankPreferCond;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = false, length = 5)
	private String status;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = false, length = 5)
	private String approveStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 业务类型（特殊需要） **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param preferResn
	 */
	public void setPreferResn(String preferResn) {
		this.preferResn = preferResn;
	}
	
    /**
     * @return preferResn
     */
	public String getPreferResn() {
		return this.preferResn;
	}
	
	/**
	 * @param cusNature
	 */
	public void setCusNature(String cusNature) {
		this.cusNature = cusNature;
	}
	
    /**
     * @return cusNature
     */
	public String getCusNature() {
		return this.cusNature;
	}
	
	/**
	 * @param cusCrdGrade
	 */
	public void setCusCrdGrade(String cusCrdGrade) {
		this.cusCrdGrade = cusCrdGrade;
	}
	
    /**
     * @return cusCrdGrade
     */
	public String getCusCrdGrade() {
		return this.cusCrdGrade;
	}
	
	/**
	 * @param lyearAverdayDepCny
	 */
	public void setLyearAverdayDepCny(java.math.BigDecimal lyearAverdayDepCny) {
		this.lyearAverdayDepCny = lyearAverdayDepCny;
	}
	
    /**
     * @return lyearAverdayDepCny
     */
	public java.math.BigDecimal getLyearAverdayDepCny() {
		return this.lyearAverdayDepCny;
	}
	
	/**
	 * @param lyearAverdayDepFor
	 */
	public void setLyearAverdayDepFor(java.math.BigDecimal lyearAverdayDepFor) {
		this.lyearAverdayDepFor = lyearAverdayDepFor;
	}
	
    /**
     * @return lyearAverdayDepFor
     */
	public java.math.BigDecimal getLyearAverdayDepFor() {
		return this.lyearAverdayDepFor;
	}
	
	/**
	 * @param lyearInterSettl
	 */
	public void setLyearInterSettl(java.math.BigDecimal lyearInterSettl) {
		this.lyearInterSettl = lyearInterSettl;
	}
	
    /**
     * @return lyearInterSettl
     */
	public java.math.BigDecimal getLyearInterSettl() {
		return this.lyearInterSettl;
	}
	
	/**
	 * @param lyearSale
	 */
	public void setLyearSale(java.math.BigDecimal lyearSale) {
		this.lyearSale = lyearSale;
	}
	
    /**
     * @return lyearSale
     */
	public java.math.BigDecimal getLyearSale() {
		return this.lyearSale;
	}
	
	/**
	 * @param otherContritutions
	 */
	public void setOtherContritutions(String otherContritutions) {
		this.otherContritutions = otherContritutions;
	}
	
    /**
     * @return otherContritutions
     */
	public String getOtherContritutions() {
		return this.otherContritutions;
	}
	
	/**
	 * @param otherIntenForCoop
	 */
	public void setOtherIntenForCoop(String otherIntenForCoop) {
		this.otherIntenForCoop = otherIntenForCoop;
	}
	
    /**
     * @return otherIntenForCoop
     */
	public String getOtherIntenForCoop() {
		return this.otherIntenForCoop;
	}
	
	/**
	 * @param preferPro
	 */
	public void setPreferPro(String preferPro) {
		this.preferPro = preferPro;
	}
	
    /**
     * @return preferPro
     */
	public String getPreferPro() {
		return this.preferPro;
	}
	
	/**
	 * @param stdBaseChrg
	 */
	public void setStdBaseChrg(String stdBaseChrg) {
		this.stdBaseChrg = stdBaseChrg;
	}
	
    /**
     * @return stdBaseChrg
     */
	public String getStdBaseChrg() {
		return this.stdBaseChrg;
	}
	
	/**
	 * @param lyearOfferRate
	 */
	public void setLyearOfferRate(String lyearOfferRate) {
		this.lyearOfferRate = lyearOfferRate;
	}
	
    /**
     * @return lyearOfferRate
     */
	public String getLyearOfferRate() {
		return this.lyearOfferRate;
	}
	
	/**
	 * @param curtAppOfferRate
	 */
	public void setCurtAppOfferRate(String curtAppOfferRate) {
		this.curtAppOfferRate = curtAppOfferRate;
	}
	
    /**
     * @return curtAppOfferRate
     */
	public String getCurtAppOfferRate() {
		return this.curtAppOfferRate;
	}
	
	/**
	 * @param preferTermStartDate
	 */
	public void setPreferTermStartDate(String preferTermStartDate) {
		this.preferTermStartDate = preferTermStartDate;
	}
	
    /**
     * @return preferTermStartDate
     */
	public String getPreferTermStartDate() {
		return this.preferTermStartDate;
	}
	
	/**
	 * @param preferTermEndDate
	 */
	public void setPreferTermEndDate(String preferTermEndDate) {
		this.preferTermEndDate = preferTermEndDate;
	}
	
    /**
     * @return preferTermEndDate
     */
	public String getPreferTermEndDate() {
		return this.preferTermEndDate;
	}
	
	/**
	 * @param otherBankPreferCond
	 */
	public void setOtherBankPreferCond(String otherBankPreferCond) {
		this.otherBankPreferCond = otherBankPreferCond;
	}
	
    /**
     * @return otherBankPreferCond
     */
	public String getOtherBankPreferCond() {
		return this.otherBankPreferCond;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	/**
	 * @return bizType
	 */
	public String getBizType() {
		return this.bizType;
	}
}