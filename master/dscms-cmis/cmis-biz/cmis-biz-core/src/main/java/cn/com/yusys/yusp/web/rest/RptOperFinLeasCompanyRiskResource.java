/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperFinLeasCompanyRisk;
import cn.com.yusys.yusp.service.RptOperFinLeasCompanyRiskService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperFinLeasCompanyRiskResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 21:11:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperfinleascompanyrisk")
public class RptOperFinLeasCompanyRiskResource {
    @Autowired
    private RptOperFinLeasCompanyRiskService rptOperFinLeasCompanyRiskService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperFinLeasCompanyRisk>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperFinLeasCompanyRisk> list = rptOperFinLeasCompanyRiskService.selectAll(queryModel);
        return new ResultDto<List<RptOperFinLeasCompanyRisk>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperFinLeasCompanyRisk>> index(QueryModel queryModel) {
        List<RptOperFinLeasCompanyRisk> list = rptOperFinLeasCompanyRiskService.selectByModel(queryModel);
        return new ResultDto<List<RptOperFinLeasCompanyRisk>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperFinLeasCompanyRisk> show(@PathVariable("pkId") String pkId) {
        RptOperFinLeasCompanyRisk rptOperFinLeasCompanyRisk = rptOperFinLeasCompanyRiskService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperFinLeasCompanyRisk>(rptOperFinLeasCompanyRisk);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperFinLeasCompanyRisk> create(@RequestBody RptOperFinLeasCompanyRisk rptOperFinLeasCompanyRisk) throws URISyntaxException {
        rptOperFinLeasCompanyRiskService.insert(rptOperFinLeasCompanyRisk);
        return new ResultDto<RptOperFinLeasCompanyRisk>(rptOperFinLeasCompanyRisk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperFinLeasCompanyRisk rptOperFinLeasCompanyRisk) throws URISyntaxException {
        int result = rptOperFinLeasCompanyRiskService.update(rptOperFinLeasCompanyRisk);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperFinLeasCompanyRiskService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperFinLeasCompanyRiskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperFinLeasCompanyRisk>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptOperFinLeasCompanyRisk>>(rptOperFinLeasCompanyRiskService.selectByModel(model));
    }
    @PostMapping("/deleteRisk")
    protected ResultDto<Integer> deleteRisk(@RequestBody RptOperFinLeasCompanyRisk rptOperFinLeasCompanyRisk){
        return new ResultDto<Integer>(rptOperFinLeasCompanyRiskService.deleteByPrimaryKey(rptOperFinLeasCompanyRisk.getPkId()));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptOperFinLeasCompanyRisk rptOperFinLeasCompanyRisk){
        return new ResultDto<Integer>(rptOperFinLeasCompanyRiskService.save(rptOperFinLeasCompanyRisk));
    }
}
