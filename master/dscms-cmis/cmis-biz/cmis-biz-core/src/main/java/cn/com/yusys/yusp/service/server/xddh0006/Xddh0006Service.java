package cn.com.yusys.yusp.service.server.xddh0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111RespDto;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0006.resp.Xddh0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.NewCoreEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 接口处理类:提前还款试算查询
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xddh0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0006Service.class);
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3111.Ln3111Service ln3111Service;// 业务逻辑处理类：贷款归还试算

    /**
     * 提前还款试算查询
     *
     * @param xddh0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0006DataRespDto xddh0006(Xddh0006DataReqDto xddh0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value);
        Xddh0006DataRespDto xddh0006DataRespDto = new Xddh0006DataRespDto();
        String billNo = xddh0006DataReqDto.getBillNo();//借据号
        String repayType = xddh0006DataReqDto.getRepayType();//还款模式
        BigDecimal advRepaymentAmt = xddh0006DataReqDto.getAdvRepaymentAmt();//提前还本金额
        // 响应Data：提前还款试算查询
        BigDecimal recTotlCap = null;//应还总本金
        BigDecimal recNormalInt = null;//应还正常利息
        BigDecimal recCi = null;//应还复利
        BigDecimal recOdint = null;//应还罚息
        BigDecimal advRepayInt = null;//提前还款利息
        try {
            Ln3111ReqDto ln3111ReqDto = ln3111Service.buildLn3111ReqDto(xddh0006DataReqDto);
            Ln3111RespDto ln3111RespDto = ln3111Service.ln3111(ln3111ReqDto);
            BigDecimal guihzcbj = ln3111RespDto.getGuihzcbj();// 归还正常本金
            BigDecimal guihyqbj = ln3111RespDto.getGuihyqbj();// 归还逾期本金
            BigDecimal guihdzbj = ln3111RespDto.getGuihdzbj();// 归还呆滞本金
            BigDecimal ghdzhabj = ln3111RespDto.getGhdzhabj();// 归还呆账本金
            recTotlCap = guihzcbj.add(guihyqbj).add(guihdzbj).add(ghdzhabj);// 应还总本金
            advRepaymentAmt = BigDecimal.ZERO;//提前还本金额
            // 还款方式为归还拖欠时提前还本金额为0,其他还款方式时提前还本金额等于应还总本金
            if (Objects.equals(NewCoreEnum.LN3111_HUANKZLE_2.key, repayType)) {
                advRepaymentAmt = BigDecimal.ZERO;
            } else {
                advRepaymentAmt = recTotlCap;
            }
            BigDecimal ghysyjlx = ln3111RespDto.getGhysyjlx();// 归还应收应计利息
            BigDecimal ghcsyjlx = ln3111RespDto.getGhcsyjlx();// 归还催收应计利息
            BigDecimal ghynshqx = ln3111RespDto.getGhynshqx();// 归还应收欠息
            BigDecimal ghcushqx = ln3111RespDto.getGhcushqx();// 归还催收欠息
            recNormalInt = ghysyjlx.add(ghcsyjlx).add(ghynshqx).add(ghcushqx);// 应归还正常利息

            BigDecimal ghyjfuxi = ln3111RespDto.getGhyjfuxi();// 归还应计复息
            BigDecimal ghfxfuxi = ln3111RespDto.getGhfxfuxi();// 归还复息
            recCi = ghyjfuxi.add(ghfxfuxi);//应还复利

            BigDecimal ghysyjfx = ln3111RespDto.getGhysyjfx();// 归还应收应计罚息
            BigDecimal ghcsyjfx = ln3111RespDto.getGhcsyjfx();// 归还催收应计罚息
            BigDecimal ghynshfx = ln3111RespDto.getGhynshfx();// 归还应收罚息
            BigDecimal ghcushfx = ln3111RespDto.getGhcushfx();// 归还催收罚息
            recOdint = ghysyjfx.add(ghcsyjfx).add(ghynshfx).add(ghcushfx);// 应还罚息
            // 当贷款归还试算种类为结清贷款或者提前还款时,主动还款正常利息等于应归还正常利息
            if (Objects.equals(NewCoreEnum.LN3111_HUANKZLE_1.key, repayType) || Objects.equals(NewCoreEnum.LN3111_HUANKZLE_3.key, repayType)) {
                advRepayInt = recNormalInt;
            }
            xddh0006DataRespDto.setAdvRepaymentAmt(advRepaymentAmt);// 提前还本金额
            xddh0006DataRespDto.setRecTotlCap(recTotlCap);// 应还总本金
            xddh0006DataRespDto.setRecNormalInt(recNormalInt);// 应还正常利息
            xddh0006DataRespDto.setRecCi(recCi);// 应还复利
            xddh0006DataRespDto.setRecOdint(recOdint);// 应还罚息
            xddh0006DataRespDto.setAdvRepayInt(advRepayInt);// 提前还款利息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, e.getMessage());
            String ln3111Meesage = e.getMessage();
            // 此处对返回信息做单独的处理
            if (ln3111Meesage.contains("Loan.E0159") && ("NM").equals(repayType)) { //新核心Loan.E0159错误码代表 【该借据不存在欠款】 此时不抛错 接口字段返回固定值0
                recTotlCap = BigDecimal.ZERO;//应还总本金
                recNormalInt = BigDecimal.ZERO;//应还正常利息
                recCi = BigDecimal.ZERO;//应还复利
                recOdint = BigDecimal.ZERO;//应还罚息
                advRepayInt = BigDecimal.ZERO;//提前还款利息
                xddh0006DataRespDto.setAdvRepaymentAmt(advRepaymentAmt);// 提前还本金额
                xddh0006DataRespDto.setRecTotlCap(recTotlCap);// 应还总本金
                xddh0006DataRespDto.setRecNormalInt(recNormalInt);// 应还正常利息
                xddh0006DataRespDto.setRecCi(recCi);// 应还复利
                xddh0006DataRespDto.setRecOdint(recOdint);// 应还罚息
                xddh0006DataRespDto.setAdvRepayInt(advRepayInt);// 提前还款利息
                return xddh0006DataRespDto;
            } else if (ln3111Meesage.contains("Loan.E0000") && ln3111Meesage.contains("贷款账户状态为[销户]，不允许进行试算，请检查！")) { //[贷款账户状态为[销户]，不允许进行试算，请检查！][新核心系统] 销户即为已结清,故进行抛错处理在直销进行展示提示客户
                // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, "借据编号为[" + billNo + "]的借据已结清!");//借据编号为[" + billNo + "]的借据已结清!
            }
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value);
        return xddh0006DataRespDto;
    }
}
