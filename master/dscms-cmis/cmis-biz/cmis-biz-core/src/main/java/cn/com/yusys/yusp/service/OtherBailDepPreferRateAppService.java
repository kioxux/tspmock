/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.OtherAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.OtherBailDepPreferRateAppCusListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.OtherBailDepPreferRateApp;
import cn.com.yusys.yusp.repository.mapper.OtherBailDepPreferRateAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherBailDepPreferRateAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xiaomei
 * @创建时间: 2021-06-07 17:47:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherBailDepPreferRateAppService {
    private static final Logger log = LoggerFactory.getLogger(OtherBailDepPreferRateAppService.class);

    @Autowired
    private OtherBailDepPreferRateAppMapper otherBailDepPreferRateAppMapper;

    @Autowired
    private OtherBailDepPreferRateAppCusListMapper otherBailDepPreferRateAppCusListMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public OtherBailDepPreferRateApp selectByPrimaryKey(String serno) {
        return otherBailDepPreferRateAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherBailDepPreferRateApp> selectAll(QueryModel model) {
        List<OtherBailDepPreferRateApp> records = (List<OtherBailDepPreferRateApp>) otherBailDepPreferRateAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<OtherBailDepPreferRateApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(OtherBailDepPreferRateApp record) {
        return otherBailDepPreferRateAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(OtherBailDepPreferRateApp record) {
        return otherBailDepPreferRateAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(OtherBailDepPreferRateApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherBailDepPreferRateAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(OtherBailDepPreferRateApp record) {
        return otherBailDepPreferRateAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherBailDepPreferRateAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherBailDepPreferRateAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteInfo(String serno) {
        int j = otherBailDepPreferRateAppCusListMapper.deleteBySerno(serno);

        OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppMapper.selectByPrimaryKey(serno);
        //操作类型  删除
        otherBailDepPreferRateApp.setOprType(CmisBizConstants.OPR_TYPE_02);
        //转换日期 ASSURE_CERT_CODE
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        User user = SessionUtils.getUserInformation();
        otherBailDepPreferRateApp.setUpdateTime(DateUtils.getCurrDate());
        otherBailDepPreferRateApp.setUpdDate(dateFormat.format(new Date()) );
        otherBailDepPreferRateApp.setUpdId(user.getLoginCode());
        otherBailDepPreferRateApp.setUpdBrId(user.getOrg().getCode());

        int i = otherBailDepPreferRateAppMapper.updateByPrimaryKeySelective(otherBailDepPreferRateApp);
        if (i != 1) {
            throw BizException.error(null, "999999", "删除：保证金存款特惠利率申请请异常");
        }

        return i;
    }

    /**
     * @方法名称: getOtherForRateAppByModel
     * @方法描述: 根据querymodel获取当前客户经理名下所有保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherBailDepPreferRateApp> getOtherBailDepPreferRateAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_000+CmisCommonConstants.COMMON_SPLIT_COMMA
                +CmisCommonConstants.WF_STATUS_111+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_992);
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getOtherForRateAppHis
     * @方法描述: 获取当前客户经理名下所有保证金存款特惠利率申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherBailDepPreferRateApp> getOtherBailDepPreferRateAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss", CmisCommonConstants.WF_STATUS_996
                + CmisCommonConstants.COMMON_SPLIT_COMMA + CmisCommonConstants.WF_STATUS_997
                + CmisCommonConstants.COMMON_SPLIT_COMMA + CmisCommonConstants.WF_STATUS_998);
        List<OtherBailDepPreferRateApp> list = otherBailDepPreferRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: addotherbaildeppreferrateapp
     * @方法描述: 新增保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addotherbaildeppreferrateapp(OtherBailDepPreferRateApp otherbaildeppreferrateapp) {
        {
            OtherBailDepPreferRateApp otherBailDepPreferRateApp = new OtherBailDepPreferRateApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherbaildeppreferrateapp, otherBailDepPreferRateApp);

                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherBailDepPreferRateApp.setOprType(CmisBizConstants.OPR_TYPE_01);
                //审批状态
                otherBailDepPreferRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherBailDepPreferRateApp.setManagerBrId(user.getOrg().getCode());
                otherBailDepPreferRateApp.setManagerId(user.getLoginCode());
                otherBailDepPreferRateApp.setInputId(user.getLoginCode());
                otherBailDepPreferRateApp.setInputBrId(user.getOrg().getCode());
                otherBailDepPreferRateApp.setInputDate(dateFormat.format(new Date()) );
                otherBailDepPreferRateApp.setCreateTime(DateUtils.getCurrDate());
                otherBailDepPreferRateApp.setUpdateTime(DateUtils.getCurrDate());
                otherBailDepPreferRateApp.setUpdDate(dateFormat.format(new Date()) );
                otherBailDepPreferRateApp.setUpdId(user.getLoginCode());
                otherBailDepPreferRateApp.setUpdBrId(user.getOrg().getCode());

                int i = otherBailDepPreferRateAppMapper.insertSelective(otherBailDepPreferRateApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：保证金存款特惠利率申请异常");
                }

            }catch (Exception e) {
                log.error("保证金存款特惠利率新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherBailDepPreferRateApp);

        }
    }

    /**
     * @方法名称: updateotherbaildeppreferrateapp
     * @方法描述: 更新保证金存款特惠利率申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateotherbaildeppreferrateapp(OtherBailDepPreferRateApp otherbaildeppreferrateapp) {
        {
            OtherBailDepPreferRateApp otherBailDepPreferRateApp = new OtherBailDepPreferRateApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherbaildeppreferrateapp, otherBailDepPreferRateApp);

                //补充 克隆缺少数据
                OtherBailDepPreferRateApp oldotherBailDepPreferRateApp = otherBailDepPreferRateAppMapper.selectByPrimaryKey(otherBailDepPreferRateApp.getSerno());
                //放入必填参数 操作类型 新增
                otherBailDepPreferRateApp.setOprType(oldotherBailDepPreferRateApp.getOprType());
                //审批状态
                otherBailDepPreferRateApp.setApproveStatus(oldotherBailDepPreferRateApp.getApproveStatus());
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherBailDepPreferRateApp.setUpdateTime(DateUtils.getCurrDate());
                otherBailDepPreferRateApp.setUpdDate(dateFormat.format(new Date()) );
                otherBailDepPreferRateApp.setUpdId(user.getLoginCode());
                otherBailDepPreferRateApp.setUpdBrId(user.getOrg().getCode());

                int i = otherBailDepPreferRateAppMapper.updateByPrimaryKeySelective(otherBailDepPreferRateApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：保证金存款特惠利率申请异常");
                }

            }catch (Exception e) {
                log.error("保证金存款特惠利率修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherBailDepPreferRateApp);

        }
    }

    /**
     * @方法名称: checkotherbaildeppreferrateapp
     * @方法描述: 校验保证金存款特惠利率申请是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto checkotherbaildeppreferrateapp(OtherBailDepPreferRateApp otherbaildeppreferrateapp) {
        OtherBailDepPreferRateApp otherBailDepPreferRateApp = otherBailDepPreferRateAppMapper.selectByPrimaryKey(otherbaildeppreferrateapp.getSerno());

        if(!CollectionUtils.nonNull(otherBailDepPreferRateApp)){
            throw BizException.error(null, "999999", "保证金存款特惠利率申请不存在");
        }
        return new ResultDto(otherBailDepPreferRateApp);
    }

    /**
     * 更新审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherBailDepPreferRateApp otherBailDepPreferRateApp = new OtherBailDepPreferRateApp();
        otherBailDepPreferRateApp.setSerno(serno);
        otherBailDepPreferRateApp.setApproveStatus(approveStatus);
        return updateSelective(otherBailDepPreferRateApp);
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author macm
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherBailDepPreferRateAppMapper.selectOtherAppDtoDataByParam(map);
    }



}
