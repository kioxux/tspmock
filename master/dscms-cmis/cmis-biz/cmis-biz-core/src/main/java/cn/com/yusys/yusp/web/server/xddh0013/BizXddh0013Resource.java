package cn.com.yusys.yusp.web.server.xddh0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0013.req.Xddh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0013.Xddh0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询提前还款授权信息列表
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "Xddh0013:查询提前还款授权信息列表")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0013Resource.class);

    @Autowired
	private Xddh0013Service xddh0013Service;
    /**
     * 交易码：xddh0013
     * 交易描述：查询提前还款授权信息列表
     *
     * @param xddh0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddh0013:查询提前还款授权信息列表")
    @PostMapping("/xddh0013")
    protected @ResponseBody
    ResultDto<Xddh0013DataRespDto> ddb0013(@Validated @RequestBody Xddh0013DataReqDto xddh0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, JSON.toJSONString(xddh0013DataReqDto));
        Xddh0013DataRespDto xddh0013DataRespDto = new Xddh0013DataRespDto();// 响应Dto:查询提前还款授权信息列表
        ResultDto<Xddh0013DataRespDto> xddh0013DataResultDto = new ResultDto<>();
		// 从xddh0013DataReqDto获取业务值进行业务逻辑处理
        try {
			String pageSize = xddh0013DataReqDto.getPageSize();
			String startPageNum = xddh0013DataReqDto.getStartPageNum();
			//非空校验
			if (StringUtils.isAnyBlank(pageSize,startPageNum)){
                xddh0013DataResultDto.setData(xddh0013DataRespDto);
                xddh0013DataResultDto.setCode(EpbEnum.EPB090009.key);
                xddh0013DataResultDto.setMessage(EpbEnum.EPB090009.value);
                return xddh0013DataResultDto;
			}
			xddh0013DataRespDto = xddh0013Service.xddh0013(xddh0013DataReqDto);
            // 封装xddh0013DataResultDto中正确的返回码和返回信息
            xddh0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, e.getMessage());
            // 封装xddh0013DataResultDto中异常返回码和返回信息
            xddh0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装ddb0013DataRespDto到xddh0013DataResultDto中
        xddh0013DataResultDto.setData(xddh0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, JSON.toJSONString(xddh0013DataResultDto));
        return xddh0013DataResultDto;
    }
}
