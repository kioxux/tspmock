/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.CommonUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.LmtSurveyEnums;
import cn.com.yusys.yusp.dto.XdWhbDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credzb.CredzbService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstMclWhbxd;
import cn.com.yusys.yusp.repository.mapper.CusLstMclWhbxdMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstMclWhbxdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrc
 * @创建时间: 2021-08-23 15:36:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstMclWhbxdService {
    private static final Logger logger = LoggerFactory.getLogger(CusLstMclWhbxdService.class);

    @Autowired
    private CusLstMclWhbxdMapper cusLstMclWhbxdMapper;

    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    @Autowired
    private CredzbService credzbService;

    @Autowired
    private Fbxw01Service fbxw01Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: @param serno @param billNo
     * @算法描述: 无
     */

    public CusLstMclWhbxd selectByPrimaryKey(String serno, String billNo) {
        return cusLstMclWhbxdMapper.selectByPrimaryKey(serno, billNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: model
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstMclWhbxd> selectAll(QueryModel model) {
        List<CusLstMclWhbxd> records = (List<CusLstMclWhbxd>) cusLstMclWhbxdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstMclWhbxd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstMclWhbxd> list = cusLstMclWhbxdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstMclWhbxd record) {
        return cusLstMclWhbxdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstMclWhbxd record) {
        return cusLstMclWhbxdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstMclWhbxd record) {
        return cusLstMclWhbxdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstMclWhbxd record) {
        return cusLstMclWhbxdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno, String billNo) {
        return cusLstMclWhbxdMapper.deleteByPrimaryKey(serno, billNo);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 新增操作
     * @param cusLstMclWhbxd
     * @return
     */
    public int insertCusLstMclWhbxd(CusLstMclWhbxd cusLstMclWhbxd) {
        cusLstMclWhbxd.setInputDate(DateUtils.getCurrDateStr());
        cusLstMclWhbxd.setCreateTime(DateUtils.getCurrDate());
        // 处理状态 0-失效 1-生效 2-暂存
        cusLstMclWhbxd.setStatus("1");
        cusLstMclWhbxd.setApplyStatus("5");
        return cusLstMclWhbxdMapper.insertSelective(cusLstMclWhbxd);
    }

    /**
     * @方法名称: credit
     * @方法描述: 查询预授信信息
     * @参数与返回说明:
     * @算法描述: 预授信功能
     * @作者：王玉坤
     */
    public ResultDto<Boolean> credit(CusLstMclWhbxd cusLstMclWhbxd) {
        // 结果标志位
        Boolean flag = false;
        ResultDto<Boolean> resultDto = new ResultDto<>();


        // 1、查询30天内是否有征信报告
        // 获取系统时间
        logger.info("调用征信查询接口开始，业务流水号【{}】", cusLstMclWhbxd.getSerno());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date nowDay = DateUtils.parseDate(openDay, "yyyy-MM-dd");
        // 30天前
        Date oldDay = DateUtils.addDay(nowDay, -30);
        // 调用通用接口
        CredzbRespDto credzbRespDto = null;
        CredzbReqDto credzbReqDto = new CredzbReqDto();
        credzbReqDto.setRuleCode("R003");
        credzbReqDto.setReqId(cusLstMclWhbxd.getSerno());
        credzbReqDto.setBrchno(cusLstMclWhbxd.getManagerBrId());
        credzbReqDto.setCertificateNum(cusLstMclWhbxd.getCertCode());
        credzbReqDto.setCustomName(cusLstMclWhbxd.getCusName());
        credzbReqDto.setStartDate(DateUtils.formatDate(oldDay, "yyyy-MM-dd"));
        credzbReqDto.setEndDate(openDay);
        try {
            credzbRespDto = credzbService.credzb(credzbReqDto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return resultDto.data(flag).code(9999).message(e.getMessage());
        }

        // 结果判断
        if (credzbRespDto != null) {
            logger.info("调用征信查询接口成功，客户【{}】", cusLstMclWhbxd.getCusName());
            if (CommonUtils.nonNull(credzbRespDto.getR003()) && StringUtils.nonBlank(credzbRespDto.getR003().getREPORTID())) {
                flag = true;
            } else {
                return resultDto.data(flag).code(9999).message("调用征信接口成功，不存在30天内有效的征信报告！");
            }
        } else {
            logger.info("征信返回数据异常！");
            return resultDto.data(flag).code(9999).message("征信返回数据异常！");
        }
        logger.info("调用征信查询接口结束，业务流水号【{}】", cusLstMclWhbxd.getSerno());

        // 2、调用预授信接口开始
        logger.info("调用授信申请接口开始，业务流水号【{}】", cusLstMclWhbxd.getSerno());
        Fbxw01ReqDto reqDto = new Fbxw01ReqDto();
        // 渠道来源  11
        reqDto.setChannel_type("11");
        // 合作平台  1002
        reqDto.setCo_platform("1002");
        // 产品类别  000015
        reqDto.setPrd_type("000015");
        // 产品代码（零售智能风控内部代码）   1002000015
        reqDto.setPrd_code("1002000015");
        // 操作类型  01 一级准入  02 二级准入
        reqDto.setOp_flag("01");
        // 业务唯一编号
        reqDto.setApply_no(cusLstMclWhbxd.getSerno());
        // 证件类型  10 身份证
        reqDto.setCert_type("10");
        // 证件号码
        reqDto.setCert_code(cusLstMclWhbxd.getCertCode());
        // 客户姓名
        reqDto.setCust_name(cusLstMclWhbxd.getCusName());
        // 移动电话
        reqDto.setPhone(cusLstMclWhbxd.getLinkMode());
        // 核心客户号
        reqDto.setCust_id_core(cusLstMclWhbxd.getCusId());
        // 原借据编号 一级准入必输
        reqDto.setOld_bill_no(cusLstMclWhbxd.getBillNo());
        // 客户经理号
        reqDto.setBiz_manager_id(cusLstMclWhbxd.getManagerId());
        // 归属机构
        reqDto.setBiz_org_id(cusLstMclWhbxd.getManagerBrId());

        // 调用零售智能风控预授信功能开始
        Fbxw01RespDto respDto = null;
        try {
            respDto = fbxw01Service.fbxw01(reqDto);
            flag = true;
        } catch (Exception e) {
            logger.error("调用预授信接口失败！", e);
            return resultDto.data(flag).code(9999).message("调用零售智能风控预授信接口异常，异常信息:".concat(e.getMessage()));
        }

        // 更新名单状态为办理中 1--办理中
        CusLstMclWhbxd cusLstMclWhbxdTemp = this.selectByPrimaryKey(cusLstMclWhbxd.getSerno(),cusLstMclWhbxd.getBillNo());
        cusLstMclWhbxdTemp.setApplyStatus("1");
        this.update(cusLstMclWhbxdTemp);
        logger.info("调用授信申请接口结束，业务流水号【{}】", cusLstMclWhbxd.getSerno());
        return resultDto.data(flag).message("调用零售智能风控预授信接口成功！");
    }

    /**
     * @方法名称: getXdWhbInfo
     * @方法描述: 获取无还本续贷借据信息
     * @参数与返回说明: List<XdWhbDto>
     * @算法描述: 无
     * @作者：尚志勇
     */
    public List<XdWhbDto> getXdWhbInfo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<XdWhbDto> list = cusLstMclWhbxdMapper.getXdWhbInfo(queryModel);
        PageHelper.clearPage();
        return list;
    }
}
