/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydLoan
 * @类描述: tmp_wyd_loan数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_loan_tmp")
public class TmpWydLoanTmp {
	
	/** 借据号 **/
	@Id
	@Column(name = "LENDING_REF")
	private String lendingRef;
	
	/** 合作机构号 **/
	@Column(name = "OPER_ORG", unique = false, nullable = true, length = 10)
	private String operOrg;
	
	/** 借款合同号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 40)
	private String contractNo;
	
	/** 流水号 **/
	@Column(name = "SEQNO", unique = false, nullable = true, length = 32)
	private String seqno;
	
	/** 客户姓名 **/
	@Column(name = "FULLNAME", unique = false, nullable = true, length = 128)
	private String fullname;
	
	/** 贷款期限 **/
	@Column(name = "TERMDATE", unique = false, nullable = true, length = 4)
	private String termdate;
	
	/** 起始日期 **/
	@Column(name = "PUTOUTDATE", unique = false, nullable = true, length = 8)
	private String putoutdate;
	
	/** 到期日期 **/
	@Column(name = "MATURITY", unique = false, nullable = true, length = 8)
	private String maturity;
	
	/** 期限月 **/
	@Column(name = "TREMMONTH", unique = false, nullable = true, length = 4)
	private String tremmonth;
	
	/** 币种 **/
	@Column(name = "BUSINESSCURRENCY", unique = false, nullable = true, length = 3)
	private String businesscurrency;
	
	/** 贷款金额 **/
	@Column(name = "BUSINESSSUM", unique = false, nullable = true, length = 16)
	private String businesssum;
	
	/** 贷款余额 **/
	@Column(name = "BALANCE", unique = false, nullable = true, length = 16)
	private String balance;
	
	/** 贷款利率 **/
	@Column(name = "BUSINESSRATE", unique = false, nullable = true, length = 16)
	private String businessrate;
	
	/** 逾期利率 **/
	@Column(name = "OVERDUEFINE", unique = false, nullable = true, length = 16)
	private String overduefine;
	
	/** 起息日 **/
	@Column(name = "STARTINTERESTDATE", unique = false, nullable = true, length = 8)
	private String startinterestdate;
	
	/** 还款日 **/
	@Column(name = "PAYDAY", unique = false, nullable = true, length = 2)
	private String payday;
	
	/** 处理标志 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 10)
	private String status;
	
	/** 贷款状态 **/
	@Column(name = "LOANSTATUS", unique = false, nullable = true, length = 10)
	private String loanstatus;
	
	/** 资产转让状态 **/
	@Column(name = "ABSSTATUS", unique = false, nullable = true, length = 10)
	private String absstatus;
	
	/** 项目编号 **/
	@Column(name = "PROJECT_ID", unique = false, nullable = true, length = 20)
	private String projectId;
	
	/** 还款方式 **/
	@Column(name = "CORPUSPAYMETHOD", unique = false, nullable = true, length = 20)
	private String corpuspaymethod;
	
	/** 贷款还款账号 **/
	@Column(name = "PAY_ACCT_NO", unique = false, nullable = true, length = 40)
	private String payAcctNo;
	
	/** 贷款还款账号名称 **/
	@Column(name = "PAY_ACCT_NO_NAME", unique = false, nullable = true, length = 100)
	private String payAcctNoName;
	
	/** 贷款还款行号 **/
	@Column(name = "PAY_ACCT_BANK_NO", unique = false, nullable = true, length = 40)
	private String payAcctBankNo;
	
	/** 贷款还款行名 **/
	@Column(name = "PAY_ACCT_BANK", unique = false, nullable = true, length = 100)
	private String payAcctBank;
	
	/** 贷款入账账号 **/
	@Column(name = "IN_ACCT_NO", unique = false, nullable = true, length = 40)
	private String inAcctNo;
	
	/** 贷款入账行名 **/
	@Column(name = "IN_ACCT_NO_NAME", unique = false, nullable = true, length = 100)
	private String inAcctNoName;
	
	/** 贷款入账行号 **/
	@Column(name = "IN_ACCT_BANK_NO", unique = false, nullable = true, length = 40)
	private String inAcctBankNo;
	
	/** 贷款入账行名 **/
	@Column(name = "IN_ACCT_BANK", unique = false, nullable = true, length = 100)
	private String inAcctBank;
	
	/** 保证金帐号 **/
	@Column(name = "BOND_ACCT_NO", unique = false, nullable = true, length = 40)
	private String bondAcctNo;
	
	/** 客户类型 **/
	@Column(name = "TYPE_OF_CUST", unique = false, nullable = true, length = 20)
	private String typeOfCust;
	
	/** 短期中长期的标识 **/
	@Column(name = "TERMFLAG", unique = false, nullable = true, length = 2)
	private String termflag;
	
	/** lpr基准利率 **/
	@Column(name = "LPRBASERATE", unique = false, nullable = true, length = 16)
	private String lprbaserate;
	
	
	/**
	 * @param operOrg
	 */
	public void setOperOrg(String operOrg) {
		this.operOrg = operOrg;
	}
	
    /**
     * @return operOrg
     */
	public String getOperOrg() {
		return this.operOrg;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param seqno
	 */
	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}
	
    /**
     * @return seqno
     */
	public String getSeqno() {
		return this.seqno;
	}
	
	/**
	 * @param fullname
	 */
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
    /**
     * @return fullname
     */
	public String getFullname() {
		return this.fullname;
	}
	
	/**
	 * @param termdate
	 */
	public void setTermdate(String termdate) {
		this.termdate = termdate;
	}
	
    /**
     * @return termdate
     */
	public String getTermdate() {
		return this.termdate;
	}
	
	/**
	 * @param putoutdate
	 */
	public void setPutoutdate(String putoutdate) {
		this.putoutdate = putoutdate;
	}
	
    /**
     * @return putoutdate
     */
	public String getPutoutdate() {
		return this.putoutdate;
	}
	
	/**
	 * @param maturity
	 */
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	
    /**
     * @return maturity
     */
	public String getMaturity() {
		return this.maturity;
	}
	
	/**
	 * @param tremmonth
	 */
	public void setTremmonth(String tremmonth) {
		this.tremmonth = tremmonth;
	}
	
    /**
     * @return tremmonth
     */
	public String getTremmonth() {
		return this.tremmonth;
	}
	
	/**
	 * @param businesscurrency
	 */
	public void setBusinesscurrency(String businesscurrency) {
		this.businesscurrency = businesscurrency;
	}
	
    /**
     * @return businesscurrency
     */
	public String getBusinesscurrency() {
		return this.businesscurrency;
	}
	
	/**
	 * @param businesssum
	 */
	public void setBusinesssum(String businesssum) {
		this.businesssum = businesssum;
	}
	
    /**
     * @return businesssum
     */
	public String getBusinesssum() {
		return this.businesssum;
	}
	
	/**
	 * @param balance
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
    /**
     * @return balance
     */
	public String getBalance() {
		return this.balance;
	}
	
	/**
	 * @param businessrate
	 */
	public void setBusinessrate(String businessrate) {
		this.businessrate = businessrate;
	}
	
    /**
     * @return businessrate
     */
	public String getBusinessrate() {
		return this.businessrate;
	}
	
	/**
	 * @param overduefine
	 */
	public void setOverduefine(String overduefine) {
		this.overduefine = overduefine;
	}
	
    /**
     * @return overduefine
     */
	public String getOverduefine() {
		return this.overduefine;
	}
	
	/**
	 * @param startinterestdate
	 */
	public void setStartinterestdate(String startinterestdate) {
		this.startinterestdate = startinterestdate;
	}
	
    /**
     * @return startinterestdate
     */
	public String getStartinterestdate() {
		return this.startinterestdate;
	}
	
	/**
	 * @param payday
	 */
	public void setPayday(String payday) {
		this.payday = payday;
	}
	
    /**
     * @return payday
     */
	public String getPayday() {
		return this.payday;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param loanstatus
	 */
	public void setLoanstatus(String loanstatus) {
		this.loanstatus = loanstatus;
	}
	
    /**
     * @return loanstatus
     */
	public String getLoanstatus() {
		return this.loanstatus;
	}
	
	/**
	 * @param absstatus
	 */
	public void setAbsstatus(String absstatus) {
		this.absstatus = absstatus;
	}
	
    /**
     * @return absstatus
     */
	public String getAbsstatus() {
		return this.absstatus;
	}
	
	/**
	 * @param projectId
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
    /**
     * @return projectId
     */
	public String getProjectId() {
		return this.projectId;
	}
	
	/**
	 * @param corpuspaymethod
	 */
	public void setCorpuspaymethod(String corpuspaymethod) {
		this.corpuspaymethod = corpuspaymethod;
	}
	
    /**
     * @return corpuspaymethod
     */
	public String getCorpuspaymethod() {
		return this.corpuspaymethod;
	}
	
	/**
	 * @param payAcctNo
	 */
	public void setPayAcctNo(String payAcctNo) {
		this.payAcctNo = payAcctNo;
	}
	
    /**
     * @return payAcctNo
     */
	public String getPayAcctNo() {
		return this.payAcctNo;
	}
	
	/**
	 * @param payAcctNoName
	 */
	public void setPayAcctNoName(String payAcctNoName) {
		this.payAcctNoName = payAcctNoName;
	}
	
    /**
     * @return payAcctNoName
     */
	public String getPayAcctNoName() {
		return this.payAcctNoName;
	}
	
	/**
	 * @param payAcctBankNo
	 */
	public void setPayAcctBankNo(String payAcctBankNo) {
		this.payAcctBankNo = payAcctBankNo;
	}
	
    /**
     * @return payAcctBankNo
     */
	public String getPayAcctBankNo() {
		return this.payAcctBankNo;
	}
	
	/**
	 * @param payAcctBank
	 */
	public void setPayAcctBank(String payAcctBank) {
		this.payAcctBank = payAcctBank;
	}
	
    /**
     * @return payAcctBank
     */
	public String getPayAcctBank() {
		return this.payAcctBank;
	}
	
	/**
	 * @param inAcctNo
	 */
	public void setInAcctNo(String inAcctNo) {
		this.inAcctNo = inAcctNo;
	}
	
    /**
     * @return inAcctNo
     */
	public String getInAcctNo() {
		return this.inAcctNo;
	}
	
	/**
	 * @param inAcctNoName
	 */
	public void setInAcctNoName(String inAcctNoName) {
		this.inAcctNoName = inAcctNoName;
	}
	
    /**
     * @return inAcctNoName
     */
	public String getInAcctNoName() {
		return this.inAcctNoName;
	}
	
	/**
	 * @param inAcctBankNo
	 */
	public void setInAcctBankNo(String inAcctBankNo) {
		this.inAcctBankNo = inAcctBankNo;
	}
	
    /**
     * @return inAcctBankNo
     */
	public String getInAcctBankNo() {
		return this.inAcctBankNo;
	}
	
	/**
	 * @param inAcctBank
	 */
	public void setInAcctBank(String inAcctBank) {
		this.inAcctBank = inAcctBank;
	}
	
    /**
     * @return inAcctBank
     */
	public String getInAcctBank() {
		return this.inAcctBank;
	}
	
	/**
	 * @param bondAcctNo
	 */
	public void setBondAcctNo(String bondAcctNo) {
		this.bondAcctNo = bondAcctNo;
	}
	
    /**
     * @return bondAcctNo
     */
	public String getBondAcctNo() {
		return this.bondAcctNo;
	}
	
	/**
	 * @param typeOfCust
	 */
	public void setTypeOfCust(String typeOfCust) {
		this.typeOfCust = typeOfCust;
	}
	
    /**
     * @return typeOfCust
     */
	public String getTypeOfCust() {
		return this.typeOfCust;
	}
	
	/**
	 * @param termflag
	 */
	public void setTermflag(String termflag) {
		this.termflag = termflag;
	}
	
    /**
     * @return termflag
     */
	public String getTermflag() {
		return this.termflag;
	}
	
	/**
	 * @param lprbaserate
	 */
	public void setLprbaserate(String lprbaserate) {
		this.lprbaserate = lprbaserate;
	}
	
    /**
     * @return lprbaserate
     */
	public String getLprbaserate() {
		return this.lprbaserate;
	}


}