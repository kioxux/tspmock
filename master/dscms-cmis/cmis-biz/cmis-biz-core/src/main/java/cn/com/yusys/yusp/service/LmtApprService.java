/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.template.FileSystemTemplate;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtApproveSubDto;
import cn.com.yusys.yusp.dto.LmtApproveSubPrdDto;
import cn.com.yusys.yusp.dto.LmtReplySubDto;
import cn.com.yusys.yusp.dto.LmtReplySubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtApprMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-09 10:54:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtApprService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtApprService.class);

    @Resource
    private LmtApprMapper lmtApprMapper;

    // 单一客户授信申请
    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    // 单一客户授信审批
    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;


    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;


    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Resource(name="sftpFileSystemTemplate")
    public FileSystemTemplate sftpFileSystemTemplate;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppr selectByPrimaryKey(String pkId) {
        return lmtApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppr> selectAll(QueryModel model) {
        List<LmtAppr> records = (List<LmtAppr>) lmtApprMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppr> list = lmtApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppr record) {
        return lmtApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppr record) {
        return lmtApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAppr record) {
        return lmtApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppr record) {
        return lmtApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtApprMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectLmtApprByParams
     * @方法描述: 通过流水号查询最终的授信审批数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public LmtAppr queryFinalLmtApprBySerno(String serno) {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("serno", serno);
        // 录入时间倒序排序
        queryMap.put("sort", "input_date desc ");
        List<LmtAppr> lmtApprList = selectLmtApprByParams(queryMap);
        LmtAppr lmtAppr = null;
        if (lmtApprList != null && lmtApprList.size() > 0){
            lmtAppr = lmtApprList.get(0);
        }
        return lmtAppr;
    }

    /**
     * @方法名称: queryFinalLmtApprByApproveSerno
     * @方法描述: 通过审批流水号查询授信审批数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public LmtAppr queryFinalLmtApprByApproveSerno(String approveSerno)throws Exception{
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("approveSerno", approveSerno);
        List<LmtAppr> lmtApprList = selectLmtApprByParams(queryMap);
        LmtAppr lmtAppr = null;
        if (lmtApprList != null && lmtApprList.size() == 1){
            lmtAppr = lmtApprList.get(0);
        }else{
            throw new Exception("根据审批流水号查询审批数据异常");
        }
        return lmtAppr;
    }

    /**
     * @方法名称: selectLmtApprByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtAppr> selectLmtApprByParams(HashMap paramsMap){
        return lmtApprMapper.selectLmtApprByParams(paramsMap);
    }

    /**
     * @方法名称: queryGrpLmtApprByGrpSerno
     * @方法描述: 查询集团成员的审批数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtAppr> queryGrpLmtApprByGrpSerno(String grpSerno){
        return lmtApprMapper.queryGrpLmtApprByGrpSerno(grpSerno);
    }

    /**
     * @方法名称:
     * @方法描述: 生成审批表信息
     * @参数与返回说明: 接受授信申请流水号，返回授信审批流水号
     * @算法描述:
     * 1.通过授信申请流水号查询最新的一笔授信审批（如果查不到查询授信申请）
     * 2.将查询到的数据插入授信审批表（如果是从审批中查询数据，则条件信息也重新插入）
     * 3.返回授信审批流水号
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     * @修改记录： 2021-09-02 zhangliang15 外部系统调信贷服务端接口时session中userInfo为空
     */
    @Transactional
    public String generateLmtApprInfo(String serno, String approveStatus) throws Exception {
        String currentUserId = "";//当前登录人
        String currentOrgId = "";//当前登陆机构
        LmtAppr newLmtAppr = new LmtAppr();
        // 1.通过授信申请流水号查询最新的一笔授信审批（如果查不到查询授信申请）
        LmtAppr lmtAppr = queryFinalLmtApprBySerno(serno);
        User userInfo = SessionUtils.getUserInformation();
        // 当userinfo为空时,获取当前登录人code和登录机构code为system
        if(userInfo == null || StringUtils.isBlank(userInfo.getLoginCode())){
            currentUserId = "system";
            currentOrgId = "system";
        }else {
            // 当userinfo不为空时,获取当前登录人code和登录机构code
            currentUserId = userInfo.getLoginCode();
            currentOrgId = userInfo.getOrg().getCode();
        }
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        log.info("当前单一授信申请表数据:"+JSON.toJSONString(lmtApp));
        // 如果审批表没数据  或者是打回提交 或者是再议重新发起流程，重新从申请表拉数据
        if(lmtAppr == null || StringUtils.isBlank(lmtAppr.getPkId()) || CmisFlowConstants.WF_STATUS_992.equals(approveStatus) || (CmisCommonConstants.LMT_TYPE_06.equals(lmtApp.getLmtType()) && CmisFlowConstants.WF_STATUS_000.equals(approveStatus))){
            if(lmtApp != null && StringUtils.nonBlank(lmtApp.getSerno())){
                log.info("单一授信数据生成审批表信息，查询数据流水号为【{}】,是否为集团授信数据【{}】", lmtApp.getSerno(), lmtApp.getIsGrp());
                // 通过授信申请生成新的授信审批信息
                newLmtAppr = generateLmtApprByLmtApp(currentUserId, currentOrgId, lmtApp);
                if(newLmtAppr != null && newLmtAppr.getApproveSerno() != null){
                    // 通过授信申请分项生成新的授信审批分项
                    lmtApprSubService.generateLmtApprSubByLmtApp(currentUserId, currentOrgId, lmtApp, newLmtAppr.getApproveSerno());
                }else{
                    return null;
                }
            }else{
                log.info("单一授信数据生成审批表信息，查询数据为空");
                // 集团的续作时候，如果成员客户是新纳进客户，而且不参与本次填报的话，是没有申请数据，
                // generateLmtGrpMemRelForLmtGrpApprByLmtGrpApp方法中关联表中没办法判断不参与填报的数据到底是存量的数据还是新增的数据
                // 所以会出现lmtApp为空的情况，这种情况不生成该客户的审批数据
                return null;
            }
        }else{
            newLmtAppr = generateLmtApprByLmtAppr(newLmtAppr, lmtAppr, currentUserId, currentOrgId);
            // 通过授信审批分项生成新的授信审批分项
            lmtApprSubService.generateLmtApprSubByLmtAppr(newLmtAppr, lmtAppr, currentUserId, currentOrgId);
            // 通过授信审批用信条件生成新的授信审批用信条件
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(newLmtAppr, lmtAppr);
        }
        return newLmtAppr.getApproveSerno();
    }

    /**
     * @方法名称:
     * @方法描述: 通过授信申请信息生成授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtAppr generateLmtApprByLmtAppr(LmtAppr newLmtAppr, LmtAppr lmtAppr, String currentUserId, String currentOrgId) {
        // 直接用原批复复制到新的批复数据
        BeanUtils.copyProperties(lmtAppr, newLmtAppr);
        newLmtAppr.setPkId(UUID.randomUUID().toString());
        newLmtAppr.setApproveSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>()));
        newLmtAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtAppr.setUpdId(currentUserId);
        newLmtAppr.setUpdBrId(currentOrgId);
        newLmtAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        log.info("根据原成员客户审批表数据,生成一笔新的成员客户审批表数据,新成员客户审批表审批流水号为:"+newLmtAppr.getApproveSerno());
        this.insert(newLmtAppr);
        return newLmtAppr;
    }

    /**
     * @方法名称:
     * @方法描述: 通过授信申请信息生成授信审批数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public LmtAppr generateLmtApprByLmtApp(String currentUserId, String currentOrgId, LmtApp lmtApp) throws Exception {
        LmtAppr newLmtAppr = new LmtAppr();
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(lmtApp.getIsGrp())){
            LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtApp.getSerno());
            // 如果是集团授信的情况下，不参与本次填报并且原批复流水号为空，则返回空
            if(CmisCommonConstants.STD_ZB_YES_NO_0.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare()) && lmtApp.getOrigiLmtReplySerno() == null){
                return null;
            }
        }
        BeanUtils.copyProperties(lmtApp, newLmtAppr);
        newLmtAppr.setPkId(UUID.randomUUID().toString());
        newLmtAppr.setApproveSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>()));
        newLmtAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtAppr.setUpdId(currentUserId);
        newLmtAppr.setUpdBrId(currentOrgId);
        newLmtAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtAppr.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtAppr.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        // 计算审批模式
        newLmtAppr.setApprMode(lmtAppService.getApprMode(lmtApp.getSerno()));
        newLmtAppr.setPerGurTerm(0);
        this.insert(newLmtAppr);
        return newLmtAppr;
    }

    /**
     * @方法名称:
     * @方法描述: 通过审批流水号生成审批表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     * @修改记录： 2021-09-02 zhangliang15 外部系统调信贷服务端接口时session中userInfo为空
     */
    @Transactional
    public String generateLmtApprInfoByLmtAppr(String apprSerno) throws Exception {
        String currentUserId = "";//当前登录人
        String currentOrgId = "";//当前登陆机构
        LmtAppr newLmtAppr = new LmtAppr();
        // 1.通过授信申请流水号查询最新的一笔授信审批（如果查不到查询授信申请）
        log.info("通过成员客户授信申请流水号查询最新的一笔授信审批表数据,成员客户授信申请审批表审批流水号为:"+apprSerno);
        LmtAppr lmtAppr = queryFinalLmtApprByApproveSerno(apprSerno);
        User userInfo = SessionUtils.getUserInformation();
        // 当userinfo为空时,获取当前登录人code和登录机构code为system
        if(userInfo == null || StringUtils.isBlank(userInfo.getLoginCode())){
            currentUserId = "system";
            currentOrgId = "system";
        }else {
            // 当userinfo不为空时,获取当前登录人code和登录机构code
            currentUserId = userInfo.getLoginCode();
            currentOrgId = userInfo.getOrg().getCode();
        }
        if(lmtAppr != null && !StringUtils.isBlank(lmtAppr.getPkId())){
            newLmtAppr = generateLmtApprByLmtAppr(newLmtAppr, lmtAppr, currentUserId, currentOrgId);
            // 通过授信审批分项生成新的授信审批分项
            lmtApprSubService.generateLmtApprSubByLmtAppr(newLmtAppr, lmtAppr, currentUserId, currentOrgId);
            // 通过授信审批用信条件生成新的授信审批用信条件
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(newLmtAppr, lmtAppr);
        }else{
            throw new Exception("查询单一客户批复审批数据异常");
        }
        return newLmtAppr.getApproveSerno();
    }
    /**
     * @方法名称：getAllApproveInfo
     * @方法描述：获取批复所有信息包括分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-5-13 下午 9:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map getAllApproveInfo(String apprSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtApprLoanCond> lmtApprLoanConds = null;
        List<LmtApproveSubDto> lmtApproveSubDtoList = null;
        try {
            if (StringUtils.isBlank(apprSerno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("apprSerno",apprSerno);
            queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
            lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByParams(queryMap);
            List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
            log.info("查询审批中的成员分项信息:"+ JSON.toJSONString(approveSubList));
            if (approveSubList != null) {
                lmtApproveSubDtoList = (List<LmtApproveSubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubList, LmtApproveSubDto.class);
                lmtApproveSubDtoList.forEach(lmtApproveSubDto  -> {
                    String apprSubSerno = lmtApproveSubDto.getApproveSubSerno();
                    String subName = lmtApproveSubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtApproveSubDto.setLmtDrawType(subName);
                    }
                    // 分项层 为空
                    lmtApproveSubDto.setLmtTerm(null);
                    if (!StringUtils.isBlank(apprSubSerno)) {
                        lmtApproveSubDto.setLmtDrawNo(lmtApproveSubDto.getSubSerno());
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("approveSubSerno", lmtApproveSubDto.getApproveSubSerno());
                        queryPrdMap.put("oprType", CmisCommonConstants.OP_TYPE_01);
                        queryPrdMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
                        List<LmtApprSubPrd> approveSubPrdList = lmtApprSubPrdService.queryLmtApproveSubPrdByParams(queryPrdMap);
                        log.info("查询审批中的成员分项信息:"+ JSON.toJSONString(approveSubPrdList));
                        List<LmtApproveSubPrdDto> lmtApproveSubPrdDtos = (List<LmtApproveSubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubPrdList, LmtApproveSubPrdDto.class);
                        List<LmtApproveSubPrdDto> resLmtApproveSubPrdDtos = new ArrayList<>();
                        for (LmtApproveSubPrdDto lmtApproveSubPrdDto : lmtApproveSubPrdDtos) {
                            String bizTypeName = lmtApproveSubPrdDto.getLmtBizTypeName();
                            String subPrdSerno = lmtApproveSubPrdDto.getSubPrdSerno();
                            if (StringUtils.nonBlank(bizTypeName)) {
                                lmtApproveSubPrdDto.setLmtDrawType(bizTypeName);
                            }
                            if (StringUtils.nonBlank(subPrdSerno)) {
                                lmtApproveSubPrdDto.setLmtDrawNo(subPrdSerno);
                            }
                        }
                        lmtApproveSubDto.setChildren(lmtApproveSubPrdDtos);
                    }
                });
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApproveSubDtoList", lmtApproveSubDtoList);
            rtnData.put("lmtApprLoanConds", lmtApprLoanConds);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: selectByApproveSerno
     * @方法描述: 根据授信审批流水号查询授信信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtAppr selectByApproveSerno(String approveSerno) {
        return lmtApprMapper.selectByApproveSerno(approveSerno);
    }




    /**
     * @方法名称：getApproveInfoBySerno
     * @方法描述：获取批复所有信息包括分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-5-13 下午 9:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map getApproveInfoBySerno(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtApprLoanCond> lmtApprLoanConds = null;
        List<LmtApproveSubDto> lmtApproveSubDtoList = null;
        try {
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("oprType", CmisCommonConstants.ADD_OPR);
            queryMap.put("apprSerno",serno);
            queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
            lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByParams(queryMap);
            List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
            log.info("查询审批中的成员分项信息:"+ JSON.toJSONString(approveSubList));
            if (approveSubList != null) {
                lmtApproveSubDtoList = (List<LmtApproveSubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubList, LmtApproveSubDto.class);
                lmtApproveSubDtoList.forEach(lmtApproveSubDto  -> {
                    String apprSubSerno = lmtApproveSubDto.getApproveSubSerno();
                    String subName = lmtApproveSubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtApproveSubDto.setLmtDrawType(subName);
                    }
                    // 分项层 为空
                    lmtApproveSubDto.setLmtTerm(null);
                    if (!StringUtils.isBlank(apprSubSerno)) {
                        lmtApproveSubDto.setLmtDrawNo(apprSubSerno);
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("approveSubSerno", lmtApproveSubDto.getApproveSubSerno());
                        queryPrdMap.put("oprType", CmisCommonConstants.OP_TYPE_01);
                        queryPrdMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
                        List<LmtApprSubPrd> approveSubPrdList = lmtApprSubPrdService.queryLmtApproveSubPrdByParams(queryPrdMap);
                        log.info("查询审批中的成员分项信息:"+ JSON.toJSONString(approveSubPrdList));
                        List<LmtApproveSubPrdDto> lmtApproveSubPrdDtos = (List<LmtApproveSubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubPrdList, LmtApproveSubPrdDto.class);
                        List<LmtApproveSubPrdDto> resLmtApproveSubPrdDtos = new ArrayList<>();
                        for (LmtApproveSubPrdDto lmtApproveSubPrdDto : lmtApproveSubPrdDtos) {
                            String bizTypeName = lmtApproveSubPrdDto.getLmtBizTypeName();
                            String apprSubPrdSerno = lmtApproveSubPrdDto.getApprSubPrdSerno();
                            String subPrdSerno = lmtApproveSubPrdDto.getSubPrdSerno();
                            if (StringUtils.nonBlank(bizTypeName)) {
                                lmtApproveSubPrdDto.setLmtDrawType(bizTypeName);
                            }
                            if (StringUtils.nonBlank(apprSubPrdSerno)) {
                                lmtApproveSubPrdDto.setLmtDrawNo(apprSubPrdSerno);
                            }
                            if (StringUtils.nonBlank(subPrdSerno)) {
                                lmtApproveSubPrdDto.setSubSerno(subPrdSerno);
                            }
                            resLmtApproveSubPrdDtos.add(lmtApproveSubPrdDto);
                        }
                        lmtApproveSubDto.setChildren(resLmtApproveSubPrdDtos);
                    }
                });
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApproveSubDtoList", lmtApproveSubDtoList);
            rtnData.put("lmtApprLoanConds", lmtApprLoanConds);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称：getApproveInfoBySernoAndReportType
     * @方法描述：获取批复所有信息包括分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-10-17 下午 9:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map getApproveInfoBySernoAndReportType(Map map) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtApproveSubDto> lmtApproveSubDtoList = new ArrayList<>();
        try {
            if (StringUtils.isBlank((String) map.get("apprSerno"))) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("apprSerno", (String) map.get("apprSerno"));
            queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
            List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
            if (approveSubList != null) {
                List<LmtApproveSubDto> lmtApproveSubDtoList1 = (List<LmtApproveSubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubList, LmtApproveSubDto.class);
                for(LmtApproveSubDto lmtApproveSubDto : lmtApproveSubDtoList1) {
                    String apprSubSerno = lmtApproveSubDto.getApproveSubSerno();
                    String subName = lmtApproveSubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtApproveSubDto.setLmtDrawType(subName);
                    }
                    // 分项层 为空
                    lmtApproveSubDto.setLmtTerm(null);
                    if (!StringUtils.isBlank(apprSubSerno)) {
                        lmtApproveSubDto.setLmtDrawNo(apprSubSerno);
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("approveSubSerno", lmtApproveSubDto.getApproveSubSerno());
                        queryPrdMap.put("oprType", CmisCommonConstants.OP_TYPE_01);
                        queryPrdMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);
                        List<LmtApprSubPrd> approveSubPrdList = lmtApprSubPrdService.queryLmtApproveSubPrdByParams(queryPrdMap);
                        List<LmtApproveSubPrdDto> lmtApproveSubPrdDtos = (List<LmtApproveSubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubPrdList, LmtApproveSubPrdDto.class);
                        List<LmtApproveSubPrdDto> resLmtApproveSubPrdDtos = new ArrayList<>();
                        for (LmtApproveSubPrdDto lmtApproveSubPrdDto : lmtApproveSubPrdDtos) {
                            String bizTypeName = lmtApproveSubPrdDto.getLmtBizTypeName();
                            String apprSubPrdSerno = lmtApproveSubPrdDto.getApprSubPrdSerno();
                            String subPrdSerno = lmtApproveSubPrdDto.getSubPrdSerno();
                            if (StringUtils.nonBlank(bizTypeName)) {
                                lmtApproveSubPrdDto.setLmtDrawType(bizTypeName);
                            }
                            if (StringUtils.nonBlank(apprSubPrdSerno)) {
                                lmtApproveSubPrdDto.setLmtDrawNo(apprSubPrdSerno);
                            }
                            if (StringUtils.nonBlank(subPrdSerno)) {
                                lmtApproveSubPrdDto.setSubSerno(subPrdSerno);
                            }
                            resLmtApproveSubPrdDtos.add(lmtApproveSubPrdDto);
                        }
                        lmtApproveSubDto.setChildren(resLmtApproveSubPrdDtos);
                    }
                    lmtApproveSubDtoList.add(lmtApproveSubDto);
                };
            }


        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            // rtnData.put("lmtAppr", lmtAppr);
            rtnData.put("lmtApproveSubDtoList", lmtApproveSubDtoList);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 根据申请流水号查询审批数据
     * @param serno
     * @return
     */
    public LmtAppr queryInfoBySerno(String serno) {
        return lmtApprMapper.queryInfoBySerno(serno);
    }

    /**
     * 根据申请流水号查询审批数据
     * @param map
     * @return
     */
    public LmtAppr queryInfoBySernoAndIssueReportType(Map map) {
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        List<LmtAppr> lmtApprs = lmtApprMapper.queryLmtApproveDataByParams(hashMap);
        if(!lmtApprs.isEmpty() && lmtApprs.size()>0){
            return lmtApprs.get(0);
        }else{
            return lmtApprMapper.queryInfoBySerno((String) map.get("serno"));
        }
    }

    /**
     * @函数名称: updateLmtApprChoose
     * @函数描述: 根据流水号更新审批选择条件
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 马顺
     * @创建时间: 2021-07-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int updateLmtApprChoose(Map<String, Object> params) {
        LmtAppr lmtAppr = this.queryFinalLmtApprBySerno((String) params.get("serno"));
        int count = 0;
        if(lmtAppr != null){
            lmtAppr.setIsSubToOtherDeptCom((String) params.get("isSubToOtherDeptCom"));
            lmtAppr.setSubOtherDeptCom((String) params.get("subOtherDeptCom"));
            lmtAppr.setIsSubOtherDeptXd((String) params.get("isSubToOtherDeptXd"));
            lmtAppr.setSubOtherDeptXd((String) params.get("subOtherDeptXd"));
            lmtAppr.setIsUpperApprAuth((String) params.get("isUpperApprAuth"));
            lmtAppr.setUpperApprAuthType((String) params.get("upperApprAuthType"));
            lmtAppr.setIsLowerApprAuth((String) params.get("isLowerApprAuth"));
            lmtAppr.setIsBigLmt((String) params.get("isDAELmtApprWeb"));
            lmtAppr.setApprBackReasonType((String) params.get("apprBackReasonType"));
            count = lmtApprMapper.updateByPrimaryKey(lmtAppr);
            log.info("更新审批表用户选择条件成功");

/*            ResultInstanceDto resultInstanceDto = JSONObject.parseObject((String) params.get("instanceIdInfo"), ResultInstanceDto.class);
            WFBizParamDto wfParams = new WFBizParamDto();
            wfParams.setBizId(resultInstanceDto.getBizId());
            wfParams.setInstanceId(resultInstanceDto.getInstanceId());
            Map<String, Object> routerParams = new HashMap<>();
            routerParams = lmtAppService.getRouterMapResult((String) params.get("serno"));
            wfParams.setParam(routerParams);
            workflowCoreClient.updateFlowParam(wfParams);
            log.info("更新流程路由节点成功");*/
        }
        return  count;
    }

    /**
     * @函数名称: saveLmtReplySubData
     * @函数描述: 更新批复金额信息
     * @创建人: css
     * @创建时间: 2021-09-04 17:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public int saveLmtReplySubData(Map<String, Object> params) {
        int num = 0;
        LmtReplySub lmtReplySub = new LmtReplySub();
        if(params.get("pkId") != null && !"".equals((String) params.get("pkId"))){
            lmtReplySub.setPkId((String) params.get("pkId"));
            lmtReplySub.setLmtAmt(new BigDecimal((String) params.get("lmtAmt")));
            num = lmtReplySubService.updateSelective(lmtReplySub);
        }
        return num;
    }

    /**
     * @函数名称: saveLmtReplySubData
     * @函数描述: 批量更新批复金额信息
     * @创建人: css
     * @创建时间: 2021-09-04 17:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public int batchSaveLmtReplySubData(List<Map<String, String>> params) {
        int num = 0;
        for(Map map : params){
            if(map.get("approveSubSerno") != null && !"".equals((String) map.get("approveSubSerno"))){
                LmtApprSub lmtApprSub = lmtApprSubService.selectBySubSerno((String) map.get("approveSubSerno"));
                map.put("subSerno",lmtApprSub.getSubSerno());
                // 根据审批分项流水号以及押品编号 去修改 关系表中的 对应融资金额
                GuarBizRel guarBizRel = guarBizRelService.queryGuarBizRelDataBySernoAndGuarNo(map);
                BigDecimal maxMortagageAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank((String) map.get("maxMortagageAmt")) ? "0" : (String) map.get("maxMortagageAmt")));
                BigDecimal correFinAmt = BigDecimal.valueOf(Double.parseDouble(StringUtils.isBlank((String) map.get("correFinAmt")) ? "0" : (String) map.get("correFinAmt")));
                guarBizRel.setCorreFinAmt(correFinAmt);
                if(maxMortagageAmt.compareTo(BigDecimal.ZERO) == 0 || correFinAmt.compareTo(BigDecimal.ZERO) == 0){
                    guarBizRel.setMortagageRate(BigDecimal.ZERO);
                }else{
                    guarBizRel.setMortagageRate(correFinAmt.divide(maxMortagageAmt,4,BigDecimal.ROUND_HALF_UP));
                }
                num += guarBizRelService.updateSelective(guarBizRel);
            }
        }
        return num;
    }

    /**
     * @函数名称:queryEntrustSubDataByParams
     * @函数描述:根据申请流水号查询项下品种委托贷款
     * @参数与返回说明:
     * @算法描述:
     */

    public Map queryEntrustSubDataByParams(Map map) {
        String serno = (String) map.get("serno");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        LmtAppr lmtAppr = null;
        List<LmtApprLoanCond> lmtApprLoanConds = null;
        List<LmtApproveSubDto> lmtApproveSubDtoList = null;
        List<LmtApproveSubDto> finLmtApproveSubDtoList = null;
        try {
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            map.put("oprType", CmisCommonConstants.ADD_OPR);
            lmtAppr = lmtApprMapper.queryLmtApproveDataByParams((HashMap<String, String>) map).get(0);
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("apprSerno",lmtAppr.getApproveSerno());
            queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);// 不展示0的分项随便传值
            lmtApprLoanConds = lmtApprLoanCondService.queryLmtApproveLoanCondDataByParams(queryMap);
            List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
            if (approveSubList != null) {
                lmtApproveSubDtoList = (List<LmtApproveSubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubList, LmtApproveSubDto.class);
                lmtApproveSubDtoList.forEach(lmtApproveSubDto  -> {
                    String subSerno = lmtApproveSubDto.getSubSerno();
                    String subName = lmtApproveSubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtApproveSubDto.setLmtDrawType(subName);
                    }
                    if (!StringUtils.isBlank(subSerno)) {
                        lmtApproveSubDto.setLmtDrawNo(subSerno);
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("approveSubSerno", lmtApproveSubDto.getApproveSubSerno());
                        // 委托贷款
                        queryPrdMap.put("isWtDk", CmisCommonConstants.YES_NO_1);
                        queryPrdMap.put("oprType", CmisCommonConstants.OP_TYPE_01);
                        queryPrdMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);// 不展示0的分项随便传值
                        List<LmtApprSubPrd> approveSubPrdList = lmtApprSubPrdService.queryLmtApproveSubPrdByParams(queryPrdMap);
                        // 判断当前分项下 如果不存在 委托贷款的分项 则将当前分项不展示
                        if(!approveSubPrdList.isEmpty()&&approveSubPrdList.size()>0){
                            List<LmtApproveSubPrdDto> lmtApproveSubPrdDtos = (List<LmtApproveSubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubPrdList, LmtApproveSubPrdDto.class);
                            List<LmtApproveSubPrdDto> resLmtApproveSubPrdDtos = new ArrayList<>();
                            for (LmtApproveSubPrdDto lmtApproveSubPrdDto : lmtApproveSubPrdDtos) {
                                String bizTypeName = lmtApproveSubPrdDto.getLmtBizTypeName();
                                String subPrdSerno = lmtApproveSubPrdDto.getSubPrdSerno();
                                String subSerno1 = lmtApproveSubPrdDto.getSubSerno();
                                if (StringUtils.nonBlank(bizTypeName)) {
                                    lmtApproveSubPrdDto.setLmtDrawType(bizTypeName);
                                }
                                if (StringUtils.nonBlank(subPrdSerno)) {
                                    lmtApproveSubPrdDto.setLmtDrawNo(subPrdSerno);
                                }
                                if (StringUtils.nonBlank(subSerno1)) {
                                    lmtApproveSubPrdDto.setSubSerno(subSerno1);
                                }
                                resLmtApproveSubPrdDtos.add(lmtApproveSubPrdDto);
                            }
                            lmtApproveSubDto.setChildren(resLmtApproveSubPrdDtos);
                            finLmtApproveSubDtoList.add(lmtApproveSubDto);
                        }

                    }
                });
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtAppr", lmtAppr);
            rtnData.put("lmtApproveSubDtoList", finLmtApproveSubDtoList);
            rtnData.put("lmtApprLoanConds", lmtApprLoanConds);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @函数名称:queryEntrustSubDataByApprSernp
     * @函数描述:根据审批流水号查询项下品种委托贷款
     * @参数与返回说明:
     * @算法描述:
     */

    public Map queryEntrustSubDataByApprSerno(Map map) {
        String apprSerno = (String) map.get("apprSerno");
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        List<LmtApproveSubDto> lmtApproveSubDtoList = null;
        List<LmtApproveSubDto> finLmtApproveSubDtoList = null;
        try {
            if (StringUtils.isBlank(apprSerno)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            HashMap<String, String> queryMap = new HashMap();
            queryMap.put("oprType", CmisCommonConstants.ADD_OPR);
            queryMap.put("apprSerno",apprSerno);
            queryMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);// 不展示0的分项随便传值
            List<LmtApprSub> approveSubList = lmtApprSubService.queryLmtApproveSubByParams(queryMap);
            if (approveSubList != null) {
                lmtApproveSubDtoList = (List<LmtApproveSubDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubList, LmtApproveSubDto.class);
                lmtApproveSubDtoList.forEach(lmtApproveSubDto  -> {
                    String subSerno = lmtApproveSubDto.getSubSerno();
                    String subName = lmtApproveSubDto.getSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtApproveSubDto.setLmtDrawType(subName);
                    }
                    if (!StringUtils.isBlank(subSerno)) {
                        lmtApproveSubDto.setLmtDrawNo(subSerno);
                        HashMap<String, String> queryPrdMap = new HashMap<>();
                        queryPrdMap.put("approveSubSerno", lmtApproveSubDto.getApproveSubSerno());
                        // 委托贷款
                        queryPrdMap.put("isWtDk", CmisCommonConstants.YES_NO_1);
                        queryPrdMap.put("oprType", CmisCommonConstants.OP_TYPE_01);
                        queryPrdMap.put("isShowZreoSub",CmisCommonConstants.YES_NO_0);// 不展示0的分项随便传值
                        List<LmtApprSubPrd> approveSubPrdList = lmtApprSubPrdService.queryLmtApproveSubPrdByParams(queryPrdMap);
                        // 判断当前分项下 如果不存在 委托贷款的分项 则将当前分项不展示
                        if(!approveSubPrdList.isEmpty()&&approveSubPrdList.size()>0){
                            List<LmtApproveSubPrdDto> lmtApproveSubPrdDtos = (List<LmtApproveSubPrdDto>) cn.com.yusys.yusp.commons.util.BeanUtils.beansCopy(approveSubPrdList, LmtApproveSubPrdDto.class);
                            List<LmtApproveSubPrdDto> resLmtApproveSubPrdDtos = new ArrayList<>();
                            for (LmtApproveSubPrdDto lmtApproveSubPrdDto : lmtApproveSubPrdDtos) {
                                String bizTypeName = lmtApproveSubPrdDto.getLmtBizTypeName();
                                String subPrdSerno = lmtApproveSubPrdDto.getSubPrdSerno();
                                if (StringUtils.nonBlank(bizTypeName)) {
                                    lmtApproveSubPrdDto.setLmtDrawType(bizTypeName);
                                }
                                if (StringUtils.nonBlank(subPrdSerno)) {
                                    lmtApproveSubPrdDto.setLmtDrawNo(subPrdSerno);
                                }
                                resLmtApproveSubPrdDtos.add(lmtApproveSubPrdDto);
                            }
                            lmtApproveSubDto.setChildren(resLmtApproveSubPrdDtos);
                            finLmtApproveSubDtoList.add(lmtApproveSubDto);
                        }

                    }
                });
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtApproveSubDtoList", finLmtApproveSubDtoList);
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 获取文件绝对路径
     * @param fileId
     * @param serverPath
     * @param relativePath
     * @param fileSystemTemplate
     * @return
     */
    public String getFileAbsolutePath(String fileId, String serverPath, String relativePath, FileSystemTemplate fileSystemTemplate) {
        String absolutePath = "";
        try {
            //获取图片绝对路径
            FileInfo oldFileInfo = FileInfoUtils.fromIdentity(fileId);
            if (oldFileInfo != null) {
                String fileName = oldFileInfo.getFileName();
                //路径缓存文件长度信息
                StringBuilder sb = new StringBuilder(fileName);
                sb.insert(sb.lastIndexOf("."), "_" + oldFileInfo.getFileSize());
                fileName = sb.toString();

                System.err.println("====fileName====>" + fileName);
                FileInfo newFileInfo = FileInfoUtils.createFileInfo(fileSystemTemplate, fileName, relativePath);
                FileInfoUtils.copy(oldFileInfo, newFileInfo, true);
                Path path = Paths.get(serverPath, newFileInfo.getFilePath(), fileName);
                absolutePath = path.toString();
                String newFileId = FileInfoUtils.toIdentity(newFileInfo);
                log.info("newFileName===>{}  newFileID===》{}", fileName, newFileId);
            }
        } catch (Exception e) {
            log.error("获取文件绝对路径失败===》", e);
        }
        return absolutePath;
    }

    /**
     * 更新核查报告文件路径
     * @param condition
     * @return
     */
    public String updateFilePath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String pkId = (String) condition.get("pkId");
        String indgtReportMode = (String) condition.get("indgtReportMode");
        String serverPath = (String) condition.get("serverPath");
        // 获取当前业务信息
        LmtAppr lmtAppr = new LmtAppr();
        lmtAppr.setPkId(pkId);
        lmtAppr = selectByPrimaryKey(pkId);
        //获取文件绝对路径
        String fileAbsolutePath = "";
        String relativePath = "/LmtAppr/"+lmtAppr.getApproveSerno();
        if (!StringUtils.isBlank(fileId)){
            fileAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,sftpFileSystemTemplate);
        }


        lmtAppr.setIndgtReportMode(indgtReportMode);
        lmtAppr.setIndgtReportPath(fileAbsolutePath);
        updateSelective(lmtAppr);
        return fileAbsolutePath;
    }
}
