/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IndustryBsinsIncomeTop;
import cn.com.yusys.yusp.service.IndustryBsinsIncomeTopService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IndustryBsinsIncomeTopResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 19:31:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/industrybsinsincometop")
public class IndustryBsinsIncomeTopResource {
    @Autowired
    private IndustryBsinsIncomeTopService industryBsinsIncomeTopService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IndustryBsinsIncomeTop>> query() {
        QueryModel queryModel = new QueryModel();
        List<IndustryBsinsIncomeTop> list = industryBsinsIncomeTopService.selectAll(queryModel);
        return new ResultDto<List<IndustryBsinsIncomeTop>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IndustryBsinsIncomeTop>> index(QueryModel queryModel) {
        List<IndustryBsinsIncomeTop> list = industryBsinsIncomeTopService.selectByModel(queryModel);
        return new ResultDto<List<IndustryBsinsIncomeTop>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IndustryBsinsIncomeTop> show(@PathVariable("pkId") String pkId) {
        IndustryBsinsIncomeTop industryBsinsIncomeTop = industryBsinsIncomeTopService.selectByPrimaryKey(pkId);
        return new ResultDto<IndustryBsinsIncomeTop>(industryBsinsIncomeTop);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IndustryBsinsIncomeTop> create(@RequestBody IndustryBsinsIncomeTop industryBsinsIncomeTop) throws URISyntaxException {
        industryBsinsIncomeTopService.insert(industryBsinsIncomeTop);
        return new ResultDto<IndustryBsinsIncomeTop>(industryBsinsIncomeTop);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IndustryBsinsIncomeTop industryBsinsIncomeTop) throws URISyntaxException {
        int result = industryBsinsIncomeTopService.update(industryBsinsIncomeTop);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = industryBsinsIncomeTopService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = industryBsinsIncomeTopService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectIndustryBsinsIncomeTopDataBySerno
     * @函数描述:通用列表查询
     * @参数与返回说明:
     * @param map 分页查询类
     * @算法描述:
     */
    @PostMapping("/selectindustrybsinsincometopdatabyserno")
    protected ResultDto<List<Map>> selectIndustryBsinsIncomeTopDataBySerno(@RequestBody Map map) {
        List<Map> list = industryBsinsIncomeTopService.selectIndustryBsinsIncomeTopDataBySerno(map);
        return new ResultDto<List<Map>>(list);
    }
}
