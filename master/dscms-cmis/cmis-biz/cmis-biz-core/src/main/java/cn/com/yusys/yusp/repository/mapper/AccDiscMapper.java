/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AccDisc;
import cn.com.yusys.yusp.dto.AccLoanInfoDto;
import cn.com.yusys.yusp.dto.BusContInfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.resp.Xdtz0029DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.resp.Xdtz0030DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccDiscMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:42:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccDiscMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccDisc selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccDisc> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccDisc record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccDisc record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccDisc record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccDisc record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 查询指定票号在信贷台账中是否已贴现
     * @param porderNo
     * @return
     */
    Xdtz0029DataRespDto getPorderByPorderNo(String porderNo);

    /**
     * 查看信贷贴现台账中票据是否已经存在
     * @param porderNo
     * @return
     */
    Xdtz0030DataRespDto getPorderNumByPorderNo(String porderNo);

    /**
     * 计算出账金额
     * @param list
     * @return
     */
    BigDecimal getPvpAmt(cn.com.yusys.yusp.dto.server.xdht0003.resp.List list);

    /**
     * @方法名称: selectAccStatusByDrftNo
     * @方法描述: 根据票号查询该笔票据0026
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectAccStatusByDrftNo(Map QueryMap);

    /**
     * @方法名称: selectDiscDateByDrftNo
     * @方法描述: 根据票号查询该笔票据台账结清日期
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectDiscDateByDrftNo(Map QueryMap);

    /**
     * @方法名称: updateCountNumByDrftNo
     * @方法描述: 根据票号更新票据台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectCountFromAccDiscBybillNo(Map QueryMap);


    /**
     * @方法名称: updateCountNumByDrftNo
     * @方法描述: 根据票号更新票据台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateCountNumByDrftNo(Map QueryMap);

    /**
     * @方法名称: updateCountNumByBillNo
     * @方法描述: 根据票号撤销出账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateCountNumByBillNo(Map QueryMap);

    /**
     * @方法名称：selectforAccDiscInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 14:57
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccDisc> selectforAccDiscInfo(@Param("cusId")String cusId);

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:57
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccDisc> selectByContNo(@Param("contNo") String contNo);

    /**
     * @param contNo
     * @return countAccLoanCountByContNo
     * @desc 根据贴现协议合同编号查询台账数量
     * @修改历史:
     */
    int countAccLoanCountByCtrDiscContNo(@Param("contNo") String contNo);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId")String cusId);

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询贴现台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccDisc selectByBillNo(@Param("billNo") String billNo);

    /**
     * 根据查询条件查询台账信息并返回
     * @param model
     * @return
     */
    List<AccDisc> querymodelByCondition(QueryModel model);

    /**
     * 根据客户编号获取台账信息
     * @param cusId
     * @return
     */
    List<AccLoanInfoDto> getAllBusAccInfo(String cusId);
}