/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinGuarApp
 * @类描述: lmt_fin_guar_app数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-02-04 11:57:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fin_guar_app")
public class LmtFinGuarApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;

	/** 融资协议名称 **/
	@Column(name = "fin_ctr_name", unique = false, nullable = true, length = 200)
	private String finCtrName;
	
	/** 原业务流水号 **/
	@Column(name = "old_serno", unique = false, nullable = true, length = 40)
	private String oldSerno;
	
	/** 融资协议编号 **/
	@Column(name = "fin_ctr_no", unique = false, nullable = true, length = 40)
	private String finCtrNo;
	
	/** 客户编号 **/
	@Column(name = "cus_id", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "cus_name", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 32)
	private String certCode;
	
	/** 授信类型 STD_ZB_LMT_TYP **/
	@Column(name = "lmt_type", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 担保类别 STD_ZB_GUAR_TYP **/
	@Column(name = "guar_type", unique = false, nullable = true, length = 5)
	private String guarType;
	
	/** 评级结果 STD_ZB_EVAL_RST **/
	@Column(name = "eval_result", unique = false, nullable = true, length = 5)
	private String evalResult;
	
	/** 担保放大倍数 **/
	@Column(name = "guar_bail_multiple", unique = false, nullable = true, length = 8)
	private String guarBailMultiple;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "cur_type", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 担保金额 **/
	@Column(name = "guar_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 单户限额 **/
	@Column(name = "sig_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigAmt;
	
	/** 单笔限额 **/
	@Column(name = "one_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oneAmt;
	
	/** 担保总敞口限额 **/
	@Column(name = "guar_totl_spac", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarTotlSpac;
	
	/** 授信期限类型 STD_ZB_TERM_TYP **/
	@Column(name = "lmt_term_type", unique = false, nullable = true, length = 5)
	private String lmtTermType;
	
	/** 授信期限 **/
	@Column(name = "lmt_term", unique = false, nullable = true, length = 16)
	private Integer lmtTerm;
	
	/** 申请日期 **/
	@Column(name = "app_date", unique = false, nullable = true, length = 20)
	private String appDate;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改时间 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "approve_status", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param finCtrName
	 */
	public void setFinCtrName(String finCtrName) {
		this.finCtrName = finCtrName;
	}

	/**
	 * @return finCtrName
	 */
	public String getFinCtrName() {
		return this.finCtrName;
	}
	
	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno;
	}
	
    /**
     * @return oldSerno
     */
	public String getOldSerno() {
		return this.oldSerno;
	}
	
	/**
	 * @param finCtrNo
	 */
	public void setFinCtrNo(String finCtrNo) {
		this.finCtrNo = finCtrNo;
	}
	
    /**
     * @return finCtrNo
     */
	public String getFinCtrNo() {
		return this.finCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param evalResult
	 */
	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult;
	}
	
    /**
     * @return evalResult
     */
	public String getEvalResult() {
		return this.evalResult;
	}
	
	/**
	 * @param guarBailMultiple
	 */
	public void setGuarBailMultiple(String guarBailMultiple) {
		this.guarBailMultiple = guarBailMultiple;
	}
	
    /**
     * @return guarBailMultiple
     */
	public String getGuarBailMultiple() {
		return this.guarBailMultiple;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return sigAmt
     */
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return oneAmt
     */
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param guarTotlSpac
	 */
	public void setGuarTotlSpac(java.math.BigDecimal guarTotlSpac) {
		this.guarTotlSpac = guarTotlSpac;
	}
	
    /**
     * @return guarTotlSpac
     */
	public java.math.BigDecimal getGuarTotlSpac() {
		return this.guarTotlSpac;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType;
	}
	
    /**
     * @return lmtTermType
     */
	public String getLmtTermType() {
		return this.lmtTermType;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}