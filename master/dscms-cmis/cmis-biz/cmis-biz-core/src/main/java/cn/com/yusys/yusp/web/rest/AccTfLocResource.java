/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.domain.AccLoan;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.service.AccTfLocService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTfLocResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 20:47:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/acctfloc")
public class AccTfLocResource {
    @Autowired
    private AccTfLocService accTfLocService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccTfLoc>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccTfLoc> list = accTfLocService.selectAll(queryModel);
        return new ResultDto<List<AccTfLoc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccTfLoc>> index(QueryModel queryModel) {
        List<AccTfLoc> list = accTfLocService.selectByModel(queryModel);
        return new ResultDto<List<AccTfLoc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccTfLoc> show(@PathVariable("pkId") String pkId) {
        AccTfLoc accTfLoc = accTfLocService.selectByPrimaryKey(pkId);
        return new ResultDto<AccTfLoc>(accTfLoc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccTfLoc> create(@RequestBody AccTfLoc accTfLoc) throws URISyntaxException {
        accTfLocService.insert(accTfLoc);
        return new ResultDto<AccTfLoc>(accTfLoc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccTfLoc accTfLoc) throws URISyntaxException {
        int result = accTfLocService.update(accTfLoc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accTfLocService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accTfLocService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:贷款出账申请待发起
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("偿还借据关联贷款台账页查询")
    @PostMapping("/selectacctflocrepay")
    protected ResultDto<List<AccTfLoc>> selectAccTfLocRepay(@RequestBody QueryModel queryModel) {
        List<AccTfLoc> list = accTfLocService.selectAccTfLocRepay(queryModel);
        return new ResultDto<List<AccTfLoc>>(list);
    }


    /**
     * @函数名称:queryAll
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     * liuquan
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<AccTfLoc>> queryAll(@RequestBody QueryModel queryModel) {
        List<AccTfLoc> list = accTfLocService.selectByModel(queryModel);
        return new ResultDto<List<AccTfLoc>>(list);
    }

    /**
     * @函数名称: selectByBillNo
     * @函数描述:  根据借据编号查询开证台账列表信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByBillNo")
    protected ResultDto<AccTfLoc> selectByBillNo(@RequestBody String billNo) {
        AccTfLoc accTfLoc = accTfLocService.selectByBillNo(billNo);
        return new ResultDto<AccTfLoc>(accTfLoc);
    }

    /**
     * 异步下载保函台账列表
     */
    @PostMapping("/exportAccTfLoc")
    public ResultDto<ProgressDto> asyncExportAccTfLoc(@RequestBody QueryModel model) {
        ProgressDto progressDto = accTfLocService.asyncExportAccTfLoc(model);
        return ResultDto.success(progressDto);
    }

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据pvpSerno查询 借据号billNO
     * @参数与返回说明:存在返回billNo 不存在返回null
     * @算法描述: 无
     */
    @PostMapping("/selectacctflocbillno")
    public String selectAccTfLocBillNo(@RequestBody String pvpSerno) {
        String billNo = accTfLocService.selectAccTfLocBillNo(pvpSerno);
        return billNo;
    }
}
