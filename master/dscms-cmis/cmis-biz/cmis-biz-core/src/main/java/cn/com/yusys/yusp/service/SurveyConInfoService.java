/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.SurveyReportBasicAndCom;
import cn.com.yusys.yusp.domain.SurveyReportBasicInfo;
import cn.com.yusys.yusp.domain.SurveyReportComInfo;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.SurveyConInfo;
import cn.com.yusys.yusp.repository.mapper.SurveyConInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyConInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-12 19:53:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class SurveyConInfoService {

    @Autowired
    private SurveyConInfoMapper surveyConInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public SurveyConInfo selectByPrimaryKey(String surveyNo) {
        return surveyConInfoMapper.selectByPrimaryKey(surveyNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<SurveyConInfo> selectAll(QueryModel model) {
        List<SurveyConInfo> records = (List<SurveyConInfo>) surveyConInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<SurveyConInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<SurveyConInfo> list = surveyConInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(SurveyConInfo record) {
        return surveyConInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(SurveyConInfo record) {
        return surveyConInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(SurveyConInfo record) {
        return surveyConInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(SurveyConInfo record) {
        return surveyConInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String surveyNo) {
        return surveyConInfoMapper.deleteByPrimaryKey(surveyNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return surveyConInfoMapper.deleteByIds(ids);
    }


    /**
     * @创建人 wzy
     * @创建时间 15:57 2021-04-12
     * @return 新增 修改  调查结论信息逻辑
     **/
    @Transactional(rollbackFor = Exception.class)
    public int saveSurveyConInfo(SurveyConInfo SurveyConInfo) {
        //基本信息有无创建
        int result;
        if (this.selectByPrimaryKey(SurveyConInfo.getSurveyNo())==null){
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            SurveyConInfo.setSurveyNo(serno);
            //无基本信息
            result = this.insertSelective(SurveyConInfo);
        }else {
            result =  this.updateSelective(SurveyConInfo);
        }
        //企业信息有无创建
        return  result ;
    }

}
