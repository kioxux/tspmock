package cn.com.yusys.yusp.service.server.xdxw0062;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.req.Xdxw0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.resp.Xdxw0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.LmtDebitCreditInfoService;
import cn.com.yusys.yusp.service.LmtSurveyConInfoService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0062Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0062Service.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;

    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private SenddxService senddxService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    @Autowired
    private LmtDebitCreditInfoService lmtDebitCreditInfoService; //借款人征信信息
    @Autowired
    private LmtVerifInfoMapper lmtVerifInfoMapper;

    /**
     * 小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
     *
     * @param xdxw0062DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0062DataRespDto saveSurveyAndSendMessage(Xdxw0062DataReqDto xdxw0062DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value);
        Xdxw0062DataRespDto xdxw0062DataRespDto = new Xdxw0062DataRespDto();
        try {
            //查询优企贷信息表 防止重复提交
            int num = lmtSurveyReportBasicInfoMapper.getExist(xdxw0062DataReqDto);
            if (num > 0) {
                xdxw0062DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                xdxw0062DataRespDto.setOpMsg("记录已存在，请勿重复提交");
            } else {
                String businessId = xdxw0062DataReqDto.getBusinessId(); //业务申请编号
                String indname = xdxw0062DataReqDto.getIndname(); //申请人姓名
                String indcertid = xdxw0062DataReqDto.getIndcertid(); //申请人证件号
                String entname = xdxw0062DataReqDto.getEntname(); //企业名称
                String indphoneid = xdxw0062DataReqDto.getIndphoneid(); //申请人手机号
                String address = xdxw0062DataReqDto.getAddress(); //生产经营地址
                String hydm = xdxw0062DataReqDto.getHydm(); //行业代码
                String sfnh = xdxw0062DataReqDto.getSfnh(); //是否农户
                String entCertType = xdxw0062DataReqDto.getEnt_cert_type(); //企业证件类型
                String entCertId = xdxw0062DataReqDto.getEnt_cert_id(); //企业证件号
                String cusMgrId = xdxw0062DataReqDto.getCus_mgr_id(); //营销客户经理编号
                String isSceneCheck = xdxw0062DataReqDto.getIs_scene_check(); //是否现场核验
                String checkReason = xdxw0062DataReqDto.getCheck_reason(); //核验原因
                String liveAddr = xdxw0062DataReqDto.getLive_addr(); //居住地址
                BigDecimal finalLmt = xdxw0062DataReqDto.getFinal_lmt(); //终审额度
                BigDecimal interestRate = xdxw0062DataReqDto.getInterest_rate(); //利率
                BigDecimal indcredit005 = xdxw0062DataReqDto.getIndcredit005(); //累计逾期次数
                BigDecimal mdzrcbIcr0007 = xdxw0062DataReqDto.getMdzrcb_icr0007(); //未结清贷款笔数
                BigDecimal mdzrcbIcr0010 = xdxw0062DataReqDto.getMdzrcb_icr0010(); //未销户信用卡授信总额
                BigDecimal mdzrcbIcr0011 = xdxw0062DataReqDto.getMdzrcb_icr0011(); //未结清贷款余额
                BigDecimal mdzrcbIcr0015 = xdxw0062DataReqDto.getMdzrcb_icr0015(); //近6个月信用卡平均支用比例
                BigDecimal mdzrcbIcr0017 = xdxw0062DataReqDto.getMdzrcb_icr0017(); //最近2年贷款审批查询机构数
                BigDecimal mdzrcbIcr0019 = xdxw0062DataReqDto.getMdzrcb_icr0019(); //未结清消费金融公司贷款笔数
                BigDecimal mdzrcbIcr0020 = xdxw0062DataReqDto.getMdzrcb_icr0020(); //未结清对外贷款担保笔数
                BigDecimal mdzrcbEcr0005 = xdxw0062DataReqDto.getMdzrcb_ecr0005(); //企业未结清业务机构数
                BigDecimal mdzrcbEcr0004 = xdxw0062DataReqDto.getMdzrcb_ecr0004(); //企业未结清信贷余额
                String tax003 = xdxw0062DataReqDto.getTax003(); //完整纳税月份数
                String mdzrcbTax0008 = xdxw0062DataReqDto.getMdzrcb_tax0008(); //当前税务信用评级
                BigDecimal mdzrcbTax0005 = xdxw0062DataReqDto.getMdzrcb_tax0005(); //近1年销售收入
                BigDecimal mdzrcbTax0007 = xdxw0062DataReqDto.getMdzrcb_tax0007(); //近1年综合应纳税额
                String continuedloan = xdxw0062DataReqDto.getContinuedloan(); //是否续贷
                String modelPreliResult = xdxw0062DataReqDto.getModel_preli_result(); //模型初步结果
                BigDecimal applyterm = xdxw0062DataReqDto.getApplyterm() == null ? new BigDecimal("0") : xdxw0062DataReqDto.getApplyterm(); //申请期限
                String istqsd = xdxw0062DataReqDto.getIs_tqsd();// 是否提前申贷
                String cnWhb = xdxw0062DataReqDto.getCn_whb();// 是否可无还本续贷
                BigDecimal whbmodelamt = xdxw0062DataReqDto.getWhbmodelamt();// 无还本续贷模型金额
                BigDecimal whbmodelrat = xdxw0062DataReqDto.getWhbmodelrat();// 无还本续贷模型利率
                String listSerno = xdxw0062DataReqDto.getListSerno();// 优企贷续贷名单流水号
                Date currDate = DateUtils.getCurrDate(); //当前系统时间
                String currDateTimeStr = DateUtils.formatDate(currDate, DateFormatEnum.DATETIME.getValue()); //当前系统时间(yyyy-MM-dd HH:mm:ss)
                String currDateStr = DateUtils.formatDate(currDate, DateFormatEnum.DEFAULT.getValue()); //当前系统时间(yyyy-MM-dd)
                //必输项校验
                if (StringUtils.isBlank(businessId)) {
                    throw new Exception("业务申请编号为空！");
                }
                if (StringUtils.isBlank(indname)) {
                    throw new Exception("申请人姓名为空！");
                }
                if (StringUtils.isBlank(indcertid)) {
                    throw new Exception("申请人证件号为空！");
                }
                if (StringUtils.isBlank(entname)) {
                    //throw new Exception("企业名称为空！");
                }
                if (StringUtils.isBlank(indphoneid)) {
                    throw new Exception("申请人手机号为空！");
                }
                if (StringUtils.isBlank(address)) {
                    //throw new Exception("生产经营地址为空！");
                }
                if (StringUtils.isBlank(hydm)) {
                    //throw new Exception("行业代码为空！");
                }
                if (StringUtils.isBlank(sfnh)) {
                    //throw new Exception("是否农户为空！");
                }
                if (StringUtils.isBlank(entCertType)) {
                    throw new Exception("企业证件类型为空！");
                }
                if (StringUtils.isBlank(entCertId)) {
                    //throw new Exception("企业证件号为空！");
                }
                if (StringUtils.isBlank(isSceneCheck)) {
                    throw new Exception("是否现场核验为空！");
                }
                if (StringUtils.isBlank(checkReason)) {
                    throw new Exception("核验原因为空！");
                }
                if (StringUtils.isBlank(liveAddr)) {
                    //throw new Exception("居住地址为空！");
                }
                if (Objects.isNull(finalLmt)) {
                    throw new Exception("终审额度为空！");
                }
                if (Objects.isNull(interestRate)) {
                    throw new Exception("利率为空！");
                }
                if (StringUtils.isBlank(continuedloan)) {
                    throw new Exception("是否续贷为空！");
                }
                if (StringUtils.isBlank(modelPreliResult)) {
                    throw new Exception("模型初步结果为空！");
                }

                //根据证件号查询客户信息
                CusBaseDto cusInfoDto = new CusBaseDto();
                List<CusBaseDto> cusBaseDtos = commonService.getCusBaseListByCertCode(indcertid);
                if (CollectionUtils.nonEmpty(cusBaseDtos)) {
                    cusInfoDto = cusBaseDtos.get(0);
                }
                if (Objects.isNull(cusInfoDto) || StringUtils.isEmpty(cusInfoDto.getCusId())) {
                    xdxw0062DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdxw0062DataRespDto.setOpMsg("客户信息不存在,请检查");
                    return xdxw0062DataRespDto;
                }
                //下面的字段在分配完客户经理后，需要更新进去
                String openday = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                // 生成调查流水号
                String surveySerno = businessId;
                //(1)LMT_SURVEY_REPORT_MAIN_INFO 客户授信调查主表
                LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
                lmtSurveyReportMainInfo.setSurveySerno(surveySerno);
                lmtSurveyReportMainInfo.setBizSerno(businessId);
                lmtSurveyReportMainInfo.setListSerno(listSerno);
                lmtSurveyReportMainInfo.setCusName(indname);
                lmtSurveyReportMainInfo.setCertCode(indcertid);
                lmtSurveyReportMainInfo.setMarId(cusMgrId);
                lmtSurveyReportMainInfo.setCreateTime(currDate);
                lmtSurveyReportMainInfo.setUpdateTime(currDate);
                lmtSurveyReportMainInfo.setBizSerno(businessId); //第三方业务流水号
                lmtSurveyReportMainInfo.setPrdName(DscmsBizXwEnum.YQD_TYPE_01.value); //产品名称
                lmtSurveyReportMainInfo.setPrdType(""); //产品类型
                lmtSurveyReportMainInfo.setPrdId(DscmsBizXwEnum.YQD_TYPE_01.key); //产品编号,暂时默认老信贷产品
                lmtSurveyReportMainInfo.setSurveyType(DscmsBizXwEnum.SURVEY_TYPE_12.key); //小微调查报告类型
                lmtSurveyReportMainInfo.setCusId(cusInfoDto.getCusId()); //客户编号
                lmtSurveyReportMainInfo.setCertType(cusInfoDto.getCertType()); //证件类型
                lmtSurveyReportMainInfo.setIsStopOffline(CommonConstance.STD_ZB_YES_NO_1); //是否线下调查
                lmtSurveyReportMainInfo.setAppAmt(finalLmt); //申请金额
                lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_000); //审批状态
                lmtSurveyReportMainInfo.setDataSource(DscmsBizXwEnum.CONFIRMSTATUS_01.key); //数据来源,01 系统推送
                lmtSurveyReportMainInfo.setIntoTime(currDateStr); //进件时间
                lmtSurveyReportMainInfo.setOprType(DscmsBizXwEnum.ADD_01.key); //操作类型,新增
                //下面的字段在分配完客户经理后，需要更新进去
                lmtSurveyReportMainInfo.setIsAutodivis(CommonConstance.STD_ZB_YES_NO_0); //是否自动分配
                lmtSurveyReportMainInfo.setManagerBrId(cusInfoDto.getMainBrId()); //主管机构
                lmtSurveyReportMainInfo.setManagerId(cusInfoDto.getManagerId()); //主管客户经理
                lmtSurveyReportMainInfo.setInputId(cusInfoDto.getInputId()); //登记人
                lmtSurveyReportMainInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                lmtSurveyReportMainInfo.setInputDate(openday); //登记日期
                lmtSurveyReportMainInfo.setUpdId(cusInfoDto.getUpdId()); //最后修改人
                lmtSurveyReportMainInfo.setUpdBrId(cusInfoDto.getUpdBrId()); //最后修改机构
                lmtSurveyReportMainInfo.setUpdDate(openday); //最后修改日期

                //保存信息至优企贷信息表，默认状态为未调查
                //(2)LMT_SURVEY_REPORT_BASIC_INFO 调查报告基本信息表
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
                lmtSurveyReportBasicInfo.setSurveySerno(surveySerno);
                lmtSurveyReportBasicInfo.setCusName(indname);
                lmtSurveyReportBasicInfo.setCertCode(indcertid);
                lmtSurveyReportBasicInfo.setPhone(indphoneid);
                lmtSurveyReportBasicInfo.setIsAgri(sfnh);
                lmtSurveyReportBasicInfo.setMarId(cusMgrId);
                lmtSurveyReportBasicInfo.setIsSceneInquest(isSceneCheck);
                lmtSurveyReportBasicInfo.setLivingAddr(liveAddr);
                lmtSurveyReportBasicInfo.setAppAmt(finalLmt);
                lmtSurveyReportBasicInfo.setIsRenewLoan(continuedloan);
                lmtSurveyReportBasicInfo.setModelFstResult(modelPreliResult);
                lmtSurveyReportBasicInfo.setCreateTime(currDate);
                lmtSurveyReportBasicInfo.setUpdateTime(currDate);
                lmtSurveyReportBasicInfo.setCusId(cusInfoDto.getCusId()); //客户编号
                lmtSurveyReportBasicInfo.setAdviceAmt(finalLmt); //建议金额
                lmtSurveyReportBasicInfo.setAdviceRate(interestRate); //建议利率
                lmtSurveyReportBasicInfo.setAdviceTerm(applyterm.toString()); //建议期限
                lmtSurveyReportBasicInfo.setWorkUnit(entname); //工作单位
                lmtSurveyReportBasicInfo.setModelAdviceAmt(finalLmt); //模型建议金额
                lmtSurveyReportBasicInfo.setModelAdviceRate(interestRate); //模型建议利率
                lmtSurveyReportBasicInfo.setIsRenewLoan(continuedloan); //是否续贷
                lmtSurveyReportBasicInfo.setLoanPurp(hydm); //贷款用途
                lmtSurveyReportBasicInfo.setOprType(DscmsBizXwEnum.ADD_01.key); //操作类型,新增
                lmtSurveyReportBasicInfo.setIsTqsd(istqsd);// 是否提前申贷
                lmtSurveyReportBasicInfo.setIsCwhb(cnWhb);// 是否可无还本续贷
                lmtSurveyReportBasicInfo.setWhbModelAmt(whbmodelamt);// 无还本续贷模型金额
                lmtSurveyReportBasicInfo.setWhbModelRate(whbmodelrat);// 无还本续贷模型利率


                lmtSurveyReportBasicInfo.setManagerBrId(cusInfoDto.getMainBrId()); //主管机构
                lmtSurveyReportBasicInfo.setManagerId(cusInfoDto.getManagerId()); //主管客户经理
                lmtSurveyReportBasicInfo.setInputId(cusInfoDto.getInputId()); //登记人
                lmtSurveyReportBasicInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                lmtSurveyReportBasicInfo.setInputDate(openday); //登记日期
                lmtSurveyReportBasicInfo.setUpdId(cusInfoDto.getUpdId()); //最后修改人
                lmtSurveyReportBasicInfo.setUpdBrId(cusInfoDto.getUpdBrId()); //最后修改机构
                lmtSurveyReportBasicInfo.setUpdDate(openday); //最后修改日期


                //(3)LMT_SURVEY_REPORT_COM_INFO 调查报告企业信息表
                LmtSurveyReportComInfo lmtSurveyReportComInfo = new LmtSurveyReportComInfo();
                lmtSurveyReportComInfo.setSurveySerno(surveySerno);
                lmtSurveyReportComInfo.setConName(entname);
                lmtSurveyReportComInfo.setOperAddr(address);
                lmtSurveyReportComInfo.setTrade(hydm);
                lmtSurveyReportComInfo.setCorpCertType(entCertType);
                lmtSurveyReportComInfo.setCorpCertCode(entCertId);
                lmtSurveyReportComInfo.setCorpFullTaxMonths(tax003);
                lmtSurveyReportComInfo.setCorpCurtTxtCdtEval(mdzrcbTax0008);
                lmtSurveyReportComInfo.setCorpLt1yearSaleIncome(mdzrcbTax0005);
                lmtSurveyReportComInfo.setCorpLt1yearInteTax(mdzrcbTax0007);
                lmtSurveyReportComInfo.setCreateTime(currDate);
                lmtSurveyReportComInfo.setUpdateTime(currDate);
                lmtSurveyReportComInfo.setOprType(DscmsBizXwEnum.ADD_01.key); //操作类型,新增
                //下面的字段在分配完客户经理后，需要更新进去
                lmtSurveyReportComInfo.setManagerBrId(""); //主管机构
                lmtSurveyReportComInfo.setManagerId(""); //主管客户经理
                lmtSurveyReportComInfo.setInputId(cusInfoDto.getInputId()); //登记人
                lmtSurveyReportComInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                lmtSurveyReportComInfo.setInputDate(openday); //登记日期
                lmtSurveyReportComInfo.setUpdId(cusInfoDto.getUpdId()); //最后修改人
                lmtSurveyReportComInfo.setUpdBrId(cusInfoDto.getUpdBrId()); //最后修改机构
                lmtSurveyReportComInfo.setUpdDate(openday); //最后修改日期

                //(4)LMT_SURVEY_TASK_DIVIS 调查任务分配表
                LmtSurveyTaskDivis lmtSurveyTaskDivis = new LmtSurveyTaskDivis();
                lmtSurveyTaskDivis.setPkId(UUID.randomUUID().toString());
                lmtSurveyTaskDivis.setSurveySerno(surveySerno);
                lmtSurveyTaskDivis.setCusName(indname);
                lmtSurveyTaskDivis.setCertCode(indcertid);
                lmtSurveyTaskDivis.setAppAmt(finalLmt);
                lmtSurveyTaskDivis.setPhone(indphoneid);
                lmtSurveyTaskDivis.setPrdName(DscmsBizXwEnum.YQD_TYPE_01.value);//产品名称  不能为空，没有产品名称 就塞了企业名称 addbychenyong20210612
                lmtSurveyTaskDivis.setPrdType("");//产品类型 暂时写产品号
                lmtSurveyTaskDivis.setPrdId(DscmsBizXwEnum.YQD_TYPE_01.key);
                lmtSurveyTaskDivis.setCusId(cusInfoDto.getCusId());
                lmtSurveyTaskDivis.setCertType(cusInfoDto.getCertType());
                lmtSurveyTaskDivis.setCreateTime(currDate);
                lmtSurveyTaskDivis.setUpdateTime(currDate);
                lmtSurveyTaskDivis.setBizSerno(businessId); //第三方业务流水号
                lmtSurveyTaskDivis.setAppChnl("06"); //申请渠道
                lmtSurveyTaskDivis.setIntoTime(currDateStr); //进件时间
                lmtSurveyTaskDivis.setMarConfirmStatus(DscmsBizXwEnum.CONFIRMSTATUS_00.value); //客户经理确认状态,"00" 默认待确认
                lmtSurveyTaskDivis.setMarId(cusMgrId); //营销人
                lmtSurveyTaskDivis.setBelgLine("01"); //所属条线,"01" 默认小微条线
                lmtSurveyTaskDivis.setIsStopOffline(CommonConstance.STD_ZB_YES_NO_1); //是否线下调查
                //下面的字段在分配完客户经理后，需要更新进去
                lmtSurveyTaskDivis.setManagerName(""); //客户经理名称
                lmtSurveyTaskDivis.setManagerId(""); //客户经理编号
                lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
                lmtSurveyTaskDivis.setDivisStatus("101"); //调查分配状态,未分配  100已分配
                lmtSurveyTaskDivis.setDivisTime(currDateStr); //分配时间
                lmtSurveyTaskDivis.setDivisId(""); //分配人
                lmtSurveyTaskDivis.setPrcId(""); //处理人


                //判断是否自动分配(默认营销人员)
                String isFengpei = this.CheckGeShuiDiZhiFenPei("yqdauthor", indcertid, cusMgrId, address);
                if ("1".equals(isFengpei) || "2".equals(isFengpei)) {
                    //自动分配（默认营销人员）
                    ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(cusMgrId);
                    if (Objects.isNull(manager.getData())) {
                        throw new Exception("无法根据该营销客户经理编号[cusMgrId]未查询到客户经理信息！");
                    }

                    String actorname = manager.getData().getUserName();
                    String telnum = manager.getData().getUserMobilephone();
                    String orgId = manager.getData().getOrgId();

                    //完善插入表数据
                    //(1)LMT_SURVEY_REPORT_BASIC_INFO 调查报告基本信息表
                    lmtSurveyReportBasicInfo.setManagerBrId(orgId); //主管机构
                    lmtSurveyReportBasicInfo.setManagerId(cusMgrId); //主管客户经理
                    lmtSurveyReportBasicInfo.setInputId(cusMgrId); //登记人
                    lmtSurveyReportBasicInfo.setInputBrId(orgId); //登记机构
                    lmtSurveyReportBasicInfo.setInputDate(currDateStr); //登记日期
                    lmtSurveyReportBasicInfo.setUpdId(cusMgrId); //最后修改人
                    lmtSurveyReportBasicInfo.setUpdBrId(orgId); //最后修改机构
                    lmtSurveyReportBasicInfo.setUpdDate(currDateStr); //最后修改日期
                    //(2)LMT_SURVEY_REPORT_MAIN_INFO 客户授信调查主表
                    lmtSurveyReportMainInfo.setIsAutodivis("1"); //是否自动分配,"1" 自动分配
                    lmtSurveyReportMainInfo.setManagerBrId(orgId); //主管机构
                    lmtSurveyReportMainInfo.setManagerId(cusMgrId); //主管客户经理
                    lmtSurveyReportMainInfo.setInputId(cusMgrId); //登记人
                    lmtSurveyReportMainInfo.setInputBrId(orgId); //登记机构
                    lmtSurveyReportMainInfo.setInputDate(currDateStr); //登记日期
                    lmtSurveyReportMainInfo.setUpdId(cusMgrId); //最后修改人
                    lmtSurveyReportMainInfo.setUpdBrId(orgId); //最后修改机构
                    lmtSurveyReportMainInfo.setUpdDate(currDateStr); //最后修改日期
                    //(3)LMT_SURVEY_REPORT_COM_INFO 调查报告企业信息表
                    lmtSurveyReportComInfo.setManagerBrId(orgId); //主管机构
                    lmtSurveyReportComInfo.setManagerId(cusMgrId); //主管客户经理
                    lmtSurveyReportComInfo.setInputId(cusMgrId); //登记人
                    lmtSurveyReportComInfo.setInputBrId(orgId); //登记机构
                    lmtSurveyReportComInfo.setInputDate(currDateStr); //登记日期
                    lmtSurveyReportComInfo.setUpdId(cusMgrId); //最后修改人
                    lmtSurveyReportComInfo.setUpdBrId(orgId); //最后修改机构
                    lmtSurveyReportComInfo.setUpdDate(currDateStr); //最后修改日期
                    //(4)LMT_SURVEY_TASK_DIVIS 调查任务分配表
                    lmtSurveyTaskDivis.setManagerName(actorname); //客户经理名称
                    lmtSurveyTaskDivis.setManagerId(cusMgrId); //客户经理编号
                    lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
                    lmtSurveyTaskDivis.setDivisStatus("100"); //调查分配状态,未分配  100已分配
                    lmtSurveyTaskDivis.setMarConfirmStatus(DscmsBizXwEnum.CONFIRMSTATUS_01.value); //客户经理确认状态,"00" 默认待确认
                    lmtSurveyTaskDivis.setDivisTime(currDateTimeStr); //分配时间
                    lmtSurveyTaskDivis.setDivisId(""); //分配人,默认营销人员时为空
                    lmtSurveyTaskDivis.setPrcId(cusMgrId); //处理人

                    if (!StringUtils.isBlank(telnum)) {
                        String sendMessage = "客户" + indname + "，在小微业务管理平台发起贷款申请，请及时处理！";
                        logger.info("发送的短信内容：" + sendMessage);

                        SenddxReqDto senddxReqDto = new SenddxReqDto();
                        senddxReqDto.setInfopt("dx");
                        SenddxReqList senddxReqList = new SenddxReqList();
                        senddxReqList.setMobile(telnum);
                        senddxReqList.setSmstxt(sendMessage);
                        ArrayList<SenddxReqList> list = new ArrayList<>();
                        list.add(senddxReqList);
                        senddxReqDto.setSenddxReqList(list);
                        try {
                            senddxService.senddx(senddxReqDto);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                } else {
                    //人工分配
                    // 以下方法需要研发提供api -已提供
                    GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
                    getUserInfoByRoleCodeDto.setRoleCode("R1015");//1103	管理岗（小贷）
                    List<AdminSmUserDto> adminSmUserDtos = commonService.getUserInfoByRoleCode(getUserInfoByRoleCodeDto);

                    if (adminSmUserDtos != null && adminSmUserDtos.size() > 0) {
                        String telnum = adminSmUserDtos.get(0).getUserMobilephone(); //电话
                        String actorname = adminSmUserDtos.get(0).getUserName(); //用户名
                        String loginCode = adminSmUserDtos.get(0).getLoginCode(); //用户码

                        //完善插入表数据
                        //(1)LMT_SURVEY_REPORT_BASIC_INFO 调查报告基本信息表
                        lmtSurveyReportBasicInfo.setManagerBrId(cusInfoDto.getMainBrId()); //主管机构
                        lmtSurveyReportBasicInfo.setManagerId(cusInfoDto.getManagerId()); //主管客户经理
                        lmtSurveyReportBasicInfo.setInputId(cusInfoDto.getInputId()); //登记人
                        lmtSurveyReportBasicInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                        lmtSurveyReportBasicInfo.setInputDate(currDateStr); //登记日期
                        lmtSurveyReportBasicInfo.setUpdId(""); //最后修改人
                        lmtSurveyReportBasicInfo.setUpdBrId(""); //最后修改机构
                        lmtSurveyReportBasicInfo.setUpdDate(currDateStr); //最后修改日期
                        //(2)LMT_SURVEY_REPORT_MAIN_INFO 客户授信调查主表
                        lmtSurveyReportMainInfo.setIsAutodivis(CommonConstance.STD_ZB_YES_NO_0); //是否自动分配
                        lmtSurveyReportMainInfo.setManagerBrId(cusInfoDto.getMainBrId()); //主管机构
                        lmtSurveyReportMainInfo.setManagerId(cusInfoDto.getManagerId()); //主管客户经理
                        lmtSurveyReportMainInfo.setInputId(cusInfoDto.getInputId()); //登记人
                        lmtSurveyReportMainInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                        lmtSurveyReportMainInfo.setInputDate(currDateStr); //登记日期
                        lmtSurveyReportMainInfo.setUpdId(""); //最后修改人
                        lmtSurveyReportMainInfo.setUpdBrId(""); //最后修改机构
                        lmtSurveyReportMainInfo.setUpdDate(currDateStr); //最后修改日期
                        //(3)LMT_SURVEY_REPORT_COM_INFO 调查报告企业信息表
                        lmtSurveyReportComInfo.setManagerBrId(cusInfoDto.getMainBrId()); //主管机构
                        lmtSurveyReportComInfo.setManagerId(cusInfoDto.getManagerId()); //主管客户经理
                        lmtSurveyReportComInfo.setInputId(cusInfoDto.getInputId()); //登记人
                        lmtSurveyReportComInfo.setInputBrId(cusInfoDto.getInputBrId()); //登记机构
                        lmtSurveyReportComInfo.setInputDate(currDateStr); //登记日期
                        lmtSurveyReportComInfo.setUpdId(""); //最后修改人
                        lmtSurveyReportComInfo.setUpdBrId(""); //最后修改机构
                        lmtSurveyReportComInfo.setUpdDate(currDateStr); //最后修改日期
                        //(4)LMT_SURVEY_TASK_DIVIS 调查任务分配表
                        lmtSurveyTaskDivis.setManagerName(actorname); //客户经理名称
                        lmtSurveyTaskDivis.setManagerId(cusMgrId); //客户经理编号
                        lmtSurveyTaskDivis.setManagerArea(""); //客户经理片区
                        lmtSurveyTaskDivis.setDivisStatus("101"); //调查分配状态,未分配  100已分配
                        lmtSurveyTaskDivis.setDivisTime(currDateStr); //分配时间
                        lmtSurveyTaskDivis.setDivisId(loginCode); //分配人
                        lmtSurveyTaskDivis.setPrcId(""); //处理人

                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, "telnum:" + telnum + ",actorname:" + actorname);
                        if (!StringUtils.isBlank(telnum)) {
                            String sendMessage = "客户" + indname + "，在小微业务管理平台发起贷款申请，请安排客户经理及时处理！";
                            logger.info("发送的短信内容：" + sendMessage);

                            SenddxReqDto senddxReqDto = new SenddxReqDto();
                            senddxReqDto.setInfopt("dx");
                            SenddxReqList senddxReqList = new SenddxReqList();
                            senddxReqList.setMobile(telnum);
                            senddxReqList.setSmstxt(sendMessage);
                            ArrayList<SenddxReqList> list = new ArrayList<>();
                            list.add(senddxReqList);
                            senddxReqDto.setSenddxReqList(list);
                            try {
                                senddxService.senddx(senddxReqDto);
                            } catch (Exception e) {
                                throw new Exception("发送短信失败！");
                            }
                        }
                    }
                }
                // 征信信息落库
                LmtDebitCreditInfo lmtDebitCreditInfo = new LmtDebitCreditInfo();
                BeanUtils.beanCopy(lmtSurveyReportMainInfo, lmtDebitCreditInfo);
                lmtDebitCreditInfo.setLoanUnpaidNums(Objects.isNull(mdzrcbIcr0007) ? StringUtils.EMPTY : mdzrcbIcr0007.toPlainString()); // 借款人未结清贷款笔数
                lmtDebitCreditInfo.setCreditCrdamtClose(mdzrcbIcr0010); // 借款人未销户信用卡授信总额
                lmtDebitCreditInfo.setLoanBalUnpaid(mdzrcbIcr0011); // 借款人未结清贷款余额
                lmtDebitCreditInfo.setCredit6mAveDefrayPerc(mdzrcbIcr0015); // 借款人近6个月信用卡平均支用比例
                lmtDebitCreditInfo.setLoan2yQryorgNums(Objects.isNull(mdzrcbIcr0017) ? StringUtils.EMPTY : mdzrcbIcr0017.toPlainString()); // 借款人近2年贷款审批查询机构数
                lmtDebitCreditInfo.setLoanConsumecomUnpaidNums(Objects.isNull(mdzrcbIcr0019) ? StringUtils.EMPTY : mdzrcbIcr0019.toPlainString()); // 借款人未结清消费金融公司贷款笔数
                lmtDebitCreditInfo.setLoanOutguarUnpaidNums(Objects.isNull(mdzrcbIcr0020) ? StringUtils.EMPTY : mdzrcbIcr0020.toPlainString()); // 借款人未结清对外担保贷款笔数
                lmtDebitCreditInfo.setComUnpaidBusiOrg(Objects.isNull(mdzrcbEcr0005) ? StringUtils.EMPTY : mdzrcbEcr0005.toPlainString()); // 企业未结清业务机构数
                lmtDebitCreditInfo.setLoanBalComUnpaid(mdzrcbEcr0004); // 企业未结清信贷余额
                // 调查结论信息落库
                LmtSurveyConInfo lmtSurveyConInfo = new LmtSurveyConInfo();
                BeanUtils.beanCopy(lmtSurveyReportMainInfo, lmtSurveyConInfo);
                lmtSurveyConInfo.setSurveySerno(surveySerno);
                lmtSurveyConInfo.setLoanTerm(applyterm.toString());
                lmtSurveyConInfo.setModelAmt(finalLmt);
                lmtSurveyConInfo.setModelRate(interestRate);
                lmtSurveyConInfo.setModelTerm(applyterm.toString());
                //落表
                logger.info("*************XDXW0062新增授信调查主表lmtSurveyReportMainInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                int mainResult = lmtSurveyReportMainInfoMapper.insert(lmtSurveyReportMainInfo);
                logger.info("*************XDXW0062新增授信调查基本信息表lmtSurveyReportBasicInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                int basicResult = lmtSurveyReportBasicInfoMapper.insert(lmtSurveyReportBasicInfo);
                logger.info("*************XDXW0062新增授信调查报告企业信息lmtSurveyReportComInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyReportComInfo));
                int comResult = lmtSurveyReportComInfoMapper.insert(lmtSurveyReportComInfo);
                logger.info("*************XDXW0062新增授信调查任务分配表信息lmtSurveyTaskDivis开始,新增参数为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
                int divisResult = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                logger.info("*************XDXW0062新增授信调查结论表信息lmtSurveyConInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
                int conResult = lmtSurveyConInfoService.insert(lmtSurveyConInfo);
                logger.info("*************XDXW0062新增借款人征信信息lmtDebitCreditInfo开始,新增参数为:{}", JSON.toJSONString(lmtDebitCreditInfo));
                int DebitResult = lmtDebitCreditInfoService.insert(lmtDebitCreditInfo);
                logger.info("*************XDXW0062新增核验信息lmt_verif_info开始,新增参数为:{}", JSON.toJSONString(surveySerno));
                LmtVerifInfo lmtVerifInfo = lmtVerifInfoMapper.selectByPrimaryKey(surveySerno);
                if(lmtVerifInfo == null){//surveySerno调查流水下核验信息不存在
                    //(5)新增核验信息
                    lmtVerifInfo = new LmtVerifInfo();
                    lmtVerifInfo.setSurveySerno(surveySerno);//调查流水
                    lmtVerifInfo.setToinvResn(checkReason);//核验原因
                    lmtVerifInfoMapper.insert(lmtVerifInfo);
                }

                if (divisResult < 1 || comResult < 1 || basicResult < 1 || mainResult < 1 || conResult < 1 || DebitResult < 1) {
                    throw new Exception("数据插入失败！");
                }

                xdxw0062DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
                xdxw0062DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, e.getMessage());
            xdxw0062DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdxw0062DataRespDto.setOpMsg("系统异常:" + e.getMessage());
            throw new Exception("系统异常:" + e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value);
        return xdxw0062DataRespDto;
    }

    /**
     * 判断是否自动分配(默认营销人员)
     *
     * @param authorType
     * @param certCode
     * @param managerId
     * @param geShuiDiZhi
     * @return
     */
    public String CheckGeShuiDiZhiFenPei(String authorType, String certCode, String managerId, String geShuiDiZhi) {
        logger.info("**********判断是否自动分配(默认营销人员)开始,参数为:{}证件号:{}营销经理:{}个税地址:{}", authorType, certCode, managerId, geShuiDiZhi);
        String flag = "0";
        try {
            //1.找到客户的客户经理
            logger.info("***************根据证件号码【{}】，查询客户信息开始******************", certCode);
            String custMgr = StringUtils.EMPTY;
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCusByCertCode(certCode);
            if (!Objects.isNull(cusBaseClientDto)) {
                logger.info("***************根据证件号码【{}】，查询到客户信息******************", certCode);
                custMgr = cusBaseClientDto.getManagerId();

                // 对于优企贷虚拟客户经理，默认无客户经理
                if ("xwd00001".equals(custMgr)) {
                    logger.info("虚拟客户经理默认无管护经理");
                    custMgr = "";
                }
            }
            logger.info("***************根据证件号码【{}】，查询客户信息结束，管护经理号【{}】******************", certCode, custMgr);
            //2.查看是否有优企贷权限，如果无权限则人工分配
            //取AreaAdminUser中的online_prd_manager字段
            logger.info("**********取AreaAdminUser中的online_prd_manager字段开始,查询参数为:{}", JSON.toJSONString(custMgr));
            AreaAdminUser areaAdminUser = areaAdminUserMapper.selectByPrimaryKey(managerId);
            if (Objects.isNull(areaAdminUser)) {
                throw new Exception("无法根据该营销客户经理编号[custMgr]查询到小贷客户经理从表信息！");
            }
            String onlinePrdManager = areaAdminUser.getOnlinePrdManager();
            logger.info("根据营销客户经理【{}】，查询线上产品权限【】", managerId, onlinePrdManager);
            int count = 0;
            if ("yxdauthor".equals(authorType)) {
                //优享贷 01
                if (StringUtils.isEmpty(onlinePrdManager) || onlinePrdManager.contains("01")) {
                    logger.info("根据营销客户经理【{}】，有优享贷权限", managerId);
                    count = 1;
                }
            } else if ("yqdauthor".equals(authorType)) {
                //优企贷 02
                if (StringUtils.isEmpty(onlinePrdManager) || onlinePrdManager.contains("02")) {
                    logger.info("根据营销客户经理【{}】，有优企贷权限", managerId);
                    count = 1;
                }
            }
            if (count == 0) {
                logger.info("营销客户经理【{}】，无产品权限", managerId);
                //营销人员无权限则人工分配
                return "0";
            }

            /*
            3.
            (1)如果有管户客户经理则判断营销管户是否一致
                   一致:则判断机构与地址关系
                   不一致：返回走人工
            (2)无管户说明是新客户判断营销经理与地址关系
            */
            if (!StringUtils.isBlank(custMgr)) {
                logger.info("***************证件号码【{}】，管护经理不为空【{}】******************", certCode, custMgr);
                // 如果有管户客户经理则判断营销管户是否一致
                // 一致则判断机构与地址关系
                if (managerId.equals(custMgr)) {
                    logger.info("***************证件号码【{}】，管护经理【{}】与营销客户经理【{}】一致******************", certCode, custMgr, managerId);
                    // 获取管护经理所在机构
                    ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(custMgr);
                    if (Objects.isNull(manager) || Objects.isNull(manager.getData())) {
                        throw BizException.error("", "", "未查询到管护客户经理用户信息！");
                    }
                    String xfOrgid = manager.getData().getOrgId();
                    logger.info("***************证件号码【{}】，管护经理【{}】,所在机构【{}】******************", certCode, custMgr, xfOrgid);
                    //张家港本地小贷，县级市张家港
                    String townOfCity = "";
                    if ((xfOrgid.startsWith("01") && !"016000".equals(xfOrgid)) || "990000".equals(xfOrgid)) {
                        townOfCity = "张家港";
                    } else {
                        townOfCity = this.getTownOfCityByXfOrgid(xfOrgid);
                    }
                    if (StringUtil.isNotEmpty(geShuiDiZhi) && StringUtil.isNotEmpty(townOfCity) && geShuiDiZhi.contains(townOfCity)) {
                        logger.info("***************管护经理所在地【{}】与个税缴纳地【{}】一致，走自动分配******************", townOfCity, geShuiDiZhi);
                        flag = "1";
                    } else {
                        logger.info("***************管护经理所在地【{}】与个税缴纳地【{}】不一致，走人工分配******************", townOfCity, geShuiDiZhi);
                        return "0";
                    }
                } else {
                    logger.info("***************证件号码【{}】，管护经理【{}】与营销客户经理【{}】不一致，走人工分配******************", certCode, custMgr, managerId);
                    return "0";
                }
            } else {
                logger.info("***************证件号码【{}】，管护经理为空【{}】******************", certCode);
                //无管户说明是新客户判断营销经理与地址关系
                //获取推荐人所在机构
                //获取推荐人所在机构
                ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(managerId);
                logger.info("**********管户说明是新客户判断营销经理与地址关系开始,返回参数为:{}", JSON.toJSONString(manager));
                String xfOrgid = "";
                if (Objects.isNull(Objects.isNull(manager) || Objects.isNull(manager.getData()))) {
                    logger.info("未查询到营销客户经理信息！");
                } else {
                    xfOrgid = manager.getData().getOrgId();
                }
                //张家港本地小贷，县级市张家港
                String townOfCity = "";
                if ((xfOrgid.startsWith("01") && !"016000".equals(xfOrgid)) || "990000".equals(xfOrgid)) {
                    townOfCity = "张家港";
                } else {
                    townOfCity = this.getTownOfCityByXfOrgid(xfOrgid);
                }
                //geShuiDiZhi、townOfCity不为空，geShuiDiZhi包含townOfCity
                if (StringUtil.isNotEmpty(geShuiDiZhi) && StringUtil.isNotEmpty(townOfCity) && geShuiDiZhi.contains(townOfCity)) {
                    logger.info("***************营销客户经理所在地【{}】与个税缴纳地【{}】一致，自动分配******************", townOfCity, geShuiDiZhi);
                    flag = "2";
                } else {
                    logger.info("***************营销客户经理所在地【{}】与个税缴纳地【{}】不一致，走人工分配******************", townOfCity, geShuiDiZhi);
                    return "0";
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            flag = "0";
        }
        return flag;
    }

    /**
     * 实现老信贷字典表中STD_YXD_GESHIX的根据opttype获取cname的功能
     *
     * @param xfOrgid
     * @return townOfCity
     * 老信贷SQL：townOfCity=imp.getStringByCondition("select cnname from s_dic where opttype='STD_YXD_GESHIX' and enname='"+xfOrgid+"'", connection);
     * 字典表对应关系：
     * 027000	通州	STD_YXD_GESHIX	优享贷个税地址县级市
     * 077000	云龙	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027500	如皋	STD_YXD_GESHIX	优享贷个税地址县级市
     * 117100	江阴	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027100	崇川	STD_YXD_GESHIX	优享贷个税地址县级市
     * 117000	无锡	STD_YXD_GESHIX	优享贷个税地址县级市
     * 047000	即墨	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027400	南通	STD_YXD_GESHIX	优享贷个税地址县级市
     * 127106	吴江	STD_YXD_GESHIX	优享贷个税地址县级市
     * 057000	邳州	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027106	崇川	STD_YXD_GESHIX	优享贷个税地址县级市
     * 077006	云龙	STD_YXD_GESHIX	优享贷个税地址县级市
     * 117200	宜兴	STD_YXD_GESHIX	优享贷个税地址县级市
     * 097000	常熟	STD_YXD_GESHIX	优享贷个税地址县级市
     * 037000	宿豫	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027200	启东	STD_YXD_GESHIX	优享贷个税地址县级市
     * 107000	昆山	STD_YXD_GESHIX	优享贷个税地址县级市
     * 087000	丹阳	STD_YXD_GESHIX	优享贷个税地址县级市
     * 127100	吴江	STD_YXD_GESHIX	优享贷个税地址县级市
     * 067000	连云港	STD_YXD_GESHIX	优享贷个税地址县级市
     * 127000	苏州	STD_YXD_GESHIX	优享贷个税地址县级市
     * 027300	海门	STD_YXD_GESHIX	优享贷个税地址县级市
     */
    public String getTownOfCityByXfOrgid(String xfOrgid) {
        String townOfCity = "";
        if ("027000".equals(xfOrgid)) {
            townOfCity = "通州";
        } else if ("077000".equals(xfOrgid)) {
            townOfCity = "云龙";
        } else if ("027500".equals(xfOrgid)) {
            townOfCity = "如皋";
        } else if ("117100".equals(xfOrgid)) {
            townOfCity = "江阴";
        } else if ("027100".equals(xfOrgid)) {
            townOfCity = "崇川";
        } else if ("117000".equals(xfOrgid)) {
            townOfCity = "无锡";
        } else if ("047000".equals(xfOrgid)) {
            townOfCity = "即墨";
        } else if ("027400".equals(xfOrgid)) {
            townOfCity = "南通";
        } else if ("127106".equals(xfOrgid)) {
            townOfCity = "吴江";
        } else if ("057000".equals(xfOrgid)) {
            townOfCity = "邳州";
        } else if ("027106".equals(xfOrgid)) {
            townOfCity = "崇川";
        } else if ("077006".equals(xfOrgid)) {
            townOfCity = "云龙";
        } else if ("117200".equals(xfOrgid)) {
            townOfCity = "宜兴";
        } else if ("097000".equals(xfOrgid)) {
            townOfCity = "常熟";
        } else if ("037000".equals(xfOrgid)) {
            townOfCity = "宿豫";
        } else if ("027200".equals(xfOrgid)) {
            townOfCity = "启东";
        } else if ("107000".equals(xfOrgid)) {
            townOfCity = "昆山";
        } else if ("087000".equals(xfOrgid)) {
            townOfCity = "丹阳";
        } else if ("127100".equals(xfOrgid)) {
            townOfCity = "吴江";
        } else if ("067000".equals(xfOrgid)) {
            townOfCity = "连云港";
        } else if ("127000".equals(xfOrgid)) {
            townOfCity = "苏州";
        } else if ("027300".equals(xfOrgid)) {
            townOfCity = "海门";
        }
        return townOfCity;
    }
}
