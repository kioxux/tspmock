package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayWayChg
 * @类描述: iqp_repay_way_chg数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-14 15:29:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpRepayWayChgDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	/** 业务流水号 **/
	private String iqpSerno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 发放日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 原还款方式 STD_ZB_REPAY_TYP **/
	private String oldRepayMode;
	
	/** 原停本付息期间 STD_ZB_PINT_TERM **/
	private String oldStopPintTerm;
	
	/** 原还款间隔周期 STD_ZB_REPAY_TERM **/
	private String oldRepayTerm;
	
	/** 原还款间隔 STD_ZB_REPAY_SPACE **/
	private String oldRepaySpace;
	
	/** 原还款日 **/
	private java.math.BigDecimal oldRepayDate;
	
	/** 还款方式 STD_ZB_REPAY_TYP **/
	private String repayMode;
	
	/** 停本付息期间 STD_ZB_PINT_TERM **/
	private String stopPintTerm;
	
	/** 还款间隔周期 STD_ZB_REPAY_TERM **/
	private String repayTerm;
	
	/** 还款间隔 STD_ZB_REPAY_SPACE **/
	private String repaySpace;
	
	/** 还款日 **/
	private String repayDate;
	
	/** 调整原因 **/
	private String changeResn;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 原还款日确定规则 STD_ZB_REPAY_RULE **/
	private String oldRepayRule;
	
	/** 原还款日类型 STD_ZB_REPAY_DT_TYPE **/
	private String oldRepayDtType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oldRepayMode
	 */
	public void setOldRepayMode(String oldRepayMode) {
		this.oldRepayMode = oldRepayMode == null ? null : oldRepayMode.trim();
	}
	
    /**
     * @return OldRepayMode
     */	
	public String getOldRepayMode() {
		return this.oldRepayMode;
	}
	
	/**
	 * @param oldStopPintTerm
	 */
	public void setOldStopPintTerm(String oldStopPintTerm) {
		this.oldStopPintTerm = oldStopPintTerm == null ? null : oldStopPintTerm.trim();
	}
	
    /**
     * @return OldStopPintTerm
     */	
	public String getOldStopPintTerm() {
		return this.oldStopPintTerm;
	}
	
	/**
	 * @param oldRepayTerm
	 */
	public void setOldRepayTerm(String oldRepayTerm) {
		this.oldRepayTerm = oldRepayTerm == null ? null : oldRepayTerm.trim();
	}
	
    /**
     * @return OldRepayTerm
     */	
	public String getOldRepayTerm() {
		return this.oldRepayTerm;
	}
	
	/**
	 * @param oldRepaySpace
	 */
	public void setOldRepaySpace(String oldRepaySpace) {
		this.oldRepaySpace = oldRepaySpace == null ? null : oldRepaySpace.trim();
	}
	
    /**
     * @return OldRepaySpace
     */	
	public String getOldRepaySpace() {
		return this.oldRepaySpace;
	}
	
	/**
	 * @param oldRepayDate
	 */
	public void setOldRepayDate(java.math.BigDecimal oldRepayDate) {
		this.oldRepayDate = oldRepayDate;
	}
	
    /**
     * @return OldRepayDate
     */	
	public java.math.BigDecimal getOldRepayDate() {
		return this.oldRepayDate;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode == null ? null : repayMode.trim();
	}
	
    /**
     * @return RepayMode
     */	
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param stopPintTerm
	 */
	public void setStopPintTerm(String stopPintTerm) {
		this.stopPintTerm = stopPintTerm == null ? null : stopPintTerm.trim();
	}
	
    /**
     * @return StopPintTerm
     */	
	public String getStopPintTerm() {
		return this.stopPintTerm;
	}
	
	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm == null ? null : repayTerm.trim();
	}
	
    /**
     * @return RepayTerm
     */	
	public String getRepayTerm() {
		return this.repayTerm;
	}
	
	/**
	 * @param repaySpace
	 */
	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace == null ? null : repaySpace.trim();
	}
	
    /**
     * @return RepaySpace
     */	
	public String getRepaySpace() {
		return this.repaySpace;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate == null ? null : repayDate.trim();
	}
	
    /**
     * @return RepayDate
     */	
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param changeResn
	 */
	public void setChangeResn(String changeResn) {
		this.changeResn = changeResn == null ? null : changeResn.trim();
	}
	
    /**
     * @return ChangeResn
     */	
	public String getChangeResn() {
		return this.changeResn;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param oldRepayRule
	 */
	public void setOldRepayRule(String oldRepayRule) {
		this.oldRepayRule = oldRepayRule == null ? null : oldRepayRule.trim();
	}
	
    /**
     * @return OldRepayRule
     */	
	public String getOldRepayRule() {
		return this.oldRepayRule;
	}
	
	/**
	 * @param oldRepayDtType
	 */
	public void setOldRepayDtType(String oldRepayDtType) {
		this.oldRepayDtType = oldRepayDtType == null ? null : oldRepayDtType.trim();
	}
	
    /**
     * @return OldRepayDtType
     */	
	public String getOldRepayDtType() {
		return this.oldRepayDtType;
	}


}