package cn.com.yusys.yusp.service.client.bsp.ciis2nd.credzb;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2Ciis2ndClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：指标通用接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CredzbService {
    private static final Logger logger = LoggerFactory.getLogger(CredzbService.class);

    // 1）注入：BSP封装调用二代征信系统的接口处理类（credzb）
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;

    /**
     * 业务逻辑处理方法：指标通用接口
     *
     * @param credzbReqDto
     * @return
     */
    public CredzbRespDto credzb(CredzbReqDto credzbReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, JSON.toJSONString(credzbReqDto));
        ResultDto<CredzbRespDto> credzbResultDto = dscms2Ciis2ndClientService.credzb(credzbReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, JSON.toJSONString(credzbResultDto));
        String credzbCode = Optional.ofNullable(credzbResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String credzbMeesage = Optional.ofNullable(credzbResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CredzbRespDto credzbRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, credzbResultDto.getCode())) {
            //  获取相关的值并解析
            credzbRespDto = credzbResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(credzbCode, credzbMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value);
        return credzbRespDto;
    }


}
