package cn.com.yusys.yusp.service.client.bsp.lsnp.Lsnp01;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.Lsnp01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.Lsnp01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2LsnpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author wangyk
 * @version 1.0.0
 * @date 2021/6/29 10:33
 * @desc 零售内评接口
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Lsnp01Service {
    private static final Logger logger = LoggerFactory.getLogger(Lsnp01Service.class);
    // 1）注入：BSP封装调用核心系统的接口
    @Autowired
    private Dscms2LsnpClientService dscms2LsnpClientService;

    /**
     * @param lsnp01ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.Lsnp01.Lsnp01RespDto
     * @author 王玉坤
     * @date 2021/6/22 20:13
     * @version 1.0.0
     * @desc 零售内评接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Lsnp01RespDto lsnp01(Lsnp01ReqDto lsnp01ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value, JSON.toJSONString(lsnp01ReqDto));
        ResultDto<Lsnp01RespDto> lsnp01RespDtoResultDto = dscms2LsnpClientService.lsnp01(lsnp01ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value, JSON.toJSONString(lsnp01RespDtoResultDto));
        String lsnp01Code = Optional.ofNullable(lsnp01RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String lsnp01Meesage = Optional.ofNullable(lsnp01RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Lsnp01RespDto lsnp01RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, lsnp01RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            lsnp01RespDto = lsnp01RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, lsnp01Code, lsnp01Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value);
        return lsnp01RespDto;
    }
}
