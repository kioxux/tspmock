package cn.com.yusys.yusp.service.server.xdxw0048;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0048.req.Xdxw0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0048.resp.Xdxw0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtModelApprResultInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.CusLstZxdService;
import cn.com.yusys.yusp.service.LmtSurveyReportMainInfoService;
import cn.com.yusys.yusp.service.client.bsp.rircp.znsp04.Znsp04Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称:
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021/6/8 14:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0048Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0048Service.class);

    @Resource
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper; //
    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper; // 客户授信调查主表
    @Resource
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper; // 调查报告基本信息
    @Resource
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Resource
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    // 其他模块
    @Autowired
    private CusLstZxdService cusLstZxdService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private Znsp04Service znsp04Service;


    /**
     * 业务场景: 在增享贷2.0风控模型A跑批审批通过之后
     * 1、模型结果落表xd_jd_model_result  新信贷 模型审批结果表 LMT_MODEL_APPR_RESULT_INFO
     * 2、审批拒绝，把主调查表和白名单内状态置为拒绝；
     * 审批通过，插入批复表（授信批复表 LMT_CRD_REPLY_INFO），发送智能审贷znsp04，智能审贷通过后把主调查表和白名单内状态置为通过
     * 3、调查报告申请表修改金额、利率、期限  （调查报告基本信息 LMT_SURVEY_REPORT_BASIC_INFO）
     *
     * @param xdxw0048DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0048DataRespDto xdxw0048(Xdxw0048DataReqDto xdxw0048DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataReqDto));
        Xdxw0048DataRespDto xdxw0048DataRespDto = new Xdxw0048DataRespDto();
        try {
            String apply_no = xdxw0048DataReqDto.getApply_no();//业务唯一编号

            // 客户授信调查主表
            logger.info("*****XDXW0048:1-审批通过,查询户授信调查主表;查询条件:" + apply_no); // "apply_no":"EDSQ2106300105693"
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByBizserno(apply_no); // 根据bizSerno
            if (null == lmtSurveyReportMainInfo) {
                throw BizException.error(null, DscmsBizXwEnum.RETURN_MESSAGE_4802.key, DscmsBizXwEnum.RETURN_MESSAGE_4802.value);
            }
            // 调查报告基本信息
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoMapper.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (null == lmtSurveyReportBasicInfo) {
                throw BizException.error(null, DscmsBizXwEnum.RETURN_MESSAGE_4803.key, DscmsBizXwEnum.RETURN_MESSAGE_4803.value);
            }

            logger.info("根据名单流水号【{}】查询增享贷白名单信息开始！", lmtSurveyReportMainInfo.getListSerno());
            CusLstZxd cusLstZxd = cusLstZxdService.selectByPrimaryKey(lmtSurveyReportMainInfo.getListSerno());
            if (null == cusLstZxd) {
                throw BizException.error(null, "9999", "未查询到白名单信息！");
            }
            logger.info("根据名单流水号【{}】查询增享贷白名单信息而结束！", lmtSurveyReportMainInfo.getListSerno());
            // 查询模型审批结果表是否存在对应记录
            logger.info("**********XDXW0048**查询模型审批结果表是否存在对应记录开始,查询参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo.getSurveySerno()));
            LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoMapper.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            logger.info("**********XDXW0048**查询模型审批结果表是否存在对应记录结束,返回结果为:{}", JSON.toJSONString(lmtModelApprResultInfo));


            String model_result = xdxw0048DataReqDto.getModel_result();//模型结果    "model_result":"3"
            String model_appr_time = xdxw0048DataReqDto.getModel_appr_time();//模型审批时间 "model_appr_time":"2021-06-30"
            String opinion = xdxw0048DataReqDto.getOpinion();//模型意见 "opinion":"程序未知错误"
            String cus_id = xdxw0048DataReqDto.getCus_id();//客户号 "cus_id":"2034108116"
            String loan_term = xdxw0048DataReqDto.getLoan_term();//贷款期限 "loan_term":"21"
            BigDecimal final_amount = xdxw0048DataReqDto.getFinal_amount();//最终金额 "final_amount":"21"
            BigDecimal final_rate = xdxw0048DataReqDto.getFinal_rate();//最终利率 "final_rate":"0.12"

            // 获取当前业务日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 1-审批通过 2-审批拒绝 3-审批异常
            if (DscmsBizXwEnum.MODEL_RST_STATUS_1.key.equals(model_result)) {
                int dayCount = Integer.valueOf(loan_term);
                // 批复结束日期 = 当前日期(openday) +贷款期限(loan_term)(月)
                String endDate = DateUtils.addMonth(openDay, DateFormatEnum.DEFAULT.getValue(), dayCount);
                // 生成 LMT_CRD_REPLY_INFO 授信批复表数据
                logger.info("*****XDXW0048:生成 LMT_CRD_REPLY_INFO 授信批复表数据;获取当前业务日期:" + openDay + ";批复结束日期:" + endDate);
                LmtCrdReplyInfo lmtCrdReplyInfo = new LmtCrdReplyInfo();
                lmtCrdReplyInfo.setReplySerno(lmtSurveyReportMainInfo.getBizSerno());
                lmtCrdReplyInfo.setSurveySerno(lmtSurveyReportMainInfo.getSurveySerno());
                lmtCrdReplyInfo.setPrdName(lmtSurveyReportMainInfo.getPrdName());
                lmtCrdReplyInfo.setPrdId(lmtSurveyReportMainInfo.getPrdId());
                lmtCrdReplyInfo.setCusId(lmtSurveyReportMainInfo.getCusId());
                lmtCrdReplyInfo.setCusName(lmtSurveyReportMainInfo.getCusName());
                lmtCrdReplyInfo.setCertType(lmtSurveyReportMainInfo.getCertType());
                lmtCrdReplyInfo.setCertCode(lmtSurveyReportMainInfo.getCertCode());
                lmtCrdReplyInfo.setCusLvl(StringUtils.EMPTY);
                lmtCrdReplyInfo.setGuarMode(lmtSurveyReportBasicInfo.getGuarMode());
                lmtCrdReplyInfo.setReplyAmt(final_amount);
                lmtCrdReplyInfo.setTermType("M");//默认 月
                lmtCrdReplyInfo.setAppTerm(Integer.valueOf(loan_term));
//                lmtCrdReplyInfo.setLmtGraper();
                lmtCrdReplyInfo.setReplyStatus("01");
                lmtCrdReplyInfo.setReplyStartDate(openDay);
                lmtCrdReplyInfo.setReplyEndDate(endDate);
                lmtCrdReplyInfo.setCurType("CNY");
                lmtCrdReplyInfo.setExecRateYear(final_rate);
                lmtCrdReplyInfo.setRepayMode(lmtSurveyReportBasicInfo.getRepayMode());
                lmtCrdReplyInfo.setLimitType("02");
//                lmtCrdReplyInfo.setCurtLoanAmt();
                lmtCrdReplyInfo.setIsBeEntrustedPay(StringUtils.EMPTY);
                lmtCrdReplyInfo.setLoanCondFlg(StringUtils.EMPTY);
                lmtCrdReplyInfo.setTruPayType(StringUtils.EMPTY);
                lmtCrdReplyInfo.setApprType(StringUtils.EMPTY);
                lmtCrdReplyInfo.setAgriFlag(StringUtils.EMPTY);
                lmtCrdReplyInfo.setApprMode(StringUtils.EMPTY);
                lmtCrdReplyInfo.setBelgLine("01");
                lmtCrdReplyInfo.setOprType(lmtSurveyReportMainInfo.getOprType());
                lmtCrdReplyInfo.setManagerBrId(lmtSurveyReportMainInfo.getManagerBrId());
                lmtCrdReplyInfo.setManagerId(lmtSurveyReportMainInfo.getManagerId());
                lmtCrdReplyInfo.setInputId(lmtSurveyReportMainInfo.getInputId());
                lmtCrdReplyInfo.setInputBrId(lmtSurveyReportMainInfo.getInputBrId());
                lmtCrdReplyInfo.setInputDate(openDay);
                lmtCrdReplyInfo.setUpdId(lmtSurveyReportMainInfo.getUpdId());
                lmtCrdReplyInfo.setUpdBrId(lmtSurveyReportMainInfo.getUpdBrId());
                lmtCrdReplyInfo.setUpdDate(openDay);
                lmtCrdReplyInfo.setCreateTime(DateUtils.getCurrDate());
                lmtCrdReplyInfo.setUpdateTime(DateUtils.getCurrDate());
                lmtCrdReplyInfo.setIsWxbxd(StringUtils.EMPTY);
                lmtCrdReplyInfo.setXdOrigiContNo(StringUtils.EMPTY);
                lmtCrdReplyInfoMapper.insert(lmtCrdReplyInfo);

                // 客户授信调查主表
                lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997); // 通过
                lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批通过,更新客户授信调查主表开始,更新参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportMainInfo);
                // 更新调查报告基本信息
                lmtSurveyReportBasicInfo.setAdviceAmt(final_amount);
                lmtSurveyReportBasicInfo.setAdviceRate(final_rate);
                lmtSurveyReportBasicInfo.setAdviceTerm(loan_term);
                lmtSurveyReportBasicInfo.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批通过,更新调查报告基本信息开始,更新参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                lmtSurveyReportBasicInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportBasicInfo);

                // 更新白名单处理状态为办结，模型审批状态为征信模型通过
                cusLstZxd.setSerno(lmtSurveyReportMainInfo.getListSerno());
                cusLstZxd.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_05.key);  // 通过
                cusLstZxd.setUpdateTime(DateUtils.getCurrDate());
                cusLstZxdService.updateSelective(cusLstZxd);

                // 模型审批状态为征信模型通过
                lmtModelApprResultInfo.setModelRstStatus(DscmsBizXwEnum.XW_MODEL_RST_STATUS_40.key);
                lmtModelApprResultInfo.setModelApprTime(model_appr_time);
                lmtModelApprResultInfo.setModelAmt(final_amount);
                lmtModelApprResultInfo.setModelRate(final_rate);
                lmtModelApprResultInfo.setJojc(StringUtils.EMPTY);
                lmtModelApprResultInfo.setModelAdvice(opinion);
                lmtModelApprResultInfo.setModelFstLmt(new BigDecimal(0));
                lmtModelApprResultInfo.setUpdateTime(DateUtils.getCurrDate());
                lmtModelApprResultInfoMapper.updateByPrimaryKey(lmtModelApprResultInfo);

                // 参考老信贷代码：KeyedCollection retColl = this.sendToZnsp(appColl, kColl, context, connection);
                // 再发送智能审批系统znsp04接口
                logger.info("*****XDXW0048:再发送智能审批系统znsp04接口;调查流水:" + apply_no);
                Znsp04ReqDto znsp04ReqDto = new Znsp04ReqDto();
                znsp04ReqDto.setSurvey_serno(apply_no);
                znsp04ReqDto.setApply_type("01");
                znsp04ReqDto.setPrd_id("SC010014");
                znsp04ReqDto.setPrd_name("增享贷");

                // 根据证件号查询客户信息 根据cus_id获取cus_name,cert_code
                logger.info("*****XDXW0048:根据证件号查询客户信息 根据cus_id获取cus_name,cert_code;客户号:" + cus_id);
                String cus_name = lmtSurveyReportMainInfo.getCusName();
                String cert_code = lmtSurveyReportMainInfo.getCertCode();
                znsp04ReqDto.setCus_name(cus_name);
                znsp04ReqDto.setIs_sjsh("");
                znsp04ReqDto.setLoan_type("10");
                znsp04ReqDto.setCus_id(cus_id);
                znsp04ReqDto.setCert_no(cert_code);
                znsp04ReqDto.setZb_manager_id(lmtSurveyReportMainInfo.getManagerId());
                znsp04ReqDto.setXb_manager_id("");
                znsp04ReqDto.setLoan_amt(final_amount);
                znsp04ReqDto.setReality_ir_y(final_rate);
                znsp04ReqDto.setLoan_term(new BigDecimal(loan_term));
                znsp04ReqDto.setGuara_way(lmtSurveyReportBasicInfo.getGuarMode());
                znsp04ReqDto.setRepay_way(lmtSurveyReportBasicInfo.getRepayMode());
                logger.info("***********XDXW0048:审批通过,送智能审批系统znsp04接口开始,更新参数为:{}", JSON.toJSONString(znsp04ReqDto));
                Znsp04RespDto znsp04RespDto = znsp04Service.znsp04(znsp04ReqDto);
                logger.info("***********XDXW0048:审批通过,送智能审批系统znsp04接口结束,返回结果为:{}", JSON.toJSONString(znsp04RespDto));

                //占用额度
                logger.info("***********XDXW0048:审批通过,调用cmislmt001接口开始,更新参数为:{}", JSON.toJSONString(lmtCrdReplyInfo));
                CmisLmt0001RespDto cmisLmt0001RespDto = cmisBizXwCommonService.sendCmisLmt0001(lmtCrdReplyInfo);
                logger.info("***********XDXW0048:审批通过,调用cmislmt001接口结束,返回结果为:{}", JSON.toJSONString(cmisLmt0001RespDto));

                xdxw0048DataRespDto.setSerno(apply_no);
                xdxw0048DataRespDto.setEnd_date(endDate);
            } else if (DscmsBizXwEnum.MODEL_RST_STATUS_2.key.equals(model_result)) { // 审批拒绝
                lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998); // 拒绝
                lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批拒绝,更新调查报告基本信息开始,更新参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportMainInfo);

                // 更新白名单处理状态为办结，模型审批状态为征信模型通过
                cusLstZxd.setSerno(lmtSurveyReportMainInfo.getListSerno());
                cusLstZxd.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_04.key);  // 拒绝
                cusLstZxd.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批拒绝,更新增享贷白名单信息开始,更新参数为:{}", JSON.toJSONString(cusLstZxd));
                cusLstZxdService.updateSelective(cusLstZxd);

                // 模型审批状态为征信模型拒绝
                lmtModelApprResultInfo.setModelRstStatus(DscmsBizXwEnum.XW_MODEL_RST_STATUS_50.key);
                lmtModelApprResultInfo.setModelApprTime(model_appr_time);
                lmtModelApprResultInfo.setModelAmt(final_amount);
                lmtModelApprResultInfo.setModelRate(final_rate);
                lmtModelApprResultInfo.setJojc(StringUtils.EMPTY);
                lmtModelApprResultInfo.setModelAdvice(opinion);
                lmtModelApprResultInfo.setModelFstLmt(new BigDecimal(0));
                lmtModelApprResultInfo.setUpdateTime(DateUtils.getCurrDate());
                lmtModelApprResultInfoMapper.updateByPrimaryKey(lmtModelApprResultInfo);
            } else if (DscmsBizXwEnum.MODEL_RST_STATUS_3.key.equals(model_result)) {
                // 不做操作（模型结果记录已插入）
                logger.info("***********XDXW0048:审批异常***********");
                lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998); // 拒绝
                lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批拒绝,更新调查报告基本信息开始,更新参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportMainInfo);

                // 更新白名单处理状态为办结，模型审批状态为征信模型通过
                cusLstZxd.setSerno(lmtSurveyReportMainInfo.getListSerno());
                cusLstZxd.setApplyStatus(DscmsBizXwEnum.XW_ZXD_APPLY_STATUS_04.key);  // 拒绝
                cusLstZxd.setUpdateTime(DateUtils.getCurrDate());
                logger.info("***********XDXW0048:审批拒绝,更新增享贷白名单信息开始,更新参数为:{}", JSON.toJSONString(cusLstZxd));
                cusLstZxdService.updateSelective(cusLstZxd);

                // 模型审批状态为征信模型异常
                lmtModelApprResultInfo.setModelRstStatus(DscmsBizXwEnum.XW_MODEL_RST_STATUS_30.key);
                lmtModelApprResultInfo.setModelApprTime(model_appr_time);
                lmtModelApprResultInfo.setModelAmt(final_amount);
                lmtModelApprResultInfo.setModelRate(final_rate);
                lmtModelApprResultInfo.setJojc(StringUtils.EMPTY);
                lmtModelApprResultInfo.setModelAdvice(opinion);
                lmtModelApprResultInfo.setModelFstLmt(new BigDecimal(0));
                lmtModelApprResultInfo.setUpdateTime(DateUtils.getCurrDate());
                lmtModelApprResultInfoMapper.updateByPrimaryKey(lmtModelApprResultInfo);
            } else {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataRespDto));
        return xdxw0048DataRespDto;
    }
}
