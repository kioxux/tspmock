/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.RepayBillRelDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRel;
import cn.com.yusys.yusp.service.PvpLoanAppRepayBillRelService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 72908
 * @创建时间: 2021-05-06 21:42:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "偿还借据")
@RestController
@RequestMapping("/api/pvploanapprepaybillrel")
public class PvpLoanAppRepayBillRelResource {
    @Autowired
    private PvpLoanAppRepayBillRelService pvpLoanAppRepayBillRelService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpLoanAppRepayBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpLoanAppRepayBillRel> list = pvpLoanAppRepayBillRelService.selectAll(queryModel);
        return new ResultDto<List<PvpLoanAppRepayBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpLoanAppRepayBillRel>> index(QueryModel queryModel) {
        List<PvpLoanAppRepayBillRel> list = pvpLoanAppRepayBillRelService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppRepayBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<PvpLoanAppRepayBillRel> show(@PathVariable("pkId") String pkId) {
        PvpLoanAppRepayBillRel pvpLoanAppRepayBillRel = pvpLoanAppRepayBillRelService.selectByPrimaryKey(pkId);
        return new ResultDto<PvpLoanAppRepayBillRel>(pvpLoanAppRepayBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpLoanAppRepayBillRel> create(@RequestBody PvpLoanAppRepayBillRel pvpLoanAppRepayBillRel) throws URISyntaxException {
        pvpLoanAppRepayBillRelService.insert(pvpLoanAppRepayBillRel);
        return new ResultDto<PvpLoanAppRepayBillRel>(pvpLoanAppRepayBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpLoanAppRepayBillRel pvpLoanAppRepayBillRel) throws URISyntaxException {
        int result = pvpLoanAppRepayBillRelService.update(pvpLoanAppRepayBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = pvpLoanAppRepayBillRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleterepaybillrel
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleterepaybillrel/{billNo}")
    protected ResultDto<Integer> deleterepaybillrel(@PathVariable("billNo") String billNo) {
        int result = pvpLoanAppRepayBillRelService.deleteByBillNo(billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = pvpLoanAppRepayBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

//    /**
//     * 合同选择框查询方法
//     * @param params
//     * @return
//     */
//    @ApiOperation("偿还借据分页查询")
//    @PostMapping("/queryRepayBillRel")
//    protected ResultDto<List<RepayBillRelDto>> queryRepayBillRel22(@RequestBody Map params) throws URISyntaxException {
//        HashMap queryMap = new HashMap<String, String>();
//        List<RepayBillRelDto> result = pvpLoanAppRepayBillRelService.queryRepayBillRel(queryMap);
//        return new ResultDto<List<RepayBillRelDto>>(result);
//    }

    @ApiOperation("偿还借据分页查询")
    @PostMapping("/queryRepayBillRel")
    protected ResultDto<List<RepayBillRelDto>> queryRepayBillRel(@RequestBody QueryModel queryModel) {
        List<RepayBillRelDto> list = pvpLoanAppRepayBillRelService.queryRepayBillRel(queryModel);
        return new ResultDto<List<RepayBillRelDto>>(list);
    }

    @ApiOperation("偿还借据引入贷款台账")
    @PostMapping("/leadInAccLoan")
    protected ResultDto<Object> leadInAccLoan(@RequestBody Map params) throws URISyntaxException {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("billNo",params.get("billNo"));
        queryMap.put("pvpSerno",params.get("pvpSerno"));
        Map result = pvpLoanAppRepayBillRelService.leadInAccLoan(queryMap);
        return new ResultDto<Object>(result);
    }
}