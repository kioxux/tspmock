/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopProAccInfo
 * @类描述: coop_pro_acc_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-14 23:45:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_pro_acc_info")
public class CoopProAccInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 项目编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PRO_NO")
	private String proNo;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方类型 **/
	@Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 10)
	private String partnerType;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 120)
	private String proName;
	
	/** 项目类型 **/
	@Column(name = "PRO_TYPE", unique = false, nullable = true, length = 10)
	private String proType;
	
	/** 项目额度（元） **/
	@Column(name = "PRO_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proLmt;
	
	/** 项目状态 **/
	@Column(name = "PRO_STATUS", unique = false, nullable = true, length = 10)
	private String proStatus;
	
	/** 是否通过销售公司销售 **/
	@Column(name = "IS_SALE_CPRT_SALE", unique = false, nullable = true, length = 10)
	private String isSaleCprtSale;
	
	/** 销售公司名称 **/
	@Column(name = "SALE_CPRT_NAME", unique = false, nullable = true, length = 120)
	private String saleCprtName;
	
	/** 销售公司电话 **/
	@Column(name = "SALE_CPRT_PHONE", unique = false, nullable = true, length = 20)
	private String saleCprtPhone;
	
	/** 负责人名称 **/
	@Column(name = "CHIEF_NAME", unique = false, nullable = true, length = 120)
	private String chiefName;
	
	/** 负责人电话 **/
	@Column(name = "CHIEF_PHONE", unique = false, nullable = true, length = 20)
	private String chiefPhone;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 60)
	private String certCode;
	
	/** 项目总投资（元） **/
	@Column(name = "PRO_TOTAL_INVEST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proTotalInvest;
	
	/** 项目权利人名称 **/
	@Column(name = "PRO_WRR_PERSON_NAME", unique = false, nullable = true, length = 120)
	private String proWrrPersonName;
	
	/** 项目总销售收入(元) **/
	@Column(name = "PRO_TOTAL_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proTotalSaleIncome;
	
	/** 其他部分销售总价(元) **/
	@Column(name = "OTHER_PART_SALE_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherPartSaleTotal;
	
	/** 项目开工时间 **/
	@Column(name = "PRO_START_DATE", unique = false, nullable = true, length = 20)
	private String proStartDate;
	
	/** 项目竣工时间 **/
	@Column(name = "PRO_END_DATE", unique = false, nullable = true, length = 20)
	private String proEndDate;
	
	/** 交付日期 **/
	@Column(name = "DELIVER_DATE", unique = false, nullable = true, length = 20)
	private String deliverDate;
	
	/** 项目地址位置A **/
	@Column(name = "PRO_ADDR_PLACE_A", unique = false, nullable = true, length = 200)
	private String proAddrPlaceA;
	
	/** 项目地址位置B **/
	@Column(name = "PRO_ADDR_PLACE_B", unique = false, nullable = true, length = 200)
	private String proAddrPlaceB;
	
	/** 项目地址位置C **/
	@Column(name = "PRO_ADDR_PLACE_C", unique = false, nullable = true, length = 200)
	private String proAddrPlaceC;
	
	/** 项目具体位置 **/
	@Column(name = "PRO_DETAIL_PLACE", unique = false, nullable = true, length = 200)
	private String proDetailPlace;
	
	/** 占地面积（㎡） **/
	@Column(name = "OCCUP_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal occupSqu;
	
	/** 住宅建筑面积（㎡） **/
	@Column(name = "RESI_ARCH_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal resiArchSqu;
	
	/** 工业建筑面积（㎡） **/
	@Column(name = "INDT_ARCH_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal indtArchSqu;
	
	/** 住宅销售均价（元/㎡） **/
	@Column(name = "RESI_SALE_AVG_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal resiSaleAvgPrice;
	
	/** 工业销售均价（元/㎡） **/
	@Column(name = "INDT_SALE_AVG_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal indtSaleAvgPrice;
	
	/** 总建筑面积（㎡） **/
	@Column(name = "TOTAL_ARCH_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalArchSqu;
	
	/** 商业建筑面积（㎡） **/
	@Column(name = "COMM_ARCH_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commArchSqu;
	
	/** 其他建筑面积（㎡） **/
	@Column(name = "OTHER_ARCH_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherArchSqu;
	
	/** 商业销售均价（元/㎡） **/
	@Column(name = "COMM_SALE_AVG_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commSaleAvgPrice;
	
	/** 其他销售均价（元/㎡） **/
	@Column(name = "OTHER_SALE_AVG_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherSaleAvgPrice;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="inputIdName")
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="inputBrIdName" )
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	
    /**
     * @return partnerType
     */
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType;
	}
	
    /**
     * @return proType
     */
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param proLmt
	 */
	public void setProLmt(java.math.BigDecimal proLmt) {
		this.proLmt = proLmt;
	}
	
    /**
     * @return proLmt
     */
	public java.math.BigDecimal getProLmt() {
		return this.proLmt;
	}
	
	/**
	 * @param proStatus
	 */
	public void setProStatus(String proStatus) {
		this.proStatus = proStatus;
	}
	
    /**
     * @return proStatus
     */
	public String getProStatus() {
		return this.proStatus;
	}
	
	/**
	 * @param isSaleCprtSale
	 */
	public void setIsSaleCprtSale(String isSaleCprtSale) {
		this.isSaleCprtSale = isSaleCprtSale;
	}
	
    /**
     * @return isSaleCprtSale
     */
	public String getIsSaleCprtSale() {
		return this.isSaleCprtSale;
	}
	
	/**
	 * @param saleCprtName
	 */
	public void setSaleCprtName(String saleCprtName) {
		this.saleCprtName = saleCprtName;
	}
	
    /**
     * @return saleCprtName
     */
	public String getSaleCprtName() {
		return this.saleCprtName;
	}
	
	/**
	 * @param saleCprtPhone
	 */
	public void setSaleCprtPhone(String saleCprtPhone) {
		this.saleCprtPhone = saleCprtPhone;
	}
	
    /**
     * @return saleCprtPhone
     */
	public String getSaleCprtPhone() {
		return this.saleCprtPhone;
	}
	
	/**
	 * @param chiefName
	 */
	public void setChiefName(String chiefName) {
		this.chiefName = chiefName;
	}
	
    /**
     * @return chiefName
     */
	public String getChiefName() {
		return this.chiefName;
	}
	
	/**
	 * @param chiefPhone
	 */
	public void setChiefPhone(String chiefPhone) {
		this.chiefPhone = chiefPhone;
	}
	
    /**
     * @return chiefPhone
     */
	public String getChiefPhone() {
		return this.chiefPhone;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param proTotalInvest
	 */
	public void setProTotalInvest(java.math.BigDecimal proTotalInvest) {
		this.proTotalInvest = proTotalInvest;
	}
	
    /**
     * @return proTotalInvest
     */
	public java.math.BigDecimal getProTotalInvest() {
		return this.proTotalInvest;
	}
	
	/**
	 * @param proWrrPersonName
	 */
	public void setProWrrPersonName(String proWrrPersonName) {
		this.proWrrPersonName = proWrrPersonName;
	}
	
    /**
     * @return proWrrPersonName
     */
	public String getProWrrPersonName() {
		return this.proWrrPersonName;
	}
	
	/**
	 * @param proTotalSaleIncome
	 */
	public void setProTotalSaleIncome(java.math.BigDecimal proTotalSaleIncome) {
		this.proTotalSaleIncome = proTotalSaleIncome;
	}
	
    /**
     * @return proTotalSaleIncome
     */
	public java.math.BigDecimal getProTotalSaleIncome() {
		return this.proTotalSaleIncome;
	}
	
	/**
	 * @param otherPartSaleTotal
	 */
	public void setOtherPartSaleTotal(java.math.BigDecimal otherPartSaleTotal) {
		this.otherPartSaleTotal = otherPartSaleTotal;
	}
	
    /**
     * @return otherPartSaleTotal
     */
	public java.math.BigDecimal getOtherPartSaleTotal() {
		return this.otherPartSaleTotal;
	}
	
	/**
	 * @param proStartDate
	 */
	public void setProStartDate(String proStartDate) {
		this.proStartDate = proStartDate;
	}
	
    /**
     * @return proStartDate
     */
	public String getProStartDate() {
		return this.proStartDate;
	}
	
	/**
	 * @param proEndDate
	 */
	public void setProEndDate(String proEndDate) {
		this.proEndDate = proEndDate;
	}
	
    /**
     * @return proEndDate
     */
	public String getProEndDate() {
		return this.proEndDate;
	}
	
	/**
	 * @param deliverDate
	 */
	public void setDeliverDate(String deliverDate) {
		this.deliverDate = deliverDate;
	}
	
    /**
     * @return deliverDate
     */
	public String getDeliverDate() {
		return this.deliverDate;
	}
	
	/**
	 * @param proAddrPlaceA
	 */
	public void setProAddrPlaceA(String proAddrPlaceA) {
		this.proAddrPlaceA = proAddrPlaceA;
	}
	
    /**
     * @return proAddrPlaceA
     */
	public String getProAddrPlaceA() {
		return this.proAddrPlaceA;
	}
	
	/**
	 * @param proAddrPlaceB
	 */
	public void setProAddrPlaceB(String proAddrPlaceB) {
		this.proAddrPlaceB = proAddrPlaceB;
	}
	
    /**
     * @return proAddrPlaceB
     */
	public String getProAddrPlaceB() {
		return this.proAddrPlaceB;
	}
	
	/**
	 * @param proAddrPlaceC
	 */
	public void setProAddrPlaceC(String proAddrPlaceC) {
		this.proAddrPlaceC = proAddrPlaceC;
	}
	
    /**
     * @return proAddrPlaceC
     */
	public String getProAddrPlaceC() {
		return this.proAddrPlaceC;
	}
	
	/**
	 * @param proDetailPlace
	 */
	public void setProDetailPlace(String proDetailPlace) {
		this.proDetailPlace = proDetailPlace;
	}
	
    /**
     * @return proDetailPlace
     */
	public String getProDetailPlace() {
		return this.proDetailPlace;
	}
	
	/**
	 * @param occupSqu
	 */
	public void setOccupSqu(java.math.BigDecimal occupSqu) {
		this.occupSqu = occupSqu;
	}
	
    /**
     * @return occupSqu
     */
	public java.math.BigDecimal getOccupSqu() {
		return this.occupSqu;
	}
	
	/**
	 * @param resiArchSqu
	 */
	public void setResiArchSqu(java.math.BigDecimal resiArchSqu) {
		this.resiArchSqu = resiArchSqu;
	}
	
    /**
     * @return resiArchSqu
     */
	public java.math.BigDecimal getResiArchSqu() {
		return this.resiArchSqu;
	}
	
	/**
	 * @param indtArchSqu
	 */
	public void setIndtArchSqu(java.math.BigDecimal indtArchSqu) {
		this.indtArchSqu = indtArchSqu;
	}
	
    /**
     * @return indtArchSqu
     */
	public java.math.BigDecimal getIndtArchSqu() {
		return this.indtArchSqu;
	}
	
	/**
	 * @param resiSaleAvgPrice
	 */
	public void setResiSaleAvgPrice(java.math.BigDecimal resiSaleAvgPrice) {
		this.resiSaleAvgPrice = resiSaleAvgPrice;
	}
	
    /**
     * @return resiSaleAvgPrice
     */
	public java.math.BigDecimal getResiSaleAvgPrice() {
		return this.resiSaleAvgPrice;
	}
	
	/**
	 * @param indtSaleAvgPrice
	 */
	public void setIndtSaleAvgPrice(java.math.BigDecimal indtSaleAvgPrice) {
		this.indtSaleAvgPrice = indtSaleAvgPrice;
	}
	
    /**
     * @return indtSaleAvgPrice
     */
	public java.math.BigDecimal getIndtSaleAvgPrice() {
		return this.indtSaleAvgPrice;
	}
	
	/**
	 * @param totalArchSqu
	 */
	public void setTotalArchSqu(java.math.BigDecimal totalArchSqu) {
		this.totalArchSqu = totalArchSqu;
	}
	
    /**
     * @return totalArchSqu
     */
	public java.math.BigDecimal getTotalArchSqu() {
		return this.totalArchSqu;
	}
	
	/**
	 * @param commArchSqu
	 */
	public void setCommArchSqu(java.math.BigDecimal commArchSqu) {
		this.commArchSqu = commArchSqu;
	}
	
    /**
     * @return commArchSqu
     */
	public java.math.BigDecimal getCommArchSqu() {
		return this.commArchSqu;
	}
	
	/**
	 * @param otherArchSqu
	 */
	public void setOtherArchSqu(java.math.BigDecimal otherArchSqu) {
		this.otherArchSqu = otherArchSqu;
	}
	
    /**
     * @return otherArchSqu
     */
	public java.math.BigDecimal getOtherArchSqu() {
		return this.otherArchSqu;
	}
	
	/**
	 * @param commSaleAvgPrice
	 */
	public void setCommSaleAvgPrice(java.math.BigDecimal commSaleAvgPrice) {
		this.commSaleAvgPrice = commSaleAvgPrice;
	}
	
    /**
     * @return commSaleAvgPrice
     */
	public java.math.BigDecimal getCommSaleAvgPrice() {
		return this.commSaleAvgPrice;
	}
	
	/**
	 * @param otherSaleAvgPrice
	 */
	public void setOtherSaleAvgPrice(java.math.BigDecimal otherSaleAvgPrice) {
		this.otherSaleAvgPrice = otherSaleAvgPrice;
	}
	
    /**
     * @return otherSaleAvgPrice
     */
	public java.math.BigDecimal getOtherSaleAvgPrice() {
		return this.otherSaleAvgPrice;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}