/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCoopShared
 * @类描述: lmt_coop_shared数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-30 16:59:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_coop_shared")
public class LmtCoopShared extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 授信协议编号 **/
	@Column(name = "lmt_ctr_no", unique = false, nullable = false, length = 40)
	private String lmtCtrNo;
	
	/** 合作方客户编号 **/
	@Column(name = "coop_cus_id", unique = false, nullable = false, length = 30)
	private String coopCusId;
	
	/** 合作方客户名称 **/
	@Column(name = "coop_cus_name", unique = false, nullable = true, length = 80)
	private String coopCusName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "cert_type", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "cert_code", unique = false, nullable = true, length = 32)
	private String certCode;
	
	/** 合作方类型 STD_ZB_COOP_TYP **/
	@Column(name = "coop_type", unique = false, nullable = true, length = 5)
	private String coopType;
	
	/** 共享范围 STD_ZB_SHR_SCOPE **/
	@Column(name = "shared_scope", unique = false, nullable = true, length = 5)
	private String sharedScope;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "cur_type", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 授信额度 **/
	@Column(name = "lmt_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 单户限额 **/
	@Column(name = "sig_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigAmt;
	
	/** 授信起始日期 **/
	@Column(name = "lmt_star_date", unique = false, nullable = true, length = 10)
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	@Column(name = "lmt_end_date", unique = false, nullable = true, length = 10)
	private String lmtEndDate;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改时间 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "approve_status", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型 STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param coopCusId
	 */
	public void setCoopCusId(String coopCusId) {
		this.coopCusId = coopCusId;
	}
	
    /**
     * @return coopCusId
     */
	public String getCoopCusId() {
		return this.coopCusId;
	}
	
	/**
	 * @param coopCusName
	 */
	public void setCoopCusName(String coopCusName) {
		this.coopCusName = coopCusName;
	}
	
    /**
     * @return coopCusName
     */
	public String getCoopCusName() {
		return this.coopCusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType;
	}
	
    /**
     * @return coopType
     */
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param sharedScope
	 */
	public void setSharedScope(String sharedScope) {
		this.sharedScope = sharedScope;
	}
	
    /**
     * @return sharedScope
     */
	public String getSharedScope() {
		return this.sharedScope;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param sigAmt
	 */
	public void setSigAmt(java.math.BigDecimal sigAmt) {
		this.sigAmt = sigAmt;
	}
	
    /**
     * @return sigAmt
     */
	public java.math.BigDecimal getSigAmt() {
		return this.sigAmt;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate;
	}
	
    /**
     * @return lmtStarDate
     */
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}
	
    /**
     * @return lmtEndDate
     */
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}