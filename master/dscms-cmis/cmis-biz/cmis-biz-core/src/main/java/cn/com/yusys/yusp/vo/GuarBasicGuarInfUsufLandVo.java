package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfUsufLand;

public class GuarBasicGuarInfUsufLandVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfUsufLand guarInfUsufLand;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfUsufLand getGuarInfUsufLand() {
        return guarInfUsufLand;
    }

    public void setGuarInfUsufLand(GuarInfUsufLand guarInfUsufLand) {
        this.guarInfUsufLand = guarInfUsufLand;
    }
}
