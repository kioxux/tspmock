/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperFinLeasCompanyForeignInvest
 * @类描述: rpt_oper_fin_leas_company_foreign_invest数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 16:45:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_fin_leas_company_foreign_invest")
public class RptOperFinLeasCompanyForeignInvest extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 被投资人 **/
	@Column(name = "INVESTOR", unique = false, nullable = true, length = 80)
	private String investor;
	
	/** 投资情况金额 **/
	@Column(name = "INVEST_CASE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investCaseAmt;
	
	/** 投资情况股权占比 **/
	@Column(name = "INVEST_CASE_STOCK_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investCaseStockPerc;
	
	/** 最近第三年投资收益 **/
	@Column(name = "NEAR_THIRD_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearThirdInvestIncome;
	
	/** 最近第二年投资收益 **/
	@Column(name = "NEAR_SECOND_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondInvestIncome;
	
	/** 最近第一年投资收益 **/
	@Column(name = "NEAR_FIRST_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstInvestIncome;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param investor
	 */
	public void setInvestor(String investor) {
		this.investor = investor;
	}
	
    /**
     * @return investor
     */
	public String getInvestor() {
		return this.investor;
	}
	
	/**
	 * @param investCaseAmt
	 */
	public void setInvestCaseAmt(java.math.BigDecimal investCaseAmt) {
		this.investCaseAmt = investCaseAmt;
	}
	
    /**
     * @return investCaseAmt
     */
	public java.math.BigDecimal getInvestCaseAmt() {
		return this.investCaseAmt;
	}
	
	/**
	 * @param investCaseStockPerc
	 */
	public void setInvestCaseStockPerc(java.math.BigDecimal investCaseStockPerc) {
		this.investCaseStockPerc = investCaseStockPerc;
	}
	
    /**
     * @return investCaseStockPerc
     */
	public java.math.BigDecimal getInvestCaseStockPerc() {
		return this.investCaseStockPerc;
	}
	
	/**
	 * @param nearThirdInvestIncome
	 */
	public void setNearThirdInvestIncome(java.math.BigDecimal nearThirdInvestIncome) {
		this.nearThirdInvestIncome = nearThirdInvestIncome;
	}
	
    /**
     * @return nearThirdInvestIncome
     */
	public java.math.BigDecimal getNearThirdInvestIncome() {
		return this.nearThirdInvestIncome;
	}
	
	/**
	 * @param nearSecondInvestIncome
	 */
	public void setNearSecondInvestIncome(java.math.BigDecimal nearSecondInvestIncome) {
		this.nearSecondInvestIncome = nearSecondInvestIncome;
	}
	
    /**
     * @return nearSecondInvestIncome
     */
	public java.math.BigDecimal getNearSecondInvestIncome() {
		return this.nearSecondInvestIncome;
	}
	
	/**
	 * @param nearFirstInvestIncome
	 */
	public void setNearFirstInvestIncome(java.math.BigDecimal nearFirstInvestIncome) {
		this.nearFirstInvestIncome = nearFirstInvestIncome;
	}
	
    /**
     * @return nearFirstInvestIncome
     */
	public java.math.BigDecimal getNearFirstInvestIncome() {
		return this.nearFirstInvestIncome;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}


}