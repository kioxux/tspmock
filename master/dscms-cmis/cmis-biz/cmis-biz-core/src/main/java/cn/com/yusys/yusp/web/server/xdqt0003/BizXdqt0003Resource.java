package cn.com.yusys.yusp.web.server.xdqt0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0003.req.Xdqt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.resp.Xdqt0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0003.Xdqt0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 接口处理类:企业网银推送预约信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0003:企业网银推送预约信息")
@RestController
@RequestMapping("/api/bizqt4bsp")
public class BizXdqt0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdqt0003Resource.class);

    @Autowired
    private Xdqt0003Service xdqt0003Service;
    /**
     * 交易码：xdqt0003
     * 交易描述：企业网银推送预约信息
     *
     * @param xdqt0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银推送预约信息")
    @PostMapping("/xdqt0003")
    protected @ResponseBody
    ResultDto<Xdqt0003DataRespDto> xdqt0003(@Validated @RequestBody Xdqt0003DataReqDto xdqt0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003DataReqDto));
        Xdqt0003DataRespDto xdqt0003DataRespDto = new Xdqt0003DataRespDto();// 响应Dto:企业网银推送预约信息
        ResultDto<Xdqt0003DataRespDto> xdqt0003DataResultDto = new ResultDto<>();
		String cusId = xdqt0003DataReqDto.getCusId();//客户编号
		String cusName = xdqt0003DataReqDto.getCusName();//客户名称
		String unifiedCreditCode = xdqt0003DataReqDto.getUnifiedCreditCode();//统一社会信用代码
		String cusLinkman = xdqt0003DataReqDto.getCusLinkman();//客户联系人
		String phone = xdqt0003DataReqDto.getPhone();//联系电话
		String appointTime = xdqt0003DataReqDto.getAppointTime();//预约时间
		String appointType = xdqt0003DataReqDto.getAppointType();//预约类型
        try {
            // 从xdqt0003DataReqDto获取业务值进行业务逻辑处理
            // 调用XXXXXService层开始
            xdqt0003DataRespDto = xdqt0003Service.getWyAppointmentAction(xdqt0003DataReqDto);
            // 调用XXXXXService层结束
            // 封装xdqt0003DataResultDto中正确的返回码和返回信息
            if (Objects.equals(DscmsBizDbEnum.FLAG_FAILD.key, xdqt0003DataRespDto.getOpFlag())) {
                xdqt0003DataResultDto.setCode(EcbEnum.ECB019999.key); // 成功失败标志
                xdqt0003DataResultDto.setMessage(EcbEnum.ECB019999.value); // 描述信息
            } else {
                xdqt0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdqt0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            xdqt0003DataResultDto.setCode(EcbEnum.ECB019999.key); // 成功失败标志
            xdqt0003DataResultDto.setMessage(EcbEnum.ECB019999.value); // 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, e.getMessage());
            // 封装xdqt0003DataResultDto中异常返回码和返回信息
            xdqt0003DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0003DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0003DataRespDto到xdqt0003DataResultDto中
        xdqt0003DataResultDto.setData(xdqt0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003DataResultDto));
        return xdqt0003DataResultDto;
    }
}
