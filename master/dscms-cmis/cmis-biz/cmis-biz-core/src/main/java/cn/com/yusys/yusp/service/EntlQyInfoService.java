/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusHouseInfo;
import cn.com.yusys.yusp.domain.EntlQyInfo;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.ENT_LIST;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.Fb1214ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.HOUSE_LIST;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.Fb1214RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusHouseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.EntlQyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.repository.mapper.VisaXdRiskMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 *
 * @类名称: EntlQyInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class EntlQyInfoService {

    @Autowired
    private EntlQyInfoMapper entlQyInfoMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;

    @Autowired
    private CusHouseInfoMapper cusHouseInfoMapper;
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private CommonService commonService;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(EntlQyInfoService.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public EntlQyInfo selectByPrimaryKey(String serno) {
        return entlQyInfoMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<EntlQyInfo> selectAll(QueryModel model) {
        List<EntlQyInfo> records = (List<EntlQyInfo>) entlQyInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<EntlQyInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<EntlQyInfo> list = entlQyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public EntlQyInfo selectBySerno(String serno) {
        return entlQyInfoMapper.selectBySerno(serno);
    }

    /**
     * @创建人 zhangl
     * @创建时间 2021-08-02 15:35
     * @注释
     */
    public List<EntlQyInfo> queryEntlQyInfo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<EntlQyInfo> list = entlQyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(EntlQyInfo record) {
        return entlQyInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(EntlQyInfo record) {
        return entlQyInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(EntlQyInfo record) {
        return entlQyInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(EntlQyInfo record) {
        return entlQyInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return entlQyInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return entlQyInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateEntlQyInfo
     * @方法描述: 企业，房产信息修改将信息同步发送风控
     * @参数与返回说明: 交易码fb1214
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public ResultDto<String> updateEntlQyInfo(EntlQyInfo record) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (record != null) {
            String crd_item_num="";//授信分项编号
            String pre_crd_amt="";//授信额度
            // 根据企业流水获取面签信息
            VisaXdRisk visaXdRisk = visaXdRiskMapper.selectByCrpserno(record.getSerno());
            if (Objects.isNull(visaXdRisk)) {
                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
            }
            // 根据面签流水获取房产信息
            CusHouseInfo cusHouseInfo = cusHouseInfoMapper.selectBySignatureSerno(visaXdRisk.getSerno());
            if (Objects.isNull(cusHouseInfo)) {
                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
            }
            // 根据企业表关联的客户编号获取授信分项额度以及授信分项 --此阶段房贷没有授信信息授信分项编号，授信额度传空
            log.info("房产信息修改同步推送风控开始,流水号【{}】", record.getSerno());
            // fb1214组装报文
            Fb1214ReqDto fb1214ReqDto = new Fb1214ReqDto();
            fb1214ReqDto.setPRD_TYPE(""); //产品类别 默认"" 参照老信贷代码逻辑
            fb1214ReqDto.setPRD_CODE("2002000001"); //产品代码 默认‘2002000001’ 参照老信贷代码逻辑
            fb1214ReqDto.setCUS_ID (record.getCusId()); //客户id
            fb1214ReqDto.setCUS_NAME(visaXdRisk.getCusName()); //客户名称
            fb1214ReqDto.setCREDIT_TYPE(commonService.toFkCertType(visaXdRisk.getCertType())); //证件类型
            fb1214ReqDto.setCREDIT_ID(visaXdRisk.getCertCode()); //证件号
            fb1214ReqDto.setCRD_ITEM_NUM(crd_item_num); //授信分项编号
            fb1214ReqDto.setPRE_CRD_AMT (pre_crd_amt); //授信额度
            // 获取房产信息列表
            List<HOUSE_LIST> houseList = new ArrayList<HOUSE_LIST>();
                HOUSE_LIST house = new HOUSE_LIST();
                house.setGUAR_NO(cusHouseInfo.getGuarNo()); // 统一押品编号
                house.setHOUSE_AREA(cusHouseInfo.getHouseSqu()); // 房产面积
                house.setHOUSE_RIGHT_NO(cusHouseInfo.getHouseLandNo()); // 产权证号
                house.setEVAL_VALUE(cusHouseInfo.getEvalAmt()); // 评估价值
                house.setBICYCLE_GARAGE_AREA(cusHouseInfo.getBicycleParkingSqu()); // 自行车库面积
                house.setPARK_SPACE_AREA(cusHouseInfo.getCarportSqu()); // 车位面积
                house.setLOFT_AREA(cusHouseInfo.getAtticSqu()); // 阁楼面积
                houseList.add(house);
            fb1214ReqDto.setHOUSE_LIST(houseList);
            // 获取企业信息列表
            List<ENT_LIST> entList = new ArrayList<ENT_LIST>();
                ENT_LIST ent = new ENT_LIST();
                ent.setENT_NAME(record.getCusName()); // 企业名称
                ent.setENT_BUZ_LIC_NO(record.getRegCde()); // 营业执照号
                ent.setENT_ANNUAL_SALES(record.getYearSaleIncome()); //年销售收入
                entList.add(ent);
            fb1214ReqDto.setENT_LIST(entList);
            log.info("房产信息修改同步推送风控请求：" + fb1214ReqDto);
            ResultDto<Fb1214RespDto> fb1214RespResultDto = dscms2CircpClientService.fb1214(fb1214ReqDto);
            log.info("房产信息修改同步推送风控返回：" + fb1214RespResultDto);
            String fb1214Code = Optional.ofNullable(fb1214RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String fb1214Meesage = Optional.ofNullable(fb1214RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("房产信息修改同步推送风控返回信息：" + fb1214Meesage);
            // 判断接口是否返回成功
            if (Objects.equals(fb1214Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                String guarNo = ""; // 押品统一编号
                // 获取修改日期
                record.setUpdateTime(DateUtils.getCurrTimestamp());
                // 更新企业信息状态CRP_STATUS 为2 --老代码逻辑，待确认
                record.setCrpStatus("2"); //信息修改状态 1待申请2已完成
                // 1.企业信息落表
                log.info("企业信息落表开始："+record.getSerno());
                int entlQyInfocount =entlQyInfoMapper.updateByPrimaryKey(record);
                if (entlQyInfocount != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷尽调结果录入企业信息修改失败！");
                }
                log.info("企业信息落表结束");
                // 2.获取返回房屋信息
                List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.HOUSE_LIST> fb1214houseList =fb1214RespResultDto.getData().getHOSE_LIST();
                guarNo = fb1214houseList.get(0).getGUAR_NO(); //获取押品统一编号
                // 根据接口返回信息更新房屋信息
                cusHouseInfo.setGuarNo(guarNo); //押品统一编号
                cusHouseInfo.setBicycleParkingAmt(fb1214houseList.get(0).getBICYCLE_GARAGE_EVAL());//自行车库价值
                cusHouseInfo.setCarportAmt(fb1214houseList.get(0).getPARK_SPACE_EVAL()); //车位价值
                cusHouseInfo.setAtticAmt(fb1214houseList.get(0).getLOFT_EVAL());//阁楼价值
                cusHouseInfo.setEvalAmt(fb1214houseList.get(0).getEVAL_VALUE()); //评估价值（房产）
                cusHouseInfo.setEvalTotalAmt(fb1214houseList.get(0).getTOTAL_EVAL());//总评估价值
                // 2.1 房产信息落表
                log.info("房屋信息落表开始："+cusHouseInfo);
                int cusHouseInfoCount = cusHouseInfoMapper.updateByPrimaryKey(cusHouseInfo);
                if (cusHouseInfoCount != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷尽点录入房产信息修改失败！");
                }
                log.info("房屋信息落表结束");
                // 2.2 根据押品编号更新押品价值
                log.info("根据押品编号更新押品评估价值开始："+guarNo);
                // 获取押品编号获取押品信息
                GuarBaseInfo guarBaseInfo = guarBaseInfoMapper.queryBaseInfoByGuarId(guarNo);
                if (Objects.nonNull(guarBaseInfo)) {
                    guarBaseInfo.setEvalAmt(fb1214houseList.get(0).getEVAL_VALUE()); //评估价值
                    int guarBaseInfoCount = guarBaseInfoMapper.updateByPrimaryKey(guarBaseInfo);
                    if (guarBaseInfoCount != 1) {
                        //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                        throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",根据押品编号更新押品评估价值失败！");
                    }
                }
                log.info("根据押品编号更新押品评估价值结束");

            } else {
                resultDto.setCode(fb1214Code);
                resultDto.setMessage(fb1214Meesage);
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        return resultDto;
    }
}
