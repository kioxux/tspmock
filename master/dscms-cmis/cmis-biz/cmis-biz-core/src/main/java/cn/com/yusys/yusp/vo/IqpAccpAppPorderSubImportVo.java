package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;
import java.time.LocalDate;

@ExcelCsv(namePrefix = "票据明细导入模板(请手动设置单元格格式：文本)", fileType = ExcelCsv.ExportFileType.XLS)
public class IqpAccpAppPorderSubImportVo {

    /*
    票据号 DRFT_NO
     */
    @ExcelField(title = "票据号", viewLength = 40 , format = "@")
    private String drftNo;

    /*
    票面金额 DRFT_AMT 16,2
     */
    @ExcelField(title = "票面金额", viewLength = 16, format = "@")
    private String drftAmt;

    /*
    起始日期 START_DATE
     */
    @ExcelField(title = "起始日期", viewLength = 20, format = "@")
    private String startDate;

    /*
    到期日期 END_DATE
     */
    @ExcelField(title = "到期日期", viewLength = 20, format = "@")
    private String endDate;

    /*
    收款人名称 PYEE_NAME
     */
    @ExcelField(title = "收款人名称", viewLength = 20, format = "@")
    private String pyeeName;

    /*
    收款人开户行名称 PYEE_ACCTSVCR_NAME
     */
    @ExcelField(title = "收款人开户行名称", viewLength = 80, format = "@")
    private String pyeeAcctsvcrName;

    /*
    收款人账号 PYEE_ACCNO
     */
    @ExcelField(title = "收款人账号", viewLength = 80, format = "@")
    private String pyeeAccno;

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public String getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(String drftAmt) {
        this.drftAmt = drftAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAcctsvcrName() {
        return pyeeAcctsvcrName;
    }

    public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
        this.pyeeAcctsvcrName = pyeeAcctsvcrName;
    }

    public String getPyeeAccno() {
        return pyeeAccno;
    }

    public void setPyeeAccno(String pyeeAccno) {
        this.pyeeAccno = pyeeAccno;
    }
}
