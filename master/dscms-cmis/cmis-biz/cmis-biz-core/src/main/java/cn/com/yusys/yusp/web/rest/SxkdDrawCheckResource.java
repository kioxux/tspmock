/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.domain.GuarGcfApp;
import cn.com.yusys.yusp.domain.LoanEdCheck;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.SxkdDrawCheck;
import cn.com.yusys.yusp.service.SxkdDrawCheckService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SxkdDrawCheckResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zl
 * @创建时间: 2021-08-11 22:06:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/sxkddrawcheck")
public class SxkdDrawCheckResource {
    @Autowired
    private SxkdDrawCheckService sxkdDrawCheckService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<SxkdDrawCheck>> query() {
        QueryModel queryModel = new QueryModel();
        List<SxkdDrawCheck> list = sxkdDrawCheckService.selectAll(queryModel);
        return new ResultDto<List<SxkdDrawCheck>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<SxkdDrawCheck>> index(QueryModel queryModel) {
        List<SxkdDrawCheck> list = sxkdDrawCheckService.selectByModel(queryModel);
        return new ResultDto<List<SxkdDrawCheck>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<SxkdDrawCheck> show(@PathVariable("serno") String serno) {
        SxkdDrawCheck sxkdDrawCheck = sxkdDrawCheckService.selectByPrimaryKey(serno);
        return new ResultDto<SxkdDrawCheck>(sxkdDrawCheck);
    }

    /**
     * @函数名称:sxkdDrawChecklist
     * @函数描述: 获取未发放省心快贷提款审核
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取省心快贷提款审核")
    @PostMapping("/sxkdDrawChecklist")
    protected ResultDto<List<SxkdDrawCheck>> sxkdDrawChecklist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SxkdDrawCheck> list = sxkdDrawCheckService.sxkdDrawChecklist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SxkdDrawCheck>>(list);
    }

    /**
     * @函数名称:sxkdDrawCheckHislist
     * @函数描述: 获取已发放省心快贷提款审核
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取房抵e点贷受托信息审核历史")
    @PostMapping("/sxkdDrawCheckHislist")
    protected ResultDto<List<SxkdDrawCheck>> riskXdGuarantyHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SxkdDrawCheck> list = sxkdDrawCheckService.sxkdDrawCheckHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SxkdDrawCheck>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过面签流水号查询企业详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过查询查封流水号查询查封详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        SxkdDrawCheck sxkdDrawCheck = sxkdDrawCheckService.selectBySerno((String) params.get("serno"));
        if (sxkdDrawCheck != null) {
            resultDto.setData(sxkdDrawCheck);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<SxkdDrawCheck> create(@RequestBody SxkdDrawCheck sxkdDrawCheck) throws URISyntaxException {
        sxkdDrawCheckService.insert(sxkdDrawCheck);
        return new ResultDto<SxkdDrawCheck>(sxkdDrawCheck);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody SxkdDrawCheck sxkdDrawCheck) throws URISyntaxException {
        int result = sxkdDrawCheckService.update(sxkdDrawCheck);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = sxkdDrawCheckService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = sxkdDrawCheckService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:sxkdDrawCheckBackOut
     * @函数描述:省心快贷提款审核撤销出账
     * @参数与返回说明:
     * @算法描述:
     * @创建人：zhangliang15
     */
    @ApiOperation("省心快贷提款审核撤销出账")
    @PostMapping("/sxkdDrawCheckBackOut")
    public ResultDto<Map> sxkdDrawCheckBackOut(@RequestBody SxkdDrawCheck sxkdDrawCheck) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = sxkdDrawCheckService.sxkdDrawCheckBackOut(sxkdDrawCheck);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:sxkdDrawChecksubmit
     * @函数描述:省心快贷提款审核提交
     * @参数与返回说明:
     * @算法描述:
     * @创建者:zhangliang15
     */
    @ApiOperation("省心快贷提款审核提交")
    @PostMapping("/sxkdDrawChecksubmit")
    protected ResultDto<Map> sxkdDrawChecksubmit(@RequestBody SxkdDrawCheck sxkdDrawCheck) throws URISyntaxException {
        Map rtnData = sxkdDrawCheckService.sxkdDrawChecksubmit(sxkdDrawCheck);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:sxkdToHxFk
     * @函数描述:省心快贷提款审核发送核心放款
     * @参数与返回说明:
     * @算法描述:
     * @创建人：zhangliang15
     */
    @ApiOperation("省心快贷提款审核撤销出账")
    @PostMapping("/sxkdToHxFk")
    public ResultDto<Map> sxkdToHxFk(@RequestBody SxkdDrawCheck sxkdDrawCheck) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = sxkdDrawCheckService.sxkdToHxFk(sxkdDrawCheck);
        return new ResultDto<>(rtnData);
    }

}
