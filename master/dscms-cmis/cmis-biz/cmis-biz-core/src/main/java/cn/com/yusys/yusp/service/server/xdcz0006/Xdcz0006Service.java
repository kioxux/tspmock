package cn.com.yusys.yusp.service.server.xdcz0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0006.req.Xdcz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.resp.Xdcz0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 接口处理类:出账记录详情查看
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0006Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0006Service.class);

    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;

    /**
     * 出账记录详情查看
     * @param xdcz0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0006DataRespDto xdcz0006(Xdcz0006DataReqDto xdcz0006DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataReqDto));
        Xdcz0006DataRespDto xdcz0006DataRespDto = new Xdcz0006DataRespDto();
        String acctNo = xdcz0006DataReqDto.getAcctNo();
        String seq = xdcz0006DataReqDto.getSeq();
        if (StringUtils.isBlank(acctNo)) {
            throw new YuspException(EcbEnum.ECB010009.key, EcbEnum.ECB010009.value + ",信贷系统没有获取到，账号acctNo的信息");
        }
        if (StringUtils.isBlank(seq)) {
            throw new YuspException(EcbEnum.ECB010009.key, EcbEnum.ECB010009.value + ",信贷系统没有获取到，批次号seq的信息");
        }

        try {
            xdcz0006DataRespDto = pvpAuthorizeMapper.getDetail(xdcz0006DataReqDto);
            if (Objects.isNull(xdcz0006DataRespDto)) {
                throw new YuspException(EcbEnum.ECB010009.key, EcbEnum.ECB010009.value + ",根据提供批次号和账号信息，查询不到已出账记录!");
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataRespDto));
        return xdcz0006DataRespDto;
    }
}
