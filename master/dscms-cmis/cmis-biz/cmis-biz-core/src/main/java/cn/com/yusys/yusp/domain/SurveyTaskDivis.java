/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyTaskDivis
 * @类描述: survey_task_divis数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 15:13:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "survey_task_divis")
public class SurveyTaskDivis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 客户编号 **/
	@Column(name = "CUS_NO", unique = false, nullable = true, length = 30)
	private String cusNo;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 32)
	private String certNo;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 20)
	private String belgLine;
	
	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 11)
	private String phone;
	
	/** 工作 **/
	@Column(name = "WORK", unique = false, nullable = true, length = 200)
	private String work;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 80)
	private String appChnl;
	
	/** 贷款类别 **/
	@Column(name = "LOAN_CLS", unique = false, nullable = true, length = 5)
	private String loanCls;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_USE", unique = false, nullable = true, length = 5)
	private String loanUse;
	
	/** 客户经理名称 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 80)
	private String managerName;
	
	/** 客户经理编号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 客户经理片区 **/
	@Column(name = "MANAGER_AREA", unique = false, nullable = true, length = 32)
	private String managerArea;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 进件时间 **/
	@Column(name = "INTO_TIME", unique = false, nullable = true, length = 10)
	private String intoTime;
	
	/** 业务状态 **/
	@Column(name = "BUSI_STATUS", unique = false, nullable = true, length = 5)
	private String busiStatus;
	
	/** 分配时间 **/
	@Column(name = "DIVIS_TIME", unique = false, nullable = true, length = 10)
	private String divisTime;
	
	/** 处理人 **/
	@Column(name = "PRC_ID", unique = false, nullable = true, length = 32)
	private String prcId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 是否需要线下调查 **/
	@Column(name = "IS_STOP_OFFLINE", unique = false, nullable = true, length = 1)
	private String isStopOffline;
	
	/** 调查报告流水号 **/
	@Column(name = "SURVEY_NO", unique = false, nullable = true, length = 40)
	private String surveyNo;
	
	/** 营销人 **/
	@Column(name = "MAR_ID", unique = false, nullable = true, length = 40)
	private String marId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusNo
	 */
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo;
	}
	
    /**
     * @return cusNo
     */
	public String getCusNo() {
		return this.cusNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param work
	 */
	public void setWork(String work) {
		this.work = work;
	}
	
    /**
     * @return work
     */
	public String getWork() {
		return this.work;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param loanCls
	 */
	public void setLoanCls(String loanCls) {
		this.loanCls = loanCls;
	}
	
    /**
     * @return loanCls
     */
	public String getLoanCls() {
		return this.loanCls;
	}
	
	/**
	 * @param loanUse
	 */
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}
	
    /**
     * @return loanUse
     */
	public String getLoanUse() {
		return this.loanUse;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerArea
	 */
	public void setManagerArea(String managerArea) {
		this.managerArea = managerArea;
	}
	
    /**
     * @return managerArea
     */
	public String getManagerArea() {
		return this.managerArea;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime;
	}
	
    /**
     * @return intoTime
     */
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param busiStatus
	 */
	public void setBusiStatus(String busiStatus) {
		this.busiStatus = busiStatus;
	}
	
    /**
     * @return busiStatus
     */
	public String getBusiStatus() {
		return this.busiStatus;
	}
	
	/**
	 * @param divisTime
	 */
	public void setDivisTime(String divisTime) {
		this.divisTime = divisTime;
	}
	
    /**
     * @return divisTime
     */
	public String getDivisTime() {
		return this.divisTime;
	}
	
	/**
	 * @param prcId
	 */
	public void setPrcId(String prcId) {
		this.prcId = prcId;
	}
	
    /**
     * @return prcId
     */
	public String getPrcId() {
		return this.prcId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param isStopOffline
	 */
	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline;
	}
	
    /**
     * @return isStopOffline
     */
	public String getIsStopOffline() {
		return this.isStopOffline;
	}
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	
    /**
     * @return surveyNo
     */
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId;
	}
	
    /**
     * @return marId
     */
	public String getMarId() {
		return this.marId;
	}


}