package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherGrtValueIdentyApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.OtherGrtValueIdentyAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 授信抵质押物价值认定申请流程业务处理类 - 寿光村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class SGCZ12BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(SGCZ12BizService.class);

    @Autowired
    private OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private LmtAppService lmtAppService;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();

        OtherGrtValueIdentyApp otherGrtValueIdentyApp = otherGrtValueIdentyAppService.selectByPrimaryKey(serno);
        String LmtSerno ="";
        if(null != otherGrtValueIdentyApp){
            LmtSerno = otherGrtValueIdentyApp.getLmtSerno();
        }
        if(!"".equals(LmtSerno)){
            // 加载路由条件
           this.put2VarParamOther(instanceInfo, LmtSerno);
        }
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                // 结束后处理
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                otherGrtValueIdentyAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * put2VarParamOther
     *
     * @param resultInstanceDto
     * @param serno
     */
    public void put2VarParamOther(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = lmtAppService.getMortMapResult(serno);
        params.remove("approveStatus");
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ12.equals(flowCode);
    }
}
