/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.AccTfLoc;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTfLocMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 14:04:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccTfLocMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccTfLoc selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccTfLoc> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccTfLoc record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccTfLoc record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccTfLoc record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccTfLoc record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * 根据合同号查找出对应的借据信息
     * @param contNo
     * @return
     */
    java.util.List<AccTfLoc> getAccTfInfoByContNo(@Param("contNo")String contNo);

    /**
     * @方法名称：selectForAccTfLocInfo
     * @方法描述：非垫款借据查询
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:06
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccTfLoc> selectForAccTfLocInfo(@Param("cusId")String cusId);


    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:57
     * @修改记录：修改时间 修改人员 修改时间
     */
    List<AccTfLoc> selectByContNo(@Param("contNo") String contNo);

    int countAccTfLocCountByContNo(String contNo);

    /**
     * @方法名称: updateBailAmtByBillNo
     * @方法描述: 更新信用证保证金额度
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateBailAmtByBillNo(Map QueryMap);


    /**
     * @方法名称: countAccTfLocCountByBillNo
     * @方法描述: 借据编号查询台账是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    int countAccTfLocCountByBillNo(@Param("billNo")String billNo);

    /**
     * @方法名称: deleteAccTfLocByBillNo
     * @方法描述: 借据编号删除台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteAccTfLocByBillNo(@Param("billNo")String billNo);



    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * @Description:根据借据号查询是否存在业务信息
     * @param billNo: 借据号
     * @return: AccTfLoc
     **/
    AccTfLoc selectAccTfLocByBillNo(@Param("billNo") String billNo);

    /**
     * @Description:根据借据号回复借据额度
     * @param billNo: 借据号
     * @return: int
     **/
    int updateAccTfAmtAndStatusByBillNo(@Param("billNo") String billNo);

    int updateAccTfAmtByBillNo(Map map);

    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询开证台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccTfLoc selectByBillNo(@Param("billNo") String billNo);




    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据pvpSerno查询 借据号billNO
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccTfLoc selectAccTfLocBillNo(@Param("pvpSerno") String pvpSerno);

    /**
     * 根据合同编号查询用信敞口余额
     * @param contNos
     * @return
     */
    BigDecimal selectTotalSpacAmtByContNos(@Param("contNos") String contNos);

    /**
     * 风险分类审批结束更新客户未结清开证台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:30
     **/
    int updateLoanFiveAndTenClassByCusId(Map<String, String> map);
}