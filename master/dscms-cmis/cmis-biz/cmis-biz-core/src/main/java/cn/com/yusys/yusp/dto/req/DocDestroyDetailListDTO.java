package cn.com.yusys.yusp.dto.req;

import cn.com.yusys.yusp.domain.DocDestroyDetailList;

import java.io.Serializable;
import java.util.List;

public class DocDestroyDetailListDTO  implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 档案销毁流水号 **/
    private String ddalSerno;

    /** 引入的档案销毁明细集合 **/
    private List<DocDestroyDetailList> docDestroyDetailLists;

    public String getDdalSerno() {
        return ddalSerno;
    }

    public void setDdalSerno(String ddalSerno) {
        this.ddalSerno = ddalSerno;
    }

    public List<DocDestroyDetailList> getDocDestroyDetailLists() {
        return docDestroyDetailLists;
    }

    public void setDocDestroyDetailLists(List<DocDestroyDetailList> docDestroyDetailLists) {
        this.docDestroyDetailLists = docDestroyDetailLists;
    }
}
