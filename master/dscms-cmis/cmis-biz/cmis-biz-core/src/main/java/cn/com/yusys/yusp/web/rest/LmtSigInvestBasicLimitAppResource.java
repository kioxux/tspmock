/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicFncAnly;
import cn.com.yusys.yusp.service.BizInvestCommonService;
import cn.com.yusys.yusp.service.LmtSigInvestBasicFncAnlyService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.service.LmtSigInvestBasicLimitAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLimitAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 15:46:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasiclimitapp")
public class LmtSigInvestBasicLimitAppResource extends BizInvestCommonService {
    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService;

    @Autowired
    private LmtSigInvestBasicFncAnlyService lmtSigInvestBasicFncAnlyService;



	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicLimitApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLimitApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicLimitApp>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLimitApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicLimitApp> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp = lmtSigInvestBasicLimitAppService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicLimitApp>(lmtSigInvestBasicLimitApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicLimitApp> create(@RequestBody LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp) throws URISyntaxException {
        lmtSigInvestBasicLimitAppService.insert(lmtSigInvestBasicLimitApp);
        return new ResultDto<LmtSigInvestBasicLimitApp>(lmtSigInvestBasicLimitApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp) throws URISyntaxException {
        int result = lmtSigInvestBasicLimitAppService.update(lmtSigInvestBasicLimitApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicLimitAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicLimitAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *根据底层授信流水号查询
     */
    @PostMapping("/selectAll")
    protected ResultDto<List<LmtSigInvestBasicLimitApp>> selectAll(@RequestBody QueryModel queryModel) {
        ResultDto<List<LmtSigInvestBasicLimitApp>> result = new ResultDto<List<LmtSigInvestBasicLimitApp>>();
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectByModel(queryModel);
        result.setData(list);
        return result;
    }

    /**
     *保存（是否循环）
     */
    @PostMapping("/saveisrevolv")
    protected ResultDto<Integer> saveIsRevolv(@RequestBody LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("basicSerno",lmtSigInvestBasicLimitApp.getBasicSerno());
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectByModel(queryModel);
        list.get(0).setIsRevolv(lmtSigInvestBasicLimitApp.getIsRevolv());
        int result = lmtSigInvestBasicLimitAppService.update(list.get(0));
        return new ResultDto<Integer>(result);
    }

    /**
     *根据底层授信流水号和底层客户号综合查询
     */
    @PostMapping("/integratedquery")
    protected ResultDto<Map> integratedQuery(@RequestBody LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("basicSerno",lmtSigInvestBasicLimitApp.getBasicSerno());
        queryModel.addCondition("cusId",lmtSigInvestBasicLimitApp.getBasicCusId());
        List<LmtSigInvestBasicFncAnly> fncAnlys = lmtSigInvestBasicFncAnlyService.selectByModel(queryModel);
        Map map = new HashMap();
        if (CollectionUtils.isNotEmpty(fncAnlys)) {
            LmtSigInvestBasicFncAnly fncAnly = fncAnlys.get(0);
            map.put("totalRessetAmt", fncAnly.getTotalRessetAmt());
            map.put("totalDebtAmt", fncAnly.getTotalDebtAmt());
            map.put("bsinsIncome", fncAnly.getBsinsIncome());
            map.put("profit", fncAnly.getProfit());
            map.put("assetDebtRate", fncAnly.getAssetDebtRate());
            map.put("otherDesc", fncAnly.getOtherDesc());
        }
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("basicSerno",lmtSigInvestBasicLimitApp.getBasicSerno());
        queryModel1.addCondition("basicCusId",lmtSigInvestBasicLimitApp.getBasicCusId());
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectByModel(queryModel1);
        if (CollectionUtils.isNotEmpty(list)) {
            LmtSigInvestBasicLimitApp limitApp = list.get(0);
            map.put("basicAssetBasicCaseAnaly",limitApp.getBasicAssetBasicCaseAnaly());
            map.put("operCaseAnaly",limitApp.getOperCaseAnaly());
            map.put("otherCaseAnaly",limitApp.getOtherCaseAnaly());
        }
        return new ResultDto<Map>(map);
    }

    /**
     *保存（底层申请主表和底层财务分析表）
     */
    @PostMapping("/savedouble")
    protected ResultDto<Integer> saveDouble(@RequestBody Map map) {
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("basicSerno",map.get("basicSerno"));
        queryModel1.addCondition("cusId",map.get("basicCusId"));
        List<LmtSigInvestBasicFncAnly> fncAnlys = lmtSigInvestBasicFncAnlyService.selectByModel(queryModel1);
        LmtSigInvestBasicFncAnly fncAnly = fncAnlys.get(0);
        changeValueToBigDecimal(map,"totalRessetAmt");
        changeValueToBigDecimal(map,"totalDebtAmt");
        changeValueToBigDecimal(map,"bsinsIncome");
        changeValueToBigDecimal(map,"profit");
        changeValueToBigDecimal(map,"assetDebtRate");
        /*fncAnly.setTotalRessetAmt((BigDecimal) map.get("totalRessetAmt"));
        fncAnly.setAssetDebtRate((BigDecimal) map.get("totalDebtAmt"));
        fncAnly.setBsinsIncome((BigDecimal) map.get("bsinsIncome"));
        fncAnly.setProfit((BigDecimal) map.get("profit"));
        fncAnly.setAssetDebtRate((BigDecimal) map.get("assetDebtRate"));
        fncAnly.setOtherDesc((String) map.get("otherDesc"));*/
        BeanUtils.mapToBean(map,fncAnly);
        int a = lmtSigInvestBasicFncAnlyService.update(fncAnly);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("basicSerno",map.get("basicSerno"));
        queryModel.addCondition("basicCusId",map.get("basicCusId"));
        List<LmtSigInvestBasicLimitApp> list = lmtSigInvestBasicLimitAppService.selectByModel(queryModel);
        LmtSigInvestBasicLimitApp limitApp = list.get(0);
        limitApp.setBasicAssetBasicCaseAnaly((String) map.get("basicAssetBasicCaseAnaly"));
        limitApp.setOperCaseAnaly((String) map.get("operCaseAnaly"));
        limitApp.setOtherCaseAnaly((String) map.get("otherCaseAnaly"));
        int b = lmtSigInvestBasicLimitAppService.update(limitApp);
        return new ResultDto<Integer>(a*b);
    }


}
