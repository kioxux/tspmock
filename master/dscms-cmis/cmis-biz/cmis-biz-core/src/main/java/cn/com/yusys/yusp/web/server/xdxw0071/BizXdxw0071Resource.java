package cn.com.yusys.yusp.web.server.xdxw0071;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0071.req.Xdxw0071DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0071.resp.Xdxw0071DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0071.Xdxw0071Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:查询优抵贷抵质押品信息
 * 01-带土地性质条件
 * 02-不带土地性质条件
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXW0071:查询优抵贷抵质押品信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0071Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0071Resource.class);

    @Autowired
	private Xdxw0071Service xdxw0071Service;
    /**
     * 交易码：xdxw0071
     * 交易描述：查询优抵贷抵质押品信息
     *
     * @param xdxw0071DataReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("查询优抵贷抵质押品信息")
    @PostMapping("/xdxw0071")
    protected @ResponseBody
    ResultDto<Xdxw0071DataRespDto> xdxw0071(@Validated @RequestBody Xdxw0071DataReqDto xdxw0071DataReqDto) throws Exception {
		logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataReqDto));
        Xdxw0071DataRespDto xdxw0071DataRespDto = new Xdxw0071DataRespDto();// 响应Dto:查询优抵贷抵质押品信息
        ResultDto<Xdxw0071DataRespDto> xdxw0071DataResultDto = new ResultDto<>();
        // 从xdxw0071DataReqDto获取业务值进行业务逻辑处理
        try {
			logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataReqDto));
        	xdxw0071DataRespDto = xdxw0071Service.xdxw0071(xdxw0071DataReqDto);
			logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataRespDto));
            // 封装xdxw0071DataResultDto中正确的返回码和返回信息
            xdxw0071DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0071DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, e.getMessage());
            // 封装xdxw0071DataResultDto中异常返回码和返回信息
            xdxw0071DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0071DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0071DataRespDto到xdxw0071DataResultDto中
        xdxw0071DataResultDto.setData(xdxw0071DataRespDto);
		logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071DataResultDto));
        return xdxw0071DataResultDto;
    }
}
