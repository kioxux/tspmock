/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtBuildingPro
 * @类描述: lmt_building_pro数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-13 14:20:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_building_pro")
public class LmtBuildingPro extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 授信协议编号 **/
	@Column(name = "lmt_ctr_no", unique = false, nullable = true, length = 40)
	private String lmtCtrNo;
	
	/** 项目编号 **/
	@Column(name = "pro_no", unique = false, nullable = false, length = 40)
	private String proNo;
	
	/** 项目名称 **/
	@Column(name = "pro_name", unique = false, nullable = true, length = 80)
	private String proName;
	
	/** 项目总投资 **/
	@Column(name = "pro_invest", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proInvest;
	
	/** 开发商名称 **/
	@Column(name = "deve_name", unique = false, nullable = true, length = 80)
	private String deveName;
	
	/** 项目开工时间 **/
	@Column(name = "pro_start_time", unique = false, nullable = true, length = 10)
	private String proStartTime;
	
	/** 是否竣工 STD_ZB_YES_NO **/
	@Column(name = "is_completion", unique = false, nullable = true, length = 5)
	private String isCompletion;
	
	/** 竣工时间 **/
	@Column(name = "completion_time", unique = false, nullable = true, length = 10)
	private String completionTime;
	
	/** 是否封顶 STD_ZB_YES_NO **/
	@Column(name = "is_capping", unique = false, nullable = true, length = 5)
	private String isCapping;
	
	/** 封顶时间 **/
	@Column(name = "capping_time", unique = false, nullable = true, length = 10)
	private String cappingTime;
	
	/** 地理位置 **/
	@Column(name = "geogra_place", unique = false, nullable = true, length = 500)
	private String geograPlace;
	
	/** 街道 **/
	@Column(name = "street", unique = false, nullable = true, length = 200)
	private String street;
	
	/** 地理面积（平米） **/
	@Column(name = "geogra_squ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal geograSqu;
	
	/** 住宅建筑面积（平米） **/
	@Column(name = "resi_arch_squ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal resiArchSqu;
	
	/** 总建筑面积（平米） **/
	@Column(name = "totl_arch_squ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totlArchSqu;
	
	/** 住宅销售单价（元/平米） **/
	@Column(name = "resi_sale_price", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal resiSalePrice;
	
	/** 商业建筑面积（平米） **/
	@Column(name = "comm_arch_squ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commArchSqu;
	
	/** 商业销售单价（元/平米） **/
	@Column(name = "comm_sale_price", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commSalePrice;
	
	/** 是否五证齐全 STD_ZB_YES_NO **/
	@Column(name = "is_comp_five_lice", unique = false, nullable = true, length = 20)
	private String isCompFiveLice;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}
	
    /**
     * @return lmtCtrNo
     */
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param proNo
	 */
	public void setProNo(String proNo) {
		this.proNo = proNo;
	}
	
    /**
     * @return proNo
     */
	public String getProNo() {
		return this.proNo;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proInvest
	 */
	public void setProInvest(java.math.BigDecimal proInvest) {
		this.proInvest = proInvest;
	}
	
    /**
     * @return proInvest
     */
	public java.math.BigDecimal getProInvest() {
		return this.proInvest;
	}
	
	/**
	 * @param deveName
	 */
	public void setDeveName(String deveName) {
		this.deveName = deveName;
	}
	
    /**
     * @return deveName
     */
	public String getDeveName() {
		return this.deveName;
	}
	
	/**
	 * @param proStartTime
	 */
	public void setProStartTime(String proStartTime) {
		this.proStartTime = proStartTime;
	}
	
    /**
     * @return proStartTime
     */
	public String getProStartTime() {
		return this.proStartTime;
	}
	
	/**
	 * @param isCompletion
	 */
	public void setIsCompletion(String isCompletion) {
		this.isCompletion = isCompletion;
	}
	
    /**
     * @return isCompletion
     */
	public String getIsCompletion() {
		return this.isCompletion;
	}
	
	/**
	 * @param completionTime
	 */
	public void setCompletionTime(String completionTime) {
		this.completionTime = completionTime;
	}
	
    /**
     * @return completionTime
     */
	public String getCompletionTime() {
		return this.completionTime;
	}
	
	/**
	 * @param isCapping
	 */
	public void setIsCapping(String isCapping) {
		this.isCapping = isCapping;
	}
	
    /**
     * @return isCapping
     */
	public String getIsCapping() {
		return this.isCapping;
	}
	
	/**
	 * @param cappingTime
	 */
	public void setCappingTime(String cappingTime) {
		this.cappingTime = cappingTime;
	}
	
    /**
     * @return cappingTime
     */
	public String getCappingTime() {
		return this.cappingTime;
	}
	
	/**
	 * @param geograPlace
	 */
	public void setGeograPlace(String geograPlace) {
		this.geograPlace = geograPlace;
	}
	
    /**
     * @return geograPlace
     */
	public String getGeograPlace() {
		return this.geograPlace;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param geograSqu
	 */
	public void setGeograSqu(java.math.BigDecimal geograSqu) {
		this.geograSqu = geograSqu;
	}
	
    /**
     * @return geograSqu
     */
	public java.math.BigDecimal getGeograSqu() {
		return this.geograSqu;
	}
	
	/**
	 * @param resiArchSqu
	 */
	public void setResiArchSqu(java.math.BigDecimal resiArchSqu) {
		this.resiArchSqu = resiArchSqu;
	}
	
    /**
     * @return resiArchSqu
     */
	public java.math.BigDecimal getResiArchSqu() {
		return this.resiArchSqu;
	}
	
	/**
	 * @param totlArchSqu
	 */
	public void setTotlArchSqu(java.math.BigDecimal totlArchSqu) {
		this.totlArchSqu = totlArchSqu;
	}
	
    /**
     * @return totlArchSqu
     */
	public java.math.BigDecimal getTotlArchSqu() {
		return this.totlArchSqu;
	}
	
	/**
	 * @param resiSalePrice
	 */
	public void setResiSalePrice(java.math.BigDecimal resiSalePrice) {
		this.resiSalePrice = resiSalePrice;
	}
	
    /**
     * @return resiSalePrice
     */
	public java.math.BigDecimal getResiSalePrice() {
		return this.resiSalePrice;
	}
	
	/**
	 * @param commArchSqu
	 */
	public void setCommArchSqu(java.math.BigDecimal commArchSqu) {
		this.commArchSqu = commArchSqu;
	}
	
    /**
     * @return commArchSqu
     */
	public java.math.BigDecimal getCommArchSqu() {
		return this.commArchSqu;
	}
	
	/**
	 * @param commSalePrice
	 */
	public void setCommSalePrice(java.math.BigDecimal commSalePrice) {
		this.commSalePrice = commSalePrice;
	}
	
    /**
     * @return commSalePrice
     */
	public java.math.BigDecimal getCommSalePrice() {
		return this.commSalePrice;
	}
	
	/**
	 * @param isCompFiveLice
	 */
	public void setIsCompFiveLice(String isCompFiveLice) {
		this.isCompFiveLice = isCompFiveLice;
	}
	
    /**
     * @return isCompFiveLice
     */
	public String getIsCompFiveLice() {
		return this.isCompFiveLice;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}