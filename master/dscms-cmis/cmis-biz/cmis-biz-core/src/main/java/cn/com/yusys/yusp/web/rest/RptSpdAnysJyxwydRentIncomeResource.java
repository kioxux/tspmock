/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.dto.JyxwydRentIncomeDto;
import cn.com.yusys.yusp.dto.RptSpdAnysJyxwydRentIncomeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwydRentIncome;
import cn.com.yusys.yusp.service.RptSpdAnysJyxwydRentIncomeService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysjyxwydrentincome")
public class RptSpdAnysJyxwydRentIncomeResource {
    @Autowired
    private RptSpdAnysJyxwydRentIncomeService rptSpdAnysJyxwydRentIncomeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysJyxwydRentIncome>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysJyxwydRentIncome> list = rptSpdAnysJyxwydRentIncomeService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysJyxwydRentIncome>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysJyxwydRentIncome>> index(QueryModel queryModel) {
        List<RptSpdAnysJyxwydRentIncome> list = rptSpdAnysJyxwydRentIncomeService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysJyxwydRentIncome>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptSpdAnysJyxwydRentIncome> show(@PathVariable("pkId") String pkId) {
        RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome = rptSpdAnysJyxwydRentIncomeService.selectByPrimaryKey(pkId);
        return new ResultDto<RptSpdAnysJyxwydRentIncome>(rptSpdAnysJyxwydRentIncome);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysJyxwydRentIncome> create(@RequestBody RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome) throws URISyntaxException {
        rptSpdAnysJyxwydRentIncomeService.insert(rptSpdAnysJyxwydRentIncome);
        return new ResultDto<RptSpdAnysJyxwydRentIncome>(rptSpdAnysJyxwydRentIncome);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome) throws URISyntaxException {
        int result = rptSpdAnysJyxwydRentIncomeService.update(rptSpdAnysJyxwydRentIncome);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptSpdAnysJyxwydRentIncomeService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysJyxwydRentIncomeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryGuarByLmtSerno
     * @函数描述:根据申请流水号查询抵押担保情况
     * @参数与返回说明:
     * @算法描述:
     */
   @PostMapping("/queryIncome")
    protected ResultDto<List<RptSpdAnysJyxwydRentIncomeDto>> queryIncome(@RequestBody Map<String,String> map) {
        String serno = map.get("serno");
        return new ResultDto<List<RptSpdAnysJyxwydRentIncomeDto>>(rptSpdAnysJyxwydRentIncomeService.queryIncome(serno));
    }

    @PostMapping("/saveIncome")
    protected ResultDto<Integer> saveIncome(@RequestBody RptSpdAnysJyxwydRentIncomeDto rptSpdAnysJyxwydRentIncomeDto){
       return new ResultDto<Integer>(rptSpdAnysJyxwydRentIncomeService.saveIncome(rptSpdAnysJyxwydRentIncomeDto));
    }

    @PostMapping("/deleteIncome")
    protected ResultDto<Integer> deleteIncome(@RequestBody RptSpdAnysJyxwydRentIncomeDto rptSpdAnysJyxwydRentIncomeDto){
       return new ResultDto<Integer>(rptSpdAnysJyxwydRentIncomeService.deleteIncome(rptSpdAnysJyxwydRentIncomeDto));
    }
}
