/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.EntlQyInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EntlQyInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface EntlQyInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    EntlQyInfo selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据面签信息看流水查询企业信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    EntlQyInfo selectBySerno(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<EntlQyInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(EntlQyInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(EntlQyInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(EntlQyInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(EntlQyInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 批量插入
     * @param entlQyInfos
     * @return
     */
    int insertEntlQyInfos(List<EntlQyInfo> entlQyInfos);

    /**
     * 查询客户是否存在房抵e点贷企业经营信息
     * @param param
     * @return
     */
    int selectCountByCusId(Map param);

    /**
     * @方法名称: updateCrpStatusBySerno
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int updateCrpStatusBySerno(@Param("serno") String serno);
}