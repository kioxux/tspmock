package cn.com.yusys.yusp.service.server.xdht0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0022.req.Xdht0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0022.resp.Xdht0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0022Service
 * @类描述: #服务类
 * @功能描述:根据客户号查询客户合同数据（合同号、合同起止日、合同状态）
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0022Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0022Service.class);
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易码：xdht0022
     * 交易描述：合同信息列表查询
     *
     * @param xdht0022DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0022DataRespDto xdht0022(Xdht0022DataReqDto xdht0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value);
        Xdht0022DataRespDto xdht0022DataRespDto = new Xdht0022DataRespDto();
        try {
            String cusId = xdht0022DataReqDto.getCusId();//客户号
            String certCode = xdht0022DataReqDto.getCertCode();//证件号码
            Integer startPageNum = xdht0022DataReqDto.getStartPageNum();//开始页数
            Integer pageSize = xdht0022DataReqDto.getPageSize();//条数

            //查询条件
            Map QueryMap = new HashMap();
            QueryMap.put("cusId", cusId);//客户号
            QueryMap.put("certCode", certCode);//客户号
            //查询结果
            logger.info("根据客户号合同信息列表查询开始,查询参数为:{}", JSON.toJSONString(QueryMap));
            PageHelper.startPage(startPageNum, pageSize);
            java.util.List<cn.com.yusys.yusp.dto.server.xdht0022.resp.List> lists = ctrLoanContMapper.queryCtrloanContDetailsByCusId(QueryMap);
            PageHelper.clearPage();
            logger.info("根据客户号合同信息列表查询结束,返回结果为:{}", JSON.toJSONString(lists));
            //查询总条数
            int num = ctrLoanContMapper.queryCtrloanContAllNUmByCusId(QueryMap);
            xdht0022DataRespDto.setTotalQnt(num);
            xdht0022DataRespDto.setList(lists);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value);
        return xdht0022DataRespDto;
    }

}
