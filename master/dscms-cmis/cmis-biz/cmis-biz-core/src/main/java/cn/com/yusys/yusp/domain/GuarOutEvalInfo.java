/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarOutEvalInfo
 * @类描述: guar_out_eval_info数据实体类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-12-22 20:14:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_out_eval_info")
public class GuarOutEvalInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 32)
	private String guarNo;
	
	/** 评估机构名称 **/
	@Column(name = "EVAL_ORG_NAME", unique = false, nullable = true, length = 450)
	private String evalOrgName;
	
	/** 外部评估机构组织机构代码 **/
	@Column(name = "EVAL_ORG_NO", unique = false, nullable = true, length = 10)
	private String evalOrgNo;
	
	/** 是否有外部预评估报告 **/
	@Column(name = "IS_PRE_REPORT", unique = false, nullable = true, length = 2)
	private String isPreReport;
	
	/** 外部预评估报告的评估价值 **/
	@Column(name = "PRE_OUT_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal preOutAmt;
	
	/** 外部正式评估报告的评估价值 **/
	@Column(name = "EVAL_OUT_AMT", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal evalOutAmt;
	
	/** 外部评估价值币种 **/
	@Column(name = "EVAL_OUT_CURRENCY", unique = false, nullable = true, length = 3)
	private String evalOutCurrency;
	
	/** 外部机构评估日期 **/
	@Column(name = "EVAL_OUT_DATE", unique = false, nullable = true, length = 10)
	private String evalOutDate;
	
	/** 外部评估价值有效期截止日 **/
	@Column(name = "OUT_END_DATE", unique = false, nullable = true, length = 10)
	private String outEndDate;
	
	/** 评估师姓名 **/
	@Column(name = "EVALER_NAME", unique = false, nullable = true, length = 100)
	private String evalerName;
	
	/** 评估师身份证号码 **/
	@Column(name = "EVALER_NO", unique = false, nullable = true, length = 18)
	private String evalerNo;
	
	/** 评估师移动电话 **/
	@Column(name = "EVALER_PHONE", unique = false, nullable = true, length = 11)
	private String evalerPhone;
	
	/** 评估师评估类型 **/
	@Column(name = "EVALER_TYPE", unique = false, nullable = true, length = 10)
	private String evalerType;
	
	/** 房地产评估师资质等级 **/
	@Column(name = "ROOM_APTI_LEVEL", unique = false, nullable = true, length = 10)
	private String roomAptiLevel;
	
	/** 土地评估师资质等级 **/
	@Column(name = "LAND_APTI_LEVEL", unique = false, nullable = true, length = 10)
	private String landAptiLevel;
	
	/** 资产评估师资质等级 **/
	@Column(name = "PROPERTY_APTI_LEVEL", unique = false, nullable = true, length = 10)
	private String propertyAptiLevel;
	
	/** 其他评估类型 **/
	@Column(name = "OTHER_TYPE", unique = false, nullable = true, length = 100)
	private String otherType;
	
	/** 其他评估师资质等级 **/
	@Column(name = "OTHER_APTI_LEVEL", unique = false, nullable = true, length = 10)
	private String otherAptiLevel;
	
	/** 评估类型 **/
	@Column(name = "EVAL_TYPE", unique = false, nullable = true, length = 2)
	private String evalType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param evalOrgName
	 */
	public void setEvalOrgName(String evalOrgName) {
		this.evalOrgName = evalOrgName;
	}
	
    /**
     * @return evalOrgName
     */
	public String getEvalOrgName() {
		return this.evalOrgName;
	}
	
	/**
	 * @param evalOrgNo
	 */
	public void setEvalOrgNo(String evalOrgNo) {
		this.evalOrgNo = evalOrgNo;
	}
	
    /**
     * @return evalOrgNo
     */
	public String getEvalOrgNo() {
		return this.evalOrgNo;
	}
	
	/**
	 * @param isPreReport
	 */
	public void setIsPreReport(String isPreReport) {
		this.isPreReport = isPreReport;
	}
	
    /**
     * @return isPreReport
     */
	public String getIsPreReport() {
		return this.isPreReport;
	}
	
	/**
	 * @param preOutAmt
	 */
	public void setPreOutAmt(java.math.BigDecimal preOutAmt) {
		this.preOutAmt = preOutAmt;
	}
	
    /**
     * @return preOutAmt
     */
	public java.math.BigDecimal getPreOutAmt() {
		return this.preOutAmt;
	}
	
	/**
	 * @param evalOutAmt
	 */
	public void setEvalOutAmt(java.math.BigDecimal evalOutAmt) {
		this.evalOutAmt = evalOutAmt;
	}
	
    /**
     * @return evalOutAmt
     */
	public java.math.BigDecimal getEvalOutAmt() {
		return this.evalOutAmt;
	}
	
	/**
	 * @param evalOutCurrency
	 */
	public void setEvalOutCurrency(String evalOutCurrency) {
		this.evalOutCurrency = evalOutCurrency;
	}
	
    /**
     * @return evalOutCurrency
     */
	public String getEvalOutCurrency() {
		return this.evalOutCurrency;
	}
	
	/**
	 * @param evalOutDate
	 */
	public void setEvalOutDate(String evalOutDate) {
		this.evalOutDate = evalOutDate;
	}
	
    /**
     * @return evalOutDate
     */
	public String getEvalOutDate() {
		return this.evalOutDate;
	}
	
	/**
	 * @param outEndDate
	 */
	public void setOutEndDate(String outEndDate) {
		this.outEndDate = outEndDate;
	}
	
    /**
     * @return outEndDate
     */
	public String getOutEndDate() {
		return this.outEndDate;
	}
	
	/**
	 * @param evalerName
	 */
	public void setEvalerName(String evalerName) {
		this.evalerName = evalerName;
	}
	
    /**
     * @return evalerName
     */
	public String getEvalerName() {
		return this.evalerName;
	}
	
	/**
	 * @param evalerNo
	 */
	public void setEvalerNo(String evalerNo) {
		this.evalerNo = evalerNo;
	}
	
    /**
     * @return evalerNo
     */
	public String getEvalerNo() {
		return this.evalerNo;
	}
	
	/**
	 * @param evalerPhone
	 */
	public void setEvalerPhone(String evalerPhone) {
		this.evalerPhone = evalerPhone;
	}
	
    /**
     * @return evalerPhone
     */
	public String getEvalerPhone() {
		return this.evalerPhone;
	}
	
	/**
	 * @param evalerType
	 */
	public void setEvalerType(String evalerType) {
		this.evalerType = evalerType;
	}
	
    /**
     * @return evalerType
     */
	public String getEvalerType() {
		return this.evalerType;
	}
	
	/**
	 * @param roomAptiLevel
	 */
	public void setRoomAptiLevel(String roomAptiLevel) {
		this.roomAptiLevel = roomAptiLevel;
	}
	
    /**
     * @return roomAptiLevel
     */
	public String getRoomAptiLevel() {
		return this.roomAptiLevel;
	}
	
	/**
	 * @param landAptiLevel
	 */
	public void setLandAptiLevel(String landAptiLevel) {
		this.landAptiLevel = landAptiLevel;
	}
	
    /**
     * @return landAptiLevel
     */
	public String getLandAptiLevel() {
		return this.landAptiLevel;
	}
	
	/**
	 * @param propertyAptiLevel
	 */
	public void setPropertyAptiLevel(String propertyAptiLevel) {
		this.propertyAptiLevel = propertyAptiLevel;
	}
	
    /**
     * @return propertyAptiLevel
     */
	public String getPropertyAptiLevel() {
		return this.propertyAptiLevel;
	}
	
	/**
	 * @param otherType
	 */
	public void setOtherType(String otherType) {
		this.otherType = otherType;
	}
	
    /**
     * @return otherType
     */
	public String getOtherType() {
		return this.otherType;
	}
	
	/**
	 * @param otherAptiLevel
	 */
	public void setOtherAptiLevel(String otherAptiLevel) {
		this.otherAptiLevel = otherAptiLevel;
	}
	
    /**
     * @return otherAptiLevel
     */
	public String getOtherAptiLevel() {
		return this.otherAptiLevel;
	}
	
	/**
	 * @param evalType
	 */
	public void setEvalType(String evalType) {
		this.evalType = evalType;
	}
	
    /**
     * @return evalType
     */
	public String getEvalType() {
		return this.evalType;
	}


}