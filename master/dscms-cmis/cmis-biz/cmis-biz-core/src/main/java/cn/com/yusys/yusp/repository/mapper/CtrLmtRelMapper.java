/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrLmtRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CtrLmtRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrLmtRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLmtRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-28 19:37:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrLmtRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    CtrLmtRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrLmtRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrLmtRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrLmtRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrLmtRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrLmtRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: selectByParamsKey
     * @方法描述: 根据业务流水号查询额度信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLmtRel> selectByParamsKey(@Param("serno") String serno);

    /**
     * 通过合同号查询数据
     * @param contNo
     * @return
     */
    List<CtrLmtRel> selectCtrLmtRelByContNo(@Param("contNo") String contNo);

    /**
     * 通过额度编号更新额度编号数据
     * @param updateMap
     * @return
     */
    int updateLmtLimitNo(Map updateMap);

    /**
     * 通过授信协议编号查询合同号信息
     * @param map
     * @return
     */
    List<String> getContnoByLmtCtrNo(Map map);
}