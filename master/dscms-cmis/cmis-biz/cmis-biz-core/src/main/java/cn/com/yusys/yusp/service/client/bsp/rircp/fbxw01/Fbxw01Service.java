package cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/20 14:37
 * @desc    无还本续贷普转，审批通过后，提交至风控二级准入
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Fbxw01Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * @param fbxw01ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto
     * @author hubp
     * @date 2021/5/20 14:42
     * @version 1.0.0
     * @desc    无还本续贷普转，审批通过后，提交至风控二级准入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Fbxw01RespDto fbxw01(Fbxw01ReqDto fbxw01ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value, JSON.toJSONString(fbxw01ReqDto));
        ResultDto<Fbxw01RespDto> fbxw01ResultDto = dscms2RircpClientService.fbxw01(fbxw01ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value, JSON.toJSONString(fbxw01ResultDto));
        String fbxw01Code = Optional.ofNullable(fbxw01ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fbxw01Meesage = Optional.ofNullable(fbxw01ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fbxw01RespDto fbxw01RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fbxw01ResultDto.getCode())) {
            //  获取相关的值并解析
            fbxw01RespDto = fbxw01ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(fbxw01Code, fbxw01Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value);
        return fbxw01RespDto;
    }

}
