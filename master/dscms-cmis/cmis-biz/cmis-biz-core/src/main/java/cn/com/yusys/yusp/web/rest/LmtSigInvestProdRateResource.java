/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestProdRate;
import cn.com.yusys.yusp.service.LmtSigInvestProdRateService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdRateResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-07 10:26:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestprodrate")
public class LmtSigInvestProdRateResource {
    @Autowired
    private LmtSigInvestProdRateService lmtSigInvestProdRateService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestProdRate>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestProdRate> list = lmtSigInvestProdRateService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestProdRate>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestProdRate>> index(QueryModel queryModel) {
        List<LmtSigInvestProdRate> list = lmtSigInvestProdRateService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestProdRate>>(list);
    }

    /**
     * @函数名称:selectall
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectall")
    protected ResultDto<List<LmtSigInvestProdRate>> selectall(@RequestBody QueryModel queryModel) {
        List<LmtSigInvestProdRate> list = lmtSigInvestProdRateService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestProdRate>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestProdRate> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestProdRate lmtSigInvestProdRate = lmtSigInvestProdRateService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestProdRate>(lmtSigInvestProdRate);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestProdRate> create(@RequestBody LmtSigInvestProdRate lmtSigInvestProdRate) throws URISyntaxException {
        lmtSigInvestProdRateService.insert(lmtSigInvestProdRate);
        return new ResultDto<LmtSigInvestProdRate>(lmtSigInvestProdRate);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestProdRate lmtSigInvestProdRate) throws URISyntaxException {
        int result = lmtSigInvestProdRateService.update(lmtSigInvestProdRate);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestProdRateService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestProdRateService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody QueryModel queryModel ) {
        String pkId = (String) queryModel.getCondition().get("pkId");
        int result = lmtSigInvestProdRateService.logicDelete(pkId);
        return new ResultDto<Integer>(result);
    }
}
