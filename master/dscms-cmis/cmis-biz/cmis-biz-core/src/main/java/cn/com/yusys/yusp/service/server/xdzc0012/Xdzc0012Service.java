package cn.com.yusys.yusp.service.server.xdzc0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.req.Xdzc0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.resp.Xdzc0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AsplAccpTaskService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:承兑行白名单额度校验
 *
 * @Author xs
 * @Date 2021/6/15 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0012Service {

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0012.Xdzc0012Service.class);

    /**
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0012DataRespDto xdzc0012Service(Xdzc0012DataReqDto xdzc0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value);
        Xdzc0012DataRespDto xdzc0012DataRespDto = new Xdzc0012DataRespDto();
        String opFlag = CmisBizConstants.SUCCESS;
        String opMsg = "承兑行白名单额度校验通过";
        String opType = xdzc0012DataReqDto.getOpType();// 为后期准备
        try {
            logger.info("承兑行白名单额度占用检验-额度系统：");
            java.util.List<cn.com.yusys.yusp.dto.server.xdzc0012.req.List> list = xdzc0012DataReqDto.getList();
            List<CmisLmt0024OccRelListReqDto> occRelList = new ArrayList<CmisLmt0024OccRelListReqDto>();
            // 判断是否是资产池银票出票
            occRelList = list.parallelStream().map(e->{
                CmisLmt0024OccRelListReqDto dealBiz = new CmisLmt0024OccRelListReqDto();
                dealBiz.setDealBizNo(e.geteBillNo());//交易业务编号
                dealBiz.setCusId(e.getImnManCusId());
                dealBiz.setCusName(e.getImnCusName());
                dealBiz.setPrdNo("012040");//产品编号
                dealBiz.setPrdName("资产池银票质押入池");//产品名称
                dealBiz.setOrigiDealBizNo("");//原交易业务编号
                dealBiz.setOrigiRecoverType("");//原交易业务恢复类型
                dealBiz.setStartDate(e.getIsseDate());//起始日(出票日)
                dealBiz.setEndDate(e .getIsseEndDate());//到期日(到期日)
                dealBiz.setOrgCusId(e.getAorgNo());//承兑行客户号
                dealBiz.setBizTotalAmt(e.getDrftAmt());//占用金额
                return dealBiz;
            }).collect(Collectors.toList());
            CmisLmt0024ReqDto cmisLmt0024ReqDto = new CmisLmt0024ReqDto();
            // 是否占用
            cmisLmt0024ReqDto.setIsOccLmt("0");
            // 系统编号
            cmisLmt0024ReqDto.setSysNo(EsbEnum.SERVTP_EBS.key);
            // 金融机构代码
            cmisLmt0024ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            cmisLmt0024ReqDto.setInputId(xdzc0012DataReqDto.getManagerId());
            cmisLmt0024ReqDto.setInputBrId(xdzc0012DataReqDto.getManagerBrId());
            cmisLmt0024ReqDto.setOccRelList(occRelList);
            cmisLmt0024ReqDto.setInputDate(DateUtils.getCurrDateStr());

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(cmisLmt0024ReqDto));
            ResultDto<CmisLmt0024RespDto> cmisLmt0024RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0024(cmisLmt0024ReqDto)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0024.value, JSON.toJSONString(cmisLmt0024RespDtoResultDto));
            if (Objects.equals(cmisLmt0024RespDtoResultDto.getCode(), "0")) {
                if(!Objects.equals(cmisLmt0024RespDtoResultDto.getData().getErrorCode(), "0000")){
                    throw BizException.error(null, "9999",cmisLmt0024RespDtoResultDto.getData().getErrorMsg());
                }
            }else{
                throw BizException.error(null, "9999",cmisLmt0024RespDtoResultDto.getMessage());
            }
        } catch (BizException e) {
            opFlag = CmisBizConstants.FAIL;
            opMsg = e.getMessage();
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, e.getMessage());
        } catch (Exception e) {
            opFlag = CmisBizConstants.FAIL;
            opMsg = e.getMessage();
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, e.getMessage());
        } finally {
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value);
            xdzc0012DataRespDto.setOpFlag(opFlag);// 成功失败标志
            xdzc0012DataRespDto.setOpMsg(opMsg);// 描述信
            return xdzc0012DataRespDto;
        }
    }
}