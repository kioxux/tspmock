///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.web.rest;
//
//import java.net.URISyntaxException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.SurveyReportComInfo;
//import cn.com.yusys.yusp.service.SurveyReportComInfoService;
//
///**
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyReportComInfoResource
// * @类描述: #资源类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 13:55:08
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/surveyreportcominfo")
//public class SurveyReportComInfoResource {
//    @Autowired
//    private SurveyReportComInfoService surveyReportComInfoService;
//
//	/**
//     * 全表查询.
//     *
//     * @return
//     */
//    @GetMapping("/query/all")
//    protected ResultDto<List<SurveyReportComInfo>> query() {
//        QueryModel queryModel = new QueryModel();
//        List<SurveyReportComInfo> list = surveyReportComInfoService.selectAll(queryModel);
//        return new ResultDto<List<SurveyReportComInfo>>(list);
//    }
//
//    /**
//     * @函数名称:index
//     * @函数描述:查询对象列表，公共API接口
//     * @参数与返回说明:
//     * @param queryModel
//     *            分页查询类
//     * @算法描述:
//     */
//    @GetMapping("/")
//    protected ResultDto<List<SurveyReportComInfo>> index(QueryModel queryModel) {
//        List<SurveyReportComInfo> list = surveyReportComInfoService.selectByModel(queryModel);
//        return new ResultDto<List<SurveyReportComInfo>>(list);
//    }
//
//    /**
//     * @函数名称:show
//     * @函数描述:查询单个对象，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @GetMapping("/{surveyNo}")
//    protected ResultDto<SurveyReportComInfo> show(@PathVariable("surveyNo") String surveyNo) {
//        SurveyReportComInfo surveyReportComInfo = surveyReportComInfoService.selectByPrimaryKey(surveyNo);
//        return new ResultDto<SurveyReportComInfo>(surveyReportComInfo);
//    }
//
//    /**
//     * @函数名称:create
//     * @函数描述:实体类创建，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/")
//    protected ResultDto<SurveyReportComInfo> create(@RequestBody SurveyReportComInfo surveyReportComInfo) throws URISyntaxException {
//        surveyReportComInfoService.insert(surveyReportComInfo);
//        return new ResultDto<SurveyReportComInfo>(surveyReportComInfo);
//    }
//
//    /**
//     * @函数名称:update
//     * @函数描述:对象修改，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/update")
//    protected ResultDto<Integer> update(@RequestBody SurveyReportComInfo surveyReportComInfo) throws URISyntaxException {
//        int result = surveyReportComInfoService.update(surveyReportComInfo);
//        return new ResultDto<Integer>(result);
//    }
//
//
//    /**
//     * @函数名称:delete
//     * @函数描述:单个对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/delete/{surveyNo}")
//    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
//        int result = surveyReportComInfoService.deleteByPrimaryKey(surveyNo);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:batchdelete
//     * @函数描述:批量对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/batchdelete/{ids}")
//    protected ResultDto<Integer> deletes(@PathVariable String ids) {
//        int result = surveyReportComInfoService.deleteByIds(ids);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @创建人 WH
//     * @创建时间 13:56 2021-04-12
//     * @return 测试接口
//     **/
//    @PostMapping("/selectenterprise")
//    protected Map findOne123(@RequestBody Map map) {
////      String newName="查询的企业是"+name+";功能开发中:等待查询企业相关接口研发";
//        Map newMap=new HashMap();
//        newMap.put("con_name",map.get("conName"));
//        newMap.put("con_type","互联网企业");
//        newMap.put("con_cert_type","100");
//        newMap.put("legal","测试法人代表");
//        newMap.put("blic_years","营业执照年限到2022年");
//        newMap.put("trade","it互联网");
//        newMap.put("blic_years","营业执照年限到2022年");
//        return newMap;
//    }
//}
