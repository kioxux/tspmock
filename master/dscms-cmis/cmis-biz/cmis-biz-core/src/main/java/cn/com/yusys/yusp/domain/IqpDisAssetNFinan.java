/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetNFinan
 * @类描述: iqp_dis_asset_n_finan数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_dis_asset_n_finan")
public class IqpDisAssetNFinan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 非金融资产表主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "NFINAN_PK")
	private String nfinanPk;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	@Column(name = "APPT_CODE", unique = false, nullable = false, length = 32)
	private String apptCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 非金融资产类型 **/
	@Column(name = "N_FIN_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String nFinAssetType;
	
	/** 房屋属性 **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 2)
	private String houseType;
	
	/** 房屋价值 **/
	@Column(name = "HOUSE_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal houseValue;
	
	/** 抵押余额 **/
	@Column(name = "MORTGAGE_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mortgageBal;
	
	/** 其他非金融资产（折算后）金额 **/
	@Column(name = "OTHER_FINAN_ASSET", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherFinanAsset;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 客户经理认定收入金额 **/
	@Column(name = "MANAGER_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	@Column(name = "BRANCH_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	@Column(name = "HEAD_EX_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal headExIncome;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param nfinanPk
	 */
	public void setNfinanPk(String nfinanPk) {
		this.nfinanPk = nfinanPk;
	}
	
    /**
     * @return nfinanPk
     */
	public String getNfinanPk() {
		return this.nfinanPk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param nFinAssetType
	 */
	public void setNFinAssetType(String nFinAssetType) {
		this.nFinAssetType = nFinAssetType;
	}
	
    /**
     * @return nFinAssetType
     */
	public String getNFinAssetType() {
		return this.nFinAssetType;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param houseValue
	 */
	public void setHouseValue(java.math.BigDecimal houseValue) {
		this.houseValue = houseValue;
	}
	
    /**
     * @return houseValue
     */
	public java.math.BigDecimal getHouseValue() {
		return this.houseValue;
	}
	
	/**
	 * @param mortgageBal
	 */
	public void setMortgageBal(java.math.BigDecimal mortgageBal) {
		this.mortgageBal = mortgageBal;
	}
	
    /**
     * @return mortgageBal
     */
	public java.math.BigDecimal getMortgageBal() {
		return this.mortgageBal;
	}
	
	/**
	 * @param otherFinanAsset
	 */
	public void setOtherFinanAsset(java.math.BigDecimal otherFinanAsset) {
		this.otherFinanAsset = otherFinanAsset;
	}
	
    /**
     * @return otherFinanAsset
     */
	public java.math.BigDecimal getOtherFinanAsset() {
		return this.otherFinanAsset;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return managerIncome
     */
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return branchExIncome
     */
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return headExIncome
     */
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}