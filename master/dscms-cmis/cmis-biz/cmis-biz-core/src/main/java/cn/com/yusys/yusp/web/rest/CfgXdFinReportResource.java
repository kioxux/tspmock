/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgXdFinReport;
import cn.com.yusys.yusp.service.CfgXdFinReportService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgXdFinReportResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:29:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgxdfinreport")
public class CfgXdFinReportResource {
    @Autowired
    private CfgXdFinReportService cfgXdFinReportService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgXdFinReport>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgXdFinReport> list = cfgXdFinReportService.selectAll(queryModel);
        return new ResultDto<List<CfgXdFinReport>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CfgXdFinReport>> index(QueryModel queryModel) {
        List<CfgXdFinReport> list = cfgXdFinReportService.selectByModel(queryModel);
        return new ResultDto<List<CfgXdFinReport>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{codeNo}")
    protected ResultDto<CfgXdFinReport> show(@PathVariable("codeNo") String codeNo) {
        CfgXdFinReport cfgXdFinReport = cfgXdFinReportService.selectByPrimaryKey(codeNo);
        return new ResultDto<CfgXdFinReport>(cfgXdFinReport);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CfgXdFinReport> create(@RequestBody CfgXdFinReport cfgXdFinReport) throws URISyntaxException {
        cfgXdFinReportService.insert(cfgXdFinReport);
        return new ResultDto<CfgXdFinReport>(cfgXdFinReport);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgXdFinReport cfgXdFinReport) throws URISyntaxException {
        int result = cfgXdFinReportService.update(cfgXdFinReport);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{codeNo}")
    protected ResultDto<Integer> delete(@PathVariable("codeNo") String codeNo) {
        int result = cfgXdFinReportService.deleteByPrimaryKey(codeNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgXdFinReportService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
