/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocImageSpplInfo
 * @类描述: doc_image_sppl_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-07 16:15:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_image_sppl_info")
public class DocImageSpplInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 影像补扫编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DISI_SERNO")
	private String disiSerno;
	
	/** 原业务流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 影像补扫类型 **/
	@Column(name = "SPPL_TYPE", unique = false, nullable = true, length = 5)
	private String spplType;
	
	/** 任务状态 **/
	@Column(name = "TASK_FLAG", unique = false, nullable = true, length = 5)
	private String taskFlag;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 是否删除资料 **/
	@Column(name = "IS_DEL_DATA", unique = false, nullable = true, length = 5)
	private String isDelData;
	
	/** 删除内容 **/
	@Column(name = "DEL_CONTENT", unique = false, nullable = true, length = 500)
	private String delContent;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 补扫业务品种 **/
	@Column(name = "SPPL_BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String spplBizType;
	
	/** 原业务流程实例 **/
	@Column(name = "BIZ_INSTANCE_ID", unique = false, nullable = true, length = 32)
	private String bizInstanceId;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;


	/**
	 * @param disiSerno
	 */
	public void setDisiSerno(String disiSerno) {
		this.disiSerno = disiSerno;
	}
	
    /**
     * @return disiSerno
     */
	public String getDisiSerno() {
		return this.disiSerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param spplType
	 */
	public void setSpplType(String spplType) {
		this.spplType = spplType;
	}
	
    /**
     * @return spplType
     */
	public String getSpplType() {
		return this.spplType;
	}
	
	/**
	 * @param taskFlag
	 */
	public void setTaskFlag(String taskFlag) {
		this.taskFlag = taskFlag;
	}
	
    /**
     * @return taskFlag
     */
	public String getTaskFlag() {
		return this.taskFlag;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param isDelData
	 */
	public void setIsDelData(String isDelData) {
		this.isDelData = isDelData;
	}
	
    /**
     * @return isDelData
     */
	public String getIsDelData() {
		return this.isDelData;
	}
	
	/**
	 * @param delContent
	 */
	public void setDelContent(String delContent) {
		this.delContent = delContent;
	}
	
    /**
     * @return delContent
     */
	public String getDelContent() {
		return this.delContent;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param spplBizType
	 */
	public void setSpplBizType(String spplBizType) {
		this.spplBizType = spplBizType;
	}
	
    /**
     * @return spplBizType
     */
	public String getSpplBizType() {
		return this.spplBizType;
	}
	
	/**
	 * @param bizInstanceId
	 */
	public void setBizInstanceId(String bizInstanceId) {
		this.bizInstanceId = bizInstanceId;
	}
	
    /**
     * @return bizInstanceId
     */
	public String getBizInstanceId() {
		return this.bizInstanceId;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}
}