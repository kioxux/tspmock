package cn.com.yusys.yusp.web.server.xdxw0076;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdxw0076.Xdxw0076Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0076.req.Xdxw0076DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0076.resp.Xdxw0076DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * 接口处理类:渠道端查询对公客户我的贷款（授信申请流程监控）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0076:渠道端查询对公客户我的贷款（授信申请流程监控）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0076Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0076Resource.class);

    @Resource
    private Xdxw0076Service xdxw0076Service;

    /**
     * 交易码：xdxw0076
     * 交易描述：渠道端查询对公客户我的贷款（授信申请流程监控）
     *
     * @param xdxw0076DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("渠道端查询对公客户我的贷款（授信申请流程监控）")
    @PostMapping("/xdxw0076")
    protected @ResponseBody
    ResultDto<Xdxw0076DataRespDto> xdxw0076(@Validated @RequestBody Xdxw0076DataReqDto xdxw0076DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataReqDto));
        Xdxw0076DataRespDto xdxw0076DataRespDto = new Xdxw0076DataRespDto();// 响应Dto:渠道端查询对公客户我的贷款（授信申请流程监控）
        ResultDto<Xdxw0076DataRespDto> xdxw0076DataResultDto = new ResultDto<>();
        try {
            String cusId = xdxw0076DataReqDto.getCusId();
            if (StringUtil.isEmpty(cusId)) {
                xdxw0076DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0076DataResultDto.setMessage("客户号【cusId】不能为空！");
                return xdxw0076DataResultDto;
            }
            // 从xdxw0076DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataReqDto));
            xdxw0076DataRespDto = xdxw0076Service.getXdxw0076(xdxw0076DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataRespDto));

            // 封装xdxw0076DataResultDto中正确的返回码和返回信息
            xdxw0076DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0076DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, e.getMessage());
            // 封装xdxw0076DataResultDto中异常返回码和返回信息
            xdxw0076DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0076DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0076DataRespDto到xdxw0076DataResultDto中
        xdxw0076DataResultDto.setData(xdxw0076DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataResultDto));
        return xdxw0076DataResultDto;
    }
}
