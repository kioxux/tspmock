package cn.com.yusys.yusp.web.server.xdtz0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0008.req.Xdtz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.resp.Xdtz0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0008.Xdtz0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号获取正常周转次数
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDTZ0008:根据客户号获取正常周转次数")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0008Resource.class);
    @Autowired
    private Xdtz0008Service xdtz0008Service;

    /**
     * 交易码：xdtz0008
     * 交易描述：根据客户号获取正常周转次数
     *
     * @param xdtz0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号获取正常周转次数")
    @PostMapping("/xdtz0008")
    protected @ResponseBody
    ResultDto<Xdtz0008DataRespDto> xdtz0008(@Validated @RequestBody Xdtz0008DataReqDto xdtz0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataReqDto));
        Xdtz0008DataRespDto xdtz0008DataRespDto = new Xdtz0008DataRespDto();// 响应Dto:根据客户号获取正常周转次数
        ResultDto<Xdtz0008DataRespDto> xdtz0008DataResultDto = new ResultDto<>();
        // 从xdtz0008DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0008DataReqDto));
            xdtz0008DataRespDto = xdtz0008Service.xdtz0008(xdtz0008DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0008DataRespDto));
            // 封装xdtz0008DataResultDto中正确的返回码和返回信息
            xdtz0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, e.getMessage());
            xdtz0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0008DataRespDto到xdtz0008DataResultDto中
        xdtz0008DataResultDto.setData(xdtz0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataResultDto));
        return xdtz0008DataResultDto;
    }
}
