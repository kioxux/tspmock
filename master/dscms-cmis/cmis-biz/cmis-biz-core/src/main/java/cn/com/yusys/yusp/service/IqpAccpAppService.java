/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.IqpAccpAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAccpAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-14 13:45:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpAccpAppService {

    private static final Logger log = LoggerFactory.getLogger(IqpAccpAppService.class);
    @Autowired
    private IqpAccpAppMapper iqpAccpAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private CtrAccpContService ctrAccpContService;//银承合同主表

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同表

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private  AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private ICusClientService icusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAccpApp selectByPrimaryKey(String pkId) {
        return iqpAccpAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpAccpApp> selectAll(QueryModel model) {
        List<IqpAccpApp> records = (List<IqpAccpApp>) iqpAccpAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpAccpApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpAccpApp> list = iqpAccpAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpAccpApp record) {
        return iqpAccpAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpAccpApp record) {
        return iqpAccpAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpAccpApp record) {
        return iqpAccpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(IqpAccpApp record) {
        return iqpAccpAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpAccpAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpAccpAppMapper.deleteByIds(ids);
    }


    /**
     * 银承合同申请新增页面点击下一步
     * @param iqpAccpApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpAccpAppInfo(IqpAccpApp iqpAccpApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpAccpApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpAccpApp.getCusId();
            // 将 地址与联系方式 信息更新值贷款申请表中
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",cusId);
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",cusId, cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpAccpApp.setPhone(CusIndivAllDto.getMobile());
                    iqpAccpApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpAccpApp.setEmail(CusIndivAllDto.getEmail());
                    iqpAccpApp.setQq(CusIndivAllDto.getQq());
                    iqpAccpApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpAccpApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpAccpApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpAccpApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpAccpApp.setFax(cusCorpDto.getFax());
                    iqpAccpApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpAccpApp.setQq(cusCorpDto.getQq());
                    iqpAccpApp.setWechat(cusCorpDto.getWechatNo());
                    iqpAccpApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }

            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpAccpApp.setInputId(userInfo.getLoginCode());
                iqpAccpApp.setInputBrId(userInfo.getOrg().getCode());
                iqpAccpApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }

            Map seqMap = new HashMap();
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            HashMap<String, String> param = new HashMap<>();
            String dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_07);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);
            iqpAccpApp.setSerno(serno);
            iqpAccpApp.setContNo(contNo);
            iqpAccpApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpAccpApp.setPkId(StringUtils.uuid(true));
            iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpAccpApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpAccpApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_07);

            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpAccpApp.getLmtAccNo())){
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpAccpApp.getLmtAccNo());
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",iqpAccpApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpAccpApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }


            int insertCount = iqpAccpAppMapper.insertSelective(iqpAccpApp);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno",serno);
            result.put("contNo",contNo);
            log.info("银承合同申请"+serno+"-保存成功！");
            //根据选择的额度分项自动生成担保合同
            IqpAccpApp iqpAccpAppData = iqpAccpAppMapper.selectByIqpSerno(serno);
            if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpAccpAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpAccpAppData.getGuarMode())
                    && !CmisCommonConstants.GUAR_MODE_40.equals(iqpAccpAppData.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpAccpAppData.getGuarMode())) {
                String subSerno = "";
                if(iqpAccpAppData.getLmtAccNo()==null||"".equals(iqpAccpAppData.getLmtAccNo())){
                    subSerno = "";
                }else{
                    subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(iqpAccpAppData.getLmtAccNo());//GUAR_BIZ_REL
                }
                GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                grtGuarContDto.setSerno(iqpAccpAppData.getSerno());//业务流水号
                grtGuarContDto.setCusId(iqpAccpAppData.getCusId());//借款人编号
                grtGuarContDto.setBizLine(iqpAccpAppData.getBelgLine());//业务条线
                grtGuarContDto.setGuarWay(iqpAccpAppData.getGuarMode());//担保方式   //10抵押 20 质押 30保证
                grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                grtGuarContDto.setGuarAmt(iqpAccpAppData.getContAmt());//担保金额
                grtGuarContDto.setGuarTerm(iqpAccpAppData.getContTerm());//担保期限
                grtGuarContDto.setGuarStartDate(iqpAccpAppData.getStartDate());//担保起始日
                grtGuarContDto.setGuarEndDate(iqpAccpAppData.getEndDate());//担保终止日
                grtGuarContDto.setReplyNo(iqpAccpAppData.getReplyNo());//批复编号
                grtGuarContDto.setLmtAccNo(iqpAccpAppData.getLmtAccNo());//授信额度编号
                grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                grtGuarContDto.setCusName(iqpAccpAppData.getCusName());//借款人名称
                grtGuarContDto.setGuarContType(iqpAccpAppData.getContType());//担保合同类型
                grtGuarContDto.setInputId(iqpAccpAppData.getInputId());//登记人
                grtGuarContDto.setInputBrId(iqpAccpAppData.getInputBrId());//登记机构
                grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
            }

            // 生成用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpAccpAppData.getReplyNo());
            queryMap.put("contNo",iqpAccpAppData.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(iqpAccpApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        }catch(YuspException e){
            log.error("银承合同申请新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("保存银承合同申请异常！",e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }


    /**
     * 银承申请提交保存方法
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpAccpAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "银承申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpAccpApp iqpAccpApp = JSONObject.parseObject(JSON.toJSONString(params), IqpAccpApp.class);
            if (iqpAccpApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存银承申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpAccpApp.setUpdId(userInfo.getLoginCode());
                iqpAccpApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpAccpApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpAccpApp.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_07);
            }

            log.info(logPrefix + "保存银承申请数据");
            int updCount = iqpAccpAppMapper.updateByPrimaryKeySelective(iqpAccpApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存银承申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("流程发起-获取银承合同申请" + iqpSerno + "申请主表信息");
        IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
        if (iqpAccpApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        int updateCount = 0;
        log.info("流程发起-更新银承合同申请" + iqpSerno + "流程审批状态为【111】-审批中");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //向额度系统发送接口,占用额度
        this.sendToLmt(iqpAccpApp);
    }

    public void sendToLmt(IqpAccpApp iqpAccpApp) {
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpAccpApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpAccpApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //向额度系统发送接口,占用额度
        String guarMode = iqpAccpApp.getGuarMode();
        //不是低风险业务
        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode) && !CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                && !CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpAccpApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpAccpApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpAccpApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpAccpApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpAccpApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpAccpApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpAccpApp.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpAccpApp.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(iqpAccpApp.getBailAmt());//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpAccpApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpAccpApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpAccpApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpAccpApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpAccpApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpAccpApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpAccpApp.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpAccpApp.getContHighAvlAmt().add(iqpAccpApp.getBailAmt()));//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(iqpAccpApp.getContHighAvlAmt());//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpAccpApp.getContHighAvlAmt().add(iqpAccpApp.getBailAmt()));//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpAccpApp.getContHighAvlAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }

        //是低风险且授信不足额的占额
        if ((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
                CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) && iqpAccpApp.getLmtAccNo()!=null){

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpAccpApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpAccpApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpAccpApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpAccpApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpAccpApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpAccpApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpAccpApp.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpAccpApp.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(iqpAccpApp.getBailAmt());//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpAccpApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpAccpApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpAccpApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpAccpApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpAccpApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpAccpApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpAccpApp.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpAccpApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpAccpApp.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
    }

    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpAccpAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + iqpSerno + "申请信息");
            IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);

            if (iqpAccpApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpAccpAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("银承合同申请否决流程-获取银承合同申请" + iqpSerno + "申请信息");
            IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
            if (iqpAccpApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (iqpAccpApp.getLmtAccNo() != null && !"".equals(iqpAccpApp.getLmtAccNo())) {
                String guarMode = iqpAccpApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpAccpApp.getContHighAvlAmt();
                } else {
                    recoverSpacAmtCny = iqpAccpApp.getContHighAvlAmt();
                    recoverAmtCny = iqpAccpApp.getContHighAvlAmt().add(iqpAccpApp.getBailAmt());
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpAccpApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpAccpApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpAccpApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("银承合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("银承合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取银承合同申请" + iqpSerno + "申请主表信息");
        IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
        if (iqpAccpApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        //更新审批状态
        int updateCount = 0;
        log.info("审批通过-更新银承业务申请" + iqpSerno + "流程审批状态为【997】-通过");
        iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        updateCount = this.updateSelective(iqpAccpApp);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //判断是否需要反向生成低风险分项，以及后续的逻辑处理
        if(iqpAccpApp.getLmtAccNo()==null || "".equals(iqpAccpApp.getLmtAccNo())){
            log.info("银承合同申请: "+iqpSerno+"反向生成低风险额度开始");
            boolean result = lmtReplyAccService.generaLmtReplyAccForLowRisk(iqpAccpApp.getContAmt(), iqpAccpApp.getCusId(), iqpAccpApp.getBusiType(), iqpAccpApp.getBailPerc(), iqpAccpApp.getChrgRate(),iqpAccpApp.getEndDate(),CmisCommonConstants.STD_ZB_YES_NO_0);
            if(!result){
                log.error("反向生成低风险额度分项异常");
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            log.info("银承合同申请: "+iqpSerno+"反向生成低风险额度结束");
            String replySerno = "";
            String lmtAccNo = "";
            log.info("银承合同申请: "+iqpSerno+"获取批复编号和授信额度编号开始");
            // 根据客户号以及产品编号查询生成的分项下对应的低风险分项明细
            CmisLmt0056ReqDto cmisLmt0056ReqDto = new CmisLmt0056ReqDto();
            cmisLmt0056ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));
            cmisLmt0056ReqDto.setPrdId(iqpAccpApp.getPrdId());
            cmisLmt0056ReqDto.setCusId(iqpAccpApp.getCusId());
            log.info("银承合同业务申请【{}】，前往额度系统查询低风险分项明细,请求报文为:【{}】", iqpSerno, cmisLmt0056ReqDto.toString());
            ResultDto<CmisLmt0056RespDto> resultDto = cmisLmtClientService.cmislmt0056(cmisLmt0056ReqDto);
            log.info("银承合同业务申请【{}】，前往额度系统查询低风险分项明细,响应报文为:【{}】", iqpSerno, resultDto.toString());
            if(!"0".equals(resultDto.getCode())){
                log.error("额度0056接口调用异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(resultDto.getData().getErrorCode())){
                log.error("查询低风险分项明细失败！异常信息为【{}】",resultDto.getData().getErrorMsg());
                throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
            }
            lmtAccNo = resultDto.getData().getApprSubSerno();
            log.info("银承合同申请【{}】的授信额度编号为【{}】",iqpSerno,lmtAccNo);
            // 根据授信额度编号获取批复流水号
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                log.error("根据授信额度编号【{}】查询授信产品明细为空！",lmtAccNo);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }

            Map map = new HashMap();
            map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
            if(Objects.isNull(lmtReplyAccSub)){
                log.error("根据分项额度编号【{}】查询授信分项明细为空！",lmtReplyAccSubPrd.getAccSubNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            replySerno = lmtReplyAccSub.getReplySerno();
            log.info("保函合同申请【{}】的批复编号为【{}】",iqpSerno,replySerno);

            //生成低风险分项后，更新申请数据
            int updateCountData = 0;
            log.info("更新银承业务申请" + iqpSerno + "开始");
            IqpAccpApp iqpAccpAppData = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
            iqpAccpAppData.setLmtAccNo(lmtAccNo);
            iqpAccpAppData.setReplyNo(replySerno);
            log.info("批复编号为【{}】,授信额度编号为【{}】",lmtAccNo,replySerno);
            updateCountData = this.updateSelective(iqpAccpAppData);
            if (updateCountData < 0) {
                log.error("更新保函业务申请【{}】异常",iqpSerno);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            log.info("更新银承业务申请" + iqpSerno + "结束");
        }


        //审批通过后，生成合同数据
        IqpAccpApp iqpAccpAppObj = iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
        log.info("审批通过生成银承合同" + iqpSerno + "开始");
        CtrAccpCont ctrAccpCont = new CtrAccpCont();
        BeanUtils.copyProperties(iqpAccpAppObj, ctrAccpCont);
        ctrAccpCont.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_07);
        ctrAccpCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);//合同状态默认【未生效】
        //iqpContHighAmtAgr.setContPrintNum(new BigDecimal("0"));//合同打印次数默认设置为【0】
        ctrAccpCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);
        int insertCount = ctrAccpContService.insertSelective(ctrAccpCont);
        if (insertCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }
        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpAccpApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpAccpApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrAccpCont origiCtrAccpCont = ctrAccpContService.selectByContNo(origiContNo);
                if(Objects.nonNull(origiCtrAccpCont)) {
                    origiCtrAccpCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    updateCount = ctrAccpContService.updateSelective(origiCtrAccpCont);
                    if (updateCount < 1) {
                        throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                    }
                }

            }
        }

        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpAccpApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpAccpApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }

        //低风险足额的占额处理
        String guarMode = iqpAccpAppObj.getGuarMode();
        //判断是否是低风险的一步流程，是：进行低风险占额后再按照正常流程走；否：直接按照正常流程走
        if((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode) ||
                CmisCommonConstants.GUAR_MODE_40.equals(guarMode))) {

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpAppObj.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpAccpAppObj.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpAccpAppObj.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpAccpAppObj.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpAccpAppObj.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpAccpAppObj.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpAccpAppObj.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpAccpAppObj.getContHighAvlAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(iqpAccpAppObj.getBailPerc());//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(iqpAccpAppObj.getBailAmt());//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpAccpAppObj.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpAccpAppObj.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpAccpAppObj.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpAccpAppObj.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpAccpAppObj.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpAccpAppObj.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpAccpAppObj.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpAccpAppObj.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpAccpAppObj.getContHighAvlAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("银承合同业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }
        log.info("生成银承合同" + iqpSerno + "结束");
    }
    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public IqpAccpApp selectByAccpSernoKey(String iqpSerno) {
        return iqpAccpAppMapper.selectByAccpSernoKey(iqpSerno);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpAccpApp> toSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_000992111);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpAccpAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpAccpApp> doneSignlist(QueryModel model) {
        HashMap<String, String > queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_996997998);
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        return iqpAccpAppMapper.selectByModel(model);
    }


    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpAccpApp> selectForLmtAccNo(String lmtAccNo){
        return iqpAccpAppMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(IqpAccpApp iqpAccpApp) {
        if(CmisCommonConstants.WF_STATUS_000.equals(iqpAccpApp.getApproveStatus())){
            //删除关联的担保合同及担保合同与押品关系表
            bizCommonService.logicDeleteGrtGuarCont(iqpAccpApp.getSerno());

            iqpAccpApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            iqpAccpAppMapper.updateByPrimaryKeySelective(iqpAccpApp);

        }else if(CmisCommonConstants.WF_STATUS_992.equals(iqpAccpApp.getApproveStatus())){
            iqpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            iqpAccpAppMapper.updateByPrimaryKeySelective(iqpAccpApp);
            //删除流程实例
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpAccpApp.getSerno());
            if (iqpAccpApp.getLmtAccNo() != null && !"".equals(iqpAccpApp.getLmtAccNo())) {
                String guarMode = iqpAccpApp.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpAccpApp.getContHighAvlAmt();
                } else {
                    recoverSpacAmtCny = iqpAccpApp.getContHighAvlAmt();
                    recoverAmtCny = iqpAccpApp.getContHighAvlAmt().add(iqpAccpApp.getBailAmt());
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpAccpApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpAccpApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpAccpApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("银承合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("银承合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpAccpApp.getSerno(), JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("调用额度12接口恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorMsg());
                }
            }
        }
        return iqpAccpAppMapper.updateByPrimaryKey(iqpAccpApp);
    }

    /**
     * @方法名称: selectByIqpSerno
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpAccpApp selectByIqpSerno(String iqpSerno) {
        return iqpAccpAppMapper.selectByIqpSerno(iqpSerno);
    }

    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 银承合同申请判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpAccpApp iqpAccpApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("银承合同申请判断是否足额,流水号【{}】", iqpAccpApp.getSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpAccpApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpAccpApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpAccpApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" + iqpAccpApp.getLmtAccNo());
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        if(cmisLmt0026RespDto != null && iqpAccpApp.getContHighAvlAmt().compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 银承合同申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("银承合同申请更新流程参数,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpAccpApp iqpAccpApp = this.selectByIqpSerno(serno);
        Map<String, Object> params = new HashMap<>();
        if(CmisCommonConstants.GUAR_MODE_60.equals(iqpAccpApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_21.equals(iqpAccpApp.getGuarMode())
                || CmisCommonConstants.GUAR_MODE_40.equals(iqpAccpApp.getGuarMode())){
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", iqpAccpApp.getContHighAvlAmt().add( iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpAccpApp.getCusId(), iqpAccpApp.getManagerBrId())));
        }else{
            // 是否低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", new BigDecimal("0"));
        }
        // 是否足额
        if(iqpAccpApp.getLmtAccNo()!=null){
            params.put("isAmtEnough", checkLmtAmtIsEnough(iqpAccpApp));
        }else{
            params.put("isAmtEnough", CmisCommonConstants.STD_ZB_YES_NO_0);
        }

        // 申请金额
        params.put("amt", iqpAccpApp.getContHighAvlAmt());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }


    /**
     * 获取基本信息
     *
     * @param params
     * @return
     */
    public IqpAccpApp queryIqpAccpAppDataByParams(HashMap<String, String> params) {
        return iqpAccpAppMapper.queryIqpAccpAppDataByParams(params);
    }

    /**
     * @方法名称: iqpAccpAppSubmitNoFlow
     * @方法描述: 村镇银行无流程提交后续处理 参照DGYX01BizService
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map iqpAccpAppSubmitNoFlow(String serno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            log.info("村镇银行银承合同申请提交审批无流程处理开始：" + serno);
            // 1.针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
            this.handleBusinessDataAfterStart(serno);
            // 2.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
            this.handleBusinessDataAfterEnd(serno);
            log.info("村镇银行银承合同申请提交审批无流程微信通知开始：" + serno);
            //微信通知
            IqpAccpApp iqpAccpApp = iqpAccpAppMapper.selectByAccpSernoKey(serno);
            String managerId = iqpAccpApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", iqpAccpApp.getCusName());
                paramMap.put("prdName", "一般合同申请");
                paramMap.put("result", "通过");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                log.info("村镇银行银承合同申请提交审批无流程处理结束：" + serno);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("村镇银行银承合同申请提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: sendOnlinePldRemind
     * @方法描述: 首页消息提醒
     * @参数与返回说明:
     * @创建者：qw
     * @算法描述: 无
     */
    public void sendOnlinePldRemind(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        log.info("业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
        //针对流程到办结节点，进行以下处理
        //首页消息提醒
        IqpAccpApp iqpAccpApp = this.selectByIqpSerno(serno);
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpAccpApp.getIsOlPld())){
            String managerId = iqpAccpApp.getManagerId();
            String managerBrId = iqpAccpApp.getManagerBrId();
            if (StringUtil.isNotEmpty(managerId)) {
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_DG_M_0001";//短信编号
                    // 翻译管护经理名称和管护经理机构名称
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
                    if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                    if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                    //短信填充参数
                    Map paramMap = new HashMap();
                    paramMap.put("cusName", iqpAccpApp.getCusName());
                    paramMap.put("managerId", managerName);
                    paramMap.put("managerBrId", managerBrName);
                    paramMap.put("instanceId", resultInstanceDto.getInstanceId());
                    paramMap.put("nodeSign", resultInstanceDto.getNodeSign());
                    //执行发送客户经理操作
                    messageCommonService.sendonlinepldremind(messageType, receivedUserType, paramMap);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
    }
}
