package cn.com.yusys.yusp.web.server.xdzc0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0003.req.Xdzc0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.resp.Xdzc0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0003.Xdzc0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:资产池协议详情查看接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0003:资产池协议详情查看接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0003Resource.class);

    @Autowired
    private Xdzc0003Service xdzc0003Service;
    /**
     * 交易码：xdzc0003
     * 交易描述：资产池协议详情查看接口
     *
     * @param xdzc0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池协议详情查看接口")
    @PostMapping("/xdzc0003")
    protected @ResponseBody
    ResultDto<Xdzc0003DataRespDto> xdzc0003(@Validated @RequestBody Xdzc0003DataReqDto xdzc0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003DataReqDto));
        Xdzc0003DataRespDto xdzc0003DataRespDto = new Xdzc0003DataRespDto();// 响应Dto:客户资产池下资产清单列表查询（出池时查询）
        ResultDto<Xdzc0003DataRespDto> xdzc0003DataResultDto = new ResultDto<>();
        try {
            // 从xdzc0003DataReqDto获取业务值进行业务逻辑处理
            xdzc0003DataRespDto = xdzc0003Service.xdzc0003Service(xdzc0003DataReqDto);
            // 封装xdzc0003DataResultDto中正确的返回码和返回信息
            xdzc0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, e.getMessage());
            // 封装xdzc0003DataResultDto中异常返回码和返回信息
            xdzc0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0003DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0003DataRespDto到xdzc0003DataResultDto中
        xdzc0003DataResultDto.setData(xdzc0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003DataResultDto));
        return xdzc0003DataResultDto;
    }
}
