package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CoopPlanApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @param
 * @功能描述:合作方准入与变更审批流程-分支机构（寿光）
 * @author: dumingdi
 * @date: 10:29 2021/9/6
 * @return:
 */
@Service
public class SGCZ36BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(SGCZ36BizService.class);

    @Autowired
    private CoopPlanAppService coopPlanAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("合作方准入申请后业务处理类型" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        try {
            CoopPlanApp coopPlanApp = coopPlanAppService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPlanAppService.update(coopPlanApp);
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                // coopPlanAppService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId());
                log.info("合作方准入申请【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                coopPlanAppService.update(coopPlanApp);
                log.info("合作方准入申请【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                if (CmisBizConstants.SGB01.equals(bizType)) {
                    //合作方申请
                    coopPlanAppService.wfApply(serno);
                } else if (CmisBizConstants.SGB02.equals(bizType)) {
                    //合作方变更
                    coopPlanAppService.wfAlter(serno);
                }
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",bizType,currentOpType,serno);
                coopPlanAppService.createImageSpplInfo(serno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),"合作方准入");
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                coopPlanAppService.update(coopPlanApp);
                Map<String, Object> params = new HashMap<>();
                WFBizParamDto param = new WFBizParamDto();
                param.setBizId(resultInstanceDto.getBizId());
                param.setInstanceId(resultInstanceDto.getInstanceId());
                params.put("apprStatus", CmisBizConstants.APPLY_STATE_CALL_BACK);
                param.setParam(params);
                workflowCoreClient.updateFlowParam(param);
                log.info("合作方准入申请【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        coopPlanAppService.update(coopPlanApp);
                    }
                });
                Map<String, Object> params = new HashMap<>();
                WFBizParamDto param = new WFBizParamDto();
                param.setBizId(resultInstanceDto.getBizId());
                param.setInstanceId(resultInstanceDto.getInstanceId());
                params.put("apprStatus", CmisBizConstants.APPLY_STATE_CALL_BACK);
                param.setParam(params);
                workflowCoreClient.updateFlowParam(param);
                log.info("合作方准入申请【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPlanAppService.update(coopPlanApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程撤回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                coopPlanAppService.update(coopPlanApp);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方准入申请【{}】，流程否决初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                coopPlanApp.setApprStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                coopPlanAppService.update(coopPlanApp);
            } else {
                log.info("合作方准入申请【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方准入申请后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ36.equals(flowCode);
    }
}