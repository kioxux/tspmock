/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: EntlQyInfo
 * @类描述: entl_qy_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:51:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "entl_qy_info")
public class EntlQyInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户所在区域 **/
	@Column(name = "CUS_AREA", unique = false, nullable = true, length = 200)
	private String cusArea;
	
	/** 营业执照号 **/
	@Column(name = "REG_CDE", unique = false, nullable = true, length = 30)
	private String regCde;
	
	/** 经营范围 **/
	@Column(name = "OPER_RANGE", unique = false, nullable = true, length = 500)
	private String operRange;
	
	/** 年销售收入 **/
	@Column(name = "YEAR_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearSaleIncome;
	
	/** 企业信息状态 **/
	@Column(name = "CRP_STATUS", unique = false, nullable = true, length = 5)
	private String crpStatus;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusArea
	 */
	public void setCusArea(String cusArea) {
		this.cusArea = cusArea;
	}
	
    /**
     * @return cusArea
     */
	public String getCusArea() {
		return this.cusArea;
	}
	
	/**
	 * @param regCde
	 */
	public void setRegCde(String regCde) {
		this.regCde = regCde;
	}
	
    /**
     * @return regCde
     */
	public String getRegCde() {
		return this.regCde;
	}
	
	/**
	 * @param operRange
	 */
	public void setOperRange(String operRange) {
		this.operRange = operRange;
	}
	
    /**
     * @return operRange
     */
	public String getOperRange() {
		return this.operRange;
	}
	
	/**
	 * @param yearSaleIncome
	 */
	public void setYearSaleIncome(java.math.BigDecimal yearSaleIncome) {
		this.yearSaleIncome = yearSaleIncome;
	}
	
    /**
     * @return yearSaleIncome
     */
	public java.math.BigDecimal getYearSaleIncome() {
		return this.yearSaleIncome;
	}
	
	/**
	 * @param crpStatus
	 */
	public void setCrpStatus(String crpStatus) {
		this.crpStatus = crpStatus;
	}
	
    /**
     * @return crpStatus
     */
	public String getCrpStatus() {
		return this.crpStatus;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

}