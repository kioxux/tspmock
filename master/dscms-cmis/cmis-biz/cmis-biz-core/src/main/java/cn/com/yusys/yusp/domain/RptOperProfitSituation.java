/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperProfitSituation
 * @类描述: rpt_oper_profit_situation数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:48:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_profit_situation")
public class RptOperProfitSituation extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	/** 最近第二年主营业务利润 **/
	@Column(name = "NEAR_SECOND_MAIN_BUSI_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondMainBusiProfit;
	
	/** 最近第二年毛利润 **/
	@Column(name = "NEAR_SECOND_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondGrossProfit;
	
	/** 最近第二年营业费用 **/
	@Column(name = "NEAR_SECOND_BUSINESS_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondBusinessExpenses;
	
	/** 最近第二年管理费用 **/
	@Column(name = "NEAR_SECOND_MANAGEMENT_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondManagementExpenses;
	
	/** 最近第二年财务费用 **/
	@Column(name = "NEAR_SECOND_FINANCIAL_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondFinancialExpenses;
	
	/** 最近第二年三大费用小计 **/
	@Column(name = "NEAR_SECOND_THREE_EXPENSES_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondThreeExpensesSum;
	
	/** 最近第二年营业利润 **/
	@Column(name = "NEAR_SECOND_OPERA_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondOperaExpenses;
	
	/** 最近第二年投资收益 **/
	@Column(name = "NEAR_SECOND_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondInvestIncome;
	
	/** 最近第二年利润总额 **/
	@Column(name = "NEAR_SECOND_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondTotalProfit;
	
	/** 最近第二年所得税 **/
	@Column(name = "NEAR_SECOND_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondIncomeTax;
	
	/** 最近第二年实际净利润 **/
	@Column(name = "NEAR_SECOND_REAL_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearSecondRealGrossProfit;
	
	/** 最近第一年主营业务利润 **/
	@Column(name = "NEAR_FIRST_MAIN_BUSI_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstMainBusiProfit;
	
	/** 最近第一年毛利润 **/
	@Column(name = "NEAR_FIRST_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstGrossProfit;
	
	/** 最近第一年营业费用 **/
	@Column(name = "NEAR_FIRST_BUSINESS_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstBusinessExpenses;
	
	/** 最近第一年管理费用 **/
	@Column(name = "NEAR_FIRST_MANAGEMENT_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstManagementExpenses;
	
	/** 最近第一年财务费用 **/
	@Column(name = "NEAR_FIRST_FINANCIAL_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstFinancialExpenses;
	
	/** 最近第一年三大费用小计 **/
	@Column(name = "NEAR_FIRST_THREE_EXPENSES_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstThreeExpensesSum;
	
	/** 最近第一年营业利润 **/
	@Column(name = "NEAR_FIRST_OPERA_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstOperaExpenses;
	
	/** 最近第一年投资收益 **/
	@Column(name = "NEAR_FIRST_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstInvestIncome;
	
	/** 最近第一年利润总额 **/
	@Column(name = "NEAR_FIRST_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstTotalProfit;
	
	/** 最近第一年所得税 **/
	@Column(name = "NEAR_FIRST_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstIncomeTax;
	
	/** 最近第一年实际净利润 **/
	@Column(name = "NEAR_FIRST_REAL_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearFirstRealGrossProfit;
	
	/** 今年当前主营业务利润 **/
	@Column(name = "CURR_MAIN_BUSI_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currMainBusiProfit;
	
	/** 今年当前毛利润 **/
	@Column(name = "CURR_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currGrossProfit;
	
	/** 今年当前营业费用 **/
	@Column(name = "CURR_BUSINESS_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currBusinessExpenses;
	
	/** 今年当前管理费用 **/
	@Column(name = "CURR_MANAGEMENT_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currManagementExpenses;
	
	/** 今年当前财务费用 **/
	@Column(name = "CURR_FINANCIAL_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currFinancialExpenses;
	
	/** 今年当前三大费用小计 **/
	@Column(name = "CURR_THREE_EXPENSES_SUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currThreeExpensesSum;
	
	/** 今年当前营业利润 **/
	@Column(name = "CURR_OPERA_EXPENSES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currOperaExpenses;
	
	/** 今年当前投资收益 **/
	@Column(name = "CURR_INVEST_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currInvestIncome;
	
	/** 今年当前利润总额 **/
	@Column(name = "CURR_TOTAL_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currTotalProfit;
	
	/** 今年当前所得税 **/
	@Column(name = "CURR_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currIncomeTax;
	
	/** 今年当前实际净利润 **/
	@Column(name = "CURR_REAL_GROSS_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currRealGrossProfit;
	
	/** 主营业务利润增减值 **/
	@Column(name = "MAIN_BUSI_PROFIT_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mainBusiProfitDiff;
	
	/** 毛利润增减值 **/
	@Column(name = "GROSS_PROFIT_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grossProfitDiff;
	
	/** 营业费用增减值 **/
	@Column(name = "BUSINESS_EXPENSES_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal businessExpensesDiff;
	
	/** 管理费用增减值 **/
	@Column(name = "MANAGEMENT_EXPENSES_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal managementExpensesDiff;
	
	/** 财务费用增减值 **/
	@Column(name = "FINANCIAL_EXPENSES_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal financialExpensesDiff;
	
	/** 三大费用小计增减值 **/
	@Column(name = "THREE_EXPENSES_SUM_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal threeExpensesSumDiff;
	
	/** 营业利润增减值 **/
	@Column(name = "OPERA_EXPENSES_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal operaExpensesDiff;
	
	/** 投资收益增减值 **/
	@Column(name = "INVEST_INCOME_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investIncomeDiff;
	
	/** 利润总额增减值 **/
	@Column(name = "TOTAL_PROFIT_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalProfitDiff;
	
	/** 所得税增减值 **/
	@Column(name = "INCOME_TAX_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incomeTaxDiff;
	
	/** 实际净利润增减值 **/
	@Column(name = "REAL_GROSS_PROFIT_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realGrossProfitDiff;
	
	/** 主营业务利润较上年同期增减 **/
	@Column(name = "MAIN_BUSI_PROFIT_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mainBusiProfitLastYearDiff;
	
	/** 毛利润较上年同期增减 **/
	@Column(name = "GROSS_PROFIT_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal grossProfitLastYearDiff;
	
	/** 营业费用较上年同期增减 **/
	@Column(name = "BUSINESS_EXPENSES_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal businessExpensesLastYearDiff;
	
	/** 管理费用较上年同期增减 **/
	@Column(name = "MANAGEMENT_EXPENSES_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal managementExpensesLastYearDiff;
	
	/** 财务费用较上年同期增减 **/
	@Column(name = "FINANCIAL_EXPENSES_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal financialExpensesLastYearDiff;
	
	/** 三大费用小计较上年同期增减 **/
	@Column(name = "THREE_EXPENSES_SUM_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal threeExpensesSumLastYearDiff;
	
	/** 营业利润较上年同期增减 **/
	@Column(name = "OPERA_EXPENSES_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal operaExpensesLastYearDiff;
	
	/** 投资收益较上年同期增减 **/
	@Column(name = "INVEST_INCOME_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investIncomeLastYearDiff;
	
	/** 利润总额较上年同期增减 **/
	@Column(name = "TOTAL_PROFIT_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalProfitLastYearDiff;
	
	/** 所得税较上年同期增减 **/
	@Column(name = "INCOME_TAX_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal incomeTaxLastYearDiff;
	
	/** 实际净利润较上年同期增减 **/
	@Column(name = "REAL_GROSS_PROFIT_LAST_YEAR_DIFF", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realGrossProfitLastYearDiff;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param nearSecondMainBusiProfit
	 */
	public void setNearSecondMainBusiProfit(java.math.BigDecimal nearSecondMainBusiProfit) {
		this.nearSecondMainBusiProfit = nearSecondMainBusiProfit;
	}
	
    /**
     * @return nearSecondMainBusiProfit
     */
	public java.math.BigDecimal getNearSecondMainBusiProfit() {
		return this.nearSecondMainBusiProfit;
	}
	
	/**
	 * @param nearSecondGrossProfit
	 */
	public void setNearSecondGrossProfit(java.math.BigDecimal nearSecondGrossProfit) {
		this.nearSecondGrossProfit = nearSecondGrossProfit;
	}
	
    /**
     * @return nearSecondGrossProfit
     */
	public java.math.BigDecimal getNearSecondGrossProfit() {
		return this.nearSecondGrossProfit;
	}
	
	/**
	 * @param nearSecondBusinessExpenses
	 */
	public void setNearSecondBusinessExpenses(java.math.BigDecimal nearSecondBusinessExpenses) {
		this.nearSecondBusinessExpenses = nearSecondBusinessExpenses;
	}
	
    /**
     * @return nearSecondBusinessExpenses
     */
	public java.math.BigDecimal getNearSecondBusinessExpenses() {
		return this.nearSecondBusinessExpenses;
	}
	
	/**
	 * @param nearSecondManagementExpenses
	 */
	public void setNearSecondManagementExpenses(java.math.BigDecimal nearSecondManagementExpenses) {
		this.nearSecondManagementExpenses = nearSecondManagementExpenses;
	}
	
    /**
     * @return nearSecondManagementExpenses
     */
	public java.math.BigDecimal getNearSecondManagementExpenses() {
		return this.nearSecondManagementExpenses;
	}
	
	/**
	 * @param nearSecondFinancialExpenses
	 */
	public void setNearSecondFinancialExpenses(java.math.BigDecimal nearSecondFinancialExpenses) {
		this.nearSecondFinancialExpenses = nearSecondFinancialExpenses;
	}
	
    /**
     * @return nearSecondFinancialExpenses
     */
	public java.math.BigDecimal getNearSecondFinancialExpenses() {
		return this.nearSecondFinancialExpenses;
	}
	
	/**
	 * @param nearSecondThreeExpensesSum
	 */
	public void setNearSecondThreeExpensesSum(java.math.BigDecimal nearSecondThreeExpensesSum) {
		this.nearSecondThreeExpensesSum = nearSecondThreeExpensesSum;
	}
	
    /**
     * @return nearSecondThreeExpensesSum
     */
	public java.math.BigDecimal getNearSecondThreeExpensesSum() {
		return this.nearSecondThreeExpensesSum;
	}
	
	/**
	 * @param nearSecondOperaExpenses
	 */
	public void setNearSecondOperaExpenses(java.math.BigDecimal nearSecondOperaExpenses) {
		this.nearSecondOperaExpenses = nearSecondOperaExpenses;
	}
	
    /**
     * @return nearSecondOperaExpenses
     */
	public java.math.BigDecimal getNearSecondOperaExpenses() {
		return this.nearSecondOperaExpenses;
	}
	
	/**
	 * @param nearSecondInvestIncome
	 */
	public void setNearSecondInvestIncome(java.math.BigDecimal nearSecondInvestIncome) {
		this.nearSecondInvestIncome = nearSecondInvestIncome;
	}
	
    /**
     * @return nearSecondInvestIncome
     */
	public java.math.BigDecimal getNearSecondInvestIncome() {
		return this.nearSecondInvestIncome;
	}
	
	/**
	 * @param nearSecondTotalProfit
	 */
	public void setNearSecondTotalProfit(java.math.BigDecimal nearSecondTotalProfit) {
		this.nearSecondTotalProfit = nearSecondTotalProfit;
	}
	
    /**
     * @return nearSecondTotalProfit
     */
	public java.math.BigDecimal getNearSecondTotalProfit() {
		return this.nearSecondTotalProfit;
	}
	
	/**
	 * @param nearSecondIncomeTax
	 */
	public void setNearSecondIncomeTax(java.math.BigDecimal nearSecondIncomeTax) {
		this.nearSecondIncomeTax = nearSecondIncomeTax;
	}
	
    /**
     * @return nearSecondIncomeTax
     */
	public java.math.BigDecimal getNearSecondIncomeTax() {
		return this.nearSecondIncomeTax;
	}
	
	/**
	 * @param nearSecondRealGrossProfit
	 */
	public void setNearSecondRealGrossProfit(java.math.BigDecimal nearSecondRealGrossProfit) {
		this.nearSecondRealGrossProfit = nearSecondRealGrossProfit;
	}
	
    /**
     * @return nearSecondRealGrossProfit
     */
	public java.math.BigDecimal getNearSecondRealGrossProfit() {
		return this.nearSecondRealGrossProfit;
	}
	
	/**
	 * @param nearFirstMainBusiProfit
	 */
	public void setNearFirstMainBusiProfit(java.math.BigDecimal nearFirstMainBusiProfit) {
		this.nearFirstMainBusiProfit = nearFirstMainBusiProfit;
	}
	
    /**
     * @return nearFirstMainBusiProfit
     */
	public java.math.BigDecimal getNearFirstMainBusiProfit() {
		return this.nearFirstMainBusiProfit;
	}
	
	/**
	 * @param nearFirstGrossProfit
	 */
	public void setNearFirstGrossProfit(java.math.BigDecimal nearFirstGrossProfit) {
		this.nearFirstGrossProfit = nearFirstGrossProfit;
	}
	
    /**
     * @return nearFirstGrossProfit
     */
	public java.math.BigDecimal getNearFirstGrossProfit() {
		return this.nearFirstGrossProfit;
	}
	
	/**
	 * @param nearFirstBusinessExpenses
	 */
	public void setNearFirstBusinessExpenses(java.math.BigDecimal nearFirstBusinessExpenses) {
		this.nearFirstBusinessExpenses = nearFirstBusinessExpenses;
	}
	
    /**
     * @return nearFirstBusinessExpenses
     */
	public java.math.BigDecimal getNearFirstBusinessExpenses() {
		return this.nearFirstBusinessExpenses;
	}
	
	/**
	 * @param nearFirstManagementExpenses
	 */
	public void setNearFirstManagementExpenses(java.math.BigDecimal nearFirstManagementExpenses) {
		this.nearFirstManagementExpenses = nearFirstManagementExpenses;
	}
	
    /**
     * @return nearFirstManagementExpenses
     */
	public java.math.BigDecimal getNearFirstManagementExpenses() {
		return this.nearFirstManagementExpenses;
	}
	
	/**
	 * @param nearFirstFinancialExpenses
	 */
	public void setNearFirstFinancialExpenses(java.math.BigDecimal nearFirstFinancialExpenses) {
		this.nearFirstFinancialExpenses = nearFirstFinancialExpenses;
	}
	
    /**
     * @return nearFirstFinancialExpenses
     */
	public java.math.BigDecimal getNearFirstFinancialExpenses() {
		return this.nearFirstFinancialExpenses;
	}
	
	/**
	 * @param nearFirstThreeExpensesSum
	 */
	public void setNearFirstThreeExpensesSum(java.math.BigDecimal nearFirstThreeExpensesSum) {
		this.nearFirstThreeExpensesSum = nearFirstThreeExpensesSum;
	}
	
    /**
     * @return nearFirstThreeExpensesSum
     */
	public java.math.BigDecimal getNearFirstThreeExpensesSum() {
		return this.nearFirstThreeExpensesSum;
	}
	
	/**
	 * @param nearFirstOperaExpenses
	 */
	public void setNearFirstOperaExpenses(java.math.BigDecimal nearFirstOperaExpenses) {
		this.nearFirstOperaExpenses = nearFirstOperaExpenses;
	}
	
    /**
     * @return nearFirstOperaExpenses
     */
	public java.math.BigDecimal getNearFirstOperaExpenses() {
		return this.nearFirstOperaExpenses;
	}
	
	/**
	 * @param nearFirstInvestIncome
	 */
	public void setNearFirstInvestIncome(java.math.BigDecimal nearFirstInvestIncome) {
		this.nearFirstInvestIncome = nearFirstInvestIncome;
	}
	
    /**
     * @return nearFirstInvestIncome
     */
	public java.math.BigDecimal getNearFirstInvestIncome() {
		return this.nearFirstInvestIncome;
	}
	
	/**
	 * @param nearFirstTotalProfit
	 */
	public void setNearFirstTotalProfit(java.math.BigDecimal nearFirstTotalProfit) {
		this.nearFirstTotalProfit = nearFirstTotalProfit;
	}
	
    /**
     * @return nearFirstTotalProfit
     */
	public java.math.BigDecimal getNearFirstTotalProfit() {
		return this.nearFirstTotalProfit;
	}
	
	/**
	 * @param nearFirstIncomeTax
	 */
	public void setNearFirstIncomeTax(java.math.BigDecimal nearFirstIncomeTax) {
		this.nearFirstIncomeTax = nearFirstIncomeTax;
	}
	
    /**
     * @return nearFirstIncomeTax
     */
	public java.math.BigDecimal getNearFirstIncomeTax() {
		return this.nearFirstIncomeTax;
	}
	
	/**
	 * @param nearFirstRealGrossProfit
	 */
	public void setNearFirstRealGrossProfit(java.math.BigDecimal nearFirstRealGrossProfit) {
		this.nearFirstRealGrossProfit = nearFirstRealGrossProfit;
	}
	
    /**
     * @return nearFirstRealGrossProfit
     */
	public java.math.BigDecimal getNearFirstRealGrossProfit() {
		return this.nearFirstRealGrossProfit;
	}
	
	/**
	 * @param currMainBusiProfit
	 */
	public void setCurrMainBusiProfit(java.math.BigDecimal currMainBusiProfit) {
		this.currMainBusiProfit = currMainBusiProfit;
	}
	
    /**
     * @return currMainBusiProfit
     */
	public java.math.BigDecimal getCurrMainBusiProfit() {
		return this.currMainBusiProfit;
	}
	
	/**
	 * @param currGrossProfit
	 */
	public void setCurrGrossProfit(java.math.BigDecimal currGrossProfit) {
		this.currGrossProfit = currGrossProfit;
	}
	
    /**
     * @return currGrossProfit
     */
	public java.math.BigDecimal getCurrGrossProfit() {
		return this.currGrossProfit;
	}
	
	/**
	 * @param currBusinessExpenses
	 */
	public void setCurrBusinessExpenses(java.math.BigDecimal currBusinessExpenses) {
		this.currBusinessExpenses = currBusinessExpenses;
	}
	
    /**
     * @return currBusinessExpenses
     */
	public java.math.BigDecimal getCurrBusinessExpenses() {
		return this.currBusinessExpenses;
	}
	
	/**
	 * @param currManagementExpenses
	 */
	public void setCurrManagementExpenses(java.math.BigDecimal currManagementExpenses) {
		this.currManagementExpenses = currManagementExpenses;
	}
	
    /**
     * @return currManagementExpenses
     */
	public java.math.BigDecimal getCurrManagementExpenses() {
		return this.currManagementExpenses;
	}
	
	/**
	 * @param currFinancialExpenses
	 */
	public void setCurrFinancialExpenses(java.math.BigDecimal currFinancialExpenses) {
		this.currFinancialExpenses = currFinancialExpenses;
	}
	
    /**
     * @return currFinancialExpenses
     */
	public java.math.BigDecimal getCurrFinancialExpenses() {
		return this.currFinancialExpenses;
	}
	
	/**
	 * @param currThreeExpensesSum
	 */
	public void setCurrThreeExpensesSum(java.math.BigDecimal currThreeExpensesSum) {
		this.currThreeExpensesSum = currThreeExpensesSum;
	}
	
    /**
     * @return currThreeExpensesSum
     */
	public java.math.BigDecimal getCurrThreeExpensesSum() {
		return this.currThreeExpensesSum;
	}
	
	/**
	 * @param currOperaExpenses
	 */
	public void setCurrOperaExpenses(java.math.BigDecimal currOperaExpenses) {
		this.currOperaExpenses = currOperaExpenses;
	}
	
    /**
     * @return currOperaExpenses
     */
	public java.math.BigDecimal getCurrOperaExpenses() {
		return this.currOperaExpenses;
	}
	
	/**
	 * @param currInvestIncome
	 */
	public void setCurrInvestIncome(java.math.BigDecimal currInvestIncome) {
		this.currInvestIncome = currInvestIncome;
	}
	
    /**
     * @return currInvestIncome
     */
	public java.math.BigDecimal getCurrInvestIncome() {
		return this.currInvestIncome;
	}
	
	/**
	 * @param currTotalProfit
	 */
	public void setCurrTotalProfit(java.math.BigDecimal currTotalProfit) {
		this.currTotalProfit = currTotalProfit;
	}
	
    /**
     * @return currTotalProfit
     */
	public java.math.BigDecimal getCurrTotalProfit() {
		return this.currTotalProfit;
	}
	
	/**
	 * @param currIncomeTax
	 */
	public void setCurrIncomeTax(java.math.BigDecimal currIncomeTax) {
		this.currIncomeTax = currIncomeTax;
	}
	
    /**
     * @return currIncomeTax
     */
	public java.math.BigDecimal getCurrIncomeTax() {
		return this.currIncomeTax;
	}
	
	/**
	 * @param currRealGrossProfit
	 */
	public void setCurrRealGrossProfit(java.math.BigDecimal currRealGrossProfit) {
		this.currRealGrossProfit = currRealGrossProfit;
	}
	
    /**
     * @return currRealGrossProfit
     */
	public java.math.BigDecimal getCurrRealGrossProfit() {
		return this.currRealGrossProfit;
	}
	
	/**
	 * @param mainBusiProfitDiff
	 */
	public void setMainBusiProfitDiff(java.math.BigDecimal mainBusiProfitDiff) {
		this.mainBusiProfitDiff = mainBusiProfitDiff;
	}
	
    /**
     * @return mainBusiProfitDiff
     */
	public java.math.BigDecimal getMainBusiProfitDiff() {
		return this.mainBusiProfitDiff;
	}
	
	/**
	 * @param grossProfitDiff
	 */
	public void setGrossProfitDiff(java.math.BigDecimal grossProfitDiff) {
		this.grossProfitDiff = grossProfitDiff;
	}
	
    /**
     * @return grossProfitDiff
     */
	public java.math.BigDecimal getGrossProfitDiff() {
		return this.grossProfitDiff;
	}
	
	/**
	 * @param businessExpensesDiff
	 */
	public void setBusinessExpensesDiff(java.math.BigDecimal businessExpensesDiff) {
		this.businessExpensesDiff = businessExpensesDiff;
	}
	
    /**
     * @return businessExpensesDiff
     */
	public java.math.BigDecimal getBusinessExpensesDiff() {
		return this.businessExpensesDiff;
	}
	
	/**
	 * @param managementExpensesDiff
	 */
	public void setManagementExpensesDiff(java.math.BigDecimal managementExpensesDiff) {
		this.managementExpensesDiff = managementExpensesDiff;
	}
	
    /**
     * @return managementExpensesDiff
     */
	public java.math.BigDecimal getManagementExpensesDiff() {
		return this.managementExpensesDiff;
	}
	
	/**
	 * @param financialExpensesDiff
	 */
	public void setFinancialExpensesDiff(java.math.BigDecimal financialExpensesDiff) {
		this.financialExpensesDiff = financialExpensesDiff;
	}
	
    /**
     * @return financialExpensesDiff
     */
	public java.math.BigDecimal getFinancialExpensesDiff() {
		return this.financialExpensesDiff;
	}
	
	/**
	 * @param threeExpensesSumDiff
	 */
	public void setThreeExpensesSumDiff(java.math.BigDecimal threeExpensesSumDiff) {
		this.threeExpensesSumDiff = threeExpensesSumDiff;
	}
	
    /**
     * @return threeExpensesSumDiff
     */
	public java.math.BigDecimal getThreeExpensesSumDiff() {
		return this.threeExpensesSumDiff;
	}
	
	/**
	 * @param operaExpensesDiff
	 */
	public void setOperaExpensesDiff(java.math.BigDecimal operaExpensesDiff) {
		this.operaExpensesDiff = operaExpensesDiff;
	}
	
    /**
     * @return operaExpensesDiff
     */
	public java.math.BigDecimal getOperaExpensesDiff() {
		return this.operaExpensesDiff;
	}
	
	/**
	 * @param investIncomeDiff
	 */
	public void setInvestIncomeDiff(java.math.BigDecimal investIncomeDiff) {
		this.investIncomeDiff = investIncomeDiff;
	}
	
    /**
     * @return investIncomeDiff
     */
	public java.math.BigDecimal getInvestIncomeDiff() {
		return this.investIncomeDiff;
	}
	
	/**
	 * @param totalProfitDiff
	 */
	public void setTotalProfitDiff(java.math.BigDecimal totalProfitDiff) {
		this.totalProfitDiff = totalProfitDiff;
	}
	
    /**
     * @return totalProfitDiff
     */
	public java.math.BigDecimal getTotalProfitDiff() {
		return this.totalProfitDiff;
	}
	
	/**
	 * @param incomeTaxDiff
	 */
	public void setIncomeTaxDiff(java.math.BigDecimal incomeTaxDiff) {
		this.incomeTaxDiff = incomeTaxDiff;
	}
	
    /**
     * @return incomeTaxDiff
     */
	public java.math.BigDecimal getIncomeTaxDiff() {
		return this.incomeTaxDiff;
	}
	
	/**
	 * @param realGrossProfitDiff
	 */
	public void setRealGrossProfitDiff(java.math.BigDecimal realGrossProfitDiff) {
		this.realGrossProfitDiff = realGrossProfitDiff;
	}
	
    /**
     * @return realGrossProfitDiff
     */
	public java.math.BigDecimal getRealGrossProfitDiff() {
		return this.realGrossProfitDiff;
	}
	
	/**
	 * @param mainBusiProfitLastYearDiff
	 */
	public void setMainBusiProfitLastYearDiff(java.math.BigDecimal mainBusiProfitLastYearDiff) {
		this.mainBusiProfitLastYearDiff = mainBusiProfitLastYearDiff;
	}
	
    /**
     * @return mainBusiProfitLastYearDiff
     */
	public java.math.BigDecimal getMainBusiProfitLastYearDiff() {
		return this.mainBusiProfitLastYearDiff;
	}
	
	/**
	 * @param grossProfitLastYearDiff
	 */
	public void setGrossProfitLastYearDiff(java.math.BigDecimal grossProfitLastYearDiff) {
		this.grossProfitLastYearDiff = grossProfitLastYearDiff;
	}
	
    /**
     * @return grossProfitLastYearDiff
     */
	public java.math.BigDecimal getGrossProfitLastYearDiff() {
		return this.grossProfitLastYearDiff;
	}
	
	/**
	 * @param businessExpensesLastYearDiff
	 */
	public void setBusinessExpensesLastYearDiff(java.math.BigDecimal businessExpensesLastYearDiff) {
		this.businessExpensesLastYearDiff = businessExpensesLastYearDiff;
	}
	
    /**
     * @return businessExpensesLastYearDiff
     */
	public java.math.BigDecimal getBusinessExpensesLastYearDiff() {
		return this.businessExpensesLastYearDiff;
	}
	
	/**
	 * @param managementExpensesLastYearDiff
	 */
	public void setManagementExpensesLastYearDiff(java.math.BigDecimal managementExpensesLastYearDiff) {
		this.managementExpensesLastYearDiff = managementExpensesLastYearDiff;
	}
	
    /**
     * @return managementExpensesLastYearDiff
     */
	public java.math.BigDecimal getManagementExpensesLastYearDiff() {
		return this.managementExpensesLastYearDiff;
	}
	
	/**
	 * @param financialExpensesLastYearDiff
	 */
	public void setFinancialExpensesLastYearDiff(java.math.BigDecimal financialExpensesLastYearDiff) {
		this.financialExpensesLastYearDiff = financialExpensesLastYearDiff;
	}
	
    /**
     * @return financialExpensesLastYearDiff
     */
	public java.math.BigDecimal getFinancialExpensesLastYearDiff() {
		return this.financialExpensesLastYearDiff;
	}
	
	/**
	 * @param threeExpensesSumLastYearDiff
	 */
	public void setThreeExpensesSumLastYearDiff(java.math.BigDecimal threeExpensesSumLastYearDiff) {
		this.threeExpensesSumLastYearDiff = threeExpensesSumLastYearDiff;
	}
	
    /**
     * @return threeExpensesSumLastYearDiff
     */
	public java.math.BigDecimal getThreeExpensesSumLastYearDiff() {
		return this.threeExpensesSumLastYearDiff;
	}
	
	/**
	 * @param operaExpensesLastYearDiff
	 */
	public void setOperaExpensesLastYearDiff(java.math.BigDecimal operaExpensesLastYearDiff) {
		this.operaExpensesLastYearDiff = operaExpensesLastYearDiff;
	}
	
    /**
     * @return operaExpensesLastYearDiff
     */
	public java.math.BigDecimal getOperaExpensesLastYearDiff() {
		return this.operaExpensesLastYearDiff;
	}
	
	/**
	 * @param investIncomeLastYearDiff
	 */
	public void setInvestIncomeLastYearDiff(java.math.BigDecimal investIncomeLastYearDiff) {
		this.investIncomeLastYearDiff = investIncomeLastYearDiff;
	}
	
    /**
     * @return investIncomeLastYearDiff
     */
	public java.math.BigDecimal getInvestIncomeLastYearDiff() {
		return this.investIncomeLastYearDiff;
	}
	
	/**
	 * @param totalProfitLastYearDiff
	 */
	public void setTotalProfitLastYearDiff(java.math.BigDecimal totalProfitLastYearDiff) {
		this.totalProfitLastYearDiff = totalProfitLastYearDiff;
	}
	
    /**
     * @return totalProfitLastYearDiff
     */
	public java.math.BigDecimal getTotalProfitLastYearDiff() {
		return this.totalProfitLastYearDiff;
	}
	
	/**
	 * @param incomeTaxLastYearDiff
	 */
	public void setIncomeTaxLastYearDiff(java.math.BigDecimal incomeTaxLastYearDiff) {
		this.incomeTaxLastYearDiff = incomeTaxLastYearDiff;
	}
	
    /**
     * @return incomeTaxLastYearDiff
     */
	public java.math.BigDecimal getIncomeTaxLastYearDiff() {
		return this.incomeTaxLastYearDiff;
	}
	
	/**
	 * @param realGrossProfitLastYearDiff
	 */
	public void setRealGrossProfitLastYearDiff(java.math.BigDecimal realGrossProfitLastYearDiff) {
		this.realGrossProfitLastYearDiff = realGrossProfitLastYearDiff;
	}
	
    /**
     * @return realGrossProfitLastYearDiff
     */
	public java.math.BigDecimal getRealGrossProfitLastYearDiff() {
		return this.realGrossProfitLastYearDiff;
	}


}