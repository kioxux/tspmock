package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFrUfrApp
 * @类描述: lmt_fr_ufr_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-30 16:38:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtFrUfrAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 全局流水号 **/
	private String serno;
	
	/** 授信申请流水号 **/
	private String lmtSerno;
	
	/** 冻结/解冻流水号 **/
	private String frUfrSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 授信协议编号 **/
	private String lmtCtrNo;
	
	/** 申请类型 STD_ZB_OPT_MODE **/
	private String appType;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 授信总额度 **/
	private java.math.BigDecimal lmtTotlAmt;
	
	/** 循环额度 **/
	private java.math.BigDecimal revolvAmt;
	
	/** 一次性额度 **/
	private java.math.BigDecimal oneAmt;
	
	/** 低风险业务类型  STD_ZB_LOW_RISK_TYP **/
	private String lowRiskType;
	
	/** 授信起始日期 **/
	private String lmtStarDate;
	
	/** 授信到期日期 **/
	private String lmtEndDate;
	
	/** 授信已冻结额度 **/
	private String lmtOutstndFrozeAmt;
	
	/** 本次冻结额度 **/
	private String lmtFrozeAmt;
	
	/** 本次解冻额度 **/
	private String lmtUnfrozeAmt;
	
	/** 冻结原因 **/
	private String frozeResn;
	
	/** 解冻原因 **/
	private String unfrozeResn;
	
	/** 主办人 **/
	private String managerId;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 申请状态 STD_ZB_APP_STATUS **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改时间 **/
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param frUfrSerno
	 */
	public void setFrUfrSerno(String frUfrSerno) {
		this.frUfrSerno = frUfrSerno == null ? null : frUfrSerno.trim();
	}
	
    /**
     * @return FrUfrSerno
     */	
	public String getFrUfrSerno() {
		return this.frUfrSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param lmtCtrNo
	 */
	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo == null ? null : lmtCtrNo.trim();
	}
	
    /**
     * @return LmtCtrNo
     */	
	public String getLmtCtrNo() {
		return this.lmtCtrNo;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType == null ? null : appType.trim();
	}
	
    /**
     * @return AppType
     */	
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtTotlAmt
	 */
	public void setLmtTotlAmt(java.math.BigDecimal lmtTotlAmt) {
		this.lmtTotlAmt = lmtTotlAmt;
	}
	
    /**
     * @return LmtTotlAmt
     */	
	public java.math.BigDecimal getLmtTotlAmt() {
		return this.lmtTotlAmt;
	}
	
	/**
	 * @param revolvAmt
	 */
	public void setRevolvAmt(java.math.BigDecimal revolvAmt) {
		this.revolvAmt = revolvAmt;
	}
	
    /**
     * @return RevolvAmt
     */	
	public java.math.BigDecimal getRevolvAmt() {
		return this.revolvAmt;
	}
	
	/**
	 * @param oneAmt
	 */
	public void setOneAmt(java.math.BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}
	
    /**
     * @return OneAmt
     */	
	public java.math.BigDecimal getOneAmt() {
		return this.oneAmt;
	}
	
	/**
	 * @param lowRiskType
	 */
	public void setLowRiskType(String lowRiskType) {
		this.lowRiskType = lowRiskType == null ? null : lowRiskType.trim();
	}
	
    /**
     * @return LowRiskType
     */	
	public String getLowRiskType() {
		return this.lowRiskType;
	}
	
	/**
	 * @param lmtStarDate
	 */
	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate == null ? null : lmtStarDate.trim();
	}
	
    /**
     * @return LmtStarDate
     */	
	public String getLmtStarDate() {
		return this.lmtStarDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate == null ? null : lmtEndDate.trim();
	}
	
    /**
     * @return LmtEndDate
     */	
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param lmtOutstndFrozeAmt
	 */
	public void setLmtOutstndFrozeAmt(String lmtOutstndFrozeAmt) {
		this.lmtOutstndFrozeAmt = lmtOutstndFrozeAmt == null ? null : lmtOutstndFrozeAmt.trim();
	}
	
    /**
     * @return LmtOutstndFrozeAmt
     */	
	public String getLmtOutstndFrozeAmt() {
		return this.lmtOutstndFrozeAmt;
	}
	
	/**
	 * @param lmtFrozeAmt
	 */
	public void setLmtFrozeAmt(String lmtFrozeAmt) {
		this.lmtFrozeAmt = lmtFrozeAmt == null ? null : lmtFrozeAmt.trim();
	}
	
    /**
     * @return LmtFrozeAmt
     */	
	public String getLmtFrozeAmt() {
		return this.lmtFrozeAmt;
	}
	
	/**
	 * @param lmtUnfrozeAmt
	 */
	public void setLmtUnfrozeAmt(String lmtUnfrozeAmt) {
		this.lmtUnfrozeAmt = lmtUnfrozeAmt == null ? null : lmtUnfrozeAmt.trim();
	}
	
    /**
     * @return LmtUnfrozeAmt
     */	
	public String getLmtUnfrozeAmt() {
		return this.lmtUnfrozeAmt;
	}
	
	/**
	 * @param frozeResn
	 */
	public void setFrozeResn(String frozeResn) {
		this.frozeResn = frozeResn == null ? null : frozeResn.trim();
	}
	
    /**
     * @return FrozeResn
     */	
	public String getFrozeResn() {
		return this.frozeResn;
	}
	
	/**
	 * @param unfrozeResn
	 */
	public void setUnfrozeResn(String unfrozeResn) {
		this.unfrozeResn = unfrozeResn == null ? null : unfrozeResn.trim();
	}
	
    /**
     * @return UnfrozeResn
     */	
	public String getUnfrozeResn() {
		return this.unfrozeResn;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
}