package cn.com.yusys.yusp.web.server.xddh0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0010.req.Xddh0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0010.Xddh0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0010:推送优享贷预警信息")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0010Resource.class);

    @Autowired
    private Xddh0010Service xddh0010Service;

    /**
     * 交易码：xddh0010
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0010DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0010")
    protected @ResponseBody
    ResultDto<Xddh0010DataRespDto> xddh0010(@Validated @RequestBody Xddh0010DataReqDto xddh0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010DataReqDto));
        Xddh0010DataRespDto xddh0010DataRespDto = new Xddh0010DataRespDto();// 响应Dto:推送优享贷预警信息
        ResultDto<Xddh0010DataRespDto> xddh0010DataResultDto = new ResultDto<>();
        try {
            //业务逻辑
            xddh0010DataRespDto = xddh0010Service.xddh0010(xddh0010DataReqDto);
            // 封装xddh0010DataResultDto中正确的返回码和返回信息
            xddh0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, e.getMessage());
            // 封装xddh0010DataResultDto中异常返回码和返回信息
            xddh0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0010DataRespDto到xddh0010DataResultDto中
        xddh0010DataResultDto.setData(xddh0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010DataResultDto));
        return xddh0010DataResultDto;
    }
}
