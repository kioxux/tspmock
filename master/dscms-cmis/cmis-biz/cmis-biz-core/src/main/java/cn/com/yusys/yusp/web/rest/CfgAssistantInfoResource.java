/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgAssistantInfo;
import cn.com.yusys.yusp.service.CfgAssistantInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgAssistantInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-28 15:47:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgassistantinfo")
public class CfgAssistantInfoResource {
    @Autowired
    private CfgAssistantInfoService cfgAssistantInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgAssistantInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgAssistantInfo> list = cfgAssistantInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgAssistantInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CfgAssistantInfo>> index(@RequestBody QueryModel queryModel) {
        List<CfgAssistantInfo> list = cfgAssistantInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgAssistantInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CfgAssistantInfo> show(@PathVariable("serno") String serno) {
        CfgAssistantInfo cfgAssistantInfo = cfgAssistantInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CfgAssistantInfo>(cfgAssistantInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CfgAssistantInfo> create(@RequestBody CfgAssistantInfo cfgAssistantInfo) throws URISyntaxException {
        cfgAssistantInfoService.insert(cfgAssistantInfo);
        return new ResultDto<CfgAssistantInfo>(cfgAssistantInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgAssistantInfo cfgAssistantInfo) throws URISyntaxException {
        int result = cfgAssistantInfoService.update(cfgAssistantInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cfgAssistantInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgAssistantInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getNoticeByPrdIdAndBizType
     * @函数描述:根据产品编号、业务阶段，查询业务小助手的注意事项
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getNoticeByPrdIdAndBizType/")
    protected ResultDto<List<Map<String, Object>>> getNoticeByPrdIdAndBizType(@RequestBody QueryModel queryModel) {
        if((queryModel.getCondition().get("prdCode")==null||"".equals(queryModel.getCondition().get("prdCode")))&&
                (queryModel.getCondition().get("prdTypeProp")==null||"".equals(queryModel.getCondition().get("prdTypeProp")))){
            return new ResultDto<List<Map<String, Object>>>();
        }
        if((queryModel.getCondition().get("prdCode")!=null && !"".equals(queryModel.getCondition().get("prdCode"))) &&
                (queryModel.getCondition().get("prdTypeProp")==null||"".equals(queryModel.getCondition().get("prdTypeProp")))
                ){
            List<Map<String, Object>> list = cfgAssistantInfoService.getNoticeByPrdIdAndBizTypeNonPrdTypeProp(queryModel);
            return new ResultDto<List<Map<String, Object>>>(list);
        }else{
            List<Map<String, Object>> list = cfgAssistantInfoService.getNoticeByPrdIdAndBizType(queryModel);
            return new ResultDto<List<Map<String, Object>>>(list);
        }

    }

}
