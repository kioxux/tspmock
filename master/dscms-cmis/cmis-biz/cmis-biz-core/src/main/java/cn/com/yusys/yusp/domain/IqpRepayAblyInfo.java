/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayAblyInfo
 * @类描述: iqp_repay_ably_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:16:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_ably_info")
public class IqpRepayAblyInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 商贷月还款金额 **/
	@Column(name = "M_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mRepayAmt;
	
	/** 公积金月还款 **/
	@Column(name = "M_HPF_REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mHpfRepayAmt;
	
	/** I/I(%) **/
	@Column(name = "M_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mAmt;
	
	/** D/I(%) **/
	@Column(name = "D_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dAmt;
	
	/** D/I(%)（折算后） **/
	@Column(name = "D_AMT_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dAmtCvt;
	
	/** 总还款额 **/
	@Column(name = "M_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mTotalAmt;
	
	/** 月收入总额（元） **/
	@Column(name = "M_INCOME_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mIncomeTot;
	
	/** 资产价值汇总（折算后） **/
	@Column(name = "TOTAL_ASSET_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalAssetCvt;
	
	/** 月物业管理费（元） **/
	@Column(name = "M_PROPERTY_FEE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal mPropertyFee;
	
	/** 已有负债月还款额汇总 **/
	@Column(name = "DEBT_M_REPAY_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtMRepayTot;
	
	/** 已有负债金额汇总 **/
	@Column(name = "DEBT_AMT_TOT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtAmtTot;
	
	/** 已有负债月还款额汇总（折算后） **/
	@Column(name = "DEBT_M_REPAY_TOT_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtMRepayTotCvt;
	
	/** 已有负债金额汇总（折算后） **/
	@Column(name = "DEBT_AMT_TOT_CVT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal debtAmtTotCvt;
	
	/** 测算阶段 **/
	@Column(name = "CALCULATE_STAGE", unique = false, nullable = true, length = 20)
	private String calculateStage;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param mRepayAmt
	 */
	public void setMRepayAmt(java.math.BigDecimal mRepayAmt) {
		this.mRepayAmt = mRepayAmt;
	}
	
    /**
     * @return mRepayAmt
     */
	public java.math.BigDecimal getMRepayAmt() {
		return this.mRepayAmt;
	}
	
	/**
	 * @param mHpfRepayAmt
	 */
	public void setMHpfRepayAmt(java.math.BigDecimal mHpfRepayAmt) {
		this.mHpfRepayAmt = mHpfRepayAmt;
	}
	
    /**
     * @return mHpfRepayAmt
     */
	public java.math.BigDecimal getMHpfRepayAmt() {
		return this.mHpfRepayAmt;
	}
	
	/**
	 * @param mAmt
	 */
	public void setMAmt(java.math.BigDecimal mAmt) {
		this.mAmt = mAmt;
	}
	
    /**
     * @return mAmt
     */
	public java.math.BigDecimal getMAmt() {
		return this.mAmt;
	}
	
	/**
	 * @param dAmt
	 */
	public void setDAmt(java.math.BigDecimal dAmt) {
		this.dAmt = dAmt;
	}
	
    /**
     * @return dAmt
     */
	public java.math.BigDecimal getDAmt() {
		return this.dAmt;
	}
	
	/**
	 * @param dAmtCvt
	 */
	public void setDAmtCvt(java.math.BigDecimal dAmtCvt) {
		this.dAmtCvt = dAmtCvt;
	}
	
    /**
     * @return dAmtCvt
     */
	public java.math.BigDecimal getDAmtCvt() {
		return this.dAmtCvt;
	}
	
	/**
	 * @param mTotalAmt
	 */
	public void setMTotalAmt(java.math.BigDecimal mTotalAmt) {
		this.mTotalAmt = mTotalAmt;
	}
	
    /**
     * @return mTotalAmt
     */
	public java.math.BigDecimal getMTotalAmt() {
		return this.mTotalAmt;
	}
	
	/**
	 * @param mIncomeTot
	 */
	public void setMIncomeTot(java.math.BigDecimal mIncomeTot) {
		this.mIncomeTot = mIncomeTot;
	}
	
    /**
     * @return mIncomeTot
     */
	public java.math.BigDecimal getMIncomeTot() {
		return this.mIncomeTot;
	}
	
	/**
	 * @param totalAssetCvt
	 */
	public void setTotalAssetCvt(java.math.BigDecimal totalAssetCvt) {
		this.totalAssetCvt = totalAssetCvt;
	}
	
    /**
     * @return totalAssetCvt
     */
	public java.math.BigDecimal getTotalAssetCvt() {
		return this.totalAssetCvt;
	}
	
	/**
	 * @param mPropertyFee
	 */
	public void setMPropertyFee(java.math.BigDecimal mPropertyFee) {
		this.mPropertyFee = mPropertyFee;
	}
	
    /**
     * @return mPropertyFee
     */
	public java.math.BigDecimal getMPropertyFee() {
		return this.mPropertyFee;
	}
	
	/**
	 * @param debtMRepayTot
	 */
	public void setDebtMRepayTot(java.math.BigDecimal debtMRepayTot) {
		this.debtMRepayTot = debtMRepayTot;
	}
	
    /**
     * @return debtMRepayTot
     */
	public java.math.BigDecimal getDebtMRepayTot() {
		return this.debtMRepayTot;
	}
	
	/**
	 * @param debtAmtTot
	 */
	public void setDebtAmtTot(java.math.BigDecimal debtAmtTot) {
		this.debtAmtTot = debtAmtTot;
	}
	
    /**
     * @return debtAmtTot
     */
	public java.math.BigDecimal getDebtAmtTot() {
		return this.debtAmtTot;
	}
	
	/**
	 * @param debtMRepayTotCvt
	 */
	public void setDebtMRepayTotCvt(java.math.BigDecimal debtMRepayTotCvt) {
		this.debtMRepayTotCvt = debtMRepayTotCvt;
	}
	
    /**
     * @return debtMRepayTotCvt
     */
	public java.math.BigDecimal getDebtMRepayTotCvt() {
		return this.debtMRepayTotCvt;
	}
	
	/**
	 * @param debtAmtTotCvt
	 */
	public void setDebtAmtTotCvt(java.math.BigDecimal debtAmtTotCvt) {
		this.debtAmtTotCvt = debtAmtTotCvt;
	}
	
    /**
     * @return debtAmtTotCvt
     */
	public java.math.BigDecimal getDebtAmtTotCvt() {
		return this.debtAmtTotCvt;
	}
	
	/**
	 * @param calculateStage
	 */
	public void setCalculateStage(String calculateStage) {
		this.calculateStage = calculateStage;
	}
	
    /**
     * @return calculateStage
     */
	public String getCalculateStage() {
		return this.calculateStage;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}