/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.OtherRecordSpecialLoanApp;
import cn.com.yusys.yusp.dto.OtherAppDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherItemApp;
import cn.com.yusys.yusp.service.OtherItemAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherItemAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: hhj123456
 * @创建时间: 2021-06-15 14:17:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/otheritemapp")
public class OtherItemAppResource {
    @Autowired
    private OtherItemAppService otherItemAppService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherItemApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherItemApp> list = otherItemAppService.selectAll(queryModel);
        return new ResultDto<List<OtherItemApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherItemApp>> index(QueryModel queryModel) {
        List<OtherItemApp> list = otherItemAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherItemApp>>(list);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<OtherItemApp>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherItemApp> list = otherItemAppService.selectByModel(queryModel);
        return new ResultDto<List<OtherItemApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<OtherItemApp> show(@PathVariable("serno") String serno) {
        OtherItemApp otherItemApp = otherItemAppService.selectByPrimaryKey(serno);
        return new ResultDto<OtherItemApp>(otherItemApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherItemApp> create(@RequestBody OtherItemApp otherItemApp) throws URISyntaxException {
        otherItemAppService.insert(otherItemApp);
        return new ResultDto<OtherItemApp>(otherItemApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherItemApp otherItemApp) throws URISyntaxException {
        int result = otherItemAppService.update(otherItemApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: updateotheritemapp
     * @方法描述: 新增其他申请事项审批表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addotheritemapp")
    protected ResultDto addotheritemapp(@RequestBody OtherItemApp otherItemApp) {
        return otherItemAppService.addotheritemapp(otherItemApp);
    }

    /**
     * @方法名称: updateotheritemapp
     * @方法描述: 修改其他申请事项审批表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateotheritemapp")
    protected ResultDto updateotheritemapp(@RequestBody OtherItemApp otherItemApp) {
        return otherItemAppService.updateotheritemapp(otherItemApp);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = otherItemAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherItemAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：updateOprType
     * @方法描述：根据主键逻辑删除
     * @创建人：zhangming12
     * @创建时间：2021/5/19 10:44
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/updateoprtype")
    protected ResultDto<Integer> updateOprType(@RequestBody String serno){
        int affectRow = otherItemAppService.updateOprType(serno);
        return new ResultDto<>(affectRow);
    }
    
    /**
     * @方法名称: getotheritemapp
     * @方法描述: 根据入参获取当前客户经理名下其他申请事项审批表 
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/getotheritemapp")
    protected ResultDto<List<OtherItemApp>> getOtherAccpPerferFeeApp(@RequestBody QueryModel model) {
        List<OtherItemApp> list = otherItemAppService.selectByModel(model);
        return new ResultDto<List<OtherItemApp>>(list);
    }

    /**
     * @方法名称: selectotheritemappdatabyserno
     * @方法描述: 授信申请中,根据授信申请流水号查询其他申报审批数据(All)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectotheritemappdatabyserno")
    protected ResultDto<List<OtherAppDto>> selectOtherItemAppDataBySerno(@RequestBody QueryModel model) {
        List<OtherAppDto> list = otherItemAppService.selectOtherItemAppDataBySerno(model);
        return ResultDto.success(list);
    }


    /**
     * @方法名称: selectOtherItemByIqpSerno
     * @方法描述: 授信申请中,根据授信申请流水号查询其他申报审批数据(All)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectOtherItemByIqpSerno")
    protected ResultDto<List<OtherAppDto>> selectOtherItemByIqpSerno(@RequestBody QueryModel model) {
        List<OtherAppDto> list = otherItemAppService.selectOtherItemByIqpSerno(model);
        return ResultDto.success(list);
    }
    
    /**
     * @方法名称: checkFlag
     * @方法描述: 查询是否有通过人民币利率定价申请-展期利率定价(All)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/checkFlag")
    protected ResultDto<String> checkFlag(@RequestBody QueryModel model) {
        String flag = otherItemAppService.checkFlag(model);
        return new ResultDto<String>(flag);
    }
    
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    /**
     * @函数名称:insertOtherApproveByLmtSerno
     * @函数描述:根据授信流水号新增其他事项申报信息
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @PostMapping("/insertOtherApproveByIqpSerno")
    protected ResultDto<OtherAppDto> insertOtherApproveByIqpSerno(@RequestBody Map map){
        OtherAppDto otherAppDto = otherItemAppService.insertOtherApproveByIqpSerno(map);
        return ResultDto.success(otherAppDto);
    }

}
