/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRecheckDetail
 * @类描述: lmt_recheck_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-15 19:41:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_recheck_detail")
public class LmtRecheckDetail extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = false, length = 40)
	private String lmtSerno;
	
	/** 环保情况是否达标 **/
	@Column(name = "ENVI_CON_FLAG", unique = false, nullable = true, length = 5)
	private String enviConFlag;
	
	/** 是否涉农 **/
	@Column(name = "AGRI_FLAG", unique = false, nullable = true, length = 5)
	private String agriFlag;
	
	/** 是否小微 **/
	@Column(name = "SMALL_IND", unique = false, nullable = true, length = 5)
	private String smallInd;
	
	/** 是否制造业 **/
	@Column(name = "MANU_IND", unique = false, nullable = true, length = 5)
	private String manuInd;
	
	/** 是否为九大行业 **/
	@Column(name = "NINE_INDU_FLAG", unique = false, nullable = true, length = 5)
	private String nineInduFlag;
	
	/** 客户内部分类 **/
	@Column(name = "CUS_INTER_CLASS", unique = false, nullable = true, length = 5)
	private String cusInterClass;
	
	/** 企业股权是否发生变更 **/
	@Column(name = "ENTERPRISE_EQYITY_HAS_CHANGE", unique = false, nullable = true, length = 5)
	private String enterpriseEqyityHasChange;
	
	/** 企业股权变更说明 **/
	@Column(name = "ENTERPRISE_EQYITY_CHANGE_INST", unique = false, nullable = true, length = 65535)
	private String enterpriseEqyityChangeInst;
	
	/** 最近第二年开票销售 **/
	@Column(name = "REC_SEC_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecInvoiceSales;
	
	/** 最近第二年缴纳增值税 **/
	@Column(name = "REC_SEC_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecVat;
	
	/** 最近第二年缴纳所得税 **/
	@Column(name = "REC_SEC_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecIncomeTax;
	
	/** 最近第二年税务报表净利润 **/
	@Column(name = "REC_SEC_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecNetProfit;
	
	/** 最近第二年自制报表销售 **/
	@Column(name = "REC_SEC_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecSelfRepSales;
	
	/** 最近第二年自制报表净利润 **/
	@Column(name = "REC_SEC_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recSecSelfRepProfit;
	
	/** 最近第一年开票销售 **/
	@Column(name = "REC_FIR_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirInvoiceSales;
	
	/** 最近第一年缴纳增值税 **/
	@Column(name = "REC_FIR_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirVat;
	
	/** 最近第一年缴纳所得税 **/
	@Column(name = "REC_FIR_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirIncomeTax;
	
	/** 最近第一年税务报表净利润 **/
	@Column(name = "REC_FIR_NETPROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirNetprofit;
	
	/** 最近第一年自制报表销售 **/
	@Column(name = "REC_FIR_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirSelfRepSales;
	
	/** 最近第一年自制报表净利润 **/
	@Column(name = "REC_FIR_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal recFirSelfRepProfit;
	
	/** 当前年月开票销售 **/
	@Column(name = "CURRENT_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentInvoiceSales;
	
	/** 当前年月缴纳增值税 **/
	@Column(name = "CURRENT_VAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentVat;
	
	/** 当前年月缴纳所得税 **/
	@Column(name = "CURRENT_INCOME_TAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentIncomeTax;
	
	/** 当前年月税务报表净利润 **/
	@Column(name = "CURRENT_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentNetProfit;
	
	/** 当前年月自制报表销售 **/
	@Column(name = "CURRENT_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentSelfRepSales;
	
	/** 当前年月自制报表净利润 **/
	@Column(name = "CURRENT_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currentSelfRepProfit;
	
	/** 变动原因分析 **/
	@Column(name = "CHAGE_RESN", unique = false, nullable = true, length = 65535)
	private String chageResn;
	
	/** 最新开台率情况 **/
	@Column(name = "LATEST_OPEN_RATE_SITU", unique = false, nullable = true, length = 65535)
	private String latestOpenRateSitu;
	
	/** 授信期内是否新增大量对外投资 **/
	@Column(name = "EXIT_LARGE_INVEST_FLAG", unique = false, nullable = true, length = 5)
	private String exitLargeInvestFlag;
	
	/** 授信期内新增大量对外投资说明 **/
	@Column(name = "EXIT_LARGE_INVEST_ACC", unique = false, nullable = true, length = 65535)
	private String exitLargeInvestAcc;
	
	/** 近期是否出现欠息情况 **/
	@Column(name = "DEBIT_INTEREST_FLAG", unique = false, nullable = true, length = 5)
	private String debitInterestFlag;
	
	/** 近期出现欠息情况说明 **/
	@Column(name = "DEBIT_INTEREST_ACC", unique = false, nullable = true, length = 65535)
	private String debitInterestAcc;
	
	/** 是否存在存货明显增加 **/
	@Column(name = "INCREASE_INVENTORY_FLAG", unique = false, nullable = true, length = 5)
	private String increaseInventoryFlag;
	
	/** 存在存货明显增加说明 **/
	@Column(name = "INCREASE_INVENTORY_ACC", unique = false, nullable = true, length = 65535)
	private String increaseInventoryAcc;
	
	/** 是否存在应收款明显增加 **/
	@Column(name = "ACCOUNT_INVENTORY_FLAG", unique = false, nullable = true, length = 5)
	private String accountInventoryFlag;
	
	/** 存在应收款明显增加说明 **/
	@Column(name = "ACCOUNT_INVENTORY_ACC", unique = false, nullable = true, length = 65535)
	private String accountInventoryAcc;
	
	/** 是否与非业务单位存在大额非业务资金往来 **/
	@Column(name = "NON_BUSINESS_CAP_TRANSACTIONS_FLAG", unique = false, nullable = true, length = 5)
	private String nonBusinessCapTransactionsFlag;
	
	/** 与非业务单位存在大额非业务资金往来说明 **/
	@Column(name = "NON_BUSINESS_CAP_TRANSACTIONS_ACC", unique = false, nullable = true, length = 65535)
	private String nonBusinessCapTransactionsAcc;
	
	/** 是否存在固定资产、在建工程明显增加情况 **/
	@Column(name = "FIXED_ASSETS_INVENTORY_FLAG", unique = false, nullable = true, length = 5)
	private String fixedAssetsInventoryFlag;
	
	/** 存在固定资产、在建工程明显增加情况说明 **/
	@Column(name = "FIXED_ASSETS_INVENTORY_ACC", unique = false, nullable = true, length = 65535)
	private String fixedAssetsInventoryAcc;
	
	/** 是否出现过风险预警 **/
	@Column(name = "RISK_WARNING_FLAG", unique = false, nullable = true, length = 5)
	private String riskWarningFlag;
	
	/** 出现过风险预警说明 **/
	@Column(name = "RISK_WARNING_ACC", unique = false, nullable = true, length = 65535)
	private String riskWarningAcc;
	
	/** 公司实际控制人是否存在“涉赌、涉毒”等情况 **/
	@Column(name = "ACTUAL_CONTROLLER_EXIT_HDD_FLAG", unique = false, nullable = true, length = 5)
	private String actualControllerExitHddFlag;
	
	/** 公司实际控制人存在“涉赌、涉毒”等情况说明 **/
	@Column(name = "ACTUAL_CONTROLLER_EXIT_HDD_ACC", unique = false, nullable = true, length = 65535)
	private String actualControllerExitHddAcc;
	
	/** 生产经营是否发生重大变化 **/
	@Column(name = "MAJOR_CHANGE_PRODUCT_FLAG", unique = false, nullable = true, length = 5)
	private String majorChangeProductFlag;
	
	/** 生产经营发生重大变化说明 **/
	@Column(name = "MAJOR_CHANGE_PRODUCT_ACC", unique = false, nullable = true, length = 65535)
	private String majorChangeProductAcc;
	
	/** 本行账户是否被查封，本行结算是否正常 **/
	@Column(name = "ACCOUNT_SEIZED_FLAG", unique = false, nullable = true, length = 5)
	private String accountSeizedFlag;
	
	/** 本行账户被查封，本行结算非正常说明 **/
	@Column(name = "ACCOUNT_SEIZED_ACC", unique = false, nullable = true, length = 65535)
	private String accountSeizedAcc;
	
	/** 是否存在未结案事项 **/
	@Column(name = "UNRESOLVED_MATTERS_FLAG", unique = false, nullable = true, length = 5)
	private String unresolvedMattersFlag;
	
	/** 存在未结案事项说明 **/
	@Column(name = "UNRESOLVED_MATTERS_ACC", unique = false, nullable = true, length = 65535)
	private String unresolvedMattersAcc;
	
	/** 一般公司保证担保人名称 **/
	@Column(name = "GENERAL_COM_ASSURE_NAME", unique = false, nullable = true, length = 80)
	private String generalComAssureName;
	
	/** 担保人与借款人关系 **/
	@Column(name = "GUAR_DEBIT_RELA", unique = false, nullable = true, length = 5)
	private String guarDebitRela;
	
	/** 企业类型 **/
	@Column(name = "CON_TYPE", unique = false, nullable = true, length = 5)
	private String conType;
	
	/** 一般公司保证担保最近第二年开票销售 **/
	@Column(name = "GENERAL_COM_REC_SEC_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecSecInvoiceSales;
	
	/** 一般公司保证担保最近第二年税务报表净利润 **/
	@Column(name = "GENERAL_COM_REC_SEC_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecSecNetProfit;
	
	/** 一般公司保证担保最近第二年自制报表销售 **/
	@Column(name = "GENERAL_COM_REC_SEC_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecSecSelfRepSales;
	
	/** 一般公司保证担保最近第二年自制报表净利润 **/
	@Column(name = "GENERAL_COM_REC_SEC_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecSecSelfRepProfit;
	
	/** 一般公司保证担保最近第二年净资产情况 **/
	@Column(name = "GENERAL_COM_REC_SEC_NET_ASSET", unique = false, nullable = true, length = 65535)
	private String generalComRecSecNetAsset;
	
	/** 一般公司保证担保最近第一年开票销售 **/
	@Column(name = "GENERAL_COM_REC_FIR_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecFirInvoiceSales;
	
	/** 一般公司保证担保最近第一年税务报表净利润 **/
	@Column(name = "GENERAL_COM_REC_FIR_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecFirNetProfit;
	
	/** 一般公司保证担保最近第一年自制报表销售 **/
	@Column(name = "GENERAL_COM_REC_FIR_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecFirSelfRepSales;
	
	/** 一般公司保证担保最近第一年自制报表净利润 **/
	@Column(name = "GENERAL_COM_REC_FIR_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComRecFirSelfRepProfit;
	
	/** 一般公司保证担保最近第一年净资产情况 **/
	@Column(name = "GENERAL_COM_REC_FIR_NET_ASSET", unique = false, nullable = true, length = 65535)
	private String generalComRecFirNetAsset;
	
	/** 一般公司保证担保当前年月开票销售 **/
	@Column(name = "GENERAL_COM_CURRENT_INVOICE_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComCurrentInvoiceSales;
	
	/** 一般公司保证担保当前年月税务报表净利润 **/
	@Column(name = "GENERAL_COM_CURRENT_NET_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComCurrentNetProfit;
	
	/** 一般公司保证担保当前年月自制报表销售 **/
	@Column(name = "GENERAL_COM_CURRENT_SELF_REP_SALES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComCurrentSelfRepSales;
	
	/** 一般公司保证担保当前年月自制报表净利润 **/
	@Column(name = "GENERAL_COM_CURRENT_SELF_REP_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal generalComCurrentSelfRepProfit;
	
	/** 一般公司保证担保当前年月净资产情况 **/
	@Column(name = "GENERAL_COM_CURRENT_NET_ASSET", unique = false, nullable = true, length = 65535)
	private String generalComCurrentNetAsset;
	
	/** 他行融资额度 **/
	@Column(name = "OTHER_BANK_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherBankLmt;
	
	/** 本行融资额度 **/
	@Column(name = "OUR_BANK_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ourBankLmt;
	
	/** 个人经营性贷款金额 **/
	@Column(name = "LIMIT_PERSONAL_LOANS_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal limitPersonalLoansAmt;
	
	/** 正常担保金额 **/
	@Column(name = "NORMAL_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal normalGuarAmt;
	
	/** 关注担保金额 **/
	@Column(name = "FOCUS_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal focusGuarAmt;
	
	/** 不良担保金额 **/
	@Column(name = "BADNESS_GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal badnessGuarAmt;
	
	/** 担保能力总体评价 **/
	@Column(name = "GUAR_ABL_EVAL", unique = false, nullable = true, length = 65535)
	private String guarAblEval;
	
	/** 一般公司保证担保其他说明 **/
	@Column(name = "GENERAL_COM_ASSURE_ACC", unique = false, nullable = true, length = 65535)
	private String generalComAssureAcc;
	
	/** 抵押担保其他说明 **/
	@Column(name = "GUAR_ASSURE_ACC", unique = false, nullable = true, length = 65535)
	private String guarAssureAcc;
	
	/** 担保公司名称 **/
	@Column(name = "GUAR_COM_NAME", unique = false, nullable = true, length = 80)
	private String guarComName;
	
	/** 一类/二类 **/
	@Column(name = "SAME_SECOND_CLASS", unique = false, nullable = true, length = 5)
	private String sameSecondClass;
	
	/** 担保公司其他说明 **/
	@Column(name = "GUAR_COM_ACC", unique = false, nullable = true, length = 65535)
	private String guarComAcc;
	
	/** 其它担保方式相关情况 **/
	@Column(name = "OTHER_GUAR_ACC", unique = false, nullable = true, length = 65535)
	private String otherGuarAcc;
	
	/** 综合分析 **/
	@Column(name = "INTE_ANALY", unique = false, nullable = true, length = 65535)
	private String inteAnaly;
	
	/** 控制意见 **/
	@Column(name = "CONTROL_OPINION", unique = false, nullable = true, length = 65535)
	private String controlOpinion;
	
	/** 结论 **/
	@Column(name = "REST_DESC", unique = false, nullable = true, length = 65535)
	private String restDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param enviConFlag
	 */
	public void setEnviConFlag(String enviConFlag) {
		this.enviConFlag = enviConFlag;
	}
	
    /**
     * @return enviConFlag
     */
	public String getEnviConFlag() {
		return this.enviConFlag;
	}
	
	/**
	 * @param agriFlag
	 */
	public void setAgriFlag(String agriFlag) {
		this.agriFlag = agriFlag;
	}
	
    /**
     * @return agriFlag
     */
	public String getAgriFlag() {
		return this.agriFlag;
	}
	
	/**
	 * @param smallInd
	 */
	public void setSmallInd(String smallInd) {
		this.smallInd = smallInd;
	}
	
    /**
     * @return smallInd
     */
	public String getSmallInd() {
		return this.smallInd;
	}
	
	/**
	 * @param manuInd
	 */
	public void setManuInd(String manuInd) {
		this.manuInd = manuInd;
	}
	
    /**
     * @return manuInd
     */
	public String getManuInd() {
		return this.manuInd;
	}
	
	/**
	 * @param nineInduFlag
	 */
	public void setNineInduFlag(String nineInduFlag) {
		this.nineInduFlag = nineInduFlag;
	}
	
    /**
     * @return nineInduFlag
     */
	public String getNineInduFlag() {
		return this.nineInduFlag;
	}
	
	/**
	 * @param cusInterClass
	 */
	public void setCusInterClass(String cusInterClass) {
		this.cusInterClass = cusInterClass;
	}
	
    /**
     * @return cusInterClass
     */
	public String getCusInterClass() {
		return this.cusInterClass;
	}
	
	/**
	 * @param enterpriseEqyityHasChange
	 */
	public void setEnterpriseEqyityHasChange(String enterpriseEqyityHasChange) {
		this.enterpriseEqyityHasChange = enterpriseEqyityHasChange;
	}
	
    /**
     * @return enterpriseEqyityHasChange
     */
	public String getEnterpriseEqyityHasChange() {
		return this.enterpriseEqyityHasChange;
	}
	
	/**
	 * @param enterpriseEqyityChangeInst
	 */
	public void setEnterpriseEqyityChangeInst(String enterpriseEqyityChangeInst) {
		this.enterpriseEqyityChangeInst = enterpriseEqyityChangeInst;
	}
	
    /**
     * @return enterpriseEqyityChangeInst
     */
	public String getEnterpriseEqyityChangeInst() {
		return this.enterpriseEqyityChangeInst;
	}
	
	/**
	 * @param recSecInvoiceSales
	 */
	public void setRecSecInvoiceSales(java.math.BigDecimal recSecInvoiceSales) {
		this.recSecInvoiceSales = recSecInvoiceSales;
	}
	
    /**
     * @return recSecInvoiceSales
     */
	public java.math.BigDecimal getRecSecInvoiceSales() {
		return this.recSecInvoiceSales;
	}
	
	/**
	 * @param recSecVat
	 */
	public void setRecSecVat(java.math.BigDecimal recSecVat) {
		this.recSecVat = recSecVat;
	}
	
    /**
     * @return recSecVat
     */
	public java.math.BigDecimal getRecSecVat() {
		return this.recSecVat;
	}
	
	/**
	 * @param recSecIncomeTax
	 */
	public void setRecSecIncomeTax(java.math.BigDecimal recSecIncomeTax) {
		this.recSecIncomeTax = recSecIncomeTax;
	}
	
    /**
     * @return recSecIncomeTax
     */
	public java.math.BigDecimal getRecSecIncomeTax() {
		return this.recSecIncomeTax;
	}
	
	/**
	 * @param recSecNetProfit
	 */
	public void setRecSecNetProfit(java.math.BigDecimal recSecNetProfit) {
		this.recSecNetProfit = recSecNetProfit;
	}
	
    /**
     * @return recSecNetProfit
     */
	public java.math.BigDecimal getRecSecNetProfit() {
		return this.recSecNetProfit;
	}
	
	/**
	 * @param recSecSelfRepSales
	 */
	public void setRecSecSelfRepSales(java.math.BigDecimal recSecSelfRepSales) {
		this.recSecSelfRepSales = recSecSelfRepSales;
	}
	
    /**
     * @return recSecSelfRepSales
     */
	public java.math.BigDecimal getRecSecSelfRepSales() {
		return this.recSecSelfRepSales;
	}
	
	/**
	 * @param recSecSelfRepProfit
	 */
	public void setRecSecSelfRepProfit(java.math.BigDecimal recSecSelfRepProfit) {
		this.recSecSelfRepProfit = recSecSelfRepProfit;
	}
	
    /**
     * @return recSecSelfRepProfit
     */
	public java.math.BigDecimal getRecSecSelfRepProfit() {
		return this.recSecSelfRepProfit;
	}
	
	/**
	 * @param recFirInvoiceSales
	 */
	public void setRecFirInvoiceSales(java.math.BigDecimal recFirInvoiceSales) {
		this.recFirInvoiceSales = recFirInvoiceSales;
	}
	
    /**
     * @return recFirInvoiceSales
     */
	public java.math.BigDecimal getRecFirInvoiceSales() {
		return this.recFirInvoiceSales;
	}
	
	/**
	 * @param recFirVat
	 */
	public void setRecFirVat(java.math.BigDecimal recFirVat) {
		this.recFirVat = recFirVat;
	}
	
    /**
     * @return recFirVat
     */
	public java.math.BigDecimal getRecFirVat() {
		return this.recFirVat;
	}
	
	/**
	 * @param recFirIncomeTax
	 */
	public void setRecFirIncomeTax(java.math.BigDecimal recFirIncomeTax) {
		this.recFirIncomeTax = recFirIncomeTax;
	}
	
    /**
     * @return recFirIncomeTax
     */
	public java.math.BigDecimal getRecFirIncomeTax() {
		return this.recFirIncomeTax;
	}
	
	/**
	 * @param recFirNetprofit
	 */
	public void setRecFirNetprofit(java.math.BigDecimal recFirNetprofit) {
		this.recFirNetprofit = recFirNetprofit;
	}
	
    /**
     * @return recFirNetprofit
     */
	public java.math.BigDecimal getRecFirNetprofit() {
		return this.recFirNetprofit;
	}
	
	/**
	 * @param recFirSelfRepSales
	 */
	public void setRecFirSelfRepSales(java.math.BigDecimal recFirSelfRepSales) {
		this.recFirSelfRepSales = recFirSelfRepSales;
	}
	
    /**
     * @return recFirSelfRepSales
     */
	public java.math.BigDecimal getRecFirSelfRepSales() {
		return this.recFirSelfRepSales;
	}
	
	/**
	 * @param recFirSelfRepProfit
	 */
	public void setRecFirSelfRepProfit(java.math.BigDecimal recFirSelfRepProfit) {
		this.recFirSelfRepProfit = recFirSelfRepProfit;
	}
	
    /**
     * @return recFirSelfRepProfit
     */
	public java.math.BigDecimal getRecFirSelfRepProfit() {
		return this.recFirSelfRepProfit;
	}
	
	/**
	 * @param currentInvoiceSales
	 */
	public void setCurrentInvoiceSales(java.math.BigDecimal currentInvoiceSales) {
		this.currentInvoiceSales = currentInvoiceSales;
	}
	
    /**
     * @return currentInvoiceSales
     */
	public java.math.BigDecimal getCurrentInvoiceSales() {
		return this.currentInvoiceSales;
	}
	
	/**
	 * @param currentVat
	 */
	public void setCurrentVat(java.math.BigDecimal currentVat) {
		this.currentVat = currentVat;
	}
	
    /**
     * @return currentVat
     */
	public java.math.BigDecimal getCurrentVat() {
		return this.currentVat;
	}
	
	/**
	 * @param currentIncomeTax
	 */
	public void setCurrentIncomeTax(java.math.BigDecimal currentIncomeTax) {
		this.currentIncomeTax = currentIncomeTax;
	}
	
    /**
     * @return currentIncomeTax
     */
	public java.math.BigDecimal getCurrentIncomeTax() {
		return this.currentIncomeTax;
	}
	
	/**
	 * @param currentNetProfit
	 */
	public void setCurrentNetProfit(java.math.BigDecimal currentNetProfit) {
		this.currentNetProfit = currentNetProfit;
	}
	
    /**
     * @return currentNetProfit
     */
	public java.math.BigDecimal getCurrentNetProfit() {
		return this.currentNetProfit;
	}
	
	/**
	 * @param currentSelfRepSales
	 */
	public void setCurrentSelfRepSales(java.math.BigDecimal currentSelfRepSales) {
		this.currentSelfRepSales = currentSelfRepSales;
	}
	
    /**
     * @return currentSelfRepSales
     */
	public java.math.BigDecimal getCurrentSelfRepSales() {
		return this.currentSelfRepSales;
	}
	
	/**
	 * @param currentSelfRepProfit
	 */
	public void setCurrentSelfRepProfit(java.math.BigDecimal currentSelfRepProfit) {
		this.currentSelfRepProfit = currentSelfRepProfit;
	}
	
    /**
     * @return currentSelfRepProfit
     */
	public java.math.BigDecimal getCurrentSelfRepProfit() {
		return this.currentSelfRepProfit;
	}
	
	/**
	 * @param chageResn
	 */
	public void setChageResn(String chageResn) {
		this.chageResn = chageResn;
	}
	
    /**
     * @return chageResn
     */
	public String getChageResn() {
		return this.chageResn;
	}
	
	/**
	 * @param latestOpenRateSitu
	 */
	public void setLatestOpenRateSitu(String latestOpenRateSitu) {
		this.latestOpenRateSitu = latestOpenRateSitu;
	}
	
    /**
     * @return latestOpenRateSitu
     */
	public String getLatestOpenRateSitu() {
		return this.latestOpenRateSitu;
	}
	
	/**
	 * @param exitLargeInvestFlag
	 */
	public void setExitLargeInvestFlag(String exitLargeInvestFlag) {
		this.exitLargeInvestFlag = exitLargeInvestFlag;
	}
	
    /**
     * @return exitLargeInvestFlag
     */
	public String getExitLargeInvestFlag() {
		return this.exitLargeInvestFlag;
	}
	
	/**
	 * @param exitLargeInvestAcc
	 */
	public void setExitLargeInvestAcc(String exitLargeInvestAcc) {
		this.exitLargeInvestAcc = exitLargeInvestAcc;
	}
	
    /**
     * @return exitLargeInvestAcc
     */
	public String getExitLargeInvestAcc() {
		return this.exitLargeInvestAcc;
	}
	
	/**
	 * @param debitInterestFlag
	 */
	public void setDebitInterestFlag(String debitInterestFlag) {
		this.debitInterestFlag = debitInterestFlag;
	}
	
    /**
     * @return debitInterestFlag
     */
	public String getDebitInterestFlag() {
		return this.debitInterestFlag;
	}
	
	/**
	 * @param debitInterestAcc
	 */
	public void setDebitInterestAcc(String debitInterestAcc) {
		this.debitInterestAcc = debitInterestAcc;
	}
	
    /**
     * @return debitInterestAcc
     */
	public String getDebitInterestAcc() {
		return this.debitInterestAcc;
	}
	
	/**
	 * @param increaseInventoryFlag
	 */
	public void setIncreaseInventoryFlag(String increaseInventoryFlag) {
		this.increaseInventoryFlag = increaseInventoryFlag;
	}
	
    /**
     * @return increaseInventoryFlag
     */
	public String getIncreaseInventoryFlag() {
		return this.increaseInventoryFlag;
	}
	
	/**
	 * @param increaseInventoryAcc
	 */
	public void setIncreaseInventoryAcc(String increaseInventoryAcc) {
		this.increaseInventoryAcc = increaseInventoryAcc;
	}
	
    /**
     * @return increaseInventoryAcc
     */
	public String getIncreaseInventoryAcc() {
		return this.increaseInventoryAcc;
	}
	
	/**
	 * @param accountInventoryFlag
	 */
	public void setAccountInventoryFlag(String accountInventoryFlag) {
		this.accountInventoryFlag = accountInventoryFlag;
	}
	
    /**
     * @return accountInventoryFlag
     */
	public String getAccountInventoryFlag() {
		return this.accountInventoryFlag;
	}
	
	/**
	 * @param accountInventoryAcc
	 */
	public void setAccountInventoryAcc(String accountInventoryAcc) {
		this.accountInventoryAcc = accountInventoryAcc;
	}
	
    /**
     * @return accountInventoryAcc
     */
	public String getAccountInventoryAcc() {
		return this.accountInventoryAcc;
	}
	
	/**
	 * @param nonBusinessCapTransactionsFlag
	 */
	public void setNonBusinessCapTransactionsFlag(String nonBusinessCapTransactionsFlag) {
		this.nonBusinessCapTransactionsFlag = nonBusinessCapTransactionsFlag;
	}
	
    /**
     * @return nonBusinessCapTransactionsFlag
     */
	public String getNonBusinessCapTransactionsFlag() {
		return this.nonBusinessCapTransactionsFlag;
	}
	
	/**
	 * @param nonBusinessCapTransactionsAcc
	 */
	public void setNonBusinessCapTransactionsAcc(String nonBusinessCapTransactionsAcc) {
		this.nonBusinessCapTransactionsAcc = nonBusinessCapTransactionsAcc;
	}
	
    /**
     * @return nonBusinessCapTransactionsAcc
     */
	public String getNonBusinessCapTransactionsAcc() {
		return this.nonBusinessCapTransactionsAcc;
	}
	
	/**
	 * @param fixedAssetsInventoryFlag
	 */
	public void setFixedAssetsInventoryFlag(String fixedAssetsInventoryFlag) {
		this.fixedAssetsInventoryFlag = fixedAssetsInventoryFlag;
	}
	
    /**
     * @return fixedAssetsInventoryFlag
     */
	public String getFixedAssetsInventoryFlag() {
		return this.fixedAssetsInventoryFlag;
	}
	
	/**
	 * @param fixedAssetsInventoryAcc
	 */
	public void setFixedAssetsInventoryAcc(String fixedAssetsInventoryAcc) {
		this.fixedAssetsInventoryAcc = fixedAssetsInventoryAcc;
	}
	
    /**
     * @return fixedAssetsInventoryAcc
     */
	public String getFixedAssetsInventoryAcc() {
		return this.fixedAssetsInventoryAcc;
	}
	
	/**
	 * @param riskWarningFlag
	 */
	public void setRiskWarningFlag(String riskWarningFlag) {
		this.riskWarningFlag = riskWarningFlag;
	}
	
    /**
     * @return riskWarningFlag
     */
	public String getRiskWarningFlag() {
		return this.riskWarningFlag;
	}
	
	/**
	 * @param riskWarningAcc
	 */
	public void setRiskWarningAcc(String riskWarningAcc) {
		this.riskWarningAcc = riskWarningAcc;
	}
	
    /**
     * @return riskWarningAcc
     */
	public String getRiskWarningAcc() {
		return this.riskWarningAcc;
	}
	
	/**
	 * @param actualControllerExitHddFlag
	 */
	public void setActualControllerExitHddFlag(String actualControllerExitHddFlag) {
		this.actualControllerExitHddFlag = actualControllerExitHddFlag;
	}
	
    /**
     * @return actualControllerExitHddFlag
     */
	public String getActualControllerExitHddFlag() {
		return this.actualControllerExitHddFlag;
	}
	
	/**
	 * @param actualControllerExitHddAcc
	 */
	public void setActualControllerExitHddAcc(String actualControllerExitHddAcc) {
		this.actualControllerExitHddAcc = actualControllerExitHddAcc;
	}
	
    /**
     * @return actualControllerExitHddAcc
     */
	public String getActualControllerExitHddAcc() {
		return this.actualControllerExitHddAcc;
	}
	
	/**
	 * @param majorChangeProductFlag
	 */
	public void setMajorChangeProductFlag(String majorChangeProductFlag) {
		this.majorChangeProductFlag = majorChangeProductFlag;
	}
	
    /**
     * @return majorChangeProductFlag
     */
	public String getMajorChangeProductFlag() {
		return this.majorChangeProductFlag;
	}
	
	/**
	 * @param majorChangeProductAcc
	 */
	public void setMajorChangeProductAcc(String majorChangeProductAcc) {
		this.majorChangeProductAcc = majorChangeProductAcc;
	}
	
    /**
     * @return majorChangeProductAcc
     */
	public String getMajorChangeProductAcc() {
		return this.majorChangeProductAcc;
	}
	
	/**
	 * @param accountSeizedFlag
	 */
	public void setAccountSeizedFlag(String accountSeizedFlag) {
		this.accountSeizedFlag = accountSeizedFlag;
	}
	
    /**
     * @return accountSeizedFlag
     */
	public String getAccountSeizedFlag() {
		return this.accountSeizedFlag;
	}
	
	/**
	 * @param accountSeizedAcc
	 */
	public void setAccountSeizedAcc(String accountSeizedAcc) {
		this.accountSeizedAcc = accountSeizedAcc;
	}
	
    /**
     * @return accountSeizedAcc
     */
	public String getAccountSeizedAcc() {
		return this.accountSeizedAcc;
	}
	
	/**
	 * @param unresolvedMattersFlag
	 */
	public void setUnresolvedMattersFlag(String unresolvedMattersFlag) {
		this.unresolvedMattersFlag = unresolvedMattersFlag;
	}
	
    /**
     * @return unresolvedMattersFlag
     */
	public String getUnresolvedMattersFlag() {
		return this.unresolvedMattersFlag;
	}
	
	/**
	 * @param unresolvedMattersAcc
	 */
	public void setUnresolvedMattersAcc(String unresolvedMattersAcc) {
		this.unresolvedMattersAcc = unresolvedMattersAcc;
	}
	
    /**
     * @return unresolvedMattersAcc
     */
	public String getUnresolvedMattersAcc() {
		return this.unresolvedMattersAcc;
	}
	
	/**
	 * @param generalComAssureName
	 */
	public void setGeneralComAssureName(String generalComAssureName) {
		this.generalComAssureName = generalComAssureName;
	}
	
    /**
     * @return generalComAssureName
     */
	public String getGeneralComAssureName() {
		return this.generalComAssureName;
	}
	
	/**
	 * @param guarDebitRela
	 */
	public void setGuarDebitRela(String guarDebitRela) {
		this.guarDebitRela = guarDebitRela;
	}
	
    /**
     * @return guarDebitRela
     */
	public String getGuarDebitRela() {
		return this.guarDebitRela;
	}
	
	/**
	 * @param conType
	 */
	public void setConType(String conType) {
		this.conType = conType;
	}
	
    /**
     * @return conType
     */
	public String getConType() {
		return this.conType;
	}
	
	/**
	 * @param generalComRecSecInvoiceSales
	 */
	public void setGeneralComRecSecInvoiceSales(java.math.BigDecimal generalComRecSecInvoiceSales) {
		this.generalComRecSecInvoiceSales = generalComRecSecInvoiceSales;
	}
	
    /**
     * @return generalComRecSecInvoiceSales
     */
	public java.math.BigDecimal getGeneralComRecSecInvoiceSales() {
		return this.generalComRecSecInvoiceSales;
	}
	
	/**
	 * @param generalComRecSecNetProfit
	 */
	public void setGeneralComRecSecNetProfit(java.math.BigDecimal generalComRecSecNetProfit) {
		this.generalComRecSecNetProfit = generalComRecSecNetProfit;
	}
	
    /**
     * @return generalComRecSecNetProfit
     */
	public java.math.BigDecimal getGeneralComRecSecNetProfit() {
		return this.generalComRecSecNetProfit;
	}
	
	/**
	 * @param generalComRecSecSelfRepSales
	 */
	public void setGeneralComRecSecSelfRepSales(java.math.BigDecimal generalComRecSecSelfRepSales) {
		this.generalComRecSecSelfRepSales = generalComRecSecSelfRepSales;
	}
	
    /**
     * @return generalComRecSecSelfRepSales
     */
	public java.math.BigDecimal getGeneralComRecSecSelfRepSales() {
		return this.generalComRecSecSelfRepSales;
	}
	
	/**
	 * @param generalComRecSecSelfRepProfit
	 */
	public void setGeneralComRecSecSelfRepProfit(java.math.BigDecimal generalComRecSecSelfRepProfit) {
		this.generalComRecSecSelfRepProfit = generalComRecSecSelfRepProfit;
	}
	
    /**
     * @return generalComRecSecSelfRepProfit
     */
	public java.math.BigDecimal getGeneralComRecSecSelfRepProfit() {
		return this.generalComRecSecSelfRepProfit;
	}
	
	/**
	 * @param generalComRecSecNetAsset
	 */
	public void setGeneralComRecSecNetAsset(String generalComRecSecNetAsset) {
		this.generalComRecSecNetAsset = generalComRecSecNetAsset;
	}
	
    /**
     * @return generalComRecSecNetAsset
     */
	public String getGeneralComRecSecNetAsset() {
		return this.generalComRecSecNetAsset;
	}
	
	/**
	 * @param generalComRecFirInvoiceSales
	 */
	public void setGeneralComRecFirInvoiceSales(java.math.BigDecimal generalComRecFirInvoiceSales) {
		this.generalComRecFirInvoiceSales = generalComRecFirInvoiceSales;
	}
	
    /**
     * @return generalComRecFirInvoiceSales
     */
	public java.math.BigDecimal getGeneralComRecFirInvoiceSales() {
		return this.generalComRecFirInvoiceSales;
	}
	
	/**
	 * @param generalComRecFirNetProfit
	 */
	public void setGeneralComRecFirNetProfit(java.math.BigDecimal generalComRecFirNetProfit) {
		this.generalComRecFirNetProfit = generalComRecFirNetProfit;
	}
	
    /**
     * @return generalComRecFirNetProfit
     */
	public java.math.BigDecimal getGeneralComRecFirNetProfit() {
		return this.generalComRecFirNetProfit;
	}
	
	/**
	 * @param generalComRecFirSelfRepSales
	 */
	public void setGeneralComRecFirSelfRepSales(java.math.BigDecimal generalComRecFirSelfRepSales) {
		this.generalComRecFirSelfRepSales = generalComRecFirSelfRepSales;
	}
	
    /**
     * @return generalComRecFirSelfRepSales
     */
	public java.math.BigDecimal getGeneralComRecFirSelfRepSales() {
		return this.generalComRecFirSelfRepSales;
	}
	
	/**
	 * @param generalComRecFirSelfRepProfit
	 */
	public void setGeneralComRecFirSelfRepProfit(java.math.BigDecimal generalComRecFirSelfRepProfit) {
		this.generalComRecFirSelfRepProfit = generalComRecFirSelfRepProfit;
	}
	
    /**
     * @return generalComRecFirSelfRepProfit
     */
	public java.math.BigDecimal getGeneralComRecFirSelfRepProfit() {
		return this.generalComRecFirSelfRepProfit;
	}
	
	/**
	 * @param generalComRecFirNetAsset
	 */
	public void setGeneralComRecFirNetAsset(String generalComRecFirNetAsset) {
		this.generalComRecFirNetAsset = generalComRecFirNetAsset;
	}
	
    /**
     * @return generalComRecFirNetAsset
     */
	public String getGeneralComRecFirNetAsset() {
		return this.generalComRecFirNetAsset;
	}
	
	/**
	 * @param generalComCurrentInvoiceSales
	 */
	public void setGeneralComCurrentInvoiceSales(java.math.BigDecimal generalComCurrentInvoiceSales) {
		this.generalComCurrentInvoiceSales = generalComCurrentInvoiceSales;
	}
	
    /**
     * @return generalComCurrentInvoiceSales
     */
	public java.math.BigDecimal getGeneralComCurrentInvoiceSales() {
		return this.generalComCurrentInvoiceSales;
	}
	
	/**
	 * @param generalComCurrentNetProfit
	 */
	public void setGeneralComCurrentNetProfit(java.math.BigDecimal generalComCurrentNetProfit) {
		this.generalComCurrentNetProfit = generalComCurrentNetProfit;
	}
	
    /**
     * @return generalComCurrentNetProfit
     */
	public java.math.BigDecimal getGeneralComCurrentNetProfit() {
		return this.generalComCurrentNetProfit;
	}
	
	/**
	 * @param generalComCurrentSelfRepSales
	 */
	public void setGeneralComCurrentSelfRepSales(java.math.BigDecimal generalComCurrentSelfRepSales) {
		this.generalComCurrentSelfRepSales = generalComCurrentSelfRepSales;
	}
	
    /**
     * @return generalComCurrentSelfRepSales
     */
	public java.math.BigDecimal getGeneralComCurrentSelfRepSales() {
		return this.generalComCurrentSelfRepSales;
	}
	
	/**
	 * @param generalComCurrentSelfRepProfit
	 */
	public void setGeneralComCurrentSelfRepProfit(java.math.BigDecimal generalComCurrentSelfRepProfit) {
		this.generalComCurrentSelfRepProfit = generalComCurrentSelfRepProfit;
	}
	
    /**
     * @return generalComCurrentSelfRepProfit
     */
	public java.math.BigDecimal getGeneralComCurrentSelfRepProfit() {
		return this.generalComCurrentSelfRepProfit;
	}
	
	/**
	 * @param generalComCurrentNetAsset
	 */
	public void setGeneralComCurrentNetAsset(String generalComCurrentNetAsset) {
		this.generalComCurrentNetAsset = generalComCurrentNetAsset;
	}
	
    /**
     * @return generalComCurrentNetAsset
     */
	public String getGeneralComCurrentNetAsset() {
		return this.generalComCurrentNetAsset;
	}
	
	/**
	 * @param otherBankLmt
	 */
	public void setOtherBankLmt(java.math.BigDecimal otherBankLmt) {
		this.otherBankLmt = otherBankLmt;
	}
	
    /**
     * @return otherBankLmt
     */
	public java.math.BigDecimal getOtherBankLmt() {
		return this.otherBankLmt;
	}
	
	/**
	 * @param ourBankLmt
	 */
	public void setOurBankLmt(java.math.BigDecimal ourBankLmt) {
		this.ourBankLmt = ourBankLmt;
	}
	
    /**
     * @return ourBankLmt
     */
	public java.math.BigDecimal getOurBankLmt() {
		return this.ourBankLmt;
	}
	
	/**
	 * @param limitPersonalLoansAmt
	 */
	public void setLimitPersonalLoansAmt(java.math.BigDecimal limitPersonalLoansAmt) {
		this.limitPersonalLoansAmt = limitPersonalLoansAmt;
	}
	
    /**
     * @return limitPersonalLoansAmt
     */
	public java.math.BigDecimal getLimitPersonalLoansAmt() {
		return this.limitPersonalLoansAmt;
	}
	
	/**
	 * @param normalGuarAmt
	 */
	public void setNormalGuarAmt(java.math.BigDecimal normalGuarAmt) {
		this.normalGuarAmt = normalGuarAmt;
	}
	
    /**
     * @return normalGuarAmt
     */
	public java.math.BigDecimal getNormalGuarAmt() {
		return this.normalGuarAmt;
	}
	
	/**
	 * @param focusGuarAmt
	 */
	public void setFocusGuarAmt(java.math.BigDecimal focusGuarAmt) {
		this.focusGuarAmt = focusGuarAmt;
	}
	
    /**
     * @return focusGuarAmt
     */
	public java.math.BigDecimal getFocusGuarAmt() {
		return this.focusGuarAmt;
	}
	
	/**
	 * @param badnessGuarAmt
	 */
	public void setBadnessGuarAmt(java.math.BigDecimal badnessGuarAmt) {
		this.badnessGuarAmt = badnessGuarAmt;
	}
	
    /**
     * @return badnessGuarAmt
     */
	public java.math.BigDecimal getBadnessGuarAmt() {
		return this.badnessGuarAmt;
	}
	
	/**
	 * @param guarAblEval
	 */
	public void setGuarAblEval(String guarAblEval) {
		this.guarAblEval = guarAblEval;
	}
	
    /**
     * @return guarAblEval
     */
	public String getGuarAblEval() {
		return this.guarAblEval;
	}
	
	/**
	 * @param generalComAssureAcc
	 */
	public void setGeneralComAssureAcc(String generalComAssureAcc) {
		this.generalComAssureAcc = generalComAssureAcc;
	}
	
    /**
     * @return generalComAssureAcc
     */
	public String getGeneralComAssureAcc() {
		return this.generalComAssureAcc;
	}
	
	/**
	 * @param guarAssureAcc
	 */
	public void setGuarAssureAcc(String guarAssureAcc) {
		this.guarAssureAcc = guarAssureAcc;
	}
	
    /**
     * @return guarAssureAcc
     */
	public String getGuarAssureAcc() {
		return this.guarAssureAcc;
	}
	
	/**
	 * @param guarComName
	 */
	public void setGuarComName(String guarComName) {
		this.guarComName = guarComName;
	}
	
    /**
     * @return guarComName
     */
	public String getGuarComName() {
		return this.guarComName;
	}
	
	/**
	 * @param sameSecondClass
	 */
	public void setSameSecondClass(String sameSecondClass) {
		this.sameSecondClass = sameSecondClass;
	}
	
    /**
     * @return sameSecondClass
     */
	public String getSameSecondClass() {
		return this.sameSecondClass;
	}
	
	/**
	 * @param guarComAcc
	 */
	public void setGuarComAcc(String guarComAcc) {
		this.guarComAcc = guarComAcc;
	}
	
    /**
     * @return guarComAcc
     */
	public String getGuarComAcc() {
		return this.guarComAcc;
	}
	
	/**
	 * @param otherGuarAcc
	 */
	public void setOtherGuarAcc(String otherGuarAcc) {
		this.otherGuarAcc = otherGuarAcc;
	}
	
    /**
     * @return otherGuarAcc
     */
	public String getOtherGuarAcc() {
		return this.otherGuarAcc;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly;
	}
	
    /**
     * @return inteAnaly
     */
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param controlOpinion
	 */
	public void setControlOpinion(String controlOpinion) {
		this.controlOpinion = controlOpinion;
	}
	
    /**
     * @return controlOpinion
     */
	public String getControlOpinion() {
		return this.controlOpinion;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc;
	}
	
    /**
     * @return restDesc
     */
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}