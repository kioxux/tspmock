package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.AreaAdminUser;
import cn.com.yusys.yusp.domain.AreaOrg;
import cn.com.yusys.yusp.domain.AreaUser;
import cn.com.yusys.yusp.domain.FbManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RedisAuthCacheService {
    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;

    @Autowired
    private AreaUserService areaUserService;

    @Autowired
    private AreaOrgService areaOrgService;

    @Autowired
    private AreaAdminUserService areaAdminUserService;

    @Autowired
    private FbManagerService fbManagerService;

    /**
     * 刷新redis缓存
     */
    @Async
    public void refershCache() {
        // 小微特殊数据权限---负责所有机构下的所有小微客户经理
        Map<String, List<String>> dataAuthUserMap = this.loadXwAreaManagerDataAuthWithUser();
        if (CollectionUtils.nonEmpty(dataAuthUserMap)) {
            Set<String> userCodes = dataAuthUserMap.keySet();
            for (String userCode : userCodes) {
                if (dataAuthUserMap.containsKey(userCode)) {
                    List<String> xwUserCodesList = dataAuthUserMap.get(userCode);
                    if (CollectionUtils.nonEmpty(xwUserCodesList)) {
                        yuspRedisTemplate.hset(CommonConstance.REDIS_KEY_AREA_XW_USER, userCode, xwUserCodesList);
                    }
                }
            }
        }

        // 小微特殊数据权限---负责所有机构下的所有机构
        Map<String, List<String>> dataAuthOrgMap = this.loadXwAreaManagerDataAuthWithOrg();
        if (CollectionUtils.nonEmpty(dataAuthOrgMap)) {
            Set<String> userCodes = dataAuthOrgMap.keySet();
            for (String userCode : userCodes) {
                if (dataAuthOrgMap.containsKey(userCode)) {
                    List<String> xwOrgCodesList = dataAuthOrgMap.get(userCode);
                    if (CollectionUtils.nonEmpty(xwOrgCodesList)) {
                        yuspRedisTemplate.hset(CommonConstance.REDIS_KEY_AREA_XW_ORG, userCode, xwOrgCodesList);
                    }
                }
            }
        }
    }

    /**
     * 获取所有小微区域管理人员（分中心负责人、分部部长）下的小微客户经理
     *
     * @return
     */
    public Map<String, List<String>> loadXwAreaManagerDataAuthWithUser() {
        Map<String, List<String>> map = new HashMap<>();
        // 1、获取所有分中心负责人数据
        List<AreaUser> areaUsers = areaUserService.selectAll(new QueryModel());
        for (AreaUser areaUser : areaUsers) {
            List<String> tempList = null;
            if (map.containsKey(areaUser.getUserNo())) {
                tempList = map.get(areaUser.getUserNo());
            } else {
                tempList = new ArrayList<>();
                map.put(areaUser.getUserNo(), tempList);
            }
            // 根据区域查询下属机构信息
            List<AreaOrg> areaOrgList = areaOrgService.selectByAreaNo(areaUser.getAreaNo());
            // 根据机构信息遍历机构下用户信息，一个区域可能关联多个机构
            // 根据机构信息查询用户信息
            List<AreaAdminUser> areaAdminUserList = areaAdminUserService.selectByOrgCodes(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            List<String> temp2List = areaAdminUserList.stream().map(AreaAdminUser::getUserNo).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(temp2List)) {
                tempList.addAll(temp2List);
            }
        }
        // 2、获取分部部长数据
        List<FbManager> fbList = fbManagerService.selectAll(new QueryModel());
        for (FbManager fbManager : fbList) {
            String bzUserCode = fbManager.getMinisterId();
            List<String> tempList = null;
            if (map.containsKey(bzUserCode)) {
                tempList = map.get(bzUserCode);
            } else {
                tempList = new ArrayList<>();
                map.put(bzUserCode, tempList);
            }
            // 获取分部对应的所管辖所有机构
            List<AreaOrg> areaOrgList = areaOrgService.selectByFbCode(fbManager.getFbCode());
            // 根据机构信息查询用户信息
            List<AreaAdminUser> areaAdminUserList = areaAdminUserService.selectByOrgCodes(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            List<String> temp2List = areaAdminUserList.stream().map(AreaAdminUser::getUserNo).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(temp2List)) {
                tempList.addAll(temp2List);
            }
        }
        return map;
    }

    /**
     * 获取所有小微区域管理人员（分中心负责人、分部部长）下的机构
     *
     * @return
     */
    public Map<String, List<String>> loadXwAreaManagerDataAuthWithOrg() {
        Map<String, List<String>> map = new HashMap<>();
        // 1、获取所有分中心负责人数据
        List<AreaUser> areaUsers = areaUserService.selectAll(new QueryModel());
        for (AreaUser areaUser : areaUsers) {
            List<String> tempList = null;
            if (map.containsKey(areaUser.getUserNo())) {
                tempList = map.get(areaUser.getUserNo());
            } else {
                tempList = new ArrayList<>();
                map.put(areaUser.getUserNo(), tempList);
            }
            // 根据区域查询下属机构信息
            List<AreaOrg> areaOrgList = areaOrgService.selectByAreaNo(areaUser.getAreaNo());
            if (CollectionUtils.nonEmpty(areaOrgList)) {
                tempList.addAll(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            }
        }
        // 2、获取分部部长数据
        List<FbManager> fbList = fbManagerService.selectAll(new QueryModel());
        for (FbManager fbManager : fbList) {
            String bzUserCode = fbManager.getMinisterId();
            List<String> tempList = null;
            if (map.containsKey(bzUserCode)) {
                tempList = map.get(bzUserCode);
            } else {
                tempList = new ArrayList<>();
                map.put(bzUserCode, tempList);
            }
            // 获取分部对应的所管辖所有机构
            List<AreaOrg> areaOrgList = areaOrgService.selectByFbCode(fbManager.getFbCode());
            if (CollectionUtils.nonEmpty(areaOrgList)) {
                tempList.addAll(areaOrgList.stream().map(AreaOrg::getOrgNo).collect(Collectors.toList()));
            }
        }
        return map;
    }
}
