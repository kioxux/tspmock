package cn.com.yusys.yusp.web.server.xdtz0054;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0054.req.Xdtz0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.Xdtz0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0054.Xdtz0054Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:将需要修改的受托支付账号生成修改记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0054:将需要修改的受托支付账号生成修改记录")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0054Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0054Resource.class);

    @Autowired
    private Xdtz0054Service xdtz0054Service;
    /**
     * 交易码：xdtz0054
     * 交易描述：将需要修改的受托支付账号生成修改记录
     *
     * @param xdtz0054DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("将需要修改的受托支付账号生成修改记录")
    @PostMapping("/xdtz0054")
    protected @ResponseBody
    ResultDto<Xdtz0054DataRespDto> xdtz0054(@Validated @RequestBody Xdtz0054DataReqDto xdtz0054DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataReqDto));
        Xdtz0054DataRespDto xdtz0054DataRespDto = new Xdtz0054DataRespDto();// 响应Dto:将需要修改的受托支付账号生成修改记录
        ResultDto<Xdtz0054DataRespDto> xdtz0054DataResultDto = new ResultDto<>();
        String serno = xdtz0054DataReqDto.getSerno();//流水号
        String billNo = xdtz0054DataReqDto.getBillNo();//借据号
        String toppAcctNo = xdtz0054DataReqDto.getToppAcctNo();//交易对手账号
        String toppName = xdtz0054DataReqDto.getToppName();//交易对手名称
        BigDecimal toppAmt = xdtz0054DataReqDto.getToppAmt();//交易对手金额
        try {
            // 从xdtz0054DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataReqDto));
            xdtz0054DataRespDto = xdtz0054Service.xdtz0054(xdtz0054DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataRespDto));
            // 封装xdtz0054DataResultDto中正确的返回码和返回信息
            xdtz0054DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0054DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, e.getMessage());
            // 封装xdtz0054DataResultDto中异常返回码和返回信息
            xdtz0054DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0054DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0054DataRespDto到xdtz0054DataResultDto中
        xdtz0054DataResultDto.setData(xdtz0054DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataResultDto));
        return xdtz0054DataResultDto;
    }
}
