package cn.com.yusys.yusp.service.server.xdht0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgLprRateDto;
import cn.com.yusys.yusp.dto.server.xdht0024.req.Xdht0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0007.CmisCus0007Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * 接口处理类:合同详情查看
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdht0024Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0024Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CmisCus0007Service cmisCus0007Service;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * 合同详情查看
     *
     * @param xdht0024DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public ResultDto<Xdht0024DataRespDto> getContDetail(Xdht0024DataReqDto xdht0024DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataReqDto));
        ResultDto<Xdht0024DataRespDto> xdht0024DataResultDto = new ResultDto<>();
        Map queryMap = new HashMap();
        String biz_type = xdht0024DataReqDto.getBiz_type();//贷款类型
        String certid = xdht0024DataReqDto.getCertid();
        String cont_state = xdht0024DataReqDto.getCont_state();
        String cusId = StringUtils.EMPTY;
        Xdht0024DataRespDto result = new Xdht0024DataRespDto();//返回信息
        try {
            /**********************第一步：必输字段非空校验*************************/
            if (StringUtils.isEmpty(certid)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "身份证号不能为空！");
            } else if (StringUtils.isEmpty(biz_type)) {
                throw BizException.error(null, EpbEnum.EPB099999.key, "业务类型不能为空！");
            }
            logger.info("根据客户证件号【{}】查询客户基本信息开始", certid);
            List<String> cusIds = commonService.getCusBaseByCertCode(certid);
            logger.info("根据客户证件号【{}】查询客户基本信息开始结束,查询结果信息【{}】", certid, JSON.toJSONString(cusIds));
            if (CollectionUtils.nonEmpty(cusIds)) {
                cusId = cusIds.get(0);
            }
            if (StringUtils.isEmpty(cusId)) {//查不到客户信息
                throw BizException.error(null, EpbEnum.EPB099999.key, "信贷根据身份证号查询不到客户信息！");
            }
            /************************第二步：拼接合同表查询条件*********************************/
            queryMap.put("contStatus", cont_state);//合同状态
            queryMap.put("cusId", cusId);//客户号
            if ("1".equals(biz_type)) {//优享贷
                queryMap.put("bizType", CmisBizConstants.CODE_YXD);
                queryMap.put("flag", "0");
            }
            if ("2".equals(biz_type)) {//增享贷
                queryMap.put("bizType", CmisBizConstants.CODE_ZXD);
                queryMap.put("flag", "1");
            }
            /************************第四步：查询贷款合同表信息*********************************/
            logger.info("****************根据查询条件【{}】查询贷款合同表信息开始", JSON.toJSONString(queryMap));
            result = Optional.ofNullable(ctrLoanContMapper.getContDetail(queryMap)).orElse(new Xdht0024DataRespDto());
            logger.info("****************根据查询条件【{}】查询贷款合同表信息结束,查询结果信息【{}】", JSON.toJSONString(queryMap), JSON.toJSONString(result));

            /************************第五步：存在合同信息,继续校验*********************************/
            String contNo = result.getCont_no();//获取合同号
            if (StringUtils.isNotBlank(contNo)) {
                logger.info("********************************计算日息开始*************************************");
                BigDecimal apply_amount = Optional.ofNullable(result.getApply_amount()).orElse(BigDecimal.ZERO);//申请金额
                BigDecimal reality_ir_y = Optional.ofNullable(result.getReality_ir_y()).orElse(BigDecimal.ZERO);//执行年利率
                BigDecimal avg_rate = apply_amount.multiply(reality_ir_y).divide(new BigDecimal(CmisBizConstants.NUM_360), CmisBizConstants.INT_TWO, BigDecimal.ROUND_HALF_UP);
                result.setAvg_rate(avg_rate);//日息
                logger.info("********************************合同起始日到期日转换(网银只要8位)*************************************");
                String loanStartDate = result.getLoan_start_date();
                String loanEndDate = result.getLoan_end_date();
                if (StringUtils.isEmpty(loanStartDate) && StringUtils.isEmpty(loanEndDate)) {//日期非空，做replaceAll操作
                    result.setLoan_end_date(result.getLoan_end_date().replaceAll("-", ""));
                    result.setLoan_start_date(result.getLoan_start_date().replaceAll("-", ""));
                }

                logger.info("************如果是增享贷，关联的批复超过30天未签约的合同不能作为有效合同进行签约***************");
                if ("2".equals(biz_type) && "100".equals(cont_state)) {
                    //增享贷起始日期取当前营业日
                    String openday = Optional.ofNullable(stringRedisTemplate.opsForValue().get("openDay")).orElse(LocalDate.now().toString()).replaceAll("-", "");//当前日期
                    result.setLoan_start_date(openday);
                    //获取批复流水号
                    String surveySerno = Optional.ofNullable(ctrLoanContMapper.getSurveySerno(queryMap)).orElse(StringUtils.EMPTY);
                    LmtCrdReplyInfo lmtCrdReplyInfo = Optional.ofNullable(lmtCrdReplyInfoMapper.selectBySurveySerno(surveySerno)).orElse(new LmtCrdReplyInfo());
                    String replyStartDate = lmtCrdReplyInfo.getReplyStartDate();//批复起始日
                    if (StringUtils.isBlank(replyStartDate)) {//起始日期不能为空
                        xdht0024DataResultDto.setCode(EpbEnum.EPB099999.key);
                        xdht0024DataResultDto.setMessage("增享贷批复时间异常!");
                        return xdht0024DataResultDto;
                    } else {
                        replyStartDate = replyStartDate.substring(0, 10);
                    }
                    logger.info("****************根据批复起始日【{}】校验关联的批复是否超过30天未签约开始", replyStartDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.add(Calendar.DAY_OF_MONTH, -30);//30天前
                    String day_before_30 = sdf.format(cal.getTime());
                    if (day_before_30.compareTo(replyStartDate) > 0) {
                        xdht0024DataResultDto.setCode(EpbEnum.EPB099999.key);
                        xdht0024DataResultDto.setMessage("关联的批复超过30天未签约或无有效未签约合同!");
                        return xdht0024DataResultDto;
                    }
                } else if ("1".equals(biz_type)) {//如果是优享贷
                    logger.info("****************根据合同号【{}】查询优享贷的可用余额开始", contNo);
                    String contlmt = ctrLoanContMapper.queryZxdCtrLoanBalanceByContType(contNo);
                    if (StringUtils.isNotEmpty(contlmt)) {//查询结果不为空
                        result.setLmt(new BigDecimal(contlmt));//赋值
                    }
                }
                //获取客户经理姓名
                String managerIdName = result.getManagerIdName();
                if (StringUtil.isNotEmpty(managerIdName)) {
                    // 根据工号获取用户表中的联系电话
                    logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerIdName);
                    logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        managerIdName = adminSmUserDto.getUserName();
                        result.setManagerIdName(managerIdName);
                    }
                }

                String valiDate = queryLprRate("A1");//跟郑磊确认优享增享最高签3年，故传A1
                result.setValiDate(valiDate);
                logger.info("*************************获取lprBp点数开始***********************执行利率:" + result.getReality_ir_y());
                BigDecimal rate = result.getReality_ir_y();
                String curLprRate = (String) iqpLoanAppService.getLprRate("A1").get("rate");
                BigDecimal lprBp = rate.subtract(new BigDecimal(curLprRate)).multiply(new BigDecimal("10000"));
                result.setLpr_bp(lprBp);
                logger.info("*************************获取lprBp点数结束***********************");
            } else {//查询不到合同信息
                logger.info("****************************未查到对应的的合同***********************");
                result.setLoan_start_date(StringUtils.EMPTY);
                result.setAvg_rate(new BigDecimal(0L));
                result.setDue_day(StringUtils.EMPTY);
                result.setLoan_end_date(StringUtils.EMPTY);
                result.setApply_amount(new BigDecimal(0L));
                result.setApply_cur_type(StringUtils.EMPTY);
                result.setCncont_no(StringUtils.EMPTY);
                result.setCont_no(StringUtils.EMPTY);
                result.setLmt(new BigDecimal(0L));
                result.setLpr_base_rate(new BigDecimal(0L));
                result.setLpr_bp(new BigDecimal(0L));
                result.setLpr_bp_type(StringUtils.EMPTY);
                result.setLpr_term(StringUtils.EMPTY);
                result.setReality_ir_y(new BigDecimal(0L));
                xdht0024DataResultDto.setData(result);
                xdht0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdht0024DataResultDto.setMessage("未查到对应的的合同！");
                return xdht0024DataResultDto;
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, e.getMessage());
            xdht0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
            return xdht0024DataResultDto;
        }
        xdht0024DataResultDto.setData(result);
        xdht0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
        xdht0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataResultDto));
        return xdht0024DataResultDto;
    }

    /********
     *根据贷款期限查询lpr利率生效日期
     * *********/
    public String queryLprRate(String newVal) {
        //查询下lpr利率
        String valiDate = "";
        try {
            logger.info("根据贷款期限查询lpr利率生效日期开始{}", newVal);
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("rateTypeId", newVal);//优享贷固定一年期
            queryModel.getCondition().put("rateTypeName", "10");
            ResultDto<CfgLprRateDto> resultDto = iCmisCfgClientService.selectone(queryModel);
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                valiDate = resultDto.getData().getValidDate();//生效日期
            }
            logger.info("根据贷款期限查询lpr利率生效日期结束{}", valiDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valiDate;
    }
}
