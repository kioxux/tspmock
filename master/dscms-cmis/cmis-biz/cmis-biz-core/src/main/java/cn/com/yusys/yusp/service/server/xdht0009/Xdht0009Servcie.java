package cn.com.yusys.yusp.service.server.xdht0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.dto.server.xdht0009.req.Xdht0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.ContList;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.Xdht0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.service.AccAccpService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.CtrAccpContService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0009Servcie
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-06 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0009Servcie {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0009Servcie.class);

    @Resource
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AccAccpService accAccpService;

    /**
     * 银承合同信息列表查询
     *
     * @param
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0009DataRespDto getXdht0009(Xdht0009DataReqDto xdht0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataReqDto));
        Xdht0009DataRespDto xdht0009DataRespDto = new Xdht0009DataRespDto();
        try {
            //客户名称
            String cusId = xdht0009DataReqDto.getCusId();
            List<ContList> list = ctrAccpContMapper.getContInfoByCusId(cusId);
            if(CollectionUtils.isEmpty(list)||list.size()<1){
                throw BizException.error(null, EcbEnum.ECB010058.key,EcbEnum.ECB010058.value);
            }
            for (ContList conList: list) {
                if (Objects.nonNull(conList.getBailRate())) {
                    conList.setBailRate(conList.getBailRate().stripTrailingZeros());
                } else {
                    conList.setBailRate(BigDecimal.ZERO);
                }

                if (Objects.nonNull(conList.getChrgRate())) {
                    conList.setChrgRate(conList.getChrgRate().stripTrailingZeros());
                } else {
                    conList.setChrgRate(BigDecimal.ZERO);
                }
                BigDecimal accDrftAmt = Optional.ofNullable(accAccpService.selectSumAmtByContNo(conList.getContNo())).orElse(BigDecimal.ZERO);
                conList.setContAmt(conList.getContAmt().subtract(accDrftAmt));
                String managerId = conList.getManagerId();
                AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(managerId);
                conList.setManagerName(adminSmUserDto.getUserName());
            }
            xdht0009DataRespDto.setContList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataRespDto));
        return xdht0009DataRespDto;
    }
}
