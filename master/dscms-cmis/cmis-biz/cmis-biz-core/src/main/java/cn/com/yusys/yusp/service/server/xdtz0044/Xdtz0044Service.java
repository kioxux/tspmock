package cn.com.yusys.yusp.service.server.xdtz0044;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0044Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0044Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 根据客户号前往信贷查找房贷借据信息
     * @param cusId
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0044DataRespDto getHouseLoanContByCusId(String cusId) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, cusId);
        Xdtz0044DataRespDto xdtz0044DataRespDto = ctrLoanContMapper.getHouseLoanContByCusId(cusId);
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataRespDto));
        return ctrLoanContMapper.getHouseLoanContByCusId(cusId);
    }
}
