/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarSpeCorp;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarSpeCorpService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarSpeCorpResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:40:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarspecorp")
public class RptLmtRepayAnysGuarSpeCorpResource {
    @Autowired
    private RptLmtRepayAnysGuarSpeCorpService rptLmtRepayAnysGuarSpeCorpService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarSpeCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarSpeCorp> list = rptLmtRepayAnysGuarSpeCorpService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarSpeCorp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarSpeCorp>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarSpeCorp> list = rptLmtRepayAnysGuarSpeCorpService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarSpeCorp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptLmtRepayAnysGuarSpeCorp> show(@PathVariable("pkId") String pkId) {
        RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp = rptLmtRepayAnysGuarSpeCorpService.selectByPrimaryKey(pkId);
        return new ResultDto<RptLmtRepayAnysGuarSpeCorp>(rptLmtRepayAnysGuarSpeCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarSpeCorp> create(@RequestBody RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp) throws URISyntaxException {
        rptLmtRepayAnysGuarSpeCorpService.insert(rptLmtRepayAnysGuarSpeCorp);
        return new ResultDto<RptLmtRepayAnysGuarSpeCorp>(rptLmtRepayAnysGuarSpeCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarSpeCorpService.update(rptLmtRepayAnysGuarSpeCorp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptLmtRepayAnysGuarSpeCorpService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptLmtRepayAnysGuarSpeCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询
     * @param map RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp
     * @return
     */
    @ApiOperation("根据流水号查看担保人为专业担保公司的信息")
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptLmtRepayAnysGuarSpeCorp>> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        QueryModel model = new QueryModel();
        model.addCondition("serno",serno);
        return new ResultDto<List<RptLmtRepayAnysGuarSpeCorp>>(rptLmtRepayAnysGuarSpeCorpService.selectByModel(model));
    }

    /**
     * 保存信息
     * @param rptLmtRepayAnysGuarSpeCorp
     * @return
     */
    @PostMapping("/saveGuarSpecorp")
    protected ResultDto<Integer> saveGuarSpecorp(@RequestBody RptLmtRepayAnysGuarSpeCorp rptLmtRepayAnysGuarSpeCorp){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarSpeCorpService.saveGuarSpecorp(rptLmtRepayAnysGuarSpeCorp));
    }

    @PostMapping("/initGuarSpecorp")
    protected ResultDto<RptLmtRepayAnysGuarSpeCorp> initGuarSpecorp(@RequestBody Map map){
        return new ResultDto<RptLmtRepayAnysGuarSpeCorp>(rptLmtRepayAnysGuarSpeCorpService.initGuarSpecorp(map));
    }
}
