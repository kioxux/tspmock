/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AsplWhtls;
import cn.com.yusys.yusp.domain.AsplWhtlsOutPortModel;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.AsplWhtlsService;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: AsplWhtlsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 09:24:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/asplwhtls")
public class AsplWhtlsResource {
    private static final Logger log = LoggerFactory.getLogger(AsplWhtlsResource.class);
    @Autowired
    private AsplWhtlsService asplWhtlsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AsplWhtls>> query() {
        QueryModel queryModel = new QueryModel();
        List<AsplWhtls> list = asplWhtlsService.selectAll(queryModel);
        return new ResultDto<List<AsplWhtls>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AsplWhtls>> index(QueryModel queryModel) {
        List<AsplWhtls> list = asplWhtlsService.selectByModel(queryModel);
        return new ResultDto<List<AsplWhtls>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AsplWhtls> show(@PathVariable("pkId") String pkId) {
        AsplWhtls asplWhtls = asplWhtlsService.selectByPrimaryKey(pkId);
        return new ResultDto<AsplWhtls>(asplWhtls);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AsplWhtls> create(@RequestBody AsplWhtls asplWhtls) throws URISyntaxException {
        asplWhtlsService.insertSelective(asplWhtls);
        return new ResultDto<AsplWhtls>(asplWhtls);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AsplWhtls asplWhtls) throws URISyntaxException {
        int result = asplWhtlsService.updateSelective(asplWhtls);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = asplWhtlsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = asplWhtlsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryAsplWhtls
     * @函数描述:重写 查询对象列表方法
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryasplwhtls")
    protected ResultDto<List<AsplWhtls>> queryAsplWhtls(@RequestBody QueryModel queryModel) {
        List<AsplWhtls> list = asplWhtlsService.selectByModel(queryModel);
        return new ResultDto<List<AsplWhtls>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/showdetail")
    protected ResultDto<AsplWhtls> showDetail(@RequestBody String serno) {
        HashMap map = new HashMap();
        map.put("serno",serno);
        map.put("oprType", CmisCommonConstants.ADD_OPR);
        AsplWhtls asplWhtls = asplWhtlsService.selectByParam(map);
        return new ResultDto<AsplWhtls>(asplWhtls);
    }

    /**
     * 异步导出承兑行名单配置
     */
    @PostMapping("/exportasplwhtls")
    public ResultDto<ProgressDto> exportAsplWhtls(@RequestBody AsplWhtls asplWhtls) {
        ProgressDto progressDto = asplWhtlsService.exportAsplWhtls(asplWhtls);
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importasplwhtls")
    public ResultDto<ProgressDto> importAsplWhtls(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(String.valueOf(EcbEnum.E_IMPORTEXCEL_EXCEPTION_02),e);
            throw new YuspException(EcbEnum.E_IMPORTEXCEL_EXCEPTION_02.key,EcbEnum.E_IMPORTEXCEL_EXCEPTION_02.value);
        }

        // 将文件内容导入数据库，StudentScore为导入数据的类
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(AsplWhtlsOutPortModel.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(asplWhtlsService::importAsplWhtls)));
        log.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }

    /**
     *异步下载资产池承兑行名单模板
     */
    @PostMapping("/exportasplwhtlsmodel")
    protected ResultDto<ProgressDto> exportAsplWhtlsModel() {
        ProgressDto progressDto = asplWhtlsService.exportAsplWhtlsModel();
        return ResultDto.success(progressDto);
    }

}
