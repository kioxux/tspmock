package cn.com.yusys.yusp.service.server.xdtz0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccTfLoc;
import cn.com.yusys.yusp.domain.CtrTfLocCont;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.req.Xdtz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.resp.Xdtz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccCvrsMapper;
import cn.com.yusys.yusp.repository.mapper.AccTfLocMapper;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrTfLocContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CmisPspClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class Xdtz0019Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0019Service.class);

    @Autowired
    private AccTfLocMapper accTfLocMapper;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private AccCvrsMapper accCvrsMapper;

    @Autowired
    private CtrTfLocContMapper ctrTfLocContMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    @Autowired
    private CmisPspClientService cmisPspClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    /**
     * 交易码：Xdtz0019
     * 交易描述  台账入账
     *
     * @param xdtz0019DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0019DataRespDto xdtz0019(Xdtz0019DataReqDto xdtz0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019,
                DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019DataReqDto));
        //定义返回信息
        Xdtz0019DataRespDto xdtz0019DataRespDto = new Xdtz0019DataRespDto();
        //得到请求字段
        String oprtype = xdtz0019DataReqDto.getOprtype();//操作类型
        String billNo = xdtz0019DataReqDto.getBillNo();//借据号
        String contNo = xdtz0019DataReqDto.getContNo();//合同编号
        String bizVariet = xdtz0019DataReqDto.getBizVariet();//业务品种
        String cusId = xdtz0019DataReqDto.getCusId();//客户号
        String cusName = xdtz0019DataReqDto.getCusName();//客户名称
        String creditNo = xdtz0019DataReqDto.getCreditNo();//信用证号码
        String issuingCurType = xdtz0019DataReqDto.getIssuingCurType();//开证币种
        String issuingContAmt = xdtz0019DataReqDto.getIssuingContAmt();//开证合同金额
        String overdueDays = xdtz0019DataReqDto.getOverdueDays();//逾期天数
        String floodactPerc = xdtz0019DataReqDto.getFloodactPerc();//溢装比例
        String exchgRate = xdtz0019DataReqDto.getExchgRate();//汇率
        String accStatus = xdtz0019DataReqDto.getAccStatus();//台账状态
        String billAmt = xdtz0019DataReqDto.getBillAmt();
        String finaBrId = xdtz0019DataReqDto.getFinaBrId();//账务机构
        Map param = new HashMap<>();
        if (!Objects.isNull(bizVariet)) {
            param.put("prdId", bizVariet);
            if (Objects.equals(bizVariet, "412265")) {
                // 国际信用证开立
                param.put("bizType", "11010101");
            } else if (Objects.equals(bizVariet, "410002")) {
                // 国内信用证开立
                param.put("bizType", "11020101");
            }
        }
        param.put("contNo", contNo);
        //业务处理开始
        try {
            if ("01".equals(oprtype)) {//入账
                //判断传过来的借据号是否存在
                int i = accTfLocMapper.countAccTfLocCountByBillNo(billNo);
                if (i > 0) {
                    xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0019DataRespDto.setOpMsg("借据信息已存在！");
                    return xdtz0019DataRespDto;
                } else {
                    //根据合同号查找相关信息
                    List<CtrTfLocCont> ctrtflocList = ctrTfLocContMapper.getContInfoByParam(param);
                    if (CollectionUtils.nonEmpty(ctrtflocList)) {
                        for (CtrTfLocCont ctrTfLocCont : ctrtflocList) {
                            AccTfLoc accTfLoc = new AccTfLoc();
                            String pkId = UUID.randomUUID().toString();
                            accTfLoc.setBillNo(billNo);//借据号
                            accTfLoc.setPkId(pkId);
                            accTfLoc.setContNo(ctrTfLocCont.getContNo());//合同号
                            accTfLoc.setPrdId(xdtz0019DataReqDto.getPrdId());//产品编号
                            accTfLoc.setPrdName(xdtz0019DataReqDto.getPrdName());//产品名称
                            accTfLoc.setCusId(cusId);//客户号
                            accTfLoc.setCusName(cusName);//客户名称
                            accTfLoc.setPvpSerno(xdtz0019DataReqDto.getPvpSerno());//交易编号
                            accTfLoc.setCreditNo(creditNo);//证件号码
                            accTfLoc.setCurType(issuingCurType);//币种
                            accTfLoc.setOcerAmt(new BigDecimal(issuingContAmt));//开证金额
                            accTfLoc.setFinaBrId(finaBrId);//账务机构
                            QueryModel queryModel = new QueryModel();
                            queryModel.addCondition("finaBrNo", accTfLoc.getFinaBrId());
                            logger.info("根据机构编号获取机构名称开始，参数为：{}", accTfLoc.getFinaBrId());
                            ResultDto<List<CfgSorgFinaDto>> listResultDto = iCmisCfgClientService.selecSorgFina(queryModel);
                            logger.info("根据机构编号获取机构名称结束，返回为：{}", JSON.toJSONString(listResultDto));
                            if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode())
                                    && CollectionUtils.nonEmpty(listResultDto.getData())) {
                                CfgSorgFinaDto cfgSorgFinaDto = JSONObject.parseObject(JSON.toJSONString(listResultDto.getData().get(0)), CfgSorgFinaDto.class);
                                accTfLoc.setFinaBrIdName(cfgSorgFinaDto.getFinaBrName());
                            }
                            accTfLoc.setCvtCnyAmt(new BigDecimal(issuingContAmt).multiply(new BigDecimal(exchgRate)));//折算人民币金额
                            if (Objects.equals("21", ctrTfLocCont.getGuarMode()) || Objects.equals("60", ctrTfLocCont.getGuarMode())) {
                                //低风险为0
                                accTfLoc.setOrigiOpenAmt(BigDecimal.ZERO);//原始敞口金额
                                accTfLoc.setSpacBal(BigDecimal.ZERO);//敞口余额
                                accTfLoc.setCvtCnySpac(BigDecimal.ZERO);//折算人民币敞口
                            } else {
                                BigDecimal spac = new BigDecimal(billAmt);//开证金额
                                BigDecimal spacCny = spac.multiply(StringUtils.isBlank(exchgRate) ? new BigDecimal("1") :
                                        new BigDecimal(exchgRate)).setScale(2, BigDecimal.ROUND_HALF_UP);//开证折算人民币金额
                                BigDecimal bailAmt = new BigDecimal(StringUtils.isBlank(xdtz0019DataReqDto.getBailAmt()) ? "0" : xdtz0019DataReqDto.getBailAmt());//保证金金额(人民币)
                                BigDecimal origiOpenAmt = spac.subtract(bailAmt.divide(StringUtils.isBlank(exchgRate) ? new BigDecimal("1") :
                                        new BigDecimal(exchgRate), 2, BigDecimal.ROUND_HALF_UP));//原始敞口金额
                                BigDecimal cvtCnySpac = spacCny.subtract(bailAmt);//折算人民币敞口
                                accTfLoc.setOrigiOpenAmt(origiOpenAmt);//原始敞口金额
                                accTfLoc.setSpacBal(origiOpenAmt);//敞口余额
                                accTfLoc.setCvtCnySpac(cvtCnySpac.compareTo(BigDecimal.ZERO) > 0 ? cvtCnySpac : BigDecimal.ZERO);//折算人民币敞口
                            }
                            accTfLoc.setCreditBal(new BigDecimal(billAmt));//信用证余额
                            accTfLoc.setFastDay(overdueDays);//远期天数
                            accTfLoc.setFloodactPerc(new BigDecimal(floodactPerc));//溢装比例
                            accTfLoc.setExchgRate(new BigDecimal(exchgRate));//汇率
                            accTfLoc.setGuarMode(ctrTfLocCont.getGuarMode());//担保方式
                            accTfLoc.setBailPerc(ctrTfLocCont.getBailPerc());//保证金比例
                            accTfLoc.setStartDate(xdtz0019DataReqDto.getStartDate());//开始日期
                            accTfLoc.setEndDate(xdtz0019DataReqDto.getEndDate());//结束日期
                            accTfLoc.setAccStatus("1");//台账默认生效
                            String openDay = stringRedisTemplate.opsForValue().get("openDay");
                            accTfLoc.setInputDate(openDay);//登记日期
                            accTfLoc.setUpdDate(openDay);//修改日期
                            Date date = new Date();
                            accTfLoc.setCreateTime(date);//创建时间
                            accTfLoc.setUpdateTime(date);//更新时间
                            accTfLoc.setInputId(ctrTfLocCont.getInputId());//录入人
                            accTfLoc.setInputBrId(ctrTfLocCont.getInputBrId());//录入机构
                            accTfLoc.setUpdId(ctrTfLocCont.getInputId());//修改人
                            accTfLoc.setUpdBrId(ctrTfLocCont.getInputBrId());//修改机构
                            accTfLoc.setManagerId(ctrTfLocCont.getManagerId());//管理人
                            accTfLoc.setManagerBrId(ctrTfLocCont.getManagerBrId());//管理机构
                            accTfLoc.setChrgRate(Optional.ofNullable(ctrTfLocCont.getChrgRate()).orElse(BigDecimal.ZERO));//手续费率
                            accTfLoc.setChrgAmt(new BigDecimal(issuingContAmt).multiply(Optional.ofNullable(ctrTfLocCont.getChrgRate()).orElse(BigDecimal.ZERO)));//手续费金额
                            accTfLoc.setIsUseLmtAmt("1");//是否使用授信额度
                            accTfLoc.setLmtAccNo(ctrTfLocCont.getLmtAccNo());//授信额度编号
                            accTfLoc.setReplyNo(ctrTfLocCont.getReplyNo());//批复编号
                            accTfLoc.setOprType(oprtype);//操作类型
                            accTfLoc.setFiveClass("10");//五级分类
                            accTfLoc.setTenClass("11");//十级分类
                            accTfLoc.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//分类日期
                            accTfLocMapper.insertSelective(accTfLoc);
                        }
                    } else {
                        xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0019DataRespDto.setOpMsg("合同信息不存在!");
                        return xdtz0019DataRespDto;
                    }
                }
            } else if ("02".equals(oprtype)) {//冲正
                int i = accTfLocMapper.deleteAccTfLocByBillNo(billNo);
                if (i == 0) {
                    xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0019DataRespDto.setOpMsg("借据号不存在!");
                    return xdtz0019DataRespDto;
                }


            } else if ("03".equals(oprtype)) {//恢复额度
                //增加校验 判断操作后额度是否小于0
                //判断借据号是否存在
                AccTfLoc accTfLoc = accTfLocMapper.selectAccTfLocByBillNo(billNo);
                if (Objects.nonNull(accTfLoc)) {
                    int a = 0;
                    if (Objects.equals(accStatus, "003")) {
                        a = accTfLocMapper.updateAccTfAmtAndStatusByBillNo(billNo);
                    } else {
                        accTfLoc.setCreditBal(new BigDecimal(billAmt));//信用证余额
                        a = accTfLocMapper.updateByPrimaryKeySelective(accTfLoc);
                    }
                    if (a == 0) {
                        xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                        xdtz0019DataRespDto.setOpMsg("更新信用证台帐失败!");
                        return xdtz0019DataRespDto;
                    }
                } else {
                    xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                    xdtz0019DataRespDto.setOpMsg("借据号不存在!");
                    return xdtz0019DataRespDto;
                }
            }
            xdtz0019DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
            xdtz0019DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value,
                JSON.toJSONString(xdtz0019DataRespDto));
        return xdtz0019DataRespDto;
    }
}