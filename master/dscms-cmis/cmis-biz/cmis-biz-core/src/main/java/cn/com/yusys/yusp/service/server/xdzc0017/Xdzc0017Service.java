package cn.com.yusys.yusp.service.server.xdzc0017;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.dto.server.xdzc0017.req.Xdzc0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.resp.Xdzc0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAccpTaskService;
import cn.com.yusys.yusp.service.DocAsplTcontService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 接口处理类:购销合同申请撤回
 * 根据贸易合同编号查询 贸易背景收集，
 * 如果审批状态 为待发起 或打回，直接置空贸易合同相关数据，
 * 其他审批状态不予变更
 * @Author xs
 * @Date 2021/6/16 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0017Service {

    @Autowired
    private DocAsplTcontService docAsplTcontService;

    @Autowired
    private AsplAccpTaskService asplAccpTaskService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0017.Xdzc0017Service.class);

    /**
     * @retu
     * @throws Exception
     */
    @Transactional
    public Xdzc0017DataRespDto xdzc0017Service(Xdzc0017DataReqDto xdzc0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value);
        Xdzc0017DataRespDto xdzc0017DataRespDto = new Xdzc0017DataRespDto();
        xdzc0017DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0017DataRespDto.setOpMsg("购销合同申请撤回，逻辑报错");// 描述信息z
        String cusId = xdzc0017DataReqDto.getCusId();// 客户号
        String tContNo = xdzc0017DataReqDto.getContNo();// 贸易合同编号
        try {
            QueryModel model = new QueryModel();
            model.addCondition("tContNo",tContNo);
            List<AsplAccpTask> AsplAccpTasks = asplAccpTaskService.selectAll(model);
            AsplAccpTask asplAccpTask = new AsplAccpTask();
            if(CollectionUtils.nonEmpty(AsplAccpTasks)){
                asplAccpTask = AsplAccpTasks.get(0);// 当前考虑只有一条
            }else{
                xdzc0017DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0017DataRespDto.setOpMsg("该笔购销合同申请不存在");// 描述信息
                return xdzc0017DataRespDto;
            }
            String approveStatus = asplAccpTask.getApproveStatus();
            if(!BizFlowConstant.WF_STATUS_000.equals(approveStatus)
                    || !BizFlowConstant.WF_STATUS_992.equals(approveStatus) ){
                xdzc0017DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0017DataRespDto.setOpMsg("购销合同申请撤回失败,申请状态"+ DscmsBizZxEnum.key(approveStatus)+"不可撤销");// 描述信息
                return xdzc0017DataRespDto;
            }

            asplAccpTaskService.updateEmptyByTContNo(tContNo);
            docAsplTcontService.updateEmptyByTContNo(tContNo);
            xdzc0017DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
            xdzc0017DataRespDto.setOpMsg("购销合同申请撤回，成功");// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value);
        return xdzc0017DataRespDto;
    }
}