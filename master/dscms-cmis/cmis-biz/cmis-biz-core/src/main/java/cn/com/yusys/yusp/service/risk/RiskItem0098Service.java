package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.req.CmisLmt0049ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0049.resp.CmisLmt0049RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 分支机构限额校验
 */
@Service
public class RiskItem0098Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0098Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private ICmisCfgClientService cmisCfgClientService;

    /**
     * 1、对公人民币贷款放款申请时，判断本次贷款放款申请金额<=当月剩余可投放对公贷款余额
     *
     * @param queryModel
     * @return
     */
    public RiskResultDto riskItem0098(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("分支机构限额校验开始*******************业务流水号：【{}】", serno);
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        //对公贷款出账申请
        boolean isPass = false;
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        if (pvpLoanApp != null) {

            //默认对公条线
            String newBelgLine = CmisCommonConstants.STD_BELG_LINE_03;
            String finaBrId = pvpLoanApp.getFinaBrId();
            String prdId = pvpLoanApp.getPrdId();
            //小微
            if (finaBrId.startsWith("0160") || (!finaBrId.endsWith("06") && !"019806".equals(finaBrId))){
                log.info("【riskItem0098】业务条线限额校验【getNewBelgLine】======>1");
                newBelgLine = CmisCommonConstants.STD_BELG_LINE_01;
            }
            //网络金融条线
            else if (finaBrId.equals("019806")){
                log.info("【riskItem0098】业务条线限额校验【getNewBelgLine】======>2");
                newBelgLine = CmisCommonConstants.STD_BELG_LINE_05;
            }else{
                //产品为消费贷款===>零售(prdType=10、11)
                log.info("【riskItem0098】业务条线限额校验【getNewBelgLine】======>3===>queryCfgPrdBasicInfo params【{}】",prdId);
                ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = cmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                log.info("【riskItem0098】业务条线限额校验【getNewBelgLine】======>4===>queryCfgPrdBasicInfo Result【{}】",JSONObject.toJSON(cfgPrdBasicinfoDtoResultDto));
                CfgPrdBasicinfoDto data = cfgPrdBasicinfoDtoResultDto.getData();
                if (data == null){
                    //授信数据获取失败 RISK_ERROR_0005
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc("产品类型获取失败");
                    return riskResultDto;
                }
                if ("10".equals(data.getPrdType()) || "11".equals(data.getPrdType())  || "9".equals(data.getPrdType())
                        || "12".equals(data.getPrdType())){
                    //此处把9、12也加上，为了在上面已经判断过小微的了，下面如果有机构不对的小微产品，则归属到零售条线  add by zhangjw 20211118(同张洪波确认)
                    log.info("【riskItem0098】业务条线限额校验【getNewBelgLine】======>5");
                    newBelgLine = CmisCommonConstants.STD_BELG_LINE_02;
                }
            }


            //pvp_loan_app表中所属条线BELG_LINE!= 对公条线；，直接通过
            if (!CmisCommonConstants.STD_BELG_LINE_03.equals(newBelgLine)) {
                isPass = true;
                log.info("【"+serno+"】非对公条线业务，不校验机构对公贷款人民币限额，校验通过");
            }
            //币种不为人民币，直接通过
            if (!CmisBizConstants.STD_ZB_CUR_TYP_CNY.equals(pvpLoanApp.getContCurType())) {
                isPass = true;
                log.info("【"+serno+"】非人民币贷款，不校验机构对公贷款人民币限额，校验通过");
            }
            //申请机构以8(村镇银行)开头，直接通过
            if (pvpLoanApp.getFinaBrId().startsWith("8")) {
                isPass = true;
                log.info("【"+serno+"】村镇银行不校验机构对公贷款人民币限额，校验通过");
            }
            if (!isPass) {
                log.info("RiskItem0098Service ==[AdminSmOrgDto-->reqDto] ===> {}", pvpLoanApp.getManagerBrId());
                AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(pvpLoanApp.getManagerBrId());
                log.info("RiskItem0098Service ==[AdminSmOrgDto-->respDto] ===> {}", adminSmOrgDto.toString());
                if (adminSmOrgDto == null) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09801); //机构信息获取失败
                    return riskResultDto;
                }
                String orgType = adminSmOrgDto.getOrgType();
                if(CmisCommonConstants.STD_ORG_TYPE_8.equals(orgType) || CmisCommonConstants.STD_ORG_TYPE_9.equals(orgType)
                    || CmisCommonConstants.STD_ORG_TYPE_A.equals(orgType)){
                    isPass = true;
                    log.info("【"+serno+"】小额贷款管理部、小贷分中心、村镇银行不校验机构对公贷款人民币限额，校验通过");
                }else{
                    BigDecimal pvpAmt = pvpLoanApp.getPvpAmt();
                    return getCurrMonthAllowLoanBalance(adminSmOrgDto, pvpAmt);
                }
            }
        } else {
            //授信数据获取失败 RISK_ERROR_0005
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09904);
            return riskResultDto;
        }
        //校验通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 机构区分：1、中心支行  2、异地支行（有分行） 3、异地支行（无分行）
     * <p>
     * 判断：
//     * 1、交易机构如果是4-中心支行，且机构层级为4，则向上找父级机构，获取父级机构对应的机构限额，（如果机构层级如果为3，则同第二点算法）
//     * 在统计机构下对应人民币对公贷款余额时，需要获取父级机构对应的所有子机构对应的对公人民币贷款余额；
//     * 2、交易机构如果是3-异地分行，则以该机构号作为查询机构，获取机构限额；
//     * 在统计机构下对应人民币对公贷款余额时，同样以该机构号作为查询机构获取机构对应的对公人民币贷款余额；
//     * 3、交易机构如果是	2-异地支行（无分行），且机构层级为4；（同第一点）
     *
     * 1、交易管户机构，机构层级为3，则以本机构机构号查询机构限额；
     * 2、交易管户机构，机构层级为4，则获取上级机构，以上级机构号获取机构限额；
     *
     * @param adminSmOrgDto
     * @param pvpAmt
     * @return
     */
    private RiskResultDto getCurrMonthAllowLoanBalance(AdminSmOrgDto adminSmOrgDto, BigDecimal pvpAmt) {
        RiskResultDto riskResultDto = new RiskResultDto();

        //当前对公人民币贷款余额
        BigDecimal currMonthAllowLoanBalance;
        String orgId = "";
        //机构类型 1、中心支行  2、异地支行（有分行） 3、异地支行（无分行）
        String orgType = adminSmOrgDto.getOrgType();
        //机构层级
        Integer orgLevel = adminSmOrgDto.getOrgLevel();

        //上月末对公贷款余额
        BigDecimal lastMonthComLoanBalance;
        //当月可净新增对公贷款投放金额
        BigDecimal currMonthAllowComAddAmt;

        /**
         * 若机构层级为4，则获取对应的上级机构，以上级机构号查找机构对公贷款限额
         */
        if (orgLevel == 4) {
            orgId = adminSmOrgDto.getUpOrgId();
        } else if (orgLevel == 3) {
            orgId = adminSmOrgDto.getOrgId();
        } else {
            orgId = adminSmOrgDto.getOrgId();
        }

        CmisLmt0049ReqDto cmisLmt0049ReqDto = new CmisLmt0049ReqDto();
        cmisLmt0049ReqDto.setOrgId(orgId);
        log.info("RiskItem0098Service ==[cmislmt0049-->reqDto] ===> 【{}】", JSONObject.toJSON(cmisLmt0049ReqDto));
        ResultDto<CmisLmt0049RespDto> resultDto = cmisLmtClientService.cmislmt0049(cmisLmt0049ReqDto);
        log.info("RiskItem0098Service ==[cmislmt0049-->resultDto] ===> 【{}】", JSONObject.toJSON(resultDto));
        if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            //根据机构编号获取分支机构额度管控信息 获取失败
            riskResultDto.setRiskResultDesc(resultDto.getMessage()+"机构号："+orgId);
            return riskResultDto;
        }

        CmisLmt0049RespDto cmisLmt0049RespDto = resultDto.getData();
        if (SuccessEnum.SUCCESS.key.equals(cmisLmt0049RespDto.getErrorCode())) {
            //0
            lastMonthComLoanBalance = cmisLmt0049RespDto.getLastMonthComLoanBalance() == null ?
                    BigDecimal.ZERO : cmisLmt0049RespDto.getLastMonthComLoanBalance();
            //10000_0000
            currMonthAllowComAddAmt = cmisLmt0049RespDto.getCurrMonthAllowComAddAmt() == null ?
                    BigDecimal.ZERO : cmisLmt0049RespDto.getCurrMonthAllowComAddAmt();
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            //根据机构编号获取分支机构额度管控信息 获取失败
            riskResultDto.setRiskResultDesc(cmisLmt0049RespDto.getErrorMsg());
            return riskResultDto;
        }

        //零售产品类型
        ResultDto<List<CfgPrdBasicinfoDto>> listResultDto = cmisCfgClientService.queryCfgPrdBasicInfoByPrdType("09,10,11,12");
        log.info("RiskItem0098Service==>获取零售业务条线产品Id=====>【{}】",JSONObject.toJSON(listResultDto));
        List<CfgPrdBasicinfoDto> data = listResultDto.getData();
        if (data == null){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("零售条线产品类型获取失败！");
            return riskResultDto;
        }
        List<String> linshouPrdIds = new ArrayList<>();
        for (Object datum : data) {
            Map<String,Object> map = (Map<String, Object>) datum;
            String prdId = map.get("prdId").toString();
            linshouPrdIds.add(prdId);
        }
        log.info("RiskItem0098Service==>获取零售业务条线产品Id=====>【{}】", JSONObject.toJSON(listResultDto));

        //机构层级为4 且 (4-中心支行)（2-异地支行（无分行）） 5-综合支行 3-异地分行
        if ("4".equals(orgType) || "2".equals(orgType) || "3".equals(orgType) || "5".equals(orgType)) {
            if (StringUtils.nonBlank(orgId)) {
                log.info("RiskItem0098Service ==[LowerOrgId-->reqDto] ===> 【{}】", orgId);
                List<String> lowerOrgId = commonService.getLowerOrgId(orgId);
                log.info("RiskItem0098Service ==[LowerOrgId-->respDto] ===> 【{}】", lowerOrgId);
                currMonthAllowLoanBalance = accLoanService.getCurrMonthAllowLoanBalance(lowerOrgId, CmisCommonConstants.STD_BELG_LINE_03,linshouPrdIds);
                log.info("RiskItem0098Service ==[currMonthAllowLoanBalance] ===> 【{}】", currMonthAllowLoanBalance);
                if (currMonthAllowLoanBalance == null) {
                    currMonthAllowLoanBalance = BigDecimal.ZERO;
                }
                //0+10000_0000-400_0000=
                BigDecimal lmt = lastMonthComLoanBalance.add(currMonthAllowComAddAmt).subtract(currMonthAllowLoanBalance);
                log.info("分支机构限额校验=====>>1  lastMonthComLoanBalance【{}】 currMonthAllowComAddAmt【{}】 currMonthAllowLoanBalance 【{}】 lmt【{}】 pvpamt 【{}】",
                        lastMonthComLoanBalance,currMonthAllowComAddAmt,currMonthAllowLoanBalance,lmt,pvpAmt);
                //当月剩余可投放对公贷款余额 = 上月末对公贷款余额+当月可净新增对公贷款投放金额-当前对公人民币贷款余额
                //对公人民币贷款放款申请时，判断本次贷款放款申请金额>当月剩余可投放对公贷款余额(拦截)
                if (pvpAmt.compareTo(lmt) > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    //本次贷款放款申请金额 超过了 当月剩余可投放对公贷款余额！
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09802);
                    return riskResultDto;
                }
            }
        }
//        //交易机构如果是3-异地分行，则以该机构号作为查询机构，获取机构限额；
//        if (CmisCommonConstants.STD_ORG_TYPE_3.equals(orgType)) {
//            List<String> orgIdList = new ArrayList<>();
//            orgIdList.add(adminSmOrgDto.getOrgCode());
//            //
//            currMonthAllowLoanBalance = accLoanService.getCurrMonthAllowLoanBalance(orgIdList, CmisCommonConstants.STD_BELG_LINE_03);
//            if (currMonthAllowLoanBalance == null) {
//                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
//                //当月剩余可投放对公贷款余额获取失败
//                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09803);
//                return riskResultDto;
//            }
//
//            //当月剩余可投放对公贷款余额 = 上月末对公贷款余额+当月可净新增对公贷款投放金额-当前对公人民币贷款余额
//            BigDecimal lmt = lastMonthComLoanBalance.add(currMonthAllowComAddAmt).subtract(currMonthAllowLoanBalance);
//            if (pvpAmt.compareTo(lmt) > 0) {
//                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
//                //本次贷款放款申请金额 超过了 当月剩余可投放对公贷款余额！
//                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_09802);
//                return riskResultDto;
//            }
//        }
        //校验通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
