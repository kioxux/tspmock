/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydEntFinInfo
 * @类描述: tmp_wyd_ent_fin_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_ent_fin_info_tmp")
public class TmpWydEntFinInfoTmp {
	
	/** 客户编号 **/
	@Column(name = "CUSTOMERID")
	private String customerid;
	
	/** 财务报表类（Y-年报，H-半年，Q-季报，M-月报） **/
	@Column(name = "REPORT_FORM_TYPE")
	private String reportFormType;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 企业名称 **/
	@Column(name = "ENTNAME", unique = false, nullable = true, length = 128)
	private String entname;
	
	/** 统一社会信用代码  **/
	@Column(name = "REGISTERNUMBER", unique = false, nullable = true, length = 32)
	private String registernumber;
	
	/** 企业成立日期 **/
	@Column(name = "REGISTER_DATE", unique = false, nullable = true, length = 19)
	private String registerDate;
	
	/** 中征码 **/
	@Column(name = "ZZM", unique = false, nullable = true, length = 64)
	private String zzm;
	
	/** 企业的行业 **/
	@Column(name = "INDUSTRY_NAME", unique = false, nullable = true, length = 100)
	private String industryName;
	
	/** 从业人数 **/
	@Column(name = "STAFF_NUMBER", unique = false, nullable = true, length = 16)
	private String staffNumber;
	
	/** 元素数据年份 **/
	@Column(name = "ANCHEYEAR", unique = false, nullable = true, length = 4)
	private String ancheyear;
	
	/** 资产总额（元） **/
	@Column(name = "ASSGRO", unique = false, nullable = true, length = 20)
	private String assgro;
	
	/** 负债总额（元） **/
	@Column(name = "LIAGRO", unique = false, nullable = true, length = 20)
	private String liagro;
	
	/** 营业总收入（元） **/
	@Column(name = "VENDINC", unique = false, nullable = true, length = 20)
	private String vendinc;
	
	/** 主营业务收入（元）   **/
	@Column(name = "MAIBUSINC", unique = false, nullable = true, length = 20)
	private String maibusinc;
	
	/** 利润总额（元）  **/
	@Column(name = "PROGRO", unique = false, nullable = true, length = 20)
	private String progro;
	
	/** 净利润（元）          **/
	@Column(name = "NETINC", unique = false, nullable = true, length = 20)
	private String netinc;
	
	/** 纳税总额（元） **/
	@Column(name = "RATGRO", unique = false, nullable = true, length = 20)
	private String ratgro;
	
	/** 所有者权益合计（元） **/
	@Column(name = "TOTEQU", unique = false, nullable = true, length = 20)
	private String totequ;
	
	/** 财报起始日期 **/
	@Column(name = "REPORT_START_DATE", unique = false, nullable = true, length = 10)
	private String reportStartDate;
	
	/** 财报截止日期 **/
	@Column(name = "REPORT_CLOSING_DATE", unique = false, nullable = true, length = 10)
	private String reportClosingDate;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param entname
	 */
	public void setEntname(String entname) {
		this.entname = entname;
	}
	
    /**
     * @return entname
     */
	public String getEntname() {
		return this.entname;
	}
	
	/**
	 * @param registernumber
	 */
	public void setRegisternumber(String registernumber) {
		this.registernumber = registernumber;
	}
	
    /**
     * @return registernumber
     */
	public String getRegisternumber() {
		return this.registernumber;
	}
	
	/**
	 * @param registerDate
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
    /**
     * @return registerDate
     */
	public String getRegisterDate() {
		return this.registerDate;
	}
	
	/**
	 * @param zzm
	 */
	public void setZzm(String zzm) {
		this.zzm = zzm;
	}
	
    /**
     * @return zzm
     */
	public String getZzm() {
		return this.zzm;
	}
	
	/**
	 * @param industryName
	 */
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	
    /**
     * @return industryName
     */
	public String getIndustryName() {
		return this.industryName;
	}
	
	/**
	 * @param staffNumber
	 */
	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}
	
    /**
     * @return staffNumber
     */
	public String getStaffNumber() {
		return this.staffNumber;
	}
	
	/**
	 * @param ancheyear
	 */
	public void setAncheyear(String ancheyear) {
		this.ancheyear = ancheyear;
	}
	
    /**
     * @return ancheyear
     */
	public String getAncheyear() {
		return this.ancheyear;
	}
	
	/**
	 * @param assgro
	 */
	public void setAssgro(String assgro) {
		this.assgro = assgro;
	}
	
    /**
     * @return assgro
     */
	public String getAssgro() {
		return this.assgro;
	}
	
	/**
	 * @param liagro
	 */
	public void setLiagro(String liagro) {
		this.liagro = liagro;
	}
	
    /**
     * @return liagro
     */
	public String getLiagro() {
		return this.liagro;
	}
	
	/**
	 * @param vendinc
	 */
	public void setVendinc(String vendinc) {
		this.vendinc = vendinc;
	}
	
    /**
     * @return vendinc
     */
	public String getVendinc() {
		return this.vendinc;
	}
	
	/**
	 * @param maibusinc
	 */
	public void setMaibusinc(String maibusinc) {
		this.maibusinc = maibusinc;
	}
	
    /**
     * @return maibusinc
     */
	public String getMaibusinc() {
		return this.maibusinc;
	}
	
	/**
	 * @param progro
	 */
	public void setProgro(String progro) {
		this.progro = progro;
	}
	
    /**
     * @return progro
     */
	public String getProgro() {
		return this.progro;
	}
	
	/**
	 * @param netinc
	 */
	public void setNetinc(String netinc) {
		this.netinc = netinc;
	}
	
    /**
     * @return netinc
     */
	public String getNetinc() {
		return this.netinc;
	}
	
	/**
	 * @param ratgro
	 */
	public void setRatgro(String ratgro) {
		this.ratgro = ratgro;
	}
	
    /**
     * @return ratgro
     */
	public String getRatgro() {
		return this.ratgro;
	}
	
	/**
	 * @param totequ
	 */
	public void setTotequ(String totequ) {
		this.totequ = totequ;
	}
	
    /**
     * @return totequ
     */
	public String getTotequ() {
		return this.totequ;
	}
	
	/**
	 * @param customerid
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
    /**
     * @return customerid
     */
	public String getCustomerid() {
		return this.customerid;
	}
	
	/**
	 * @param reportFormType
	 */
	public void setReportFormType(String reportFormType) {
		this.reportFormType = reportFormType;
	}
	
    /**
     * @return reportFormType
     */
	public String getReportFormType() {
		return this.reportFormType;
	}
	
	/**
	 * @param reportStartDate
	 */
	public void setReportStartDate(String reportStartDate) {
		this.reportStartDate = reportStartDate;
	}
	
    /**
     * @return reportStartDate
     */
	public String getReportStartDate() {
		return this.reportStartDate;
	}
	
	/**
	 * @param reportClosingDate
	 */
	public void setReportClosingDate(String reportClosingDate) {
		this.reportClosingDate = reportClosingDate;
	}
	
    /**
     * @return reportClosingDate
     */
	public String getReportClosingDate() {
		return this.reportClosingDate;
	}


}