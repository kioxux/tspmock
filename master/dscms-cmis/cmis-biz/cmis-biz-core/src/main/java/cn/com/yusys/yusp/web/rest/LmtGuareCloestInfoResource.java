/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGuareCloestInfo;
import cn.com.yusys.yusp.service.LmtGuareCloestInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCloestInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 20:01:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "抵押物云评估信息")
@RequestMapping("/api/lmtguarecloestinfo")
public class LmtGuareCloestInfoResource {
    @Autowired
    private LmtGuareCloestInfoService lmtGuareCloestInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGuareCloestInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGuareCloestInfo> list = lmtGuareCloestInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtGuareCloestInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGuareCloestInfo>> index(QueryModel queryModel) {
        List<LmtGuareCloestInfo> list = lmtGuareCloestInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareCloestInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGuareCloestInfo> show(@PathVariable("pkId") String pkId) {
        LmtGuareCloestInfo lmtGuareCloestInfo = lmtGuareCloestInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGuareCloestInfo>(lmtGuareCloestInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGuareCloestInfo> create(@RequestBody LmtGuareCloestInfo lmtGuareCloestInfo) throws URISyntaxException {
        lmtGuareCloestInfoService.insert(lmtGuareCloestInfo);
        return new ResultDto<LmtGuareCloestInfo>(lmtGuareCloestInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGuareCloestInfo lmtGuareCloestInfo) throws URISyntaxException {
        int result = lmtGuareCloestInfoService.update(lmtGuareCloestInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGuareCloestInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGuareCloestInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/18 15:49
     * @注释 条件分页查询 POST
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtGuareCloestInfo>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtGuareCloestInfo> list = lmtGuareCloestInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtGuareCloestInfo>>(list);
    }

    /**
     * @param lmtGuareCloestInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.LmtGuareCloestInfo>>
     * @author hubp
     * @date 2021/7/12 15:16
     * @version 1.0.0
     * @desc    移动OA新增
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addlmtguarecloestinfo")
    protected ResultDto<Integer> addLmtGuareCloestInfo(@RequestBody LmtGuareCloestInfo lmtGuareCloestInfo) {
        return new ResultDto<Integer>(lmtGuareCloestInfoService.insert(lmtGuareCloestInfo));
    }
}
