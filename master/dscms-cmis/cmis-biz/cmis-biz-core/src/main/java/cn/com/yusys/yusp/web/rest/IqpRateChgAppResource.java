/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRateChgApp;
import cn.com.yusys.yusp.service.IqpRateChgAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRateChgAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-19 20:41:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpratechgapp")
public class IqpRateChgAppResource {
    @Autowired
    private IqpRateChgAppService iqpRateChgAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<IqpRateChgApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRateChgApp> list = iqpRateChgAppService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<IqpRateChgApp>> index(@RequestBody QueryModel queryModel) {
        List<IqpRateChgApp> list = iqpRateChgAppService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{iqpSerno}")
    protected ResultDto<IqpRateChgApp> show(@PathVariable("iqpSerno") String iqpSerno) {
        IqpRateChgApp iqpRateChgApp = iqpRateChgAppService.selectByPrimaryKey(iqpSerno);
        return new ResultDto<>(iqpRateChgApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpRateChgApp> create(@RequestBody IqpRateChgApp iqpRateChgApp) throws URISyntaxException {
        iqpRateChgAppService.insertSelective(iqpRateChgApp);
        return new ResultDto<>(iqpRateChgApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRateChgApp iqpRateChgApp) throws URISyntaxException {
        int result = iqpRateChgAppService.updateSelective(iqpRateChgApp);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{iqpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("iqpSerno") String iqpSerno) {
        int result = iqpRateChgAppService.deleteByPrimaryKey(iqpSerno);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRateChgAppService.deleteByIds(ids);
        return new ResultDto<>(result);
    }
}
