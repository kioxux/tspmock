package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGrade
 * @类描述: major_grade数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-08 20:06:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class MajorGradeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信申请流水号 **/
	private String lmtSerno;
	
	/** 行业分类 **/
	private String tradeClass;
	
	/** 贷款用途 **/
	private String loanPurp;
	
	/** 项目融资具体用途 **/
	private String useRzType;
	
	/** 房地产融资具体用途 **/
	private String useFdcType;
	
	/** 项目融资还款来源 **/
	private String rzRepayWay;
	
	/** 房地产融资还款来源 **/
	private String fdcRepayWay;
	
	/** 债务人是否基本没有其它实质性资产和业务 **/
	private String hasOtherAsset;
	
	/** 银行对融资形成的资产控制权 **/
	private String bankContrlRz;
	
	/** 专业贷款类型 **/
	private String majorLoanType;
	
	/** 专业贷款评级模型 **/
	private String majorGradeMode;
	
	/** 专业贷款逾期损失率 **/
	private java.math.BigDecimal majorPreLost;
	
	/** 专业贷款本行既期信用等级 **/
	private String creditGrade;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 风险大类 **/
	private String bigRiskClass;
	
	/** 风险中类 **/
	private String midRiskClass;
	
	/** 风险小类 **/
	private String fewRiskClass;
	
	/** 风险划分日期 **/
	private String riskDivideDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass == null ? null : tradeClass.trim();
	}
	
    /**
     * @return TradeClass
     */	
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}
	
    /**
     * @return LoanPurp
     */	
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param useRzType
	 */
	public void setUseRzType(String useRzType) {
		this.useRzType = useRzType == null ? null : useRzType.trim();
	}
	
    /**
     * @return UseRzType
     */	
	public String getUseRzType() {
		return this.useRzType;
	}
	
	/**
	 * @param useFdcType
	 */
	public void setUseFdcType(String useFdcType) {
		this.useFdcType = useFdcType == null ? null : useFdcType.trim();
	}
	
    /**
     * @return UseFdcType
     */	
	public String getUseFdcType() {
		return this.useFdcType;
	}
	
	/**
	 * @param rzRepayWay
	 */
	public void setRzRepayWay(String rzRepayWay) {
		this.rzRepayWay = rzRepayWay == null ? null : rzRepayWay.trim();
	}
	
    /**
     * @return RzRepayWay
     */	
	public String getRzRepayWay() {
		return this.rzRepayWay;
	}
	
	/**
	 * @param fdcRepayWay
	 */
	public void setFdcRepayWay(String fdcRepayWay) {
		this.fdcRepayWay = fdcRepayWay == null ? null : fdcRepayWay.trim();
	}
	
    /**
     * @return FdcRepayWay
     */	
	public String getFdcRepayWay() {
		return this.fdcRepayWay;
	}
	
	/**
	 * @param hasOtherAsset
	 */
	public void setHasOtherAsset(String hasOtherAsset) {
		this.hasOtherAsset = hasOtherAsset == null ? null : hasOtherAsset.trim();
	}
	
    /**
     * @return HasOtherAsset
     */	
	public String getHasOtherAsset() {
		return this.hasOtherAsset;
	}
	
	/**
	 * @param bankContrlRz
	 */
	public void setBankContrlRz(String bankContrlRz) {
		this.bankContrlRz = bankContrlRz == null ? null : bankContrlRz.trim();
	}
	
    /**
     * @return BankContrlRz
     */	
	public String getBankContrlRz() {
		return this.bankContrlRz;
	}
	
	/**
	 * @param majorLoanType
	 */
	public void setMajorLoanType(String majorLoanType) {
		this.majorLoanType = majorLoanType == null ? null : majorLoanType.trim();
	}
	
    /**
     * @return MajorLoanType
     */	
	public String getMajorLoanType() {
		return this.majorLoanType;
	}
	
	/**
	 * @param majorGradeMode
	 */
	public void setMajorGradeMode(String majorGradeMode) {
		this.majorGradeMode = majorGradeMode == null ? null : majorGradeMode.trim();
	}
	
    /**
     * @return MajorGradeMode
     */	
	public String getMajorGradeMode() {
		return this.majorGradeMode;
	}
	
	/**
	 * @param majorPreLost
	 */
	public void setMajorPreLost(java.math.BigDecimal majorPreLost) {
		this.majorPreLost = majorPreLost;
	}
	
    /**
     * @return MajorPreLost
     */	
	public java.math.BigDecimal getMajorPreLost() {
		return this.majorPreLost;
	}
	
	/**
	 * @param creditGrade
	 */
	public void setCreditGrade(String creditGrade) {
		this.creditGrade = creditGrade == null ? null : creditGrade.trim();
	}
	
    /**
     * @return CreditGrade
     */	
	public String getCreditGrade() {
		return this.creditGrade;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bigRiskClass
	 */
	public void setBigRiskClass(String bigRiskClass) {
		this.bigRiskClass = bigRiskClass == null ? null : bigRiskClass.trim();
	}
	
    /**
     * @return BigRiskClass
     */	
	public String getBigRiskClass() {
		return this.bigRiskClass;
	}
	
	/**
	 * @param midRiskClass
	 */
	public void setMidRiskClass(String midRiskClass) {
		this.midRiskClass = midRiskClass == null ? null : midRiskClass.trim();
	}
	
    /**
     * @return MidRiskClass
     */	
	public String getMidRiskClass() {
		return this.midRiskClass;
	}
	
	/**
	 * @param fewRiskClass
	 */
	public void setFewRiskClass(String fewRiskClass) {
		this.fewRiskClass = fewRiskClass == null ? null : fewRiskClass.trim();
	}
	
    /**
     * @return FewRiskClass
     */	
	public String getFewRiskClass() {
		return this.fewRiskClass;
	}
	
	/**
	 * @param riskDivideDate
	 */
	public void setRiskDivideDate(String riskDivideDate) {
		this.riskDivideDate = riskDivideDate == null ? null : riskDivideDate.trim();
	}
	
    /**
     * @return RiskDivideDate
     */	
	public String getRiskDivideDate() {
		return this.riskDivideDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}