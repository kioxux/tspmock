/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperProfitSituation;
import cn.com.yusys.yusp.service.RptOperProfitSituationService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperProfitSituationResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:48:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperprofitsituation")
public class RptOperProfitSituationResource {
    @Autowired
    private RptOperProfitSituationService rptOperProfitSituationService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperProfitSituation>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperProfitSituation> list = rptOperProfitSituationService.selectAll(queryModel);
        return new ResultDto<List<RptOperProfitSituation>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperProfitSituation>> index(QueryModel queryModel) {
        List<RptOperProfitSituation> list = rptOperProfitSituationService.selectByModel(queryModel);
        return new ResultDto<List<RptOperProfitSituation>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptOperProfitSituation> show(@PathVariable("serno") String serno) {
        RptOperProfitSituation rptOperProfitSituation = rptOperProfitSituationService.selectByPrimaryKey(serno);
        return new ResultDto<RptOperProfitSituation>(rptOperProfitSituation);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperProfitSituation> create(@RequestBody RptOperProfitSituation rptOperProfitSituation) throws URISyntaxException {
        rptOperProfitSituationService.insert(rptOperProfitSituation);
        return new ResultDto<RptOperProfitSituation>(rptOperProfitSituation);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperProfitSituation rptOperProfitSituation) throws URISyntaxException {
        int result = rptOperProfitSituationService.update(rptOperProfitSituation);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptOperProfitSituationService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperProfitSituationService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptOperProfitSituation> selectBySerno(@RequestBody Map<String, Object> map) {
        String serno = map.get("serno").toString();
        return new ResultDto<RptOperProfitSituation>(rptOperProfitSituationService.selectByPrimaryKey(serno));
    }

    @PostMapping("/updateProfit")
    protected ResultDto<Integer> updateProfit(@RequestBody RptOperProfitSituation rptOperProfitSituation) {
        return new ResultDto<Integer>(rptOperProfitSituationService.updateSelective(rptOperProfitSituation));
    }

    @PostMapping("/insertProfit")
    protected ResultDto<Integer> insertProfit(@RequestBody RptOperProfitSituation rptOperProfitSituation) {
        return new ResultDto<Integer>(rptOperProfitSituationService.insertSelective(rptOperProfitSituation));
    }

    /**
     * 初始化
     *
     * @param map
     * @return
     */
    @PostMapping("/initProfitSituation")
    protected ResultDto<Integer> initProFitSituation(@RequestBody Map map) {
        return new ResultDto<Integer>(rptOperProfitSituationService.initProFitSituation(map));
    }

    @PostMapping("/deleteProfit")
    protected ResultDto<Integer> deleteProfit(@RequestBody String serno){
        return new ResultDto<Integer>(rptOperProfitSituationService.deleteByPrimaryKey(serno));
    }
}
