/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysGdxmd
 * @类描述: rpt_spd_anys_gdxmd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-19 10:35:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_gdxmd")
public class RptSpdAnysGdxmd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 固定资产贷款类型 **/
	@Column(name = "FIXED_ASSETS_LOANS_TYPE", unique = false, nullable = true, length = 5)
	private String fixedAssetsLoansType;
	
	/** 项目名称 **/
	@Column(name = "ENTRY_NAME", unique = false, nullable = true, length = 255)
	private String entryName;
	
	/** 项目概况、建设背景 **/
	@Column(name = "PROJECT_OVERVIEW", unique = false, nullable = true, length = 65535)
	private String projectOverview;
	
	/** 容积率 **/
	@Column(name = "PLOT_RATIO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal plotRatio;
	
	/** 楼面价 **/
	@Column(name = "FLOOR_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal floorPrice;
	
	/** 建设地点 **/
	@Column(name = "CONSTRUCTION_SITE", unique = false, nullable = true, length = 500)
	private String constructionSite;
	
	/** 投资规模 **/
	@Column(name = "INVESTMENT_SCALE", unique = false, nullable = true, length = 500)
	private String investmentScale;
	
	/** 建设周期 **/
	@Column(name = "CONSTRUCTION_CYCLE", unique = false, nullable = true, length = 500)
	private String constructionCycle;
	
	/** 资金来源 **/
	@Column(name = "SOURCE_OF_FUNDS", unique = false, nullable = true, length = 500)
	private String sourceOfFunds;
	
	/** 项目基本资料情况可研 **/
	@Column(name = "FEASIBILITY_STUDY", unique = false, nullable = true, length = 500)
	private String feasibilityStudy;
	
	/** 项目基本资料情况项目（发改委）批文 **/
	@Column(name = "APPROVAL_DOCUMENT_OF_THE_PROJECT", unique = false, nullable = true, length = 500)
	private String approvalDocumentOfTheProject;
	
	/** 项目基本资料情况项目选址意见书 **/
	@Column(name = "PROPOSAL_FOR_PROJECT_SITE_SELECTION", unique = false, nullable = true, length = 500)
	private String proposalForProjectSiteSelection;
	
	/** 项目基本资料情况环评 **/
	@Column(name = "EIA", unique = false, nullable = true, length = 500)
	private String eia;
	
	/** 项目基本资料情况建设用地规划许可证 **/
	@Column(name = "CONSTRUCTION_LAND_PLANNING_PERMIT", unique = false, nullable = true, length = 500)
	private String constructionLandPlanningPermit;
	
	/** 项目基本资料情况国有土地使用证 **/
	@Column(name = "STATE_OWNED_LAND_USE_CERTIFICATE", unique = false, nullable = true, length = 500)
	private String stateOwnedLandUseCertificate;
	
	/** 项目基本资料情况建设工程规划许可证 **/
	@Column(name = "CONSTRUCTION_PROJECT_PLANNING_PERMIT", unique = false, nullable = true, length = 500)
	private String constructionProjectPlanningPermit;
	
	/** 项目基本资料情况建设工程施工许可证 **/
	@Column(name = "CONSTRUCTION_PERMIT", unique = false, nullable = true, length = 500)
	private String constructionPermit;
	
	/** 项目基本资料情况其他所需资料 **/
	@Column(name = "OTHER_REQUIRED_INFORMATION", unique = false, nullable = true, length = 500)
	private String otherRequiredInformation;
	
	/** 工程施工合同 **/
	@Column(name = "PROJECT_CONSTRUCTION_CONTRACT", unique = false, nullable = true, length = 500)
	private String projectConstructionContract;
	
	/** 项目实施进度 **/
	@Column(name = "PROJECT_IMPLEMENTATION_SCHEDULE", unique = false, nullable = true, length = 500)
	private String projectImplementationSchedule;
	
	/** 预售许可证领取时间、取得预售收入情况 **/
	@Column(name = "PRE_SALE_REVENUE", unique = false, nullable = true, length = 500)
	private String preSaleRevenue;
	
	/** 已投入资金来源 **/
	@Column(name = "SOURCES_OF_FUNDS_INVESTED", unique = false, nullable = true, length = 500)
	private String sourcesOfFundsInvested;
	
	/** 效益测算 **/
	@Column(name = "BENEFIT_CALCULATION", unique = false, nullable = true, length = 500)
	private String benefitCalculation;
	
	/** 还款计划 **/
	@Column(name = "REPAYMENT_PLAN", unique = false, nullable = true, length = 65535)
	private String repaymentPlan;
	
	/** 其他需说明事项 **/
	@Column(name = "OTHER_EXPLAINED_MATTERS", unique = false, nullable = true, length = 65535)
	private String otherExplainedMatters;
	
	/** 项目贷或房地产贷标识 **/
	@Column(name = "FLAG", unique = false, nullable = true, length = 5)
	private String flag;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param fixedAssetsLoansType
	 */
	public void setFixedAssetsLoansType(String fixedAssetsLoansType) {
		this.fixedAssetsLoansType = fixedAssetsLoansType;
	}
	
    /**
     * @return fixedAssetsLoansType
     */
	public String getFixedAssetsLoansType() {
		return this.fixedAssetsLoansType;
	}
	
	/**
	 * @param entryName
	 */
	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}
	
    /**
     * @return entryName
     */
	public String getEntryName() {
		return this.entryName;
	}
	
	/**
	 * @param projectOverview
	 */
	public void setProjectOverview(String projectOverview) {
		this.projectOverview = projectOverview;
	}
	
    /**
     * @return projectOverview
     */
	public String getProjectOverview() {
		return this.projectOverview;
	}
	
	/**
	 * @param plotRatio
	 */
	public void setPlotRatio(java.math.BigDecimal plotRatio) {
		this.plotRatio = plotRatio;
	}
	
    /**
     * @return plotRatio
     */
	public java.math.BigDecimal getPlotRatio() {
		return this.plotRatio;
	}
	
	/**
	 * @param floorPrice
	 */
	public void setFloorPrice(java.math.BigDecimal floorPrice) {
		this.floorPrice = floorPrice;
	}
	
    /**
     * @return floorPrice
     */
	public java.math.BigDecimal getFloorPrice() {
		return this.floorPrice;
	}
	
	/**
	 * @param constructionSite
	 */
	public void setConstructionSite(String constructionSite) {
		this.constructionSite = constructionSite;
	}
	
    /**
     * @return constructionSite
     */
	public String getConstructionSite() {
		return this.constructionSite;
	}
	
	/**
	 * @param investmentScale
	 */
	public void setInvestmentScale(String investmentScale) {
		this.investmentScale = investmentScale;
	}
	
    /**
     * @return investmentScale
     */
	public String getInvestmentScale() {
		return this.investmentScale;
	}
	
	/**
	 * @param constructionCycle
	 */
	public void setConstructionCycle(String constructionCycle) {
		this.constructionCycle = constructionCycle;
	}
	
    /**
     * @return constructionCycle
     */
	public String getConstructionCycle() {
		return this.constructionCycle;
	}
	
	/**
	 * @param sourceOfFunds
	 */
	public void setSourceOfFunds(String sourceOfFunds) {
		this.sourceOfFunds = sourceOfFunds;
	}
	
    /**
     * @return sourceOfFunds
     */
	public String getSourceOfFunds() {
		return this.sourceOfFunds;
	}
	
	/**
	 * @param feasibilityStudy
	 */
	public void setFeasibilityStudy(String feasibilityStudy) {
		this.feasibilityStudy = feasibilityStudy;
	}
	
    /**
     * @return feasibilityStudy
     */
	public String getFeasibilityStudy() {
		return this.feasibilityStudy;
	}
	
	/**
	 * @param approvalDocumentOfTheProject
	 */
	public void setApprovalDocumentOfTheProject(String approvalDocumentOfTheProject) {
		this.approvalDocumentOfTheProject = approvalDocumentOfTheProject;
	}
	
    /**
     * @return approvalDocumentOfTheProject
     */
	public String getApprovalDocumentOfTheProject() {
		return this.approvalDocumentOfTheProject;
	}
	
	/**
	 * @param proposalForProjectSiteSelection
	 */
	public void setProposalForProjectSiteSelection(String proposalForProjectSiteSelection) {
		this.proposalForProjectSiteSelection = proposalForProjectSiteSelection;
	}
	
    /**
     * @return proposalForProjectSiteSelection
     */
	public String getProposalForProjectSiteSelection() {
		return this.proposalForProjectSiteSelection;
	}
	
	/**
	 * @param eia
	 */
	public void setEia(String eia) {
		this.eia = eia;
	}
	
    /**
     * @return eia
     */
	public String getEia() {
		return this.eia;
	}
	
	/**
	 * @param constructionLandPlanningPermit
	 */
	public void setConstructionLandPlanningPermit(String constructionLandPlanningPermit) {
		this.constructionLandPlanningPermit = constructionLandPlanningPermit;
	}
	
    /**
     * @return constructionLandPlanningPermit
     */
	public String getConstructionLandPlanningPermit() {
		return this.constructionLandPlanningPermit;
	}
	
	/**
	 * @param stateOwnedLandUseCertificate
	 */
	public void setStateOwnedLandUseCertificate(String stateOwnedLandUseCertificate) {
		this.stateOwnedLandUseCertificate = stateOwnedLandUseCertificate;
	}
	
    /**
     * @return stateOwnedLandUseCertificate
     */
	public String getStateOwnedLandUseCertificate() {
		return this.stateOwnedLandUseCertificate;
	}
	
	/**
	 * @param constructionProjectPlanningPermit
	 */
	public void setConstructionProjectPlanningPermit(String constructionProjectPlanningPermit) {
		this.constructionProjectPlanningPermit = constructionProjectPlanningPermit;
	}
	
    /**
     * @return constructionProjectPlanningPermit
     */
	public String getConstructionProjectPlanningPermit() {
		return this.constructionProjectPlanningPermit;
	}
	
	/**
	 * @param constructionPermit
	 */
	public void setConstructionPermit(String constructionPermit) {
		this.constructionPermit = constructionPermit;
	}
	
    /**
     * @return constructionPermit
     */
	public String getConstructionPermit() {
		return this.constructionPermit;
	}
	
	/**
	 * @param otherRequiredInformation
	 */
	public void setOtherRequiredInformation(String otherRequiredInformation) {
		this.otherRequiredInformation = otherRequiredInformation;
	}
	
    /**
     * @return otherRequiredInformation
     */
	public String getOtherRequiredInformation() {
		return this.otherRequiredInformation;
	}
	
	/**
	 * @param projectConstructionContract
	 */
	public void setProjectConstructionContract(String projectConstructionContract) {
		this.projectConstructionContract = projectConstructionContract;
	}
	
    /**
     * @return projectConstructionContract
     */
	public String getProjectConstructionContract() {
		return this.projectConstructionContract;
	}
	
	/**
	 * @param projectImplementationSchedule
	 */
	public void setProjectImplementationSchedule(String projectImplementationSchedule) {
		this.projectImplementationSchedule = projectImplementationSchedule;
	}
	
    /**
     * @return projectImplementationSchedule
     */
	public String getProjectImplementationSchedule() {
		return this.projectImplementationSchedule;
	}
	
	/**
	 * @param preSaleRevenue
	 */
	public void setPreSaleRevenue(String preSaleRevenue) {
		this.preSaleRevenue = preSaleRevenue;
	}
	
    /**
     * @return preSaleRevenue
     */
	public String getPreSaleRevenue() {
		return this.preSaleRevenue;
	}
	
	/**
	 * @param sourcesOfFundsInvested
	 */
	public void setSourcesOfFundsInvested(String sourcesOfFundsInvested) {
		this.sourcesOfFundsInvested = sourcesOfFundsInvested;
	}
	
    /**
     * @return sourcesOfFundsInvested
     */
	public String getSourcesOfFundsInvested() {
		return this.sourcesOfFundsInvested;
	}
	
	/**
	 * @param benefitCalculation
	 */
	public void setBenefitCalculation(String benefitCalculation) {
		this.benefitCalculation = benefitCalculation;
	}
	
    /**
     * @return benefitCalculation
     */
	public String getBenefitCalculation() {
		return this.benefitCalculation;
	}
	
	/**
	 * @param repaymentPlan
	 */
	public void setRepaymentPlan(String repaymentPlan) {
		this.repaymentPlan = repaymentPlan;
	}
	
    /**
     * @return repaymentPlan
     */
	public String getRepaymentPlan() {
		return this.repaymentPlan;
	}
	
	/**
	 * @param otherExplainedMatters
	 */
	public void setOtherExplainedMatters(String otherExplainedMatters) {
		this.otherExplainedMatters = otherExplainedMatters;
	}
	
    /**
     * @return otherExplainedMatters
     */
	public String getOtherExplainedMatters() {
		return this.otherExplainedMatters;
	}
	
	/**
	 * @param flag
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
    /**
     * @return flag
     */
	public String getFlag() {
		return this.flag;
	}


}