package cn.com.yusys.yusp.web.server.xdcz0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0009.req.Xdcz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0009.resp.Xdcz0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:出账记录详情查看
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0009:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0009.BizXdcz0009Resource.class);
    /**
     * 交易码：xdcz0009
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0009DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0009")
    protected @ResponseBody
    ResultDto<Xdcz0009DataRespDto>  xdcz0009(@Validated @RequestBody Xdcz0009DataReqDto xdcz0009DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009DataReqDto));
        Xdcz0009DataRespDto  xdcz0009DataRespDto  = new Xdcz0009DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0009DataRespDto>xdcz0009DataResultDto = new ResultDto<>();

        BigDecimal amt = xdcz0009DataReqDto.getAmt();//开证金额
        String startDate = xdcz0009DataReqDto.getStartDate();//开证日期
        String endDate = xdcz0009DataReqDto.getEndDate();//信用证到期日
        String huser = xdcz0009DataReqDto.getHuser();//经办人
        String bankNo = xdcz0009DataReqDto.getBankNo();//他行开证行号
        String bankName = xdcz0009DataReqDto.getBankName();//他行开证名称
        String cusId = xdcz0009DataReqDto.getCusId();//客户号
        String cusName = xdcz0009DataReqDto.getCusName();//客户中文名称
        String serno = xdcz0009DataReqDto.getSerno();//交易流水号
        String guarantNo = xdcz0009DataReqDto.getGuarantNo();//信用证号码
        String oprType = xdcz0009DataReqDto.getOprType();//操作类型
        String limitType = xdcz0009DataReqDto.getLimitType();//同业间额度类型

        try {
            // 从xdcz0009DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0009DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdcz0009DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xdcz0009DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdcz0009DataRespDto对象结束
            // 封装xdcz0009DataResultDto中正确的返回码和返回信息
            xdcz0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value,e.getMessage());
            // 封装xdcz0009DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdcz0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdcz0009DataRespDto到xdcz0009DataResultDto中
        xdcz0009DataResultDto.setData(xdcz0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009DataRespDto));
        return xdcz0009DataResultDto;
    }
}
