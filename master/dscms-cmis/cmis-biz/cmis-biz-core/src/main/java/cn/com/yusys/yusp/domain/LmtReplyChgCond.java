/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgCond
 * @类描述: lmt_reply_chg_cond数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-26 14:33:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_reply_chg_cond")
public class LmtReplyChgCond extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复变更流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 条件类型 **/
	@Column(name = "COND_TYPE", unique = false, nullable = true, length = 40)
	private String condType;
	
	/** 条件说明 **/
	@Column(name = "COND_DESC", unique = false, nullable = true, length = 40)
	private String condDesc;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 255)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 5)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param condType
	 */
	public void setCondType(String condType) {
		this.condType = condType;
	}
	
    /**
     * @return condType
     */
	public String getCondType() {
		return this.condType;
	}
	
	/**
	 * @param condDesc
	 */
	public void setCondDesc(String condDesc) {
		this.condDesc = condDesc;
	}
	
    /**
     * @return condDesc
     */
	public String getCondDesc() {
		return this.condDesc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}