package cn.com.yusys.yusp.vo;


import cn.com.yusys.yusp.domain.*;

import java.util.List;

public class CoopReplyAppAllVo {
    private CoopReplyApp coopReplyApp;

    private List<CoopReplyAppPro> coopReplyAppProList;

    private List<CoopReplyAppSub> coopReplyAppSubList;

    private List<CoopReplyAppCond> coopReplyAppCondList;

    private List<CoopReplyAppPsp> coopReplyAppPspList;

    public CoopReplyApp getCoopReplyApp() {
        return coopReplyApp;
    }

    public void setCoopReplyApp(CoopReplyApp coopReplyApp) {
        this.coopReplyApp = coopReplyApp;
    }

    public List<CoopReplyAppPro> getCoopReplyAppProList() {
        return coopReplyAppProList;
    }

    public void setCoopReplyAppProList(List<CoopReplyAppPro> coopReplyAppProList) {
        this.coopReplyAppProList = coopReplyAppProList;
    }

    public List<CoopReplyAppSub> getCoopReplyAppSubList() {
        return coopReplyAppSubList;
    }

    public void setCoopReplyAppSubList(List<CoopReplyAppSub> coopReplyAppSubList) {
        this.coopReplyAppSubList = coopReplyAppSubList;
    }

    public List<CoopReplyAppCond> getCoopReplyAppCondList() {
        return coopReplyAppCondList;
    }

    public void setCoopReplyAppCondList(List<CoopReplyAppCond> coopReplyAppCondList) {
        this.coopReplyAppCondList = coopReplyAppCondList;
    }

    public List<CoopReplyAppPsp> getCoopReplyAppPspList() {
        return coopReplyAppPspList;
    }

    public void setCoopReplyAppPspList(List<CoopReplyAppPsp> coopReplyAppPspList) {
        this.coopReplyAppPspList = coopReplyAppPspList;
    }
}
