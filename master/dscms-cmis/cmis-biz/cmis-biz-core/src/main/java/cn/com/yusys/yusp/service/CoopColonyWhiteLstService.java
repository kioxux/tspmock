/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CoopColonyWhiteLst;
import cn.com.yusys.yusp.domain.CoopPlanApp;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CoopColonyWhiteLstMapper;
import cn.com.yusys.yusp.vo.CoopColonyWhiteLstVo;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopColonyWhiteLstService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: AbsonZ
 * @创建时间: 2021-04-16 15:47:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopColonyWhiteLstService {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private CoopColonyWhiteLstMapper coopColonyWhiteLstMapper;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CoopColonyWhiteLst selectByPrimaryKey(String coopPlanNo) {
        return coopColonyWhiteLstMapper.selectByPrimaryKey(coopPlanNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CoopColonyWhiteLst> selectAll(QueryModel model) {
        List<CoopColonyWhiteLst> records = (List<CoopColonyWhiteLst>) coopColonyWhiteLstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopColonyWhiteLst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopColonyWhiteLst> list = coopColonyWhiteLstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CoopColonyWhiteLst record) {
        //存在性校验
        QueryModel model = new QueryModel();
        model.addCondition("coopPlanNo", record.getCoopPlanNo());
        model.addCondition("cusId", record.getCusId());
        model.addCondition("oprType", "01");
        List<CoopColonyWhiteLst> list = coopColonyWhiteLstMapper.selectByModel(model);
        if (!list.isEmpty()) {
            //已存在准入申请
            throw BizException.error(null, EcbEnum.COOP_COLONY_WHITE_LST.key, EcbEnum.COOP_COLONY_WHITE_LST.value);
        }
        return coopColonyWhiteLstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CoopColonyWhiteLst record) {
        return coopColonyWhiteLstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CoopColonyWhiteLst record) {
        return coopColonyWhiteLstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CoopColonyWhiteLst record) {
        return coopColonyWhiteLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String coopPlanNo) {
        return coopColonyWhiteLstMapper.deleteByPrimaryKey(coopPlanNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopColonyWhiteLstMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CoopColonyWhiteLstVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param cusLstGlfGljyyjedVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> cusLstGlfGljyyjedVoList) {
        User userInfo = SessionUtils.getUserInformation();
        List<CoopColonyWhiteLstVo> cusBatCusLstGlf = (List<CoopColonyWhiteLstVo>) BeanUtils.beansCopy(cusLstGlfGljyyjedVoList, CoopColonyWhiteLstVo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CoopColonyWhiteLstMapper coopColonyWhiteLstMapper = sqlSession.getMapper(CoopColonyWhiteLstMapper.class);

            for (CoopColonyWhiteLstVo cusLstGlfGljyyjedVo : cusBatCusLstGlf) {
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getCoopPlanNo())) {
                    throw BizException.error(null, "9999", "合作方案编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getPartnerNo())) {
                    throw BizException.error(null, "9999", "合作方编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getPartnerName())) {
                    throw BizException.error(null, "9999", "合作方名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getCusName())) {
                    throw BizException.error(null, "9999", "客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getCertType())) {
                    throw BizException.error(null, "9999", "证件类型不能为空");
                }
                if (StringUtil.isEmpty(cusLstGlfGljyyjedVo.getCertCode())) {
                    throw BizException.error(null, "9999", "证件号码不能为空");
                }
                CoopColonyWhiteLst clg = new CoopColonyWhiteLst();
                BeanUtils.beanCopy(cusLstGlfGljyyjedVo, clg);
                QueryModel qm = new QueryModel();
                qm.getCondition().put("coopPlanNo", clg.getCoopPlanNo());
                qm.addCondition("certCode", clg.getCertCode());
                qm.addCondition("oprType", "01");
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                clg.setPkId(UUID.randomUUID().toString().replaceAll("-", ""));
                clg.setPartnerType(cusLstGlfGljyyjedVo.getPartnerType().substring(0,1));
                System.out.println(cusLstGlfGljyyjedVo.getPartnerType());
                clg.setOprTime(date1);
                clg.setOprType("01");
                clg.setInputBrId(userInfo.getOrg().getCode());
                clg.setInputId(userInfo.getLoginCode());
                clg.setInputDate(date);
                clg.setCreateTime(date1);
                clg.setUpdateTime(date1);
                clg.setUpdId(userInfo.getLoginCode());
                clg.setUpdBrId(userInfo.getOrg().getCode());
                List<CoopColonyWhiteLst> glfList = coopColonyWhiteLstMapper.selectByModel(qm);
                if (glfList != null && glfList.size() > 0) {
                    throw BizException.error(null, null ,"证件号码【" + clg.getCertCode() + "】存在有效记录，本次导入已取消");
                }else{
                    coopColonyWhiteLstMapper.insertSelective(clg);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return cusBatCusLstGlf == null ? 0 : cusBatCusLstGlf.size();
    }

    /**
     * @方法名称: updateCusId
     * @方法描述: 一键更新客户号
     * @参数与返回说明:
     * @算法描述:
     */
    public int updateCusId() {
        //查询客户号为空的数据
        List<CoopColonyWhiteLst> cusLstGlfList = coopColonyWhiteLstMapper.selectCusIdIsNull();
        for (CoopColonyWhiteLst cusLstGlf : cusLstGlfList) {
            if(StringUtil.isNotEmpty(cusLstGlf.getPartnerType())){
            if (cusLstGlf.getPartnerType().equals(CmisCusConstants.STD_ZB_CUS_CATALOG_1)) {//客户大类1-对私
                try {
                    S10501ReqDto s10501ReqDto = new S10501ReqDto();
                    s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
                    s10501ReqDto.setIdtftp(cusLstGlf.getCertType());//   证件类型
                    s10501ReqDto.setIdtfno(cusLstGlf.getCertCode());//   证件号码
                    // 发送客户三要素发送ecfi查询
                    ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
                    String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    S10501RespDto s10501RespDto = null;
                    if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取查询结果 todo联调验证交易成功查询失败情况
                        s10501RespDto = s10501ResultDto.getData();
                            if (s10501RespDto.getListnm() != null) {
                                List<ListArrayInfo> listArrayInfo = s10501RespDto.getListArrayInfo();
                                // 客户号
                                cusLstGlf.setCusId(listArrayInfo.get(0).getCustno());
                                coopColonyWhiteLstMapper.updateByPrimaryKeySelective(cusLstGlf);
                            }
                    }
                } catch (Exception e) {

                }
            } else {//客户大类2-对公
                try {
                    G10501ReqDto g10501ReqDto = new G10501ReqDto();
                    g10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
                    g10501ReqDto.setIdtfno(cusLstGlf.getCertCode());//   证件类型
                    g10501ReqDto.setIdtftp(cusLstGlf.getCertType());//   证件号码
                    ResultDto<G10501RespDto> g10501Resp = dscms2EcifClientService.g10501(g10501ReqDto);
                    String s10501Code = Optional.ofNullable(g10501Resp.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    G10501RespDto g10501RespDto = null;
                    if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取查询结果 todo联调验证交易成功查询失败情况
                        g10501RespDto = g10501Resp.getData();
                            if (g10501RespDto.getListnm() != null) {
                                List<cn.com.yusys.yusp.dto.client.esb.ecif.g10501.ListArrayInfo> listArrayInfo = g10501RespDto.getListArrayInfo();
                                // 客户号
                                cusLstGlf.setCusId(listArrayInfo.get(0).getCustno());
                                coopColonyWhiteLstMapper.updateByPrimaryKeySelective(cusLstGlf);
                            }
                        }
                    coopColonyWhiteLstMapper.updateByPrimaryKeySelective(cusLstGlf);
                } catch (Exception e) {
                    //不抛出异常
                }

            }
        }
        }
        return 0;
    }
}
