/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtAppRelGuar;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.service.LmtAppRelGuarService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicFncAnly;
import cn.com.yusys.yusp.service.LmtSigInvestBasicFncAnlyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicFncAnlyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 20:54:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasicfncanly")
public class LmtSigInvestBasicFncAnlyResource {
    @Autowired
    private LmtSigInvestBasicFncAnlyService lmtSigInvestBasicFncAnlyService;

    @Autowired
    private LmtAppRelGuarService lmtAppRelGuarService;


	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicFncAnly>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicFncAnly> list = lmtSigInvestBasicFncAnlyService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicFncAnly>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicFncAnly>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicFncAnly> list = lmtSigInvestBasicFncAnlyService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicFncAnly>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicFncAnly> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly = lmtSigInvestBasicFncAnlyService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicFncAnly>(lmtSigInvestBasicFncAnly);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicFncAnly> create(@RequestBody LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly) throws URISyntaxException {
        lmtSigInvestBasicFncAnlyService.insert(lmtSigInvestBasicFncAnly);
        return new ResultDto<LmtSigInvestBasicFncAnly>(lmtSigInvestBasicFncAnly);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly) throws URISyntaxException {
        int result = lmtSigInvestBasicFncAnlyService.update(lmtSigInvestBasicFncAnly);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicFncAnlyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicFncAnlyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *根据底层授信流水号查询底层担保人信息
     */
    @PostMapping("/selectall")
    protected ResultDto<List<LmtSigInvestBasicFncAnly>> selectAll(@RequestBody QueryModel queryModel) {
        String basicSerno = (String) queryModel.getCondition().get("basicSerno");
        QueryModel queryModel1 = new QueryModel();
        queryModel1.addCondition("serno",basicSerno);
        List<LmtAppRelGuar> relGuars = lmtAppRelGuarService.selectByModel(queryModel1);
        List<LmtSigInvestBasicFncAnly> fncAnlyList = new ArrayList<LmtSigInvestBasicFncAnly>();
        for (LmtAppRelGuar lmtAppRelGuar:relGuars) {
            QueryModel queryModel2 = new QueryModel();
            queryModel2.addCondition("basicSerno",basicSerno);
            queryModel2.addCondition("cusId",lmtAppRelGuar.getGrtCusId());
            List<LmtSigInvestBasicFncAnly> list = lmtSigInvestBasicFncAnlyService.selectByModel(queryModel2);
            if (CollectionUtils.isNotEmpty(list)) {
                fncAnlyList.add(list.get(0));
            }
        }
        return new ResultDto<List<LmtSigInvestBasicFncAnly>>(fncAnlyList);
    }

    /**
     *更新信息
     */
    @PostMapping("/updateall")
    protected ResultDto<Integer> updateAll(@RequestBody LmtSigInvestBasicFncAnly lmtSigInvestBasicFncAnly) throws URISyntaxException{
        int result = lmtSigInvestBasicFncAnlyService.update(lmtSigInvestBasicFncAnly);
        return new ResultDto<Integer>(result);
    }

}
