/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorizeSub
 * @类描述: pvp_authorize_sub数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-08 20:47:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_authorize_sub")
public class PvpAuthorizeSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 授权编号 **/
	@Column(name = "AUTH_NO", unique = false, nullable = false, length = 40)
	private String authNo;
	
	/** 业务类别 **/
	@Column(name = "BUSI_CLS", unique = false, nullable = true, length = 10)
	private String busiCls;
	
	/** FLDVALUE01 **/
	@Column(name = "FLDVALUE01", unique = false, nullable = true, length = 200)
	private String fldvalue01;
	
	/** FLDVALUE02 **/
	@Column(name = "FLDVALUE02", unique = false, nullable = true, length = 200)
	private String fldvalue02;
	
	/** FLDVALUE03 **/
	@Column(name = "FLDVALUE03", unique = false, nullable = true, length = 200)
	private String fldvalue03;
	
	/** FLDVALUE04 **/
	@Column(name = "FLDVALUE04", unique = false, nullable = true, length = 200)
	private String fldvalue04;
	
	/** FLDVALUE05 **/
	@Column(name = "FLDVALUE05", unique = false, nullable = true, length = 200)
	private String fldvalue05;
	
	/** FLDVALUE06 **/
	@Column(name = "FLDVALUE06", unique = false, nullable = true, length = 200)
	private String fldvalue06;
	
	/** FLDVALUE07 **/
	@Column(name = "FLDVALUE07", unique = false, nullable = true, length = 200)
	private String fldvalue07;
	
	/** FLDVALUE08 **/
	@Column(name = "FLDVALUE08", unique = false, nullable = true, length = 200)
	private String fldvalue08;
	
	/** FLDVALUE09 **/
	@Column(name = "FLDVALUE09", unique = false, nullable = true, length = 200)
	private String fldvalue09;
	
	/** FLDVALUE10 **/
	@Column(name = "FLDVALUE10", unique = false, nullable = true, length = 200)
	private String fldvalue10;
	
	/** FLDVALUE11 **/
	@Column(name = "FLDVALUE11", unique = false, nullable = true, length = 200)
	private String fldvalue11;
	
	/** FLDVALUE12 **/
	@Column(name = "FLDVALUE12", unique = false, nullable = true, length = 200)
	private String fldvalue12;
	
	/** FLDVALUE13 **/
	@Column(name = "FLDVALUE13", unique = false, nullable = true, length = 200)
	private String fldvalue13;
	
	/** FLDVALUE14 **/
	@Column(name = "FLDVALUE14", unique = false, nullable = true, length = 200)
	private String fldvalue14;
	
	/** FLDVALUE15 **/
	@Column(name = "FLDVALUE15", unique = false, nullable = true, length = 200)
	private String fldvalue15;
	
	/** FLDVALUE16 **/
	@Column(name = "FLDVALUE16", unique = false, nullable = true, length = 200)
	private String fldvalue16;
	
	/** FLDVALUE17 **/
	@Column(name = "FLDVALUE17", unique = false, nullable = true, length = 200)
	private String fldvalue17;
	
	/** FLDVALUE18 **/
	@Column(name = "FLDVALUE18", unique = false, nullable = true, length = 200)
	private String fldvalue18;
	
	/** FLDVALUE19 **/
	@Column(name = "FLDVALUE19", unique = false, nullable = true, length = 200)
	private String fldvalue19;
	
	/** FLDVALUE20 **/
	@Column(name = "FLDVALUE20", unique = false, nullable = true, length = 200)
	private String fldvalue20;
	
	/** FLDVALUE21 **/
	@Column(name = "FLDVALUE21", unique = false, nullable = true, length = 200)
	private String fldvalue21;
	
	/** FLDVALUE22 **/
	@Column(name = "FLDVALUE22", unique = false, nullable = true, length = 200)
	private String fldvalue22;
	
	/** FLDVALUE23 **/
	@Column(name = "FLDVALUE23", unique = false, nullable = true, length = 200)
	private String fldvalue23;
	
	/** FLDVALUE24 **/
	@Column(name = "FLDVALUE24", unique = false, nullable = true, length = 200)
	private String fldvalue24;
	
	/** FLDVALUE25 **/
	@Column(name = "FLDVALUE25", unique = false, nullable = true, length = 200)
	private String fldvalue25;
	
	/** FLDVALUE26 **/
	@Column(name = "FLDVALUE26", unique = false, nullable = true, length = 200)
	private String fldvalue26;
	
	/** FLDVALUE27 **/
	@Column(name = "FLDVALUE27", unique = false, nullable = true, length = 200)
	private String fldvalue27;
	
	/** FLDVALUE28 **/
	@Column(name = "FLDVALUE28", unique = false, nullable = true, length = 200)
	private String fldvalue28;
	
	/** FLDVALUE29 **/
	@Column(name = "FLDVALUE29", unique = false, nullable = true, length = 200)
	private String fldvalue29;
	
	/** FLDVALUE30 **/
	@Column(name = "FLDVALUE30", unique = false, nullable = true, length = 200)
	private String fldvalue30;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param authNo
	 */
	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}
	
    /**
     * @return authNo
     */
	public String getAuthNo() {
		return this.authNo;
	}
	
	/**
	 * @param busiCls
	 */
	public void setBusiCls(String busiCls) {
		this.busiCls = busiCls;
	}
	
    /**
     * @return busiCls
     */
	public String getBusiCls() {
		return this.busiCls;
	}
	
	/**
	 * @param fldvalue01
	 */
	public void setFldvalue01(String fldvalue01) {
		this.fldvalue01 = fldvalue01;
	}
	
    /**
     * @return fldvalue01
     */
	public String getFldvalue01() {
		return this.fldvalue01;
	}
	
	/**
	 * @param fldvalue02
	 */
	public void setFldvalue02(String fldvalue02) {
		this.fldvalue02 = fldvalue02;
	}
	
    /**
     * @return fldvalue02
     */
	public String getFldvalue02() {
		return this.fldvalue02;
	}
	
	/**
	 * @param fldvalue03
	 */
	public void setFldvalue03(String fldvalue03) {
		this.fldvalue03 = fldvalue03;
	}
	
    /**
     * @return fldvalue03
     */
	public String getFldvalue03() {
		return this.fldvalue03;
	}
	
	/**
	 * @param fldvalue04
	 */
	public void setFldvalue04(String fldvalue04) {
		this.fldvalue04 = fldvalue04;
	}
	
    /**
     * @return fldvalue04
     */
	public String getFldvalue04() {
		return this.fldvalue04;
	}
	
	/**
	 * @param fldvalue05
	 */
	public void setFldvalue05(String fldvalue05) {
		this.fldvalue05 = fldvalue05;
	}
	
    /**
     * @return fldvalue05
     */
	public String getFldvalue05() {
		return this.fldvalue05;
	}
	
	/**
	 * @param fldvalue06
	 */
	public void setFldvalue06(String fldvalue06) {
		this.fldvalue06 = fldvalue06;
	}
	
    /**
     * @return fldvalue06
     */
	public String getFldvalue06() {
		return this.fldvalue06;
	}
	
	/**
	 * @param fldvalue07
	 */
	public void setFldvalue07(String fldvalue07) {
		this.fldvalue07 = fldvalue07;
	}
	
    /**
     * @return fldvalue07
     */
	public String getFldvalue07() {
		return this.fldvalue07;
	}
	
	/**
	 * @param fldvalue08
	 */
	public void setFldvalue08(String fldvalue08) {
		this.fldvalue08 = fldvalue08;
	}
	
    /**
     * @return fldvalue08
     */
	public String getFldvalue08() {
		return this.fldvalue08;
	}
	
	/**
	 * @param fldvalue09
	 */
	public void setFldvalue09(String fldvalue09) {
		this.fldvalue09 = fldvalue09;
	}
	
    /**
     * @return fldvalue09
     */
	public String getFldvalue09() {
		return this.fldvalue09;
	}
	
	/**
	 * @param fldvalue10
	 */
	public void setFldvalue10(String fldvalue10) {
		this.fldvalue10 = fldvalue10;
	}
	
    /**
     * @return fldvalue10
     */
	public String getFldvalue10() {
		return this.fldvalue10;
	}
	
	/**
	 * @param fldvalue11
	 */
	public void setFldvalue11(String fldvalue11) {
		this.fldvalue11 = fldvalue11;
	}
	
    /**
     * @return fldvalue11
     */
	public String getFldvalue11() {
		return this.fldvalue11;
	}
	
	/**
	 * @param fldvalue12
	 */
	public void setFldvalue12(String fldvalue12) {
		this.fldvalue12 = fldvalue12;
	}
	
    /**
     * @return fldvalue12
     */
	public String getFldvalue12() {
		return this.fldvalue12;
	}
	
	/**
	 * @param fldvalue13
	 */
	public void setFldvalue13(String fldvalue13) {
		this.fldvalue13 = fldvalue13;
	}
	
    /**
     * @return fldvalue13
     */
	public String getFldvalue13() {
		return this.fldvalue13;
	}
	
	/**
	 * @param fldvalue14
	 */
	public void setFldvalue14(String fldvalue14) {
		this.fldvalue14 = fldvalue14;
	}
	
    /**
     * @return fldvalue14
     */
	public String getFldvalue14() {
		return this.fldvalue14;
	}
	
	/**
	 * @param fldvalue15
	 */
	public void setFldvalue15(String fldvalue15) {
		this.fldvalue15 = fldvalue15;
	}
	
    /**
     * @return fldvalue15
     */
	public String getFldvalue15() {
		return this.fldvalue15;
	}
	
	/**
	 * @param fldvalue16
	 */
	public void setFldvalue16(String fldvalue16) {
		this.fldvalue16 = fldvalue16;
	}
	
    /**
     * @return fldvalue16
     */
	public String getFldvalue16() {
		return this.fldvalue16;
	}
	
	/**
	 * @param fldvalue17
	 */
	public void setFldvalue17(String fldvalue17) {
		this.fldvalue17 = fldvalue17;
	}
	
    /**
     * @return fldvalue17
     */
	public String getFldvalue17() {
		return this.fldvalue17;
	}
	
	/**
	 * @param fldvalue18
	 */
	public void setFldvalue18(String fldvalue18) {
		this.fldvalue18 = fldvalue18;
	}
	
    /**
     * @return fldvalue18
     */
	public String getFldvalue18() {
		return this.fldvalue18;
	}
	
	/**
	 * @param fldvalue19
	 */
	public void setFldvalue19(String fldvalue19) {
		this.fldvalue19 = fldvalue19;
	}
	
    /**
     * @return fldvalue19
     */
	public String getFldvalue19() {
		return this.fldvalue19;
	}
	
	/**
	 * @param fldvalue20
	 */
	public void setFldvalue20(String fldvalue20) {
		this.fldvalue20 = fldvalue20;
	}
	
    /**
     * @return fldvalue20
     */
	public String getFldvalue20() {
		return this.fldvalue20;
	}
	
	/**
	 * @param fldvalue21
	 */
	public void setFldvalue21(String fldvalue21) {
		this.fldvalue21 = fldvalue21;
	}
	
    /**
     * @return fldvalue21
     */
	public String getFldvalue21() {
		return this.fldvalue21;
	}
	
	/**
	 * @param fldvalue22
	 */
	public void setFldvalue22(String fldvalue22) {
		this.fldvalue22 = fldvalue22;
	}
	
    /**
     * @return fldvalue22
     */
	public String getFldvalue22() {
		return this.fldvalue22;
	}
	
	/**
	 * @param fldvalue23
	 */
	public void setFldvalue23(String fldvalue23) {
		this.fldvalue23 = fldvalue23;
	}
	
    /**
     * @return fldvalue23
     */
	public String getFldvalue23() {
		return this.fldvalue23;
	}
	
	/**
	 * @param fldvalue24
	 */
	public void setFldvalue24(String fldvalue24) {
		this.fldvalue24 = fldvalue24;
	}
	
    /**
     * @return fldvalue24
     */
	public String getFldvalue24() {
		return this.fldvalue24;
	}
	
	/**
	 * @param fldvalue25
	 */
	public void setFldvalue25(String fldvalue25) {
		this.fldvalue25 = fldvalue25;
	}
	
    /**
     * @return fldvalue25
     */
	public String getFldvalue25() {
		return this.fldvalue25;
	}
	
	/**
	 * @param fldvalue26
	 */
	public void setFldvalue26(String fldvalue26) {
		this.fldvalue26 = fldvalue26;
	}
	
    /**
     * @return fldvalue26
     */
	public String getFldvalue26() {
		return this.fldvalue26;
	}
	
	/**
	 * @param fldvalue27
	 */
	public void setFldvalue27(String fldvalue27) {
		this.fldvalue27 = fldvalue27;
	}
	
    /**
     * @return fldvalue27
     */
	public String getFldvalue27() {
		return this.fldvalue27;
	}
	
	/**
	 * @param fldvalue28
	 */
	public void setFldvalue28(String fldvalue28) {
		this.fldvalue28 = fldvalue28;
	}
	
    /**
     * @return fldvalue28
     */
	public String getFldvalue28() {
		return this.fldvalue28;
	}
	
	/**
	 * @param fldvalue29
	 */
	public void setFldvalue29(String fldvalue29) {
		this.fldvalue29 = fldvalue29;
	}
	
    /**
     * @return fldvalue29
     */
	public String getFldvalue29() {
		return this.fldvalue29;
	}
	
	/**
	 * @param fldvalue30
	 */
	public void setFldvalue30(String fldvalue30) {
		this.fldvalue30 = fldvalue30;
	}
	
    /**
     * @return fldvalue30
     */
	public String getFldvalue30() {
		return this.fldvalue30;
	}


}