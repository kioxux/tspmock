/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest.bat;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.bat.BatBizAssetsAnalyse;
import cn.com.yusys.yusp.service.bat.BatBizAssetsAnalyseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizAssetsAnalyseResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batbizassetsanalyse")
public class BatBizAssetsAnalyseResource {
    @Autowired
    private BatBizAssetsAnalyseService batBizAssetsAnalyseService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatBizAssetsAnalyse>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatBizAssetsAnalyse> list = batBizAssetsAnalyseService.selectAll(queryModel);
        return new ResultDto<List<BatBizAssetsAnalyse>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<BatBizAssetsAnalyse>> index(@RequestBody QueryModel queryModel) {
        List<BatBizAssetsAnalyse> list = batBizAssetsAnalyseService.selectByModel(queryModel);
        return new ResultDto<List<BatBizAssetsAnalyse>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BatBizAssetsAnalyse> show(@PathVariable("pkId") String pkId) {
        BatBizAssetsAnalyse batBizAssetsAnalyse = batBizAssetsAnalyseService.selectByPrimaryKey(pkId);
        return new ResultDto<BatBizAssetsAnalyse>(batBizAssetsAnalyse);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<BatBizAssetsAnalyse> create(@RequestBody BatBizAssetsAnalyse batBizAssetsAnalyse) throws URISyntaxException {
        batBizAssetsAnalyseService.insert(batBizAssetsAnalyse);
        return new ResultDto<BatBizAssetsAnalyse>(batBizAssetsAnalyse);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatBizAssetsAnalyse batBizAssetsAnalyse) throws URISyntaxException {
        int result = batBizAssetsAnalyseService.update(batBizAssetsAnalyse);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = batBizAssetsAnalyseService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batBizAssetsAnalyseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
