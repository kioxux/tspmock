/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtIntbankAccAndLmtSigDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.req.CmisLmt0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0003.resp.CmisLmt0003RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAccService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 16:54:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankAccService {

    @Autowired
    private LmtIntbankAccMapper lmtIntbankAccMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private LmtIntbankAccService lmtIntbankAccService ;

    @Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtIntbankReplyService lmtIntbankReplyService;

    @Autowired
    private LmtIntbankReplySubService lmtIntbankReplySubService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private BizCommonService bizCommonService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankAcc selectByPrimaryKey(String pkId) {
        return lmtIntbankAccMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankAcc> selectAll(QueryModel model) {
        List<LmtIntbankAcc> records = (List<LmtIntbankAcc>) lmtIntbankAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAcc> list = lmtIntbankAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankAcc record) {
        return lmtIntbankAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankAcc record) {
        return lmtIntbankAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankAcc record) {
        return lmtIntbankAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankAcc record) {
        return lmtIntbankAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankAccMapper.deleteByIds(ids);
    }

    /**
     * 条件查询
     * @param model
     * @return
     */
    public List<LmtIntbankAcc> selectByModelList(QueryModel model){
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAcc> list= lmtIntbankAccMapper.selectByModelList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据台账编号查询
     * @param accNo
     * @return
     */
    public LmtIntbankAcc selectByAccNo(String accNo){
        return lmtIntbankAccMapper.selectByAccNo(accNo);
    }

    /**
     * 获取有效批复编号
     * @param model
     * @return
     */
    public List<LmtIntbankAcc> getReplySerno(QueryModel model){
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAcc> list=  lmtIntbankAccMapper.getReplySerno(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据批复编号查询
     */
    public LmtIntbankAcc selectByReplyNo(String replySerno){
        return  lmtIntbankAccMapper.selectByReplySerno(replySerno);
    }

    /**
     * @方法名称: initLmtIntbankAccInfo
     * @方法描述: 初始化同业授信台账信息
     * @参数与返回说明: LmtIntbankAcc 返回同业授信台账信息
     * @算法描述: 无
     */
    public LmtIntbankAcc initLmtIntbankAccInfo(LmtIntbankAppr entityBean,String lmtType) throws Exception{
        try{
            //登记人
            String inputId =  entityBean.getInputId();
            //等级机构
            String inputBrId =  entityBean.getInputBrId();
            //01 授信新增 02 授信变更 03 授信续作 05 授信复议

            //同业授信申请表，期限，起始日，到期日等字段需要重新赋值
            Integer lmtTerm =  entityBean.getLmtTerm() ;
            if(lmtTerm==null) lmtTerm=0 ;
            //原批复编号
            String origiLmtReplySerno = entityBean.getOrigiLmtReplySerno() ;

//        User user = SessionUtils.getUserInformation() ;
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            //初始化同业授信台账表信息
            LmtIntbankAcc lmtIntbankAcc = null ;
            if(!CmisLmtConstants.STD_SX_LMT_TYPE_02.equals(lmtType)){
                lmtIntbankAcc = new LmtIntbankAcc() ;
                //将 申请或者审批表中的数据拷贝到台账表中
                BeanUtils.copyProperties(entityBean, lmtIntbankAcc) ;
                //审批表中无主管机构，主管客户经理，以审批表中登记人登记机构代替
                lmtIntbankAcc.setManagerId(inputId);
                lmtIntbankAcc.setManagerBrId(inputBrId);
                //生成主键
                Map paramMap= new HashMap<>() ;
                String pkValue = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, paramMap);
                String accNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.INTBANK_LMT_ACC_SEQ, paramMap);
                //设置主键
                lmtIntbankAcc.setPkId(pkValue);
                //台账编号
                lmtIntbankAcc.setAccNo(accNo);
                //起始日期
                String startDate = openday ;
                //到期日
                String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
                //期限
                lmtIntbankAcc.setTerm(lmtTerm);
                //起始日期
                lmtIntbankAcc.setStartDate(startDate);
                //到日期
                lmtIntbankAcc.setEndDate(endDate);
                //终审机构
                lmtIntbankAcc.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
                lmtIntbankAcc.setApprMode(entityBean.getApprMode());
                lmtIntbankAcc.setApprResult(entityBean.getReviewResult());
                //状态 01 生效
                lmtIntbankAcc.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                //操作状态
                lmtIntbankAcc.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                //登记日期
                lmtIntbankAcc.setInputDate(openday);
                //创建日期
                lmtIntbankAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            }else{
                lmtIntbankAcc = lmtIntbankAccService.selectByReplyNo(origiLmtReplySerno) ;
                String pkId = lmtIntbankAcc.getPkId() ;
                //将 申请或者审批表中的数据拷贝到台账表中
                BeanUtils.copyProperties(entityBean, lmtIntbankAcc) ;
                //起始日期
                String startDate = lmtIntbankAcc.getStartDate() ;
                //到期日
                String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd",lmtTerm);
                lmtIntbankAcc.setEndDate(endDate);
                lmtIntbankAcc.setPkId(pkId);
                lmtIntbankAcc.setApprMode(entityBean.getApprMode());
                lmtIntbankAcc.setApprResult(entityBean.getReviewResult());

                //add by lizx 20211029 变更时，温昕要求责任人要改变
                lmtIntbankAcc.setManagerBrId(entityBean.getInputBrId());
                lmtIntbankAcc.setManagerId(entityBean.getInputId());
                lmtIntbankAcc.setInputId(entityBean.getInputId());
                lmtIntbankAcc.setInputBrId(entityBean.getInputBrId());

            }
            //最新更新日期
            lmtIntbankAcc.setUpdDate(openday);
            //更新日期
            lmtIntbankAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            return lmtIntbankAcc ;

        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    /**
     * 根据申请流水号查询同业授信台账信息
     * @param serno
     * @return
     */
    public LmtIntbankAcc selectBySerno(String serno){
        return lmtIntbankAccMapper.selectBySerno(serno);
    }
    /**
     * 根据批复流水号查询同业授信台账信息
     * @param replySerno
     * @return
     */
    public LmtIntbankAcc selectByReplySerno(String replySerno ){
        return lmtIntbankAccMapper.selectByReplySerno(replySerno);
    }

    /**
     * 同业客户额度审批后通知额度系统
     * @param serno 申请流水号
     */
    public void lmtIntbankSendCmisLmt0003(String serno,String lmtType) throws Exception {
        Logger log = LoggerFactory.getLogger(LmtSigInvestApprService.class);
        //String serno = lmtIntbankAcc.getSerno() ;
        //发送额度系统，接口额度同步操作
        CmisLmt0003ReqDto cmisLmt0003ReqDto = new CmisLmt0003ReqDto() ;
        cmisLmt0003ReqDto.setSysId(CmisCommonConstants.STD_PERIPHERAL_SYS_XDG);

        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.selectBySerno(serno);
        cmisLmt0003ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);

        //批复台账编号
        String accNo = lmtIntbankAcc.getAccNo();
        cmisLmt0003ReqDto.setAccNo(accNo);

        //客户编号
        cmisLmt0003ReqDto.setCusId(lmtIntbankAcc.getCusId());
        //客户名称
        cmisLmt0003ReqDto.setCusName(lmtIntbankAcc.getCusName());
        //客户主体类型 默认3--同业
        cmisLmt0003ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG3);
        //是否覆盖原批复台账 默认为否
        String isCreateAcc = CmisLmtConstants.STD_ZB_YES_NO_N;
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.selectBySerno(serno);
        //原授信批复流水号
        String origiLmtReplySerno = lmtIntbankReply.getOrigiLmtReplySerno();

        if(CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(lmtType)){
            //授信类型为03--续作，则是否覆盖原批复台账为是
            isCreateAcc = CmisLmtConstants.STD_ZB_YES_NO_Y;
        }

        if (CmisLmtConstants.STD_ZB_YES_NO_Y.equals(isCreateAcc)){
            //如果是否覆盖原批复台账为是，则根据原批复流水号查询原批复台账编号
//            LmtIntbankAcc origiLmtIntbankAcc = lmtIntbankAccMapper.selectByReplySerno(origiLmtReplySerno);
            String origiAccNo = bizCommonService.getLmtIntbankRelAccNo(origiLmtReplySerno);
            //原批复台账编号
            cmisLmt0003ReqDto.setOrigiAccNo(origiAccNo);
        }else{
            //如果OrigiAccNo不设值，则调cmislmt0003接口时请求参数会因为OrigiAccNo是null而舍掉，这里将批复台账编号赋值给OrigiAccNo
            cmisLmt0003ReqDto.setOrigiAccNo(accNo);
        }

        //是否覆盖原批复台账
        cmisLmt0003ReqDto.setIsCreateAcc(isCreateAcc);
        //授信金额
        cmisLmt0003ReqDto.setLmtAmt(lmtIntbankAcc.getLmtAmt());
        //授信期限
        cmisLmt0003ReqDto.setTerm(lmtIntbankAcc.getTerm());
        //起始日
        cmisLmt0003ReqDto.setStartDate(lmtIntbankAcc.getStartDate());
        //到期日
        cmisLmt0003ReqDto.setEndDate(lmtIntbankAcc.getEndDate());
        //币种
        cmisLmt0003ReqDto.setCurType(lmtIntbankAcc.getCurType());
        //批复台账状态 默认 01--生效
        cmisLmt0003ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //责任人
        cmisLmt0003ReqDto.setManagerId(lmtIntbankAcc.getManagerId());
        //责任机构
        cmisLmt0003ReqDto.setManagerBrId(lmtIntbankAcc.getManagerBrId());
        //登记人
        cmisLmt0003ReqDto.setInputId(lmtIntbankAcc.getInputId());
        //登记机构
        cmisLmt0003ReqDto.setInputBrId(lmtIntbankAcc.getInputBrId());
        //登记日期
        cmisLmt0003ReqDto.setInputDate(lmtIntbankAcc.getInputDate());

        List<CmisLmt0003LmtSubListReqDto> lmtSubList = new ArrayList<>();

        List<LmtIntbankAccSub> lmtIntbankAccSubList = lmtIntbankAccSubService.selectBySerno(serno);

        if (lmtIntbankAccSubList!=null && lmtIntbankAccSubList.size()>0){
            for (LmtIntbankAccSub accSub: lmtIntbankAccSubList) {
                CmisLmt0003LmtSubListReqDto lmtSub = new CmisLmt0003LmtSubListReqDto();

                //授信分项编号	accSubNo
                lmtSub.setAccSubNo(accSub.getAccSubNo());

                if (CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(lmtType)){
                    //如果是否覆盖原批复台账为是，则根据原批复流水号查询原批复台账编号
                    LmtIntbankReplySub lmtIntbankReplySub = lmtIntbankReplySubService.selectByReplySubSerno(accSub.getReplySubSerno());
                    //TODO：感觉此处多次一举
                    if(lmtIntbankReplySub!=null && StringUtils.isNotEmpty(lmtIntbankReplySub.getOrigiLmtSubNo())){
                        LmtIntbankAccSub origilmtIntbankAccSub = lmtIntbankAccSubService.selectByAccSubNo(lmtIntbankReplySub.getOrigiLmtSubNo());
                        //原授信分项编号
                        lmtSub.setOrigiAccSubNo(origilmtIntbankAccSub.getAccSubNo());
                    }
                }

                //父节点	默认是批复台账编号 update by lizx 2021-06-16 同业授信不存在分项分级，此处父级目录注释掉
                //lmtSub.setParentId(accNo);
                //授信品种编号
                lmtSub.setLimitSubNo(accSub.getLmtBizType());
                //授信品种名称
                lmtSub.setLimitSubName(accSub.getLmtBizTypeName());
                //授信金额
                lmtSub.setAvlamt(accSub.getLmtAmt());
                //币种
                lmtSub.setCurType(accSub.getCurType());
                //授信期限
                lmtSub.setTerm(accSub.getLmtTerm());
                //额度起始日
                lmtSub.setStartDate(accSub.getStartDate());
                //额度到期日
                lmtSub.setEndDate(accSub.getEndDate());
                //是否涉及货币基金
                lmtSub.setIsIvlMf(accSub.getIsIvlMf());
                //货币基金总授信额度
                lmtSub.setLmtMfAmt(accSub.getLmtMfAmt());
                //单只货币基金授信额度
                lmtSub.setLmtSingleMfAmt(accSub.getLmtSingleMfAmt());
                //是否可循环
                lmtSub.setIsRevolv(accSub.getIsRevolv());
                //批复分项状态 默认01--生效
                lmtSub.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                //操作类型 默认01--新增
                lmtSub.setOprType(accSub.getOprType());

                lmtSubList.add(lmtSub);
            }
        }

        //TODO 缺少增加删除的分项处理

        cmisLmt0003ReqDto.setLmtSubList(lmtSubList);

        // 调用额度接口
        log.info("根据业务申请编号【{}】,前往额度系统进行同业额度同步开始！请求报文{}", serno, JSON.toJSONString(cmisLmt0003ReqDto));
        ResultDto<CmisLmt0003RespDto> cmisLmt0003RespDto = cmisLmtClientService.cmisLmt0003(cmisLmt0003ReqDto) ;;
        if (cmisLmt0003RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0003RespDto.getData().getErrorCode())) {
            log.info("根据业务申请编号【{}】,前往额度系统进行同业额度同步成功！响应报文{}", serno, JSON.toJSONString(cmisLmt0003RespDto));
        } else {
            String errorMsg = "";
            if(cmisLmt0003RespDto!=null){
                errorMsg = cmisLmt0003RespDto.getData().getErrorMsg();
            }
            log.info("根据业务申请编号【{}】,前往额度系统进行同业额度同步失败！", serno+","+errorMsg);
            throw new YuspException("前往额度系统进行同业额度同步失败",errorMsg);
        }
    }

    /**
     * 投后业务查询（贷后）
     * @param model
     * @return
     * @创建人：周茂伟
     */
    public List<LmtIntbankAccAndLmtSigDto> selectForPsp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankAccAndLmtSigDto> list = lmtIntbankAccMapper.selectForPsp(model);
        PageHelper.clearPage();
        return list;
    }

}
