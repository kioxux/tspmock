package cn.com.yusys.yusp.web.server.xdxw0080;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0080.req.Xdxw0080DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0080.resp.Xdxw0080DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0080.Xdxw0080Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优惠利率申请结果通知
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0080:优惠利率申请结果通知")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0080Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0080Resource.class);

    @Autowired
    private Xdxw0080Service xdxw0080Service;

    /**
     * 交易码：xdxw0080
     * 交易描述：优惠利率申请结果通知
     *
     * @param xdxw0080DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优惠利率申请结果通知")
    @PostMapping("/xdxw0080")
    protected @ResponseBody
    ResultDto<Xdxw0080DataRespDto> xdxw0080(@Validated @RequestBody Xdxw0080DataReqDto xdxw0080DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataReqDto));
        Xdxw0080DataRespDto xdxw0080DataRespDto = new Xdxw0080DataRespDto();// 响应Dto:优惠利率申请结果通知
        ResultDto<Xdxw0080DataRespDto> xdxw0080DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0080DataReqDto获取业务值进行业务逻辑处理
            xdxw0080DataRespDto = xdxw0080Service.xdxw0080(xdxw0080DataReqDto);
            // 封装xdxw0080DataResultDto中正确的返回码和返回信息
            xdxw0080DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0080DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, e.getMessage());
            // 封装xdxw0080DataResultDto中异常返回码和返回信息
            xdxw0080DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0080DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, e.getMessage());
            // 封装xdxw0080DataResultDto中异常返回码和返回信息
            xdxw0080DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0080DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0080DataRespDto到xdxw0080DataResultDto中
        xdxw0080DataResultDto.setData(xdxw0080DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataRespDto));
        return xdxw0080DataResultDto;
    }
}
