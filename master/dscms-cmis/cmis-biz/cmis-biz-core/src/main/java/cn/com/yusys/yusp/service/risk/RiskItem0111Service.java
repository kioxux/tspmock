package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtLadEval;
import cn.com.yusys.yusp.domain.LmtNpGreenApp;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.LmtLadEvalService;
import cn.com.yusys.yusp.service.LmtNpGreenAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

/**
 * @author lyj
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 对公客户分成比例协办客户经理校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0111Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0111Service.class);

    @Autowired
    private ICusClientService iCusClientService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author lyj
     * @date 2021/8/31 21:20
     * @version 1.0.0
     * @desc 判断 “获取分成比例协办客户经理编号” 是否为空。如果为空 提示“经查询，暂无协办客户经理，请先维护客户经理分成比例录入”
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0111(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";
        String cusId = "";
        log.info("*************对公客户分成比例协办客户经理校验***********【{}】",serno);
        if (StringUtils.isBlank(queryModel.getCondition().get("bizId").toString())) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        serno = queryModel.getCondition().get("bizId").toString();
        if (StringUtils.isBlank(queryModel.getCondition().get("bizUserId").toString())) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0099);
            return riskResultDto;
        }
        cusId=queryModel.getCondition().get("bizUserId").toString();

        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        if(Objects.isNull(cusBaseClientDto)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0108); //获取客户信息失败
            return riskResultDto;
        }
        //获取客户大类(对公客户则进行校验)
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){
            //获取分成比例协办客户经理编号
            CusMgrDividePercDto cusMgrDividePercDto = iCusClientService.selectXBManagerId(cusId).getData();
            if(cusMgrDividePercDto != null && !StringUtils.isBlank(cusMgrDividePercDto.getManagerId())){
                String managerId = cusMgrDividePercDto.getManagerId();
                if(StringUtils.isBlank(managerId)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11101); //经查询，暂无协办客户经理，请先维护客户经理分成比例录入
                    return riskResultDto;
                }
            }else{
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11101); //经查询，暂无协办客户经理，请先维护客户经理分成比例录入
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************对公客户分成比例协办客户经理校验结束***********【{}】",serno);
        return riskResultDto;
    }
}
