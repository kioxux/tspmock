package cn.com.yusys.yusp.domain.dto;

import cn.com.yusys.yusp.domain.LmtDebitCreditInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/1 20:17
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class DebitAndComDto {

    private LmtDebitCreditInfo lmtDebitCreditInfo;

    private LmtSurveyReportComInfo lmtSurveyReportComInfo;

    public LmtDebitCreditInfo getLmtDebitCreditInfo() {
        return lmtDebitCreditInfo;
    }

    public void setLmtDebitCreditInfo(LmtDebitCreditInfo lmtDebitCreditInfo) {
        this.lmtDebitCreditInfo = lmtDebitCreditInfo;
    }

    public LmtSurveyReportComInfo getLmtSurveyReportComInfo() {
        return lmtSurveyReportComInfo;
    }

    public void setLmtSurveyReportComInfo(LmtSurveyReportComInfo lmtSurveyReportComInfo) {
        this.lmtSurveyReportComInfo = lmtSurveyReportComInfo;
    }
}
