package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtAppSub;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.dto.FptMPpCustomerBlackDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CmisPspClientService;
import cn.com.yusys.yusp.service.LmtAppService;
import cn.com.yusys.yusp.service.LmtAppSubPrdService;
import cn.com.yusys.yusp.service.LmtAppSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class RiskItem0129Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0129Service.class);

    @Autowired
    private CmisPspClientService cmisPspClientService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    /**
     * @方法名称: riskItem0129
     * @方法描述: 客户是否为黑灰名单客户
     * @参数与返回说明:
     * @算法描述:
     * 特色产品：省心快贷、外贸贷、结息贷、优税贷，客户是否为黑灰名单客户，则提示。
     * @创建人: zhangliang15
     * @创建时间: 2021-10-19 09:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0129(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        log.info("客户是否为黑灰名单客户校验开始*******************业务流水号：【{}】，客户号：【{}】",serno,cusId);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0021); //获取客户信息失败
            return riskResultDto;
        }

        // 查询授信申请信息
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }

        // 根据申请流水号获取当前授信申请对应的授信分项及授信分项下的适用授信品种
        Map paramsSub = new HashMap();
        paramsSub.put("serno", serno);
        paramsSub.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        paramsSub.put("lmtAmt", "0");
        List<LmtAppSub> subList = lmtAppSubService.selectByParams(paramsSub);
        if (Objects.isNull(subList) || subList.isEmpty()) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
            return riskResultDto;
        }
        boolean isExitFlag = false; //判断是否存在特色产品：省心快贷、外贸贷、结息贷、优税贷分项标识
        for (LmtAppSub lmtAppSub : subList){
            Map paramsSubPrd = new HashMap();
            // 获取分项产品明细
            paramsSubPrd.put("subSerno", lmtAppSub.getSubSerno());
            paramsSubPrd.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtAppSubPrd> subListPrd = lmtAppSubPrdService.selectByParams(paramsSubPrd);
            for (LmtAppSubPrd lmtAppSubPrd : subListPrd){
                // 省心快贷:P011 外贸贷:P032 结息贷：P013 优税贷：P014
                if (Objects.equals("P011", lmtAppSubPrd.getLmtBizTypeProp()) || Objects.equals("P032", lmtAppSubPrd.getLmtBizTypeProp()) ||
                        Objects.equals("P013", lmtAppSubPrd.getLmtBizTypeProp()) || Objects.equals("P014", lmtAppSubPrd.getLmtBizTypeProp())) {
                    isExitFlag = true;
                    break;
                } else {
                    isExitFlag = false;
                }
            }
            if(isExitFlag) {
                break;
            }
        }
        // 判断若为特色产品：省心快贷、外贸贷、结息贷、优税贷，则判断客户是否为黑灰名单客户。
        if(isExitFlag) {
            ResultDto<FptMPpCustomerBlackDto> resultDto = cmisPspClientService.queryFptMPpCustomerBlackByCusId(cusId);
            if (Objects.isNull(resultDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10701);
                return riskResultDto;
            }
            FptMPpCustomerBlackDto fptMPpCustomerBlackDto = resultDto.getData();
            if (Objects.nonNull(fptMPpCustomerBlackDto)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012901);
                return riskResultDto;
            }
        }
        log.info("客户是否为黑灰名单客户校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}