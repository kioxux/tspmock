/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;
import cn.com.yusys.yusp.domain.GuarInfLivingRoom;
import cn.com.yusys.yusp.domain.GuarInfMachEqui;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarInfMachEqui;
import cn.com.yusys.yusp.service.GuarInfMachEquiService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMachEquiResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:09:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfmachequi")
public class GuarInfMachEquiResource {
    @Autowired
    private GuarInfMachEquiService guarInfMachEquiService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarInfMachEqui>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarInfMachEqui> list = guarInfMachEquiService.selectAll(queryModel);
        return new ResultDto<List<GuarInfMachEqui>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<GuarInfMachEqui>> index(QueryModel queryModel) {
        List<GuarInfMachEqui> list = guarInfMachEquiService.selectByModel(queryModel);
        return new ResultDto<List<GuarInfMachEqui>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<GuarInfMachEqui> show(@PathVariable("serno") String serno) {
        GuarInfMachEqui guarInfMachEqui = guarInfMachEquiService.selectByPrimaryKey(serno);
        return new ResultDto<GuarInfMachEqui>(guarInfMachEqui);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<GuarInfMachEqui> create(@RequestBody GuarInfMachEqui guarInfMachEqui) throws URISyntaxException {
        guarInfMachEquiService.insert(guarInfMachEqui);
        return new ResultDto<GuarInfMachEqui>(guarInfMachEqui);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody GuarInfMachEqui guarInfMachEqui) throws URISyntaxException {
        int result = guarInfMachEquiService.update(guarInfMachEqui);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = guarInfMachEquiService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = guarInfMachEquiService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:preserveGuarInfMachEqui
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/preserveGuarInfMachEqui")
    protected ResultDto<Integer> preserveGuarInfMachEqui(@RequestBody GuarInfMachEqui guarInfMachEqui) {
        int result = guarInfMachEquiService.preserveGuarInfMachEqui(guarInfMachEqui);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryBySerno
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBySerno")
    protected ResultDto<GuarInfMachEqui> queryBySerno(@RequestBody GuarInfMachEqui record) {
        GuarInfMachEqui guarInfMachEqui = guarInfMachEquiService.selectByPrimaryKey(record.getSerno());
        return new ResultDto<GuarInfMachEqui>(guarInfMachEqui);
    }

}
