package cn.com.yusys.yusp.service.server.xdtz0055;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.server.xdtz0055.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpChgTrupayAcctAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0.0
 * @项目名称：
 * @类名称：
 * @类描述： #Dao类
 * @功能描述：
 * @创建人：YX-WJ
 * @创建时间：2021/6/4 22:01
 * @修改备注
 * @修改记录： 修改时间 修改人员 修改原因
 * --------------------------------------------------
 * @Copyrigth(c) 宇信科技-版权所有
 */
@Service
public class Xdtz0055Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0055Service.class);
    @Autowired
    private IqpChgTrupayAcctAppMapper iqpChgTrupayAcctAppMapper;
    @Autowired
    private AccLoanMapper accLoanMapper;
    /**
     * @Description:根据客户id或者客户账号查询受托信息
     * @Author: YX-WJ
     * @Date: 2021/6/4 23:53
     * @param cusId:客户id
     * @param cusAcctNo:客户账号
     * @return: cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto
     **/
    public Xdtz0055DataRespDto xdtz0055(String cusId, String cusAcctNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value);
        Xdtz0055DataRespDto xdtz0055DataRespDto = new Xdtz0055DataRespDto();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, cusId,cusAcctNo);
            //开始查询受托信息
            java.util.List<cn.com.yusys.yusp.dto.server.xdtz0055.resp.List> lists =  iqpChgTrupayAcctAppMapper.selectByCusIdOrCusAcctNo(cusId,cusAcctNo);
            logger.info("**********XDTZ0055**根据客户id或者客户账号查询受托信息结束,返回结果为:{}", JSON.toJSONString(lists));
            if (CollectionUtils.nonEmpty(lists) && lists.size()>0){
                //根据查询到的billNo，去台账表查询指定信息
                for (cn.com.yusys.yusp.dto.server.xdtz0055.resp.List list:
                lists) {
                    AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(list.getBillNo());
                    list.setCusId(accLoan.getCusId());
                    list.setCusName(accLoan.getCusName());
                    list.setContNo(accLoan.getContNo());
                }
            }
            xdtz0055DataRespDto.setList(lists);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, cusId,cusAcctNo);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value);
        return xdtz0055DataRespDto;
    }
}
