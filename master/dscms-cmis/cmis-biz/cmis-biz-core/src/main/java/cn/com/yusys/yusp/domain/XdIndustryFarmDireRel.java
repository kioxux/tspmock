/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: XdIndustryFarmDireRel
 * @类描述: xd_industry_farm_dire_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-10-13 22:48:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "xd_industry_farm_dire_rel")
public class XdIndustryFarmDireRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = false, length = 10)
	private String bizType;
	
	/** 是否农业 **/
	@Column(name = "IS_NY", unique = false, nullable = true, length = 5)
	private String isNy;
	
	/** 行业类型 **/
	@Column(name = "INDUSTRY_TYPE", unique = false, nullable = true, length = 5)
	private String industryType;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 贷款投向 **/
	@Column(name = "CFARM_DIRECTION", unique = false, nullable = true, length = 20)
	private String cfarmDirection;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param isNy
	 */
	public void setIsNy(String isNy) {
		this.isNy = isNy;
	}
	
    /**
     * @return isNy
     */
	public String getIsNy() {
		return this.isNy;
	}
	
	/**
	 * @param industryType
	 */
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	
    /**
     * @return industryType
     */
	public String getIndustryType() {
		return this.industryType;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cfarmDirection
	 */
	public void setCfarmDirection(String cfarmDirection) {
		this.cfarmDirection = cfarmDirection;
	}
	
    /**
     * @return cfarmDirection
     */
	public String getCfarmDirection() {
		return this.cfarmDirection;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}