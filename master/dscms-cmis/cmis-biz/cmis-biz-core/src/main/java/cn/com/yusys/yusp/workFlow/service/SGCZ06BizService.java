package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class SGCZ06BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(SGCZ06BizService.class);//定义log

    @Autowired
    private PvpAccpAppService pvpAccpAppService;
    @Autowired
    private PvpEntrustLoanAppService pvpEntrustLoanAppService;
    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private BGYW01BizService bgyw01BizService;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07Bizservice;
    @Autowired
    private BGYW09BizService bgyw09Bizservice;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;
    @Autowired
    private LSYW03BizService lsyw03BizService;
    @Autowired
    private BGYW14BizService bGYW14BizService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String serno = resultInstanceDto.getBizId();
        String bizType = resultInstanceDto.getBizType();
        if (CmisFlowConstants.FLOW_TYPE_SGD01.equals(bizType)) {
            handleSGD01Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)) {
            handleSGD02Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }  else if (CmisFlowConstants.FLOW_TYPE_SGD03.equals(bizType)) {
            handleSGD03Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }  else if (CmisFlowConstants.FLOW_TYPE_SGD04.equals(bizType)) {
            handleSGD04Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH04.equals(bizType)) {
            // 利率变更协议签订审核--寿光
            bgyw01BizService.bizOp(resultInstanceDto);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH06.equals(bizType)) {
            // 展期申请协议 -寿光
            bgyw02BizService.bizOp(resultInstanceDto);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH08.equals(bizType)) {
            // 担保协议签订 -寿光
            bgyw03Bizservice.bizOp(resultInstanceDto);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_SGH11.equals(bizType)) {
            // 还款计划变更协议签订-寿光
            bgyw07Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_SGH13.equals(bizType)){
            //延期还款协议签订审核-寿光
            bgyw09Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_SGE04.equals(bizType)){
            //SGE04零售放款申请（寿光）
            handlerSGE04(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)){
            //SGD02 对公放款申请（寿光）
            handlerSGD02(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_SGH16.equals(bizType)){
            // SGH16受托支付账号变更（寿光）
            bGYW14BizService.handleBG025BizApp(resultInstanceDto,currentOpType,serno,currentUserId,currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 委托贷款申请流程
    private void handleSGD04Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("委托贷款申请流程(寿光)" + pvpSerno + "流程操作");
        try {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
            this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                if ("648_3".equals(resultInstanceDto.getCurrentNodeId())){
                    pvpEntrustLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                pvpEntrustLoanAppService.handleBusinessDataAfterEnd(pvpSerno);

                //微信通知
                String managerId = pvpEntrustLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpEntrustLoanApp.getCusName());
                        paramMap.put("prdName", "委托贷款出账");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
                    this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
                    //微信通知
                    String managerId = pvpEntrustLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpEntrustLoanApp.getCusName());
                            paramMap.put("prdName", "委托贷款出账");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpEntrustLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(pvpSerno);
                    this.put2VarParamForWT(resultInstanceDto, pvpEntrustLoanApp, pvpSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("委托贷款申请流程(寿光)" + pvpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pvpEntrustLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                pvpEntrustLoanAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("委托贷款申请流程(寿光)" + pvpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParamForWT
     * @方法描述: 委托贷款申请流程(寿光) 参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamForWT(ResultInstanceDto resultInstanceDto, PvpEntrustLoanApp pvpEntrustLoanApp, String pvpSerno) {
        log.info("委托贷款申请流程(寿光)【{}】审批流程参数更新开始", pvpSerno);
        String approveStatus = pvpEntrustLoanApp.getApproveStatus();
        BigDecimal pvpAmt =  pvpEntrustLoanApp.getPvpAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        params.put("approveStatus", approveStatus);
        params.put("pvpAmt", pvpAmt);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("委托贷款申请流程(寿光)【{}】审批流程参数更新结束", pvpSerno);
    }

    // 银承贷款申请流程
    private void handleSGD03Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("银承出账(寿光)审批流程" + currentOpType);
        try {
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(pvpSerno);
            this.put2VarParamForYP(resultInstanceDto, pvpAccpApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("银承出账(寿光)申请" + pvpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                if ("648_3".equals(resultInstanceDto.getCurrentNodeId())){
                    pvpAccpAppService.handleBusinessDataAfterStart(pvpSerno);
                }
                // 资料未全生成影像补扫任务
                // log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,pvpSerno);
                // pvpAccpAppService.createImageSpplInfo(pvpSerno,resultInstanceDto.getBizType(),pvpAccpApp,resultInstanceDto.getInstanceId());
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("银承出账(寿光)申请" + pvpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("银承出账(寿光)申请" + pvpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                pvpAccpAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_997);
                pvpAccpAppService.handleBusinessDataAfterEnd(pvpSerno);
                //微信通知
                String managerId = pvpAccpApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpAccpApp.getCusName());
                        paramMap.put("prdName", "银承出账(寿光)");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    pvpAccpAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpAccpApp = pvpAccpAppService.selectBySerno(pvpSerno);
                    this.put2VarParamForYP(resultInstanceDto, pvpAccpApp, pvpSerno);
                    //微信通知
                    String managerId = pvpAccpApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpAccpApp.getCusName());
                            paramMap.put("prdName", "银承出账(寿光)");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("银承出账(寿光)申请"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpAccpAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpAccpApp = pvpAccpAppService.selectBySerno(pvpSerno);
                    this.put2VarParamForYP(resultInstanceDto, pvpAccpApp, pvpSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("银承出账(寿光)申请"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    pvpAccpAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("银承出账(寿光)申请" + pvpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                pvpAccpAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("银承出账(寿光)申请" + pvpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pvpAccpApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                pvpAccpAppService.updateApprStatus(pvpSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("银承出账(寿光)申请" + pvpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParamForYP
     * @方法描述: 银承出账(寿光)审批流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: liuzz
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamForYP(ResultInstanceDto resultInstanceDto, PvpAccpApp pvpAccpApp,String pvpSerno) {
        log.info("银承出账(寿光)【{}】审批流程参数更新开始", pvpSerno);
        String approveStatus = pvpAccpApp.getApproveStatus();
        BigDecimal appAmt =  pvpAccpApp.getAppAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        params.put("approveStatus", approveStatus);
        params.put("appAmt", appAmt);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("银承出账(寿光)【{}】审批流程参数更新结束", pvpSerno);
    }

    // 普通贷款业务申请
    private void handleSGD02Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程操作:" + currentOpType+"后业务处理");
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                if ("648_3".equals(resultInstanceDto.getCurrentNodeId())){
                    pvpLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                }
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程提交操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程结束操作，流程参数："+ resultInstanceDto.toString());
                //放款申请通过，调用后续的业务处理过程
                pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
                // 资料未全生成影像补扫任务
                //微信通知
                String managerId = pvpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpLoanApp.getCusName());
                        paramMap.put("prdName", "一般贷款出账");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"退回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                    //微信通知
                    String managerId = pvpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpLoanApp.getCusName());
                            paramMap.put("prdName", "一般贷款出账");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //针对客户经理拿回操作，应当将业务申请主表的业务状态更新为【打回-992】
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"拿回初始节点操作，流程参数："+ resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"否决操作，流程参数："+ resultInstanceDto.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                pvpLoanAppService.handleBusinessAfterRefuse(pvpSerno);
            } else {
                log.warn("对公贷款出账申请(寿光)"+pvpSerno+"未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 对公贷款出账申请 参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParamForPT(ResultInstanceDto resultInstanceDto, PvpLoanApp pvpLoanApp, String pvpSerno) {
        log.info("对公贷款出账申请(寿光)【{}】审批流程参数更新开始", pvpSerno);
        String approveStatus = pvpLoanApp.getApproveStatus();
        BigDecimal pvpAmt =  pvpLoanApp.getPvpAmt();
        Map<String, Object> params = new HashMap<>();
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        params.put("approveStatus", approveStatus);
        params.put("pvpAmt", pvpAmt);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
        log.info("对公贷款出账申请(寿光)【{}】审批流程参数更新结束", pvpSerno);
    }

    // 合同影像审核
    private void handleSGD01Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("合同影像审核" + currentOpType);
        try {
            CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(iqpSerno);
            ctrContImageAuditAppService.put2VarParam(resultInstanceDto,iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                ctrContImageAuditAppService.handleBusinessDataAfterStart(iqpSerno);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                ctrContImageAuditAppService.handleBusinessDataAfterEnd(iqpSerno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("合同影像申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合同影像申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合同影像申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                ctrContImageAuditApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                ctrContImageAuditAppService.updateSelective(ctrContImageAuditApp);
            } else {
                log.warn("合同影像申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.SGCZ06.equals(flowCode);
    }

    //DHE04零售放款申请（寿光）
    private void handlerSGE04(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(extSerno);

        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                //零售-放款申请提交-额度占用
                if("000".equals(pvpLoanApp.getApproveStatus()) || "992".equals(pvpLoanApp.getApproveStatus())){
                    pvpLoanAppService.retaillimitOccupy(extSerno);
                }
                pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                pvpLoanAppService.updateSelective(pvpLoanApp);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                pvpLoanAppService.reviewend(pvpLoanApp);
                PvpAuthorize pvpAuthorize = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpLoanApp.getPvpSerno());
                log.info("零售出账开始："+pvpAuthorize.getTranSerno());
                pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
                lsyw03BizService.sendSgMessage(resultInstanceDto,"同意");
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    pvpLoanAppService.updateSelective(pvpLoanApp);
                    pvpLoanAppService.retailRestore(extSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    pvpLoanAppService.updateSelective(pvpLoanApp);
                    pvpLoanAppService.retailRestore(extSerno);
                    lsyw03BizService.sendSgMessage(resultInstanceDto,"退回");
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if(isFirstNode){
                    pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    pvpLoanAppService.updateSelective(pvpLoanApp);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                pvpLoanAppService.updateSelective(pvpLoanApp);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                pvpLoanAppService.updateSelective(pvpLoanApp);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    //SGD02对公放款申请（寿光）
    private void handlerSGD02(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno, String currentUserId, String currentOrgId) {
        log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程操作:" + currentOpType+"后业务处理");
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                if ("648_3".equals(resultInstanceDto.getCurrentNodeId())){
                    pvpLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                }
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程提交操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"流程结束操作，流程参数："+ resultInstanceDto.toString());
                //放款申请通过，调用后续的业务处理过程
                pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
                //微信通知
                String managerId = pvpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpLoanApp.getCusName());
                        paramMap.put("prdName", "一般贷款出账");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"退回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                    //微信通知
                    String managerId = pvpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpLoanApp.getCusName());
                            paramMap.put("prdName", "一般贷款出账");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    this.put2VarParamForPT(resultInstanceDto, pvpLoanApp, pvpSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //针对客户经理拿回操作，应当将业务申请主表的业务状态更新为【打回-992】
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("对公贷款出账申请"+pvpSerno+"拿回初始节点操作，流程参数："+ resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("对公贷款出账申请(寿光)"+pvpSerno+"否决操作，流程参数："+ resultInstanceDto.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                pvpLoanAppService.handleBusinessAfterRefuse(pvpSerno);
            } else {
                log.warn("对公贷款出账申请(寿光)"+pvpSerno+"未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
}
