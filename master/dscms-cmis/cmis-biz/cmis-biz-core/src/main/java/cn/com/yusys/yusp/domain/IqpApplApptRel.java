/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptRel
 * @类描述: iqp_appl_appt_rel数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:23:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_appl_appt_rel")
public class IqpApplApptRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 申请人编号 **/
	@Column(name = "APPT_CODE", unique = false, nullable = true, length = 32)
	private String apptCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 32)
	private String cusId;
	
	/** 配偶编号 **/
	@Column(name = "SPOUSE_APPT_CODE", unique = false, nullable = true, length = 32)
	private String spouseApptCode;
	
	/** 配偶客户编号 **/
	@Column(name = "SPOUSE_CUS_ID", unique = false, nullable = true, length = 21)
	private String spouseCusId;
	
	/** 关系类型 **/
	@Column(name = "REL_TYPE", unique = false, nullable = true, length = 2)
	private String relType;
	
	/** 最后修改时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode;
	}
	
    /**
     * @return apptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param spouseApptCode
	 */
	public void setSpouseApptCode(String spouseApptCode) {
		this.spouseApptCode = spouseApptCode;
	}
	
    /**
     * @return spouseApptCode
     */
	public String getSpouseApptCode() {
		return this.spouseApptCode;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId;
	}
	
    /**
     * @return spouseCusId
     */
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param relType
	 */
	public void setRelType(String relType) {
		this.relType = relType;
	}
	
    /**
     * @return relType
     */
	public String getRelType() {
		return this.relType;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}