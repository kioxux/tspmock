package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CardBusinessDto
 * @类描述: CardBusinessDto数据实体类
 * @功能描述: 
 * @创建人: zsm
 * @创建时间: 2021-06-04 16:30:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CardBusinessDto implements Serializable{
	private static final long serialVersionUID = 1L;

	//卡号
	private String cardNo;

	//币种
	private String currCd;

	//账单起始年月
	private String stmtStartDate;

	//账单截止日期
	private String stmtEndDate;

	//开始位置
	private String firstRow;

	//结束位置
	private String lastRow;

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCurrCd() {
		return currCd;
	}

	public void setCurrCd(String currCd) {
		this.currCd = currCd;
	}

	public String getStmtStartDate() {
		return stmtStartDate;
	}

	public void setStmtStartDate(String stmtStartDate) {
		this.stmtStartDate = stmtStartDate;
	}

	public String getStmtEndDate() {
		return stmtEndDate;
	}

	public void setStmtEndDate(String stmtEndDate) {
		this.stmtEndDate = stmtEndDate;
	}

	public String getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(String firstRow) {
		this.firstRow = firstRow;
	}

	public String getLastRow() {
		return lastRow;
	}

	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}
}