package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.LstklnbDkzhzy;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.req.Lc0323ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespListDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.req.Xddb03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.resp.Xddb03RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.*;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.flow.dto.result.ResultNodeDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.github.pagehelper.PageHelper;
import io.netty.util.internal.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantManageAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:24:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarWarrantManageAppService {

    private final Logger log = LoggerFactory.getLogger(GuarWarrantManageAppService.class);

    @Autowired
    private GuarWarrantManageAppMapper guarWarrantManageAppMapper;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private Dscms2YpqzxtClientService dscms2YpqzxtClientService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;

    @Autowired
    private AdminSmTreeDicClientService adminSmTreeDicClientService;

    @Autowired
    private SequenceTemplateService sequenceTemplateService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private GuarMortgageManageAppMapper guarMortgageManageAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private GuarWarrantManageAppService guarWarrantManageAppService;

    @Autowired
    private GuarMortgageManageRelMapper guarMortgageManageRelMapper;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private IqpGuarChgAppService iqpGuarChgAppService;

    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;

    @Autowired
    private GuarContRelWarrantMapper guarContRelWarrantMapper;

    @Autowired
    private BlankCertiInfoMapper blankCertiInfoMapper;

    @Autowired
    private BlankCertiInfoService blankCertiInfoService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private MessageSendService messageSendService;
    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CentralFileTaskService centralFileTaskService;

    @Autowired
    private GuarMortgageManageAppService guarMortgageManageAppService;

    @Autowired
    private GrtStockDebtRelService grtStockDebtRelService;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;

    @Autowired
    private GuarWarrantInfoMapper guarWarrantInfoMapper;

    @Autowired
    private BizCommonService bizCommonService;

    @Autowired
    private Dscms2LcxtClientService dscms2LcxtClientService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private AccTfLocService accTfLocService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public GuarWarrantManageApp selectByPrimaryKey(String serno) {
        return guarWarrantManageAppMapper.selectByPrimaryKey(serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarWarrantManageApp> selectAll(QueryModel model) {
        List<GuarWarrantManageApp> records = (List<GuarWarrantManageApp>) guarWarrantManageAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarWarrantManageApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarWarrantManageApp> list = guarWarrantManageAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GuarWarrantManageApp record) {
        return guarWarrantManageAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GuarWarrantManageApp record) {
        return guarWarrantManageAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GuarWarrantManageApp record) {
        return guarWarrantManageAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GuarWarrantManageApp record) {
        return guarWarrantManageAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarWarrantManageAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarWarrantManageAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:deleteOnlogic
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    public int deleteOnlogic(String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //退回状态的删除,更改状态为自行退出
        if (CmisCommonConstants.WF_STATUS_992.equals(guarWarrantManageApp.getApproveStatus())) {
            guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
        } else {
            guarWarrantManageApp.setOprType(CmisBizConstants.OPR_TYPE_02);
        }
        return guarWarrantManageAppMapper.updateByPrimaryKeySelective(guarWarrantManageApp);
    }

    /**
     * @函数名称:queryGuarWarrantManageAppDtoBySerno
     * @函数描述:根据流水号查询权证(出入库、续借)申请对象
     * @param serno
     * @return
     */
    public GuarWarrantManageAppDto queryGuarWarrantManageAppDtoBySerno(String serno) {
        GuarWarrantManageAppDto guarWarrantManageAppDto = new GuarWarrantManageAppDto();
        //获取GuarWarrantManageApp表数据
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //把guarWarrantManageApp中的数据复制给guarWarrantManageAppDto
        BeanUtils.copyProperties(guarWarrantManageApp, guarWarrantManageAppDto);
        //获取权证编号
        String warrantNo = guarWarrantManageApp.getWarrantNo();
        if(StringUtils.nonEmpty(warrantNo)){
            GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByWarrantNo(warrantNo);
            guarWarrantManageAppDto.setGuarWarrantInfo(guarWarrantInfo);
        }
        if (guarWarrantManageApp != null) {
            // 登记人
            String inputIdName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());
            // 登记机构
            String inputBrIdName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getInputBrId());
            //责任人
            String userName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getManagerId());
            //责任机构
            String orgName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getManagerBrId());
            guarWarrantManageAppDto.setInputIdName(inputIdName);
            guarWarrantManageAppDto.setInputBrIdName(inputBrIdName);
            guarWarrantManageAppDto.setManagerIdName(userName);
            guarWarrantManageAppDto.setManagerBrIdName(orgName);
        }
        return guarWarrantManageAppDto;
    }

    /**
     * @函数名称:tempSave
     * @函数描述:根据权证(出入库、续借)对象暂存(新增/修改)数据
     * @param guarWarrantManageAppDto
     * @return
     */
    public int tempSave(GuarWarrantManageAppDto guarWarrantManageAppDto) {
        int result;
        //申请流水号
        String serno = guarWarrantManageAppDto.getSerno();
        //权证出入库申请类型
        String warrantAppType = guarWarrantManageAppDto.getWarrantAppType();

        if (CmisBizConstants.STD_WARRANT_APP_TYPE_01.equals(warrantAppType)){
            return tempSave4WarrantIn(guarWarrantManageAppDto);
        }

        //获取权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantManageAppDto.getGuarWarrantInfo();
        //权证编号
        String warrantNo = guarWarrantInfo.getWarrantNo();
        GuarWarrantManageApp guarWarrantManageApp = new GuarWarrantManageApp();

        //复制数据到guarWarrantManageApp
        BeanUtils.copyProperties(guarWarrantManageAppDto, guarWarrantManageApp);
        GuarWarrantManageApp repGuarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        guarWarrantManageAppDto.setWarrantNo(warrantNo);

        //判断新增还是修改数据
        if (repGuarWarrantManageApp == null) {
            log.info("根据权证出库申请流水号【"+serno+"】查不到记录，属于新增操作");
            //插入guarWarrantManageApp表数据
            String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
            String guarContNo = guarWarrantManageAppMapper.selectGuarContNoByCoreGuarantyNo(coreGuarantyNo);

            if (StringUtils.isEmpty(guarContNo)){
                //互联网+不动产登记抵押推给信贷的没有权证入库记录，从担保合同关联押品及权证的关系表查担保合同编号
                List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectByCoreGuarantyNo(coreGuarantyNo);

                if (CollectionUtils.isEmpty(guarContRelWarrants)){
                    log.info("核心担保编号为【"+coreGuarantyNo+"】的权证对应的担保合同编号为空！");
                }else{
                    guarContNo = guarContRelWarrants.get(0).getGuarContNo();
                }
            }

            guarWarrantManageApp.setGuarContNo(guarContNo);
            result = guarWarrantManageAppMapper.insert(guarWarrantManageApp);
        } else {
            log.info("根据权证出入库申请流水号【"+serno+"】可以查到记录，更新该笔记录");
            result = guarWarrantManageAppMapper.updateByPrimaryKeySelective(guarWarrantManageApp);
        }

        return result;
    }

    /**
     * @函数名称:tempSave4WarrantIn
     * @函数描述:根据权证入库对象暂存(新增/修改)数据
     * @param guarWarrantManageAppDto
     * @return
     */
    public int tempSave4WarrantIn(GuarWarrantManageAppDto guarWarrantManageAppDto) {
        int result;
        //申请流水号
        String serno = guarWarrantManageAppDto.getSerno();
        //获取权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantManageAppDto.getGuarWarrantInfo();
        //抵押办理与押品基本信息关联关系列表
        List<GuarContRelWarrant> guarContRelWarrantList = guarWarrantManageAppDto.getGuarContRelWarrantList();

        GuarWarrantManageApp guarWarrantManageApp = new GuarWarrantManageApp();

        //复制数据到guarWarrantManageApp
        BeanUtils.copyProperties(guarWarrantManageAppDto, guarWarrantManageApp);
        GuarWarrantManageApp repGuarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);

        //账务机构
        String finaBrId = guarWarrantManageApp.getManagerBrId();

        if (finaBrId.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            finaBrId = "810100";
        }else if(finaBrId.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            finaBrId = "800100";
        }

        //权证编号
        String warrantNo = "";
        //担保合同编号
        String guarContNo = guarWarrantManageAppDto.getGuarContNo();
        //抵质押标志
        String grtFlag = guarWarrantManageApp.getGrtFlag();

        if (StringUtils.isEmpty(guarContNo)){
            guarContNo = guarWarrantManageAppService.selectByPrimaryKey(serno).getGuarContNo();
        }

        //关联权证编号
        if(guarWarrantInfo != null){
            guarWarrantManageAppDto.setWarrantNo(guarWarrantInfo.getWarrantNo());
            warrantNo = guarWarrantInfo.getWarrantNo();
        }

        //判断新增还是修改数据
        if (repGuarWarrantManageApp == null) {
            log.info("根据权证入库申请流水号【"+serno+"】查不到记录，属于新增操作");
            //插入guarWarrantManageApp表数据
            guarWarrantManageApp.setWarrantNo(warrantNo);
            result = guarWarrantManageAppMapper.insert(guarWarrantManageApp);

            //遍历插入担保合同与押品及权证关联关系表
            for (GuarContRelWarrant guarContRelWarrant : guarContRelWarrantList) {
                //设置权证编号
                guarContRelWarrant.setWarrantNo(warrantNo);
                //设置流水号
                guarContRelWarrant.setSerno(serno);
                //设置担保合同号
                guarContRelWarrant.setGuarContNo(guarContNo);
                //设置核心担保编号
                guarContRelWarrant.setCoreGuarantyNo(guarWarrantManageAppDto.getCoreGuarantyNo());
                guarContRelWarrant.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                result = guarContRelWarrantService.insert(guarContRelWarrant);
            }

            if (guarWarrantInfo != null && !StringUtils.isEmpty(warrantNo)) {
                log.info("权证入库申请流水号【"+serno+"】，查询权证编号【"+warrantNo+"】对应的权证台账信息");

                if(!CmisBizConstants.STD_ZB_GUAR_TYPE_CD_17.equals(guarWarrantManageApp.getGagTyp())){
                    //权证编号唯一性校验
                    checkWarrantNoIsExist(warrantNo);
                }

                if ("02".equals(grtFlag)){
                    //如果抵质押分类是02--质押，则抵押顺位为1--一抵
                    guarWarrantInfo.setMortOrderFlag("1");
                }else{
                    //从担保合同中获取押品顺位标示,存到 WarrantInfo 中,用于集中作业入库模式数据录入数据
                    String pldOrder = getPldOrder(guarContNo);

                    if (StringUtils.isEmpty(pldOrder)){
                        throw BizException.error(null, "","担保合同【"+guarContNo+"】的抵押顺位为空！");
                    }
                    guarWarrantInfo.setMortOrderFlag(pldOrder);
                }

                guarWarrantInfo.setFinaBrId(finaBrId);
                guarWarrantInfo.setInputId(guarWarrantManageApp.getInputId());
                guarWarrantInfo.setInputBrId(guarWarrantManageApp.getInputBrId());
                guarWarrantInfo.setManagerId(guarWarrantManageApp.getManagerId());
                guarWarrantInfo.setManagerBrId(guarWarrantManageApp.getManagerBrId());
                log.info("权证出入库申请流水号【"+serno+"】，权证编号【"+warrantNo+"】对应的权证台账信息插入表里");
                result = guarWarrantInfoService.insert(guarWarrantInfo);
            }
        } else {
            log.info("根据权证入库申请流水号【"+serno+"】可以查到记录，更新该笔记录");

            if (StringUtils.nonEmpty(warrantNo)){
                guarWarrantManageApp.setWarrantNo(warrantNo);
            }

            result = guarWarrantManageAppMapper.updateByPrimaryKeySelective(guarWarrantManageApp);

            if (guarWarrantInfo != null && !StringUtils.isEmpty(warrantNo)) {
                log.info("权证入库申请流水号【"+serno+"】，页面的权证编号有值");
                //从查询出来的申请表中获取权证编号,跟前端传入的权证编号进行比较,用于区分权证编号是否更改过(这里的操作主要是对于入库模式-集中作业入库的区分操作)
                String repWarrantNo = repGuarWarrantManageApp.getWarrantNo();
                //权证台账信息记录里的账务机构取权证出入库续借申请里的主管机构
                guarWarrantInfo.setFinaBrId(finaBrId);

                log.info("权证入库申请流水号【"+serno+"】，warrantNo为【"+warrantNo+"】,repWarrantNo为【"+repWarrantNo+"】");

                if (StringUtils.isEmpty(repWarrantNo)) {
                    if ("02".equals(grtFlag)){
                        //如果抵质押分类是02--质押，则抵押顺位为1--一抵
                        guarWarrantInfo.setMortOrderFlag("1");
                    }else{
                        //从担保合同中获取押品顺位标示,存到 WarrantInfo 中,用于集中作业入库模式数据录入数据
                        String pldOrder = getPldOrder(guarContNo);

                        if (StringUtils.isEmpty(pldOrder)){
                            throw BizException.error(null, "","担保合同【"+guarContNo+"】的抵押顺位为空！");
                        }
                        guarWarrantInfo.setMortOrderFlag(pldOrder);
                    }

                    guarWarrantInfo.setInputId(guarWarrantManageApp.getInputId());
                    guarWarrantInfo.setInputBrId(guarWarrantManageApp.getInputBrId());
                    guarWarrantInfo.setManagerId(guarWarrantManageApp.getManagerId());
                    guarWarrantInfo.setManagerBrId(guarWarrantManageApp.getManagerBrId());

                    if(!CmisBizConstants.STD_ZB_GUAR_TYPE_CD_17.equals(guarWarrantManageApp.getGagTyp())){
                        //权证编号唯一性校验
                        checkWarrantNoIsExist(warrantNo);
                    }

                    result = guarWarrantInfoService.insert(guarWarrantInfo);
                } else {
                    GuarWarrantInfo repGuarWarrInfo = guarWarrantInfoService.selectByWarrantNo(repWarrantNo);
                    guarWarrantInfo.setPkId(repGuarWarrInfo.getPkId());
                    //前端传入权证编号跟数据库中的一样,修改权证信息表
                    String newWarrantNo = guarWarrantManageAppDto.getWarrantNo();

                    if (repWarrantNo.equals(newWarrantNo)) {
                        log.info("前端传入权证编号与数据库中的一致，均为【"+repWarrantNo+"】");
                        result = guarWarrantInfoService.updateSelective(guarWarrantInfo);
                    } else {
                        log.info("前端传入权证编号【"+newWarrantNo+"】与数据库中的【"+repWarrantNo+"】不一致");

                        if(!CmisBizConstants.STD_ZB_GUAR_TYPE_CD_17.equals(guarWarrantManageApp.getGagTyp())){
                            //权证编号唯一性校验
                            checkWarrantNoIsExist(warrantNo);
                        }

                        guarWarrantInfoMapper.deleteByPrimaryKey(repGuarWarrInfo.getPkId());
                        BeanUtils.copyProperties(guarWarrantInfo, repGuarWarrInfo);

                        if (StringUtils.isEmpty(repGuarWarrInfo.getMortOrderFlag())){
                            if ("02".equals(grtFlag)){
                                //如果抵质押分类是02--质押，则抵押顺位为1--一抵
                                repGuarWarrInfo.setMortOrderFlag("1");
                            }else{
                                //从担保合同中获取押品顺位标示,存到 WarrantInfo 中,用于集中作业入库模式数据录入数据
                                String pldOrder = getPldOrder(guarContNo);

                                if (StringUtils.isEmpty(pldOrder)){
                                    throw BizException.error(null, "","担保合同【"+guarContNo+"】的抵押顺位为空！");
                                }
                                repGuarWarrInfo.setMortOrderFlag(pldOrder);
                            }
                        }
                        guarWarrantInfoService.insert(repGuarWarrInfo);
                    }
                }
                //更新担保合同关联押品及权证关系表里的权证编号字段
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno",serno);
                queryModel.addCondition("warrantNo",warrantNo);
                queryModel.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);

                guarContRelWarrantMapper.updateWarrantNoByQueryModel(queryModel);
            }
        }

        return result;
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 权证出入库、续借流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        if (guarWarrantManageApp==null || StringUtils.isEmpty(guarWarrantManageApp.getSerno())){
            return;
        }
        guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 权证出入库、续借流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: //TODO
     * 调用这个方法的流程:
     * DBGL04	权证入库审批流程_纸质权证集中入库模式
     * DBGL05	权证入库审批流程_电子权证手工入库模式
     * DBGL06	权证出库审批流程_非诉讼原因借阅
     * DBGL07	权证出库审批流程_诉讼原因借阅
     * DBGL08	权证出库审批流程_其他
     *
     * 1.将审批状态更新为997 通过
     * 2.
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterEnd(String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
        //权证出入库申请类型 01-入库申请 02-出库申请 03-权证续借
        String warrantAppType = guarWarrantManageApp.getWarrantAppType();

        if(CmisBizConstants.STD_WARRANT_APP_TYPE_01.equals(warrantAppType)){
            warrantIn(serno);

            //核心担保编号
            String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
            //根据权证编号查询权证信息
            GuarWarrantInfo warrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);
            //权利凭证号
            String certiRecordId = warrantInfo.getCertiRecordId();

            BlankCertiInfo blankCertiInfo = blankCertiInfoService.selectByCertiNo(certiRecordId);

            if (blankCertiInfo!=null){
                //是否集中作业打印
                if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(warrantInfo.getIsFocusWorkPrint())){
                    log.info("权证入库流水号【"+serno+"】,核心担保编号【"+coreGuarantyNo+"】对应台账的是否集中作业打印为是，更新空白凭证簿的凭证状态为【已用】");
                    blankCertiInfo.setCertiStatus(CmisBizConstants.STD_ZB_CERTI_STATUS_02);
                }
                //核心担保编号
                blankCertiInfo.setCoreGuarantyNo(coreGuarantyNo);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String inputDate = sdf.format(new Date());
                //使用日期
                blankCertiInfo.setUseDate(inputDate);
                blankCertiInfoMapper.updateByPrimaryKey(blankCertiInfo);
            }
            //权证入库模式
            String warrantInType = guarWarrantManageApp.getWarrantInType();
            if(CmisBizConstants.STD_WARRANT_IN_TYPE_02.equals(warrantInType) || CmisBizConstants.STD_WARRANT_IN_TYPE_04.equals(warrantInType)){
                //纸质权证集中入库模式、电子权证手工入库模式
                MessageSendDto messageSendDto = new MessageSendDto();
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                receivedUserDto.setReceivedUserType("1");
                receivedUserDto.setUserId(warrantInfo.getManagerId());

                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(warrantInfo.getManagerId());
                receivedUserDto.setMobilePhone(byLoginCode.getData().getUserMobilephone());
                receivedUserList.add(receivedUserDto);
                messageSendDto.setMessageType("MSG_DB_M_0001");
                Map<String,String> map = new HashMap<>();

                List<GuarContRelWarrant> list = guarContRelWarrantService.selectBySerno(serno);

                if(CollectionUtils.isNotEmpty(list)){
                    for (GuarContRelWarrant gcrw:list) {
                        map.put("guarNo",gcrw.getGuarNo());
                        messageSendDto.setParams(map);
                        messageSendDto.setReceivedUserList(receivedUserList);
                        messageSendService.sendMessage(messageSendDto);
                    }
                }
            }
        }else if(CmisBizConstants.STD_WARRANT_APP_TYPE_02.equals(warrantAppType)){
            warrantOut(serno);

            MessageSendDto messageSendDto = new MessageSendDto();
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            ReceivedUserDto receivedUserDto = new ReceivedUserDto();
            receivedUserDto.setReceivedUserType("1");
            receivedUserDto.setUserId(guarWarrantManageApp.getManagerId());
            ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(guarWarrantManageApp.getManagerId());
            receivedUserDto.setMobilePhone(byLoginCode.getData().getUserMobilephone());
            receivedUserList.add(receivedUserDto);

            String warrantOutType = guarWarrantManageApp.getWarrantOutTypeSub();
            if ("04".equals(warrantOutType) || "05".equals(warrantOutType)) {
                messageSendDto.setMessageType("MSG_DB_M_0002");
            }else if("99".equals(warrantOutType)) {
                messageSendDto.setMessageType("MSG_DB_M_0003");
            }
            Map<String, String> map = new HashMap<>();
            map.put("warrantNo", guarWarrantManageApp.getWarrantNo());
            map.put("status", "通过");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 权证出入库、续借流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterBack(String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
        String warrantAppType = guarWarrantManageApp.getWarrantAppType();
        String warrantNo = guarWarrantManageApp.getWarrantNo();
        if("02".equals(warrantAppType)){//出库申请
            MessageSendDto messageSendDto = new MessageSendDto();
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            ReceivedUserDto receivedUserDto = new ReceivedUserDto();
            receivedUserDto.setReceivedUserType("1");
            receivedUserDto.setUserId(guarWarrantManageApp.getManagerId());
            ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(guarWarrantManageApp.getManagerId());
            receivedUserDto.setMobilePhone(byLoginCode.getData().getUserMobilephone());
            receivedUserList.add(receivedUserDto);
            if (guarWarrantManageApp.getWarrantOutTypeSub().equals("04")||guarWarrantManageApp.getWarrantOutTypeSub().equals("05")) {
                messageSendDto.setMessageType("MSG_DB_M_0002");
            }else if(guarWarrantManageApp.getWarrantOutTypeSub().equals("99")) {
                messageSendDto.setMessageType("MSG_DB_M_0003");
            }
            Map<String, String> map = new HashMap<>();
            map.put("warrantNo", warrantNo);
            map.put("status", "退回");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 权证出入库、续借流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 打回
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        guarWarrantManageApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
    }

    /**
     * @方法名称: warrantOut
     * @方法描述: 权证出库 1.调co3202接口发核心系统 2.调certis接口将权证状态同步押品系统 3.如果是权证借阅，则调cwm001接口将权证借阅信息同步权证系统;否，调cwm004接口将权证信息同步权证系统
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int warrantOut(String serno) {
        //营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        //根据业务流水号获取权证出库申请信息
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //权证出库原因细类
        String warrantOutTypeSub = guarWarrantManageApp.getWarrantOutTypeSub();
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        //根据核心担保编号获取权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

        //根据权证编号号获取第一条担保合同关联押品及权证关联信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("coreGuarantyNo",coreGuarantyNo);
        List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectByModel(queryModel);

        if (CollectionUtils.isEmpty(guarContRelWarrants)){
            throw new BizException(null, "", null, "根据核心担保编号【"+coreGuarantyNo+"】没有从担保合同与押品及权证关联表里查到记录！");
        }
        GuarContRelWarrant guarContRelWarrant = guarContRelWarrants.get(0);
        //押品编号
        String guarNo = guarContRelWarrant.getGuarNo();

        //根据押品编号获取押品信息
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryBaseInfoByGuarId(guarNo);
//        String gage_type = guarBaseInfo.getGuarTypeCd(); // 抵押物类型
//        gage_type = gage_type.substring(0,4);
//        String dzywzlei = PUBUtilTools.changeGuarTypeCd(gage_type);
//        String yewusx01 = PUBUtilTools.changeCoreDzywzlei(dzywzlei);
        //是否张家港地区不动产
        String is_local = guarBaseInfo.getAreaCode();
        if (!StringUtils.isEmpty(is_local) && is_local.length() > 5) {
            is_local = is_local.substring(0, 6);
        }
        is_local = "320582".equals(is_local) ? "1" : "0";
        //押品类型  1：电子      2：实物
        String isElectronic = "2";
        if ("1".equals(guarWarrantInfo.getIsEWarrant())) {
            isElectronic = "1";
        }
        //是否总行办理注销登记 //TODO 017001总行营业部 待确定
        String iszhRegist = "";
        if (!StringUtil.isNullOrEmpty(guarWarrantInfo.getInputBrId())) {
            iszhRegist = "017001".equals(guarWarrantInfo.getInputBrId()) ? "1" : "2";
        }

        if (!CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_04.equals(warrantOutTypeSub) && !CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_05.equals(warrantOutTypeSub)){
            log.info("流水号【"+serno+"】，不是权证借阅，则发核心");
            sendCo3202(serno,guarWarrantManageApp);
        }

        //2.核心成功后权证状态和押品状态更新
        //2.1更新权证状态
        //权证临时借用人名称
        String qzjyrm = "";
        //权证外借日期			状态为10019-已借阅时，必输
        String qzwjrq = "";
        //预计归还日期			状态为10019-已借阅时，必输
        String yjghrq = "";
        //权证外借原因			状态为10019-已借阅时，必输
        String qzwjyy = "";

        //权证状态
        String qzztyp = "";

        if(CmisBizConstants.STD_WARRANT_OUT_TYPE_04.equals(warrantOutTypeSub) || CmisBizConstants.STD_WARRANT_OUT_TYPE_05.equals(warrantOutTypeSub)){
            log.info("权证出库流水号【"+serno+"】，权证出库原因细类是权证借阅");

            guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_06);//06出借未记账
            qzwjrq = openDay;
            yjghrq = guarWarrantManageApp.getPreBackDate();
            qzjyrm = guarWarrantManageApp.getInputId();

            if (CmisBizConstants.STD_WARRANT_OUT_TYPE_05.equals(guarWarrantManageApp.getWarrantOutTypeSub())) {
                qzwjyy = "09";
            } else {
                qzwjyy = "08";
            }
            qzztyp = "10019";
        } else {
            log.info("权证出入库续借管理申请流水号：" + serno + "的权证出库原因细类不是权证借阅");
            guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_09);//09出库未记账
            //10008-正常出库
            qzztyp = "10008";
        }

        guarWarrantInfo.setOutDate(openDay);
        guarWarrantInfoService.updateSelective(guarWarrantInfo);

        log.info("将核心担保编号为【"+coreGuarantyNo+"】的权证的权证状态改成"+guarWarrantInfo.getCertiState()+";权证正常出库日期改为"+openDay);

        if (CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_04.equals(warrantOutTypeSub) || CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_05.equals(warrantOutTypeSub)) {
            //3.押品系统  权证状态同步
            CertisReqDto certisReqDto = new CertisReqDto();
            List<CertisListInfo> certisListInfo = new ArrayList();
            CertisListInfo certisInfo = new CertisListInfo();

            //权证流水号
            certisInfo.setSernoy(guarWarrantInfo.getCoreGuarantyNo());
            //权利凭证号
            certisInfo.setQlpzhm(guarWarrantInfo.getWarrantNo());
            //权证类型
            certisInfo.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(guarWarrantInfo.getCertiTypeCd()));
            //权证出入库状态
            certisInfo.setQzztyp(qzztyp);
            //权证入库日期 状态为10006-已入库时必输
            certisInfo.setQzrkrq("");
            //权证正常出库日期    状态为10008-正常出库时必输
            certisInfo.setQzckrq(guarWarrantInfo.getOutDate());
            //权证临时借用人名称    状态为10019-已借阅时，必输
            certisInfo.setQzjyrm(qzjyrm);
            //权证外借日期			状态为10019-已借阅时，必输
            certisInfo.setQzwjrq(qzwjrq);
            //预计归还日期			状态为10019-已借阅时，必输
            certisInfo.setYjghrq(yjghrq);
            //权证实际归还日期		状态为10020-外借归还是，必输
            certisInfo.setSjghrq("");
            //权证外借原因			状态为10019-已借阅时，必输
            certisInfo.setQzwjyy(qzwjyy);
            //其他文本输入
            certisInfo.setQtwbsr("");
            certisListInfo.add(certisInfo);
            certisReqDto.setList(certisListInfo);

            log.info("流水号【"+serno+"】，权证出库调押品系统certis进行权证状态同步 开始，请求报文：" + certisReqDto);
            ResultDto<CertisRespDto> certisRespDto = dscms2YpxtClientService.certis(certisReqDto);
            log.info("流水号【"+serno+"】，权证出库调押品系统certis进行权证状态同步 结束，响应报文：" + certisRespDto);

            String code = Optional.ofNullable(certisRespDto.getCode()).orElse(StringUtils.EMPTY);
            String meesage = Optional.ofNullable(certisRespDto.getMessage()).orElse(StringUtils.EMPTY);

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                CertisRespDto data = certisRespDto.getData();
                log.info("流水号【"+serno+"】，权证出库调押品系统certis进行权证状态同步 成功：" + data);
            } else {
                log.error("流水号【"+serno+"】，权证出库调押品系统certis进行权证状态同步 失败：" + meesage);
                throw new BizException(null, "", null, "流水号【"+serno+"】，权证出库调押品系统certis进行权证状态同步 失败："+meesage);
            }

            //登记机构
            String inputBrId = guarWarrantManageApp.getInputBrId();
            String orgName = OcaTranslatorUtils.getOrgName(inputBrId);
            String usrName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());

            //4.发送权证出库信息到权证系统
            Cwm001ReqDto cwm001ReqDto = new Cwm001ReqDto();
            //操作员机构号
            cwm001ReqDto.setOrgcode(inputBrId);
            //操作员机构名称
            cwm001ReqDto.setOrgname(orgName);

            if (!StringUtils.isEmpty(inputBrId) && inputBrId.startsWith("01")) {
                //本地机构以01开头，是否异地支行为0--否
                cwm001ReqDto.setIsyd(CmisCommonConstants.YES_NO_0);
            } else {
                //是否异地支行为1--是
                cwm001ReqDto.setIsyd(CmisCommonConstants.YES_NO_1);
            }

            //权证预计归还时间
            String preBackDate = guarWarrantManageApp.getPreBackDate();
            //归还时间
            cwm001ReqDto.setBacktime(dateFormat(preBackDate));

            String qzSysGageType = guarBaseInfo.getGuarTypeCd();
            //柜员名称
            cwm001ReqDto.setUserName(usrName);

            List<ListArrayInfo> listArrayInfos = new ArrayList<>();
            ListArrayInfo listArrayInfo = new ListArrayInfo();
            //核心担保品编号
            listArrayInfo.setGageId(guarWarrantManageApp.getCoreGuarantyNo());
            //抵质押类型
            String gageType = transferGageTypeDict(qzSysGageType);
            listArrayInfo.setGageType(gageType);

            //抵质押类型名称
            String gageTypeName = getGageTypeNameFromAdminSmTreeDic(qzSysGageType);
            listArrayInfo.setGageTypeName(gageTypeName);

            //权利价值
            listArrayInfo.setMaxAmt(guarWarrantInfo.getCertiAmt());
            //抵押人
            listArrayInfo.setGageUser(guarBaseInfo.getGuarCusName());
            //账务机构
//            String managerBrId = guarWarrantManageApp.getManagerBrId();
//
//            if (managerBrId.startsWith("81")){
//                //东海村镇默认810100--东海村镇银行营业部
//                managerBrId = "810100";
//                managerBrName = "东海村镇银行营业部";
//            }else if(managerBrId.startsWith("80")){
//                //寿光村镇默认800100--寿光村镇银行营业部
//                managerBrId = "800100";
//                managerBrName = "寿光村镇银行营业部";
//            }

//            managerBrId = managerBrId.substring(0, managerBrId.length() - 1) + "1";
            String finaBrId = guarWarrantInfo.getFinaBrId();
            listArrayInfo.setAcctBrch(finaBrId);
            //账务机构名称
            finaBrId = finaBrId.substring(0, finaBrId.length() - 1) + "0";
            listArrayInfo.setAcctBrchName(OcaTranslatorUtils.getOrgName(finaBrId));
            //取件人 非必输
//            listArrayInfo.setFetchUser();
            //取件时间 非必输
//            listArrayInfo.setFetchTime();
            //取件人证件号 非必输
//            listArrayInfo.setFetchCardno();
            //是否住房按揭
            String isMortgage = getIsMortgage(guarWarrantManageApp.getCoreGuarantyNo());
            listArrayInfo.setIsMortgage(isMortgage);
            //库位 TODO
//            listArrayInfo.setLocale();
            //档案盒号 TODO
//            listArrayInfo.setBoxid();
            //抵押物名称
            listArrayInfo.setPawn(guarBaseInfo.getPldimnMemo());
            //是否张家港地区不动产
            listArrayInfo.setIsEstate(is_local);
            //是否电子类押品 1--电子 2--实物
            listArrayInfo.setIsElectronic(isElectronic);
            //备注描述
            listArrayInfo.setRemark("");

            listArrayInfos.add(listArrayInfo);

            cwm001ReqDto.setList(listArrayInfos);

            log.info("流水号【" + serno + "】，调权证系统cwm001接口进行权证借阅出库 开始：" + cwm001ReqDto);
            ResultDto<Cwm001RespDto> cwm001RespResultDto = dscms2YpqzxtClientService.cwm001(cwm001ReqDto);
            log.info("流水号【" + serno + "】，调权证系统cwm001接口进行权证借阅出库 结束：" + cwm001RespResultDto);

            code = Optional.ofNullable(cwm001RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            meesage = Optional.ofNullable(cwm001RespResultDto.getMessage()).orElse(StringUtils.EMPTY);

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                Cwm001RespDto data = cwm001RespResultDto.getData();
                log.info("流水号【" + serno + "】，调权证系统cwm001接口进行权证借阅出库 成功：" + data);
            } else {
                log.error("流水号【" + serno + "】，调权证系统cwm001接口进行权证借阅出库 失败：" + meesage);
                throw new BizException(null, "", null, "流水号【" + serno + "】，调权证系统cwm001接口进行权证借阅出库 失败：" + meesage);
            }
        }

        return 1;
    }
    /**
     * @方法名称: warrantIn
     * @方法描述: 权证入库 1.调co3200接口进行押品开户 2.调cetinf接口将权证信息同步押品系统 3.调certis接口将权证状态同步押品系统 4.调cwm003接口将权证信息同步权证系统
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int warrantIn(String serno) {
        //营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        //根据流水获取申请信息
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //权证编号
        String warrantNo = guarWarrantManageApp.getWarrantNo();
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        //根据核心担保编号查询权证信息
        GuarWarrantInfo warrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

        if(warrantInfo==null){
            throw new BizException(null, "", null, "根据核心担保编号【"+coreGuarantyNo+"】没有从权证台账表里查到记录！");
        }

        List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectBySerno(serno);

        if (CollectionUtils.isEmpty(guarContRelWarrants)){
            throw new BizException(null, "", null, "根据权证入库流水号【"+serno+"】没有从guar_cont_rel_warrant表里查到记录！");
        }
        GuarContRelWarrant guarContRelWarrant = guarContRelWarrants.get(0);
        //押品编号
        String guarNo = guarContRelWarrant.getGuarNo();
        GuarBaseInfo guarBaseInfoDto = guarBaseInfoService.queryBaseInfoByGuarId(guarNo);

        if (CmisBizConstants.STD_ZB_GUAR_TYPE_CD_28.equals(guarWarrantManageApp.getGagTyp())){
            log.info("权证入库,抵质押物种类是28--本行理财产品");
            //担保分类代码
            String guarTypeCd = guarBaseInfoDto.getGuarTypeCd();
            if ("ZY9901001".equals(guarTypeCd) || "ZY9901002".equals(guarTypeCd)){
                log.info("权证入库,押品【"+guarNo+"】的担保分类代码是【"+guarTypeCd+"】,属于我行理财产品");
                boolean isFreeze = checkFinancialPrdIsFreeze(guarBaseInfoDto, serno);

                if (!isFreeze){
                    throw new BizException(null, "", null, "该理财产品未被冻结！");
                }
            }
        }

        log.info("权证入库调co3200接口进行押品开户 开始，流水号【"+serno+"】");
        sendCo3200(guarWarrantManageApp,warrantInfo,guarBaseInfoDto,serno);
        log.info("权证入库调co3200接口进行押品开户 结束，流水号【"+serno+"】");

        //核心成功后权证状态和押品状态更新
        //更新权证状态 03--入库未记账
        warrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_03);
        warrantInfo.setInDate(openDay);
        log.info("权证编号【"+warrantNo+"】的权证状态改成【03】--入库未记账");
        guarWarrantInfoService.updateSelective(warrantInfo);
        //存入核心担保编号
        guarBaseInfoDto.setCoreGuarantyNo(coreGuarantyNo);
        guarBaseInfoService.updateSelective(guarBaseInfoDto);

        if (CmisBizConstants.STD_WARRANT_IN_TYPE_03.equals(guarWarrantManageApp.getWarrantInType())){
            //03-纸质权证柜面入库模式只需要发送核心
            return 1;
        }

        //3.权证入库权证信息发送押品，权证状态同步给押品系统
        //3.1权证信息同步押品系统
        log.info("权证入库调cetinf接口将权证信息同步押品系统 开始，流水号【"+serno+"】");
        sendCetinf(guarWarrantManageApp,warrantInfo,guarContRelWarrants,openDay,serno);
        log.info("权证入库调cetinf接口将权证信息同步押品系统 结束，流水号【"+serno+"】");

        //3.2权证状态同步押品
        log.info("权证入库调certis接口将权证状态同步押品系统 开始，流水号【"+serno+"】");
        sendCertis(warrantInfo,openDay,serno);
        log.info("权证入库调certis接口将权证状态同步押品系统 结束，流水号【"+serno+"】");

        //4.通知押品权证入库 cwm003
        //发送权证信息到权证系统
        log.info("权证入库调cwm003接口将权证信息同步权证系统 开始，流水号【"+serno+"】");
        sendCwm003(guarWarrantManageApp,guarBaseInfoDto,warrantInfo,openDay,serno);
        log.info("权证入库调cwm003接口将权证信息同步权证系统 结束，流水号【"+serno+"】");

        return 1;
    }

    /**
     * @方法名称: warrantRenew
     * @方法描述: 权证续借
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhengfq
     * @创建时间: 2021-05-19
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int warrantRenew(String serno) {
        //根据业务流水号获取权证出库申请信息
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //根据权证编号获取权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByWarrantNo(guarWarrantManageApp.getWarrantNo());
        //根据押品编号获取押品信息
//        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryBaseInfoByGuarId(guarWarrantManageApp.getGuarNo());
        //TODO
        return 1;
    }

    /**
     * @方法名称: riskItem0035
     * @方法描述: 权证出库校验权证关联合同状态
     * @参数与返回说明:
     * @算法描述: 押品关联合同为“非结清状态时”，选择结清出库需要拦截
     * @创建人: dumingdi
     * @创建时间: 2021-7-13 13:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0035(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = selectByPrimaryKey(serno);
        //权证出库原因大类
        String warrantOutType = guarWarrantManageApp.getWarrantOutType();
        //权证出库原因细类
        String warrantOutTypeSub = guarWarrantManageApp.getWarrantOutTypeSub();

        log.info("riskItem0035--权证出库校验押品关联合同状态,权证出库原因大类："+warrantOutType+";权证出库原因细类:"+warrantOutTypeSub);
        //担保合同编号
        String guarContNo = guarWarrantManageApp.getGuarContNo();

        if (StringUtils.isEmpty(guarContNo)){
            log.info("核心担保编号为【"+guarWarrantManageApp.getCoreGuarantyNo()+"】的权证对应的担保合同编号为空！");
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }

        //查询权证对应的未结清贷款台账记录数
        int count = accLoanService.countUsedRecordsByGuarContNo(guarContNo);

        if ("01".equals(warrantOutType)){
            //权证出库原因大类是01--结清出库
            if (count>0){
                //权证出库原因是结清出库时，押品关联合同必须为结清状态
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0035_01);
                return riskResultDto;
            }

            //权证出库时，除了检查担保合同本身关联的主全合同结清、 纳入担保范围的借款合同也必须结清
            String contNos = grtStockDebtRelService.selectContNosByGuarContNo(guarContNo);

            if (StringUtils.nonEmpty(contNos)){
                String[] contNosArr = contNos.split(",");

                for (String contNo : contNosArr) {
                    int record = accLoanService.countUsedRecordsByContNo(contNo);
                    if (record>0){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc("结清出库时，纳入担保范围的借款合同【"+contNo+"】必须为非结清状态！");
                        return riskResultDto;
                    }
                }
            }
        }else{
            //查询权证对应的贷款台账记录数
            int totalCount = accLoanService.countAccLoanRecordsByGuarContNo(guarContNo);

            if(count==0 && !"99".equals(warrantOutTypeSub) && totalCount>0){
                //权证出库原因是未结清出库且权证出库原因细类不是99--其他时，押品关联合同必须为非结清状态
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0035_02);
                return riskResultDto;
            }else if (count==0 && "99".equals(warrantOutTypeSub)){
                //权证出库原因细类是99--其他时,查询借款台账状态是已核销或转让的记录数
                int record = grtGuarBizRstRelService.countCtrLoanContRecordsByGuarContNo(guarContNo);
                if (record==0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                }

                int records = accLoanService.countHxAndZrAccLoanRecords(guarContNo);
                if (records==0){
                    //如果关联的借款台账状态既没有已核销也没有转让，则拦截
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0035_03);
                    return riskResultDto;
                }
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0036
     * @方法描述: 权证更新出库时校验产品编号
     * @参数与返回说明:
     * @算法描述: 权证更新出库时,产品必须是个人一手住房按揭贷款、个人一手商用房按揭贷款
     * @创建人: dumingdi
     * @创建时间: 2021-7-13 13:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0036(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = selectByPrimaryKey(serno);
        //权证出库原因细类
        String warrantOutTypeSub = guarWarrantManageApp.getWarrantOutTypeSub();

        log.info("riskItem0036--权证更新出库时校验产品编号,权证出库原因细类:"+warrantOutTypeSub);

        if (CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_03.equals(warrantOutTypeSub)){
            //担保合同编号
            String guarContNo = guarWarrantManageApp.getGuarContNo();

            if (StringUtils.isEmpty(guarContNo)){
                log.info("核心担保编号为【"+guarWarrantManageApp.getCoreGuarantyNo()+"】的权证对应的担保合同编号为空！");
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            //权证出库原因细类是03--权证更新出库，查询权证对应的产品是个人一手住房按揭贷款、个人一手商用房按揭贷款的贷款台账记录数
            int count = accLoanService.countFirstHouseAccLoanRecords(guarContNo);

            if (count>0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0036);
                return riskResultDto;
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0037
     * @方法描述: 权证出库校验押品关联合同是否存在在途出账
     * @参数与返回说明:
     * @算法描述: 权证出库原因是结清出库，押品对应合同不能有在途的出账申请
     * @创建人: dumingdi
     * @创建时间: 2021-7-13 13:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0037(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = selectByPrimaryKey(serno);
        //权证出库原因大类
        String warrantOutType = guarWarrantManageApp.getWarrantOutType();

        if ("01".equals(warrantOutType)){
            //担保合同编号
            String guarContNo = guarWarrantManageApp.getGuarContNo();

            if (StringUtils.isEmpty(guarContNo)){
                log.info("核心担保编号为【"+guarWarrantManageApp.getCoreGuarantyNo()+"】的权证对应的担保合同编号为空！");
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            //权证出库原因大类是01--结清出库，查询担保合同对应合同的是否存在在途的出账申请
            int count = pvpLoanAppService.countOnTheWayPvpLoanAppRecords(guarContNo);

            if (count>0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0037);
                return riskResultDto;
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0046
     * @方法描述: 纸质权证集中入库校验权证信息是否录入
     * @参数与返回说明:
     * @算法描述: 纸质权证集中入库校验权证信息是否录入
     * @创建人: dumingdi
     * @创建时间: 2021-7-21 14:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0046(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = selectByPrimaryKey(serno);
        //权证入库模式
        String warrantInType = guarWarrantManageApp.getWarrantInType();

        log.info("riskItem0046--纸质权证集中入库校验权证信息是否录入,权证入库模式:"+warrantInType);

        if (CmisBizConstants.STD_WARRANT_IN_TYPE_02.equals(warrantInType)){
            //如果权证入库模式是02--纸质权证集中入库模式，则检查权证编号是否有值

            //权证编号
            String warrantNo = guarWarrantManageApp.getWarrantNo();

            if (StringUtils.isEmpty(warrantNo)){
                //权证编号为空
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_046);
                return riskResultDto;
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0077
     * @方法描述: 权证置换出库校验押品关联担保合同状态
     * @参数与返回说明:
     * @算法描述: 权证置换出库校验押品关联担保合同状态,如果关系不是解除则拦截
     * @创建人: dumingdi
     * @创建时间: 2021-8-11 09:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0077(String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        GuarWarrantManageApp guarWarrantManageApp = selectByPrimaryKey(serno);
        //权证出库原因细类
        String warrantOutTypeSub = guarWarrantManageApp.getWarrantOutTypeSub();

        if(CmisBizConstants.STD_WARRANT_OUT_TYPE_SUB_02.equals(warrantOutTypeSub)){
            //02--押品置换出库
            String guarContNo = guarWarrantManageApp.getGuarContNo();

            if (StringUtils.isEmpty(guarContNo)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }

            //校验权证对应押品关联的担保合同已无关联的主合同，如有则校验权证对应押品关联的担保合同所担保主合同项下的用信敞口余额为零
            String result = checkGuarContIsRelCont(guarContNo);

            if(StringUtils.nonEmpty(result)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(result);
                return riskResultDto;
            }
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
            return riskResultDto;
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 检查押品是否有在途的权证入库申请
     * @param guarNo
     * @return
     */
    public String checkGuarNoIsOnTheWay(String guarNo){
        int count = guarWarrantManageAppMapper.countOnTheWayRecordsByGuarNo(guarNo);

        if (count>0){
            return "exist";
        }
        return "noExist";
    }

    /**
     * 查询核心担保编号在途的权证出库申请记录数
     * @param coreGuarantyNo
     * @return
     */
    public String checkCoreGuarantyNoIsOnTheWay(String coreGuarantyNo){
        int count = guarWarrantManageAppMapper.countOnTheWayRecordsByCoreGuarantyNo(coreGuarantyNo);

        if (count>0){
            return "exist";
        }
        return "noExist";
    }

    /**
     * 将形如2021-07-01转换为形如2021/7/1的格式
     * @param preBackDate
     * @return
     */
    public String dateFormat(String preBackDate){

        if (!StringUtils.isEmpty(preBackDate)){
            preBackDate = preBackDate.replaceAll("-","/");

            if (preBackDate.charAt(5)=='0'){
                preBackDate = preBackDate.substring(0,5)+preBackDate.substring(6);
            }

            if (preBackDate.charAt(7)=='0'){
                preBackDate = preBackDate.substring(0,7)+preBackDate.substring(8);
            }
        }

        return preBackDate;
    }

    /**
     * 获取抵质押物名称
     * @param gageType
     * @return
     */
    public String getGageTypeNameFromAdminSmTreeDic(String gageType){
        AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
        adminSmTreeDicDto.setOptType("STD_DZY_TYPE");
        adminSmTreeDicDto.setCode(gageType);
        List<Map<String, String>> listMap = adminSmTreeDicClientService.querySingle(adminSmTreeDicDto).getData();
        if(listMap != null && listMap.size()>0){
            return listMap.get(0).get("label");
        }
        return "";
    }

    /**
     * 从担保合同中获取押品顺位标示
     * @param guarContNo
     * @return
     */
    public String getPldOrder(String guarContNo){
        QueryModel queryModel = new QueryModel();
        queryModel.setSort("guarContNo asc");
        queryModel.addCondition("guarContNo",guarContNo);
        queryModel.addCondition("guarContState","101");
        //这里有个奇怪的逻辑，选取创建日期最早且生效的担保合同
        GrtGuarCont grtGuarCont = null;
        List<GrtGuarCont> list = grtGuarContService.selectByModel(queryModel);
        if(list!=null && list.size()>0){
            grtGuarCont =list.get(0);
        }else{
            throw BizException.error(null, EcbEnum.IQP_GBRA_REL_WARRANT_EXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_REL_WARRANT_EXISTS_EXCEPTION.value);
        }
        return grtGuarCont.getPldOrder();
    }

    /**
     * 抵质押类型转换：押品转信贷，然后信贷转权证
     * @param qzSysGageType
     * @return
     */
    public String transferGageTypeDict(String qzSysGageType){
        //押品转信贷
        String gageType = CmisBizConstants.gageTypeMap.get(qzSysGageType);

        //抵押
        if (gageType==null){
            gageType = qzSysGageType;
        }else if(gageType.equals("10001")){
            gageType="DZY002";
        }else if(gageType.equals("10002")){
            gageType="DZY005";
        }else if(gageType.equals("20001")){
            gageType="DZY007";
        }else if(gageType.equals("20002")){
            gageType="DZY007";
        }else if(gageType.equals("10003")){
            gageType="DZY001";
        }else if(gageType.equals("10004")){
            gageType="DZY006";
        }else if(gageType.equals("10005")){
            gageType="DZY004";
        }else if(gageType.equals("10006")){
            gageType="DZY006";
        }else if(gageType.equals("10007")){
            gageType="DZY003";
        }else if(gageType.equals("10008")){
            gageType="DZY007";
        }else if(gageType.equals("10009")){
            gageType="DZY007";
        }else if(gageType.equals("10010")){
            gageType="DZY007";
        }
        else if(gageType.equals("10040")){
            gageType="DZY027";//不动产
        }
        //质押
        else if(gageType.equals("10024")){
            gageType="DZY008";
        }else if(gageType.equals("10025")){
            gageType="DZY008";
        }else if(gageType.equals("10011")){
            gageType="DZY019";
            //押品改造后，10012仅代表债券，国债变更为10042
        }else if(gageType.equals("10012")||gageType.equals("10042")){
            gageType="DZY011";
        }else if(gageType.equals("10013")){
            gageType="DZY019";
        }else if(gageType.equals("10014")){
            gageType="DZY008";
        }else if(gageType.equals("10015")){
            gageType="DZY013";
        }else if(gageType.equals("10016")){
            gageType="DZY015";
        }else if(gageType.equals("10017")){
            gageType="DZY012";
        }else if(gageType.equals("10018")){
            gageType="DZY017";
        }else if(gageType.equals("10019")){
            gageType="DZY019";
        }else if(gageType.equals("10021")){
            gageType="DZY019";
        }else if(gageType.equals("10022")){
            gageType="DZY016";
        }else if(gageType.equals("10023")){
            gageType="DZY016";
        }else if(gageType.equals("10028")){
            gageType="DZY025";
        }else if(gageType.equals("10029")){
            gageType="DZY016";
        }else if(gageType.equals("10026")){
            gageType="DZY016";
        }else if(gageType.equals("10030")){
            gageType="DZY020";
        }else if(gageType.equals("10031")){
            gageType="DZY021";
        }else if(gageType.equals("10032")){
            gageType="DZY022";
        }else if(gageType.equals("10033")){
            gageType="DZY023";
        }else if(gageType.equals("10034")){
            gageType="DZY024";
        }else if(gageType.equals("10041")){
            gageType="DZY004";
        }

        return gageType;
    }

    /**
     * 权证借阅调co3202接口发核心系统
     * @param serno
     * @param guarWarrantManageApp
     */
    public void sendCo3202(String serno,GuarWarrantManageApp guarWarrantManageApp){
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        //根据权证编号获取权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

        if (guarWarrantInfo==null){
            throw new BizException(null, "", null, "根据核心担保编号【"+coreGuarantyNo+"】没有从权证台账表里查到记录！");
        }

        //根据权证编号获取第一条担保合同关联押品及权证关联信息
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("coreGuarantyNo",coreGuarantyNo);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<GuarContRelWarrant> guarContRelWarrants = guarContRelWarrantService.selectByModel(queryModel);

        if (CollectionUtils.isEmpty(guarContRelWarrants)){
            throw new BizException(null, "", null, "根据核心担保编号【"+coreGuarantyNo+"】没有从担保合同与押品及权证关联表里查到记录！");
        }
        GuarContRelWarrant guarContRelWarrant = guarContRelWarrants.get(0);

        //押品编号
        String guarNo = guarContRelWarrant.getGuarNo();
        //根据押品编号获取押品信息
        GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryBaseInfoByGuarId(guarNo);

        //转换处理
        //权证出入库类型信贷1结清出库2非结清出库--01-押品部分出库02-押品置换出库03-权证更新出库04-权证借阅（诉讼）05-权证借阅（非诉讼）99-其他
        //权证出入库类型核心1--临时出库2--结清出库3--强制出库4--再回库
        String chrkleix = "2";

        //货币代号
        String curType = guarBaseInfo.getCurType();//币种 需要转换新核心对应码值
        curType = PUBUtilTools.currencyType(curType);
//        String gage_type = guarBaseInfo.getGuarTypeCd(); // 抵押物类型
//        gage_type = gage_type.substring(0,4);
//        String dzywzlei = PUBUtilTools.changeGuarTypeCd(gage_type);
//        String yewusx01 = PUBUtilTools.changeCoreDzywzlei(dzywzlei);

        //发核心系统,账务机构处理
        String finaBrId = bizCommonService.getFinaBrNoByManagerBrId(guarWarrantManageApp.getManagerBrId());
        log.info("管理机构【"+guarWarrantManageApp.getManagerBrId()+"】的账务机构是【"+finaBrId+"】");

        //1.发送核心系统:抵质押物出库处理
        Co3202ReqDto co3202ReqDto = new Co3202ReqDto();
        //业务操作标志 1录入2复核3直通
        co3202ReqDto.setDaikczbz("1");
        //抵质押物编号
        co3202ReqDto.setDzywbhao(coreGuarantyNo);
        //抵质押方式 信贷01抵押02质押>>>核心1抵押2质押3抵质押组4保证
        co3202ReqDto.setDizyfshi("01".equals(guarBaseInfo.getGrtFlag()) ? "1" : "2");
        //出入库类型
        co3202ReqDto.setChrkleix(chrkleix);
        //所有权人客户号
        co3202ReqDto.setSyqrkehh(guarBaseInfo.getGuarCusId());
        //所有权人客户名
        co3202ReqDto.setSyqrkehm(guarBaseInfo.getGuarCusName());
        //入账机构
        co3202ReqDto.setRuzjigou(finaBrId);
        //货币代号
        co3202ReqDto.setHuobdhao(curType);
        //名义价值
        co3202ReqDto.setMinyjiaz(guarWarrantInfo.getCertiAmt());
        //实际价值
        co3202ReqDto.setShijjiaz(guarWarrantInfo.getCertiAmt());
        //抵质押比率
        co3202ReqDto.setDizybilv(guarBaseInfo.getMortagageRate());
        //可用金额
        co3202ReqDto.setKeyongje(null);
        //生效日期
        String certiStartDate = guarWarrantInfo.getCertiStartDate();
        if (!StringUtils.isEmpty(certiStartDate)){
            co3202ReqDto.setShengxrq(certiStartDate.replace("-", "").substring(0,8));
        }
        //到期日期
        String certiEndDate = guarWarrantInfo.getCertiEndDate();
        if (!StringUtils.isEmpty(certiEndDate)){
            co3202ReqDto.setDaoqriqi(certiEndDate.replace("-", "").substring(0,8));
        }

        log.info("流水号【"+serno+"】，权证出库发核心系统开始，请求报文："+co3202ReqDto);
        ResultDto<Co3202RespDto> co3202RespDtoResultDto = dscms2CoreCoClientService.co3202(co3202ReqDto);
        log.info("流水号【"+serno+"】，权证出库发核心系统结束，响应报文："+co3202RespDtoResultDto);

        String code = Optional.ofNullable(co3202RespDtoResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(co3202RespDtoResultDto.getMessage()).orElse(StringUtils.EMPTY);
        Co3202RespDto co3202RespDto1;

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            co3202RespDto1 = co3202RespDtoResultDto.getData();
            guarWarrantManageApp.setHxSerno(co3202RespDto1.getJiaoyils());
            guarWarrantManageApp.setHxDate(co3202RespDto1.getJiaoyirq());
            guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
            log.info("流水号【"+serno+"】，核算:权证出库 成功" + co3202RespDto1);
        } else {
            throw new BizException(null, "", null, "流水号【"+serno+"】，核算:权证出库 失败" + meesage);
        }
    }

    /**
     * 权证入库，调co3200接口进行押品开户
     * @param guarWarrantManageApp
     * @param warrantInfo
     * @param guarBaseInfoDto
     * @param serno
     */
    public void sendCo3200(GuarWarrantManageApp guarWarrantManageApp,GuarWarrantInfo warrantInfo,GuarBaseInfo guarBaseInfoDto,String serno){
        //信贷与核心抵质押种类对应
        String guarTypeCd = guarBaseInfoDto.getGuarTypeCd(); // 抵押物类型
        //记账余额属性
        String yewusx01 = guarWarrantManageAppService.transferGageTypeDict(guarTypeCd);
//        gage_type = gage_type.substring(0,4);
//        String dzywzlei = PUBUtilTools.changeGuarTypeCd(gage_type);
//        String yewusx01 = PUBUtilTools.changeCoreDzywzlei(dzywzlei);
        String cusID = guarBaseInfoDto.getGuarCusId(); //客户号
        String cusName = guarBaseInfoDto.getGuarCusName(); //客户名称
//        String curType = guarBaseInfoDto.getCurType();//币种 需要转换新核心对应码值
//        curType = PUBUtilTools.currencyType(curType);

        //抵质押物标志
        String grtFlag = guarBaseInfoDto.getGrtFlag();

        //发核心系统,账务机构处理
        String finaBrId = warrantInfo.getFinaBrId();

        if (finaBrId.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            finaBrId = "810100";
        }else if(finaBrId.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            finaBrId = "800100";
        }
        finaBrId = finaBrId.substring(0, finaBrId.length()-1) +"1";
        //1.权证入库发送核心（押品开户）
        Co3200ReqDto co3200ReqDto = new Co3200ReqDto();
        //开户操作标志  1--录入 2--修改 3--复核 4--直通
        if (CmisBizConstants.STD_WARRANT_IN_TYPE_03.equals(guarWarrantManageApp.getWarrantInType())){
            //权证入库（纸质权证柜面入库模式）,开户操作标志取1--录入
            log.info("权证入库流水号【"+serno+"】，权证入库模式是03--纸质权证柜面入库模式，发核销的开户操作标志送1--录入");
            co3200ReqDto.setDkkhczbz("1");
        }else{
            co3200ReqDto.setDkkhczbz("4");
            log.info("权证入库流水号【"+serno+"】，权证入库模式不是纸质权证柜面入库模式，发核销的开户操作标志送4--直通");
        }

        //权利金额
        BigDecimal certiAmt = warrantInfo.getCertiAmt();

        //抵质押物编号(yp号，核心担保编号bookSerno)
        co3200ReqDto.setDzywbhao(guarWarrantManageApp.getCoreGuarantyNo());
        //抵质押物名称
        co3200ReqDto.setDzywminc(guarBaseInfoDto.getPldimnMemo());
        //营业机构
        co3200ReqDto.setYngyjigo(finaBrId);
        //抵质押物种类
        co3200ReqDto.setDzywzlei(guarWarrantManageApp.getGagTyp());
        //核心抵质押方式1--抵押 2--质押 3--抵质押组 4--保证
        //(信贷系统) 01 抵押 02 质押
        co3200ReqDto.setDizyfshi("01".equals(grtFlag) ? "1" : "2");
        //受益人客户号
        co3200ReqDto.setSyrkhhao(cusID);
        //受益人客户名
        co3200ReqDto.setSyrkhmin(cusName);
        //所有权人客户号
        co3200ReqDto.setSyqrkehh(cusID);
        //所有权人客户名
        co3200ReqDto.setSyqrkehm(cusName);
        //货币代号 默认01--人民币
        co3200ReqDto.setHuobdhao("01");
        //名义价值取权证台账信息里的权利金额
//        C03200reqDto.setMinyjiaz(guarBaseInfoDto.getEvalAmt());
        co3200ReqDto.setMinyjiaz(certiAmt);
        //实际价值取权证台账信息里的权利金额
//        C03200reqDto.setShijjiaz(guarBaseInfoDto.getEvalAmt());
        co3200ReqDto.setShijjiaz(certiAmt);
        //评估价值取权证台账信息里的权利金额
//        C03200reqDto.setPingjiaz(guarBaseInfoDto.getEvalAmt());
        co3200ReqDto.setPingjiaz(certiAmt);
        //生效日期
        String certiStartDate = warrantInfo.getCertiStartDate();
        if (!StringUtils.isEmpty(certiStartDate)){
            co3200ReqDto.setShengxrq(certiStartDate.replace("-", ""));
        }
        //到期日期
        String certiEndDate = warrantInfo.getCertiEndDate();
        if (!StringUtils.isEmpty(certiEndDate)){
            co3200ReqDto.setDaoqriqi(certiEndDate.replace("-", ""));
        }

        //权证编号
        String warrantNo = warrantInfo.getWarrantNo();
        //关联业务编号  --传权证号(理财传理财产品代码)
        co3200ReqDto.setGlywbhao(warrantNo);
        //记账余额属性
        co3200ReqDto.setYewusx01(yewusx01);

        if(CmisBizConstants.STD_ZB_GUAR_TYPE_CD_17.equals(guarWarrantManageApp.getGagTyp())){
            //存单需要送账户质押明细
            List<LstklnbDkzhzy> lstklnb_dkzhzy = new ArrayList<>();
            LstklnbDkzhzy lstklnbDkzhzy = new LstklnbDkzhzy();

            //是否本行账号 默认为1--是
            lstklnbDkzhzy.setShfbhzhh("1");
            //客户账号 取权证编号
            lstklnbDkzhzy.setKehuzhao(warrantNo);
            //客户名称 取押品所有人客户名称
            lstklnbDkzhzy.setKehmingc(cusName);
            //质押方式 默认1--金额冻结
            lstklnbDkzhzy.setZhjzhyfs("1");
            //调查询存单票据信息xddb03接口获取客户账号子序号
            Xddb03RespDto xddb03RespDto = xddb03(guarBaseInfoDto, serno);

            //客户账号子序号
            lstklnbDkzhzy.setKhzhhzxh(xddb03RespDto.getAccount_num());
            //质押金额
            lstklnbDkzhzy.setZhiyajee(xddb03RespDto.getOrigin_amt());
            lstklnb_dkzhzy.add(lstklnbDkzhzy);
            co3200ReqDto.setLstklnb_dkzhzy(lstklnb_dkzhzy);
        }

//        String khzhhzxh = imp.getStringByCondition("select t.account_num from t_inf_depo_receipt@CMS t where t.guar_no in (select guaranty_id from Grt_Gp_Inout_Details where book_serno ='"+book_serno+"')",connection);

        //下面是判断是否本行账号的
//        String shfbhzhh = "";
//        String newcode = "select newcode from Grt_P_Basic_Info where guaranty_id in (select guaranty_id from Grt_Gp_Inout_Details where book_serno ='"+book_serno+";
//        if("A10101".equals(newcode) || "A10102".equals(newcode) || "ZY0102001".equals(newcode) || "ZY0102002".equals(newcode)){
//            shfbhzhh = "1";
//        }else{
//            shfbhzhh = "0";
//        }

        log.info("流水号【"+serno+"】，权证入库调核心系统co3200接口进行抵质押物开户 开始，请求报文："+ co3200ReqDto);
        ResultDto<Co3200RespDto> co3200RespDto = dscms2CoreCoClientService.co3200(co3200ReqDto);
        log.info("流水号【"+serno+"】，权证入库调核心系统co3200接口进行抵质押物开户 结束，响应报文："+ co3200RespDto);

        String code = Optional.ofNullable(co3200RespDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(co3200RespDto.getMessage()).orElse(StringUtils.EMPTY);

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            Co3200RespDto data = co3200RespDto.getData();
            log.info("流水号【"+serno+"】，权证入库调核心系统co3200接口进行抵质押物开户 成功："+ data);

            guarWarrantManageApp.setHxSerno(data.getJiaoyils());
            guarWarrantManageApp.setHxDate(data.getJiaoyirq());
            guarWarrantManageAppMapper.updateByPrimaryKey(guarWarrantManageApp);
            log.info("流水号【"+serno+"】，将权证入库记录里的核心抵质押品出入库返回交易流水改为【"+data.getJiaoyils()+"】，核心抵质押品出入库返回交易日期改为【"+data.getJiaoyirq()+"】");
        } else {
            log.error("流水号【"+serno+"】，权证入库调核心系统co3200接口进行抵质押物开户 失败："+meesage);
            throw new BizException(null,"",null,"流水号【"+serno+"】，权证入库调核心系统co3200接口进行抵质押物开户 失败："+meesage);
        }
    }

    /**
     * 权证入库调cetinf接口将权证信息同步押品系统
     * @param guarWarrantManageApp
     * @param warrantInfo
     * @param guarContRelWarrants
     * @param serno
     */
    public void sendCetinf(GuarWarrantManageApp guarWarrantManageApp,GuarWarrantInfo warrantInfo,List<GuarContRelWarrant> guarContRelWarrants,String inputDate,String serno){
        try {
            CetinfReqDto reqDto = new CetinfReqDto();
            //担保合同编号
            reqDto.setDbhtbh(guarWarrantManageApp.getGuarContNo());
            //核心担保编号/抵押登记编号YP号
            reqDto.setSernoy(warrantInfo.getCoreGuarantyNo());
            //抵押顺位标识
            reqDto.setDyswbs(warrantInfo.getMortOrderFlag());
            //权利凭证号
            reqDto.setQlpzhm(warrantInfo.getWarrantNo());
            //权证类型
            String certiTypeCd = warrantInfo.getCertiTypeCd();
            reqDto.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(certiTypeCd));
            //权证发证机关名称
            reqDto.setQzfzjg(warrantInfo.getCertiOrgName());
            //权证发证日期
            reqDto.setQzffrq(warrantInfo.getCertiStartDate());
            //权证到期日期
            reqDto.setQzdqrq(warrantInfo.getCertiEndDate());
            //权利金额
            reqDto.setQljeyp(warrantInfo.getCertiAmt().toString());
            //权证状态
            reqDto.setQzztyp("10006");
            //登记人
            reqDto.setDjrmyp(warrantInfo.getInputId());
            //登记机构
            reqDto.setDjjgyp(warrantInfo.getInputBrId());
            //登记日期
            reqDto.setDjrqyp(inputDate);
            //操作
            reqDto.setOperat("01");
            //押品编号
            List<CetinfListInfo> cetinfListInfo = new ArrayList<>();

            for (GuarContRelWarrant guarContRelWarrant : guarContRelWarrants) {
                String guarNo = guarContRelWarrant.getGuarNo();
                CetinfListInfo cetinfInfo = new CetinfListInfo();
                cetinfInfo.setYptybh(guarNo);//押品编号
                cetinfListInfo.add(cetinfInfo);
            }

            reqDto.setList(cetinfListInfo);

            log.info("流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 开始，请求报文："+ reqDto);
            ResultDto<CetinfRespDto> cetinfRespDto = dscms2YpxtClientService.cetinf(reqDto);
            log.info("流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 结束，响应报文："+ cetinfRespDto);

            String code = Optional.ofNullable(cetinfRespDto.getCode()).orElse(StringUtils.EMPTY);
            String meesage = Optional.ofNullable(cetinfRespDto.getMessage()).orElse(StringUtils.EMPTY);

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                CetinfRespDto data = cetinfRespDto.getData();
                log.info("流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 成功，"+ data);
            } else {
                log.error("流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 失败，"+meesage);
                throw new BizException(null, "", null, meesage);
            }
        } catch (Exception e) {
            log.info("流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 失败，", e.getMessage());
            throw new BizException(null, "", null, "流水号【"+serno+"】，权证入库调押品系统cetinf接口进行权证信息同步 失败，"+e.getMessage());
        }
    }

    /**
     * 权证入库调certis接口将权证状态同步押品系统
     * @param warrantInfo
     * @param inputDate
     * @param serno
     */
    public void sendCertis(GuarWarrantInfo warrantInfo,String inputDate,String serno){
        try {
            CertisReqDto certisReqDto = new CertisReqDto();
            List<CertisListInfo> certisListInfo = new ArrayList();
            CertisListInfo certisInfo = new CertisListInfo();

            //权证流水号
            certisInfo.setSernoy(warrantInfo.getCoreGuarantyNo());
            //权利凭证号
            certisInfo.setQlpzhm(warrantInfo.getWarrantNo());
            //权证类型
            certisInfo.setQzlxyp(CmisBizConstants.certiTypeCdMap.get(warrantInfo.getCertiTypeCd()));
            //权证出入库状态
            certisInfo.setQzztyp("10006");
            //权证入库日期 状态为10006-已入库时必输
            certisInfo.setQzrkrq(inputDate);
            //权证正常出库日期    状态为10008-正常出库时必输
            certisInfo.setQzckrq("");
            //权证临时借用人名称    状态为10019-已借阅时，必输
            certisInfo.setQzjyrm("");
            //权证外借日期			状态为10019-已借阅时，必输
            certisInfo.setQzwjrq("");
            //预计归还日期			状态为10019-已借阅时，必输
            certisInfo.setYjghrq("");
            //权证实际归还日期		状态为10020-外借归还是，必输
            certisInfo.setSjghrq("");
            //权证外借原因			状态为10019-已借阅时，必输
            certisInfo.setQzwjyy("");
            //其他文本输入
            certisInfo.setQtwbsr("");
            certisListInfo.add(certisInfo);
            certisReqDto.setList(certisListInfo);

            log.info("流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 开始，请求报文："+ certisReqDto);
            ResultDto<CertisRespDto> certisRespDto = dscms2YpxtClientService.certis(certisReqDto);
            log.info("流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 结束，响应报文："+ certisRespDto);

            String code = Optional.ofNullable(certisRespDto.getCode()).orElse(StringUtils.EMPTY);
            String meesage = Optional.ofNullable(certisRespDto.getMessage()).orElse(StringUtils.EMPTY);

            if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
                CertisRespDto data = certisRespDto.getData();
                log.info("流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 成功："+ data);
            } else {
                log.error("流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 失败："+meesage);
                throw new BizException(null, "", null, meesage);
            }
        } catch (Exception e) {
            log.info("流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 失败：", e.getMessage());
            throw new BizException(null, "", null, "流水号【"+serno+"】，权证入库调押品系统certis接口进行权证状态同步 失败："+e.getMessage());
        }
    }

    /**
     * 权证入库调cwm003接口将权证信息同步权证系统
     * @param guarWarrantManageApp
     * @param guarBaseInfoDto
     * @param warrantInfo
     * @param inputDate
     * @param serno
     */
    public void sendCwm003(GuarWarrantManageApp guarWarrantManageApp,GuarBaseInfo guarBaseInfoDto,GuarWarrantInfo warrantInfo,String inputDate,String serno){
        Cwm003ReqDto cwm003ReqDto = new Cwm003ReqDto();
        String orgName = OcaTranslatorUtils.getOrgName(guarWarrantManageApp.getInputBrId());
        String usrName = OcaTranslatorUtils.getUserName(guarWarrantManageApp.getInputId());
        String managerBrId = guarWarrantManageApp.getManagerBrId();
        String managerBrName = OcaTranslatorUtils.getOrgName(managerBrId);
        //担保分类代码
        String qzSysGageType = guarBaseInfoDto.getGuarTypeCd();
        //柜员名称
        cwm003ReqDto.setUserName(usrName);
        //申请时间
        cwm003ReqDto.setApplyTime(inputDate);
        //核心担保品编号
        cwm003ReqDto.setGageId(guarWarrantManageApp.getCoreGuarantyNo());
        //抵质押类型
        String gageType = guarWarrantManageAppService.transferGageTypeDict(qzSysGageType);
        cwm003ReqDto.setGageType(gageType);
        //抵质押类型名称
        String gageTypeName = guarWarrantManageAppService.getGageTypeNameFromAdminSmTreeDic(qzSysGageType);
        cwm003ReqDto.setGageTypeName(gageTypeName);
        //权利价值
        cwm003ReqDto.setMaxAmt(warrantInfo.getCertiAmt().toString());
        //抵押人
        cwm003ReqDto.setGageUser(guarBaseInfoDto.getGuarCusName());
        String applyBrchNo = managerBrId;

        if (applyBrchNo.startsWith("81")){
            //东海村镇默认810100--东海村镇银行营业部
            applyBrchNo = "810100";
            managerBrName = "东海村镇银行营业部";
        }else if(applyBrchNo.startsWith("80")){
            //寿光村镇默认800100--寿光村镇银行营业部
            applyBrchNo = "800100";
            managerBrName = "寿光村镇银行营业部";
        }
        applyBrchNo = applyBrchNo.substring(0, applyBrchNo.length()-1) +"1";

        //账务机构号
        cwm003ReqDto.setAcctBrch(applyBrchNo);
        //账务机构名称
        cwm003ReqDto.setAcctBrchName(managerBrName);
        //申请支行号
        cwm003ReqDto.setApplyBrchNo(guarWarrantManageApp.getInputBrId());
        //申请支行名称
        cwm003ReqDto.setApplyBrchName(orgName);
        //申请人操作号
        cwm003ReqDto.setApplyUserCode(guarWarrantManageApp.getInputId());
        //申请人姓名
        cwm003ReqDto.setApplyUserName(usrName);
        //是否住房按揭
        String isMortgage = getIsMortgage(guarWarrantManageApp.getCoreGuarantyNo());
        cwm003ReqDto.setIsMortgage(isMortgage);
        //备注
        cwm003ReqDto.setRemark("");
        //传入抵押品名称
        cwm003ReqDto.setGageName(guarBaseInfoDto.getPldimnMemo());
        //传入本地标志 不是柜面审核，is_local送1，会走到集中作业洪向阳那边
        cwm003ReqDto.setIsLocal("1");
        //传入是否电子权证 1--是 2--否
        String isElectronic = "2";

        if (CmisBizConstants.STD_ZB_YES_NO_Y.equals(warrantInfo.getIsEWarrant())) {
            isElectronic = "1";
        }
        cwm003ReqDto.setIsElectronic(isElectronic);

        //是否柜面审核 默认 N：不是柜面审核 Y：柜面审核
//        cwm003ReqDto.setIsgm("N");

        log.info("流水号【"+serno+"】，权证入库调权证系统cwm003接口进行入库申请 开始，请求报文："+ cwm003ReqDto);
        ResultDto<Cwm003RespDto> cwm003RespResultDto = dscms2YpqzxtClientService.cwm003(cwm003ReqDto);
        log.info("流水号【"+serno+"】，权证入库调权证系统cwm003接口进行入库申请 结束，响应报文："+ cwm003RespResultDto);

        String code = Optional.ofNullable(cwm003RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(cwm003RespResultDto.getMessage()).orElse(StringUtils.EMPTY);

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            Cwm003RespDto data = cwm003RespResultDto.getData();
            log.info("流水号【"+serno+"】，权证入库调权证系统cwm003接口进行入库申请 成功："+ data);

            //将权证状态由入库未记账改为入库已记账
            warrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_04);
            log.info("将核心担保编号为【"+warrantInfo.getCoreGuarantyNo()+"】的权证状态由入库未记账改为入库已记账");
            guarWarrantInfoMapper.updateByPrimaryKeySelective(warrantInfo);
        } else {
            log.error("流水号【"+serno+"】，权证入库调权证系统cwm003接口进行入库申请 失败，"+meesage);
            throw new BizException(null, "", null, "流水号【"+serno+"】，权证入库调权证系统cwm003接口进行入库申请 失败，"+meesage);
        }
    }

    /**
     * 根据权利凭证号查询在途的权证入库流水号
     * @param certiRecordId
     * @return
     */
    public String selectOnTheWaySernoByCertiRecordId(String certiRecordId){
        return guarWarrantManageAppMapper.selectOnTheWaySernoByCertiRecordId(certiRecordId);
    }

    /**
     * 调押品系统xddb03接口查询存单票据信息
     * @param guarBaseInfo
     * @param serno
     * @return
     */
    public Xddb03RespDto xddb03(GuarBaseInfo guarBaseInfo,String serno){
        Xddb03ReqDto xddb03ReqDto = new Xddb03ReqDto();
        xddb03ReqDto.setGuaranty_id(guarBaseInfo.getGuarNo());
        //类型 取担保分类代码
        xddb03ReqDto.setType(guarBaseInfo.getGuarTypeCd());
        log.info("流水号【"+serno+"】，权证入库调押品系统xddb03接口查询存单票据信息 开始："+ xddb03ReqDto);
        ResultDto<Xddb03RespDto> xddb03RespDtoResultDto = dscms2YphsxtClientService.xddb03(xddb03ReqDto);
        log.info("流水号【"+serno+"】，权证入库调押品系统xddb03接口查询存单票据信息 结束："+ xddb03RespDtoResultDto);

        String code = Optional.ofNullable(xddb03RespDtoResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(xddb03RespDtoResultDto.getMessage()).orElse(StringUtils.EMPTY);

        if (!Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            throw new BizException(null, "", null, "流水号【"+serno+"】，权证入库调押品系统xddb03接口查询存单票据信息 失败：" + meesage);
        }

        Xddb03RespDto data = xddb03RespDtoResultDto.getData();
        log.info("流水号【"+serno+"】，权证入库调押品系统xddb03接口查询存单票据信息 成功：" + data);
        return data;
    }

    /**
     * 权证编号唯一性校验
     * @param warrantNo
     */
    public void checkWarrantNoIsExist(String warrantNo){
        GuarWarrantInfo dataFlag = guarWarrantInfoService.queryByWarrantNo(warrantNo);
        //权证编号唯一性校验
        if (dataFlag != null) {
            throw BizException.error(null, EcbEnum.IQP_GBRA_IMPORT_WARRANT_EXISTS_EXCEPTION.key, EcbEnum.IQP_GBRA_IMPORT_WARRANT_EXISTS_EXCEPTION.value);
        }
    }

    /**
     * 查询核心担保编号对应的产品是否为住房按揭贷款
     * @param coreGuarantyNo
     * @return
     */
    public String getIsMortgage(String coreGuarantyNo){
        int count = guarWarrantInfoService.selectAccommoDation(coreGuarantyNo);

        if (count>0){
            //是住房按揭贷款
            return "1";
        }
        return "0";
    }

    /**
     * 生成集中作业档案池任务
     * @param taskSerno
     * @param bizType
     */
    public void createCentralFileTask(GuarMortgageManageApp guarMortgageManageApp,String taskSerno,String instanceId,String nodeId,String bizType){
        CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
        centralFileTaskdto.setSerno(taskSerno);
        //根据担保合同编号查询担保合同信息
        GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(guarMortgageManageApp.getGuarContNo());

        if (grtGuarCont!=null){
            //客户编号取担保合同客户编号
            centralFileTaskdto.setCusId(grtGuarCont.getCusId());
            //客户名称取担保合同客户名称
            centralFileTaskdto.setCusName(grtGuarCont.getCusName());
        }

        centralFileTaskdto.setBizType(bizType);
        centralFileTaskdto.setInputId(guarMortgageManageApp.getInputId());
        centralFileTaskdto.setInputBrId(guarMortgageManageApp.getInputBrId());

        //档案任务操作类型 01--纯指令
        centralFileTaskdto.setOptType(CmisBizConstants.STD_OPT_TYPE_01);
        centralFileTaskdto.setInstanceId(instanceId);
        centralFileTaskdto.setNodeId(nodeId);
        //档案任务类型 03--派发
        centralFileTaskdto.setTaskType(CmisBizConstants.STD_FILE_TASK_TYPE_03);
        centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急

        centralFileTaskService.insertSelective(centralFileTaskdto);
    }

    /**
     * 根据担保编号查询权证出库核心担保编号
     * @param guarContNo
     * @return
     */
    public String selectCoreGuarantyNoByGuarContNo(String guarContNo){
        return guarWarrantManageAppMapper.selectCoreGuarantyNoByGuarContNo(guarContNo);
    }

    /**
     * 权证入库冲正操作
     * @param serno
     * @return
     */
    public String warrantInRighting(String serno){
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

        if (guarWarrantInfo==null){
            return "获取不到权证台账信息！";
        }
        //权证状态
        String certiState = guarWarrantInfo.getCertiState();

        if (CmisBizConstants.STD_ZB_CERTI_STATE_03.equals(certiState)){
            //权证状态是入库未记账，可以进行冲正
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = sendIb1241(guarWarrantManageApp);

            if(SuccessEnum.CMIS_SUCCSESS.key.equals(ib1241RespDtoResultDto.getCode())){
                //更新权证台账的状态为13--入库冲正
                guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_13);
                guarWarrantInfoMapper.updateByPrimaryKey(guarWarrantInfo);
                log.info("权证入库冲正成功，将核心担保编号为【"+coreGuarantyNo+"】,权证编号为【"+guarWarrantInfo.getWarrantNo()+"】的权证状态由【入库未记账】改为【入库冲正】");
                return "success";
            }else{
                return ib1241RespDtoResultDto.getMessage();
            }
        }else{
            return "该权证状态不是入库未记账，无法进行冲正！";
        }
    }

    /**
     * 权证出库冲正
     * @param serno
     * @return
     */
    public String warrantOutRighting(String serno){
        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppMapper.selectByPrimaryKey(serno);
        //核心担保编号
        String coreGuarantyNo = guarWarrantManageApp.getCoreGuarantyNo();
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGuarantyNo);

        if (guarWarrantInfo==null){
            return "获取不到权证台账信息！";
        }
        //权证状态
        String certiState = guarWarrantInfo.getCertiState();

        if (CmisBizConstants.STD_ZB_CERTI_STATE_09.equals(certiState)){
            //权证状态是出库未记账，可以进行冲正
            ResultDto<Ib1241RespDto> ib1241RespDtoResultDto = sendIb1241(guarWarrantManageApp);

            if(SuccessEnum.CMIS_SUCCSESS.key.equals(ib1241RespDtoResultDto.getCode())){
                //更新权证台账的状态为04--入库已记账
                guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_04);
                guarWarrantInfoMapper.updateByPrimaryKey(guarWarrantInfo);
                log.info("权证出库冲正成功，将核心担保编号为【"+coreGuarantyNo+"】,权证编号为【"+guarWarrantInfo.getWarrantNo()+"】的权证状态由【出库未记账】改为【入库已记账】");
                return "success";
            }else {
                return ib1241RespDtoResultDto.getMessage();
            }
        }else{
            return "该权证状态不是出库未记账，无法进行冲正！";
        }
    }

    /**
     * 调核心的ib1241接口进行冲正
     * @param guarWarrantManageApp
     * @return
     */
    public ResultDto<Ib1241RespDto> sendIb1241(GuarWarrantManageApp guarWarrantManageApp){
        String hxSerno = guarWarrantManageApp.getHxSerno();
        String hxDate = guarWarrantManageApp.getHxDate();

        Ib1241ReqDto ib1241ReqDto = new Ib1241ReqDto();
        ib1241ReqDto.setYjiaoyrq(hxDate);//原交易日期
        ib1241ReqDto.setYgyliush(hxSerno);//原交易流水
        ib1241ReqDto.setYqzhriqi("");//原主机日期（原核心交易日期）
        ib1241ReqDto.setYqzhlshu("");//原主机流水（原核心柜员流水）
        ib1241ReqDto.setQianzhrq("");//前置日期
        ib1241ReqDto.setQianzhls("");//前置流水
        log.info("流水号【"+guarWarrantManageApp.getSerno()+"】，发核心系统进行冲正 开始："+ ib1241ReqDto);
        ResultDto<Ib1241RespDto> ib1241RespDto = dscms2CoreIbClientService.ib1241(ib1241ReqDto);
        log.info("流水号【"+guarWarrantManageApp.getSerno()+"】，发核心系统进行冲正 结束："+ ib1241RespDto);
        return ib1241RespDto;
    }

    /**
     * 根据核心担保编号查询权证入库流水号
     * @param coreGuarantyNo
     * @return
     */
    public String selectSernoByCoreGuarantyNo(String coreGuarantyNo){
        return guarWarrantManageAppMapper.selectSernoByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 根据核心担保编号查询权证出库信息
     * @param coreGuarantyNo
     * @return
     */
    public GuarWarrantManageApp selectWarrantOutInfoByCoreGuarantyNo(String coreGuarantyNo){
        return guarWarrantManageAppMapper.selectWarrantOutInfoByCoreGuarantyNo(coreGuarantyNo);
    }

    /**
     * 检查担保合同下的押品能否做入库
     * @param queryModel
     * @return
     */
    public String checkWarrantState(QueryModel queryModel){
        //押品编号
        String guarNo = (String) queryModel.getCondition().get("guarNo");
        //担保合同编号
        String guarContNo = (String) queryModel.getCondition().get("guarContNo");

        //根据担保合同编号和押品编号查询最新的权证信息
        GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByGuarContNoAndGuarNo(queryModel);

        if (guarWarrantInfo==null){
            return "success";
        }

        //权证状态
        String certiState = guarWarrantInfo.getCertiState();
        //核心担保编号
        String coreGuarantyNo = guarWarrantInfo.getCoreGuarantyNo();

        if (CmisBizConstants.STD_ZB_CERTI_STATE_03.equals(certiState)){
            return "担保合同【"+guarContNo+"】和押品【"+guarNo+"】对应的核心担保编号为【"+coreGuarantyNo+"】的权证是入库未记账状态，该押品无法再进行入库操作！";
        }else if (CmisBizConstants.STD_ZB_CERTI_STATE_04.equals(certiState)){
            return "担保合同【"+guarContNo+"】和押品【"+guarNo+"】对应的核心担保编号为【"+coreGuarantyNo+"】的权证是入库已记账状态，该押品无法再进行入库操作！";
        }

        return "success";
    }

    /**
     * 查询理财产品是否冻结
     * @param guarBaseInfo
     * @param serno
     * @return
     */
    public boolean checkFinancialPrdIsFreeze(GuarBaseInfo guarBaseInfo,String serno){
        //调押品缓释系统的xddb03
        Xddb03RespDto xddb03RespDto = xddb03(guarBaseInfo, serno);
        //账户号码
        String acctNo = xddb03RespDto.getAcct_no();
        //理财产品代码
        String moneyProdCode = xddb03RespDto.getMoney_prod_code();

        Lc0323ReqDto reqDto = new Lc0323ReqDto();

        GuarWarrantManageApp guarWarrantManageApp = guarWarrantManageAppService.selectByPrimaryKey(serno);
        //管理机构
        String managerBrId = guarWarrantManageApp.getManagerBrId();
        managerBrId = managerBrId.substring(0, managerBrId.length()-1) +"1";

        //冻结交易码
        reqDto.setTransCode("100222");
        //客户银行账号
        reqDto.setAccount(acctNo);
        //账户类型0
        reqDto.setAccType("0");
        //产品类型1
        reqDto.setPrdType("1");
        //理财接口交易码
        reqDto.setFunctionId("100323");
        //流水号 20211019004230000278
        String exSerial = sequenceTemplateClient.getSequenceTemplate(SeqConstant.GUAR_BASE_SEQ, new HashMap<>()).substring(2);
        reqDto.setExSerial(exSerial);
        //银行编号 默认本行00
        reqDto.setBankNo("00");
        //交易机构
        reqDto.setBranchNo(managerBrId);
        //渠道编号 6
        reqDto.setChannel("6");
        //终端代码
        reqDto.setTermNo("");
        //00080163 操作员编号
        reqDto.setOperNo("00080163");
        //交易日期
        reqDto.setTransDate(stringRedisTemplate.opsForValue().get("openDay").replaceAll("-",""));
        //交易时间 4230
        reqDto.setTransTime("4230");
        //查询起始位置
        reqDto.setOffSet("1");
        //本次查询总记录数
        reqDto.setQueryNum("10");

        log.info("权证入库流水号【"+serno+"】，调lc0323接口查询理财是否冻结 请求报文："+reqDto);
        ResultDto<Lc0323RespDto> respDtoResultDto = dscms2LcxtClientService.lc0323(reqDto);
        log.info("权证入库流水号【"+serno+"】，调lc0323接口查询理财是否冻结 请响应报文："+respDtoResultDto);

        String code = Optional.ofNullable(respDtoResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(respDtoResultDto.getMessage()).orElse(StringUtils.EMPTY);

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            Lc0323RespDto data = respDtoResultDto.getData();
            log.info("权证入库调lc0323接口查询理财是否冻结 成功"+ data);

            List<Lc0323RespListDto> list = data.getList();

            for (Lc0323RespListDto respListDto : list) {
                //判断是不是需要用的这笔理财产品
                //产品代码
                String prdCode = respListDto.getPrdCode();

                if (moneyProdCode.equals(prdCode)){
                    return true;
                }
            }
        } else {
            log.error("权证入库调lc0323接口查询理财是否冻结 失败，"+meesage);
            throw new BizException(null, "", null, meesage);
        }

        return false;
    }

    /**
     * 校验权证对应押品关联的担保合同已无关联的主合同,如有则校验权证对应押品关联的担保合同所担保主合同项下的用信敞口余额为零。
     * @param guarContNo
     * @return
     */
    public String checkGuarContIsRelCont(String guarContNo){
        //根据担保合同编号查询关联的合同编号
        String contNos = grtGuarBizRstRelService.selectContNosByGuarContNo(guarContNo);

        if (StringUtils.isEmpty(contNos)){
            return "";
        }

        //贷款台账--用信敞口余额
        BigDecimal accLoanBalance = accLoanService.selectTotalSpacAmtByContNos(contNos);

        if (accLoanBalance.compareTo(BigDecimal.ZERO)==1){
            return "担保合同【"+guarContNo+"】关联的主合同的贷款台账--用信敞口余额【"+accLoanBalance+"】不能大于0";
        }

        //银承台账--用信敞口余额
        BigDecimal accAccpBalance = accAccpService.selectTotalSpacAmtByContNos(contNos);
        if (accAccpBalance.compareTo(BigDecimal.ZERO)==1){
            return "担保合同【"+guarContNo+"】关联的主合同的银承台账--用信敞口余额【"+accAccpBalance+"】不能大于0";
        }

        //保函台账--用信敞口余额
        BigDecimal accCvrsBalance = accCvrsService.selectTotalSpacAmtByContNos(contNos);
        if (accCvrsBalance.compareTo(BigDecimal.ZERO)==1){
            return "担保合同【"+guarContNo+"】关联的主合同的保函台账--用信敞口余额【"+accCvrsBalance+"】不能大于0";
        }

        //开证台账--用信敞口余额
        BigDecimal accTfLocBalance = accTfLocService.selectTotalSpacAmtByContNos(contNos);
        if (accTfLocBalance.compareTo(BigDecimal.ZERO)==1){
            return "担保合同【"+guarContNo+"】关联的主合同的开证台账--用信敞口余额【"+accTfLocBalance+"】不能大于0";
        }

        return "";
    }

    /**
     * 查询存单的账户号码
     * @param queryModel
     * @return
     */
    public String getGuarAcctNo(QueryModel queryModel){
        Xddb03RespDto xddb03RespDto;
        Xddb03ReqDto xddb03ReqDto = new Xddb03ReqDto();
        //押品编号
        String guarNo = (String) queryModel.getCondition().get("guarNo");
        xddb03ReqDto.setGuaranty_id(guarNo);

        //类型 取担保分类代码
        String guarTypeCd = (String) queryModel.getCondition().get("guarTypeCd");
        xddb03ReqDto.setType(guarTypeCd);

        log.info("押品编号【"+guarNo+"】，调xddb03接口 开始："+ xddb03ReqDto);
        ResultDto<Xddb03RespDto> xddb03RespDtoResultDto = dscms2YphsxtClientService.xddb03(xddb03ReqDto);
        log.info("押品编号【"+guarNo+"】，调xddb03接口 结束："+ xddb03RespDtoResultDto);

        String code = Optional.ofNullable(xddb03RespDtoResultDto.getCode()).orElse(StringUtils.EMPTY);
        String meesage = Optional.ofNullable(xddb03RespDtoResultDto.getMessage()).orElse(StringUtils.EMPTY);

        if (Objects.equals(code, SuccessEnum.CMIS_SUCCSESS.key)) {
            xddb03RespDto = xddb03RespDtoResultDto.getData();
            log.info("押品编号【"+guarNo+"】，调 xddb03 成功" + xddb03RespDto);
        } else {
            throw new BizException(null, "", null, "押品编号【"+guarNo+"】，调xddb03 失败" + meesage);
        }
        return xddb03RespDto.getAcct_no();
    }
}