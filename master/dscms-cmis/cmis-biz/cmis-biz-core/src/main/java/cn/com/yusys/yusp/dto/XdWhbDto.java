package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import java.math.BigDecimal;

public class XdWhbDto extends BaseDomain implements Serializable {
        private static final long serialVersionUID = 1L;

        /** 借据号 */
        private String billNo;

        /** 客户号 */
        private String cusId;

        /** 客户名称 */
        private String cusName;

        /** 借据金额 */
        private BigDecimal loanAmt;

        public BigDecimal getLoanAmt() {
                return loanAmt;
        }

        public void setLoanAmt(BigDecimal loanAmt) {
                this.loanAmt = loanAmt;
        }

        public BigDecimal getLoanBalance() {
                return loanBalance;
        }

        public void setLoanBalance(BigDecimal loanBalance) {
                this.loanBalance = loanBalance;
        }

        public String getLoanStartDate() {
                return loanStartDate;
        }

        public void setLoanStartDate(String loanStartDate) {
                this.loanStartDate = loanStartDate;
        }

        public String getLoanEndDate() {
                return loanEndDate;
        }

        public void setLoanEndDate(String loanEndDate) {
                this.loanEndDate = loanEndDate;
        }

        /** 借据余额 */
        private BigDecimal loanBalance;

        /** 借据起始日 */
        private String loanStartDate;

        /** 借据到期日 */
        private String loanEndDate;

        /** 还款方式 */
        private String repayMode;

        /** 合同到期日 */
        private String contEndDate;

        /** 联系方式 */
        private String linkMode;

        /** 身份证号 */
        private String certCode;

        /** 借据产品类型 */
        private String prdId;

        /** 借款产品名称 */
        private String prdName;

        public String getBillNo() {
                return billNo;
        }

        public void setBillNo(String billNo) {
                this.billNo = billNo;
        }

        public String getCusId() {
                return cusId;
        }

        public void setCusId(String cusId) {
                this.cusId = cusId;
        }

        public String getCusName() {
                return cusName;
        }

        public void setCusName(String cusName) {
                this.cusName = cusName;
        }

        public String getContEndDate() {
                return contEndDate;
        }

        public void setContEndDate(String contEndDate) {
                this.contEndDate = contEndDate;
        }

        public String getLinkMode() {
                return linkMode;
        }

        public void setLinkMode(String linkMode) {
                this.linkMode = linkMode;
        }

        public String getCertCode() {
                return certCode;
        }

        public void setCertCode(String certCode) {
                this.certCode = certCode;
        }

        public String getPrdId() {
                return prdId;
        }

        public void setPrdId(String prdId) {
                this.prdId = prdId;
        }

        public String getRepayMode() {
                return repayMode;
        }

        public void setRepayMode(String repayMode) {
                this.repayMode = repayMode;
        }

        public String getPrdName() {
                return prdName;
        }

        public void setPrdName(String prdName) {
                this.prdName = prdName;
        }

        @Override
        public String toString() {
                return "XdWhbDto{" +
                        "billNo='" + billNo + '\'' +
                        ", cusId='" + cusId + '\'' +
                        ", cusName='" + cusName + '\'' +
                        ", repayMode='" + repayMode + '\'' +
                        ", contEndDate='" + contEndDate + '\'' +
                        ", linkMode='" + linkMode + '\'' +
                        ", certCode='" + certCode + '\'' +
                        ", prdId='" + prdId + '\'' +
                        ", prdName='" + prdName + '\'' +
                        '}';
        }
}
