/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpWriteoffApp;
import cn.com.yusys.yusp.domain.IqpWriteoffDetl;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpWriteoffAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: IqpWriteoffAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:31:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpWriteoffAppService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(IqpWriteoffAppService.class);

    @Autowired
    private IqpWriteoffAppMapper iqpWriteoffAppMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    //核销详情信息服务接口
    @Autowired
    private IqpWriteoffDetlService iqpWriteoffDetlService;

    @Autowired
    private AccWriteoffService accWriteoffService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpWriteoffApp selectByPrimaryKey(String iqpSerno) {
        return iqpWriteoffAppMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpWriteoffApp> selectAll(QueryModel model) {
        List<IqpWriteoffApp> records = (List<IqpWriteoffApp>) iqpWriteoffAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpWriteoffApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpWriteoffApp> list = iqpWriteoffAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpWriteoffApp record) {
        return iqpWriteoffAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpWriteoffApp record) {
        return iqpWriteoffAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpWriteoffApp record) {
        return iqpWriteoffAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpWriteoffApp record) {
        return iqpWriteoffAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpWriteoffAppMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpWriteoffAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveIqpWriteOffAppLeadInfo
     * @方法描述: 通过页面引导信息保存核销申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map saveIqpWriteOffAppLeadInfo(IqpWriteoffApp iqpWriteoffApp) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        String iqpSerno = null;
        try {

            // 业务校验 无

            // 通过流水号生成器生成业务流水号
            iqpSerno =null;// sequenceTemplateClient.getSequenceTemplate(EcbEnum.SEQ_IQP_WRITEOFF_APP, new HashMap<>());
            // 生成流水号异常 如空的情况
            if (StringUtils.isEmpty(iqpSerno)) {
                rtnCode = EcbEnum.E_IQP_CHG_GEN_PK_ERROR.key;
                rtnMsg = EcbEnum.E_IQP_CHG_GEN_PK_ERROR.value;
                return rtnData;
            }

            log.info(String.format("保存核销申请,生成流水号%s", iqpSerno));
            // 申请流水号
            iqpWriteoffApp.setIqpSerno(iqpSerno);
            // 数据操作标志为新增
            iqpWriteoffApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 流程状态
            iqpWriteoffApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);

            log.info(String.format("保存核销申请%s-获取当前登录用户数据", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_EXCEPTION.value;
                return rtnData;
            } else {
                iqpWriteoffApp.setInputId(userInfo.getLoginCode());
                iqpWriteoffApp.setInputBrId(userInfo.getOrg().getCode());
                iqpWriteoffApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpWriteoffApp.setUpdId(userInfo.getLoginCode());
                iqpWriteoffApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpWriteoffApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }


            log.info(String.format("保存核销申请,流水号%s", iqpSerno));
            int count = iqpWriteoffAppMapper.insert(iqpWriteoffApp);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }


        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存核销新增申请出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("iqpSerno", iqpSerno);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateIqpWriteOffAppTotalInfo
     * @方法描述: 通过流水号更新核销申请信息中的汇总信息
     * @参数与返回说明: iqpSerno
     * @算法描述: 无
     */
    @Transactional(rollbackFor = YuspException.class)
    public void updateIqpWriteOffAppTotalInfo(String iqpSerno) throws YuspException {
        List<IqpWriteoffDetl> iqpWriteOffDetailsList;
        HashMap<String, String> queryMap = new HashMap<>();
        // 根据业务流水号查询所有类型为新增的明细列表数据
        queryMap.put("iqpSerno", iqpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info(String.format("根据呆账核销申请流水号%s,获取呆账核销申请明细列表信息", iqpSerno));
        iqpWriteOffDetailsList = iqpWriteoffDetlService.selectIqpWriteOffDetailsListByParams(queryMap);
        if (iqpWriteOffDetailsList != null ) {
            log.info(String.format("根据呆账核销申请流水号%s,获取呆账核销申请信息", iqpSerno));
            IqpWriteoffApp iqpWriteoffApp = iqpWriteoffAppMapper.selectByPrimaryKey(iqpSerno);
            log.info(String.format("根据呆账核销申请流水号%s,计算呆账核销明细列表信息中的金额总和", iqpSerno));
            iqpWriteoffApp.setTotalAmt(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getLoanAmt).reduce(BigDecimal.ZERO, BigDecimal::add));
            iqpWriteoffApp.setTotalBal(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getLoanBalance).reduce(BigDecimal.ZERO, BigDecimal::add));
            iqpWriteoffApp.setTotalInnerInt(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getInnerOweInt).reduce(BigDecimal.ZERO, BigDecimal::add));
            iqpWriteoffApp.setTotalOutInt(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getOutOweInt).reduce(BigDecimal.ZERO, BigDecimal::add));
            iqpWriteoffApp.setTotalWriteoffCap(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getWriteoffCap).reduce(BigDecimal.ZERO, BigDecimal::add));
            iqpWriteoffApp.setTotalWriteoffInt(iqpWriteOffDetailsList.stream().map(IqpWriteoffDetl::getWriteoffInt).reduce(BigDecimal.ZERO, BigDecimal::add));
            log.info(String.format("根据呆账核销申请流水号%s,更新呆账核销明细列表信息中的金额总和", iqpSerno));

            log.info(String.format("根据呆账核销申请流水号%s-更新呆账核销明细列表信息中的金额总和,获取当前登录用户数据", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpWriteoffApp.setUpdId(userInfo.getLoginCode());
                iqpWriteoffApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpWriteoffApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            int updateCount = iqpWriteoffAppMapper.updateByPrimaryKey(iqpWriteoffApp);
            if (updateCount != 1) {
                throw new YuspException(EcbEnum.IQP_WRITEOFF_APP_UPDATE_EXCEPTION_1.key, EcbEnum.IQP_WRITEOFF_APP_UPDATE_EXCEPTION_1.value);
            }
        }
    }

    /**
     * @方法名称: deleteIqpWriteOffAppLogic
     * @方法描述: 根据主键对核销申请明细信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 对核销申请信息进行逻辑删除，更新对核销申请明细信息进行逻辑删除
     */
    @Transactional
    public Map deleteIqpWriteOffAppLogic(String iqpSerno) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        try {
            log.info(String.format("根据主键%s对核销申请明细信息进行逻辑删除", iqpSerno));
            IqpWriteoffApp iqpWriteoffApp = iqpWriteoffAppMapper.selectByPrimaryKey(iqpSerno);
            iqpWriteoffApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);

            log.info(String.format("根据主键%s对核销申请明细信息进行逻辑删除,获取用户登录信息", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpWriteoffApp.setUpdId(userInfo.getLoginCode());
                iqpWriteoffApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpWriteoffApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对核销申请信息进行逻辑删除,信息进行逻辑删除", iqpSerno));
            iqpWriteoffAppMapper.updateByPrimaryKeySelective(iqpWriteoffApp);

            log.info(String.format("根据主键%s对核销申请明细信息进行逻辑删除,信息进行逻辑删除", iqpSerno));
            iqpWriteoffDetlService.deleteIqpWriteOffDetailsByIqpSerno(iqpSerno, userInfo);


        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除核销申请信息出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /***
     * 根据主键更新申请状态
     * @param iqpSerno
     * @param approveStatus
     * @return
     */
    public int updateIqpWriteOffAppStatus(String iqpSerno, String approveStatus){
        IqpWriteoffApp iqpWriteoffApp = selectByPrimaryKey(iqpSerno);
        if (iqpWriteoffApp != null){
            iqpWriteoffApp.setApproveStatus(approveStatus);
            return updateSelective(iqpWriteoffApp);
        }
        return 0;
    }

    /**
     * 审批通过后进行的业务操作
     * 1、
     * @param iqpSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) {
        updateIqpWriteOffAppStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
        // 根据申请流水号生成核销台账
        accWriteoffService.insertAccWriteOffs(iqpSerno);
    }

}
