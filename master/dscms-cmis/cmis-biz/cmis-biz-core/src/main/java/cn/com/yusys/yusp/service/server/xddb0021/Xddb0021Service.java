package cn.com.yusys.yusp.service.server.xddb0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.List;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.dto.server.xddb0021.req.Xddb0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0021.resp.Xddb0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.bsp.ypxt.xdypgyrcx.XdypgyrcxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:押品共有人信息查询
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xddb0021Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0021Service.class);

    @Resource
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private XdypgyrcxService xdypgyrcxService;//业务逻辑处理类：押品共有人信息查询接口
    @Autowired
    private AdminSmUserService adminSmUserService;//用户信息查询公共方法

    /**
     * 押品共有人信息查询
     *
     * @param xddb0021DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0021DataRespDto xddb0021(Xddb0021DataReqDto xddb0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value);
        Xddb0021DataRespDto xddb0021DataRespDto = new Xddb0021DataRespDto();

        try {
            String guarNo = xddb0021DataReqDto.getGuaranty_id();//押品编号

            //获取管护人工号
            String managerId = guarBaseInfoMapper.getGuarInfoManageridByGuarNo(guarNo);

            if (StringUtil.isNotEmpty(managerId)) {//管户经理不为空
                //根据管户人号查询客户经理
                logger.info("***********XDDB0021根据管户人号查询客户经理开始,查询参数为:{}", JSON.toJSONString(managerId));
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                logger.info("***********XDDB0021根据管户人号查询客户经理结束,返回结果为:{}", JSON.toJSONString(resultDto));
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    String userName = adminSmUserDto.getUserName();
                    String userMobilephone = adminSmUserDto.getUserMobilephone();
                    String certCode = adminSmUserDto.getCertNo();
                    //处理返回信息
                    xddb0021DataRespDto.setManager_id(managerId);// 客户经理号
                    xddb0021DataRespDto.setManager_name(userName);// 客户经理姓名
                    xddb0021DataRespDto.setManager_ph(userMobilephone);// 客户经理号电话号码
                    xddb0021DataRespDto.setManager_zjh(certCode);// 客户经理号身份证号码
                } else { //不存在返回信息,返回空值
                    xddb0021DataRespDto.setManager_id(managerId);// 客户经理号
                    xddb0021DataRespDto.setManager_name(StringUtils.EMPTY);// 客户经理姓名
                    xddb0021DataRespDto.setManager_ph(StringUtils.EMPTY);// 客户经理号电话号码
                    xddb0021DataRespDto.setManager_zjh(StringUtils.EMPTY);// 客户经理号身份证号码
                }
            } else {
                logger.info("***********XDDB0021根据管户人号查询客户经理开始,查询参数为空:{}", JSON.toJSONString(managerId));
            }
            //调用押品系统共有人 查询 接口 xdypdywcx
            XdypgyrcxReqDto xdypgyrcxReqDto = new XdypgyrcxReqDto();
            xdypgyrcxReqDto.setGuaranty_id(guarNo);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxReqDto));
            XdypgyrcxRespDto xdypgyrcx2ResultDto = xdypgyrcxService.xdypgyrcx(xdypgyrcxReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcx2ResultDto));

            if (xdypgyrcx2ResultDto != null) {
                java.util.List<List> lists = xdypgyrcx2ResultDto.getList();
                if (CollectionUtils.nonEmpty(lists)) {//存在共有人信息
                    xddb0021DataRespDto.setIs_gyou(DscmsBizDbEnum.COMMON_OWNER_01.key);// 是否共有
                    String Share_owner_type = lists.get(0).getShare_owner_type();
                    String owner_title = StringUtils.EMPTY;
                    if (StringUtil.isNotEmpty(Share_owner_type) && DscmsBizDbEnum.COMMON_TYPE_02.key.equals(Share_owner_type)) {// 共同共有
                        owner_title = DscmsBizDbEnum.COMMON_BIL_01.value;
                    } else {
                        BigDecimal bigOwnerTitle = BigDecimal.ZERO;
                        for (int i = 0; i < lists.size(); i++) {
                            String commonHoldPortio = lists.get(i).getCommon_hold_portio();
                            if (StringUtil.isNotEmpty(commonHoldPortio)) {
                                BigDecimal bigHoldPortio = new BigDecimal(commonHoldPortio);
                                bigOwnerTitle = (bigOwnerTitle.add(bigHoldPortio)).multiply(new BigDecimal("100"));
                            }
                        }
                        owner_title = bigOwnerTitle.toString();
                        owner_title += "%";
                    }

                    xddb0021DataRespDto.setOwner_type(Share_owner_type);// 共有类型
                    xddb0021DataRespDto.setOwner_title(owner_title);// 共有份额
                } else {//不存在共有人
                    xddb0021DataRespDto.setIs_gyou(DscmsBizDbEnum.COMMON_OWNER_02.key);// 是否共有
                    xddb0021DataRespDto.setOwner_type(StringUtils.EMPTY);// 共有类型
                    xddb0021DataRespDto.setOwner_title(StringUtils.EMPTY);// 共有份额
                }
            } else {
                xddb0021DataRespDto.setIs_gyou(DscmsBizDbEnum.COMMON_OWNER_02.key);// 是否共有
                xddb0021DataRespDto.setOwner_type(StringUtils.EMPTY);// 共有类型
                xddb0021DataRespDto.setOwner_title(StringUtils.EMPTY);// 共有份额
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value);
        return xddb0021DataRespDto;
    }
}
