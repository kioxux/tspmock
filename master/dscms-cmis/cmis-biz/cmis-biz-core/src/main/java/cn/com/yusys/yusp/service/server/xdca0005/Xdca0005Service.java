package cn.com.yusys.yusp.service.server.xdca0005;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditCtrLoanCont;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CreditCtrLoanContMapper;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.Dscms2TonglianClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0005Service
 * @类描述: #服务类 大额分期合同签订接口
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdca0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0005Service.class);

    @Autowired
    private CreditCtrLoanContMapper creditCtrLoanContMapper;
    @Autowired
    private CmisCusClientService cmisCusClientService;//客户信息模块
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;

    /**
     * 大额分期合同签订接口
     *
     * @param xdca0005DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0005DataRespDto xdca0005(Xdca0005DataReqDto xdca0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value);
        Xdca0005DataRespDto xdca0005DataRespDto = new Xdca0005DataRespDto();
        try {
            //请求字段
            String contNo = xdca0005DataReqDto.getContNo();//合同编号

            //通过合同编号查询卡号
            CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContMapper.selectByPrimaryKey(contNo);
            String cardNo = creditCtrLoanCont.getCardNo();

            // 调用通联放款接口，待定
            D13087ReqDto reqDto = new D13087ReqDto();
            reqDto.setCardno(cardNo);//卡号
            reqDto.setRgstid("");//分期申请号
            reqDto.setRemark("");//备注信息
            logger.info("*********XDCA0005*大额分期合同签订接口调用通联放款接口开始,查询参数为:{}", JSON.toJSONString(reqDto));
            ResultDto<D13087RespDto> resultDto = dscms2TonglianClientService.d13087(reqDto);
            logger.info("*********XDCA0005*大额分期合同签订接口调用通联放款接口结束,返回参数为:{}", JSON.toJSONString(resultDto));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDto.getCode())) {//返回成功
                String lentus = resultDto.getData().getLentus();//放款状态

                //放款成功，更新合同状态日期
                creditCtrLoanCont.setContNo(contNo);
                creditCtrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_200);
                String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                creditCtrLoanCont.setContSignDate(openDay);
                logger.info("*********XDCA0005*大额分期合同签订接口放款成功，更新合同状态日期开始,查询参数为:{}", JSON.toJSONString(creditCtrLoanCont));
                int count = creditCtrLoanContMapper.updateByPrimaryKeySelective(creditCtrLoanCont);
                logger.info("*********XDCA0005*大额分期合同签订接口放款成功，更新合同状态日期结束,返回参数为:{}", JSON.toJSONString(count));

                //返回信息
                if (count > 0) {
                    xdca0005DataRespDto.setContNo(contNo);// 合同编号
                    xdca0005DataRespDto.setContStatus(CmisBizConstants.IQP_CONT_STS_200);// 合同状态
                    xdca0005DataRespDto.setContSignDate(openDay);// 合同签订日期
                } else {
                    xdca0005DataRespDto.setContNo(StringUtils.EMPTY);// 合同编号
                    xdca0005DataRespDto.setContStatus(StringUtils.EMPTY);// 合同状态
                    xdca0005DataRespDto.setContSignDate(StringUtils.EMPTY);// 合同签订日期
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value);
        return xdca0005DataRespDto;
    }
}
