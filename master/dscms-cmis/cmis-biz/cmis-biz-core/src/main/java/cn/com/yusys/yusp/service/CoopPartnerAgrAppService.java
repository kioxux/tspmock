/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.repository.mapper.BusiImageRelInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAppMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAccInfoMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 10:44:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerAgrAppService {

    @Autowired
    private CoopPartnerAgrAppMapper coopPartnerAgrAppMapper;

    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;

    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;

    @Autowired
    private BusiImageRelInfoMapper busiImageRelInfoMapper;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private MessageSendService messageSendService;
    @Autowired
    private CoopPartnerAgrAddRelService coopPartnerAgrAddRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CoopPartnerAgrApp selectByPrimaryKey(String serno) {
        return coopPartnerAgrAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CoopPartnerAgrApp> selectAll(QueryModel model) {
        List<CoopPartnerAgrApp> records = coopPartnerAgrAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopPartnerAgrApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerAgrApp> list = coopPartnerAgrAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CoopPartnerAgrApp record) {
        return coopPartnerAgrAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CoopPartnerAgrApp record) {
        return coopPartnerAgrAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CoopPartnerAgrApp record) {
        return coopPartnerAgrAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CoopPartnerAgrApp record) {
        return coopPartnerAgrAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        //删除影像数据
        BusiImageRelInfo busiImageRelInfo = new BusiImageRelInfo();
        busiImageRelInfo.setSerno(serno);
        busiImageRelInfoMapper.deleteBySerno(busiImageRelInfo);
        return coopPartnerAgrAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPartnerAgrAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: coopPartnerAgrApp
     * @方法描述: 检查协议金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto checkAmt(CoopPartnerAgrApp coopPartnerAgrApp) {
        ResultDto dto = new ResultDto();
        String no = coopPartnerAgrApp.getPartnerNo();
        String planno = coopPartnerAgrApp.getCoopPlanNo();
        //查询生效的合作协议台账
        QueryModel qm = new QueryModel();
        qm.getCondition().put("coopPlanNo", planno);
        qm.getCondition().put("agrStatus", "01");
        List<CoopPartnerAgrAccInfo> list = coopPartnerAgrAccInfoMapper.selectByModel(qm);
        //查询生效的合作方台账
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("coopPlanNo", planno);
        queryModel.getCondition().put("coopPlanStatus", "1");
        List<CoopPlanAccInfo> listPlan = coopPlanAccInfoMapper.selectByModel(queryModel);

        if (listPlan.size() == 0) {
            dto.setCode("1");
            dto.setMessage("合作方案信息异常");
            return dto;
        }
        CoopPlanAccInfo info = listPlan.get(0);
        BigDecimal amout = new BigDecimal(BigInteger.ZERO);
        for (int i = 0; i < list.size(); i++) {
            CoopPartnerAgrAccInfo app = list.get(i);
            if(coopPartnerAgrApp.getCoopAgrNo().equals(app.getCoopAgrNo())){
                continue;
            }
            amout = amout.add(app.getCoopAgrAmt());
        }
        if( "3".equals(coopPartnerAgrApp.getCoopAgrOprType())){ //续签减原协议金额
            amout = amout.add(coopPartnerAgrApp.getCoopAgrAmt());
            QueryModel qmCoopPartnerAgrAddRel = new QueryModel();
            qmCoopPartnerAgrAddRel.addCondition("serno",coopPartnerAgrApp.getSerno());
            List<CoopPartnerAgrAccInfo> listCoopPartnerAgrAccInfo = coopPartnerAgrAddRelService.selectByModel(qmCoopPartnerAgrAddRel);
            BigDecimal oldAmt = BigDecimal.ZERO;
            if(!CollectionUtils.isEmpty(listCoopPartnerAgrAccInfo)){
                for (int i = 0; i < listCoopPartnerAgrAccInfo.size(); i++) {
                    CoopPartnerAgrAccInfo coopPartnerAgrAccInfo= listCoopPartnerAgrAccInfo.get(i);
                    if(coopPartnerAgrAccInfo!=null && coopPartnerAgrAccInfo.getCoopAgrAmt()!=null){
                        oldAmt = oldAmt.add(coopPartnerAgrAccInfo.getCoopAgrAmt());
                    }
                }
            }
            amout = amout.subtract(oldAmt);
        }else{
            amout = amout.add(coopPartnerAgrApp.getCoopAgrAmt());
        }

        if (amout.compareTo(info.getTotlCoopLmtAmt()) == 1) {
            dto.setCode("1");
            dto.setMessage("协议金额不能超过方案总合作额度");
            return dto;
        }
        return dto;
    }

    /**
     * @函数名称:checkSubmit
     * @函数描述:协议提交之前检查必输项以及合作额度
     * @参数与返回说明:
     * @算法描述:
     */
    public ResultDto checkSubmit(CoopPartnerAgrApp coopPartnerAgrApp) {
        ResultDto dto = new ResultDto();
        String startDate = coopPartnerAgrApp.getCoopAgrStartDate();
        String endDate = coopPartnerAgrApp.getCoopAgrEndDate();
        String signDate = coopPartnerAgrApp.getCoopAgrSignDate();
        BigDecimal coopAgrAmt = coopPartnerAgrApp.getCoopAgrAmt();
        if (StringUtils.isBlank(startDate)) {
            dto.setCode("1");
            dto.setMessage("协议起始日期不能为空");
            return dto;
        }
        if (StringUtils.isBlank(endDate)) {
            dto.setCode("1");
            dto.setMessage("协议截止日期不能为空");
            return dto;
        }
        if (StringUtils.isBlank(signDate)) {
            dto.setCode("1");
            dto.setMessage("协议签订日期不能为空");
            return dto;
        }
        if (coopAgrAmt == null || coopAgrAmt.compareTo(BigDecimal.ZERO) == 0) {
            dto.setCode("1");
            dto.setMessage("协议金额不能为空或者零");
            return dto;
        }
        String no = coopPartnerAgrApp.getPartnerNo();
        QueryModel qm = new QueryModel();
        qm.getCondition().put("partnerNo", no);
        List<CoopPartnerAgrApp> list = coopPartnerAgrAppMapper.selectByModel(qm);
        String planno = coopPartnerAgrApp.getCoopPlanNo();
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("coopPlanNo", planno);
        List<CoopPlanAccInfo> listPlan = coopPlanAccInfoMapper.selectByModel(queryModel);
        if (listPlan.size() == 0) {
            dto.setCode("1");
            dto.setMessage("合作方案信息异常");
            return dto;
        }
        CoopPlanAccInfo info = listPlan.get(0);
        BigDecimal amout = new BigDecimal(BigInteger.ZERO);
        for (int i = 0; i < list.size(); i++) {
            CoopPartnerAgrApp app = list.get(i);
            amout = amout.add(app.getCoopAgrAmt());
        }
        amout = amout.add(coopPartnerAgrApp.getCoopAgrAmt());
        if (amout.compareTo(info.getTotlCoopLmtAmt()) == 1) {
            dto.setCode("1");
            dto.setMessage("协议金额不能超过方案总合作额度");
            return dto;
        }
        return dto;
    }

    /**
     * 资料未全生成影像补扫任务
     * @author jijian_yx
     * @date 2021/9/11 16:03
     **/
    public void createImageSpplInfo(String serno, String bizType, String instanceId,String iqpName) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(serno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            CoopPartnerAgrApp coopPartnerAgrApp = coopPartnerAgrAppMapper.selectByPrimaryKey(serno);
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(serno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_05);// 影像补扫类型 05:合作方协议签订影像补扫
            docImageSpplClientDto.setCusId(coopPartnerAgrApp.getPartnerNo());// 客户号
            docImageSpplClientDto.setCusName(coopPartnerAgrApp.getPartnerName());// 客户名称
            docImageSpplClientDto.setInputId(coopPartnerAgrApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(coopPartnerAgrApp.getManagerBrId());// 登记机构
            docImageSpplClientDto.setContNo(coopPartnerAgrApp.getCoopAgrNo());
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), coopPartnerAgrApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", coopPartnerAgrApp.getPartnerName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }
}
