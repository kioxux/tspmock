/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyTaskDivis
 * @类描述: lmt_survey_task_divis数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 10:25:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_survey_task_divis")
public class LmtSurveyTaskDivis extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = false, length = 40)
	private String surveySerno;
	
	/** 第三方业务流水号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 80)
	private String bizSerno;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = false, length = 80)
	private String prdName;
	
	/** 产品类型 **/
	@Column(name = "PRD_TYPE", unique = false, nullable = false, length = 5)
	private String prdType;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = false, length = 40)
	private String prdId;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 80)
	private String appChnl;
	
	/** 贷款类别 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
	private String loanType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = false, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = false, length = 32)
	private String certCode;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 手机号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 11)
	private String phone;
	
	/** 工作 **/
	@Column(name = "WORK", unique = false, nullable = true, length = 200)
	private String work;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
	private String loanPurp;
	
	/** 客户经理名称 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 80)
	private String managerName;
	
	/** 客户经理编号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 客户经理片区 **/
	@Column(name = "MANAGER_AREA", unique = false, nullable = true, length = 32)
	private String managerArea;
	
	/** 是否线下调查 **/
	@Column(name = "IS_STOP_OFFLINE", unique = false, nullable = true, length = 1)
	private String isStopOffline;
	
	/** 进件时间 **/
	@Column(name = "INTO_TIME", unique = false, nullable = true, length = 20)
	private String intoTime;
	
	/** 调查分配状态 **/
	@Column(name = "DIVIS_STATUS", unique = false, nullable = true, length = 5)
	private String divisStatus;
	
	/** 客户经理确认状态 **/
	@Column(name = "MAR_CONFIRM_STATUS", unique = false, nullable = true, length = 5)
	private String marConfirmStatus;
	
	/** 分配时间 **/
	@Column(name = "DIVIS_TIME", unique = false, nullable = true, length = 20)
	private String divisTime;
	
	/** 分配人 **/
	@Column(name = "DIVIS_ID", unique = false, nullable = true, length = 32)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="divisIdName")
	private String divisId;
	
	/** 营销人 **/
	@Column(name = "MAR_ID", unique = false, nullable = true, length = 32)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="marIdName")
	private String marId;
	
	/** 处理人 **/
	@Column(name = "PRC_ID", unique = false, nullable = true, length = 32)
	private String prcId;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 20)
	private String belgLine;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 20)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 20)
	private java.util.Date updateTime;

	/** 获客来源 **/
	@Column(name = "CUS_CHANNEL", unique = false, nullable = true, length = 5)
	private String cusChannel;

	/** 经营地址 **/
	@Column(name = "OPER_ADDR", unique = false, nullable = true, length = 2000)
	private String operAddr;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	
    /**
     * @return prdType
     */
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param work
	 */
	public void setWork(String work) {
		this.work = work;
	}
	
    /**
     * @return work
     */
	public String getWork() {
		return this.work;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}
	
    /**
     * @return loanPurp
     */
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerArea
	 */
	public void setManagerArea(String managerArea) {
		this.managerArea = managerArea;
	}
	
    /**
     * @return managerArea
     */
	public String getManagerArea() {
		return this.managerArea;
	}
	
	/**
	 * @param isStopOffline
	 */
	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline;
	}
	
    /**
     * @return isStopOffline
     */
	public String getIsStopOffline() {
		return this.isStopOffline;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime;
	}
	
    /**
     * @return intoTime
     */
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param divisStatus
	 */
	public void setDivisStatus(String divisStatus) {
		this.divisStatus = divisStatus;
	}
	
    /**
     * @return divisStatus
     */
	public String getDivisStatus() {
		return this.divisStatus;
	}
	
	/**
	 * @param marConfirmStatus
	 */
	public void setMarConfirmStatus(String marConfirmStatus) {
		this.marConfirmStatus = marConfirmStatus;
	}
	
    /**
     * @return marConfirmStatus
     */
	public String getMarConfirmStatus() {
		return this.marConfirmStatus;
	}
	
	/**
	 * @param divisTime
	 */
	public void setDivisTime(String divisTime) {
		this.divisTime = divisTime;
	}
	
    /**
     * @return divisTime
     */
	public String getDivisTime() {
		return this.divisTime;
	}
	
	/**
	 * @param divisId
	 */
	public void setDivisId(String divisId) {
		this.divisId = divisId;
	}
	
    /**
     * @return divisId
     */
	public String getDivisId() {
		return this.divisId;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId;
	}
	
    /**
     * @return marId
     */
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param prcId
	 */
	public void setPrcId(String prcId) {
		this.prcId = prcId;
	}
	
    /**
     * @return prcId
     */
	public String getPrcId() {
		return this.prcId;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getCusChannel() {
		return cusChannel;
	}

	public void setCusChannel(String cusChannel) {
		this.cusChannel = cusChannel;
	}

	public String getOperAddr() {
		return operAddr;
	}

	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr;
	}
}