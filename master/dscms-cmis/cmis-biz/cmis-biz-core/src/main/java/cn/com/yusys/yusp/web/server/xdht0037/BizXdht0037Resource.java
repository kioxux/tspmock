package cn.com.yusys.yusp.web.server.xdht0037;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0037.req.Xdht0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0037.resp.Xdht0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:大家e贷、乐悠金根据客户号查询房贷首付款比例
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0037:大家e贷、乐悠金根据客户号查询房贷首付款比例")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0037Resource.class);

    /**
     * 交易码：xdht0037
     * 交易描述：大家e贷、乐悠金根据客户号查询房贷首付款比例
     *
     * @param xdht0037DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大家e贷、乐悠金根据客户号查询房贷首付款比例")
    @PostMapping("/xdht0037")
    protected @ResponseBody
    ResultDto<Xdht0037DataRespDto> xdht0037(@Validated @RequestBody Xdht0037DataReqDto xdht0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037DataReqDto));
        Xdht0037DataRespDto xdht0037DataRespDto = new Xdht0037DataRespDto();// 响应Dto:大家e贷、乐悠金根据客户号查询房贷首付款比例
        ResultDto<Xdht0037DataRespDto> xdht0037DataResultDto = new ResultDto<>();
        String cusId = xdht0037DataReqDto.getCusId();//客户号
        try {
            // 从xdht0037DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            xdht0037DataRespDto.setFirstpayPerc(new BigDecimal(0L));// 首付款比例
            // 封装xdht0037DataResultDto中正确的返回码和返回信息
            xdht0037DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0037DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, e.getMessage());
            // 封装xdht0037DataResultDto中异常返回码和返回信息
            xdht0037DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0037DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0037DataRespDto到xdht0037DataResultDto中
        xdht0037DataResultDto.setData(xdht0037DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037DataResultDto));
        return xdht0037DataResultDto;
    }
}
