/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.OtherCnyRateAppTax;
import cn.com.yusys.yusp.domain.OtherCnyRateLoanRelOldRate;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.service.OtherCnyRateAppFinDetailsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppFinDetailsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-05 14:38:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/othercnyrateappfindetails")
public class OtherCnyRateAppFinDetailsResource {
    @Autowired
    private OtherCnyRateAppFinDetailsService otherCnyRateAppFinDetailsService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsService.selectAll(queryModel);
        return new ResultDto<List<OtherCnyRateAppFinDetails>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> index(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateAppFinDetails>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{subSerno}")
    protected ResultDto<OtherCnyRateAppFinDetails> show(@PathVariable("subSerno") String subSerno) {
        OtherCnyRateAppFinDetails otherCnyRateAppFinDetails = otherCnyRateAppFinDetailsService.selectByPrimaryKey(subSerno);
        return new ResultDto<OtherCnyRateAppFinDetails>(otherCnyRateAppFinDetails);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherCnyRateAppFinDetails> create(@RequestBody OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) throws URISyntaxException {
        otherCnyRateAppFinDetailsService.insert(otherCnyRateAppFinDetails);
        return new ResultDto<OtherCnyRateAppFinDetails>(otherCnyRateAppFinDetails);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) throws URISyntaxException {
        int result = otherCnyRateAppFinDetailsService.update(otherCnyRateAppFinDetails);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{subSerno}")
    protected ResultDto<Integer> delete(@PathVariable("subSerno") String subSerno) {
        int result = otherCnyRateAppFinDetailsService.deleteByPrimaryKey(subSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherCnyRateAppFinDetailsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: addothercnyrateappfindetails
     * @方法描述: 新增本次融资信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/addothercnyrateappfindetails")
    protected ResultDto addothercnyrateappfindetails(@RequestBody OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) {
        return otherCnyRateAppFinDetailsService.addothercnyrateappfindetails(otherCnyRateAppFinDetails);
    }

    /**
     * @方法名称: selectEffectiveListByCusId
     * @方法描述: 根据客户编号获取客户有效定价申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @GetMapping("/selectEffectiveList")
    protected ResultDto selectEffectiveList(QueryModel queryModel) {
        return otherCnyRateAppFinDetailsService.selectEffectiveList(queryModel);
    }

    /**
     * @方法名称: lastFinDetails
     * @方法描述: 获取上期融资明细信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @GetMapping("/lastFinDetails")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> lastFinDetails(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> lastFinDetails = otherCnyRateAppFinDetailsService.lastFinDetails(queryModel);
        return new ResultDto<>(lastFinDetails);
    }

    /**
     * @方法名称: currFinDetails
     * @方法描述: 获取本期融资明细信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @GetMapping("/currFinDetails")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> currFinDetails(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> currFinDetails = otherCnyRateAppFinDetailsService.currFinDetails(queryModel);
        return new ResultDto<>(currFinDetails);
    }

    /**
     * @方法名称: grpFinDetails
     * @方法描述: 集团企业及关联企业在我行上期融资情况
     * @参数与返回说明:
     * @算法描述: 无
     */
    @GetMapping("/grpFinDetails")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> grpFinDetails(QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> currFinDetails = otherCnyRateAppFinDetailsService.grpFinDetails(queryModel);
        return new ResultDto<>(currFinDetails);
    }

    /**
     * @方法名称: checkothercnyrateappfindetails
     * @方法描述: 校验本次融资信息是否存在
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/checkothercnyrateappfindetails")
    protected ResultDto checkothercnyrateappfindetails(@RequestBody OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) {
        return otherCnyRateAppFinDetailsService.checkothercnyrateappfindetails(otherCnyRateAppFinDetails);
    }

    /**
     * @方法名称: updateothercnyrateappfindetails
     * @方法描述: 修改融资信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updateothercnyrateappfindetails")
    protected ResultDto updateothercnyrateappfindetails(@RequestBody OtherCnyRateAppFinDetails otherCnyRateAppFinDetails) {
        return otherCnyRateAppFinDetailsService.updateothercnyrateappfindetails(otherCnyRateAppFinDetails);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<OtherCnyRateAppFinDetails>> selectByModel(@RequestBody QueryModel queryModel) {
        List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateAppFinDetails>>(list);
    }
}
