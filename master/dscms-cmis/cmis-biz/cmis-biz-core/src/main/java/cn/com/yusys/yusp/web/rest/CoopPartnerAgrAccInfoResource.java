/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPartnerAgrAccInfo;
import cn.com.yusys.yusp.service.CoopPartnerAgrAccInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAccInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-27 10:44:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cooppartneragraccinfo")
public class CoopPartnerAgrAccInfoResource {
    @Autowired
    private CoopPartnerAgrAccInfoService coopPartnerAgrAccInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPartnerAgrAccInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPartnerAgrAccInfo> list = coopPartnerAgrAccInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopPartnerAgrAccInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPartnerAgrAccInfo>> index(QueryModel queryModel) {
        List<CoopPartnerAgrAccInfo> list = coopPartnerAgrAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerAgrAccInfo>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPartnerAgrAccInfo>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CoopPartnerAgrAccInfo> list = coopPartnerAgrAccInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPartnerAgrAccInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{coopAgrNo}")
    protected ResultDto<CoopPartnerAgrAccInfo> show(@PathVariable("coopAgrNo") String coopAgrNo) {
        CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = coopPartnerAgrAccInfoService.selectByPrimaryKey(coopAgrNo);
        return new ResultDto<CoopPartnerAgrAccInfo>(coopPartnerAgrAccInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPartnerAgrAccInfo> create(@RequestBody CoopPartnerAgrAccInfo coopPartnerAgrAccInfo) throws URISyntaxException {
        coopPartnerAgrAccInfoService.insert(coopPartnerAgrAccInfo);
        return new ResultDto<CoopPartnerAgrAccInfo>(coopPartnerAgrAccInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPartnerAgrAccInfo coopPartnerAgrAccInfo) throws URISyntaxException {
        int result = coopPartnerAgrAccInfoService.update(coopPartnerAgrAccInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{coopAgrNo}")
    protected ResultDto<Integer> delete(@PathVariable("coopAgrNo") String coopAgrNo) {
        int result = coopPartnerAgrAccInfoService.deleteByPrimaryKey(coopAgrNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPartnerAgrAccInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
