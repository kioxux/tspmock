/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppRepay
 * @类描述: iqp_loan_app_repay数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-04 10:35:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_app_repay")
public class IqpLoanAppRepay extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;

	/** 还款方式 STD_ZB_REPAY_TYP **/
	@Column(name = "REPAY_TYPE", unique = false, nullable = true, length = 5)
	private String repayType;

	/** 停本付息期间 STD_ZB_PINT_TERM **/
	@Column(name = "STOP_PINT_TERM", unique = false, nullable = true, length = 5)
	private String stopPintTerm;

	/** 还款间隔周期 STD_ZB_REPAY_TERM **/
	@Column(name = "REPAY_TERM", unique = false, nullable = true, length = 5)
	private String repayTerm;

	/** 还款间隔 STD_ZB_REPAY_SPACE **/
	@Column(name = "REPAY_SPACE", unique = false, nullable = true, length = 5)
	private String repaySpace;

	/** 还款日确定规则 STD_ZB_REPAY_RULE **/
	@Column(name = "REPAY_RULE", unique = false, nullable = true, length = 5)
	private String repayRule;

	/** 还款日类型 STD_ZB_REPAY_DT_TYPE **/
	@Column(name = "REPAY_DT_TYPE", unique = false, nullable = true, length = 5)
	private String repayDtType;

	/** 还款日 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 10)
	private Integer repayDate;

	/** 本金宽限方式 STD_ZB_GRAPER_TYPE **/
	@Column(name = "CAP_GRAPER_TYPE", unique = false, nullable = true, length = 5)
	private String capGraperType;

	/** 本金宽限天数 **/
	@Column(name = "CAP_GRAPER_DAY", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal capGraperDay;

	/** 利息宽限方式 STD_ZB_GRAPER_TYPE **/
	@Column(name = "INT_GRAPER_TYPE", unique = false, nullable = true, length = 5)
	private String intGraperType;

	/** 本金宽限天数 **/
	@Column(name = "INT_GRAPER_DAY", unique = false, nullable = true, length = 3)
	private java.math.BigDecimal intGraperDay;

	/** 扣款扣息方式 STD_ZB_DEDUCT_TYPE **/
	@Column(name = "DEDUCT_DEDU_TYPE", unique = false, nullable = true, length = 5)
	private String deductDeduType;

	/** 还款频率类型 STD_ZB_REPAY_FRE_TYPE **/
	@Column(name = "REPAY_FRE_TYPE", unique = false, nullable = true, length = 5)
	private String repayFreType;

	/** 本息还款频率 **/
	@Column(name = "REPAY_FRE", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal repayFre;

	/** 提前还款违约金免除时间(月) **/
	@Column(name = "LIQU_FREE_TIME", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal liquFreeTime;

	/** 分段方式 STD_ZB_SUB_TYPE **/
	@Column(name = "SUB_TYPE", unique = false, nullable = true, length = 5)
	private String subType;

	/** 保留期限 **/
	@Column(name = "RESERVE_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal reserveTerm;

	/** 计算期限 **/
	@Column(name = "CAL_TERM", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal calTerm;

	/** 保留金额 **/
	@Column(name = "RESERVE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal reserveAmt;

	/** 第一阶段还款期数 **/
	@Column(name = "REPAY_TERM_ONE", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal repayTermOne;

	/** 第一阶段还款本金 **/
	@Column(name = "REPAY_AMT_ONE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmtOne;

	/** 第二阶段还款期数 **/
	@Column(name = "REPAY_TERM_TWO", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal repayTermTwo;

	/** 第二阶段还款本金 **/
	@Column(name = "REPAY_AMT_TWO", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmtTwo;

	/** 利率选取日期种类 STD_ZB_RATE_SEL_TYPE **/
	@Column(name = "RATE_SEL_TYPE", unique = false, nullable = true, length = 5)
	private String rateSelType;

	/** 贴息方式 STD_ZB_SBSY_WAY **/
	@Column(name = "SBSY_MODE", unique = false, nullable = true, length = 5)
	private String sbsyMode;

	/** 贴息比例 **/
	@Column(name = "SBSY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyPerc;

	/** 贴息金额 **/
	@Column(name = "SBSY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyAmt;

	/** 贴息单位名称 **/
	@Column(name = "SBSY_UNIT_NAME", unique = false, nullable = true, length = 80)
	private String sbsyUnitName;

	/** 贴息方账户 **/
	@Column(name = "SBSY_ACCT", unique = false, nullable = true, length = 40)
	private String sbsyAcct;

	/** 贴息方账户户名 **/
	@Column(name = "SBSY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String sbsyAcctName;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}

    /**
     * @return repayType
     */
	public String getRepayType() {
		return this.repayType;
	}

	/**
	 * @param stopPintTerm
	 */
	public void setStopPintTerm(String stopPintTerm) {
		this.stopPintTerm = stopPintTerm;
	}

    /**
     * @return stopPintTerm
     */
	public String getStopPintTerm() {
		return this.stopPintTerm;
	}

	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(String repayTerm) {
		this.repayTerm = repayTerm;
	}

    /**
     * @return repayTerm
     */
	public String getRepayTerm() {
		return this.repayTerm;
	}

	/**
	 * @param repaySpace
	 */
	public void setRepaySpace(String repaySpace) {
		this.repaySpace = repaySpace;
	}

    /**
     * @return repaySpace
     */
	public String getRepaySpace() {
		return this.repaySpace;
	}

	/**
	 * @param repayRule
	 */
	public void setRepayRule(String repayRule) {
		this.repayRule = repayRule;
	}

    /**
     * @return repayRule
     */
	public String getRepayRule() {
		return this.repayRule;
	}

	/**
	 * @param repayDtType
	 */
	public void setRepayDtType(String repayDtType) {
		this.repayDtType = repayDtType;
	}

    /**
     * @return repayDtType
     */
	public String getRepayDtType() {
		return this.repayDtType;
	}

	/**
	 * @param repayDate
	 */
	public void setRepayDate(Integer repayDate) {
		this.repayDate = repayDate;
	}

    /**
     * @return repayDate
     */
	public Integer getRepayDate() {
		return this.repayDate;
	}

	/**
	 * @param capGraperType
	 */
	public void setCapGraperType(String capGraperType) {
		this.capGraperType = capGraperType;
	}

    /**
     * @return capGraperType
     */
	public String getCapGraperType() {
		return this.capGraperType;
	}

	/**
	 * @param capGraperDay
	 */
	public void setCapGraperDay(java.math.BigDecimal capGraperDay) {
		this.capGraperDay = capGraperDay;
	}

    /**
     * @return capGraperDay
     */
	public java.math.BigDecimal getCapGraperDay() {
		return this.capGraperDay;
	}

	/**
	 * @param intGraperType
	 */
	public void setIntGraperType(String intGraperType) {
		this.intGraperType = intGraperType;
	}

    /**
     * @return intGraperType
     */
	public String getIntGraperType() {
		return this.intGraperType;
	}

	/**
	 * @param intGraperDay
	 */
	public void setIntGraperDay(java.math.BigDecimal intGraperDay) {
		this.intGraperDay = intGraperDay;
	}

    /**
     * @return intGraperDay
     */
	public java.math.BigDecimal getIntGraperDay() {
		return this.intGraperDay;
	}

	/**
	 * @param deductDeduType
	 */
	public void setDeductDeduType(String deductDeduType) {
		this.deductDeduType = deductDeduType;
	}

    /**
     * @return deductDeduType
     */
	public String getDeductDeduType() {
		return this.deductDeduType;
	}

	/**
	 * @param repayFreType
	 */
	public void setRepayFreType(String repayFreType) {
		this.repayFreType = repayFreType;
	}

    /**
     * @return repayFreType
     */
	public String getRepayFreType() {
		return this.repayFreType;
	}

	/**
	 * @param repayFre
	 */
	public void setRepayFre(java.math.BigDecimal repayFre) {
		this.repayFre = repayFre;
	}

    /**
     * @return repayFre
     */
	public java.math.BigDecimal getRepayFre() {
		return this.repayFre;
	}

	/**
	 * @param liquFreeTime
	 */
	public void setLiquFreeTime(java.math.BigDecimal liquFreeTime) {
		this.liquFreeTime = liquFreeTime;
	}

    /**
     * @return liquFreeTime
     */
	public java.math.BigDecimal getLiquFreeTime() {
		return this.liquFreeTime;
	}

	/**
	 * @param subType
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

    /**
     * @return subType
     */
	public String getSubType() {
		return this.subType;
	}

	/**
	 * @param reserveTerm
	 */
	public void setReserveTerm(java.math.BigDecimal reserveTerm) {
		this.reserveTerm = reserveTerm;
	}

    /**
     * @return reserveTerm
     */
	public java.math.BigDecimal getReserveTerm() {
		return this.reserveTerm;
	}

	/**
	 * @param calTerm
	 */
	public void setCalTerm(java.math.BigDecimal calTerm) {
		this.calTerm = calTerm;
	}

    /**
     * @return calTerm
     */
	public java.math.BigDecimal getCalTerm() {
		return this.calTerm;
	}

	/**
	 * @param reserveAmt
	 */
	public void setReserveAmt(java.math.BigDecimal reserveAmt) {
		this.reserveAmt = reserveAmt;
	}

    /**
     * @return reserveAmt
     */
	public java.math.BigDecimal getReserveAmt() {
		return this.reserveAmt;
	}

	/**
	 * @param repayTermOne
	 */
	public void setRepayTermOne(java.math.BigDecimal repayTermOne) {
		this.repayTermOne = repayTermOne;
	}

    /**
     * @return repayTermOne
     */
	public java.math.BigDecimal getRepayTermOne() {
		return this.repayTermOne;
	}

	/**
	 * @param repayAmtOne
	 */
	public void setRepayAmtOne(java.math.BigDecimal repayAmtOne) {
		this.repayAmtOne = repayAmtOne;
	}

    /**
     * @return repayAmtOne
     */
	public java.math.BigDecimal getRepayAmtOne() {
		return this.repayAmtOne;
	}

	/**
	 * @param repayTermTwo
	 */
	public void setRepayTermTwo(java.math.BigDecimal repayTermTwo) {
		this.repayTermTwo = repayTermTwo;
	}

    /**
     * @return repayTermTwo
     */
	public java.math.BigDecimal getRepayTermTwo() {
		return this.repayTermTwo;
	}

	/**
	 * @param repayAmtTwo
	 */
	public void setRepayAmtTwo(java.math.BigDecimal repayAmtTwo) {
		this.repayAmtTwo = repayAmtTwo;
	}

    /**
     * @return repayAmtTwo
     */
	public java.math.BigDecimal getRepayAmtTwo() {
		return this.repayAmtTwo;
	}

	/**
	 * @param rateSelType
	 */
	public void setRateSelType(String rateSelType) {
		this.rateSelType = rateSelType;
	}

    /**
     * @return rateSelType
     */
	public String getRateSelType() {
		return this.rateSelType;
	}

	/**
	 * @param sbsyMode
	 */
	public void setSbsyMode(String sbsyMode) {
		this.sbsyMode = sbsyMode;
	}

    /**
     * @return sbsyMode
     */
	public String getSbsyMode() {
		return this.sbsyMode;
	}

	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

    /**
     * @return sbsyPerc
     */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}

	/**
	 * @param sbsyAmt
	 */
	public void setSbsyAmt(java.math.BigDecimal sbsyAmt) {
		this.sbsyAmt = sbsyAmt;
	}

    /**
     * @return sbsyAmt
     */
	public java.math.BigDecimal getSbsyAmt() {
		return this.sbsyAmt;
	}

	/**
	 * @param sbsyUnitName
	 */
	public void setSbsyUnitName(String sbsyUnitName) {
		this.sbsyUnitName = sbsyUnitName;
	}

    /**
     * @return sbsyUnitName
     */
	public String getSbsyUnitName() {
		return this.sbsyUnitName;
	}

	/**
	 * @param sbsyAcct
	 */
	public void setSbsyAcct(String sbsyAcct) {
		this.sbsyAcct = sbsyAcct;
	}

    /**
     * @return sbsyAcct
     */
	public String getSbsyAcct() {
		return this.sbsyAcct;
	}

	/**
	 * @param sbsyAcctName
	 */
	public void setSbsyAcctName(String sbsyAcctName) {
		this.sbsyAcctName = sbsyAcctName;
	}

    /**
     * @return sbsyAcctName
     */
	public String getSbsyAcctName() {
		return this.sbsyAcctName;
	}


}
