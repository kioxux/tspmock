package cn.com.yusys.yusp.service.server.xdcz0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.GetIsXwUserDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.req.Xdcz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.resp.Xdcz0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.PvpAuthorizeMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import cn.com.yusys.yusp.service.server.xdcz0017.Xdcz0017Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class Xdcz0016Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0017Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    /**
     * 业务场景：支用列表查询(微信小程序)
     * 接口说明：根据客户号，查询信贷系统的支用列表一览
     * 1.查询出账状态没有成功出账（<>'2'）、放款金额<=50万、机构为小贷的出账授权表和台账表的待放款一览信息；
     * 2.查询出账状态为成功出账（='2'）的，以及冲正（='A'）、放款金额<=50万、机构为小贷的出账授权表和台账表的已放款一览信息；
     * 3.查询放款金额<=50万、机构为小贷的出账授权表和台账表的所有放款一览信息（出账日期为以当前营业日期为基准，2个月之内，即出账日期>=当前营业日期-2个月）；
     * 4.查询是否该客户是否为小贷客户（管户客户经理是否为小贷客户经理），list返回为空。
     * <p>
     * 适用产品范围：小贷产品
     * <p>
     * 备注:对应老系统代码SendAuthtoCoreAndHsOp.java
     *
     * @param xdcz0016DataReqDto
     * @return
     * @author
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0016DataRespDto xdcz0016(Xdcz0016DataReqDto xdcz0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataReqDto));
        Xdcz0016DataRespDto xdcz0016DataRespDto = new Xdcz0016DataRespDto();
        try {
            String cusNo = xdcz0016DataReqDto.getCusId();
            String queryType = xdcz0016DataReqDto.getQryType();
            String selectSql = "";
            if (!StringUtils.hasLength(queryType) || !StringUtils.hasLength(cusNo)) {
                throw BizException.error(null, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, "查询原因和客户号码不能为空");
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String buzDt = stringRedisTemplate.opsForValue().get("openDay");
            Calendar date1 = Calendar.getInstance();
            date1.setTime(simpleDateFormat.parse(buzDt));
            date1.add(Calendar.DAY_OF_MONTH, -2);

            Calendar date2 = Calendar.getInstance();
            date2.setTime(simpleDateFormat.parse(buzDt));
            date2.add(Calendar.DAY_OF_MONTH, +2);
            //根据查询类型，查询已支用和未支用
            //2020-08-24改造，支用金额从合同50万，改成借据50万
            /********
             * ******授权状态 AUTH_STATUS （auth_status = '2'）
             *0	未出帐
             * 1	已发送核心【未知】
             * 2	已发送核心校验成功
             * 3	已发送核心校验失败
             * 4	出账已撤销
             * 5	已记帐已撤销
             * 6	抹账被核心拒绝
             * 7	抹帐
             * 8	已发送核心未冲正
             * 9	已发送核心冲正成功
             * A	冲正申请
             *
             **********出账状态 TRADE_STATUS  （TRADE_STATUS != '2'）
             * 2	已出账
             * * 0	0
             * 1	未出账
             * *********/

            List<PvpAuthorize> pvpAuthorizeList = null;
            if ("1".equals(queryType)) {
                pvpAuthorizeList = pvpAuthorizeMapper.queryWxDzyList(cusNo, buzDt);
            } else if ("2".equals(queryType)) {
                pvpAuthorizeList = pvpAuthorizeMapper.queryWxYzyList(cusNo);
            } else {
                pvpAuthorizeList = pvpAuthorizeMapper.queryWxAllzyList(cusNo, simpleDateFormat.format(date1.getTime()), simpleDateFormat.format(date2.getTime()));
            }

            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, "查询sql：" + selectSql);

            int countSize = pvpAuthorizeList.size();

            Xdkh0001DataRespDto xdkh0001DataRespDto = queryXdkh0001DataRespDto(cusNo);
            String managerId = xdkh0001DataRespDto.getManagerId();
            // 是否为小贷客户经理
            String isManagerId = "N";
            if (StringUtils.hasLength(managerId)) {
                if (StringUtil.isNotEmpty(managerId)) {//客户经理不能为空
                    logger.info("************xdcz0016*第一步根据客户经理编号【{}】查询客户经理信息开始", managerId);
                    ResultDto<GetIsXwUserDto> getIsXwUserDtoResultDto = adminSmUserService.getIsXWUserByLoginCode(managerId);
                    logger.info("************xdcz0016*第一步根据客户经理编号【{}】查询客户经理信息结束,客户信息【{}】", managerId, JSON.toJSONString(getIsXwUserDtoResultDto));
                    if (ResultDto.success().getCode().equals(getIsXwUserDtoResultDto.getCode())) {
                        GetIsXwUserDto getIsXwUserDto = getIsXwUserDtoResultDto.getData();
                        if (Objects.nonNull(getIsXwUserDto)) {
                            isManagerId = getIsXwUserDto.getIsXWUser();
                        }
                    }
                }
            }
            xdcz0016DataRespDto.setIsManagerId(isManagerId);
            xdcz0016DataRespDto.setManagerIdNo(managerId);
            List<cn.com.yusys.yusp.dto.server.xdcz0016.resp.List> xdczList = new ArrayList<>();
            //有符合要求的支用记录
            if (countSize > 0) {
                for (int i = 0; i < pvpAuthorizeList.size(); i++) {
                    PvpAuthorize pvpAuthorize = pvpAuthorizeList.get(i);
                    String billNo = pvpAuthorize.getBillNo();
                    //控制借据必须是小微信贷系统审批通过和在待出账的借据
                    PvpLoanApp pvpLoanApp = null;
                    QueryModel pvpLoanAppModel = new QueryModel();
                    pvpLoanAppModel.addCondition("billNo", billNo);
                    pvpLoanAppModel.addCondition("approveStatus", "997");
                    List<PvpLoanApp> pvpLoanAppList = pvpLoanAppMapper.selectByModel(pvpLoanAppModel);
                    if (CollectionUtils.nonEmpty(pvpLoanAppList)) {
                        pvpLoanApp = pvpLoanAppList.get(0);
                    }
                    if (null == pvpLoanApp) {
                        countSize--;
                        continue;
                    }
                    cn.com.yusys.yusp.dto.server.xdcz0016.resp.List list = new cn.com.yusys.yusp.dto.server.xdcz0016.resp.List();
                    list.setLoanAmt(pvpLoanApp.getPvpAmt());
                    list.setBizType(pvpLoanApp.getPrdId());
                    list.setBillStartDate(pvpLoanApp.getLoanStartDate());
                    list.setBillEndDate(pvpLoanApp.getLoanEndDate());
                    list.setLoanAcctNo(pvpLoanApp.getLoanPayoutAccno());
                    list.setRepayAcctNo(pvpLoanApp.getRepayAccno());
                    list.setPrdName(pvpLoanApp.getPrdName());
                    list.setCusName(pvpLoanApp.getCusName());
                    list.setRepayType(pvpLoanApp.getRepayMode());
                    list.setAssureMeans(pvpLoanApp.getGuarMode());
                    list.setBillNo(pvpLoanApp.getBillNo());
                    list.setPhone(xdkh0001DataRespDto.getMobileNo());
                    xdczList.add(list);
                }
                xdcz0016DataRespDto.setResponseRecordQnt(String.valueOf(countSize));
            } else {
                xdcz0016DataRespDto.setResponseRecordQnt("0");
            }

            xdcz0016DataRespDto.setResponseList(xdczList);

        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, e.getMessage());
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, e.getMessage());
            throw e;
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataRespDto));
        return xdcz0016DataRespDto;
    }

    /**
     * 客户基本信息
     *
     * @param cusId
     * @return
     */
    public Xdkh0001DataRespDto queryXdkh0001DataRespDto(String cusId) {
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();//请求Dto：查询个人客户基本信息
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;//响应Dto：查询个人客户基本信息
        xdkh0001DataReqDto.setCusId(cusId);
        xdkh0001DataReqDto.setQueryType("01");

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        ResultDto<Xdkh0001DataRespDto> xdkh0001DataRespDtoResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDtoResultDto));

        String xdkh0001Code = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdkh0001Meesage = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            xdkh0001DataRespDto = xdkh0001DataRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xdkh0001Code, xdkh0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value);
        return xdkh0001DataRespDto;
    }

}
