package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class RiskItem0109Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0109Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: riskItem0109
     * @方法描述: 按期付息按计划还本校验
     * @参数与返回说明:
     * @算法描述:
     * 还款方式选择按期付息按计划还本的时候，如果不填写还本计划，则拦截
     * @创建人: yfs
     * @创建时间: 2021-09-02 23:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public  RiskResultDto riskItem0109(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("按期付息按计划还本校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        // 普通贷款合同申请流程
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002,bizType)) {
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if(Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 还款方式选择按期付息按计划还本的时候，如果不填写还本计划，则拦截
            if(Objects.equals("A040",iqpLoanApp.getRepayMode())) {
                QueryModel model = new QueryModel();
                model.addCondition("serno",iqpLoanApp.getIqpSerno());
                model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                model.setSort("terms desc");
                model.setPage(99);
                List<RepayCapPlan>  list = repayCapPlanService.selectAll(model);
                if(CollectionUtils.isEmpty(list)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10901);
                    return riskResultDto;
                }
                if(list.size() < 2) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10903);
                    return riskResultDto;
                }
                Set<String> set = new HashSet<>();
                Boolean flag = true;
                BigDecimal total = BigDecimal.ZERO;
                String tempDate = "9999-12-31";
                for (int i = 0; i < list.size(); i++) {
                    RepayCapPlan repayCapPlan = list.get(i);
                    if(i>0){
                        RepayCapPlan LastRepayCapPlan = list.get(i-1);
                        // 还款计划的期数越大还款日期越大
                        if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(LastRepayCapPlan.getRepayDate(),"yyyy-MM-dd"))>=0) {
                            log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                            log.info("上期期数【{}】的还款日期为【{}】",LastRepayCapPlan.getTerms(),LastRepayCapPlan.getRepayDate());
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10909);
                            return riskResultDto;
                        }
                    }
                    if(StringUtils.isBlank(repayCapPlan.getRepayDate()) || Objects.isNull(repayCapPlan.getRepayAmt())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10902);
                        return riskResultDto;
                    }
                    // 还款日期大于合同开始日期
                    if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(iqpLoanApp.getStartDate(),"yyyy-MM-dd"))<= 0) {
                        log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                        log.info("合同的起始日期为【{}】",iqpLoanApp.getStartDate());
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10904);
                        return riskResultDto;
                    }
                    // 还款日期小于等于合同到期日
                    if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(iqpLoanApp.getEndDate(),"yyyy-MM-dd"))> 0) {
                        log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                        log.info("合同的到期日期为【{}】",iqpLoanApp.getEndDate());
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10905);
                        return riskResultDto;
                    }
                    // 最后一期还款日期等于合同到日期日期
                    if(flag) {
                        if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(iqpLoanApp.getEndDate(),"yyyy-MM-dd"))!= 0) {
                            log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                            log.info("合同的到期日期为【{}】",iqpLoanApp.getEndDate());
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10906);
                            return riskResultDto;
                        }
                        flag = false;
                    }
                    // 还款金额不能小于或者等于0
                    if(repayCapPlan.getRepayAmt().compareTo(BigDecimal.ZERO) <= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10907);
                        return riskResultDto;
                    }
                    // 还款金额不能等于借据金额
                    if(repayCapPlan.getRepayAmt().compareTo(Objects.nonNull(iqpLoanApp.getCvtCnyAmt()) ? iqpLoanApp.getCvtCnyAmt() : BigDecimal.ZERO) == 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10908);
                        return riskResultDto;
                    }
                    total = total.add(repayCapPlan.getRepayAmt());
                    set.add(repayCapPlan.getRepayDate());
                    tempDate = repayCapPlan.getRepayDate();
                }
                if(total.compareTo(Objects.nonNull(iqpLoanApp.getCvtCnyAmt()) ? iqpLoanApp.getCvtCnyAmt() : BigDecimal.ZERO) != 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10910);
                    return riskResultDto;
                }
                if(set.size() != list.size()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10911);
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX011,bizType)) { // 对公贷款出账
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            // 还款方式选择按期付息按计划还本的时候，如果不填写还本计划，则拦截
            if(Objects.equals("A040",pvpLoanApp.getRepayMode())) {
                QueryModel model = new QueryModel();
                model.addCondition("serno",pvpLoanApp.getPvpSerno());
                model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                model.setSort("terms desc");
                model.setPage(99);
                List<RepayCapPlan>  list = repayCapPlanService.selectAll(model);
                if(CollectionUtils.isEmpty(list)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10901);
                    return riskResultDto;
                }
                if(list.size() < 2) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10903);
                    return riskResultDto;
                }
                Set<String> set = new HashSet<>();
                Boolean flag = true;
                BigDecimal total = BigDecimal.ZERO;
                String tempDate = "9999-12-31";
                for (int i = 0; i < list.size(); i++) {
                    RepayCapPlan repayCapPlan = list.get(i);
                    if(i>0){
                        RepayCapPlan LastRepayCapPlan = list.get(i-1);
                        // 还款计划的期数越大还款日期越大
                        if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(LastRepayCapPlan.getRepayDate(),"yyyy-MM-dd"))>=0) {
                            log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                            log.info("上期期数【{}】的还款日期为【{}】",LastRepayCapPlan.getTerms(),LastRepayCapPlan.getRepayDate());
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10909);
                            return riskResultDto;
                        }
                    }
                    // 还款日期大于合同开始日期
                    if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(pvpLoanApp.getStartDate(),"yyyy-MM-dd"))<= 0) {
                        log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                        log.info("合同的起始日期为【{}】",pvpLoanApp.getStartDate());
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10904);
                        return riskResultDto;
                    }
                    // 还款日期小于等于合同到期日
                    if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(pvpLoanApp.getEndDate(),"yyyy-MM-dd"))> 0) {
                        log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                        log.info("合同的到期日期为【{}】",pvpLoanApp.getEndDate());
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10905);
                        return riskResultDto;
                    }
                    // 最后一期还款日期等于合同到日期日期
                    if(flag) {
                        if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(pvpLoanApp.getEndDate(),"yyyy-MM-dd"))!= 0) {
                            log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                            log.info("合同的到期日期为【{}】",pvpLoanApp.getEndDate());
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10906);
                            return riskResultDto;
                        }
                        flag = false;
                    }
                    // 还款金额不能小于或者等于0
                    if(repayCapPlan.getRepayAmt().compareTo(BigDecimal.ZERO) <= 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10907);
                        return riskResultDto;
                    }
                    // 还款金额不能等于借据金额
                    if(repayCapPlan.getRepayAmt().compareTo(Objects.nonNull(pvpLoanApp.getCvtCnyAmt()) ? pvpLoanApp.getCvtCnyAmt() : BigDecimal.ZERO) == 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10908);
                        return riskResultDto;
                    }
                    total = total.add(repayCapPlan.getRepayAmt());
                    set.add(repayCapPlan.getRepayDate());
                    tempDate = repayCapPlan.getRepayDate();
                }
                if(total.compareTo(Objects.nonNull(pvpLoanApp.getCvtCnyAmt()) ? pvpLoanApp.getCvtCnyAmt() : BigDecimal.ZERO) != 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10915);
                    return riskResultDto;
                }
                if(set.size() != list.size()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10911);
                    return riskResultDto;
                }
            }
        }else if(Objects.equals(CmisFlowConstants.FLOW_ID_DGSX01,bizType)){// 单一客户授信申报
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            if(Objects.isNull(lmtApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
            // 根据授信申请流水号查询授信申请分项明细
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
            if(CollectionUtils.isEmpty(lmtAppSubList)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0023);
                return riskResultDto;
            }
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                // 根据授信申请流水号查询授信申请分项明细
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                if(CollectionUtils.isEmpty(lmtAppSubPrdList)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0024);
                    return riskResultDto;
                }
                for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                    // 还款方式选择按期付息按计划还本的时候，如果不填写还本计划，则拦截
                    if(CmisCommonConstants.STD_REPAY_MODE_A040.equals(lmtAppSubPrd.getRepayMode())){
                        QueryModel model = new QueryModel();
                        model.addCondition("serno",lmtAppSubPrd.getSubPrdSerno());
                        model.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
                        model.setSort("terms desc");
                        model.setPage(99);
                        List<RepayCapPlan> list = repayCapPlanService.selectAll(model);
                        if(CollectionUtils.isEmpty(list)) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10901);
                            return riskResultDto;
                        }
                        // 还本计划期数须大于2期
                        if(list.size() <= 2) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10903);
                            return riskResultDto;
                        }
                        Set<String> set = new HashSet<>();
                        BigDecimal total = BigDecimal.ZERO;
                        String tempDate = "9999-12-31";
                        for (int i = 0; i < list.size(); i++) {
                            RepayCapPlan repayCapPlan = list.get(i);
                            if(i>0){
                                RepayCapPlan LastRepayCapPlan = list.get(i-1);
                                // 还款计划的期数越大还款日期越大
                                if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(LastRepayCapPlan.getRepayDate(),"yyyy-MM-dd"))>=0) {
                                    log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                                    log.info("上期期数【{}】的还款日期为【{}】",LastRepayCapPlan.getTerms(),LastRepayCapPlan.getRepayDate());
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10909);
                                    return riskResultDto;
                                }
                            }
                                // 还款日期须大于当前营业日期
                            if(DateUtils.compare(DateUtils.parseDate(repayCapPlan.getRepayDate(),"yyyy-MM-dd"),DateUtils.parseDate(stringRedisTemplate.opsForValue().get("openDay"),"yyyy-MM-dd"))<= 0) {
                                log.info("当前期数【{}】的还款日期为【{}】",repayCapPlan.getTerms(),repayCapPlan.getRepayDate());
                                log.info("当前营业日期为【{}】",stringRedisTemplate.opsForValue().get("openDay"));
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10914);
                                return riskResultDto;
                            }

                            // 还款金额不能小于或者等于0
                            if(repayCapPlan.getRepayAmt().compareTo(BigDecimal.ZERO) <= 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10907);
                                return riskResultDto;
                            }
                            // 还款金额不能等于授信额度
                            if(repayCapPlan.getRepayAmt().compareTo(Objects.nonNull(lmtAppSubPrd.getLmtAmt()) ? lmtAppSubPrd.getLmtAmt() : BigDecimal.ZERO) == 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10912);
                                return riskResultDto;
                            }
                            total = total.add(repayCapPlan.getRepayAmt());
                            set.add(repayCapPlan.getRepayDate());
                            tempDate = repayCapPlan.getRepayDate();
                        }
                        // 还本计划总金额须等于授信额度
                        if(total.compareTo(Objects.nonNull(lmtAppSubPrd.getLmtAmt()) ? lmtAppSubPrd.getLmtAmt() : BigDecimal.ZERO) != 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10913);
                            return riskResultDto;
                        }
                        // 还款日期不允许重复
                        if(set.size() != list.size()) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10911);
                            return riskResultDto;
                        }
                    }
                }
            }
        }else{
            // 不需要此项校验
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
            return riskResultDto;
        }
        log.info("按期付息按计划还本校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}