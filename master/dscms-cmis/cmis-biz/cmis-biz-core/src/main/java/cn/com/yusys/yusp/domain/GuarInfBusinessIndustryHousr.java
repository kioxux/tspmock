/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBusinessIndustryHousr
 * @类描述: guar_inf_business_industry_housr数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_business_industry_housr")
public class GuarInfBusinessIndustryHousr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 一手/二手标识 STD_ZB_IS_USED **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	@Column(name = "TWOCARD2ONE_IND", unique = false, nullable = true, length = 10)
	private String twocard2oneInd;
	
	/** 产权证号 **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 40)
	private String houseLandNo;
	
	/** 该产证是否全部抵押 STD_ZB_YES_NO **/
	@Column(name = "HOUSE_ALL_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	@Column(name = "PART_REG_POSITION_DESC", unique = false, nullable = true, length = 750)
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	@Column(name = "BUSINESS_HOUSE_NO", unique = false, nullable = true, length = 40)
	private String businessHouseNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 8)
	private String buildArea;
	
	/** 竣工日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 建成年份 **/
	@Column(name = "ACTIVATE_YEARS", unique = false, nullable = true, length = 10)
	private String activateYears;
	
	/** 楼龄 **/
	@Column(name = "FLOOR_AGE", unique = false, nullable = true, length = 3)
	private String floorAge;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 门牌号/弄号 **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 100)
	private String houseNo;
	
	/** 产权地址 **/
	@Column(name = "POC_ADDR", unique = false, nullable = true, length = 100)
	private String pocAddr;
	
	/** 层次（标的楼层） **/
	@Column(name = "BDLC", unique = false, nullable = true, length = 3)
	private String bdlc;
	
	/** 层数（标的楼高） **/
	@Column(name = "BDGD", unique = false, nullable = true, length = 3)
	private String bdgd;
	
	/** 房地产所在地段情况 STD_ZB_HOUSE_PLACE_INFO **/
	@Column(name = "HOUSE_PLACE_INFO", unique = false, nullable = true, length = 10)
	private String housePlaceInfo;
	
	/** 土地证号 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 40)
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_USE_QUAL **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_USE_WAY **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 100)
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LAND_PURP **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 750)
	private String landExplain;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_USE_AREA", unique = false, nullable = true, length = 18)
	private String landUseArea;
	
	/** 土地使用权单位 **/
	@Column(name = "LAND_USE_UNIT", unique = false, nullable = true, length = 100)
	private String landUseUnit;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	@Column(name = "HOUSE_USE_TYPE", unique = false, nullable = true, length = 10)
	private String houseUseType;
	
	/** 承租人名称 **/
	@Column(name = "LESSEE_NAME", unique = false, nullable = true, length = 100)
	private String lesseeName;
	
	/** 年租金（元） **/
	@Column(name = "ANNUAL_RENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	@Column(name = "LEASE", unique = false, nullable = true, length = 5)
	private String lease;
	
	/** 剩余租期（月） **/
	@Column(name = "LEFT_LEASE", unique = false, nullable = true, length = 5)
	private String leftLease;
	
	/** 修改次数 **/
	@Column(name = "MODIFY_NUM", unique = false, nullable = true, length = 10)
	private Integer modifyNum;
	
	/** 房产取得方式 STD_ZB_ESTATE_ACQUIRE_WAY **/
	@Column(name = "ESTATE_ACQUIRE_WAY", unique = false, nullable = true, length = 2)
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	@Column(name = "IN_USE_YEAR", unique = false, nullable = true, length = 10)
	private String inUseYear;
	
	/** 物业类型 STD_ZB_PROPERTY_TYPE **/
	@Column(name = "HOUSE_TYPE", unique = false, nullable = true, length = 2)
	private String houseType;
	
	/** 发证日期 **/
	@Column(name = "ISSUE_CERTI_DATE", unique = false, nullable = true, length = 10)
	private String issueCertiDate;
	
	/** 工业房地产开发模式 STD_ZB_DECELOP_MODEL **/
	@Column(name = "INDUSTRY_DECELOP_MODEL", unique = false, nullable = true, length = 10)
	private String industryDecelopModel;
	
	/** 工业园区等级 **/
	@Column(name = "INDUSTRY_ZONE_LEVEL", unique = false, nullable = true, length = 2)
	private String industryZoneLevel;
	
	/** 盈利模式 STD_ZB_PROFIT_MODEL **/
	@Column(name = "PROFIT_MODEL", unique = false, nullable = true, length = 2)
	private String profitModel;
	
	/** 基础设施情况描述 **/
	@Column(name = "BASC_FACI_DESC", unique = false, nullable = true, length = 100)
	private String bascFaciDesc;
	
	/** 是否满足该行业厂房设计标准 STD_ZB_YES_NO **/
	@Column(name = "IS_INDUSTRY_DESIGN_PLANT", unique = false, nullable = true, length = 2)
	private String isIndustryDesignPlant;
	
	/** 物业情况 **/
	@Column(name = "PROPERTY_CASE", unique = false, nullable = true, length = 2)
	private String propertyCase;
	
	/** 房产使用情况 **/
	@Column(name = "HOUSE_PROPERTY", unique = false, nullable = true, length = 2)
	private String houseProperty;
	
	/** 所属地理位置 **/
	@Column(name = "BELONG_AREA", unique = false, nullable = true, length = 2)
	private String belongArea;
	
	/** 楼号 **/
	@Column(name = "BUILDING_ROOM_NUM", unique = false, nullable = true, length = 100)
	private String buildingRoomNum;
	
	/** 室号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 100)
	private String roomNum;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUCTURE **/
	@Column(name = "HOUSE_STRUCTURE", unique = false, nullable = true, length = 10)
	private String houseStructure;
	
	/** 市场租金 **/
	@Column(name = "MARKET_RENT", unique = false, nullable = true, length = 26)
	private java.math.BigDecimal marketRent;
	
	/** 租赁合同生效日 **/
	@Column(name = "LEASE_CON_EFT_DT", unique = false, nullable = true, length = 10)
	private String leaseConEftDt;
	
	/** 租赁合同到期日 **/
	@Column(name = "LEASE_CON_END_DT", unique = false, nullable = true, length = 10)
	private String leaseConEndDt;
	
	/** 租赁形式 STD_ZB_LEASE_TYPE **/
	@Column(name = "LEASE_TYPE", unique = false, nullable = true, length = 2)
	private String leaseType;
	
	/** 楼盘名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 100)
	private String communityName;
	
	/** 商业繁华程度 STD_ZB_BUSTLING_LEVEL **/
	@Column(name = "BUSTLING_LEVEL", unique = false, nullable = true, length = 2)
	private String bustlingLevel;
	
	/** 交通便捷程度 STD_ZB_TRAFFIC_CONVENIENT_LEVEL **/
	@Column(name = "TRAFFIC_CONVENIENT_LEVEL", unique = false, nullable = true, length = 2)
	private String trafficConvenientLevel;
	
	/** 外界环境 STD_ZB_ENVIRONMENT **/
	@Column(name = "ENVIRONMENT", unique = false, nullable = true, length = 2)
	private String environment;
	
	/** 空置率 **/
	@Column(name = "VACANCY_RATE", unique = false, nullable = true, length = 26)
	private java.math.BigDecimal vacancyRate;
	
	/** 临街情况 STD_ZB_STREET_SITUATION **/
	@Column(name = "STREET_SITUATION", unique = false, nullable = true, length = 2)
	private String streetSituation;
	
	/** 是否位于城市地带 STD_ZB_YES_NO **/
	@Column(name = "IS_URBAN_ZONE", unique = false, nullable = true, length = 2)
	private String isUrbanZone;
	
	/** 建筑外观与公共装修 STD_ZB_BUILD_PUB_FIT **/
	@Column(name = "BUILD_PUB_FIT", unique = false, nullable = true, length = 2)
	private String buildPubFit;
	
	/** 物业管理资质等级 STD_ZB_NATUR_LEVEL **/
	@Column(name = "REALTY_MANA_NATUR_LEVEL", unique = false, nullable = true, length = 2)
	private String realtyManaNaturLevel;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd;
	}
	
    /**
     * @return twocard2oneInd
     */
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd;
	}
	
    /**
     * @return houseAllPledgeInd
     */
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc;
	}
	
    /**
     * @return partRegPositionDesc
     */
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo;
	}
	
    /**
     * @return businessHouseNo
     */
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(String buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public String getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears;
	}
	
    /**
     * @return activateYears
     */
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge;
	}
	
    /**
     * @return floorAge
     */
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr;
	}
	
    /**
     * @return pocAddr
     */
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc;
	}
	
    /**
     * @return bdlc
     */
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd;
	}
	
    /**
     * @return bdgd
     */
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo;
	}
	
    /**
     * @return housePlaceInfo
     */
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea;
	}
	
    /**
     * @return landUseArea
     */
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setLandUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit;
	}
	
    /**
     * @return landUseUnit
     */
	public String getLandUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType;
	}
	
    /**
     * @return houseUseType
     */
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}
	
    /**
     * @return lesseeName
     */
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return annualRent
     */
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease;
	}
	
    /**
     * @return lease
     */
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease;
	}
	
    /**
     * @return leftLease
     */
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param modifyNum
	 */
	public void setModifyNum(Integer modifyNum) {
		this.modifyNum = modifyNum;
	}
	
    /**
     * @return modifyNum
     */
	public Integer getModifyNum() {
		return this.modifyNum;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay;
	}
	
    /**
     * @return estateAcquireWay
     */
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear;
	}
	
    /**
     * @return inUseYear
     */
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}
	
    /**
     * @return houseType
     */
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param issueCertiDate
	 */
	public void setIssueCertiDate(String issueCertiDate) {
		this.issueCertiDate = issueCertiDate;
	}
	
    /**
     * @return issueCertiDate
     */
	public String getIssueCertiDate() {
		return this.issueCertiDate;
	}
	
	/**
	 * @param industryDecelopModel
	 */
	public void setIndustryDecelopModel(String industryDecelopModel) {
		this.industryDecelopModel = industryDecelopModel;
	}
	
    /**
     * @return industryDecelopModel
     */
	public String getIndustryDecelopModel() {
		return this.industryDecelopModel;
	}
	
	/**
	 * @param industryZoneLevel
	 */
	public void setIndustryZoneLevel(String industryZoneLevel) {
		this.industryZoneLevel = industryZoneLevel;
	}
	
    /**
     * @return industryZoneLevel
     */
	public String getIndustryZoneLevel() {
		return this.industryZoneLevel;
	}
	
	/**
	 * @param profitModel
	 */
	public void setProfitModel(String profitModel) {
		this.profitModel = profitModel;
	}
	
    /**
     * @return profitModel
     */
	public String getProfitModel() {
		return this.profitModel;
	}
	
	/**
	 * @param bascFaciDesc
	 */
	public void setBascFaciDesc(String bascFaciDesc) {
		this.bascFaciDesc = bascFaciDesc;
	}
	
    /**
     * @return bascFaciDesc
     */
	public String getBascFaciDesc() {
		return this.bascFaciDesc;
	}
	
	/**
	 * @param isIndustryDesignPlant
	 */
	public void setIsIndustryDesignPlant(String isIndustryDesignPlant) {
		this.isIndustryDesignPlant = isIndustryDesignPlant;
	}
	
    /**
     * @return isIndustryDesignPlant
     */
	public String getIsIndustryDesignPlant() {
		return this.isIndustryDesignPlant;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase;
	}
	
    /**
     * @return propertyCase
     */
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty;
	}
	
    /**
     * @return houseProperty
     */
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea;
	}
	
    /**
     * @return belongArea
     */
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum;
	}
	
    /**
     * @return buildingRoomNum
     */
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure;
	}
	
    /**
     * @return houseStructure
     */
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param marketRent
	 */
	public void setMarketRent(java.math.BigDecimal marketRent) {
		this.marketRent = marketRent;
	}
	
    /**
     * @return marketRent
     */
	public java.math.BigDecimal getMarketRent() {
		return this.marketRent;
	}
	
	/**
	 * @param leaseConEftDt
	 */
	public void setLeaseConEftDt(String leaseConEftDt) {
		this.leaseConEftDt = leaseConEftDt;
	}
	
    /**
     * @return leaseConEftDt
     */
	public String getLeaseConEftDt() {
		return this.leaseConEftDt;
	}
	
	/**
	 * @param leaseConEndDt
	 */
	public void setLeaseConEndDt(String leaseConEndDt) {
		this.leaseConEndDt = leaseConEndDt;
	}
	
    /**
     * @return leaseConEndDt
     */
	public String getLeaseConEndDt() {
		return this.leaseConEndDt;
	}
	
	/**
	 * @param leaseType
	 */
	public void setLeaseType(String leaseType) {
		this.leaseType = leaseType;
	}
	
    /**
     * @return leaseType
     */
	public String getLeaseType() {
		return this.leaseType;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bustlingLevel
	 */
	public void setBustlingLevel(String bustlingLevel) {
		this.bustlingLevel = bustlingLevel;
	}
	
    /**
     * @return bustlingLevel
     */
	public String getBustlingLevel() {
		return this.bustlingLevel;
	}
	
	/**
	 * @param trafficConvenientLevel
	 */
	public void setTrafficConvenientLevel(String trafficConvenientLevel) {
		this.trafficConvenientLevel = trafficConvenientLevel;
	}
	
    /**
     * @return trafficConvenientLevel
     */
	public String getTrafficConvenientLevel() {
		return this.trafficConvenientLevel;
	}
	
	/**
	 * @param environment
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
    /**
     * @return environment
     */
	public String getEnvironment() {
		return this.environment;
	}
	
	/**
	 * @param vacancyRate
	 */
	public void setVacancyRate(java.math.BigDecimal vacancyRate) {
		this.vacancyRate = vacancyRate;
	}
	
    /**
     * @return vacancyRate
     */
	public java.math.BigDecimal getVacancyRate() {
		return this.vacancyRate;
	}
	
	/**
	 * @param streetSituation
	 */
	public void setStreetSituation(String streetSituation) {
		this.streetSituation = streetSituation;
	}
	
    /**
     * @return streetSituation
     */
	public String getStreetSituation() {
		return this.streetSituation;
	}
	
	/**
	 * @param isUrbanZone
	 */
	public void setIsUrbanZone(String isUrbanZone) {
		this.isUrbanZone = isUrbanZone;
	}
	
    /**
     * @return isUrbanZone
     */
	public String getIsUrbanZone() {
		return this.isUrbanZone;
	}
	
	/**
	 * @param buildPubFit
	 */
	public void setBuildPubFit(String buildPubFit) {
		this.buildPubFit = buildPubFit;
	}
	
    /**
     * @return buildPubFit
     */
	public String getBuildPubFit() {
		return this.buildPubFit;
	}
	
	/**
	 * @param realtyManaNaturLevel
	 */
	public void setRealtyManaNaturLevel(String realtyManaNaturLevel) {
		this.realtyManaNaturLevel = realtyManaNaturLevel;
	}
	
    /**
     * @return realtyManaNaturLevel
     */
	public String getRealtyManaNaturLevel() {
		return this.realtyManaNaturLevel;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}