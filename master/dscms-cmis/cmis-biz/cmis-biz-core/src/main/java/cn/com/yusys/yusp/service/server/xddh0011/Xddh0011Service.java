package cn.com.yusys.yusp.service.server.xddh0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.server.xddh0011.req.PspDebitInfoRep;
import cn.com.yusys.yusp.dto.server.xddh0011.req.PspTaskListRep;
import cn.com.yusys.yusp.dto.server.xddh0011.req.PspWarningInfoListRep;
import cn.com.yusys.yusp.dto.server.xddh0011.req.Xddh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisPspClientService;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xddh0011Service
 * @类描述: #服务类
 * @功能描述:生成小微平台贷后预警定期任务
 * @创建人: YX-Qianxin
 * @创建时间: 2021-06-09 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddh0011Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0011Service.class);
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private AccLoanMapper accloanMapper;
    @Autowired
    private CmisPspClientService cmisPspClientService;

    /**
     * 生成小微平台贷后预警定期任务</br>
     *
     * @param xddh0011DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0011DataRespDto xddh0011(Xddh0011DataReqDto xddh0011DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value);
        Xddh0011DataRespDto xddh0011DataRespDto = new Xddh0011DataRespDto();
        try {
            String yw_date = xddh0011DataReqDto.getYw_date();//任务日期
            BigDecimal dqrw_num = xddh0011DataReqDto.getDqrw_num();//任务数量
            String doc_name = xddh0011DataReqDto.getDoc_name();//文件名称
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            String saveLocalftpPath = "/weblogic/webroot/download/XVP/";//小贷微银优企贷定期检查任务文件保存路径
            File file = new File(saveLocalftpPath + doc_name);
            int i = 0;
            ResultDto<Boolean> m = null;
            ResultDto<Boolean> n = null;
            String managerId = "";
            if (file.exists()) {
                /**** 执行异步操作 *******/
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, "优企贷" + yw_date + "贷后定期任务开始处理，任务文件" + doc_name);
                InputStreamReader read = new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8"));
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                String serno = "";
                String cus_id = "";
                String cus_name = "";
                String cont_id = "";
                String chk_num = "";
                String chk_bln = "";
                String crt_date = "";
                String rqr_fin_date = "";
                String if_xccz = "";
                PspTaskListRep pspTaskList = null;
                PspDebitInfoRep pspDebitInfoist = null;
                PspWarningInfoListRep pspWarningInfoListRep = null;
                //拼接序列号
                HashMap<String, String> param = new HashMap<>();
                //全局序列号业务类型 10-PC端
                param.put("biz", SeqConstant.YPSEQ_BIZ_10);
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    if ((lineTxt != null) && lineTxt.length() > 0) {
                        String[] line = lineTxt.split("\\#");
                        serno = line[0].trim();// 任务编号
                        if (serno.startsWith("YQDDZRB")) {
                            i++;
                            cus_id = line[2].trim();// 客户号
                            cus_name = line[3].trim();// 客户名称
                            cont_id = line[4].trim();// 合同号
                            chk_num = line[5].trim();// 贷款笔数
                            chk_bln = line[6].trim();// 贷款余额
                            crt_date = line[7].trim();// 任务生成日期
                            rqr_fin_date = line[8].trim();// 任务完成日期
                            if_xccz = line[9].trim();//是否现场处置标志
                            //设置查询参数
                            Map map = new HashMap();
                            map.put("contNo", cont_id);//合同号
                            //查询客户机构号
                            String managerBrId = ctrLoanContMapper.queryCtrLoanContManagerBrIdByContNo(map);
                            managerId = ctrLoanContMapper.queryCtrLoanContManagerIdByContNo(map);
                            //根据机构号查询机构名称
                            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                            String orgName = "";//机构名称
                            if (ResultDto.success().getCode().equals(adminSmOrgDtoResultDto.getCode())) {
                                AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                                if (!Objects.isNull(adminSmOrgDto)) {
                                    orgName = adminSmOrgDto.getOrgName();
                                }
                            }
                            String billNo = accloanMapper.getBillNoByContNo(cont_id);
                            if (null == pspTaskList) {
                                throw new RuntimeException("未查询到贷后检查任务数据！");
                            }
                            pspTaskList.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param));
                            pspTaskList.setTaskNo(serno);
                            pspTaskList.setTaskType("2");//自动检查
                            pspTaskList.setCheckType("");
                            pspTaskList.setCusId(cus_id);
                            pspTaskList.setCusName(cus_name);
                            pspTaskList.setBillNo(billNo);
                            pspTaskList.setContNo(cont_id);
                            pspTaskList.setPrdName("");
                            pspTaskList.setTaskStartDt(crt_date);
                            pspTaskList.setTaskEndDt(rqr_fin_date);
                            pspTaskList.setExecId(managerBrId);
                            pspTaskList.setExecBrId(orgName);
                            pspTaskList.setCheckStatus("1");//待检查
                            pspTaskList.setApproveStatus("000");
                            pspTaskList.setRptType("");
                            pspTaskList.setCheckDate(openDay);
                            pspTaskList.setLoanStartDate("");
                            pspTaskList.setLoanEndDate("");
                            pspTaskList.setRiskLvl("");
                            pspTaskList.setGuarMode("");
                            pspTaskList.setIssueId("");
                            pspTaskList.setIssueBrId("");
                            pspTaskList.setIssueDate("");
                            pspTaskList.setLoanAmt(BigDecimal.ZERO);
                            n = null;//cmisPspClientService.insert(pspTaskList);

                            pspDebitInfoist.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param));
                            pspDebitInfoist.setTaskNo(serno);
                            pspDebitInfoist.setCusId(cus_id);
                            pspDebitInfoist.setCusName(cus_name);
                            pspDebitInfoist.setBillNo(billNo);
                            pspDebitInfoist.setContNo(cont_id);
                            pspDebitInfoist.setPrdName("");
                            pspDebitInfoist.setLoanStartDate("");
                            pspDebitInfoist.setLoanEndDate("");
                            pspDebitInfoist.setGuarMode("");
                            pspDebitInfoist.setLoanAmt(BigDecimal.ZERO);
                            pspDebitInfoist.setLoanBalance(BigDecimal.ZERO);
                            pspDebitInfoist.setDebitInt(BigDecimal.ZERO);
                            pspDebitInfoist.setOverdueDay(0);
                            pspDebitInfoist.setExecRateYear(BigDecimal.ZERO);
                            pspDebitInfoist.setFiveClass("");
                            m = null;//cmisPspClientService.create(pspDebitInfoist);
                        } else if (serno.startsWith("-")) {

                        } else {
                            String id = line[0].trim();// id
                            String ruleid = line[1].trim();// 规则id
                            String bussinessid = line[2].trim();// 业务申请流水号
                            String rulecode = line[3].trim();// 规则代码
                            String rulename = line[4].trim();// 规则名称
                            String warnsignal = line[5].trim();// 预警信号（过滤后的）
                            String signalname = line[6].trim();// 信号名称
                            String createtime = line[7].trim();// 预警时间
                            String isfirstwarn = line[8].trim();// 是否首次预警
                            String latest3m_warncount = line[9].trim();// 3个月出现预警次数
                            String latest6m_warncount = line[10].trim();// 6个月出现预警次数
                            serno = line[11].trim();//任务编号
                            String ruledetail = line[12].trim();//规则详情
                            if (null == pspWarningInfoListRep) {
                                throw new RuntimeException("未查询到定期检查任务数据！");
                            }
                            pspWarningInfoListRep.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param));
                            pspWarningInfoListRep.setTaskNo(serno);
                            pspWarningInfoListRep.setAltSinNo(rulecode);
                            pspWarningInfoListRep.setAltDate(createtime);
                            pspWarningInfoListRep.setInfoSource(warnsignal);
                            pspWarningInfoListRep.setAltTypeMax("");
                            pspWarningInfoListRep.setAltType("");
                            pspWarningInfoListRep.setAltSubType("");
                            pspWarningInfoListRep.setAltDesc("");
                            pspWarningInfoListRep.setAltLvl("");
                            m = null;//cmisPspClientService.createData(pspWarningInfoListRep);
                        }
                    }
                }
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, "优企贷" + yw_date + "贷后定期任务处理结束，任务文件" + doc_name);
            } else {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            }
            if (null == m || null == n) {
                throw new RuntimeException("m或n未查询到数据！");
            }
            if (!m.getData() || !n.getData()) {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
                // 短信通知
                sendMsg(managerId);
            } else {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, e.getMessage());
            xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, e.getMessage());
            xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value);
        return xddh0011DataRespDto;
    }


    /**
     * 发送短信
     *
     * @param managerId
     * @throws Exception
     */
    private void sendMsg(String managerId) throws Exception {
        String message = "【张家港农商银行】今日优企贷贷后定期任务文件处理失败，请及时处理";
        logger.info("发送的短信内容：" + message);

        try {
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
            ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
            AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());

            SenddxReqDto senddxReqDto = new SenddxReqDto();
            senddxReqDto.setInfopt("dx");
            SenddxReqList senddxReqList = new SenddxReqList();
            senddxReqList.setMobile(adminSmUserDto.getUserMobilephone());
            senddxReqList.setSmstxt(message);
            ArrayList<SenddxReqList> list = new ArrayList<>();
            list.add(senddxReqList);
            senddxReqDto.setSenddxReqList(list);

            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
            ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

            String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            SenddxRespDto senddxRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                //  获取相关的值并解析
                senddxRespDto = senddxResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(senddxCode, senddxMeesage);
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
