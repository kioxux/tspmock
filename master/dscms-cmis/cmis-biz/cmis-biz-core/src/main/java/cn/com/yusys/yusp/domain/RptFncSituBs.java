/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituBs
 * @类描述: rpt_fnc_situ_bs数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-06 16:31:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_fnc_situ_bs")
public class RptFncSituBs extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 最近一期报表分析其他说明 **/
	@Column(name = "LAST_DESC", unique = false, nullable = true, length = 65535)
	private String lastDesc;
	
	/** 近两年一期报表分析资产其他说明 **/
	@Column(name = "BELAST_ASSET_DESC", unique = false, nullable = true, length = 65535)
	private String belastAssetDesc;
	
	/** 近两年一期报表分析负债其他说明 **/
	@Column(name = "BELAST_DEBT_DESC", unique = false, nullable = true, length = 65535)
	private String belastDebtDesc;
	
	/** 财务总体评价 **/
	@Column(name = "OVERALL_FINANCIAL_EVALUATION", unique = false, nullable = true, length = 65535)
	private String overallFinancialEvaluation;
	
	/** 录入年月 **/
	@Column(name = "INPUT_YEAR", unique = false, nullable = true, length = 20)
	private String inputYear;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lastDesc
	 */
	public void setLastDesc(String lastDesc) {
		this.lastDesc = lastDesc;
	}
	
    /**
     * @return lastDesc
     */
	public String getLastDesc() {
		return this.lastDesc;
	}
	
	/**
	 * @param belastAssetDesc
	 */
	public void setBelastAssetDesc(String belastAssetDesc) {
		this.belastAssetDesc = belastAssetDesc;
	}
	
    /**
     * @return belastAssetDesc
     */
	public String getBelastAssetDesc() {
		return this.belastAssetDesc;
	}
	
	/**
	 * @param belastDebtDesc
	 */
	public void setBelastDebtDesc(String belastDebtDesc) {
		this.belastDebtDesc = belastDebtDesc;
	}
	
    /**
     * @return belastDebtDesc
     */
	public String getBelastDebtDesc() {
		return this.belastDebtDesc;
	}
	
	/**
	 * @param overallFinancialEvaluation
	 */
	public void setOverallFinancialEvaluation(String overallFinancialEvaluation) {
		this.overallFinancialEvaluation = overallFinancialEvaluation;
	}
	
    /**
     * @return overallFinancialEvaluation
     */
	public String getOverallFinancialEvaluation() {
		return this.overallFinancialEvaluation;
	}
	
	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}
	
    /**
     * @return inputYear
     */
	public String getInputYear() {
		return this.inputYear;
	}


}