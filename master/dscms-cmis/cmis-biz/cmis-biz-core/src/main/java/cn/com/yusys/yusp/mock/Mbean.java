package cn.com.yusys.yusp.mock;

public class Mbean {

    private String memcliento;

    private String memclientname;


    private String memclienttype;

    private String  memclinetmanage;

    private String inst;

    public String getMemcliento() {
        return memcliento;
    }

    public void setMemcliento(String memcliento) {
        this.memcliento = memcliento;
    }

    public String getMemclientname() {
        return memclientname;
    }

    public void setMemclientname(String memclientname) {
        this.memclientname = memclientname;
    }

    public String getMemclienttype() {
        return memclienttype;
    }

    public void setMemclienttype(String memclienttype) {
        this.memclienttype = memclienttype;
    }

    public String getMemclinetmanage() {
        return memclinetmanage;
    }

    public void setMemclinetmanage(String memclinetmanage) {
        this.memclinetmanage = memclinetmanage;
    }

    public String getInst() {
        return inst;
    }

    public void setInst(String inst) {
        this.inst = inst;
    }
}
