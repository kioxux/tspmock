package cn.com.yusys.yusp.web.server.xddb0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0004.req.ReqList;
import cn.com.yusys.yusp.dto.server.xddb0004.req.Xddb0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0004.resp.Xddb0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 接口处理类:提醒任务处理结果实时同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0004:提醒任务处理结果实时同步")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0004Resource.class);

    /**
     * 交易码：xddb0004
     * 交易描述：提醒任务处理结果实时同步
     *
     * @param xddb0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提醒任务处理结果实时同步")
    @PostMapping("/xddb0004")
    protected @ResponseBody
    ResultDto<Xddb0004DataRespDto> xddb0004(@Validated @RequestBody Xddb0004DataReqDto xddb0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004DataReqDto));
        Xddb0004DataRespDto xddb0004DataRespDto = new Xddb0004DataRespDto();// 响应Dto:提醒任务处理结果实时同步
        ResultDto<Xddb0004DataRespDto> xddb0004DataResultDto = new ResultDto<>();
        try {
            List<ReqList> reqList = xddb0004DataReqDto.getReqList();
            String newcod = reqList.get(0).getNewcod();//担保三级分类
            String guarid = reqList.get(0).getGuarid();//押品统一编号
            String type = reqList.get(0).getType();//任务类型
            String result = reqList.get(0).getResult();//处理结果
            String mangid = reqList.get(0).getMangid();//处理人工号
            String mabrid = reqList.get(0).getMabrid();//处理人所属机构ID
            BigDecimal evamt = reqList.get(0).getEvamt();//评估价值
            String time = reqList.get(0).getTime();//下次估值到期日
            // 从xddb0004DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddb0004DataRespDto对象开始
            xddb0004DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xddb0004DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xddb0004DataRespDto对象结束
            // 封装xddb0004DataResultDto中正确的返回码和返回信息
            xddb0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, e.getMessage());
            // 封装xddb0004DataResultDto中异常返回码和返回信息
            // TODO EpbEnum.EPB099999 待调整 开始
            xddb0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EpbEnum.EPB099999 待调整  结束
        }
        // 封装xddb0004DataRespDto到xddb0004DataResultDto中
        xddb0004DataResultDto.setData(xddb0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004DataRespDto));
        return xddb0004DataResultDto;
    }
}
