package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.BusContInfoDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpCvrgAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrCvrgContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-13 16:08:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrCvrgContService {
    private static final Logger log = LoggerFactory.getLogger(CtrCvrgContService.class);
    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private IqpCvrgAppMapper iqpCvrgAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrCvrgCont selectByPrimaryKey(String pkId) {
        return ctrCvrgContMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrCvrgCont> selectAll(QueryModel model) {
        List<CtrCvrgCont> records = (List<CtrCvrgCont>) ctrCvrgContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrCvrgCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrCvrgCont> list = ctrCvrgContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CtrCvrgCont record) {
        return ctrCvrgContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(CtrCvrgCont record) {
        return ctrCvrgContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CtrCvrgCont record) {
        int i = ctrCvrgContMapper.updateByPrimaryKey(record);
        if(!CmisCommonConstants.GUAR_MODE_40.equals(record.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(record.getGuarMode())
                && !CmisCommonConstants.GUAR_MODE_00.equals(record.getGuarMode())){
            List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.getByContNo(record.getContNo());
            if (CollectionUtils.isEmpty(list)) {
                log.error("主合同【{}】未关联担保合同！",record.getContNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            //担保合同签订生效
            for (GrtGuarBizRstRel grtGuarBizRstRel : list) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                grtGuarCont.setSignDate(record.getPaperContSignDate());
                if(org.apache.commons.lang.StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);
            }
        }

        CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.selectByContNo(record.getContNo());
        // 判断是否为合同续签,若为续签合同则恢复原合同的占额
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrCvrgCont.getIsRenew())){
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrCvrgCont.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrCvrgCont.getOrigiContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
            cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
            cmisLmt0012ReqDto.setInputId(ctrCvrgCont.getInputId());//登记人
            cmisLmt0012ReqDto.setInputBrId(ctrCvrgCont.getInputBrId());//登记机构
            cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
            log.info("保函合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrCvrgCont.getContNo(), ctrCvrgCont.getOrigiContNo(), JSON.toJSONString(cmisLmt0012ReqDto));
            ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("保函合同【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrCvrgCont.getContNo(), ctrCvrgCont.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
            if (!"0".equals(resultDtoDto.getCode())) {
                log.error("保函合同【{}】签订异常",ctrCvrgCont.getContNo());
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
            }
        }
        guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_08, record.getContNo());

        // 生成档案归档任务
        if (i > 0 ) {
            try {
                //档案归档任务规则
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(record.getCusId());
                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                docArchiveClientDto.setArchiveMode("02");// 归档模式
                docArchiveClientDto.setDocClass("03");// 档案分类 03重要信息档案
                docArchiveClientDto.setDocType("13");// 档案类型 13保函
                docArchiveClientDto.setBizSerno(record.getSerno());// 关联流水号
                docArchiveClientDto.setCusId(record.getCusId());// 客户号
                docArchiveClientDto.setCusName(record.getCusName());// 客户名称
                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());// 证件号
                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());// 证件类型
                docArchiveClientDto.setManagerId(record.getManagerId());// 责任人
                docArchiveClientDto.setManagerBrId(record.getManagerBrId());// 责任机构
                docArchiveClientDto.setInputId(record.getInputId());// 登记人
                docArchiveClientDto.setInputBrId(record.getInputBrId());// 登记机构
                docArchiveClientDto.setContNo(record.getContNo());
                docArchiveClientDto.setLoanAmt(record.getContAmt());
                docArchiveClientDto.setStartDate(record.getStartDate());
                docArchiveClientDto.setEndDate(record.getEndDate());
                docArchiveClientDto.setPrdId(record.getPrdId());
                docArchiveClientDto.setPrdName(record.getPrdName());
                int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                if (num < 1) {
                    log.info("系统生成保函档案归档任务失败");
                }
            } catch (Exception e) {
                log.info("系统生成保函档案归档任务异常");
            }
        }
        return i;
    }

    /**
     * 合同注销保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map logout(@RequestBody Map params) throws ParseException {
        {
            Map result = new HashMap();
            String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
            String rtnMsg = "";
            String serno = "";
            int pvpLoanAppCount = 0;
            int accCvrgCount = 0;
            try {
                //获取申请流水号
                serno = (String) params.get("serno");
                if (StringUtils.isBlank(serno)) {
                    rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                    rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                    return result;
                }

                String logPrefix = "合同签订" + serno;

                log.info(logPrefix + "获取申请数据");
                CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.selectByContNo((String) params.get("contNo"));
                if (ctrCvrgCont == null) {
                    rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                    rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                    return result;
                }
                // 先判断合同是否存在未结清的业务
                String responseResult = ctrLoanContService.isContUncleared(ctrCvrgCont.getContNo());
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                    rtnCode = EcbEnum.ECB020055.key;
                    rtnMsg = EcbEnum.ECB020055.value;
                    return result;
                }
                // 获取合同注销后的合同状态
                String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrCvrgCont.getEndDate(),ctrCvrgCont.getContStatus());

                log.info(logPrefix + "合同注销-获取当前登录用户信息");
                User userInfo = SessionUtils.getUserInformation();
                if (userInfo == null) {
                    rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                    rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                    return result;
                } else {
                    ctrCvrgCont.setInputId(userInfo.getLoginCode());
                    ctrCvrgCont.setInputBrId(userInfo.getOrg().getCode());
                    ctrCvrgCont.setManagerId(userInfo.getLoginCode());
                    ctrCvrgCont.setManagerBrId(userInfo.getOrg().getCode());
                    ctrCvrgCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    ctrCvrgCont.setContStatus(finalContStatus);
                }
                //合同注销后，释放额度
                String guarMode = ctrCvrgCont.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = ctrCvrgCont.getCvtCnyAmt();
                } else {
                    recoverSpacAmtCny = ctrCvrgCont.getCvtCnyAmt();
                    recoverAmtCny = ctrCvrgCont.getCvtCnyAmt().add(ctrCvrgCont.getBailCvtCnyAmt());
                }
                // 根据合是否存在业务，得到恢复类型
                String recoverType = "";
                List<AccCvrs> accCvrsList = accCvrsService.selectByContNo(ctrCvrgCont.getContNo());
                if(CollectionUtils.nonEmpty(accCvrsList)){
                    //结清注销
                    for (AccCvrs accCvrs : accCvrsList) {
                        if(CmisCommonConstants.ACC_STATUS_7.equals(accCvrs.getAccStatus())){
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                        }else{
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                            break;
                        }
                    }
                }else {
                    //未用注销
                    recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrCvrgCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(ctrCvrgCont.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(ctrCvrgCont.getInputId());
                cmisLmt0012ReqDto.setInputBrId(ctrCvrgCont.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorMsg());
                }
                log.info(logPrefix + "保存委托贷款合同数据");
                int updCount = ctrCvrgContMapper.updateByPrimaryKeySelective(ctrCvrgCont);
                if (updCount < 0) {
                    throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
                }

                //不存在电子用印的情况下，需要作废档案归档任务
                if (!"1".equals(ctrCvrgCont.getIsESeal())) {
                    log.info("根据业务申请编号【" + ctrCvrgCont.getSerno() + "】需要作废档案归档任务开始");
                    try {
                        docArchiveInfoService.invalidByBizSerno(ctrCvrgCont.getSerno(),"");
                    } catch (Exception e) {
                        log.info("根据业务申请编号【" + ctrCvrgCont.getSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                    }
                    log.info("根据业务申请编号【" + ctrCvrgCont.getSerno() + "】需要作废档案归档任务结束");
                }


            } catch (Exception e) {
                log.error("保存保函合同" + serno + "异常", e.getMessage(), e);
                rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
                rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
                throw e;
            } finally {
                result.put("rtnCode", rtnCode);
                result.put("rtnMsg", rtnMsg);
            }
            return result;
        }
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(CtrCvrgCont record) {
        return ctrCvrgContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 根据合同号更新lmtAccNo
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateLmtAccNoByContNo(CtrCvrgCont record) {
        return ctrCvrgContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return ctrCvrgContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return ctrCvrgContMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteIqpCvrgAppinfo
     * @方法描述: 根据主键对委托贷款申请明细信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 对委托贷款申请信息进行逻辑删除，更新对核销申请明细信息进行逻辑删除
     */
    @Transactional
    public Map deleteCtrCvrgContinfo(String iqpSerno) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            log.info(String.format("根据主键%s对委托贷款申请明细信息进行逻辑删除", iqpSerno));
            CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.selectByCvrgSernoKey(iqpSerno);
            ctrCvrgCont.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            log.info(String.format("根据主键%s对委托贷款申请明细信息进行逻辑删除,获取用户登录信息", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                ctrCvrgCont.setUpdId(userInfo.getLoginCode());
                ctrCvrgCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrCvrgCont.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对委托贷款申请信息进行逻辑删除,信息进行逻辑删除", iqpSerno));
            //iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            ctrCvrgContMapper.deleteByPrimaryKey(ctrCvrgCont.getPkId());
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除委托贷款申请信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无CtrCvrgCont ctrCvrgCont
     */
    @Transactional
    public List<CtrCvrgCont> toSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        return ctrCvrgContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrCvrgCont> doneSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatusOther", CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrCvrgContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrCvrgCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrCvrgContMapper.selectByLmtAccNo(lmtAccNo);
    }


    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:14
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrCvrgCont selectByIqpSerno(String serno) {
        return ctrCvrgContMapper.selectByCvrgSernoKey(serno);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:14
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrCvrgCont selectByContNo(String conNo) {
        return ctrCvrgContMapper.selectByContNo(conNo);
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrCvrgCont> selectByQuerymodel(QueryModel model) {
        return ctrCvrgContMapper.selectByModel(model);
    }

    /**
     * @param contNo
     * @return BigDecimal
     * @author qw
     * @date 2021/8/20 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可出账金额
     * @修改历史:
     */
    public BigDecimal getCanOutAccountAmt(String contNo) {
        //可出账金额
        BigDecimal canOutAccountAmt = BigDecimal.ZERO;
        CtrCvrgCont ctrCvrgCont = ctrCvrgContMapper.selectByContNo(contNo);
        if(Objects.isNull(ctrCvrgCont)){
            throw BizException.error(null, EcbEnum.ECB020022.key, EcbEnum.ECB020022.value);
        }
        //合同最高可用信金额折算人民币金额
        BigDecimal contHighAmt = ctrCvrgCont.getCvtCnyAmt();

        //业务敞口余额（未扣减保证金）
        BigDecimal totalSpacBalance = BigDecimal.ZERO;
        //业务敞口初始总金额（未扣减保证金）
        BigDecimal totalOrigiSpacAmt = BigDecimal.ZERO;

        //业务敞口余额（扣减保证金）
        BigDecimal totalSpacBalanceSubtractBailAmt = BigDecimal.ZERO;
        //业务敞口初始总金额（扣减保证金）
        BigDecimal totalOrigiSpacSubtractBailAmt = BigDecimal.ZERO;

        //保证金币种
        String bailCurType = "";
        //保证金折算人民币金额
        BigDecimal bailAmtCny = BigDecimal.ZERO;
        List<AccCvrs> accCvrsList = accCvrsService.selectByContNo(ctrCvrgCont.getContNo());
        if(CollectionUtils.nonEmpty(accCvrsList)){
            for (AccCvrs accCvrs : accCvrsList) {
                totalSpacBalance = totalSpacBalance.add(accCvrs.getExchangeRmbSpac());//业务敞口余额（未扣减保证金）
                totalOrigiSpacAmt = totalOrigiSpacAmt.add(accCvrs.getOrigiOpenAmt().multiply(accCvrs.getExchangeRate()));//业务敞口初始总金额（未扣减保证金）

                totalSpacBalanceSubtractBailAmt = totalSpacBalanceSubtractBailAmt.add(accCvrs.getExchangeRmbSpac()).subtract(accCvrs.getBailCvtCnyAmt());//业务敞口余额（扣减保证金）
                totalOrigiSpacSubtractBailAmt = (totalOrigiSpacSubtractBailAmt.add(accCvrs.getOrigiOpenAmt().multiply(accCvrs.getExchangeRate()).subtract(accCvrs.getBailCvtCnyAmt())));//业务敞口初始总金额（扣减保证金）
            }
            //低风险
            if(CmisCommonConstants.GUAR_MODE_21.equals(ctrCvrgCont.getGuarMode())||
                    CmisCommonConstants.GUAR_MODE_40.equals(ctrCvrgCont.getGuarMode())||
                    CmisCommonConstants.GUAR_MODE_60.equals(ctrCvrgCont.getGuarMode())){
                //一般合同
                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrCvrgCont.getContType())){
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalance);
                }else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacAmt);
                }
            }else{
                //一般合同
                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrCvrgCont.getContType())){
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalanceSubtractBailAmt);
                }else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacSubtractBailAmt);
                }
            }
        }else{
            canOutAccountAmt = contHighAmt;
        }
        return canOutAccountAmt;
    }

    /**
     * @方法名称: getSumContAmt
     * @方法描述: 根据额度编号查询合同金额总和
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BigDecimal getSumContAmt(String lmtAccNo) {
        return ctrCvrgContMapper.getSumContAmt(lmtAccNo);
    }

    /**
     * @方法名称: getAllBusContInfo
     * @方法描述: 查询所有类型合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<BusContInfoDto> getAllBusContInfo(String cusId) {
        List<BusContInfoDto> list = ctrCvrgContMapper.getAllBusContInfo(cusId);
        return list;
    }
}
