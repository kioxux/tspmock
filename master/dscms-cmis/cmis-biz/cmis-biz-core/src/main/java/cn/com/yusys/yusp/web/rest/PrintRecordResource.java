/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PrintRecord;
import cn.com.yusys.yusp.service.PrintRecordService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PrintRecordResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-13 13:50:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/printrecord")
public class PrintRecordResource {
    @Autowired
    private PrintRecordService printRecordService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PrintRecord>> query() {
        QueryModel queryModel = new QueryModel();
        List<PrintRecord> list = printRecordService.selectAll(queryModel);
        return new ResultDto<List<PrintRecord>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PrintRecord>> index(QueryModel queryModel) {
        List<PrintRecord> list = printRecordService.selectByModel(queryModel);
        return new ResultDto<List<PrintRecord>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PrintRecord> create(@RequestBody PrintRecord printRecord) throws URISyntaxException {
        printRecordService.insert(printRecord);
        return new ResultDto<PrintRecord>(printRecord);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PrintRecord printRecord) throws URISyntaxException {
        int result = printRecordService.update(printRecord);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String batchNo, String bizNo) {
        int result = printRecordService.deleteByPrimaryKey(batchNo, bizNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param printRecord
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author 王玉坤
     * @date 2021/10/13 23:36
     * @version 1.0.0
     * @desc 保存合同打印信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addprintrecord")
    protected ResultDto<Integer> addPrintRecord(@RequestBody PrintRecord printRecord) {
        int result = printRecordService.addPrintRecord(printRecord);
        return new ResultDto<Integer>(result);
    }

}
