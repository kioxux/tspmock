package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.GuarMortgageManageRel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageApp
 * @类描述: guar_mortgage_manage_app数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarMortgageManageAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 主合同编号 **/
	private String mainContNo;
	
	/** 抵押办理类型 **/
	private String regType;
	
	/** 抵押注销类型 **/
	private String regSubType;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 担保合同类型 **/
	private String guarContType;
	
	/** 担保方式 **/
	private String guarWay;
	
	/** 担保金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 申请原因 **/
	private String regReason;
	
	/** 是否在线办理抵押登记/注销 **/
	private String isRegOnline;
	
	/** 是否先放款后抵押 **/
	private String beforehandInd;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 是否还款即解押 **/
	private String isRepayRemoveGuar;

	private List<GuarMortgageManageRel> guarMortgageManageRelList;
	private List<GuarMortgageManageRelDto> guarMortgageManageRelDtos;
	private String inputIdName;

	private String inputBrIdName;

	private String managerBrIdName;

	private String managerIdName;

	public String getInputIdName() {
		return inputIdName;
	}

	public void setInputIdName(String inputIdName) {
		this.inputIdName = inputIdName;
	}

	public String getInputBrIdName() {
		return inputBrIdName;
	}

	public void setInputBrIdName(String inputBrIdName) {
		this.inputBrIdName = inputBrIdName;
	}

	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	public String getManagerIdName() {
		return managerIdName;
	}

	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}

	public List<GuarMortgageManageRel> getGuarMortgageManageRelList() {
		return guarMortgageManageRelList;
	}

	public void setGuarMortgageManageRelList(List<GuarMortgageManageRel> guarMortgageManageRelList) {
		this.guarMortgageManageRelList = guarMortgageManageRelList;
	}

	public List<GuarMortgageManageRelDto> getGuarMortgageManageRelDtos() {
		return guarMortgageManageRelDtos;
	}

	public void setGuarMortgageManageRelDtos(List<GuarMortgageManageRelDto> guarMortgageManageRelDtos) {
		this.guarMortgageManageRelDtos = guarMortgageManageRelDtos;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainContNo
	 */
	public void setMainContNo(String mainContNo) {
		this.mainContNo = mainContNo == null ? null : mainContNo.trim();
	}
	
    /**
     * @return MainContNo
     */	
	public String getMainContNo() {
		return this.mainContNo;
	}
	
	/**
	 * @param regType
	 */
	public void setRegType(String regType) {
		this.regType = regType == null ? null : regType.trim();
	}
	
    /**
     * @return RegType
     */	
	public String getRegType() {
		return this.regType;
	}
	
	/**
	 * @param regSubType
	 */
	public void setRegSubType(String regSubType) {
		this.regSubType = regSubType == null ? null : regSubType.trim();
	}
	
    /**
     * @return RegSubType
     */	
	public String getRegSubType() {
		return this.regSubType;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType == null ? null : guarContType.trim();
	}
	
    /**
     * @return GuarContType
     */	
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}
	
    /**
     * @return GuarWay
     */	
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param regReason
	 */
	public void setRegReason(String regReason) {
		this.regReason = regReason == null ? null : regReason.trim();
	}
	
    /**
     * @return RegReason
     */	
	public String getRegReason() {
		return this.regReason;
	}
	
	/**
	 * @param isRegOnline
	 */
	public void setIsRegOnline(String isRegOnline) {
		this.isRegOnline = isRegOnline == null ? null : isRegOnline.trim();
	}
	
    /**
     * @return IsRegOnline
     */	
	public String getIsRegOnline() {
		return this.isRegOnline;
	}
	
	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd == null ? null : beforehandInd.trim();
	}
	
    /**
     * @return BeforehandInd
     */	
	public String getBeforehandInd() {
		return this.beforehandInd;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param isRepayRemoveGuar
	 */
	public void setIsRepayRemoveGuar(String isRepayRemoveGuar) {
		this.isRepayRemoveGuar = isRepayRemoveGuar == null ? null : isRepayRemoveGuar.trim();
	}
	
    /**
     * @return IsRepayRemoveGuar
     */	
	public String getIsRepayRemoveGuar() {
		return this.isRepayRemoveGuar;
	}


}