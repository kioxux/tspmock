/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherForRateApp;
import cn.com.yusys.yusp.dto.OtherAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.OtherForRateAppMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherForRateAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-04 16:07:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherForRateAppService {
    private static final Logger log = LoggerFactory.getLogger(OtherForRateAppService.class);

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private OtherForRateAppMapper otherForRateAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherForRateApp selectByPrimaryKey(String serno) {
        return otherForRateAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherForRateApp> selectAll(QueryModel model) {
        List<OtherForRateApp> records = (List<OtherForRateApp>) otherForRateAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherForRateApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherForRateApp> list = otherForRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherForRateApp record) {
        return otherForRateAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherForRateApp record) {
        return otherForRateAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherForRateApp record) {
        return otherForRateAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherForRateApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return otherForRateAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return otherForRateAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherForRateAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteInfo(String serno) {
        OtherForRateApp otherForRateApp = otherForRateAppMapper.selectByPrimaryKey(serno);
        if (otherForRateApp == null){
            throw BizException.error(null, "999999", "删除：外币利率申请异常");
        }
        //否决、打回
        if(CmisBizConstants.APPLY_STATE_CALL_BACK.equals(otherForRateApp.getApproveStatus())) {
            otherForRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            otherForRateApp.setOprType(CmisBizConstants.OPR_TYPE_01);
            //TODO 流程否决结束 2021-05-18
            log.info("授信申请流程删除 bizId: {}", otherForRateApp.getSerno());
            workflowCoreClient.deleteByBizId(otherForRateApp.getSerno());
            int i = otherForRateAppMapper.updateByPrimaryKeySelective(otherForRateApp);
            if (i != 1) {
                throw BizException.error(null, "999999", "删除：外币利率申请异常");
            }
            return i;
        }else{
            //操作类型  删除
            otherForRateApp.setOprType(CmisBizConstants.OPR_TYPE_02);
            //转换日期 ASSURE_CERT_CODE
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
            User user = SessionUtils.getUserInformation();
            otherForRateApp.setUpdateTime(DateUtils.getCurrDate());
            otherForRateApp.setUpdDate(dateFormat.format(new Date()) );
            otherForRateApp.setUpdId(user.getLoginCode());
            otherForRateApp.setUpdBrId(user.getOrg().getCode());

            int i = otherForRateAppMapper.updateByPrimaryKeySelective(otherForRateApp);
            if (i != 1) {
                throw BizException.error(null, "999999", "删除：外币利率申请异常");
            }

            return i;
        }
    }

    /**
     * @方法名称: getOtherForRateAppByModel
     * @方法描述: 根据querymodel获取当前客户经理名下所有外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherForRateApp> getOtherForRateAppByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_000+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_111
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_990+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_991
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_992+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_993);
        List<OtherForRateApp> list = otherForRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getOtherForRateAppHis
     * @方法描述: 获取当前客户经理名下所有外币利率定价申请历史
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<OtherForRateApp> getOtherForRateAppHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("apprStatuss",CmisCommonConstants.WF_STATUS_996+CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_997
                +CmisCommonConstants.COMMON_SPLIT_COMMA+CmisCommonConstants.WF_STATUS_998
        );
        List<OtherForRateApp> list = otherForRateAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: addotherforrateapp
     * @方法描述: 新增外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addotherforrateapp(OtherForRateApp otherforrateapp) {
        {
            OtherForRateApp otherForRateApp = new OtherForRateApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherforrateapp, otherForRateApp);

                //调整克隆对象
                /** 上年我行存款日均人民币 **/
                BigDecimal lyearAverdayDepCny  = otherForRateApp.getLyearAverdayDepCny().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearAverdayDepCny(lyearAverdayDepCny);
                /** 上年我行存款日均外币 **/
                BigDecimal lyearAverdayDepFor = otherForRateApp.getLyearAverdayDepFor().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearAverdayDepFor(lyearAverdayDepFor);
                /** 上年我行国际结算量 **/
                BigDecimal lyearInterSettl = otherForRateApp.getLyearInterSettl().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearInterSettl(lyearInterSettl);
                /** 上年我行结售汇量 **/
                BigDecimal lyearSale = otherForRateApp.getLyearSale().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearSale(lyearSale);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherForRateApp.setOprType(CmisBizConstants.OPR_TYPE_01);
                //审批状态
                otherForRateApp.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);
                //状态 带生效
                otherForRateApp.setStatus(CmisBizConstants.STD_CUS_LIST_STATUS_00);
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherForRateApp.setManagerBrId(user.getOrg().getCode());
                otherForRateApp.setManagerId(user.getLoginCode());
                otherForRateApp.setInputId(user.getLoginCode());
                otherForRateApp.setInputBrId(user.getOrg().getCode());
                otherForRateApp.setInputDate(dateFormat.format(new Date()) );
                otherForRateApp.setCreateTime(DateUtils.getCurrDate());
                otherForRateApp.setUpdateTime(DateUtils.getCurrDate());
                otherForRateApp.setUpdDate(dateFormat.format(new Date()) );
                otherForRateApp.setUpdId(user.getLoginCode());
                otherForRateApp.setUpdBrId(user.getOrg().getCode());

                int i = otherForRateAppMapper.insertSelective(otherForRateApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：外币利率申请异常");
                }

            }catch (Exception e) {
                log.error("外币利率新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherForRateApp);

        }
    }

    /**
     * @方法名称: updateotherforrateapp
     * @方法描述: 更新外币利率定价申请
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateotherforrateapp(OtherForRateApp otherforrateapp) {
        {
            OtherForRateApp otherForRateApp = new OtherForRateApp();

            try {
                //克隆对象
                BeanUtils.copyProperties(otherforrateapp, otherForRateApp);

                //调整克隆对象
                /** 上年我行存款日均人民币 **/
                BigDecimal lyearAverdayDepCny  = otherForRateApp.getLyearAverdayDepCny().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearAverdayDepCny(lyearAverdayDepCny);
                /** 上年我行存款日均外币 **/
                BigDecimal lyearAverdayDepFor = otherForRateApp.getLyearAverdayDepFor().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearAverdayDepFor(lyearAverdayDepFor);
                /** 上年我行国际结算量 **/
                BigDecimal lyearInterSettl = otherForRateApp.getLyearInterSettl().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearInterSettl(lyearInterSettl);
                /** 上年我行结售汇量 **/
                BigDecimal lyearSale = otherForRateApp.getLyearSale().multiply(new BigDecimal(10000));
                otherForRateApp.setLyearSale(lyearSale);
                //补充 克隆缺少数据
                OtherForRateApp oldotherForRateApp = otherForRateAppMapper.selectByPrimaryKey(otherForRateApp.getSerno());
                //放入必填参数 操作类型 新增
                otherForRateApp.setOprType(oldotherForRateApp.getOprType());
                //审批状态
                otherForRateApp.setApproveStatus(oldotherForRateApp.getApproveStatus());
                //状态 带生效
                otherForRateApp.setStatus(oldotherForRateApp.getStatus());
                //时间 登记人 等级机构相关

                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherForRateApp.setUpdateTime(DateUtils.getCurrDate());
                otherForRateApp.setUpdDate(dateFormat.format(new Date()) );
                otherForRateApp.setUpdId(user.getLoginCode());
                otherForRateApp.setUpdBrId(user.getOrg().getCode());

                int i = otherForRateAppMapper.updateByPrimaryKeySelective(otherForRateApp);
                if (i != 1) {
                    throw BizException.error(null, "999999", "修改：外币利率申请异常");
                }

            }catch (Exception e) {
                log.error("外币利率修改异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherForRateApp);

        }
    }

    /**
     * 更新外币利率定价审批状态
     *
     * @author lyh
     */
    public int updateApproveStatus(String serno, String approveStatus) {
        OtherForRateApp otherForRateApp = new OtherForRateApp();
        otherForRateApp.setSerno(serno);
        otherForRateApp.setApproveStatus(approveStatus);
        return updateSelective(otherForRateApp);
    }

    /**
     * 审批结束后处理
     *
     * @param serno 申请流水号
     * @author liyonghai
     * @date 2021/6/8 10:31
     */
    public void handleBusinessDataAfterEnd(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            // 1. 更新申请表状态
            OtherForRateApp otherForRateApp = new OtherForRateApp();
            otherForRateApp.setSerno(serno);
            otherForRateApp.setStatus(CommonConstance.STATUS_1);    // 1-生效
            otherForRateApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);    // 997-通过
            updateSelective(otherForRateApp);

            // 2. 置客户名下其他定价申请为失效
            int count = otherForRateAppMapper.failOtherAppByCus(otherForRateApp.getCusId(), serno);
            log.info("申请流水号：【{}】，置客户名下其他定价申请为失效，失效条数：【{}】", serno, count);

        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 根据流水号查询其他事项申报数据
     *
     * @author css
     */

    public OtherAppDto selectOtherAppDtoDataByParam(Map map) {
        return otherForRateAppMapper.selectOtherAppDtoDataByParam(map);
    }
    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public  List<OtherForRateApp> selectByCusId(Map params) {
        String  cusId = (String) params.get("cusId");
        if(null!=cusId || !"".equals(cusId)) {
            return otherForRateAppMapper.selectByCusId(params);
        }else {
            throw new RuntimeException("Oops: cusId can't be null!" );
        }
    }
}
