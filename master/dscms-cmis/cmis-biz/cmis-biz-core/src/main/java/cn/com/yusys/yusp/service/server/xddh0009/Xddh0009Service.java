package cn.com.yusys.yusp.service.server.xddh0009;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.xddh0009.req.Xddh0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0009.resp.Xddh0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 * 接口处理类:还款日期卡控
 *
 * @author zl
 * @version 1.0
 */
@Service
public class Xddh0009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0009Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0009DataRespDto xddh0009(Xddh0009DataReqDto xddh0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value);
        Xddh0009DataRespDto xddh0009DataRespDto = new Xddh0009DataRespDto();
        try {
            String billNo = xddh0009DataReqDto.getBill_no();//获取借据号
            if (StringUtils.isEmpty(billNo)) {
                throw new Exception("借据号【bill_no】不能为空");
            }
            String prd_type = accLoanMapper.selectPrdTypeByBillNo(billNo);
            String prd_id = accLoanMapper.selectPrdIdByBillNo(billNo);
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);//利用服务获取营业日期
            String day = openDay.substring(8, 10);//当前日
            if("30024".equals(prd_type)){//木材便捷贷
                //获取卡控日期 老信贷是 select t.lmt_amt_date from lmt_cont_amt_hkrqkk t where t.biz_type='SC030024'
                ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prd_id);
                String endDay = CfgPrdBasicinfoDto.getData().getAdvRepayEndDate();
                if(StringUtils.hasLength(day) && StringUtils.hasLength(endDay) && Integer.parseInt(day) >= Integer.parseInt(endDay)){
                    throw new Exception( "当月木材便捷贷还款日期已过" + endDay + "号，如有疑问，请联系客户经理。");
                }
            }
            //成功返回
            xddh0009DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
            xddh0009DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, e.getMessage());
            xddh0009DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0009DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, e.getMessage());
            xddh0009DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0009DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F + e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value);
        return xddh0009DataRespDto;
    }
}
