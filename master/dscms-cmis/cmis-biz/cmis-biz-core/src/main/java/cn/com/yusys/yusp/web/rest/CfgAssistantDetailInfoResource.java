/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CfgAssistantDetailInfo;
import cn.com.yusys.yusp.service.CfgAssistantDetailInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgAssistantDetailInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-28 15:47:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cfgassistantdetailinfo")
public class CfgAssistantDetailInfoResource {
    @Autowired
    private CfgAssistantDetailInfoService cfgAssistantDetailInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CfgAssistantDetailInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CfgAssistantDetailInfo> list = cfgAssistantDetailInfoService.selectAll(queryModel);
        return new ResultDto<List<CfgAssistantDetailInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CfgAssistantDetailInfo>> index(@RequestBody QueryModel queryModel) {
        List<CfgAssistantDetailInfo> list = cfgAssistantDetailInfoService.selectByModel(queryModel);
        return new ResultDto<List<CfgAssistantDetailInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CfgAssistantDetailInfo> show(@PathVariable("pkId") String pkId) {
        CfgAssistantDetailInfo cfgAssistantDetailInfo = cfgAssistantDetailInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<CfgAssistantDetailInfo>(cfgAssistantDetailInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CfgAssistantDetailInfo> create(@RequestBody CfgAssistantDetailInfo cfgAssistantDetailInfo) throws URISyntaxException {
        cfgAssistantDetailInfoService.insert(cfgAssistantDetailInfo);
        return new ResultDto<CfgAssistantDetailInfo>(cfgAssistantDetailInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CfgAssistantDetailInfo cfgAssistantDetailInfo) throws URISyntaxException {
        int result = cfgAssistantDetailInfoService.update(cfgAssistantDetailInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cfgAssistantDetailInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cfgAssistantDetailInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchupdate
     * @函数描述:批量对象修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchupdate")
    protected ResultDto<Long> batchreceive(@RequestBody List<CfgAssistantDetailInfo> lists) {
        Long count = cfgAssistantDetailInfoService.dealList(lists);
        return new ResultDto<>(count);
    }
}
