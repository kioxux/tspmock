package cn.com.yusys.yusp.web.server.xdxw0024;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0024.req.Xdxw0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0024.resp.Xdxw0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0024.Xdxw0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询是否有信贷记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0024:查询是否有信贷记录")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0024Resource.class);

    @Autowired
    private Xdxw0024Service xdxw0024Service;

    /**
     * 交易码：xdxw0024
     * 交易描述：查询是否有信贷记录
     *
     * @param xdxw0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询是否有信贷记录")
    @PostMapping("/xdxw0024")
    protected @ResponseBody
    ResultDto<Xdxw0024DataRespDto> xdxw0024(@Validated @RequestBody Xdxw0024DataReqDto xdxw0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataReqDto));
        Xdxw0024DataRespDto xdxw0024DataRespDto = new Xdxw0024DataRespDto();// 响应Dto:查询是否有信贷记录
        ResultDto<Xdxw0024DataRespDto> xdxw0024DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataReqDto));
            xdxw0024DataRespDto = xdxw0024Service.getXdxw0024(xdxw0024DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataRespDto));
            // 封装xdxw0024DataResultDto中正确的返回码和返回信息
            xdxw0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, e.getMessage());
            // 封装xdxw0024DataResultDto中异常返回码和返回信息
            xdxw0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0024DataRespDto到xdxw0024DataResultDto中
        xdxw0024DataResultDto.setData(xdxw0024DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataResultDto));
        return xdxw0024DataResultDto;
    }
}
