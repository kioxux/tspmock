package cn.com.yusys.yusp.web.server.xdxw0082;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0082.req.Xdxw0082DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0082.resp.Xdxw0082DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0082.Xdxw0082Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDXW0082:查询上一笔抵押贷款的余额接口（增享贷）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0082Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0082Resource.class);

    @Autowired
    private Xdxw0082Service xdxw0082Service;

    /**
     * 交易码：xdxw0082
     * 交易描述：勘验任务状态同步
     *
     * @param xdxw0082DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询上一笔抵押贷款的余额接口（增享贷）")
    @PostMapping("/xdxw0082")
    protected @ResponseBody
    ResultDto<Xdxw0082DataRespDto> xdxw0082(@Validated @RequestBody Xdxw0082DataReqDto xdxw0082DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataReqDto));
        Xdxw0082DataRespDto xdxw0082DataRespDto = new Xdxw0082DataRespDto();// 响应Dto:勘验任务状态同步
        ResultDto<Xdxw0082DataRespDto> xdxw0082DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0082DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataReqDto));
            xdxw0082DataRespDto = xdxw0082Service.xdxw0082(xdxw0082DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataRespDto));
            // 封装xdxw0082DataResultDto中正确的返回码和返回信息
            xdxw0082DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0082DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch(BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, e.getMessage());
            // 封装xdxw0082DataResultDto中异常返回码和返回信息
            xdxw0082DataResultDto.setCode(e.getErrorCode());
            xdxw0082DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, e.getMessage());
            // 封装xdxw0082DataResultDto中异常返回码和返回信息
            xdxw0082DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0082DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0082DataRespDto到xdxw0082DataResultDto中
        xdxw0082DataResultDto.setData(xdxw0082DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataResultDto));
        return xdxw0082DataResultDto;
    }
}
