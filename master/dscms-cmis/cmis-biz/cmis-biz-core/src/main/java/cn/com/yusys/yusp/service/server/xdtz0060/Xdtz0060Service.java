package cn.com.yusys.yusp.service.server.xdtz0060;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0060.req.Xdtz0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.resp.Xdtz0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.server.xdtz0059.Xdtz0059Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 业务逻辑类:通过借据号查询借据信息
 *业务逻辑：
 * 1查询企业：企业在我行有未结清逾期、呆账、呆滞、垫款业务；   （台账状态）
 *           贷款台账 ACC_LOAN
 *           银承台账  ACC_ACCP
 *           贴现台账 ACC_DISC
 *           保函 ACC_CVRS、
 *           开证  ACC_TF_LOC
 * 2查询企业实际控制人：企业实际控制人在我行贷款存在逾期、呆账、呆滞  （台账状态）
 *        贷款台账 ACC_LOAN
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0060Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0060Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0060DataRespDto xdtz0060(Xdtz0060DataReqDto xdtz0060DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataReqDto));
        Xdtz0060DataRespDto xdtz0060DataRespDto = new Xdtz0060DataRespDto();
        String queryType = xdtz0060DataReqDto.getQueryType();
        int num = 0;
        try {
            if (Objects.equals(queryType, "01")) {
                num += accLoanMapper.getEnterpriseCount(xdtz0060DataReqDto);
            }
            if (Objects.equals(queryType, "02")) {
                num += accLoanMapper.getControllerCount(xdtz0060DataReqDto);
            }
            if (num > 0) {
                xdtz0060DataRespDto.setIsOverdueOrBadDebts(CmisBizConstants.YES);
            } else {
                xdtz0060DataRespDto.setIsOverdueOrBadDebts(CmisBizConstants.NO);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataRespDto));
        return xdtz0060DataRespDto;
    }
}
