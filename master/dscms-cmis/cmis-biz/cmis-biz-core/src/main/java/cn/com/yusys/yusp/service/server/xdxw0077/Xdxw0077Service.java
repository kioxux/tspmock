package cn.com.yusys.yusp.service.server.xdxw0077;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0077.req.Xdxw0077DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0077.resp.Xdxw0077DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0077Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-12 14:03:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0077Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0077Service.class);

    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;
    /**
     * 渠道端查询我的贷款（流程监控）
     * @param xdxw0077DataReqDto
     * @return
     */
    public Xdxw0077DataRespDto getXdxw0077(Xdxw0077DataReqDto xdxw0077DataReqDto) throws BizException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataReqDto));
        Xdxw0077DataRespDto xdxw0077DataRespDto = new Xdxw0077DataRespDto();
        try {
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0077.resp.List> lists = new ArrayList<>();
            Map param = new HashMap<>();
            //01-个人客户查询；
            //02-企业客户查询；
            String queryType = xdxw0077DataReqDto.getQueryType();
            if(Objects.equals(CommonConstance.QUERY_TYPE_01,queryType)){
                //根据个人客户查询时传业务流水号和产品编号
                //业务流水号
                String serno = xdxw0077DataReqDto.getSerno();
                //产品编号
                String prdNo = xdxw0077DataReqDto.getPrdNo();
                if(StringUtils.isEmpty(serno)){
                    throw BizException.error(null, EcbEnum.ECB010050.key,EcbEnum.ECB010050.value);
                }
                if(StringUtils.isEmpty(prdNo)){
                    throw BizException.error(null, EcbEnum.ECB010051.key,EcbEnum.ECB010051.value);
                }
                param.put("serno",serno);
                param.put("prdNo",prdNo);
                logger.info("***********XDXW0077个人客户查询出账信息开始,查询参数为:{}", JSON.toJSONString(param));
                lists = pvpLoanAppMapper.getLoanInfo4Indiv(param);
                logger.info("***********XDXW0077个人客户查询出账信息结束,返回结果为:{}", JSON.toJSONString(lists));
            }else if(Objects.equals(CommonConstance.QUERY_TYPE_02,queryType)){
                //根据对公客户查询时传客户编号
                String cusId = xdxw0077DataReqDto.getCusId();
                if(StringUtils.isEmpty(cusId)){
                    throw BizException.error(null, EcbEnum.ECB010014.key,EcbEnum.ECB010014.value);
                }
                param.put("cusId",cusId);
                logger.info("***********XDXW0077企业客户查询出账信息开始,查询参数为:{}", JSON.toJSONString(param));
                lists = pvpLoanAppMapper.getLoanInfo4Cus(param);
                logger.info("***********XDXW0077企业客户查询出账信息结束,返回结果为:{}", JSON.toJSONString(lists));
            }
            xdxw0077DataRespDto.setList(lists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataRespDto));
        return xdxw0077DataRespDto;
    }
}
