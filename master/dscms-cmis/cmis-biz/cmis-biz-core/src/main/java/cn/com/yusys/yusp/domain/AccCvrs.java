/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccCvrs
 * @类描述: acc_cvrs数据实体类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:54:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_cvrs")
public class AccCvrs extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = false, length = 40)
	private String pvpSerno;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 保函类型 **/
	@Column(name = "GUARANT_TYPE", unique = false, nullable = true, length = 5)
	private String guarantType;
	
	/** 保函种类 **/
	@Column(name = "GUARANT_MODE", unique = false, nullable = true, length = 5)
	private String guarantMode;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 汇率 **/
	@Column(name = "EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRate;
	
	/** 保函金额 **/
	@Column(name = "GUARANT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarantAmt;
	
	/** 折合人民币金额 **/
	@Column(name = "EXCHANGE_RMB_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRmbAmt;
	
	/** 敞口余额 **/
	@Column(name = "SPAC_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spacBal;

	/** 原始敞口金额 **/
	@Column(name = "ORIGI_OPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenAmt;

	/** 折合人民币敞口 **/
	@Column(name = "EXCHANGE_RMB_SPAC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRmbSpac;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金币种 **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 保证金汇率 **/
	@Column(name = "BAIL_EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailExchangeRate;
	
	/** 保证金折算人民币金额 **/
	@Column(name = "BAIL_CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailCvtCnyAmt;
	
	/** 保证金计算方式 **/
	@Column(name = "BAIL_CAL_TYPE", unique = false, nullable = true, length = 5)
	private String bailCalType;
	
	/** 手续费率 **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;
	
	/** 手续费金额 **/
	@Column(name = "CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgAmt;
	
	/** 生效日期 **/
	@Column(name = "INURE_DATE", unique = false, nullable = true, length = 20)
	private String inureDate;
	
	/** 失效日期 **/
	@Column(name = "INVL_DATE", unique = false, nullable = true, length = 20)
	private String invlDate;
	
	/** 逾期垫款年利率 **/
	@Column(name = "OVERDUE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRate;
	
	/** 保函付款方式 **/
	@Column(name = "GUARANT_PAY_MODE", unique = false, nullable = true, length = 5)
	private String guarantPayMode;
	
	/** 受益人名称 **/
	@Column(name = "BENEFICIAR_NAME", unique = false, nullable = true, length = 80)
	private String beneficiarName;
	
	/** 结算账号 **/
	@Column(name = "SETTL_ACCNO", unique = false, nullable = true, length = 40)
	private String settlAccno;
	
	/** 结算账户名称 **/
	@Column(name = "SETTL_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String settlAcctName;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 20)
	private String replyNo;
	
	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;
	
	/** 分类日期 **/
	@Column(name = "CLASS_DATE", unique = false, nullable = true, length = 20)
	private String classDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 操作类型 STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return this.pvpSerno;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param guarantType
	 */
	public void setGuarantType(String guarantType) {
		this.guarantType = guarantType;
	}
	
    /**
     * @return guarantType
     */
	public String getGuarantType() {
		return this.guarantType;
	}
	
	/**
	 * @param guarantMode
	 */
	public void setGuarantMode(String guarantMode) {
		this.guarantMode = guarantMode;
	}
	
    /**
     * @return guarantMode
     */
	public String getGuarantMode() {
		return this.guarantMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return exchangeRate
     */
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param guarantAmt
	 */
	public void setGuarantAmt(java.math.BigDecimal guarantAmt) {
		this.guarantAmt = guarantAmt;
	}
	
    /**
     * @return guarantAmt
     */
	public java.math.BigDecimal getGuarantAmt() {
		return this.guarantAmt;
	}
	
	/**
	 * @param exchangeRmbAmt
	 */
	public void setExchangeRmbAmt(java.math.BigDecimal exchangeRmbAmt) {
		this.exchangeRmbAmt = exchangeRmbAmt;
	}
	
    /**
     * @return exchangeRmbAmt
     */
	public java.math.BigDecimal getExchangeRmbAmt() {
		return this.exchangeRmbAmt;
	}
	
	/**
	 * @param spacBal
	 */
	public void setSpacBal(java.math.BigDecimal spacBal) {
		this.spacBal = spacBal;
	}

	/**
	 * @param origiOpenAmt
	 */
	public void setOrigiOpenAmt(java.math.BigDecimal origiOpenAmt) {
		this.origiOpenAmt = origiOpenAmt;
	}

	/**
	 * @return origiOpenAmt
	 */
	public java.math.BigDecimal getOrigiOpenAmt() {
		return this.origiOpenAmt;
	}

    /**
     * @return spacBal
     */
	public java.math.BigDecimal getSpacBal() {
		return this.spacBal;
	}
	
	/**
	 * @param exchangeRmbSpac
	 */
	public void setExchangeRmbSpac(java.math.BigDecimal exchangeRmbSpac) {
		this.exchangeRmbSpac = exchangeRmbSpac;
	}
	
    /**
     * @return exchangeRmbSpac
     */
	public java.math.BigDecimal getExchangeRmbSpac() {
		return this.exchangeRmbSpac;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}
	
    /**
     * @return bailCurType
     */
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailExchangeRate
	 */
	public void setBailExchangeRate(java.math.BigDecimal bailExchangeRate) {
		this.bailExchangeRate = bailExchangeRate;
	}
	
    /**
     * @return bailExchangeRate
     */
	public java.math.BigDecimal getBailExchangeRate() {
		return this.bailExchangeRate;
	}
	
	/**
	 * @param bailCvtCnyAmt
	 */
	public void setBailCvtCnyAmt(java.math.BigDecimal bailCvtCnyAmt) {
		this.bailCvtCnyAmt = bailCvtCnyAmt;
	}
	
    /**
     * @return bailCvtCnyAmt
     */
	public java.math.BigDecimal getBailCvtCnyAmt() {
		return this.bailCvtCnyAmt;
	}
	
	/**
	 * @param bailCalType
	 */
	public void setBailCalType(String bailCalType) {
		this.bailCalType = bailCalType;
	}
	
    /**
     * @return bailCalType
     */
	public String getBailCalType() {
		return this.bailCalType;
	}
	
	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}
	
    /**
     * @return chrgRate
     */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}
	
	/**
	 * @param chrgAmt
	 */
	public void setChrgAmt(java.math.BigDecimal chrgAmt) {
		this.chrgAmt = chrgAmt;
	}
	
    /**
     * @return chrgAmt
     */
	public java.math.BigDecimal getChrgAmt() {
		return this.chrgAmt;
	}
	
	/**
	 * @param inureDate
	 */
	public void setInureDate(String inureDate) {
		this.inureDate = inureDate;
	}
	
    /**
     * @return inureDate
     */
	public String getInureDate() {
		return this.inureDate;
	}
	
	/**
	 * @param invlDate
	 */
	public void setInvlDate(String invlDate) {
		this.invlDate = invlDate;
	}
	
    /**
     * @return invlDate
     */
	public String getInvlDate() {
		return this.invlDate;
	}
	
	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}
	
    /**
     * @return overdueRate
     */
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}
	
	/**
	 * @param guarantPayMode
	 */
	public void setguarantPayMode(String guarantPayMode) {
		this.guarantPayMode = guarantPayMode;
	}
	
    /**
     * @return guarantPayMode
     */
	public String getguarantPayMode() {
		return this.guarantPayMode;
	}
	
	/**
	 * @param beneficiarName
	 */
	public void setBeneficiarName(String beneficiarName) {
		this.beneficiarName = beneficiarName;
	}
	
    /**
     * @return beneficiarName
     */
	public String getBeneficiarName() {
		return this.beneficiarName;
	}
	
	/**
	 * @param settlAccno
	 */
	public void setSettlAccno(String settlAccno) {
		this.settlAccno = settlAccno;
	}
	
    /**
     * @return settlAccno
     */
	public String getSettlAccno() {
		return this.settlAccno;
	}
	
	/**
	 * @param settlAcctName
	 */
	public void setSettlAcctName(String settlAcctName) {
		this.settlAcctName = settlAcctName;
	}
	
    /**
     * @return settlAcctName
     */
	public String getSettlAcctName() {
		return this.settlAcctName;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}
	
    /**
     * @return tenClass
     */
	public String getTenClass() {
		return this.tenClass;
	}
	
	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}
	
    /**
     * @return classDate
     */
	public String getClassDate() {
		return this.classDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}