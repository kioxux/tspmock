/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.service.LmtReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReply;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreply")
public class LmtReplyResource {
    @Autowired
    private LmtReplyService lmtReplyService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReply>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReply> list = lmtReplyService.selectAll(queryModel);
        return new ResultDto<List<LmtReply>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReply>> index(QueryModel queryModel) {
        List<LmtReply> list = lmtReplyService.selectByModel(queryModel);
        return new ResultDto<List<LmtReply>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReply> show(@PathVariable("pkId") String pkId) {
        LmtReply lmtReply = lmtReplyService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReply>(lmtReply);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReply> create(@RequestBody LmtReply lmtReply) throws URISyntaxException {
        lmtReplyService.insert(lmtReply);
        return new ResultDto<LmtReply>(lmtReply);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReply lmtReply) throws URISyntaxException {
        int result = lmtReplyService.update(lmtReply);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getHisReply
     * @方法描述：获取批复历史沿革
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-19 下午 2:29
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getHisReply")
    protected ResultDto<List<LmtReply>> getHisReply(@RequestBody QueryModel queryModel) {
        List<LmtReply> lmtReplyList = lmtReplyService.getOriginReply(queryModel);
        return new ResultDto<>(lmtReplyList);
    }

    /**
     * @方法名称：getChangeableReply
     * @方法描述：查询可变更的批复
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-21 下午 8:06
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getChangeableReply")
    protected ResultDto<List<LmtReply>> getChangeableReply(@RequestBody QueryModel model) {
        List<LmtReply> lmtReplyList = lmtReplyService.qryChangeableReply(model);
        return new ResultDto<>(lmtReplyList);
    }

    /**
     * @方法名称：getAllReplyInfo
     * @方法描述：获取批复所有数据，包括分项、产品、用信条件
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-28 上午 10:36
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getAllReplyInfo")
    protected ResultDto<Map> getAllReplyInfo(@RequestBody String replySerno) {
        Map map = lmtReplyService.getAllReplyInfo(replySerno);
        return new ResultDto<Map>(map);
    }

    /**
       * @方法名称：replyForManager
       * @方法描述：获取当前登录人下的批复信息
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-29 上午 9:07
       * @修改记录：修改时间   修改人员  修改原因
       */
    @PostMapping("/replyForManager")
    protected ResultDto<List<LmtReply>> replyForManager() {
        List<LmtReply> lmtReplyList = lmtReplyService.queryByManagerId();
        return new ResultDto<>(lmtReplyList);
    }

    /**
     * @方法名称：queryByCusId
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/24 9:58
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/querybycusid")
    protected ResultDto<List<LmtReply>> queryByCusId(@RequestBody QueryModel queryModel) {
        List<LmtReply> lmtReplyList = lmtReplyService.selectByCusId(queryModel);
        return new ResultDto<>(lmtReplyList);
    }

    /**
     * @方法名称：selectOpenTotalLmtAmtByCusId
     * @方法描述：
     * @创建人：dumingdi
     * @创建时间：2021/9/22 11:00
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/selectopentotallmtamtbycusid")
    protected ResultDto<BigDecimal> selectOpenTotalLmtAmtByCusId(@RequestBody String cusId) {
        BigDecimal openTotalLmtAmt = lmtReplyService.selectOpenTotalLmtAmtByCusId(cusId);
        return new ResultDto<>(openTotalLmtAmt);
    }
}
