/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaInfo;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRelFinaInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-19 10:26:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRelFinaInfoService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtSigInvestRelFinaInfoService.class);

    @Autowired
    private LmtSigInvestRelFinaInfoMapper lmtSigInvestRelFinaInfoMapper;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRelFinaInfo selectByPrimaryKey(String pkId) {
        return lmtSigInvestRelFinaInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRelFinaInfo> selectAll(QueryModel model) {
        List<LmtSigInvestRelFinaInfo> records = (List<LmtSigInvestRelFinaInfo>) lmtSigInvestRelFinaInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRelFinaInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRelFinaInfo> list = lmtSigInvestRelFinaInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestRelFinaInfo record) {
        return lmtSigInvestRelFinaInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRelFinaInfo record) {
        return lmtSigInvestRelFinaInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(LmtSigInvestRelFinaInfo record) {
        return lmtSigInvestRelFinaInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRelFinaInfo record) {
        return lmtSigInvestRelFinaInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRelFinaInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRelFinaInfoMapper.deleteByIds(ids);
    }

    /**
     * 主题授信-主体分析详情获取
     * @param serno
     * @param cusId
     * @return
     */
    public LmtSigInvestRelFinaInfo selectBySerno(String serno, String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        queryModel.addCondition("cusId",cusId);
        BizInvestCommonService.checkParamsIsNull("cusId",cusId);
        List<LmtSigInvestRelFinaInfo> lmtSigInvestRelFinaInfos = lmtSigInvestRelFinaInfoMapper.selectByModel(queryModel);
        if (lmtSigInvestRelFinaInfos!= null && lmtSigInvestRelFinaInfos.size()>0){
            return lmtSigInvestRelFinaInfos.get(0);
        }
        return null;
    }

    /**
     * 主体授信-（同业）添加单笔投资授信主体及增信人财务信息
     * @param serno
     * @param data
     * @param cusType
     * @param cusCatalog
     * @return
     */
    public int insertFinalInfoBy0010(String serno, CmisCus0010RespDto data,String cusType, String cusCatalog) {
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = new LmtSigInvestRelFinaInfo();
        lmtSigInvestRelFinaInfo.setPkId(generatePkId());
        lmtSigInvestRelFinaInfo.setSerno(serno);
        lmtSigInvestRelFinaInfo.setCusId(data.getCusId());
        lmtSigInvestRelFinaInfo.setCusName(data.getCusName());
        lmtSigInvestRelFinaInfo.setCusType(cusType);
        lmtSigInvestRelFinaInfo.setCusCatalog(cusCatalog);
        lmtSigInvestRelFinaInfo.setOprType(CmisBizConstants.OPR_TYPE_01);
        lmtSigInvestRelFinaInfo.setInputBrId(getCurrentUser().getOrg().getCode());
        lmtSigInvestRelFinaInfo.setInputDate(getCurrrentDateStr());
        lmtSigInvestRelFinaInfo.setInputId(getCurrentUser().getLoginCode());
        lmtSigInvestRelFinaInfo.setCreateTime(getCurrrentDate());
        lmtSigInvestRelFinaInfo.setUpdBrId(getCurrentUser().getOrg().getCode());
        lmtSigInvestRelFinaInfo.setUpdId(getCurrentUser().getLoginCode());
        lmtSigInvestRelFinaInfo.setUpdDate(getCurrrentDateStr());
        lmtSigInvestRelFinaInfo.setUpdateTime(getCurrrentDate());
        return insert(lmtSigInvestRelFinaInfo);
    }

    /**
     * 主体授信-(法人)添加单笔投资授信主体及增信人财务信息
     * @param serno
     * @param data
     * @param cusType
     * @param cusCatalog
     * @return
     */
    public int insertFinalInfoBy0014(String serno, CmisCus0014RespDto data, String cusType, String cusCatalog) {
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = new LmtSigInvestRelFinaInfo();
        lmtSigInvestRelFinaInfo.setPkId(generatePkId());
        lmtSigInvestRelFinaInfo.setSerno(serno);
        lmtSigInvestRelFinaInfo.setCusId(data.getCusId());
        lmtSigInvestRelFinaInfo.setCusName(data.getCusName());
        lmtSigInvestRelFinaInfo.setCusType(data.getCusType());
        lmtSigInvestRelFinaInfo.setCusCatalog(cusCatalog);
        lmtSigInvestRelFinaInfo.setOprType(CmisBizConstants.OPR_TYPE_01);
        lmtSigInvestRelFinaInfo.setInputBrId(getCurrentUser().getOrg().getCode());
        lmtSigInvestRelFinaInfo.setInputDate(getCurrrentDateStr());
        lmtSigInvestRelFinaInfo.setInputId(getCurrentUser().getLoginCode());
        lmtSigInvestRelFinaInfo.setCreateTime(getCurrrentDate());
        lmtSigInvestRelFinaInfo.setUpdBrId(getCurrentUser().getOrg().getCode());
        lmtSigInvestRelFinaInfo.setUpdId(getCurrentUser().getLoginCode());
        lmtSigInvestRelFinaInfo.setUpdDate(getCurrrentDateStr());
        lmtSigInvestRelFinaInfo.setUpdateTime(getCurrrentDate());
        return insert(lmtSigInvestRelFinaInfo);

    }

    /**
     * 根据serno和cusid 更新修改的字段
     * TODO  引导页面的选择的企业和担保人所选企业相同 会冲突
     * @param lmtSigInvestRelFinaInfo
     * @return
     */
    public int updateByKeyAndVal(LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo) {
        //从数据库中获取该记录
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",lmtSigInvestRelFinaInfo.getSerno());
        BizInvestCommonService.checkParamsIsNull("serno",lmtSigInvestRelFinaInfo.getSerno());
        queryModel.addCondition("cusId",lmtSigInvestRelFinaInfo.getCusId());
        BizInvestCommonService.checkParamsIsNull("cusId",lmtSigInvestRelFinaInfo.getCusId());
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtSigInvestRelFinaInfo> lmtSigInvestRelFinaInfos = selectByModel(queryModel);
        if (lmtSigInvestRelFinaInfos!=null && lmtSigInvestRelFinaInfos.size()>0){
            LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo1 = lmtSigInvestRelFinaInfos.get(0);
            //设置pkid
            lmtSigInvestRelFinaInfo.setPkId(lmtSigInvestRelFinaInfo1.getPkId());
            return updateSelective(lmtSigInvestRelFinaInfo);
        }
        return 0;
    }


    /**
     * 更新保存图片绝对路径
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = new LmtSigInvestRelFinaInfo();
        lmtSigInvestRelFinaInfo.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/"+getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)){
            picAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,fanruanFileTemplate);
        }
        setValByKey(lmtSigInvestRelFinaInfo,key,picAbsolutePath);
        updateSelective(lmtSigInvestRelFinaInfo);
        return picAbsolutePath;
    }

    /**
     *
     * @param origiSerno
     * @param serno
     * @param cusId
     */
    public void copyRelFinaInfo(String origiSerno, String serno, String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",origiSerno);
        BizInvestCommonService.checkParamsIsNull("serno",origiSerno);
        queryModel.addCondition("cusId",cusId);
        BizInvestCommonService.checkParamsIsNull("cusId",cusId);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestRelFinaInfo> lmtSigInvestRelFinaInfos = selectAll(queryModel);
        for (LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo : lmtSigInvestRelFinaInfos) {
            LmtSigInvestRelFinaInfo newLmtSigInvestRelFinaInfo = new LmtSigInvestRelFinaInfo();
            copyProperties(lmtSigInvestRelFinaInfo,newLmtSigInvestRelFinaInfo);
            //初始化(新增)通用domain信息
            initInsertDomainProperties(newLmtSigInvestRelFinaInfo);
            newLmtSigInvestRelFinaInfo.setSerno(serno);
            insert(newLmtSigInvestRelFinaInfo);
        }
    }

    /**
     * 获取文件信息
     * @param condition
     * @return
     */
    public TempFileInfo selectFileInfo(Map condition) {
        String filePath = (String) condition.get("filePath");
        if (!StringUtils.isBlank(filePath)){
            TempFileInfo tempFileInfo = getTempFileInfo(filePath);
            log.info("============>tempFileInfo====>{}",tempFileInfo);
            return tempFileInfo;
        }
        return null;
    }

    /**
     * 更新主体分析
     * @param lmtSigInvestRelFinaInfo
     * @return
     */
    public int updateZtfxOrCwzk(LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo) {
        String serno = lmtSigInvestRelFinaInfo.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"ztfx");
        updateMustCheckStatus(serno,"cwzk");
        return updateSelective(lmtSigInvestRelFinaInfo);
    }
}
