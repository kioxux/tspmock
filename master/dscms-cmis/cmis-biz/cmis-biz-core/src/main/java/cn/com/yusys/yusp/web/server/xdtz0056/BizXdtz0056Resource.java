package cn.com.yusys.yusp.web.server.xdtz0056;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0056.Xdtz0056Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0056.req.Xdtz0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0056.resp.Xdtz0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询还款业务类型
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0056:查询还款业务类型")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0056Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0056Resource.class);

    @Autowired
    private Xdtz0056Service xdtz0056Service;

    /**
     * 交易码：xdtz0056
     * 交易描述：查询还款业务类型
     *
     * @param xdtz0056DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询还款业务类型")
    @PostMapping("/xdtz0056")
    protected @ResponseBody
    ResultDto<Xdtz0056DataRespDto> xdtz0056(@Validated @RequestBody Xdtz0056DataReqDto xdtz0056DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056DataReqDto));
        // 响应Dto:查询还款业务类型
        Xdtz0056DataRespDto xdtz0056DataRespDto = new Xdtz0056DataRespDto();
        ResultDto<Xdtz0056DataRespDto> xdtz0056DataResultDto = new ResultDto<>();
        //借据号
        String billno = xdtz0056DataReqDto.getBillno();
        try {
            //billNo不得为空
            if (StringUtils.isBlank(billno)){
                xdtz0056DataResultDto.setData(xdtz0056DataRespDto);
                xdtz0056DataResultDto.setCode(EpbEnum.EPB090009.key);
                xdtz0056DataResultDto.setMessage(EpbEnum.EPB090009.value);
                return xdtz0056DataResultDto;
            }
            // 从xdtz0056DataReqDto获取业务值进行业务逻辑处理
            xdtz0056DataRespDto = xdtz0056Service.xdtz0056(billno);
            //查询不到结果则认为查询异常
            if (StringUtils.isBlank(xdtz0056DataRespDto.getBiztype())){
                xdtz0056DataResultDto.setData(xdtz0056DataRespDto);
                xdtz0056DataResultDto.setCode(EpbEnum.EPB090004.key);
                xdtz0056DataResultDto.setMessage(EpbEnum.EPB090004.value);
                return xdtz0056DataResultDto;
            }
            // 封装xdtz0056DataResultDto中正确的返回码和返回信息
            xdtz0056DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0056DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, e.getMessage());
            // 封装xdtz0056DataResultDto中异常返回码和返回信息
            xdtz0056DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0056DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0056DataRespDto到xdtz0056DataResultDto中
        xdtz0056DataResultDto.setData(xdtz0056DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056DataResultDto));
        return xdtz0056DataResultDto;
    }
}
