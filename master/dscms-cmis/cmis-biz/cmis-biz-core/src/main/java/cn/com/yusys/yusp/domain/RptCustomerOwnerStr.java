/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCustomerOwnerStr
 * @类描述: rpt_customer_owner_str数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-21 19:34:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_customer_owner_str")
public class RptCustomerOwnerStr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 股东名称 **/
	@Column(name = "SHD_CUS_NAME", unique = false, nullable = true, length = 40)
	private String shdCusName;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 占比 **/
	@Column(name = "PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal perc;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCap;
	
	/** 出资方式 **/
	@Column(name = "INV_APP", unique = false, nullable = true, length = 5)
	private String invApp;
	
	/** 溢价 **/
	@Column(name = "PREMIUM", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal premium;
	
	/** 出资日期 **/
	@Column(name = "INVT_DATE", unique = false, nullable = true, length = 20)
	private String invtDate;
	
	/** 出资额 **/
	@Column(name = "INVT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal invtAmt;
	
	/** 股东主要情况 **/
	@Column(name = "SHD_MAIN_CASE", unique = false, nullable = true, length = 65535)
	private String shdMainCase;
	
	/** 认缴金额 **/
	@Column(name = "PERMIUM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal permiumAmt;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param shdCusName
	 */
	public void setShdCusName(String shdCusName) {
		this.shdCusName = shdCusName;
	}
	
    /**
     * @return shdCusName
     */
	public String getShdCusName() {
		return this.shdCusName;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param perc
	 */
	public void setPerc(java.math.BigDecimal perc) {
		this.perc = perc;
	}
	
    /**
     * @return perc
     */
	public java.math.BigDecimal getPerc() {
		return this.perc;
	}
	
	/**
	 * @param paidCap
	 */
	public void setPaidCap(java.math.BigDecimal paidCap) {
		this.paidCap = paidCap;
	}
	
    /**
     * @return paidCap
     */
	public java.math.BigDecimal getPaidCap() {
		return this.paidCap;
	}
	
	/**
	 * @param invApp
	 */
	public void setInvApp(String invApp) {
		this.invApp = invApp;
	}
	
    /**
     * @return invApp
     */
	public String getInvApp() {
		return this.invApp;
	}
	
	/**
	 * @param premium
	 */
	public void setPremium(java.math.BigDecimal premium) {
		this.premium = premium;
	}
	
    /**
     * @return premium
     */
	public java.math.BigDecimal getPremium() {
		return this.premium;
	}
	
	/**
	 * @param invtDate
	 */
	public void setInvtDate(String invtDate) {
		this.invtDate = invtDate;
	}
	
    /**
     * @return invtDate
     */
	public String getInvtDate() {
		return this.invtDate;
	}
	
	/**
	 * @param invtAmt
	 */
	public void setInvtAmt(java.math.BigDecimal invtAmt) {
		this.invtAmt = invtAmt;
	}
	
    /**
     * @return invtAmt
     */
	public java.math.BigDecimal getInvtAmt() {
		return this.invtAmt;
	}
	
	/**
	 * @param shdMainCase
	 */
	public void setShdMainCase(String shdMainCase) {
		this.shdMainCase = shdMainCase;
	}
	
    /**
     * @return shdMainCase
     */
	public String getShdMainCase() {
		return this.shdMainCase;
	}
	
	/**
	 * @param permiumAmt
	 */
	public void setPermiumAmt(java.math.BigDecimal permiumAmt) {
		this.permiumAmt = permiumAmt;
	}
	
    /**
     * @return permiumAmt
     */
	public java.math.BigDecimal getPermiumAmt() {
		return this.permiumAmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}


}