/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.LmtApproveSubPrdDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.LmtApprSubMapper;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:35:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtApprSubService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppSubService.class);

    @Resource
    private LmtApprSubMapper lmtApprSubMapper;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtApprSubPrdService lmtApprSubPrdService;

    @Autowired
    private LmtApprSubService lmtApprSubService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtApprService lmtApprService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtGrpApprService lmtGrpApprService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtApprSub selectByPrimaryKey(String pkId) {
        return lmtApprSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtApprSub> selectAll(QueryModel model) {
        List<LmtApprSub> records = (List<LmtApprSub>) lmtApprSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtApprSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtApprSub> list = lmtApprSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtApprSub record) {
        return lmtApprSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtApprSub record) {
        return lmtApprSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtApprSub record) {
        return lmtApprSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtApprSub record) {
        return lmtApprSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtApprSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtApprSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selectLmtApprSubByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprSub> selectLmtApprSubByParams(HashMap paramsMap){
        return lmtApprSubMapper.selectLmtApprSubByParams(paramsMap);
    }

    /**
     * @方法名称: generateLmtApprSubByLmtAppSub
     * @方法描述: 通过授信申请信息生成授信审批分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubByLmtApp(String currentUserId, String currentOrgId, LmtApp lmtApp, String apprSerno) throws Exception {
        // 遍历分项
        HashMap<String, String> querySubMap = new HashMap<String, String>();
        querySubMap.put("serno", lmtApp.getSerno());
        querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByParams(querySubMap);
        if(lmtAppSubList != null && lmtAppSubList.size() > 0){
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                String apprSubSerno = generateLmtApprSubByLmtAppSub(currentUserId, currentOrgId, apprSerno, lmtAppSub);
                lmtApprSubPrdService.generateLmtApprSubPrdByLmtAppSub(currentUserId, currentOrgId, lmtAppSub, apprSubSerno);
            }
        }else {
            throw new Exception("授信申请分项数据查询异常!");
        }
    }

    /**
     * @方法名称: generateLmtApprSubByLmtAppSub
     * @方法描述: 通过授信申请分项信息生成授信审批分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtApprSubByLmtAppSub(String currentUserId, String currentOrgId, String apprSerno, LmtAppSub lmtAppSub) {
        LmtApprSub newLmtApprSub = new LmtApprSub();
        List<GuarBizRel> guarBizRelList = new ArrayList<>();
        BeanUtils.copyProperties(lmtAppSub, newLmtApprSub);
        String apprSubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
        newLmtApprSub.setPkId(UUID.randomUUID().toString());
        newLmtApprSub.setApproveSubSerno(apprSubSerno);
        newLmtApprSub.setApproveSerno(apprSerno);
        newLmtApprSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSub.setUpdId(currentUserId);
        newLmtApprSub.setUpdBrId(currentOrgId);
        newLmtApprSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtApprSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        this.insert(newLmtApprSub);
        guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtAppSub.getSubSerno());
        for(GuarBizRel guarBizRel: guarBizRelList){
            GuarBizRel guarBizRelAppr = new GuarBizRel();
            BeanUtils.copyProperties(guarBizRel, guarBizRelAppr);
            guarBizRelAppr.setSerno(apprSubSerno);
            guarBizRelAppr.setPkId(UUID.randomUUID().toString());
            guarBizRelAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            guarBizRelAppr.setUpdId(currentUserId);
            guarBizRelAppr.setUpdBrId(currentOrgId);
            guarBizRelAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            guarBizRelService.insert(guarBizRelAppr);
        }
        return apprSubSerno;
    }

    /**
     * @方法名称: generateLmtApprSubByLmtAppr
     * @方法描述: 通过授信审批分项信息生成授信审批分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtApprSubByLmtAppr(LmtAppr newLmtAppr, LmtAppr lmtAppr, String currentUserId, String currentOrgId) throws Exception {
        HashMap<String, String> querySubMap = new HashMap<String, String>();
        querySubMap.put("apprSerno", lmtAppr.getApproveSerno());
        querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        log.info("根据原成员客户审批表信息查询成员客户授信分项信息,原成员客户审批表审批流水号为:"+lmtAppr.getApproveSerno());
        List<LmtApprSub> lmtApprSubList = lmtApprSubService.selectLmtApprSubByParams(querySubMap);
        if(lmtApprSubList != null && lmtApprSubList.size() > 0){
            for (LmtApprSub lmtApprSub : lmtApprSubList) {
                String repaySubSerno = generateLmtApprSubByLmtApprSub(newLmtAppr, currentUserId, currentOrgId, lmtApprSub);
                lmtApprSubPrdService.generateLmtApprSubPrdByLmtApprSub(currentUserId, currentOrgId, lmtApprSub, repaySubSerno);
            }
        }else{
            throw new Exception("授信审批分项数据查询异常!");
        }
    }

    /**
     * @方法名称: generateLmtApprSubByLmtApprSub
     * @方法描述: 通过授信审批分项信息生成授信审批分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtApprSubByLmtApprSub(LmtAppr newLmtAppr, String currentUserId, String currentOrgId, LmtApprSub lmtApprSub) {
        LmtApprSub newLmtApprSub = new LmtApprSub();
        BeanUtils.copyProperties(lmtApprSub, newLmtApprSub);
        String repaySubSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_APPR_SERNO, new HashMap<>());
        newLmtApprSub.setPkId(UUID.randomUUID().toString());
        newLmtApprSub.setApproveSerno(newLmtAppr.getApproveSerno());
        newLmtApprSub.setApproveSubSerno(repaySubSerno);
        newLmtApprSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
//        newLmtApprSub.setUpdId(currentUserId);
//        newLmtApprSub.setUpdBrId(currentOrgId);
        newLmtApprSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        newLmtApprSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        newLmtApprSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        log.info("根据新成员客户审批表数据生成新的成员客户审批分项表数据,新成员客户审批分项表审批分项流水号为:"+newLmtApprSub.getApproveSubSerno());
        lmtApprSubService.insert(newLmtApprSub);
        List<GuarBizRel> guarBizRelList = new ArrayList<>();
        guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(lmtApprSub.getApproveSubSerno());
        for(GuarBizRel guarBizRel: guarBizRelList){
            GuarBizRel guarBizRelAppr = new GuarBizRel();
            BeanUtils.copyProperties(guarBizRel, guarBizRelAppr);
            guarBizRelAppr.setSerno(repaySubSerno);
            guarBizRelAppr.setPkId(UUID.randomUUID().toString());
            guarBizRelAppr.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            guarBizRelAppr.setUpdId(currentUserId);
            guarBizRelAppr.setUpdBrId(currentOrgId);
            guarBizRelAppr.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            guarBizRelService.insert(guarBizRelAppr);
        }
        return repaySubSerno;
    }
    /**
     * @方法名称: queryLmtApproveSubByParams
     * @方法描述: 通过参数查询授信批复分项数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtApprSub> queryLmtApproveSubByParams(HashMap<String,String> queryMaps){
        return lmtApprSubMapper.queryLmtApproveSubByParams(queryMaps);
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据授信分项流水号查询授信分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtApprSub selectBySubSerno(String subSerno) {
        return lmtApprSubMapper.selectBySubSerno(subSerno);
    }

    /**
     * @方法名称: selectBySubSerno
     * @方法描述: 根据授信分项流水号查询授信分项信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtApprSub> selectAllByApprSerno(String apprSerno) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("apprSerno", apprSerno);
        hashMap.put("oprTpye", CmisCommonConstants.OPR_TYPE_ADD);
        return this.queryLmtApproveSubByParams(hashMap);
    }


    /**
     * @方法名称: updateLmtApprSub
     * @方法描述: 根据前台传入表单数据更新授信分项数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-10 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Map updateLmtApprSub(LmtApprSub lmtApprSub) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try{

            // TODO 保存前校验 待补充

            //获取原授信分项
            LmtApprSub originLmtAppSub = lmtApprSubMapper.selectByPrimaryKey(lmtApprSub.getPkId());
            BigDecimal originLmtAmt = originLmtAppSub.getLmtAmt();
            //获取对应授信申请信息
            LmtAppr lmtAppr = lmtApprService.selectByApproveSerno(lmtApprSub.getApproveSerno());
            log.info("当前审批中的分项对应的审批申请数据:"+ JSON.toJSONString(lmtAppr));
            User userInfo = SessionUtils.getUserInformation();
            lmtApprSub.setUpdId(userInfo.getLoginCode());
            lmtApprSub.setUpdBrId(userInfo.getOrg().getCode());
            lmtApprSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtApprSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            log.info(String.format("更新授信分项信息:"+JSON.toJSONString(lmtApprSub)));
            int count = lmtApprSubMapper.updateByPrimaryKeySelective(lmtApprSub);
            List<LmtApprSub> apprSubList = lmtApprSubService.selectAllByApprSerno(lmtApprSub.getApproveSerno());
            log.info(String.format("所有授信分项信息:"+JSON.toJSONString(apprSubList)));
            BigDecimal subOpenAmt = BigDecimal.ZERO;
            BigDecimal subLowAmt = BigDecimal.ZERO;
            for (LmtApprSub apprSub : apprSubList) {
                if("60".equals(apprSub.getGuarMode())){
                    subLowAmt = subLowAmt.add(Optional.ofNullable(apprSub.getLmtAmt()).orElse(BigDecimal.ZERO));
                }else{
                    subOpenAmt = subOpenAmt.add(Optional.ofNullable(apprSub.getLmtAmt()).orElse(BigDecimal.ZERO));
                }
            }
            log.info("计算后的单一敞口[{}]，低风险[{}]",subOpenAmt,subLowAmt);
            lmtAppr.setOpenTotalLmtAmt(subOpenAmt);
            lmtAppr.setLowRiskTotalLmtAmt(subLowAmt);
            //更新对应授信申请数据
            int countApp = lmtApprService.update(lmtAppr);
            log.info(String.format("更新授信申请数据,流水号%s", lmtAppr.getSerno()));
            if (count != 1 || countApp != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",更新授信分项信息失败！");
            }
            // 集团成员的分项保存
            if(CmisCommonConstants.YES_NO_1.equals(lmtAppr.getIsGrp())){
                // 同步更新集团关系表数据以及集团主表
                LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtAppr.getApproveSerno());
                log.info("当前审批中的成员授信对应的关系表数据[{}]",JSON.toJSONString(lmtGrpMemRel));
                lmtGrpMemRel.setOpenLmtAmt(lmtAppr.getOpenTotalLmtAmt());
                lmtGrpMemRel.setLowRiskLmtAmt(lmtAppr.getLowRiskTotalLmtAmt());
                lmtGrpMemRelService.update(lmtGrpMemRel);
                HashMap paramsMap = new HashMap();
                paramsMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
                paramsMap.put("grpApproveSerno",lmtGrpMemRel.getGrpSerno());
                LmtGrpAppr lmtGrpAppr = new LmtGrpAppr();
                List<LmtGrpAppr> lmtGrpApprs = lmtGrpApprService.selectLmtGrpApprByParams(paramsMap);
                if (lmtGrpApprs != null && lmtGrpApprs.size() >= 1){
                    lmtGrpAppr = lmtGrpApprs.get(0);
                }
                log.info("当前审批中的集团授信数据[{}]",JSON.toJSONString(lmtGrpAppr));
                List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpAppr.getGrpApproveSerno());
                log.info("当前审批中的集团授信项下所有成员关系表数据[{}]",JSON.toJSONString(lmtGrpMemRels));
                BigDecimal openAmt = BigDecimal.ZERO;
                BigDecimal lowAmt = BigDecimal.ZERO;
                for(LmtGrpMemRel lmtGrpMemRel1 : lmtGrpMemRels){
                    openAmt = openAmt.add(Optional.ofNullable(lmtGrpMemRel1.getOpenLmtAmt()).orElse(BigDecimal.ZERO));
                    lowAmt = lowAmt.add(Optional.ofNullable(lmtGrpMemRel1.getLowRiskLmtAmt()).orElse(BigDecimal.ZERO));
                }
                log.info("计算后的集团敞口[{}]，低风险[{}]",openAmt,lowAmt);
                lmtGrpAppr.setOpenTotalLmtAmt(openAmt);
                lmtGrpAppr.setLowRiskTotalLmtAmt(lowAmt);
                lmtGrpApprService.update(lmtGrpAppr);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新授信分项信息出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: checkPrdLmtDataRules
     * @方法描述: 校验分项与分项明细规则
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String checkPrdLmtDataRules(LmtApprSub lmtApprSub) {
        String rtnCode = EcbEnum.ECB010000.key;
        try{
            log.info("当前分项信息:"+ JSON.toJSONString(lmtApprSub));
            BigDecimal sumSubAmt = new BigDecimal(0);
            HashMap map = new HashMap();
            map.put("apprSubSerno",lmtApprSub.getApproveSubSerno());
            map.put("oprType",CmisCommonConstants.OP_TYPE_01);
            List<LmtApprSubPrd> lmtApprSubPrdList = lmtApprSubPrdService.selectLmtApprSubPrdByParams(map);
            log.info("当前分项下所有的品种信息:"+ JSON.toJSONString(lmtApprSubPrdList));
            for(LmtApprSubPrd lmtApprSubPrd1 : lmtApprSubPrdList){
                if(lmtApprSubPrd1.getLmtAmt() != null){
                    sumSubAmt = sumSubAmt.add(lmtApprSubPrd1.getLmtAmt());

                    if(lmtApprSub.getLmtAmt().compareTo(lmtApprSubPrd1.getLmtAmt())<0){
                        rtnCode = EcbEnum.ECB020051.key;
                        return rtnCode;
                    }
                }
            }
            log.info("当前分项下所有的品种额度之和:"+ sumSubAmt);
            if(lmtApprSub.getLmtAmt().compareTo(sumSubAmt) > 0){
                rtnCode = EcbEnum.ECB020050.key;
                return rtnCode;
            }
        }catch (Exception e){
            rtnCode = String.valueOf(e.hashCode());
            throw BizException.error(null,String.valueOf(e.hashCode()),e.getMessage());
        }
        return rtnCode;
    }

    /**
     * @param approveSubSerno
     * @函数名称:selectPrdListByApproveSubSerno
     * @函数描述:查询审批分项品种表数据
     * @参数与返回说明:
     * @算法描述:
     */

    public List<LmtApproveSubPrdDto> selectPrdListByApproveSubSerno(String approveSubSerno) {
        Map map = new HashMap();
        map.put("approveSubSerno", approveSubSerno);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtApproveSubPrdDto> list = lmtApprSubMapper.selectPrdListByApproveSubSerno(map);
        return list;
    }
}
