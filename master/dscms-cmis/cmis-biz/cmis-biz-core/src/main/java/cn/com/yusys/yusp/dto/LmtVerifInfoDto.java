package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtVerifInfo
 * @类描述: LMT_VERIF_INFO数据实体类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-15 21:44:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtVerifInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String surveyNo;
	
	/** 勘察原因 **/
	private String toinvResn;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo == null ? null : surveyNo.trim();
	}
	
    /**
     * @return SurveyNo
     */	
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param toinvResn
	 */
	public void setToinvResn(String toinvResn) {
		this.toinvResn = toinvResn == null ? null : toinvResn.trim();
	}
	
    /**
     * @return ToinvResn
     */	
	public String getToinvResn() {
		return this.toinvResn;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}