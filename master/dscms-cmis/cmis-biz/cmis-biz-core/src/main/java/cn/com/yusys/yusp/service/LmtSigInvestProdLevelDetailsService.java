/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisLmtConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSigInvestProdLevelDetails;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestProdLevelDetailsMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdLevelDetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 17:25:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestProdLevelDetailsService {

    @Autowired
    private LmtSigInvestProdLevelDetailsMapper lmtSigInvestProdLevelDetailsMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestProdLevelDetails selectByPrimaryKey(String pkId) {
        return lmtSigInvestProdLevelDetailsMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestProdLevelDetails> selectAll(QueryModel model) {
        List<LmtSigInvestProdLevelDetails> records = (List<LmtSigInvestProdLevelDetails>) lmtSigInvestProdLevelDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestProdLevelDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestProdLevelDetails> list = lmtSigInvestProdLevelDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestProdLevelDetails record) {
        return lmtSigInvestProdLevelDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestProdLevelDetails record) {
        return lmtSigInvestProdLevelDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestProdLevelDetails record) {
        return lmtSigInvestProdLevelDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestProdLevelDetails record) {
        return lmtSigInvestProdLevelDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestProdLevelDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestProdLevelDetailsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtSigInvestProdLevelDetailsMapper.updateByParams(delMap);
    }
}
