/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.session.compatible.dto.Obj;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwydRentIncomeDetail;
import cn.com.yusys.yusp.dto.JyxwydRentIncomeDto;
import cn.com.yusys.yusp.dto.RptSpdAnysJyxwydRentIncomeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwydRentIncome;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysJyxwydRentIncomeMapper;

import javax.management.Query;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 10:36:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysJyxwydRentIncomeService {

    @Autowired
    private RptSpdAnysJyxwydRentIncomeMapper rptSpdAnysJyxwydRentIncomeMapper;

    @Autowired
    private RptSpdAnysJyxwydRentIncomeDetailService rptSpdAnysJyxwydRentIncomeDetailService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysJyxwydRentIncome selectByPrimaryKey(String pkId) {
        return rptSpdAnysJyxwydRentIncomeMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptSpdAnysJyxwydRentIncome> selectAll(QueryModel model) {
        List<RptSpdAnysJyxwydRentIncome> records = (List<RptSpdAnysJyxwydRentIncome>) rptSpdAnysJyxwydRentIncomeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysJyxwydRentIncome> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysJyxwydRentIncome> list = rptSpdAnysJyxwydRentIncomeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysJyxwydRentIncome record) {
        return rptSpdAnysJyxwydRentIncomeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysJyxwydRentIncome record) {
        return rptSpdAnysJyxwydRentIncomeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysJyxwydRentIncome record) {
        return rptSpdAnysJyxwydRentIncomeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysJyxwydRentIncome record) {
        return rptSpdAnysJyxwydRentIncomeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptSpdAnysJyxwydRentIncomeMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysJyxwydRentIncomeMapper.deleteByIds(ids);
    }

    public RptSpdAnysJyxwydRentIncome selectByParams(Map map) {
        return rptSpdAnysJyxwydRentIncomeMapper.selectByParams(map);
    }

    public List<RptSpdAnysJyxwydRentIncomeDto> queryIncome(String serno) {
        List<RptSpdAnysJyxwydRentIncomeDto> result = new ArrayList<>();
        List<RptSpdAnysJyxwydRentIncome> rptSpdAnysJyxwydRentIncomes = rptSpdAnysJyxwydRentIncomeMapper.selectBySerno(serno);
        if (CollectionUtils.nonEmpty(rptSpdAnysJyxwydRentIncomes)) {
            for (RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome : rptSpdAnysJyxwydRentIncomes) {
                String pkId = rptSpdAnysJyxwydRentIncome.getPkId();
                List<RptSpdAnysJyxwydRentIncomeDetail> rptSpdAnysJyxwydRentIncomeDetails = rptSpdAnysJyxwydRentIncomeDetailService.selectByRentSerno(pkId);
                if (CollectionUtils.nonEmpty(rptSpdAnysJyxwydRentIncomeDetails)) {
                    for (RptSpdAnysJyxwydRentIncomeDetail rptSpdAnysJyxwydRentIncomeDetail : rptSpdAnysJyxwydRentIncomeDetails) {
                        RptSpdAnysJyxwydRentIncomeDto rptSpdAnysJyxwydRentIncomeDto = new RptSpdAnysJyxwydRentIncomeDto();
                        rptSpdAnysJyxwydRentIncomeDto.setAmt(rptSpdAnysJyxwydRentIncomeDetail.getAmt());
                        rptSpdAnysJyxwydRentIncomeDto.setYear(rptSpdAnysJyxwydRentIncomeDetail.getYear());
                        rptSpdAnysJyxwydRentIncomeDto.setLessee(rptSpdAnysJyxwydRentIncome.getLessee());
                        rptSpdAnysJyxwydRentIncomeDto.setTermOfExistingLeaseContract(rptSpdAnysJyxwydRentIncome.getTermOfExistingLeaseContract());
                        rptSpdAnysJyxwydRentIncomeDto.setPkId(rptSpdAnysJyxwydRentIncome.getPkId());
                        rptSpdAnysJyxwydRentIncomeDto.setSerno(serno);
                        result.add(rptSpdAnysJyxwydRentIncomeDto);
                    }
                }
            }
        }
        return result;
    }

    public int saveIncome(RptSpdAnysJyxwydRentIncomeDto rptSpdAnysJyxwydRentIncomeDto) {
        String lessee = rptSpdAnysJyxwydRentIncomeDto.getLessee();
        Map param = new HashMap();
        param.put("lessee", lessee);
        param.put("serno", rptSpdAnysJyxwydRentIncomeDto.getSerno());
        RptSpdAnysJyxwydRentIncome income = rptSpdAnysJyxwydRentIncomeMapper.selectByParams(param);
        if (Objects.nonNull(income)) {
            String year = rptSpdAnysJyxwydRentIncomeDto.getYear();
            Map map = new HashMap();
            map.put("rentSerno", income.getPkId());
            map.put("year", year);
            RptSpdAnysJyxwydRentIncomeDetail rptSpdAnysJyxwydRentIncomeDetail = rptSpdAnysJyxwydRentIncomeDetailService.selectByParams(map);
            if (Objects.nonNull(rptSpdAnysJyxwydRentIncomeDetail)) {
                rptSpdAnysJyxwydRentIncomeDetail.setAmt(rptSpdAnysJyxwydRentIncomeDto.getAmt());
                return rptSpdAnysJyxwydRentIncomeDetailService.updateSelective(rptSpdAnysJyxwydRentIncomeDetail);
            } else {
                RptSpdAnysJyxwydRentIncomeDetail temp = new RptSpdAnysJyxwydRentIncomeDetail();
                temp.setPkId(StringUtils.getUUID());
                temp.setAmt(rptSpdAnysJyxwydRentIncomeDto.getAmt());
                temp.setYear(rptSpdAnysJyxwydRentIncomeDto.getYear());
                temp.setSerno(rptSpdAnysJyxwydRentIncomeDto.getSerno());
                temp.setRentSerno(income.getPkId());
                return rptSpdAnysJyxwydRentIncomeDetailService.insert(temp);
            }
        } else {
            RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome = new RptSpdAnysJyxwydRentIncome();
            rptSpdAnysJyxwydRentIncome.setPkId(StringUtils.getUUID());
            rptSpdAnysJyxwydRentIncome.setLessee(lessee);
            rptSpdAnysJyxwydRentIncome.setSerno(rptSpdAnysJyxwydRentIncomeDto.getSerno());
            rptSpdAnysJyxwydRentIncome.setTermOfExistingLeaseContract(rptSpdAnysJyxwydRentIncomeDto.getTermOfExistingLeaseContract());
            rptSpdAnysJyxwydRentIncomeMapper.insert(rptSpdAnysJyxwydRentIncome);
            RptSpdAnysJyxwydRentIncomeDetail detail = new RptSpdAnysJyxwydRentIncomeDetail();
            detail.setPkId(StringUtils.getUUID());
            detail.setSerno(rptSpdAnysJyxwydRentIncomeDto.getSerno());
            detail.setRentSerno(rptSpdAnysJyxwydRentIncome.getPkId());
            detail.setYear(rptSpdAnysJyxwydRentIncomeDto.getYear());
            detail.setAmt(rptSpdAnysJyxwydRentIncomeDto.getAmt());
            return rptSpdAnysJyxwydRentIncomeDetailService.insert(detail);
        }
    }

    public int deleteIncome(RptSpdAnysJyxwydRentIncomeDto rptSpdAnysJyxwydRentIncomeDto){
        String pkId = rptSpdAnysJyxwydRentIncomeDto.getPkId();
        RptSpdAnysJyxwydRentIncome rptSpdAnysJyxwydRentIncome = selectByPrimaryKey(pkId);
        Map map = new HashMap();
        map.put("rentSerno", pkId);
        map.put("year", rptSpdAnysJyxwydRentIncomeDto.getYear());
        RptSpdAnysJyxwydRentIncomeDetail rptSpdAnysJyxwydRentIncomeDetail = rptSpdAnysJyxwydRentIncomeDetailService.selectByParams(map);
        if(Objects.nonNull(rptSpdAnysJyxwydRentIncomeDetail)){
           int count =  rptSpdAnysJyxwydRentIncomeDetailService.deleteByPrimaryKey(rptSpdAnysJyxwydRentIncomeDetail.getPkId(),rptSpdAnysJyxwydRentIncomeDetail.getSerno());
            List<RptSpdAnysJyxwydRentIncomeDetail> rptSpdAnysJyxwydRentIncomeDetails = rptSpdAnysJyxwydRentIncomeDetailService.selectByRentSerno(pkId);
            if(CollectionUtils.isEmpty(rptSpdAnysJyxwydRentIncomeDetails)){
                deleteByPrimaryKey(pkId);
            }
            return count ;
        }
        return 0;
    }
}
