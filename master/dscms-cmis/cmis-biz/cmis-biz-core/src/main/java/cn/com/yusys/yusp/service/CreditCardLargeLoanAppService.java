/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CreditcradShisuanDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.CreditCardLargeLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credxx.CredxxService;
import cn.com.yusys.yusp.util.CmisBizCiis2ndUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.CreditCardLargeLoanAppMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardLargeLoanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-08-18 20:58:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardLargeLoanAppService {

    private static final Logger log = LoggerFactory.getLogger(CreditCardLargeLoanAppService.class);

    @Autowired
    private CreditCardLargeLoanAppMapper CreditCardLargeLoanAppMapper;
    @Autowired
    private CreditCardLargeLoanAppService CreditCardLargeLoanAppService;
    @Autowired
    private CreditCardLargeJudgInifoService creditCardLargeJudgInifoService;
    @Autowired
    private CreditCtrLoanContService creditCtrLoanContService;
    @Autowired
    private CreditCardLargeAmtInfoService creditCardLargeAmtInfoService;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;
    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CredxxService credxxService;
    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;
    @Autowired
    private CreditQryBizRealService creditQryBizRealService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Value("${application.creditUrl.url}")
    private String creditUrl;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CreditCardLargeLoanApp selectByPrimaryKey(String serno) {
        return CreditCardLargeLoanAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CreditCardLargeLoanApp> selectAll(QueryModel model) {
        List<CreditCardLargeLoanApp> records = (List<CreditCardLargeLoanApp>) CreditCardLargeLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardLargeLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModelStatus
     * @方法描述: 条件查询 - 查询条件为待处理的数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardLargeLoanApp> selectByModelStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppMapper.selectByModelStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByNotStatus
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CreditCardLargeLoanApp> selectByNotStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppMapper.selectByNotStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CreditCardLargeLoanApp record) {
        return CreditCardLargeLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int save(CreditCardLargeLoanApp record) {
        //更新审批中流程参数
        log.info("更新审批中流程参数开始:"+record.getSerno());
        List<CreditCardLargeJudgInifo> list = creditCardLargeJudgInifoService.selectBySerno(record.getSerno());
        if(list != null){
            if(list.size()>0){
                for(int i = 0;i<list.size();i++){
                    CreditCardLargeJudgInifo creditCardLargeJudgInifo = list.get(i);
                    creditCardLargeJudgInifo.setApproveAmt(record.getLoanAmount());
                    creditCardLargeJudgInifo.setLoanTerm(record.getLoanTerm());
                    creditCardLargeJudgInifo.setLoanFeeRate(record.getLoanFeeRate());
                    creditCardLargeJudgInifo.setYearInterestRate(record.getYearInterestRate());
                    creditCardLargeJudgInifoService.updateSelective(creditCardLargeJudgInifo);
                }
            }
        }
        log.info("更新审批中流程参数结束:"+record.getSerno());
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(record.getSerno());
        if (CreditCardLargeLoanApp != null) {
            return CreditCardLargeLoanAppMapper.updateByPrimaryKeySelective(record);
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            record.setAppDate(simpleDateFormat.format(new Date()));
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setOprType("01");
            record.setAppChnl("02");
            return CreditCardLargeLoanAppMapper.insert(record);
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CreditCardLargeLoanApp record) {
        return CreditCardLargeLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CreditCardLargeLoanApp record) {
        return CreditCardLargeLoanAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CreditCardLargeLoanApp record) {
        return CreditCardLargeLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return CreditCardLargeLoanAppMapper.deleteByPrimaryKey(serno);

    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int cutByPrimaryKey(String serno) {
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(serno);
        if (CreditCardLargeLoanApp != null) {
            //如果需要删除的额度申请状态不是待处理，不可以删除
            if (!CmisCommonConstants.WF_STATUS_000.equals(CreditCardLargeLoanApp.getApproveStatus())
                    && !CmisCommonConstants.WF_STATUS_992.equals(CreditCardLargeLoanApp.getApproveStatus())) {
                throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, "该大额分期申请不可删除！");
            } else if (CmisCommonConstants.WF_STATUS_992.equals(CreditCardLargeLoanApp.getApproveStatus())) {
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId："+serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
                return CreditCardLargeLoanAppMapper.changeApproveStatusAndOprType(serno, CmisCommonConstants.WF_STATUS_996, "02");
            } else {
                //流程删除 修改为自行退出
                log.info("流程删除==》bizId：",serno);
                // 删除流程实例
                workflowCoreClient.deleteByBizId(serno);
                return CreditCardLargeLoanAppMapper.deleteByPrimaryKey(serno);
            }
        } else {
            throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, "未找到该大额分期申请！");
        }
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return CreditCardLargeLoanAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<CreditcradShisuanDto> shisuan(CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        D13160ReqDto reqDto = new D13160ReqDto();
        reqDto.setCardno(CreditCardLargeLoanApps.getCardNo()); //卡号
        reqDto.setCrcycd("156"); //币种
        reqDto.setLoannt(CreditCardLargeLoanApps.getLoanAmount()); //分期金额
        reqDto.setLoanterm(CreditCardLargeLoanApps.getLoanTerm().toString()); //分期期数
        reqDto.setLnfemd(CreditCardLargeLoanApps.getLoanFeeMethod()); //分期手续费收取方式
        reqDto.setLoanmethod(CreditCardLargeLoanApps.getLoanPrinDistMethod()); //分期本金分配方式
        reqDto.setLothod(CreditCardLargeLoanApps.getLoanFeeCalcMethod()); //分期手续费计算方式
        reqDto.setLoanrate(CreditCardLargeLoanApps.getLoanFeeRate().toString()); //分期手续费比例
        reqDto.setDbkact(CreditCardLargeLoanApps.getDdBankAccNo());
        reqDto.setLoanind("Y");
        reqDto.setOptcod("0");
        //调用通联接口进行试算
        ResultDto<D13160RespDto> d13160RespDtoResultDto = dscms2TonglianClientService.d13160(reqDto);
        if (!"0".equals(d13160RespDtoResultDto.getCode())) {
            throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, d13160RespDtoResultDto.getMessage());
        }
        CreditcradShisuanDto creditcradShisuanDto = new CreditcradShisuanDto();
        D13160RespDto d13160RespDto = d13160RespDtoResultDto.getData();
        creditcradShisuanDto.setLnittm(d13160RespDto.getLnittm()); //分期总期数
        creditcradShisuanDto.setLnitpn(d13160RespDto.getLnitpn()); //分期总本金
        creditcradShisuanDto.setLnfdpt(d13160RespDto.getLnfdpt()); //分期每期应还本金
        creditcradShisuanDto.setLnfttm(d13160RespDto.getLnfttm()); //分期首期应还本金
        creditcradShisuanDto.setLnflt2(d13160RespDto.getLnflt2()); //分期末期应还本金
        creditcradShisuanDto.setLnitfi(d13160RespDto.getLnitfi()); //分期总手续费
        creditcradShisuanDto.setLnfdfi(d13160RespDto.getLnfdfi()); //分期每期手续费
        creditcradShisuanDto.setLnftfi(d13160RespDto.getLnftfi()); //分期首期手续费
        creditcradShisuanDto.setLnfltm(d13160RespDto.getLnfltm()); //分期末期手续费
        creditcradShisuanDto.setAnrate(new BigDecimal(d13160RespDto.getAnrate()));//分期近似年化利率
        ResultDto<CreditcradShisuanDto> resultDto = new ResultDto<>();
        resultDto.setCode(d13160RespDtoResultDto.getCode());
        resultDto.setData(creditcradShisuanDto);
        resultDto.setMessage(d13160RespDtoResultDto.getMessage());
        return resultDto;
    }

    /**
     * @param CreditCardLargeLoanApps
     * @return
     * @author wzy
     * @date 2021/8/9 21:09
     * @version 1.0.0
     * @desc  流程中进行大额分期试算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<CreditcradShisuanDto> shisuanStep(CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        D13160ReqDto reqDto = new D13160ReqDto();
        reqDto.setCrcycd("156"); //币种
        reqDto.setLoannt(CreditCardLargeLoanApps.getLoanAmount()); //分期金额
        reqDto.setLoanterm(CreditCardLargeLoanApps.getLoanTerm().toString()); //分期期数
        reqDto.setLoanrate(CreditCardLargeLoanApps.getLoanFeeRate().toPlainString()); //分期手续费比例
        CreditCardLargeLoanApp record = CreditCardLargeLoanAppService.selectByPrimaryKey(CreditCardLargeLoanApps.getSerno());
        reqDto.setLnfemd(record.getLoanFeeMethod()); //分期手续费收取方式
        reqDto.setLoanmethod(record.getLoanPrinDistMethod()); //分期本金分配方式
        reqDto.setLothod(record.getLoanFeeCalcMethod()); //分期手续费计算方式
        reqDto.setCardno(record.getCardNo()); //卡号
        reqDto.setDbkact(record.getDdBankAccNo());
        reqDto.setLoanind("Y");
        reqDto.setOptcod("0");
        //调用通联接口进行试算
        ResultDto<D13160RespDto> d13160RespDtoResultDto = dscms2TonglianClientService.d13160(reqDto);
        if (!"0".equals(d13160RespDtoResultDto.getCode())) {
            throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, d13160RespDtoResultDto.getMessage());
        }
        CreditcradShisuanDto creditcradShisuanDto = new CreditcradShisuanDto();
        D13160RespDto d13160RespDto = d13160RespDtoResultDto.getData();
        creditcradShisuanDto.setLnittm(d13160RespDto.getLnittm()); //分期总期数
        creditcradShisuanDto.setLnitpn(d13160RespDto.getLnitpn()); //分期总本金
        creditcradShisuanDto.setLnfdpt(d13160RespDto.getLnfdpt()); //分期每期应还本金
        creditcradShisuanDto.setLnfttm(d13160RespDto.getLnfttm()); //分期首期应还本金
        creditcradShisuanDto.setLnflt2(d13160RespDto.getLnflt2()); //分期末期应还本金
        creditcradShisuanDto.setLnitfi(d13160RespDto.getLnitfi()); //分期总手续费
        creditcradShisuanDto.setLnfdfi(d13160RespDto.getLnfdfi()); //分期每期手续费
        creditcradShisuanDto.setLnftfi(d13160RespDto.getLnftfi()); //分期首期手续费
        creditcradShisuanDto.setLnfltm(d13160RespDto.getLnfltm()); //分期末期手续费
        creditcradShisuanDto.setAnrate(new BigDecimal(d13160RespDto.getAnrate()));//分期近似年化利率
        ResultDto<CreditcradShisuanDto> resultDto = new ResultDto<>();
        resultDto.setCode(d13160RespDtoResultDto.getCode());
        resultDto.setData(creditcradShisuanDto);
        resultDto.setMessage(d13160RespDtoResultDto.getMessage());
        return resultDto;
    }

    /**
     * @param serno
     * @return
     * @author zsm
     * @date 2021/5/26 9:57
     * @version 1.0.0
     * @desc 额度申请流程处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void handleBusinessDataAfterEnd(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取大额分期申请" + serno + "大额分期申请信息");
            CreditCardLargeLoanApp CreditCardLargeLoanApp = this.selectByPrimaryKey(serno);
            //查询最终审批信息
            CreditCardLargeJudgInifo  lastCreditCardLargeJudgInifo = creditCardLargeJudgInifoService.selectFinalInfo(serno);
            // TODO(“需要调用通联13160接口") ；
            D13160ReqDto reqDto = new D13160ReqDto();
            reqDto.setCardno(CreditCardLargeLoanApp.getCardNo()); //卡号
            reqDto.setCrcycd("156"); //币种
            reqDto.setLoannt(lastCreditCardLargeJudgInifo.getApproveAmt()); //分期金额
            reqDto.setLoanterm(lastCreditCardLargeJudgInifo.getLoanTerm().toString()); //分期期数
            reqDto.setLnfemd(CreditCardLargeLoanApp.getLoanFeeMethod()); //分期手续费收取方式
            reqDto.setLoanmethod(CreditCardLargeLoanApp.getLoanPrinDistMethod()); //分期本金分配方式
            reqDto.setLothod(CreditCardLargeLoanApp.getLoanFeeCalcMethod()); //分期手续费计算方式
            reqDto.setLoanrate(lastCreditCardLargeJudgInifo.getLoanFeeRate().toString()); //分期手续费比例
            reqDto.setDbkact(CreditCardLargeLoanApp.getDdBankAccNo());//分期放款账号
            reqDto.setLoanind("Y");
            reqDto.setOptcod("1");
            //调用通联接口进行申请
            ResultDto<D13160RespDto> d13160RespDtoResultDto = dscms2TonglianClientService.d13160(reqDto);
            if (d13160RespDtoResultDto.getData() != null) {
                CreditCardLargeLoanApp.setRegisterId(d13160RespDtoResultDto.getData().getRgstid());
            }
            CreditCardLargeLoanApp.setLoanAmount(lastCreditCardLargeJudgInifo.getApproveAmt());
            CreditCardLargeLoanApp.setLoanTerm(lastCreditCardLargeJudgInifo.getLoanTerm());
            CreditCardLargeLoanApp.setLoanFeeRate(lastCreditCardLargeJudgInifo.getLoanFeeRate());
            int result = this.updateSelective(CreditCardLargeLoanApp);
            if (result < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            log.info("流程发起-新增大额分期申请额度表" + serno + "大额分期申请额度");
            //审批完成，调用额度创建接口
            handleBizEnd(serno);
            //新增大额分期申请-额度信息表
            CreditCardLargeAmtInfo creditCardLargeAmtInfo = new CreditCardLargeAmtInfo();
            creditCardLargeAmtInfo.setSerno(CreditCardLargeLoanApp.getSerno());
            creditCardLargeAmtInfo.setCardNo(CreditCardLargeLoanApp.getCardNo());
            creditCardLargeAmtInfo.setLoanTerm(CreditCardLargeLoanApp.getLoanTerm());
            creditCardLargeAmtInfo.setLoanFeeRate(CreditCardLargeLoanApp.getLoanFeeRate());
            creditCardLargeAmtInfo.setApproveAmt(lastCreditCardLargeJudgInifo.getApproveAmt());
            creditCardLargeAmtInfoService.insert(creditCardLargeAmtInfo);
            //新增大额分期合同信息表
            CreditCtrLoanCont creditCtrLoanCont = new CreditCtrLoanCont();
            String htSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, new HashMap<>());
            creditCtrLoanCont.setContNo(htSerno);
            creditCtrLoanCont.setSerno(CreditCardLargeLoanApp.getSerno());//流水号
            creditCtrLoanCont.setCusName(CreditCardLargeLoanApp.getCusName());//客户姓名
            creditCtrLoanCont.setCertType(CreditCardLargeLoanApp.getCertType());//证件类型
            creditCtrLoanCont.setCertCode(CreditCardLargeLoanApp.getCertCode());//证件号码
            creditCtrLoanCont.setCardNo(CreditCardLargeLoanApp.getCardNo());//卡号
            creditCtrLoanCont.setLoanPlan(CreditCardLargeLoanApp.getLoanPlan());//分期计划
            creditCtrLoanCont.setLoanAmount(lastCreditCardLargeJudgInifo.getApproveAmt());//分期金额
            creditCtrLoanCont.setLoanTerm(lastCreditCardLargeJudgInifo.getLoanTerm());//分期期数
            creditCtrLoanCont.setSendMode(CreditCardLargeLoanApp.getSendMode());//放款方式
            creditCtrLoanCont.setGuarMode(CreditCardLargeLoanApp.getGuarMode());//担保方式
            creditCtrLoanCont.setLoanFeeMethod(CreditCardLargeLoanApp.getLoanFeeMethod());//分期手续费收取方式
            creditCtrLoanCont.setLoanPrinDistMethod(CreditCardLargeLoanApp.getLoanPrinDistMethod());//分期本金分配方式
            creditCtrLoanCont.setLoanFeeCalcMethod(CreditCardLargeLoanApp.getLoanFeeCalcMethod());//分期手续费计算方式
            creditCtrLoanCont.setLoanFeeRate(lastCreditCardLargeJudgInifo.getLoanFeeRate());//分期手续费比例
            creditCtrLoanCont.setLoanrTarget(CreditCardLargeLoanApp.getLoanrTarget());//分期放款账户对公/对私标识
            creditCtrLoanCont.setDdBankBranch(CreditCardLargeLoanApp.getDdBankBranch());//分期放款开户行号
            creditCtrLoanCont.setDdBankName(CreditCardLargeLoanApp.getDdBankName());//分期放款银行名称1
            creditCtrLoanCont.setDdBankAccNo(CreditCardLargeLoanApp.getDdBankAccNo());//分期放款账号
            creditCtrLoanCont.setDdBankAccName(CreditCardLargeLoanApp.getDdBankAccName());//分期放款账户姓名
            creditCtrLoanCont.setDisbAcctPhone(CreditCardLargeLoanApp.getDisbAcctPhone());//放款账户移动电话
            creditCtrLoanCont.setDisbAcctCertType(CreditCardLargeLoanApp.getDisbAcctCertType());//放款账户证件类型
            creditCtrLoanCont.setDisbAcctCertCode(CreditCardLargeLoanApp.getDisbAcctCertCode());//放款账户证件号码
            creditCtrLoanCont.setPaymentPurpose(CreditCardLargeLoanApp.getPaymentPurpose());//资金用途
            creditCtrLoanCont.setYearInterestRate(lastCreditCardLargeJudgInifo.getYearInterestRate());//分期折算近似年化利率
            creditCtrLoanCont.setSalesManNo(CreditCardLargeLoanApp.getSalesManNo());//分期营销客户经理号
            creditCtrLoanCont.setSalesMan(CreditCardLargeLoanApp.getSalesMan());//分期营销人员姓名
            creditCtrLoanCont.setSalesManPhone(CreditCardLargeLoanApp.getSalesManPhone());//分期营销人员手机号
            creditCtrLoanCont.setSalesManOwingBranch(CreditCardLargeLoanApp.getSalesManOwingBranch());//分期营销人员所属支行
            creditCtrLoanCont.setContSignType("线下");//合同签订方式
            creditCtrLoanCont.setContStatus("100");//合同状态 未生效
            creditCtrLoanCont.setApproveStatus("000");//审批状态 审批中
            creditCtrLoanCont.setInputId(CreditCardLargeLoanApp.getInputId());
            creditCtrLoanCont.setInputBrId(CreditCardLargeLoanApp.getInputBrId());
            creditCtrLoanCont.setRecomId(CreditCardLargeLoanApp.getRecomId());
            creditCtrLoanCont.setRecomName(CreditCardLargeLoanApp.getRecomName());
            creditCtrLoanCont.setManagerBrId(CreditCardLargeLoanApp.getManagerBrId());
            creditCtrLoanCont.setManagerId(CreditCardLargeLoanApp.getManagerId());
            creditCtrLoanContService.insert(creditCtrLoanCont);
            log.info("流程发起-更新大额分期申请" + serno + "流程审批状态为【997】-已通过");
            CreditCardLargeLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            CreditCardLargeLoanAppService.updateSelective(CreditCardLargeLoanApp);
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * @param serno
     * @return
     * @author wzy
     * @date 2021/7/8 10:31
     * @version 1.0.0
     * @desc 审批完成，调用额度创建接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBizEnd(String serno) throws Exception {
        CreditCardLargeLoanApp CreditCardLargeLoanApp = this.selectByPrimaryKey(serno);
        log.info("大额分期业务申请审批结束后业务处理逻辑开始【{}】", CreditCardLargeLoanApp.getSerno());
        try {
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            cmisLmt0001ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            cmisLmt0001ReqDto.setAccNo(CreditCardLargeLoanApp.getSerno());
            cmisLmt0001ReqDto.setCurType("CNY");
            cmisLmt0001ReqDto.setCusId(CreditCardLargeLoanApp.getCusId());
            cmisLmt0001ReqDto.setCusName(CreditCardLargeLoanApp.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("0");
            cmisLmt0001ReqDto.setLmtAmt(CreditCardLargeLoanApp.getLoanAmount());
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
            cmisLmt0001ReqDto.setTerm(Integer.parseInt(CreditCardLargeLoanApp.getLoanTerm()));
            cmisLmt0001ReqDto.setStartDate(nowDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date futureDay = DateUtils.addMonth(simpleDateFormat.parse(nowDate),Integer.parseInt(CreditCardLargeLoanApp.getLoanTerm()));
            String lastDay = DateUtils.formatDate(futureDay, "yyyy-MM-dd");
            cmisLmt0001ReqDto.setEndDate(lastDay);
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001ReqDto.setManagerId(CreditCardLargeLoanApp.getInputId());
            cmisLmt0001ReqDto.setManagerBrId(CreditCardLargeLoanApp.getInputBrId());
            cmisLmt0001ReqDto.setInputId(CreditCardLargeLoanApp.getInputId());
            cmisLmt0001ReqDto.setInputBrId(CreditCardLargeLoanApp.getInputBrId());
            cmisLmt0001ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<CmisLmt0001LmtSubListReqDto>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(CreditCardLargeLoanApp.getSerno());
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo("");
            cmisLmt0001LmtSubListReqDto.setParentId("");
            cmisLmt0001LmtSubListReqDto.setLimitSubNo("22010201");
            cmisLmt0001LmtSubListReqDto.setLimitSubName("大额分期");
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt());
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType());
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate());
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate());
            cmisLmt0001LmtSubListReqDto.setBailPreRate(new BigDecimal(0));
            cmisLmt0001LmtSubListReqDto.setRateYear(CreditCardLargeLoanApp.getYearInterestRate());
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay("");
            cmisLmt0001LmtSubListReqDto.setLmtGraper(0);
            cmisLmt0001LmtSubListReqDto.setIsRevolv("0");
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0");
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0");
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);
            // 调用额度接口
            log.info("根据业务申请编号【" + CreditCardLargeLoanApp.getSerno() + "】前往额度系统-建立额度请求报文：" + cmisLmt0001ReqDto.toString());
            ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
            log.info("根据业务申请编号【" + CreditCardLargeLoanApp.getSerno() + "】前往额度系统-建立额度返回报文：" + cmisLmt0001RespDto.toString());

            if (cmisLmt0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0001RespDto.getData().getErrorCode())) {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度成功！", CreditCardLargeLoanApp.getSerno());
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度失败！", CreditCardLargeLoanApp.getSerno());
                throw new Exception("额度系统-前往额度系统建立额度失败:" + cmisLmt0001RespDto.getData().getErrorMsg());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
        log.info("零售按揭类业务申请审批结束后业务处理逻辑结束【{}】", CreditCardLargeLoanApp.getSerno());
    }

    /**
     * @param CreditCardLargeLoanApp
     * @return
     * @author zsm
     * @date 2021/5/26 9:57
     * @version 1.0.0
     * @desc 额度申请流程处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void checkNameAndStatus(CreditCardLargeLoanApp CreditCardLargeLoanApp) {
        if (CreditCardLargeLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        log.info("流程发起-校验大额分期申请{}", CreditCardLargeLoanApp.getSerno());
        D14020ReqDto reqDto = new D14020ReqDto();
        reqDto.setCardno(CreditCardLargeLoanApp.getCardNo());
        ResultDto<D14020RespDto> repDto = dscms2TonglianClientService.d14020(reqDto);
        if (!CreditCardLargeLoanApp.getCusName().equals(repDto.getData().getCdhdnm())) {
            throw BizException.error(null, EcbEnum.E_IQP_DELETE_FAILED.key, "获取的账号名不一致，请确认");
        }
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/6/26 16:42
     * @version 1.0.0
     * @desc 大额分期申请征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int getCreditReportInfo(CreditCardLargeLoanApp CreditCardLargeLoanApp) {
        try {
//            if (StringUtils.isBlank(serno)) {
//                throw BizException.error(null,EcbEnum.E_IQP_PARAMS_EXCEPTION.key,EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
//            }
//            log.info("查询信用卡信息" + serno + "申请主表信息");
//            CreditCardLargeLoanApp CreditCardLargeLoanApp = this.selectByPrimaryKey(serno);
            if (CreditCardLargeLoanApp == null) {
                throw BizException.error(null, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            String serno = CreditCardLargeLoanApp.getSerno();
            int updateCount = 0;
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(CreditCardLargeLoanApp.getInputId());
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            CredxxReqDto credxxReqDto = new CredxxReqDto();
            credxxReqDto.setCreditType("0");//报告类型 0 个人 1 企业
            credxxReqDto.setCustomName(CreditCardLargeLoanApp.getCusName());//客户名称
            credxxReqDto.setCertificateType(toConverCurrency(CreditCardLargeLoanApp.getCertType()));//证件类型
            credxxReqDto.setCertificateNum(CreditCardLargeLoanApp.getCertCode());//证件号
            credxxReqDto.setQueryReason("03");//查询原因 信用卡申请
            credxxReqDto.setCreateUserIdCard(adminSmUserDto.getCertNo());//客户经理身份证号
            credxxReqDto.setCreateUserName(adminSmUserDto.getUserName());//客户经理名称
            credxxReqDto.setCreateUserId(CreditCardLargeLoanApp.getInputId());//客户经理工号
            credxxReqDto.setReportType("H");//信用报告返回格式H ：html X:xml J:json
            //1、若该值为负数,则查询该值绝对值内的本地报告，不查询征信中心;
            //2、若该值为0，则强制查询征信中心；
            //3、若该值为正数，则查询该值内的本地报告，本地无报告则查询征信中心
            credxxReqDto.setQueryType("30");//信用报告复用策略
            //记录发生查询请求的具体业务线，业务线需要在前台进行数据字典维护。
            // 00~09为系统保留，不要使用。10~99为对接系统使用。
            credxxReqDto.setBusinessLine("101");//产品业务线
            credxxReqDto.setSysCode("1");//系统来源
            //总行：朱真尧 东海：邵雯
            if (CreditCardLargeLoanApp.getInputBrId().startsWith("81")) {
                credxxReqDto.setApprovalName("邵雯");//审批人姓名
                credxxReqDto.setApprovalIdCard("32072219960820002X");//审批人身份证号
            } else {
                credxxReqDto.setApprovalName("朱真尧");//审批人姓名
                credxxReqDto.setApprovalIdCard("320582199401122613");//审批人身份证号
            }
            //取系统日期
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            //生成征信流水号
            String crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
            credxxReqDto.setArchiveCreateDate(CreditCardLargeLoanApp.getCreditAuthDate());//授权书签订日期
            credxxReqDto.setCreditDocId(crqlSerno);//授权影像编号
            //授权结束日期=授权日期+30天
            String expireDate = DateUtils.addDay(CreditCardLargeLoanApp.getCreditAuthDate(), "yyyy-MM-dd", 30);
            credxxReqDto.setArchiveExpireDate(expireDate);//授权结束日期
            credxxReqDto.setBorrowPersonRelationName(CreditCardLargeLoanApp.getCusName());//主借款人名称
            credxxReqDto.setBorrowPersonRelationNumber(CreditCardLargeLoanApp.getCertCode());//主借款人证件号
            credxxReqDto.setAuditReason("002;004");//授权书内容
            credxxReqDto.setBrchno(CreditCardLargeLoanApp.getInputBrId());
            credxxReqDto.setBorrowPersonRelation("001");
            log.info("发送征信接口" + serno);
            CredxxRespDto credxxRespDto = credxxService.credxx(credxxReqDto);
            String zxSerno = credxxRespDto.getReportId();
            CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
            String authbookNoSeq = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.AUTHBOOK_NO_SEQ, new HashMap<>());
            creditReportQryLst.setAuthbookNo(authbookNoSeq);
            creditReportQryLst.setAuthbookDate(CreditCardLargeLoanApp.getCreditAuthDate());
            creditReportQryLst.setCrqlSerno(crqlSerno);
            creditReportQryLst.setReportNo(zxSerno);
            creditReportQryLst.setBorrowRel("001");
            creditReportQryLst.setQryCls("0");
            creditReportQryLst.setApproveStatus("997");
            creditReportQryLst.setQryFlag("04");
            if (!StringUtils.isEmpty(zxSerno)) {
                creditReportQryLst.setImageNo(crqlSerno);
                creditReportQryLst.setIsSuccssInit("1");
                creditReportQryLst.setReportCreateTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                creditReportQryLst.setSendTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                creditReportQryLst.setQryStatus("003");
                CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
                callCiis2ndReqDto.setPrefixUrl(creditUrl);
                callCiis2ndReqDto.setReqId(credxxRespDto.getReqId());
                callCiis2ndReqDto.setOrgName("信贷");
                callCiis2ndReqDto.setUserCode(CreditCardLargeLoanApp.getInputId());
                callCiis2ndReqDto.setUserName(adminSmUserDto.getUserName());
                callCiis2ndReqDto.setReportType(DscmsBizZxEnum.REPORT_TYPE_PER.key);
                String url = CmisBizCiis2ndUtils.callciis2nd(callCiis2ndReqDto);
                creditReportQryLst.setCreditUrl(url);
                creditReportQryLst.setCertType(CreditCardLargeLoanApp.getCertType());
                creditReportQryLst.setCertCode(CreditCardLargeLoanApp.getCertCode());
                creditReportQryLst.setCusId(CreditCardLargeLoanApp.getCusId());
                creditReportQryLst.setBorrowerCusName(CreditCardLargeLoanApp.getCusName());
                creditReportQryLst.setBorrowerCertCode(CreditCardLargeLoanApp.getCertCode());
                creditReportQryLst.setCusName(CreditCardLargeLoanApp.getCusName());
                creditReportQryLst.setManagerId(CreditCardLargeLoanApp.getInputId());
                creditReportQryLst.setManagerBrId(CreditCardLargeLoanApp.getInputBrId());
                creditReportQryLst.setQryResn("03");//征信查询原因 信用卡申请
                creditReportQryLst.setAuthbookContent("002;007");//授权书内容
                //生成业务与征信关系表
                CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
                creditQryBizReal.setBizSerno(serno);
                creditQryBizReal.setCrqlSerno(crqlSerno);
                creditQryBizReal.setCertType(CreditCardLargeLoanApp.getCertType());
                creditQryBizReal.setCertCode(CreditCardLargeLoanApp.getCertCode());
                creditQryBizReal.setCusId("");
                creditQryBizReal.setCusName(CreditCardLargeLoanApp.getCusName());
                creditQryBizReal.setBorrowRel("001");
                creditQryBizReal.setScene("01");
                int result = creditQryBizRealService.insertSelective(creditQryBizReal);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成征信关联表数据异常！");
                }
                //生产征信报告
                result = creditReportQryLstMapper.insertSelective(creditReportQryLst);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                CreditAuthbookInfo creditAuthbookInfo = new CreditAuthbookInfo();
                org.springframework.beans.BeanUtils.copyProperties(creditReportQryLst, creditAuthbookInfo);
                creditAuthbookInfo.setOtherAuthbookContent(creditAuthbookInfo.getAuthbookContent());
                creditAuthbookInfo.setAuthMode("02");
                creditAuthbookInfo.setAuthbookContent("002;007");
                result = creditAuthbookInfoService.insert(creditAuthbookInfo);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "生成生产征信报告数据异常！");
                }
                return result;
            } else {
                throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, "该客户为查询到征信信息！");
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("征信业务申请流程审批通过业务处理发生异常！", e.getMessage(), e);
            throw BizException.error(null, EcbEnum.E_IQP_HANDLE_EXCEPTION.key, e.getMessage());
        }
    }

    /*
     * 证件映射 信贷--->征信
     * @param xdbz 信贷证件码值
     * @return hxbz 征信证件码值
     */
    public static String toConverCurrency(String xdbz) {
        String hxbz = "";
        if ("C".equals(xdbz)) { //户口簿
            hxbz = "1";
        } else if ("B".equals(xdbz)) {//护照
            hxbz = "2";
        } else if ("D".equals(xdbz)) {//港澳居民来往内地通行证
            hxbz = "5";
        } else if ("E".equals(xdbz)) {//台湾同胞来往内地通行证
            hxbz = "6";
        } else if ("12".equals(xdbz)) { // 外国人居留证
            hxbz = "8";
        } else if ("Y".equals(xdbz)) {//警官证
            hxbz = "9";
        } else if ("13".equals(xdbz)) {//香港身份证
            hxbz = "A";
        } else if ("14".equals(xdbz)) {//澳门身份证
            hxbz = "B";
        } else if ("15".equals(xdbz)) {//台湾身份证
            hxbz = "C";
        } else if ("16".equals(xdbz)) {//其他证件
            hxbz = "X";
        } else if ("A".equals(xdbz)) {//居民身份证及其他以公民身份证号为标识的证件
            hxbz = "10";
        } else if ("11".equals(xdbz)) {//军人身份证件
            hxbz = "20";
        } else if ("06".equals(xdbz)) {//工商注册号
            hxbz = "01";
        } else if ("01".equals(xdbz)) {//机关和事业单位登记号
            hxbz = "02";
        } else if ("02".equals(xdbz)) {//社会团体登记号
            hxbz = "03";
        } else if ("03".equals(xdbz)) {//民办非企业登记号
            hxbz = "04";
        } else if ("04".equals(xdbz)) {//基金会登记号
            hxbz = "05";
        } else if ("05".equals(xdbz)) {//宗教证书登记号
            hxbz = "06";
        } else if ("P2".equals(xdbz)) {//中征码
            hxbz = "10";
        } else if ("R".equals(xdbz)) {//统一社会信用代码
            hxbz = "20";
        } else if ("Q".equals(xdbz)) {//组织机构代码
            hxbz = "30";
        } else if ("07".equals(xdbz)) {//纳税人识别号（国税）
            hxbz = "41";
        } else if ("08".equals(xdbz)) {//纳税人识别号（地税）
            hxbz = "42";
        } else {
            hxbz = xdbz;//未匹配到的证件类型
        }
        return hxbz;
    }
}
