package cn.com.yusys.yusp.service.server.xdxw0013;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0013.resp.List;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.web.server.xdxw0013.BizXdxw0013Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0013Service {

    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0013Resource.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易描述：优企贷共借人、合同信息查询
     *
     * @param commonCertNo
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public java.util.List<List> selectCtrLoanContsByCommonCertNo(String commonCertNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, commonCertNo);
        java.util.List<List> lists = null;
        try {
            lists = ctrLoanContMapper.selectCtrLoanContsByCommonCertNo(commonCertNo);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(lists));
        return lists;
    }
}
