/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyChg
 * @类描述: lmt_crd_reply_chg数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:22:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_crd_reply_chg")
public class LmtCrdReplyChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复编号 **/
	@Id
	@Column(name = "REPLY_NO")
	private String replyNo;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 产品代码 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 原批复额度 **/
	@Column(name = "OLD_REPLY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldReplyAmt;
	
	/** 原批复期限 **/
	@Column(name = "OLD_REPLY_TERM", unique = false, nullable = true, length = 10)
	private Integer oldReplyTerm;
	
	/** 原批复利率 **/
	@Column(name = "OLD_REPLY_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldReplyRate;
	
	/** 原还款方式 **/
	@Column(name = "OLD_REPAY_MODE", unique = false, nullable = true, length = 5)
	private String oldRepayMode;
	
	/** 原担保方式 **/
	@Column(name = "OLD_GUAR_MODE", unique = false, nullable = true, length = 5)
	private String oldGuarMode;
	
	/** 原用信条件 **/
	@Column(name = "OLD_LOAN_COND", unique = false, nullable = true, length = 2000)
	private String oldLoanCond;
	
	/** 原风控建议 **/
	@Column(name = "OLD_RISK_ADVICE", unique = false, nullable = true, length = 2000)
	private String oldRiskAdvice;
	
	/** 批复变更额度 **/
	@Column(name = "REPLY_AMT_CHG", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyAmtChg;
	
	/** 批复变更期限 **/
	@Column(name = "REPLY_TERM_CHG", unique = false, nullable = true, length = 10)
	private Integer replyTermChg;
	
	/** 批复变更利率 **/
	@Column(name = "REPLY_RATE_CHG", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal replyRateChg;
	
	/** 批复变更还款方式 **/
	@Column(name = "REPAY_MODE_CHG", unique = false, nullable = true, length = 5)
	private String repayModeChg;
	
	/** 批复变更担保方式 **/
	@Column(name = "GUAR_MODE_CHG", unique = false, nullable = true, length = 5)
	private String guarModeChg;
	
	/** 批复变更用信条件 **/
	@Column(name = "LOAN_COND_CHG", unique = false, nullable = true, length = 2000)
	private String loanCondChg;
	
	/** 批复变更风控建议 **/
	@Column(name = "RISK_ADVICE_CHG", unique = false, nullable = true, length = 2000)
	private String riskAdviceChg;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 备注 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 2000)
	private String memo;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param oldReplyAmt
	 */
	public void setOldReplyAmt(java.math.BigDecimal oldReplyAmt) {
		this.oldReplyAmt = oldReplyAmt;
	}
	
    /**
     * @return oldReplyAmt
     */
	public java.math.BigDecimal getOldReplyAmt() {
		return this.oldReplyAmt;
	}
	
	/**
	 * @param oldReplyTerm
	 */
	public void setOldReplyTerm(Integer oldReplyTerm) {
		this.oldReplyTerm = oldReplyTerm;
	}
	
    /**
     * @return oldReplyTerm
     */
	public Integer getOldReplyTerm() {
		return this.oldReplyTerm;
	}
	
	/**
	 * @param oldReplyRate
	 */
	public void setOldReplyRate(java.math.BigDecimal oldReplyRate) {
		this.oldReplyRate = oldReplyRate;
	}
	
    /**
     * @return oldReplyRate
     */
	public java.math.BigDecimal getOldReplyRate() {
		return this.oldReplyRate;
	}
	
	/**
	 * @param oldRepayMode
	 */
	public void setOldRepayMode(String oldRepayMode) {
		this.oldRepayMode = oldRepayMode;
	}
	
    /**
     * @return oldRepayMode
     */
	public String getOldRepayMode() {
		return this.oldRepayMode;
	}
	
	/**
	 * @param oldGuarMode
	 */
	public void setOldGuarMode(String oldGuarMode) {
		this.oldGuarMode = oldGuarMode;
	}
	
    /**
     * @return oldGuarMode
     */
	public String getOldGuarMode() {
		return this.oldGuarMode;
	}
	
	/**
	 * @param oldLoanCond
	 */
	public void setOldLoanCond(String oldLoanCond) {
		this.oldLoanCond = oldLoanCond;
	}
	
    /**
     * @return oldLoanCond
     */
	public String getOldLoanCond() {
		return this.oldLoanCond;
	}
	
	/**
	 * @param oldRiskAdvice
	 */
	public void setOldRiskAdvice(String oldRiskAdvice) {
		this.oldRiskAdvice = oldRiskAdvice;
	}
	
    /**
     * @return oldRiskAdvice
     */
	public String getOldRiskAdvice() {
		return this.oldRiskAdvice;
	}
	
	/**
	 * @param replyAmtChg
	 */
	public void setReplyAmtChg(java.math.BigDecimal replyAmtChg) {
		this.replyAmtChg = replyAmtChg;
	}
	
    /**
     * @return replyAmtChg
     */
	public java.math.BigDecimal getReplyAmtChg() {
		return this.replyAmtChg;
	}
	
	/**
	 * @param replyTermChg
	 */
	public void setReplyTermChg(Integer replyTermChg) {
		this.replyTermChg = replyTermChg;
	}
	
    /**
     * @return replyTermChg
     */
	public Integer getReplyTermChg() {
		return this.replyTermChg;
	}
	
	/**
	 * @param replyRateChg
	 */
	public void setReplyRateChg(java.math.BigDecimal replyRateChg) {
		this.replyRateChg = replyRateChg;
	}
	
    /**
     * @return replyRateChg
     */
	public java.math.BigDecimal getReplyRateChg() {
		return this.replyRateChg;
	}
	
	/**
	 * @param repayModeChg
	 */
	public void setRepayModeChg(String repayModeChg) {
		this.repayModeChg = repayModeChg;
	}
	
    /**
     * @return repayModeChg
     */
	public String getRepayModeChg() {
		return this.repayModeChg;
	}
	
	/**
	 * @param guarModeChg
	 */
	public void setGuarModeChg(String guarModeChg) {
		this.guarModeChg = guarModeChg;
	}
	
    /**
     * @return guarModeChg
     */
	public String getGuarModeChg() {
		return this.guarModeChg;
	}
	
	/**
	 * @param loanCondChg
	 */
	public void setLoanCondChg(String loanCondChg) {
		this.loanCondChg = loanCondChg;
	}
	
    /**
     * @return loanCondChg
     */
	public String getLoanCondChg() {
		return this.loanCondChg;
	}
	
	/**
	 * @param riskAdviceChg
	 */
	public void setRiskAdviceChg(String riskAdviceChg) {
		this.riskAdviceChg = riskAdviceChg;
	}
	
    /**
     * @return riskAdviceChg
     */
	public String getRiskAdviceChg() {
		return this.riskAdviceChg;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}