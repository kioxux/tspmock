package cn.com.yusys.yusp.service.client.bsp.rircp.fbxw04;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：惠享贷规则审批申请接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Fbxw04Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw04Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * 业务逻辑处理方法：惠享贷规则审批申请接口
     *
     * @param fbxw04ReqDto
     * @return
     */
    @Transactional
    public Fbxw04RespDto fbxw04(Fbxw04ReqDto fbxw04ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value, JSON.toJSONString(fbxw04ReqDto));
        ResultDto<Fbxw04RespDto> fbxw04ResultDto = dscms2RircpClientService.fbxw04(fbxw04ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value, JSON.toJSONString(fbxw04ResultDto));
        String fbxw04Code = Optional.ofNullable(fbxw04ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fbxw04Meesage = Optional.ofNullable(fbxw04ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fbxw04RespDto fbxw04RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fbxw04ResultDto.getCode())) {
            //  获取相关的值并解析
            fbxw04RespDto = fbxw04ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(fbxw04Code, fbxw04Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value);
        return fbxw04RespDto;
    }


}
