/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoAsso
 * @类描述: rpt_basic_info_asso数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:30:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_asso")
public class RptBasicInfoAsso extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;
	
	/** 企业性质 **/
	@Column(name = "CORP_CHA", unique = false, nullable = true, length = 5)
	private String corpCha;
	
	/** 主体评级 **/
	@Column(name = "SUBJECT_LEVEL", unique = false, nullable = true, length = 5)
	private String subjectLevel;
	
	/** 现五级分类 **/
	@Column(name = "CURRENT_FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String currentFiveClass;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 实收资本 **/
	@Column(name = "PAID_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCapAmt;
	
	/** 实际控制人客户号 **/
	@Column(name = "REAL_OPER_CUS_ID", unique = false, nullable = true, length = 40)
	private String realOperCusId;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 40)
	private String legal;
	
	/** 注册地址 **/
	@Column(name = "COM_REG_ADD", unique = false, nullable = true, length = 200)
	private String comRegAdd;
	
	/** 实际经营地址 **/
	@Column(name = "OPER_ADDR_ACT", unique = false, nullable = true, length = 200)
	private String operAddrAct;
	
	/** 经营范围 **/
	@Column(name = "NAT_BUSI", unique = false, nullable = true, length = 200)
	private String natBusi;
	
	/** 股东情况其他说明 **/
	@Column(name = "SHD_CASE_OTHER_MEMO", unique = false, nullable = true, length = 65535)
	private String shdCaseOtherMemo;
	
	/** 基本信息其他需说明的事项 **/
	@Column(name = "BASIC_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String basicOtherNeedDesc;
	
	/** 是否涉农 **/
	@Column(name = "AGR_IND", unique = false, nullable = true, length = 5)
	private String agrInd;
	
	/** 是否小微 **/
	@Column(name = "SMALL_IND", unique = false, nullable = true, length = 5)
	private String smallInd;
	
	/** 是否制造业 **/
	@Column(name = "MANU_IND", unique = false, nullable = true, length = 5)
	private String manuInd;
	
	/** 是否两高一剩 **/
	@Column(name = "TWO_HIGHT_ONE_LEFT_IND", unique = false, nullable = true, length = 5)
	private String twoHightOneLeftInd;
	
	/** 是否为我行规定的9类易燃易爆行业 **/
	@Column(name = "BURN_EXPLODE_INDUSTRY_IND", unique = false, nullable = true, length = 5)
	private String burnExplodeIndustryInd;
	
	/** 环保是否达标 **/
	@Column(name = "ENVIRON_STAND_IND", unique = false, nullable = true, length = 5)
	private String environStandInd;
	
	/** 公司历史沿革 **/
	@Column(name = "COMPANY_HISTORY", unique = false, nullable = true, length = 40)
	private String companyHistory;
	
	/** 目前主要业务概况、经营模式、盈利模式 **/
	@Column(name = "MAIN_BUSINESS", unique = false, nullable = true, length = 5)
	private String mainBusiness;
	
	/** 企业环评情况及安全生产情况 **/
	@Column(name = "CUS_EIA_SAFE_CASE", unique = false, nullable = true, length = 65535)
	private String cusEiaSafeCase;
	
	/** 房地产需说明的事项 **/
	@Column(name = "ESTATE_OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String estateOtherNeedDesc;
	
	/** 租用土地面积 **/
	@Column(name = "RENT_LAND_SQU", unique = false, nullable = true, length = 40)
	private String rentLandSqu;
	
	/** 租用厂房面积 **/
	@Column(name = "RENT_WORKSHOP_SQU", unique = false, nullable = true, length = 40)
	private String rentWorkshopSqu;
	
	/** 租用年租金 **/
	@Column(name = "RENT_AMT_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rentAmtYear;
	
	/** 自有土地面积 **/
	@Column(name = "SELF_LAND_SQU", unique = false, nullable = true, length = 40)
	private String selfLandSqu;
	
	/** 自有有证土地面积 **/
	@Column(name = "SELF_CERT_LAND_SQU", unique = false, nullable = true, length = 40)
	private String selfCertLandSqu;
	
	/** 自有有证土地性质 **/
	@Column(name = "SELF_CERT_LAND_CHA", unique = false, nullable = true, length = 40)
	private String selfCertLandCha;
	
	/** 自有房产面积 **/
	@Column(name = "SELF_HOUSE_SQU", unique = false, nullable = true, length = 40)
	private String selfHouseSqu;
	
	/** 自有有证房产面积 **/
	@Column(name = "SELF_CERT_HOUSE_SQU", unique = false, nullable = true, length = 40)
	private String selfCertHouseSqu;

	/** 关联企业其他说明事项 **/
	@Column(name = "ENTERPRISES_OTHER_DESC", unique = false, nullable = true, length = 65535)
	private String enterprisesOtherDesc;

	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param corpCha
	 */
	public void setCorpCha(String corpCha) {
		this.corpCha = corpCha;
	}
	
    /**
     * @return corpCha
     */
	public String getCorpCha() {
		return this.corpCha;
	}
	
	/**
	 * @param subjectLevel
	 */
	public void setSubjectLevel(String subjectLevel) {
		this.subjectLevel = subjectLevel;
	}
	
    /**
     * @return subjectLevel
     */
	public String getSubjectLevel() {
		return this.subjectLevel;
	}
	
	/**
	 * @param currentFiveClass
	 */
	public void setCurrentFiveClass(String currentFiveClass) {
		this.currentFiveClass = currentFiveClass;
	}
	
    /**
     * @return currentFiveClass
     */
	public String getCurrentFiveClass() {
		return this.currentFiveClass;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}
	
    /**
     * @return paidCapAmt
     */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}
	
	/**
	 * @param realOperCusId
	 */
	public void setRealOperCusId(String realOperCusId) {
		this.realOperCusId = realOperCusId;
	}
	
    /**
     * @return realOperCusId
     */
	public String getRealOperCusId() {
		return this.realOperCusId;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param comRegAdd
	 */
	public void setComRegAdd(String comRegAdd) {
		this.comRegAdd = comRegAdd;
	}
	
    /**
     * @return comRegAdd
     */
	public String getComRegAdd() {
		return this.comRegAdd;
	}
	
	/**
	 * @param operAddrAct
	 */
	public void setOperAddrAct(String operAddrAct) {
		this.operAddrAct = operAddrAct;
	}
	
    /**
     * @return operAddrAct
     */
	public String getOperAddrAct() {
		return this.operAddrAct;
	}
	
	/**
	 * @param natBusi
	 */
	public void setNatBusi(String natBusi) {
		this.natBusi = natBusi;
	}
	
    /**
     * @return natBusi
     */
	public String getNatBusi() {
		return this.natBusi;
	}
	
	/**
	 * @param shdCaseOtherMemo
	 */
	public void setShdCaseOtherMemo(String shdCaseOtherMemo) {
		this.shdCaseOtherMemo = shdCaseOtherMemo;
	}
	
    /**
     * @return shdCaseOtherMemo
     */
	public String getShdCaseOtherMemo() {
		return this.shdCaseOtherMemo;
	}
	
	/**
	 * @param basicOtherNeedDesc
	 */
	public void setBasicOtherNeedDesc(String basicOtherNeedDesc) {
		this.basicOtherNeedDesc = basicOtherNeedDesc;
	}
	
    /**
     * @return basicOtherNeedDesc
     */
	public String getBasicOtherNeedDesc() {
		return this.basicOtherNeedDesc;
	}
	
	/**
	 * @param agrInd
	 */
	public void setAgrInd(String agrInd) {
		this.agrInd = agrInd;
	}
	
    /**
     * @return agrInd
     */
	public String getAgrInd() {
		return this.agrInd;
	}
	
	/**
	 * @param smallInd
	 */
	public void setSmallInd(String smallInd) {
		this.smallInd = smallInd;
	}
	
    /**
     * @return smallInd
     */
	public String getSmallInd() {
		return this.smallInd;
	}
	
	/**
	 * @param manuInd
	 */
	public void setManuInd(String manuInd) {
		this.manuInd = manuInd;
	}
	
    /**
     * @return manuInd
     */
	public String getManuInd() {
		return this.manuInd;
	}
	
	/**
	 * @param twoHightOneLeftInd
	 */
	public void setTwoHightOneLeftInd(String twoHightOneLeftInd) {
		this.twoHightOneLeftInd = twoHightOneLeftInd;
	}
	
    /**
     * @return twoHightOneLeftInd
     */
	public String getTwoHightOneLeftInd() {
		return this.twoHightOneLeftInd;
	}
	
	/**
	 * @param burnExplodeIndustryInd
	 */
	public void setBurnExplodeIndustryInd(String burnExplodeIndustryInd) {
		this.burnExplodeIndustryInd = burnExplodeIndustryInd;
	}
	
    /**
     * @return burnExplodeIndustryInd
     */
	public String getBurnExplodeIndustryInd() {
		return this.burnExplodeIndustryInd;
	}
	
	/**
	 * @param environStandInd
	 */
	public void setEnvironStandInd(String environStandInd) {
		this.environStandInd = environStandInd;
	}
	
    /**
     * @return environStandInd
     */
	public String getEnvironStandInd() {
		return this.environStandInd;
	}
	
	/**
	 * @param companyHistory
	 */
	public void setCompanyHistory(String companyHistory) {
		this.companyHistory = companyHistory;
	}
	
    /**
     * @return companyHistory
     */
	public String getCompanyHistory() {
		return this.companyHistory;
	}
	
	/**
	 * @param mainBusiness
	 */
	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}
	
    /**
     * @return mainBusiness
     */
	public String getMainBusiness() {
		return this.mainBusiness;
	}
	
	/**
	 * @param cusEiaSafeCase
	 */
	public void setCusEiaSafeCase(String cusEiaSafeCase) {
		this.cusEiaSafeCase = cusEiaSafeCase;
	}
	
    /**
     * @return cusEiaSafeCase
     */
	public String getCusEiaSafeCase() {
		return this.cusEiaSafeCase;
	}
	
	/**
	 * @param estateOtherNeedDesc
	 */
	public void setEstateOtherNeedDesc(String estateOtherNeedDesc) {
		this.estateOtherNeedDesc = estateOtherNeedDesc;
	}
	
    /**
     * @return estateOtherNeedDesc
     */
	public String getEstateOtherNeedDesc() {
		return this.estateOtherNeedDesc;
	}
	
	/**
	 * @param rentLandSqu
	 */
	public void setRentLandSqu(String rentLandSqu) {
		this.rentLandSqu = rentLandSqu;
	}
	
    /**
     * @return rentLandSqu
     */
	public String getRentLandSqu() {
		return this.rentLandSqu;
	}
	
	/**
	 * @param rentWorkshopSqu
	 */
	public void setRentWorkshopSqu(String rentWorkshopSqu) {
		this.rentWorkshopSqu = rentWorkshopSqu;
	}
	
    /**
     * @return rentWorkshopSqu
     */
	public String getRentWorkshopSqu() {
		return this.rentWorkshopSqu;
	}
	
	/**
	 * @param rentAmtYear
	 */
	public void setRentAmtYear(java.math.BigDecimal rentAmtYear) {
		this.rentAmtYear = rentAmtYear;
	}
	
    /**
     * @return rentAmtYear
     */
	public java.math.BigDecimal getRentAmtYear() {
		return this.rentAmtYear;
	}
	
	/**
	 * @param selfLandSqu
	 */
	public void setSelfLandSqu(String selfLandSqu) {
		this.selfLandSqu = selfLandSqu;
	}
	
    /**
     * @return selfLandSqu
     */
	public String getSelfLandSqu() {
		return this.selfLandSqu;
	}
	
	/**
	 * @param selfCertLandSqu
	 */
	public void setSelfCertLandSqu(String selfCertLandSqu) {
		this.selfCertLandSqu = selfCertLandSqu;
	}
	
    /**
     * @return selfCertLandSqu
     */
	public String getSelfCertLandSqu() {
		return this.selfCertLandSqu;
	}
	
	/**
	 * @param selfCertLandCha
	 */
	public void setSelfCertLandCha(String selfCertLandCha) {
		this.selfCertLandCha = selfCertLandCha;
	}
	
    /**
     * @return selfCertLandCha
     */
	public String getSelfCertLandCha() {
		return this.selfCertLandCha;
	}
	
	/**
	 * @param selfHouseSqu
	 */
	public void setSelfHouseSqu(String selfHouseSqu) {
		this.selfHouseSqu = selfHouseSqu;
	}
	
    /**
     * @return selfHouseSqu
     */
	public String getSelfHouseSqu() {
		return this.selfHouseSqu;
	}
	
	/**
	 * @param selfCertHouseSqu
	 */
	public void setSelfCertHouseSqu(String selfCertHouseSqu) {
		this.selfCertHouseSqu = selfCertHouseSqu;
	}
	
    /**
     * @return selfCertHouseSqu
     */
	public String getSelfCertHouseSqu() {
		return this.selfCertHouseSqu;
	}

	public String getEnterprisesOtherDesc() {
		return enterprisesOtherDesc;
	}

	public void setEnterprisesOtherDesc(String enterprisesOtherDesc) {
		this.enterprisesOtherDesc = enterprisesOtherDesc;
	}
}