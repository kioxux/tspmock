package cn.com.yusys.yusp.web.server.xdtz0058;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0058.req.Xdtz0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.resp.Xdtz0058DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0058.Xdtz0058Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:台账信息通用列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0058:台账信息通用列表查询")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0058Resource.class);
    @Autowired
    private Xdtz0058Service xdtz0058Service;

    /**
     * 交易码：xdtz0058
     * 交易描述：台账信息通用列表查询
     *
     * @param xdtz0058DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账信息通用列表查询")
    @PostMapping("/xdtz0058")
    protected @ResponseBody
    ResultDto<Xdtz0058DataRespDto> xdtz0058(@Validated @RequestBody Xdtz0058DataReqDto xdtz0058DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataReqDto));
        Xdtz0058DataRespDto xdtz0058DataRespDto = new Xdtz0058DataRespDto();// 响应Dto:台账信息通用列表查询
        ResultDto<Xdtz0058DataRespDto> xdtz0058DataResultDto = new ResultDto<>();

        try {
            String queryType = xdtz0058DataReqDto.getQueryType();//查询类型
            String billNo = xdtz0058DataReqDto.getBillNo(); // 借据编号
            String certNo = xdtz0058DataReqDto.getCertNo();

            if ("queryLoanAmountFormAccLoan".equals(queryType) || "queryLoanMonthFormAccLoan".equals(queryType) || "queryFinaBrIdFromAccLoan".equals(queryType)) {
                if (StringUtil.isEmpty(billNo)) {
                    xdtz0058DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdtz0058DataResultDto.setMessage("借据号【billNo】不能为空！");
                    return xdtz0058DataResultDto;
                }
            } else if ("queryOverTimesTotalFromAccLoan".equals(queryType)) {
                if (StringUtil.isEmpty(certNo)) {
                    xdtz0058DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdtz0058DataResultDto.setMessage("证件号【certNo】不能为空！");
                    return xdtz0058DataResultDto;
                }
            } else if (StringUtil.isEmpty(queryType)) {
                xdtz0058DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0058DataResultDto.setMessage("查询类型【queryType】不能为空！");
                return xdtz0058DataResultDto;
            }

            // 从xdtz0058DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataReqDto));
            xdtz0058DataRespDto = xdtz0058Service.xdtz0058(xdtz0058DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataRespDto));
            // 封装xdtz0058DataResultDto中正确的返回码和返回信息
            xdtz0058DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0058DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
            // 封装xdtz0058DataResultDto中异常返回码和返回信息
            xdtz0058DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0058DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0058DataRespDto到xdtz0058DataResultDto中
        xdtz0058DataResultDto.setData(xdtz0058DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataResultDto));
        return xdtz0058DataResultDto;
    }
}
