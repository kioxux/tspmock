package cn.com.yusys.yusp.service.client.bsp.core.ln3246;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/21 10:33
 * @desc 根贷款还款计划明细查询
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ln3246Service {
    private static final Logger logger = LoggerFactory.getLogger(Ln3246Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param ln3246ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246RespDto
     * @author hubp
     * @date 2021/5/21 10:41
     * @version 1.0.0
     * @desc 根贷款还款计划明细查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public ResultDto<Ln3246RespDto> ln3246(Ln3246ReqDto ln3246ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(ln3246ReqDto));
        ResultDto<Ln3246RespDto> ln3246ResultDto = dscms2CoreLnClientService.ln3246(ln3246ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(ln3246ResultDto));
//        String ln3246Code = Optional.ofNullable(ln3246ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
//        String ln3246Meesage = Optional.ofNullable(ln3246ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
//        Ln3246RespDto ln3246RespDto = null;
//        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3246ResultDto.getCode())) {
//            //  获取相关的值并解析
//            ln3246RespDto = ln3246ResultDto.getData();
//        } else {
//            //  抛出错误异常
//
//            // throw new YuspException(ln3246Code, ln3246Meesage);
//        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value);
        return ln3246ResultDto;
    }
}
