package cn.com.yusys.yusp.service.server.xdxw0063;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0063.req.Xdxw0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.resp.Xdxw0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0063Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0063Service.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    /**
     * 调查基本信息查询
     * @param xdxw0063DataReqDto
     * @return
     */
    @Transactional
    public Xdxw0063DataRespDto getReportBasicInfo(Xdxw0063DataReqDto xdxw0063DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063DataReqDto));
        Xdxw0063DataRespDto result = lmtSurveyReportBasicInfoMapper.getReportBasicInfo(xdxw0063DataReqDto);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(result));
        return result;
    }
}
