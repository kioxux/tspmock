/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtAppRelCusInfo;
import cn.com.yusys.yusp.domain.LmtIntbankApp;
import cn.com.yusys.yusp.domain.LmtIntbankAppSub;
import cn.com.yusys.yusp.domain.LmtSigInvestRelFinaInfo;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusIntbankMgrDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014StockHolderListRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppRelCusInfoMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelCusInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-15 09:37:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppRelCusInfoService extends BizInvestCommonService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppRelCusInfoService.class);

    @Autowired
    private LmtAppRelCusInfoMapper lmtAppRelCusInfoMapper;

    /**
     * 客户管理服务接口
     */
    @Autowired
    private CmisCusClientService cusClientService;

    /**
     * 单笔投资授信主体及增信人财务信息
     */
    @Autowired
    private LmtSigInvestRelFinaInfoService lmtSigInvestRelFinaInfoService;

    /**
     * 同业授信申请审批表
     */
    @Autowired
    private LmtIntbankApprService lmtIntbankApprService;

    /**
     * 同业授信申请表
     */
    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    /**
     * 同业授信申请表
     */
    @Autowired
    private LmtIntbankAppSubService  lmtIntbankAppSubService;

    /**
     * 申请关联股东表
     */
    @Autowired
    private LmtAppCorreShdService lmtAppCorreShdService;

    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;

    //实时计算审批权限使用
    private BigDecimal tmpLmtAmt;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppRelCusInfo selectByPrimaryKey(String pkId) {
        return lmtAppRelCusInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppRelCusInfo> selectAll(QueryModel model) {
        List<LmtAppRelCusInfo> records = (List<LmtAppRelCusInfo>) lmtAppRelCusInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppRelCusInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppRelCusInfo> list = lmtAppRelCusInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppRelCusInfo record) {
        return lmtAppRelCusInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppRelCusInfo record) {
        return lmtAppRelCusInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(LmtAppRelCusInfo record) {
        record.setUpdateTime(getCurrrentDate());
        record.setUpdId(getCurrentUser().getLoginCode());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdDate(getCurrrentDateStr());
        return lmtAppRelCusInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppRelCusInfo record) {
        return lmtAppRelCusInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppRelCusInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppRelCusInfoMapper.deleteByIds(ids);
    }


    /**
     * 根据serno获取企业基本信息
     * @param condition
     * @return
     */
    public LmtAppRelCusInfo selectBySernoAndCusId(Map condition) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",condition.get("serno"));
        BizInvestCommonService.checkParamsIsNull("serno",condition.get("serno"));
        queryModel.addCondition("cusId",condition.get("cusId"));
        BizInvestCommonService.checkParamsIsNull("cusId",condition.get("cusId"));
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtAppRelCusInfo> lmtAppRelCusInfos = selectByModel(queryModel);
        if (lmtAppRelCusInfos!=null && lmtAppRelCusInfos.size()>0){
            return lmtAppRelCusInfos.get(0);
        }
        return null;
    }

    /**
     * 根据serno和cusId获取企业基本信息
     * @param serno
     * @param cusId
     * @return
     */
    public LmtAppRelCusInfo selectBySernoAndCusId(String serno,String cusId){
        Map condition = new HashMap();
        condition.put("serno",serno);
        condition.put("cusId",cusId);
        return selectBySernoAndCusId(condition);
    }

    /**
     * 删除增信人信息 -- 逻辑删除关联客户信息包含主体分析
     * @param grtCusId
     * @param serno
     * @return
     */
    public int deleteLogic(String grtCusId, String serno) {
        int result = 0;
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",grtCusId);
        BizInvestCommonService.checkParamsIsNull("cusId",grtCusId);
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtAppRelCusInfo> lmtAppRelCusInfos = selectByModel(queryModel);
        if (lmtAppRelCusInfos!= null && lmtAppRelCusInfos.size()>0){
            LmtAppRelCusInfo lmtAppRelCusInfo = lmtAppRelCusInfos.get(0);
            lmtAppRelCusInfo.setOprType(CmisBizConstants.OPR_TYPE_02);
            result = update(lmtAppRelCusInfo);
            LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = lmtSigInvestRelFinaInfoService.selectBySerno(serno, grtCusId);
            if (lmtSigInvestRelFinaInfo != null){
                lmtSigInvestRelFinaInfo.setOprType(CmisBizConstants.OPR_TYPE_02);
                lmtSigInvestRelFinaInfoService.update(lmtSigInvestRelFinaInfo);
            }
        }
        return result;
    }


    /**
     * 根据cusId获取同业客户详情
     * @return
     */
    public Map selectCusInfo(String cusId, String cusCatalog) {

        Map result = new HashMap();
        if (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)) {
            try {
                CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
                cmisCus0010ReqDto.setCusId(cusId);
                ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cusClientService.cmiscus0010(cmisCus0010ReqDto);
                CmisCus0010RespDto data = cmisCus0010RespDtoResultDto.getData();
                //返回报文日志打印，方面查询处理
                log.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value,
                        JSON.toJSONString(data));
                if (data != null) {
                    result.put("cusName",data.getCusName());
                    result.put("cusId",cusId);
                    result.put("intbankOrgType",data.getIntbankType());//
                    result.put("buildDate",data.getIntbankBuildDate());
                    result.put("busiLic",data.getBankProLic());//
                    result.put("isStock",data.getMrkFlg());
                    //添加主体授信关联客户股东表（股东列表）
                    List<CusIntbankMgrDto> cusIntbankMgrList = data.getCusIntbankMgrList();
                    if(CollectionUtils.nonEmpty(cusIntbankMgrList)){
                        for (CusIntbankMgrDto cusIntbankMgrDto : cusIntbankMgrList) {
                            //判断当前记录是否为实际控制人（控制人类型 ： 01-实际控制人）
                            if (CmisBizConstants.STD_TYPE_CONTROLLER_01.equals(cusIntbankMgrDto.getTypeController())) {
                                //添加控制人信息
                                result.put("realOperCusName",cusIntbankMgrDto.getMrgName());
                            }
                        }
                    }

                }else{
                    result.put("cusId",cusId);
                }
            } catch (Exception e) {
                log.error("同业客户详情获取报错==》", e);
                //客户信息查询失败
                throw BizException.error(null, EclEnum.LMT_INVEST_TONGYEINFO_GET_FAILED.key, EclEnum.LMT_INVEST_TONGYEINFO_GET_FAILED.value);
                //result.put("cusId",cusId);
            }
        }
        return result;
    }

    /**
     * 添加企业相关信息（lmt_app_rel_cus_info、lmt_app_corre_shd、lmt_sig_invest_rel_fina_info（授信主体及增信人财务信息总表））
     * 同业客户接口cmiscus0010   对公（法人）：cmiscus0014
     * @param serno
     * @param cusId
     * @param cusType
     * @param cusCatalog
     * @return
     */
    public int insertCusInfoApp(String serno, String cusId, String cusType, String cusCatalog){
        return insertCusInfoApp(serno,cusId,cusType,cusCatalog,true);
    }

    /**
     * 添加企业相关信息（lmt_app_rel_cus_info、lmt_app_corre_shd、lmt_sig_invest_rel_fina_info（授信主体及增信人财务信息总表））
     * 同业客户接口cmiscus0010   对公（法人）：cmiscus0014
     * @param serno
     * @param cusId
     * @param cusType
     * @param cusCatalog
     * @param needRelFinaInfo 是否需要获取（单笔投资授信主体及增信人财务信息）
     * @return
     */
    public int insertCusInfoApp(String serno, String cusId, String cusType, String cusCatalog,boolean needRelFinaInfo) {
        long start = System.currentTimeMillis();

        int result = 0;
        /**
         * 判断是否为同业客户 使用cmiscus0010 接口获取
         * 1.获取同业客户的控制人：   控制人类型  为  01-实际控制人
         * 2.获取同业客户的股东： 高管类别  为  02-控制人  且     控制人类型  为  02-控股方
         */
        if (CmisBizConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)){
            try {
                CmisCus0010ReqDto cmisCus0010ReqDto = new CmisCus0010ReqDto();
                cmisCus0010ReqDto.setCusId(cusId);
                ResultDto<CmisCus0010RespDto> cmisCus0010RespDtoResultDto = cusClientService.cmiscus0010(cmisCus0010ReqDto);
                CmisCus0010RespDto data = cmisCus0010RespDtoResultDto.getData();
                //返回报文日志打印，方面查询处理
                log.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value,
                        JSON.toJSONString(data));
                if (data != null) {
                    //添加主体授信关联客户信息（企业信息/担保人信息）
                    LmtAppRelCusInfo lmtAppRelCusInfo = insertCusInfoBy0010(serno, data, cusType, cusCatalog);
                    //同业机构准入不需要获取该信息
                    if (needRelFinaInfo) {
                        //添加单笔投资授信主体及增信人财务信息(主体分析)
                        lmtSigInvestRelFinaInfoService.insertFinalInfoBy0010(serno, data, cusType, cusCatalog);
                    }
                    //添加主体授信关联客户股东表（股东列表）
                    List<CusCorpApitalDto> cusCorpApitalDtoList = data.getCusCorpApitalDtoList();
                    if (CollectionUtils.nonEmpty(cusCorpApitalDtoList)){
                        cusCorpApitalDtoList.forEach(a->{
                            //添加股东信息
                            lmtAppCorreShdService.insertLmtAppCorreShdBy0010(serno, cusId, a);
                        });
                    }
                    //获取实际控制人
                    List<CusIntbankMgrDto> cusIntbankMgrList = data.getCusIntbankMgrList();
                    if (CollectionUtils.nonEmpty(cusIntbankMgrList)) {
                        CusIntbankMgrDto cusIntbankMgrDto1 = cusIntbankMgrList.get(0);
                        //添加控制人信息
                        lmtAppRelCusInfo.setRealOperCusId(cusIntbankMgrDto1.getMrgCertCode());
                        lmtAppRelCusInfo.setRealOperCusName(cusIntbankMgrDto1.getMrgName());
                    }
                    //保存企业信息
                    result = insert(lmtAppRelCusInfo);
                }else{
                    throw BizException.error(null, EclEnum.LMT_INVEST_TONGYEINFO_GET_FAILED.key,EclEnum.LMT_INVEST_TONGYEINFO_GET_FAILED.value);
                }
            } catch (Exception e) {
                log.error("同业客户详情获取报错==》", e);
                result = 0;
                if (e instanceof BizException){
                    throw e;
                }
                //客户信息查询失败
                throw BizException.error(null, EclEnum.LMT_INVEST_TONGYEINFO_SAVE_FAILED.key,EclEnum.LMT_INVEST_TONGYEINFO_SAVE_FAILED.value);
            }
        }
        /**
         * 判断是否为法人客户 使用cmiscus0014接口获取
         * cus_corp_apital cus_corp<->cus_base(对公是相互对应的)
         */
        if(CmisBizConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){
            CmisCus0014RespDto data = null;
            try {
                CmisCus0014ReqDto cmisCus0014ReqDto = new CmisCus0014ReqDto();
                cmisCus0014ReqDto.setCusId(cusId);
                ResultDto<CmisCus0014RespDto> cmisCus0014RespDtoResultDto = cusClientService.cmiscus0014(cmisCus0014ReqDto);
                log.info("法人客户详情:cmiscus0014====》【{}】", JSON.toJSONString(cmisCus0014RespDtoResultDto));
                data = cmisCus0014RespDtoResultDto.getData();
                if (SuccessEnum.SUCCESS.key.equals(data.getErrorCode())){
                    //添加主体授信关联客户信息（企业信息/担保人信息）
                    LmtAppRelCusInfo lmtAppRelCusInfo = insertCusInfoBy0014(serno, data, cusType, cusCatalog);
                    if (needRelFinaInfo) {
                        //添加单笔投资授信主体及增信人财务信息(主体分析)
                        lmtSigInvestRelFinaInfoService.insertFinalInfoBy0014(serno, data, cusType, cusCatalog);
                    }
                    //添加主体授信关联客户股东表（股东列表）
                    List<CmisCus0014StockHolderListRespDto> stockHolderList = data.getStockHolderList();
                    if (stockHolderList!=null && stockHolderList.size()>0) {
                        for (CmisCus0014StockHolderListRespDto cmisCus0014StockHolderListRespDto : stockHolderList) {
                            //添加股东信息
                            lmtAppCorreShdService.insertLmtAppCorreShdBy0014(serno, cusId, cmisCus0014StockHolderListRespDto);
                        }
                    }
                    //获取实际控制人
                    List<CusCorpMgrDto> cusCorpMgrDtoList = data.getCusCorpMgrDtoList();
                    if (CollectionUtils.nonEmpty(cusCorpMgrDtoList)) {
                        CusCorpMgrDto cusCorpMgrDto = cusCorpMgrDtoList.get(0);
                        //添加控制人信息
                        lmtAppRelCusInfo.setRealOperCusId(cusCorpMgrDto.getMrgCertCode());
                        lmtAppRelCusInfo.setRealOperCusName(cusCorpMgrDto.getMrgName());
                    }

                    result = insert(lmtAppRelCusInfo);
                }else{
                    //客户信息查询失败
                    throw BizException.error(null, data.getErrorCode(),data.getErrorMsg());
                }
                result = 1;
            } catch (Exception e) {
                log.error("法人客户信息获取失败==》",e);
                result = 0;
                throw BizException.error(null,"99999","法人客户保存失败！");
            }
        }

        log.info(" insertCusInfoApp 执行时间===> {}",System.currentTimeMillis()-start );
        return result;
    }

    /**
     * 法人客户 添加客户信息
     * @param serno
     * @param data
     * @param cusType
     * @param cusCatalog
     * @return
     */
    private LmtAppRelCusInfo insertCusInfoBy0014(String serno, CmisCus0014RespDto data, String cusType, String cusCatalog) {
        LmtAppRelCusInfo lmtAppRelCusInfo = new LmtAppRelCusInfo();

        lmtAppRelCusInfo.setSerno(serno);
        lmtAppRelCusInfo.setCusId(data.getCusId());
        lmtAppRelCusInfo.setCusName(data.getCusName());
        lmtAppRelCusInfo.setCusCatalog(cusCatalog);
        //同业客户没有机构类型
        lmtAppRelCusInfo.setCusType(data.getCusType());
        //关联客户性质(法人客户-企业性质)
        lmtAppRelCusInfo.setCusCha(data.getCorpOwnersType());
//        lmtAppRelCusInfo.setIntbankOrgType(data.geti);
        lmtAppRelCusInfo.setBuildDate(data.getBuildDate());
//        lmtAppRelCusInfo.setBusiLic();  //金融业务许可证
        lmtAppRelCusInfo.setIsStock(data.getIsStockCorp());
        lmtAppRelCusInfo.setRegiCapCurType(data.getRegiCurType());//注册资金币种
        lmtAppRelCusInfo.setRegiCap(data.getRegiCapAmt());
        //注册登记类型
        lmtAppRelCusInfo.setRegiType(data.getRegiType());
        //实际经营地址
        lmtAppRelCusInfo.setOperAddrAct(data.getOperAddrAct());

        //注册地行政区划为空的话  取注册登记地
        String rigiAreaCodeName = StringUtils.isBlank(data.getRegiAreaCode()) ? data.getRegiAddr() : getAreaCodeName(data.getRegiAreaCode());
        //注册地行政区划
        lmtAppRelCusInfo.setRegiAreaCode(data.getRegiAreaCode());
        //注册地行政区划名称
        lmtAppRelCusInfo.setRegiAreaCodeName(rigiAreaCodeName);

        /**
         * mainOptScp 主营范围
         * partOptScp 兼营范围
         */
        lmtAppRelCusInfo.setOperRange(data.getMainOptScp());
        //实际控制人客户号
//        lmtAppRelCusInfo.setRealOperCusId(data.);
        //实际控制人客户名称
//        lmtAppRelCusInfo.setRealOperCusName();
        //是否城投
        lmtAppRelCusInfo.setIsCtinve(data.getIsCtinve());
        //评级时间(外部)
        lmtAppRelCusInfo.setEvalTimOuter(data.getEvalDate());
        //评级机构(外部)
        lmtAppRelCusInfo.setEvalOrgOuter(data.getEvalOrgId());
        //评级结果(外部)
        lmtAppRelCusInfo.setEvalResultOuter(data.getCreditLevelOuter());
        //评级结果(内部)
        lmtAppRelCusInfo.setEvalResultInner(data.getFinalRank());
        //评级时间(内部)
        lmtAppRelCusInfo.setEvalTimeInner(data.getDueDt());
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtAppRelCusInfo);
        return lmtAppRelCusInfo;
    }


    /**
     *  根据主体授信选择cusId添加对应客户信息(同业客户)
     * @param serno
     * @param data
     * @param cusType
     * @param cusCatalog
     */
    public LmtAppRelCusInfo insertCusInfoBy0010(String serno, CmisCus0010RespDto data,String cusType,String cusCatalog) {
        LmtAppRelCusInfo lmtAppRelCusInfo = new LmtAppRelCusInfo();
        lmtAppRelCusInfo.setSerno(serno);
        lmtAppRelCusInfo.setCusId(data.getCusId());
        lmtAppRelCusInfo.setCusName(data.getCusName());
        lmtAppRelCusInfo.setCusCatalog(cusCatalog);
        //同业客户没有机构类型
        lmtAppRelCusInfo.setCusType(cusType);
        //关联客户性质
//        lmtAppRelCusInfo.setCusCha();
        lmtAppRelCusInfo.setIntbankOrgType(data.getIntbankType());
        //成立日期
        lmtAppRelCusInfo.setBuildDate(data.getIntbankBuildDate());
        //金融业务许可证
        lmtAppRelCusInfo.setBusiLic(data.getBankProLic());
        //是否上市
        lmtAppRelCusInfo.setIsStock(data.getMrkFlg());
        //注册资金币种
        lmtAppRelCusInfo.setRegiCapCurType(data.getRegCapCurType());
        //注册资金
        lmtAppRelCusInfo.setRegiCap(data.getRegCapAmt());
        //控股类型
        lmtAppRelCusInfo.setHoldType(data.getHoldType());

        //评级时间(外部) -- 评级到期日期
        lmtAppRelCusInfo.setEvalTimOuter(data.getEvalEndDt());
        //评级机构(外部)
        lmtAppRelCusInfo.setEvalOrgOuter(data.getOutApprOrg());
        //评级结果(外部)
        lmtAppRelCusInfo.setEvalResultOuter(data.getIntbankEval());
        //评级结果(内部)
        lmtAppRelCusInfo.setEvalResultInner(data.getFinalRank());
        //评级时间(内部)
        lmtAppRelCusInfo.setEvalTimeInner(data.getEffDt());
        //资金注册类型
//        lmtAppRelCusInfo.setRegiType();
        //实际经营地址
//        lmtAppRelCusInfo.setOperAddrAct();
        //经营范围
        lmtAppRelCusInfo.setOperRange(data.getNatBusi());
        //注册地行政区划
        lmtAppRelCusInfo.setRegiAreaCode(data.getRegiAreaCode());
        //注册地行政区划名称
        lmtAppRelCusInfo.setRegiAreaCodeName(getAreaCodeName(data.getRegiAreaCode()));

        //实际控制人客户号
//        lmtAppRelCusInfo.setRealOperCusId();
        //实际控制人客户名称
//        lmtAppRelCusInfo.setRealOperCusName();
        //是否城投
//        lmtAppRelCusInfo.setIsCtinve();
        initInsertDomainProperties(lmtAppRelCusInfo);
        return lmtAppRelCusInfo;
    }

    /**
     * 根据申请流水号和客户号判断综合授信是否在信贷管理部权限下
     * 同业综合授信  信贷管理部负责人权限判断
     * @param serno
     * @param cusId
     * @return
     */
    public Integer rightJudgeXDGL(String serno,String cusId){
        int result = 0;

        try {
            Map map = new HashMap<>();
            //申请流水号
            map.put("serno",serno);
            //客户号
            map.put("cusId",cusId);
            LmtAppRelCusInfo lmtAppRelCusInfo = selectBySernoAndCusId(map);
            //同业机构类型
            String intbankOrgType = lmtAppRelCusInfo.getIntbankOrgType();
            //内部评级
            String evalResultInnerStr = lmtAppRelCusInfo.getEvalResultInner();
            Integer evalResultInner = 170;//默认无内部评级
            if (evalResultInnerStr != null){
                evalResultInner = Integer.valueOf(evalResultInnerStr);//STD_ZB_GRADE_RANK
            }
            log.info("【serno】判断是否信贷管理部权限：evalResultInner-------------->"+ evalResultInner);

            //监管评级
            String supeEval = lmtAppRelCusInfo.getSupeEval() == null ? "" : lmtAppRelCusInfo.getSupeEval() ;
            log.info("【serno】判断是否信贷管理部权限：supeEval-------------->"+supeEval);
            //资产规模
            BigDecimal assetSize = lmtAppRelCusInfo.getAssetSize();
            log.info("【serno】判断是否信贷管理部权限  资产规模：assetSize-------------->"+ assetSize);
            //注册地行政区划
            String regiAreaCode = lmtAppRelCusInfo.getRegiAreaCode();
            log.info("【serno】判断是否信贷管理部权限  注册地行政区划：regiAreaCode-------------->"+ regiAreaCode);

            LmtIntbankApp lmtIntbankApp = lmtIntbankAppService.selectBySerno(serno);
            BigDecimal lmtAmt = BigDecimal.ZERO;
            if(lmtIntbankApp!=null){
                //授信金额
                lmtAmt = getTmpLmtAmt() == null ? lmtIntbankApp.getLmtAmt() : getTmpLmtAmt();
            }

            /**add by zhangjw 同业综合授信在判断是否在信贷管理部负责人权限内的话，要扣减掉同业管理类额度*/
            List<LmtIntbankAppSub> lmtIntbankAppSubs = lmtIntbankAppSubService.selectSubListBySerno(serno);
            if(lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
                //modify by zhangjw 20210822 增加判断如果同业授信只有同业管理类额度，则审批权限默认为信贷管理部负责人权限内
                if(lmtIntbankAppSubs.size()==1){
                    log.info("【serno】判断是否信贷管理部权限  lmtIntbankAppSubs.size()：-------------->1 ");
                    LmtIntbankAppSub lmtIntbankAppSub = lmtIntbankAppSubs.get(0);
                    if(CmisBizConstants.LMT_INTBANK_BIZ_TYPE_3006.equals(lmtIntbankAppSub.getLmtBizType())){
                        result = 1;
                        log.info("【serno】判断是否信贷管理部权限：-------------->仅有同业管理类额度，默认为信贷管理部权限");
                        return result;
                    }
                }
                for(LmtIntbankAppSub lmtIntbankAppSub : lmtIntbankAppSubs ){
                    if(CmisBizConstants.LMT_INTBANK_BIZ_TYPE_3006.equals(lmtIntbankAppSub.getLmtBizType())){
                        BigDecimal lmtsubAmt = lmtIntbankAppSub.getLmtAmt();
                        lmtAmt = lmtAmt.subtract(lmtsubAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
                        break;
                    }

                }
                log.info("【serno】判断是否信贷管理部权限：lmtAmt-------------->"+lmtAmt);
            }

            //机构是银行
            log.info("【serno】判断是否信贷管理部权限：intbankOrgType-------------->"+intbankOrgType);
            if(intbankOrgType.startsWith("C")){
                //机构是国有商业银行/全国性股份行并且评级在AA-及以上
                if((CmisBizConstants.STD_ZB_INTBANK_TYPE_C12111.equals(intbankOrgType)||CmisBizConstants.STD_ZB_INTBANK_TYPE_C12141.equals(intbankOrgType)
                        ||CmisBizConstants.STD_ZB_INTBANK_TYPE_C12112.equals(intbankOrgType)) && evalResultInner < 50){
                    if(compare(lmtAmt,1500000000L) <= 0){
                        result = 1;
                    }
                    return result;
                    //机构是省内城商行/农商行并且评级在AA-及以上
                }else if( (CmisBizConstants.STD_ZB_INTBANK_TYPE_C121321.equals(intbankOrgType)
                        ||CmisBizConstants.STD_ZB_INTBANK_TYPE_C121311.equals(intbankOrgType))
                         && evalResultInner < 50){
                    if(compare(assetSize,600_0000_0000L) >= 0 && compare(lmtAmt,10_0000_0000L) <= 0){
                        result = 1;
                        return result;
                    }
                    if(compare(assetSize,300_0000_0000L) >= 0 && compare(lmtAmt,6_0000_0000L) <= 0){
                        result = 1;
                        return result;
                    }
                    if(compare(assetSize,300_0000_0000L) < 0 && compare(lmtAmt,4_0000_0000L) <= 0){
                        result = 1;
                        return result;
                    }
                    return result;
                }
                //其他银行
                if (CmisBizConstants.STD_ZB_INTBANK_Others.contains(intbankOrgType+",")){
                    if(evalResultInner < 50 && compare(lmtAmt,3_0000_0000L) < 0 ){
                        result = 1;
                        return result;
                    }
                }
            }
            //机构是证券公司
            if(intbankOrgType.equals(CmisBizConstants.STD_ZB_INTBANK_TYPE_E10000) && evalResultInner < 50
                    && (supeEval.equals("A") || supeEval.equals("AA") || supeEval.equals("AAA")) && compare(lmtAmt,200000000L) < 0){
                result = 1;
            }
        } catch (Exception e) {
            log.error("rightJudgeXDGL=====>",e);
            result = 0;
        }
        log.info("【serno】判断是否信贷管理部权限：result-------------->"+result);
        return result;
    }

    /**
     * 根据serno和cusId判断综合授信是否在副行长权限内
     * 同业综合授信判断
     * @param serno
     * @param cusId
     * @return
     */
    public Integer rightJudgeFHZ(String serno,String cusId){
        int result = 0;

        try {
            Map map = new HashMap<>();
            //申请流水号
            map.put("serno",serno);
            //客户号
            map.put("cusId",cusId);
            LmtAppRelCusInfo lmtAppRelCusInfo = selectBySernoAndCusId(map);
            //同业机构类型
            String intbankOrgType = lmtAppRelCusInfo.getIntbankOrgType();
            //内部评级
            String evalResultInnerStr = lmtAppRelCusInfo.getEvalResultInner();
            Integer evalResultInner = 170;//默认无评级
            if (evalResultInnerStr != null){
                evalResultInner = Integer.valueOf(evalResultInnerStr);
            }
            String regiAreaCode = lmtAppRelCusInfo.getRegiAreaCode();
            log.info("【serno】判断是否副行长权限:intbankOrgType ------------>"+intbankOrgType);
            log.info("【serno】判断是否副行长权限:evalResultInner ------------> "+evalResultInnerStr);
            //监管评级
            String supeEval = lmtAppRelCusInfo.getSupeEval()==null ? "" : lmtAppRelCusInfo.getSupeEval() ;
            log.info("【serno】判断是否副行长权限: supeEval ------------>"+supeEval);
            //资产规模
            BigDecimal assetSize = lmtAppRelCusInfo.getAssetSize();
            LmtIntbankApp lmtIntbankApp = lmtIntbankAppService.selectBySerno(serno);
            //授信金额
            BigDecimal lmtAmt = BigDecimal.ZERO;
            if(lmtIntbankApp!=null){
                lmtAmt = getTmpLmtAmt() == null ? lmtIntbankApp.getLmtAmt() : getTmpLmtAmt();
            }

            /**add by zhangjw 同业综合授信在判断是否在信贷管理部负责人权限内的话，要扣减掉同业管理类额度*/
            List<LmtIntbankAppSub> lmtIntbankAppSubs = lmtIntbankAppSubService.selectSubListBySerno(serno);
            if(lmtIntbankAppSubs!=null && lmtIntbankAppSubs.size()>0){
                for(LmtIntbankAppSub lmtIntbankAppSub : lmtIntbankAppSubs ){
                    if(CmisBizConstants.LMT_INTBANK_BIZ_TYPE_3006.equals(lmtIntbankAppSub.getLmtBizType())){
                        BigDecimal lmtsubAmt = lmtIntbankAppSub.getLmtAmt();
                        lmtAmt = lmtAmt.subtract(lmtsubAmt).setScale(2,BigDecimal.ROUND_HALF_UP);
                        break;
                    }
                }

                log.info("【serno】判断是否副行长权限: 扣减掉同业管理类额度后 lmtAmt ------------>"+lmtAmt);

                //客户只有同业管理类额度
                if(lmtAmt.compareTo(BigDecimal.ZERO)==0){
                    log.info("【serno】判断是否副行长权限:lmtAmt ------------>客户仅有同业管理类额度 "+lmtAmt );
                    //modify by zhangjw 20210824只有同业管理类额度，则信贷管理部权限内
                    result = 1;
                    return result;
                }else if(evalResultInner == null ){
                    log.info("【serno】判断是否副行长权限: ------------>内部评级为空默认分管行长权限范围外 " );
                    //modify by zhangjw 20210824同业综合授信，没有内部评级则上会，只有同业管理类额度时除外
                    return result;
                }
            }

            //机构是银行
            if(intbankOrgType.startsWith("C")){
                //机构是国有商业银行/全国性股份行并且评级在AA-及以上
                if((CmisBizConstants.STD_ZB_INTBANK_TYPE_C12111.equals(intbankOrgType)
                        ||CmisBizConstants.STD_ZB_INTBANK_TYPE_C12141.equals(intbankOrgType)
                        ||CmisBizConstants.STD_ZB_INTBANK_TYPE_C12112.equals(intbankOrgType)) && evalResultInner < 50){
                    if(compare(lmtAmt,1600000000L) <= 0){
                        result =  1;
                    }
                    //机构是省内城商行/农商行并且评级在AA-及以上
                }else if((intbankOrgType.equals(CmisBizConstants.STD_ZB_INTBANK_TYPE_C121321)
                        ||intbankOrgType.equals(CmisBizConstants.STD_ZB_INTBANK_TYPE_C121311) )
                        && evalResultInner < 50){
                    if(compare(assetSize,60000000000L) >= 0 && compare(lmtAmt,1100000000L) <= 0){
                        result =  1;
                    }
                    if(compare(assetSize,30000000000L) >= 0 && compare(lmtAmt,700000000L) <= 0){
                        result =  1;
                    }
                    if(compare(assetSize,30000000000L) < 0 && compare(lmtAmt,500000000L) <= 0){
                        result =  1;
                    }
                }
                //其他银行
                if (CmisBizConstants.STD_ZB_INTBANK_Others.contains(intbankOrgType+",")){
                    if(evalResultInner < 50 && compare(lmtAmt,300000000L) < 0 ){
                        result =  1;
                    }
                }
            }
            //机构是证券公司
            if(intbankOrgType.equals(CmisBizConstants.STD_ZB_INTBANK_TYPE_E10000) && evalResultInner < 50
                    && (supeEval.equals("A") || supeEval.equals("AA") || supeEval.equals("AAA")) && compare(lmtAmt,200000000L) < 0){
                return 1;
            }
            log.error("rightJudgeFHZ====>",result);
        } catch (Exception e) {
            log.error("rightJudgeFHZ====>",e);
            result = 0;
        }
        return result;
    }

    //BigDecimal的比较方法：a相比于b,返回值1是大于，0是等于，-1是小于
    public int compare(BigDecimal a,long b){
        int i = a.compareTo(BigDecimal.valueOf(b));
        return i;
    }

    /**
     * 更新企业信息和（同业授信/准入）信息
     * @param lmtAppRelCusInfo
     * @return
     */
    @Transactional
    public int updateForCusInfo(Map lmtAppRelCusInfo) {
        LmtAppRelCusInfo lmtAppRelCusInfo1 = new LmtAppRelCusInfo();
        mapToBean(lmtAppRelCusInfo,lmtAppRelCusInfo1);
        updateSelective(lmtAppRelCusInfo1);

        //更新同业授信/准入
        String intbankLmtAdmit = (String) lmtAppRelCusInfo.get("intbankLmtAdmit");
        String serno = lmtAppRelCusInfo1.getSerno();
        String cusId = lmtAppRelCusInfo1.getCusId();

        //lmt_sig_invest_rel_fina_info
        LmtSigInvestRelFinaInfo lmtSigInvestRelFinaInfo = lmtSigInvestRelFinaInfoService.selectBySerno(serno, cusId);
        if (lmtSigInvestRelFinaInfo!=null){
            lmtSigInvestRelFinaInfo.setIntbankLmtAdmit(intbankLmtAdmit);
            lmtSigInvestRelFinaInfoService.update(lmtSigInvestRelFinaInfo);
        }
        return 1;
    }

    @Transactional
    public int updateQyxx(LmtAppRelCusInfo lmtAppRelCusInfo) {
        updateMustCheckStatus(lmtAppRelCusInfo.getSerno(), "qyxx");
        update(lmtAppRelCusInfo);
        //重新计算审批权限
        return lmtSigInvestAppService.reCalculateApprMode(lmtAppRelCusInfo.getSerno());
    }

    /**
     * 获取注册地行政区划名称
     * @param areaCode
     * @return
     */
    public String getAreaCodeName(String areaCode){
        String labelPath = "";
        try {
            if (StringUtils.isBlank(areaCode)){
                return labelPath;
            }
            AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
            adminSmTreeDicDto.setOptType("STD_ZB_AREA_CODE");
            adminSmTreeDicDto.setCode(areaCode);
            ResultDto<Map<String, String>> mapResultDto = dscmsCfgClientService.querySingleLabelPath(adminSmTreeDicDto);
            Map<String, String> data1 = mapResultDto.getData();
            if (data1 != null){
                labelPath = data1.get("labelPath");
            }
        } catch (Exception e) {
            log.error("获取地区名称失败！{}", e);
        }
        return labelPath;
    }

    public BigDecimal getTmpLmtAmt() {
        return tmpLmtAmt;
    }

    public void setTmpLmtAmt(BigDecimal tmpLmtAmt) {
        this.tmpLmtAmt = tmpLmtAmt;
    }
}
