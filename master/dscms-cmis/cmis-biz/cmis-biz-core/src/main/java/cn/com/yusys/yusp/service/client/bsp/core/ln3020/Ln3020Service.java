package cn.com.yusys.yusp.service.client.bsp.core.ln3020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Ln3020ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Ln3020RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：贷款开户
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Ln3020Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw01Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * 业务逻辑处理方法：贷款开户
     *
     * @param ln3020ReqDto
     * @return
     */
    public Ln3020RespDto ln3020(Ln3020ReqDto ln3020ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value, JSON.toJSONString(ln3020ReqDto));
        ResultDto<Ln3020RespDto> ln3020ResultDto = dscms2CoreLnClientService.ln3020(ln3020ReqDto);
        System.out.println("ln3020ReqDto: "+ ln3020ReqDto.toString());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value, JSON.toJSONString(ln3020ResultDto));
        String ln3020Code = Optional.ofNullable(ln3020ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3020Meesage = Optional.ofNullable(ln3020ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3020RespDto ln3020RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3020ResultDto.getCode())) {
            //  获取相关的值并解析
            ln3020RespDto = ln3020ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, ln3020Code, ln3020Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value);
        return ln3020RespDto;
    }

    /**
     * 根据 [TODO  ]组装[请求Dto：贷款开户]
     *
     * @param
     * @return
     */
    public Ln3020ReqDto buildLn3020ReqDto() {
        Ln3020ReqDto ln3020ReqDto = new Ln3020ReqDto();
        ln3020ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3020ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        // 已和核心确认
        return ln3020ReqDto;
    }
}
