/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.domain.OtherCnyRateApprSubRequestDto;
import cn.com.yusys.yusp.dto.CfgSorgLoanManyDto;
import cn.com.yusys.yusp.service.OtherCnyRateAppFinDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.OtherCnyRateApprSub;
import cn.com.yusys.yusp.service.OtherCnyRateApprSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateApprSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-03 17:23:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController


@RequestMapping("/api/othercnyrateapprsub")
public class OtherCnyRateApprSubResource {
    @Autowired
    private OtherCnyRateApprSubService otherCnyRateApprSubService;

    @Autowired
    private OtherCnyRateAppFinDetailsService otherCnyRateAppFinDetailsService;
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<OtherCnyRateApprSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<OtherCnyRateApprSub> list = otherCnyRateApprSubService.selectAll(queryModel);
        return new ResultDto<List<OtherCnyRateApprSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<OtherCnyRateApprSub>> index(QueryModel queryModel) {
        List<OtherCnyRateApprSub> list = otherCnyRateApprSubService.selectByModel(queryModel);
        return new ResultDto<List<OtherCnyRateApprSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{approveSerno}")
    protected ResultDto<OtherCnyRateApprSub> show(@PathVariable("approveSerno") String approveSerno) {
        OtherCnyRateApprSub otherCnyRateApprSub = otherCnyRateApprSubService.selectByPrimaryKey(approveSerno);
        return new ResultDto<OtherCnyRateApprSub>(otherCnyRateApprSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<OtherCnyRateApprSub> create(@RequestBody OtherCnyRateApprSub otherCnyRateApprSub) throws URISyntaxException {
        otherCnyRateApprSubService.insert(otherCnyRateApprSub);
        return new ResultDto<OtherCnyRateApprSub>(otherCnyRateApprSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody OtherCnyRateApprSub otherCnyRateApprSub) throws URISyntaxException {
        int result = otherCnyRateApprSubService.update(otherCnyRateApprSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{approveSerno}")
    protected ResultDto<Integer> delete(@PathVariable("approveSerno") String approveSerno) {
        int result = otherCnyRateApprSubService.deleteByPrimaryKey(approveSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = otherCnyRateApprSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    
    /**
     * 根据本期融资信息获取审批信息
     * @param
     * @return
     */
    @GetMapping("/queryByCurrFinDetails")
    public ResultDto<List<OtherCnyRateApprSub>> queryByCurrFinDetails(QueryModel queryModel) {
    	List<OtherCnyRateApprSub> apprSubList = otherCnyRateApprSubService.queryByCurrFinDetails(queryModel);
    	return new ResultDto<>(apprSubList);
    }
    /**
     * 修改本期融资信息获取审批信息
     * @param
     * @return
     */
    @PostMapping("/updaterateCnySub")
    protected ResultDto<Integer> updateinfComplete(@RequestBody OtherCnyRateApprSubRequestDto otherCnyRateApprSubRequestDto) throws URISyntaxException {
        int result = otherCnyRateApprSubService.updaterateCnySub(otherCnyRateApprSubRequestDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据本期融资信息获取审批信息
     * @param
     * @return
     */
    @PostMapping ("/queryOtherCnyRateApprSub")
    public ResultDto<List<OtherCnyRateApprSubRequestDto>> queryOtherCnyRateApprSub(@RequestBody  QueryModel queryModel) {
        List<OtherCnyRateApprSubRequestDto> apprSubList = otherCnyRateApprSubService.queryByCurrFin(queryModel);
        return new ResultDto<List<OtherCnyRateApprSubRequestDto>>(apprSubList);
    }




}
