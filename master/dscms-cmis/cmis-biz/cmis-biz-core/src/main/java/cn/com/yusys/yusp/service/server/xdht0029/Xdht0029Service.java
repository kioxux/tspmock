package cn.com.yusys.yusp.service.server.xdht0029;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.dto.server.xdht0029.req.Xdht0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0029.resp.Xdht0029DataRespDto;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0007.CmisCus0007Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称:
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021/6/16 22:54
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0029Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0029Service.class);

    @Autowired
    private CmisCus0007Service cmisCus0007Service;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CommonService commonService;

    /**
     * 客户非小微业务经营性贷款总金额查询
     *
     * @param xdht0029DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0029DataRespDto xdht0029Execute(Xdht0029DataReqDto xdht0029DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value);
        Xdht0029DataRespDto xdht0029DataRespDto = new Xdht0029DataRespDto();
        BigDecimal totalAmt = new BigDecimal(0);
        try {
            // 身份证号码
            String certNo = xdht0029DataReqDto.getCertNo();
            //客户编号
            String cusId = "";
            // 通过客户证件号查询客户信息
            List<CusBaseDto> cusBaseDtos = commonService.getCusBaseListByCertCode(certNo);
            if(CollectionUtils.nonEmpty(cusBaseDtos)&&cusBaseDtos.size()>0){
                cusId=cusBaseDtos.get(0).getCusId();
            }
            // 根据sql去cfg库查询  cfg微服务暂时没有接口    SELECT en_name  FROM CMIS_CFG.ADMIN_SM_TREE_DIC A   WHERE  OPT_TYPE = 'STD_PRD_TYPE'  and  LOCATE LIKE '100000,020000,021000,%';
            List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtos=commonService.queryBasicInfoByCatalog("->业务品种->个人经营性贷款->一般个人经营性贷款");
            cfgPrdBasicinfoDtos= JSON.parseObject(JSON.toJSONString(cfgPrdBasicinfoDtos), new TypeReference< List<CfgPrdBasicinfoDto>>(){});
            String en_name = "";
            for (int i = 0; i < cfgPrdBasicinfoDtos.size(); i++) {
                en_name += cfgPrdBasicinfoDtos.get(i).getPrdId();
                if(i<cfgPrdBasicinfoDtos.size()-1){
                    en_name += ",";
                }
            }
            en_name +=",022004";//个人经营性物业贷款
            // 根据客户号、业务类型、合同状态查询非小微业务经营性贷款总金额
            if (StringUtils.hasText(cusId) && StringUtils.hasText(en_name)) {
                Map queryMap = new HashMap();
                queryMap.put("cusId", cusId);
                queryMap.put("bizType", en_name);// 业务类型
                totalAmt = ctrLoanContMapper.selectCtrLoanContTotalAmtByCusIdAndBizType(queryMap);
            }
            xdht0029DataRespDto.setTotlApplyAmount(String.valueOf(totalAmt));

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029DataRespDto));
        return xdht0029DataRespDto;
    }
}
