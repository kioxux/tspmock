/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfo;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-29 11:00:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoMapper lmtSigInvestBasicInfoMapper;

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfo selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfo> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfo> records = (List<LmtSigInvestBasicInfo>) lmtSigInvestBasicInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfo> list = lmtSigInvestBasicInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据serno获取企业基本信息
     * @param condition
     * @return
     */
    public LmtSigInvestBasicInfo selectBySernoAndCusId(Map condition) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",condition.get("serno"));
        BizInvestCommonService.checkParamsIsNull("serno",condition.get("serno"));
        queryModel.addCondition("cusId",condition.get("cusId"));
        BizInvestCommonService.checkParamsIsNull("cusId",condition.get("cusId"));
        queryModel.setPage(1);
        queryModel.setSize(1);
        List<LmtSigInvestBasicInfo> lmtSigInvestBasicInfos = selectByModel(queryModel);
        if (lmtSigInvestBasicInfos!=null && lmtSigInvestBasicInfos.size()>0){
            return lmtSigInvestBasicInfos.get(0);
        }
        return null;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfo record) {
        return lmtSigInvestBasicInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfo record) {
        return lmtSigInvestBasicInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfo record) {
        return lmtSigInvestBasicInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfo record) {
        return lmtSigInvestBasicInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoMapper.deleteByIds(ids);
    }

    /**
     * 根据流水号获取详情
     * @param serno
     * @return
     */
    public LmtSigInvestBasicInfo selectBySerno(String serno){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestBasicInfo> lmtSigInvestBasicInfoList = selectByModel(queryModel);
        if (lmtSigInvestBasicInfoList!=null && lmtSigInvestBasicInfoList.size()>0){
            return lmtSigInvestBasicInfoList.get(0);
        }
        return null;
    }

    /**
     * 主体授信-新增-初始化
     * @param serno
     */
    public void insertInvestBasicInfo(String serno) {
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = new LmtSigInvestBasicInfo();

        lmtSigInvestBasicInfo.setSerno(serno);
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtSigInvestBasicInfo);
        insert(lmtSigInvestBasicInfo);
    }

    /**
     * 图片上传
     * @param condition
     * @return
     */
    public String updatePicAbsoultPath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String key = (String) condition.get("key");
        String pkId = (String) condition.get("pkId");
        String serverPath = (String) condition.get("serverPath");
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = new LmtSigInvestBasicInfo();
        lmtSigInvestBasicInfo.setPkId(pkId);
        //获取图片绝对路径
        String picAbsolutePath = " ";
        String relativePath = "/image/"+getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)){
            picAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,fanruanFileTemplate);
        }
        setValByKey(lmtSigInvestBasicInfo,key,picAbsolutePath);
        updateSelective(lmtSigInvestBasicInfo);
        return picAbsolutePath;
    }

    /**
     * 保存底层资产分析
     * @param lmtSigInvestBasicInfo
     */
    public int insertDczcfx(LmtSigInvestBasicInfo lmtSigInvestBasicInfo) {
        String serno = lmtSigInvestBasicInfo.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"dczcfx");
        return insert(lmtSigInvestBasicInfo);
    }

    /**
     * 更新底层资产分析
     * @param lmtSigInvestBasicInfo
     * @return
     */
    public int updateDczcfx(LmtSigInvestBasicInfo lmtSigInvestBasicInfo) {
        String serno = lmtSigInvestBasicInfo.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"dczcfx");
        return update(lmtSigInvestBasicInfo);

    }

    /**
     * 保存主体情况分析
     * @param lmtSigInvestBasicInfo
     * @return
     */
    public int updateZtqkfx(LmtSigInvestBasicInfo lmtSigInvestBasicInfo) {
        String serno = lmtSigInvestBasicInfo.getSerno();
        //更新必填页面校验为已完成
        updateMustCheckStatus(serno,"ztqkfx");
        return update(lmtSigInvestBasicInfo);
    }
}
