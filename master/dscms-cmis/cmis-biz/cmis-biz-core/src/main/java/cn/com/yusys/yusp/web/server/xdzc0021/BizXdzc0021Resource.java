package cn.com.yusys.yusp.web.server.xdzc0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0021.req.Xdzc0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.resp.Xdzc0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0021.Xdzc0021Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:融资汇总查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0021:融资汇总查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0021Resource.class);

    @Autowired
    private Xdzc0021Service xdzc0021Service;
    /**
     * 交易码：xdzc0021
     * 交易描述：融资汇总查询
     *
     * @param xdzc0021DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("融资汇总查询")
    @PostMapping("/xdzc0021")
    protected @ResponseBody
    ResultDto<Xdzc0021DataRespDto> xdzc0021(@Validated @RequestBody Xdzc0021DataReqDto xdzc0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021DataReqDto));
        Xdzc0021DataRespDto xdzc0021DataRespDto = new Xdzc0021DataRespDto();// 响应Dto:融资汇总查询
        ResultDto<Xdzc0021DataRespDto> xdzc0021DataResultDto = new ResultDto<>();
        try {
            xdzc0021DataRespDto = xdzc0021Service.xdzc0021Service(xdzc0021DataReqDto);
            xdzc0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, e.getMessage());
            // 封装xdzc0021DataResultDto中异常返回码和返回信息
            xdzc0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0021DataRespDto到xdzc0021DataResultDto中
        xdzc0021DataResultDto.setData(xdzc0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021DataResultDto));
        return xdzc0021DataResultDto;
    }
}
