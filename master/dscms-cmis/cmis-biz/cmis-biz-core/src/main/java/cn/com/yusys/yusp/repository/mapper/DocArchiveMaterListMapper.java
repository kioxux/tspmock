/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.DocArchiveMaterList;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveMaterListMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface DocArchiveMaterListMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    DocArchiveMaterList selectByPrimaryKey(@Param("admlSerno") String admlSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<DocArchiveMaterList> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(DocArchiveMaterList record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(DocArchiveMaterList record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(DocArchiveMaterList record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(DocArchiveMaterList record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("admlSerno") String admlSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据档案归档流水号删除关联资料清单信息
     * @author jijian_yx
     * @date 2021/6/21 10:14
     **/
    int deleteByDocSerno(String docSerno);

    /**
     *
     * @函数名称:selectCountsByDocTypeData
     * @函数描述:查询同一材料类型条数
     * @参数与返回说明:
     * @算法描述:
     */
    int selectCountsByDocTypeData(QueryModel model);

    /**
     * 获取一级影像目录编号集合
     * @author jijian_yx
     * @date 2021/8/20 15:14
     **/
    List<String> selectOutsystemCodeList(String docSerno);

    /**
     * 获取以";"拼接的一级影像目录编号
     * @author jijian_yx
     * @date 2021/8/20 15:14
     **/
    String selectTopOutsystemCode(String docSerno);

    /**
     * 获取以","拼接的二级影像目录编号
     * @author jijian_yx
     * @date 2021/8/20 15:14
     **/
    String selectSecOutsystemCode(String docSerno);
}