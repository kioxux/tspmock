package cn.com.yusys.yusp.web.server.xdxw0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0014.req.Xdxw0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.resp.Xdxw0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0014.Xdxw0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优企贷共借人签订
 *
 * @author sunzhen
 * @version 1.0
 */
@Api(tags = "XDXW0014:优企贷共借人签订")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0014Resource.class);

    @Autowired
    private Xdxw0014Service xdxw0014Service;

    /**
     * 交易码：xdxw0014
     * 交易描述：优企贷共借人签订
     *
     * @param xdxw0014DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷共借人签订")
    @PostMapping("/xdxw0014")
    protected @ResponseBody
    ResultDto<Xdxw0014DataRespDto> xdxw0014(@Validated @RequestBody Xdxw0014DataReqDto xdxw0014DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataReqDto));
        Xdxw0014DataRespDto xdxw0014DataRespDto = new Xdxw0014DataRespDto();// 响应Dto:优企贷共借人签订
        ResultDto<Xdxw0014DataRespDto> xdxw0014DataResultDto = new ResultDto<>();
        // 从xdxw0014DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdxw0014Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataReqDto));
            xdxw0014DataRespDto = xdxw0014Service.updateSignStatByCommonCertNo(xdxw0014DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataRespDto));
            // 封装xdxw0014DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0014DataRespDto.getOpFlag();
            String opMessage = xdxw0014DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0014DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0014DataResultDto.setMessage(opMessage);
            } else {
                xdxw0014DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, e.getMessage());
            // 封装xdxw0014DataResultDto中异常返回码和返回信息
            xdxw0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0014DataResultDto.setMessage(e.getMessage());
            xdxw0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0014DataRespDto到xdxw0014DataResultDto中
        xdxw0014DataResultDto.setData(xdxw0014DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataResultDto));
        return xdxw0014DataResultDto;
    }
}
