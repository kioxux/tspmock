package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAccList
 * @类描述: doc_acc_list数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-22 16:01:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DocAccListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 档案流水号 **/
	private String docSerno;
	
	/** 档案编号 **/
	private String docNo;
	
	/** 归档模式 **/
	private String archiveMode;
	
	/** 档案分类 **/
	private String docClass;
	
	/** 档案类型 **/
	private String docType;
	
	/** 关联业务编号 **/
	private String bizSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 入库操作人 **/
	private String optUsr;
	
	/** 入库操作机构 **/
	private String optOrg;
	
	/** 入库操作时间 **/
	private String storageOptDate;
	
	/** 档案入现金库时间 **/
	private String storageCashDate;
	
	/** 归还日期 **/
	private String backDate;
	
	/** 档案状态 **/
	private String docStauts;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 借据编号 **/
	private String billNo;

	/** 档案业务品种 **/
	private String docBizType;

	/** 起始日期 **/
	private String startDate;

	/** 产品编号 **/
	private String prdId;

	/** 产品名称 **/
	private String prdName;

	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;

	/** 到期日期 **/
	private String endDate;


	/**
	 * @param docSerno
	 */
	public void setDocSerno(String docSerno) {
		this.docSerno = docSerno == null ? null : docSerno.trim();
	}
	
    /**
     * @return DocSerno
     */	
	public String getDocSerno() {
		return this.docSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo == null ? null : docNo.trim();
	}
	
    /**
     * @return DocNo
     */	
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param archiveMode
	 */
	public void setArchiveMode(String archiveMode) {
		this.archiveMode = archiveMode == null ? null : archiveMode.trim();
	}
	
    /**
     * @return ArchiveMode
     */	
	public String getArchiveMode() {
		return this.archiveMode;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass == null ? null : docClass.trim();
	}
	
    /**
     * @return DocClass
     */	
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType == null ? null : docType.trim();
	}
	
    /**
     * @return DocType
     */	
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr == null ? null : optUsr.trim();
	}
	
    /**
     * @return OptUsr
     */	
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg == null ? null : optOrg.trim();
	}
	
    /**
     * @return OptOrg
     */	
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param storageOptDate
	 */
	public void setStorageOptDate(String storageOptDate) {
		this.storageOptDate = storageOptDate == null ? null : storageOptDate.trim();
	}
	
    /**
     * @return StorageOptDate
     */	
	public String getStorageOptDate() {
		return this.storageOptDate;
	}
	
	/**
	 * @param storageCashDate
	 */
	public void setStorageCashDate(String storageCashDate) {
		this.storageCashDate = storageCashDate == null ? null : storageCashDate.trim();
	}
	
    /**
     * @return StorageCashDate
     */	
	public String getStorageCashDate() {
		return this.storageCashDate;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate == null ? null : backDate.trim();
	}
	
    /**
     * @return BackDate
     */	
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts == null ? null : docStauts.trim();
	}
	
    /**
     * @return DocStauts
     */	
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param docBizType
	 */
	public void setDocBizType(String docBizType) {
		this.docBizType = docBizType == null ? null : docBizType.trim();
	}

    /**
     * @return DocBizType
     */
	public String getDocBizType() {
		return this.docBizType;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}

    /**
     * @return StartDate
     */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}

    /**
     * @return PrdId
     */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}

    /**
     * @return PrdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

    /**
     * @return LoanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}

    /**
     * @return EndDate
     */
	public String getEndDate() {
		return this.endDate;
	}


}