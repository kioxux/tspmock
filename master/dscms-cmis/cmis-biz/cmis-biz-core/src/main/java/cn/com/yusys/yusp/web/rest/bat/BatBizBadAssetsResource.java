/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest.bat;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.bat.BatBizBadAssets;
import cn.com.yusys.yusp.service.bat.BatBizBadAssetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BatBizBadAssetsResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-18 11:11:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/batbizbadassets")
public class BatBizBadAssetsResource {
    @Autowired
    private BatBizBadAssetsService batBizBadAssetsService;

	/**
     * 全表查询.
     *
     * @ return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BatBizBadAssets>> query() {
        QueryModel queryModel = new QueryModel();
        List<BatBizBadAssets> list = batBizBadAssetsService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<BatBizBadAssets>> index(@RequestBody QueryModel queryModel) {
        List<BatBizBadAssets> list = batBizBadAssetsService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<BatBizBadAssets> show(@PathVariable("pkId") String pkId) {
        BatBizBadAssets batBizBadAssets = batBizBadAssetsService.selectByPrimaryKey(pkId);
        return new ResultDto<>(batBizBadAssets);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<BatBizBadAssets> create(@RequestBody BatBizBadAssets batBizBadAssets) throws URISyntaxException {
        batBizBadAssetsService.insert(batBizBadAssets);
        return new ResultDto<>(batBizBadAssets);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BatBizBadAssets batBizBadAssets) throws URISyntaxException {
        int result = batBizBadAssetsService.update(batBizBadAssets);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = batBizBadAssetsService.deleteByPrimaryKey(pkId);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = batBizBadAssetsService.deleteByIds(ids);
        return new ResultDto<>(result);
    }
}
