package cn.com.yusys.yusp.service.server.xdtz0036;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.server.xdtz0036.req.Xdtz0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.resp.Xdtz0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.client.batch.cmisbatch0004.CmisBatch0004Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

/**
 * 接口处理类:根据客户号查询申请人行内还款（利息）单次逾期天数不超10天次数（5天则改成5）
 *
 * @author YX-YD
 * @version 1.0
 */
@Service
public class Xdtz0036Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0036Service.class);
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private CmisBatch0004Service cmisBatch0004Service;

    /**
     * @param xdtz0036DataReqDto
     * @Description:根据客户号查询申请人行内还款（利息）单次逾期天数不超10天次数（5天则改成5）
     * @Author: YX-YD
     * @Date: 2021/6/4 20:26
     * @return: xdtz0036DataRespDto
     **/
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0036DataRespDto xdtz0036(Xdtz0036DataReqDto xdtz0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataReqDto));
        // 定义返回信息
        Xdtz0036DataRespDto xdtz0036DataRespDto = new Xdtz0036DataRespDto();
        /**
         * 查询类型:
         * 10:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>10
         * 05:查询条件为:上次还款日(schkriqi)-到期日期(daoqriqi)>5
         * 0:无查询条件
         */
        String queryType = "";//查询类型
        int countNum = 0;
        // 客户号
        String cusId = xdtz0036DataReqDto.getCusId();
        // 借据号
        String billNo = xdtz0036DataReqDto.getBillNo();
        // 最大逾期天数
        String maxOverdueDays = xdtz0036DataReqDto.getMaxOverdueDays();
        String overdueTimes = "";
        try {
            if (StringUtil.isEmpty(billNo) && StringUtil.isEmpty(cusId)) {
                // 请求参数不存在
                xdtz0036DataRespDto.setOverdueTimes(StringUtils.EMPTY);
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            } else {
                if (StringUtil.isNotEmpty(cusId)) {
                    // 服务一：请求参数客户号（cust_id）
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("cusId", cusId);
                    List<AccLoan> accLoanList = accLoanService.selectByModel(queryModel);
                    if (CollectionUtils.nonEmpty(accLoanList)) {
                        for (int i = 0; i < accLoanList.size(); i++) {
                            billNo = accLoanList.get(i).getBillNo();//贷款借据号
                            queryType = "10";//查询类型
                            Cmisbatch0004ReqDto cmisbatch0004ReqDto = new Cmisbatch0004ReqDto();
                            cmisbatch0004ReqDto.setDkjiejuh(billNo);
                            cmisbatch0004ReqDto.setQueryType(queryType);
                            Cmisbatch0004RespDto cmisbatch0004RespDto = cmisBatch0004Service.cmisbatch0004(cmisbatch0004ReqDto);
                            countNum += cmisbatch0004RespDto.getCountNum();
                        }
                        ;
                    }
                } else if (StringUtil.isNotEmpty(billNo)) {
                    // 服务二：请求参数借据号（dkjiejuh）
                    String[] split = billNo.split(",");
                    for (String dkjiejuh : split) {
                        Cmisbatch0004ReqDto cmisbatch0004ReqDto = new Cmisbatch0004ReqDto();
                        queryType = "05";
                        cmisbatch0004ReqDto.setDkjiejuh(dkjiejuh);
                        cmisbatch0004ReqDto.setQueryType(queryType);
                        Cmisbatch0004RespDto cmisbatch0004RespDto = cmisBatch0004Service.cmisbatch0004(cmisbatch0004ReqDto);
                        countNum += cmisbatch0004RespDto.getCountNum();
                    }

                }
            }

            overdueTimes = String.valueOf(countNum);

            if (StringUtil.isNotEmpty(overdueTimes)) {
                xdtz0036DataRespDto.setOverdueTimes(overdueTimes);
            } else {
                xdtz0036DataRespDto.setOverdueTimes(SuccessEnum.CMIS_SUCCSESS.key);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataRespDto));
        return xdtz0036DataRespDto;
    }
}
