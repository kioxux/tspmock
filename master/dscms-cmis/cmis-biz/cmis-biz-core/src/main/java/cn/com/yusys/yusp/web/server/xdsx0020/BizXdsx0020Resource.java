package cn.com.yusys.yusp.web.server.xdsx0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0020.req.Xdsx0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.resp.Xdsx0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0020.Xdsx0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:担保公司合作协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0020:担保公司合作协议查询")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0020Resource.class);

    @Autowired
    private Xdsx0020Service  xdsx0020Service;

    /**
     * 交易码：xdsx0020
     * 交易描述：担保公司合作协议查询
     *
     * @param xdsx0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("担保公司合作协议查询")
    @PostMapping("/xdsx0020")
    protected @ResponseBody
    ResultDto<Xdsx0020DataRespDto> xdsx0020(@Validated @RequestBody Xdsx0020DataReqDto xdsx0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020DataReqDto));
        Xdsx0020DataRespDto xdsx0020DataRespDto = new Xdsx0020DataRespDto();// 响应Dto:担保公司合作协议查询
        ResultDto<Xdsx0020DataRespDto> xdsx0020DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0020DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdsx0020DataRespDto = xdsx0020Service.xdsx0020(xdsx0020DataReqDto);
            // TODO 调用XXXXXService层结束
            // 封装xdsx0020DataResultDto中正确的返回码和返回信息
            xdsx0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, e.getMessage());
            // 封装xdsx0020DataResultDto中异常返回码和返回信息
            xdsx0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0020DataRespDto到xdsx0020DataResultDto中
        xdsx0020DataResultDto.setData(xdsx0020DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020DataResultDto));
        return xdsx0020DataResultDto;
    }
}
