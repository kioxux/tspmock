/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: VisaXdRisk
 * @类描述: visa_xd_risk数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:50:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "visa_xd_risk")
public class VisaXdRisk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户信息流水号 **/
	@Column(name = "CRP_SERNO", unique = false, nullable = true, length = 40)
	private String crpSerno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 客户手机号码 **/
	@Column(name = "MOBILE_NO", unique = false, nullable = true, length = 20)
	private String mobileNo;
	
	/** 管户经理编号 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 10)
	private String managerId;
	
	/** 管户经理名称 **/
	@Column(name = "MANAGER_NAME", unique = false, nullable = true, length = 40)
	private String managerName;
	
	/** 面签时间 **/
	@Column(name = "SIGNATURE_TIME", unique = false, nullable = true, length = 40)
	private String signatureTime;
	
	/** 面签地址 **/
	@Column(name = "SIGNATURE_ADDR", unique = false, nullable = true, length = 80)
	private String signatureAddr;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param crpSerno
	 */
	public void setCrpSerno(String crpSerno) {
		this.crpSerno = crpSerno;
	}
	
    /**
     * @return crpSerno
     */
	public String getCrpSerno() {
		return this.crpSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
    /**
     * @return mobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	
    /**
     * @return managerName
     */
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param signatureTime
	 */
	public void setSignatureTime(String signatureTime) {
		this.signatureTime = signatureTime;
	}
	
    /**
     * @return signatureTime
     */
	public String getSignatureTime() {
		return this.signatureTime;
	}
	
	/**
	 * @param signatureAddr
	 */
	public void setSignatureAddr(String signatureAddr) {
		this.signatureAddr = signatureAddr;
	}
	
    /**
     * @return signatureAddr
     */
	public String getSignatureAddr() {
		return this.signatureAddr;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}