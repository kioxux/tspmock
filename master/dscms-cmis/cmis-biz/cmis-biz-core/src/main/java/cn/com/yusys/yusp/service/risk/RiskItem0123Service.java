package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.BailAccInfo;
import cn.com.yusys.yusp.domain.PvpAccpApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.XbdInfo;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.BailAccInfoService;
import cn.com.yusys.yusp.service.PvpAccpAppService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.XbdInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0123Service {

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    public RiskResultDto riskItem0123(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";
        if(Objects.nonNull(queryModel.getCondition().get("bizId"))){
            serno= queryModel.getCondition().get("bizId").toString();
        }
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
        List<BailAccInfo> list = bailAccInfoService.selectBySerno(serno);
        if (Objects.nonNull(pvpAccpApp) && Objects.nonNull(pvpAccpApp.getBailAmt()) && CollectionUtils.nonEmpty(list)) {
            for (BailAccInfo bailAccInfo : list) {
                if (Objects.isNull(bailAccInfo.getBailAmt()) || bailAccInfo.getBailAmt().compareTo(pvpAccpApp.getBailAmt()) != 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012302); //保证金信息缺失
                    return riskResultDto;
                }
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_012301); //保证金信息缺失
            return riskResultDto;
        }
        
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
