/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtApprLoanCond;
import cn.com.yusys.yusp.service.LmtApprLoanCondService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprLoanCondResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:21:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapprloancond")
public class LmtApprLoanCondResource {
    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtApprLoanCond>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtApprLoanCond> list = lmtApprLoanCondService.selectAll(queryModel);
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtApprLoanCond>> index(QueryModel queryModel) {
        List<LmtApprLoanCond> list = lmtApprLoanCondService.selectByModel(queryModel);
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtApprLoanCond> show(@PathVariable("pkId") String pkId) {
        LmtApprLoanCond lmtApprLoanCond = lmtApprLoanCondService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtApprLoanCond>(lmtApprLoanCond);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtApprLoanCond> create(@RequestBody LmtApprLoanCond lmtApprLoanCond) throws URISyntaxException {
        lmtApprLoanCondService.insert(lmtApprLoanCond);
        return new ResultDto<LmtApprLoanCond>(lmtApprLoanCond);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtApprLoanCond lmtApprLoanCond) throws URISyntaxException {
        int result = lmtApprLoanCondService.update(lmtApprLoanCond);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtApprLoanCondService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtApprLoanCondService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteLmtApprLoanCond
     * @函数描述:逻辑删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletelmtapprloancond")
    protected ResultDto<Map> deleteLmtApprLoanCond(@RequestBody String pkId) {
        Map rtnData = new HashMap();
        rtnData = lmtApprLoanCondService.deleteByPkId(pkId);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:saveLmtApprLoanCond
     * @函数描述:保存授信申请新增数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/savelmtapprloancond")
    protected ResultDto<Map> saveLmtApprLoanCond(@RequestBody LmtApprLoanCond lmtApprLoanCond) {
        Map map = lmtApprLoanCondService.saveLmtApprLoanCond(lmtApprLoanCond);
        return new ResultDto<>(map);
    }

    /**
     * @方法名称: queryLmtApproveLoanCondDataByApprSerno
     * @方法描述: 根据业务流水号查出该笔业务
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/querylmtapproveloanconddatabyapprserno")
    protected ResultDto<List<LmtApprLoanCond>> queryLmtApproveLoanCondDataByApprSerno(@RequestBody String apprSerno) {
        List<LmtApprLoanCond> list = lmtApprLoanCondService.queryLmtApproveLoanCondDataByApprSerno(apprSerno.replaceAll("\"", ""));
        return new ResultDto<List<LmtApprLoanCond>>(list);
    }

    /**
     * 条件查询
     * @param queryModel
     * @return
     */
    @PostMapping("/selectByQueryModel")
    protected  ResultDto<List<LmtApprLoanCond>> selectByQueryModel(@RequestBody QueryModel queryModel){
        return new ResultDto<List<LmtApprLoanCond>>(lmtApprLoanCondService.selectByModel(queryModel));
    }

    /**
     * 新增其他要求
     * @param model
     * @return
     */
    @PostMapping("/insertLoanCond")
    protected  ResultDto<Integer> insertLoanCond(@RequestBody QueryModel model){
        return new ResultDto<Integer>(lmtApprLoanCondService.insertLoanCond(model));
    }

    @PostMapping("/deleteByPkId")
    protected  ResultDto<Integer> deleteByPkId(@RequestBody LmtApprLoanCond lmtApprLoanCond){
        return new ResultDto<Integer>(lmtApprLoanCondService.deleteByPrimaryKey(lmtApprLoanCond.getPkId()));
    }

    /**
     * @函数名称:batchSaveLmtApprLoanCond
     * @函数描述:批量保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchsavelmtapprloancond")
    protected ResultDto<Integer> batchSaveLmtApprLoanCond(@RequestBody List<LmtApprLoanCond> list) {
        return ResultDto.success(lmtApprLoanCondService.batchSaveLmtApprLoanCond(list));
    }
}
