/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrHighAmtAgrCont
 * @类描述: ctr_high_amt_agr_cont数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-13 14:40:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CtrHighAmtAgrContDto extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 流水号 **/
	private String serno;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 合同编号 **/
	private String contNo;

	/** 中文合同编号 **/
	private String contCnNo;

	/** 业务类型 **/
	private String busiType;

	/** 合同类型 **/
	private String contType;

	/** 担保方式 **/
	private String guarMode;

	/** 协议币种 **/
	private String agrType;

	/** 协议金额 **/
	private java.math.BigDecimal agrAmt;

	/** 已用金额 **/
	private java.math.BigDecimal useAmt;

	/** 本协议项下最高可用信金额 **/
	private java.math.BigDecimal agrContHighAvlAmt;

	/** 协议期限 **/
	private Integer agrTerm;

	/** 协议起始日 **/
	private String startDate;

	/** 协议到期日 **/
	private String endDate;

	/** 纸质合同签订日期 **/
	private String paperContSignDate;

	/** 是否续签 **/
	private String isRenew;

	/** 原合同编号 **/
	private String origiContNo;

	/** 是否使用授信额度 **/
	private String isUtilLmt;

	/** 授信台账编号 **/
	private String lmtAccNo;

	/** 批复编号 **/
	private String replyNo;

	/** 其他约定 **/
	private String otherAgreed;

	/** 是否电子用印 **/
	private String isESeal;

	/** 是否在线抵押 **/
	private String isOlPld;

	/** 双录编号 **/
	private String doubleRecordNo;

	/** 合同状态 **/
	private String contStatus;

	/** 联系人 **/
	private String linkman;

	/** 电话 **/
	private String phone;

	/** 传真 **/
	private String fax;

	/** 邮箱 **/
	private String email;

	/** QQ **/
	private String qq;

	/** 微信 **/
	private String wechat;

	/** 送达地址 **/
	private String deliveryAddr;

	/** 操作类型 **/
	private String oprType;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 主管客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String managerBrId;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}

	/**
	 * @return contCnNo
	 */
	public String getContCnNo() {
		return this.contCnNo;
	}

	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}

	/**
	 * @return busiType
	 */
	public String getBusiType() {
		return this.busiType;
	}

	/**
	 * @param contType
	 */
	public void setContType(String contType) {
		this.contType = contType;
	}

	/**
	 * @return contType
	 */
	public String getContType() {
		return this.contType;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param agrType
	 */
	public void setAgrType(String agrType) {
		this.agrType = agrType;
	}

	/**
	 * @return agrType
	 */
	public String getAgrType() {
		return this.agrType;
	}

	/**
	 * @param agrAmt
	 */
	public void setAgrAmt(java.math.BigDecimal agrAmt) {
		this.agrAmt = agrAmt;
	}

	/**
	 * @return agrAmt
	 */
	public java.math.BigDecimal getAgrAmt() {
		return this.agrAmt;
	}

	/**
	 * @param agrContHighAvlAmt
	 */
	public void setAgrContHighAvlAmt(java.math.BigDecimal agrContHighAvlAmt) {
		this.agrContHighAvlAmt = agrContHighAvlAmt;
	}

	/**
	 * @return agrContHighAvlAmt
	 */
	public java.math.BigDecimal getAgrContHighAvlAmt() {
		return this.agrContHighAvlAmt;
	}

	/**
	 * @param agrTerm
	 */
	public void setAgrTerm(Integer agrTerm) {
		this.agrTerm = agrTerm;
	}

	/**
	 * @return agrTerm
	 */
	public Integer getAgrTerm() {
		return this.agrTerm;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param paperContSignDate
	 */
	public void setPaperContSignDate(String paperContSignDate) {
		this.paperContSignDate = paperContSignDate;
	}

	/**
	 * @return paperContSignDate
	 */
	public String getPaperContSignDate() {
		return this.paperContSignDate;
	}

	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}

	/**
	 * @return isRenew
	 */
	public String getIsRenew() {
		return this.isRenew;
	}

	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}

	/**
	 * @return origiContNo
	 */
	public String getOrigiContNo() {
		return this.origiContNo;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param otherAgreed
	 */
	public void setOtherAgreed(String otherAgreed) {
		this.otherAgreed = otherAgreed;
	}

	/**
	 * @return otherAgreed
	 */
	public String getOtherAgreed() {
		return this.otherAgreed;
	}

	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}

	/**
	 * @return isESeal
	 */
	public String getIsESeal() {
		return this.isESeal;
	}

	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}

	/**
	 * @return isOlPld
	 */
	public String getIsOlPld() {
		return this.isOlPld;
	}

	/**
	 * @param doubleRecordNo
	 */
	public void setDoubleRecordNo(String doubleRecordNo) {
		this.doubleRecordNo = doubleRecordNo;
	}

	/**
	 * @return doubleRecordNo
	 */
	public String getDoubleRecordNo() {
		return this.doubleRecordNo;
	}

	/**
	 * @param contStatus
	 */
	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	/**
	 * @return contStatus
	 */
	public String getContStatus() {
		return this.contStatus;
	}

	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	/**
	 * @return linkman
	 */
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return phone
	 */
	public String getPhone() {
		return this.phone;
	}

	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return fax
	 */
	public String getFax() {
		return this.fax;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}

	/**
	 * @return qq
	 */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	/**
	 * @return wechat
	 */
	public String getWechat() {
		return this.wechat;
	}

	/**
	 * @param deliveryAddr
	 */
	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}

	/**
	 * @return deliveryAddr
	 */
	public String getDeliveryAddr() {
		return this.deliveryAddr;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public BigDecimal getUseAmt() {
		return useAmt;
	}

	public void setUseAmt(BigDecimal useAmt) {
		this.useAmt = useAmt;
	}
}