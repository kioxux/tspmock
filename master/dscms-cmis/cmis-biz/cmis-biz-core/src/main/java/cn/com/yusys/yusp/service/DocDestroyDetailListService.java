/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.DocDestroyDetailList;
import cn.com.yusys.yusp.repository.mapper.DocDestroyDetailListMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocDestroyDetailListService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 20:16:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocDestroyDetailListService {

    @Autowired
    private DocDestroyDetailListMapper docDestroyDetailListMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public DocDestroyDetailList selectByPrimaryKey(String dddlSerno) {
        return docDestroyDetailListMapper.selectByPrimaryKey(dddlSerno);
    }

    /**
     * @方法名称: selectBydocNo
     * @方法描述: 根据档案编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocDestroyDetailList> selectBydocNo(String docNo) {
        return docDestroyDetailListMapper.selectBydocNo(docNo);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<DocDestroyDetailList> selectAll(QueryModel model) {
        List<DocDestroyDetailList> records = (List<DocDestroyDetailList>) docDestroyDetailListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<DocDestroyDetailList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocDestroyDetailList> list = docDestroyDetailListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(DocDestroyDetailList record) {
        return docDestroyDetailListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(DocDestroyDetailList record) {
        return docDestroyDetailListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(DocDestroyDetailList record) {
        return docDestroyDetailListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(DocDestroyDetailList record) {
        return docDestroyDetailListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dddlSerno) {
        return docDestroyDetailListMapper.deleteByPrimaryKey(dddlSerno);
    }

    /**
     * 根据档案销毁流水号进行删除
     * @param ddalSerno
     * @return
     */
    public int deleteByDdalSerno(String ddalSerno) {
        return docDestroyDetailListMapper.deleteByDdalSerno(ddalSerno);
    }

    /**
     * 根据档案销毁流水号进行更新：更新为已销毁
     * @param docNoList
     * @return
     */
    public int updateAppDestroyDetailStatus(List<String> docNoList,String status) {
        return docDestroyDetailListMapper.updateAppDestroyDetailStatus(docNoList,status);
    }

    /**
     * 根据档案销毁流水号进行查询：查询所有的档案编号
     * @param ddalSerno
     * @return
     */
    public List<String> selectDocNoListByDdalSerno(String ddalSerno) {
        return docDestroyDetailListMapper.selectDocNoListByDdalSerno(ddalSerno);
    }
}
