package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusLsnpInfo
 * @类描述: iqp_cus_lsnp_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-04 19:28:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCusLsnpInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 数字解读值 **/
	private String digIntVal;
	
	/** 数字解读值风险等级 **/
	private String digIntValRiskLvl;
	
	/** 申请评分 **/
	private String appScore;
	
	/** 申请评分风险等级 **/
	private String appScoreRiskLvl;
	
	/** 规则风险等级 **/
	private String ruleRiskLvl;
	
	/** 综合风险等级 **/
	private String inteRiskLvl;
	
	/** 额度建议 **/
	private java.math.BigDecimal lmtAdvice;
	
	/** 定价建议 **/
	private java.math.BigDecimal priceAdvice;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param digIntVal
	 */
	public void setDigIntVal(String digIntVal) {
		this.digIntVal = digIntVal == null ? null : digIntVal.trim();
	}
	
    /**
     * @return DigIntVal
     */	
	public String getDigIntVal() {
		return this.digIntVal;
	}
	
	/**
	 * @param digIntValRiskLvl
	 */
	public void setDigIntValRiskLvl(String digIntValRiskLvl) {
		this.digIntValRiskLvl = digIntValRiskLvl == null ? null : digIntValRiskLvl.trim();
	}
	
    /**
     * @return DigIntValRiskLvl
     */	
	public String getDigIntValRiskLvl() {
		return this.digIntValRiskLvl;
	}
	
	/**
	 * @param appScore
	 */
	public void setAppScore(String appScore) {
		this.appScore = appScore == null ? null : appScore.trim();
	}
	
    /**
     * @return AppScore
     */	
	public String getAppScore() {
		return this.appScore;
	}
	
	/**
	 * @param appScoreRiskLvl
	 */
	public void setAppScoreRiskLvl(String appScoreRiskLvl) {
		this.appScoreRiskLvl = appScoreRiskLvl == null ? null : appScoreRiskLvl.trim();
	}
	
    /**
     * @return AppScoreRiskLvl
     */	
	public String getAppScoreRiskLvl() {
		return this.appScoreRiskLvl;
	}
	
	/**
	 * @param ruleRiskLvl
	 */
	public void setRuleRiskLvl(String ruleRiskLvl) {
		this.ruleRiskLvl = ruleRiskLvl == null ? null : ruleRiskLvl.trim();
	}
	
    /**
     * @return RuleRiskLvl
     */	
	public String getRuleRiskLvl() {
		return this.ruleRiskLvl;
	}
	
	/**
	 * @param inteRiskLvl
	 */
	public void setInteRiskLvl(String inteRiskLvl) {
		this.inteRiskLvl = inteRiskLvl == null ? null : inteRiskLvl.trim();
	}
	
    /**
     * @return InteRiskLvl
     */	
	public String getInteRiskLvl() {
		return this.inteRiskLvl;
	}
	
	/**
	 * @param lmtAdvice
	 */
	public void setLmtAdvice(java.math.BigDecimal lmtAdvice) {
		this.lmtAdvice = lmtAdvice;
	}
	
    /**
     * @return LmtAdvice
     */	
	public java.math.BigDecimal getLmtAdvice() {
		return this.lmtAdvice;
	}
	
	/**
	 * @param priceAdvice
	 */
	public void setPriceAdvice(java.math.BigDecimal priceAdvice) {
		this.priceAdvice = priceAdvice;
	}
	
    /**
     * @return PriceAdvice
     */	
	public java.math.BigDecimal getPriceAdvice() {
		return this.priceAdvice;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}