package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AccCommonDto;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrHighAmtAgrContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpHighAmtAgrAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import cn.com.yusys.yusp.web.rest.CtrLoanContResource;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrHighAmtAgrContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-12 14:34:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrHighAmtAgrContService {

    private static final Logger log = LoggerFactory.getLogger(CtrLoanContResource.class);

    @Autowired
    private CtrHighAmtAgrContMapper ctrHighAmtAgrContMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private IqpHighAmtAgrAppMapper iqpHighAmtAgrAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrHighAmtAgrCont selectByPrimaryKey(String pkId) {
        return ctrHighAmtAgrContMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrHighAmtAgrCont> selectAll(QueryModel model) {
        List<CtrHighAmtAgrCont> records = (List<CtrHighAmtAgrCont>) ctrHighAmtAgrContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CtrHighAmtAgrCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrHighAmtAgrCont> list = ctrHighAmtAgrContMapper.selectByModel(model);

        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CtrHighAmtAgrCont record) {
        return ctrHighAmtAgrContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insertSelective(CtrHighAmtAgrCont record) {
        return ctrHighAmtAgrContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CtrHighAmtAgrCont record) {
        return ctrHighAmtAgrContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(CtrHighAmtAgrCont record) {
        return ctrHighAmtAgrContMapper.updateByPrimaryKeySelective(record);
    }

    @Transactional
    public int updateLmtAccNoByContNo(CtrHighAmtAgrCont record) {
        return ctrHighAmtAgrContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: selectDataByContNo
     * @方法描述: 根据合同号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CtrHighAmtAgrCont selectDataByContNo(String contNo) {
        return ctrHighAmtAgrContMapper.selectDataByContNo(contNo);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return ctrHighAmtAgrContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return ctrHighAmtAgrContMapper.deleteByIds(ids);
    }

    public static void main(String[] args) {
        String time = "2021-09-09 123123123";
        System.out.println(time.substring(0, 10));
    }
    /**
     * @方法名称: contSign
     * @方法描述: 合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map contSign(@RequestBody Map map) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) map.get("serno");
            if (StringUtils.isBlank(serno)) {
//                throw BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
                rtnCode = EcbEnum.E_IQP_NOT_EXISTS_FAILED.key;
                rtnMsg = EcbEnum.E_IQP_NOT_EXISTS_FAILED.value;
                return result;
            }
            if (Objects.nonNull(map.get("paperContSignDate")) && StringUtils.isNotBlank(map.get("paperContSignDate").toString())) {
                String paperContSignDate = map.get("paperContSignDate").toString();
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate openDayLocal = LocalDate.parse(openDay);
                LocalDate paperContSignDateLocal = LocalDate.parse(paperContSignDate.substring(0, 10));
                if (paperContSignDateLocal.isAfter(openDayLocal)) {
                    rtnCode = EcbEnum.E_IQP_NOT_EXISTS_FAILED.key;
                    rtnMsg = "纸质合同日期必须小于等于当前日期!";
                    return result;
                }
            } else {
                rtnCode = EcbEnum.E_IQP_NOT_EXISTS_FAILED.key;
                rtnMsg = "纸质合同日期必须小于等于当前日期!";
                return result;
            }

            String logPrefix = "合同签订" + serno;

            log.info(logPrefix + "获取合同签订数据");
            CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContMapper.selectByIqpSerno(serno);
            if (ctrHighAmtAgrCont == null) {
//                throw  BizException.error(null, EcbEnum.E_IQP_NOT_EXISTS_FAILED.key, EcbEnum.E_IQP_NOT_EXISTS_FAILED.value);
                rtnCode = EcbEnum.E_IQP_NOT_EXISTS_FAILED.key;
                rtnMsg = EcbEnum.E_IQP_NOT_EXISTS_FAILED.value;
                return result;
            }
            List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.selectByLmtSerno(ctrHighAmtAgrCont.getSerno());
            if (CollectionUtils.isEmpty(list) && !CmisCommonConstants.GUAR_MODE_00.equals(ctrHighAmtAgrCont.getGuarMode())) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_GUARRELNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_GUARRELNULL_EXCEPTION.value;
                return result;
            }
            //担保合同签订生效
            for (GrtGuarBizRstRel grtGuarBizRstRel : list) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);

                // 信贷业务与押品关联关系信息同步
                HashMap queryMap = new HashMap();
                queryMap.put("bizType", CmisCommonConstants.STD_BUSI_TYPE_02);
                queryMap.put("isFlag", "1");
                queryMap.put("bizSerno", ctrHighAmtAgrCont.getSerno());
                queryMap.put("guarContNo", grtGuarCont.getGuarContNo());
                guarBaseInfoService.buscon(queryMap);
            }

            log.info(logPrefix + "合同签订-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                ctrHighAmtAgrCont.setInputId(userInfo.getLoginCode());
                ctrHighAmtAgrCont.setInputBrId(userInfo.getOrg().getCode());
                ctrHighAmtAgrCont.setManagerId(userInfo.getLoginCode());
                ctrHighAmtAgrCont.setManagerBrId(userInfo.getOrg().getCode());
                ctrHighAmtAgrCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ctrHighAmtAgrCont.setContStatus(CmisCommonConstants.CONT_STATUS_200);
                ctrHighAmtAgrCont.setPaperContSignDate((String) map.get("paperContSignDate"));
            }
            log.info(logPrefix + "保存最高额授信协议数据");
            int updCount = ctrHighAmtAgrContMapper.updateByPrimaryKeySelective(ctrHighAmtAgrCont);
            // 判断合同签订状态是否修改成功
            if (updCount < 1) {
                throw BizException.error(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

            // 判断是否为合同续签,若为续签合同则恢复原合同的占额
            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrHighAmtAgrCont.getIsRenew())){
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrHighAmtAgrCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(ctrHighAmtAgrCont.getOrigiContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
                cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
                cmisLmt0012ReqDto.setInputId(ctrHighAmtAgrCont.getInputId());//登记人
                cmisLmt0012ReqDto.setInputBrId(ctrHighAmtAgrCont.getInputBrId());//登记机构
                cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                log.info("最高额授信协议【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrHighAmtAgrCont.getContNo(), ctrHighAmtAgrCont.getOrigiContNo(),JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("最高额授信协议【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrHighAmtAgrCont.getContNo(), ctrHighAmtAgrCont.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
                if (!"0".equals(resultDtoDto.getCode())) {
                    log.error("最高额授信协议【{}】签订异常",ctrHighAmtAgrCont.getContNo());
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB019999.value;
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                    rtnCode = code;
                    rtnMsg = resultDtoDto.getData().getErrorCode();
                    throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
                }
            }
            // 根据业务流水号同步押品与业务关联信息
            // TODO (逻辑需要调整)guarBusinessRelService.sendBuscon(ctrHighAmtAgrCont.getSerno(), CmisCommonConstants.STD_BUSI_TYPE_02);
            // 信贷业务合同信息同步
            guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_01, ctrHighAmtAgrCont.getContNo());
            //合同申请选择续签时，新签订成功后，原合同作废
            if (CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrHighAmtAgrCont.getIsRenew())) {
                CtrHighAmtAgrCont ctrHighAmtAgrContData = this.selectDataByContNo(ctrHighAmtAgrCont.getOrigiContNo());
                ctrHighAmtAgrContData.setContStatus(CmisCommonConstants.CONT_STATUS_800);
                ctrHighAmtAgrContMapper.updateByPrimaryKeySelective(ctrHighAmtAgrContData);
            }
            log.info("原合同：" + ctrHighAmtAgrCont.getOrigiContNo() + "作废成功");

        } catch (Exception e) {
            log.error("保存保存最高额授信协议" + serno + "异常", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value;
//            throw BizException.error(EcbEnum.IQP_EXCEPTION_DEF.key, EcbEnum.IQP_EXCEPTION_DEF.value);
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: contLogout
     * @方法描述: 合同注销
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map contLogout(CtrHighAmtAgrCont ctrHighAmtAgrCont) {
        {
            Map result = new HashMap();
            String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
            String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
            String contNo = ctrHighAmtAgrCont.getContNo();//合同编号
            String contStatus = ctrHighAmtAgrCont.getContStatus();//合同状态
            int pvpLoanAppCount = 0;
            int accLoanCount = 0;
            try {
                // 判断合同下是否存在未结清的业务
                String responseResult = ctrLoanContService.isContUncleared(contNo);
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                    rtnCode = EcbEnum.ECB020055.key;
                    rtnMsg = EcbEnum.ECB020055.value;
                    return result;
                }
                // 获取合同注销后的合同状态
                String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrHighAmtAgrCont.getEndDate(),contStatus);
                log.info("获取合同注销后的合同状态【{}】",finalContStatus);
                User userInfo = SessionUtils.getUserInformation();
                log.info("获取当前登录人信息【{}】",userInfo.toString());
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                if(Objects.nonNull(userInfo)){
                    ctrHighAmtAgrCont.setUpdId(userInfo.getLoginCode());
                    ctrHighAmtAgrCont.setUpdBrId(userInfo.getOrg().getCode());
                }
                ctrHighAmtAgrCont.setUpdDate(nowDate);
                ctrHighAmtAgrCont.setContStatus(finalContStatus);
                updateSelective(ctrHighAmtAgrCont);

                Boolean isRecoverLmt = this.isRecoverLmt(ctrHighAmtAgrCont.getContNo());
                if(isRecoverLmt==false){
                    rtnCode = EcbEnum.ECB020034.key;
                    rtnMsg = EcbEnum.ECB020034.value;
                    throw BizException.error(null,EcbEnum.ECB020034.key,EcbEnum.ECB020034.value);
                }

                //不存在电子用印的情况下，需要作废档案归档任务
                if (!"1".equals(ctrHighAmtAgrCont.getIsESeal())) {
                    log.info("根据业务申请编号【" + ctrHighAmtAgrCont.getSerno() + "】需要作废档案归档任务开始");
                    try {
                        docArchiveInfoService.invalidByContNo(ctrHighAmtAgrCont.getSerno());
                    } catch (Exception e) {
                        log.info("根据业务申请编号【" + ctrHighAmtAgrCont.getSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                    }
                    log.info("根据业务申请编号【" + ctrHighAmtAgrCont.getSerno() + "】需要作废档案归档任务结束");
                }

            } catch (Exception e) {
                log.error("最高额贷款合同申请注销异常", e.getMessage(), e);
                rtnCode = EcbEnum.ECB020034.key;
                rtnMsg = EcbEnum.ECB020034.value;
            } finally {
                //成功则无需处理
                result.put("rtnCode", rtnCode);
                result.put("rtnMsg", rtnMsg);
            }
            return result;
        }
    }

    /**
     * @方法名称: 合同注销方法
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Boolean isRecoverLmt(@RequestBody String contNO) {
        Boolean result = true;
        CtrHighAmtAgrCont ctrHighAmtAgrCont = this.selectDataByContNo(contNO);
        //合同注销后，恢复占额
        String guarMode = ctrHighAmtAgrCont.getGuarMode();
        //恢复敞口
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
        //恢复总额
        BigDecimal recoverAmtCny = BigDecimal.ZERO;
        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            recoverSpacAmtCny = BigDecimal.ZERO;
            recoverAmtCny = ctrHighAmtAgrCont.getAgrContHighAvlAmt();
        } else {
            recoverSpacAmtCny = ctrHighAmtAgrCont.getAgrContHighAvlAmt();
            recoverAmtCny = ctrHighAmtAgrCont.getAgrContHighAvlAmt();
        }
        //合同注销后，释放额度
        IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppMapper.selectByHighAmtAgrSernoKey(ctrHighAmtAgrCont.getSerno());
        // 根据合是否存在业务，得到恢复类型
        String recoverType = "";
        Map map = new HashMap();
        map.put("contNo", iqpHighAmtAgrApp.getContNo());
        List<AccLoan> accLoanList = accLoanService.selectAccLoanByContNo(map);
        if(CollectionUtils.nonEmpty(accLoanList)){
            //结清注销
            for (AccLoan accLoan : accLoanList) {
                if(CmisCommonConstants.ACC_STATUS_7.equals(accLoan.getAccStatus())){
                    recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                }else{
                    recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                    break;
                }
            }
        }else {
            //未用注销
            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
        }
        CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
        cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrHighAmtAgrCont.getManagerBrId()));//金融机构代码
        cmisLmt0012ReqDto.setBizNo(ctrHighAmtAgrCont.getContNo());//合同编号
        cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
        cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
        cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
        cmisLmt0012ReqDto.setInputId(iqpHighAmtAgrApp.getInputId());
        cmisLmt0012ReqDto.setInputBrId(iqpHighAmtAgrApp.getInputBrId());
        cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        log.info("最高额授信协议【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", ctrHighAmtAgrCont.getContNo(), cmisLmt0012ReqDto.toString());
        ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
        log.info("最高额授信协议【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", ctrHighAmtAgrCont.getContNo(), resultDto.toString());
        if(!"0".equals(resultDto.getCode())){
            log.error("合同: "+contNO+" 恢复额度异常！");
            throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        String code = resultDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.error("合同: "+contNO+" 恢复额度异常！");
            result = false;
        }
        return result;
    }

    /**
     * @方法名称: 普通贷款合同重签
     * @方法描述:
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map contReSign(@RequestBody Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        String contNo = "";
        int pvpLoanAppCount = 0;
        int accLoanCount = 0;
        try {
            // 更新当前登录信息
            User userInfo = SessionUtils.getUserInformation();
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            HashMap<String, String> queryData = new HashMap<String, String>();
            contNo = (String) params.get("contNo");//主键合同编号
            queryData.put("contNo", contNo);
            CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContMapper.queryCtrHighAmtAgrContDataByParams(queryData);
            Map pvpQueryMap = new HashMap();
            pvpQueryMap.put("contNo", contNo);
            pvpLoanAppCount = pvpLoanAppMapper.countPvpLoanAppCountByContNo(pvpQueryMap);
            accLoanCount = accLoanMapper.countAccLoanCountByContNo(contNo);
            if (pvpLoanAppCount > 0) {
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_PVP.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_PVP.value;
            } else if (accLoanCount > 0) {
                rtnCode = EcbEnum.ON_RESIGN_EXCEPTION_ACC.key;
                rtnMsg = EcbEnum.ON_RESIGN_EXCEPTION_ACC.value;
            } else {
                ctrHighAmtAgrCont.setUpdId(userInfo.getLoginCode());
                ctrHighAmtAgrCont.setUpdBrId(userInfo.getOrg().getCode());
                ctrHighAmtAgrCont.setUpdDate(nowDate);
                ctrHighAmtAgrCont.setPaperContSignDate("");//纸质合同签订日期为空
                ctrHighAmtAgrCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);
                updateSelective(ctrHighAmtAgrCont);
            }
        } catch (Exception e) {
            log.error("最高额贷款合同申请重签异常", e.getMessage(), e);
            rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
        } finally {
            //成功则无需处理
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrHighAmtAgrCont> toSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        return ctrHighAmtAgrContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrHighAmtAgrCont> doneSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("contStatusOther", CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrHighAmtAgrContMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param params
     * @return
     */
    public CtrHighAmtAgrCont queryCtrHighAmtAgrContDataByParams(HashMap<String, String> params) {
        return ctrHighAmtAgrContMapper.queryCtrHighAmtAgrContDataByParams(params);
    }

    /**
     * @return
     * @方法名称: selectCtrCont
     * @方法描述: 通过查询合同选择列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccCommonDto> queryAccInfo(HashMap<String, String> queryMap) {
        List<AccCommonDto> ctrList = ctrHighAmtAgrContMapper.queryAccInfoByParams(queryMap);
        return ctrList;
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrHighAmtAgrCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrHighAmtAgrContMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 14:47
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrHighAmtAgrCont selectByIqpSerno(String serno) {
        return ctrHighAmtAgrContMapper.selectByIqpSerno(serno);
    }

    /**
     * @return
     * @方法名称: selectCtrCont
     * @方法描述: 最高额授信申请新增弹框中通过查询合同选择列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrLoanContDto> selectCtrCont(QueryModel model) {
        List<CtrLoanContDto> ctrList = ctrLoanContService.selectCtrCont(model);
        return ctrList;
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrHighAmtAgrCont> selectByQuerymodel(QueryModel model) {
        return ctrHighAmtAgrContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectDataByCusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrHighAmtAgrCont> selectDataByCusId(String cusId) {
        return ctrHighAmtAgrContMapper.selectByLmtAccNo(cusId);
    }
}
