/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpAppSubDto
 * @类描述: lmt_app_sub数据实体类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-04-19 21:43:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtGrpAppSubDto extends BaseDomain implements Serializable {

    private static final long serialVersionUID = 1L;

	public String getSingleSerno() {
		return singleSerno;
	}

	public void setSingleSerno(String singleSerno) {
		this.singleSerno = singleSerno;
	}

	// 单一客户授信流水号
	private String singleSerno;

    // 客户编号
	private String cusId;

	// 客户名称
	private String cusName;

	// 分项品种流水号
	private String subPrdSerno;

	// 原额度台账分项品种编号
	private String origiLmtAccSubPrdNo;

	// 授信品种编号
	private String lmtBizType;

	// 授信品种编号名称
	private String lmtBizTypeName;

	// 是否循环额度
	private String isRevolvLimit;

	// 是否预授信额度
	private String isPreLmt;

	// 担保方式
	private String guarMode;

	// 原额度台账分项品种金额
	private BigDecimal origiLmtAccSubPrdAmt;

	// 授信额度
	private BigDecimal lmtAmt;

	// 原额度台账分项品种期限
	private Integer origiLmtAccSubPrdTerm;

	// 授信期限
	private Integer lmtTerm;

	// 变更标志
	private String chgFlag;

	// 是否存量授信标志
	private String isSfcaLmt;

	// 是否本次细化
	private String isCurtRefine;

	// 币种
	private String curType;

	// 年利率
	private String rateYear;

	// 最低保证金比例
	private String bailPreRate;

	// 授信是否可调剂
	private String isAdjustFlag;

	// 产品类型属性
	private String lmtBizTypeProp;

	public String getLmtBizTypeProp() {
		return lmtBizTypeProp;
	}

	public void setLmtBizTypeProp(String lmtBizTypeProp) {
		this.lmtBizTypeProp = lmtBizTypeProp;
	}

	public String getIsAdjustFlag() {
		return isAdjustFlag;
	}

	public void setIsAdjustFlag(String isAdjustFlag) {
		this.isAdjustFlag = isAdjustFlag;
	}



	// 子节点
	private List<LmtGrpAppSubDto> childrenLmtGrpAppSubDtoList;

	public String getCurType() {
		return curType;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public String getRateYear() {
		return rateYear;
	}

	public void setRateYear(String rateYear) {
		this.rateYear = rateYear;
	}

	public String getBailPreRate() {
		return bailPreRate;
	}

	public void setBailPreRate(String bailPreRate) {
		this.bailPreRate = bailPreRate;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getSubPrdSerno() {
		return subPrdSerno;
	}

	public void setSubPrdSerno(String subPrdSerno) {
		this.subPrdSerno = subPrdSerno;
	}

	public String getOrigiLmtAccSubPrdNo() {
		return origiLmtAccSubPrdNo;
	}

	public void setOrigiLmtAccSubPrdNo(String origiLmtAccSubPrdNo) {
		this.origiLmtAccSubPrdNo = origiLmtAccSubPrdNo;
	}

	public String getLmtBizType() {
		return lmtBizType;
	}

	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType;
	}

	public String getLmtBizTypeName() {
		return lmtBizTypeName;
	}

	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName;
	}

	public String getIsRevolvLimit() {
		return isRevolvLimit;
	}

	public void setIsRevolvLimit(String isRevolvLimit) {
		this.isRevolvLimit = isRevolvLimit;
	}

	public String getIsPreLmt() {
		return isPreLmt;
	}

	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt;
	}

	public String getGuarMode() {
		return guarMode;
	}

	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	public BigDecimal getOrigiLmtAccSubPrdAmt() {
		return origiLmtAccSubPrdAmt;
	}

	public void setOrigiLmtAccSubPrdAmt(BigDecimal origiLmtAccSubPrdAmt) {
		this.origiLmtAccSubPrdAmt = origiLmtAccSubPrdAmt;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public Integer getOrigiLmtAccSubPrdTerm() {
		return origiLmtAccSubPrdTerm;
	}

	public void setOrigiLmtAccSubPrdTerm(Integer origiLmtAccSubPrdTerm) {
		this.origiLmtAccSubPrdTerm = origiLmtAccSubPrdTerm;
	}

	public Integer getLmtTerm() {
		return lmtTerm;
	}

	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	public String getChgFlag() {
		return chgFlag;
	}

	public void setChgFlag(String chgFlag) {
		this.chgFlag = chgFlag;
	}

	public String getIsSfcaLmt() {
		return isSfcaLmt;
	}

	public void setIsSfcaLmt(String isSfcaLmt) {
		this.isSfcaLmt = isSfcaLmt;
	}

	public String getIsCurtRefine() {
		return isCurtRefine;
	}

	public void setIsCurtRefine(String isCurtRefine) {
		this.isCurtRefine = isCurtRefine;
	}

	public List<LmtGrpAppSubDto> getChildrenLmtGrpAppSubDtoList() {
		return childrenLmtGrpAppSubDtoList;
	}

	public void setChildrenLmtGrpAppSubDtoList(List<LmtGrpAppSubDto> childrenLmtGrpAppSubDtoList) {
		this.childrenLmtGrpAppSubDtoList = childrenLmtGrpAppSubDtoList;
	}
}