package cn.com.yusys.yusp.service.client.batch.cmisbatch0004;


import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0004.Cmisbatch0004RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询[贷款账户主表]和[贷款账户还款表]关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class CmisBatch0004Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisBatch0004Service.class);

    // 1）注入：封装的接口类:批量日终管理模块
    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 业务逻辑处理方法：查询[贷款账户主表]和[贷款账户还款表]关联信息
     *
     * @param cmisbatch0004ReqDto
     * @return
     */
    @Transactional
    public Cmisbatch0004RespDto cmisbatch0004(Cmisbatch0004ReqDto cmisbatch0004ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004ReqDto));
        ResultDto<Cmisbatch0004RespDto> cmisbatch0004ResultDto = cmisBatchClientService.cmisbatch0004(cmisbatch0004ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value, JSON.toJSONString(cmisbatch0004ResultDto));

        String cmisbatch0004Code = Optional.ofNullable(cmisbatch0004ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisbatch0004Meesage = Optional.ofNullable(cmisbatch0004ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Cmisbatch0004RespDto cmisbatch0004RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0004ResultDto.getCode())) {
            //  获取相关的值并解析
            cmisbatch0004RespDto = cmisbatch0004ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, cmisbatch0004Code, cmisbatch0004Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBATCH0004.key, DscmsEnum.TRADE_CODE_CMISBATCH0004.value);
        return cmisbatch0004RespDto;
    }
}
