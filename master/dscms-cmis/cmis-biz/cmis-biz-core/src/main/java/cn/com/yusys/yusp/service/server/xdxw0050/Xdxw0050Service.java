package cn.com.yusys.yusp.service.server.xdxw0050;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0050.req.Xdxw0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0050.resp.Xdxw0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyConInfoMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0036Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0050Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0050Service.class);
    @Autowired
    private LmtSurveyConInfoMapper lmtSurveyConInfoMapper;

    /**
     * 查询优抵贷调查结论
     *
     * @param Xdxw0050DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0050DataRespDto xdxw0050(Xdxw0050DataReqDto Xdxw0050DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value);
        Xdxw0050DataRespDto Xdxw0050DataRespDto = new Xdxw0050DataRespDto();
        String applySerno = Xdxw0050DataReqDto.getApplySerno();//业务编号
        String queryType = Xdxw0050DataReqDto.getQueryType();//查询类型 01-按授信调查流水号查询;
        String indgtSerno = Xdxw0050DataReqDto.getIndgtSerno();//
        String subject = Xdxw0050DataReqDto.getSubject();//
        try {
            //根据客户调查表编号查询调查表信息
            Map queryMap = new HashMap();
            queryMap.put("applySerno", applySerno);
            queryMap.put("indgtSerno", indgtSerno);
            queryMap.put("subject", subject);

            BigDecimal curtAmtTotal = new BigDecimal(0);
            if (DscmsBizXwEnum.QUERY_TYPE_01.key.equals(queryType)) {//01-按授信调查流水号查询;
                curtAmtTotal = lmtSurveyConInfoMapper.selectCurtAmtBySurveySerno(queryMap);
            } else {
                curtAmtTotal = lmtSurveyConInfoMapper.selectCurtAmtByIndgtSerno(queryMap);
            }
            if (curtAmtTotal != null) {
                Xdxw0050DataRespDto.setCurtAmtTotal(curtAmtTotal.toString());
            } else {
                Xdxw0050DataRespDto.setCurtAmtTotal(StringUtils.EMPTY);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value);
        return Xdxw0050DataRespDto;
    }
}
