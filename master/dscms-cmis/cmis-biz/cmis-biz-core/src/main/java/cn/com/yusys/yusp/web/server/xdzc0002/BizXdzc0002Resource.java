package cn.com.yusys.yusp.web.server.xdzc0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0002.req.Xdzc0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.resp.Xdzc0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0002.Xdzc0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户资产池协议维护（协议激活）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0002:客户资产池协议维护（协议激活）")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0002Resource.class);

    @Autowired
    private Xdzc0002Service xdzc0002Service;

    /**
     * 交易码：xdzc0002
     * 交易描述：客户资产池协议维护（协议激活）
     *
     * @param xdzc0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产池协议维护（协议激活）")
    @PostMapping("/xdzc0002")
    protected @ResponseBody
    ResultDto<Xdzc0002DataRespDto> xdzc0002(@Validated @RequestBody Xdzc0002DataReqDto xdzc0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002DataReqDto));
        Xdzc0002DataRespDto xdzc0002DataRespDto = new Xdzc0002DataRespDto();// 响应Dto:客户资产池协议维护（协议激活）
        ResultDto<Xdzc0002DataRespDto> xdzc0002DataResultDto = new ResultDto<>();
		String contNo = xdzc0002DataReqDto.getContNo();//协议编号
        try {
            // 从xdzc0002DataReqDto获取业务值进行业务逻辑处理
            xdzc0002DataRespDto = xdzc0002Service.xdzc0002Service(xdzc0002DataReqDto);
            // 封装xdzc0002DataResultDto中正确的返回码和返回信息
            xdzc0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, e.getMessage());
            // 封装xdzc0002DataResultDto中异常返回码和返回信息
            xdzc0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0002DataRespDto到xdzc0002DataResultDto中
        xdzc0002DataResultDto.setData(xdzc0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002DataResultDto));
        return xdzc0002DataResultDto;
    }
}
