package cn.com.yusys.yusp.service.server.xdsx0018;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.FdyddWhbxdApproval;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.req.CmisLmt0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0021.resp.CmisLmt0021RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.req.Xdsx0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.resp.Xdsx0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.req.Xdsx0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.resp.Xdsx0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.FdyddWhbxdApprovalMapper;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0021.CmisLmt0021Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 接口处理类:房抵e点贷无还本续贷审核
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdsx0018Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0018Service.class);

    @Autowired
    private CommonService commonService;
    @Autowired
    private FdyddWhbxdApprovalMapper fdyddWhbxdApprovalMapper;//房抵e点贷无还本续贷审核
    /**
     * 查询对公客户授信信息
     *
     * @param xdsx0018DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdsx0018DataRespDto Xdsx0018(Xdsx0018DataReqDto xdsx0018DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value);
        //返回对象
        Xdsx0018DataRespDto xdsx0018DataRespDto = new Xdsx0018DataRespDto();
        try {
            if("".equals(xdsx0018DataReqDto.getSerno())){
                xdsx0018DataRespDto.setOpFlag("F");
                xdsx0018DataRespDto.setOpMsg("流水号不能为空");
                return xdsx0018DataRespDto;
            }else{
                FdyddWhbxdApproval fdyddWhbxdApproval = new FdyddWhbxdApproval();
                //流水号
                fdyddWhbxdApproval.setSerno(xdsx0018DataReqDto.getSerno());
                //客户号
                fdyddWhbxdApproval.setCusId(xdsx0018DataReqDto.getCus_id());
                //客户名称
                fdyddWhbxdApproval.setCusName(xdsx0018DataReqDto.getCus_name());
                //合同编号
                fdyddWhbxdApproval.setContNo(xdsx0018DataReqDto.getCont_no());
                //中文合同编号
                fdyddWhbxdApproval.setCnContNo(xdsx0018DataReqDto.getCn_cont_no());
                //客户经理号
                fdyddWhbxdApproval.setManagerId(xdsx0018DataReqDto.getManager_id());
                //客户经理名称
                fdyddWhbxdApproval.setManagerName(xdsx0018DataReqDto.getManager_name());
                //旧借据编号
                fdyddWhbxdApproval.setOldBillNo(xdsx0018DataReqDto.getOld_bill_no());
                //新借据编号
                fdyddWhbxdApproval.setNewBillNo(xdsx0018DataReqDto.getNew_bill_no());
                //新借据金额
                fdyddWhbxdApproval.setNewLoanAmount(xdsx0018DataReqDto.getNew_loan_amount());
                //新借据期限
                fdyddWhbxdApproval.setNewBillTime(xdsx0018DataReqDto.getNew_bill_time());
                //新借据起始日
                fdyddWhbxdApproval.setNewStartDate(xdsx0018DataReqDto.getNew_start_date());
                //新借据到期日
                fdyddWhbxdApproval.setNewEndDate(xdsx0018DataReqDto.getNew_end_date());
                //新借据审批利率
                fdyddWhbxdApproval.setNewBillChackRate(xdsx0018DataReqDto.getNew_bill_chack_rate());
                //新借据放款/还款卡号
                fdyddWhbxdApproval.setNewBillCardNum(xdsx0018DataReqDto.getNew_bill_card_num());
                //默认审批状态为待发起
                fdyddWhbxdApproval.setApproveStatus("000");
                //插入房抵e点贷无还本续贷审核表中
                fdyddWhbxdApprovalMapper.insertSelective(fdyddWhbxdApproval);
                xdsx0018DataRespDto.setOpFlag("S");
                xdsx0018DataRespDto.setOpMsg("交易成功");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value);
        return xdsx0018DataRespDto;
    }
}
