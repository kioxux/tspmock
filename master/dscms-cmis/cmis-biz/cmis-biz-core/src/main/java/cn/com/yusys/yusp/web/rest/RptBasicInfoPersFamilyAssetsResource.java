/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptBasicInfoPersFamilyAssets;
import cn.com.yusys.yusp.service.RptBasicInfoPersFamilyAssetsService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoPersFamilyAssetsResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-10 16:54:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptbasicinfopersfamilyassets")
public class RptBasicInfoPersFamilyAssetsResource {
    @Autowired
    private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptBasicInfoPersFamilyAssets> list = rptBasicInfoPersFamilyAssetsService.selectAll(queryModel);
        return new ResultDto<List<RptBasicInfoPersFamilyAssets>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> index(QueryModel queryModel) {
        List<RptBasicInfoPersFamilyAssets> list = rptBasicInfoPersFamilyAssetsService.selectByModel(queryModel);
        return new ResultDto<List<RptBasicInfoPersFamilyAssets>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptBasicInfoPersFamilyAssets> show(@PathVariable("pkId") String pkId) {
        RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets = rptBasicInfoPersFamilyAssetsService.selectByPrimaryKey(pkId);
        return new ResultDto<RptBasicInfoPersFamilyAssets>(rptBasicInfoPersFamilyAssets);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptBasicInfoPersFamilyAssets> create(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets) throws URISyntaxException {
        rptBasicInfoPersFamilyAssetsService.insert(rptBasicInfoPersFamilyAssets);
        return new ResultDto<RptBasicInfoPersFamilyAssets>(rptBasicInfoPersFamilyAssets);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets) throws URISyntaxException {
        int result = rptBasicInfoPersFamilyAssetsService.update(rptBasicInfoPersFamilyAssets);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptBasicInfoPersFamilyAssetsService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptBasicInfoPersFamilyAssetsService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * 条件查询
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptBasicInfoPersFamilyAssets>>(rptBasicInfoPersFamilyAssetsService.selectByModel(model));
    }

    @PostMapping("/delectFamily")
    protected  ResultDto<Integer> delectFamily(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets){
        String pkId = rptBasicInfoPersFamilyAssets.getPkId();
        return new ResultDto<Integer>(rptBasicInfoPersFamilyAssetsService.deleteByPrimaryKey(pkId));
    }
    @PostMapping("/addFamily")
    protected ResultDto<Integer> addFamily(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets){
        rptBasicInfoPersFamilyAssets.setPkId(StringUtils.getUUID());
        return  new ResultDto<Integer>(rptBasicInfoPersFamilyAssetsService.insertSelective(rptBasicInfoPersFamilyAssets));
    }
    @PostMapping("/updateFamily")
    protected ResultDto<Integer> updateFamily(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets){
        return  new ResultDto<Integer>(rptBasicInfoPersFamilyAssetsService.updateSelective(rptBasicInfoPersFamilyAssets));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAssets){
        return  new ResultDto<Integer>(rptBasicInfoPersFamilyAssetsService.save(rptBasicInfoPersFamilyAssets));
    }
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptBasicInfoPersFamilyAssets>> selectBySerno(@RequestBody Map map){
        String serno = map.get("serno").toString();
        return new ResultDto<List<RptBasicInfoPersFamilyAssets>>(rptBasicInfoPersFamilyAssetsService.selectBySerno(serno));
    }
}
