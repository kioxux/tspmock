package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @描述：所有合同类型的字段并集
 * @创建人：zhangming12
 * @创建时间：2021/5/20 9:23
 * @修改记录：修改时间 修改人员 修改时间
 */
public class AllContDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 合同类型判断
     **/
    private String contTypeOpr;

    /**
     * 合同编号
     **/
    private String contNo;

    /**
     * 中文合同编号
     **/
    private String contCnNo;

    /**
     * 调查编号
     **/
    private String surveySerno;

    /**
     * 业务申请流水号
     **/
    private String iqpSerno;

    /**
     * 申请流水号
     **/
    private String serno;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 产品编号
     **/
    private String prdId;

    /**
     * 产品名称
     **/
    private String prdName;

    /**
     * 证件类型
     **/
    private String certType;

    /**
     * 证件号码
     **/
    private String certCode;

    /**
     * 手机号码
     **/
    private String phone;

    /**
     * 业务类型
     **/
    private String bizType;

    /**
     * 授信额度编号
     **/
    private String lmtAccNo;

    /**
     * 特殊业务类型
     **/
    private String especBizType;

    /**
     * 贷款用途
     **/
    private String loanPurp;

    /**
     * 贷款形式
     **/
    private String loanModal;

    /**
     * 贷款性质
     **/
    private String loanCha;

    /**
     * 是否曾被拒绝
     **/
    private String isHasRefused;

    /**
     * 主担保方式
     **/
    private String guarWay;

    /**
     * 是否共同申请人
     **/
    private String isCommonRqstr;

    /**
     * 是否确认支付方式
     **/
    private String isCfirmPayWay;

    /**
     * 支付方式
     **/
    private String payMode;

    /**
     * 币种
     **/
    private String curType;

    /**
     * 合同金额
     **/
    private java.math.BigDecimal contAmt;

    /**
     * 合同余额
     **/
    private java.math.BigDecimal contBalance;

    /**
     * 合同汇率
     **/
    private java.math.BigDecimal contRate;

    /**
     * 合同类型
     **/
    private String contType;

    /**
     * 纸质合同签订日期
     **/
    private String paperContSignDate;

    /**
     * 当前LPR利率(%)
     **/
    private java.math.BigDecimal curtLprRate;

    /**
     * LPR定价区间
     **/
    private String lprPriceInterval;

    /**
     * LPR浮动点(BP)
     **/
    private java.math.BigDecimal lprBp;

    /**
     * 结息方式
     **/
    private String eiMode;

    /**
     * 结息具体说明
     **/
    private String eiModeExpl;

    /**
     * 提款方式
     **/
    private String drawMode;

    /**
     * 提款天数限制
     **/
    private String dayLimit;

    /**
     * 地址
     **/
    private String addr;

    /**
     * 传真
     **/
    private String fax;

    /**
     * 邮箱
     **/
    private String email;

    /**
     * QQ
     **/
    private String qq;

    /**
     * 微信
     **/
    private String wechat;

    /**
     * 其他通讯方式及账号
     **/
    private String otherPhone;

    /**
     * 双录编号
     **/
    private String doubleRecordNo;

    /**
     * 签订方式
     **/
    private String signMode;

    /**
     * 签约渠道
     **/
    private String signChannel;

    /**
     * 所属条线
     **/
    private String belgLine;

    /**
     * 保证金来源
     **/
    private String bailSour;

    /**
     * 保证金汇率
     **/
    private java.math.BigDecimal bailExchangeRate;

    /**
     * 保证金比例
     **/
    private java.math.BigDecimal bailPerc;

    /**
     * 保证金币种
     **/
    private String bailCurType;

    /**
     * 保证金金额
     **/
    private java.math.BigDecimal bailAmt;

    /**
     * 保证金折算人民币金额
     **/
    private java.math.BigDecimal bailCvtCnyAmt;

    /**
     * 合同起始日期
     **/
    private String contStartDate;

    /**
     * 合同到期日期
     **/
    private String contEndDate;

    /**
     * 期限类型
     **/
    private String termType;

    /**
     * 申请期限
     **/
    private java.math.BigDecimal appTerm;

    /**
     * 利率依据方式
     **/
    private String irAccordType;

    /**
     * 利率种类
     **/
    private String irType;

    /**
     * 基准利率（年）
     **/
    private java.math.BigDecimal rulingIr;

    /**
     * 对应基准利率(月)
     **/
    private java.math.BigDecimal rulingIrM;

    /**
     * 计息方式
     **/
    private String loanRatType;

    /**
     * 利率调整类型
     **/
    private String irAdjustType;

    /**
     * 利率调整周期(月)
     **/
    private java.math.BigDecimal irAdjustTerm;

    /**
     * 调息方式
     **/
    private String praType;

    /**
     * 利率形式
     **/
    private String rateType;

    /**
     * 正常利率浮动方式
     **/
    private String irFloatType;

    /**
     * 利率浮动百分比
     **/
    private java.math.BigDecimal irFloatRate;

    /**
     * 固定加点值
     **/
    private java.math.BigDecimal irFloatPoint;

    /**
     * 执行年利率
     **/
    private java.math.BigDecimal execRateYear;

    /**
     * 执行利率(月)
     **/
    private java.math.BigDecimal realityIrM;

    /**
     * 逾期利率浮动方式
     **/
    private String overdueFloatType;

    /**
     * 逾期利率浮动加点值
     **/
    private java.math.BigDecimal overduePoint;

    /**
     * 逾期利率浮动百分比
     **/
    private java.math.BigDecimal overdueRate;

    /**
     * 逾期利率(年)
     **/
    private java.math.BigDecimal overdueRateY;

    /**
     * 违约利率浮动方式
     **/
    private String defaultFloatType;

    /**
     * 违约利率浮动加点值
     **/
    private java.math.BigDecimal defaultPoint;

    /**
     * 违约利率浮动百分比
     **/
    private java.math.BigDecimal defaultRate;

    /**
     * 违约利率(年)
     **/
    private java.math.BigDecimal defaultRateY;

    /**
     * 风险敞口金额
     **/
    private java.math.BigDecimal riskOpenAmt;

    /**
     * 还款方式
     **/
    private String repayMode;

    /**
     * 停本付息期间
     **/
    private String stopPintTerm;

    /**
     * 还款间隔周期
     **/
    private String repayTerm;

    /**
     * 还款间隔
     **/
    private String repaySpace;

    /**
     * 还款日确定规则
     **/
    private String repayRule;

    /**
     * 还款日类型
     **/
    private String repayDtType;

    /**
     * 还款日
     **/
    private String repayDate;

    /**
     * 本金宽限方式
     **/
    private String capGraperType;

    /**
     * 本金宽限天数
     **/
    private String capGraperDay;

    /**
     * 利息宽限方式
     **/
    private String intGraperType;

    /**
     * 利息宽限天数
     **/
    private String intGraperDay;

    /**
     * 扣款扣息方式
     **/
    private String deductDeduType;

    /**
     * 还款频率类型
     **/
    private String repayFreType;

    /**
     * 本息还款频率
     **/
    private String repayFre;

    /**
     * 提前还款违约金免除时间(月)
     **/
    private Integer liquFreeTime;

    /**
     * 分段方式
     **/
    private String subType;

    /**
     * 保留期限
     **/
    private Integer reserveTerm;

    /**
     * 计算期限
     **/
    private Integer calTerm;

    /**
     * 保留金额
     **/
    private java.math.BigDecimal reserveAmt;

    /**
     * 第一阶段还款期数
     **/
    private Integer repayTermOne;

    /**
     * 第一阶段还款本金
     **/
    private java.math.BigDecimal repayAmtOne;

    /**
     * 第二阶段还款期数
     **/
    private Integer repayTermTwo;

    /**
     * 第二阶段还款本金
     **/
    private java.math.BigDecimal repayAmtTwo;

    /**
     * 利率选取日期种类
     **/
    private String rateSelType;

    /**
     * 贴息方式
     **/
    private String sbsyMode;

    /**
     * 贴息比例
     **/
    private java.math.BigDecimal sbsyPerc;

    /**
     * 贴息金额
     **/
    private java.math.BigDecimal sbsyAmt;

    /**
     * 贴息单位名称
     **/
    private String sbsyUnitName;

    /**
     * 贴息方账户
     **/
    private String sbsyAcct;

    /**
     * 贴息方账户户名
     **/
    private String sbsyAcctName;

    /**
     * 五级分类
     **/
    private String fiveClass;

    /**
     * 贷款投向
     **/
    private String loanTer;

    /**
     * 工业转型升级标识
     **/
    private String comUpIndtify;

    /**
     * 战略新兴产业类型
     **/
    private String strategyNewLoan;

    /**
     * 是否文化产业
     **/
    private String isCulEstate;

    /**
     * 贷款种类
     **/
    private String loanType;

    /**
     * 产业结构调整类型
     **/
    private String estateAdjustType;

    /**
     * 新兴产业贷款
     **/
    private String newPrdLoan;

    /**
     * 还款来源
     **/
    private String repaySour;

    /**
     * 是否委托人办理
     **/
    private String isAuthorize;

    /**
     * 委托人姓名
     **/
    private String authedName;

    /**
     * 委托人证件类型
     **/
    private String consignorCertType;

    /**
     * 委托人证件号
     **/
    private String consignorCertCode;

    /**
     * 委托人联系方式
     **/
    private String authedTelNo;

    /**
     * 签订日期
     **/
    private String signDate;

    /**
     * 注销日期
     **/
    private String logoutDate;

    /**
     * 渠道来源
     **/
    private String chnlSour;

    /**
     * 争议解决方式
     **/
    private String billDispupeOpt;

    /**
     * 法院所在地
     **/
    private String courtAddr;

    /**
     * 仲裁委员会
     **/
    private String arbitrateBch;

    /**
     * 仲裁委员会地点
     **/
    private String arbitrateAddr;

    /**
     * 其他方式
     **/
    private String otherOpt;

    /**
     * 合同份数
     **/
    private java.math.BigDecimal contQnt;

    /**
     * 公积金贷款合同编号
     **/
    private String pundContNo;

    /**
     * 补充条款
     **/
    private String spplClause;

    /**
     * 签约地点
     **/
    private String signAddr;

    /**
     * 营业网点
     **/
    private String busiNetwork;

    /**
     * 主要营业场所地址
     **/
    private String mainBusiPalce;

    /**
     * 合同模板
     **/
    private String contTemplate;

    /**
     * 合同打印次数
     **/
    private java.math.BigDecimal contPrintNum;

    /**
     * 合同状态
     **/
    private String contStatus;

    /**
     * 签章审批状态
     **/
    private String signApproveStatus;

    /**
     * 所属团队
     **/
    private String team;

    /**
     * 主管机构
     **/
    private String managerBrId;

    /**
     * 主管客户经理
     **/
    private String managerId;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记日期
     **/
    private String inputDate;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 最后修改人
     **/
    private String updId;

    /**
     * 最后修改机构
     **/
    private String updBrId;

    /**
     * 最后修改日期
     **/
    private String updDate;

    /**
     * 其他约定
     **/
    private String otherAgreed;

    /**
     * 开户行名称
     **/
    private String acctsvcrName;

    /**
     * 贷款发放账号
     **/
    private String loanPayoutAccno;

    /**
     * 贷款发放账号名称
     **/
    private String loanPayoutAccName;

    /**
     * 合同模式
     **/
    private String contMode;

    /**
     * 是否线上提款
     **/
    private String isOnlineDraw;

    /**
     * 线上合同启用标识
     **/
    private String ctrBeginFlag;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 申请状态
     **/
    private String approveStatus;

    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;


//    最高额
    /**
     * 主键
     **/
    private String pkId;

    /**
     * 担保方式 STD_ZB_ASSURE_MEANS
     **/
    private String guarMode;

    /**
     * 协议币种 STD_ZX_CUR_TYPE
     **/
    private String agrType;

    /**
     * 协议金额
     **/
    private java.math.BigDecimal agrAmt;

    /**
     * 本协议项下最高可用信金额
     **/
    private java.math.BigDecimal agrContHighAvlAmt;

    /**
     * 协议期限
     **/
    private Integer agrTerm;

    /**
     * 协议起始日
     **/
    private String startDate;

    /**
     * 协议到期日
     **/
    private String endDate;

    /**
     * 是否续签  STD_ZB_YES_NO
     **/
    private String isRenew;

    /**
     * 原合同编号
     **/
    private String origiContNo;

    /**
     * 是否使用授信额度 STD_ZB_YES_NO
     **/
    private String isUtilLmt;

    /**
     * 批复编号
     **/
    private String replyNo;

    /**
     * 是否电子用印 STD_ZB_YES_NO
     **/
    private String isESeal;

    /**
     * 是否在线抵押 STD_ZB_YES_NO
     **/
    private String isOlPld;

    /**
     * 联系人
     **/
    private String linkman;

    /**
     * 送达地址
     **/
    private String deliveryAddr;

    /**
     * 申请人账号
     **/
    private String rqstrAccNo;

    /**
     * 申请人账户名称
     **/
    private String rqstrAccName;

    /**
     * 买入类型
     **/
    private String purType;

    /**
     * 票面总金额
     **/
    private String drftTotalAmt;

    /**
     * 是否先贴后查  STD_ZB_YES_NO
     **/
    private String isAtcf;

    /**
     * 付息方式  STD_ZB_PAY_INTEREST
     **/
    private String pintMode;

    /**
     * 申请人授信额度编号
     **/
    private String rqstrLmtLimitNo;

    /**
     * 申请人批复编号
     **/
    private String rqstrReplyNo;

    /**
     * 承兑企业授信额度编号
     **/
    private String acptCrpLmtNo;

    /**
     * 承兑企业批复编号
     **/
    private String acptCrpReplyNo;

    /**
     * 承兑企业客户编号
     **/
    private String acptCrpCusId;

    /**
     * 承兑企业客户名称
     **/
    private String acptCrpCusName;

    /**
     * 债项等级 DEBT_GRADE
     **/
    private String debtLevel;


//    银城

    /**
     * 本合同项下最高可用信金额
     **/
    private java.math.BigDecimal contHighAvlAmt;

    /**
     * 合同期限
     **/
    private Integer contTerm;

    /**
     * 签发类型   STD_ZB_ACPT_TYPE
     **/
    private String signissueType;

    /**
     * 是否电子票据    STD_ZB_YES_NO
     **/
    private String isEDrft;

    /**
     * 手续费率（‰）
     **/
    private java.math.BigDecimal chrgRate;

    /**
     * 手续费金额
     **/
    private java.math.BigDecimal chrgAmt;

    /**
     * 承兑行类型    缺少字典项
     **/
    private String aorgType;

    /**
     * 承兑行行号
     **/
    private String aorgNo;

    /**
     * 承兑行名称
     **/
    private String aorgName;

    /**
     * 出票人开户行账号
     **/
    private String daorgNo;

    /**
     * 出票人开户户名
     **/
    private String daorgName;


//保函

    /**
     * 业务类型
     **/
    private String busiType;

    /**
     * 授信额度编号
     **/
    private String lmtNo;

    /**
     * 保函类型   缺少字典项
     **/
    private String guarantType;

    /**
     * 保函种类   缺少字典项
     **/
    private String guarantMode;

    /**
     * 汇率
     **/
    private String exchangeRate;

    /**
     * 折算人民币金额
     **/
    private java.math.BigDecimal cvtCnyAmt;

    /**
     * 是否线上保函   STD_ZB_YES_NO
     **/
    private String isEGuarant;

    /**
     * 是否为转开代理行保函   STD_ZB_YES_NO
     **/
    private String isAgentbankGuarant;

    /**
     * 代理行名称
     **/
    private String agentbankName;

    /**
     * 项目名称
     **/
    private String proName;

    /**
     * 项目金额
     **/
    private java.math.BigDecimal proAmt;

    /**
     * 合同协议
     **/
    private String contAgr;

    /**
     * 受益人名称
     **/
    private String beneficiarName;

    /**
     * 保函付款方式
     **/
    private String guarantPayMode;

    /**
     * 保函承付条件说明
     **/
    private String guarantHonourCond;

    /**
     * 相关贸易合同金额
     **/
    private java.math.BigDecimal correBusnesContAmt;


    /**
     * 违约损失率LGD
     **/
    private java.math.BigDecimal lgd;

    /**
     * 违约风险暴露EAD
     **/
    private java.math.BigDecimal ead;

    /**
     * 转敞口对象的PD
     **/
    private String pd;

// 贴现
    /**
     * 贴现协议类型   STD_ZB_DISC_CONT_TYPE
     **/
    private String discContType;

    /**
     * 票据种类   STD_ZB_DRFT_TYPE
     **/
    private String drftType;

    /**
     * 贴现币种  STD_ZX_CUR_TYPE
     **/
    private String discCurType;


//    委托贷款

    /**
     * 委托人类型
     **/
    private String consignorType;

    /**
     * 委托贷款类型
     **/
    private String consignorLoanType;

    /**
     * 委托人客户编号
     **/
    private String consignorCusId;

    /**
     * 委托人客户名称
     **/
    private String consignorCusName;

    /**
     * 委托贷款手续费收取方式
     **/
    private String csgnLoanChrgCollectType;

    /**
     * 委托贷款手续费率
     **/
    private java.math.BigDecimal csgnLoanChrgCollectRate;

    /**
     * 委托条件
     **/
    private String csgnCond;

    /**
     * 利率调整方式
     **/
    private String rateAdjMode;

    /**
     * 借款利率调整日
     **/
    private String loanRateAdjDay;

    /**
     * LPR利率区间
     **/
    private String lprRateIntval;

    /**
     * 浮动点数
     **/
    private String rateFloatPoint;

    /**
     * 结息方式
     **/
    private String eiType;

    /**
     * 提款期限
     **/
    private String drawTerm;

    /**
     * 贷款发放账户名称
     **/
    private String loanPayoutAcctName;

    /**
     * 还款具体说明
     **/
    private String repayDetail;

    /**
     * 本笔信贷投入的必要性及详细用途分析
     **/
    private String cretUseAnaly;

    /**
     * 调查人结论
     **/
    private String inveConclu;

//开证

    /**
     * 客户外文名称
     **/
    private String cusNameEn;

    /**
     * 贸易合同号
     **/
    private String tcontNo;

    /**
     * 溢装比例
     **/
    private java.math.BigDecimal floodactPerc;

    /**
     * 货物名称
     **/
    private String goodsName;


    public String getContTypeOpr() {
        return contTypeOpr;
    }

    public void setContTypeOpr(String contTypeOpr) {
        this.contTypeOpr = contTypeOpr;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContCnNo() {
        return contCnNo;
    }

    public void setContCnNo(String contCnNo) {
        this.contCnNo = contCnNo;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getEspecBizType() {
        return especBizType;
    }

    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType;
    }

    public String getLoanPurp() {
        return loanPurp;
    }

    public void setLoanPurp(String loanPurp) {
        this.loanPurp = loanPurp;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getLoanCha() {
        return loanCha;
    }

    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha;
    }

    public String getIsHasRefused() {
        return isHasRefused;
    }

    public void setIsHasRefused(String isHasRefused) {
        this.isHasRefused = isHasRefused;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getIsCommonRqstr() {
        return isCommonRqstr;
    }

    public void setIsCommonRqstr(String isCommonRqstr) {
        this.isCommonRqstr = isCommonRqstr;
    }

    public String getIsCfirmPayWay() {
        return isCfirmPayWay;
    }

    public void setIsCfirmPayWay(String isCfirmPayWay) {
        this.isCfirmPayWay = isCfirmPayWay;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getContBalance() {
        return contBalance;
    }

    public void setContBalance(BigDecimal contBalance) {
        this.contBalance = contBalance;
    }

    public BigDecimal getContRate() {
        return contRate;
    }

    public void setContRate(BigDecimal contRate) {
        this.contRate = contRate;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getPaperContSignDate() {
        return paperContSignDate;
    }

    public void setPaperContSignDate(String paperContSignDate) {
        this.paperContSignDate = paperContSignDate;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public String getLprPriceInterval() {
        return lprPriceInterval;
    }

    public void setLprPriceInterval(String lprPriceInterval) {
        this.lprPriceInterval = lprPriceInterval;
    }

    public BigDecimal getLprBp() {
        return lprBp;
    }

    public void setLprBp(BigDecimal lprBp) {
        this.lprBp = lprBp;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getEiModeExpl() {
        return eiModeExpl;
    }

    public void setEiModeExpl(String eiModeExpl) {
        this.eiModeExpl = eiModeExpl;
    }

    public String getDrawMode() {
        return drawMode;
    }

    public void setDrawMode(String drawMode) {
        this.drawMode = drawMode;
    }

    public String getDayLimit() {
        return dayLimit;
    }

    public void setDayLimit(String dayLimit) {
        this.dayLimit = dayLimit;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getDoubleRecordNo() {
        return doubleRecordNo;
    }

    public void setDoubleRecordNo(String doubleRecordNo) {
        this.doubleRecordNo = doubleRecordNo;
    }

    public String getSignMode() {
        return signMode;
    }

    public void setSignMode(String signMode) {
        this.signMode = signMode;
    }

    public String getSignChannel() {
        return signChannel;
    }

    public void setSignChannel(String signChannel) {
        this.signChannel = signChannel;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getBailSour() {
        return bailSour;
    }

    public void setBailSour(String bailSour) {
        this.bailSour = bailSour;
    }

    public BigDecimal getBailExchangeRate() {
        return bailExchangeRate;
    }

    public void setBailExchangeRate(BigDecimal bailExchangeRate) {
        this.bailExchangeRate = bailExchangeRate;
    }

    public BigDecimal getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(BigDecimal bailPerc) {
        this.bailPerc = bailPerc;
    }

    public String getBailCurType() {
        return bailCurType;
    }

    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public BigDecimal getBailCvtCnyAmt() {
        return bailCvtCnyAmt;
    }

    public void setBailCvtCnyAmt(BigDecimal bailCvtCnyAmt) {
        this.bailCvtCnyAmt = bailCvtCnyAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public BigDecimal getAppTerm() {
        return appTerm;
    }

    public void setAppTerm(BigDecimal appTerm) {
        this.appTerm = appTerm;
    }

    public String getIrAccordType() {
        return irAccordType;
    }

    public void setIrAccordType(String irAccordType) {
        this.irAccordType = irAccordType;
    }

    public String getIrType() {
        return irType;
    }

    public void setIrType(String irType) {
        this.irType = irType;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public BigDecimal getRulingIrM() {
        return rulingIrM;
    }

    public void setRulingIrM(BigDecimal rulingIrM) {
        this.rulingIrM = rulingIrM;
    }

    public String getLoanRatType() {
        return loanRatType;
    }

    public void setLoanRatType(String loanRatType) {
        this.loanRatType = loanRatType;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public BigDecimal getIrAdjustTerm() {
        return irAdjustTerm;
    }

    public void setIrAdjustTerm(BigDecimal irAdjustTerm) {
        this.irAdjustTerm = irAdjustTerm;
    }

    public String getPraType() {
        return praType;
    }

    public void setPraType(String praType) {
        this.praType = praType;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public BigDecimal getIrFloatPoint() {
        return irFloatPoint;
    }

    public void setIrFloatPoint(BigDecimal irFloatPoint) {
        this.irFloatPoint = irFloatPoint;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getRealityIrM() {
        return realityIrM;
    }

    public void setRealityIrM(BigDecimal realityIrM) {
        this.realityIrM = realityIrM;
    }

    public String getOverdueFloatType() {
        return overdueFloatType;
    }

    public void setOverdueFloatType(String overdueFloatType) {
        this.overdueFloatType = overdueFloatType;
    }

    public BigDecimal getOverduePoint() {
        return overduePoint;
    }

    public void setOverduePoint(BigDecimal overduePoint) {
        this.overduePoint = overduePoint;
    }

    public BigDecimal getOverdueRate() {
        return overdueRate;
    }

    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    public BigDecimal getOverdueRateY() {
        return overdueRateY;
    }

    public void setOverdueRateY(BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    public String getDefaultFloatType() {
        return defaultFloatType;
    }

    public void setDefaultFloatType(String defaultFloatType) {
        this.defaultFloatType = defaultFloatType;
    }

    public BigDecimal getDefaultPoint() {
        return defaultPoint;
    }

    public void setDefaultPoint(BigDecimal defaultPoint) {
        this.defaultPoint = defaultPoint;
    }

    public BigDecimal getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    public BigDecimal getDefaultRateY() {
        return defaultRateY;
    }

    public void setDefaultRateY(BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    public BigDecimal getRiskOpenAmt() {
        return riskOpenAmt;
    }

    public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
        this.riskOpenAmt = riskOpenAmt;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getStopPintTerm() {
        return stopPintTerm;
    }

    public void setStopPintTerm(String stopPintTerm) {
        this.stopPintTerm = stopPintTerm;
    }

    public String getRepayTerm() {
        return repayTerm;
    }

    public void setRepayTerm(String repayTerm) {
        this.repayTerm = repayTerm;
    }

    public String getRepaySpace() {
        return repaySpace;
    }

    public void setRepaySpace(String repaySpace) {
        this.repaySpace = repaySpace;
    }

    public String getRepayRule() {
        return repayRule;
    }

    public void setRepayRule(String repayRule) {
        this.repayRule = repayRule;
    }

    public String getRepayDtType() {
        return repayDtType;
    }

    public void setRepayDtType(String repayDtType) {
        this.repayDtType = repayDtType;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getCapGraperType() {
        return capGraperType;
    }

    public void setCapGraperType(String capGraperType) {
        this.capGraperType = capGraperType;
    }

    public String getCapGraperDay() {
        return capGraperDay;
    }

    public void setCapGraperDay(String capGraperDay) {
        this.capGraperDay = capGraperDay;
    }

    public String getIntGraperType() {
        return intGraperType;
    }

    public void setIntGraperType(String intGraperType) {
        this.intGraperType = intGraperType;
    }

    public String getIntGraperDay() {
        return intGraperDay;
    }

    public void setIntGraperDay(String intGraperDay) {
        this.intGraperDay = intGraperDay;
    }

    public String getDeductDeduType() {
        return deductDeduType;
    }

    public void setDeductDeduType(String deductDeduType) {
        this.deductDeduType = deductDeduType;
    }

    public String getRepayFreType() {
        return repayFreType;
    }

    public void setRepayFreType(String repayFreType) {
        this.repayFreType = repayFreType;
    }

    public String getRepayFre() {
        return repayFre;
    }

    public void setRepayFre(String repayFre) {
        this.repayFre = repayFre;
    }

    public Integer getLiquFreeTime() {
        return liquFreeTime;
    }

    public void setLiquFreeTime(Integer liquFreeTime) {
        this.liquFreeTime = liquFreeTime;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Integer getReserveTerm() {
        return reserveTerm;
    }

    public void setReserveTerm(Integer reserveTerm) {
        this.reserveTerm = reserveTerm;
    }

    public Integer getCalTerm() {
        return calTerm;
    }

    public void setCalTerm(Integer calTerm) {
        this.calTerm = calTerm;
    }

    public BigDecimal getReserveAmt() {
        return reserveAmt;
    }

    public void setReserveAmt(BigDecimal reserveAmt) {
        this.reserveAmt = reserveAmt;
    }

    public Integer getRepayTermOne() {
        return repayTermOne;
    }

    public void setRepayTermOne(Integer repayTermOne) {
        this.repayTermOne = repayTermOne;
    }

    public BigDecimal getRepayAmtOne() {
        return repayAmtOne;
    }

    public void setRepayAmtOne(BigDecimal repayAmtOne) {
        this.repayAmtOne = repayAmtOne;
    }

    public Integer getRepayTermTwo() {
        return repayTermTwo;
    }

    public void setRepayTermTwo(Integer repayTermTwo) {
        this.repayTermTwo = repayTermTwo;
    }

    public BigDecimal getRepayAmtTwo() {
        return repayAmtTwo;
    }

    public void setRepayAmtTwo(BigDecimal repayAmtTwo) {
        this.repayAmtTwo = repayAmtTwo;
    }

    public String getRateSelType() {
        return rateSelType;
    }

    public void setRateSelType(String rateSelType) {
        this.rateSelType = rateSelType;
    }

    public String getSbsyMode() {
        return sbsyMode;
    }

    public void setSbsyMode(String sbsyMode) {
        this.sbsyMode = sbsyMode;
    }

    public BigDecimal getSbsyPerc() {
        return sbsyPerc;
    }

    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    public BigDecimal getSbsyAmt() {
        return sbsyAmt;
    }

    public void setSbsyAmt(BigDecimal sbsyAmt) {
        this.sbsyAmt = sbsyAmt;
    }

    public String getSbsyUnitName() {
        return sbsyUnitName;
    }

    public void setSbsyUnitName(String sbsyUnitName) {
        this.sbsyUnitName = sbsyUnitName;
    }

    public String getSbsyAcct() {
        return sbsyAcct;
    }

    public void setSbsyAcct(String sbsyAcct) {
        this.sbsyAcct = sbsyAcct;
    }

    public String getSbsyAcctName() {
        return sbsyAcctName;
    }

    public void setSbsyAcctName(String sbsyAcctName) {
        this.sbsyAcctName = sbsyAcctName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getComUpIndtify() {
        return comUpIndtify;
    }

    public void setComUpIndtify(String comUpIndtify) {
        this.comUpIndtify = comUpIndtify;
    }

    public String getStrategyNewLoan() {
        return strategyNewLoan;
    }

    public void setStrategyNewLoan(String strategyNewLoan) {
        this.strategyNewLoan = strategyNewLoan;
    }

    public String getIsCulEstate() {
        return isCulEstate;
    }

    public void setIsCulEstate(String isCulEstate) {
        this.isCulEstate = isCulEstate;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getEstateAdjustType() {
        return estateAdjustType;
    }

    public void setEstateAdjustType(String estateAdjustType) {
        this.estateAdjustType = estateAdjustType;
    }

    public String getNewPrdLoan() {
        return newPrdLoan;
    }

    public void setNewPrdLoan(String newPrdLoan) {
        this.newPrdLoan = newPrdLoan;
    }

    public String getRepaySour() {
        return repaySour;
    }

    public void setRepaySour(String repaySour) {
        this.repaySour = repaySour;
    }

    public String getIsAuthorize() {
        return isAuthorize;
    }

    public void setIsAuthorize(String isAuthorize) {
        this.isAuthorize = isAuthorize;
    }

    public String getAuthedName() {
        return authedName;
    }

    public void setAuthedName(String authedName) {
        this.authedName = authedName;
    }

    public String getConsignorCertType() {
        return consignorCertType;
    }

    public void setConsignorCertType(String consignorCertType) {
        this.consignorCertType = consignorCertType;
    }

    public String getConsignorCertCode() {
        return consignorCertCode;
    }

    public void setConsignorCertCode(String consignorCertCode) {
        this.consignorCertCode = consignorCertCode;
    }

    public String getAuthedTelNo() {
        return authedTelNo;
    }

    public void setAuthedTelNo(String authedTelNo) {
        this.authedTelNo = authedTelNo;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getBillDispupeOpt() {
        return billDispupeOpt;
    }

    public void setBillDispupeOpt(String billDispupeOpt) {
        this.billDispupeOpt = billDispupeOpt;
    }

    public String getCourtAddr() {
        return courtAddr;
    }

    public void setCourtAddr(String courtAddr) {
        this.courtAddr = courtAddr;
    }

    public String getArbitrateBch() {
        return arbitrateBch;
    }

    public void setArbitrateBch(String arbitrateBch) {
        this.arbitrateBch = arbitrateBch;
    }

    public String getArbitrateAddr() {
        return arbitrateAddr;
    }

    public void setArbitrateAddr(String arbitrateAddr) {
        this.arbitrateAddr = arbitrateAddr;
    }

    public String getOtherOpt() {
        return otherOpt;
    }

    public void setOtherOpt(String otherOpt) {
        this.otherOpt = otherOpt;
    }

    public BigDecimal getContQnt() {
        return contQnt;
    }

    public void setContQnt(BigDecimal contQnt) {
        this.contQnt = contQnt;
    }

    public String getPundContNo() {
        return pundContNo;
    }

    public void setPundContNo(String pundContNo) {
        this.pundContNo = pundContNo;
    }

    public String getSpplClause() {
        return spplClause;
    }

    public void setSpplClause(String spplClause) {
        this.spplClause = spplClause;
    }

    public String getSignAddr() {
        return signAddr;
    }

    public void setSignAddr(String signAddr) {
        this.signAddr = signAddr;
    }

    public String getBusiNetwork() {
        return busiNetwork;
    }

    public void setBusiNetwork(String busiNetwork) {
        this.busiNetwork = busiNetwork;
    }

    public String getMainBusiPalce() {
        return mainBusiPalce;
    }

    public void setMainBusiPalce(String mainBusiPalce) {
        this.mainBusiPalce = mainBusiPalce;
    }

    public String getContTemplate() {
        return contTemplate;
    }

    public void setContTemplate(String contTemplate) {
        this.contTemplate = contTemplate;
    }

    public BigDecimal getContPrintNum() {
        return contPrintNum;
    }

    public void setContPrintNum(BigDecimal contPrintNum) {
        this.contPrintNum = contPrintNum;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getSignApproveStatus() {
        return signApproveStatus;
    }

    public void setSignApproveStatus(String signApproveStatus) {
        this.signApproveStatus = signApproveStatus;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOtherAgreed() {
        return otherAgreed;
    }

    public void setOtherAgreed(String otherAgreed) {
        this.otherAgreed = otherAgreed;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutAccName() {
        return loanPayoutAccName;
    }

    public void setLoanPayoutAccName(String loanPayoutAccName) {
        this.loanPayoutAccName = loanPayoutAccName;
    }

    public String getContMode() {
        return contMode;
    }

    public void setContMode(String contMode) {
        this.contMode = contMode;
    }

    public String getIsOnlineDraw() {
        return isOnlineDraw;
    }

    public void setIsOnlineDraw(String isOnlineDraw) {
        this.isOnlineDraw = isOnlineDraw;
    }

    public String getCtrBeginFlag() {
        return ctrBeginFlag;
    }

    public void setCtrBeginFlag(String ctrBeginFlag) {
        this.ctrBeginFlag = ctrBeginFlag;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getAgrType() {
        return agrType;
    }

    public void setAgrType(String agrType) {
        this.agrType = agrType;
    }

    public BigDecimal getAgrAmt() {
        return agrAmt;
    }

    public void setAgrAmt(BigDecimal agrAmt) {
        this.agrAmt = agrAmt;
    }

    public BigDecimal getAgrContHighAvlAmt() {
        return agrContHighAvlAmt;
    }

    public void setAgrContHighAvlAmt(BigDecimal agrContHighAvlAmt) {
        this.agrContHighAvlAmt = agrContHighAvlAmt;
    }

    public Integer getAgrTerm() {
        return agrTerm;
    }

    public void setAgrTerm(Integer agrTerm) {
        this.agrTerm = agrTerm;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(String isRenew) {
        this.isRenew = isRenew;
    }

    public String getOrigiContNo() {
        return origiContNo;
    }

    public void setOrigiContNo(String origiContNo) {
        this.origiContNo = origiContNo;
    }

    public String getIsUtilLmt() {
        return isUtilLmt;
    }

    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getIsESeal() {
        return isESeal;
    }

    public void setIsESeal(String isESeal) {
        this.isESeal = isESeal;
    }

    public String getIsOlPld() {
        return isOlPld;
    }

    public void setIsOlPld(String isOlPld) {
        this.isOlPld = isOlPld;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getRqstrAccNo() {
        return rqstrAccNo;
    }

    public void setRqstrAccNo(String rqstrAccNo) {
        this.rqstrAccNo = rqstrAccNo;
    }

    public String getRqstrAccName() {
        return rqstrAccName;
    }

    public void setRqstrAccName(String rqstrAccName) {
        this.rqstrAccName = rqstrAccName;
    }

    public String getPurType() {
        return purType;
    }

    public void setPurType(String purType) {
        this.purType = purType;
    }

    public String getDrftTotalAmt() {
        return drftTotalAmt;
    }

    public void setDrftTotalAmt(String drftTotalAmt) {
        this.drftTotalAmt = drftTotalAmt;
    }

    public String getIsAtcf() {
        return isAtcf;
    }

    public void setIsAtcf(String isAtcf) {
        this.isAtcf = isAtcf;
    }

    public String getPintMode() {
        return pintMode;
    }

    public void setPintMode(String pintMode) {
        this.pintMode = pintMode;
    }

    public String getRqstrLmtLimitNo() {
        return rqstrLmtLimitNo;
    }

    public void setRqstrLmtLimitNo(String rqstrLmtLimitNo) {
        this.rqstrLmtLimitNo = rqstrLmtLimitNo;
    }

    public String getRqstrReplyNo() {
        return rqstrReplyNo;
    }

    public void setRqstrReplyNo(String rqstrReplyNo) {
        this.rqstrReplyNo = rqstrReplyNo;
    }

    public String getAcptCrpLmtNo() {
        return acptCrpLmtNo;
    }

    public void setAcptCrpLmtNo(String acptCrpLmtNo) {
        this.acptCrpLmtNo = acptCrpLmtNo;
    }

    public String getAcptCrpReplyNo() {
        return acptCrpReplyNo;
    }

    public void setAcptCrpReplyNo(String acptCrpReplyNo) {
        this.acptCrpReplyNo = acptCrpReplyNo;
    }

    public String getAcptCrpCusId() {
        return acptCrpCusId;
    }

    public void setAcptCrpCusId(String acptCrpCusId) {
        this.acptCrpCusId = acptCrpCusId;
    }

    public String getAcptCrpCusName() {
        return acptCrpCusName;
    }

    public void setAcptCrpCusName(String acptCrpCusName) {
        this.acptCrpCusName = acptCrpCusName;
    }

    public String getDebtLevel() {
        return debtLevel;
    }

    public void setDebtLevel(String debtLevel) {
        this.debtLevel = debtLevel;
    }

    public BigDecimal getContHighAvlAmt() {
        return contHighAvlAmt;
    }

    public void setContHighAvlAmt(BigDecimal contHighAvlAmt) {
        this.contHighAvlAmt = contHighAvlAmt;
    }

    public Integer getContTerm() {
        return contTerm;
    }

    public void setContTerm(Integer contTerm) {
        this.contTerm = contTerm;
    }

    public String getSignissueType() {
        return signissueType;
    }

    public void setSignissueType(String signissueType) {
        this.signissueType = signissueType;
    }

    public String getIsEDrft() {
        return isEDrft;
    }

    public void setIsEDrft(String isEDrft) {
        this.isEDrft = isEDrft;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public BigDecimal getChrgAmt() {
        return chrgAmt;
    }

    public void setChrgAmt(BigDecimal chrgAmt) {
        this.chrgAmt = chrgAmt;
    }

    public String getAorgType() {
        return aorgType;
    }

    public void setAorgType(String aorgType) {
        this.aorgType = aorgType;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getDaorgNo() {
        return daorgNo;
    }

    public void setDaorgNo(String daorgNo) {
        this.daorgNo = daorgNo;
    }

    public String getDaorgName() {
        return daorgName;
    }

    public void setDaorgName(String daorgName) {
        this.daorgName = daorgName;
    }

    public String getBusiType() {
        return busiType;
    }

    public void setBusiType(String busiType) {
        this.busiType = busiType;
    }

    public String getLmtNo() {
        return lmtNo;
    }

    public void setLmtNo(String lmtNo) {
        this.lmtNo = lmtNo;
    }

    public String getGuarantType() {
        return guarantType;
    }

    public void setGuarantType(String guarantType) {
        this.guarantType = guarantType;
    }

    public String getGuarantMode() {
        return guarantMode;
    }

    public void setGuarantMode(String guarantMode) {
        this.guarantMode = guarantMode;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getCvtCnyAmt() {
        return cvtCnyAmt;
    }

    public void setCvtCnyAmt(BigDecimal cvtCnyAmt) {
        this.cvtCnyAmt = cvtCnyAmt;
    }

    public String getIsEGuarant() {
        return isEGuarant;
    }

    public void setIsEGuarant(String isEGuarant) {
        this.isEGuarant = isEGuarant;
    }

    public String getIsAgentbankGuarant() {
        return isAgentbankGuarant;
    }

    public void setIsAgentbankGuarant(String isAgentbankGuarant) {
        this.isAgentbankGuarant = isAgentbankGuarant;
    }

    public String getAgentbankName() {
        return agentbankName;
    }

    public void setAgentbankName(String agentbankName) {
        this.agentbankName = agentbankName;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public BigDecimal getProAmt() {
        return proAmt;
    }

    public void setProAmt(BigDecimal proAmt) {
        this.proAmt = proAmt;
    }

    public String getContAgr() {
        return contAgr;
    }

    public void setContAgr(String contAgr) {
        this.contAgr = contAgr;
    }

    public String getBeneficiarName() {
        return beneficiarName;
    }

    public void setBeneficiarName(String beneficiarName) {
        this.beneficiarName = beneficiarName;
    }

    public String getGuarantPayMode() {
        return guarantPayMode;
    }

    public void setGuarantPayMode(String guarantPayMode) {
        this.guarantPayMode = guarantPayMode;
    }

    public String getGuarantHonourCond() {
        return guarantHonourCond;
    }

    public void setGuarantHonourCond(String guarantHonourCond) {
        this.guarantHonourCond = guarantHonourCond;
    }

    public BigDecimal getCorreBusnesContAmt() {
        return correBusnesContAmt;
    }

    public void setCorreBusnesContAmt(BigDecimal correBusnesContAmt) {
        this.correBusnesContAmt = correBusnesContAmt;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public String getPd() {
        return pd;
    }

    public void setPd(String pd) {
        this.pd = pd;
    }

    public String getDiscContType() {
        return discContType;
    }

    public void setDiscContType(String discContType) {
        this.discContType = discContType;
    }

    public String getDrftType() {
        return drftType;
    }

    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getDiscCurType() {
        return discCurType;
    }

    public void setDiscCurType(String discCurType) {
        this.discCurType = discCurType;
    }

    public String getConsignorType() {
        return consignorType;
    }

    public void setConsignorType(String consignorType) {
        this.consignorType = consignorType;
    }

    public String getConsignorLoanType() {
        return consignorLoanType;
    }

    public void setConsignorLoanType(String consignorLoanType) {
        this.consignorLoanType = consignorLoanType;
    }

    public String getConsignorCusId() {
        return consignorCusId;
    }

    public void setConsignorCusId(String consignorCusId) {
        this.consignorCusId = consignorCusId;
    }

    public String getConsignorCusName() {
        return consignorCusName;
    }

    public void setConsignorCusName(String consignorCusName) {
        this.consignorCusName = consignorCusName;
    }

    public String getCsgnLoanChrgCollectType() {
        return csgnLoanChrgCollectType;
    }

    public void setCsgnLoanChrgCollectType(String csgnLoanChrgCollectType) {
        this.csgnLoanChrgCollectType = csgnLoanChrgCollectType;
    }

    public BigDecimal getCsgnLoanChrgCollectRate() {
        return csgnLoanChrgCollectRate;
    }

    public void setCsgnLoanChrgCollectRate(BigDecimal csgnLoanChrgCollectRate) {
        this.csgnLoanChrgCollectRate = csgnLoanChrgCollectRate;
    }

    public String getCsgnCond() {
        return csgnCond;
    }

    public void setCsgnCond(String csgnCond) {
        this.csgnCond = csgnCond;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getLoanRateAdjDay() {
        return loanRateAdjDay;
    }

    public void setLoanRateAdjDay(String loanRateAdjDay) {
        this.loanRateAdjDay = loanRateAdjDay;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public String getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(String rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public String getEiType() {
        return eiType;
    }

    public void setEiType(String eiType) {
        this.eiType = eiType;
    }

    public String getDrawTerm() {
        return drawTerm;
    }

    public void setDrawTerm(String drawTerm) {
        this.drawTerm = drawTerm;
    }

    public String getLoanPayoutAcctName() {
        return loanPayoutAcctName;
    }

    public void setLoanPayoutAcctName(String loanPayoutAcctName) {
        this.loanPayoutAcctName = loanPayoutAcctName;
    }

    public String getRepayDetail() {
        return repayDetail;
    }

    public void setRepayDetail(String repayDetail) {
        this.repayDetail = repayDetail;
    }

    public String getCretUseAnaly() {
        return cretUseAnaly;
    }

    public void setCretUseAnaly(String cretUseAnaly) {
        this.cretUseAnaly = cretUseAnaly;
    }

    public String getInveConclu() {
        return inveConclu;
    }

    public void setInveConclu(String inveConclu) {
        this.inveConclu = inveConclu;
    }

    public String getCusNameEn() {
        return cusNameEn;
    }

    public void setCusNameEn(String cusNameEn) {
        this.cusNameEn = cusNameEn;
    }

    public String getTcontNo() {
        return tcontNo;
    }

    public void setTcontNo(String tcontNo) {
        this.tcontNo = tcontNo;
    }

    public BigDecimal getFloodactPerc() {
        return floodactPerc;
    }

    public void setFloodactPerc(BigDecimal floodactPerc) {
        this.floodactPerc = floodactPerc;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

}
