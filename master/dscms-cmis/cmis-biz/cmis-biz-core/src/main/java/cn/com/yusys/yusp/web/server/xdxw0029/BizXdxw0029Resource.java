package cn.com.yusys.yusp.web.server.xdxw0029;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0029.req.Xdxw0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0029.resp.Xdxw0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0029.Xdxw0029Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号查询现有融资总额、总余额、担保方式
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0029:根据客户号查询现有融资总额、总余额、担保方式")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0029Resource.class);

    @Autowired
    private Xdxw0029Service xdxw0029Service;

    /**
     * 交易码：xdxw0029
     * 交易描述：根据客户号查询现有融资总额、总余额、担保方式
     *
     * @param xdxw0029DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询现有融资总额、总余额、担保方式")
    @PostMapping("/xdxw0029")
    protected @ResponseBody
    ResultDto<Xdxw0029DataRespDto> xdxw0029(@Validated @RequestBody Xdxw0029DataReqDto xdxw0029DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataReqDto));
        Xdxw0029DataRespDto xdxw0029DataRespDto = new Xdxw0029DataRespDto();// 响应Dto:根据客户号查询现有融资总额、总余额、担保方式
        ResultDto<Xdxw0029DataRespDto> xdxw0029DataResultDto = new ResultDto<>();
        String cusNo = xdxw0029DataReqDto.getCusNo();//客户号
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataReqDto));
            xdxw0029DataRespDto = xdxw0029Service.getXdxw0029(xdxw0029DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataRespDto));
            // 封装xdxw0029DataResultDto中正确的返回码和返回信息
            xdxw0029DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0029DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            xdxw0029DataResultDto.setCode(e.getErrorCode());
            xdxw0029DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            // 封装xdxw0029DataResultDto中异常返回码和返回信息
            xdxw0029DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0029DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0029DataRespDto到xdxw0029DataResultDto中
        xdxw0029DataResultDto.setData(xdxw0029DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataResultDto));
        return xdxw0029DataResultDto;
    }
}
