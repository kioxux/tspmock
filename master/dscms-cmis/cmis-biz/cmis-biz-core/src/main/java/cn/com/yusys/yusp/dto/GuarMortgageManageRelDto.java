package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageRel
 * @类描述: guar_mortgage_manage_rel数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarMortgageManageRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 押品编号 **/
	private String guarNo;
	
	/** 押品名称 **/
	private String pldimnMemo;
	
	/** 押品类型 **/
	private String guarType;
	
	/** 押品所有人编号 **/
	private String guarCusId;
	
	/** 押品所有人名称 **/
	private String guarCusName;
	
	/** 押品所有人证件类型 **/
	private String guarCertType;
	
	/** 押品所有人证件号码 **/
	private String guarCertCode;
	
	/** 押品评估价值 **/
	private java.math.BigDecimal evalAmt;
	
	/** 押品认定价值 **/
	private java.math.BigDecimal confirmAmt;

	/** 担保分类代码 **/
	private String guarTypeCd;

	/** 担保分类代码名称**/
	private String guarTypeCdName;

	/** 权证编号 **/
	private String warrantNo;

	/** 核心担保编号**/
	private String coreGuarantyNo;

	/** 抵质押标识**/
	private String grtFlag;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}
	
    /**
     * @return GuarType
     */	
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId == null ? null : guarCusId.trim();
	}
	
    /**
     * @return GuarCusId
     */	
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName == null ? null : guarCusName.trim();
	}
	
    /**
     * @return GuarCusName
     */	
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param guarCertType
	 */
	public void setGuarCertType(String guarCertType) {
		this.guarCertType = guarCertType == null ? null : guarCertType.trim();
	}
	
    /**
     * @return GuarCertType
     */	
	public String getGuarCertType() {
		return this.guarCertType;
	}
	
	/**
	 * @param guarCertCode
	 */
	public void setGuarCertCode(String guarCertCode) {
		this.guarCertCode = guarCertCode == null ? null : guarCertCode.trim();
	}
	
    /**
     * @return GuarCertCode
     */	
	public String getGuarCertCode() {
		return this.guarCertCode;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param confirmAmt
	 */
	public void setConfirmAmt(java.math.BigDecimal confirmAmt) {
		this.confirmAmt = confirmAmt;
	}
	
    /**
     * @return ConfirmAmt
     */	
	public java.math.BigDecimal getConfirmAmt() {
		return this.confirmAmt;
	}

	public String getGuarTypeCd() {
		return guarTypeCd;
	}

	public void setGuarTypeCd(String guarTypeCd) {
		this.guarTypeCd = guarTypeCd;
	}

	public String getGuarTypeCdName() {
		return guarTypeCdName;
	}

	public void setGuarTypeCdName(String guarTypeCdName) {
		this.guarTypeCdName = guarTypeCdName;
	}

	public String getWarrantNo() {
		return warrantNo;
	}

	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo;
	}

	public String getCoreGuarantyNo() {
		return coreGuarantyNo;
	}

	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}

	public String getGrtFlag() {
		return grtFlag;
	}

	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}
}