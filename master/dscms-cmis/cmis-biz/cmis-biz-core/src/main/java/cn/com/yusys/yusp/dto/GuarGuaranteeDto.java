package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGuarantee
 * @类描述: guar_guarantee数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-28 20:27:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarGuaranteeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 保证_ID **/
	private String guarantyId;
	
	/** 保证担保形式 STD_ZB_GUARANTY_TYPE **/
	private String guarantyType;
	
	/** 保证方式 STD_ZB_GUARANTEE_TY **/
	private String guaranteeType;
	
	/** 币种 STD_ZX_CUR_TYPE **/
	private String currency;
	
	/** 担保金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 备注 **/
	private String remark;
	
	/** 担保人客户号 **/
	private String cusId;
	
	/** 担保人类型 STD_ZB_CUS_TYP **/
	private String cusTyp;
	
	/** 担保人名称 **/
	private String assureName;
	
	/** 与借款人关系  STD_ZB_RELATION **/
	private String relationLender;
	
	/** 担保人贷款卡号 **/
	private String comLoanCard;
	
	/** 保证人状态 STD_ZB_GUAR_STATUS **/
	private String statusCode;
	
	/** 担保人证件号码 **/
	private String assureCertCode;
	
	/** 担保人证件类型 STD_ZB_CERT_TYP **/
	private String cerType;
	
	/** 担保类型细分 STD_ZB_DB_DETAIL **/
	private String guideType;
	
	/** 专业担保公司合作方台帐编号 **/
	private String prjAccNo;
	
	/** 已恢复额度 **/
	private java.math.BigDecimal restoredAmt;
	
	/** 创建人用户编号 **/
	private String createUserId;
	
	/** 客户类型STD_ZB_CUS_TYPE **/
	private String cusType;
	
	/** 企业所有制STD_ZB_QYSYZ **/
	private String comQysyz;
	
	/** 保证法律有效性STD_ZB_BZFLYXX **/
	private String ensureLegalValidity;
	
	/** 内部评级STD_ZB_GRADE_RANK **/
	private String innerLevel;
	
	/** 是否专业担保公司 **/
	private String isGuarCom;
	
	/** 保证人与借款人关联关系STD_ZB_BZRYJKRGLGX **/
	private String guarantorBorrowerRelation;
	
	/** 保证人净资产 **/
	private java.math.BigDecimal cusNetAssets;
	
	/** 保证人唯一标识 **/
	private String guarantyIdNew;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 借款人客户编号 **/
	private String borrowerCusId;

	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;
	
	
	/**
	 * @param guarantyId
	 */
	public void setGuarantyId(String guarantyId) {
		this.guarantyId = guarantyId == null ? null : guarantyId.trim();
	}
	
    /**
     * @return GuarantyId
     */	
	public String getGuarantyId() {
		return this.guarantyId;
	}
	
	/**
	 * @param guarantyType
	 */
	public void setGuarantyType(String guarantyType) {
		this.guarantyType = guarantyType == null ? null : guarantyType.trim();
	}
	
    /**
     * @return GuarantyType
     */	
	public String getGuarantyType() {
		return this.guarantyType;
	}
	
	/**
	 * @param guaranteeType
	 */
	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType == null ? null : guaranteeType.trim();
	}
	
    /**
     * @return GuaranteeType
     */	
	public String getGuaranteeType() {
		return this.guaranteeType;
	}
	
	/**
	 * @param currency
	 */
	public void setCurrency(String currency) {
		this.currency = currency == null ? null : currency.trim();
	}
	
    /**
     * @return Currency
     */	
	public String getCurrency() {
		return this.currency;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusTyp
	 */
	public void setCusTyp(String cusTyp) {
		this.cusTyp = cusTyp == null ? null : cusTyp.trim();
	}
	
    /**
     * @return CusTyp
     */	
	public String getCusTyp() {
		return this.cusTyp;
	}
	
	/**
	 * @param assureName
	 */
	public void setCusName(String assureName) {
		this.assureName = assureName == null ? null : assureName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.assureName;
	}
	
	/**
	 * @param relationLender
	 */
	public void setRelationLender(String relationLender) {
		this.relationLender = relationLender == null ? null : relationLender.trim();
	}
	
    /**
     * @return RelationLender
     */	
	public String getRelationLender() {
		return this.relationLender;
	}
	
	/**
	 * @param comLoanCard
	 */
	public void setComLoanCard(String comLoanCard) {
		this.comLoanCard = comLoanCard == null ? null : comLoanCard.trim();
	}
	
    /**
     * @return ComLoanCard
     */	
	public String getComLoanCard() {
		return this.comLoanCard;
	}
	
	/**
	 * @param statusCode
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode == null ? null : statusCode.trim();
	}
	
    /**
     * @return StatusCode
     */	
	public String getStatusCode() {
		return this.statusCode;
	}
	
	/**
	 * @param assureCertCode
	 */
	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode == null ? null : assureCertCode.trim();
	}
	
    /**
     * @return AssureCertCode
     */	
	public String getAssureCertCode() {
		return this.assureCertCode;
	}
	
	/**
	 * @param cerType
	 */
	public void setCerType(String cerType) {
		this.cerType = cerType == null ? null : cerType.trim();
	}
	
    /**
     * @return CerType
     */	
	public String getCerType() {
		return this.cerType;
	}
	
	/**
	 * @param guideType
	 */
	public void setGuideType(String guideType) {
		this.guideType = guideType == null ? null : guideType.trim();
	}
	
    /**
     * @return GuideType
     */	
	public String getGuideType() {
		return this.guideType;
	}
	
	/**
	 * @param prjAccNo
	 */
	public void setPrjAccNo(String prjAccNo) {
		this.prjAccNo = prjAccNo == null ? null : prjAccNo.trim();
	}
	
    /**
     * @return PrjAccNo
     */	
	public String getPrjAccNo() {
		return this.prjAccNo;
	}
	
	/**
	 * @param restoredAmt
	 */
	public void setRestoredAmt(java.math.BigDecimal restoredAmt) {
		this.restoredAmt = restoredAmt;
	}
	
    /**
     * @return RestoredAmt
     */	
	public java.math.BigDecimal getRestoredAmt() {
		return this.restoredAmt;
	}
	
	/**
	 * @param createUserId
	 */
	public void setcreateUserId(String createUserId) {
		this.createUserId = createUserId == null ? null : createUserId.trim();
	}
	
    /**
     * @return createUserId
     */	
	public String getcreateUserId() {
		return this.createUserId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param comQysyz
	 */
	public void setComQysyz(String comQysyz) {
		this.comQysyz = comQysyz == null ? null : comQysyz.trim();
	}
	
    /**
     * @return ComQysyz
     */	
	public String getComQysyz() {
		return this.comQysyz;
	}
	
	/**
	 * @param ensureLegalValidity
	 */
	public void setEnsureLegalValidity(String ensureLegalValidity) {
		this.ensureLegalValidity = ensureLegalValidity == null ? null : ensureLegalValidity.trim();
	}
	
    /**
     * @return EnsureLegalValidity
     */	
	public String getEnsureLegalValidity() {
		return this.ensureLegalValidity;
	}
	
	/**
	 * @param innerLevel
	 */
	public void setInnerLevel(String innerLevel) {
		this.innerLevel = innerLevel == null ? null : innerLevel.trim();
	}
	
    /**
     * @return InnerLevel
     */	
	public String getInnerLevel() {
		return this.innerLevel;
	}
	
	/**
	 * @param isGuarCom
	 */
	public void setIsGuarCom(String isGuarCom) {
		this.isGuarCom = isGuarCom == null ? null : isGuarCom.trim();
	}
	
    /**
     * @return IsGuarCom
     */	
	public String getIsGuarCom() {
		return this.isGuarCom;
	}
	
	/**
	 * @param guarantorBorrowerRelation
	 */
	public void setGuarantorBorrowerRelation(String guarantorBorrowerRelation) {
		this.guarantorBorrowerRelation = guarantorBorrowerRelation == null ? null : guarantorBorrowerRelation.trim();
	}
	
    /**
     * @return GuarantorBorrowerRelation
     */	
	public String getGuarantorBorrowerRelation() {
		return this.guarantorBorrowerRelation;
	}
	
	/**
	 * @param cusNetAssets
	 */
	public void setCusNetAssets(java.math.BigDecimal cusNetAssets) {
		this.cusNetAssets = cusNetAssets;
	}
	
    /**
     * @return CusNetAssets
     */	
	public java.math.BigDecimal getCusNetAssets() {
		return this.cusNetAssets;
	}
	
	/**
	 * @param guarantyIdNew
	 */
	public void setGuarantyIdNew(String guarantyIdNew) {
		this.guarantyIdNew = guarantyIdNew == null ? null : guarantyIdNew.trim();
	}
	
    /**
     * @return GuarantyIdNew
     */	
	public String getGuarantyIdNew() {
		return this.guarantyIdNew;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param borrowerCusId
	 */
	public void setBorrowerCusId(String borrowerCusId) {
		this.borrowerCusId = borrowerCusId == null ? null : borrowerCusId.trim();
	}
	
    /**
     * @return BorrowerCusId
     */	
	public String getBorrowerCusId() {
		return this.borrowerCusId;
	}

	public String getAssureName() {
		return assureName;
	}

	public void setAssureName(String assureName) {
		this.assureName = assureName;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}