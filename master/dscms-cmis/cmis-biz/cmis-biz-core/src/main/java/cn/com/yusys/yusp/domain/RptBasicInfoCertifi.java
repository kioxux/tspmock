/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoCertifi
 * @类描述: rpt_basic_info_certifi数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:57:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_certifi")
public class RptBasicInfoCertifi extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 证书编号 **/
	@Column(name = "LICE_NO", unique = false, nullable = true, length = 100)
	private String liceNo;
	
	/** 证书名称 **/
	@Column(name = "LICE_NAME", unique = false, nullable = true, length = 120)
	private String liceName;
	
	/** 有效期 **/
	@Column(name = "LICE_IDATE", unique = false, nullable = true, length = 20)
	private String liceIdate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param liceNo
	 */
	public void setLiceNo(String liceNo) {
		this.liceNo = liceNo;
	}
	
    /**
     * @return liceNo
     */
	public String getLiceNo() {
		return this.liceNo;
	}
	
	/**
	 * @param liceName
	 */
	public void setLiceName(String liceName) {
		this.liceName = liceName;
	}
	
    /**
     * @return liceName
     */
	public String getLiceName() {
		return this.liceName;
	}
	
	/**
	 * @param liceIdate
	 */
	public void setLiceIdate(String liceIdate) {
		this.liceIdate = liceIdate;
	}
	
    /**
     * @return liceIdate
     */
	public String getLiceIdate() {
		return this.liceIdate;
	}


}