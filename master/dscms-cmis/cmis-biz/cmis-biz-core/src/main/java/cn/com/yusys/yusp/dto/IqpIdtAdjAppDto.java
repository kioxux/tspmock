package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 授信审批通知书有效期变更dto
 */
public class IqpIdtAdjAppDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;

	/** 授信协议编号 **/
	@Column(name = "LMT_CTR_NO", unique = false, nullable = true, length = 40)
	private String lmtCtrNo;

	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = false, length = 16)
	private BigDecimal lmtAmt;

	/** 授信总额度 **/
	@Column(name = "LMT_TOTL_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal lmtTotlAmt;

	/** 循环额度**/
	@Column(name = "REVOLV_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal revolvAmt;

	/** 一次性额度**/
	@Column(name = "ONE_AMT", unique = false, nullable = false, length = 16)
	private BigDecimal oneAmt;

	/** 低风险业务类型  STD_ZB_LOW_RISK_TYP**/
	@Column(name = "LOW_RISK_TYPE", unique = false, nullable = false, length = 5)
	private String lowRiskType;

	/** 授信起始日期 **/
	@Column(name = "LMT_STAR_DATE", unique = false, nullable = false, length = 10)
	private String lmtStarDate;

	/** 授信到期日期 **/
	@Column(name = "LMT_END_DATE", unique = false, nullable = true, length = 10)
	private String lmtEndDate;

	/** 授信审批通知书有效期 **/
	@Column(name = "LMT_DOC_IDATE", unique = false, nullable = true, length = 10)
	private String lmtDocIdate;

	/** 调整后的授信审批通知书有效期 **/
	@Column(name = "ADJUST_IDATE", unique = false, nullable = true, length = 4)
	private String AdjustIdate;

	/** 调整原因 **/
	@Column(name = "ADJUST_RESN", unique = false, nullable = true, length = 400)
	private String adjustResn;

	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getIqpSerno() {
		return iqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	public String getLmtCtrNo() {
		return lmtCtrNo;
	}

	public void setLmtCtrNo(String lmtCtrNo) {
		this.lmtCtrNo = lmtCtrNo;
	}

	public BigDecimal getLmtAmt() {
		return lmtAmt;
	}

	public void setLmtAmt(BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}

	public BigDecimal getLmtTotlAmt() {
		return lmtTotlAmt;
	}

	public void setLmtTotlAmt(BigDecimal lmtTotlAmt) {
		this.lmtTotlAmt = lmtTotlAmt;
	}

	public BigDecimal getRevolvAmt() {
		return revolvAmt;
	}

	public void setRevolvAmt(BigDecimal revolvAmt) {
		this.revolvAmt = revolvAmt;
	}

	public BigDecimal getOneAmt() {
		return oneAmt;
	}

	public void setOneAmt(BigDecimal oneAmt) {
		this.oneAmt = oneAmt;
	}

	public String getLowRiskType() {
		return lowRiskType;
	}

	public void setLowRiskType(String lowRiskType) {
		this.lowRiskType = lowRiskType;
	}

	public String getLmtStarDate() {
		return lmtStarDate;
	}

	public void setLmtStarDate(String lmtStarDate) {
		this.lmtStarDate = lmtStarDate;
	}

	public String getLmtEndDate() {
		return lmtEndDate;
	}

	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate;
	}

	public String getLmtDocIdate() {
		return lmtDocIdate;
	}

	public void setLmtDocIdate(String lmtDocIdate) {
		this.lmtDocIdate = lmtDocIdate;
	}

	public String getAdjustIdate() {
		return AdjustIdate;
	}

	public void setAdjustIdate(String adjustIdate) {
		AdjustIdate = adjustIdate;
	}

	public String getAdjustResn() {
		return adjustResn;
	}

	public void setAdjustResn(String adjustResn) {
		this.adjustResn = adjustResn;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getInputId() {
		return inputId;
	}

	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	public String getInputBrId() {
		return inputBrId;
	}

	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getUpdId() {
		return updId;
	}

	public void setUpdId(String updId) {
		this.updId = updId;
	}

	public String getUpdBrId() {
		return updBrId;
	}

	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getOprType() {
		return oprType;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
}