package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicCptAnly
 * @类描述: lmt_sig_invest_basic_cpt_anly数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 19:07:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSigInvestBasicCptAnlyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 底层分析类别值 **/
	private String bottomAnalyClsValve;
	
	/** 担保方式 **/
	private String guarMode;
	
	/** 单户余额区间 **/
	private String singleBalanceRange;
	
	/** 利率分布 **/
	private String rateDistribution;
	
	/** 剩余期限 **/
	private String remainingTerm;
	
	/** 行业 **/
	private String trade;
	
	/** 余额占比 **/
	private java.math.BigDecimal balPerc;
	
	/** 笔数占比 **/
	private java.math.BigDecimal qntPerc;
	
	/** 分布类型 **/
	private String distributType;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近更新人 **/
	private String updId;
	
	/** 最近更新机构 **/
	private String updBrId;
	
	/** 最近更新日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bottomAnalyClsValve
	 */
	public void setBottomAnalyClsValve(String bottomAnalyClsValve) {
		this.bottomAnalyClsValve = bottomAnalyClsValve == null ? null : bottomAnalyClsValve.trim();
	}
	
    /**
     * @return BottomAnalyClsValve
     */	
	public String getBottomAnalyClsValve() {
		return this.bottomAnalyClsValve;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode == null ? null : guarMode.trim();
	}
	
    /**
     * @return GuarMode
     */	
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param singleBalanceRange
	 */
	public void setSingleBalanceRange(String singleBalanceRange) {
		this.singleBalanceRange = singleBalanceRange == null ? null : singleBalanceRange.trim();
	}
	
    /**
     * @return SingleBalanceRange
     */	
	public String getSingleBalanceRange() {
		return this.singleBalanceRange;
	}
	
	/**
	 * @param rateDistribution
	 */
	public void setRateDistribution(String rateDistribution) {
		this.rateDistribution = rateDistribution == null ? null : rateDistribution.trim();
	}
	
    /**
     * @return RateDistribution
     */	
	public String getRateDistribution() {
		return this.rateDistribution;
	}
	
	/**
	 * @param remainingTerm
	 */
	public void setRemainingTerm(String remainingTerm) {
		this.remainingTerm = remainingTerm == null ? null : remainingTerm.trim();
	}
	
    /**
     * @return RemainingTerm
     */	
	public String getRemainingTerm() {
		return this.remainingTerm;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade == null ? null : trade.trim();
	}
	
    /**
     * @return Trade
     */	
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param balPerc
	 */
	public void setBalPerc(java.math.BigDecimal balPerc) {
		this.balPerc = balPerc;
	}
	
    /**
     * @return BalPerc
     */	
	public java.math.BigDecimal getBalPerc() {
		return this.balPerc;
	}
	
	/**
	 * @param qntPerc
	 */
	public void setQntPerc(java.math.BigDecimal qntPerc) {
		this.qntPerc = qntPerc;
	}
	
    /**
     * @return QntPerc
     */	
	public java.math.BigDecimal getQntPerc() {
		return this.qntPerc;
	}
	
	/**
	 * @param distributType
	 */
	public void setDistributType(String distributType) {
		this.distributType = distributType == null ? null : distributType.trim();
	}
	
    /**
     * @return DistributType
     */	
	public String getDistributType() {
		return this.distributType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}