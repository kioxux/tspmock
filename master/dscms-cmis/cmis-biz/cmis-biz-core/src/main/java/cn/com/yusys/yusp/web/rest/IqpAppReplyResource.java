/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAppReply;
import cn.com.yusys.yusp.service.IqpAppReplyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAppReplyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: wangyukun
 * @创建时间: 2021-05-05 13:46:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "业务申请批复")
@RequestMapping("/api/iqpappreply")
public class IqpAppReplyResource {
    @Autowired
    private IqpAppReplyService iqpAppReplyService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAppReply>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAppReply> list = iqpAppReplyService.selectAll(queryModel);
        return new ResultDto<List<IqpAppReply>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAppReply>> index(QueryModel queryModel) {
        List<IqpAppReply> list = iqpAppReplyService.selectByModel(queryModel);
        return new ResultDto<List<IqpAppReply>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{replyNo}")
    protected ResultDto<IqpAppReply> show(@PathVariable("replyNo") String replyNo) {
        IqpAppReply iqpAppReply = iqpAppReplyService.selectByPrimaryKey(replyNo);
        return new ResultDto<IqpAppReply>(iqpAppReply);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAppReply> create(@RequestBody IqpAppReply iqpAppReply) throws URISyntaxException {
        iqpAppReplyService.insert(iqpAppReply);
        return new ResultDto<IqpAppReply>(iqpAppReply);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAppReply iqpAppReply) throws URISyntaxException {
        int result = iqpAppReplyService.update(iqpAppReply);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{replyNo}")
    protected ResultDto<Integer> delete(@PathVariable("replyNo") String replyNo) {
        int result = iqpAppReplyService.deleteByPrimaryKey(replyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAppReplyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
