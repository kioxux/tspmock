package cn.com.yusys.yusp.web.server.xdtz0031;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0031.req.Xdtz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.AccLoanList;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.Xdtz0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.xdtz0031.Xdtz0031Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 接口处理类:根据合同号获取借据信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0031:根据合同号获取借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0031Resource.class);


    @Autowired
    private Xdtz0031Service xdtz0031Service;

    @Autowired
    private CommonService commonService;

    /**
     * 交易码：xdtz0031
     * 交易描述：根据合同号获取借据信息
     *
     * @param xdtz0031DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0031:根据合同号获取借据信息")
    @PostMapping("/xdtz0031")
    protected @ResponseBody
    ResultDto<Xdtz0031DataRespDto> xdtz0031(@Validated @RequestBody Xdtz0031DataReqDto xdtz0031DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031DataReqDto));
        Xdtz0031DataRespDto xdtz0031DataRespDto = new Xdtz0031DataRespDto();// 响应Dto:根据合同号获取借据信息
        ResultDto<Xdtz0031DataRespDto> xdtz0031DataResultDto = new ResultDto<>();
        // 从xdtz0031DataReqDto获取业务值进行业务逻辑处理
        String cert_code = xdtz0031DataReqDto.getCert_code();//证件号
        String cus_id = xdtz0031DataReqDto.getCus_id();//客户号
        String cont_no = xdtz0031DataReqDto.getCont_no();//合同号
        String account_status = xdtz0031DataReqDto.getAccount_status();//借据状态
        try {
            if (StringUtils.isBlank(cert_code) && StringUtils.isBlank(cus_id) && StringUtils.isBlank(cont_no)) {
                xdtz0031DataResultDto.setCode(EpbEnum.EPB090008.key);
                xdtz0031DataResultDto.setMessage(EpbEnum.EPB090008.value);
                xdtz0031DataResultDto.setData(xdtz0031DataRespDto);
                logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031DataResultDto));
                return xdtz0031DataResultDto;
            }
            // 调用xdtz0031Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031DataReqDto));
            List<AccLoanList> loanList = xdtz0031Service.getLoanList(xdtz0031DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(loanList));
            xdtz0031DataRespDto.setAccLoanList(loanList);
            // 封装xdtz0031DataResultDto中正确的返回码和返回信息
            xdtz0031DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0031DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, e.getMessage());
            // 封装xdtz0031DataResultDto中异常返回码和返回信息
            xdtz0031DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0031DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0031DataRespDto到xdtz0031DataResultDto中
        xdtz0031DataResultDto.setData(xdtz0031DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031DataResultDto));
        return xdtz0031DataResultDto;
    }
}
