package cn.com.yusys.yusp.constant;
/**
 * 服务端字典常量类
 */
public class CommonConstance {
    /** 合同查询类型 01：信用证 */
    public static final String CONT_QUERY_TYPE_01 = "01";
    /** 合同查询类型 02：保函 */
    public static final String CONT_QUERY_TYPE_02 = "02";
    /** 合同查询类型 03：贸易融资 */
    public static final String CONT_QUERY_TYPE_03 = "03";
    /** 合同查询类型 04：福费廷 */
    public static final String CONT_QUERY_TYPE_04 = "04";

    /** 合同类型 01 借款 */
    public static final String CONT_TYPE_01 = "01";
    /** 合同类型 01 担保 */
    public static final String CONT_TYPE_02 = "02";

    /** 操作成功标志位 S 成功 */
    public static final String OP_FLAG_S = "S";
    /** 操作成功标志位 F 失败 */
    public static final String OP_FLAG_F = "F";
    /** 描述信息 操作成功 */
    public static final String OP_MSG_S = "操作成功！";
    /** 描述信息 操作失败 */
    public static final String OP_MSG_F = "操作失败！";

    /** XDHT0007接口合同查询类型 01-借款 */
    public static final String CONT_QRY_TYPE_01 = "01";
    /** XDHT0007接口合同查询类型 02-担保 */
    public static final String CONT_QRY_TYPE_02 = "02";

    /** 签约视频是否上传 0-已上传  */
    public static final String SIGN_VIDEO_IS_UPLOAD_0 = "0";
    /** 签约视频是否上传 1-未上传  */
    public static final String SIGN_VIDEO_IS_UPLOAD_1 = "1";

    /** 是否 Y:是  */
    public static final String STD_YES_OR_NO_Y = "Y";
    /** 是否 Y:是  */
    public static final String STD_YES_OR_NO_N = "N";

    /** 合同状态 100:未生效  */
    public static final String STD_ZB_CONT_TYP_100 = "100";
    /** 合同状态 200:生效  */
    public static final String STD_ZB_CONT_TYP_200 = "200";
    /** 合同状态 300:注销  */
    public static final String STD_ZB_CONT_TYP_300 = "300";
    /** 合同状态 400:冻结  */
    public static final String STD_ZB_CONT_TYP_400 = "400";
    /** 合同状态 800:已出账  */
    public static final String STD_ZB_CONT_TYP_800 = "800";
    /** 合同状态 999:未生效  */
    public static final String STD_ZB_CONT_TYP_999 = "999";


    /** 票据类型  1 银行承兑汇票直贴 */
    public static final String DRFT_TYPE_1 = "1";
    /** 票据类型  2 商业承兑汇票直贴 */
    public static final String DRFT_TYPE_2 = "2";
    /** 票据类型  3 银行承兑电票贴现 */
    public static final String DRFT_TYPE_3 = "3";
    /** 票据类型  4 商业承兑电票贴现 */
    public static final String DRFT_TYPE_4 = "4";

    /** 批复状态  01 生效 */
    public static final String ACC_STATUS_01 = "01";
    /** 批复状态  11 冻结 */
    public static final String ACC_STATUS_11 = "11";
    /** 批复状态  90 失效未结清 */
    public static final String ACC_STATUS_90 = "90";
    /** 批复状态  99 失效已结清 */
    public static final String ACC_STATUS_99 = "99";

    /** 票据产品编号和名称  052198 银行承兑汇票直现 */
    public static final String STD_PJ_PRD_ID_052198 = "052198";
    public static final String STD_PJ_PRD_NAME_052198 = "银行承兑汇票直现";

    /** 票据产品编号和名称  052199 商业承兑汇票直现 */
    public static final String STD_PJ_PRD_ID_052199 = "052199";
    public static final String STD_PJ_PRD_NAME_052199 = "商业承兑汇票直现";

    /** 票据产品编号和名称  053198 银行承兑电票贴现 */
    public static final String STD_PJ_PRD_ID_053198 = "053198";
    public static final String STD_PJ_PRD_NAME_053198 = "银行承兑电票贴现";

    /** 票据产品编号和名称  053199 商业承兑电票贴现 */
    public static final String STD_PJ_PRD_ID_053199 = "053199";
    public static final String STD_PJ_PRD_NAME_053199 = "商业承兑电票贴现";

    /** 操作类型 01 新增 */
    public static final String OPR_TYPE_ADD = "01";
    /** 操作类型 02 删除*/
    public static final String OPR_TYPE_DELETE = "02";

    /**查询类型 01 */
    public static final String QUERY_TYPE_01 = "01";
    /**查询类型 02 */
    public static final String QUERY_TYPE_02 = "02";
    /**查询类型 03 */
    public static final String QUERY_TYPE_03 = "03";
    /**查询类型 04 */
    public static final String QUERY_TYPE_04 = "04";
    /**查询类型 05 */
    public static final String QUERY_TYPE_05 = "05";


    /**
     * 担保合同和业务的关系 关联关系
     * 1生效*/
    public static final String STD_ZB_CORRE_REL_1="1";
    /**2新增*/
    public static final String STD_ZB_CORRE_REL_2="2";
    /**3解除*/
    public static final String STD_ZB_CORRE_REL_3="3";

    /** 合同类型 1 一般合同 */
    public static final String CONT_TYPE_1 = "1";
    /** 合同类型 2 最高额合同 */
    public static final String CONT_TYPE_2 = "2";
    /** 合同类型 3 最高额授信协议 */
    public static final String CONT_TYPE_3 = "3";

    /** 担保方式 00 信用 */
    public static final String GUAR_MODE_00 = "00";
    /** 担保方式 10 抵押 */
    public static final String GUAR_MODE_10 = "10";
    /** 担保方式 20 质押 */
    public static final String GUAR_MODE_20 = "20";
    /** 担保方式 21 低风险质押 */
    public static final String GUAR_MODE_21 = "21";
    /** 担保方式 22 准全额质押 */
    public static final String GUAR_MODE_22 = "22";
    /** 担保方式 30 保证 */
    public static final String GUAR_MODE_30 = "30";
    /** 担保方式 40 全额保证金 */
    public static final String GUAR_MODE_40 = "40";
    /** 担保方式 50 准全额保证金 */
    public static final String GUAR_MODE_50 = "50";
    /** 担保方式 60 低风险 */
    public static final String GUAR_MODE_60 = "60";

    /** 利率调整方式 01 固定利率 */
    public static final String RATE_ADJ_MODE_01 = "01";
    /** 利率调整方式 02 浮动利率 */
    public static final String RATE_ADJ_MODE_02 = "02";

    /** LPR利率区间 A1 一年期 */
    public static final String LPR_PRICE_INTERVAL_A1 = "A1";
    /** LPR利率区间 A2 五年期 */
    public static final String LPR_PRICE_INTERVAL_A2 = "A2";

    /**
     * 是否 1是  0否
     */
    public static final String STD_ZB_YES_NO_0="0";
    public static final String STD_ZB_YES_NO_1="1";

    /**
     * 资产类型（STD_ASPL_ASSET_TYPE）
     * 01银行承兑汇票（电子）
     * 02本行存单（电子）
     * 03已承兑出口信用证向下单据
     * 04已承兑国内信用证向下单据
     */
     public static final String STD_ASPL_ASSET_TYPE_01 = "01";
     public static final String STD_ASPL_ASSET_TYPE_02 = "02";
     public static final String STD_ASPL_ASSET_TYPE_03 = "03";
     public static final String STD_ASPL_ASSET_TYPE_04 = "04";




    // (生效)状态
    /** 0-失效 **/
    public static final String STATUS_0 = "0";
    /** 1-生效 **/
    public static final String STATUS_1 = "1";

    // 导入模式
    /** 01-人工维护 **/
    public static final String IMPORT_MODE_01 = "01";
    /** 02-自动 **/
    public static final String IMPORT_MODE_02 = "02";

    // 审批状态
    /** 000-待发起 **/
    public static final String APPROVE_STATUS_000 = "000";

    /**
     * 台账状态
     * STD_ACC_STATUS
     * 0已关闭
     * 1正常
     * 2逾期
     * 3呆滞
     * 4呆账
     * 5已核销
     * 6未出账
     * 7作废
     */
     public static  final String STD_ACC_STATUS_0 = "0";
     public static  final String STD_ACC_STATUS_1 = "1";
     public static  final String STD_ACC_STATUS_2 = "2";
     public static  final String STD_ACC_STATUS_3 = "3";
     public static  final String STD_ACC_STATUS_4 = "4";
     public static  final String STD_ACC_STATUS_5 = "5";
     public static  final String STD_ACC_STATUS_6 = "6";
     public static  final String STD_ACC_STATUS_7 = "7";

    /**
     * 所属条线
     * STD_BELG_LINE
     * 01 小微
     * 02 零售
     * 03 对公
     * 04 资产池
     */
    public static  final String STD_BELG_LINE_01 = "01";
    public static  final String STD_BELG_LINE_02 = "02";
    public static  final String STD_BELG_LINE_03 = "03";
    public static  final String STD_BELG_LINE_04 = "04";

    /** 批复台账状态 01 生效 */
    public static final String STD_REPLY_STATUS_01 = "01";
    /** 批复台账状态 02 失效 */
    public static final String STD_REPLY_STATUS_02 = "02";

    /**
     * 是否自动出账 STD_PVP_MODE
     * 1 系统自动出账
     * 2 客户经理手动出账
     */
    public static final String STD_PVP_MODE_1 = "1";
    public static final String STD_PVP_MODE_2 = "2";

    /**
     * 区域中心负责人及分部部长下的小微客户经理Key
     */
    public static final String REDIS_KEY_AREA_XW_USER = "areaXwUser";

    /**
     * 区域中心负责人及分部部长下的机构Key
     */
    public static final String REDIS_KEY_AREA_XW_ORG = "areaXwOrg";
}
