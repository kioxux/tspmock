/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocReadDetailInfo
 * @类描述: doc_read_detail_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 18:57:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "doc_read_detail_info")
public class DocReadDetailInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调阅明细流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "DRDI_SERNO")
	private String drdiSerno;
	
	/** 调阅申请编号 **/
	@Column(name = "DRAI_SERNO", unique = false, nullable = true, length = 40)
	private String draiSerno;
	
	/** 档案编号 **/
	@Column(name = "DOC_NO", unique = false, nullable = true, length = 40)
	private String docNo;
	
	/** 档案分类 **/
	@Column(name = "DOC_CLASS", unique = false, nullable = true, length = 5)
	private String docClass;
	
	/** 档案类型 **/
	@Column(name = "DOC_TYPE", unique = false, nullable = true, length = 5)
	private String docType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 关联业务编号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = true, length = 40)
	private String bizSerno;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 入库操作人 **/
	@Column(name = "OPT_USR", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName",refFieldName="optUsrName" )
	private String optUsr;
	
	/** 入库操作机构 **/
	@Column(name = "OPT_ORG", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="optOrgName" )
	private String optOrg;
	
	/** 入库操作时间 **/
	@Column(name = "OPT_DATE", unique = false, nullable = true, length = 20)
	private String optDate;
	
	/** 档案入库时间 **/
	@Column(name = "STORAGE_DATE", unique = false, nullable = true, length = 20)
	private String storageDate;
	
	/** 档案状态 **/
	@Column(name = "DOC_STAUTS", unique = false, nullable = true, length = 5)
	private String docStauts;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 档案入现金库时间 **/
	@Column(name = "STORAGE_CASH_DATE", unique = false, nullable = true, length = 20)
	private String storageCashDate;
	
	/**
	 * @param drdiSerno
	 */
	public void setDrdiSerno(String drdiSerno) {
		this.drdiSerno = drdiSerno;
	}
	
    /**
     * @return drdiSerno
     */
	public String getDrdiSerno() {
		return this.drdiSerno;
	}
	
	/**
	 * @param draiSerno
	 */
	public void setDraiSerno(String draiSerno) {
		this.draiSerno = draiSerno;
	}
	
    /**
     * @return draiSerno
     */
	public String getDraiSerno() {
		return this.draiSerno;
	}
	
	/**
	 * @param docNo
	 */
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	
    /**
     * @return docNo
     */
	public String getDocNo() {
		return this.docNo;
	}
	
	/**
	 * @param docClass
	 */
	public void setDocClass(String docClass) {
		this.docClass = docClass;
	}
	
    /**
     * @return docClass
     */
	public String getDocClass() {
		return this.docClass;
	}
	
	/**
	 * @param docType
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	
    /**
     * @return docType
     */
	public String getDocType() {
		return this.docType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr;
	}
	
    /**
     * @return optUsr
     */
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg;
	}
	
    /**
     * @return optOrg
     */
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param optDate
	 */
	public void setOptDate(String optDate) {
		this.optDate = optDate;
	}
	
    /**
     * @return optDate
     */
	public String getOptDate() {
		return this.optDate;
	}
	
	/**
	 * @param storageDate
	 */
	public void setStorageDate(String storageDate) {
		this.storageDate = storageDate;
	}
	
    /**
     * @return storageDate
     */
	public String getStorageDate() {
		return this.storageDate;
	}
	
	/**
	 * @param docStauts
	 */
	public void setDocStauts(String docStauts) {
		this.docStauts = docStauts;
	}
	
    /**
     * @return docStauts
     */
	public String getDocStauts() {
		return this.docStauts;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getStorageCashDate() {
		return storageCashDate;
	}

	public void setStorageCashDate(String storageCashDate) {
		this.storageCashDate = storageCashDate;
	}
}