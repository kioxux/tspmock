/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.compatible.dto.Obj;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChg;
import cn.com.yusys.yusp.service.LmtIntbankReplyChgService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-20 21:18:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankreplychg")
public class LmtIntbankReplyChgResource {
    @Autowired
    private LmtIntbankReplyChgService lmtIntbankReplyChgService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankReplyChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankReplyChg> list = lmtIntbankReplyChgService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankReplyChg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankReplyChg>> index(QueryModel queryModel) {
        List<LmtIntbankReplyChg> list = lmtIntbankReplyChgService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankReplyChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankReplyChg> show(@PathVariable("pkId") String pkId) {
        LmtIntbankReplyChg lmtIntbankReplyChg = lmtIntbankReplyChgService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankReplyChg>(lmtIntbankReplyChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankReplyChg> create(@RequestBody LmtIntbankReplyChg lmtIntbankReplyChg) throws URISyntaxException {
        lmtIntbankReplyChgService.insert(lmtIntbankReplyChg);
        return new ResultDto<LmtIntbankReplyChg>(lmtIntbankReplyChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankReplyChg lmtIntbankReplyChg) throws URISyntaxException {
        int result = lmtIntbankReplyChgService.update(lmtIntbankReplyChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankReplyChgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankReplyChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 条件查询批复变更
     * @param queryModel
     * @return
     */
    @PostMapping("/selectByCondition")
    protected ResultDto<List<LmtIntbankReplyChg>> selectByCondition(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" serno desc");
        }
        return new ResultDto<List<LmtIntbankReplyChg>>(lmtIntbankReplyChgService.selectByCondition(queryModel));
    }
    /**
     * 条件查询批复变更历史
     * @param queryModel
     * @return
     */
    @PostMapping("/selectHisByCondition")
    protected ResultDto<List<LmtIntbankReplyChg>> selectHisByCondition(@RequestBody  QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" serno desc");
        }
        return new ResultDto<List<LmtIntbankReplyChg>>(lmtIntbankReplyChgService.selectHisByCondition(queryModel));
    }
    /**
     * 新增批复变更表
     * @param map
     * @return
     */
    @PostMapping("/insertReplyChg")
    protected  ResultDto<Map<String,Object>> insertReplyChg(@RequestBody Map<String,Object> map){

        return new ResultDto<Map<String,Object>>(lmtIntbankReplyChgService.insertReplyChg(map));
    }

    /**
     * 根据申请流水号查询
     * @param map
     * @return
     */
    @PostMapping("selectBySerno")
    protected ResultDto<LmtIntbankReplyChg> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        return  new ResultDto<LmtIntbankReplyChg>(lmtIntbankReplyChgService.selectBySerno(serno));
    }

    /**
     * 根据批复编号查询
     * @param map
     * @return
     */
    @PostMapping("/selectByReplySerno")
    protected ResultDto<LmtIntbankReplyChg> selectByReplySerno(@RequestBody Map<String,Object> map){
        String replySerno = map.get("replySerno").toString();
        return  new ResultDto<LmtIntbankReplyChg>(lmtIntbankReplyChgService.selectByReplySerno(replySerno));
    }

    /**
     * 更改批复变更信息
     * @param queryModel
     * @return
     */
    @PostMapping("/updateReplyChg")
    protected  ResultDto<Integer> updateReplyChg(@RequestBody QueryModel queryModel){
        String lmtAmt = queryModel.getCondition().get("lmtAmt").toString();
        String replyChgContentMemo= queryModel.getCondition().get("replyChgContentMemo").toString();
        String serno =queryModel.getCondition().get("serno").toString();
        LmtIntbankReplyChg lmtIntbankReplyChg = new LmtIntbankReplyChg();
        lmtIntbankReplyChg.setLmtAmt(new BigDecimal(lmtAmt));
        lmtIntbankReplyChg.setReplyChgContentMemo(replyChgContentMemo);
        lmtIntbankReplyChg.setSerno(serno);
        int result = lmtIntbankReplyChgService.updateReplyChg(lmtIntbankReplyChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * 逻辑删除
     * @param
     * @return
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody LmtIntbankReplyChg lmtIntbankReplyChg){
        lmtIntbankReplyChg.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        int result = lmtIntbankReplyChgService.logicalDelete(lmtIntbankReplyChg);
        return new ResultDto<Integer>(result);
    }
}
