package cn.com.yusys.yusp.domain;

import java.io.Serializable;

public class HomePageDto<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    //标题
    private String title;
    //流程类型（业务类型）
    private String[] bizType;
    //业务总数
    private int total;
    //是否跳转业务页面
    private String flag;
    //数据
    private T data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getBizType() {
        return bizType;
    }

    public void setBizType(String[] bizType) {
        this.bizType = bizType;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
