package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfBuilProject;

import java.math.BigDecimal;
import java.util.Date;

public class  CreditCardBusinessListVo extends PageQuery {

    /** 客户姓名 **/
    private String cusName;

    /** 账号 **/
    private String cardNo;

    /** 账户类型 **/
    private String cardType;

    /** 账单日期 **/
    private String billDate;

    /** 上期账单金额 **/
    private java.math.BigDecimal upBillBal;

    /** 当期借记金额 **/
    private java.math.BigDecimal curDebitBal;

    /** 当期贷记金额 **/
    private java.math.BigDecimal curCrditBal;

    /** 全部应还款项 **/
    private java.math.BigDecimal allNeedPay;

    /** 当期账余额 **/
    private java.math.BigDecimal curAccountBal;

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public BigDecimal getUpBillBal() {
        return upBillBal;
    }

    public void setUpBillBal(BigDecimal upBillBal) {
        this.upBillBal = upBillBal;
    }

    public BigDecimal getCurDebitBal() {
        return curDebitBal;
    }

    public void setCurDebitBal(BigDecimal curDebitBal) {
        this.curDebitBal = curDebitBal;
    }

    public BigDecimal getCurCrditBal() {
        return curCrditBal;
    }

    public void setCurCrditBal(BigDecimal curCrditBal) {
        this.curCrditBal = curCrditBal;
    }

    public BigDecimal getAllNeedPay() {
        return allNeedPay;
    }

    public void setAllNeedPay(BigDecimal allNeedPay) {
        this.allNeedPay = allNeedPay;
    }

    public BigDecimal getCurAccountBal() {
        return curAccountBal;
    }

    public void setCurAccountBal(BigDecimal curAccountBal) {
        this.curAccountBal = curAccountBal;
    }
}
