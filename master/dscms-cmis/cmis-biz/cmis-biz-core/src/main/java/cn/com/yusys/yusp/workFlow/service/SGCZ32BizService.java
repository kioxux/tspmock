package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CoopPartnerStatusChgApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CoopPartnerStatusChgAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 合作方名单退出审批流程-分支机构（寿光）
 */
@Service
public class SGCZ32BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(SGCZ32BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CoopPartnerStatusChgAppService service;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CoopPartnerStatusChgApp domain = service.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("合作方名单退出审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("合作方名单退出审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApprStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("合作方名单退出审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                service.coopPartnerQuit(serno);
                log.info("合作方名单退出审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("合作方名单退出审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                List<NextNodeInfoDto> list = resultInstanceDto.getNextNodeInfos();
                list.stream().forEach(next->{
                    ResultDto<Boolean> flowResultDto = workflowCoreClient.isFirstNode(next.getNextNodeId());
                    if (flowResultDto.getData()) {
                        domain.setApprStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                        service.update(domain);
                    }
                });
                log.info("合作方名单退出审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("合作方名单退出审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("合作方名单退出审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApprStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("合作方名单退出审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApprStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("合作方名单退出审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("合作方名单退出审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ32.equals(flowCode);
    }
}
