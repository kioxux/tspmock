/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpGuarChgAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-20 16:50:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpGuarChgAppService {
    private final Logger log = LoggerFactory.getLogger(IqpGuarChgAppService.class);//定义log

    @Autowired
    private IqpGuarChgAppMapper iqpGuarChgAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private DocImageSpplInfoService docImageSpplInfoService;

    @Autowired
    private BusinessInformationService businessInformationService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private IqpGuarChgAppRelService iqpGuarChgAppRelService;

    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpGuarChgApp selectByPrimaryKey(String serno) {
        return iqpGuarChgAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpGuarChgApp> selectAll(QueryModel model) {
        List<IqpGuarChgApp> records = (List<IqpGuarChgApp>) iqpGuarChgAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpGuarChgApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpGuarChgApp> list = iqpGuarChgAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpGuarChgApp record) {
        return iqpGuarChgAppMapper.insert(record);
    }

    @Autowired
    private IqpGuarChgAppRelMapper iqpGuarChgAppRelMapper;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CtrHighAmtAgrContMapper ctrHighAmtAgrContMapper;


    /**
     * @param record 实体类
     * @return int
     * @author tangxun
     * @date 2021/5/17 14:37
     * @version 1.0.0
     * @desc 新增担保变更
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int insertSelective(IqpGuarChgApp record) {

        QueryModel model1 = new QueryModel();
        model1.addCondition("contNo", record.getContNo());
        model1.addCondition("apply", "Y");
        List<IqpGuarChgApp> list1 = this.selectByModel(model1);
        if (list1.size() > 0) {
            throw BizException.error(null, "999999", "\"该合同存在在途担保变更申请，请勿重复发起！\"" + "保存失败！");
        }

        //变更的合同号
        int res = 0;
        String contNo = record.getContNo();
        CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(contNo);
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContMapper.selectDataByContNo(contNo);
        if (Objects.nonNull(ctrLoanCont)) {
            record.setCusId(ctrLoanCont.getCusId());
            record.setCusName(ctrLoanCont.getCusName());
            record.setContAmt(ctrLoanCont.getContAmt());
            record.setContBalance(ctrLoanCont.getContBalance());
            record.setContType(ctrLoanCont.getContType());
            record.setPrdId(ctrLoanCont.getPrdId());
            record.setPrdName(ctrLoanCont.getPrdName());
            record.setContStartDate(ctrLoanCont.getContStartDate());
            record.setContEndDate(ctrLoanCont.getContEndDate());
            record.setGuarWay(ctrLoanCont.getGuarWay());
            record.setBelgLine(ctrLoanCont.getBelgLine());
            record.setApproveStatus("000");
            record.setContApproveStatus("000");
            record.setAppChnl("02"); // 申请渠道 02-PC端
            record.setManagerId(ctrLoanCont.getManagerId());
            record.setManagerBrId(ctrLoanCont.getManagerBrId());
            User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
            record.setInputId(userInfo.getLoginCode()); // 当前用户号
            record.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
            record.setInputDate(DateUtils.getCurrDateStr());
            record.setCreateTime(DateUtils.getCurrTimestamp());
            record.setUpdateTime(record.getCreateTime());
            record.setUpdDate(record.getInputDate());
            record.setUpdId(record.getInputId());
            record.setUpdBrId(record.getInputBrId());
            res = iqpGuarChgAppMapper.insertSelective(record);
            //担保合同列表 先根据流水号查，再根据合同号查
            List<GrtGuarCont> guarConts = grtGuarContService.selectDataBySerno(ctrLoanCont.getIqpSerno());
            if (guarConts == null || guarConts.size() == 0) {
                guarConts = grtGuarContMapper.selectDataByContNo(contNo);
            }
            guarConts.stream().map(grtGuarCont -> {
                IqpGuarChgAppRel iqpGuarChgAppRel = new IqpGuarChgAppRel();
                iqpGuarChgAppRel.setSerno(record.getIqpSerno());
                iqpGuarChgAppRel.setContNo(contNo);
                iqpGuarChgAppRel.setGuarContNo(grtGuarCont.getGuarContNo());
                iqpGuarChgAppRel.setGuarContType(grtGuarCont.getGuarContType());
                iqpGuarChgAppRel.setGuarWay(grtGuarCont.getGuarWay());
                iqpGuarChgAppRel.setGuarAmt(grtGuarCont.getGuarAmt());
                iqpGuarChgAppRel.setCusId(record.getCusId());
                iqpGuarChgAppRel.setIsUnderCont("1");
                //默认不变
                iqpGuarChgAppRel.setOpFlag("01");
                iqpGuarChgAppRel.setCusName(record.getCusName());
                iqpGuarChgAppRel.setGuarContState(grtGuarCont.getGuarContState());
                iqpGuarChgAppRel.setInputDate(grtGuarCont.getInputDate());
                iqpGuarChgAppRel.setManagerId(record.getManagerId());
                iqpGuarChgAppRel.setManagerBrId(record.getManagerBrId());
                iqpGuarChgAppRel.setInputId(record.getInputId()); // 当前用户号
                iqpGuarChgAppRel.setInputBrId(record.getInputBrId()); // 当前用户机构
                iqpGuarChgAppRel.setInputDate(DateUtils.getCurrDateStr());
                iqpGuarChgAppRel.setCreateTime(DateUtils.getCurrTimestamp());
                iqpGuarChgAppRel.setUpdateTime(iqpGuarChgAppRel.getCreateTime());
                iqpGuarChgAppRel.setUpdDate(iqpGuarChgAppRel.getInputDate());
                iqpGuarChgAppRel.setUpdId(iqpGuarChgAppRel.getInputId());
                iqpGuarChgAppRel.setUpdBrId(iqpGuarChgAppRel.getInputBrId());
                return iqpGuarChgAppRel;
            }).map(iqpGuarChgAppRelMapper::insertSelective).count();
        } else if (Objects.nonNull(ctrHighAmtAgrCont)) {
            record.setCusId(ctrHighAmtAgrCont.getCusId());
            record.setCusName(ctrHighAmtAgrCont.getCusName());
            record.setContAmt(ctrHighAmtAgrCont.getAgrAmt());
            record.setContBalance(ctrHighAmtAgrCont.getAgrContHighAvlAmt());
            record.setContType(ctrHighAmtAgrCont.getContType());
            record.setPrdId("");
            record.setPrdName("");
            record.setContStartDate(ctrHighAmtAgrCont.getStartDate());
            record.setContEndDate(ctrHighAmtAgrCont.getEndDate());
            record.setGuarWay(ctrHighAmtAgrCont.getGuarMode());
            record.setBelgLine(ctrHighAmtAgrCont.getBelgLine());
            record.setApproveStatus("000");
            record.setContApproveStatus("000");
            record.setAppChnl("02"); // 申请渠道 02-PC端
            record.setManagerId(ctrHighAmtAgrCont.getManagerId());
            record.setManagerBrId(ctrHighAmtAgrCont.getManagerBrId());
            User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
            record.setInputId(userInfo.getLoginCode()); // 当前用户号
            record.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
            record.setInputDate(DateUtils.getCurrDateStr());
            record.setCreateTime(DateUtils.getCurrTimestamp());
            record.setUpdateTime(record.getCreateTime());
            record.setUpdDate(record.getInputDate());
            record.setUpdId(record.getInputId());
            record.setUpdBrId(record.getInputBrId());
            res = iqpGuarChgAppMapper.insertSelective(record);
            //担保合同列表 先根据流水号查，再根据合同号查
            List<GrtGuarCont> guarConts = grtGuarContMapper.selectDataByContNo(contNo);
            guarConts.stream().map(grtGuarCont -> {
                IqpGuarChgAppRel iqpGuarChgAppRel = new IqpGuarChgAppRel();
                iqpGuarChgAppRel.setSerno(record.getIqpSerno());
                iqpGuarChgAppRel.setContNo(contNo);
                iqpGuarChgAppRel.setGuarContNo(grtGuarCont.getGuarContNo());
                iqpGuarChgAppRel.setGuarContType(grtGuarCont.getGuarContType());
                iqpGuarChgAppRel.setGuarWay(grtGuarCont.getGuarWay());
                iqpGuarChgAppRel.setGuarAmt(grtGuarCont.getGuarAmt());
                iqpGuarChgAppRel.setCusId(record.getCusId());
                iqpGuarChgAppRel.setIsUnderCont("1");
                //默认不变
                iqpGuarChgAppRel.setOpFlag("01");
                iqpGuarChgAppRel.setCusName(record.getCusName());
                iqpGuarChgAppRel.setGuarContState(grtGuarCont.getGuarContState());
                iqpGuarChgAppRel.setInputDate(grtGuarCont.getInputDate());
                iqpGuarChgAppRel.setManagerId(record.getManagerId());
                iqpGuarChgAppRel.setManagerBrId(record.getManagerBrId());
                iqpGuarChgAppRel.setInputId(record.getInputId()); // 当前用户号
                iqpGuarChgAppRel.setInputBrId(record.getInputBrId()); // 当前用户机构
                iqpGuarChgAppRel.setInputDate(DateUtils.getCurrDateStr());
                iqpGuarChgAppRel.setCreateTime(DateUtils.getCurrTimestamp());
                iqpGuarChgAppRel.setUpdateTime(iqpGuarChgAppRel.getCreateTime());
                iqpGuarChgAppRel.setUpdDate(iqpGuarChgAppRel.getInputDate());
                iqpGuarChgAppRel.setUpdId(iqpGuarChgAppRel.getInputId());
                iqpGuarChgAppRel.setUpdBrId(iqpGuarChgAppRel.getInputBrId());
                return iqpGuarChgAppRel;
            }).map(iqpGuarChgAppRelMapper::insertSelective).count();
        } else {
            throw BizException.error(null, "5000", "未找到对应的合同");
        }

        return res;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpGuarChgApp record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpGuarChgAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpGuarChgApp record) {
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpGuarChgAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return iqpGuarChgAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", ids);
        queryModel.addCondition("isUnderCont", "0");
        List<IqpGuarChgAppRel> list = iqpGuarChgAppRelService.selectByModel(queryModel);
        if (null != list && list.size() > 0) {
            for (IqpGuarChgAppRel iqpGuarChgAppRel : list) {
                String guarContNo=iqpGuarChgAppRel.getGuarContNo();
                grtGuarBizRstRelMapper.deleteByGuarNo(guarContNo);
            }
        }
        iqpGuarChgAppRelMapper.deleteBySernos(ids);
        return iqpGuarChgAppMapper.deleteByIds(ids);
    }

    /**
     * 推送首页提醒事项
     *
     * @param iqpGuarChgApp,messageType,comment,inputId,inputBrId add by  20210804
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendWbMsgNotice(IqpGuarChgApp iqpGuarChgApp, String messageType, String comment, String inputId, String inputBrId) throws Exception {
        log.info("担保变更申报审批流程申请启用授信申报审批流程【{}】，流程打回或退回操作，推送首页提升事项", iqpGuarChgApp.getIqpSerno());

        MessageSendDto messageSendDto = new MessageSendDto();
        Map<String, String> map = new HashMap<>();
        map.put("cusName", iqpGuarChgApp.getCusName());
        map.put("prdName", "担保变更申报");
        map.put("result", "打回");
        List<ReceivedUserDto> receivedUserList = new ArrayList<>();
        ReceivedUserDto receivedUserDto = new ReceivedUserDto();

        // 1--客户经理 2--借款人  借款人必输
        receivedUserDto.setReceivedUserType("1");// 发送人员类型
        receivedUserDto.setUserId(inputId);// 客户经理工号
        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
        receivedUserList.add(receivedUserDto);
        messageSendDto.setMessageType(CmisCommonConstants.MSG_CF_M_0016);
        messageSendDto.setParams(map);
        messageSendDto.setReceivedUserList(receivedUserList);
        messageSendService.sendMessage(messageSendDto);
    }

    /**
     * 根据担保合同编号查询关联的担保置换的记录数
     *
     * @param guarContNo
     * @return
     */
    public int countGuarDisplaceRecordsByGuarContNo(String guarContNo) {
        return iqpGuarChgAppMapper.countGuarDisplaceRecordsByGuarContNo(guarContNo);
    }

    /**
     * 资料未全生成影像补扫任务
     *
     * @author jijian_yx
     * @date 2021/8/27 21:12
     **/
    public void createImageSpplInfo(String iqpSerno, String bizType, String instanceId,String iqpName,String spplType) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(iqpSerno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            IqpGuarChgApp iqpGuarChgApp = iqpGuarChgAppMapper.selectByPrimaryKey(iqpSerno);
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(iqpSerno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(spplType);// 影像补扫类型 08:担保变更申请影像补扫 10:担保变更协议影像补扫
            docImageSpplClientDto.setCusId(iqpGuarChgApp.getCusId());// 客户号
            docImageSpplClientDto.setCusName(iqpGuarChgApp.getCusName());// 客户名称
            docImageSpplClientDto.setContNo(iqpGuarChgApp.getContNo());// 合同编号
            docImageSpplClientDto.setInputId(iqpGuarChgApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(iqpGuarChgApp.getManagerBrId());// 登记机构
            docImageSpplClientDto.setPrdId(iqpGuarChgApp.getPrdId());
            docImageSpplClientDto.setPrdName(iqpGuarChgApp.getPrdName());
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);

            //生成首页提醒,对象：责任机构下所有资料扫描岗FZH03
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            GetUserInfoByDutyCodeDto getUserInfoByDutyCodeDto = new GetUserInfoByDutyCodeDto();
            getUserInfoByDutyCodeDto.setDutyCode("FZH03");
            getUserInfoByDutyCodeDto.setPageNum(1);
            getUserInfoByDutyCodeDto.setPageSize(300);
            List<AdminSmUserDto> adminSmUserDtoList = commonService.getUserInfoByDutyCodeDtoNew(getUserInfoByDutyCodeDto);
            if (null != adminSmUserDtoList && adminSmUserDtoList.size() > 0) {
                for (AdminSmUserDto adminSmUserDto : adminSmUserDtoList) {
                    if (Objects.equals(adminSmUserDto.getOrgId(), iqpGuarChgApp.getManagerBrId())) {
                        ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                        receivedUserDto.setReceivedUserType("1");// 发送人员类型
                        receivedUserDto.setUserId(adminSmUserDto.getUserCode());// 客户经理工号
                        receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                        receivedUserList.add(receivedUserDto);
                    }
                }
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            Map<String, String> map = new HashMap<>();
            map.put("cusName", iqpGuarChgApp.getCusName());
            map.put("iqpName", iqpName);
            messageSendDto.setMessageType("MSG_DA_M_0001");
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        }
    }
}
