/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpStpUnstpIntApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpStpUnstpIntAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.BizUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: IqpStpUnstpIntAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-01-13 18:08:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpStpUnstpIntAppService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(IqpStpUnstpIntAppService.class);

    @Autowired
    private IqpStpUnstpIntAppMapper iqpStpUnstpIntAppMapper;

    // 借据服务
    @Autowired
    private AccLoanService accLoanService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpStpUnstpIntApp selectByPrimaryKey(String iqpSerno) {
        return iqpStpUnstpIntAppMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpStpUnstpIntApp> selectAll(QueryModel model) {
        List<IqpStpUnstpIntApp> records = (List<IqpStpUnstpIntApp>) iqpStpUnstpIntAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpStpUnstpIntApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpStpUnstpIntApp> list = iqpStpUnstpIntAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpStpUnstpIntApp record) {
        return iqpStpUnstpIntAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpStpUnstpIntApp record) {
        return iqpStpUnstpIntAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpStpUnstpIntApp record) {
        return iqpStpUnstpIntAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpStpUnstpIntApp record) {
        return iqpStpUnstpIntAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpStpUnstpIntAppMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpStpUnstpIntAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveIqpStpUnstpIntAppLeadInfo
     * @方法描述: 通过向导页面的借据编号新增停息申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public Map saveIqpStpIntAppLeadInfo(String billNo) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        String iqpSerno = null;
        try {
            // 校验必须字段
            if (StringUtils.isBlank(billNo)) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_PARAM_FILED.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_PARAM_FILED.value;
                return rtnData;
            }

            // 1保存校验
            // 1.1 根据借据号查询借据台账状态为【正常】且停息状态是否存在唯一一条记录
            Map accLoanQueryMap = new HashMap();
            // 借据编号
            accLoanQueryMap.put("billNo", billNo);
            // 台账状态为正常
            accLoanQueryMap.put("accStatus", CmisBizConstants.IQP_ACC_STATUS_1);
            log.info(String.format("保存停息新增申请,借据号%s,获取正常台账状态数据", billNo));
            List<AccLoan> accLoanList = accLoanService.selectAccLoanByParams(accLoanQueryMap);
            if (CollectionUtils.isEmpty(accLoanList) && accLoanList.size() != 1) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_CHECK_ACC_LOAN_FAIL.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_CHECK_ACC_LOAN_FAIL.value;
                return rtnData;
            }

            // 1.2 根据借据号 查询是否存在已经待发起、审批中、追回、打回、拿回的新增数据 且数据状态为新增的数据
            Map iqpStpIntQueryMap = new HashMap();
            // 借据编号
            iqpStpIntQueryMap.put("billNo", billNo);
            // 数据状态为新增
            iqpStpIntQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            // 不允许重复新增
            iqpStpIntQueryMap.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            log.info(String.format("保存停息新增申请,借据号%s,获取停息数据", billNo));
            List<IqpStpUnstpIntApp> iqpStpUnstpIntAppList = iqpStpUnstpIntAppMapper.selectIqpStpUnstpIntAppInfoByParams(iqpStpIntQueryMap);
            if (!CollectionUtils.isEmpty(iqpStpUnstpIntAppList) && iqpStpUnstpIntAppList.size() > 0) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_EXISTS_BILL.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_EXISTS_BILL.value;
                return rtnData;
            }


            log.info(String.format("保存停息新增申请,借据号%s,数据-转化入参数据", billNo));
//            log.info(String.format("保存停息新增申请,借据号%s,数据映射处理-调用cmis-cfg服务接口获取映射关系-开始，源表:%s,目标表:%s",
//                    billNo, EcbEnum.IQP_CHG_SOURCE_TABLE_NAME, EcbEnum.IQP_STP_UNSTOP_INT_APP_SOURCE_TABLE_NAME));

            // 获取贷款台账数据
            AccLoan accLoan = accLoanList.get(0);
            //调用工具类
            BizUtils bizUtils = new BizUtils();
            IqpStpUnstpIntApp iqpStpUnstpIntApp = new IqpStpUnstpIntApp();
            iqpStpUnstpIntApp = (IqpStpUnstpIntApp) bizUtils.getMappingValueBySourceAndDisTable(accLoan, iqpStpUnstpIntApp);

            if (iqpStpUnstpIntApp == null) {
                rtnCode = EcbEnum.E_GETMAPPING_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETMAPPING_EXCEPTION.value;
                return rtnData;
            }

            // 通过流水号生成器生成业务流水号
//            iqpSerno = sequenceTemplateClient.getSequenceTemplate(EcbEnum.SEQ_IQP_STP_UNSTOP_INT_APP, new HashMap<>());
            log.info(String.format("保存停息新增申请,生成流水号%s", iqpSerno));
            // 停息申请赋值默认值
            iqpStpUnstpIntApp.setIqpSerno(iqpSerno);
            // 数据操作标志为新增
            iqpStpUnstpIntApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            // 停息/恢复计息标志为停息
//            iqpStpUnstpIntApp.setIntType(EcbEnum.STD_ZB_INT_TYPE_01);
            // 是否计收复利 默认是
//            iqpStpUnstpIntApp.setIsCalCi(EcbEnum.STD_ZB_YES_NO_Y);
            // 流程状态
            iqpStpUnstpIntApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);

            log.info(String.format("保存停息新增申请%s-获取当前登录用户数据", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_EXCEPTION.value;
                return rtnData;
            } else {
                iqpStpUnstpIntApp.setInputId(userInfo.getLoginCode());
                iqpStpUnstpIntApp.setInputBrId(userInfo.getOrg().getCode());
                iqpStpUnstpIntApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpStpUnstpIntApp.setUpdId(userInfo.getLoginCode());
                iqpStpUnstpIntApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpStpUnstpIntApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(String.format("保存停息新增申请,流水号%s,根据借据号%s,数据保存", iqpSerno, billNo));
            int count = iqpStpUnstpIntAppMapper.insert(iqpStpUnstpIntApp);
            if (count != 1){
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }


        } catch(YuspException e){
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存停息新增申请出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("iqpSerno", iqpSerno);
        }
        return rtnData;
    }


    /**
     * @方法名称: saveIqpUnStpIntAppLeadInfo
     * @方法描述: 通过向导页面的借据编号新增停息申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public Map saveIqpUnStpIntAppLeadInfo(String billNo) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        String iqpSerno = null;
        try {
            // 校验必须字段
            if (StringUtils.isBlank(billNo)) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_PARAM_FILED.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_PARAM_FILED.value;
                return rtnData;
            }

            // 1保存校验
            // 1.1 根据借据号查询借据台账状态为【正常】且停息状态是否存在唯一一条记录
            Map accLoanQueryMap = new HashMap();
            // 借据编号
            accLoanQueryMap.put("billNo", billNo);
            // 台账状态为正常
            accLoanQueryMap.put("accStatus", CmisBizConstants.IQP_ACC_STATUS_1);
            log.info(String.format("保存恢复计息新增申请,借据号%s,获取正常停息状态数据", billNo));
            List<AccLoan> accLoanList = accLoanService.selectAccLoanByParams(accLoanQueryMap);
            if (CollectionUtils.isEmpty(accLoanList) && accLoanList.size() != 1) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_CHECK_ACC_LOAN_FAIL.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_CHECK_ACC_LOAN_FAIL.value;
                return rtnData;
            }

            // 1.2 根据借据号 查询是否存在已经待发起、审批中、追回、打回、拿回的新增数据 且数据状态为新增的数据
            Map iqpStpIntQueryMap = new HashMap();
            // 借据编号
            iqpStpIntQueryMap.put("billNo", billNo);
            // 数据状态为新增
            iqpStpIntQueryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            // 不允许重复新增
            iqpStpIntQueryMap.put("applyExistsStatus", CmisCommonConstants.WF_STATUS_CAN_NOT_APPLY_SAME);
            log.info(String.format("保存恢复计息新增申请,借据号%s,获取停息数据", billNo));
            List<IqpStpUnstpIntApp> iqpStpUnstpIntAppList = iqpStpUnstpIntAppMapper.selectIqpStpUnstpIntAppInfoByParams(iqpStpIntQueryMap);
            if (CollectionUtils.isEmpty(iqpStpUnstpIntAppList) && iqpStpUnstpIntAppList.size() > 0) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_EXISTS_BILL.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_EXISTS_BILL.value;
                return rtnData;
            }


            log.info(String.format("保存恢复计息新增申请,借据号%s,数据-转化入参数据", billNo));
//            log.info(String.format("保存恢复计息新增申请,借据号%s,数据映射处理-调用cmis-cfg服务接口获取映射关系-开始，源表:%s,目标表:%s",
//                    billNo, EcbEnum.IQP_CHG_SOURCE_TABLE_NAME, EcbEnum.IQP_STP_UNSTOP_INT_APP_SOURCE_TABLE_NAME));

            // 获取贷款台账数据
            AccLoan accLoan = accLoanList.get(0);
            //调用工具类
            BizUtils bizUtils = new BizUtils();
            IqpStpUnstpIntApp iqpStpUnstpIntApp = new IqpStpUnstpIntApp();
            iqpStpUnstpIntApp = (IqpStpUnstpIntApp) bizUtils.getMappingValueBySourceAndDisTable(accLoan, iqpStpUnstpIntApp);

            if (iqpStpUnstpIntApp == null) {
                rtnCode = EcbEnum.E_GETMAPPING_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETMAPPING_EXCEPTION.value;
                return rtnData;
            }
            // 通过流水号生成器生成业务流水号
//            iqpSerno = sequenceTemplateClient.getSequenceTemplate(EcbEnum.SEQ_IQP_STP_UNSTOP_INT_APP, new HashMap<>());
            log.info(String.format("保存恢复计息新增申请,生成流水号%s", iqpSerno));
            // 停息申请赋值默认值
            iqpStpUnstpIntApp.setIqpSerno(iqpSerno);
            // 数据操作标志为新增
            iqpStpUnstpIntApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
//            // 停息/恢复计息标志为恢复计息
//            iqpStpUnstpIntApp.setIntType(EcbEnum.STD_ZB_INT_TYPE_02);
//            // 是否计收复利 默认是
//            iqpStpUnstpIntApp.setIsCalCi(EcbEnum.STD_ZB_YES_NO_Y);
            // 流程状态
            iqpStpUnstpIntApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);

            log.info(String.format("保存恢复计息新增申请%s-获取当前登录用户数据", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_EXCEPTION.value;
                return rtnData;
            } else {
                iqpStpUnstpIntApp.setInputId(userInfo.getLoginCode());
                iqpStpUnstpIntApp.setInputBrId(userInfo.getOrg().getCode());
                iqpStpUnstpIntApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpStpUnstpIntApp.setUpdId(userInfo.getLoginCode());
                iqpStpUnstpIntApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpStpUnstpIntApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(String.format("保存恢复计息新增申请,流水号%s,根据借据号%s,数据保存", iqpSerno, billNo));
            int count = iqpStpUnstpIntAppMapper.insert(iqpStpUnstpIntApp);
            if (count != 1){
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }

        } catch(YuspException e){
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存恢复计息新增申请出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("iqpSerno", iqpSerno);
        }
        return rtnData;
    }

    /***
     * 根据主键更新申请状态
     * @param iqpSerno
     * @param approveStatus
     * @return
     */
    public int updateIqpStpUnStpIntAppStatus(String iqpSerno, String approveStatus){
        IqpStpUnstpIntApp iqpStpUnstpIntApp = selectByPrimaryKey(iqpSerno);
        if (iqpStpUnstpIntApp != null){
            iqpStpUnstpIntApp.setApproveStatus(approveStatus);
            return updateSelective(iqpStpUnstpIntApp);
        }
        return 0;
    }

    /***
     *  停息/恢复计息申请流程审批提交之后的业务逻辑处理
     * @param iqpSerno
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String iqpSerno){

    }

    /***
     *  停息/恢复计息申请流程审批通过之后的业务逻辑处理
     * @param iqpSerno
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterEnd(String iqpSerno){

    }



}
