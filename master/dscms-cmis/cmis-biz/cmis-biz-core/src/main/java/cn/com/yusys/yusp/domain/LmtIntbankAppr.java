/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppr
 * @类描述: lmt_intbank_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 11:07:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_intbank_appr")
public class LmtIntbankAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 审批流水号 **/
	@Column(name = "APPROVE_SERNO")
	private String approveSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;
	
	/** 原授信批复流水号 **/
	@Column(name = "ORIGI_LMT_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiLmtReplySerno;
	
	/** 原授信期限 **/
	@Column(name = "ORIGI_LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtTerm;
	
	/** 原授信金额 **/
	@Column(name = "ORIGI_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLmtAmt;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 同业授信准入 **/
	@Column(name = "INTBANK_LMT_ADMIT", unique = false, nullable = true, length = 2000)
	private String intbankLmtAdmit;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 2000)
	private String indgtResult;
	
	/** 综合分析 **/
	@Column(name = "INTE_ANALY", unique = false, nullable = true, length = 65535)
	private String inteAnaly;
	
	/** 总行出具综合分析 **/
	@Column(name = "INTE_ANALY_ZH", unique = false, nullable = true, length = 65535)
	private String inteAnalyZh;
	
	/** 终审机构类型 **/
	@Column(name = "FINAL_APPR_BR_TYPE", unique = false, nullable = true, length = 5)
	private String finalApprBrType;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 评审结论 **/
	@Column(name = "REVIEW_RESULT", unique = false, nullable = true, length = 5)
	private String reviewResult;
	
	/** 总行出具评审结论 **/
	@Column(name = "REVIEW_RESULT_ZH", unique = false, nullable = true, length = 5)
	private String reviewResultZh;
	
	/** 结论性描述 **/
	@Column(name = "REST_DESC", unique = false, nullable = true, length = 65535)
	private String restDesc;
	
	/** 总行出具结论性描述 **/
	@Column(name = "REST_DESC_ZH", unique = false, nullable = true, length = 65535)
	private String restDescZh;
	
	/** 出具报告类型 **/
	@Column(name = "ISSUE_REPORT_TYPE", unique = false, nullable = true, length = 5)
	private String issueReportType;
	
	/** 是否大额授信 **/
	@Column(name = "IS_LARGE_LMT", unique = false, nullable = true, length = 5)
	private String isLargeLmt;
	
	/** 是否需报备董事长 **/
	@Column(name = "IS_REPORT_CHAIRMAN", unique = false, nullable = true, length = 5)
	private String isReportChairman;
	
	/** 是否上调审批权限 **/
	@Column(name = "IS_UPPER_APPR_AUTH", unique = false, nullable = true, length = 5)
	private String isUpperApprAuth;
	
	/** 经营情况和财务情况 **/
	@Column(name = "OPER_FINA_SITU", unique = false, nullable = true, length = 65535)
	private String operFinaSitu;
	
	/** 经营情况和财务情况图片路径 **/
	@Column(name = "OPER_FINA_SITU_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String operFinaSituPicturePath;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}
	
    /**
     * @return approveSerno
     */
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}
	
    /**
     * @return lmtType
     */
	public String getLmtType() {
		return this.lmtType;
	}
	
	/**
	 * @param origiLmtReplySerno
	 */
	public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
		this.origiLmtReplySerno = origiLmtReplySerno;
	}
	
    /**
     * @return origiLmtReplySerno
     */
	public String getOrigiLmtReplySerno() {
		return this.origiLmtReplySerno;
	}
	
	/**
	 * @param origiLmtTerm
	 */
	public void setOrigiLmtTerm(Integer origiLmtTerm) {
		this.origiLmtTerm = origiLmtTerm;
	}
	
    /**
     * @return origiLmtTerm
     */
	public Integer getOrigiLmtTerm() {
		return this.origiLmtTerm;
	}
	
	/**
	 * @param origiLmtAmt
	 */
	public void setOrigiLmtAmt(java.math.BigDecimal origiLmtAmt) {
		this.origiLmtAmt = origiLmtAmt;
	}
	
    /**
     * @return origiLmtAmt
     */
	public java.math.BigDecimal getOrigiLmtAmt() {
		return this.origiLmtAmt;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param intbankLmtAdmit
	 */
	public void setIntbankLmtAdmit(String intbankLmtAdmit) {
		this.intbankLmtAdmit = intbankLmtAdmit;
	}
	
    /**
     * @return intbankLmtAdmit
     */
	public String getIntbankLmtAdmit() {
		return this.intbankLmtAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly;
	}
	
    /**
     * @return inteAnaly
     */
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param inteAnalyZh
	 */
	public void setInteAnalyZh(String inteAnalyZh) {
		this.inteAnalyZh = inteAnalyZh;
	}
	
    /**
     * @return inteAnalyZh
     */
	public String getInteAnalyZh() {
		return this.inteAnalyZh;
	}
	
	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}
	
    /**
     * @return finalApprBrType
     */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param reviewResult
	 */
	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult;
	}
	
    /**
     * @return reviewResult
     */
	public String getReviewResult() {
		return this.reviewResult;
	}
	
	/**
	 * @param reviewResultZh
	 */
	public void setReviewResultZh(String reviewResultZh) {
		this.reviewResultZh = reviewResultZh;
	}
	
    /**
     * @return reviewResultZh
     */
	public String getReviewResultZh() {
		return this.reviewResultZh;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc;
	}
	
    /**
     * @return restDesc
     */
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param restDescZh
	 */
	public void setRestDescZh(String restDescZh) {
		this.restDescZh = restDescZh;
	}
	
    /**
     * @return restDescZh
     */
	public String getRestDescZh() {
		return this.restDescZh;
	}
	
	/**
	 * @param issueReportType
	 */
	public void setIssueReportType(String issueReportType) {
		this.issueReportType = issueReportType;
	}
	
    /**
     * @return issueReportType
     */
	public String getIssueReportType() {
		return this.issueReportType;
	}
	
	/**
	 * @param isLargeLmt
	 */
	public void setIsLargeLmt(String isLargeLmt) {
		this.isLargeLmt = isLargeLmt;
	}
	
    /**
     * @return isLargeLmt
     */
	public String getIsLargeLmt() {
		return this.isLargeLmt;
	}
	
	/**
	 * @param isReportChairman
	 */
	public void setIsReportChairman(String isReportChairman) {
		this.isReportChairman = isReportChairman;
	}
	
    /**
     * @return isReportChairman
     */
	public String getIsReportChairman() {
		return this.isReportChairman;
	}
	
	/**
	 * @param isUpperApprAuth
	 */
	public void setIsUpperApprAuth(String isUpperApprAuth) {
		this.isUpperApprAuth = isUpperApprAuth;
	}
	
    /**
     * @return isUpperApprAuth
     */
	public String getIsUpperApprAuth() {
		return this.isUpperApprAuth;
	}
	
	/**
	 * @param operFinaSitu
	 */
	public void setOperFinaSitu(String operFinaSitu) {
		this.operFinaSitu = operFinaSitu;
	}
	
    /**
     * @return operFinaSitu
     */
	public String getOperFinaSitu() {
		return this.operFinaSitu;
	}
	
	/**
	 * @param operFinaSituPicturePath
	 */
	public void setOperFinaSituPicturePath(String operFinaSituPicturePath) {
		this.operFinaSituPicturePath = operFinaSituPicturePath;
	}
	
    /**
     * @return operFinaSituPicturePath
     */
	public String getOperFinaSituPicturePath() {
		return this.operFinaSituPicturePath;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}