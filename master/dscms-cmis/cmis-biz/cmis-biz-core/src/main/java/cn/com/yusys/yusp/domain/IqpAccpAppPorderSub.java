/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAccpAppPorderSub
 * @类描述: iqp_accp_app_porder_sub数据实体类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-19 10:08:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_accp_app_porder_sub")
public class IqpAccpAppPorderSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 合同申请业务号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 票据号 **/
	@Column(name = "DRFT_NO", unique = false, nullable = true, length = 40)
	private String drftNo;
	
	/** 票面金额 **/
	@Column(name = "DRFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal drftAmt;
	
	/** 开始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;
	
	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 收款人名称 **/
	@Column(name = "PYEE_NAME", unique = false, nullable = true, length = 80)
	private String pyeeName;
	
	/** 收款人开户行行名 **/
	@Column(name = "PYEE_ACCTSVCR_NAME", unique = false, nullable = true, length = 80)
	private String pyeeAcctsvcrName;
	
	/** 收款人账号 **/
	@Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 80)
	private String pyeeAccno;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param drftNo
	 */
	public void setDrftNo(String drftNo) {
		this.drftNo = drftNo;
	}
	
    /**
     * @return drftNo
     */
	public String getDrftNo() {
		return this.drftNo;
	}
	
	/**
	 * @param drftAmt
	 */
	public void setDrftAmt(java.math.BigDecimal drftAmt) {
		this.drftAmt = drftAmt;
	}
	
    /**
     * @return drftAmt
     */
	public java.math.BigDecimal getDrftAmt() {
		return this.drftAmt;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param pyeeName
	 */
	public void setPyeeName(String pyeeName) {
		this.pyeeName = pyeeName;
	}
	
    /**
     * @return pyeeName
     */
	public String getPyeeName() {
		return this.pyeeName;
	}
	
	/**
	 * @param pyeeAcctsvcrName
	 */
	public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
		this.pyeeAcctsvcrName = pyeeAcctsvcrName;
	}
	
    /**
     * @return pyeeAcctsvcrName
     */
	public String getPyeeAcctsvcrName() {
		return this.pyeeAcctsvcrName;
	}
	
	/**
	 * @param pyeeAccno
	 */
	public void setPyeeAccno(String pyeeAccno) {
		this.pyeeAccno = pyeeAccno;
	}
	
    /**
     * @return pyeeAccno
     */
	public String getPyeeAccno() {
		return this.pyeeAccno;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}