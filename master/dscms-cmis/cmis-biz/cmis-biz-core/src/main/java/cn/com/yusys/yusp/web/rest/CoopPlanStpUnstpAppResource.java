/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanStpUnstpApp;
import cn.com.yusys.yusp.service.CoopPlanStpUnstpAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanStpUnstpAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: AbsonZ
 * @创建时间: 2021-04-17 09:18:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplanstpunstpapp")
public class CoopPlanStpUnstpAppResource {
    @Autowired
    private CoopPlanStpUnstpAppService coopPlanStpUnstpAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanStpUnstpApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanStpUnstpApp> list = coopPlanStpUnstpAppService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanStpUnstpApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanStpUnstpApp>> index(QueryModel queryModel) {
        List<CoopPlanStpUnstpApp> list = coopPlanStpUnstpAppService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanStpUnstpApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CoopPlanStpUnstpApp> show(@PathVariable("serno") String serno) {
        CoopPlanStpUnstpApp coopPlanStpUnstpApp = coopPlanStpUnstpAppService.selectByPrimaryKey(serno);
        return new ResultDto<CoopPlanStpUnstpApp>(coopPlanStpUnstpApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPlanStpUnstpApp> create(@RequestBody CoopPlanStpUnstpApp coopPlanStpUnstpApp) throws URISyntaxException {
        coopPlanStpUnstpAppService.insert(coopPlanStpUnstpApp);
        return new ResultDto<CoopPlanStpUnstpApp>(coopPlanStpUnstpApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPlanStpUnstpApp coopPlanStpUnstpApp) throws URISyntaxException {
        int result = coopPlanStpUnstpAppService.update(coopPlanStpUnstpApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = coopPlanStpUnstpAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanStpUnstpAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
