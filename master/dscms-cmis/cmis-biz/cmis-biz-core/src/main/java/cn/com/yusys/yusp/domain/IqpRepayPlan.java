/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayPlan
 * @类描述: iqp_repay_plan数据实体类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-30 09:06:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_repay_plan")
public class IqpRepayPlan extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 计划还款日期 **/
	@Column(name = "PLAN_REPAY_DATE", unique = false, nullable = true, length = 10)
	private String planRepayDate;
	
	/** 还款日期 **/
	@Column(name = "REPAY_DATE", unique = false, nullable = true, length = 10)
	private String repayDate;
	
	/** 还款金额 **/
	@Column(name = "REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmt;
	
	/** 计划应还款本金 **/
	@Column(name = "PLAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal planAmt;
	
	/** 期数 **/
	@Column(name = "TIMES", unique = false, nullable = true, length = 10)
	private Integer times;
	
	/** 放款流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="inputIdName")
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="inputBrIdName" )
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="updIdName")
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="updBrIdName" )
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改日期 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param planRepayDate
	 */
	public void setPlanRepayDate(String planRepayDate) {
		this.planRepayDate = planRepayDate;
	}
	
    /**
     * @return planRepayDate
     */
	public String getPlanRepayDate() {
		return this.planRepayDate;
	}
	
	/**
	 * @param repayDate
	 */
	public void setRepayDate(String repayDate) {
		this.repayDate = repayDate;
	}
	
    /**
     * @return repayDate
     */
	public String getRepayDate() {
		return this.repayDate;
	}
	
	/**
	 * @param repayAmt
	 */
	public void setRepayAmt(java.math.BigDecimal repayAmt) {
		this.repayAmt = repayAmt;
	}
	
    /**
     * @return repayAmt
     */
	public java.math.BigDecimal getRepayAmt() {
		return this.repayAmt;
	}
	
	/**
	 * @param planAmt
	 */
	public void setPlanAmt(java.math.BigDecimal planAmt) {
		this.planAmt = planAmt;
	}
	
    /**
     * @return planAmt
     */
	public java.math.BigDecimal getPlanAmt() {
		return this.planAmt;
	}
	
	/**
	 * @param times
	 */
	public void setTimes(Integer times) {
		this.times = times;
	}
	
    /**
     * @return times
     */
	public Integer getTimes() {
		return this.times;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}