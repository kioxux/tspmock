/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import cn.com.yusys.yusp.service.RiskXdGuarantyService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskXdGuarantyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-05 23:15:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/riskxdguaranty")
public class RiskXdGuarantyResource {
    @Autowired
    private RiskXdGuarantyService riskXdGuarantyService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RiskXdGuaranty>> query() {
        QueryModel queryModel = new QueryModel();
        List<RiskXdGuaranty> list = riskXdGuarantyService.selectAll(queryModel);
        return new ResultDto<List<RiskXdGuaranty>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RiskXdGuaranty>> index(QueryModel queryModel) {
        List<RiskXdGuaranty> list = riskXdGuarantyService.selectByModel(queryModel);
        return new ResultDto<List<RiskXdGuaranty>>(list);
    }

    /**
     * @函数名称:riskXdGuarantylist
     * @函数描述: 获取申请信息押品查询查封
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取申请信息押品查询查封")
    @PostMapping("/riskXdGuarantylist")
    protected ResultDto<List<RiskXdGuaranty>> riskXdGuarantylist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("apply_time desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<RiskXdGuaranty> list = riskXdGuarantyService.riskXdGuarantylist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<RiskXdGuaranty>>(list);
    }

    /**
     * @函数名称:riskXdGuarantyHislist
     * @函数描述: 获取申请历史押品查询查封
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取申请历史押品查询查封")
    @PostMapping("/riskXdGuarantyHislist")
    protected ResultDto<List<RiskXdGuaranty>> riskXdGuarantyHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("apply_time desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<RiskXdGuaranty> list = riskXdGuarantyService.riskXdGuarantyHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<RiskXdGuaranty>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过面签流水号查询企业详情
     * @创建者：zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过查询查封流水号查询查封详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        RiskXdGuaranty riskXdGuaranty = riskXdGuarantyService.selectBySerno((String) params.get("serno"));
        if (riskXdGuaranty != null) {
            resultDto.setCode(200);
            resultDto.setData(riskXdGuaranty);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RiskXdGuaranty> show(@PathVariable("serno") String serno) {
        RiskXdGuaranty riskXdGuaranty = riskXdGuarantyService.selectByPrimaryKey(serno);
        return new ResultDto<RiskXdGuaranty>(riskXdGuaranty);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RiskXdGuaranty> create(@RequestBody RiskXdGuaranty riskXdGuaranty) throws URISyntaxException {
        riskXdGuarantyService.insert(riskXdGuaranty);
        return new ResultDto<RiskXdGuaranty>(riskXdGuaranty);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RiskXdGuaranty riskXdGuaranty) throws URISyntaxException {
        riskXdGuaranty.setSubmitTime(stringRedisTemplate.opsForValue().get("openDay"));
        int result = riskXdGuarantyService.update(riskXdGuaranty);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateRiskXdGuaranty
     * @函数描述:押品查询查封将信息同步发送风控 调用fb1168抵押查封结果推送
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("押品查询查封将信息同步发送风控")
    @PostMapping("/updateRiskXdGuaranty")
    protected ResultDto<String> updateRiskXdGuaranty(@RequestBody RiskXdGuaranty riskXdGuaranty) {
        return  riskXdGuarantyService.updateRiskXdGuaranty(riskXdGuaranty);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = riskXdGuarantyService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = riskXdGuarantyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
