package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMortgageOther
 * @类描述: guar_inf_mortgage_other数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:17:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfMortgageOtherDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 用途 **/
	private String usr;
	
	/** 数量 **/
	private String qnt;
	
	/** 购入日期 **/
	private String purDate;
	
	/** 持有到期日 **/
	private String holdEndDate;
	
	/** 抵押质物说明 **/
	private String pldimnMemo;
	
	/** 备注 **/
	private String memo;
	
	/** 押品取得方式  STD_ZB_GUAR_GET_TYPE **/
	private String guarGetType;
	
	/** 计量单位 STD_GB_GUAR_UNIT **/
	private String guarUnit;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param usr
	 */
	public void setUsr(String usr) {
		this.usr = usr == null ? null : usr.trim();
	}
	
    /**
     * @return Usr
     */	
	public String getUsr() {
		return this.usr;
	}
	
	/**
	 * @param qnt
	 */
	public void setQnt(String qnt) {
		this.qnt = qnt == null ? null : qnt.trim();
	}
	
    /**
     * @return Qnt
     */	
	public String getQnt() {
		return this.qnt;
	}
	
	/**
	 * @param purDate
	 */
	public void setPurDate(String purDate) {
		this.purDate = purDate == null ? null : purDate.trim();
	}
	
    /**
     * @return PurDate
     */	
	public String getPurDate() {
		return this.purDate;
	}
	
	/**
	 * @param holdEndDate
	 */
	public void setHoldEndDate(String holdEndDate) {
		this.holdEndDate = holdEndDate == null ? null : holdEndDate.trim();
	}
	
    /**
     * @return HoldEndDate
     */	
	public String getHoldEndDate() {
		return this.holdEndDate;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo == null ? null : pldimnMemo.trim();
	}
	
    /**
     * @return PldimnMemo
     */	
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo == null ? null : memo.trim();
	}
	
    /**
     * @return Memo
     */	
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param guarGetType
	 */
	public void setGuarGetType(String guarGetType) {
		this.guarGetType = guarGetType == null ? null : guarGetType.trim();
	}
	
    /**
     * @return GuarGetType
     */	
	public String getGuarGetType() {
		return this.guarGetType;
	}
	
	/**
	 * @param guarUnit
	 */
	public void setGuarUnit(String guarUnit) {
		this.guarUnit = guarUnit == null ? null : guarUnit.trim();
	}
	
    /**
     * @return GuarUnit
     */	
	public String getGuarUnit() {
		return this.guarUnit;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}