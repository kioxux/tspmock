package cn.com.yusys.yusp.domain;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

/**
 * 
 * @项目名称:eval
 * @类名称:IndexMethodEvalRequestDTO
 * @类描述:指数法估值计算请求DTO
 * @功能描述:
 * @创建人:wangxx7@yusys.com.cn
 * @创建时间:2018年7月28日
 * @修改备注:
 * @修改日期		修改人员		修改原因
 * --------    --------		----------------------------------------
 * @version 1.0.0
 * @Copyright (c) 2018宇信科技-版权所有
 */
public class IndexMethodEvalRequestDTO {
    
    // 初估价值
    @NotNull
    private BigDecimal evalOutAmt;
    // 当前价格指数值 (元)
    @NotNull
    private BigDecimal nowEvaluatIndexValue;
    // 比较期价格指数值 (元)
    @NotNull
    private BigDecimal compareEvaluatIndexValue;
    // 房屋物理折损率(%)
    @NotNull
    private BigDecimal housingRate;
    // 当前评估日期
    @NotNull
    private String nowEvaluatDate;
    // 初估认定日期
    @NotNull
    private String evalOutDate;
    
    public BigDecimal getEvalOutAmt() {
        return evalOutAmt;
    }
    public void setEvalOutAmt(BigDecimal evalOutAmt) {
        this.evalOutAmt = evalOutAmt;
    }
    public BigDecimal getNowEvaluatIndexValue() {
        return nowEvaluatIndexValue;
    }
    public void setNowEvaluatIndexValue(BigDecimal nowEvaluatIndexValue) {
        this.nowEvaluatIndexValue = nowEvaluatIndexValue;
    }
    public BigDecimal getCompareEvaluatIndexValue() {
        return compareEvaluatIndexValue;
    }
    public void setCompareEvaluatIndexValue(BigDecimal compareEvaluatIndexValue) {
        this.compareEvaluatIndexValue = compareEvaluatIndexValue;
    }
    public BigDecimal getHousingRate() {
        return housingRate;
    }
    public void setHousingRate(BigDecimal housingRate) {
        this.housingRate = housingRate;
    }
    public String getNowEvaluatDate() {
        return nowEvaluatDate;
    }
    public void setNowEvaluatDate(String nowEvaluatDate) {
        this.nowEvaluatDate = nowEvaluatDate;
    }
    public String getEvalOutDate() {
        return evalOutDate;
    }
    public void setEvalOutDate(String evalOutDate) {
        this.evalOutDate = evalOutDate;
    }
}
