package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.IqpRepayInterestChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpRepayInterestChgMapper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;

@Service
public class IqpRepayInterestChgService {
    private static final Logger log = LoggerFactory.getLogger(IqpRepayInterestChgService.class);

    @Autowired
    private IqpRepayInterestChgMapper iqpRepayInterestChgMapper;
    
    @Autowired
    private  IqpRepayWayChgService  iqpRepayWayChgService;
    /**
     * 添加利息减免   变更申请  初始化审核状态为带发起   操作类型为新增
     * @param iqpRepayInterestChg
     * @return
     */
    public int save(IqpRepayInterestChg iqpRepayInterestChg) {
        iqpRepayInterestChg.setApproveStatus("000");
        iqpRepayInterestChg.setOprType("01");
        BigDecimal  reducTotalAmt=new BigDecimal(0);
        if (iqpRepayInterestChg.getReducNormalInt() !=null ){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getReducNormalInt());
        }else{
            iqpRepayInterestChg.setReducNormalInt(new BigDecimal(0));
        }
        if (iqpRepayInterestChg.getreducOverdueInt()!= null){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getreducOverdueInt());
        }else{
            iqpRepayInterestChg.setreducOverdueInt(new BigDecimal(0));
        }
        if (iqpRepayInterestChg.getReducCi()!=null){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getReducCi());
        }else{
            iqpRepayInterestChg.setReducCi(new BigDecimal(0));
        }
        iqpRepayInterestChg.setReducTotalAmt(reducTotalAmt);
        return  this.iqpRepayInterestChgMapper.insert(iqpRepayInterestChg);
    }

    public int checkIsExistChgBizByBillNo(IqpRepayInterestChg IqpRepayInterestChg) {
        return  0;//this.iqpRepayWayChgService.checkIsExistChgBizByBillNo(IqpRepayInterestChg.getIqpSerno(),IqpRepayInterestChg.getBillNo());
    }

    /**
     * 校验利息表当中该借据号的业务 是否有流程正在审批流程当中
     * @param iqpSerno
     * @param billNo
     * @return
     */
    public int checkIsExistIqpRepayInterestChgBizByBillNo(String iqpSerno, String billNo) {
        try {
            HashMap<String,String> param = new HashMap<String,String >();
            log.info("校验是否存在在途的利息减免变更申请【{}】", JSONObject.toJSON(iqpSerno));
            // 获取还款日变更信息
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew = iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            // 判断是否需要排除当前流水号对应的  还款日变更信息
            if(iqpRepayInterestChgNew != null ){
                param.put("iqp_serno",iqpSerno);
            }
            param.put("wfExistsFlag", CmisFlowConstants.REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME);
            // 放入需要的操作类型
            param.put("opr_type", CmisCommonConstants.OPR_TYPE_ADD);
            // 放入借据号
            param.put("bill_no",billNo);
            return this.iqpRepayInterestChgMapper.checkIsExistChgBizByBillNo(param);
        } catch (YuspException e) {
            log.error("利息减免变更申请新增失败！", e);
            return -1;
        }
    }

    /**
     * 根据流水号修改状态为审批中
     * @param iqpSerno
     */
    public void handleBusinessDataAfterStart(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew= this.iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            if(iqpRepayInterestChgNew==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【111】-审批中");
            int result= this.iqpRepayInterestChgMapper.updateByPrimaryKey(iqpRepayInterestChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 根据流水号修改状态为已通过
     * @param iqpSerno
     */
    public void handleBusinessDataAfterEnd(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew= this.iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            if(iqpRepayInterestChgNew==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【997】-已通过");
            /**修改审批状态为已通过,并修改台账表的数据 维护最后修改人信息**/
            //TODO 保留接口  更新核算系统数据
            int result= this.iqpRepayInterestChgMapper.updateYtgByPrimaryKey(iqpRepayInterestChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 根据流水号修改状态为'打回'
     * @param iqpSerno
     */
    public void handleBusinessDataAfteCallBack(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew= this.iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            if(iqpRepayInterestChgNew==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【992】-打回");
            int result= this.iqpRepayInterestChgMapper.handleBusinessDataAfteCallBack(iqpRepayInterestChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 根据流水号修改状态为'追回'
     * @param iqpSerno
     */
    public void handleBusinessDataAfteTackBack(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew= this.iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            if(iqpRepayInterestChgNew==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【991】-追回");
            int result= this.iqpRepayInterestChgMapper.handleBusinessDataAfteTackBack(iqpRepayInterestChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 根据流水号修改状态为'不通过'
     * @param iqpSerno
     */
    public void handleBusinessDataAfterRefuse(String iqpSerno) {
        try{
            if(StringUtils.isBlank(iqpSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取业务申请"+iqpSerno+"申请主表信息");
            IqpRepayInterestChg iqpRepayInterestChg=new IqpRepayInterestChg();
            iqpRepayInterestChg.setIqpSerno(iqpSerno);
            IqpRepayInterestChg iqpRepayInterestChgNew= this.iqpRepayInterestChgMapper.selectByPrimaryKey(iqpRepayInterestChg);
            if(iqpRepayInterestChgNew==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+iqpSerno+"流程审批状态为【998】-审批不通过");
            int result= this.iqpRepayInterestChgMapper.handleBusinessDataAfterRefuse(iqpRepayInterestChg);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }


    /**
     * 主页面点击修改  点击保存按钮保存数据
     * @param iqpRepayInterestChg
     * @return
     */
    public int updateCommit(IqpRepayInterestChg iqpRepayInterestChg) {
        BigDecimal  reducTotalAmt=new BigDecimal(0);
        if (iqpRepayInterestChg.getReducNormalInt() !=null ){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getReducNormalInt());
        }else{
            iqpRepayInterestChg.setReducNormalInt(new BigDecimal(0));
        }
        if (iqpRepayInterestChg.getreducOverdueInt()!= null){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getreducOverdueInt());
        }else{
            iqpRepayInterestChg.setreducOverdueInt(new BigDecimal(0));
        }
        if (iqpRepayInterestChg.getReducCi()!=null){
            reducTotalAmt=reducTotalAmt.add(iqpRepayInterestChg.getReducCi());
        }else{
            iqpRepayInterestChg.setReducCi(new BigDecimal(0));
        }
        iqpRepayInterestChg.setReducTotalAmt(reducTotalAmt);
        return   this.iqpRepayInterestChgMapper.updateCommitByPrimaryKey(iqpRepayInterestChg);
    }
}
