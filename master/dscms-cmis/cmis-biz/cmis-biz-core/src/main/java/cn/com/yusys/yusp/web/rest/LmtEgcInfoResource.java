/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtCobInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtEgcInfo;
import cn.com.yusys.yusp.service.LmtEgcInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtEgcInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: sl
 * @创建时间: 2021-04-25 19:49:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "紧急联系人信息")
@RequestMapping("/api/lmtegcinfo")
public class LmtEgcInfoResource {
    @Autowired
    private LmtEgcInfoService lmtEgcInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtEgcInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtEgcInfo> list = lmtEgcInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtEgcInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtEgcInfo>> index(QueryModel queryModel) {
        List<LmtEgcInfo> list = lmtEgcInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtEgcInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{pkId}")
    protected ResultDto<LmtEgcInfo> show(@PathVariable("pkId") String pkId) {
        LmtEgcInfo lmtEgcInfo = lmtEgcInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtEgcInfo>(lmtEgcInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtEgcInfo> create(@RequestBody LmtEgcInfo lmtEgcInfo) throws URISyntaxException {
        lmtEgcInfoService.insert(lmtEgcInfo);
        return new ResultDto<LmtEgcInfo>(lmtEgcInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtEgcInfo lmtEgcInfo) throws URISyntaxException {
        int result = lmtEgcInfoService.update(lmtEgcInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtEgcInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtEgcInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/18 19:51
     * @注释 列表条件查询 POST
     */
    @ApiOperation("查询对象列表，POST请求")
    @PostMapping("/selectbymodel")
    protected ResultDto<List> selectbymodel(@RequestBody QueryModel queryModel) {
        List list = lmtEgcInfoService.selectByModel(queryModel);
        return new ResultDto<List>(list);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/deletebypkid")
    protected ResultDto<Integer> deletebypkid(@RequestBody LmtEgcInfo lmtEgcInfo) {
        int result = lmtEgcInfoService.deleteByPrimaryKey(lmtEgcInfo.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/30 15:04
     * @注释 由于移动端要求 新增接口变更
     */
    @ApiOperation("新增接口")
    @PostMapping("/addegc")
    protected ResultDto<LmtEgcInfo> addegc(@RequestBody LmtEgcInfo lmtEgcInfo) throws URISyntaxException {
        lmtEgcInfoService.insert(lmtEgcInfo);
        return new ResultDto<LmtEgcInfo>(lmtEgcInfo);
    }
}
