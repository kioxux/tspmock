package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditInfo
 * @类描述: iqp_cus_credit_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-24 15:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCusCreditInfoRetailDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	private String IqpSerno;

	/** 与借款人关系 **/
	private String debitRela;

	/** 证件号码 **/
	private String certCode;

	/** 姓名 **/
	private String cusName;

	public String getIqpSerno() {
		return IqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		IqpSerno = iqpSerno;
	}

	public String getDebitRela() {
		return debitRela;
	}

	public void setDebitRela(String debitRela) {
		this.debitRela = debitRela;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	@Override
	public String toString() {
		return "IqpCusCreditInfoRetailDto{" +
				"IqpSerno='" + IqpSerno + '\'' +
				", debitRela='" + debitRela + '\'' +
				", certCode='" + certCode + '\'' +
				", cusName='" + cusName + '\'' +
				'}';
	}

}