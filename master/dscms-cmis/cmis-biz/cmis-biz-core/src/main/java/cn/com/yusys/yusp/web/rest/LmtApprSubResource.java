/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtApprSub;
import cn.com.yusys.yusp.service.LmtApprSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:35:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtapprsub")
public class LmtApprSubResource {
    @Autowired
    private LmtApprSubService lmtApprSubService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtApprSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtApprSub> list = lmtApprSubService.selectAll(queryModel);
        return new ResultDto<List<LmtApprSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtApprSub>> index(QueryModel queryModel) {
        List<LmtApprSub> list = lmtApprSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtApprSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtApprSub> show(@PathVariable("pkId") String pkId) {
        LmtApprSub lmtApprSub = lmtApprSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtApprSub>(lmtApprSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtApprSub> create(@RequestBody LmtApprSub lmtApprSub) throws URISyntaxException {
        lmtApprSubService.insert(lmtApprSub);
        return new ResultDto<LmtApprSub>(lmtApprSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtApprSub lmtApprSub) throws URISyntaxException {
        int result = lmtApprSubService.update(lmtApprSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtApprSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtApprSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getBySubSerno
     * @函数描述:根据分项流水号得到该条分项信息
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @PostMapping("/getbysubserno")
    protected ResultDto<LmtApprSub> getBySubSerno(@RequestBody Map map) {
        String subSerno = (String) map.get("subSerno");
        LmtApprSub lmtApprSub = lmtApprSubService.selectBySubSerno(subSerno.replaceAll("\"", ""));
        return new ResultDto<LmtApprSub>(lmtApprSub);
    }

    /**
     * @函数名称:updateLmtAppSub
     * @函数描述:保存授信分项修改数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatelmtapprsub")
    protected ResultDto<Map> updateLmtApprSub(@RequestBody LmtApprSub lmtApprSub) {
        Map map = lmtApprSubService.updateLmtApprSub(lmtApprSub);
        return ResultDto.success(map);
    }
}
