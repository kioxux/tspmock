/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLimitApp;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLmtRst;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLmtRstMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtRstService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:23:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicLmtRstService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicLmtRstMapper lmtSigInvestBasicLmtRstMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtRst selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtRstMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicLmtRst> selectAll(QueryModel model) {
        List<LmtSigInvestBasicLmtRst> records = (List<LmtSigInvestBasicLmtRst>) lmtSigInvestBasicLmtRstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicLmtRst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicLmtRst> list = lmtSigInvestBasicLmtRstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据批复流水号查询底层授信批复信息
     * @param replySerno
     * @return
     */
    public List<LmtSigInvestBasicLmtRst> selectByReplySerno(String replySerno){
        return lmtSigInvestBasicLmtRstMapper.selectByReplySerno(replySerno);
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicLmtRst record) {
        return lmtSigInvestBasicLmtRstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicLmtRst record) {
        return lmtSigInvestBasicLmtRstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicLmtRst record) {
        return lmtSigInvestBasicLmtRstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicLmtRst record) {
        return lmtSigInvestBasicLmtRstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicLmtRstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicLmtRstMapper.deleteByIds(ids);
    }




    /**
     * @方法名称: initLmtSigInvestBasicLmtRstInfo
     * @方法描述: 底层授信额度批复表初始化
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicLmtRst initLmtSigInvestBasicLmtRstInfo(Object lmtSigInvestBasicLmtAppr) {
        //主键
        String pkValue = generatePkId();
        //初始化对象
        LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst = new LmtSigInvestBasicLmtRst() ;
        //拷贝对象
        BeanUtils.copyProperties(lmtSigInvestBasicLmtAppr, lmtSigInvestBasicLmtRst);
        //生成主键
        lmtSigInvestBasicLmtRst.setPkId(pkValue);
        //批复底层流水号
        //lmtSigInvestBasicLmtRst.setBasicReplySerno(basicReplySerno);
        //终审机构
        lmtSigInvestBasicLmtRst.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //批复状态
        lmtSigInvestBasicLmtRst.setReplyStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        //批复生效日期
        lmtSigInvestBasicLmtRst.setReplyInureDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
        //最新更新日期
        lmtSigInvestBasicLmtRst.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicLmtRst.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicLmtRst.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestBasicLmtRst;
    }

    /**
     * 批复变更-生成批复信息
     * @param lmtSigInvestBasicLimitApp
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     */
    public void insertBasicLmtRst(LmtSigInvestBasicLimitApp lmtSigInvestBasicLimitApp, String oldSerno, String replySerno, String currentOrgId, String currentUserId) {
        LmtSigInvestBasicLmtRst lmtSigInvestBasicLmtRst = new LmtSigInvestBasicLmtRst();
        copyProperties(lmtSigInvestBasicLimitApp,lmtSigInvestBasicLmtRst);

        lmtSigInvestBasicLmtRst.setSerno(oldSerno);
        lmtSigInvestBasicLmtRst.setReplySerno(replySerno);
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtSigInvestBasicLmtRst,currentUserId,currentOrgId);
        insert(lmtSigInvestBasicLmtRst);
    }
}
