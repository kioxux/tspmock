package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpStockLoanInfo;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.IqpStockLoanInfoDto;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.IqpStockLoanInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpStockLoanInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-05-08 14:39:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpStockLoanInfoService {

    @Autowired
    private IqpStockLoanInfoMapper iqpStockLoanInfoMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpStockLoanInfo selectByPrimaryKey(String pkId) {
        return iqpStockLoanInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<IqpStockLoanInfo> selectAll(QueryModel model) {
        List<IqpStockLoanInfo> records = (List<IqpStockLoanInfo>) iqpStockLoanInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<IqpStockLoanInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpStockLoanInfo> list = iqpStockLoanInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(IqpStockLoanInfo record) {
        return iqpStockLoanInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(IqpStockLoanInfo record) {
        return iqpStockLoanInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(IqpStockLoanInfo record) {
        return iqpStockLoanInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(IqpStockLoanInfo record) {
        return iqpStockLoanInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return iqpStockLoanInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return iqpStockLoanInfoMapper.deleteByIds(ids);
    }

    /***
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/5/8 14:52
     * @version 1.0.0
     * @desc 通过业务流水号查询存量业务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public List<IqpStockLoanInfo> selectByIqpSerno(QueryModel queryModel) {
        //查询存量业务表中是否存在信息
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpStockLoanInfo> list = iqpStockLoanInfoMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        String iqpSerno = (String) queryModel.getCondition().get("iqpSerno");
        if (list == null || list.size() == 0) {
            //如果不存在,先查询客户编号和客户姓名
            List<IqpStockLoanInfo> listInfo = null;
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            String cusId = iqpLoanApp.getCusId();
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);
            model.addCondition("oprType","01");
            model.addCondition("belgLine","02");
            List<LmtCrdReplyInfo> LmtCrdReplyInfoList = lmtCrdReplyInfoService.selectByModel(model);

            for(int i = 0; i < LmtCrdReplyInfoList.size(); i++){

                LmtCrdReplyInfo lmtCrdReplyInfo = LmtCrdReplyInfoList.get(i);
                IqpStockLoanInfo iqpStockLoanInfo = new IqpStockLoanInfo();
                iqpStockLoanInfo.setIqpSerno(iqpSerno);
                iqpStockLoanInfo.setPrdId(lmtCrdReplyInfo.getPrdId());
                iqpStockLoanInfo.setPrdName(lmtCrdReplyInfo.getPrdName());
                iqpStockLoanInfo.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());
                iqpStockLoanInfo.setGuarMode(lmtCrdReplyInfo.getGuarMode());
                //根据业务流水查询贷款余额
                BigDecimal usedAmount = accLoanMapper.queryAccLoanLoanBalanceSum(iqpSerno);
                if(usedAmount == null){
                    iqpStockLoanInfo.setBalance(new BigDecimal("0"));
                }else{
                    iqpStockLoanInfo.setBalance(usedAmount);
                }
                iqpStockLoanInfo.setCreateTime(DateUtils.getCurrDate());
                iqpStockLoanInfo.setUpdateTime(DateUtils.getCurrDate());
                insertSelective(iqpStockLoanInfo);
            }
            return iqpStockLoanInfoMapper.selectByModel(queryModel);
        } else {
            return list;
        }


    }

    /**
     * @author shenli
     * @date 2021-6-17
     * @version 1.0.0
     * @desc 根据流水统计存量授信额度合计
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpStockLoanInfoDto selectLmtAmtTotal(IqpStockLoanInfo iqpStockLoanInfo) {
        IqpStockLoanInfoDto iqpStockLoanInfoDto = iqpStockLoanInfoMapper.selecTotalByIqpSerno(iqpStockLoanInfo.getIqpSerno());
        return iqpStockLoanInfoDto;
    }

}
