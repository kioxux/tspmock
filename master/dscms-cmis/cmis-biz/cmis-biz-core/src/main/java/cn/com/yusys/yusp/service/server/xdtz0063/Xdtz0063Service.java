package cn.com.yusys.yusp.service.server.xdtz0063;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.ToppAccPayDetail;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.req.Xdtz0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.resp.Xdtz0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.ToppAccPayDetailService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务逻辑类:他行退汇冻结编号更新
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0063Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0063Service.class);

    @Autowired
    private ToppAccPayDetailService toppAccPayDetailService;

    /**
     * 交易码：xdtz0063
     * 交易描述：他行退汇冻结编号更新
     *
     * @param xdtz0063DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0063DataRespDto xdtz0063(Xdtz0063DataReqDto xdtz0063DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataReqDto));
        Xdtz0063DataRespDto xdtz0063DataRespDto = new Xdtz0063DataRespDto();
        try {
            logger.info("***********他行退汇冻结编号更新**************查询参数:" + xdtz0063DataReqDto.toString());
            // 业务受理编号
            String busNo = xdtz0063DataReqDto.getHstrsq();
            // 新冻结编号
            String freezeNo = xdtz0063DataReqDto.getXdjbh();
            // 请求数据不能为空
            if(StringUtils.isBlank(busNo) || StringUtils.isBlank(freezeNo)){
                logger.info("***********他行退汇冻结编号更新**************必输校验为空");
                throw new Exception(EpbEnum.EPB090009.value);
            }
            logger.info("***********他行退汇冻结编号更新**************根据业务受理编号【"+ busNo +"】查询受托支付登记簿数据");
            ToppAccPayDetail toppAccPayDetail = toppAccPayDetailService.selecctByBusNo(busNo);
            if(toppAccPayDetail == null || StringUtils.isBlank(toppAccPayDetail.getSerno())){
                throw new Exception(EcbEnum.ECB020070.value);
            }
            logger.info("***********他行退汇冻结编号更新**************根据业务受理编号【"+ busNo +"】查询受托支付登记簿数据");
            toppAccPayDetail.setFreezeNo(freezeNo);
            logger.info("***********他行退汇冻结编号更新**************根据业务受理编号【"+ busNo +"】更新受托支付登记簿数据");
            toppAccPayDetailService.update(toppAccPayDetail);
            xdtz0063DataRespDto.setOpFlag("S");
            xdtz0063DataRespDto.setOpMsg("更新冻结编号成功");
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataRespDto));
        return xdtz0063DataRespDto;
    }
}
