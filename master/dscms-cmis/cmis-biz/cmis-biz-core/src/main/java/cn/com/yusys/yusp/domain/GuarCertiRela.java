/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-guar模块
 * @类名称: GuarCertiRela
 * @类描述: guar_certi_rela数据实体类
 * @功能描述:
 * @创建人: www58
 * @创建时间: 2021-01-22 11:57:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_certi_rela")
public class GuarCertiRela extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;

	/** 权证类型 STD_ZB_CERTI_TYPE_CD **/
	@Column(name = "CERTI_TYPE_CD", unique = false, nullable = false, length = 10)
	private String certiTypeCd;

	/** 权利凭证号 **/
	@Column(name = "CERTI_RECORD_ID", unique = false, nullable = false, length = 40)
	private String certiRecordId;

	/** 权证类别 STD_ZB_CERTI_CATALOG  **/
	@Column(name = "CERTI_CATALOG", unique = false, nullable = true, length = 10)
	private String certiCatalog;

	/** 权证发证机关名称 STD_ZB_ **/
	@Column(name = "CERTI_ORG_NAME", unique = false, nullable = true, length = 100)
	private String certiOrgName;

	/** 权证发证日期 **/
	@Column(name = "CERTI_START_DATE", unique = false, nullable = true, length = 10)
	private String certiStartDate;

	/** 权证到期日期 **/
	@Column(name = "CERTI_END_DATE", unique = false, nullable = true, length = 10)
	private String certiEndDate;

	/** 权证入库日期 **/
	@Column(name = "IN_DATE", unique = false, nullable = true, length = 10)
	private String inDate;

	/** 权证正常出库日期 **/
	@Column(name = "OUT_DATE", unique = false, nullable = true, length = 10)
	private String outDate;

	/** 权证临时借用人名称 **/
	@Column(name = "TEMP_BORROWER_NAME", unique = false, nullable = true, length = 100)
	private String tempBorrowerName;

	/** 权证临时出库日期 **/
	@Column(name = "TEMP_OUT_DATE", unique = false, nullable = true, length = 10)
	private String tempOutDate;

	/** 权证预计归还时间 **/
	@Column(name = "PRE_BACK_DATE", unique = false, nullable = true, length = 10)
	private String preBackDate;

	/** 权证实际归还日期 **/
	@Column(name = "REAL_BACK_DATE", unique = false, nullable = true, length = 10)
	private String realBackDate;

	/** 权证临时出库原因 **/
	@Column(name = "TEMP_OUT_REASON", unique = false, nullable = true, length = 10)
	private String tempOutReason;

	/** 权证状态 STD_ZB_CERTI_STATE **/
	@Column(name = "CERTI_STATE", unique = false, nullable = true, length = 10)
	private String certiState;

	/** 保管机构 **/
	@Column(name = "DEPOSIT_ORG_NO", unique = false, nullable = true, length = 100)
	private String depositOrgNo;

	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;

	/** 权证备注信息 **/
	@Column(name = "CERTI_COMMENT", unique = false, nullable = true, length = 1000)
	private String certiComment;

	/** 权证续期原因 **/
	@Column(name = "EXT_REASON", unique = false, nullable = true, length = 4000)
	private String extReason;

	/** 权利金额 **/
	@Column(name = "CERTI_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal certiAmt;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 100)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 40)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 100)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 40)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 操作类型  **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	/**
	 * @return guarNo
	 */
	public String getGuarNo() {
		return this.guarNo;
	}

	/**
	 * @param certiTypeCd
	 */
	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd;
	}

	/**
	 * @return certiTypeCd
	 */
	public String getCertiTypeCd() {
		return this.certiTypeCd;
	}

	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId;
	}

	/**
	 * @return certiRecordId
	 */
	public String getCertiRecordId() {
		return this.certiRecordId;
	}

	/**
	 * @param certiCatalog
	 */
	public void setCertiCatalog(String certiCatalog) {
		this.certiCatalog = certiCatalog;
	}

	/**
	 * @return certiCatalog
	 */
	public String getCertiCatalog() {
		return this.certiCatalog;
	}

	/**
	 * @param certiOrgName
	 */
	public void setCertiOrgName(String certiOrgName) {
		this.certiOrgName = certiOrgName;
	}

	/**
	 * @return certiOrgName
	 */
	public String getCertiOrgName() {
		return this.certiOrgName;
	}

	/**
	 * @param certiStartDate
	 */
	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate;
	}

	/**
	 * @return certiStartDate
	 */
	public String getCertiStartDate() {
		return this.certiStartDate;
	}

	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate;
	}

	/**
	 * @return certiEndDate
	 */
	public String getCertiEndDate() {
		return this.certiEndDate;
	}

	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}

	/**
	 * @return inDate
	 */
	public String getInDate() {
		return this.inDate;
	}

	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}

	/**
	 * @return outDate
	 */
	public String getOutDate() {
		return this.outDate;
	}

	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName;
	}

	/**
	 * @return tempBorrowerName
	 */
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}

	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate;
	}

	/**
	 * @return tempOutDate
	 */
	public String getTempOutDate() {
		return this.tempOutDate;
	}

	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate;
	}

	/**
	 * @return preBackDate
	 */
	public String getPreBackDate() {
		return this.preBackDate;
	}

	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate;
	}

	/**
	 * @return realBackDate
	 */
	public String getRealBackDate() {
		return this.realBackDate;
	}

	/**
	 * @param tempOutReason
	 */
	public void setTempOutReason(String tempOutReason) {
		this.tempOutReason = tempOutReason;
	}

	/**
	 * @return tempOutReason
	 */
	public String getTempOutReason() {
		return this.tempOutReason;
	}

	/**
	 * @param certiState
	 */
	public void setCertiState(String certiState) {
		this.certiState = certiState;
	}

	/**
	 * @return certiState
	 */
	public String getCertiState() {
		return this.certiState;
	}

	/**
	 * @param depositOrgNo
	 */
	public void setDepositOrgNo(String depositOrgNo) {
		this.depositOrgNo = depositOrgNo;
	}

	/**
	 * @return depositOrgNo
	 */
	public String getDepositOrgNo() {
		return this.depositOrgNo;
	}

	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	/**
	 * @return finaBrId
	 */
	public String getFinaBrId() {
		return this.finaBrId;
	}

	/**
	 * @param certiComment
	 */
	public void setCertiComment(String certiComment) {
		this.certiComment = certiComment;
	}

	/**
	 * @return certiComment
	 */
	public String getCertiComment() {
		return this.certiComment;
	}

	/**
	 * @param extReason
	 */
	public void setExtReason(String extReason) {
		this.extReason = extReason;
	}

	/**
	 * @return extReason
	 */
	public String getExtReason() {
		return this.extReason;
	}

	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}

	/**
	 * @return certiAmt
	 */
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}
}