package cn.com.yusys.yusp.service.server.xdcz0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.CfgWyClassRelClientDto;
import cn.com.yusys.yusp.dto.GetIsXwUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.req.CmisCus0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.resp.CmisCus0015RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0026.req.CmisCus0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0026.resp.CmisCus0026RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.req.Xdcz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.resp.Xdcz0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.OptList;
import cn.com.yusys.yusp.server.xdxt0013.resp.Xdxt0013DataRespDto;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0021Service
 * @类描述: #服务类
 * @功能描述:支用申请
 * @创建人:
 * @创建时间: 2021-06-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0021Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdcz0021.Xdcz0021Service.class);

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;
    @Autowired
    private Dscms2XwhClientService dscms2XwhClientService;
    @Autowired
    private DscmsCfgXtClientService dscmsCfgXtClientService;
    @Autowired
    private DscmsCusClientService dscmsCusClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;
    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;
    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private AccEntrustLoanMapper accEntrustLoanMapper;
    @Autowired
    private CtrEntrustLoanContMapper ctrEntrustLoanContMapper;
    @Autowired
    private PvpEntrustLoanAppMapper pvpEntrustLoanAppMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private PvpBillContOnlineRelMapper pvpBillContOnlineRelMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private PvpJxhjRepayLoanService pvpJxhjRepayLoanService;
    @Autowired
    private PvpLoanAppRepayBillRellService pvpLoanAppRepayBillRellService;
    @Resource
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private DiscCouponBusinRelMapper discCouponBusinRelMapper;
    @Autowired
    private XdPrdClassRelMapper xdPrdClassRelMapper;
    @Autowired
    private XdIndustryFarmDireRelMapper xdIndustryFarmDireRelMapper;
    @Autowired
    private XdPrdIdentRelMapper xdPrdIdentRelMapper;
    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;//合同申请信息表


    /**
     * 支用申请
     *
     * @param xdcz0021DataReqDto
     * @return
     * @desc
     */
    public Xdcz0021DataRespDto xdcz0021(Xdcz0021DataReqDto xdcz0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value);
        Xdcz0021DataRespDto xdcz0021DataRespDto = new Xdcz0021DataRespDto();
        // 默认成功
        String msg = "success";
        // 业务类型
        String prdId = "";
        // 小贷客户调查中的业务细分
        String prdIdXd = "";
        // 小贷客户调查中的业务细分
        String prdNameXd = "";
        // 用途对应客户表里面存的值
        String prdIdRel = "";
        String telnum = "";//客户经理号码
        String repaymentAccount = null;
        String managerBrId = null;
        // 营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        BigDecimal lmt = null;
        String cusName = null;
        // 逾期罚息利率
        BigDecimal overDueRate = BigDecimal.ZERO;
        // 复利利率
        BigDecimal fuliReate = BigDecimal.ZERO;
        String FinaBrId = null;
        String loanBrId = "";
        String LoanTermQj = null;
        String RepayFlag = null;
        String managerId = null;
        String contCnNo = null;
        String contStatus = null;
        // 担保方式
        String guarWay = null;
        // 贷款投向
        String loanTer = null;
        String direction_name = null;
        String lmtAccNo = null;
        String comCusId = "";
        String billNo = "";
        // 单位所属行业
        String indivComFld = "";
        String cusType = "";
        String loanTypeDetail = "";//经营其他
        String isJyxwydk = "";
        String isGmhy = "";
        String isBxghy = "";
        String isFptxdk = "";
        String isBzxajgc = "";
        String isXgsyryxedb = "";
        String loanUseType2 = "";//流动资金贷款
        // 合同到期日
        String contEndDate = "";
        BigDecimal execRateYear = null;
        //涉农投向
        String farmDirection = null;
        String agriflg = null;
        BigDecimal rulingIr = null;
        BigDecimal rulingIrM = null;
        BigDecimal irFloatRate = null;
        // 合同金额
        BigDecimal contAmt = null;
        String certCode = "";
        // 是否直接出账
        boolean zjczFlag = true;
        String pvpSerno = null;
        String surveySerno = null;
        CtrLoanCont ctrLoanCont = null;
        String cusId = null;
        String prdName = null;
        AdminSmUserDto adminSmUserDto = null;
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;
        LmtCrdReplyInfo lmtCrdReplyInfo = null;

        // 出账金额
        BigDecimal pvpAmt = xdcz0021DataReqDto.getPvpAmt();
        // 借据起始日期
        String billStartDate = xdcz0021DataReqDto.getBillStartDate();
        // 借据期限
        String billTerm = xdcz0021DataReqDto.getBillTerm();
        // 借据到期日
        String billEndDate = xdcz0021DataReqDto.getBillEndDate();
        // 还款方式
        String repayType = xdcz0021DataReqDto.getRepayType();
        // 借款人账号
        repaymentAccount = xdcz0021DataReqDto.getDebitAcctNo();
        // 贷款用途
        String loanUseType = xdcz0021DataReqDto.getLoanUseType();
        // 推荐客户经理号
        String recomManagerId = xdcz0021DataReqDto.getRecomManagerId();
        // 证件号
        certCode = xdcz0021DataReqDto.getCertNo();
        // 借款合同号
        String loanContNo = xdcz0021DataReqDto.getLoanContNo();
        // 贷款来源
        String loanSour = xdcz0021DataReqDto.getLoanSour();
        // 是否免息
        String isIntFree = xdcz0021DataReqDto.getIsIntFree();
        // 免息天数
        String intFreeDay = xdcz0021DataReqDto.getIntFreeDay();
        // 优惠券编号
        String preferCode = xdcz0021DataReqDto.getPreferCode();
        // 无还本借据金额
        String billAmount = xdcz0021DataReqDto.getBillAmount();
        // 无还本原借据
        String oldBillNo = xdcz0021DataReqDto.getOldBillNo();

        try {
            // 数据校验
            if (null == pvpAmt) {
                msg = "出账金额为空!";
            } else if (!StringUtils.hasLength(billTerm)) {
                msg = "借据期限为空!";
            } else if (!StringUtils.hasLength(repayType)) {
                msg = "还款方式为空!";
            } else if (!StringUtils.hasLength(repaymentAccount)) {
                msg = "放款及还款账号为空!";
            } else if (!StringUtils.hasLength(loanUseType)) {
                msg = "贷款用途为空!";
            } else if (!StringUtils.hasLength(loanContNo)) {
                msg = "借款合同号为空!";
            }
            // 必填字段检验完毕
            if ("success".equals(msg)) {
                // 查询合同详情
                ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(loanContNo);

                if (Objects.isNull(ctrLoanCont)) {
                    logger.info("根据合同号【{}】未查询合同信息！", loanContNo);
                    throw BizException.error(null, EpbEnum.EPB090004.value(), "未查询到合同信息！");
                }
                // 客户编号
                cusId = ctrLoanCont.getCusId();
                // 合同状态
                contStatus = ctrLoanCont.getContStatus();
                // 存在可用额度
                if (StringUtils.hasLength(cusId) && "200".equals(contStatus)) {
                    xdkh0001DataRespDto = queryXdkh0001DataRespDto(cusId);
                    // 合同金额
                    contAmt = ctrLoanCont.getContAmt();
                    // 获取合同可用额度
                    lmt = contAmt.subtract(accLoanMapper.selectSumLoanBalAmtByParams(loanContNo, "'1', '2', '5'".split(",")));
                    // 业务类型
                    prdId = ctrLoanCont.getPrdId();
                    prdName = ctrLoanCont.getPrdName();
                    execRateYear = ctrLoanCont.getExecRateYear();
                    managerId = ctrLoanCont.getManagerId();
                    managerBrId = ctrLoanCont.getManagerBrId();
                    guarWay = ctrLoanCont.getGuarWay();
                    loanTer = ctrLoanCont.getLoanTer();
                    contEndDate = ctrLoanCont.getContEndDate();
                    rulingIr = ctrLoanCont.getRulingIr();
                    irFloatRate = ctrLoanCont.getIrFloatRate();
                    lmtAccNo = ctrLoanCont.getLmtAccNo();
                    surveySerno = ctrLoanCont.getSurveySerno();
                    agriflg = xdkh0001DataRespDto.getIsAgri();
                    indivComFld = xdkh0001DataRespDto.getIndivComTrade();
                    cusType = xdkh0001DataRespDto.getCusType();
                    cusName = xdkh0001DataRespDto.getCusName();
                    lmtCrdReplyInfo = Optional.ofNullable(lmtCrdReplyInfoMapper.selectBySurveySerno(surveySerno)).orElse(new LmtCrdReplyInfo());
                    prdIdXd = lmtCrdReplyInfo.getPrdId();
                    prdNameXd = lmtCrdReplyInfo.getPrdName();
                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
                    ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
                    logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
                    adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());
                    telnum = adminSmUserDto.getUserMobilephone();
                    if (BigDecimal.ZERO.compareTo(overDueRate) == 0) {
                        overDueRate = execRateYear.multiply(BigDecimal.valueOf(1.5));
                    }
                    if (BigDecimal.ZERO.compareTo(fuliReate) == 0) {
                        fuliReate = execRateYear.multiply(BigDecimal.valueOf(1.5));
                    }
                    // 本来想直接从合同表里面取fina_br_id但是从存量的白领贷来看好像也不准

                    // 判断是否是小微客户经理
                    String isXWUser = "";
                    if (StringUtil.isNotEmpty(managerId)) {//客户经理不能为空
                        logger.info("*************第一步根据客户经理编号【{}】查询客户经理信息开始", managerId);
                        ResultDto<GetIsXwUserDto> getIsXwUserDtoResultDto = adminSmUserService.getIsXWUserByLoginCode(managerId);
                        logger.info("*************第一步根据客户经理编号【{}】查询客户经理信息结束,客户信息【{}】", managerId, JSON.toJSONString(getIsXwUserDtoResultDto));
                        if (ResultDto.success().getCode().equals(getIsXwUserDtoResultDto.getCode())) {
                            GetIsXwUserDto getIsXwUserDto = getIsXwUserDtoResultDto.getData();
                            if (Objects.nonNull(getIsXwUserDto)) {
                                isXWUser = getIsXwUserDto.getIsXWUser();
                            }
                        }
                    }
                    if (StringUtil.isNotEmpty(isXWUser) && "Y".equals(isXWUser)) {
                        // 工薪贷都为小贷业务，本地小贷都记在016001 异地的为归属地最后一位改成6
                        if ("022034".equals(prdId) || "SC020010".equals(prdIdXd)
                                || "SC030014".equals(prdIdXd)
                                || "SC030024".equals(prdIdXd) || "SC010008".equals(prdId) || "SC020009".equals(prdId)) {//优企贷
                            if (managerBrId.startsWith("01")) {
                                FinaBrId = "016001";
                            } else {
                                FinaBrId = managerBrId.substring(0, 5) + "6";
                            }
                        } else {// 如果是小贷的其他业务从归属地取
                            FinaBrId = managerBrId.substring(0, 5) + "1";
                        }
                    } else {
                        FinaBrId = managerBrId.substring(0, 5) + "1";
                    }
                }

                // 小贷的利率都是取第一笔线下支用的利率，而不是取合同利率
                if ("022034".equals(prdId) || "SC010008".equals(prdId) || StringUtils.hasLength(prdIdXd)) {
                    // 小贷的需要以第一笔为准的
                    AccLoan accLoan = accLoanMapper.selectFirstAccLoanByParams(loanContNo);
                    if (accLoan != null) {//台账不能为空
                        execRateYear = accLoan.getExecRateYear();
                        // 排序取第一笔线上支用
                        overDueRate = execRateYear.multiply(BigDecimal.valueOf(1.5));
                        fuliReate = execRateYear.multiply(BigDecimal.valueOf(1.5));
                    }
                }

                if (null == pvpAmt) {
                    throw BizException.error(null, EpbEnum.EPB090009.value(), "未查询出账金额数据！");
                }

                if (pvpAmt.compareTo(lmt) > 0) {
                    msg = "贷款申请金额已经超过目前贷款合同可用余额，如果您有无用的贷款出账申请，请先予以撤销！";
                    logger.info("*****XDCZ0021:1-贷款申请金额已经超过目前贷款合同可用余额，如果您有无用的贷款出账申请，请先予以撤销！");
                } else if (billEndDate.compareTo(contEndDate) > 0) {
                    msg = "您所签署的借款合同到期日为" + contEndDate + ",请在借款合同期限内进行贷款出账！";
                    logger.info("*****XDCZ0021:2-您所签署的借款合同到期,请在借款合同期限内进行贷款出账！");
                }
                // 如果白领易贷通
                if ("022028".equals(prdId)) {
                    BigDecimal sumTranAmt = pvpAuthorizeMapper.querySumTranAmtByParams(loanContNo, openDay);

                    if (pvpAmt.compareTo(new BigDecimal(300000)) > 0) {
                        msg = "白领易贷通单次支用金额不允许超过30万元！";
                        logger.info("*****XDCZ0021:3-白领易贷通单次支用金额不允许超过30万元！");
                    } else if (pvpAmt.add(sumTranAmt).compareTo(new BigDecimal(300000)) > 0) {
                        msg = "白领易贷通当日申请发放金额不允许超过30万元，如果您有无用的贷款出账申请，请先予以撤销！";
                        logger.info("*****XDCZ0021:4-白领易贷通当日申请发放金额不允许超过30万元，如果您有无用的贷款出账申请，请先予以撤销！");
                    }
                    /**** add 2021-10-14
                     *担保方式为信用的白领易贷通支取期限最长不能超过1年
                     * ***************************************/
                    if (new BigDecimal(billTerm).compareTo(new BigDecimal("12")) > 0 && "00".equals(guarWay)) {//信用
                        logger.info("*****XDCZ0021:3-信用类白领易贷通支取期限最长不能超过1年！");
                        xdcz0021DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                        xdcz0021DataRespDto.setOpMsg("信用类白领易贷通支取期限最长不能超过1年");
                        return xdcz0021DataRespDto;
                    }
                } else if ("022032".equals(prdId)) {//薪易贷(新信贷无此产品)
                    //再次校验是否存在逾期贷款
                } else if ("SC010008".equals(prdId) || "SC020010".equals(prdIdXd)) {
                    // 线上业务限额校验
                    String getLmtOlsql = "";
                    BigDecimal balance = BigDecimal.ZERO;
                    BigDecimal lmtAmt = BigDecimal.ZERO;
                    String lmtAmtDate = "10";
                    // TODO 待开发 限额配置暂未完成
                    /*// 优企贷
                    if ("SC010008".equals(prdId)) {
                        getLmtOlsql =
                                "select  nvl(b.balance, 0) lmtOl, lmt_amt, lmt_amt_date" +
                                        "  from  (select lmt_amt,lmt_amt_date from lmt_cont_amt_ol where lmt_amt_type='01') a," +
                                        " (select sum(loan_balance) balance from acc_loan where account_status in ('1','2') and biz_type='SC010008' " +
                                        " and substr(LOAN_START_DATE, 9, 10) <= " +
                                        " (select lmt_amt_date " +
                                        "  from lmt_cont_amt_ol " +
                                        " where lmt_amt_type = '01' " +
                                        "  and rownum = 1)" +
                                        " and substr(LOAN_START_DATE, 0, 7)  = '" + today.substring(0, 7) + "'" +
                                        ") b ";
                    } else {// 优农贷
                        getLmtOlsql =
                                "select nvl(b.balance, 0) lmtOl, lmt_amt, lmt_amt_date" +
                                        "  from  (select lmt_amt,lmt_amt_date from lmt_cont_amt_ol where lmt_amt_type='02') a," +
                                        " (select sum(loan_balance) balance from acc_loan where account_status in ('1','2') and cont_no in (select cont_no from ctr_loan_cont where survey_serno in (select survey_serno from xd_cus_survey where biz_type='SC020010' and approve_status='997')) " +
                                        " and substr(LOAN_START_DATE, 9, 10) <= " +
                                        " (select lmt_amt_date " +
                                        "  from lmt_cont_amt_ol " +
                                        " where lmt_amt_type = '02' " +
                                        "  and rownum = 1)" +
                                        " and substr(LOAN_START_DATE, 0, 7)  = '" + today.substring(0, 7) + "'" +
                                        ") b ";
                    }
                    st = conn.createStatement();
                    rs = st.executeQuery(getLmtOlsql);
                    while (rs.next()) {
                        balance = rs.getDouble("lmtOl");
                        lmtAmt = rs.getDouble("lmt_amt");
                        lmtAmtDate = rs.getString("lmt_amt_date");
                    }*/
                    System.out.println("借据余额：" + balance);
                    System.out.println("限额总额：" + lmtAmt);
                    System.out.println("限额日期：" + lmtAmtDate);
                    if ((pvpAmt.add(balance)).compareTo(lmtAmt) > 0) {
                        // msg = "因系统升级，请下月1号使用。";
                    }
                    lmtAmtDate = openDay.substring(0, 7) + "-" + lmtAmtDate;
                    if (DateUtils.parseDate(openDay, "yyyy-MM-dd").after(DateUtils.parseDate(lmtAmtDate, "yyyy-MM-dd"))) {
                        //  msg = "因系统升级，请下月1号使用。";
                    }
                    String contType = ctrLoanCont.getContType();

                    String stsCondition = null;
                    // 优企贷
                    if ("SC010008".equals(prdId)) {
                        if ("2".equals(contType)) {
                            stsCondition = "'1', '2','6','3','4','5'";
                        } else {
                            stsCondition = "'0','1', '2', '5', '6'";
                        }
                    } else {//优农贷，随借随还
                        stsCondition = "'1', '2', '5'";
                    }
                    // 可用余额
                    BigDecimal loanBalanceAmount = BigDecimal.ZERO;
                    // 合同生效状态
                    if ("200".equals(contStatus)) {
                        loanBalanceAmount = contAmt.subtract(accLoanMapper.selectSumLoanAmtByParams(loanContNo, stsCondition.split(",")));
                    }

                    if (pvpAmt.compareTo(loanBalanceAmount) > 0) {
                        msg = "贷款申请金额已经超过目前贷款合同可用余额，无法继续支用！";
                        logger.info("*****XDCZ0021:5-贷款申请金额已经超过目前贷款合同可用余额，无法继续支用！");
                    }
                }
                //  优抵贷2.0支用校验
                if ("SC020009".equals(prdIdXd)) {
                    Date date = DateUtils.parseDate(openDay, "yyyy-MM-dd");
                    // 过去7天（包含今天）
                    String dayBefore7 = DateUtils.formatDate(DateUtils.addDay(date, -6), "yyyy-MM-dd");

                    LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoMapper.selectByPrimaryKey(surveySerno);
                    // 企业类型
                    String qyType = lmtSurveyReportComInfo.getCorpType();
                    // 个体工商户
                    if ("2".equals(qyType)) {
                        // 过去7天（包含今天）支用额度
                        BigDecimal yddAmountSum7 = accLoanMapper.selectXdSumLoanAmtIn7th(cusId, openDay, dayBefore7);
                        // 加上本次支用额度
                        BigDecimal yddSumAmountSum7 = yddAmountSum7.add(pvpAmt);
                        BigDecimal validAmount = new BigDecimal(500000);
                        if (yddSumAmountSum7.compareTo(validAmount) > 0) {
                            BigDecimal syAmount = validAmount.subtract(yddAmountSum7).divide(new BigDecimal(10000));
                            if (syAmount.compareTo(BigDecimal.ZERO) > 0) {
                                msg = "个体工商户一周之内最多支用50万元！本次最多可支用" + syAmount + "万元！";
                            } else {
                                msg = "个体工商户一周之内支用已超过50万元！无法支用！";
                            }
                        }
                    }
                    // 批复起始日
                    Date replyStartDate = DateUtils.parseDate(lmtCrdReplyInfo.getReplyStartDate(), "yyyy-MM-dd");
                    // 加3年
                    Date yearAfter3 = DateUtils.addYear(replyStartDate, 3);
                    if (date.after(yearAfter3)) {
                        msg = "批复有效期不能超过起始日3年！";
                        logger.info("*****XDCZ0021:6-批复有效期不能超过起始日3年！");
                    }
                }

                // 房易贷(不在新信贷了)和一商场的贷款需要通知到客户经理去信贷系统中放款
                prdIdRel = prdId;
                if ("SC030014".equals(prdIdXd) || "SC030024".equals(prdIdXd)) {
                    zjczFlag = false;
                    sendMsg("您管户下客户" + cusName + "在线上申请支用了一笔" + prdNameXd + ",请登录信贷系统进行确认!", telnum);
                    prdIdRel = prdIdXd;
                }
            } else {
                msg = "抱歉,没有找到您的可用额度!";
                logger.info("*****XDCZ0021:6-抱歉,没有找到您的可用额度!");
            }
            // 贷款科目号
            String AccountClass = "";
            // 优企贷
            if ("SC010008".equals(prdId) && "success".equals(msg)) {
                // 客户信息校验
                msg = this.grkhValid(xdkh0001DataRespDto, adminSmUserDto);
                logger.info("****************************XDCZ0021:优企贷客户信息校验开始:" + msg);
                if ("success".equals(msg)) {
                    String sfnh = "";
                    String hydm = "";
                    // 单位所属行业与是否农户 1是 0否
                    if (!StringUtils.hasLength(indivComFld) || !StringUtils.hasLength(agriflg)) {
                        // 授信调查基本信息
                        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoMapper.selcetLastReportBycoerCode(certCode);
                        String lastSurverySerno = lmtSurveyReportBasicInfo.getSurveySerno();
                        // 是否农户
                        sfnh = lmtSurveyReportBasicInfo.getIsAgri();
                        // 授信调查企业信息
                        LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoMapper.selectByPrimaryKey(lastSurverySerno);
                        // 行业代码
                        hydm = lmtSurveyReportComInfo.getTrade();

                        if (!StringUtils.hasLength(indivComFld)) {
                            indivComFld = hydm;
                            // 更新客户单位行业
                            CmisCus0015ReqDto cmisCus0015ReqDto = new CmisCus0015ReqDto();
                            cmisCus0015ReqDto.setCusId(cusId);
                            cmisCus0015ReqDto.setIndivComTrade(indivComFld);
                            logger.info("***********xdcz0021***更新客户单位行业开始,查询参数为:{}", JSON.toJSONString(cmisCus0015ReqDto));
                            ResultDto<CmisCus0015RespDto> resultDto = cmisCusClientService.cmiscus0015(cmisCus0015ReqDto);
                            logger.info("***********xdcz0021***更新客户单位行业结束,返回结果为:{}", JSON.toJSONString(resultDto));
                        }

                        if (!StringUtils.hasLength(agriflg)) {
                            agriflg = sfnh;
                            // 更新客户是否农户
                            CmisCus0026ReqDto cmisCus0026ReqDto = new CmisCus0026ReqDto();
                            cmisCus0026ReqDto.setCusId(cusId);
                            cmisCus0026ReqDto.setIsAgri(agriflg);
                            logger.info("***********xdcz0021***客户联系信息维护开始,查询参数为:{}", JSON.toJSONString(cmisCus0026ReqDto));
                            ResultDto<CmisCus0026RespDto> resultDto = cmisCusClientService.cmiscus0026(cmisCus0026ReqDto);
                            logger.info("***********xdcz0021***客户联系信息维护结束,返回结果为:{}", JSON.toJSONString(resultDto));
                        }

                    }
                    String firstIndustryCode = indivComFld.substring(0, 1);
                    if ("A".equals(firstIndustryCode)) {
                        //String lastIndustryCode = indivComFld.substring(1); 2021-10-14 新信贷带字母 例如 （A0111 稻谷种植）
                        String locate = "";
                        locate = getSTreedic("STD_ZB_TRADE_CLASS", indivComFld);
                        if (StringUtils.hasLength(locate)) {
                            String seccondIndustryCode = locate.split(",")[2];
                            if ("01".equals(seccondIndustryCode)) {
                                farmDirection = "F01";
                            } else if ("02".equals(seccondIndustryCode)) {
                                farmDirection = "F02";
                            } else if ("03".equals(seccondIndustryCode)) {
                                farmDirection = "F03";
                            } else if ("04".equals(seccondIndustryCode)) {
                                farmDirection = "F04";
                            } else if ("05".equals(seccondIndustryCode)) {
                                farmDirection = "F05";
                            }
                        }
                    }
                    if (Long.parseLong(billTerm) <= 12) {
                        if ("1".equals(agriflg)) {
                            AccountClass = "13011405";
                            if (!"A".equals(firstIndustryCode)) {
                                farmDirection = "F13";
                            }
                        } else {
                            if ("A".equals(firstIndustryCode)) {
                                AccountClass = "13040705";
                            } else {
                                AccountClass = "13041005";
                                farmDirection = "F14";
                            }
                        }
                    } else {
                        if ("1".equals(agriflg)) {
                            AccountClass = "13013405";
                            if (!"A".equals(firstIndustryCode)) {
                                farmDirection = "F13";
                            }
                        } else {
                            if ("A".equals(firstIndustryCode)) {
                                AccountClass = "13042705";
                            } else {
                                AccountClass = "13043005";
                                farmDirection = "F14";
                            }
                        }
                    }
                    LoanTermQj = "12";
                }
            }
            // 优农贷、优抵贷
            else if ("SC020010".equals(prdId) || "SC020009".equals(prdId)) {
                prdIdRel = prdIdXd;
                String firstIndustryCode = indivComFld.substring(0, 1);
                /******************从xd_prd_class_rel表中匹配科目***************/
                String is_ny = "";
                String is_cq = "";
                String industryType = "";
                if ("1".equals(agriflg)) {
                    is_ny = "1";
                } else {
                    is_ny = "0";
                }
                if (Long.parseLong(billTerm) <= 12) {
                    is_cq = "0";
                    LoanTermQj = "12";
                    RepayFlag = "1";
                } else {
                    is_cq = "1";
                    LoanTermQj = "36";
                    RepayFlag = "2";
                }
                if ("A".equals(firstIndustryCode)) {
                    industryType = "A";
                } else {
                    industryType = "0";
                }
                if ("SC020009".equals(prdIdXd) && ("J".equals(firstIndustryCode) || "S".equals(firstIndustryCode))) {
                    msg = "优抵贷禁止金融业、公共管理、社会保障和社会组织贷款投向，请联系管理员";
                    logger.info("*****XDCZ0021:7-优抵贷禁止金融业、公共管理、社会保障和社会组织贷款投向，请联系管理员");
                }

                /*************************从xd_industry_farm_dire_rel表中匹配科目****************************************************************/
                XdPrdClassRel record = new XdPrdClassRel();
                record.setIsNy(is_ny);
                record.setIsCq(is_cq);
                record.setUseId(loanUseType);
                record.setBizType(prdId);
                record.setCusType(cusType);
                record.setClassNo(industryType);//
                logger.info("**********根据贷款用途，产品代码，是否农业，是否长期查询小贷产品（线上）支用时候科目对应关系表开始,查询参数为:{}", JSON.toJSONString(record));
                XdPrdClassRel retuenRecord = xdPrdClassRelMapper.selectXdPrdClassRelByPrdId(record);
                logger.info("**********根据贷款用途，产品代码，是否农业，是否长期查询小贷产品（线上）支用时候科目对应关系表结束,返回参数为:{}", JSON.toJSONString(retuenRecord));
                if (retuenRecord != null) {
                    AccountClass = retuenRecord.getFarmDirection();//获取科目号
                    if (StringUtil.isEmpty(AccountClass)) {
                        msg = "没有匹配到对应科目，请联系管理员";
                    }
                } else {
                    msg = "没有匹配到对应科目，请联系管理员";
                }


                /*************************从xd_industry_farm_dire_rel表中匹配涉农投向*********************************************************/
                String industryCode = "";
                if ("A".equals(firstIndustryCode)) {
                    //String lastIndustryCode = indivComFld.substring(1); 2021-10-14 新信贷带字母 例如 （A0111 稻谷种植）
                    String locate = "";
                    locate = getSTreedic("STD_ZB_TRADE_CLASS", indivComFld);
                    if (StringUtils.hasLength(locate)) {
                        industryCode = locate.split(",")[2];
                    }
                } else {
                    industryCode = "0";
                }
                XdIndustryFarmDireRel farmRecord = new XdIndustryFarmDireRel();
                farmRecord.setBizType(prdId);
                farmRecord.setIsNy(is_ny);
                farmRecord.setIndustryType(industryCode);
                logger.info("**********根据贷款用途，产品代码，是否农业，匹配涉农投向开始,查询参数为:{}", JSON.toJSONString(farmRecord));
                XdIndustryFarmDireRel returnfarmRecord = xdIndustryFarmDireRelMapper.selectXdIndustryFarmDireRelByPrdId(farmRecord);
                logger.info("**********根据贷款用途，产品代码，是否农业，匹配涉农投向开始,返回参数为:{}", JSON.toJSONString(returnfarmRecord));
                if (returnfarmRecord != null) {
                    farmDirection = returnfarmRecord.getCfarmDirection();
                    if (StringUtil.isEmpty(farmDirection)) {
                        msg = "没有匹配到对应涉农投向，请联系管理员";
                    }
                } else {
                    msg = "没有匹配到对应涉农投向，请联系管理员";
                }

                /*************************从xd_prd_ident_rel表中根据产品代码查询借据标识********************************************************/
                XdPrdIdentRel identRelrecord = new XdPrdIdentRel();
                identRelrecord.setBizType(prdId);
                logger.info("**********根据产品代码查询借据标识开始,查询参数为:{}", JSON.toJSONString(identRelrecord));
                XdPrdIdentRel returnIdentRelrecord = xdPrdIdentRelMapper.selectXdPrdIdentRelByPrdId(identRelrecord);
                logger.info("**********根据产品代码查询借据标识结束,查询参数为:{}", JSON.toJSONString(returnIdentRelrecord));
                if (returnIdentRelrecord != null) {//结果不为空
                    loanTypeDetail = returnIdentRelrecord.getLoanTypeDetail();
                    isJyxwydk = returnIdentRelrecord.getIsJyxwydk();
                    isGmhy = returnIdentRelrecord.getIsGmhy();
                    isBxghy = returnIdentRelrecord.getIsBxghy();
                    isFptxdk = returnIdentRelrecord.getIsFptxdk();
                    isBzxajgc = returnIdentRelrecord.getIsBzxajgc();
                    isXgsyryxedb = returnIdentRelrecord.getIsXgsyryxedb();
                    loanUseType2 = returnIdentRelrecord.getLoanUseType();
                }
            } else {
                /******************从wy_class_rel表中匹配科目***************/
                String is_ny = "";
                String is_cq = "";
                if ("1".equals(agriflg)) {
                    is_ny = "1";
                    farmDirection = "F16";
                } else {
                    is_ny = "0";
                    farmDirection = "F14";
                }
                if (Long.parseLong(billTerm) <= 12) {
                    is_cq = "0";
                    LoanTermQj = "12";
                    RepayFlag = "1";
                } else {
                    is_cq = "1";
                    LoanTermQj = "36";
                    RepayFlag = "2";
                }
                //AccountClass = "13042705";
                CfgWyClassRelClientDto cfgWyClassRelClientDto = new CfgWyClassRelClientDto();
                cfgWyClassRelClientDto.setLoanPurp(loanUseType);
                cfgWyClassRelClientDto.setPrdCode(prdIdRel);
                cfgWyClassRelClientDto.setIsNy(is_ny);
                cfgWyClassRelClientDto.setIsCq(is_cq);
                logger.info("**********根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系开始,查询参数为:{}", JSON.toJSONString(cfgWyClassRelClientDto));
                ResultDto<CfgWyClassRelClientDto> resultDto = iCmisCfgClientService.selectbycondition(cfgWyClassRelClientDto);
                logger.info("**********根据贷款用途，产品代码，是否农业，是否长期查询网银支用科目对应关系结束,返回结果为:{}", JSON.toJSONString(resultDto));
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    cfgWyClassRelClientDto = resultDto.getData();
                    AccountClass = cfgWyClassRelClientDto.getSubjectNo();
                }
            }
            logger.info("**************************XDCZ0021:1-匹配到对应科目，匹配结果:" + msg + ";" + AccountClass);
            if (StringUtils.isEmpty(AccountClass) && "success".equals(msg)) {
                msg = "没有匹配到对应科目，请联系管理员";
            }
            /*********************************/
            if ("success".equals(msg)) {
                Map seqMap = new HashMap();
                seqMap.put("contNo", loanContNo);
                billNo = cmisBizXwCommonService.getBillNo(loanContNo);
                pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, seqMap);
                String tranSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
                //优抵贷、优农贷、优企贷借据编号取小贷线上借据合同关联信息表
                if ("SC020009".equals(prdId) || "SC020010".equals(prdId) || "SC010008".equals(prdId)) {
                    logger.info("*******XDCZ0021:优抵贷、优农贷、优企贷借据编号取小贷线上借据合同关联信息表*******");
                    String pvpbillNo = pvpBillContOnlineRelMapper.queryPvpBillContByPrimaryKey(seqMap);
                    logger.info("*******XDCZ0021:优抵贷、优农贷、优企贷借据编号取小贷线上借据合同关联信息表结果:" + pvpbillNo);
                    if (StringUtil.isNotEmpty(pvpbillNo)) {//不能为空
                        billNo = pvpbillNo;
                        //修改状态为已启用
                        seqMap.put("billNo", billNo);//借据编号
                        int updateNUm = pvpBillContOnlineRelMapper.updatePvpBillContByPrimaryKey(seqMap);
                        logger.info("*******XDCZ0021:优抵贷、优农贷、优企贷借据编号取小贷线上借据合同关联信息表更改状态结果:" + pvpbillNo);
                    }
                }
                PvpAuthorize pvpAuthorize = new PvpAuthorize();
                pvpAuthorize.setTranSerno(tranSerno);
                pvpAuthorize.setPvpSerno(pvpSerno);                                //出账号
                pvpAuthorize.setTranDate(openDay);                        //出账日期

                pvpAuthorize.setFldvalue01(AccountClass);                                    //贷款科目代码
                pvpAuthorize.setFldvalue02("LN003");                                        //***贷款类型
                pvpAuthorize.setContNo(loanContNo);                                        //合同号
                pvpAuthorize.setCusId(cusId);                                            //借款人客户号
                pvpAuthorize.setCusName(cusName);                                        //借款人客户名称
                pvpAuthorize.setBillNo(billNo);                                                //借据号
                pvpAuthorize.setPrdName(prdName);
                pvpAuthorize.setPrdId(prdId);
                pvpAuthorize.setFldvalue03(repaymentAccount);                            //收款人账户
                pvpAuthorize.setFldvalue04(repaymentAccount);                            //借款人结算账号

                pvpAuthorize.setFinaBrId(FinaBrId);                                        //账务机构
                pvpAuthorize.setManagerBrId(managerBrId);                                    //受理机构
                pvpAuthorize.setFldvalue41(managerBrId);                                    //放贷机构
                pvpAuthorize.setFldvalue08(billEndDate);                                    //到期日
                //pvpAuthorize.setFldvalue09(String.valueOf(monthDiffer));              //期限代码
                pvpAuthorize.setFldvalue10("CNY");                                        //币种
                pvpAuthorize.setTranAmt(pvpAmt);                                //放款金额
                pvpAuthorize.setFldvalue11("1");                                            //本金自动归还标志    0否1是
                pvpAuthorize.setFldvalue12("1");                                            //利息自动归还标志     0否1是
                pvpAuthorize.setFldvalue13("G");                                            //重定价代码
                //pvpAuthorize.setFldvalue14();                    //基准利率代码
                //pvpAuthorize.setFldvalue15(String.valueOf(pvpLoanApp.getFloatingRate())); //正常贷款利率浮动比例
                pvpAuthorize.setFldvalue16("0.5");                                        //逾期贷款利率浮动比例
                pvpAuthorize.setFldvalue17(overDueRate.toPlainString());                                    //逾期贷款执行利率
                pvpAuthorize.setFldvalue18(execRateYear.toPlainString());                                    //正常贷款执行利率
                pvpAuthorize.setFldvalue19(RepayFlag);                                        //还款方式      1-普通贷款 2-按揭分期贷款3-非按揭分期贷款
                pvpAuthorize.setFldvalue20("IP001");                                        //普通贷款结息方式代码
                //pvpAuthorize.setFldvalue21(pvpLoanApp.getRepaymentMode());       //分期贷款还款方式代码
                //pvpAuthorize.setFldvalue22(pvpLoanApp.getAssignLoanTotal());         //分期贷款总期数
                //pvpAuthorize.setFldvalue23(String.valueOf(pvpLoanApp.getDiscountRate()));   //贴息比例
                pvpAuthorize.setFldvalue24(billTerm);                        //借据期限
                pvpAuthorize.setFldvalue25("2");                //贷款承诺标志
                //pvpAuthorize.setFldvalue26(pvpLoanApp.getDealerAccount());            //经销商账户名
                //1大中型企业贷款承诺 2 小企业贷款承诺3 个人贷款承诺
                pvpAuthorize.setFldvalue27(cusName);                //放款账户名
                //pvpAuthorize.setFldvalue28(pvpLoanApp.getTrusteeAccountChild());    //委托存款子户号
                pvpAuthorize.setFldvalue29("1");                                            //贷款类型 1新增，3借新还旧
                pvpAuthorize.setManagerId(managerId);                                        //客户经理号码
                pvpAuthorize.setFldvalue30("1");                                            //是否计复息   0不计算、1计算

                pvpAuthorize.setFldvalue31("19");                                        //用途代码     00  流动资金   01  固定资金  02  技术改造  04  流资转期
                //05  固资转期 06  财政性直贷 07  财政性自营贷款 08  政府指令性贷款   09  其他
                //分期贷款：10  住房  11  汽车 12  商铺  13  工程机械       19  其他
                pvpAuthorize.setFldvalue32("2");                                            //是否受托支付
                pvpAuthorize.setFldvalue33("0");                                            //委托贷款手续费收取比例
                pvpAuthorize.setFldvalue34("0");                                            //委托贷款手续费
                //pvpAuthorize.setFldvalue35(pvpLoanApp.getPayStageDate());        //按揭还款日（缺省为放款日）
                pvpAuthorize.setFldvalue36("1");                                            //贷款利率浮动方式 1  加百分比 2 加点 3正常加点逾期加百分比
                pvpAuthorize.setFldvalue37("0");                                            //正常利率浮动点数(年利率)
                pvpAuthorize.setFldvalue38("0");                                            //逾期利率浮动点数(年利率)
                pvpAuthorize.setFldvalue39(prdId);                                        //业务品种
                pvpAuthorize.setFldvalue40("【贷款】");

                //法人账户透支
                pvpAuthorize.setFldvalue42(LoanTermQj);                                        //借用  执行利率方式
                pvpAuthorize.setFldvalue43(fuliReate.toPlainString());                                    //复利年利率
                pvpAuthorize.setFldvalue44(billEndDate);                                    //贷款到期日
                //pvpAuthorize.setFldvalue45(pvpLoanApp.getBizType());          //免息到期日
                // 贷款出账交易码
                pvpAuthorize.setTranId("CRE400");
//                    pvpAuthorize.setTradeCode(TradeCodeConstant.DKCZ);       //贷款出账交易码
                pvpAuthorize.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1);                            //核心状态未出账
//                    pvpAuthorize.setLoanFrom(Loan_from);//贷款来源1网银2手机
                pvpAuthorize.setAcctNo(repaymentAccount);
                pvpAuthorize.setCurType("CNY");
                pvpAuthorize.setBelgLine(ctrLoanCont.getBelgLine());

                int authorizeCnt = pvpAuthorizeMapper.insert(pvpAuthorize);
                if (authorizeCnt < 1) {
                    throw new Exception("插入出账授权时出错");
                }


                /** 插入贷款台帐 */
                // 原借据号不为空，为无还本续贷
                // 贷款形式 默认1--新增 6--无还本续贷
                String loanModal = "1";
                if (StringUtil.isNotEmpty(oldBillNo)) {
                    loanModal = "6";
                }

                AccLoan accLoan = new AccLoan();
                String pkid = cn.com.yusys.yusp.commons.util.StringUtils.uuid(true);
                accLoan.setPkId(pkid); // 主键
                accLoan.setPvpSerno(pvpSerno);
                accLoan.setBillNo(billNo);
                accLoan.setContNo(loanContNo);
                accLoan.setPrdId(prdId);
                accLoan.setPrdName(prdName);
                accLoan.setCusId(cusId);
                accLoan.setCusName(cusName);
                accLoan.setSubjectNo(AccountClass);
                accLoan.setLoanModal(loanModal);
                accLoan.setGuarMode(guarWay);
                accLoan.setContCurType("CNY");
                accLoan.setLoanAmt(pvpAmt);
                accLoan.setLoanBalance(pvpAmt);
                accLoan.setLoanStartDate(openDay);
                accLoan.setLoanEndDate(billEndDate);
                accLoan.setOrigExpiDate(billEndDate);//原到期日期
                accLoan.setRulingIr(rulingIr);
                accLoan.setRateFloatPoint(irFloatRate);
                accLoan.setExecRateYear(execRateYear);
                accLoan.setOverdueExecRate(overDueRate);
                accLoan.setOverdueBalance(BigDecimal.ZERO);
                //贷款投向
                accLoan.setLoanTer(loanTer);
                accLoan.setExtTimes("0");
                Map map = pvpLoanAppService.getFiveAndTenClass(cusId);
                accLoan.setFiveClass((String) map.get("fiveClass"));
                accLoan.setClassDate(openDay);
                accLoan.setManagerId(managerId);
                accLoan.setFinaBrId(FinaBrId);
                accLoan.setManagerBrId(managerBrId);
                accLoan.setDisbOrgNo(managerBrId);//放款机构
                accLoan.setUpdDate(openDay);
                accLoan.setAccStatus("6");
                accLoan.setReplyNo(repaymentAccount);
                accLoan.setTenClass("11");
                accLoan.setInputId(managerId);
                accLoan.setLoanUseType("19");
                accLoan.setRepayAccno(repaymentAccount);
                String minuesDay = "21";
                /***************************获取合同表还款日期***************************/
                String repayDate = ctrLoanCont.getRepayDate();//还款日期
                logger.info("***********调用iCmisCfgClientService查询产品条线*START**************产品编号:" + prdId);
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                String prdCode = prdresultDto.getCode();//返回结果
                String prdBelgLine = "";
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                    if (CfgPrdBasicinfoDto != null) {
                        prdBelgLine = CfgPrdBasicinfoDto.getPrdBelgLine();
                    }
                }
                logger.info("***********调用iCmisCfgClientService查询产品条线结束*START**************条线:" + prdBelgLine);
                if ("01".equals(prdBelgLine)) {//产品条线为小微条线
                    if (StringUtil.isNotEmpty(repayDate)) {//合同表中不为空,取合同表还款日
                        minuesDay = repayDate;
                    } else {
                        logger.info("****************扣款日期获取开始*START**************:");
                        RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
                        minuesDay = repayDayRecordInfo.getRepayDate().toString();
                    }
                } else if (StringUtil.isNotEmpty(repayDate)) {
                    minuesDay = repayDate;
                } else {//所有条件皆不满足,默认21号
                    minuesDay = "21";
                }
                accLoan.setDeductDay(minuesDay);
                accLoan.setEiIntervalUnit("M");
                accLoan.setEiIntervalCycle("1");
                accLoan.setRepayMode(repayType);
//                    accLoan.setLoanRepayMthd("AUTOPAY");
//                    accLoan.setLoanFixOdIntInd("N");
//                accLoan.setNextRateAdjInterval("1");
//                    accLoan.setDiverAdjPct(0);
//                    accLoan.setLoanOdIntRate1( Double.parseDouble(overDueRate));
                accLoan.setOverdueRatePefloat(BigDecimal.valueOf(0.5));
//                    accLoan.setRecommendId(recommend_id);
                accLoan.setAgriLoanTer(farmDirection);//涉农投向
//                    accLoan.setLoanFrom(Loan_from);
                accLoan.setLoanTermUnit("M");
                accLoan.setLoanTerm(billTerm);
                accLoan.setRateAdjMode("01");////利率调整方式 信贷字典项 01--固定利率 02--浮动利率
                accLoan.setBelgLine(ctrLoanCont.getBelgLine());
                accLoan.setLmtAccNo(ctrLoanCont.getLmtAccNo());
                // 是否使用授信额度
                accLoan.setIsUtilLmt("1");
                // 是否受托支付 0-否 1-是
                accLoan.setIsBeEntrustedPay("0");
                accLoan.setExchangeRate(BigDecimal.ONE);//汇率
                String LprRateIntval = new BigDecimal(billTerm).compareTo(new BigDecimal("60"))> 0 ? "A2" : "A1";
                accLoan.setLprRateIntval(LprRateIntval);
                BigDecimal curtLprRate = queryLprRate(LprRateIntval);//获取当前LPR利率
                accLoan.setCurtLprRate(curtLprRate);//当前LPR利率
                accLoan.setCiRatePefloat(new BigDecimal(0.5));//复息利率浮动比
                accLoan.setCiExecRate(execRateYear.multiply(new BigDecimal(1.5)));//复息执行利率
                accLoan.setDeductType("AUTO");//扣款方式
                accLoan.setExchangeRmbAmt(pvpAmt);//折合人民币金额
                accLoan.setLoanPayoutAccno(repaymentAccount);//贷款发放账号
                accLoan.setLoanPayoutSubNo("00001");//贷款发放账号子序号
                accLoan.setPayoutAcctName(ctrLoanCont.getCusName());//发放账号名称
                accLoan.setRepayAccno(repaymentAccount);//贷款还款账号
                accLoan.setRepaySubAccno("00001");//贷款还款账户子序号
                accLoan.setRepayAcctName(ctrLoanCont.getCusName());//还款账户名称
                accLoan.setRateFloatPoint(execRateYear.subtract(curtLprRate).multiply(new BigDecimal("10000")));//浮动点数

                if ("SC010008".equals(prdId)) {//优企贷
                    accLoan.setLoanTypeDetail("60");//经营其他
                    accLoan.setIsOperPropertyLoan("0");
                    accLoan.setIsSteelLoan("0");
                    accLoan.setIsStainlessLoan("0");
                    accLoan.setIsPovertyReliefLoan("0");
                    accLoan.setGoverSubszHouseLoan("00");
//                        accLoan.setIs_xgsyryxedb("04");
                    accLoan.setLoanUseType("00");//流动资金贷款
                } else if ("SC020010".equals(prdIdXd) || "SC020009".equals(prdIdXd)) {//优农贷、优抵贷
                    accLoan.setLoanTypeDetail(loanTypeDetail);
                    accLoan.setIsOperPropertyLoan(isJyxwydk);
                    accLoan.setIsSteelLoan(isGmhy);
                    accLoan.setIsStainlessLoan(isBxghy);
                    accLoan.setIsPovertyReliefLoan(isFptxdk);
                    accLoan.setGoverSubszHouseLoan(isBzxajgc);
//                  accLoan.setIs_xgsyryxedb(isXgsyryxedb);
                    accLoan.setLoanUseType(loanUseType);
                }
                int accLoanCnt = accLoanMapper.insert(accLoan);
                if (accLoanCnt < 1) {
                    throw new Exception("插入贷款台帐记录时出错");
                }


                //优企贷插入出账申请表
//                if ("SC010008".equals(prdId)) {
                PvpLoanApp pvpLoanApp = new PvpLoanApp();
                // 放款流水号
                pvpLoanApp.setPvpSerno(pvpSerno);
                // 业务申请流水号
                pvpLoanApp.setIqpSerno(ctrLoanCont.getIqpSerno());
                // 借据编号
                pvpLoanApp.setBillNo(billNo);
                // 合同编号
                pvpLoanApp.setContNo(loanContNo);
                // 调查编号
                pvpLoanApp.setSurveySerno(ctrLoanCont.getSurveySerno());
                // 中文合同编号
                pvpLoanApp.setContCnNo(ctrLoanCont.getContCnNo());
                // 客户编号
                pvpLoanApp.setCusId(cusId);
                // 客户名称
                pvpLoanApp.setCusName(cusName);
                // 产品编号
                pvpLoanApp.setPrdId(prdId);
                // 产品名称
                pvpLoanApp.setPrdName(prdName);
                // 贷款形式
                pvpLoanApp.setLoanModal(ctrLoanCont.getLoanModal());
                // 合同币种
                pvpLoanApp.setContCurType(ctrLoanCont.getCurType());
                pvpLoanApp.setCurType(ctrLoanCont.getCurType());//合同币种
                // 起始日
                pvpLoanApp.setStartDate(ctrLoanCont.getContStartDate());
                // 到期日
                pvpLoanApp.setEndDate(ctrLoanCont.getContEndDate());
                // 贷款起始日
                pvpLoanApp.setLoanStartDate(billStartDate);
                // 贷款到期日
                pvpLoanApp.setLoanEndDate(billEndDate);
                // 贷款期限
                pvpLoanApp.setLoanTerm(billTerm);
                // 贷款期限单位
                pvpLoanApp.setLoanTermUnit("M");
                // 出账金额
                pvpLoanApp.setPvpAmt(pvpAmt);
                // 合同金额
                pvpLoanApp.setContAmt(ctrLoanCont.getContAmt());
                // 担保方式
                pvpLoanApp.setGuarMode(guarWay);
                // 登记人
                pvpLoanApp.setInputId(managerId);
                // 登记机构
                pvpLoanApp.setInputBrId(managerBrId);
                // 主管客户经理
                pvpLoanApp.setManagerId(managerId);
                // 主管机构
                pvpLoanApp.setManagerBrId(managerBrId);
                //业务条线
                pvpLoanApp.setBelgLine(ctrLoanCont.getBelgLine());
                //
                pvpLoanApp.setFinaBrId(FinaBrId);
                pvpLoanApp.setDisbOrgNo(managerBrId);
                pvpLoanApp.setLoanPayoutAccno(repaymentAccount);//贷款发放账号
                pvpLoanApp.setLoanPayoutSubNo("00001");//贷款发放账号子序号
                pvpLoanApp.setPayoutAcctName(ctrLoanCont.getCusName());//发放账号名称
                pvpLoanApp.setRepayAccno(repaymentAccount);//贷款还款账号
                pvpLoanApp.setRepaySubAccno("00001");//贷款还款账户子序号
                pvpLoanApp.setRepayAcctName(ctrLoanCont.getCusName());//还款账户名称
                // 执行利率(年)
                pvpLoanApp.setExecRateYear(execRateYear);
                /*********************************** 默认值 ***********************************/
                // 贷款承诺标志STD_ZX_YES_NO
                pvpLoanApp.setLoanPromiseFlag("0");
                // 逾期利率浮动比
                pvpLoanApp.setOverdueRatePefloat(new BigDecimal(0.5));
                pvpLoanApp.setOverdueExecRate(execRateYear.multiply(new BigDecimal(1.5)));
                // 复息利率浮动比
                pvpLoanApp.setCiRatePefloat(new BigDecimal(0.5));
//                        KeyedCollectionUtil.setValue(kColl, "interest_acc_mode", "IP001");
                pvpLoanApp.setCiExecRate(execRateYear.multiply(new BigDecimal(1.5)));
                // 利率调整方式
                pvpLoanApp.setRateAdjMode("01");//利率调整方式 信贷字典项 01--固定利率 02--浮动利率
                // 是否使用授信额度
                pvpLoanApp.setIsUtilLmt("1");
                //本金自动归还标志
                pvpLoanApp.setCapAutobackFlag("0");
                // 利息自动归还标志
                pvpLoanApp.setIntAutobackFlag("1");
                pvpLoanApp.setRateFloatPoint(execRateYear.subtract(curtLprRate).multiply(new BigDecimal("10000")));//浮动点数
                // 是否贴息
                pvpLoanApp.setIsSbsy("0");
                // 是否计复息
                pvpLoanApp.setIsMeterCi("1");
                // 扣款日
                pvpLoanApp.setDeductDay(minuesDay);
                // 结息周期
                pvpLoanApp.setEiIntervalUnit("M");
                // 扣款方式
                pvpLoanApp.setDeductType("AUTO");
                // 结息间隔周期
                pvpLoanApp.setEiIntervalCycle("1");

//                        KeyedCollectionUtil.setValue(kColl, "if_spec_flag", "2");// 是否剔除考核贷款
                //pvpLoanApp.setIrFloatRate(BigDecimal.ZERO);//固定利率不需要
                // 是否落实贷款
                pvpLoanApp.setIsPactLoan("1");
                pvpLoanApp.setInputDate(openDay);
                // 是否受托支付 0-否；1-是
                pvpLoanApp.setIsBeEntrustedPay("0");
                //支付方式 0-自主支付；1-受托支付
                pvpLoanApp.setPayMode("0");
                // 申请状态
                pvpLoanApp.setApproveStatus("997");
                // 贷款投向
                pvpLoanApp.setLoanTer(loanTer);
                // 贷款类别
                if ("SC010008".equals(prdId)) {//优企贷
                    pvpLoanApp.setLoanTypeDetail("60");
                }
                pvpLoanApp.setLoanTypeDetail("");
                // 涉农贷款投向
                pvpLoanApp.setAgriLoanTer(farmDirection);

                pvpLoanApp.setChnlSour("01");// 默认手机银行

                pvpLoanApp.setPvpMode("1");//自动出账
                pvpLoanApp.setLoanSubjectNo(AccountClass);//科目号
                pvpLoanApp.setLmtAccNo(ctrLoanCont.getLmtAccNo());
                pvpLoanApp.setExchangeRate(BigDecimal.ONE);//汇率
                pvpLoanApp.setCvtCnyAmt(pvpAmt);//折算人民币金额
                pvpLoanApp.setLprRateIntval(LprRateIntval);//LPR授信利率区间
                pvpLoanApp.setCurtLprRate(curtLprRate);//当前LPR利率
                pvpLoanApp.setRepayMode(repayType);//还款方式

                pvpLoanAppMapper.insert(pvpLoanApp);

                // 若是优企贷无还本续贷，需插入借新还旧信息表
                if ("SC010008".equals(prdId) && StringUtil.isNotEmpty(oldBillNo)) {
                    // 插入借新还旧信息表
                    logger.info("放款流水号【{}】,插入借新还旧信息开始", pvpLoanApp.getPvpSerno());
                    PvpJxhjRepayLoan pvpJxhjRepayLoan = new PvpJxhjRepayLoan();
                    pvpJxhjRepayLoan.setSerno(pvpLoanApp.getPvpSerno());
                    pvpJxhjRepayLoan.setBillNo(oldBillNo);
                    pvpJxhjRepayLoan.setNewLoanAmt(pvpLoanApp.getPvpAmt().toString());
                    pvpJxhjRepayLoan.setSelfPrcp("0");
                    pvpJxhjRepayLoan.setSelfInt("0");
                    pvpJxhjRepayLoan.setInputId(pvpLoanApp.getInputId());
                    pvpJxhjRepayLoan.setInputBrId(pvpLoanApp.getInputBrId());
                    pvpJxhjRepayLoan.setInputDate(openDay);
                    pvpJxhjRepayLoan.setUpdId(pvpLoanApp.getInputId());
                    pvpJxhjRepayLoan.setUpdBrId(pvpLoanApp.getInputBrId());
                    pvpJxhjRepayLoan.setUpdDate(openDay);
                    pvpJxhjRepayLoanService.insert(pvpJxhjRepayLoan);
                    logger.info("放款流水号【{}】,插入借新还旧信息结束", pvpLoanApp.getPvpSerno());

                    // 插入PVP_LOAN_APP_REPAY_BILL_RELL
                    logger.info("放款流水号【{}】,插入借新还旧信息关系表开始", pvpLoanApp.getPvpSerno());
                    PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell = new PvpLoanAppRepayBillRell();
                    pvpLoanAppRepayBillRell.setSerno(pvpLoanApp.getPvpSerno());
                    pvpLoanAppRepayBillRell.setBillNo(oldBillNo);
                    pvpLoanAppRepayBillRell.setOprType("01");
                    pvpLoanAppRepayBillRell.setInputId(pvpLoanApp.getInputId());
                    pvpLoanAppRepayBillRell.setInputBrId(pvpLoanApp.getInputBrId());
                    pvpLoanAppRepayBillRell.setInputDate(openDay);
                    pvpLoanAppRepayBillRell.setUpdId(pvpLoanApp.getInputId());
                    pvpLoanAppRepayBillRell.setUpdBrId(pvpLoanApp.getInputBrId());
                    pvpLoanAppRepayBillRell.setUpdDate(openDay);
                    pvpLoanAppRepayBillRellService.insert(pvpLoanAppRepayBillRell);
                    logger.info("放款流水号【{}】,插入借新还旧信息关系表结束", pvpLoanApp.getPvpSerno());
                }

                /***************************************放款成功后进行台账校验*********************************************/
                cmisBizXwCommonService.loanLmtForXw(pvpLoanApp);

                // TODO 优惠券与免息 待开发
                if (StringUtils.hasLength(preferCode)) {
                    //保存优惠券信息
                    DiscCouponBusinRel discCouponBusinRel = new DiscCouponBusinRel();
                    String pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ_BIZ_13, new HashMap<>());
                    discCouponBusinRel.setPkId(pkId);
                    discCouponBusinRel.setBusiType("01");
                    discCouponBusinRel.setCreateTime(new Date());
                    discCouponBusinRel.setDiscountCode(preferCode);
                    discCouponBusinRel.setSerno(pvpLoanApp.getPvpSerno());
                    discCouponBusinRel.setUpdateTime(new Date());
                    discCouponBusinRelMapper.insert(discCouponBusinRel);
                    //如果不是白领贷
                    if (!"022028".equals(prdId)) {
                        //2、调用小微公众号接口核销优惠券
                        Xwh003ReqDto xwh003ReqDto = new Xwh003ReqDto();
                        xwh003ReqDto.setCardSecret(preferCode);
                        xwh003ReqDto.setCusId(pvpLoanApp.getCusId());//客户号
                        xwh003ReqDto.setIdCard(certCode);//证件号
                        //状态1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）
                        xwh003ReqDto.setStatus("2");
                        xwh003ReqDto.setBillNo(pvpLoanApp.getBillNo());
                        xwh003ReqDto.setLoanContNo("");
                        ResultDto<Xwh003RespDto> resultDto = dscms2XwhClientService.xwh003(xwh003ReqDto);
                    }
                }
                //保存免息信息
//                if (StringUtils.hasLength(isIntFree)) {
//
//                }
            }

            logger.info("**************************XDCZ0021:1-数据准备完毕，判断结果:" + msg + ";" + zjczFlag);
            if ("success".equals(msg) && zjczFlag) {//出账
                msg = chuzhang(adminSmUserDto, xdkh0001DataRespDto, pvpSerno, loanContNo, billNo, prdId, recomManagerId);
            }

            if ("success".equals(msg)) {
                xdcz0021DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_SUCCESS.key);
                xdcz0021DataRespDto.setOpMsg(msg);
            } else {
                xdcz0021DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdcz0021DataRespDto.setOpMsg(msg);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, e.getMessage());
            xdcz0021DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0021DataRespDto.setOpMsg("支取失败");
            throw BizException.error(null, EpbEnum.EPB099999.value, e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, e.getMessage());
            xdcz0021DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
            xdcz0021DataRespDto.setOpMsg("支取失败");
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value);
        return xdcz0021DataRespDto;
    }


    /*
     * 客户信息校验
     * @param context
     * @param connection
     * @param dao
     * @param imp
     * @param cusId
     */
    private String grkhValid(Xdkh0001DataRespDto xdkh0001DataRespDto, AdminSmUserDto adminSmUserDto) throws Exception {
        /*String sql =
                "SELECT ci.CUS_NAME, ci.AREA_CODE, ci.AREA_NAME, ci.AGRI_FLG, ci.CUS_TYPE, ci.MOBILE AS CUS_MOBILE, su.TELNUM AS CUST_MANAGER_MOBILE, " +
                        "ci.INDIV_COM_NAME, ci.INDIV_COM_FLD, ci.INDIV_COM_FLD_NAME " +
                        "FROM CUS_INDIV ci " +
                        "LEFT JOIN S_USER su ON ci.CUST_MGR = su.ACTORNO " +
                        "WHERE ci.CUS_ID = '" + cusId + "'";*/
        String cusName = xdkh0001DataRespDto.getCusName();
        String areaCode = xdkh0001DataRespDto.getRegionalism();
        String areaName = xdkh0001DataRespDto.getRegionName();
        String agriflg = xdkh0001DataRespDto.getIsAgri();
        String cusType = xdkh0001DataRespDto.getCusType();
        String cusMobile = xdkh0001DataRespDto.getMobileNo();
        String custManagerMobile = adminSmUserDto.getUserMobilephone();
        String indivComName = xdkh0001DataRespDto.getUnitName();
        String indivComFld = xdkh0001DataRespDto.getIndivComTrade();
        String indivComFldName = xdkh0001DataRespDto.getIndivComFldName();

        String msg = "success";

        if (!StringUtils.hasLength(areaCode)) {
            msg = "居住区域编码为空！";
        }
//        if (!StringUtils.hasLength(areaName)) {// 居住区域名称为空校验去掉,客户信息只存code，页面返显
//            msg = "居住区域名称为空！";
//        }
        if (!StringUtils.hasLength(agriflg)) {
            msg = "是否农户为空！";
        }
        if (!StringUtils.hasLength(indivComName)) {
            msg = "经营企业名称/工作单位为空！";
        }
        if (!StringUtils.hasLength(indivComFld)) {
            msg = "单位所属行业为空！";
        }
        if (!StringUtils.hasLength(indivComFldName)) {
            msg = "单位所属行业名称为空！";
        }
        if (!"150".equals(cusType)) {
            msg = "客户类型非小微企业主！";
        }
//        String areaSql = "SELECT COUNT(*) FROM XD_AGRICU_AREA WHERE AREA_CODE = '" + areaCode + "' AND AGRI_FLG = '" + agriflg + "'";
//        String count = impComponent.getStringByCondition(areaSql, connection);
        // TODO 需要修改
        int count = 1;
        if (count == 0) {
            msg = "居住区域编码和是否农户不一致！";
        }
        logger.info("*****XDCZ0021:1-客户信息校验:" + msg);
        if (!"success".equals(msg)) {
            String sendCusMessage = "您的信息核验失败，请联系客户经理";
            String sendMgrMessage = "您的客户[" + cusName + "]支用申请时，" + msg + "请及时联系处理";
            //号码不能为空
            if (StringUtil.isNotEmpty(cusMobile) && StringUtil.isNotEmpty(custManagerMobile)) {
                sendMsg(sendCusMessage, cusMobile);
                sendMsg(sendMgrMessage, custManagerMobile);
            }
        }

        return msg;
    }

    /**
     * 发送核心
     *
     * @param pvpSerno
     * @param contNo
     * @param billNo
     * @param prdId
     * @return
     * @throws
     */
    public String chuzhang(AdminSmUserDto adminSmUserDto, Xdkh0001DataRespDto xdkh0001DataRespDto, String pvpSerno, String contNo, String billNo, String prdId, String recommendId) throws BizException {
        logger.info("**********XDCZ0021***1*发送核心操作开始,参数为:{}", JSON.toJSONString(xdkh0001DataRespDto));
        String msg = "success";
        PvpAuthorize pa = new PvpAuthorize();
        // 普通贷款
        AccLoan accLoan = new AccLoan();
        CtrLoanCont cont = new CtrLoanCont();
        // 委托贷款
        AccEntrustLoan accAuthLoan = new AccEntrustLoan();
        // 委托贷款合同表
        CtrEntrustLoanCont ctrAuthloanCont = new CtrEntrustLoanCont();

//        KeyedCollection reqPkg = new KeyedCollection();
        PvpAuthorize paOld = null;

        Map repPkg = new HashMap();
        try {

            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            logger.info("**********XDCZ0021***2*根据出账流水号查询,查询参数为:{}", JSON.toJSONString(pvpSerno));
            pa = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            logger.info("**********XDCZ0021***2*根据出账流水号查询,返回参数为:{}", JSON.toJSONString(pa));
            String tradeCode = pa.getTranId();

            if ("WTDK".equals(tradeCode)) {
                accAuthLoan = accEntrustLoanMapper.selectAccEntrustLoanByBillNo(billNo);
                ctrAuthloanCont = ctrEntrustLoanContMapper.selectByContNo(contNo);
            } else {
                accLoan = accLoanMapper.selectByBillNo(billNo);
                cont = ctrLoanContMapper.selectByPrimaryKey(contNo);
            }
            logger.info("**********XDCZ0021***3*根据出账流水号查询,查询参数为:{}", JSON.toJSONString(pvpSerno));
            paOld = pvpAuthorizeMapper.selectPvpAuthorizeBySerno(pvpSerno);
            logger.info("**********XDCZ0021***3*根据出账流水号查询,返回参数为:{}", JSON.toJSONString(paOld));
            String tradeStatus = paOld.getTradeStatus();
            //判断是否是交易成功状态，如果是交易成功状态不做修改
            if (!CmisCommonConstants.TRADE_STATUS_2.equals(tradeStatus)) {
                //更新授权表状态为发送核心未知，
                pa.setTradeStatus(CmisCommonConstants.TRADE_STATUS_1);
            }

            PvpEntrustLoanApp pvpEntrustLoanApp = null;
            PvpLoanApp pvpLoanApp = null;
            if ("042175".equals(prdId)) {//委托贷款
                pvpEntrustLoanApp = pvpEntrustLoanAppMapper.selectByEntrustSernoKey(pvpSerno);
            } else {
                //否则获取贷款业务出账信息表
                pvpLoanApp = pvpLoanAppMapper.selectByPvpLoanSernoKey(pvpSerno);
            }
            pvpAuthorizeMapper.updateByPrimaryKey(pa);
            logger.info("**********XDCZ0021***4*sendAuthToCoreForXd发送核心开始,发送参数为:{}", JSON.toJSONString(pa));
            ResultDto resultDto = pvpAuthorizeService.sendAuthToCoreForXd(pa);
            logger.info("**********XDCZ0021***4*sendAuthToCoreForXd发送核心结束,返回参数为:{}", JSON.toJSONString(resultDto));
            String coreRepCode = resultDto.getCode();
            String errorMsg = resultDto.getMessage();

            if ("9999".equals(resultDto.getCode())) {
                msg = resultDto.getMessage();
            }

            //查询出账信息
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(coreRepCode)) {
//                ImpComponent imp= (ImpComponent)CMISComponentFactory.getComponentFactoryInstance().getComponentInstance("ImpComponent", context,connection);
                //房抵e点贷放款成功后 生成购销合同补扫
//                String contno=pa.getContNo();
//                String product=imp.getStringByCondition("select PRODUCT from ctr_loan_cont where cont_no='"+contno+"'", connection);
//                String is_xs=imp.getStringByCondition("select is_xs from ctr_loan_cont where cont_no='"+contno+"'", connection);

                if ("022011".equals(prdId)/*&&"5".equals(product)&&"1".equals(is_xs)*/) {
                    this.htyxbs(pa);
                }

                //如果为小贷放款成功后，通知客户经理
                String managerBrId = pa.getManagerBrId();
                if ("016000".equals(managerBrId)) {
                    String telNum = adminSmUserDto.getUserMobilephone();
                    String cusName = pa.getCusName();
                    if (telNum != null && !"".equals(telNum)) {
                        String content = "客户" + cusName + "已成功放款 ！";
                        sendMsg(content, telNum);
                    }
                    // 通知小微公众号
//                    XwWeChatOfficailAccountCommOp xwWeChatOfficailAccountCommOp = new XwWeChatOfficailAccountCommOp();
//                    xwWeChatOfficailAccountCommOp.sendAccLoanInfo(accLoan, pa, context, connection);
                    try {
                        Xwh001ReqDto xwh001ReqDto = new Xwh001ReqDto();
                        xwh001ReqDto.setCusId(accLoan.getCusId());
                        xwh001ReqDto.setIdCard(xdkh0001DataRespDto.getCertCode());
                        xwh001ReqDto.setCusName(cusName);
                        xwh001ReqDto.setBillNo(billNo);
                        xwh001ReqDto.setLoanAmount(accLoan.getLoanAmt());
                        xwh001ReqDto.setMonth(Integer.parseInt(accLoan.getLoanTerm()));
                        xwh001ReqDto.setStartDate(accLoan.getLoanStartDate());
                        xwh001ReqDto.setEndDate(accLoan.getLoanEndDate());
                        xwh001ReqDto.setRecommendId(recommendId);
                        xwh001ReqDto.setProductName(accLoan.getPrdName());
                        xwh001ReqDto.setPhone(xdkh0001DataRespDto.getMobileNo());
                        dscms2XwhClientService.xwh001(xwh001ReqDto);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }

        } catch (Exception e) {
            msg = e.getMessage();
            logger.error(e.getMessage(), e);
        }
        return msg;
    }

    private void htyxbs(PvpAuthorize pvpAuthorize) {
        try {
            // TODO 不确定流水号规则
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>());

            String cus_name = pvpAuthorize.getCusName();//客户名称
            String cusId = pvpAuthorize.getCusId();
            String managerId = pvpAuthorize.getManagerId();
            String inputDate = pvpAuthorize.getInputDate();
            String managerBrId = pvpAuthorize.getManagerBrId();
            String serno1 = pvpAuthorize.getPvpSerno();
            String contNo = pvpAuthorize.getContNo();
            String billNo = pvpAuthorize.getBillNo();

            String insertimagesql =
                    "INSERT INTO ADD_IMAGEINFO (" +
                            "SERNO, SERNO_OLD, TYPE, APPROVE_STATUS, INPUT_ID, INPUT_DATE, INPUT_ORG, CUS_ID, CUS_NAME, LOAN_NO, CONT_NO, PRD_NAME, " +
                            "PRD_CODE, APPLY_AMOUNT, LOAN_START_DATE, LOAN_END_DATE, YX_SERNO, SX_YX_FLAG, CUS_TYPE, SYSGEN_FLAG, LMT_BS_TYPE) " +
                            "VALUES (" +
                            "'" + serno + "', '" + serno1 + "', '2', '000', '" + managerId + "', '" + inputDate + "', '" + managerBrId + "', '" + cusId + "', '" + cus_name + "', '" + billNo + "', '" + contNo + "', '【房抵e点贷购销合同】', " +
                            "'022011', null, null, null, null, null, null, null, null)";
            // TODO 无新信贷表
//            impComponent.updateByCondition(insertimagesql, connection);

            // TODO 没有经办人
//            String jb_mansql = "update pvp_authorize set JB_MANAGER_ID='" + manager_id + "' where bill_no='" + bill_no + "'";
//            impComponent.updateByCondition(jb_mansql, connection);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * 客户基本信息
     *
     * @param cusId
     * @return
     */
    private Xdkh0001DataRespDto queryXdkh0001DataRespDto(String cusId) {
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();//请求Dto：查询个人客户基本信息
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;//响应Dto：查询个人客户基本信息
        xdkh0001DataReqDto.setCusId(cusId);
        xdkh0001DataReqDto.setQueryType("01");

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        ResultDto<Xdkh0001DataRespDto> xdkh0001DataRespDtoResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDtoResultDto));

        String xdkh0001Code = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdkh0001Meesage = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            xdkh0001DataRespDto = xdkh0001DataRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xdkh0001Code, xdkh0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value);
        return xdkh0001DataRespDto;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
            ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

            String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            SenddxRespDto senddxRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                //  获取相关的值并解析
                senddxRespDto = senddxResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(senddxCode, senddxMeesage);
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new Exception("发送短信失败！");
        }
    }

    /**
     * 获取树形字典的目录
     *
     * @param opttype
     * @param enname
     * @return
     */
    private String getSTreedic(String opttype, String enname) {
        String locate = "";
        Xdxt0013DataReqDto xdxt0013DataReqDto = new Xdxt0013DataReqDto();
        xdxt0013DataReqDto.setOptType(opttype);
        xdxt0013DataReqDto.setEnname(enname);

        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataReqDto));
        ResultDto<Xdxt0013DataRespDto> xdxt0013DataRespDtoResultDto = dscmsCfgXtClientService.xdxt0013(xdxt0013DataReqDto);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataRespDtoResultDto));

        Xdxt0013DataRespDto xdxt0013DataRespDto = Optional.ofNullable(xdxt0013DataRespDtoResultDto.getData()).orElse(new Xdxt0013DataRespDto());
        List<OptList> optList = xdxt0013DataRespDto.getOptList();
        if (null != optList && null != optList.get(0)) {
            locate = optList.get(0).getLocate();
        }
        return locate;
    }

    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(String newVal) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0");
        try {
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
            }else{
                throw BizException.error("", "", "未查询到LPR利率,请检查!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }

}