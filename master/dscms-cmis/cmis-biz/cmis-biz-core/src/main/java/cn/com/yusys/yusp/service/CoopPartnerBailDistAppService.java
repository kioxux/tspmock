/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPartnerBailDistApp;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerBailDistAppMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerBailDistAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 13:14:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPartnerBailDistAppService {

    @Autowired
    private CoopPartnerBailDistAppMapper coopPartnerBailDistAppMapper;
    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPartnerBailDistApp selectByPrimaryKey(String serno) {
        return coopPartnerBailDistAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPartnerBailDistApp> selectAll(QueryModel model) {
        List<CoopPartnerBailDistApp> records = (List<CoopPartnerBailDistApp>) coopPartnerBailDistAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPartnerBailDistApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPartnerBailDistApp> list = coopPartnerBailDistAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopPartnerBailDistApp record) {
        return coopPartnerBailDistAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPartnerBailDistApp record) {
        return coopPartnerBailDistAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopPartnerBailDistApp record) {
        return coopPartnerBailDistAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPartnerBailDistApp record) {
        return coopPartnerBailDistAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return coopPartnerBailDistAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPartnerBailDistAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:saveTemp
     * @函数描述:保证金账户信息查询
     * @参数与返回说明:
     * @算法描述:
     */
    public Ib1253RespDto queryBailAcctInfo(Map<String, Object> map) {
        Map<String,String> resultMap = new HashMap<>();
        // 2) 对应的业务逻辑处增加以下代码：
        Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
        //  入参赋值 开始
        ib1253ReqDto.setKehuzhao((String) map.get("kehuhao"));// 客户账号
        ib1253ReqDto.setZhhaoxuh((String) map.get("zixuhao"));// 子序号
        //  ib1253ReqDto其他值赋予对应的业务值
        //  入参赋值 结束
        ResultDto<Ib1253RespDto> ib1253ResultDto = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
        String ib1253Code = Optional.ofNullable(ib1253ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ib1253Meesage = Optional.ofNullable(ib1253ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ib1253RespDto ib1253RespDto = null;
        if (Objects.equals(ib1253Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取相关的值并解析
            ib1253RespDto = ib1253ResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null,ib1253Code, ib1253Meesage);
        }
        return ib1253RespDto;
    }
}
