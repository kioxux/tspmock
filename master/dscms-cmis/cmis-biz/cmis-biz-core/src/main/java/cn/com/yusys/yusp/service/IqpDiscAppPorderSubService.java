/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.IqpAccpAppPorderSub;
import cn.com.yusys.yusp.domain.IqpDiscAppPorderSub;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpDiscAppPorderSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.vo.IqpAccpAppPorderSubImportVo;
import cn.com.yusys.yusp.vo.IqpDiscAppPorderSubImportVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiscAppPorderSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: chenlong9
 * @创建时间: 2021-04-12 15:06:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpDiscAppPorderSubService {
    private static final Logger log = LoggerFactory.getLogger(IqpDiscAppPorderSubService.class);

    @Autowired
    private IqpDiscAppPorderSubMapper iqpDiscAppPorderSubMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateService sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpDiscAppPorderSub selectByPrimaryKey(String pkId) {
        return iqpDiscAppPorderSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDiscAppPorderSub> selectAll(QueryModel model) {
        List<IqpDiscAppPorderSub> records = (List<IqpDiscAppPorderSub>) iqpDiscAppPorderSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpDiscAppPorderSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<IqpDiscAppPorderSub> list = iqpDiscAppPorderSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpDiscAppPorderSub record) {
        return iqpDiscAppPorderSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpDiscAppPorderSub record) {
        return iqpDiscAppPorderSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpDiscAppPorderSub record) {
        return iqpDiscAppPorderSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpDiscAppPorderSub record) {
        return iqpDiscAppPorderSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpDiscAppPorderSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDiscAppPorderSubMapper.deleteByIds(ids);
    }

    /**
     * 贴现汇票明细新增页面
     * @param iqpDiscAppPorderSub
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIadPorderSubInfo(IqpDiscAppPorderSub iqpDiscAppPorderSub) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpDiscAppPorderSub == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpDiscAppPorderSub.setInputId(userInfo.getLoginCode());
                iqpDiscAppPorderSub.setInputBrId(userInfo.getOrg().getCode());
                iqpDiscAppPorderSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();
            iqpDiscAppPorderSub.setPkId(StringUtils.uuid(true));
            iqpDiscAppPorderSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            int insertCount = iqpDiscAppPorderSubMapper.insertSelective(iqpDiscAppPorderSub);
            if(insertCount<=0){
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

        }catch(YuspException e){
            log.error("贴现汇票明细新增保存异常！",e.getMsg());
            throw e;
        }catch(Exception e){
            log.error("保存贴现汇票明细异常！",e.getMessage());
            throw e;
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(IqpDiscAppPorderSub iqpDiscAppPorderSub) {
        iqpDiscAppPorderSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return iqpDiscAppPorderSubMapper.updateByPrimaryKey(iqpDiscAppPorderSub);
    }

    /**
     * 异步下载贴现汇票明细模板
     * @return 导出进度信息
     */
    public ProgressDto asyncExportIqpDiscAppPorderSubTemp() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {

                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(IqpDiscAppPorderSubImportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);

    }

    /**
     * 批量插入贴现汇票明细
     *
     * @param perWhiteInfoList 解析出的Excel数据
     * @return 本次批量插入数据量   IqpDiscAppPorderSub iqpDiscAppPorderSub
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertIqpDiscAppPorderSub(List<IqpDiscAppPorderSubImportVo> perWhiteInfoList) {
        List<IqpDiscAppPorderSub> iqpDiscAppPorderSubList = (List<IqpDiscAppPorderSub>) BeanUtils.beansCopy(perWhiteInfoList, IqpDiscAppPorderSub.class);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            IqpDiscAppPorderSubMapper iqpDiscAppPorderSubMapper = sqlSession.getMapper(IqpDiscAppPorderSubMapper.class);
            for (IqpDiscAppPorderSub iqpDiscAppPorderSub : iqpDiscAppPorderSubList) {
                QueryModel query = new QueryModel() ;
                query.addCondition("drftNo", iqpDiscAppPorderSub.getDrftNo()); //票据号
                query.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);  //操作类型
                List<IqpDiscAppPorderSub> iqpDiscAppPorderSubs = iqpDiscAppPorderSubMapper.selectByModel(query) ;
                //数据库中已存在，更新信息
                if (CollectionUtils.isNotEmpty(iqpDiscAppPorderSubs)){
                    IqpDiscAppPorderSub bizIqpDiscAppPorderSub = iqpDiscAppPorderSubs.get(0);
                    //到期日期
                    bizIqpDiscAppPorderSub.setEndDate(iqpDiscAppPorderSub.getEndDate());
                    //最近修改人
                    bizIqpDiscAppPorderSub.setUpdId(userInfo.getLoginCode());
                    //最近修改机构
                    bizIqpDiscAppPorderSub.setUpdBrId(userInfo.getOrg().getCode());
                    //最近修改日期
                    bizIqpDiscAppPorderSub.setUpdDate(sf.format(new Date()));
                    //修改时间
                    bizIqpDiscAppPorderSub.setUpdateTime(new Date());
                    iqpDiscAppPorderSubMapper.updateByPrimaryKeySelective(bizIqpDiscAppPorderSub);
                }else{//数据库中不存在，新增信息
                    //生成主键
                    Map paramMap= new HashMap<>() ;
                    String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, paramMap);
                    iqpDiscAppPorderSub.setPkId(pkValue);
                    //申请流水号,塞入模板工厂里面的流水号
//                    iqpAccpAppPorderSub.setSerno();
                    //票据号
//                    iqpAccpAppPorderSub.setDrftNo();
                    //票面金额,转换
                    iqpDiscAppPorderSub.setDrftAmt(iqpDiscAppPorderSub.getDrftAmt().multiply(new BigDecimal(10000)));
                    //操作类型
                    iqpDiscAppPorderSub.setOprType(CmisLmtConstants.OPR_TYPE_ADD);
                    //登记人
                    iqpDiscAppPorderSub.setInputId(userInfo.getLoginCode());
                    //登记机构
                    iqpDiscAppPorderSub.setInputBrId(userInfo.getOrg().getCode());
                    //登记日期
                    iqpDiscAppPorderSub.setInputDate(sf.format(new Date()));
                    //创建时间
                    iqpDiscAppPorderSub.setCreateTime(new Date());
                    iqpDiscAppPorderSubMapper.insertSelective(iqpDiscAppPorderSub);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perWhiteInfoList.size();
    }
}
