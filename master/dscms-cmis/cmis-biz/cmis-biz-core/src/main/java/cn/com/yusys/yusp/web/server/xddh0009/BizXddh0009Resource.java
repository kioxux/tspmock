package cn.com.yusys.yusp.web.server.xddh0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0009.req.Xddh0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0009.resp.Xddh0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddh0009.Xddh0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:还款日期卡控
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0009:还款日期卡控")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0009Resource.class);

    @Autowired
    private Xddh0009Service xddh0009Service;

    /**
     * 交易码：xddh0009
     * 交易描述：还款日期卡控
     *
     * @param xddh0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款日期卡控")
    @PostMapping("/xddh0009")
    protected @ResponseBody
    ResultDto<Xddh0009DataRespDto> xddh0009(@Validated @RequestBody Xddh0009DataReqDto xddh0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009DataReqDto));
        Xddh0009DataRespDto xddh0009DataRespDto = new Xddh0009DataRespDto();// 响应Dto:还款日期卡控
        ResultDto<Xddh0009DataRespDto> xddh0009DataResultDto = new ResultDto<>();
        String bill_no = xddh0009DataReqDto.getBill_no();//借据号
        try {
            // 从xddh0009DataReqDto获取业务值进行业务逻辑处理
            xddh0009DataRespDto = xddh0009Service.xddh0009(xddh0009DataReqDto);
            // 封装xddh0009DataResultDto中正确的返回码和返回信息
            xddh0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, e.getMessage());
            // 封装xddh0009DataResultDto中异常返回码和返回信息

            xddh0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0009DataResultDto.setMessage(EpbEnum.EPB099999.value);

        }
        // 封装xddh0009DataRespDto到xddh0009DataResultDto中
        xddh0009DataResultDto.setData(xddh0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009DataResultDto));
        return xddh0009DataResultDto;
    }
}
