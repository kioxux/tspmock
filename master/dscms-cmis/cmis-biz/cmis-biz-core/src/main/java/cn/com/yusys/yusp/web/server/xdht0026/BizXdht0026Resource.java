package cn.com.yusys.yusp.web.server.xdht0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0026.req.Xdht0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0026.resp.Xdht0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0026.Xdht0026Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:根据客户号获取信贷合同信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0026:根据客户号获取信贷合同信息")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0026Resource.class);
    @Resource
    private Xdht0026Service xdht0026Service;
    /**
     * 交易码：xdht0026
     * 交易描述：根据客户号获取信贷合同信息
     *
     * @param xdht0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号获取信贷合同信息")
    @PostMapping("/xdht0026")
    protected @ResponseBody
    ResultDto<Xdht0026DataRespDto> xdht0026(@Validated @RequestBody Xdht0026DataReqDto xdht0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataReqDto));
        Xdht0026DataRespDto xdht0026DataRespDto = new Xdht0026DataRespDto();// 响应Dto:根据zheng获取信贷合同信息
        ResultDto<Xdht0026DataRespDto> xdht0026DataResultDto = new ResultDto<Xdht0026DataRespDto>();
        try {
            // 从xdht0026DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataReqDto));
            xdht0026DataRespDto = xdht0026Service.queryContStatusByCertCode(xdht0026DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataReqDto));
            // 封装xdht0026DataResultDto中正确的返回码和返回信息
            xdht0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, e.getMessage());
            // 封装xdht0026DataResultDto中异常返回码和返回信息
            //  EpbEnum.EPB099999  开始
            xdht0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0026DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EpbEnum.EPB099999   结束
        }
        // 封装xdht0027DataRespDto到xdht0027DataResultDto中
        xdht0026DataResultDto.setData(xdht0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataResultDto));
        return xdht0026DataResultDto;
    }
}
