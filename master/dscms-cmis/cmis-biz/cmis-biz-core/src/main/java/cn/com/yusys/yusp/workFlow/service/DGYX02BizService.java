package cn.com.yusys.yusp.workFlow.service;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpDiscApp;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.IqpDiscAppService;
import cn.com.yusys.yusp.service.TaskUrgentAppClientService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Service
public class DGYX02BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX02BizService.class);//定义log

    @Autowired
    private IqpDiscAppService iqpDiscAppService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private TaskUrgentAppClientService taskUrgentAppClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageCommonService messageCommonService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();

        if(CmisFlowConstants.FLOW_TYPE_TYPE_YX009.equals(bizType)){
            handleYX009Biz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value),resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 贴现贷款申请流程
    private void handleYX009Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        log.info("贴现贷款申请流程" + iqpSerno + "流程操作");
        try {
            String currNodeId = resultInstanceDto.getCurrentNodeId();
            iqpDiscAppService.put2VarParam(resultInstanceDto,iqpSerno);
            IqpDiscApp iqpDiscApp = iqpDiscAppService.selectBySerno(iqpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                if(BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)){
                    //发起节点执行下面的逻辑
                    iqpDiscAppService.handleBusinessDataAfterStart(iqpSerno);
                    if(CmisCommonConstants.GUAR_MODE_21.equals(iqpDiscApp.getGuarMode()) || CmisCommonConstants.GUAR_MODE_40.equals(iqpDiscApp.getGuarMode())){
                        TaskUrgentAppDto taskUrgentAppDto = new TaskUrgentAppDto();
                        taskUrgentAppDto.setBizType(CmisFlowConstants.FLOW_TYPE_TYPE_YX009);
                        taskUrgentAppDto.setCusId(iqpDiscApp.getCusId());
                        taskUrgentAppDto.setCusName(iqpDiscApp.getCusName());
                        taskUrgentAppDto.setSerno(iqpSerno);
                        taskUrgentAppDto.setUrgentType("3");
                        taskUrgentAppDto.setPwbrSerno(resultInstanceDto.getInstanceId());
                        taskUrgentAppDto.setUrgentResn("系统加急");
                        taskUrgentAppDto.setManagerBrId(iqpDiscApp.getManagerBrId());
                        taskUrgentAppDto.setManagerId(iqpDiscApp.getManagerId());
                        taskUrgentAppDto.setInputBrId(iqpDiscApp.getInputBrId());
                        taskUrgentAppDto.setInputId(iqpDiscApp.getInputId());
                        taskUrgentAppDto.setInputDate(DateUtils.getCurrDateStr());
                        taskUrgentAppDto.setUpdDate(DateUtils.getCurrDateStr());
                        ResultDto<Integer> result = taskUrgentAppClientService.createTaskUrgentApp(taskUrgentAppDto);
                        if(!"0".equals(result.getCode()) || 1 != result.getData()){
                            throw BizException.error(null, "999999","新增系统加急记录失败！");
                        }
                    }
                }
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
                iqpDiscAppService.handleBusinessDataAfterEnd(iqpSerno);
                //微信通知
                String managerId = iqpDiscApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", iqpDiscApp.getCusName());
                        paramMap.put("prdName", "贴现协议申请");
                        paramMap.put("result", "通过");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpDiscAppService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
                    iqpDiscAppService.put2VarParam(resultInstanceDto,iqpSerno);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("贴现业务申请"+iqpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpDiscAppService.handleBusinessAfterCallBack(iqpSerno);
                    iqpDiscAppService.put2VarParam(resultInstanceDto,iqpSerno);
                    //微信通知
                    String managerId = iqpDiscApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", iqpDiscApp.getCusName());
                            paramMap.put("prdName", "贴现协议申请");
                            paramMap.put("result", "退回");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("贴现业务申请"+iqpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                    iqpDiscAppService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "拿回初始节点操作，流程参数：" + resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpDiscAppService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("贴现业务申请" + iqpSerno + "否决操作，流程参数：" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
//                iqpDiscApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
//                iqpDiscAppService.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
                iqpDiscAppService.handleBusinessAfterRefuse(iqpSerno);
            } else {
                log.warn("贴现业务申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_DGYX02.equals(flowCode);
    }
}
