/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfOtherHouse
 * @类描述: guar_inf_other_house数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:11
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_other_house")
public class GuarInfOtherHouse extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 现房/期房标识 STD_ZB_HOUSE_STATUS **/
	@Column(name = "READY_OR_PERIOD_HOUSE", unique = false, nullable = true, length = 10)
	private String readyOrPeriodHouse;
	
	/** 预购商品房预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_NO", unique = false, nullable = true, length = 40)
	private String purchseHouseNo;
	
	/** 预购商品房抵押权预告登记证明号 **/
	@Column(name = "PURCHSE_HOUSE_REG_NO", unique = false, nullable = true, length = 40)
	private String purchseHouseRegNo;
	
	/** 预售许可证编号 **/
	@Column(name = "PRESELL_PERMIT_NO", unique = false, nullable = true, length = 40)
	private String presellPermitNo;
	
	/** 预售许可证有效期 **/
	@Column(name = "PRESELL_PERMIT_VAL_DATE", unique = false, nullable = true, length = 10)
	private String presellPermitValDate;
	
	/** 预计交房年月 **/
	@Column(name = "PREDICT_OTHERS_DATE", unique = false, nullable = true, length = 10)
	private String predictOthersDate;
	
	/** 一手/二手标识 STD_ZB_YESBS **/
	@Column(name = "IS_USED", unique = false, nullable = true, length = 10)
	private String isUsed;
	
	/** 是否两证合一 STD_ZB_YES_NO **/
	@Column(name = "TWOCARD2ONE_IND", unique = false, nullable = true, length = 10)
	private String twocard2oneInd;
	
	/** 产权证号 **/
	@Column(name = "HOUSE_LAND_NO", unique = false, nullable = true, length = 40)
	private String houseLandNo;
	
	/** 销售许可证编号 **/
	@Column(name = "MARKET_PERMIT_NO", unique = false, nullable = true, length = 40)
	private String marketPermitNo;
	
	/** 房、地是否均已抵押我行 STD_ZB_YES_NO **/
	@Column(name = "HOUSE_LAND_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseLandPledgeInd;
	
	/** 该产证是否全部抵押 STD_ZB_YES_NO **/
	@Column(name = "HOUSE_ALL_PLEDGE_IND", unique = false, nullable = true, length = 10)
	private String houseAllPledgeInd;
	
	/** 部分抵押描述 **/
	@Column(name = "PART_REG_POSITION_DESC", unique = false, nullable = true, length = 750)
	private String partRegPositionDesc;
	
	/** 房地产买卖合同编号 **/
	@Column(name = "BUSINESS_HOUSE_NO", unique = false, nullable = true, length = 40)
	private String businessHouseNo;
	
	/** 购买日期 **/
	@Column(name = "PURCHASE_DATE", unique = false, nullable = true, length = 10)
	private String purchaseDate;
	
	/** 购买价格（元） **/
	@Column(name = "PURCHASE_ACCNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal purchaseAccnt;
	
	/** 是否本次申请所购房产 STD_ZB_YES_NO **/
	@Column(name = "APPLY_FOR_HOUSE", unique = false, nullable = true, length = 10)
	private String applyForHouse;
	
	/** 抵押住房是否权属人唯一住所 STD_ZB_YES_NO **/
	@Column(name = "HOUSE_OWNERSHIP_IND", unique = false, nullable = true, length = 10)
	private String houseOwnershipInd;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 8)
	private String buildArea;
	
	/** 建成年份 **/
	@Column(name = "ACTIVATE_YEARS", unique = false, nullable = true, length = 10)
	private String activateYears;
	
	/** 房屋产权期限信息 **/
	@Column(name = "HOUSE_PR_DESC", unique = false, nullable = true, length = 3)
	private String housePrDesc;
	
	/** 楼龄 **/
	@Column(name = "FLOOR_AGE", unique = false, nullable = true, length = 3)
	private String floorAge;
	
	/** 朝向 STD_ZB_FWCX **/
	@Column(name = "ORIENTATIONS", unique = false, nullable = true, length = 10)
	private String orientations;
	
	/** 房屋结构 STD_ZB_HOUSE_STRUC **/
	@Column(name = "HOUSE_STRUCTURE", unique = false, nullable = true, length = 10)
	private String houseStructure;
	
	/** 地面构造 STD_ZB_DMWDGZ **/
	@Column(name = "GROUND_STRUCTURE", unique = false, nullable = true, length = 10)
	private String groundStructure;
	
	/** 屋顶构造 STD_ZB_DMWDGZ **/
	@Column(name = "ROOF_STRUCTURE", unique = false, nullable = true, length = 10)
	private String roofStructure;
	
	/** 所在/注册省份 **/
	@Column(name = "PROVINCE_CD", unique = false, nullable = true, length = 10)
	private String provinceCd;
	
	/** 所在/注册市 **/
	@Column(name = "CITY_CD", unique = false, nullable = true, length = 10)
	private String cityCd;
	
	/** 所在县（区） **/
	@Column(name = "COUNTY_CD", unique = false, nullable = true, length = 10)
	private String countyCd;
	
	/** 街道/村镇/路名 **/
	@Column(name = "STREET", unique = false, nullable = true, length = 100)
	private String street;
	
	/** 门牌号/弄号 **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 100)
	private String houseNo;
	
	/** 楼号 **/
	@Column(name = "BUILDING_ROOM_NUM", unique = false, nullable = true, length = 100)
	private String buildingRoomNum;
	
	/** 室号 **/
	@Column(name = "ROOM_NUM", unique = false, nullable = true, length = 100)
	private String roomNum;
	
	/** 产权地址 **/
	@Column(name = "POC_ADDR", unique = false, nullable = true, length = 100)
	private String pocAddr;
	
	/** 楼盘（社区）名称 **/
	@Column(name = "COMMUNITY_NAME", unique = false, nullable = true, length = 100)
	private String communityName;
	
	/** 层次（标的楼层） **/
	@Column(name = "BDLC", unique = false, nullable = true, length = 3)
	private String bdlc;
	
	/** 层数（标的楼高） **/
	@Column(name = "BDGD", unique = false, nullable = true, length = 3)
	private String bdgd;
	
	/** 房地产所在地段情况 STD_ZB_FCDDQK **/
	@Column(name = "HOUSE_PLACE_INFO", unique = false, nullable = true, length = 10)
	private String housePlaceInfo;
	
	/** 建筑物说明 **/
	@Column(name = "BUILD_DESC", unique = false, nullable = true, length = 750)
	private String buildDesc;
	
	/** 是否包含土地 STD_ZB_YES_NO **/
	@Column(name = "FULL_LAND", unique = false, nullable = true, length = 10)
	private String fullLand;
	
	/** 土地证号 **/
	@Column(name = "LAND_NO", unique = false, nullable = true, length = 40)
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	@Column(name = "LAND_USE_QUAL", unique = false, nullable = true, length = 10)
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	@Column(name = "LAND_USE_WAY", unique = false, nullable = true, length = 10)
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	@Column(name = "LAND_USE_BEGIN_DATE", unique = false, nullable = true, length = 10)
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	@Column(name = "LAND_USE_END_DATE", unique = false, nullable = true, length = 10)
	private String landUseEndDate;
	
	/** 土地使用年限 **/
	@Column(name = "LAND_USE_YEARS", unique = false, nullable = true, length = 100)
	private String landUseYears;
	
	/** 土地用途 STD_ZB_LANDYT **/
	@Column(name = "LAND_PURP", unique = false, nullable = true, length = 10)
	private String landPurp;
	
	/** 土地说明 **/
	@Column(name = "LAND_EXPLAIN", unique = false, nullable = true, length = 750)
	private String landExplain;
	
	/** 土地使用权面积 **/
	@Column(name = "LAND_USE_AREA", unique = false, nullable = true, length = 18)
	private String landUseArea;
	
	/** 所属土地使用权人 **/
	@Column(name = "LAND_USE_UNIT", unique = false, nullable = true, length = 100)
	private String landUseUnit;
	
	/** 房产类别 STD_ZB_INV_PTY_TYP **/
	@Column(name = "REALPRO_CLS", unique = false, nullable = true, length = 10)
	private String realproCls;
	
	/** 房屋用途 STD_ZB_HOUSE_USE_TYPE **/
	@Column(name = "HOUSE_USE_TYPE", unique = false, nullable = true, length = 10)
	private String houseUseType;
	
	/** 承租人名称 **/
	@Column(name = "LESSEE_NAME", unique = false, nullable = true, length = 100)
	private String lesseeName;
	
	/** 年租金（元） **/
	@Column(name = "ANNUAL_RENT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal annualRent;
	
	/** 租期（月） **/
	@Column(name = "LEASE", unique = false, nullable = true, length = 5)
	private String lease;
	
	/** 剩余租期（月） **/
	@Column(name = "LEFT_LEASE", unique = false, nullable = true, length = 5)
	private String leftLease;
	
	/** 房产取得方式  STD_ZB_ESTATE_ACQUIRE_WAY **/
	@Column(name = "ESTATE_ACQUIRE_WAY", unique = false, nullable = true, length = 2)
	private String estateAcquireWay;
	
	/** 已使用年限 **/
	@Column(name = "IN_USE_YEAR", unique = false, nullable = true, length = 10)
	private String inUseYear;
	
	/** 所属地段 **/
	@Column(name = "BELONG_AREA", unique = false, nullable = true, length = 2)
	private String belongArea;
	
	/** 墙壁结构 **/
	@Column(name = "WALL_STRUCTURE", unique = false, nullable = true, length = 2)
	private String wallStructure;
	
	/** 物业情况 **/
	@Column(name = "PROPERTY_CASE", unique = false, nullable = true, length = 2)
	private String propertyCase;
	
	/** 房产使用情况 **/
	@Column(name = "HOUSE_PROPERTY", unique = false, nullable = true, length = 2)
	private String houseProperty;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型   **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param readyOrPeriodHouse
	 */
	public void setReadyOrPeriodHouse(String readyOrPeriodHouse) {
		this.readyOrPeriodHouse = readyOrPeriodHouse;
	}
	
    /**
     * @return readyOrPeriodHouse
     */
	public String getReadyOrPeriodHouse() {
		return this.readyOrPeriodHouse;
	}
	
	/**
	 * @param purchseHouseNo
	 */
	public void setPurchseHouseNo(String purchseHouseNo) {
		this.purchseHouseNo = purchseHouseNo;
	}
	
    /**
     * @return purchseHouseNo
     */
	public String getPurchseHouseNo() {
		return this.purchseHouseNo;
	}
	
	/**
	 * @param purchseHouseRegNo
	 */
	public void setPurchseHouseRegNo(String purchseHouseRegNo) {
		this.purchseHouseRegNo = purchseHouseRegNo;
	}
	
    /**
     * @return purchseHouseRegNo
     */
	public String getPurchseHouseRegNo() {
		return this.purchseHouseRegNo;
	}
	
	/**
	 * @param presellPermitNo
	 */
	public void setPresellPermitNo(String presellPermitNo) {
		this.presellPermitNo = presellPermitNo;
	}
	
    /**
     * @return presellPermitNo
     */
	public String getPresellPermitNo() {
		return this.presellPermitNo;
	}
	
	/**
	 * @param presellPermitValDate
	 */
	public void setPresellPermitValDate(String presellPermitValDate) {
		this.presellPermitValDate = presellPermitValDate;
	}
	
    /**
     * @return presellPermitValDate
     */
	public String getPresellPermitValDate() {
		return this.presellPermitValDate;
	}
	
	/**
	 * @param predictOthersDate
	 */
	public void setPredictOthersDate(String predictOthersDate) {
		this.predictOthersDate = predictOthersDate;
	}
	
    /**
     * @return predictOthersDate
     */
	public String getPredictOthersDate() {
		return this.predictOthersDate;
	}
	
	/**
	 * @param isUsed
	 */
	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
    /**
     * @return isUsed
     */
	public String getIsUsed() {
		return this.isUsed;
	}
	
	/**
	 * @param twocard2oneInd
	 */
	public void setTwocard2oneInd(String twocard2oneInd) {
		this.twocard2oneInd = twocard2oneInd;
	}
	
    /**
     * @return twocard2oneInd
     */
	public String getTwocard2oneInd() {
		return this.twocard2oneInd;
	}
	
	/**
	 * @param houseLandNo
	 */
	public void setHouseLandNo(String houseLandNo) {
		this.houseLandNo = houseLandNo;
	}
	
    /**
     * @return houseLandNo
     */
	public String getHouseLandNo() {
		return this.houseLandNo;
	}
	
	/**
	 * @param marketPermitNo
	 */
	public void setMarketPermitNo(String marketPermitNo) {
		this.marketPermitNo = marketPermitNo;
	}
	
    /**
     * @return marketPermitNo
     */
	public String getMarketPermitNo() {
		return this.marketPermitNo;
	}
	
	/**
	 * @param houseLandPledgeInd
	 */
	public void setHouseLandPledgeInd(String houseLandPledgeInd) {
		this.houseLandPledgeInd = houseLandPledgeInd;
	}
	
    /**
     * @return houseLandPledgeInd
     */
	public String getHouseLandPledgeInd() {
		return this.houseLandPledgeInd;
	}
	
	/**
	 * @param houseAllPledgeInd
	 */
	public void setHouseAllPledgeInd(String houseAllPledgeInd) {
		this.houseAllPledgeInd = houseAllPledgeInd;
	}
	
    /**
     * @return houseAllPledgeInd
     */
	public String getHouseAllPledgeInd() {
		return this.houseAllPledgeInd;
	}
	
	/**
	 * @param partRegPositionDesc
	 */
	public void setPartRegPositionDesc(String partRegPositionDesc) {
		this.partRegPositionDesc = partRegPositionDesc;
	}
	
    /**
     * @return partRegPositionDesc
     */
	public String getPartRegPositionDesc() {
		return this.partRegPositionDesc;
	}
	
	/**
	 * @param businessHouseNo
	 */
	public void setBusinessHouseNo(String businessHouseNo) {
		this.businessHouseNo = businessHouseNo;
	}
	
    /**
     * @return businessHouseNo
     */
	public String getBusinessHouseNo() {
		return this.businessHouseNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
    /**
     * @return purchaseDate
     */
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return purchaseAccnt
     */
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param applyForHouse
	 */
	public void setApplyForHouse(String applyForHouse) {
		this.applyForHouse = applyForHouse;
	}
	
    /**
     * @return applyForHouse
     */
	public String getApplyForHouse() {
		return this.applyForHouse;
	}
	
	/**
	 * @param houseOwnershipInd
	 */
	public void setHouseOwnershipInd(String houseOwnershipInd) {
		this.houseOwnershipInd = houseOwnershipInd;
	}
	
    /**
     * @return houseOwnershipInd
     */
	public String getHouseOwnershipInd() {
		return this.houseOwnershipInd;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(String buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public String getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param activateYears
	 */
	public void setActivateYears(String activateYears) {
		this.activateYears = activateYears;
	}
	
    /**
     * @return activateYears
     */
	public String getActivateYears() {
		return this.activateYears;
	}
	
	/**
	 * @param housePrDesc
	 */
	public void setHousePrDesc(String housePrDesc) {
		this.housePrDesc = housePrDesc;
	}
	
    /**
     * @return housePrDesc
     */
	public String getHousePrDesc() {
		return this.housePrDesc;
	}
	
	/**
	 * @param floorAge
	 */
	public void setFloorAge(String floorAge) {
		this.floorAge = floorAge;
	}
	
    /**
     * @return floorAge
     */
	public String getFloorAge() {
		return this.floorAge;
	}
	
	/**
	 * @param orientations
	 */
	public void setOrientations(String orientations) {
		this.orientations = orientations;
	}
	
    /**
     * @return orientations
     */
	public String getOrientations() {
		return this.orientations;
	}
	
	/**
	 * @param houseStructure
	 */
	public void setHouseStructure(String houseStructure) {
		this.houseStructure = houseStructure;
	}
	
    /**
     * @return houseStructure
     */
	public String getHouseStructure() {
		return this.houseStructure;
	}
	
	/**
	 * @param groundStructure
	 */
	public void setGroundStructure(String groundStructure) {
		this.groundStructure = groundStructure;
	}
	
    /**
     * @return groundStructure
     */
	public String getGroundStructure() {
		return this.groundStructure;
	}
	
	/**
	 * @param roofStructure
	 */
	public void setRoofStructure(String roofStructure) {
		this.roofStructure = roofStructure;
	}
	
    /**
     * @return roofStructure
     */
	public String getRoofStructure() {
		return this.roofStructure;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	
    /**
     * @return provinceCd
     */
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	
    /**
     * @return cityCd
     */
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd;
	}
	
    /**
     * @return countyCd
     */
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	
    /**
     * @return street
     */
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param buildingRoomNum
	 */
	public void setBuildingRoomNum(String buildingRoomNum) {
		this.buildingRoomNum = buildingRoomNum;
	}
	
    /**
     * @return buildingRoomNum
     */
	public String getBuildingRoomNum() {
		return this.buildingRoomNum;
	}
	
	/**
	 * @param roomNum
	 */
	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}
	
    /**
     * @return roomNum
     */
	public String getRoomNum() {
		return this.roomNum;
	}
	
	/**
	 * @param pocAddr
	 */
	public void setPocAddr(String pocAddr) {
		this.pocAddr = pocAddr;
	}
	
    /**
     * @return pocAddr
     */
	public String getPocAddr() {
		return this.pocAddr;
	}
	
	/**
	 * @param communityName
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
    /**
     * @return communityName
     */
	public String getCommunityName() {
		return this.communityName;
	}
	
	/**
	 * @param bdlc
	 */
	public void setBdlc(String bdlc) {
		this.bdlc = bdlc;
	}
	
    /**
     * @return bdlc
     */
	public String getBdlc() {
		return this.bdlc;
	}
	
	/**
	 * @param bdgd
	 */
	public void setBdgd(String bdgd) {
		this.bdgd = bdgd;
	}
	
    /**
     * @return bdgd
     */
	public String getBdgd() {
		return this.bdgd;
	}
	
	/**
	 * @param housePlaceInfo
	 */
	public void setHousePlaceInfo(String housePlaceInfo) {
		this.housePlaceInfo = housePlaceInfo;
	}
	
    /**
     * @return housePlaceInfo
     */
	public String getHousePlaceInfo() {
		return this.housePlaceInfo;
	}
	
	/**
	 * @param buildDesc
	 */
	public void setBuildDesc(String buildDesc) {
		this.buildDesc = buildDesc;
	}
	
    /**
     * @return buildDesc
     */
	public String getBuildDesc() {
		return this.buildDesc;
	}
	
	/**
	 * @param fullLand
	 */
	public void setFullLand(String fullLand) {
		this.fullLand = fullLand;
	}
	
    /**
     * @return fullLand
     */
	public String getFullLand() {
		return this.fullLand;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo;
	}
	
    /**
     * @return landNo
     */
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual;
	}
	
    /**
     * @return landUseQual
     */
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay;
	}
	
    /**
     * @return landUseWay
     */
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate;
	}
	
    /**
     * @return landUseBeginDate
     */
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate;
	}
	
    /**
     * @return landUseEndDate
     */
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(String landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return landUseYears
     */
	public String getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp;
	}
	
    /**
     * @return landPurp
     */
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain;
	}
	
    /**
     * @return landExplain
     */
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea;
	}
	
    /**
     * @return landUseArea
     */
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landUseUnit
	 */
	public void setLandUseUnit(String landUseUnit) {
		this.landUseUnit = landUseUnit;
	}
	
    /**
     * @return landUseUnit
     */
	public String getLandUseUnit() {
		return this.landUseUnit;
	}
	
	/**
	 * @param realproCls
	 */
	public void setRealproCls(String realproCls) {
		this.realproCls = realproCls;
	}
	
    /**
     * @return realproCls
     */
	public String getRealproCls() {
		return this.realproCls;
	}
	
	/**
	 * @param houseUseType
	 */
	public void setHouseUseType(String houseUseType) {
		this.houseUseType = houseUseType;
	}
	
    /**
     * @return houseUseType
     */
	public String getHouseUseType() {
		return this.houseUseType;
	}
	
	/**
	 * @param lesseeName
	 */
	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}
	
    /**
     * @return lesseeName
     */
	public String getLesseeName() {
		return this.lesseeName;
	}
	
	/**
	 * @param annualRent
	 */
	public void setAnnualRent(java.math.BigDecimal annualRent) {
		this.annualRent = annualRent;
	}
	
    /**
     * @return annualRent
     */
	public java.math.BigDecimal getAnnualRent() {
		return this.annualRent;
	}
	
	/**
	 * @param lease
	 */
	public void setLease(String lease) {
		this.lease = lease;
	}
	
    /**
     * @return lease
     */
	public String getLease() {
		return this.lease;
	}
	
	/**
	 * @param leftLease
	 */
	public void setLeftLease(String leftLease) {
		this.leftLease = leftLease;
	}
	
    /**
     * @return leftLease
     */
	public String getLeftLease() {
		return this.leftLease;
	}
	
	/**
	 * @param estateAcquireWay
	 */
	public void setEstateAcquireWay(String estateAcquireWay) {
		this.estateAcquireWay = estateAcquireWay;
	}
	
    /**
     * @return estateAcquireWay
     */
	public String getEstateAcquireWay() {
		return this.estateAcquireWay;
	}
	
	/**
	 * @param inUseYear
	 */
	public void setInUseYear(String inUseYear) {
		this.inUseYear = inUseYear;
	}
	
    /**
     * @return inUseYear
     */
	public String getInUseYear() {
		return this.inUseYear;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea;
	}
	
    /**
     * @return belongArea
     */
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param wallStructure
	 */
	public void setWallStructure(String wallStructure) {
		this.wallStructure = wallStructure;
	}
	
    /**
     * @return wallStructure
     */
	public String getWallStructure() {
		return this.wallStructure;
	}
	
	/**
	 * @param propertyCase
	 */
	public void setPropertyCase(String propertyCase) {
		this.propertyCase = propertyCase;
	}
	
    /**
     * @return propertyCase
     */
	public String getPropertyCase() {
		return this.propertyCase;
	}
	
	/**
	 * @param houseProperty
	 */
	public void setHouseProperty(String houseProperty) {
		this.houseProperty = houseProperty;
	}
	
    /**
     * @return houseProperty
     */
	public String getHouseProperty() {
		return this.houseProperty;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}