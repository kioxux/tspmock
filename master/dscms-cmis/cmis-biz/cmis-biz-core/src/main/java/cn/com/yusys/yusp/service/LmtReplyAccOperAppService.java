package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtReplyAccOperApp;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSub;
import cn.com.yusys.yusp.domain.LmtReplyAccOperAppSubPrd;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.req.CmisLmt0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0006.resp.CmisLmt0006RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.req.CmisLmt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0007.resp.CmisLmt0007RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccOperAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccOperAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-10 11:23:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccOperAppService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyAccOperAppService.class);

    @Autowired
    private LmtReplyAccOperAppMapper lmtReplyAccOperAppMapper;

    @Autowired
    private LmtReplyAccOperAppSubService lmtReplyAccOperAppSubService;

    @Autowired
    private LmtReplyAccOperAppSubPrdService lmtReplyAccOperAppSubPrdService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    // 单一客户批复台账服务
    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAccOperApp selectByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtReplyAccOperApp> selectAll(QueryModel model) {
        List<LmtReplyAccOperApp> records = (List<LmtReplyAccOperApp>) lmtReplyAccOperAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplyAccOperApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccOperApp> list = lmtReplyAccOperAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtReplyAccOperApp record) {
        record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        return lmtReplyAccOperAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtReplyAccOperApp record) {
        return lmtReplyAccOperAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtReplyAccOperApp record) {
        return lmtReplyAccOperAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtReplyAccOperApp record) {
        return lmtReplyAccOperAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccOperAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtReplyAccOperAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: guideSave
     * @方法描述: 单一客户额度冻结/解冻/终止向导申请新增保存
     * @参数与返回说明:
     * @算法描述: 1.根据客户号查询额度系统的客户额度查询接口
     * 2.根据返回结果保存数据到授信额度冻结/解冻/终止相关表
     * 2.1 加工批复台账操作表数据并插入数据库
     * 2.2 把批复台账操作表的数据插入授信台账额度操作分项表
     * 2.2.1 额度系统的客户额度查询接口查询出父节点并判断是否为null，为null则插入到授信台账额度操作分项表，不为null则插入到插入授信台账额度操作分项适用品种明细表
     * 2.2.1通过客户号查询批复台账操作表并将查询出来的数据插入授信台账额度操作分项表
     * 2.3 把批复台账操作表的数据插入授信台账额度操作分项适用品种明细表
     * 2.3.1
     * @创建人: yangwl
     * @创建时间: 2021-05-13 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */

    public String guideSave(LmtReplyAccOperApp lmtReplyAccOperApp) throws Exception {
        CmisLmt0015ReqDto cmisLmt0015ReqDto = new CmisLmt0015ReqDto();
        cmisLmt0015ReqDto.setSerno(UUID.randomUUID().toString());
        cmisLmt0015ReqDto.setInstuCde("C1115632000023");
        cmisLmt0015ReqDto.setQueryType(CmisCommonConstants.QUERY_TYPE_01);
        cmisLmt0015ReqDto.setCusId(lmtReplyAccOperApp.getCusId());
        cmisLmt0015ReqDto.setStartNum(0);
        cmisLmt0015ReqDto.setPageCount(10000);
        //根据客户号查询额度系统的客户额度查询接口
        ResultDto<CmisLmt0015RespDto> cmisLmt0015RespDtoResultDto = cmisLmtClientService.cmisLmt0015(cmisLmt0015ReqDto);
        if (cmisLmt0015RespDtoResultDto != null && cmisLmt0015RespDtoResultDto.getData() != null && "0000".equals(cmisLmt0015RespDtoResultDto.getData().getErrorCode())) {
            log.info("根据客户号查询额度系统的客户额度查询接口成功");
        } else {
            throw new Exception("根据客户号查询额度系统的客户额度查询接口异常");
        }
        List<CmisLmt0015LmtAccListRespDto> lmtAccList = cmisLmt0015RespDtoResultDto.getData().getLmtAccList();
        if (lmtAccList != null && lmtAccList.size() == 1) {
            //根据客户号查询额度系统的客户额度分项列表
            List<CmisLmt0015LmtSubAccListRespDto> lmtAccSubList = cmisLmt0015RespDtoResultDto.getData().getLmtSubAccList();
            if (lmtAccSubList != null && lmtAccSubList.size() != 0) {
                log.info("根据客户号查询额度系统的客户额度额度分项成功");
            } else {
                throw new Exception("根据客户号查询额度系统的客户额度分项异常");
            }
            // 获取调额度系统查出的一条数据
            CmisLmt0015LmtAccListRespDto cmisLmt0015LmtAccListRespDto = lmtAccList.get(0);
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            // 赋值
            lmtReplyAccOperApp.setPkId(UUID.randomUUID().toString());
            lmtReplyAccOperApp.setSerno(serno);
            lmtReplyAccOperApp.setLmtAccNo(lmtAccSubList.get(0).getAccNo());
            lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            lmtReplyAccOperApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw new Exception("当前登录用户为空或不存在");
            } else {
                lmtReplyAccOperApp.setInputId(userInfo.getLoginCode());
                lmtReplyAccOperApp.setInputBrId(userInfo.getOrg().getCode());
                lmtReplyAccOperApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                lmtReplyAccOperApp.setUpdId(userInfo.getLoginCode());
                lmtReplyAccOperApp.setUpdBrId(userInfo.getOrg().getCode());
                lmtReplyAccOperApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                lmtReplyAccOperApp.setManagerId(userInfo.getLoginCode());
                lmtReplyAccOperApp.setManagerBrId(userInfo.getOrg().getCode());
                lmtReplyAccOperApp.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                lmtReplyAccOperApp.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                //批复台账操作表数据并插入数据库
                int result = lmtReplyAccOperAppMapper.insert(lmtReplyAccOperApp);
                log.info("根据额度系统新增一条额度批复台账操作数据成功!");
                if (result < 0) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",批复台账操作表数据插入数据库失败！");
                }
                // 开始处理分项层数据
                log.info("-----------处理分项层数据----start----!");
                for (CmisLmt0015LmtSubAccListRespDto cmisLmt0015LmtSubAccListRespDto : lmtAccSubList) {
                    // 获取父节点
                    String parentId = cmisLmt0015LmtSubAccListRespDto.getParentId();
                    //授信模式 01--综合授信
                    String lmtMode = cmisLmt0015LmtSubAccListRespDto.getLmtMode();
                    // 分项层数据
                    LmtReplyAccOperAppSub lmtReplyAccOperAppSub = new LmtReplyAccOperAppSub();
                    if (StringUtils.isBlank(parentId) && CmisLmtConstants.STD_ZB_LMT_MODE_01.equals(lmtMode)) {
                        lmtReplyAccOperAppSub.setPkId(UUID.randomUUID().toString());
                        lmtReplyAccOperAppSub.setSerno(lmtReplyAccOperApp.getSerno());
                        lmtReplyAccOperAppSub.setSubSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>()));
                        // 授信品种编号
                        lmtReplyAccOperAppSub.setAccSubNo(cmisLmt0015LmtSubAccListRespDto.getSubSerno());
                        // 授信品种名称
                        lmtReplyAccOperAppSub.setAccSubName(cmisLmt0015LmtSubAccListRespDto.getLimitSubName());
                        lmtReplyAccOperAppSub.setLmtAccNo(lmtReplyAccOperApp.getLmtAccNo());
                        lmtReplyAccOperAppSub.setIsPreLmt(cmisLmt0015LmtSubAccListRespDto.getIsPreCrd());
                        lmtReplyAccOperAppSub.setGuarMode(cmisLmt0015LmtSubAccListRespDto.getSuitGuarWay());
                        lmtReplyAccOperAppSub.setLmtAmt(cmisLmt0015LmtSubAccListRespDto.getAvlAmt());
                        lmtReplyAccOperAppSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtReplyAccOperAppSub.setInputId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSub.setInputBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSub.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSub.setUpdId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSub.setUpdBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSub.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSubService.insertSelective(lmtReplyAccOperAppSub);
                        log.info("-----------新增一条分项层数据--------分项流水号:"+lmtReplyAccOperAppSub.getSubSerno());
                    }
                }
                log.info("-----------处理分项层数据----end----!");
                log.info("-----------处理分项品种层数据----start----!");
                for (CmisLmt0015LmtSubAccListRespDto cmisLmt0015LmtSubAccListRespDto : lmtAccSubList) {
                    // 获取父节点
                    String parentId = cmisLmt0015LmtSubAccListRespDto.getParentId();
                    //授信模式 01--综合授信
                    String lmtMode = cmisLmt0015LmtSubAccListRespDto.getLmtMode();
                    // 分项品种层数据
                    LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd = new LmtReplyAccOperAppSubPrd();

                    if (!StringUtils.isBlank(parentId) && CmisLmtConstants.STD_ZB_LMT_MODE_01.equals(lmtMode)) {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("serno", serno);
                        map.put("accSubNo", parentId);
                        List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubs = lmtReplyAccOperAppSubService.selectByParams(map);
                        LmtReplyAccOperAppSub lmtReplyAccOperAppSub = lmtReplyAccOperAppSubs.get(0);
                        String subSerno = lmtReplyAccOperAppSub.getSubSerno();

                        lmtReplyAccOperAppSubPrd.setPkId(UUID.randomUUID().toString());
                        lmtReplyAccOperAppSubPrd.setSubSerno(subSerno);
                        lmtReplyAccOperAppSubPrd.setSubPrdSerno(sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>()));
                        lmtReplyAccOperAppSubPrd.setAccSubNo(cmisLmt0015LmtSubAccListRespDto.getParentId());
                        lmtReplyAccOperAppSubPrd.setAccSubPrdNo(cmisLmt0015LmtSubAccListRespDto.getSubSerno());
                        lmtReplyAccOperAppSubPrd.setCusId(cmisLmt0015LmtAccListRespDto.getCusId());
                        lmtReplyAccOperAppSubPrd.setCusName(cmisLmt0015LmtAccListRespDto.getCusName());
                        lmtReplyAccOperAppSubPrd.setLmtBizType(cmisLmt0015LmtSubAccListRespDto.getLimitSubNo());
                        lmtReplyAccOperAppSubPrd.setLmtBizTypeName(cmisLmt0015LmtSubAccListRespDto.getLimitSubName());
                        lmtReplyAccOperAppSubPrd.setIsRevolvLimit(cmisLmt0015LmtSubAccListRespDto.getIsRevolv());
                        lmtReplyAccOperAppSubPrd.setGuarMode(cmisLmt0015LmtSubAccListRespDto.getSuitGuarWay());
                        lmtReplyAccOperAppSubPrd.setCurType(cmisLmt0015LmtSubAccListRespDto.getCny());
                        lmtReplyAccOperAppSubPrd.setLmtAmt(cmisLmt0015LmtSubAccListRespDto.getAvlAmt());
                        lmtReplyAccOperAppSubPrd.setStartDate(cmisLmt0015LmtSubAccListRespDto.getStartDate());
                        lmtReplyAccOperAppSubPrd.setEndDate(cmisLmt0015LmtSubAccListRespDto.getEndDate());
                        lmtReplyAccOperAppSubPrd.setLmtTerm(cmisLmt0015LmtSubAccListRespDto.getTerm());
                        lmtReplyAccOperAppSubPrd.setOrigiLmtStatus(cmisLmt0015LmtSubAccListRespDto.getStatus());
                        // 修改后额度状态
                        lmtReplyAccOperAppSubPrd.setAfterLmtStatus(lmtReplyAccOperApp.getLmtAccOperType());
                        lmtReplyAccOperAppSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                        lmtReplyAccOperAppSubPrd.setInputId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSubPrd.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSubPrd.setUpdId(userInfo.getLoginCode());
                        lmtReplyAccOperAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                        lmtReplyAccOperAppSubPrd.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                        lmtReplyAccOperAppSubPrd.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSubPrd.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), "yyyy-MM-dd"));
                        lmtReplyAccOperAppSubPrdService.insert(lmtReplyAccOperAppSubPrd);
                        log.info("-----------新增一条分项品种层数据--------分项品种流水号:"+lmtReplyAccOperAppSubPrd.getSubPrdSerno());
                    }
                }
                log.info("-----------处理分项品种层数据----end----!");
            }
        } else {
            throw new Exception("客户额度台账查询异常，查询为空或多条");
        }
        return lmtReplyAccOperApp.getSerno();
    }


    /**
     * @方法名称: queryLmtReplyAccOperAppByParams
     * @方法描述: 通过条件查询数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyAccOperApp> queryLmtReplyAccOperAppByParams(HashMap<String, String> queryMap) {
        return lmtReplyAccOperAppMapper.queryLmtReplyAccOperAppByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtReplyAccOperAppBySerno
     * @方法描述: 通过申请流水号查询申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-04 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAccOperApp queryLmtReplyAccOperAppBySerno(String serno) throws Exception {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("serno", serno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyAccOperApp> lmtReplyAccOperAppList = queryLmtReplyAccOperAppByParams(queryMap);
        if (lmtReplyAccOperAppList != null && lmtReplyAccOperAppList.size() == 1) {
            return lmtReplyAccOperAppList.get(0);
        } else {
            throw new Exception("查询授信台账操作申请表异常");
        }
    }

    /**
     * @方法名称: handleBusinessAfterStart
     * @方法描述: 授信冻结解冻终止流程发起逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为111 审批中
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterStart(String serno) throws Exception {
        LmtReplyAccOperApp lmtReplyAccOperApp = queryLmtReplyAccOperAppBySerno(serno);
        lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        update(lmtReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterRefuse
     * @方法描述: 授信冻结解冻终止流程拒绝逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为998 否决
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterRefuse(String serno) throws Exception {
        LmtReplyAccOperApp lmtReplyAccOperApp = queryLmtReplyAccOperAppBySerno(serno);
        lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        update(lmtReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterBack
     * @方法描述: 授信冻结解冻终止流程打回逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为992 打回
     * @创建人: mashun
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterBack(String serno) throws Exception {
        LmtReplyAccOperApp lmtReplyAccOperApp = queryLmtReplyAccOperAppBySerno(serno);
        lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        update(lmtReplyAccOperApp);
    }

    /**
     * @方法名称: handleBusinessAfterEnd
     * @方法描述: 授信冻结解冻终止流程通过逻辑处理
     * @参数与返回说明:
     * @算法描述: 1.将审批状态更新为997 通过
     * 2.根据冻结解冻状态更新台账中的状态
     * 5.推送批复台账至额度系统
     * @创建人: yangwl
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessAfterEnd(String serno, String currentUserId, String currentOrgId) throws Exception {
        // 将审批状态更新为997
        LmtReplyAccOperApp lmtReplyAccOperApp = queryLmtReplyAccOperAppBySerno(serno);
        lmtReplyAccOperApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        update(lmtReplyAccOperApp);
        User userInfo = SessionUtils.getUserInformation();
        // 通过授信台账操作流水号查询授信台账分项适用品种数据
        lmtReplyAccSubPrdService.updateLmtReplyAccSubPrdBySerno(serno);
        List<LmtReplyAccOperAppSub> lmtReplyAccOperAppSubs = lmtReplyAccOperAppSubService.queryBySerno(serno);
        List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrds = lmtReplyAccOperAppSubPrdService.queryLmtReplyAccOperAppSubPrdBySerno(serno);
        //冻结
        if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_01).equals(lmtReplyAccOperApp.getLmtAccOperType())) {
            for (LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrds) {
                if (StringUtils.isNotEmpty(lmtReplyAccOperAppSubPrd.getAfterLmtStatus())&& CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_01.equals(lmtReplyAccOperAppSubPrd.getAfterLmtStatus())) {
                    CmisLmt0006ReqDto cmisLmt0006ReqDto = new CmisLmt0006ReqDto();
                    // 组装额度系统报文
                    cmisLmt0006ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
                    cmisLmt0006ReqDto.setInputId(lmtReplyAccOperApp.getInputId());
                    cmisLmt0006ReqDto.setInputBrId(lmtReplyAccOperApp.getInputBrId());
                    cmisLmt0006ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    cmisLmt0006ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);

                    List<CmisLmt0006ApprListReqDto> cmisLmt0006ApprListReqDtos = new ArrayList<CmisLmt0006ApprListReqDto>();
                    CmisLmt0006ApprListReqDto cmisLmt0006ApprListReqDto = new CmisLmt0006ApprListReqDto();
                    cmisLmt0006ApprListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0006ApprListReqDto.setOptType(CmisCommonConstants.LMT_COOP_CTR_OPT_TYPE_01);
                    cmisLmt0006ApprListReqDto.setApprSerno(lmtReplyAccOperApp.getLmtAccNo());
                    cmisLmt0006ApprListReqDtos.add(cmisLmt0006ApprListReqDto);
                    cmisLmt0006ReqDto.setApprList(cmisLmt0006ApprListReqDtos);

                    List<CmisLmt0006ApprSubListReqDto> cmisLmt0006ApprSubListReqDtos = new ArrayList<CmisLmt0006ApprSubListReqDto>();
                    CmisLmt0006ApprSubListReqDto cmisLmt0006ApprSubListReqDto = new CmisLmt0006ApprSubListReqDto();
                    cmisLmt0006ApprSubListReqDto.setApprSubSerno(lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                    cmisLmt0006ApprSubListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0006ApprSubListReqDtos.add(cmisLmt0006ApprSubListReqDto);
                    cmisLmt0006ReqDto.setApprSubList(cmisLmt0006ApprSubListReqDtos);

                    ResultDto<CmisLmt0006RespDto> cmisLmt0006RespDtoResultDto = cmisLmtClientService.cmisLmt0006(cmisLmt0006ReqDto);
                    log.info("额度系统返回：{}" + cmisLmt0006RespDtoResultDto.toString());
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0006RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0006RespDtoResultDto.getCode(), cmisLmt0006RespDtoResultDto.getMessage());
                    }
                }
            }
        } else if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_02).equals(lmtReplyAccOperApp.getLmtAccOperType())) {
            for (LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrds) {
                if (StringUtils.isNotEmpty(lmtReplyAccOperAppSubPrd.getAfterLmtStatus()) && CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_02.equals(lmtReplyAccOperAppSubPrd.getAfterLmtStatus())) {
                    CmisLmt0007ReqDto cmisLmt0007ReqDto = new CmisLmt0007ReqDto();
                    // 组装额度系统报文
                    cmisLmt0007ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
                    cmisLmt0007ReqDto.setInputId(lmtReplyAccOperApp.getInputId());
                    cmisLmt0007ReqDto.setInputBrId(lmtReplyAccOperApp.getInputBrId());
                    cmisLmt0007ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    cmisLmt0007ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);

                    List<CmisLmt0007ApprListReqDto> cmisLmt0007ApprListReqDtos = new ArrayList<CmisLmt0007ApprListReqDto>();
                    CmisLmt0007ApprListReqDto cmisLmt0007ApprListReqDto = new CmisLmt0007ApprListReqDto();
                    cmisLmt0007ApprListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0007ApprListReqDto.setOptType(CmisCommonConstants.LMT_COOP_CTR_OPT_TYPE_01);
                    cmisLmt0007ApprListReqDto.setApprSerno(lmtReplyAccOperApp.getLmtAccNo());
                    cmisLmt0007ApprListReqDtos.add(cmisLmt0007ApprListReqDto);
                    cmisLmt0007ReqDto.setApprList(cmisLmt0007ApprListReqDtos);

                    List<CmisLmt0007ApprSubListReqDto> cmisLmt0007ApprSubListReqDtos = new ArrayList<CmisLmt0007ApprSubListReqDto>();
                    CmisLmt0007ApprSubListReqDto cmisLmt0007ApprSubListReqDto = new CmisLmt0007ApprSubListReqDto();
                    cmisLmt0007ApprSubListReqDto.setApprSubSerno(lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                    cmisLmt0007ApprSubListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0007ApprSubListReqDtos.add(cmisLmt0007ApprSubListReqDto);
                    cmisLmt0007ReqDto.setApprSubList(cmisLmt0007ApprSubListReqDtos);

                    ResultDto<CmisLmt0007RespDto> cmisLmt0007RespDtoResultDto = cmisLmtClientService.cmisLmt0007(cmisLmt0007ReqDto);
                    log.info("额度系统返回：{}" + cmisLmt0007RespDtoResultDto.toString());
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0007RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0007RespDtoResultDto.getCode(), cmisLmt0007RespDtoResultDto.getMessage());
                    }
                }
            }
        } else if ((CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_03).equals(lmtReplyAccOperApp.getLmtAccOperType())) {
            for (LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrds) {
                if (StringUtils.isNotEmpty(lmtReplyAccOperAppSubPrd.getAfterLmtStatus()) && CmisCommonConstants.STD_ZB_LMT_ACC_OPER_TYPE_03.equals(lmtReplyAccOperAppSubPrd.getAfterLmtStatus())) {
                    CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
                    // 组装额度系统报文
                    cmisLmt0008ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
                    cmisLmt0008ReqDto.setInputId(lmtReplyAccOperApp.getInputId());
                    cmisLmt0008ReqDto.setInputBrId(lmtReplyAccOperApp.getInputBrId());
                    cmisLmt0008ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    cmisLmt0008ReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);

                    List<CmisLmt0008ApprListReqDto> cmisLmt0008ApprListReqDtos = new ArrayList<CmisLmt0008ApprListReqDto>();
                    CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
                    cmisLmt0008ApprListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0008ApprListReqDto.setOptType(CmisCommonConstants.LMT_COOP_CTR_OPT_TYPE_01);
                    cmisLmt0008ApprListReqDto.setApprSerno(lmtReplyAccOperApp.getLmtAccNo());
                    cmisLmt0008ApprListReqDtos.add(cmisLmt0008ApprListReqDto);
                    cmisLmt0008ReqDto.setApprList(cmisLmt0008ApprListReqDtos);

                    List<CmisLmt0008ApprSubListReqDto> cmisLmt0008ApprSubListReqDtos = new ArrayList<CmisLmt0008ApprSubListReqDto>();
                    CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
                    cmisLmt0008ApprSubListReqDto.setApprSubSerno(lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                    cmisLmt0008ApprSubListReqDto.setCusId(lmtReplyAccOperApp.getCusId());
                    cmisLmt0008ApprSubListReqDtos.add(cmisLmt0008ApprSubListReqDto);
                    cmisLmt0008ReqDto.setApprSubList(cmisLmt0008ApprSubListReqDtos);

                    ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDtoResultDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
                    log.info("额度系统返回：{}" + cmisLmt0008RespDtoResultDto.toString());
                    if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cmisLmt0008RespDtoResultDto.getCode())) {
                        throw BizException.error(null, cmisLmt0008RespDtoResultDto.getCode(), cmisLmt0008RespDtoResultDto.getMessage());
                    }
                }
            }
        }
    }

    /**
     * @方法名称: queryAll
     * @方法描述: 授信台账额度查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtReplyAccOperApp> queryAll(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_000);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_111);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_992);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyAccOperApp> records = (List<LmtReplyAccOperApp>) lmtReplyAccOperAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return records;
    }

    /**
     * @方法名称: queryHis
     * @方法描述: 授信台账额度历史查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtReplyAccOperApp> queryHis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<String> apprStatuss = new ArrayList<>();
        apprStatuss.add(CmisCommonConstants.WF_STATUS_996);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_997);
        apprStatuss.add(CmisCommonConstants.WF_STATUS_998);
        model.getCondition().put("apprStatuss", apprStatuss);
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyAccOperApp> records = (List<LmtReplyAccOperApp>) lmtReplyAccOperAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return records;
    }

    /**
     * @方法名称: updateoperapp
     * @方法描述: 客户额度冻结/解冻/终止申请暂存
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateoperapp(LmtReplyAccOperApp record) {
        User userInfo = SessionUtils.getUserInformation();
        record.setUpdId(userInfo.getLoginCode());
        record.setUpdBrId(userInfo.getOrg().getCode());
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        return lmtReplyAccOperAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @函数名称:selectlmtreplyaccoperappbyserno
     * @函数描述:客户额度冻结/解冻/终止申请根据流水号查询
     * @创建者: yangwl
     * @创建时间: 2021/5/21
     * @参数与返回说明:
     * @算法描述:
     */
    public LmtReplyAccOperApp selectlmtreplyaccoperappbyserno(String serno) {
        return lmtReplyAccOperAppMapper.selectlmtreplyaccoperappbyserno(serno);
    }

    /**
     * @函数名称:selectlmtreplyaccoperappbyserno
     * @函数描述:查询客户额度冻结/解冻/终止申请是否已存在
     * @创建者: yangwl
     * @创建时间: 2021/6/24
     * @参数与返回说明:
     * @算法描述:
     */

    public Map selectLmtReplyAccOperApp(LmtReplyAccOperApp lmtReplyAccOperApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String serno = "";
        try {
            lmtReplyAccOperApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtReplyAccOperApp> lmtReplyAccOperApps = lmtReplyAccOperAppMapper.selectLmtReplyAccOperApp(lmtReplyAccOperApp);
            if(CollectionUtils.nonEmpty(lmtReplyAccOperApps)) {
                for(LmtReplyAccOperApp app : lmtReplyAccOperApps) {
                    if ("01".equals(app.getLmtAccOperType())) {
                        rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_FRO_EXISTS_EXCEPTION.key;
                        rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_FRO_EXISTS_EXCEPTION.value;
                        break;
                    } else if ("02".equals(app.getLmtAccOperType())) {
                        rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_THAW_EXISTS_EXCEPTION.key;
                        rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_THAW_EXISTS_EXCEPTION.value;
                        break;
                    } else if ("03".equals(app.getLmtAccOperType())) {
                        rtnCode = EcbEnum.LMT_RE_ACC_OPER_APP_TERM_EXISTS_EXCEPTION.key;
                        rtnMsg = EcbEnum.LMT_RE_ACC_OPER_APP_TERM_EXISTS_EXCEPTION.value;
                        break;
                    }
                }
            }else {
                serno = this.guideSave(lmtReplyAccOperApp);
            }

        }catch (Exception e) {
            log.error("查询额度冻结解冻异常！", e.getMessage());
            rtnCode = "9999";
            rtnMsg = e.getMessage();

        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
            result.put("serno", serno);
        }
        return result;
    }

}
