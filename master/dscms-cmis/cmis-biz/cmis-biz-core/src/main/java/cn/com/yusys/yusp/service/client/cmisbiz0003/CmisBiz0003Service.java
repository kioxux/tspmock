package cn.com.yusys.yusp.service.client.cmisbiz0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CurrentMonthAllowLoanBlanceDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CmisBiz0003Service {

    private static final Logger logger = LoggerFactory.getLogger(CmisBiz0003Service.class);

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private ICmisCfgClientService cmisCfgClientService;

    public CmisBiz0003RespDto execute(CmisBiz0003ReqDto reqDto) {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value);

        CmisBiz0003RespDto respDto = new CmisBiz0003RespDto();

        Map<String, CurrentMonthAllowLoanBlanceDto> currentMonthAllowLoanBlanceDtoMap = new HashMap<>();

        BigDecimal totalCurrMonthAllowLoanBalance = BigDecimal.ZERO;
        try {
            List<String> orgIdList = reqDto.getOrgIdList();
            if (CollectionUtils.isEmpty(orgIdList)) {
                respDto.setErrorCode(EpbEnum.EPB090004.key);
                respDto.setErrorMsg("机构Id不能为空！");
                return respDto;
            }

            //零售产品类型
            ResultDto<List<CfgPrdBasicinfoDto>> listResultDto = cmisCfgClientService.queryCfgPrdBasicInfoByPrdType("09,10,11,12");
            logger.info("CmisBiz0003Service  获取零售业务条线产品Id=====>【{}】",JSONObject.toJSON(listResultDto));
            List<CfgPrdBasicinfoDto> data = listResultDto.getData();
            if (data == null){
                respDto.setErrorCode(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                respDto.setErrorMsg("零售条线产品类型获取失败！");
                return respDto;
            }
            List<String> linshouPrdIds = new ArrayList<>();
            for (Object datum : data) {
                Map<String,Object> map = (Map<String, Object>) datum;
                String prdId = map.get("prdId").toString();
                linshouPrdIds.add(prdId);
            }
            logger.info("CmisBiz0003Service  获取零售业务条线产品Id=====>【{}】", JSONObject.toJSON(listResultDto));

            Integer selectType = reqDto.getSelectType();
            //业务条线限额获取（业务条线获取限额 不需要获取下属机构）
            if ("1".equals(String.valueOf(selectType)) ){
                BigDecimal currMonthAllowLoanBalance = accLoanService.getCurrMonthAllowLoanBalance(orgIdList, reqDto.getBelgLine(),linshouPrdIds);
                respDto.setTotalCurrMonthAllowLoanBalance(currMonthAllowLoanBalance);
                respDto.setErrorCode(SuccessEnum.SUCCESS.key);
                respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                return respDto;
            }
            //机构限额获取
            for (String orgId : orgIdList) {
                CurrentMonthAllowLoanBlanceDto currentMonthAllowLoanBlanceDto = new CurrentMonthAllowLoanBlanceDto();
                currentMonthAllowLoanBlanceDto.setOrgId(orgId);
                BigDecimal currMonthAllowLoanBalance = BigDecimal.ZERO;
                AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(orgId);
                //未获取到当前机构信息
                if (adminSmOrgDto == null) {
                    currentMonthAllowLoanBlanceDto.setErrorCode(EpbEnum.EPB090004.key);
                    currentMonthAllowLoanBlanceDto.setErrorMsg(EpbEnum.EPB090004.value);
                    currentMonthAllowLoanBlanceDto.setCurrentMonthAllowLoanBlance(currMonthAllowLoanBalance);
                    currentMonthAllowLoanBlanceDtoMap.put(orgId, currentMonthAllowLoanBlanceDto);
                    continue;
                }
                //机构类型
                String orgType = adminSmOrgDto.getOrgType();
                boolean flag = !"4".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) && !"5".equals(orgType);
                if (flag){
                    currentMonthAllowLoanBlanceDto.setErrorCode(EpbEnum.EPB090004.key);
                    currentMonthAllowLoanBlanceDto.setErrorMsg(EpbEnum.EPB090004.value);
                    currentMonthAllowLoanBlanceDtoMap.put(orgId, currentMonthAllowLoanBlanceDto);
                    continue;
                }
                //机构层级为4 且 (4-中心支行)（2-异地支行（无分行）） 5-综合支行
                List<String> lowerOrgId = commonService.getLowerOrgId(orgId);
                //未查到下属机构
                if (CollectionUtils.isEmpty(lowerOrgId)){
                    currentMonthAllowLoanBlanceDto.setErrorCode(EpbEnum.EPB090004.key);
                    currentMonthAllowLoanBlanceDto.setErrorMsg("未查到到下属机构");
                    currentMonthAllowLoanBlanceDtoMap.put(orgId, currentMonthAllowLoanBlanceDto);
                    continue;
                }
                currMonthAllowLoanBalance = accLoanService.getCurrMonthAllowLoanBalance(lowerOrgId, reqDto.getBelgLine(),linshouPrdIds);

                //余额获取失败
                if (currMonthAllowLoanBalance == null){
                    currentMonthAllowLoanBlanceDto.setCurrentMonthAllowLoanBlance(BigDecimal.ZERO);
                    currentMonthAllowLoanBlanceDto.setErrorCode(EpbEnum.EPB090004.key);
                    String belgText = getBelgText(reqDto.getBelgLine());
                    currentMonthAllowLoanBlanceDto.setErrorMsg("当前{1}人民币贷款余额获取失败！".replace("{1}", belgText));
                    currentMonthAllowLoanBlanceDtoMap.put(orgId, currentMonthAllowLoanBlanceDto);
                    continue;
                }

                totalCurrMonthAllowLoanBalance.add(currMonthAllowLoanBalance);
                currentMonthAllowLoanBlanceDto.setCurrentMonthAllowLoanBlance(currMonthAllowLoanBalance);
                currentMonthAllowLoanBlanceDto.setErrorCode(SuccessEnum.SUCCESS.key);
                currentMonthAllowLoanBlanceDto.setErrorMsg(SuccessEnum.SUCCESS.value);
                currentMonthAllowLoanBlanceDtoMap.put(orgId, currentMonthAllowLoanBlanceDto);
            }
            respDto.setTotalCurrMonthAllowLoanBalance(totalCurrMonthAllowLoanBalance);
            respDto.setCurrMonthAllowLoanBalanceMap(currentMonthAllowLoanBlanceDtoMap);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(DscmsEnum.TRADE_CODE_CMISBIZ0003.value, e);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISBIZ0003.key, DscmsEnum.TRADE_CODE_CMISBIZ0003.value);
        return respDto;
    }

    /**
     * 03	对公业务条线
     * 01	小微金融条线
     * 02	零售条线
     * 05	网络金融条线
     * 06	资金业务条线
     */
    private String getBelgText(String belgLine) {
        String belgText = "";
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_03.equals(belgLine)) {
            belgText = "对公";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_02.equals(belgLine)) {
            belgText = "零售";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_01.equals(belgLine)) {
            belgText = "小微金融";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_05.equals(belgLine)) {
            belgText = "网络金融";
        }
        if (CmisBizConstants.STD_ZB_SUIT_BIZ_LINE_06.equals(belgLine)) {
            belgText = "资金";
        }
        return belgText;
    }
}
