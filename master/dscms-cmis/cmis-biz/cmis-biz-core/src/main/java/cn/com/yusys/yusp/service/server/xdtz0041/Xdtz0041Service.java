package cn.com.yusys.yusp.service.server.xdtz0041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Xdtz0041Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdtz0041Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param cusId
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0041DataRespDto getHouseLoanByCusId(String cusId) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, cusId);
        Xdtz0041DataRespDto xdtz0041DataRespDto = ctrLoanContMapper.getHouseLoanByCusId(cusId);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataRespDto));
        return ctrLoanContMapper.getHouseLoanByCusId(cusId);
    }
}
