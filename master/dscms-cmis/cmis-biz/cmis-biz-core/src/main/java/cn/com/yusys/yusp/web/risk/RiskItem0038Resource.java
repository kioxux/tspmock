package cn.com.yusys.yusp.web.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpHighAmtAgrAppService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: riskItem0038Resource
 * @类描述: 合同金额校验
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-06-21 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0038合同金额校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0038")
public class RiskItem0038Resource {

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @方法名称: riskItem0038
     * @方法描述: 合同金额校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "合同金额校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0038(@RequestBody QueryModel queryModel) {
        return ResultDto.success(iqpLoanAppService.riskItem0038(queryModel.getCondition().get("bizId").toString()));
    }
}
