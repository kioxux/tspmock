package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusCreditInfo
 * @类描述: iqp_cus_credit_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-24 15:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpCusCreditInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	private String bizNo;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 与借款人关系 **/
	private String debitRela;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 现有消费类融资余额(万元) **/
	private java.math.BigDecimal consumeFinBal;
	
	/** 现有消费类融资月还款额(万元) **/
	private java.math.BigDecimal consumeMonRepay;
	
	/** 贷款当前逾期金额 **/
	private java.math.BigDecimal loanCurtOverdueAmt;
	
	/** 贷款单月最高逾期金额(元) **/
	private java.math.BigDecimal loanHighOverdueAmt;
	
	/** 贷款最长逾期月数 **/
	private String loanLgstOverdueMon;
	
	/** 贷记卡当前逾期金额 **/
	private java.math.BigDecimal debitCurtOverdueAmt;
	
	/** 贷记卡单月最高逾期金额(元) **/
	private java.math.BigDecimal debitHighOverdueAmt;
	
	/** 贷记卡最长逾期月数 **/
	private String debitLgstOverdueMon;
	
	/** 两年内逾期次数 **/
	private String inTwoOverdueTimes;
	
	/** 两年外逾期次数 **/
	private String outTwoOverdueTimes;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param bizNo
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo == null ? null : bizNo.trim();
	}
	
    /**
     * @return BizNo
     */	
	public String getBizNo() {
		return this.bizNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param debitRela
	 */
	public void setDebitRela(String debitRela) {
		this.debitRela = debitRela == null ? null : debitRela.trim();
	}
	
    /**
     * @return DebitRela
     */	
	public String getDebitRela() {
		return this.debitRela;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param consumeFinBal
	 */
	public void setConsumeFinBal(java.math.BigDecimal consumeFinBal) {
		this.consumeFinBal = consumeFinBal;
	}
	
    /**
     * @return ConsumeFinBal
     */	
	public java.math.BigDecimal getConsumeFinBal() {
		return this.consumeFinBal;
	}
	
	/**
	 * @param consumeMonRepay
	 */
	public void setConsumeMonRepay(java.math.BigDecimal consumeMonRepay) {
		this.consumeMonRepay = consumeMonRepay;
	}
	
    /**
     * @return ConsumeMonRepay
     */	
	public java.math.BigDecimal getConsumeMonRepay() {
		return this.consumeMonRepay;
	}
	
	/**
	 * @param loanCurtOverdueAmt
	 */
	public void setLoanCurtOverdueAmt(java.math.BigDecimal loanCurtOverdueAmt) {
		this.loanCurtOverdueAmt = loanCurtOverdueAmt;
	}
	
    /**
     * @return LoanCurtOverdueAmt
     */	
	public java.math.BigDecimal getLoanCurtOverdueAmt() {
		return this.loanCurtOverdueAmt;
	}
	
	/**
	 * @param loanHighOverdueAmt
	 */
	public void setLoanHighOverdueAmt(java.math.BigDecimal loanHighOverdueAmt) {
		this.loanHighOverdueAmt = loanHighOverdueAmt;
	}
	
    /**
     * @return LoanHighOverdueAmt
     */	
	public java.math.BigDecimal getLoanHighOverdueAmt() {
		return this.loanHighOverdueAmt;
	}
	
	/**
	 * @param loanLgstOverdueMon
	 */
	public void setLoanLgstOverdueMon(String loanLgstOverdueMon) {
		this.loanLgstOverdueMon = loanLgstOverdueMon == null ? null : loanLgstOverdueMon.trim();
	}
	
    /**
     * @return LoanLgstOverdueMon
     */	
	public String getLoanLgstOverdueMon() {
		return this.loanLgstOverdueMon;
	}
	
	/**
	 * @param debitCurtOverdueAmt
	 */
	public void setDebitCurtOverdueAmt(java.math.BigDecimal debitCurtOverdueAmt) {
		this.debitCurtOverdueAmt = debitCurtOverdueAmt;
	}
	
    /**
     * @return DebitCurtOverdueAmt
     */	
	public java.math.BigDecimal getDebitCurtOverdueAmt() {
		return this.debitCurtOverdueAmt;
	}
	
	/**
	 * @param debitHighOverdueAmt
	 */
	public void setDebitHighOverdueAmt(java.math.BigDecimal debitHighOverdueAmt) {
		this.debitHighOverdueAmt = debitHighOverdueAmt;
	}
	
    /**
     * @return DebitHighOverdueAmt
     */	
	public java.math.BigDecimal getDebitHighOverdueAmt() {
		return this.debitHighOverdueAmt;
	}
	
	/**
	 * @param debitLgstOverdueMon
	 */
	public void setDebitLgstOverdueMon(String debitLgstOverdueMon) {
		this.debitLgstOverdueMon = debitLgstOverdueMon == null ? null : debitLgstOverdueMon.trim();
	}
	
    /**
     * @return DebitLgstOverdueMon
     */	
	public String getDebitLgstOverdueMon() {
		return this.debitLgstOverdueMon;
	}
	
	/**
	 * @param inTwoOverdueTimes
	 */
	public void setInTwoOverdueTimes(String inTwoOverdueTimes) {
		this.inTwoOverdueTimes = inTwoOverdueTimes == null ? null : inTwoOverdueTimes.trim();
	}
	
    /**
     * @return InTwoOverdueTimes
     */	
	public String getInTwoOverdueTimes() {
		return this.inTwoOverdueTimes;
	}
	
	/**
	 * @param outTwoOverdueTimes
	 */
	public void setOutTwoOverdueTimes(String outTwoOverdueTimes) {
		this.outTwoOverdueTimes = outTwoOverdueTimes == null ? null : outTwoOverdueTimes.trim();
	}
	
    /**
     * @return OutTwoOverdueTimes
     */	
	public String getOutTwoOverdueTimes() {
		return this.outTwoOverdueTimes;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}