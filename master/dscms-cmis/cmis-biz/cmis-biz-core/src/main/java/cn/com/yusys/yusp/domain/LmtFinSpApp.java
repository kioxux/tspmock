/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinSpApp
 * @类描述: lmt_fin_sp_app数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 09:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_fin_sp_app")
public class LmtFinSpApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 融资协议编号 **/
	@Column(name = "fin_ctr_no", unique = false, nullable = false, length = 40)
	private String finCtrNo;
	
	/** 担保公司客户编号 **/
	@Column(name = "cus_id", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 代偿申请日期 **/
	@Column(name = "subpay_app_date", unique = false, nullable = true, length = 10)
	private String subpayAppDate;
	
	/** 本次代偿笔数 **/
	@Column(name = "curt_cur_subpay_qnt", unique = false, nullable = true, length = 8)
	private String curtCurSubpayQnt;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "cur_type", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 代偿总额 **/
	@Column(name = "subpay_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subpayAmt;
	
	/** 收款人账号 **/
	@Column(name = "pyee_acct_no", unique = false, nullable = true, length = 40)
	private String pyeeAcctNo;
	
	/** 收款人账户名 **/
	@Column(name = "pyee_acct_name", unique = false, nullable = true, length = 80)
	private String pyeeAcctName;
	
	/** 收款人开户行行号 **/
	@Column(name = "pyee_opac_org_no", unique = false, nullable = true, length = 20)
	private String pyeeOpacOrgNo;
	
	/** 收款人开户行行名 **/
	@Column(name = "pyee_opan_org_name", unique = false, nullable = true, length = 100)
	private String pyeeOpanOrgName;
	
	/** 主办人 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "input_id", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "input_br_id", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "input_date", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "upd_id", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "upd_br_id", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "upd_date", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	@Column(name = "approve_status", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param finCtrNo
	 */
	public void setFinCtrNo(String finCtrNo) {
		this.finCtrNo = finCtrNo;
	}
	
    /**
     * @return finCtrNo
     */
	public String getFinCtrNo() {
		return this.finCtrNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param subpayAppDate
	 */
	public void setSubpayAppDate(String subpayAppDate) {
		this.subpayAppDate = subpayAppDate;
	}
	
    /**
     * @return subpayAppDate
     */
	public String getSubpayAppDate() {
		return this.subpayAppDate;
	}
	
	/**
	 * @param curtCurSubpayQnt
	 */
	public void setCurtCurSubpayQnt(String curtCurSubpayQnt) {
		this.curtCurSubpayQnt = curtCurSubpayQnt;
	}
	
    /**
     * @return curtCurSubpayQnt
     */
	public String getCurtCurSubpayQnt() {
		return this.curtCurSubpayQnt;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param subpayAmt
	 */
	public void setSubpayAmt(java.math.BigDecimal subpayAmt) {
		this.subpayAmt = subpayAmt;
	}
	
    /**
     * @return subpayAmt
     */
	public java.math.BigDecimal getSubpayAmt() {
		return this.subpayAmt;
	}
	
	/**
	 * @param pyeeAcctNo
	 */
	public void setPyeeAcctNo(String pyeeAcctNo) {
		this.pyeeAcctNo = pyeeAcctNo;
	}
	
    /**
     * @return pyeeAcctNo
     */
	public String getPyeeAcctNo() {
		return this.pyeeAcctNo;
	}
	
	/**
	 * @param pyeeAcctName
	 */
	public void setPyeeAcctName(String pyeeAcctName) {
		this.pyeeAcctName = pyeeAcctName;
	}
	
    /**
     * @return pyeeAcctName
     */
	public String getPyeeAcctName() {
		return this.pyeeAcctName;
	}
	
	/**
	 * @param pyeeOpacOrgNo
	 */
	public void setPyeeOpacOrgNo(String pyeeOpacOrgNo) {
		this.pyeeOpacOrgNo = pyeeOpacOrgNo;
	}
	
    /**
     * @return pyeeOpacOrgNo
     */
	public String getPyeeOpacOrgNo() {
		return this.pyeeOpacOrgNo;
	}
	
	/**
	 * @param pyeeOpanOrgName
	 */
	public void setPyeeOpanOrgName(String pyeeOpanOrgName) {
		this.pyeeOpanOrgName = pyeeOpanOrgName;
	}
	
    /**
     * @return pyeeOpanOrgName
     */
	public String getPyeeOpanOrgName() {
		return this.pyeeOpanOrgName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}