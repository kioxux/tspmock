/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperAgricultureBreedSituation;
import cn.com.yusys.yusp.repository.mapper.RptOperAgricultureBreedSituationMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperAgricultureBreedSituationService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 10:36:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperAgricultureBreedSituationService {

    @Autowired
    private RptOperAgricultureBreedSituationMapper rptOperAgricultureBreedSituationMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptOperAgricultureBreedSituation selectByPrimaryKey(String pkId) {
        return rptOperAgricultureBreedSituationMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptOperAgricultureBreedSituation> selectAll(QueryModel model) {
        List<RptOperAgricultureBreedSituation> records = (List<RptOperAgricultureBreedSituation>) rptOperAgricultureBreedSituationMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptOperAgricultureBreedSituation> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperAgricultureBreedSituation> list = rptOperAgricultureBreedSituationMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptOperAgricultureBreedSituation record) {
        return rptOperAgricultureBreedSituationMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptOperAgricultureBreedSituation record) {
        return rptOperAgricultureBreedSituationMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptOperAgricultureBreedSituation record) {
        return rptOperAgricultureBreedSituationMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptOperAgricultureBreedSituation record) {
        return rptOperAgricultureBreedSituationMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptOperAgricultureBreedSituationMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperAgricultureBreedSituationMapper.deleteByIds(ids);
    }

    public int save(RptOperAgricultureBreedSituation rptOperAgricultureBreedSituation){
        String pkId = rptOperAgricultureBreedSituation.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptOperAgricultureBreedSituation);
        }else {
            rptOperAgricultureBreedSituation.setPkId(StringUtils.getUUID());
            return insert(rptOperAgricultureBreedSituation);
        }
    }
}
