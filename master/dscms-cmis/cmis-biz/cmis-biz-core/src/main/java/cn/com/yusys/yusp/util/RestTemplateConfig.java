package cn.com.yusys.yusp.util;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/5/7 9:54 下午
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}