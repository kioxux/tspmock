/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpAcct;
import cn.com.yusys.yusp.domain.PvpRruUpdApp;
import cn.com.yusys.yusp.domain.PvpTruPayInfo;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.PvpRruUpdAppMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRruUpdAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-28 14:37:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpRruUpdAppService {
    private static final Logger log = LoggerFactory.getLogger(PvpRruUpdAppService.class);
    @Autowired
    private   AccLoanService  accLoanService;

    @Autowired
    private PvpTruPayInfoService  pvpTruPayInfoService;

    @Autowired
    private   IqpAcctService  iqpAcctService;

    @Autowired
    private PvpRruUpdAppMapper pvpRruUpdAppMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public PvpRruUpdApp selectByPrimaryKey(String rruSerno) {
        return pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpRruUpdApp> selectAll(QueryModel model) {
        List<PvpRruUpdApp> records = (List<PvpRruUpdApp>) pvpRruUpdAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<PvpRruUpdApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpRruUpdApp> list = pvpRruUpdAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(PvpRruUpdApp record) {
        return pvpRruUpdAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(PvpRruUpdApp pvpRruUpdApp) {
        pvpRruUpdApp.setApproveStatus("000");
        pvpRruUpdApp.setAuthStatus("00");;
        pvpRruUpdApp.setOprType("01");
        return pvpRruUpdAppMapper.insertSelective(pvpRruUpdApp);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(PvpRruUpdApp record) {
        return pvpRruUpdAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(PvpRruUpdApp record) {
        return pvpRruUpdAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String rruSerno) {
        return pvpRruUpdAppMapper.deleteByPrimaryKey(rruSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpRruUpdAppMapper.deleteByIds(ids);
    }

    /**
     * 校验该借据是否已经存在一条在途的退汇申请
     * @param pvpRruUpdApp
     * @return
     */
    public Integer checkIsExistPvpRruUpdAppBizByBillNo(PvpRruUpdApp pvpRruUpdApp) {
        return this.pvpRruUpdAppMapper.checkIsExistPvpRruUpdAppBizByBillNo(pvpRruUpdApp);
    }

    /**
     * 流程审批进程中修改审批状态  待发起-审核中
     * @param rruSerno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterStart(String rruSerno) {
        try{
            if(StringUtils.isBlank(rruSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+rruSerno+"申请主表信息");
            PvpRruUpdApp pvpRruUpdApp = this.pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
            if(pvpRruUpdApp==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+rruSerno+"流程审批状态为【111】-审批中");
            PvpRruUpdApp pvpRruUpdAppNew=new PvpRruUpdApp();
            pvpRruUpdAppNew.setRruSerno(rruSerno);
            pvpRruUpdAppNew.setApproveStatus("111");
            int result= this.pvpRruUpdAppMapper.updateByPrimaryKeySelective(pvpRruUpdAppNew);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }

    }
    /**
     * 流程审批进程中修改审批状态  待发起-已通过
     * @param rruSerno
     * 流程审批通过后，自动将该笔借据的受托支付申请自动展示在 【未受托支付】菜单列表中（其中：未受托支付类型=01-退汇，支付状态=未支付）。
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessDataAfterEnd(String rruSerno) {
        try{
            if(StringUtils.isBlank(rruSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+rruSerno+"申请主表信息");
            PvpRruUpdApp pvpRruUpdApp = this.pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
            if(pvpRruUpdApp==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+rruSerno+"流程审批状态为【997】-已通过");
            //根据借据编号  查询受托信息表   拿到一条受托信息数据
            PvpTruPayInfo  pvpTruPayInfo=this.pvpTruPayInfoService.selectByBillNo(pvpRruUpdApp.getBillNo());
            PvpTruPayInfo  pvpTruPayInfoNew=new PvpTruPayInfo();
            pvpTruPayInfoNew.setTranSerno(pvpTruPayInfo.getTranSerno());
            pvpTruPayInfoNew.setAcctNo(pvpRruUpdApp.getAcctNo());
            pvpTruPayInfoNew.setAcctName(pvpRruUpdApp.getAcctName());
            pvpTruPayInfoNew.setUntruPayType("01");
            pvpTruPayInfoNew.setPayStatus("00");
            this.pvpTruPayInfoService.updateSelective(pvpTruPayInfoNew);
            PvpRruUpdApp pvpRruUpdAppNew=new PvpRruUpdApp();
            pvpRruUpdAppNew.setRruSerno(rruSerno);
            pvpRruUpdAppNew.setApproveStatus("997");
            int result= this.pvpRruUpdAppMapper.updateByPrimaryKeySelective(pvpRruUpdAppNew);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 流程审批进程中修改审批状态  待发起-打回
     * @param rruSerno
     */
    public void handleBusinessDataAfteCallBack(String rruSerno) {
        try{
            if(StringUtils.isBlank(rruSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+rruSerno+"申请主表信息");
            PvpRruUpdApp pvpRruUpdApp = this.pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
            if(pvpRruUpdApp==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+rruSerno+"流程审批状态为【992】-打回");
            PvpRruUpdApp pvpRruUpdAppNew=new PvpRruUpdApp();
            pvpRruUpdAppNew.setRruSerno(rruSerno);
            pvpRruUpdAppNew.setApproveStatus("992");
            int result= this.pvpRruUpdAppMapper.updateByPrimaryKeySelective(pvpRruUpdAppNew);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }


    /**
     * 流程审批进程中修改审批状态  待发起-拿回
     * @param rruSerno
     */
    public void handleBusinessDataAfteTackBack(String rruSerno) {
        try{
            if(StringUtils.isBlank(rruSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+rruSerno+"申请主表信息");
            PvpRruUpdApp pvpRruUpdApp = this.pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
            if(pvpRruUpdApp==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+rruSerno+"流程审批状态为【991】-拿回");
            PvpRruUpdApp pvpRruUpdAppNew=new PvpRruUpdApp();
            pvpRruUpdAppNew.setRruSerno(rruSerno);
            pvpRruUpdAppNew.setApproveStatus("991");
            int result= this.pvpRruUpdAppMapper.updateByPrimaryKeySelective(pvpRruUpdAppNew);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }


    /**
     * 流程审批进程中修改审批状态  待发起-否决
     * @param rruSerno
     */
    public void handleBusinessDataAfterRefuse(String rruSerno) {
        try{
            if(StringUtils.isBlank(rruSerno)){
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请"+rruSerno+"申请主表信息");
            PvpRruUpdApp pvpRruUpdApp = this.pvpRruUpdAppMapper.selectByPrimaryKey(rruSerno);
            if(pvpRruUpdApp==null){
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请"+rruSerno+"流程审批状态为【998】-否决");
            PvpRruUpdApp pvpRruUpdAppNew=new PvpRruUpdApp();
            pvpRruUpdAppNew.setRruSerno(rruSerno);
            pvpRruUpdAppNew.setApproveStatus("998");
            int result= this.pvpRruUpdAppMapper.updateByPrimaryKeySelective(pvpRruUpdAppNew);
            if(result<0){
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("业务申请流程发起业务处理发生异常！",e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }
    /**
     * 根据借据编号查询  借据基本信息
     * @param pvpTruPayInfo
     * @return
     */
    public AccLoan getAccloanByPrimary(PvpTruPayInfo pvpTruPayInfo) {
        return this.accLoanService.selectByPrimaryKey(pvpTruPayInfo.getBillNo());
    }

    /**
     * 根据账号查询账号信息
     * @param acctNo
     * @return
     */
        public  IqpAcct getIqpAcct(String acctNo) {
        return this.iqpAcctService.getIqpAcct(acctNo);
    }
}
