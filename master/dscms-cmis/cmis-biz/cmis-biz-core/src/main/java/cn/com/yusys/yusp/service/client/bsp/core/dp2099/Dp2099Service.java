package cn.com.yusys.yusp.service.client.bsp.core.dp2099;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreDpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021年8月30日19:43:32
 * @desc 根据客户号查询客户账号
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Dp2099Service {

    private static final Logger logger = LoggerFactory.getLogger(Dp2099Service.class);
    // 1）注入：BSP封装调用核心系统的接口
    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    /**
     * @param dp2099ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto
     * @author hubp
     * @date 2021/8/30 19:43
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Dp2099RespDto dp2099(Dp2099ReqDto dp2099ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value, JSON.toJSONString(dp2099ReqDto));
        ResultDto<Dp2099RespDto> dp2099RespDtoResultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value, JSON.toJSONString(dp2099RespDtoResultDto));
        String dp2099Code = Optional.ofNullable(dp2099RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String dp2099Meesage = Optional.ofNullable(dp2099RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Dp2099RespDto dp2099RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, dp2099RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            dp2099RespDto = dp2099RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(dp2099Code, dp2099Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value);
        return dp2099RespDto;
    }
}
