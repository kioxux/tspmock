package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtModelApprResultInfo;
import cn.com.yusys.yusp.domain.LmtSurveyTaskDivis;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.HxdxdInfoDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtModelApprResultInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyTaskDivisMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: HxdListQueryServices
 * @类描述: #服务类 唤醒贷信息查询
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class HxdListQueryServices {
    private static final Logger logger = LoggerFactory.getLogger(HxdListQueryServices.class);

    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;

    @Autowired
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: hxlist
     * @方法描述: 根据条件唤醒贷信息查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<HxdxdInfoDto> hxlist(HxdxdInfoDto hxdxdInfoDto) {
        //查询条件
        String certCode = hxdxdInfoDto.getCertCode();//证件号
        String cusId = hxdxdInfoDto.getCusId();//客户号
        String cusName = hxdxdInfoDto.getCusName();//客户名称
        String status = hxdxdInfoDto.getState();//状态
        String pageNum = hxdxdInfoDto.getStartPageNum();//页码
        String pageSize = hxdxdInfoDto.getPageSize();//每页条数

        //分页查询
        int startPageNum = 1;//页码
        int pageSizeNum = 10;//每页条数
        //定义返回列表
        java.util.List<HxdxdInfoDto> resList = new ArrayList<>();
        if (StringUtil.isEmpty(pageSize) && StringUtil.isEmpty(pageNum)) {
            return resList;
        } else if (StringUtil.isEmpty(status)) {//状态不为空
            return resList;
        } else {
            startPageNum = Integer.parseInt(pageNum);
            pageSizeNum = Integer.parseInt(pageSize);
        }

        //查询条件
        Map map = new HashMap<>();
        map.put("status", status);//调查分配状态
        map.put("cusId", cusId);
        map.put("cusName", cusName);
        //调查任务分配表信息查询
        logger.info("************根据条件唤醒贷信息查询开始,查询参数为:{}", JSON.toJSONString(map));
        PageHelper.startPage(startPageNum, pageSizeNum);
        resList = lmtSurveyTaskDivisMapper.selectHxdMessageByModel(map);
        PageHelper.clearPage();
        logger.info("**********根据条件唤醒贷信息查询结束,返回结果为:{}", JSON.toJSONString(resList));
        resList = resList.parallelStream().map(ret -> {
            HxdxdInfoDto temp = new HxdxdInfoDto();
            BeanUtils.copyProperties(ret, temp);
            //获取客户经理姓名
            AdminSmUserDto adminSmUserDto = queryAdminSmUser(temp.getManagerId());
            temp.setManagerName(adminSmUserDto.getUserName());
            //获取分配人姓名
            adminSmUserDto = queryAdminSmUser(temp.getDistributionId());
            temp.setDistributionName(adminSmUserDto.getUserName());
            //返回列表
            return temp;
        }).collect(Collectors.toList());
        //返回
        return resList;
    }

    /**
     * @方法名称: hxdeta
     * @方法描述: 换醒贷客户信息查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public HxdxdInfoDto hxdeta(HxdxdInfoDto hxdxdInfoDto) {
        //查询条件
        String serno = hxdxdInfoDto.getSerno();//调查流水号
        //分页查询
        //定义返回信息
        if (StringUtil.isEmpty(serno)) {//调查流水号不能为空
            return hxdxdInfoDto;
        }
        //换醒贷客户信息查询
        logger.info("************换醒贷客户信息查询开始,查询参数为:{}", JSON.toJSONString(serno));
        LmtSurveyTaskDivis lmtSurveyTaskDivis = lmtSurveyTaskDivisMapper.selectBySurveySerno(serno);
        logger.info("**********换醒贷客户信息查询结束,返回结果为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
        if (lmtSurveyTaskDivis != null) {
            hxdxdInfoDto.setSerno(serno);// 业务流水
            hxdxdInfoDto.setCertCode(lmtSurveyTaskDivis.getCertCode()); //申请人证件号
            hxdxdInfoDto.setBusinessAddress(lmtSurveyTaskDivis.getOperAddr());    //经营地址
            hxdxdInfoDto.setCusId(lmtSurveyTaskDivis.getCusId()); //客户编号
            hxdxdInfoDto.setCusName(lmtSurveyTaskDivis.getCusName()); //客户名称
            hxdxdInfoDto.setDistributionDate(lmtSurveyTaskDivis.getDivisTime());//分配时间
            hxdxdInfoDto.setDistributionId(lmtSurveyTaskDivis.getDivisId());//责任人
            hxdxdInfoDto.setManagerId(lmtSurveyTaskDivis.getManagerId());//责任人
            hxdxdInfoDto.setInputDate(lmtSurveyTaskDivis.getIntoTime());  //登记日期
            hxdxdInfoDto.setTelPhone(lmtSurveyTaskDivis.getPhone()); //申请人手机号
            hxdxdInfoDto.setState(lmtSurveyTaskDivis.getDivisStatus());
            AdminSmUserDto adminSmUserDto = queryAdminSmUser(lmtSurveyTaskDivis.getManagerId());
            hxdxdInfoDto.setDistributionName(adminSmUserDto.getUserName());  //分配人
            hxdxdInfoDto.setManagerName(adminSmUserDto.getUserName()); //责任人
        }
        return hxdxdInfoDto;
    }

    /**
     * @方法名称: hxcxcz
     * @方法描述: 换醒贷客户撤销操作
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Integer hxcxcz(HxdxdInfoDto hxdxdInfoDto) {
        int flag = 0;
        //查询条件
        String serno = hxdxdInfoDto.getSerno();//调查流水号
        //分页查询
        //定义返回信息
        if (StringUtil.isEmpty(serno)) {//调查流水号不能为空
            return flag;
        }

        String resultStatus = "";//模型结果状态
        String apprStatus = "";
        String surveySerno = "";//调查流水号
        //换醒贷客户撤销操作
        logger.info("************换醒贷客户撤销操作开始,查询参数为:{}", JSON.toJSONString(serno));
        LmtModelApprResultInfo LmtModelApprResultInfo = lmtModelApprResultInfoMapper.selecthxcxczByPrimaryKey(serno);
        logger.info("**********换醒贷客户撤销查询信息结束,返回结果为:{}", JSON.toJSONString(LmtModelApprResultInfo));
        if (LmtModelApprResultInfo != null) {
            resultStatus = LmtModelApprResultInfo.getModelRstStatus();//模型结果状态
            surveySerno = LmtModelApprResultInfo.getSurveySerno();//调查流水号
        }
        if ("000".equals(resultStatus)) {//待发起
            LmtSurveyTaskDivis record = new LmtSurveyTaskDivis();
            record.setDivisStatus("101");
            record.setDivisId("");//分配人
            record.setDivisTime("");//分配时间
            record.setSurveySerno(surveySerno);
            flag = lmtSurveyTaskDivisMapper.updateBySernoSelective(record);

        } else {
            logger.info("************模型审批处在审批中、被打回、通过状态，不能撤回！********");
        }

        return flag;
    }

    /**
     * @方法名称: hxcxcz
     * @方法描述: 换醒贷客户分配操作
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Integer hxfpcz(HxdxdInfoDto hxdxdInfoDto) {
        int flag = 0;
        //查询条件
        String serno = hxdxdInfoDto.getSerno();//调查流水号
        String managerId = hxdxdInfoDto.getManagerId();
        String manager_name = hxdxdInfoDto.getManagerName();
        String distribution_id = hxdxdInfoDto.getDistributionId();
        String distribution_name = hxdxdInfoDto.getDistributionName();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        //分页查询
        //定义返回信息
        if (StringUtil.isEmpty(serno)) {//调查流水号不能为空
            return flag;
        }
        // 根据工号获取客户经理信息
        String state = "";//员工状态
        AdminSmUserDto adminSmUserDto = queryAdminSmUser(distribution_id);
        state = adminSmUserDto.getUserSts();
        //判断状态
        if (StringUtil.isEmpty(state) || "I".equals(state)) {
            logger.info("***********客户经理已离职或信贷系统不存在该客户经理，请重新分配！**************");
            throw BizException.error(null, EpbEnum.EPB099999.key, "客户经理已离职或信贷系统不存在该客户经理，请重新分配！");
        } else {
            //查询改人员角色是否为1138(todo OCA缺少通过用户查询角色的方法)
            GetUserInfoByRoleCodeDto getUserInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
            getUserInfoByRoleCodeDto.setRoleCode("R1015");
            List<AdminSmUserDto> adminSmUserDtos = commonService.getUserInfoByRoleCode(getUserInfoByRoleCodeDto);
            if (CollectionUtils.nonEmpty(adminSmUserDtos)) {
                for (AdminSmUserDto list : adminSmUserDtos) {
                    if (distribution_id.equals(list.getUserCode())) {

                    } else {
                        logger.info("***********客户经理已离职或非本分中心人员，请重新分配！**************");
                        throw BizException.error(null, EpbEnum.EPB099999.key, "客户经理已离职或非本分中心人员，请重新分配！");
                    }
                }
            }
        }
        //执行更新操作
        LmtSurveyTaskDivis record = new LmtSurveyTaskDivis();
        record.setDivisStatus("100");
        record.setDivisId(distribution_id);//分配人
        record.setManagerName(manager_name);
        record.setManagerId(managerId);
        record.setDivisTime(openDay);//分配时间
        record.setSurveySerno(serno);
        logger.info("************换醒贷客户分配操作开始,查询参数为:{}", JSON.toJSONString(record));
        flag = lmtSurveyTaskDivisMapper.updateBySernoSelective(record);
        logger.info("**********换醒贷客户分配操作开始,返回结果为:{}", JSON.toJSONString(flag));
        return flag;
    }

    public AdminSmUserDto queryAdminSmUser(String managerId) {
        AdminSmUserDto adminSmUserDto = new AdminSmUserDto();
        if(StringUtil.isNotEmpty(managerId)){
            logger.info("************调用AdminSmUserService用户信息查询服务开始*START,查询参数为:{}", JSON.toJSONString(managerId));
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("**********调用AdminSmUserService用户信息查询服务结束,返回结果为:{}", JSON.toJSONString(resultDto));
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
            }
        }
        return adminSmUserDto;
    }


}
