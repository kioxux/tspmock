package cn.com.yusys.yusp.constant;

/**
 * 放款模块常量以及异常枚举定义
 */
public enum PvpDefEnums {


    /**放款申请-数据源表**/
    APPLY_SOURCE_TABLE_NAME("ctr_loan_cont"),
    /**放款申请-数据目标表**/
    APPLY_DIST_TABLE_NAME("pvp_loan_app"),
    /**额度关系数据为空提示消息**/
    PVP_QUERY_LMTRELNULL_MSG("额度关系数据不存在！"),
    /**放款到期日校验-合同到期日后的月数参数值**/
    PVP_END_DATE_DEF_CONT_MONTH("6"),

    /**数据流映射  账户表名  iqp_acct**/
    PVP_IQP_ACCT_TABLE_NAME("iqp_acct"),
    /**数据流映射  账号信息与借据关系表  iqp_acct_no_bill_rel**/
    PVP_IQP_ACCT_NO_BILL_REL_TABLE_NAME("iqp_acct_no_bill_rel"),
    /**未受托表-支付状态  00-未支付**/
    PVP_TRU_PAY_INFO_PAY_STATUS_00("00"),
    /**未受托表-支付状态  01-支付失败**/
    PVP_TRU_PAY_INFO_PAY_STATUS_01("01"),
    /**未受托表-支付状态  02-支付成功**/
    PVP_TRU_PAY_INFO_PAY_STATUS_02("02"),
    /**未受托表-支付状态  03-支付已重发**/
    PVP_TRU_PAY_INFO_PAY_STATUS_03("03"),
    ;
    /** 异常编码 **/
    private String exceptionCode;
    /** 异常描述 **/
    private String exceptionDesc;

    PvpDefEnums(String exceptionCode, String exceptionDesc){
        this.exceptionCode = exceptionCode;
        this.exceptionDesc = exceptionDesc;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public String getExceptionDesc() {
        return exceptionDesc;
    }

    /**常量定义**/
    private String value;
    PvpDefEnums(String value){
        this.value = value;
    }

    public String getValue(){
        return  value;
    }

}
