/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CtrLoanExt;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.service.IqpContExtBillService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpContExtBillResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 15:34:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpcontextbill")
public class IqpContExtBillResource {
    @Autowired
    private IqpContExtBillService iqpContExtBillService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpContExtBill>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpContExtBill> list = iqpContExtBillService.selectAll(queryModel);
        return new ResultDto<List<IqpContExtBill>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpContExtBill>> index(QueryModel queryModel) {
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryModel);
        return new ResultDto<List<IqpContExtBill>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<IqpContExtBill>> query(@RequestBody QueryModel queryModel) {
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryModel);
        return new ResultDto<List<IqpContExtBill>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<IqpContExtBill> show(@PathVariable("serno") String serno) {
        IqpContExtBill iqpContExtBill = iqpContExtBillService.selectByPrimaryKey(serno);
        return new ResultDto<IqpContExtBill>(iqpContExtBill);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpContExtBill> create(@RequestBody IqpContExtBill iqpContExtBill) throws URISyntaxException {
        iqpContExtBillService.insert(iqpContExtBill);
        return new ResultDto<IqpContExtBill>(iqpContExtBill);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpContExtBill iqpContExtBill) throws URISyntaxException {
        int result = iqpContExtBillService.update(iqpContExtBill);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = iqpContExtBillService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpContExtBillService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *
     * @author 刘权
     **/
    @ApiOperation("展期信息")
    @PostMapping("/queryExtensionInfo")
    protected ResultDto<List<IqpContExtBill>> queryExtensionInfo(@RequestBody QueryModel model) {
        List<IqpContExtBill> list = iqpContExtBillService.queryExtensionInfo(model);
        return new ResultDto<List<IqpContExtBill>>(list);
    }
}
