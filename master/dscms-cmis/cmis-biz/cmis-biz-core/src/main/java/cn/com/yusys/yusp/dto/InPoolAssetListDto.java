package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoan
 * @类描述: acc_loan数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-04 14:09:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class InPoolAssetListDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 资产池协议编号 **/
	private String contNo;

	/** 资产池协议状态 **/
	private String contStatus;

	/** 抵质押物编号(字段未定，getset) **/


	/** 资产类型 **/
	private String assetType;

	/** 资产价值 **/
	private BigDecimal assetValue;

	/** 资产到期日期 **/
	private String assetEndDate;

	/** 资产质押状态(字段未定，getset) **/


	/** 是否入池 **/
	private String isPool;


	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public BigDecimal getAssetValue() {
		return assetValue;
	}

	public void setAssetValue(BigDecimal assetValue) {
		this.assetValue = assetValue;
	}

	public String getAssetEndDate() {
		return assetEndDate;
	}

	public void setAssetEndDate(String assetEndDate) {
		this.assetEndDate = assetEndDate;
	}

	public String getIsPool() {
		return isPool;
	}

	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}
}