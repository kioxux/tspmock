/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardAmtInfo;
import cn.com.yusys.yusp.repository.mapper.CreditCardAmtInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAmtInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-18 15:54:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardAmtInfoService {

    @Autowired
    private CreditCardAmtInfoMapper creditCardAmtInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardAmtInfo selectByPrimaryKey(String pkId, String serno) {
        return creditCardAmtInfoMapper.selectByPrimaryKey(pkId, serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardAmtInfo> selectAll(QueryModel model) {
        List<CreditCardAmtInfo> records = (List<CreditCardAmtInfo>) creditCardAmtInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardAmtInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardAmtInfo> list = creditCardAmtInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardAmtInfo record) {
        return creditCardAmtInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardAmtInfo record) {
        return creditCardAmtInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardAmtInfo record) {
        return creditCardAmtInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardAmtInfo record) {
        return creditCardAmtInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return creditCardAmtInfoMapper.deleteByPrimaryKey(pkId, serno);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/5/29 11:09
     * @version 1.0.0
     * @desc  根据流水号和产品编号查询终审信息
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public CreditCardAmtInfo queryCreditJudgeInfo(CreditCardAmtInfo record) {
        Map param = new HashMap();
        String serno = record.getSerno();
        String applyCardPrd = record.getCardPrd();
        param.put("serno",serno);
        param.put("cardPrd",applyCardPrd);
        //如果到初审审批结束
        CreditCardAmtInfo creditCardAmtInfo =  creditCardAmtInfoMapper.queryBySernoAndPrd(param);
        return creditCardAmtInfo;
    }
}
