package cn.com.yusys.yusp.service.server.xdxw0079;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtRenewLoanAppInfo;
import cn.com.yusys.yusp.dto.server.xdxw0079.req.Xdxw0079DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0079.resp.Xdxw0079DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtRenewLoanAppInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

/**
 * 接口处理类:小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdxw0079Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdxw0079.Xdxw0079Service.class);

    @Autowired
    private LmtRenewLoanAppInfoMapper lmtRenewLoanAppInfoMapper;

    /**
     * 小微续贷白名单查询
     *
     * @param xdxw0079DataReqDto
     * @return
     */
    public Xdxw0079DataRespDto xdxw0079(Xdxw0079DataReqDto xdxw0079DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataReqDto));
        Xdxw0079DataRespDto xdxw0079DataRespDto = new Xdxw0079DataRespDto();
        try {
            //获取请求参数
            String type = xdxw0079DataReqDto.getType();//操作类型
            String certCode = xdxw0079DataReqDto.getCertCode();//证件号码
            List<cn.com.yusys.yusp.dto.server.xdxw0079.req.List> list = xdxw0079DataReqDto.getList();
            String serno = StringUtils.EMPTY;
            String approveRs = StringUtils.EMPTY;

            if ("0".equals(type) || "1".equals(type)) {//0:校验白名单查询；1：名单查询
                if (CollectionUtils.nonEmpty(list)) {
                    cn.com.yusys.yusp.dto.server.xdxw0079.req.List list1 = list.get(0);
                    serno = list1.getSerno();
                    approveRs = list1.getApproveRs();
                }
                QueryModel model = new QueryModel();
                model.addCondition("certCode", certCode);
                model.addCondition("serno", serno);
                model.addCondition("status", approveRs);
                logger.info("根据查询条件【{}】小微续贷白名单信息开始", JSON.toJSONString(model));
                List<cn.com.yusys.yusp.dto.server.xdxw0079.resp.List> lmtRenewLoanAppInfoList = lmtRenewLoanAppInfoMapper.selectLmtRenewLoanAppInfoByModel(model);
                logger.info("根据查询条件【{}】小微续贷白名单信息结束,微贷区域机构信息【{}】", JSON.toJSONString(model), JSON.toJSONString(lmtRenewLoanAppInfoList));
                //返回信息
                xdxw0079DataRespDto.setList(lmtRenewLoanAppInfoList);
            } else if ("2".equals(type)) {//2：名单更新
                int result = 0;
                if (CollectionUtils.nonEmpty(list)) {
                    cn.com.yusys.yusp.dto.server.xdxw0079.req.List list1 = list.get(0);
                    serno = list1.getSerno();
                    approveRs = list1.getApproveRs();
                    certCode = list1.getCertCode();

                    LmtRenewLoanAppInfo record = new LmtRenewLoanAppInfo();
                    record.setSerno(serno);
                    record.setStatus(approveRs);
                    record.setCertCode(certCode);
                    logger.info("根据参数条件【{}】修改小微续贷白名单信息开始", JSON.toJSONString(record));
                    if ("001".equals(approveRs)) {
                        record.setUpdateTime(DateUtils.getCurrTimestamp());
                        result = lmtRenewLoanAppInfoMapper.updateByxdxw0079Selective(record);
                    } else {
                        result = lmtRenewLoanAppInfoMapper.updateByxdxw0079Selective(record);
                    }
                    logger.info("根据参数条件【{}】修改小微续贷白名单信息结束,返回处理结果【{}】", JSON.toJSONString(record), result);
                    if (result == 0) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "更新失败");
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataRespDto));
        return xdxw0079DataRespDto;
    }
}
