/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDelayPayment;
import cn.com.yusys.yusp.repository.mapper.IqpDelayPaymentMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDelayPaymentService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-26 15:07:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpDelayPaymentService {

    @Autowired
    private IqpDelayPaymentMapper iqpDelayPaymentMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private AccLoanService accLoanService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpDelayPayment selectByPrimaryKey(String delaySerno) {
        return iqpDelayPaymentMapper.selectByPrimaryKey(delaySerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDelayPayment> selectAll(QueryModel model) {
        List<IqpDelayPayment> records = (List<IqpDelayPayment>) iqpDelayPaymentMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpDelayPayment> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDelayPayment> list = iqpDelayPaymentMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpDelayPayment record) {
        return iqpDelayPaymentMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpDelayPayment record) {
        QueryModel model1 = new QueryModel();
        model1.addCondition("billNo", record.getBillNo());
        model1.addCondition("apply","1");
        List<IqpDelayPayment> list1 = this.selectByModel(model1);
        if(list1.size()>0){
            throw BizException.error(null, "999999","\"该借据存在在途延期还款申请，请勿重复发起！\"" + "保存失败！");
        }

        AccLoan accLoan = accLoanService.selectByBillNo(record.getBillNo());
        record.setDelaySerno(sequenceTemplateClient.getSequenceTemplate("IQP",new HashMap<>()));
        record.setCusId(accLoan.getCusId());                                  //客户编号
        record.setCusName(accLoan.getCusName());                              //客户名称
        record.setContNo(accLoan.getContNo());                                //合同编号
        record.setLoanAmt(accLoan.getLoanAmt());                              //贷款金额
        record.setLoanBalance(accLoan.getLoanBalance());                      //贷款余额
        record.setPrdId(accLoan.getPrdId());                                  //产品ID
        record.setPrdName(accLoan.getPrdName());                              //产品名称
        record.setApproveStatus("000");
        record.setManagerId(accLoan.getManagerId());// 主管客户经理
        record.setManagerBrId(accLoan.getManagerBrId());// 主管机构
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        record.setInputId(userInfo.getLoginCode()); // 当前用户号
        record.setInputBrId(userInfo.getOrg().getCode()); // 当前用户机构
        record.setInputDate(DateUtils.getCurrDateStr());
        record.setCreateTime(DateUtils.getCurrTimestamp());
        record.setUpdateTime(record.getCreateTime());
        record.setUpdDate(record.getInputDate());
        record.setUpdId(record.getInputId());
        record.setUpdBrId(record.getInputBrId());
        return iqpDelayPaymentMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpDelayPayment record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpDelayPaymentMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpDelayPayment record) {
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        record.setUpdDate(DateUtils.getCurrDateStr());
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode());
            record.setUpdBrId(userInfo.getOrg().getCode());
        }
        return iqpDelayPaymentMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String delaySerno) {
        return iqpDelayPaymentMapper.deleteByPrimaryKey(delaySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDelayPaymentMapper.deleteByIds(ids);
    }
}
