/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAppr
 * @类描述: intbank_org_admit_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 09:32:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "intbank_org_admit_appr")
public class IntbankOrgAdmitAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 审批流水号 **/
	@Column(name = "APPROVE_SERNO")
	private String approveSerno;
	
	/** 机构准入申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 申请类型 **/
	@Column(name = "APP_TYPE", unique = false, nullable = true, length = 5)
	private String appType;
	
	/** 原批复流水号 **/
	@Column(name = "ORIGI_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiReplySerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 同业机构准入 **/
	@Column(name = "INTBANK_ORG_ADMIT", unique = false, nullable = true, length = 2000)
	private String intbankOrgAdmit;
	
	/** 经营情况和财务情况 **/
	@Column(name = "OPER_FINA_SITU", unique = false, nullable = true, length = 65535)
	private String operFinaSitu;
	
	/** 经营情况和财务情况图片路径 **/
	@Column(name = "OPER_FINA_SITU_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String operFinaSituPicturePath;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 2000)
	private String indgtResult;
	
	/** 准入期限 **/
	@Column(name = "TERM", unique = false, nullable = true, length = 10)
	private Integer term;
	
	/** 综合分析 **/
	@Column(name = "INTE_ANALY", unique = false, nullable = true, length = 65535)
	private String inteAnaly;
	
	/** 总行出具综合分析 **/
	@Column(name = "INTE_ANALY_ZH", unique = false, nullable = true, length = 65535)
	private String inteAnalyZh;
	
	/** 评审结论 **/
	@Column(name = "REVIEW_RESULT", unique = false, nullable = true, length = 5)
	private String reviewResult;
	
	/** 总行出具评审结论 **/
	@Column(name = "REVIEW_RESULT_ZH", unique = false, nullable = true, length = 5)
	private String reviewResultZh;
	
	/** 出具报告类型 **/
	@Column(name = "ISSUE_REPORT_TYPE", unique = false, nullable = true, length = 5)
	private String issueReportType;
	
	/** 结论性描述 **/
	@Column(name = "REST_DESC", unique = false, nullable = true, length = 65535)
	private String restDesc;
	
	/** 总行出具结论性描述 **/
	@Column(name = "REST_DESC_ZH", unique = false, nullable = true, length = 65535)
	private String restDescZh;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}
	
    /**
     * @return approveSerno
     */
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	
    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param origiReplySerno
	 */
	public void setOrigiReplySerno(String origiReplySerno) {
		this.origiReplySerno = origiReplySerno;
	}
	
    /**
     * @return origiReplySerno
     */
	public String getOrigiReplySerno() {
		return this.origiReplySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param intbankOrgAdmit
	 */
	public void setIntbankOrgAdmit(String intbankOrgAdmit) {
		this.intbankOrgAdmit = intbankOrgAdmit;
	}
	
    /**
     * @return intbankOrgAdmit
     */
	public String getIntbankOrgAdmit() {
		return this.intbankOrgAdmit;
	}
	
	/**
	 * @param operFinaSitu
	 */
	public void setOperFinaSitu(String operFinaSitu) {
		this.operFinaSitu = operFinaSitu;
	}
	
    /**
     * @return operFinaSitu
     */
	public String getOperFinaSitu() {
		return this.operFinaSitu;
	}
	
	/**
	 * @param operFinaSituPicturePath
	 */
	public void setOperFinaSituPicturePath(String operFinaSituPicturePath) {
		this.operFinaSituPicturePath = operFinaSituPicturePath;
	}
	
    /**
     * @return operFinaSituPicturePath
     */
	public String getOperFinaSituPicturePath() {
		return this.operFinaSituPicturePath;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(Integer term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public Integer getTerm() {
		return this.term;
	}
	
	/**
	 * @param inteAnaly
	 */
	public void setInteAnaly(String inteAnaly) {
		this.inteAnaly = inteAnaly;
	}
	
    /**
     * @return inteAnaly
     */
	public String getInteAnaly() {
		return this.inteAnaly;
	}
	
	/**
	 * @param inteAnalyZh
	 */
	public void setInteAnalyZh(String inteAnalyZh) {
		this.inteAnalyZh = inteAnalyZh;
	}
	
    /**
     * @return inteAnalyZh
     */
	public String getInteAnalyZh() {
		return this.inteAnalyZh;
	}
	
	/**
	 * @param reviewResult
	 */
	public void setReviewResult(String reviewResult) {
		this.reviewResult = reviewResult;
	}
	
    /**
     * @return reviewResult
     */
	public String getReviewResult() {
		return this.reviewResult;
	}
	
	/**
	 * @param reviewResultZh
	 */
	public void setReviewResultZh(String reviewResultZh) {
		this.reviewResultZh = reviewResultZh;
	}
	
    /**
     * @return reviewResultZh
     */
	public String getReviewResultZh() {
		return this.reviewResultZh;
	}
	
	/**
	 * @param issueReportType
	 */
	public void setIssueReportType(String issueReportType) {
		this.issueReportType = issueReportType;
	}
	
    /**
     * @return issueReportType
     */
	public String getIssueReportType() {
		return this.issueReportType;
	}
	
	/**
	 * @param restDesc
	 */
	public void setRestDesc(String restDesc) {
		this.restDesc = restDesc;
	}
	
    /**
     * @return restDesc
     */
	public String getRestDesc() {
		return this.restDesc;
	}
	
	/**
	 * @param restDescZh
	 */
	public void setRestDescZh(String restDescZh) {
		this.restDescZh = restDescZh;
	}
	
    /**
     * @return restDescZh
     */
	public String getRestDescZh() {
		return this.restDescZh;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}