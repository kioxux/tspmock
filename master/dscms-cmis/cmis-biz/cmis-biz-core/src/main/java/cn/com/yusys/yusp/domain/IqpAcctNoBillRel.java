/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctNoBillRel
 * @类描述: iqp_acct_no_bill_rel数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-25 14:55:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_acct_no_bill_rel")
public class IqpAcctNoBillRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 账户属性 STD_ZB_BR_ID_ATTR **/
	@Column(name = "ACCT_ATTR", unique = false, nullable = true, length = 5)
	private String acctAttr;
	
	/** 账号归属 **/
	@Column(name = "ACCT_BELONG", unique = false, nullable = true, length = 5)
	private String acctBelong;
	
	/** 账号分类 **/
	@Column(name = "ACCT_CLASS", unique = false, nullable = true, length = 5)
	private String acctClass;
	
	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账号名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 开户行行号 **/
	@Column(name = "OPAN_ORG_NO", unique = false, nullable = true, length = 20)
	private String opanOrgNo;
	
	/** 开户行行名 **/
	@Column(name = "OPAN_ORG_NAME", unique = false, nullable = true, length = 100)
	private String opanOrgName;
	
	/** 机构编号 **/
	@Column(name = "ORG_NO", unique = false, nullable = true, length = 40)
	private String orgNo;
	
	/** 机构名称 **/
	@Column(name = "ORG_NAME", unique = false, nullable = true, length = 80)
	private String orgName;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 支付金额 **/
	@Column(name = "PAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payAmt;
	
	/** 支付用途 **/
	@Column(name = "PAY_USE", unique = false, nullable = true, length = 400)
	private String payUse;
	
	/** 账号状态 STD_ZB_PVP_ACCT_ST **/
	@Column(name = "ACCT_STATUS", unique = false, nullable = true, length = 1)
	private String acctStatus;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param acctAttr
	 */
	public void setAcctAttr(String acctAttr) {
		this.acctAttr = acctAttr;
	}
	
    /**
     * @return acctAttr
     */
	public String getAcctAttr() {
		return this.acctAttr;
	}
	
	/**
	 * @param acctBelong
	 */
	public void setAcctBelong(String acctBelong) {
		this.acctBelong = acctBelong;
	}
	
    /**
     * @return acctBelong
     */
	public String getAcctBelong() {
		return this.acctBelong;
	}
	
	/**
	 * @param acctClass
	 */
	public void setAcctClass(String acctClass) {
		this.acctClass = acctClass;
	}
	
    /**
     * @return acctClass
     */
	public String getAcctClass() {
		return this.acctClass;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param opanOrgNo
	 */
	public void setOpanOrgNo(String opanOrgNo) {
		this.opanOrgNo = opanOrgNo;
	}
	
    /**
     * @return opanOrgNo
     */
	public String getOpanOrgNo() {
		return this.opanOrgNo;
	}
	
	/**
	 * @param opanOrgName
	 */
	public void setOpanOrgName(String opanOrgName) {
		this.opanOrgName = opanOrgName;
	}
	
    /**
     * @return opanOrgName
     */
	public String getOpanOrgName() {
		return this.opanOrgName;
	}
	
	/**
	 * @param orgNo
	 */
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	
    /**
     * @return orgNo
     */
	public String getOrgNo() {
		return this.orgNo;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param payAmt
	 */
	public void setPayAmt(java.math.BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	
    /**
     * @return payAmt
     */
	public java.math.BigDecimal getPayAmt() {
		return this.payAmt;
	}
	
	/**
	 * @param payUse
	 */
	public void setPayUse(String payUse) {
		this.payUse = payUse;
	}
	
    /**
     * @return payUse
     */
	public String getPayUse() {
		return this.payUse;
	}
	
	/**
	 * @param acctStatus
	 */
	public void setAcctStatus(String acctStatus) {
		this.acctStatus = acctStatus;
	}
	
    /**
     * @return acctStatus
     */
	public String getAcctStatus() {
		return this.acctStatus;
	}


}