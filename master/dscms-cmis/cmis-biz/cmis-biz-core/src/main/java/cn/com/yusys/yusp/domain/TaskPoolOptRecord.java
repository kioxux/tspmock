/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TaskPoolOptRecord
 * @类描述: task_pool_opt_record数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "task_pool_opt_record")
public class TaskPoolOptRecord extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 业务类型细分 **/
	@Column(name = "BIZ_SUB_TYPE", unique = false, nullable = true, length = 5)
	private String bizSubType;
	
	/** 任务类型 **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 5)
	private String taskType;
	
	/** 操作类型   **/
	@Column(name = "OPT_TYPE", unique = false, nullable = true, length = 5)
	private String optType;
	
	/** 操作人 **/
	@Column(name = "OPT_USR", unique = false, nullable = true, length = 20)
	private String optUsr;
	
	/** 操作机构 **/
	@Column(name = "OPT_ORG", unique = false, nullable = true, length = 20)
	private String optOrg;
	
	/** 操作时间 **/
	@Column(name = "OPT_TIME", unique = false, nullable = true, length = 20)
	private String optTime;
	
	/** 操作原因 **/
	@Column(name = "OPT_REASON", unique = false, nullable = true, length = 1000)
	private String optReason;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizSubType
	 */
	public void setBizSubType(String bizSubType) {
		this.bizSubType = bizSubType;
	}
	
    /**
     * @return bizSubType
     */
	public String getBizSubType() {
		return this.bizSubType;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param optType
	 */
	public void setOptType(String optType) {
		this.optType = optType;
	}
	
    /**
     * @return optType
     */
	public String getOptType() {
		return this.optType;
	}
	
	/**
	 * @param optUsr
	 */
	public void setOptUsr(String optUsr) {
		this.optUsr = optUsr;
	}
	
    /**
     * @return optUsr
     */
	public String getOptUsr() {
		return this.optUsr;
	}
	
	/**
	 * @param optOrg
	 */
	public void setOptOrg(String optOrg) {
		this.optOrg = optOrg;
	}
	
    /**
     * @return optOrg
     */
	public String getOptOrg() {
		return this.optOrg;
	}
	
	/**
	 * @param optTime
	 */
	public void setOptTime(String optTime) {
		this.optTime = optTime;
	}
	
    /**
     * @return optTime
     */
	public String getOptTime() {
		return this.optTime;
	}
	
	/**
	 * @param optReason
	 */
	public void setOptReason(String optReason) {
		this.optReason = optReason;
	}
	
    /**
     * @return optReason
     */
	public String getOptReason() {
		return this.optReason;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}