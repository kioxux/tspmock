/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptFncSituBsLowRisk;
import cn.com.yusys.yusp.repository.mapper.RptFncSituBsLowRiskMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituBsLowRiskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-26 21:05:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptFncSituBsLowRiskService {

    @Autowired
    private RptFncSituBsLowRiskMapper rptFncSituBsLowRiskMapper;
	@Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptFncSituBsLowRisk selectByPrimaryKey(String pkId, String serno) {
        return rptFncSituBsLowRiskMapper.selectByPrimaryKey(pkId, serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptFncSituBsLowRisk> selectAll(QueryModel model) {
        List<RptFncSituBsLowRisk> records = (List<RptFncSituBsLowRisk>) rptFncSituBsLowRiskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptFncSituBsLowRisk> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptFncSituBsLowRisk> list = rptFncSituBsLowRiskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptFncSituBsLowRisk record) {
        return rptFncSituBsLowRiskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptFncSituBsLowRisk record) {
        return rptFncSituBsLowRiskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptFncSituBsLowRisk record) {
        return rptFncSituBsLowRiskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptFncSituBsLowRisk record) {
        return rptFncSituBsLowRiskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return rptFncSituBsLowRiskMapper.deleteByPrimaryKey(pkId, serno);
    }
    public int save(RptFncSituBsLowRisk rptFncSituBsLowRisk){
        String pkId = rptFncSituBsLowRisk.getPkId();
        if(StringUtils.nonBlank(pkId)){
            return update(rptFncSituBsLowRisk);
        }else {
            String sequenceTemplate = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, new HashMap<>());
            rptFncSituBsLowRisk.setPkId(sequenceTemplate);
            return insert(rptFncSituBsLowRisk);
        }
    }
}
