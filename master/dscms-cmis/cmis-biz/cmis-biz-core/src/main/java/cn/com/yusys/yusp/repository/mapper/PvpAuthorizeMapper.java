/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import cn.com.yusys.yusp.dto.server.xdcz0006.req.Xdcz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.resp.Xdcz0006DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpAuthorizeMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-17 09:37:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface PvpAuthorizeMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    PvpAuthorize selectByPrimaryKey(@Param("tranSerno") String tranSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<PvpAuthorize> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(PvpAuthorize record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(PvpAuthorize record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(PvpAuthorize record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(PvpAuthorize record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("tranSerno") String tranSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);



    /**
     * 根据主键修改授权状态为注销
     * @param pvpAuthorize
     * @return
     */
    int updateAuthstatusTocancelByPrimaryKey(PvpAuthorize pvpAuthorize);

    /**
     * 修改授权状态为已授权
     * @param pvpAuthorize
     * @return
     */
    int updateAuthstatusToYSQByPrimaryKey(PvpAuthorize pvpAuthorize);


    /**
     * @author zlf
     * @date 2021/4/23 22:00
     * @version 1.0.0
     * @desc修改授权状态
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int updateTrade(PvpAuthorize pvpAuthorize);

    /**
     * @方法名称: selectEnstrustByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAuthorize> selectEntrustByModel(QueryModel model);


    /**
     * @方法名称: selectPvpAuthorizeBySerno
     * @方法描述: 根据出账流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    PvpAuthorize selectPvpAuthorizeBySerno(@Param("pvpSerno")String pvpSerno);

    /**
     * @param billNo
     * @return cn.com.yusys.yusp.domain.PvpAuthorize
     * @author 王玉坤
     * @date 2021/10/7 0:15
     * @version 1.0.0
     * @desc 根据借据号查询借据信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    PvpAuthorize selectPvpAuthorizeByBillNo(@Param("billNO")String billNo);

    /**
     * @方法名称: selectPvpAuthorizeBySerno
     * @方法描述: 根据合同号及交易日期查询总交易金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    BigDecimal querySumTranAmtByParams(@Param("contNo")String contNo, @Param("tranDate")String tranDate);

    /**
     * 作废出帐信息
     * @param pvpSerno
     * @return
     */
    int updateAuthStatusByPvpSerno(@Param("pvpSerno")String pvpSerno);

    int SelectPvpAuthorizeBySerno1(@Param("serno")String serno);

    String selectPvpAuthorizeBySernocunzai(@Param("pvpSerno") String serno1);

    /**
     * 出账记录详情查看
     * @param xdcz0006DataReqDto
     * @return
     */
    Xdcz0006DataRespDto getDetail(Xdcz0006DataReqDto xdcz0006DataReqDto);


    /**
     * 借据条数查询信息
     *
     * @param map
     * @return
     */
    int getJjhCountInfo(Map map);

    /**
     * 借据流水号查询
     *
     * @param map
     * @return
     */
    String getJjhSernoInfo(Map map);

    /**
     * 插入受托支付信息
     *
     * @param map
     * @return
     */
    int insertJjhStzfInfo(Map map);

    /**
     * MANAGER_BR_ID查询
     *
     * @param pcSerno
     * @return
     */
    PvpAuthorize getBzjbjInfo(@Param("pcSerno") String pcSerno);

    /**
     * 查询合同号
     *
     * @param pcSerno
     * @return
     */
    String getcontNo(@Param("pcSerno")String pcSerno);
    
    /**
     * @方法名称: queryWxDzyList
     * @方法描述: 微信待支用
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAuthorize> queryWxDzyList(@Param("cusId") String cusId, @Param("tranDate") String tranDate);


    /**
     * @方法名称: queryWxYzyList
     * @方法描述: 微信已支用
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAuthorize> queryWxYzyList(@Param("cusId") String cusId);


    /**
     * @方法名称: queryWxAllzyList
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAuthorize> queryWxAllzyList(@Param("cusId") String cusId, @Param("startDate") String startDate, @Param("endDate") String endDate);

    int UpdatePvpAuthorizeByserno1(@Param("serno") String serno);

    int UpdatePvpAuthorizeByserno2(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<PvpAuthorize> selectPvpAuthorizeList(QueryModel model);

    /**
     * @方法名称: updateTradeStatusSuccess
     * @方法描述: 根据出账流水号更新出账状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateTradeStatusSuccess(@Param("pvpSerno") String pvpSerno, @Param("tradeStatus") String tradeStatus, @Param("returnDesc") String returnDesc);

    int updateByPvpSerNo(PvpAuthorize pvpAuthorize1);
}