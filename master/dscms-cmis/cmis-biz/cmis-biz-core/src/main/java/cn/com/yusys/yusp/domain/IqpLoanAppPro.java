/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppPro
 * @类描述: iqp_loan_app_pro数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 21:02:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_loan_app_pro")
public class IqpLoanAppPro extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 合同申请业务号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 项目类型    STD_PRJ_TYPE **/
	@Column(name = "PRO_TYPE", unique = false, nullable = true, length = 5)
	private String proType;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 255)
	private String proName;
	
	/** 项目投资 **/
	@Column(name = "PRO_INVEST", unique = false, nullable = true, length = 255)
	private String proInvest;
	
	/** 项目金额 **/
	@Column(name = "PRO_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal proAmt;
	
	/** 立项批文 **/
	@Column(name = "PRO_APPR", unique = false, nullable = true, length = 40)
	private String proAppr;
	
	/** 批文文号 **/
	@Column(name = "APPR_NO", unique = false, nullable = true, length = 40)
	private String apprNo;
	
	/** 规划许可证编号 **/
	@Column(name = "PLAN_LIC_NO", unique = false, nullable = true, length = 40)
	private String planLicNo;
	
	/** 建设用地许可证编号 **/
	@Column(name = "BUILD_LIC_NO", unique = false, nullable = true, length = 40)
	private String buildLicNo;
	
	/** 环评许可证编号 **/
	@Column(name = "EIA_LIC_NO", unique = false, nullable = true, length = 40)
	private String eiaLicNo;
	
	/** 施工许可证编号 **/
	@Column(name = "CONS_LIC_NO", unique = false, nullable = true, length = 40)
	private String consLicNo;
	
	/** 其他许可证 **/
	@Column(name = "OTHER_LIC", unique = false, nullable = true, length = 40)
	private String otherLic;
	
	/** 其他许可证编号 **/
	@Column(name = "OTHER_LIC_NO", unique = false, nullable = true, length = 40)
	private String otherLicNo;
	
	/** 开工日期 **/
	@Column(name = "START_WORK_DATE", unique = false, nullable = true, length = 40)
	private String startWorkDate;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param proType
	 */
	public void setProType(String proType) {
		this.proType = proType;
	}
	
    /**
     * @return proType
     */
	public String getProType() {
		return this.proType;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param proInvest
	 */
	public void setProInvest(String proInvest) {
		this.proInvest = proInvest;
	}
	
    /**
     * @return proInvest
     */
	public String getProInvest() {
		return this.proInvest;
	}
	
	/**
	 * @param proAmt
	 */
	public void setProAmt(java.math.BigDecimal proAmt) {
		this.proAmt = proAmt;
	}
	
    /**
     * @return proAmt
     */
	public java.math.BigDecimal getProAmt() {
		return this.proAmt;
	}
	
	/**
	 * @param proAppr
	 */
	public void setProAppr(String proAppr) {
		this.proAppr = proAppr;
	}
	
    /**
     * @return proAppr
     */
	public String getProAppr() {
		return this.proAppr;
	}
	
	/**
	 * @param apprNo
	 */
	public void setApprNo(String apprNo) {
		this.apprNo = apprNo;
	}
	
    /**
     * @return apprNo
     */
	public String getApprNo() {
		return this.apprNo;
	}
	
	/**
	 * @param planLicNo
	 */
	public void setPlanLicNo(String planLicNo) {
		this.planLicNo = planLicNo;
	}
	
    /**
     * @return planLicNo
     */
	public String getPlanLicNo() {
		return this.planLicNo;
	}
	
	/**
	 * @param buildLicNo
	 */
	public void setBuildLicNo(String buildLicNo) {
		this.buildLicNo = buildLicNo;
	}
	
    /**
     * @return buildLicNo
     */
	public String getBuildLicNo() {
		return this.buildLicNo;
	}
	
	/**
	 * @param eiaLicNo
	 */
	public void setEiaLicNo(String eiaLicNo) {
		this.eiaLicNo = eiaLicNo;
	}
	
    /**
     * @return eiaLicNo
     */
	public String getEiaLicNo() {
		return this.eiaLicNo;
	}
	
	/**
	 * @param consLicNo
	 */
	public void setConsLicNo(String consLicNo) {
		this.consLicNo = consLicNo;
	}
	
    /**
     * @return consLicNo
     */
	public String getConsLicNo() {
		return this.consLicNo;
	}
	
	/**
	 * @param otherLic
	 */
	public void setOtherLic(String otherLic) {
		this.otherLic = otherLic;
	}
	
    /**
     * @return otherLic
     */
	public String getOtherLic() {
		return this.otherLic;
	}
	
	/**
	 * @param otherLicNo
	 */
	public void setOtherLicNo(String otherLicNo) {
		this.otherLicNo = otherLicNo;
	}
	
    /**
     * @return otherLicNo
     */
	public String getOtherLicNo() {
		return this.otherLicNo;
	}
	
	/**
	 * @param startWorkDate
	 */
	public void setStartWorkDate(String startWorkDate) {
		this.startWorkDate = startWorkDate;
	}
	
    /**
     * @return startWorkDate
     */
	public String getStartWorkDate() {
		return this.startWorkDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}