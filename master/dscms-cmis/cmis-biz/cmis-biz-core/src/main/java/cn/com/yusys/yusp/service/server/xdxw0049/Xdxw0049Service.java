package cn.com.yusys.yusp.service.server.xdxw0049;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0049.req.Xdxw0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0049.resp.Xdxw0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称:
 * @类描述: #智能风控删除通知
 * @功能描述:
 * @创建人: lx
 * @创建时间: 2021/6/14 14:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0049Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0049Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0049DataRespDto xdxw0049(Xdxw0049DataReqDto xdxw0049DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key,
                DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataReqDto));
        Xdxw0049DataRespDto xdxw0049DataRespDto = new Xdxw0049DataRespDto();
        try {
            String survey_serno = xdxw0049DataReqDto.getSurvey_serno();//业务流水号
            logger.info("*****XDXW0049:查询合同表信息开始********业务流水号:" + survey_serno);
            int number = ctrLoanContMapper.selectBySurveySerno(survey_serno);
            logger.info("*****XDXW0049:查询合同表信息结束***********查询结果:" + number);
            if (number > 0) {
                xdxw0049DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作失败标志位
                xdxw0049DataRespDto.setOpMsg(CommonConstance.OP_MSG_F + ":" + "已存在合同！");// 描述信息
            } else {
                logger.info("*****XDXW0049:查询调查信息主表开始***********业务流水号:" + survey_serno);
                int result = lmtSurveyReportMainInfoMapper.updateBySurveySerno(survey_serno);
                logger.info("*****XDXW0049:查询调查信息主表结束***********查询结果:" + result);
                if (result > 0) {
                    xdxw0049DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);// 操作成功标志位
                    xdxw0049DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);// 描述信息
                } else {
                    xdxw0049DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作失败标志位
                    xdxw0049DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);// 描述信息
                }
            }
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, e.getMessage(), e);
            xdxw0049DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作失败标志位
            xdxw0049DataRespDto.setOpMsg(CommonConstance.OP_MSG_F + ":" + e.getMessage());// 描述信息
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataRespDto));
        return xdxw0049DataRespDto;
    }
}
