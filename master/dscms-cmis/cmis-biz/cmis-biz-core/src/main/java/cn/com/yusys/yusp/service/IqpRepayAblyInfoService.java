/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRepayAblyInfo;
import cn.com.yusys.yusp.repository.mapper.IqpRepayAblyInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayAblyInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:16:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpRepayAblyInfoService {

    @Autowired
    private IqpRepayAblyInfoMapper iqpRepayAblyInfoMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpRepayAblyInfo selectByPrimaryKey(String serno) {
        return iqpRepayAblyInfoMapper.selectByPrimaryKey(serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpRepayAblyInfo> selectAll(QueryModel model) {
        List<IqpRepayAblyInfo> records = (List<IqpRepayAblyInfo>) iqpRepayAblyInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpRepayAblyInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayAblyInfo> list = iqpRepayAblyInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpRepayAblyInfo record) {
        return iqpRepayAblyInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpRepayAblyInfo record) {
        return iqpRepayAblyInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpRepayAblyInfo record) {
        return iqpRepayAblyInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpRepayAblyInfo record) {
        return iqpRepayAblyInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return iqpRepayAblyInfoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRepayAblyInfoMapper.deleteByIds(ids);
    }

    public int updateByParams(Map delMap) {
        return iqpRepayAblyInfoMapper.updateByParams(delMap);
    }

    public List<IqpRepayAblyInfo> selectByIqpSerno(String iqpSernoOld) {
        return iqpRepayAblyInfoMapper.selectByIqpSerno(iqpSernoOld);
    }
}
