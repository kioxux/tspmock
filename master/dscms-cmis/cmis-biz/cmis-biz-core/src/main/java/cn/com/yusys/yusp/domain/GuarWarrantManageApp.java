/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantManageApp
 * @类描述: guar_warrant_manage_app数据实体类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:24:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_warrant_manage_app")
public class GuarWarrantManageApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 抵质押分类 **/
	@Column(name = "GRT_FLAG", unique = false, nullable = true, length = 5)
	private String grtFlag;
	
	/** 权证编号 **/
	@Column(name = "WARRANT_NO", unique = false, nullable = true, length = 200)
	private String warrantNo;
	
	/** 权证入库模式 **/
	@Column(name = "WARRANT_IN_TYPE", unique = false, nullable = true, length = 5)
	private String warrantInType;
	
	/** 权证出入库申请类型 **/
	@Column(name = "WARRANT_APP_TYPE", unique = false, nullable = true, length = 5)
	private String warrantAppType;
	
	/** 权证出库原因大类 **/
	@Column(name = "WARRANT_OUT_TYPE", unique = false, nullable = true, length = 5)
	private String warrantOutType;
	
	/** 权证出库原因细类 **/
	@Column(name = "WARRANT_OUT_TYPE_SUB", unique = false, nullable = true, length = 5)
	private String warrantOutTypeSub;
	
	/** 是否出库到集中作业 **/
	@Column(name = "IS_ZHBLZX", unique = false, nullable = true, length = 5)
	private String isZhblzx;
	
	/** 权证预计归还时间 **/
	@Column(name = "PRE_BACK_DATE", unique = false, nullable = true, length = 10)
	private String preBackDate;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 核心抵质押品出入库返回交易流水 **/
	@Column(name = "HX_SERNO", unique = false, nullable = true, length = 40)
	private String hxSerno;
	
	/** 核心抵质押品出入库返回交易日期 **/
	@Column(name = "HX_DATE", unique = false, nullable = true, length = 20)
	private String hxDate;
	
	/** 核心担保编号 **/
	@Column(name = "CORE_GUARANTY_NO", unique = false, nullable = true, length = 40)
	private String coreGuarantyNo;
	
	/** 核心担保品序号 **/
	@Column(name = "CORE_GUARANTY_SEQ", unique = false, nullable = true, length = 40)
	private String coreGuarantySeq;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 抵质押物种类 **/
	@Column(name = "GAG_TYP", unique = false, nullable = true, length = 5)
	private String gagTyp;
	
	/** 是否还款即解押 **/
	@Column(name = "IS_REPAY_REMOVE_GUAR", unique = false, nullable = true, length = 2)
	private String isRepayRemoveGuar;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param grtFlag
	 */
	public void setGrtFlag(String grtFlag) {
		this.grtFlag = grtFlag;
	}
	
    /**
     * @return grtFlag
     */
	public String getGrtFlag() {
		return this.grtFlag;
	}
	
	/**
	 * @param warrantNo
	 */
	public void setWarrantNo(String warrantNo) {
		this.warrantNo = warrantNo;
	}
	
    /**
     * @return warrantNo
     */
	public String getWarrantNo() {
		return this.warrantNo;
	}
	
	/**
	 * @param warrantInType
	 */
	public void setWarrantInType(String warrantInType) {
		this.warrantInType = warrantInType;
	}
	
    /**
     * @return warrantInType
     */
	public String getWarrantInType() {
		return this.warrantInType;
	}
	
	/**
	 * @param warrantAppType
	 */
	public void setWarrantAppType(String warrantAppType) {
		this.warrantAppType = warrantAppType;
	}
	
    /**
     * @return warrantAppType
     */
	public String getWarrantAppType() {
		return this.warrantAppType;
	}
	
	/**
	 * @param warrantOutType
	 */
	public void setWarrantOutType(String warrantOutType) {
		this.warrantOutType = warrantOutType;
	}
	
    /**
     * @return warrantOutType
     */
	public String getWarrantOutType() {
		return this.warrantOutType;
	}
	
	/**
	 * @param warrantOutTypeSub
	 */
	public void setWarrantOutTypeSub(String warrantOutTypeSub) {
		this.warrantOutTypeSub = warrantOutTypeSub;
	}
	
    /**
     * @return warrantOutTypeSub
     */
	public String getWarrantOutTypeSub() {
		return this.warrantOutTypeSub;
	}
	
	/**
	 * @param isZhblzx
	 */
	public void setIsZhblzx(String isZhblzx) {
		this.isZhblzx = isZhblzx;
	}
	
    /**
     * @return isZhblzx
     */
	public String getIsZhblzx() {
		return this.isZhblzx;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate;
	}
	
    /**
     * @return preBackDate
     */
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param hxSerno
	 */
	public void setHxSerno(String hxSerno) {
		this.hxSerno = hxSerno;
	}
	
    /**
     * @return hxSerno
     */
	public String getHxSerno() {
		return this.hxSerno;
	}
	
	/**
	 * @param hxDate
	 */
	public void setHxDate(String hxDate) {
		this.hxDate = hxDate;
	}
	
    /**
     * @return hxDate
     */
	public String getHxDate() {
		return this.hxDate;
	}
	
	/**
	 * @param coreGuarantyNo
	 */
	public void setCoreGuarantyNo(String coreGuarantyNo) {
		this.coreGuarantyNo = coreGuarantyNo;
	}
	
    /**
     * @return coreGuarantyNo
     */
	public String getCoreGuarantyNo() {
		return this.coreGuarantyNo;
	}
	
	/**
	 * @param coreGuarantySeq
	 */
	public void setCoreGuarantySeq(String coreGuarantySeq) {
		this.coreGuarantySeq = coreGuarantySeq;
	}
	
    /**
     * @return coreGuarantySeq
     */
	public String getCoreGuarantySeq() {
		return this.coreGuarantySeq;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param gagTyp
	 */
	public void setGagTyp(String gagTyp) {
		this.gagTyp = gagTyp;
	}
	
    /**
     * @return gagTyp
     */
	public String getGagTyp() {
		return this.gagTyp;
	}
	
	/**
	 * @param isRepayRemoveGuar
	 */
	public void setIsRepayRemoveGuar(String isRepayRemoveGuar) {
		this.isRepayRemoveGuar = isRepayRemoveGuar;
	}
	
    /**
     * @return isRepayRemoveGuar
     */
	public String getIsRepayRemoveGuar() {
		return this.isRepayRemoveGuar;
	}


}