package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.GrtGuarContService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RiskItem0132Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0132Service.class);

    @Autowired
    private GrtGuarContService grtGuarContService;

    public RiskResultDto riskItem0132(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("展期申请是否存在未签订的追加担保合同校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 根据流水号查询追加担保信息
        QueryModel params = new QueryModel();
        params.addCondition("serno",serno);
        List<GrtGuarCont> list = grtGuarContService.selectGuarContByContNo(params);
        for (int i = 0; i < list.size() ; i++) {
            GrtGuarCont grtGuarCont = list.get(i);
            if(!"101".equals(grtGuarCont.getGuarContState())){ // 	生效
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc("存在未签订的追加担保合同,请先签订,担保合同编号:"+grtGuarCont.getGuarContNo());
                return riskResultDto;
            }
        }

        log.info("展期申请是否存在未签订的追加担保合同校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}