/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyChgHis;
import cn.com.yusys.yusp.service.LmtSurveyChgHisService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyChgHisResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-26 10:40:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsurveychghis")
public class LmtSurveyChgHisResource {
    @Autowired
    private LmtSurveyChgHisService lmtSurveyChgHisService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyChgHis>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyChgHis> list = lmtSurveyChgHisService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyChgHis>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyChgHis>> index(QueryModel queryModel) {
        List<LmtSurveyChgHis> list = lmtSurveyChgHisService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyChgHis>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSurveyChgHis> create(@RequestBody LmtSurveyChgHis lmtSurveyChgHis) throws URISyntaxException {
        lmtSurveyChgHisService.insert(lmtSurveyChgHis);
        return new ResultDto<LmtSurveyChgHis>(lmtSurveyChgHis);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyChgHis lmtSurveyChgHis) throws URISyntaxException {
        int result = lmtSurveyChgHisService.update(lmtSurveyChgHis);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String contNo) {
        int result = lmtSurveyChgHisService.deleteByPrimaryKey(pkId, contNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtSurveyChgHis
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/8/26 10:42
     * @version 1.0.0
     * @desc  调查报告替换
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/replace")
    protected ResultDto surveyReplace(@RequestBody LmtSurveyChgHis lmtSurveyChgHis) {
        return lmtSurveyChgHisService.surveyReplace(lmtSurveyChgHis);
    }
}
