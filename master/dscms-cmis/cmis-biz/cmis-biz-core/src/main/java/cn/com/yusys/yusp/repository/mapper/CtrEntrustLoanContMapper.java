/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrEntrustLoanCont;
import cn.com.yusys.yusp.domain.CtrHighAmtAgrCont;
import cn.com.yusys.yusp.dto.CtrEntrustLoanContShowDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrEntrustLoanContMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-05-11 21:53:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrEntrustLoanContMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CtrEntrustLoanCont selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrEntrustLoanCont> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrEntrustLoanCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrEntrustLoanCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrEntrustLoanCont record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrEntrustLoanCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByEntrustLoanSernoKey
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrEntrustLoanCont selectByEntrustLoanSernoKey(String serno);

    /**
     * @方法名称: selectCtrContEntrustByParams
     * @方法描述: 通过查询报表编号查询子表的参数配置
     * @参数与返回说明:
     * @算法描述: 无
     * @return
     */
    List<CtrEntrustLoanContShowDto> selectCtrContEntrustByParams(QueryModel queryModel);

    /**
     * @方法名称: queryCtrEntrustLoanByDataParams
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrEntrustLoanCont queryCtrEntrustLoanByDataParams(HashMap<String, String> queryMap);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrEntrustLoanCont> selectByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * 根据合同编号查询
     * @param contNo
     * @return
     */
    CtrEntrustLoanCont selectByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:12
     * @修改记录：修改时间 修改人员 修改时间
    */
    CtrEntrustLoanCont selectByIqpSerno(@Param("serno") String serno);


    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: selectDataByContNo
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrEntrustLoanCont selectDataByContNo(@Param("contNo") String contNo);

    /**
     * @方法名称: updateLmtAccNoByContNo
     * @方法描述: 根据合同号更新lmtAccNo
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateLmtAccNoByContNo(CtrEntrustLoanCont record);
}