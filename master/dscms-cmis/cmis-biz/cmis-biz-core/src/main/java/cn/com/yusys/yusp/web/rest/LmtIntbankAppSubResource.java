/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.req.CmisLmt0036ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0036.resp.CmisLmt0036RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.LmtIntbankAppService;
import cn.com.yusys.yusp.web.server.xddb0011.BizXddb0011Resource;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankAppSub;
import cn.com.yusys.yusp.service.LmtIntbankAppSubService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankAppSubResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-21 16:09:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankappsub")
public class LmtIntbankAppSubResource {

    private static final Logger logger = LoggerFactory.getLogger(BizXddb0011Resource.class);

    @Autowired
    private LmtIntbankAppSubService lmtIntbankAppSubService;

    @Autowired
    private LmtIntbankAppService lmtIntbankAppService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService ;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankAppSub>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankAppSub> list = lmtIntbankAppSubService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankAppSub>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankAppSub>> index(QueryModel queryModel) {
        List<LmtIntbankAppSub> list = lmtIntbankAppSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankAppSub>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtIntbankAppSub>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtIntbankAppSub> list = lmtIntbankAppSubService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankAppSub>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankAppSub> show(@PathVariable("pkId") String pkId) {
        LmtIntbankAppSub lmtIntbankAppSub = lmtIntbankAppSubService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankAppSub>(lmtIntbankAppSub);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankAppSub> create(@RequestBody LmtIntbankAppSub lmtIntbankAppSub) throws URISyntaxException {
        lmtIntbankAppSubService.insert(lmtIntbankAppSub);
        return new ResultDto<LmtIntbankAppSub>(lmtIntbankAppSub);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankAppSub lmtIntbankAppSub) throws URISyntaxException {
        int result = lmtIntbankAppSubService.update(lmtIntbankAppSub);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankAppSubService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankAppSubService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:logicalDelete
     * @函数描述:产品删除时，数据逻辑删除(既将操作类型更新为“02-删除”)，不进行物理删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/logicalDelete")
    protected ResultDto<Integer> logicalDelete(@RequestBody QueryModel queryModel ) {
        String pkId = (String) queryModel.getCondition().get("pkId");
        int result = lmtIntbankAppSubService.logicDelete(pkId);
        String serno = (String) queryModel.getCondition().get("serno");
        lmtIntbankAppService.updateLmtAmt(serno);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:checksublimitprdid
     * @函数描述:校验产品是否允许新增
     * @参数与返回说明:bizTotalBalanceAmtCny
     * @算法描述:
     */
    @PostMapping("/checksublimitprdid")
    protected ResultDto<Boolean> checkSubLimitPrdId(@RequestBody LmtIntbankAppSub lmtIntbankAppSub) {
        QueryModel model = new QueryModel() ;
        model.addCondition("serno", lmtIntbankAppSub.getSerno());
        model.addCondition("lmtBizType", lmtIntbankAppSub.getLmtBizType());
        model.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        Boolean result = lmtIntbankAppSubService.checkSubLimitPrdId(model) ;
        return new ResultDto<Boolean>(result);
    }

    /**
     * @函数名称:getbiztotalbalamtcny
     * @函数描述:根据分项编号查询分项占用总余额
     * @参数与返回说明:、origiLmtAccSubNo
     * @算法描述:
     */
    @PostMapping("/getbiztotalbalamtcny")
    protected ResultDto<BigDecimal> getBizTotalBalAmtCny(@RequestBody QueryModel queryModel) throws Exception {
        String lmtSubNo = (String) queryModel.getCondition().get("subSerno");
        CmisLmt0036ReqDto reqDto = new CmisLmt0036ReqDto() ;
        reqDto.setAccSubNo(lmtSubNo);
        // 调用额度接口
        logger.info("根据业务申请编号【" + lmtSubNo + "】前往额度系统-建立额度请求报文：" + reqDto.toString());
        ResultDto<CmisLmt0036RespDto> cmisLmt0036RespDtoResultDto  = cmisLmtClientService.cmislmt0036(reqDto) ;

        BigDecimal bal = BigDecimal.ZERO ;
        if (cmisLmt0036RespDtoResultDto.getCode().equals(SuccessEnum.CMIS_SUCCSESS.key)) {
            CmisLmt0036RespDto data = cmisLmt0036RespDtoResultDto.getData();
            if (data != null) {
                bal = data.getBizTotalBalAmtCny() ;
            }
        }

        /*logger.info("根据业务申请编号【" + lmtSubNo + "】前往额度系统-建立额度返回报文：" + cmisLmt0036RespDtoResultDto.toString());
        if (cmisLmt0036RespDtoResultDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0036RespDtoResultDto.getData().getErrorCode())) {
            logger.info("根据分项编号【{}】,获取余额信息成功！", lmtSubNo);
        } else {
            logger.info("根据业务申请编号【{}】,前往额度系统建立额度失败！", lmtSubNo);
            throw new Exception("额度系统-前往额度系统建立额度失败:" + cmisLmt0036RespDtoResultDto.getData().getErrorMsg());
        }*/

        return new ResultDto<BigDecimal>(bal);
    }
}
