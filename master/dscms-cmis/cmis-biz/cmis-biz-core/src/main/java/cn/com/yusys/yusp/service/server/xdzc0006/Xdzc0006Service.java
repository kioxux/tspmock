package cn.com.yusys.yusp.service.server.xdzc0006;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.OpRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.req.Xdzc0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.resp.Xdzc0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 接口处理类:资产池出池校验
 *
 * @Author xs
 * @Date 2021/06/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0006Service {

    @Autowired
    private AsplAssetsListService asplAssetsListService;


    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0006.Xdzc0006Service.class);

    @Transactional
    public Xdzc0006DataRespDto xdzc0006Service(Xdzc0006DataReqDto xdzc0006DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value);
        Xdzc0006DataRespDto xdzc0006DataRespDto = new Xdzc0006DataRespDto();
        //默认失败
        xdzc0006DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
        xdzc0006DataRespDto.setOpMsg("出池额度校验逻辑报错");// 描述信息
        String contNO = xdzc0006DataReqDto.getContNo();
        List<cn.com.yusys.yusp.dto.server.xdzc0006.req.List> list = xdzc0006DataReqDto.getList();
        try {
            List<String> reqAssetNoList = list.stream()
                    .map(e -> e.getAssetNo())
                    .collect(Collectors.toList());
            OpRespDto opRespDto = asplAssetsListService.isOupPoolVaild(contNO,reqAssetNoList);
            xdzc0006DataRespDto.setOpFlag(opRespDto.getOpFlag());// 成功失败标志
            xdzc0006DataRespDto.setOpMsg(opRespDto.getOpMsg());// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, e.getMessage());
        } catch (Exception e){
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value);
        return xdzc0006DataRespDto;
    }
}