package cn.com.yusys.yusp.web.server.xdxw0074;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0074.req.Xdxw0074DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0074.resp.Xdxw0074DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0074.Xdxw0074Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户身份证号查询线上产品申请记录
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0074:根据客户身份证号查询线上产品申请记录")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0074Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0074Resource.class);

    @Autowired
    private Xdxw0074Service xdxw0074Service;

    /**
     * 交易码：xdxw0074
     * 交易描述：根据客户身份证号查询线上产品申请记录
     *
     * @param xdxw0074DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户身份证号查询线上产品申请记录")
    @PostMapping("/xdxw0074")
    protected @ResponseBody
    ResultDto<Xdxw0074DataRespDto> xdxw0074(@Validated @RequestBody Xdxw0074DataReqDto xdxw0074DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataReqDto));
        Xdxw0074DataRespDto xdxw0074DataRespDto = new Xdxw0074DataRespDto();// 响应Dto:根据客户身份证号查询线上产品申请记录
        ResultDto<Xdxw0074DataRespDto> xdxw0074DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0074DataReqDto获取业务值进行业务逻辑处理
            // 证件号
            String certNo = xdxw0074DataReqDto.getCertNo();
            if (StringUtils.isEmpty(certNo)) {
                xdxw0074DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0074DataResultDto.setMessage(EpbEnum.EPB099999.value);
                return xdxw0074DataResultDto;
            }
            xdxw0074DataRespDto = xdxw0074Service.xdxw0074(xdxw0074DataReqDto);
            // 封装xdxw0074DataResultDto中正确的返回码和返回信息
            xdxw0074DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0074DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, e.getMessage());
            // 封装xdxw0074DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdxw0074DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0074DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdxw0074DataRespDto到xdxw0074DataResultDto中
        xdxw0074DataResultDto.setData(xdxw0074DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataRespDto));
        return xdxw0074DataResultDto;
    }
}
