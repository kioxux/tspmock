/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptOperEnergySales;
import cn.com.yusys.yusp.repository.mapper.RptOperEnergySalesMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySalesService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:44:08
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptOperEnergySalesService {

    @Autowired
    private RptOperEnergySalesMapper rptOperEnergySalesMapper;
    @Autowired
    private ICusClientService iCusClientService;

    private static final Logger log = LoggerFactory.getLogger(RptOperEnergySalesService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptOperEnergySales selectByPrimaryKey(String serno) {
        return rptOperEnergySalesMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptOperEnergySales> selectAll(QueryModel model) {
        List<RptOperEnergySales> records = (List<RptOperEnergySales>) rptOperEnergySalesMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptOperEnergySales> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptOperEnergySales> list = rptOperEnergySalesMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptOperEnergySales record) {
        return rptOperEnergySalesMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptOperEnergySales record) {
        return rptOperEnergySalesMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptOperEnergySales record) {
        return rptOperEnergySalesMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptOperEnergySales record) {
        return rptOperEnergySalesMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptOperEnergySalesMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptOperEnergySalesMapper.deleteByIds(ids);
    }

    /**
     * 初始化信息
     *
     * @param map
     * @return
     */
    public Integer initSales(Map map) {
        String serno = map.get("serno").toString();
        String cusId = map.get("cusId").toString();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        RptOperEnergySales rptOperEnergySales = new RptOperEnergySales();
        rptOperEnergySales.setSerno(serno);
        ResultDto<List<FinanIndicAnalyDto>> rptFncTotalProfit = iCusClientService.getRptFncTotalProfit(map);
        if (rptFncTotalProfit != null && rptFncTotalProfit.getData() != null) {
            List<FinanIndicAnalyDto> data = rptFncTotalProfit.getData();
            for (FinanIndicAnalyDto finanIndicAnalyDto : data) {
                if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                    if ("销售收入".equals(finanIndicAnalyDto.getItemName())) {
                        String inputYear = finanIndicAnalyDto.getInputYear();
                        Map param1 = new HashMap();
                        param1.put("cusId", cusId);
                        param1.put("statPrd", inputYear);
                        //获取当年财报类型
                        ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                        log.info("获取当年财报类型[{}]", JSON.toJSONString(mapResultDto1));
                        if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                            Map data1 = mapResultDto1.getData();
                            Object reportType = data1.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    log.info("当年财报报表类型为税务报表");
                                    rptOperEnergySales.setCurrAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    log.info("当年财报报表类型为自制报表");
                                    rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                } else {
                                    log.info("当年财报无报表类型");
                                    rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            } else {
                                log.info("当年财报无报表类型");
                                rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            }
                        }
                        String lastYear = (String.valueOf((Integer.parseInt(inputYear.substring(0, 4)) - 1))).concat("12");
                        Map param2 = new HashMap();
                        param2.put("cusId", cusId);
                        param2.put("statPrd", lastYear);
                        //获去年财报类型
                        ResultDto<Map> mapResultDto2 = iCusClientService.selectFncByCusIdRptYear(param2);
                        log.info("获取去年财报类型[{}]", JSON.toJSONString(mapResultDto2));
                        if (mapResultDto2 != null && mapResultDto2.getData() != null) {
                            Map data2 = mapResultDto2.getData();
                            Object reportType = data2.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    log.info("去年财报报表类型为税务报表");
                                    rptOperEnergySales.setNearFirstAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    log.info("去年财报报表类型为自制报表");
                                    rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                } else {
                                    log.info("去年财报无报表类型");
                                    rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                }
                            } else {
                                log.info("去年财报无报表类型");
                                rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            }
                        }
                        String lastSecondYear = (String.valueOf((Integer.parseInt(inputYear.substring(0, 4)) - 2))).concat("12");
                        Map param3 = new HashMap();
                        param3.put("cusId", cusId);
                        param3.put("statPrd", lastSecondYear);
                        //获取前年财报类型
                        ResultDto<Map> mapResultDto3 = iCusClientService.selectFncByCusIdRptYear(param3);
                        log.info("获取前年财报类型[{}]", JSON.toJSONString(mapResultDto3));
                        if (mapResultDto3 != null && mapResultDto3.getData() != null) {
                            Map data3 = mapResultDto3.getData();
                            Object reportType = data3.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    log.info("前年财报报表类型为税务报表");
                                    rptOperEnergySales.setNearSecondAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    log.info("前年财报报表类型为自制报表");
                                    rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                } else {
                                    log.info("前年财报无报表类型");
                                    rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                }
                            } else {
                                log.info("前年财报无报表类型");
                                rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            }
                        }
                        BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        if (rptOperEnergySales.getNearSecondAppSaleIncome() != null && rptOperEnergySales.getNearFirstAppSaleIncome() != null) {
                            rptOperEnergySales.setAppSaleIncomeDiff(lastdiff);
                        } else if (rptOperEnergySales.getNearSecondRealSaleIncome() != null && rptOperEnergySales.getNearFirstRealSaleIncome() != null) {
                            rptOperEnergySales.setRealSaleIncomeDiff(lastdiff);
                        }
                        if (rptOperEnergySales.getCurrAppSaleIncome() != null && rptOperEnergySales.getNearFirstAppSaleIncome() != null) {
                            rptOperEnergySales.setAppSaleIncomeLastYearDiff(diff);
                        } else if (rptOperEnergySales.getCurrRealSaleIncome() != null && rptOperEnergySales.getNearFirstRealSaleIncome() != null) {
                            rptOperEnergySales.setRealSaleIncomeLastYearDiff(diff);
                        }
                        rptOperEnergySales.setInputYear(finanIndicAnalyDto.getInputYear());
                        break;
                    }
                } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                    if ("年度销售收入".equals(finanIndicAnalyDto.getItemName())) {
                        String inputYear = finanIndicAnalyDto.getInputYear();
                        Map param1 = new HashMap();
                        param1.put("cusId", cusId);
                        param1.put("statPrd", inputYear);
                        //获取当年财报类型
                        ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                        if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                            Map data1 = mapResultDto1.getData();
                            Object reportType = data1.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    rptOperEnergySales.setCurrAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                                else {
                                    rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            } else {
                                rptOperEnergySales.setCurrRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            }
                        }
                        String lastYear = (String.valueOf((Integer.parseInt(inputYear.substring(0, 4)) - 1))).concat("12");
                        Map param2 = new HashMap();
                        param2.put("cusId", cusId);
                        param2.put("statPrd", lastYear);
                        ResultDto<Map> mapResultDto2 = iCusClientService.selectFncByCusIdRptYear(param2);
                        if (mapResultDto2 != null && mapResultDto2.getData() != null) {
                            Map data2 = mapResultDto2.getData();
                            Object reportType = data2.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    rptOperEnergySales.setNearFirstAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                }
                                else {
                                    rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                                }
                            } else {
                                rptOperEnergySales.setNearFirstRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                            }
                        }
                        String lastSecondYear = (String.valueOf((Integer.parseInt(inputYear.substring(0, 4)) - 2))).concat("12");
                        Map param3 = new HashMap();
                        param3.put("cusId", cusId);
                        param3.put("statPrd", lastSecondYear);
                        ResultDto<Map> mapResultDto3 = iCusClientService.selectFncByCusIdRptYear(param3);
                        if (mapResultDto3 != null && mapResultDto3.getData() != null) {
                            Map data3 = mapResultDto3.getData();
                            Object reportType = data3.get("reportType");
                            if (Objects.nonNull(reportType)) {
                                String fncType = reportType.toString();
                                if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                    rptOperEnergySales.setNearSecondAppSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                    rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                }
                                else {
                                    rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                                }
                            } else {
                                rptOperEnergySales.setNearSecondRealSaleIncome(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                            }
                        }
                        BigDecimal lastdiff = (Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearSecondValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearSecondValue());
                        BigDecimal diff = (Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue()).subtract(Objects.isNull(finanIndicAnalyDto.getNearFirstValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getNearFirstValue());
                        if (rptOperEnergySales.getNearSecondAppSaleIncome() != null && rptOperEnergySales.getNearFirstAppSaleIncome() != null) {
                            rptOperEnergySales.setAppSaleIncomeDiff(lastdiff);
                        } else if (rptOperEnergySales.getNearSecondRealSaleIncome() != null && rptOperEnergySales.getNearFirstRealSaleIncome() != null) {
                            rptOperEnergySales.setRealSaleIncomeDiff(lastdiff);
                        }
                        if (rptOperEnergySales.getCurrAppSaleIncome() != null && rptOperEnergySales.getNearFirstAppSaleIncome() != null) {
                            rptOperEnergySales.setAppSaleIncomeLastYearDiff(diff);
                        } else if (rptOperEnergySales.getCurrRealSaleIncome() != null && rptOperEnergySales.getNearFirstRealSaleIncome() != null) {
                            rptOperEnergySales.setRealSaleIncomeLastYearDiff(diff);
                        }
                        rptOperEnergySales.setInputYear(finanIndicAnalyDto.getInputYear());
                        break;
                    }
                }

            }
        }
        return insert(rptOperEnergySales);
    }
}
