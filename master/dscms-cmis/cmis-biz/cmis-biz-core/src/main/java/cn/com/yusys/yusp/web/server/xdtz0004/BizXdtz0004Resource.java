package cn.com.yusys.yusp.web.server.xdtz0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0004.req.Xdtz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.Xdtz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0004.Xdtz0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:在查询经营性贷款借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0004:在查询经营性贷款借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0004Resource.class);

    @Autowired
    private Xdtz0004Service xdtz0004Service;

    /**
     * 交易码：xdtz0004
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param xdtz0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("在查询经营性贷款借据信息")
    @PostMapping("/xdtz0004")
    protected @ResponseBody
    ResultDto<Xdtz0004DataRespDto> xdtz0004(@Validated @RequestBody Xdtz0004DataReqDto xdtz0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataReqDto));
        Xdtz0004DataRespDto xdtz0004DataRespDto = new Xdtz0004DataRespDto();// 响应Dto:在查询经营性贷款借据信息
        ResultDto<Xdtz0004DataRespDto> xdtz0004DataResultDto = new ResultDto<>();
        try {
            // 从xdtz0004DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataReqDto));
            xdtz0004DataRespDto = xdtz0004Service.xdtz0004(xdtz0004DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataRespDto));
            // 封装xdtz0004DataResultDto中正确的返回码和返回信息
            xdtz0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, e.getMessage());
            // 封装xdtz0004DataResultDto中异常返回码和返回信息
            xdtz0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0004DataRespDto到xdtz0004DataResultDto中
        xdtz0004DataResultDto.setData(xdtz0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataResultDto));
        return xdtz0004DataResultDto;
    }
}
