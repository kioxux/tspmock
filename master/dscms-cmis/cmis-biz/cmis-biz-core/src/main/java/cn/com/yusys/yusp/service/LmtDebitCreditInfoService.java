package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.LmtDebitCreditInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;
import cn.com.yusys.yusp.domain.dto.DebitAndComDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.repository.mapper.LmtDebitCreditInfoMapper;
import cn.com.yusys.yusp.service.client.bsp.ciis2nd.credzb.CredzbService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtDebitCreditInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-15 22:24:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtDebitCreditInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtDebitCreditInfoService.class);

    @Autowired
    private LmtDebitCreditInfoMapper lmtDebitCreditInfoMapper;
    @Autowired
    private CredzbService credzbService;
    @Autowired
    private LmtSurveyReportBasicInfoService basicInfoService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private LmtSurveyReportComInfoService lmtSurveyReportComInfoService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtDebitCreditInfo selectByPrimaryKey(String surveyNo) {
        return lmtDebitCreditInfoMapper.selectByPrimaryKey(surveyNo);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtDebitCreditInfo> selectAll(QueryModel model) {
        List<LmtDebitCreditInfo> records = (List<LmtDebitCreditInfo>) lmtDebitCreditInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtDebitCreditInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtDebitCreditInfo> list = lmtDebitCreditInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtDebitCreditInfo record) {
        return lmtDebitCreditInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtDebitCreditInfo record) {
        return lmtDebitCreditInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtDebitCreditInfo record) {
        return lmtDebitCreditInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtDebitCreditInfo record) {
        return lmtDebitCreditInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveyNo) {
        return lmtDebitCreditInfoMapper.deleteByPrimaryKey(surveyNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtDebitCreditInfoMapper.deleteByIds(ids);
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto
     * @author hubp
     * @date 2021/5/24 15:45
     * @version 1.0.0
     * @desc 根据征信风险指标查询信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CredzbRespDto selectDebit(String surveySerno) {
        logger.info("根据征信风险指标查询信息开始..................");
        CredzbRespDto credzbRespDto = null;
        try {
            CredzbReqDto credzbReqDto = new CredzbReqDto();
            //根据流水号查询相关信息
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(surveySerno);
            credzbReqDto.setCustomName(iqpLoanApp.getCusName());
            credzbReqDto.setCertificateNum(iqpLoanApp.getCertCode());
            credzbReqDto.setRuleCode("R008");
            credzbReqDto.setBrchno(iqpLoanApp.getManagerBrId());
            credzbRespDto = credzbService.credzb(credzbReqDto);
        } catch (YuspException e) {
            logger.info("根据征信风险指标查询信息失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("根据征信风险指标查询信息结束..................");
        }
        credzbRespDto.getResult();
        return credzbRespDto;
    }

    /**
     * @param surveyNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/7/1 20:12
     * @version 1.0.0
     * @desc 优企贷---查询征信和企业信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto selectDebitAndCom(String surveyNo) {
        DebitAndComDto DebitAndComDto = new DebitAndComDto();
        LmtDebitCreditInfo lmtDebitCreditInfo = lmtDebitCreditInfoMapper.selectByPrimaryKey(surveyNo);
        LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(surveyNo);
        if (null != lmtDebitCreditInfo) {
            DebitAndComDto.setLmtDebitCreditInfo(lmtDebitCreditInfo);
        }
        if (null != lmtSurveyReportComInfo) {
            DebitAndComDto.setLmtSurveyReportComInfo(lmtSurveyReportComInfo);
        }
        return new ResultDto(DebitAndComDto);
    }
}
