package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppr;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @param
 * @author shenli
 * @version 1.0.0
 * @return
 * @date 2021-9-14 14:56:01
 * @desc 零售授信申请审批流程-东海
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DHCZ07BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ07BizService.class);
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private IqpLoanApprService iqpLoanApprService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        String bizType = resultInstanceDto.getBizType();
        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if ("DHE01".equals(bizType)) {
            handlerDHE01(resultInstanceDto);
        } else if ("DHE02".equals(bizType)) {
            handlerDHE02(resultInstanceDto);
        } else if ("DHE03".equals(bizType)) {
            handlerDHE03(resultInstanceDto);
        }
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType);
    }

    //零售业务授信新增（东海）
    private void handlerDHE01(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        String nodeId = resultInstanceDto.getNodeId();
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        try {
            updatFlowParam(resultInstanceDto,iqpLoanApp);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                saveOrUpdate(iqpLoanApp,nodeId);
                occupy(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
                // 资料未全生成影像补扫任务
/*                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,extSerno);
                createImageSpplInfo(extSerno,resultInstanceDto.getBizType(),iqpLoanApp,resultInstanceDto.getInstanceId());*/
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno, CmisCommonConstants.WF_STATUS_997);
                // 资料未全生成影像补扫任务
/*                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,extSerno);
                createImageSpplInfo(extSerno,resultInstanceDto.getBizType(),iqpLoanApp,resultInstanceDto.getInstanceId());*/
                sendMessage(resultInstanceDto,"通过");
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(extSerno);
                log.info("审批权限节点",resultInstanceDto.getNodeName()+"-----"+extSerno);
                lmtCrdReplyInfo.setApprAuth(resultInstanceDto.getNodeName());
                lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                restore(extSerno);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    //SGE02零售业务授信复议（东海）
    private void handlerDHE02(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        String nodeId = resultInstanceDto.getNodeId();
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        try {
            updatFlowParam(resultInstanceDto,iqpLoanApp);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                saveOrUpdate(iqpLoanApp,nodeId);
                occupy(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno, CmisCommonConstants.WF_STATUS_997);
                sendMessage(resultInstanceDto,"通过");
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(extSerno);
                log.info("审批权限节点",resultInstanceDto.getNodeName()+"-----"+extSerno);
                lmtCrdReplyInfo.setApprAuth(resultInstanceDto.getNodeName());
                lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                restore(extSerno);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    //SGE03零售业务授信变更（东海）
    private void handlerDHE03(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        try {
            updatFlowParam(resultInstanceDto,iqpLoanApp);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                occupy(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno, CmisCommonConstants.WF_STATUS_997);
                sendMessage(resultInstanceDto,"通过");
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(extSerno);
                log.info("审批权限节点",resultInstanceDto.getNodeName()+"-----"+extSerno);
                lmtCrdReplyInfo.setApprAuth(resultInstanceDto.getNodeName());
                lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    sendMessage(resultInstanceDto,"退回");
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                boolean isFirstNode = BizCommonUtils.isFirstNodeCheck(resultInstanceDto);
                if (isFirstNode) {
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                restore(extSerno);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                log.info("发送异常消息开始:" + resultInstanceDto);
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
                log.info("发送异常消息开始结束");
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-5-14
     * @注释
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.DHCZ07.equals(flowCode);

    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-14
     * @注释 审批状态更换   替换对应的 service
     */
    public void updateStatus(String serno, String state)  throws Exception {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
        iqpLoanApp.setApproveStatus(state);
        iqpLoanAppService.updateSelective(iqpLoanApp);
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-14
     * @注释 审批通过 替换对应的service和 自己对应的修改完成后的方法 reviewend
     */
    public void end(String extSerno, String state) throws Exception {
        //审批通过的操作
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        iqpLoanApp.setApproveStatus(state);
        iqpLoanAppService.updateSelective(iqpLoanApp);
        iqpLoanAppService.handleBizEnd(iqpLoanApp);
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-14
     * @注释 第三方额度占用
     */
    public void occupy (String extSerno) throws Exception {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
        String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
        String  approveStatus = iqpLoanApp.getApproveStatus();
        log.info("根据业务申请编号【"+extSerno+"】前往额度系统-第三方额度开始" + approveStatus);
        if("10".equals(prdType) && ("000".equals(approveStatus) || "992".equals(approveStatus))){//零售按揭
            String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识
            //第三方额度占用
            if("1".equalsIgnoreCase(thirdPartyFlag)){//第三方启用
                String proNo = iqpLoanApp.getProNo();//项目编号
                String proSerno = iqpLoanApp.getTdpAgrNo();//项目流水号

                //1. 向额度系统发送接口：校验额度
                CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0009ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0009ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//合同编号
                cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
                cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
                cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
                cmisLmt0009ReqDto.setBizAttr("1");//交易属性
                cmisLmt0009ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0009ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0009ReqDto.setInputDate(iqpLoanApp.getInputDate());
                //cmisLmt0009ReqDto.setOrigiDealBizNo(iqpLoanApp.getIqpSerno());//原交易业务编号
                //cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                //cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                //cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
                cmisLmt0009ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
                cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                cmisLmt0009ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                cmisLmt0009ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                cmisLmt0009OccRelListReqDto.setLmtSubNo(proSerno);//额度分项编号
                cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
                cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
                cmisLmt0009ReqDto.setBussStageType("01");//申请阶段

                log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验请求报文："+ cmisLmt0009ReqDto.toString());
                ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验返回报文："+ cmisLmt0009RespDto.toString());

                String code =  cmisLmt0009RespDto.getData().getErrorCode();

                if("0000".equals(code)){
                    log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验成功！", iqpLoanApp.getIqpSerno());
                    //2. 向额度系统发送接口：占用额度
                    CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                    cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0011ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//业务流水号
                    cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                    cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                    cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                    cmisLmt0011ReqDto.setBizAttr("1");//交易属性
                    cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                    cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                    cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
                    cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
                    cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
                    //cmisLmt0011ReqDto.setOrigiDealBizNo("");//原交易业务编号
                    //cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                    //cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                    //cmisLmt0011ReqDto.setOrigiBizAttr("1");//原交易属性
                    cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
                    cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                    cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                    cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                    cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
                    cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                    cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                    cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
                    cmisLmt0011ReqDto.setBelgLine("04");//零售条线
                    cmisLmt0011ReqDto.setBussStageType("01");//申请阶段


                    List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
                    CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                    cmisLmt0011OccRelListDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                    cmisLmt0011OccRelListDto.setLmtSubNo(proSerno);//额度分项编号
                    cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
                    cmisLmt0011OccRelListDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                    cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                    cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                    cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用请求报文："+ cmisLmt0011ReqDto.toString());
                    ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用返回报文："+ cmisLmt0011RespDto.toString());
                    code =  cmisLmt0011RespDto.getData().getErrorCode();

                    if("0000".equals(code)){
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用成功！", iqpLoanApp.getIqpSerno());
                    } else {
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用异常！", iqpLoanApp.getIqpSerno());
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度占用异常:"+cmisLmt0011RespDto.getData().getErrorMsg());
                    }
                } else if(!"0000".equals(code) && !"70125".equals(code)){
                    log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验失败！", iqpLoanApp.getIqpSerno());
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度校验失败:"+cmisLmt0009RespDto.getData().getErrorMsg());
                }
            }
        }
    }

    /**
     * @创建人 shenli
     * @创建时间 2021-5-14
     * @注释 第三方额度恢复
     */
    public void restore(String extSerno) throws Exception {
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(extSerno);
        ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
        String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
        if("10".equals(prdType)){//零售按揭
            String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识
            if ("1".equalsIgnoreCase(thirdPartyFlag)) {
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getIqpSerno());//业务编号
                cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(iqpLoanApp.getAppAmt());// 恢复敞口金额(人民币)
                cmisLmt0012ReqDto.setRecoverAmtCny(iqpLoanApp.getAppAmt());//恢复总额(人民币)
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                cmisLmt0012ReqDto.setIsRecoverCoop("1");

                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-第三方额度恢复请求报文：" + cmisLmt0012ReqDto.toString());
                ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-第三方额度恢复返回报文：" + cmisLmt0012RespDto.toString());
                String code = cmisLmt0012RespDto.getData().getErrorCode();
                if ("0000".equals(code)) {
                    log.info("根据业务申请编号【{}】,前往额度系统调第三方额度恢复成功！", iqpLoanApp.getIqpSerno());
                } else {
                    log.info("根据业务申请编号【{}】,前往额度系统调第三方额度恢复失败！", iqpLoanApp.getIqpSerno());
                    throw new Exception("额度系统-第三方额度恢复异常:" + cmisLmt0012RespDto.getData().getErrorMsg());
                }
            }
        }
    }


/*    public void createCentralFileTask (String  iqpSerno,String bizType,ResultInstanceDto resultInstanceDto){
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
            try {
                log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发起临时档案开始");
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(iqpLoanApp.getInputBrId());
                String orgType = resultDto.getData().getOrgType();
                //        0-总行部室
                //        1-异地支行（有分行）
                //        2-异地支行（无分行）
                //        3-异地分行
                //        4-中心支行
                //        5-综合支行
                //        6-对公支行
                //        7-零售支行
                if(!"1".equals(orgType) && !"2".equals(orgType) && !"3".equals(orgType) &&!"8".equals(orgType) && !"9".equals(orgType) && !"A".equals(orgType)){

                    //新增临时档案任务
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(iqpLoanApp.getIqpSerno());
                    if("LS002".equals(bizType)){//复议传原流水号
                        centralFileTaskdto.setTraceId(iqpLoanApp.getOldIqpSerno());
                    }else{
                        centralFileTaskdto.setTraceId(iqpLoanApp.getContNo());
                    }
                    centralFileTaskdto.setCusId(iqpLoanApp.getCusId());
                    centralFileTaskdto.setCusName(iqpLoanApp.getCusName());
                    centralFileTaskdto.setBizType(bizType); //
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    centralFileTaskdto.setInputId(iqpLoanApp.getInputId());
                    centralFileTaskdto.setInputBrId(iqpLoanApp.getInputBrId());
                    centralFileTaskdto.setOptType("02"); // 非纯指令
                    centralFileTaskdto.setTaskType("02"); // 档案暂存
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                    log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发起临时档案发送成功");
                }
                log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发起临时档案结束");
            }catch (Exception e){
                log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-归档异常------------------"+e);
            }
    }*/

    /**
     * 资料未全生成影像补扫任务
     * @author jijian_yx
     * @date 2021/8/27 20:43
     **/
/*    private void createImageSpplInfo(String extSerno, String bizType, IqpLoanApp iqpLoanApp, String instanceId) {
        //查询审批结果
        BusinessInformation businessInformation = businessInformationService.selectByPrimaryKey(extSerno, bizType);
        if (null != businessInformation && "0".equals(businessInformation.getComplete())) {
            // 资料不齐全
            DocImageSpplClientDto docImageSpplClientDto = new DocImageSpplClientDto();
            docImageSpplClientDto.setBizSerno(extSerno);// 关联业务流水号
            docImageSpplClientDto.setBizInstanceId(instanceId);// 原业务流程实例
            docImageSpplClientDto.setSpplType(CmisCommonConstants.STD_SPPL_TYPE_03);// 影像补扫类型 03:授信类型资料补录
            docImageSpplClientDto.setSpplBizType(CmisCommonConstants.STD_SPPL_BIZ_TYPE_07);
            docImageSpplClientDto.setCusId(iqpLoanApp.getCusId());// 客户号
            docImageSpplClientDto.setCusName(iqpLoanApp.getCusName());// 客户名称
            docImageSpplClientDto.setContNo(iqpLoanApp.getContNo());// 合同编号
            docImageSpplClientDto.setInputId(iqpLoanApp.getManagerId());// 登记人
            docImageSpplClientDto.setInputBrId(iqpLoanApp.getManagerBrId());// 登记机构
            docImageSpplClientDto.setPrdId(iqpLoanApp.getPrdId());
            docImageSpplClientDto.setPrdName(iqpLoanApp.getPrdName());
            docImageSpplInfoService.createDocImageSpplBySys(docImageSpplClientDto);
        }
    }*/


    /**
     * 发送短信
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void sendMessage(ResultInstanceDto resultInstanceDto,String result) {
        String iqpSerno = resultInstanceDto.getBizId();
        log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信开始------------------");
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
        String managerId = iqpLoanApp.getManagerId();
        try {
            String mgrTel = "";
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            String messageType = "MSG_LS_M_0001";// 短信编号
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", resultInstanceDto.getBizUserName());
            paramMap.put("flowName", "零售业务授信");
            paramMap.put("result", result);
            //执行发送借款人操作
            log.info("零售业务授信审批流程："+"流水号："+resultInstanceDto.getBizId()+"-发送短信------------------");
            ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
        } catch (Exception e) {
            log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信失败------------------"+e);
        }

        log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信结束------------------");
    }


    /**
     * 更新流程变量-审批状态
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void updatFlowParam (ResultInstanceDto resultInstanceDto,IqpLoanApp iqpLoanApp) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        Map<String, Object> params = new HashMap<>();
        params = resultInstanceDto.getParam();
        log.info("流程变量前:" + params.toString());
        params.put("appAmt",iqpLoanApp.getAppAmt());
        log.info("流程变量后:" + params.toString());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    /**
     * 发起人默认插一条 业务申请审批表信息
     * @author shenli
     * @date 2021-10-15 21:42:38
     **/
    private void saveOrUpdate(IqpLoanApp iqpLoanApp,String nodeId) {

        if ("706_4".equals(nodeId)) {
            IqpLoanAppr iqpLoanAppr = new IqpLoanAppr();
            iqpLoanAppr.setPkId("");
            iqpLoanAppr.setIqpSerno(iqpLoanApp.getIqpSerno());
            iqpLoanAppr.setReplyAmt(iqpLoanApp.getAppAmt());
            iqpLoanAppr.setReplyTerm(iqpLoanApp.getAppTerm().intValue());
            iqpLoanAppr.setRepayMode(iqpLoanApp.getRepayMode());
            iqpLoanAppr.setGuarMode(iqpLoanApp.getGuarWay());
            iqpLoanAppr.setReplyRate(iqpLoanApp.getExecRateYear());
            iqpLoanAppr.setIsUpperApprAuth("0");
            iqpLoanAppr.setApprovePost(nodeId);
            //登记人
            iqpLoanAppr.setUpdId(iqpLoanApp.getInputId());
            //登记人
            iqpLoanAppr.setInputId(iqpLoanApp.getInputId());
            //登记机构
            iqpLoanAppr.setInputBrId(iqpLoanApp.getInputBrId());
            //最后修改机构
            iqpLoanAppr.setUpdBrId(iqpLoanApp.getInputBrId());
            iqpLoanAppr.setApproveConclusion(OpType.RUN);
            //登记日期
            iqpLoanAppr.setInputDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            //最后修改日期
            iqpLoanAppr.setUpdDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            //创建时间
            iqpLoanAppr.setCreateTime(new Date());
            //修改日期
            iqpLoanAppr.setUpdateTime(new Date());
            iqpLoanApprService.saveOrUpdate(iqpLoanAppr);
        }
    }
}

