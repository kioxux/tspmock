/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.RptPersonalBusiness;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptBasicInfoAsso;
import cn.com.yusys.yusp.repository.mapper.RptBasicInfoAssoMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoAssoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:30:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptBasicInfoAssoService {

    @Autowired
    private RptBasicInfoAssoMapper rptBasicInfoAssoMapper;

    @Autowired
    private RptPersonalBusinessService rptPersonalBusinessService;

    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private AdminSmPropService adminSmPropService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptBasicInfoAsso selectByPrimaryKey(String serno) {
        return rptBasicInfoAssoMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptBasicInfoAsso> selectAll(QueryModel model) {
        List<RptBasicInfoAsso> records = (List<RptBasicInfoAsso>) rptBasicInfoAssoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptBasicInfoAsso> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptBasicInfoAsso> list = rptBasicInfoAssoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptBasicInfoAsso record) {
        return rptBasicInfoAssoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptBasicInfoAsso record) {
        return rptBasicInfoAssoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptBasicInfoAsso record) {
        return rptBasicInfoAssoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptBasicInfoAsso record) {
        return rptBasicInfoAssoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptBasicInfoAssoMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptBasicInfoAssoMapper.deleteByIds(ids);
    }


    public int updateRptInfo(RptBasicInfoAssoAndPersonalBusinessDto model) {
        RptPersonalBusiness rptPersonalBusiness = model.getRptPersonalBusiness();
        RptBasicInfoAsso rptBasicInfoAsso = model.getRptBasicInfoAsso();
        int count = 0;
        if (rptBasicInfoAssoMapper.updateByPrimaryKey(rptBasicInfoAsso) > 0) {
            count++;
        }
        if (rptPersonalBusinessService.update(rptPersonalBusiness) > 0) {
            count++;
        }
        return count;
    }

    /**
     * 引入客户管理数据
     *
     * @param serno
     * @return
     */
    public int initAsso(String serno) {
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        String cusId = lmtApp.getCusId();
        ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
        RptBasicInfoAsso rptBasicInfoAsso = new RptBasicInfoAsso();
        if (cusCorpDtoResultDto != null && cusCorpDtoResultDto.getData() != null) {
            CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
            BeanUtils.beanCopy(cusCorpDto, rptBasicInfoAsso);
            //企业性质
            rptBasicInfoAsso.setCorpCha(cusCorpDto.getCorpQlty());
            //注册地址
            rptBasicInfoAsso.setComRegAdd(cusCorpDto.getRegiAddr());
            //经营范围
            if(StringUtils.nonBlank(cusCorpDto.getMainOptScp())){
                rptBasicInfoAsso.setNatBusi(cusCorpDto.getMainOptScp());
            }
            if(StringUtils.nonBlank(cusCorpDto.getCorpScale())){
                if(CmisCusConstants.STD_ZB_CUS_SCALE_3.equals(cusCorpDto.getCorpScale())||CmisCusConstants.STD_ZB_CUS_SCALE_4.equals(cusCorpDto.getCorpScale())){
                    rptBasicInfoAsso.setSmallInd(CmisCommonConstants.STD_ZB_YES_NO_1);
                }else {
                    rptBasicInfoAsso.setSmallInd(CmisCommonConstants.STD_ZB_YES_NO_0);
                }
            }
        }

        //获取实际控制人
        Map<String, Object> map1 = new HashMap<>();
        map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
        map1.put("cusIdRel", cusId);
        ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType = cmisCusClientService.getCusCorpMgrByParams(map1);
        if (cusCorpMgrByMrgType != null && cusCorpMgrByMrgType.getData() != null) {
            String realOperCusId = cusCorpMgrByMrgType.getData().getMrgName();
            rptBasicInfoAsso.setRealOperCusId(realOperCusId);
        }
        //获取法人代表
        Map<String, Object> map2 = new HashMap<>();
        map2.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
        map2.put("cusIdRel", cusId);
        ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType1 = cmisCusClientService.getCusCorpMgrByParams(map2);
        if (cusCorpMgrByMrgType1 != null && cusCorpMgrByMrgType1.getData() != null) {
            String legal = cusCorpMgrByMrgType1.getData().getMrgName();
            rptBasicInfoAsso.setLegal(legal);
        }

        //主体评级
        ResultDto<Map<String, String>> gradeInfoByCusId = iCusClientService.selectGradeInfoByCusId(cusId);
        if (gradeInfoByCusId != null && gradeInfoByCusId.getData() != null) {
            String finalRank = gradeInfoByCusId.getData().get("finalRank");
            rptBasicInfoAsso.setSubjectLevel(finalRank);
        }
        //现五级分类
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        model.setSort("inputDate desc");
        List<AccLoan> accLoans = accLoanService.selectByModel(model);
        if(CollectionUtils.nonEmpty(accLoans)){
            AccLoan accLoan = accLoans.get(0);
            String fiveClass = accLoan.getFiveClass();
            if(StringUtils.nonBlank(fiveClass)){
                rptBasicInfoAsso.setCurrentFiveClass(fiveClass);
            }
        }
        if(StringUtils.nonBlank(rptBasicInfoAsso.getTradeClass())){
            String tradeClass = rptBasicInfoAsso.getTradeClass();
            //是否制造业
            if(tradeClass.startsWith("C")){
                rptBasicInfoAsso.setManuInd(CmisCommonConstants.STD_ZB_YES_NO_1);
            }else{
                rptBasicInfoAsso.setManuInd(CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            //两高一剩
            // 两高一剩行业类别集合
            String twoHightOneLeftInd = "";
            //获取系统参数获取
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(CmisCommonConstants.TWO_HIGH_ONE_LEFT_IND);
            AdminSmPropDto adminSmPropDto= adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
            if(Objects.isNull(adminSmPropDto) || StringUtils.isBlank(adminSmPropDto.getPropValue())) {
                // 棉印染精加工,毛染整精加工,麻染整精加工,丝印染精加工,化纤织物染整精加工,皮革鞣制加工,毛皮鞣制加工,
                // 木竹浆制造,非木竹浆制造,炼焦,无机酸制造,无机碱制造,电石*,甲醇*,有机硅单体*,黄磷*,氮肥制造,磷肥制造,电石法聚氯乙烯*,
                // 斜交轮胎*,力车胎*,水泥制造,平板玻璃制造,多晶硅*,炼铁,炼钢,铁合金冶炼,铝冶炼,金属船舶制造
                twoHightOneLeftInd = "C1713,C1723,C1733,C1743,C1752,C1910,C1931,C2211,C2212,C2520,C2611,C2612,C2613,C2614,C2614,C2619,C2621,C2622,C2651,C2911,C2911,C3011,C3041,C3099,C3110,C3120,C3150,C3216,C3731";
            } else {
                twoHightOneLeftInd = adminSmPropDto.getPropValue();
            }
            if(twoHightOneLeftInd.contains(tradeClass)){
                rptBasicInfoAsso.setTwoHightOneLeftInd(CmisCommonConstants.STD_ZB_YES_NO_1);
            }else{
                rptBasicInfoAsso.setTwoHightOneLeftInd(CmisCommonConstants.STD_ZB_YES_NO_0);
            }
            //是否为我行规定的9类易燃易爆行业 TODO

        }
        //申请流水号
        rptBasicInfoAsso.setSerno(serno);
        int count = rptBasicInfoAssoMapper.insertSelective(rptBasicInfoAsso);
        return count;
    }


}
