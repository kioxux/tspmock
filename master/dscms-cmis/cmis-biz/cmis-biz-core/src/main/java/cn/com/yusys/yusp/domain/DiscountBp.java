/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DiscountBp
 * @类描述: DISCOUNT_BP数据实体类
 * @功能描述: 
 * @创建人: zy
 * @创建时间: 2021-06-05 14:23:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "DISCOUNT_BP")
public class DiscountBp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 优惠BP **/
	@Column(name = "APP_MINI_RATE", unique = false, nullable = false, length = 10)
	private String appMiniRate;
	
	/** 优惠起始日 **/
	@Column(name = "DISCOUNT_START_DATE", unique = false, nullable = false, length = 20)
	private String discountStartDate;
	
	/** 优惠结束日 **/
	@Column(name = "DISCOUNT_END_DATE", unique = false, nullable = false, length = 20)
	private String discountEndDate;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 优惠次数 **/
	@Column(name = "SXEF_NUMBER", unique = false, nullable = true, length = 10)
	private String sxefNumber;
	
	
	/**
	 * @param appMiniRate
	 */
	public void setAppMiniRate(String appMiniRate) {
		this.appMiniRate = appMiniRate;
	}
	
    /**
     * @return appMiniRate
     */
	public String getAppMiniRate() {
		return this.appMiniRate;
	}
	
	/**
	 * @param discountStartDate
	 */
	public void setDiscountStartDate(String discountStartDate) {
		this.discountStartDate = discountStartDate;
	}
	
    /**
     * @return discountStartDate
     */
	public String getDiscountStartDate() {
		return this.discountStartDate;
	}
	
	/**
	 * @param discountEndDate
	 */
	public void setDiscountEndDate(String discountEndDate) {
		this.discountEndDate = discountEndDate;
	}
	
    /**
     * @return discountEndDate
     */
	public String getDiscountEndDate() {
		return this.discountEndDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param sxefNumber
	 */
	public void setSxefNumber(String sxefNumber) {
		this.sxefNumber = sxefNumber;
	}
	
    /**
     * @return sxefNumber
     */
	public String getSxefNumber() {
		return this.sxefNumber;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}


}