///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.web.rest;
//
//import java.net.URISyntaxException;
//import java.util.List;
//
//import cn.com.yusys.yusp.domain.SurveyReportBasicAndCom;
//import cn.com.yusys.yusp.domain.SurveyReportComInfo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.SurveyReportBasicInfo;
//import cn.com.yusys.yusp.service.SurveyReportBasicInfoService;
//
///**
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyReportBasicInfoResource
// * @类描述: #资源类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 13:55:08
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/surveyreportbasicinfo")
//public class SurveyReportBasicInfoResource {
//    @Autowired
//    private SurveyReportBasicInfoService surveyReportBasicInfoService;
//
//	/**
//     * 全表查询.
//     *
//     * @return
//     */
//    @GetMapping("/query/all")
//    protected ResultDto<List<SurveyReportBasicInfo>> query() {
//        QueryModel queryModel = new QueryModel();
//        List<SurveyReportBasicInfo> list = surveyReportBasicInfoService.selectAll(queryModel);
//        return new ResultDto<List<SurveyReportBasicInfo>>(list);
//    }
//
//    /**
//     * @函数名称:index
//     * @函数描述:查询对象列表，公共API接口
//     * @参数与返回说明:
//     * @param queryModel
//     *            分页查询类
//     * @算法描述:
//     */
//    @GetMapping("/")
//    protected ResultDto<List<SurveyReportBasicInfo>> index(QueryModel queryModel) {
//        List<SurveyReportBasicInfo> list = surveyReportBasicInfoService.selectByModel(queryModel);
//        return new ResultDto<List<SurveyReportBasicInfo>>(list);
//    }
//
//    /**
//     * @函数名称:show
//     * @函数描述:查询单个对象，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @GetMapping("/{surveyNo}")
//    protected ResultDto<SurveyReportBasicInfo> show(@PathVariable("surveyNo") String surveyNo) {
//        SurveyReportBasicInfo surveyReportBasicInfo = surveyReportBasicInfoService.selectByPrimaryKey(surveyNo);
//        return new ResultDto<SurveyReportBasicInfo>(surveyReportBasicInfo);
//    }
//
//    /**
//     * @函数名称:create
//     * @函数描述:实体类创建，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/")
//    protected ResultDto<SurveyReportBasicInfo> create(@RequestBody SurveyReportBasicInfo surveyReportBasicInfo) throws URISyntaxException {
//        surveyReportBasicInfoService.insert(surveyReportBasicInfo);
//        return new ResultDto<SurveyReportBasicInfo>(surveyReportBasicInfo);
//    }
//
//    /**
//     * @函数名称:update
//     * @函数描述:对象修改，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/update")
//    protected ResultDto<Integer> update(@RequestBody SurveyReportBasicInfo surveyReportBasicInfo) throws URISyntaxException {
//        int result = surveyReportBasicInfoService.update(surveyReportBasicInfo);
//        return new ResultDto<Integer>(result);
//    }
//
//
//    /**
//     * @函数名称:delete
//     * @函数描述:单个对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/delete/{surveyNo}")
//    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
//        int result = surveyReportBasicInfoService.deleteByPrimaryKey(surveyNo);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:batchdelete
//     * @函数描述:批量对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/batchdelete/{ids}")
//    protected ResultDto<Integer> deletes(@PathVariable String ids) {
//        int result = surveyReportBasicInfoService.deleteByIds(ids);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @创建人 WH
//     * @创建时间 14:12 2021-04-12
//     * @return 保存修改基本信息 企业信息
//     **/
//    @PostMapping("/savebasicandcom")
//    protected ResultDto<Integer> savebasicandcom(@RequestBody SurveyReportBasicAndCom surveyReportBasicAndCom) {
//        int result =   surveyReportBasicInfoService.savebasicandcom(surveyReportBasicAndCom);
//        return new ResultDto<Integer>(result).message("保存成功");
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 2021-04-16 18:12
//     * @注释 提交审批接口
//     */
//    @PostMapping("/modelapprove")
//    protected ResultDto<Integer> modelapprove(@RequestBody SurveyReportBasicAndCom surveyReportBasicAndCom) {
//       //调征信接口查询该用户是否有三十天内征信  有的话提交审批
////        int result =   surveyReportBasicInfoService.savebasicandcom(surveyReportBasicAndCom);
////        SurveyReportBasicInfo surveyReportBasicInfo = surveyReportBasicInfoService.selectByPrimaryKey(surveyNo);
//        int result= surveyReportBasicInfoService.modelapprove(surveyReportBasicAndCom);
//
//        return new ResultDto<Integer>(result).message("该用户有三十天内征信报告 提交审批");
//    }
//
//    /**
//     * @创建人 WH
//     * @创建时间 2021-04-16 18:12
//     * @注释 查询单条数据 并返回给前端展示
//     */
//    @PostMapping("/selectbasicandcom/{ids}")
//    protected ResultDto<SurveyReportBasicAndCom> selectbasicandcom(@PathVariable String ids) {
//        ResultDto result= surveyReportBasicInfoService.selectbasicandcom(ids);
//        return result;
//    }
//
//    /**
//     * @创建人 wzy
//     * @创建时间 14:12 2021-04-12
//     * @return 保存修改基本信息
//     **/
//    @PostMapping("/savebasic")
//    protected ResultDto<Integer> savebasic(@RequestBody SurveyReportBasicInfo surveyReportBasicInfo) {
//        int result =   surveyReportBasicInfoService.savebasic(surveyReportBasicInfo);
//        return new ResultDto<Integer>(result).message("保存成功");
//    }
//}
