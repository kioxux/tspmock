package cn.com.yusys.yusp.service.client.bsp.rircp.fbxw05;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：惠享贷规则审批申请接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class Fbxw05Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw05Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * 业务逻辑处理方法：惠享贷规则审批申请接口
     *
     * @param fbxw05ReqDto
     * @return
     */
    @Transactional
    public Fbxw05RespDto fbxw05(Fbxw05ReqDto fbxw05ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value, JSON.toJSONString(fbxw05ReqDto));
        ResultDto<Fbxw05RespDto> fbxw05ResultDto = dscms2RircpClientService.fbxw05(fbxw05ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value, JSON.toJSONString(fbxw05ResultDto));
        String fbxw05Code = Optional.ofNullable(fbxw05ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fbxw05Meesage = Optional.ofNullable(fbxw05ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fbxw05RespDto fbxw05RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fbxw05ResultDto.getCode())) {
            //  获取相关的值并解析
            fbxw05RespDto = fbxw05ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(fbxw05Code, fbxw05Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value);
        return fbxw05RespDto;
    }


}
