package cn.com.yusys.yusp.dto;

public class LmtReViewPortDto {
    private static final long serialVersionUID = 1L;

    private String guarNo;
    private String approveSubSerno;
    private String guarTypeCd;
    private String guarCusId;
    private String guarCusName;
    private String pldLocation;
    private String squ;
    private String isPldOrder;
    private String evalAmt;
    private String maxMortagageAmt;
    private String correFinAmt;
    private String mortagageRate;
    private String subSerno;

    public String getPldLocation() {
        return pldLocation;
    }

    public void setPldLocation(String pldLocation) {
        this.pldLocation = pldLocation;
    }

    public String getSubSerno() {
        return subSerno;
    }

    public void setSubSerno(String subSerno) {
        this.subSerno = subSerno;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getApproveSubSerno() {
        return approveSubSerno;
    }

    public void setApproveSubSerno(String approveSubSerno) {
        this.approveSubSerno = approveSubSerno;
    }

    public String getGuarTypeCd() {
        return guarTypeCd;
    }

    public void setGuarTypeCd(String guarTypeCd) {
        this.guarTypeCd = guarTypeCd;
    }

    public String getPldimnMemo() {
        return pldimnMemo;
    }

    public void setPldimnMemo(String pldimnMemo) {
        this.pldimnMemo = pldimnMemo;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public String getSqu() {
        return squ;
    }

    public void setSqu(String squ) {
        this.squ = squ;
    }

    public String getIsPldOrder() {
        return isPldOrder;
    }

    public void setIsPldOrder(String isPldOrder) {
        this.isPldOrder = isPldOrder;
    }

    public String getEvalAmt() {
        return evalAmt;
    }

    public void setEvalAmt(String evalAmt) {
        this.evalAmt = evalAmt;
    }

    public String getMaxMortagageAmt() {
        return maxMortagageAmt;
    }

    public void setMaxMortagageAmt(String maxMortagageAmt) {
        this.maxMortagageAmt = maxMortagageAmt;
    }

    public String getCorreFinAmt() {
        return correFinAmt;
    }

    public void setCorreFinAmt(String correFinAmt) {
        this.correFinAmt = correFinAmt;
    }

    public String getMortagageRate() {
        return mortagageRate;
    }

    public void setMortagageRate(String mortagageRate) {
        this.mortagageRate = mortagageRate;
    }

    private String pldimnMemo;

    public String getGuarCusId() {
        return guarCusId;
    }

    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId;
    }


}
