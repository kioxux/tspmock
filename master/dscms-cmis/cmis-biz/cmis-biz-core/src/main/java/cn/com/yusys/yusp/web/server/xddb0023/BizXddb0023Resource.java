package cn.com.yusys.yusp.web.server.xddb0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0023.req.Xddb0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0023.resp.Xddb0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0023.Xddb0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 响应Dto：更新抵质押品状态
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDDB0023:更新抵质押品状态")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0023Resource.class);

    @Autowired
    private Xddb0023Service xddb0023sService;

    /**
     * 交易码：xddb0023
     * 交易描述：更新抵质押品状态
     *
     * @param xddb0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新抵质押品状态")
    @PostMapping("/xddb0023")
    protected @ResponseBody
    ResultDto<Xddb0023DataRespDto> xddb0023(@Validated @RequestBody Xddb0023DataReqDto xddb0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataReqDto));
        Xddb0023DataRespDto xddb0023DataRespDto = new Xddb0023DataRespDto();// 响应Dto:根据客户名查询抵押物类型
        ResultDto<Xddb0023DataRespDto> xddb0023DataResultDto = new ResultDto<>();
        try {
            // 从xddb0023DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataReqDto));
            xddb0023DataRespDto = xddb0023sService.xddb0023(xddb0023DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataRespDto));
            // 封装xddb0023DataResultDto中正确的返回码和返回信息
            xddb0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            //xddb0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            xddb0023DataResultDto.setMessage("Xddb0023Service逻辑设计中");

        } catch (BizException ee) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, ee.getMessage());
            // 封装xddb0023DataResultDto中异常返回码和返回信息
            xddb0023DataResultDto.setCode(ee.getErrorCode());
            xddb0023DataResultDto.setMessage(ee.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, e.getMessage());
            // 封装xddb0023DataResultDto中异常返回码和返回信息
            xddb0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0023DataRespDto到xddb0023DataResultDto中
        xddb0023DataResultDto.setData(xddb0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataResultDto));
        return xddb0023DataResultDto;
    }
}
