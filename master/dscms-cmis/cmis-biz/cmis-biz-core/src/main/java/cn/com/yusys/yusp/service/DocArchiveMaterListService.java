/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CardPrdCode;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.repository.mapper.DocArchiveMaterListMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocArchiveMaterListService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 13:53:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocArchiveMaterListService {

    @Autowired
    private DocArchiveMaterListMapper docArchiveMaterListMapper;
    @Autowired
    private CfgDocParamsListService cfgDocParamsListService;
    @Autowired
    private CfgDocParamsDetailService cfgDocParamsDetailService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;
    @Autowired
    private CreditCardAppInfoService creditCardAppInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocArchiveMaterList selectByPrimaryKey(String admlSerno) {
        return docArchiveMaterListMapper.selectByPrimaryKey(admlSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocArchiveMaterList> selectAll(QueryModel model) {
        List<DocArchiveMaterList> records = (List<DocArchiveMaterList>) docArchiveMaterListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocArchiveMaterList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocArchiveMaterList> list = docArchiveMaterListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocArchiveMaterList record) {
        return docArchiveMaterListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocArchiveMaterList record) {
        return docArchiveMaterListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocArchiveMaterList record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docArchiveMaterListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocArchiveMaterList record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docArchiveMaterListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String admlSerno) {
        return docArchiveMaterListMapper.deleteByPrimaryKey(admlSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docArchiveMaterListMapper.deleteByIds(ids);
    }

    /**
     * 根据档案归档流水号删除关联资料清单信息
     *
     * @author jijian_yx
     * @date 2021/6/21 10:13
     **/
    public int deleteByDocSerno(String docSerno) {
        return docArchiveMaterListMapper.deleteByDocSerno(docSerno);
    }

    /**
     * 根据档案流水号获取资料清单
     *
     * @author jijian_yx
     * @date 2021/6/21 16:04
     **/
    public List<DocArchiveMaterList> queryByDocSerno(QueryModel queryModel) {
        String docSerno = Optional.ofNullable((String) queryModel.getCondition().get("docSerno")).orElse("");

        String docId = "";// 影像主键
        String docType = Optional.ofNullable((String) queryModel.getCondition().get("docType")).orElse("");
        String bizSerno = Optional.ofNullable((String) queryModel.getCondition().get("bizSerno")).orElse("");
        String contNo = Optional.ofNullable((String) queryModel.getCondition().get("contNo")).orElse(bizSerno);
        String cusId = Optional.ofNullable((String) queryModel.getCondition().get("cusId")).orElse("");

        if ("01".equals(docType) || "02".equals(docType)) {
            docId = cusId;
        } else if ("03".equals(docType) || "04".equals(docType) || "07".equals(docType)
                || "23".equals(docType) || "24".equals(docType) || "25".equals(docType) || "26".equals(docType) || "27".equals(docType)) {
            docId = bizSerno;
        } else {
            docId = contNo;
        }

        queryModel.setSort("sort+0 asc,doc_type_data desc");
        List<DocArchiveMaterList> list = docArchiveMaterListMapper.selectByModel(queryModel);
        if (list == null || list.size() < 1) {
            // 任务生成时可能没有相应的档案文件清单配置，修改进入时重新匹配清单配置
            DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docSerno);
            if (docArchiveInfo != null) {
                insertMaterList(docArchiveInfo);
                list = docArchiveMaterListMapper.selectByModel(queryModel);
            }
        }

        // 首次进入资料总页数为0才反显资料页数
        DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docSerno);
        if (null != docArchiveInfo && Integer.parseInt(Optional.ofNullable(docArchiveInfo.getTotalPage()).orElse("0")) > 0) {
            return list;
        }

        //影像参数配置
        List<String> imageList = docArchiveMaterListMapper.selectOutsystemCodeList(docSerno);

        //获取影像文件数量
        Map<String, Map<String, Integer>> imageMap = new HashMap<>();

        if (null != imageList && imageList.size() > 0) {
            for (String outsystemCode : imageList) {
                ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
                imageDataReqDto.setDocId(docId);
                imageDataReqDto.setOutSystemCode(outsystemCode);
                ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
                if ("0".equals(map.getCode())) {
                    // 01个人客户资料和02对公客户资料 财报影像文件特殊处理
                    if ("01".equals(docType) && null != map.getData() && map.getData().size() > 0) {
                        int dskhzl016 = 0;
                        int dskhzl017 = 0;
                        int dskhzl015 = 0;
                        for (Map.Entry<String, Integer> entry : map.getData().entrySet()) {
                            if (entry.getKey().contains("DSKHZL016")) {
                                dskhzl016 += entry.getValue();
                            } else if (entry.getKey().contains("DSKHZL017")) {
                                dskhzl017 += entry.getValue();
                            } else if (entry.getKey().contains("DSKHZL015")) {
                                dskhzl015 += entry.getValue();
                            }
                        }
                        if (dskhzl016 > 0) {
                            map.getData().put("DSKHZL016", dskhzl016);
                        }
                        if (dskhzl017 > 0) {
                            map.getData().put("DSKHZL017", dskhzl017);
                        }
                        if (dskhzl015 > 0) {
                            map.getData().put("DSKHZL015", dskhzl015);
                        }
                    } else if ("02".equals(docType) && null != map.getData() && map.getData().size() > 0) {
                        int dgkhzl24 = 0;
                        int dgkhzl25 = 0;
                        int dgkhzl26 = 0;
                        for (Map.Entry<String, Integer> entry : map.getData().entrySet()) {
                            if (entry.getKey().contains("DGKHZL24")) {
                                dgkhzl24 += entry.getValue();
                            } else if (entry.getKey().contains("DGKHZL25")) {
                                dgkhzl25 += entry.getValue();
                            } else if (entry.getKey().contains("DGKHZL26")) {
                                dgkhzl26 += entry.getValue();
                            }
                        }
                        if (dgkhzl24 > 0) {
                            map.getData().put("DGKHZL24", dgkhzl24);
                        }
                        if (dgkhzl25 > 0) {
                            map.getData().put("DGKHZL25", dgkhzl25);
                        }
                        if (dgkhzl26 > 0) {
                            map.getData().put("DGKHZL26", dgkhzl26);
                        }
                    }
                    imageMap.put(outsystemCode, map.getData());
                }
                // 白领易贷通16、零售消费业务17 获取授信影像资料页数
                if (("16".equals(docType) || "17".equals(docType)) && "GRXFDKSX".equals(outsystemCode)) {
                    ImageDataReqDto imageDataReqDto2 = new ImageDataReqDto();
                    imageDataReqDto2.setDocId(bizSerno);
                    imageDataReqDto2.setOutSystemCode(outsystemCode);
                    ResultDto<Map<String, Integer>> map2 = dscms2ImageClientService.imageImageDataSize(imageDataReqDto2);
                    if ("0".equals(map2.getCode())) {
                        imageMap.put(outsystemCode, map2.getData());
                    }
                }
            }
        }
        if (list != null && list.size() > 0 && imageMap.size() > 0) {
            for (DocArchiveMaterList docArchiveMaterList : list) {
                // 反显影像系统影像获取的文件数量
                Map<String, Integer> numMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                if (null != numMap) {
                    String num = String.valueOf(numMap.get(docArchiveMaterList.getFileOutsystemCode()));
                    if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                        docArchiveMaterList.setPagesNum(num);
                    }
                }
                if (Objects.equals(docType, "01")) {
                    // 反显 个人及企业财务报表-2019年/个人及企业财务报表-2020年/个人及企业财务报表-2021年 文件数量
                    if (Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL016")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL017")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL015")) {
                        Map<String, Integer> cbMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                        if (null != cbMap) {
                            String num = String.valueOf(cbMap.get(docArchiveMaterList.getSecOutsystemCode()));
                            if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                                docArchiveMaterList.setPagesNum(num);
                            }
                        }
                    }
                } else if (Objects.equals(docType, "02")) {
                    // 反显 企业财务报表-2019年度/企业财务报表-2020年度/企业财务报表-2021年度 文件数量
                    if (Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL25")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL26")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL24")) {
                        Map<String, Integer> cbMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                        if (null != cbMap) {
                            String num = String.valueOf(cbMap.get(docArchiveMaterList.getSecOutsystemCode()));
                            if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                                docArchiveMaterList.setPagesNum(num);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }


    /**
     * 根据档案流水号获取资料清单(查看)
     *
     * @author 刘奇
     * @date 2021/6/29
     **/
    public List<DocArchiveMaterList> queryByDocSerno2(QueryModel queryModel) {
        List<DocArchiveMaterList> list = docArchiveMaterListMapper.selectByModel(queryModel);

        String docSerno = Optional.ofNullable((String) queryModel.getCondition().get("docSerno")).orElse("");

        if (list == null || list.size() < 1) {
            // 任务生成时可能没有相应的档案文件清单配置，进入时重新匹配清单配置
            DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docSerno);
            if (docArchiveInfo != null) {
                insertMaterList(docArchiveInfo);
                list = docArchiveMaterListMapper.selectByModel(queryModel);
            }
        }

        // 首次进入资料总页数为0才反显资料页数
        DocArchiveInfo docArchiveInfo = docArchiveInfoService.selectByPrimaryKey(docSerno);
        if (null != docArchiveInfo && Integer.parseInt(Optional.ofNullable(docArchiveInfo.getTotalPage()).orElse("0")) > 0) {
            return list;
        }

        String docId = "";// 影像主键
        String docType = Optional.ofNullable((String) queryModel.getCondition().get("docType")).orElse("");
        String bizSerno = Optional.ofNullable((String) queryModel.getCondition().get("bizSerno")).orElse("");
        String contNo = Optional.ofNullable((String) queryModel.getCondition().get("contNo")).orElse(bizSerno);
        String cusId = Optional.ofNullable((String) queryModel.getCondition().get("cusId")).orElse("");

        if ("01".equals(docType) || "02".equals(docType)) {
            docId = cusId;
        } else if ("03".equals(docType) || "04".equals(docType) || "07".equals(docType)
                || "23".equals(docType) || "24".equals(docType) || "25".equals(docType) || "26".equals(docType) || "27".equals(docType)) {
            docId = bizSerno;
        } else {
            docId = contNo;
        }

        //影像参数配置
        List<String> imageList = docArchiveMaterListMapper.selectOutsystemCodeList(docSerno);

        //获取影像文件数量
        Map<String, Map<String, Integer>> imageMap = new HashMap<>();

        if (null != imageList && imageList.size() > 0) {
            for (String outsystemCode : imageList) {
                ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
                imageDataReqDto.setDocId(docId);
                imageDataReqDto.setOutSystemCode(outsystemCode);
                ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
                if ("0".equals(map.getCode())) {
                    // 01个人客户资料和02对公客户资料 财报影像文件特殊处理
                    if ("01".equals(docType) && null != map.getData() && map.getData().size() > 0) {
                        int dskhzl016 = 0;
                        int dskhzl017 = 0;
                        int dskhzl015 = 0;
                        for (Map.Entry<String, Integer> entry : map.getData().entrySet()) {
                            if (entry.getKey().contains("DSKHZL016")) {
                                dskhzl016 += entry.getValue();
                            } else if (entry.getKey().contains("DSKHZL017")) {
                                dskhzl017 += entry.getValue();
                            } else if (entry.getKey().contains("DSKHZL015")) {
                                dskhzl015 += entry.getValue();
                            }
                        }
                        if (dskhzl016 > 0) {
                            map.getData().put("DSKHZL016", dskhzl016);
                        }
                        if (dskhzl017 > 0) {
                            map.getData().put("DSKHZL017", dskhzl017);
                        }
                        if (dskhzl015 > 0) {
                            map.getData().put("DSKHZL015", dskhzl015);
                        }
                    } else if ("02".equals(docType) && null != map.getData() && map.getData().size() > 0) {
                        int dgkhzl24 = 0;
                        int dgkhzl25 = 0;
                        int dgkhzl26 = 0;
                        for (Map.Entry<String, Integer> entry : map.getData().entrySet()) {
                            if (entry.getKey().contains("DGKHZL24")) {
                                dgkhzl24 += entry.getValue();
                            } else if (entry.getKey().contains("DGKHZL25")) {
                                dgkhzl25 += entry.getValue();
                            } else if (entry.getKey().contains("DGKHZL26")) {
                                dgkhzl26 += entry.getValue();
                            }
                        }
                        if (dgkhzl24 > 0) {
                            map.getData().put("DGKHZL24", dgkhzl24);
                        }
                        if (dgkhzl25 > 0) {
                            map.getData().put("DGKHZL25", dgkhzl25);
                        }
                        if (dgkhzl26 > 0) {
                            map.getData().put("DGKHZL26", dgkhzl26);
                        }
                    }
                    imageMap.put(outsystemCode, map.getData());
                }
                // 白领易贷通16、零售消费业务17 获取授信影像资料页数
                if (("16".equals(docType) || "17".equals(docType)) && "GRXFDKSX".equals(outsystemCode)) {
                    ImageDataReqDto imageDataReqDto2 = new ImageDataReqDto();
                    imageDataReqDto2.setDocId(bizSerno);
                    imageDataReqDto2.setOutSystemCode(outsystemCode);
                    ResultDto<Map<String, Integer>> map2 = dscms2ImageClientService.imageImageDataSize(imageDataReqDto2);
                    if ("0".equals(map2.getCode())) {
                        imageMap.put(outsystemCode, map2.getData());
                    }
                }
            }
        }

        Set<String> docTypeDataSet = new HashSet<>();
        if (list != null && list.size() > 0) {
            for (DocArchiveMaterList docArchiveMaterList : list) {
                String docTypeData = docArchiveMaterList.getDocTypeData();
                if (!docTypeDataSet.contains(docTypeData)) {
                    queryModel.addCondition("docTypeData", docTypeData);
                    int count = docArchiveMaterListMapper.selectCountsByDocTypeData(queryModel);
                    if (count > 1 || count == 1) {
                        docArchiveMaterList.setDocTypeDataCount(count);
                    }
                    docTypeDataSet.add(docTypeData);
                } else {
                    docArchiveMaterList.setDocTypeDataCount(0);
                }
                // 反显影像系统影像获取的文件数量
                Map<String, Integer> numMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                if (null != numMap) {
                    String num = String.valueOf(numMap.get(docArchiveMaterList.getFileOutsystemCode()));
                    if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                        docArchiveMaterList.setPagesNum(num);
                    }
                }
                if (Objects.equals(docType, "01")) {
                    // 反显 个人及企业财务报表-2019年/个人及企业财务报表-2020年/个人及企业财务报表-2021年 文件数量
                    if (Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL016")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL017")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DSKHZL015")) {
                        Map<String, Integer> cbMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                        if (null != cbMap) {
                            String num = String.valueOf(cbMap.get(docArchiveMaterList.getSecOutsystemCode()));
                            if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                                docArchiveMaterList.setPagesNum(num);
                            }
                        }
                    }
                } else if (Objects.equals(docType, "02")) {
                    // 反显 企业财务报表-2019年度/企业财务报表-2020年度/企业财务报表-2021年度 文件数量
                    if (Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL25")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL26")
                            || Objects.equals(docArchiveMaterList.getSecOutsystemCode(), "DGKHZL24")) {
                        Map<String, Integer> cbMap = imageMap.get(docArchiveMaterList.getTopOutsystemCode());
                        if (null != cbMap) {
                            String num = String.valueOf(cbMap.get(docArchiveMaterList.getSecOutsystemCode()));
                            if (StringUtils.nonEmpty(num) && !"null".equals(num)) {
                                docArchiveMaterList.setPagesNum(num);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    /**
     * 根据档案类型生成资料清单
     *
     * @author jijian_yx
     * @date 2021/6/22 14:55
     **/
    public void insertMaterList(DocArchiveInfo record) {
        String docSerno = record.getDocSerno();// 档案流水号
        String docType = record.getDocType();// 档案类型 STD_DOC_TYPE
        if (StringUtils.nonEmpty(docType)) {
            /** 获取启用状态的档案文件清单配置 **/
            CfgDocParamsList cfgDocParamsList = cfgDocParamsListService.getEnableCfgDoc(docType);
            if (cfgDocParamsList != null) {
                String cdpl_serno = cfgDocParamsList.getCdplSerno();
                if (StringUtils.nonEmpty(cdpl_serno)) {
                    /** 获取关联档案资料清单 **/
                    QueryModel model = new QueryModel();
                    model.getCondition().put("cdplSerno", cdpl_serno);
                    if (Objects.equals(record.getDocType(), "20")) {
                        // 信用卡清单只插入匹配的影像清单
                        if (Objects.equals(record.getDocBizType(), "04")) {
                            List<String> list = new ArrayList<>();
                            // 信用卡大额分期
                            list.add("XXD_ZXFQ");
                            list.add("XXD_XYKTE");
                            model.addCondition("topImageCodes", list);
                        } else if (Objects.equals(record.getDocBizType(), "03")) {
                            // 信用卡进件
                            CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(record.getBizSerno());
                            if (null != creditCardAppInfo) {
                                String cardCusType = creditCardAppInfo.getCardCusType();
                                String applyCardPrd = creditCardAppInfo.getApplyCardPrd();
                                setXYKImageCode(model, cardCusType, applyCardPrd);
                            }
                        } else {
                            // 无类型显示全量清单
                        }
                    }
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    List<CfgDocParamsDetail> detailList = cfgDocParamsDetailService.selectByModel(model);
                    for (CfgDocParamsDetail detail : detailList) {
                        DocArchiveMaterList docArchiveMater = new DocArchiveMaterList();
                        docArchiveMater.setAdmlSerno(StringUtils.getUUID());
                        docArchiveMater.setDocSerno(docSerno);// 档案流水号
                        docArchiveMater.setDocTypeData(detail.getDocTypeData());// 材料类别
                        docArchiveMater.setDocListData(detail.getDocListData());// 材料清单
                        docArchiveMater.setPagesNum("0");// 资料页数
                        docArchiveMater.setIsSourceFlag(detail.getIsSourceFlag());// 是否原件
                        docArchiveMater.setMemo(detail.getMemo());// 备注
                        docArchiveMater.setInputDate(openDay);
                        docArchiveMater.setInputId(record.getInputId());
                        docArchiveMater.setInputBrId(record.getInputBrId());
                        docArchiveMater.setSort(detail.getSort());// 排序
                        docArchiveMater.setTopOutsystemCode(detail.getTopOutsystemCode());// 一级影像目录编号
                        docArchiveMater.setSecOutsystemCode(detail.getSecOutsystemCode());// 二级影像目录编号
                        docArchiveMater.setFileOutsystemCode(detail.getFileOutsystemCode());// 文件影像目录编号
                        docArchiveMaterListMapper.insertSelective(docArchiveMater);
                    }
                }
            }
        }
    }

    /**
     * 设置信用卡清单
     *
     * @author jijian_yx
     * @date 2021/10/14 15:59
     **/
    private void setXYKImageCode(QueryModel model, String cardCusType, String applyCardPrd) {
        List<String> topImageCodes = new ArrayList<>();
        List<String> secImageCodes = new ArrayList<>();
        if (!Objects.equals(applyCardPrd, CardPrdCode.CARD_PRD_CODE_LYJK) && !Objects.equals(applyCardPrd, CardPrdCode.CARD_PRD_CODE_LYJDZK)) {
            topImageCodes.add("XXD_PK");
            secImageCodes.add("XXD_PK_" + cardCusType);
        } else {
            if(Objects.equals(cardCusType,"C02")){
                topImageCodes.add("XXD_LYJ");
                secImageCodes.add("XXD_LYJ_C04");
            }else{
                topImageCodes.add("XXD_LYJ");
                secImageCodes.add("XXD_LYJ_" + cardCusType);
            }
        }
        model.addCondition("topImageCodes", topImageCodes);
        model.addCondition("secImageCodes", secImageCodes);
    }
}
