/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.Asserts;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constant.RetailPrdAttributionEnums;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.IqpDisAssetIncomeDto;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Lstdkhk;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09RespDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req.XxdrelReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.XxdrelRespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp.Wxp001RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.req.CmisLmt0046ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0046.resp.CmisLmt0046RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.req.CmisLmt0048ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0048.resp.CmisLmt0048RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.req.CmisLmt0063ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0063.resp.CmisLmt0063SubListRespDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0007.req.Xdkh0007ReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.req.Xdkh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.resp.Xdkh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.dto.result.ResultMessageDto;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.core.ln3110.Ln3110Service;
import cn.com.yusys.yusp.service.client.cfg.CfgPrdBasicInfoService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-03 15:22:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpLoanAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanAppService.class);
    @Resource
    private IqpLoanAppMapper iqpLoanAppMapper;//业务主表
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private IqpLoanAppRepayService iqpLoanAppRepayService;//还款信息
    @Autowired
    private IqpLoanAppAssistService iqpLoanAppAssistService;//辅助信息
    @Autowired
    private IqpLoanAppApprModeService iqpLoanAppApprModeService;//业务申请-审批模式
    @Autowired
    private IqpLmtRelService iqpLmtRelService;//授信/第三方关系
    @Autowired
    private IqpBillRelService iqpBillRelService;//借据关系
    @Autowired
    private IqpHouseService iqpHouseService;//房产信息
    @Autowired
    private IqpCarService iqpCarService;//车产信息
    @Autowired
    private IqpApplApptRelService iqpApplApptRelService;//共同申请人信息
    @Autowired
    private IqpGuarBizRelAppService iqpGuarBizRelAppService;//担保人关系
    @Autowired
    private IqpAcctService iqpAcctService;//账户关系表
    @Autowired
    private ComBailService comBailService;//保证金表
    @Autowired
    private BizCorreManagerInfoService bizCorreManagerInfoService;//办理人员
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CtrLoanContService ctrLoanContService;//合同主表
    @Autowired
    private IqpRepayAblyInfoService iqpRepayAblyInfoService;//还款能力
    @Autowired
    private IqpDiffAffirmService iqpDiffAffirmService;//面谈面测
    @Autowired
    private IqpApprReportService iqpApprReportService;//核准上报
    @Autowired
    private IqpApplStatementInfoService iqpApplStatementInfoService;//对账单信息
    @Autowired
    private LcApplSellInfoService lcApplSellInfoService;//营销管护
    @Autowired
    private IqpTelSurvService iqpTelSurvService;//电核信息
    @Autowired
    private IqpFaceChatService iqpFaceChatService;//面谈面测
    @Autowired
    private IqpApplApptService iqpApplApptService;//申请人
    @Autowired
    private IqpDisAssetIncomeService iqpDisAssetIncomeService;//月收入
    @Autowired
    private IqpDisAssetFinanService iqpDisAssetFinanService;//金融资产
    @Autowired
    private IqpDisAssetNFinanService iqpDisAssetNFinanService;//非金融
    @Autowired
    private IqpRiskChkCfrmService iqpRiskChkCfrmService;//审核认定信息
    @Autowired
    private IqpApplRateInfoService iqpApplRateInfoService;//多段固定利率
    @Autowired
    private GrtGuarContService grtGuarContService;//担保合同服务

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;//担保合同与押品关系服务
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;//业务与担保合同关系结果服务
    @Autowired
    private CtrLmtRelService ctrLmtRelService;//合同与授信/第三方额度关系服务
    @Resource
    private ICusClientService iCusClientService;//注入客户服务接口
    @Autowired
    private AccLoanService accLoanService;//借据服务
    //    @Autowired
//    private UserServiceCacheImpl userService;//注入通用的用户服务接口
    @Autowired
    private IqpAcctNoBillRelService iqpAcctNoBillRelService; //账号信息与借据关系表
    @Autowired
    private LmtCobInfoService lmtCobInfoService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private LmtGrpAppService lmtGrpAppService;
    @Autowired
    private IqpLoanFirstPayInfoService iqpLoanFirstPayInfoService;

    @Autowired
    private IqpAppReplyService iqpAppReplyService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private GuarBizRelMapper guarBizRelMapper;

    @Autowired
    private CreditQryBizRealService creditQryBizRealService;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Autowired
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;
    @Autowired
    private LmtCobInfoMapper lmtCobInfoMapper;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private Dscms2RlzyxtClientService dscms2RlzyxtClientService;// BSP 人力资源

    @Autowired
    private IqpCusLsnpInfoService iqpCusLsnpInfoService;

    @Autowired
    private IqpApproveModeService iqpApproveModeService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;


    @Autowired
    private Ln3110Service ln3110Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpStockLoanInfoMapper iqpStockLoanInfoMapper;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private Dscms2GjjsClientService dscms2GjjsClientService;


    @Autowired
    private GrtGuarContMapper grtGuarContMapper;

    @Autowired
    private  GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;

    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    @Autowired
    private IqpLoanApprService iqpLoanApprService;

    @Autowired
    private ImgCondDetailsService imgCondDetailsService;

    @Autowired
    private IqpSurveyConInfoMapper iqpSurveyConInfoMapper;
    @Autowired
    private  IqpLoanApprMapper iqpLoanApprMapper;

    @Autowired
    private IqpCusCreditInfoService iqpCusCreditInfoService;
    @Autowired
    private  AdminSmUserService adminSmUserService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Autowired
    private  CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private  RetailPrimeRateAppService retailPrimeRateAppService;
    @Autowired
    private RetailPrimeRateApprService retailPrimeRateApprService;
    @Autowired
    private BizCommonService bizCommonService;
    @Autowired
    private ToppAcctSubService toppAcctSubService;
    @Autowired
    private IqpCusCreditConInfoService iqpCusCreditConInfoService;
    @Autowired
    private ICusClientService icusClientService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLoanApp selectByPrimaryKey(String iqpSerno) {
        return iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpLoanApp> selectAll(QueryModel model) {
        List<IqpLoanApp> records = (List<IqpLoanApp>) iqpLoanAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpLoanApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanApp> list = iqpLoanAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    public List<IqpLoanAppXw> xwSelectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanAppXw> list = iqpLoanAppMapper.xwSelectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param model
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List < cn.com.yusys.yusp.domain.IqpLoanApp>>
     * @author hubp
     * @date 2021/4/27 21:56
     * @version 1.0.0
     * @desc 根据申请状态查询业务申请历史信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<IqpLoanApp> selectByStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanApp> list = iqpLoanAppMapper.selectByStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpLoanApp record) {
        return iqpLoanAppMapper.insert(record);
    }

    /**
     * @方法名称: insertIqpLoanApp
     * @方法描述: 普通贷款合同新增下一步 公共方法
     * @参数与返回说明:
     * @算法描述: 无
     * @修改历史：zhangliang15 房抵e点贷：房抵e点贷合同申请做注销后，由信贷系统手工发起新增，需要IS_ONLINE_DRAW为1；THIRD_CHANNEL_SERNO 手机银行影像流水号
     */

    public IqpLoanApp insertIqpLoanApp(IqpLoanApp record) {
        String serno = "";
        try {
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            //生成合同编号
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key, EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value);
            } else {
                record.setInputId(userInfo.getLoginCode());
                record.setInputBrId(userInfo.getOrg().getCode());
                record.setManagerId(userInfo.getLoginCode());
                record.setManagerBrId(userInfo.getOrg().getCode());
                record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }
            HashMap param = new HashMap();
            String contNo = "";
            String dkSeq = "";
            if(CmisCommonConstants.STD_BUSI_TYPE_05.equals(record.getBizType())){
                dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_05);
            }else{
                dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_02);
            }
            contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);

            record.setIqpSerno(serno);
            record.setContNo(contNo);
            record.setTermType("M");
            record.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            // 判断是否为房抵e点贷
            if (StringUtil.isNotEmpty(record.getPrdTypeProp()) && "P034".equals(record.getPrdTypeProp())) {
                record.setIsOnlineDraw("1"); //是否线上提款
                // 通过批复编号获取手机银行影像流水号 此场景适用于房抵e点贷XDSX0015自动生成合同申请做注销后，由信贷系统手工发起新增
                String yx_serno = iqpLoanAppMapper.selectThirdChnlSernoByReplyNo(record.getReplyNo());
                record.setThirdChnlSerno(yx_serno);//手机银行影像流水号
            }
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",record.getCusId());
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(record.getCusId());
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",record.getCusId(), cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(record.getCusId());
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    record.setPhone(CusIndivAllDto.getMobile());
                    record.setFax(CusIndivAllDto.getFaxCode());
                    record.setEmail(CusIndivAllDto.getEmail());
                    record.setQq(CusIndivAllDto.getQq());
                    record.setWechat(CusIndivAllDto.getWechatNo());
                    record.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(record.getCusId()).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    record.setLinkman(cusCorpDto.getFreqLinkman());
                    record.setPhone(cusCorpDto.getFreqLinkmanTel());
                    record.setFax(cusCorpDto.getFax());
                    record.setEmail(cusCorpDto.getLinkmanEmail());
                    record.setQq(cusCorpDto.getQq());
                    record.setWechat(cusCorpDto.getWechatNo());
                    record.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }
            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(record.getLmtAccNo())){
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(record.getLmtAccNo());
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",record.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                if(CmisCommonConstants.STD_CONT_TYPE_1.equals(record.getContType())){
                    record.setEiMode(lmtReplyAccSubPrd.getEiMode());
                    record.setRepayMode(lmtReplyAccSubPrd.getRepayMode());
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                record.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            iqpLoanAppMapper.insertSelective(record);
            //根据选择的额度分项自动生成担保合同
            log.info("根据品种分项额度编号"+record.getLmtAccNo()+" 查询分项流水号");

            IqpLoanApp iqpLoanAppData = iqpLoanAppMapper.selectByIqpSerno(serno);
            if (!CmisCommonConstants.STD_BUSI_TYPE_05.equals(iqpLoanAppData.getBizType())){
                if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpLoanAppData.getGuarWay()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanAppData.getGuarWay())
                        && !CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanAppData.getGuarWay()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpLoanAppData.getGuarWay())) {
                    String subSerno = "";
                    if(iqpLoanAppData.getLmtAccNo()==null||"".equals(iqpLoanAppData.getLmtAccNo())){
                        subSerno = "";
                    }else{
                        subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(iqpLoanAppData.getLmtAccNo());//GUAR_BIZ_REL
                    }
                    GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                    grtGuarContDto.setSerno(iqpLoanAppData.getIqpSerno());//业务流水号
                    grtGuarContDto.setCusId(iqpLoanAppData.getCusId());//借款人编号
                    grtGuarContDto.setCusName(iqpLoanAppData.getCusName());//借款人名称
                    grtGuarContDto.setGuarWay(iqpLoanAppData.getGuarWay());//担保方式   //10抵押 20 质押 30保证
                    grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                    grtGuarContDto.setGuarContType(iqpLoanAppData.getContType());//担保合同类型
                    grtGuarContDto.setGuarAmt(iqpLoanAppData.getContAmt());//担保金额
                    grtGuarContDto.setGuarTerm(iqpLoanAppData.getContTerm());//担保期限
                    grtGuarContDto.setGuarStartDate(iqpLoanAppData.getStartDate());//担保起始日
                    grtGuarContDto.setGuarEndDate(iqpLoanAppData.getEndDate());//担保终止日
                    grtGuarContDto.setReplyNo(iqpLoanAppData.getReplyNo());//批复编号
                    grtGuarContDto.setBizLine(iqpLoanAppData.getBelgLine());//业务条线
                    grtGuarContDto.setLmtAccNo(iqpLoanAppData.getLmtAccNo());//授信额度编号
                    grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                    grtGuarContDto.setInputId(iqpLoanAppData.getInputId());//登记人
                    grtGuarContDto.setInputBrId(iqpLoanAppData.getInputBrId());//登记机构
                    grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                    log.info("主合同："+iqpLoanAppData.getContNo()+" 生成担保合同以及担保业务关系开始");
                    grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
                    log.info("主合同："+iqpLoanAppData.getContNo()+" 生成担保合同以及担保业务关系结束");
                }
                // 生成用信条件落实情况数据
                Map queryMap = new HashMap();
                queryMap.put("replySerno",iqpLoanAppData.getReplyNo());
                queryMap.put("contNo",iqpLoanAppData.getContNo());
                int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//                if(insertCountData<=0){
//                    throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//                }
            }

            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(record.getCusId());
            creditReportQryLstAndRealDto.setCusId(record.getCusId());
            creditReportQryLstAndRealDto.setCusName(record.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            log.error("保存合同申请数据出现异常！", e);
        } catch (Exception e) {
            log.error("保存合同申请数据出现异常！", e);
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        } finally {

        }
        return record;
    }

    /**
     * @方法名称: insertIqpLoanAppForftin
     * @方法描述: 福费廷合同
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpLoanApp insertIqpLoanAppForftin(IqpLoanApp record) {
        String serno = "";
        try {
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            //record.setSerno(serno);
            record.setIqpSerno(serno);
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(record.getCusId()).getData();
            if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                record.setLinkman(cusCorpDto.getFreqLinkman());
                record.setPhone(cusCorpDto.getFreqLinkmanTel());
                record.setFax(cusCorpDto.getFax());
                record.setEmail(cusCorpDto.getLinkmanEmail());
                record.setQq(cusCorpDto.getQq());
                record.setWechat(cusCorpDto.getWechatNo());
                record.setDeliveryAddr(cusCorpDto.getSendAddr());
                record.setBizType(CmisCommonConstants.STD_BUSI_TYPE_05);
            }
            iqpLoanAppMapper.insertSelective(record);
        } catch (YuspException e) {
            log.error("保存合同申请数据出现异常！", e);
        } catch (Exception e) {
            log.error("保存合同申请数据出现异常！", e);
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        } finally {

        }
        return record;
    }


    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanApp record) {
        return iqpLoanAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int update(IqpLoanApp record) {
        return iqpLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int updateSelective(IqpLoanApp record) {
        return iqpLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @param record
     * @return int
     * @author hubp
     * @date 2021/7/12 9:34
     * @version 1.0.0
     * @desc 修改前加是否通过判断
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int updateXw(IqpLoanApp record) {
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(record.getIqpSerno());
        if (null != iqpLoanApp && "997".equals(iqpLoanApp.getApproveStatus())) {
            return -1;
        }
        Map rtnData = getLprRate(record.getLprRateIntval());
        BigDecimal rate = new BigDecimal("0");
        if(rtnData.containsKey("rate")){
            rate = new BigDecimal(rtnData.get("rate").toString());
        }
        BigDecimal execRateYear = new BigDecimal(record.getExecRateYear().toString());
        BigDecimal floateRate = execRateYear.subtract(rate).multiply(new BigDecimal(10000));
        record.setRateFloatPoint(floateRate);
        // 插入CVT_CNY_AMT折算人民币金额
        record.setCvtCnyAmt(record.getContAmt());
        record.setCurtLprRate(rate);
        // 额度分项编号
        record.setLmtAccNo(record.getReplyNo());
        // 更新时间
        record.setUpdateTime(DateUtils.getCurrDate());
        return iqpLoanAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpLoanAppMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanAppMapper.deleteByIds(ids);
    }


    /**
     * 获取基本信息
     *
     * @param iqpSerno
     * @return
     */
    public HashMap<String, Object> getBaseInfoDto(@Param("iqpSerno") String iqpSerno) {
        return iqpLoanAppMapper.getBaseInfoDto(iqpSerno);
    }

    /**
     * 通过业务主表的主键进行主表跟子表的逻辑删除
     */
    @Transactional(rollbackFor = Exception.class)
    public int deleteIqpInfoByIqpSerno(Map param) {
        int rtnCode = 0;

        String iqpSerno = (String) param.get("iqpSerno");

        log.info("删除业务" + iqpSerno + "申请信息开始！");
        if (iqpSerno == null || "".equals(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_PARAM_FAILED.key, EcbEnum.E_IQP_DELETE_PARAM_FAILED.value);
        }

        //优先更新子表的标志位，子表更新没有问题之后再更新主表的标志位
        //当前所有业务都是直接进行逻辑删除操作，没有针对业务做额外的处理，业务类型暂不使用
        String bizType = (String) param.get("bizType");

        Map delMap = new HashMap();
        delMap.put("iqpSerno", iqpSerno);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType", CmisCommonConstants.OPR_TYPE_DELETE);
        int delCount = 0;

        //授信与业务关系表，包含第三方的关系
        log.info("删除业务" + iqpSerno + "信息-授信(第三方额度)与业务关系表");
        delCount = iqpLmtRelService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //业务与借据关系表
        log.info("删除业务" + iqpSerno + "信息-借据与业务关系表");
        delCount = iqpBillRelService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }


        //还款表
        log.info("删除业务" + iqpSerno + "信息-还款信息表");
        delCount = iqpLoanAppRepayService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //业务申请-审批模式 表
        log.info("删除业务" + iqpSerno + "信息-业务申请-审批模式 表");
        delCount = iqpLoanAppApprModeService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //房产信息表
        log.info("删除业务" + iqpSerno + "信息-房产信息表");
        delCount = iqpHouseService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //车产信息
        log.info("删除业务" + iqpSerno + "信息-车产信息表");
        delCount = iqpCarService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //委托贷款，一期不做，预留

        //共同申请人
        log.info("删除业务" + iqpSerno + "信息-共同借款人与业务关系表");
        delCount = iqpApplApptRelService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //担保信息
        log.info("删除业务" + iqpSerno + "信息-担保与业务关系表");
        delCount = iqpGuarBizRelAppService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //账户信息
        log.info("删除业务" + iqpSerno + "信息-账户与业务关系表");
        delCount = iqpAcctService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //保证金信息
        log.info("删除业务" + iqpSerno + "信息-保证金信息表");
        delCount = comBailService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //影像信息？？？

        //办理人员信息
        log.info("删除业务" + iqpSerno + "信息-办理人员信息表");
        delCount = bizCorreManagerInfoService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //还款能力表 iqp_repay_ably_info
        log.info("删除业务" + iqpSerno + "信息-还款能力表");
        delCount = iqpRepayAblyInfoService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }
        //差别化信贷政策 iqp_diff_affirm
        log.info("删除业务" + iqpSerno + "信息-差别化信贷政策");
        delCount = iqpDiffAffirmService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //核准上报情况  iqp_appr_report
        log.info("删除业务" + iqpSerno + "信息-核准上报情况");
        delCount = iqpApprReportService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //对账单信息  iqp_appl_statement_info_list
        log.info("删除业务" + iqpSerno + "信息-对账单信息");
        delCount = iqpApplStatementInfoService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //贷款申请营销管户信息lc_appl_sell_info
        log.info("删除业务" + iqpSerno + "信息-贷款申请营销管户信息");
        delCount = lcApplSellInfoService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //业务申请人电核信息   iqp_tel_surv_list
        log.info("删除业务" + iqpSerno + "信息-业务申请人电核");
        delCount = iqpTelSurvService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        //面谈面测  iqp_face_chat
        log.info("删除业务" + iqpSerno + "信息-面谈面测");
        delCount = iqpFaceChatService.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }


        //主表信息
        log.info("删除业务" + iqpSerno + "信息-业务主表");
        delCount = iqpLoanAppMapper.updateByParams(delMap);
        if (delCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
        }

        log.info("删除业务" + iqpSerno + "信息成功");
        return rtnCode;
    }


    /**
     * 通过入参查询集合数据
     *
     * @param params
     * @return
     */
    public List<IqpLoanApp> selectIqpLoanAppListByParams(Map params) {
        return iqpLoanAppMapper.selectIqpLoanAppListByParams(params);
    }

    @Transactional
    public int updateApproveStatus(String iqpSerno, String approveStatus) {
        return iqpLoanAppMapper.updateApproveStatus(iqpSerno, approveStatus);
    }

    /**
     * 流程审批通过后,生成业务合同数据，同步更新申请状态为审批通过
     *
     * @param iqpSerno
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int generateContInfo(String iqpSerno) {
        //生成合同表信息
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
        IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppAssistService.selectByPrimaryKey(iqpSerno);
        IqpLoanAppRepay iqpLoanAppRepay = iqpLoanAppRepayService.selectByPrimaryKey(iqpSerno);

        CtrLoanCont ctrLoanCont = new CtrLoanCont();
        //赋值
        BeanUtils.copyProperties(iqpLoanAppAssist, ctrLoanCont);
        BeanUtils.copyProperties(iqpLoanAppRepay, ctrLoanCont);
        BeanUtils.copyProperties(iqpLoanApp, ctrLoanCont);

        //获取主键,生成合同数据
        HashMap<String, String> param = new HashMap<>();
        param.put("bizType", iqpLoanApp.getBizType());
        String contNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, param);
        ctrLoanCont.setContNo(contNo);
        //设置合同信息为待发起 100
        ctrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_100);
        int count = ctrLoanContService.insertSelective(ctrLoanCont);
        if (count <= 0) {
            log.info("业务合同生成完成失败:{}" + ctrLoanCont);
            throw new YuspException(EcbEnum.E_IQP_EXISTS.key, EcbEnum.E_IQP_EXISTS.value);
        } else {
            log.info("业务合同生成完成:{}" + ctrLoanCont);
        }
        //更新业务申请的状态
        iqpLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);

        return count;
    }

    /**
     * 保存复议信息
     *
     * @param iqpSerno
     */
/*
    @Transactional(rollbackFor = Exception.class)
    public String addReconsidIqpLoanApp(String iqpSerno) {
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_EXISTS.key, EcbEnum.E_IQP_EXISTS.value);
        }
        //保存基础表的信息
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
        IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppAssistService.selectByPrimaryKey(iqpSerno);
        IqpLoanAppRepay iqpLoanAppRepay = iqpLoanAppRepayService.selectByPrimaryKey(iqpSerno);
        IqpLoanAppApprMode iqpLoanAppApprMode = iqpLoanAppApprModeService.selectByPrimaryKey(iqpSerno);

        String bizType = iqpLoanApp.getBizType();
        String biz = iqpLoanAppAssist.getChnlSour();
        //拼接序列号
        HashMap<String, String> param = new HashMap<>();
        param.put("biz", biz);
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param);
        param.clear();
        param.put("bizType", bizType);
        String iqpSernoNew = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, param);
        String iqpSernoOld = iqpSerno;
        //通过新旧流水号，查询出33张关联表信息
        log.info("复议更新信息开始,旧业务流水号{}:" + iqpSerno + " !新业务流水号{}: " + iqpSernoNew);
        //1.旧数据存在，新数据不存在的情况下,才会做插入操作
        iqpLoanApp.setSerno(serno);
        iqpLoanApp.setIqpSerno(iqpSernoNew);
        iqpLoanApp.setOldIqpSerno(iqpSerno);
        iqpLoanApp.setIsReconsid(CmisBizConstants.IS_RESONSID_Y);//设置是复议标志
        iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//设置为待发起
        iqpLoanAppMapper.insertSelective(iqpLoanApp);
        log.info("保存申请主表信息成功" + iqpLoanApp.toString());

        iqpLoanAppAssist.setIqpSerno(iqpSernoNew);
        iqpLoanAppAssistService.insertSelective(iqpLoanAppAssist);
        log.info("保存iqpLoanAppAssist表信息成功" + iqpLoanAppAssist.toString());

        iqpLoanAppRepay.setIqpSerno(iqpSernoNew);
        iqpLoanAppRepayService.insertSelective(iqpLoanAppRepay);
        log.info("保存iqpLoanAppRepay表信息成功" + iqpLoanAppRepay.toString());

        iqpLoanAppApprMode.setIqpSerno(iqpSernoNew);
        iqpLoanAppApprModeService.insertSelective(iqpLoanAppApprMode);
        log.info("保存iqpLoanAppApprMode表信息成功" + iqpLoanAppApprMode.toString());

        //保存biz_corre_manager_info 办理人员信息
        Map<String, String> managerParam = new HashMap<>();
        managerParam.put("bizSerno", iqpSerno);
        List<BizCorreManagerInfo> bizCorreManagerInfos = bizCorreManagerInfoService.selectBizCorreManagerInfoByBizSerno(managerParam);
        if (CollectionUtils.isEmpty(bizCorreManagerInfos)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_FAILED.key, "原申请无主办人员信息,数据有误！原业务申请流水号:" + iqpSerno);
        }
        for (BizCorreManagerInfo managerInfo : bizCorreManagerInfos) {
            String pkId = StringUtils.uuid(true);
            managerInfo.setPkId(pkId);
            managerInfo.setBizSerno(iqpSernoNew);
            bizCorreManagerInfoService.insertSelective(managerInfo);
        }
        log.info("保存biz_corre_manager_info" + iqpSernoNew + "完成！");

        //授信与业务关系表 IQP_LMT_REL
        List<IqpLmtRel> iqpLmtRelListOld = iqpLmtRelService.selectByIqpSerno(iqpSernoOld);
        List<IqpLmtRel> iqpLmtRelListNew = iqpLmtRelService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(iqpLmtRelListOld))
                && CollectionUtils.isEmpty(iqpLmtRelListNew)) {
            for (IqpLmtRel temp : iqpLmtRelListOld) {
                String pkId = StringUtils.uuid(false);
                temp.setIqpSerno(iqpSernoNew);
                temp.setPkId(pkId);
                iqpLmtRelService.insertSelective(temp);
            }
        }
        log.info("授信与业务关系表 IQP_LMT_REL{}" + iqpSernoNew);

        //住房信息   IQP_HOUSE
        List<IqpHouse> iqpHouseListOld = iqpHouseService.selectByIqpSerno(iqpSernoOld);
        List<IqpHouse> iqpHouseListNew = iqpHouseService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(iqpHouseListOld))
                && CollectionUtils.isEmpty(iqpHouseListNew)) {
            for (IqpHouse temp : iqpHouseListOld) {
                String pkId = StringUtils.uuid(false);
                temp.setIqpSerno(iqpSernoNew);
                temp.setPkId(pkId);
                iqpHouseService.insertSelective(temp);
            }
        }
        log.info("住房信息   IQP_HOUSE{}" + iqpSernoNew);

        //购车信息 IQP_CAR
        List<IqpCar> iqpCarListOld = iqpCarService.selectByIqpSerno(iqpSernoOld);
        List<IqpCar> iqpCarListNew = iqpCarService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(iqpCarListOld))
                && CollectionUtils.isEmpty(iqpCarListNew)) {
            for (IqpCar temp : iqpCarListOld) {
                String pkId = StringUtils.uuid(false);
                temp.setIqpSerno(iqpSernoNew);
                temp.setPkId(pkId);
                iqpCarService.insertSelective(temp);
            }
        }
        log.info("购车信息 IQP_CAR" + iqpSernoNew);

        //账号信息表 IQP_ACCT
        List<IqpAcct> iqpAcctListOld = iqpAcctService.selectByIqpSerno(iqpSernoOld);
        List<IqpAcct> IqpAcctListNew = iqpAcctService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(iqpAcctListOld))
                && CollectionUtils.isEmpty(IqpAcctListNew)) {
            for (IqpAcct temp : iqpAcctListOld) {
                String pkId = StringUtils.uuid(false);
                temp.setIqpSerno(iqpSernoNew);
                temp.setPkId(pkId);
                iqpAcctService.insertSelective(temp);
            }
        }
        log.info("账号信息表 IQP_ACCT" + iqpSernoNew);
        //保证金信息 COM_BAIL
        List<ComBail> oldComBailList = comBailService.selectByIqpSerno(iqpSernoOld);
        List<ComBail> newComBailList = comBailService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldComBailList))
                && CollectionUtils.isEmpty(newComBailList)) {
            for (ComBail temp : oldComBailList) {
                String pkId = StringUtils.uuid(false);
                temp.setIqpSerno(iqpSernoNew);
                temp.setPkId(pkId);
                comBailService.insertSelective(temp);
            }
        }
        log.info("保证金信息更新完成 COM_BAIL" + iqpSernoNew);
        //对账单通知信息 IQP_APPL_STATEMENT_INFO
        List<IqpApplStatementInfo> oldIqpApplStatementInfoList = iqpApplStatementInfoService.selectByIqpSerno(iqpSernoOld);
        List<IqpApplStatementInfo> newIqpApplStatementInfoList = iqpApplStatementInfoService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpApplStatementInfoList))
                && CollectionUtils.isEmpty(newIqpApplStatementInfoList)) {
            for (IqpApplStatementInfo temp : oldIqpApplStatementInfoList) {
                temp.setIqpSerno(iqpSernoNew);
                iqpApplStatementInfoService.insertSelective(temp);
            }
        }
        log.info("对账单通知信息 IQP_APPL_STATEMENT_INFO" + iqpSernoNew);
        //单笔单批业务申请人信息表 IQP_APPL_APPT
        List<IqpApplAppt> oldIqpApplApptList = iqpApplApptService.selectByIqpSerno(iqpSernoOld);
        List<IqpApplAppt> newIqpApplApptList = iqpApplApptService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpApplApptList))
                && CollectionUtils.isEmpty(newIqpApplApptList)) {
            for (IqpApplAppt temp : oldIqpApplApptList) {
                temp.setIqpSerno(iqpSernoNew);
                iqpApplApptService.updateSelective(temp);
            }
        }
        log.info("单笔单批业务申请人信息表 IQP_APPL_APPT" + iqpSernoNew);

        //业务申请人关系表(含共同申请人) IQP_APPL_APPT_RELsaveIqpBillRel
        List<IqpApplApptRel> oldIqpApplApptRelList = iqpApplApptRelService.selectByIqpSerno(iqpSernoOld);
        List<IqpApplApptRel> newIqpApplApptRelList = iqpApplApptRelService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpApplApptRelList))
                && CollectionUtils.isEmpty(newIqpApplApptRelList)) {
            for (IqpApplApptRel temp : oldIqpApplApptRelList) {
                String pkId = StringUtils.uuid(false);
                temp.setPkId(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpApplApptRelService.insertSelective(temp);
            }
        }
        log.info("单笔单批业务申请人信息表 IQP_APPL_APPT" + iqpSernoNew);

        //资产折算-月收入表 IQP_DIS_ASSET_INCOME
        List<IqpDisAssetIncome> oldIqpDisAssetIncomeList = iqpDisAssetIncomeService.selectByIqpSerno(iqpSernoOld);
        List<IqpDisAssetIncome> newIqpDisAssetIncomeList = iqpDisAssetIncomeService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpDisAssetIncomeList))
                && CollectionUtils.isEmpty(newIqpDisAssetIncomeList)) {
            for (IqpDisAssetIncome temp : oldIqpDisAssetIncomeList) {
                String pkId = StringUtils.uuid(false);
                temp.setIncomePk(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpDisAssetIncomeService.insertSelective(temp);
            }
        }
        log.info("资产折算-月收入表 IQP_DIS_ASSET_INCOME" + iqpSernoNew);

        //资产折算-金融资产表 IQP_DIS_ASSET_FINAN
        List<IqpDisAssetFinan> oldIqpDisAssetFinanList = iqpDisAssetFinanService.selectByIqpSerno(iqpSernoOld);
        List<IqpDisAssetFinan> newIqpDisAssetFinanList = iqpDisAssetFinanService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpDisAssetFinanList))
                && CollectionUtils.isEmpty(newIqpDisAssetFinanList)) {
            for (IqpDisAssetFinan temp : oldIqpDisAssetFinanList) {
                String pkId = StringUtils.uuid(false);
                temp.setFinanPk(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpDisAssetFinanService.insertSelective(temp);
            }
        }
        log.info("资产折算-金融资产表 IQP_DIS_ASSET_FINAN" + iqpSernoNew);

        //资产折算-非金融资产表 IQP_DIS_ASSET_N_FINAN
        List<IqpDisAssetNFinan> oldIqpDisAssetNFinanList = iqpDisAssetNFinanService.selectByIqpSerno(iqpSernoOld);
        List<IqpDisAssetNFinan> newIqpDisAssetNFinanList = iqpDisAssetNFinanService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpDisAssetNFinanList))
                && CollectionUtils.isEmpty(newIqpDisAssetNFinanList)) {
            for (IqpDisAssetNFinan temp : oldIqpDisAssetNFinanList) {
                String pkId = StringUtils.uuid(false);
                temp.setNfinanPk(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpDisAssetNFinanService.insertSelective(temp);
            }
        }
        log.info("资产折算-非金融资产表 IQP_DIS_ASSET_N_FINAN" + iqpSernoNew);

        //面谈面测信息 IQP_FACE_CHAT
        List<IqpFaceChat> oldIqpFaceChatList = iqpFaceChatService.selectByIqpSerno(iqpSernoOld);
        List<IqpFaceChat> newIqpFaceChatList = iqpFaceChatService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpFaceChatList))
                && CollectionUtils.isEmpty(newIqpFaceChatList)) {
            for (IqpFaceChat temp : oldIqpFaceChatList) {
                String pkId = StringUtils.uuid(false);
                temp.setSerno(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpFaceChatService.insertSelective(temp);
            }
        }
        log.info("面谈面测信息 IQP_FACE_CHAT" + iqpSernoNew);

        //还款能力测算信息 IQP_REPAY_ABLY_INFO
        List<IqpRepayAblyInfo> oldIqpRepayAblyInfoList = iqpRepayAblyInfoService.selectByIqpSerno(iqpSernoOld);
        List<IqpRepayAblyInfo> newIqpRepayAblyInfoList = iqpRepayAblyInfoService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpRepayAblyInfoList))
                && CollectionUtils.isEmpty(newIqpRepayAblyInfoList)) {
            for (IqpRepayAblyInfo temp : oldIqpRepayAblyInfoList) {
                String pkId = StringUtils.uuid(false);
                temp.setSerno(pkId);
                temp.setIqpSerno(iqpSernoNew);
                iqpRepayAblyInfoService.insertSelective(temp);
            }
        }
        log.info("还款能力测算信息 IQP_REPAY_ABLY_INFO" + iqpSernoNew);

        //差别化信贷政策认定 IQP_DIFF_AFFIRM
        List<IqpDiffAffirm> oldIqpDiffAffirmList = iqpDiffAffirmService.selectByIqpSerno(iqpSernoOld);
        List<IqpDiffAffirm> newIqpDiffAffirmList = iqpDiffAffirmService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpDiffAffirmList))
                && CollectionUtils.isEmpty(newIqpDiffAffirmList)) {
            for (IqpDiffAffirm temp : oldIqpDiffAffirmList) {
                temp.setIqpSerno(iqpSernoNew);
                iqpDiffAffirmService.insertSelective(temp);
            }
        }
        log.info("差别化信贷政策认定 IQP_DIFF_AFFIRM" + iqpSernoNew);

        //核准上报情况 IQP_APPR_REPORT
        List<IqpApprReport> oldIqpApprReportList = iqpApprReportService.selectByIqpSerno(iqpSernoOld);
        List<IqpApprReport> newIqpApprReportList = iqpApprReportService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpApprReportList))
                && CollectionUtils.isEmpty(newIqpApprReportList)) {
            for (IqpApprReport temp : oldIqpApprReportList) {
                temp.setIqpSerno(iqpSernoNew);
                iqpApprReportService.insertSelective(temp);
            }
        }
        log.info("核准上报情况 IQP_APPR_REPORT" + iqpSernoNew);

        //电调信息 IQP_TEL_SURV
        List<IqpTelSurv> oldIqpTelSurvList = iqpTelSurvService.selectByIqpSerno(iqpSernoOld);
        List<IqpTelSurv> newIqpTelSurvList = iqpTelSurvService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpTelSurvList))
                && CollectionUtils.isEmpty(newIqpTelSurvList)) {
            for (IqpTelSurv temp : oldIqpTelSurvList) {
                temp.setIqpSerno(iqpSernoNew);
                String pkId = StringUtils.uuid(false);
                temp.setSerno(pkId);
                iqpTelSurvService.insertSelective(temp);
            }
        }
        log.info("电调信息 IQP_TEL_SURV" + iqpSernoNew);

        //审核认定信息 IQP_RISK_CHK_CFRM
        List<IqpRiskChkCfrm> oldIqpRiskChkCfrmList = iqpRiskChkCfrmService.selectByIqpSerno(iqpSernoOld);
        List<IqpRiskChkCfrm> newIqpRiskChkCfrmList = iqpRiskChkCfrmService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpRiskChkCfrmList))
                && CollectionUtils.isEmpty(newIqpRiskChkCfrmList)) {
            for (IqpRiskChkCfrm temp : oldIqpRiskChkCfrmList) {
                temp.setIqpSerno(iqpSernoNew);
                String pkId = StringUtils.uuid(false);
                temp.setSerno(pkId);
                iqpRiskChkCfrmService.insertSelective(temp);
            }
        }
        log.info("审核认定信息 IQP_RISK_CHK_CFRM" + iqpSernoNew);

        //多阶段固定利率信息 IQP_APPL_RATE_INFO
        List<IqpApplRateInfo> oldIqpApplRateInfoList = iqpApplRateInfoService.selectByIqpSerno(iqpSernoOld);
        List<IqpApplRateInfo> newIqpApplRateInfoList = iqpApplRateInfoService.selectByIqpSerno(iqpSernoNew);
        if ((!CollectionUtils.isEmpty(oldIqpApplRateInfoList))
                && CollectionUtils.isEmpty(newIqpApplRateInfoList)) {
            for (IqpApplRateInfo temp : oldIqpApplRateInfoList) {
                temp.setIqpSerno(iqpSernoNew);
                String pkId = StringUtils.uuid(false);
                temp.setPkId(pkId);
                iqpApplRateInfoService.insertSelective(temp);
            }
        }
        log.info("多阶段固定利率信息 IQP_APPL_RATE_INFO" + iqpSernoNew);
        return iqpSernoNew;
    }
*/



    /*

     * @方法名称: reconsidIqpLoanApp
     * @方法描述: 零售业务申请复议
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public String reconsidIqpLoanApp(String iqpSerno) {
        log.info("复议更新信息开始,旧业务流水号:" + iqpSerno);
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key, EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value);
        }
        String iqpSernoNew = "";
        try{
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);//主表申请主表
            String iqpApproveStatus = iqpLoanApp.getApproveStatus();
            if("997".equals(iqpApproveStatus)){
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectContByIqpSerno(iqpLoanApp.getIqpSerno());
                //合同状态
                String contStatus = ctrLoanCont.getContStatus();
                String contApproveStatus = ctrLoanCont.getApproveStatus();
                //未生效且审批状态不在流程中的
                if(!"200".equals(contStatus) && "000,997,996,998".indexOf(contApproveStatus) > -1){
                    //额度提前终止
                    CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
                    cmisLmt0008ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0008ReqDto.setLmtType("01");//授信类型
                    cmisLmt0008ReqDto.setInputId(ctrLoanCont.getInputId());//登记人
                    cmisLmt0008ReqDto.setInputBrId(ctrLoanCont.getInputBrId());//登记机构
                    cmisLmt0008ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期

                    List<CmisLmt0008ApprListReqDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0008ApprListReqDto>();
                    CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
                    cmisLmt0008ApprListReqDto.setCusId(ctrLoanCont.getCusId());
                    cmisLmt0008ApprListReqDto.setOptType("02");
                    cmisLmt0008ApprListReqDto.setApprSerno(iqpLoanApp.getIqpSerno());
                    cmisLmt0011OccRelListDtoList.add(cmisLmt0008ApprListReqDto);
                    cmisLmt0008ReqDto.setApprList(cmisLmt0011OccRelListDtoList);

                    List<CmisLmt0008ApprSubListReqDto> cmisLmt0008ApprSubListReqDtoList = new ArrayList<CmisLmt0008ApprSubListReqDto>();
                    CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
                    cmisLmt0008ApprSubListReqDto.setCusId(ctrLoanCont.getCusId());
                    cmisLmt0008ApprSubListReqDto.setApprSubSerno(iqpLoanApp.getReplyNo());
                    cmisLmt0008ApprSubListReqDtoList.add(cmisLmt0008ApprSubListReqDto);
                    cmisLmt0008ReqDto.setApprSubList(cmisLmt0008ApprSubListReqDtoList);

                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度提前终止请求报文：" + cmisLmt0008ReqDto.toString());
                    ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
                    log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-额度提前终止返回报文：" + cmisLmt0008RespDto.toString());

                    if (!"0000".equals(cmisLmt0008RespDto.getData().getErrorCode())) {
                        log.info("零售-额度提前终止异常：" + cmisLmt0008RespDto.getData().getErrorMsg());
                        throw BizException.error(null, "9999", "零售额度提前终止异常");
                    }

                    //第三方额度恢复
                    String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识
                    if ("1".equalsIgnoreCase(thirdPartyFlag)) {
                        CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                        cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                        cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                        cmisLmt0012ReqDto.setBizNo(ctrLoanCont.getContNo());//业务编号
                        cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
                        cmisLmt0012ReqDto.setRecoverSpacAmtCny(iqpLoanApp.getCvtCnyAmt());// 恢复敞口金额(人民币)
                        cmisLmt0012ReqDto.setRecoverAmtCny(iqpLoanApp.getCvtCnyAmt());//恢复总额(人民币)
                        cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                        cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                        cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                        cmisLmt0012ReqDto.setIsRecoverCoop("1");


                        log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用请求报文：" + cmisLmt0012ReqDto.toString());
                        ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                        log.info("根据业务申请编号【" + ctrLoanCont.getIqpSerno() + "】前往额度系统-第三方恢复占用返回报文：" + cmisLmt0012RespDto.toString());

                        if (!"0000".equals(cmisLmt0012RespDto.getData().getErrorCode())) {
                            log.info("零售-第三方恢复占用异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
                            throw BizException.error(null, "9999", "零售-第三方恢复占用异常");
                        }
                    }
                    ctrLoanCont.setContStatus("600");//更新为注销
                    ctrLoanContMapper.updateByPrimaryKeySelective(ctrLoanCont);
                }else{
                    throw BizException.error(null, "9999","存在生效的合同或在审批中的合同，不可复议！");
                }
            }

            //拉取主表和所有主表关联信息
            IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppAssistService.selectByPrimaryKey(iqpSerno);//业务申请-辅助信息
//            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(iqpSerno);//业务申请批复结果表
            IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);//购房信息
            List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(iqpSerno);//共有人信息
            List<IqpLoanFirstPayInfo> iqpLoanFirstPayInfoList = iqpLoanFirstPayInfoService.selectByIqpSerno(iqpSerno);//购房首付款来源
            List<IqpDisAssetIncome> iqpDisAssetIncomeList = iqpDisAssetIncomeService.selectByIqpSerno(iqpSerno);//还款能力分析表
            IqpApplAppt iqpApplAppt = iqpApplApptService.selectByPrimaryKey(iqpSerno);//业务申请人信息表
            //押品信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", iqpSerno);
            List<GuarBizRel> guarBizRelList = guarBizRelService.selectByModel(queryModel);


            HashMap<String, String> param = new HashMap<>();
            param.put("bizType", iqpLoanApp.getBizType());
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, param);//暂时通过UUID生成全局序列号
            iqpSernoNew = serno;//暂时通过UUID生成新序列号
            String iqpSernoOld = iqpSerno;
            log.info("复议更新信息开始,旧业务流水号{}:" + iqpSernoOld + " !新业务流水号{}: " + iqpSernoNew);
            //iqpLoanApp.setSerno(serno);//放入全局流水号
            iqpLoanApp.setIqpSerno(iqpSernoNew);//放入新的业务流水号
            iqpLoanApp.setOldIqpSerno(iqpSernoOld);//在新主表中放入旧业务流水号
            iqpLoanApp.setIsReconsid("1");//设置是复议标志
            iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//设置为待发起
            iqpLoanApp.setOprType(CmisBizConstants.OPR_TYPE_01);//设置操作类型新增
            iqpLoanAppMapper.insertSelective(iqpLoanApp);
            log.info("保存申请主表信息成功" + iqpLoanApp.toString());
            if (null != iqpLoanAppAssist) {
                iqpLoanAppAssist.setIqpSerno(iqpSernoNew);
                iqpLoanAppAssistService.insertSelective(iqpLoanAppAssist);
                log.info("保存iqpLoanAppAssist表信息成功" + iqpLoanAppAssist.toString());
            }

            if (null != guarBizRelList && guarBizRelList.size() > 0) {
                for(int i = 0;i<guarBizRelList.size();i++){
                    GuarBizRel guarBizRel = guarBizRelList.get(i);
                    guarBizRel.setPkId(UUID.randomUUID().toString());
                    guarBizRel.setSerno(iqpSernoNew);
                    guarBizRelService.insertSelective(guarBizRel);
                    log.info("保存iqpLoanAppRepay表信息成功" + guarBizRel.toString());
                }
            }

//            if (null != lmtCrdReplyInfo) {
//                lmtCrdReplyInfo.setSurveySerno(iqpSernoNew);
//                lmtCrdReplyInfoService.insertSelective(lmtCrdReplyInfo);
//                log.info("保存iqpLoanAppRepay表信息成功" + lmtCrdReplyInfo.toString());
//            }
            if (null != iqpHouse) {//保存iqpHouse表信息
                iqpHouse.setPkId(iqpSernoNew);
                iqpHouse.setIqpSerno(iqpSernoNew);
                iqpHouseService.insertSelective(iqpHouse);
                log.info("保存iqpHouse表信息成功" + iqpHouse.toString());
            }
            if (lmtCobInfoList.size() > 0) {//保存lmtCobInfo表信息
                for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                    lmtCobInfo.setPkId(UUID.randomUUID().toString());
                    lmtCobInfo.setSurveySerno(iqpSernoNew);
                    lmtCobInfoService.insertSelective(lmtCobInfo);
                    log.info("保存lmtCobInfo表信息成功" + lmtCobInfo.toString());
                }
            }
            if (iqpLoanFirstPayInfoList.size() > 0) {//保存iqpLoanFirstPayInfo表信息
                for (IqpLoanFirstPayInfo iqpLoanFirstPayInfo : iqpLoanFirstPayInfoList) {
                    iqpLoanFirstPayInfo.setPkId(UUID.randomUUID().toString());
                    iqpLoanFirstPayInfo.setIqpSerno(iqpSernoNew);
                    iqpLoanFirstPayInfoService.insertSelective(iqpLoanFirstPayInfo);
                    log.info("保存iqpLoanFirstPayInfo表信息成功" + iqpLoanFirstPayInfo.toString());
                }
            }
            if (iqpDisAssetIncomeList.size() > 0) {//保存iqpDisAssetIncome表信息
                for (IqpDisAssetIncome iqpDisAssetIncome : iqpDisAssetIncomeList) {
                    iqpDisAssetIncome.setIncomePk(UUID.randomUUID().toString());
                    iqpDisAssetIncome.setIqpSerno(iqpSernoNew);
                    iqpDisAssetIncomeService.insertSelective(iqpDisAssetIncome);
                    log.info("保存iqpDisAssetIncome表信息成功" + iqpDisAssetIncome.toString());
                }
            }
            if (null != iqpApplAppt) {//保存iqpApplAppt表信息
                iqpApplAppt.setIqpSerno(iqpSernoNew);
                iqpApplApptService.insertSelective(iqpApplAppt);
                log.info("保存iqpApplAppt表信息成功" + iqpApplAppt.toString());
            }

            List<CreditQryBizReal> creditQryBizRealList = creditQryBizRealService.selectBySerno(iqpSernoOld);
            if(creditQryBizRealList != null && creditQryBizRealList.size() >0){
                log.info("保存CreditQryBizReal表信息开始" + creditQryBizRealList.toString());
                for (int i= 0;i<creditQryBizRealList.size();i++){
                    CreditQryBizReal creditQryBizReal = creditQryBizRealList.get(i);
                    creditQryBizReal.setBizSerno(iqpSernoNew);
                    creditQryBizReal.setCqbrSerno(UUID.randomUUID().toString());
                    creditQryBizRealService.insert(creditQryBizReal);
                }
                log.info("保存CreditQryBizReal表信息结束");
            }


            IqpSurveyConInfo iqpSurveyConInfo = iqpSurveyConInfoMapper.selectByPrimaryKey(iqpSernoOld);
            if(iqpSurveyConInfo != null){
                log.info("保存iqpSurveyConInfo表信息开始" + iqpSurveyConInfo.toString());
                iqpSurveyConInfo.setIqpSerno(iqpSernoNew);
                iqpSurveyConInfoMapper.insert(iqpSurveyConInfo);
                log.info("保存iqpSurveyConInfo表信息结束");
            }

            IqpCusCreditConInfo iqpCusCreditConInfo = iqpCusCreditConInfoService.selectByPrimaryKey(iqpSernoOld);
            if(iqpCusCreditConInfo != null){
                log.info("保存iqpCusCreditConInfo表信息开始" + iqpCusCreditConInfo.toString());
                iqpCusCreditConInfo.setIqpSerno(iqpSernoNew);
                iqpCusCreditConInfoService.insert(iqpCusCreditConInfo);
                log.info("保存iqpCusCreditConInfo表信息结束");
            }

            IqpLoanApp iqpLoanAppOld = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);//主表申请主表
            iqpLoanAppOld.setApproveStatus("996");//自行退出
            iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanAppOld);

        }catch (Exception e ){
            log.info("复议异常："+e.getMessage());
            throw BizException.error(null, "9999",e.getMessage());
        }
        return iqpSernoNew;
    }


    /**
     * 校验申请金额与担保合同金额
     * 1、申请金额变化时，需要申请金额入参
     * 2、担保金额变化【引入担保合同、新增担保品/保证人】时
     *
     * @param params
     * @return
     */
    public Map checkGuarAmtAndAppAmt(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String iqpSerno = (String) params.get("iqpSerno");
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_PARAM_EXCEPTION.key, EcbEnum.APPAMT_CHECK_PARAM_EXCEPTION.value);
            }

            log.info("获取业务申请" + iqpSerno + "申请金额信息");
            BigDecimal appAmt = new BigDecimal(0);
            String appAmtString = (String) params.get("appAmt");
            if (StringUtils.isBlank(appAmtString)) {
                log.info("入参的申请金额为空，查询业务申请" + iqpSerno + "主表信息获取申请金额");
                IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
                if (iqpLoanApp == null) {
                    throw new YuspException(EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.key, EcbEnum.APPAMT_CHECK_APPNOTEXISTS_EXCEPTION.value);
                }

                appAmt = iqpLoanApp.getAppAmt();
                if (appAmt == null || appAmt.compareTo(new BigDecimal(0)) <= 0) {
                    rtnMsg = "申请金额为空";
                    return rtnData;
                }
            } else {
                //因为前端入参的金额，存在，分隔符，因此在转化数值前需要将，替换
                appAmt = new BigDecimal(appAmtString.replace(",", ""));
            }

            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            log.info("获取业务申请" + iqpSerno + "下业务与担保关系数据");
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectIGBRAByParams(params);
            if (CollectionUtils.isEmpty(iqpGuarBizRelAppList)) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_IGBANOTEXISTS_EXCEPTION.key, EcbEnum.APPAMT_CHECK_IGBANOTEXISTS_EXCEPTION.value);
            }

            log.info("获取业务申请" + iqpSerno + "下担保合同编号信息");
            String guarContNos = "";
            for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
                guarContNos += iqpGuarBizRelApp.getGuarContNo() + ",";
            }

            if (guarContNos.endsWith(",")) {
                guarContNos = guarContNos.substring(0, guarContNos.length() - 1);
            }

            log.info("获取业务申请" + iqpSerno + "下担保合同编号信息为：" + guarContNos);

            log.info("获取业务申请" + iqpSerno + "下担保金额信息");
            Map guarTotalAmtMap = grtGuarContRelService.getGuarTotalAmt(guarContNos, "");

            BigDecimal guarTotalAmt = new BigDecimal(0);
            if (CollectionUtils.isEmpty(guarTotalAmtMap)) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.key, EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.value);
            }
            String amtRtnCode = (String) guarTotalAmtMap.get("rtnCode");
            String amtRtnMsg = (String) guarTotalAmtMap.get("rtnMSg");
            if (!EcbEnum.IQP_SUCCESS_DEF.key.equals(amtRtnCode)) {
                throw new YuspException(amtRtnCode, amtRtnMsg);
            }

            guarTotalAmt = (BigDecimal) guarTotalAmtMap.get("guarTotalAmt");

            log.info("获取业务申请" + iqpSerno + "下担保总金额为：" + guarTotalAmt);

            if (appAmt.compareTo(guarTotalAmt) > 0) {
                throw new YuspException(EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.key, EcbEnum.APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION.value);
            }
        } catch (YuspException e) {
            log.error("校验担保金额与申请金额异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 校验第三方额度合作方额度信息
     *
     * @param iqpLmtRelDto
     * @return
     */
    private Map checkThCooperAmtInsert(IqpLmtRelDto iqpLmtRelDto) {
        log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "开始！额度类型为【合作方额度】");
        Map checkMap = new HashMap();
        String checkCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String checkMsg = EcbEnum.IQP_SUCCESS_DEF.value;

        try {
            //获取合作方授信额度lmtAmt
            BigDecimal lmtAmt = iqpLmtRelDto.getLmtAmt();
            //获取合作方单户限额sigAmt
            BigDecimal sigAmt = iqpLmtRelDto.getSigAmt();
            if (lmtAmt == null || sigAmt == null) {
                throw new YuspException(EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.value);
            }
            //调用获取历史金额的方法
            Map<String, BigDecimal> amtMap = getHisAmt(iqpLmtRelDto);
            BigDecimal totalAmt = amtMap.get("totalAmt");
            BigDecimal cusTotalAmt = amtMap.get("cusTotalAmt");
            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取申请历史中引用该笔合作方额度的占用金额为：" + totalAmt.toString());
            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取该客户申请历史中引用该笔合作方额度的占用金额为：" + cusTotalAmt.toString());

            //新增时校验
            if (CmisBizConstants.TH_LMT_CHECK_STAGE_INSERT.equals(iqpLmtRelDto.getThCheckStage())) {
                //比较授信额度与历史占用总额度
                if (lmtAmt.compareTo(totalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CILMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_CILMT_EXCEPTION.value);
                }
                //比较单户限额与客户历史占额
                if (sigAmt.compareTo(cusTotalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CISIG_EXCEPTION.key, EcbEnum.THAMT_CHECK_CISIG_EXCEPTION.value);
                }
            } else if (CmisBizConstants.TH_LMT_CHECK_STAGE_UPDATE.equals(iqpLmtRelDto.getThCheckStage())) {//修改时校验
                //获取本次的风险敞口金额
                BigDecimal riskOpenAmt = iqpLmtRelDto.getRiskOpenAmt();
                if (riskOpenAmt == null) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.value);
                }
                //比较授信额度与(本次申请+历史占用总额度）
                if (lmtAmt.compareTo(riskOpenAmt.add(totalAmt)) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CULMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_CULMT_EXCEPTION.value);
                }
                //比较单户限额与(本次申请+客户历史占额)
                if (sigAmt.compareTo(riskOpenAmt.add(cusTotalAmt)) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CISIG_EXCEPTION.key, EcbEnum.THAMT_CHECK_CISIG_EXCEPTION.value);
                }
            }
        } catch (YuspException e) {
            checkCode = e.getCode();
            checkMsg = e.getMsg();
        } catch (Exception e) {
            log.error("操作异常！", e);
            checkCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            checkMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "异常原因：" + e.getMessage();
        } finally {
            checkMap.put("checkCode", checkCode);
            checkMap.put("checkMsg", checkMsg);
        }
        return checkMap;
    }

    /**
     * 单笔单批+额度项下业务申请时，新增页引入第三方额度时，校验引入的第三方额度是否可用；修改时，校验引入的额度是否能够覆盖申请主表的风险敞口金额
     * 申请时，仅能选择合作方额度、融资担保额度中的一种，通过额度类型进行对应的校验即可
     * 存量申请金额为：流程中的申请风险敞口金额 + 审批通过且合同状态为待生效/已生效的合同金额
     * 1、新增时
     * a、合作方额度的授信额度lmtAmt 大于 引入该笔合作方额度的存量申请金额
     * b、合作方额度的单户限额sigAmt 大于 该客户名下引入该笔合作方额度的存量申请金额
     * c、融资担保额度的单户限额singleAmt 大于 该客户名下引入该笔融资担保额度的存量申请金额
     * d、融资担保额度的担保总敞口限额guarTotlSpac 大于 引入该笔融资担保额度的存量申请金额
     * e、融资担保额度的产品总限额totlAmt  大于 引入该笔融资担保额度下对应产品的存量申请金额
     * f、融资担保额度的产品单户限额sigAmt 大于 该客户的引入该笔融资担保额度下对应产品的存量申请金额
     * 2、修改时
     * a、合作方额度的授信额度lmtAmt 大于等于  （本次申请的风险敞口金额  +  引入该笔合作方额度的存量申请金额）
     * b、合作方额度的单户限额sigAmt 大于等于  （本次申请的风险敞口金额  +  该客户名下引入该笔合作方额度的存量申请金额）
     * c、融资担保额度的单户限额singleAmt 大于等于 （本次申请的风险敞口金额  +  该客户名下引入该笔融资担保额度的存量申请金额）
     * d、融资担保额度的担保总敞口限额guarTotlSpac 大于等于 （本次申请的风险敞口金额  + 引入该笔融资担保额度的存量申请金额）
     * e、融资担保额度的产品总限额totlAmt  大于等于 （本次申请的风险敞口金额  + 引入该笔融资担保额度下对应产品的存量申请金额）
     * f、融资担保额度的产品单户限额sigAmt 大于等于 （本次申请的风险敞口金额  + 该客户的引入该笔融资担保额度下对应产品的存量申请金额）
     * g、融资担保额度的单笔限额oneAmt  大于等于  本次申请的风险敞口金额
     *
     * @param iqpLmtRelDto
     * @return
     */
    public Map checkThAmtInfo(IqpLmtRelDto iqpLmtRelDto) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            if (iqpLmtRelDto == null) {
                throw new YuspException(EcbEnum.THAMT_CHECK_PARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_PARAM_EXCEPTION.value);
            }

            String thCheckStage = iqpLmtRelDto.getThCheckStage();//获取校验阶段
            String thirdLimitType = iqpLmtRelDto.getThirdLimitType();//三方额度类型
            String iqpSerno = iqpLmtRelDto.getIqpSerno();//业务主键
            String thirdLimitId = iqpLmtRelDto.getThirdLimitId();//第三方额度编号
            String cusId = iqpLmtRelDto.getCusId();//客户编号
            String prdId = iqpLmtRelDto.getPrdId();//产品编号

            if (StringUtils.isBlank(thirdLimitType) || StringUtils.isBlank(thirdLimitId)) {
                log.info("未引入第三方额度，不校验限额信息！");
                return rtnData;
            }

            if (StringUtils.isBlank(thCheckStage)
                    || StringUtils.isBlank(iqpSerno)
                    || StringUtils.isBlank(cusId) || StringUtils.isBlank(prdId)) {
                throw new YuspException(EcbEnum.THAMT_CHECK_PARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_PARAM_EXCEPTION.value);
            }

            //定义校验的结果集合
            Map checkMap = new HashMap();

            if (CmisBizConstants.TH_LMT_CHECK_STAGE_INSERT.equals(thCheckStage)) {
                log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！");
                if (CmisBizConstants.IQP_LMT_REL_TYPE_COOPER.equals(thirdLimitType)) {//合作方额度
                    checkMap = checkThCooperAmtInsert(iqpLmtRelDto);
                } else if (CmisBizConstants.IQP_LMT_REL_TYPE_FIN.equals(thirdLimitType)) {//担保公司额度
                    log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！额度类型为【融资担保公司额度】");
                    checkMap = checkThFinAmtInsert(iqpLmtRelDto);
                } else if (CmisBizConstants.IQP_LMT_REL_TYPE_SQ.equals(thirdLimitType)) {//商圈类型额度 一期暂时不做
                    log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！额度类型为【商圈额度】");
                    log.info("商圈额度一期暂不开放");
                }
            } else {
                log.info("修改保存时，业务申请" + iqpSerno + "第三方额度" + thirdLimitId + "校验开始！");
                if (CmisBizConstants.IQP_LMT_REL_TYPE_COOPER.equals(thirdLimitType)) {//合作方额度
                    log.info("修改保存时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！额度类型为【合作方额度】");
                    checkMap = checkThCooperAmtInsert(iqpLmtRelDto);
                } else if (CmisBizConstants.IQP_LMT_REL_TYPE_FIN.equals(thirdLimitType)) {//担保公司额度
                    log.info("修改保存时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！额度类型为【融资担保公司额度】");
                    checkMap = checkThFinAmtInsert(iqpLmtRelDto);
                } else if (CmisBizConstants.IQP_LMT_REL_TYPE_SQ.equals(thirdLimitType)) {//商圈类型额度 一期暂时不做
                    log.info("修改保存时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验开始！额度类型为【商圈额度】");
                    log.info("商圈额度一期暂不开放");
                }
            }

            if (CollectionUtils.isEmpty(checkMap)) {
                throw new YuspException(EcbEnum.THAMT_CHECK_RESULT_EXCEPTION.key, EcbEnum.THAMT_CHECK_RESULT_EXCEPTION.value);
            }

            //获取返回的结果信息
            rtnCode = (String) checkMap.get("checkCode");
            rtnMsg = (String) checkMap.get("checkMsg");

            log.info("业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验完成！返回结果：" + rtnCode + "，返回信息：" + rtnMsg);
        } catch (YuspException e) {
            log.error("校验第三方额度出现异常！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("校验第三方额度出现异常！", e);
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, EcbEnum.IQP_EXCEPTION_DEF.value + " 异常原因：" + e.getMessage());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 校验第三方额度-融资担保公司额度信息
     *
     * @param iqpLmtRelDto
     * @return
     */
    private Map checkThFinAmtInsert(IqpLmtRelDto iqpLmtRelDto) {
        log.info("新增时，业务申请" + iqpLmtRelDto.getIqpSerno() + "引入第三方额度" + iqpLmtRelDto.getThirdLimitId() + "校验开始！额度类型为【融资担保公司额度】");
        Map checkMap = new HashMap();
        String checkCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String checkMsg = EcbEnum.IQP_SUCCESS_DEF.value;

        try {
            //获取融资担保公司的单户限额singleAmt
            BigDecimal singleAmt = iqpLmtRelDto.getSingleAmt();
            //融资担保额度的担保总敞口限额guarTotlSpac
            BigDecimal guarTotlSpac = iqpLmtRelDto.getGuarTotlSpac();
            //融资担保额度的产品总限额totlAmt
            BigDecimal totlAmt = iqpLmtRelDto.getTotlAmt();
            //融资担保额度的产品单户限额sigAmt
            BigDecimal sigAmt = iqpLmtRelDto.getSigAmt();


            if (singleAmt == null || guarTotlSpac == null || totlAmt == null || sigAmt == null) {
                throw new YuspException(EcbEnum.THAMT_CHECK_FIPARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_FIPARAM_EXCEPTION.value);
            }
            //调用获取历史金额的方法
            Map<String, BigDecimal> amtMap = getHisAmt(iqpLmtRelDto);
            BigDecimal totalAmt = amtMap.get("totalAmt");
            BigDecimal cusTotalAmt = amtMap.get("cusTotalAmt");
            BigDecimal prdTotalAmt = amtMap.get("prdTotalAmt");
            BigDecimal cusPrdTotalAmt = amtMap.get("cusPrdTotalAmt");

            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取申请历史中引用该笔融资担保公司额度的占用金额为：" + totalAmt.toString());
            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取该客户申请历史中引用该笔融资担保公司额度的占用金额为：" + cusTotalAmt.toString());
            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取引用该笔融资担保公司下产品" + iqpLmtRelDto.getPrdId() + "额度的占用金额为：" + prdTotalAmt.toString());
            log.info("业务申请" + iqpLmtRelDto.getIqpSerno() + "校验第三方额度" + iqpLmtRelDto.getThirdLimitId() + "-获取该客户申请历史中引用该笔融资担保公司下产品" + iqpLmtRelDto.getPrdId() + "额度的占用金额为：" + cusPrdTotalAmt.toString());
            //新增时校验
            if (CmisBizConstants.TH_LMT_CHECK_STAGE_INSERT.equals(iqpLmtRelDto.getThCheckStage())) {
                //比较融资担保额度的担保总敞口限额与历史占用总额度
                if (guarTotlSpac.compareTo(totalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FIGUARTOTLSPAC_EXCEPTION.key, EcbEnum.THAMT_CHECK_FIGUARTOTLSPAC_EXCEPTION.value);
                }
                //比较融资担保额度的单户限额与客户历史占额
                if (singleAmt.compareTo(cusTotalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FISINGLE_EXCEPTION.key, EcbEnum.THAMT_CHECK_FISINGLE_EXCEPTION.value);
                }
                //比较融资担保额度的产品总限额与产品历史占用额度
                if (totlAmt.compareTo(prdTotalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.value);
                }
                //比较融资担保额度的产品单户限额与客户历史选用该产品的占用额度
                if (sigAmt.compareTo(cusPrdTotalAmt) <= 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.value);
                }
            } else if (CmisBizConstants.TH_LMT_CHECK_STAGE_UPDATE.equals(iqpLmtRelDto.getThCheckStage())) {//修改时校验
                //获取本次的风险敞口金额
                BigDecimal riskOpenAmt = iqpLmtRelDto.getRiskOpenAmt();
                //融资担保额度的单次限额oneAmt
                BigDecimal oneAmt = iqpLmtRelDto.getOneAmt();
                if (riskOpenAmt == null || oneAmt == null) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.key, EcbEnum.THAMT_CHECK_CIPARAM_EXCEPTION.value);
                }
                //比较融资担保额度的担保总敞口限额与(本次申请风险敞口金额+历史占用总额度）
                if (guarTotlSpac.compareTo(riskOpenAmt.add(totalAmt)) < 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FIGUARTOTLSPAC_EXCEPTION.key, EcbEnum.THAMT_CHECK_FIGUARTOTLSPAC_EXCEPTION.value);
                }
                //比较融资担保额度的单户限额与(本次申请风险敞口金额+客户历史占额）
                if (singleAmt.compareTo(riskOpenAmt.add(cusTotalAmt)) < 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FISINGLE_EXCEPTION.key, EcbEnum.THAMT_CHECK_FISINGLE_EXCEPTION.value);
                }
                //比较融资担保额度的产品总限额与(本次申请风险敞口金额+产品历史占用额度）
                if (totlAmt.compareTo(riskOpenAmt.add(prdTotalAmt)) < 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.value);
                }
                //比较融资担保额度的产品单户限额与(本次申请风险敞口金额+客户历史选用该产品的占用额度）
                if (sigAmt.compareTo(riskOpenAmt.add(cusPrdTotalAmt)) < 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.key, EcbEnum.THAMT_CHECK_FITOTLAMT_EXCEPTION.value);
                }
                //比较融资担保单次限额与本次申请风险敞口金额
                if (oneAmt.compareTo(riskOpenAmt) < 0) {
                    throw new YuspException(EcbEnum.THAMT_CHECK_FIONE_EXCEPTION.key, EcbEnum.THAMT_CHECK_FIONE_EXCEPTION.value);
                }
            }

        } catch (YuspException e) {
            checkCode = e.getCode();
            checkMsg = e.getMsg();
        } catch (Exception e) {
            log.error("操作异常！", e);
            checkCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            checkMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "原因：" + e.getMessage();
        } finally {
            checkMap.put("checkCode", checkCode);
            checkMap.put("checkMsg", checkMsg);
        }
        return checkMap;
    }

    /**
     * 获取历史的占用额度信息
     * totalAmt 引用该笔第三方额度的总金额
     * cusTotalAmt 该客户名下引用该笔第三方的总金额
     * prdTotalAmt 引用该笔第三方下对应产品的总额度
     * cusPrdTotalAmt 该客户历史引用该笔第三方下对应产品的总额度
     *
     * @param iqpLmtRelDto
     * @return
     */
    private Map<String, BigDecimal> getHisAmt(IqpLmtRelDto iqpLmtRelDto) {
        Map amtMap = new HashMap();
        BigDecimal totalAmt = new BigDecimal(0);//引用该笔第三方额度的总金额
        BigDecimal cusTotalAmt = new BigDecimal(0);//该客户名下引用该笔第三方的总金额
        BigDecimal prdTotalAmt = new BigDecimal(0);//引用该笔第三方下对应产品的总额度
        BigDecimal cusPrdTotalAmt = new BigDecimal(0);//该客户历史引用该笔第三方下对应产品的总额度

        String iqpSerno = iqpLmtRelDto.getIqpSerno();
        String thirdLimitId = iqpLmtRelDto.getThirdLimitId();
        String cusId = iqpLmtRelDto.getCusId();
        String prdId = iqpLmtRelDto.getPrdId();
        Map params = new HashMap();
        params.put("thLimitType", iqpLmtRelDto.getThirdLimitType());
        params.put("thLimitId", iqpLmtRelDto.getThirdLimitId());
        log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验-流程中引用该笔第三方额度的数据查询");
        List<IqpLoanApp> wfIqpLoanAppList = iqpLoanAppMapper.selectIqpLoanAppByThLimitId(params);
        log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验-审批通过数据中引用该笔第三方额度的数据查询");
        List<CtrLoanCont> ctrLoanContList = ctrLoanContService.selectCtrLoanContByThLimitId(params);

        log.info("新增时，业务申请" + iqpSerno + "引入第三方额度" + thirdLimitId + "校验-获取申请历史中引用该笔第三方额度的占用金额");
        //累加流程中的风险敞口金额
        if (CollectionUtils.nonEmpty(wfIqpLoanAppList)) {
            for (IqpLoanApp iqpLoanApp : wfIqpLoanAppList) {
                //针对风险敞口金额为空的场景，跳过处理
                if (iqpLoanApp.getRiskOpenAmt() == null) continue;
                //累加风险敞口金额
                totalAmt = totalAmt.add(iqpLoanApp.getRiskOpenAmt());

                //客户名下累加金额
                if (cusId.equals(iqpLoanApp.getCusId())) {
                    cusTotalAmt = cusTotalAmt.add(iqpLoanApp.getRiskOpenAmt());
                }

                //该产品累加金额
                if (prdId.equals(iqpLoanApp.getPrdId())) {
                    prdTotalAmt = prdTotalAmt.add(iqpLoanApp.getRiskOpenAmt());
                }

                //该客户引用该额度下的产品的总金额
                if (cusId.equals(iqpLoanApp.getCusId()) && prdId.equals(iqpLoanApp.getPrdId())) {
                    cusPrdTotalAmt = cusPrdTotalAmt.add(iqpLoanApp.getRiskOpenAmt());
                }
            }
        }

        //累加已办结的风险敞口金额
        if (CollectionUtils.nonEmpty(ctrLoanContList)) {
            for (CtrLoanCont ctrLoanCont : ctrLoanContList) {
                //针对风险敞口金额为空的场景，跳过处理
                if (ctrLoanCont.getRiskOpenAmt() == null) continue;
                //累加风险敞口金额
                totalAmt = totalAmt.add(ctrLoanCont.getRiskOpenAmt());

                //客户名下累加金额
                if (cusId.equals(ctrLoanCont.getCusId())) {
                    cusTotalAmt = cusTotalAmt.add(ctrLoanCont.getRiskOpenAmt());
                }

                //该产品累加金额
                if (prdId.equals(ctrLoanCont.getPrdId())) {
                    prdTotalAmt = prdTotalAmt.add(ctrLoanCont.getRiskOpenAmt());
                }

                //该客户引用该额度下的产品的总金额
                if (cusId.equals(ctrLoanCont.getCusId()) && prdId.equals(ctrLoanCont.getPrdId())) {
                    cusPrdTotalAmt = cusPrdTotalAmt.add(ctrLoanCont.getRiskOpenAmt());
                }
            }
        }

        amtMap.put("totalAmt", totalAmt);
        amtMap.put("cusTotalAmt", cusTotalAmt);
        amtMap.put("prdTotalAmt", prdTotalAmt);
        amtMap.put("cusPrdTotalAmt", cusPrdTotalAmt);

        return amtMap;
    }


    /**
     * 根据流水号，查询是否存在待发起和审批中的复议信息
     *
     * @param iqpSerno
     * @return
     */
    public HashMap<String, String> getDoReconsidFromBk(String iqpSerno) {
        return iqpLoanAppMapper.getDoReconsidFromBk(iqpSerno);
    }

    /**
     * 流程发起进行的业务处理
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
     * 1、更新申请主表的审批状态-111
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String iqpSerno)  throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("流程发起-获取业务申请" + iqpSerno + "申请主表信息");
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(iqpSerno);
        if (iqpLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        int updateCount = 0;

        log.info("流程发起-更新业务申请" + iqpSerno + "流程审批状态为【111】-审批中");
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_111);
        if (updateCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //向额度系统发送接口,占用额度
        this.sendToLmt(iqpLoanApp);
    }

    public void sendToLmt(IqpLoanApp iqpLoanApp) {
        //向额度系统发送接口,占用额度
        String guarMode = iqpLoanApp.getGuarWay();
        //判断是否无缝衔接
        String isSeajnt = "";
        if(iqpLoanApp.getIsSeajnt()!=null && !"".equals(iqpLoanApp.getIsSeajnt())){
            isSeajnt = iqpLoanApp.getIsSeajnt();
        }else{
            if(CmisCommonConstants.STD_LOAN_MODAL_3.equals(iqpLoanApp.getLoanModal())){
                isSeajnt = CmisCommonConstants.STD_ZB_YES_NO_1;
            }else{
                isSeajnt = CmisCommonConstants.STD_ZB_YES_NO_0;
            }
        }
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpLoanApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //不是低风险业务
        if (!CmisCommonConstants.GUAR_MODE_60.equals(guarMode) && !CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                && !CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(isSeajnt);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpLoanApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpLoanApp.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(iqpLoanApp.getContHighAvlAmt());//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanApp.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(iqpLoanApp.getCvtCnyAmt());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("普通贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("普通贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,code, resultDtoDto.getData().getErrorMsg());
            }
        }

        //是低风险且授信不足额的占额
        if ((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) && iqpLoanApp.getLmtAccNo()!=null){

            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(isSeajnt);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpLoanApp.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpLoanApp.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpLoanApp.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanApp.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanApp.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("普通贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("普通贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,code, resultDtoDto.getData().getErrorMsg());
            }
        }

    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【打回】，业务与担保合同关系结果表数据更新为【打回】
     *
     * 放款申请打回后，仅将当前申请状态变更为“打回”，该笔业务的贷款合同及合同与担保合同关系结果不变，若需作废合同，则人工在合同管理模块将该合同作废。
     * 2、更新申请主表的审批状态为998 打回
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("放款申请打回流程-获取放款申请" + iqpSerno + "申请信息");
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = iqpLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("放款申请" + iqpSerno + "流程打回业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }

    /**
     * 放款申请拒绝，后续的业务处理
     * 0、针对单笔单批业务，更新审批模式子表中的流程状态
     * 1、合同与担保合同关系结果表数据状态更新为【拒绝】，业务与担保合同关系结果表数据更新为【拒绝】
     * 2、更新申请主表的审批状态为998 【拒绝】
     *
     * @param iqpSerno
     */
    public void handleBusinessAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PARAMS_EXCEPTION.value);
            }
            log.info("贷款合同申请否决流程-获取贷款合同申请申请" + iqpSerno + "申请信息");
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.key, EcbEnum.E_PVP_WFHAND_PVPNULL_EXCEPTION.value);
            }
            int updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if(updateCount<=0){
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            if (iqpLoanApp.getLmtAccNo() != null && !"".equals(iqpLoanApp.getLmtAccNo())) {
                String guarMode = iqpLoanApp.getGuarWay();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if(CmisCommonConstants.STD_BUSI_TYPE_05.equals(iqpLoanApp.getBizType())){
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                }else{
                    if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                            || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                        recoverSpacAmtCny = BigDecimal.ZERO;
                        recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                    } else {
                        recoverSpacAmtCny = iqpLoanApp.getCvtCnyAmt();
                        recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                    }
                }

                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("贷款合同申请【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0012ReqDto));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("贷款合同申请【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", iqpSerno, JSON.toJSONString(resultDto));
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorCode());
                }
            }
        } catch (BizException e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new BizException(null,e.getErrorCode(),null,e.getMessage());
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请" + iqpSerno + "流程否决业务处理异常！原因：" + e.getMessage());
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + "," + e.getMessage());
        }
    }


    /**
     * 审批通过后进行的业务操作
     * 0、针对单笔单批业务，更新审批模式表中的全流程状态为【2-审批通过】
     * 1、将申请主表、辅助信息表、还款信息表数据添加到合同主表中，生成合同记录
     * 2、将生成的合同编号信息更新到业务子表中
     * 3、添加业务相关的结果表，担保和业务关系结果表、授信与合同关联表(包括授信与第三方额度)
     * 4、更新申请主表的审批状态以及审批通过时间
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String iqpSerno) throws Exception{
        if (StringUtils.isBlank(iqpSerno)) {
            throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
        }

        log.info("审批通过-获取业务申请" + iqpSerno + "申请主表信息");
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(iqpSerno);
        if (iqpLoanApp == null) {
            throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
        }

        int updateCount = 0;
        updateCount = this.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_997);
        if (updateCount <= 0) {
            throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
        }
        //判断是否无缝衔接
        String isSeajnt = "";
        if(iqpLoanApp.getLoanModal()!=null && !"".equals(iqpLoanApp.getLoanModal())){
            isSeajnt = iqpLoanApp.getLoanModal();
        }else{
            if(CmisCommonConstants.STD_LOAN_MODAL_3.equals(iqpLoanApp.getLoanModal())){
                isSeajnt = CmisCommonConstants.STD_ZB_YES_NO_1;
            }else{
                isSeajnt = CmisCommonConstants.STD_ZB_YES_NO_0;
            }
        }
        //是否合同重签
        String isBizRev = "";
        //原交易业务编号
        String origiDealBizNo = "";
        //原交易业务状态
        String origiDealBizStatus = "";
        //原交易业务恢复类型
        String origiRecoverType = "";
        //原交易属性D
        String origiBizAttr = "";
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsRenew())){
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_1;
            origiDealBizNo = iqpLoanApp.getOrigiContNo();
            origiDealBizStatus = CmisLmtConstants.STD_ZB_BIZ_STATUS_300;
            origiRecoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
            origiBizAttr = CmisLmtConstants.STD_ZB_BIZ_ATTR_1;
        }else{
            isBizRev = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        //判断是否需要反向生成低风险分项，以及后续的逻辑处理
        if (iqpLoanApp.getLmtAccNo() == null || "".equals(iqpLoanApp.getLmtAccNo())) {
            BigDecimal bailPerc = BigDecimal.ZERO;//保证金比例
            BigDecimal chrgRate = BigDecimal.ZERO;//手续费率
            boolean result = lmtReplyAccService.generaLmtReplyAccForLowRisk(iqpLoanApp.getCvtCnyAmt(), iqpLoanApp.getCusId(), iqpLoanApp.getBizType(), bailPerc, chrgRate,iqpLoanApp.getEndDate(),CmisCommonConstants.STD_ZB_YES_NO_0);
            if (!result) {
                log.error("普通贷款合同申请: " + iqpSerno + "反向生成低风险额度分项异常");
                throw BizException.error(null, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            String replySerno = "";
            String lmtAccNo = "";
            log.info("贷款合同申请: "+iqpSerno+"获取批复编号和授信额度编号开始");
            // 根据客户号以及产品编号查询生成的分项下对应的低风险分项明细
            CmisLmt0056ReqDto cmisLmt0056ReqDto = new CmisLmt0056ReqDto();
            cmisLmt0056ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));
            cmisLmt0056ReqDto.setPrdId(iqpLoanApp.getPrdId());
            cmisLmt0056ReqDto.setCusId(iqpLoanApp.getCusId());
            log.info("贷款合同业务申请【{}】，前往额度系统查询低风险分项明细,请求报文为:【{}】", iqpSerno, cmisLmt0056ReqDto.toString());
            ResultDto<CmisLmt0056RespDto> resultDto = cmisLmtClientService.cmislmt0056(cmisLmt0056ReqDto);
            log.info("贷款合同业务申请【{}】，前往额度系统查询低风险分项明细,响应报文为:【{}】", iqpSerno, resultDto.toString());
            if(!"0".equals(resultDto.getCode())){
                log.error("额度0056接口调用异常！");
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            if(!"0000".equals(resultDto.getData().getErrorCode())){
                log.error("查询低风险分项明细失败！异常信息为【{}】",resultDto.getData().getErrorMsg());
                throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
            }
            lmtAccNo = resultDto.getData().getApprSubSerno();
            log.info("贷款合同申请【{}】的授信额度编号为【{}】",iqpSerno,lmtAccNo);
            // 根据授信额度编号获取批复流水号
            LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtAccNo);
            if(Objects.isNull(lmtReplyAccSubPrd)){
                log.error("根据授信额度编号【{}】查询授信产品明细为空！",lmtAccNo);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }

            Map map = new HashMap();
            map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
            if(Objects.isNull(lmtReplyAccSub)){
                log.error("根据分项额度编号【{}】查询授信分项明细为空！",lmtReplyAccSubPrd.getAccSubNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            replySerno = lmtReplyAccSub.getReplySerno();
            log.info("贷款合同申请【{}】的批复编号为【{}】",iqpSerno,replySerno);

            //生成低风险分项后，更新申请数据
            int updateCountData = 0;
            log.info("更新贷款业务申请" + iqpSerno + "开始");
            IqpLoanApp iqpLoanAppData = iqpLoanAppMapper.selectByIqpSerno(iqpSerno);
            iqpLoanAppData.setLmtAccNo(lmtAccNo);
            iqpLoanAppData.setReplyNo(replySerno);
            log.info("批复编号为【{}】,授信额度编号为【{}】",lmtAccNo,replySerno);
            updateCountData = this.updateSelective(iqpLoanAppData);
            if (updateCountData < 0) {
                log.error("更新贷款业务申请【{}】异常",iqpSerno);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            log.info("更新贷款业务申请" + iqpSerno + "结束");
        }

        IqpLoanApp iqpLoanAppObj = iqpLoanAppMapper.selectByIqpSerno(iqpSerno);
        log.info("审批通过生成合同信息-合同填充业务申请" + iqpSerno + "信息");
        //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
        //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
        CtrLoanCont ctrLoanCont = new CtrLoanCont();
        log.info("获取合同申请信息【{}】",iqpLoanAppObj);
        BeanUtils.copyProperties(iqpLoanAppObj, ctrLoanCont);
        log.info("获取合同信息【{}】",ctrLoanCont);
        ctrLoanCont.setOprType(CommonConstance.OPR_TYPE_ADD);
        ctrLoanCont.setBizType(iqpLoanAppObj.getBizType());
        ctrLoanCont.setContStatus(CmisCommonConstants.CONT_STATUS_100);//合同状态默认【生效】
        ctrLoanCont.setContPrintNum(BigDecimal.ZERO);//合同打印次数默认设置为【0】
        // 个别字段没有复制成功
        ctrLoanCont.setDayLimit(iqpLoanAppObj.getDrawTerm());// 合同金额
        ctrLoanCont.setContAmt(iqpLoanAppObj.getContAmt());// 合同金额
        ctrLoanCont.setAddr(iqpLoanAppObj.getDeliveryAddr());// 地址
        ctrLoanCont.setContStartDate(iqpLoanAppObj.getStartDate());// 合同起始日
        ctrLoanCont.setCvtCnyAmt(iqpLoanAppObj.getCvtCnyAmt());// 折算人民币金额
        ctrLoanCont.setContEndDate(iqpLoanAppObj.getEndDate());// 合同到期日
        ctrLoanCont.setContRate(iqpLoanAppObj.getAppRate());//APP_RATE -》 CONT_RATE 汇率
        ctrLoanCont.setHighAvlAmt(iqpLoanAppObj.getContHighAvlAmt());//合同下可用余额
        ctrLoanCont.setLprRateIntval(iqpLoanAppObj.getLprRateIntval()); //LPR定价区间
        ctrLoanCont.setAddr(iqpLoanAppObj.getDeliveryAddr()); //地址

        ctrLoanCont.setCtrBeginFlag(CmisCommonConstants.STD_ZB_YES_NO_0);//线上合同启用标识
        ctrLoanCont.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        log.info("获取合同信息【{}】",ctrLoanCont);
        int insertCount = ctrLoanContService.insert(ctrLoanCont);
        if (insertCount < 0) {
            throw new YuspException(EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.key, EcbEnum.E_IQP_CTRLOANCONTINSERT_EXCEPTION.value);
        }
        // 判断是否续签合同，如果是续签合同则原合同置中止
        if(Objects.equals("1",iqpLoanApp.getIsRenew())) {
            // 获取原合同
            String origiContNo = iqpLoanApp.getOrigiContNo();
            if(StringUtils.nonBlank(origiContNo)) {
                CtrLoanCont origiCtrLoanCont = ctrLoanContMapper.selectByPrimaryKey(origiContNo);
                if(Objects.nonNull(origiCtrLoanCont)) {
                    origiCtrLoanCont.setContStatus(CmisBizConstants.IQP_CONT_STS_500);
                    updateCount = ctrLoanContService.update(origiCtrLoanCont);
                    if (updateCount < 1) {
                        throw new YuspException(EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.key, EcbEnum.E_IQP_ORIGICTRLOANCONT_EXCEPTION.value);
                    }
                }

            }
        }
        //如果为福费廷且授信足额的情况直接走低风险占额
        if (CmisCommonConstants.STD_BUSI_TYPE_05.equals(iqpLoanApp.getBizType())) {
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanAppObj.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(iqpLoanAppObj.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(iqpLoanAppObj.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(iqpLoanAppObj.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType(iqpLoanAppObj.getContType());//交易业务类型
            cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
            cmisLmt0011ReqDto.setPrdId(iqpLoanAppObj.getPrdId());//产品编号
            cmisLmt0011ReqDto.setPrdName(iqpLoanAppObj.getPrdName());//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz(isSeajnt);//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
            cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
            cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
            cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
            cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
            cmisLmt0011ReqDto.setDealBizAmt(iqpLoanAppObj.getCvtCnyAmt());//交易业务金额
            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
            cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
            cmisLmt0011ReqDto.setStartDate(iqpLoanAppObj.getStartDate());//合同起始日
            cmisLmt0011ReqDto.setEndDate(iqpLoanAppObj.getEndDate());//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
            cmisLmt0011ReqDto.setInputId(iqpLoanAppObj.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(iqpLoanAppObj.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(iqpLoanAppObj.getInputDate());//登记日期

            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanAppObj.getLmtAccNo());//额度分项编号
            cmisLmt0011OccRelListDto.setPrdTypeProp(iqpLoanAppObj.getPrdTypeProp());//授信品种类型
            cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanAppObj.getContHighAvlAmt());//占用总额(原币种)
            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanAppObj.getCvtCnyAmt());//占用总额(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

            log.info("福费廷贷款业务申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("福费廷贷款业务申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
            if(!"0".equals(resultDtoDto.getCode())){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if(!"0000".equals(code)){
                log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        }else{
            String guarMode = iqpLoanAppObj.getGuarWay();
            String isAmtEnough =  checkLmtAmtIsEnough(iqpLoanAppObj);
            //判断是否是低风险的一步流程，是：进行低风险占额后再按照正常流程走；否：直接按照正常流程走
            if((CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_40.equals(guarMode))) {

                CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanAppObj.getManagerBrId()));//金融机构代码
                cmisLmt0011ReqDto.setDealBizNo(iqpLoanAppObj.getContNo());//合同编号
                cmisLmt0011ReqDto.setCusId(iqpLoanAppObj.getCusId());//客户编号
                cmisLmt0011ReqDto.setCusName(iqpLoanAppObj.getCusName());//客户名称
                cmisLmt0011ReqDto.setDealBizType(iqpLoanAppObj.getContType());//交易业务类型
                cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                cmisLmt0011ReqDto.setPrdId(iqpLoanAppObj.getPrdId());//产品编号
                cmisLmt0011ReqDto.setPrdName(iqpLoanAppObj.getPrdName());//产品名称
                cmisLmt0011ReqDto.setIsLriskBiz(CmisCommonConstants.STD_ZB_YES_NO_1);//是否低风险
                cmisLmt0011ReqDto.setIsFollowBiz(isSeajnt);//是否无缝衔接
                cmisLmt0011ReqDto.setIsBizRev(isBizRev);//是否合同重签
                cmisLmt0011ReqDto.setOrigiDealBizNo(origiDealBizNo);//原交易业务编号
                cmisLmt0011ReqDto.setOrigiDealBizStatus(origiDealBizStatus);//原交易业务状态
                cmisLmt0011ReqDto.setOrigiRecoverType(origiRecoverType);//原交易业务恢复类型
                cmisLmt0011ReqDto.setOrigiBizAttr(origiBizAttr);//原交易属性D
                cmisLmt0011ReqDto.setDealBizAmt(iqpLoanAppObj.getCvtCnyAmt());//交易业务金额
                cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
                cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
                cmisLmt0011ReqDto.setStartDate(iqpLoanAppObj.getStartDate());//合同起始日
                cmisLmt0011ReqDto.setEndDate(iqpLoanAppObj.getEndDate());//合同到期日
                cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
                cmisLmt0011ReqDto.setInputId(iqpLoanAppObj.getInputId());//登记人
                cmisLmt0011ReqDto.setInputBrId(iqpLoanAppObj.getInputBrId());//登记机构
                cmisLmt0011ReqDto.setInputDate(iqpLoanAppObj.getInputDate());//登记日期

                List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
                CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
                cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanAppObj.getLmtAccNo());//额度分项编号
                cmisLmt0011OccRelListDto.setPrdTypeProp(iqpLoanAppObj.getPrdTypeProp());//授信品种类型
                cmisLmt0011OccRelListDto.setBizTotalAmt(iqpLoanAppObj.getContHighAvlAmt());//占用总额(原币种)
                cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(原币种)
                cmisLmt0011OccRelListDto.setBizTotalAmtCny(iqpLoanAppObj.getCvtCnyAmt());//占用总额(折人民币)
                cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
                cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                log.info("贷款合同申请【{}】，前往额度系统进行额度占用开始,请求报文为:【{}】", iqpSerno, JSON.toJSONString(cmisLmt0011ReqDto));
                ResultDto<CmisLmt0011RespDto>  resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                log.info("贷款合同申请【{}】，前往额度系统进行额度占用结束,返回报文为:【{}】", iqpSerno, JSON.toJSONString(resultDtoDto));
                if(!"0".equals(resultDtoDto.getCode())){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                String code = resultDtoDto.getData().getErrorCode();
                if(!"0000".equals(code)){
                    log.error("业务申请占用额度异常！"+resultDtoDto.getData().getErrorMsg());
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
        log.info("审批通过生成合同" + iqpSerno + "结束");
    }

    /**
     * 单笔单批生成放款申请信息
     *
     * @param iqpSerno
     */
    public void genPvpLoanApp(String iqpSerno) {
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);//申请信息
        String bizType;
        bizType = iqpLoanApp.getBizType();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", iqpLoanApp.getIqpSerno());//全局编号
        List<CtrLoanCont> ctrLoanConts = ctrLoanContService.selectByModel(queryModel);
        CtrLoanCont ctrLoanCont = ctrLoanConts.get(0);//合同信息
        String contNo = ctrLoanCont.getContNo();//合同号
        String pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());


        //保存进行校验
        //1、该笔合同是否存在【待发起、拿回、打回、流程中】的放款申请数据
        Map pvpQueryMap = new HashMap();
        pvpQueryMap.put("contNo", contNo);
        pvpQueryMap.put("wfExistsFlag", true);//是否存在待审批的申请标志位
        log.info("保存放款申请" + contNo + "数据-校验是否存在待审批的放款申请数据");
        //List<PvpLoanApp> pvpLoanAppList = pvpLoanAppService.selectPvpLoanAppInfoByParams(pvpQueryMap);
        /*if (CollectionUtils.nonEmpty(pvpLoanAppList) && pvpLoanAppList.size() > 0) {
            throw new YuspException(EcbEnum.E_PVP_WFEXISTS.key, EcbEnum.E_PVP_WFEXISTS.value);
        }*/

        //2、针对非额度项下申请，是否存在待生效、生效的借据数据
        log.info("保存放款申请" + contNo + "数据-校验是否存在生效的借据信息");
        if (!CmisBizConstants.BIZ_TYPE_UQ.equals(bizType)) {
            pvpQueryMap.put("wfEndExistsFlag", true);//是否存在有效的借据标志位

            List<AccLoan> accLoanList = accLoanService.selectAccLoanByParams(pvpQueryMap);
            if (CollectionUtils.nonEmpty(accLoanList) && accLoanList.size() > 0) {
                throw new YuspException(EcbEnum.E_PVP_WFEXISTS.key, EcbEnum.E_PVP_WFEXISTS.value);
            }
        }

        //调用工具类
        BizUtils bizUtils = new BizUtils();
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        pvpLoanApp = (PvpLoanApp) bizUtils.getMappingValueBySourceAndDisTable(ctrLoanCont, pvpLoanApp);
        if (pvpLoanApp == null) {
            throw new YuspException(EcbEnum.E_GETMAPPING_EXCEPTION.key, EcbEnum.E_GETMAPPING_EXCEPTION.value);
        }

        log.info("保存放款申请" + pvpSerno + "数据-获取当前登录用户数据");

        pvpLoanApp.setInputId(iqpLoanApp.getInputId());
        pvpLoanApp.setInputBrId(iqpLoanApp.getInputBrId());
        pvpLoanApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));

        pvpLoanApp.setPvpSerno(pvpSerno);
        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//设置状态为"待发起"
        pvpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//设置操作状态为"新增"

        log.info("保存放款申请" + pvpSerno + "数据-计算默认的到期日期");
        String appTerm = pvpLoanApp.getAppTerm();
        String termType = pvpLoanApp.getTermType();
        if (StringUtils.nonBlank(appTerm) && StringUtils.nonBlank(termType)) {
            String currentDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            String endDate = "";
            if (CmisBizConstants.IQP_TERM_TYPE_YEAR.equals(termType)) {//期限类型为年
                endDate = DateUtils.addYear(currentDate, DateFormatEnum.DEFAULT.getValue(), Integer.parseInt(appTerm));
            } else if (CmisBizConstants.IQP_TERM_TYPE_MONTH.equals(termType)) {//期限类型为月
                endDate = DateUtils.addMonth(currentDate, DateFormatEnum.DEFAULT.getValue(), Integer.parseInt(appTerm));
            } else if (CmisBizConstants.IQP_TERM_TYPE_DAY.equals(termType)) {//期限类型为日
                endDate = DateUtils.addDay(currentDate, DateFormatEnum.DEFAULT.getValue(), Integer.parseInt(appTerm));
            }
            if (StringUtils.nonBlank(endDate)) {
                pvpLoanApp.setEndDate(endDate);
            }
        }
        log.info("保存放款申请" + pvpSerno + "数据-落地申请");

        int insertCount = pvpLoanAppService.insertSelective(pvpLoanApp);
        if (insertCount < 0) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + ",保存失败！");
        }

        //将合同主办人信息复制到放款申请主办人信息中
        log.info("保存放款申请" + pvpSerno + "数据-获取合同主办人员信息");
        pvpQueryMap.put("bizSerno", pvpLoanApp.getIqpSerno());
        pvpQueryMap.put("correMgrType", CmisBizConstants.CORRE_REL_TYPE_MAIN);//主办人员
        List<BizCorreManagerInfo> bizCorreManagerInfoList = bizCorreManagerInfoService.selectBizCorreManagerInfoByBizSerno(pvpQueryMap);
        if (CollectionUtils.isEmpty(bizCorreManagerInfoList) || bizCorreManagerInfoList.size() == 0) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + ",未获取到主办人员信息！");
        }

        BizCorreManagerInfo bizCorreManagerInfo = bizCorreManagerInfoList.get(0);
        bizCorreManagerInfo.setPkId(StringUtils.uuid(true));
        bizCorreManagerInfo.setBizSerno(pvpSerno);//设置为放款申请的流水号


        log.info("保存放款申请" + pvpSerno + "数据-保存放款申请主办人员信息");
        insertCount = bizCorreManagerInfoService.insertSelective(bizCorreManagerInfo);
        if (insertCount < 0) {
            //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
            throw new YuspException(EcbEnum.PVP_EXCEPTION_DEF.key, EcbEnum.PVP_EXCEPTION_DEF.value + ",保存放款申请主办人信息异常！");
        }
    }

    /**
     * 单笔单批构建台账表信息
     *
     * @param iqpSerno
     * @param contNo
     * @return
     */
    private AccLoan genAccLoanInfo(String iqpSerno, String contNo) {
        AccLoan accLoan = new AccLoan();
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);//申请信息
        IqpLoanAppRepay iqpLoanAppRepay = iqpLoanAppRepayService.selectByPrimaryKey(iqpSerno);//还款信息
        IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppAssistService.selectByPrimaryKey(iqpSerno);//辅助信息表

        HashMap<String, String> param = new HashMap<>();
        param.put("contNo", contNo);
        String billNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, param);//生成billNo
        accLoan.setBillNo(billNo);//借据号
        //accLoan.setSerno(iqpLoanApp.getSerno());//全局serno
        String pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
        accLoan.setPvpSerno(pvpSerno);//放款流水号
        accLoan.setContNo(contNo);//合同号
        accLoan.setCusId(iqpLoanApp.getCusId());//用户编号
        accLoan.setPrdId(iqpLoanApp.getPrdId());//产品编号
        /**
         * zhanyb修改20210427。以下为原内容
         accLoan.setGuarWay(iqpLoanApp.getGuarWay());//担保方式
         accLoan.setRepayWay(iqpLoanAppRepay.getRepayType());//还款方式
         accLoan.setStopPintTerm(iqpLoanAppRepay.getStopPintTerm());//停本付息期间
         accLoan.setRepayTerm(iqpLoanAppRepay.getRepayTerm());//还款间隔周期
         accLoan.setRepaySpace(iqpLoanAppRepay.getRepaySpace());//还款间隔
         accLoan.setRepayRule(iqpLoanAppRepay.getRepayRule());//还款日确定规则
         accLoan.setRepayDtType(iqpLoanAppRepay.getRepayDtType());//确定还款日 类型
         accLoan.setRepayDate(iqpLoanAppRepay.getRepayDate());//还款日
         accLoan.setTermType(iqpLoanApp.getTermType());//期限类型
         accLoan.setAppTerm(iqpLoanApp.getAppTerm());//申请期限
         accLoan.setRulingIr(iqpLoanApp.getRulingIr());//基准利率
         accLoan.setRealityIrY(iqpLoanApp.getRealityIrY());//执行利率年
         accLoan.setOverdueRateY(iqpLoanApp.getOverdueRateY());//执行逾期利率
         accLoan.setDefaultRateY(iqpLoanApp.getDefaultRateY());//违约利率
         accLoan.setCurType(iqpLoanApp.getAppCurType());//币种
         accLoan.setLoanBalance(iqpLoanApp.getAppAmt());//贷款余额
         accLoan.setStartDate(iqpLoanApp.getAppDate());//贷款起始日期
         */
        accLoan.setGuarMode(iqpLoanApp.getGuarWay());//担保方式
        accLoan.setRepayMode(iqpLoanAppRepay.getRepayType());//还款方式
        //accLoan.setRepayTerm(iqpLoanAppRepay.getRepayTerm());//待找需求确定是否要此字段
        accLoan.setEiIntervalCycle(iqpLoanAppRepay.getRepaySpace());//结息间隔周期
        //accLoan.setRepayRule(iqpLoanAppRepay.getRepayRule());//待找需求确定是否要此字段
        //accLoan.setRepayDtType(iqpLoanAppRepay.getRepayDtType());//待找需求确定是否要此字段
        accLoan.setDeductDay(iqpLoanAppRepay.getRepayDate().toString());//扣款日DEDUCT_DAY
        accLoan.setLoanTer(iqpLoanApp.getAppTerm().toString());//贷款期限LOAN_TERM
        accLoan.setRulingIr(iqpLoanApp.getRulingIr());//基准利率
        accLoan.setExecRateYear(iqpLoanApp.getExecRateYear());//执行利率(年)
        accLoan.setOverdueExecRate(iqpLoanApp.getOverdueExecRate());//逾期执行利率(年利率)
        accLoan.setOverdueExecRate(iqpLoanApp.getDefaultRateY());//违约利率 OVERDUE_EXEC_RATE逾期执行利率(年利率)
        accLoan.setContCurType(iqpLoanApp.getAppCurType());//币种 CONT_CUR_TYPE
        accLoan.setLoanBalance(iqpLoanApp.getAppAmt());//贷款余额LOAN_BAL
        accLoan.setLoanStartDate(iqpLoanApp.getAppDate());//贷款起始日期
        accLoan.setRulingIr(iqpLoanApp.getRulingIr());//基准利率
        accLoan.setLoanAmt(iqpLoanApp.getAppAmt());//贷款金额
        /*设置 日期add 参数  1 年, 2 月, 5 日 */
        int realTermType = realTypeGet(iqpLoanApp.getTermType());
        BigDecimal appTerm = iqpLoanApp.getAppTerm();//申请期限
        Date endDate = DateUtils.add(DateUtils.parseDate(iqpLoanApp.getAppDate(), "yyyy-MM-dd"), realTermType, appTerm.intValue());//结束日期=申请日期+申请期限*申请类型
        /**
         * zhanyb修改20210427。以下为原内容
         accLoan.setEndDate(DateUtils.formatDate(endDate, "yyyy-MM-dd"));//结束日期
         accLoan.setChnlSour(iqpLoanAppAssist.getChnlSour());//渠道来源
         accLoan.setAcctBrId(iqpLoanApp.getInputId());//放款机构
         */
        accLoan.setLoanEndDate(DateUtils.formatDate(endDate, "yyyy-MM-dd"));//贷款到期日
        //accLoan.setChnlSour(iqpLoanAppAssist.getChnlSour());//渠道来源
        accLoan.setDisbOrgNo(iqpLoanApp.getInputId());//放款机构编号
        accLoan.setManagerId(iqpLoanApp.getInputId());//主办人
        accLoan.setManagerBrId(iqpLoanApp.getManagerBrId());//主办机构
        accLoan.setInputId(iqpLoanApp.getInputId());//登记人
        accLoan.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
        accLoan.setInputDate(DateUtils.formatDate(new Date(), "yyyy-MM-dd"));//登记日期
        accLoan.setAccStatus(CmisBizConstants.IQP_ACC_STATUS_1);//台账状态 1 正常
        return accLoan;
    }


    /**
     * 获取时间入参 1 年 2 月 3 日
     *
     * @param termType
     * @return
     */
    private int realTypeGet(String termType) {
        int realTermType;
        if (CmisBizConstants.LMT_TERM_TYPE_001.equals(termType)) {
            return realTermType = 1;
        } else if (CmisBizConstants.LMT_TERM_TYPE_002.equals(termType)) {
            return realTermType = 2;
        } else if (CmisBizConstants.LMT_TERM_TYPE_003.equals(termType)) {
            return realTermType = 5;
        } else {
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 审批通过生成合同信息-处理业务申请子表信息
     *
     * @param iqpSerno
     * @param contNo
     * @return
     */
    public void updateContNoIntoIqpSublist(String iqpSerno, String contNo) {
        String subList = "";//子表名称
        try {
            Map updateMap = new HashMap();
            updateMap.put("iqpSerno", iqpSerno);
            updateMap.put("contNo", contNo);

            log.info("审批通过-业务申请" + iqpSerno + "处理【保证金】信息");
            subList = CmisBizConstants.IQP_SUBLIST_COM_BAIL;
            int updateCount = comBailService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过-业务申请" + iqpSerno + "处理【账户】信息");
            subList = CmisBizConstants.IQP_SUBLIST_IQP_ACCT;
            updateCount = iqpAcctService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过-业务申请" + iqpSerno + "处理【业务申请人关系】信息");
            subList = CmisBizConstants.IQP_SUBLIST_IQP_AAR;
            updateCount = iqpApplApptRelService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过-业务申请" + iqpSerno + "处理【业务与借据关系】信息");
            subList = CmisBizConstants.IQP_SUBLIST_BILL_REL;
            updateCount = iqpBillRelService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过-业务申请" + iqpSerno + "处理【车产】信息");
            subList = CmisBizConstants.IQP_SUBLIST_IQP_CAR;
            updateCount = iqpCarService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过-业务申请" + iqpSerno + "处理【委托贷款】信息");
            //一期暂时

            log.info("审批通过-业务申请" + iqpSerno + "处理【担保合同与业务关系】信息");
            subList = CmisBizConstants.IQP_SUBLIST_IQP_GBRA;
            updateCount = iqpGuarBizRelAppService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            log.info("审批通过息-业务申请" + iqpSerno + "处理【房产】信息");
            subList = CmisBizConstants.IQP_SUBLIST_IQP_HOUSE;
            updateCount = iqpHouseService.updateByParams(updateMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
            }

            int insertCount = 0;
            log.info("审批通过-业务申请" + iqpSerno + "生成【担保和业务关系结果】信息");
            subList = CmisBizConstants.IQP_SUBLIST_GRT_GUAR_BIZ;
            log.info("审批通过-业务申请" + iqpSerno + "查询担保与业务关系数据");
            Map guarMap = new HashMap();
            guarMap.put("iqpSerno", iqpSerno);
            guarMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectIGBRAByParams(guarMap);

            //调用工具类
            BizUtils bizUtils = new BizUtils();
            //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
            //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
            if (CollectionUtils.nonEmpty(iqpGuarBizRelAppList)) {
                for (IqpGuarBizRelApp iqpGuarBizRelApp : iqpGuarBizRelAppList) {
                    log.info("审批通过-业务申请" + iqpSerno + "转化担保合同" + iqpGuarBizRelApp.getGuarContNo() + "数据为结果表数据");
                    GrtGuarBizRstRel grtGuarBizRstRel = new GrtGuarBizRstRel();
                    //调用转化的方法
                    grtGuarBizRstRel = (GrtGuarBizRstRel) bizUtils.getMappingValueBySourceAndDisTable(iqpGuarBizRelApp, grtGuarBizRstRel);
                    grtGuarBizRstRel.setPkId(StringUtils.uuid(true));
                    log.info("审批通过-业务申请" + iqpSerno + "转化担保合同" + iqpGuarBizRelApp.getGuarContNo() + "数据为结果表数据-落地");
                    insertCount = grtGuarBizRstRelService.insertSelective(grtGuarBizRstRel);
                    if (insertCount < 0) {
                        throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
                    }
                }
            }


            log.info("审批通过-业务申请" + iqpSerno + "生成【授信与合同关联(包括授信与第三方额度)】信息");
            subList = CmisBizConstants.IQP_SUBLIST_CTR_LMT_REL;
            log.info("审批通过-业务申请" + iqpSerno + "查询业务与授信关系数据");
            List<IqpLmtRel> iqpLmtRelList = iqpLmtRelService.selectByIqpSerno(iqpSerno);
            if (CollectionUtils.nonEmpty(iqpLmtRelList)) {
                for (IqpLmtRel iqpLmtRel : iqpLmtRelList) {
                    log.info("审批通过-业务申请" + iqpSerno + "转化授信编号为" + iqpLmtRel.getLmtCtrNo() + "数据为合同与授信关系数据");
                    //数据映射通过cfg接口获取对应的映射，入参源表名以及目标表名以及操作标识
                    //返回两种场景：1、直接返回sql，调用执行的sql；2、返回映射关系，通过映射关系生成目标实体，调用目标实体的mapper执行sql
                    CtrLmtRel ctrLmtRel = new CtrLmtRel();
                    ctrLmtRel = (CtrLmtRel) bizUtils.getMappingValueBySourceAndDisTable(iqpLmtRel, ctrLmtRel);
                    ctrLmtRel.setPkId(StringUtils.uuid(true));
                    //因为业务与授信关系表中无合同编号字段，无法进行映射操作，因此直接设值
                    ctrLmtRel.setContNo(contNo);
                    log.info("审批通过-业务申请" + iqpSerno + "转化授信编号为" + iqpLmtRel.getLmtCtrNo() + "数据为合同与授信关系数据-落地");
                    insertCount = ctrLmtRelService.insertSelective(ctrLmtRel);
                    if (insertCount < 0) {
                        throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
                    }
                }
            }

        } catch (YuspException e) {
            log.error("处理业务子表" + subList + "出现异常！", e);
            throw e;
        } catch (Exception e) {
            log.error("处理业务子表" + subList + "出现异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.key, EcbEnum.E_IQP_HANDLESUBLIST_EXCEPTION.value + subList);
        }
    }

    /**
     * 业务申请流程拒绝逻辑处理
     * 0、判断业务类型，若是单笔单批，更新业务审批模式表中的全流程状态为审批拒绝
     * 1、更新业务与担保合同关系表中的状态为【解除】
     * 2、更新业务申请主表的审批状态为审批拒绝-998
     *
     * @param iqpSerno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterRefuse(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("业务申请审批拒绝-获取业务申请" + iqpSerno + "申请主表信息");
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            //定义更新标志位
            int updateCount = 0;
            if (CmisBizConstants.BIZ_TYPE_SIGNLE.equals(iqpLoanApp.getBizType())) {
                log.info("业务申请审批拒绝-业务申请" + iqpSerno + "对应业务类型为：单笔单批业务。更新审批模式表【全流程状态】为【审批拒绝】");
                updateCount = iqpLoanAppApprModeService.updateWfStatusByParams(iqpSerno, CmisBizConstants.WF_STATUS_03);
                if (updateCount < 0) {
                    throw new YuspException(EcbEnum.E_IQP_UPDWFSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDWFSTATUS_EXCEPTION.value);
                }
            }

            Map guarBizRelMap = new HashMap();
            guarBizRelMap.put("iqpSerno", iqpLoanApp.getIqpSerno());
            guarBizRelMap.put("correRel", CmisBizConstants.CORRE_REL_3);//关系类型更新为【解除】
            updateCount = iqpGuarBizRelAppService.updateByParams(guarBizRelMap);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDWFSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDWFSTATUS_EXCEPTION.value);
            }

            log.info("业务申请审批拒绝-更新业务申请" + iqpSerno + "流程审批状态为【998】-拒绝");
            updateCount = iqpLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_998);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 业务申请流程退回 打回逻辑处理
     * 1、更新业务申请主表的审批状态为审批打回-992
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfteCallBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("业务申请审批-获取业务申请" + iqpSerno + "申请主表信息");
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            //定义更新标志位
            int updateCount = 0;
            log.info("业务申请审批退回 打回-更新业务申请" + iqpSerno + "流程审批状态为【992】-打回");
            updateCount = iqpLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_992);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 业务申请流程拿回 追回逻辑处理
     * 1、更新业务申请主表的审批状态为审批追回-991
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfteTackBack(String iqpSerno) {
        try {
            if (StringUtils.isBlank(iqpSerno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }

            log.info("业务申请审批-获取业务申请" + iqpSerno + "申请主表信息");
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
            if (iqpLoanApp == null) {
                throw new YuspException(EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.key, EcbEnum.E_IQP_IQPNOTEIXSTS_EXCEPTION.value);
            }
            //定义更新标志位
            int updateCount = 0;
            log.info("业务申请审批拿回 追回-更新业务申请" + iqpSerno + "流程审批状态为【991】-追回");
            updateCount = iqpLoanAppMapper.updateApproveStatus(iqpSerno, CmisCommonConstants.WF_STATUS_991);
            if (updateCount < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程审批通过业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key, EcbEnum.E_IQP_HANDLE_EXCEPTION.value);
        }
    }


    /**
     * 校验业务申请引入的借据总金额是否能够本次申请金额
     *
     * @param iqpSerno
     * @param appAmt
     * @return
     */
    private Map checkBillData(String iqpSerno, BigDecimal appAmt) {
        Map checkResult = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";

        try {
            log.info("获取业务申请" + iqpSerno + "借据关系信息");
            Map queryMap = new HashMap();
            queryMap.put("iqpSerno", iqpSerno);
            queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpBillRel> iqpBillRelList = iqpBillRelService.selectByParams(queryMap);
            if (CollectionUtils.isEmpty(iqpBillRelList) || iqpBillRelList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_BILLLISTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_BILLLISTNULL_EXCEPTION.value;
                return checkResult;
            }
            log.info("获取业务申请" + iqpSerno + "借据数据");
            String billNos = "";
            for (IqpBillRel iqpBillRel : iqpBillRelList) {
                billNos += iqpBillRel.getBillNo() + ",";
            }

            queryMap.clear();
            queryMap.put("billNos", billNos);
            queryMap.put("wfEndExistsFlag", "Y");
            List<AccLoan> accLoanList = accLoanService.selectAccLoanByParams(queryMap);
            if (CollectionUtils.isEmpty(accLoanList) || accLoanList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_BILLLISTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_BILLLISTNULL_EXCEPTION.value;
                return checkResult;
            }
            log.info("获取业务申请" + iqpSerno + "借据总余额");
            BigDecimal balanceTotal = new BigDecimal("0");
            for (AccLoan accLoan : accLoanList) {
                //balanceTotal.add(accLoan.getLoanBalance());
                balanceTotal.add(accLoan.getLoanBalance());
            }
            log.info("获取业务申请" + iqpSerno + "比较借据总余额与申请");
            if (appAmt.compareTo(balanceTotal) > 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_BILLAMTSMALL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_BILLAMTSMALL_EXCEPTION.value;
                return checkResult;
            }
        } catch (Exception e) {
            log.error("校验业务申请" + iqpSerno + "借据数据异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            checkResult.put("rtnCode", rtnCode);
            checkResult.put("rtnMsg", rtnMsg);
        }

        return checkResult;
    }

    /**
     * 校验账户信息
     *
     * @param iqpSerno   业务流水号
     * @param defrayMode 支付方式
     * @return
     */
    private Map checkIqpAcctInfo(String iqpSerno, String defrayMode) {
        Map checkResult = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            //查询账户列表数据
            log.info("获取业务申请" + iqpSerno + "账户信息");
            List<IqpAcct> iqpAcctList = iqpAcctService.selectAffectInfoByIqpSerno(iqpSerno);
            if (CollectionUtils.isEmpty(iqpAcctList) || iqpAcctList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_IQPACCTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_IQPACCTNULL_EXCEPTION.value;
                return checkResult;
            }

            log.info("获取业务申请" + iqpSerno + "处理账户信息");
            int pvpCount = 0;//放款账户数
            int repayCount = 0;//还款账户数
            int epCount = 0;//受托支付账户数
            for (IqpAcct iqpAcct : iqpAcctList) {
                if (CmisBizConstants.ID_ATTR_PVP.equals(iqpAcct.getAcctAttr())) {
                    pvpCount += 1;
                } else if (CmisBizConstants.ID_ATTR_EP.equals(iqpAcct.getAcctAttr())) {
                    epCount += 1;
                } else if (CmisBizConstants.ID_ATTR_REPAY.equals(iqpAcct.getAcctAttr())) {
                    repayCount += 1;
                }
            }
            //校验放款账户
            if (pvpCount != 1) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_IQPPVPACCTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_IQPPVPACCTNULL_EXCEPTION.value;
                return checkResult;
            }
            //校验还款账户
            if (repayCount == 0 || repayCount > 3) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_IQPREPAYACCTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_IQPREPAYACCTNULL_EXCEPTION.value;
                return checkResult;
            }
            //校验受托支付账户
            if (StringUtils.nonBlank(defrayMode) && CmisBizConstants.PAY_WAY_ENTRUSTEDPAYMENT.equals(defrayMode)
                    && epCount == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_IQPEPACCTNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_IQPEPACCTNULL_EXCEPTION.value;
                return checkResult;
            }
        } catch (Exception e) {
            log.error("校验业务申请" + iqpSerno + "账户信息异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            checkResult.put("rtnCode", rtnCode);
            checkResult.put("rtnMsg", rtnMsg);
        }
        return checkResult;
    }

    /**
     * 校验保证金列表数据并校验保证金列表总金额是否能够覆盖保证金金额
     *
     * @param iqpSerno
     * @param securityAmt
     * @return
     */
    private Map checkComBailInfo(String iqpSerno, BigDecimal securityAmt) {
        Map checkResult = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            log.info("获取业务申请" + iqpSerno + "保证金列表数据");
            List<ComBail> comBailList = comBailService.selectByIqpSerno(iqpSerno);

            if (CollectionUtils.isEmpty(comBailList) || comBailList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_COMBAILNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_COMBAILNULL_EXCEPTION.value;
                return checkResult;
            }

            log.info("获取业务申请" + iqpSerno + "保证金列表累加金额");
            BigDecimal comBailTotalAmt = new BigDecimal("0");
            for (ComBail comBail : comBailList) {
                comBailTotalAmt = comBailTotalAmt.add(comBail.getAcctAmtBalance());
            }

            log.info("获取业务申请" + iqpSerno + "保证金列表累加金额比较保证金金额");
            if (comBailTotalAmt.compareTo(securityAmt) < 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_COMBAILSMALL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_COMBAILSMALL_EXCEPTION.value;
                return checkResult;
            }
        } catch (Exception e) {
            log.error("校验业务申请" + iqpSerno + "保证金列表数据异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            checkResult.put("rtnCode", rtnCode);
            checkResult.put("rtnMsg", rtnMsg);
        }
        return checkResult;
    }

    /* *//**
     * 校验客户是否不宜贷款客户
     * @param cusId 客户编号
     * @return
     *//*
    public Map checkCusPubBlacklistRsClientDto(String cusId) {
        Map checkResult = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        boolean checkBlackCusFlag = false;
        try{
            if(StringUtils.isBlank(cusId)){
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_PARAMS_EXCEPTION.value+"未获取到客户编号！";
                return checkResult;
            }

            log.info("校验客户"+cusId+"是否不良贷款客户-调用接口！");
            QueryCusPubBlacklistRsDto queryCusPubBlacklistRsDto = new QueryCusPubBlacklistRsDto();
            queryCusPubBlacklistRsDto.setCusId(cusId);
            try{
                queryCusPubBlacklistRsDto = iCusClientService.queryCusPubBlacklistRs(queryCusPubBlacklistRsDto);
            }catch(Exception e){
                log.error("校验客户"+cusId+"是否不良贷款客户-调用接口异常！",e);
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCALLNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCALLNULL_EXCEPTION.value+e.getMessage();
                return checkResult;
            }

            if(queryCusPubBlacklistRsDto==null){
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCALLNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCALLNULL_EXCEPTION.value;
                return checkResult;
            }

            rtnCode = queryCusPubBlacklistRsDto.getRtnCode();
            if(!EcbEnum.IQP_SUCCESS_DEF.key.equals(rtnCode)){
                rtnMsg = queryCusPubBlacklistRsDto.getRtnMsg();
                return checkResult;
            }

            CusPubBlacklistRsClientDto cusPubBlacklistRsClientDto = queryCusPubBlacklistRsDto.getCusPubBlacklistRsClientDto();
            //校验客户是否为不宜贷款户时，若是查询返回的结果为空，则该校验返回应当也是成功
            if(cusPubBlacklistRsClientDto==null){
                rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
                rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
            }else{
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCHECKTRUE.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_CUSBLACKCHECKTRUE.value;
                checkBlackCusFlag = true;
            }
        }catch(Exception e){
            log.error("校验客户是否不宜贷款户异常！",e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value+e.getMessage();
        }finally {
            checkResult.put("rtnCode",rtnCode);
            checkResult.put("rtnMsg",rtnMsg);
            checkResult.put("checkBlackCusFlag",checkBlackCusFlag);
        }

        return checkResult;
    }*/

    /**
     * 校验本次申请所引入担保合同的本次担保金额总值与申请金额的大小
     * 20210308  主担保方式若是不为信用，则必须存在一条对应担保类型的担保合同数据
     *
     * @param iqpSerno 业务申请流水号，用于获取业务与担保合同关系数据
     * @param appAmt   申请金额
     * @param guarWay  主担保方式
     * @return
     */
    private Map checkThisTotalGuarAmtAndAppAmt(String iqpSerno, BigDecimal appAmt, String guarWay) {
        Map checkResult = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {

            log.info("校验业务申请" + iqpSerno + "担保合同总金额及申请金额大小-担保合同关系数据");
            Map queryMap = new HashMap();
            queryMap.put("iqpSerno", iqpSerno);
            queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpGuarBizRelApp> iqpGuarBizRelAppList = iqpGuarBizRelAppService.selectIGBRAByParams(queryMap);

            if (CollectionUtils.isEmpty(iqpGuarBizRelAppList) || iqpGuarBizRelAppList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_GUARRELNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_GUARRELNULL_EXCEPTION.value;
                return checkResult;
            }
            log.info("校验业务申请" + iqpSerno + "担保合同总金额及申请金额大小-获取本次担保总金额");
            BigDecimal totalThisGuarAmt = BigDecimal.ZERO;
            String guarNos = "";
            for (int i = 0; i < iqpGuarBizRelAppList.size(); i++) {
                IqpGuarBizRelApp igba = iqpGuarBizRelAppList.get(i);
                if (igba.getThisGuarAmt() != null) {
                    totalThisGuarAmt = totalThisGuarAmt.add(igba.getThisGuarAmt());
                }
                guarNos += igba.getGuarContNo() + ",";
            }
            log.info("校验业务申请" + iqpSerno + "是否存在对应主担保方式的担保合同的数据");
            queryMap.clear();
            queryMap.put("guarWay", guarWay);
            queryMap.put("guarContNos", guarNos);
            List<GrtGuarCont> grtGuarContList = grtGuarContService.queryByParams(queryMap);
            if (CollectionUtils.isEmpty(grtGuarContList) || grtGuarContList.size() == 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_GUARWAY_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_GUARWAY_EXCEPTION.value;
                return checkResult;
            }

            log.info("校验业务申请" + iqpSerno + "担保合同总金额及申请金额大小-关系数据比较");
            if (totalThisGuarAmt.compareTo(appAmt) < 0) {
                rtnCode = EcbEnum.E_IQP_BIZDATACHECK_GUARAMTSMALL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_BIZDATACHECK_GUARAMTSMALL_EXCEPTION.value;
                return checkResult;
            }
        } catch (Exception e) {
            log.error("校验业务申请" + iqpSerno + "担保合同总金额及申请金额大小异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            checkResult.put("rtnCode", rtnCode);
            checkResult.put("rtnMsg", rtnMsg);
        }
        return checkResult;
    }

    /**
     * 查看拒绝记录
     *
     * @param cusId
     * @return
     */
    public int getCusRejectByCusId(String cusId) {
        return iqpLoanAppMapper.getCusRejectByCusId(cusId);
    }

    /**
     * 根据协议编号查询所有下属合同的业务申请流水号
     *
     * @param lmtCtrNo
     * @return
     */
    public List<String> getIqpSernoNumbers(String lmtCtrNo) {
        return this.iqpLoanAppMapper.getIqpSernoNumbers(lmtCtrNo);
    }

    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     *
     * @param lmtCtrNo
     * @return
     */
    public Integer getZaiTuAppCount(String lmtCtrNo) {
        return this.iqpLoanAppMapper.getZaiTuAppCount(lmtCtrNo);
    }

    /**
     * 根据个人授信额度分项编号 查询在途的业务申请（特殊业务、额度项下业务）申请流水号
     *
     * @param map
     * @return
     */
    public List<String> getIqpsernoByLmtCtrNo(Map map) {
        return iqpLoanAppMapper.getIqpsernoByLmtCtrNo((String) map.get("lmtLimitNos"));
    }


    /**
     * @return 根据传入流水号来查询是否有授信编号相同的数据
     * @创建人 WH
     * @创建时间 19:44 2021-04-13
     **/
    public IqpLoanApp selectBySurveyNo(String surveyNo) {
        return iqpLoanAppMapper.selectBySurveyNo(surveyNo);
    }

    /**
     * 单笔单批提交前校验
     *
     * @param iqpSerno
     * @return
     */
    public String checkBeforeCommit(String iqpSerno) {
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
        String isCfirmPayWay = iqpLoanApp.getIsCfirmPayWay();
        if (!CmisBizConstants.IS_CFIRM_PAY_N.equals(isCfirmPayWay)) {//已经确认 支付信息
            String defrayMode = iqpLoanApp.getPayMode();//支付方式
            //查询 账户信息
            List<IqpAcct> iqpAccts = iqpAcctService.selectAffectInfoByIqpSerno(iqpSerno);
            if (CollectionUtils.isEmpty(iqpAccts)) {
                if (CmisBizConstants.PAY_WAY_INDEPENTMENTPAYMENT.equals(defrayMode)) {//自主支付
                    throw new YuspException(EcbEnum.E_IQP_ACCT_ERROR1.key,
                            EcbEnum.E_IQP_ACCT_ERROR1.value);
                } else if (CmisBizConstants.PAY_WAY_ENTRUSTEDPAYMENT.equals(defrayMode)) {//受托支付
                    throw new YuspException(EcbEnum.E_IQP_ACCT_ERROR1.key,
                            EcbEnum.E_IQP_ACCT_ERROR2.value);
                }
            } else {
                Map<String, Long> collectMap = iqpAccts.stream().collect(Collectors.groupingBy(IqpAcct::getAcctAttr, Collectors.counting()));
                Long indepentNum = collectMap.get("1") == null ? 0L : collectMap.get("1");//自主支付 账号
                Long entrustedNum = collectMap.get("4") == null ? 0L : collectMap.get("4");//受托支付 账号
                if (CmisBizConstants.PAY_WAY_INDEPENTMENTPAYMENT.equals(defrayMode) && indepentNum <= 0L) {//自主支付
                    throw new YuspException(EcbEnum.E_IQP_ACCT_ERROR1.key,
                            EcbEnum.E_IQP_ACCT_ERROR1.value);
                } else if (CmisBizConstants.PAY_WAY_ENTRUSTEDPAYMENT.equals(defrayMode) && entrustedNum <= 0L) {//受托支付
                    throw new YuspException(EcbEnum.E_IQP_ACCT_ERROR1.key,
                            EcbEnum.E_IQP_ACCT_ERROR2.value);
                }
            }
        }
        return "ok";
    }

    /**
     * 贸易融资合同申请新增页面点击下一步
     *
     * @param iqpTcontApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpTcontAppInfo(IqpLoanApp iqpTcontApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpTcontApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpTcontApp.getCusId();


            log.info("客户" + cusId + "新增个人额度申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpTcontApp.setInputId(userInfo.getLoginCode());
                iqpTcontApp.setInputBrId(userInfo.getOrg().getCode());
                iqpTcontApp.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }
            // 调用 对公客户基本信息查询接口，
            // 将 地址与联系方式 信息更新值贷款申请表中
            log.info("通过客户编号：【{}】，查询客户信息开始",iqpTcontApp.getCusId());
            CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(iqpTcontApp.getCusId());
            log.info("通过客户编号：【{}】，查询客户信息结束，响应报文为：【{}】",iqpTcontApp.getCusId(), cusBaseDtoResultDto.toString());
            // 个人客户
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBaseDtoResultDto.getCusCatalog())){
                CusIndivContactDto CusIndivAllDto = icusClientService.queryCusIndivByCusId(cusId);
                if (CusIndivAllDto != null && !"".equals(CusIndivAllDto.getCusId()) && CusIndivAllDto.getCusId() != null) {
                    iqpTcontApp.setPhone(CusIndivAllDto.getMobile());
                    iqpTcontApp.setFax(CusIndivAllDto.getFaxCode());
                    iqpTcontApp.setEmail(CusIndivAllDto.getEmail());
                    iqpTcontApp.setQq(CusIndivAllDto.getQq());
                    iqpTcontApp.setWechat(CusIndivAllDto.getWechatNo());
                    iqpTcontApp.setDeliveryAddr(CusIndivAllDto.getDeliveryStreet());
                }
            }else{
                CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(iqpTcontApp.getCusId()).getData();
                if (cusCorpDto != null && !"".equals(cusCorpDto.getCusId()) && cusCorpDto.getCusId() != null) {
                    iqpTcontApp.setLinkman(cusCorpDto.getFreqLinkman());
                    iqpTcontApp.setPhone(cusCorpDto.getFreqLinkmanTel());
                    iqpTcontApp.setFax(cusCorpDto.getFax());
                    iqpTcontApp.setEmail(cusCorpDto.getLinkmanEmail());
                    iqpTcontApp.setQq(cusCorpDto.getQq());
                    iqpTcontApp.setWechat(cusCorpDto.getWechatNo());
                    iqpTcontApp.setDeliveryAddr(cusCorpDto.getSendAddr());
                }
            }
            Map seqMap = new HashMap();
            String iqpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            String serno = iqpSerno;
            //生成合同编号
            HashMap param = new HashMap();
            String dkSeq = iqpHighAmtAgrAppService.getSuitableContNo(userInfo.getOrg().getCode(),CmisCommonConstants.STD_BUSI_TYPE_04);
            String contNo = sequenceTemplateClient.getSequenceTemplate(dkSeq, param);

            iqpTcontApp.setIqpSerno(iqpSerno);
            iqpTcontApp.setContNo(contNo);
            iqpTcontApp.setTermType("M");
            iqpTcontApp.setBelgLine(CmisCommonConstants.STD_BELG_LINE_03);
            iqpTcontApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpTcontApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpTcontApp.setBizType(CmisCommonConstants.STD_BUSI_TYPE_04);
            // 通过授信额度编号查询批复流水号
            if(StringUtils.nonBlank(iqpTcontApp.getLmtAccNo())){
                LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(iqpTcontApp.getLmtAccNo());
                if(Objects.isNull(lmtReplyAccSubPrd)){
                    log.error("通过产品额度编号【{}】未查询到产品额度信息",iqpTcontApp.getLmtAccNo());
                    throw BizException.error(null, EcbEnum.ECB020068.key, EcbEnum.ECB020068.value);
                }
                log.info("分项额度编号为【{}】",lmtReplyAccSubPrd.getAccSubNo());
                Map map = new HashMap();
                map.put("accSubNo",lmtReplyAccSubPrd.getAccSubNo());
                LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
                if(StringUtils.isBlank(lmtReplyAccSub.getReplySerno())){
                    log.error("通过分项额度编号【{}】未查询到分项额度信息",lmtReplyAccSubPrd.getAccSubNo());
                    throw BizException.error(null, EcbEnum.ECB020069.key, EcbEnum.ECB020069.value);
                }
                log.info("批复流水号为【{}】",lmtReplyAccSub.getReplySerno());
                iqpTcontApp.setReplyNo(lmtReplyAccSub.getReplySerno());
            }
            int insertCount = iqpLoanAppMapper.insertSelective(iqpTcontApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno", serno);
            result.put("contNo", contNo);
            log.info("贸易融资合同申请" + serno + "-保存成功！");
            //根据选择的额度分项自动生成担保合同
            String subSerno = "";
            IqpLoanApp iqpLoanAppData = iqpLoanAppMapper.selectBySerno(serno);
            if (!CmisCommonConstants.GUAR_MODE_00.equals(iqpLoanAppData.getGuarWay()) && !CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanAppData.getGuarWay()) &&
                    !CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanAppData.getGuarWay()) && !CmisCommonConstants.GUAR_MODE_60.equals(iqpLoanAppData.getGuarWay())) {
                if(iqpLoanAppData.getLmtAccNo()==null||"".equals(iqpLoanAppData.getLmtAccNo())){
                    subSerno = "";
                }else{
                    subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(iqpLoanAppData.getLmtAccNo());//GUAR_BIZ_REL
                }
                GrtGuarContDto grtGuarContDto = new GrtGuarContDto();
                grtGuarContDto.setSerno(iqpLoanAppData.getIqpSerno());//业务流水号
                grtGuarContDto.setCusId(iqpLoanAppData.getCusId());//借款人编号
                grtGuarContDto.setBizLine(iqpLoanAppData.getBelgLine());//业务条线
                grtGuarContDto.setGuarWay(iqpLoanAppData.getGuarWay());//担保方式   //10抵押 20 质押 30保证
                grtGuarContDto.setIsUnderLmt(CmisCommonConstants.STD_ZB_YES_NO_1);
                grtGuarContDto.setGuarAmt(iqpLoanAppData.getContAmt());//担保金额
                grtGuarContDto.setGuarTerm(iqpLoanAppData.getContTerm());//担保期限
                grtGuarContDto.setGuarStartDate(iqpLoanAppData.getStartDate());//担保起始日
                grtGuarContDto.setGuarEndDate(iqpLoanAppData.getEndDate());//担保终止日
                grtGuarContDto.setReplyNo(iqpLoanAppData.getReplyNo());//批复编号
                grtGuarContDto.setLmtAccNo(iqpLoanAppData.getLmtAccNo());//授信额度编号
                grtGuarContDto.setSubSerno(subSerno);//授信分项流水号
                grtGuarContDto.setCusName(iqpLoanAppData.getCusName());//借款人名称
                grtGuarContDto.setGuarContType(iqpLoanAppData.getContType());//担保合同类型
                grtGuarContDto.setInputId(iqpLoanAppData.getInputId());//登记人
                grtGuarContDto.setInputBrId(iqpLoanAppData.getInputBrId());//登记机构
                grtGuarContDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
                grtGuarContService.lmtAutoCreateGrtGuarCont(grtGuarContDto, contNo);
            }

            // 新增用信条件落实情况数据
            Map queryMap = new HashMap();
            queryMap.put("replySerno",iqpLoanAppData.getReplyNo());
            queryMap.put("contNo",iqpLoanAppData.getContNo());
            int insertCountData = imgCondDetailsService.generateImgCondDetailsData(queryMap);
//            if(insertCountData<=0){
//                throw BizException.error(null, EcbEnum.ECB020029.key, EcbEnum.ECB020029.value);
//            }
            // TODO 添加征信信息
            CreditReportQryLstAndRealDto creditReportQryLstAndRealDto = new CreditReportQryLstAndRealDto();
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
            creditReportQryLstAndRealDto.setCusId(cusId);
            creditReportQryLstAndRealDto.setCusName(iqpTcontApp.getCusName());
            creditReportQryLstAndRealDto.setCertCode(cusBaseClientDto.getCertCode());
            creditReportQryLstAndRealDto.setCertType(cusBaseClientDto.getCertType());
            creditReportQryLstAndRealDto.setBorrowRel("001");
            String qryCls = "1".equals(cusBaseClientDto.getCusCatalog()) ? "0" : "1";
            creditReportQryLstAndRealDto.setQryCls(qryCls);
            creditReportQryLstAndRealDto.setApproveStatus("000");
            creditReportQryLstAndRealDto.setIsSuccssInit("0");
            creditReportQryLstAndRealDto.setQryFlag("02");
            creditReportQryLstAndRealDto.setQryStatus("001");
            //生成关联征信数据
            creditReportQryLstAndRealDto.setBizSerno(serno);
            creditReportQryLstAndRealDto.setScene("02");
            ResultDto<Integer> doCreateCreditAuto = cmisBizClientService.createCreditAuto(creditReportQryLstAndRealDto);
            if (!doCreateCreditAuto.getCode().equals("0")) {
                log.info("业务流水号：{}，生成征信关联数据异常", creditReportQryLstAndRealDto.getBizSerno());
            }
        } catch (YuspException e) {
            log.error("贸易融资合同申请新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存贸易融资合同申请异常！", e.getMessage());
            throw new YuspException(EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 贸易融资合同申请申请提交保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpTcontAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String iqpSerno = "";
        try {
            //获取申请流水号
            iqpSerno = (String) params.get("iqpSerno");
            if (StringUtils.isBlank(iqpSerno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "贸易融资合同申请" + iqpSerno;

            log.info(logPrefix + "获取申请数据");
            IqpLoanApp iqpTcontApp = JSONObject.parseObject(JSON.toJSONString(params), IqpLoanApp.class);
            if (iqpTcontApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存贸易融资合同申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpTcontApp.setUpdId(userInfo.getLoginCode());
                iqpTcontApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpTcontApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存贸易融资合同申请数据");
            int updCount = iqpLoanAppMapper.updateByPrimaryKeySelective(iqpTcontApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存贸易融资合同申请" + iqpSerno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @param
     * @return HashMap<String, String>
     * @author shenli
     * @date 2021/4/20 0020 10:16
     * @version 1.0.0
     * @desc 发接口去核心查询当前客户账号，若客户账号不存在或状态异常，系统自动情况“放款账号”值并给出明确提示；若存在正常状态的账号，则回显放款账号名称值。
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public HashMap<String, String> getLoanPayoutAccName(String loan_payout_acc_no) {

        log.info("接口去核心查询当前客户账号。。。。。");

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("status", "000");
        map.put("message", "查询成功");
        map.put("loanpayoutaccname", "还款账户0001");
        return map;
    }

    /**
     * 福费廷合同申请新增页面点击下一步
     *
     * @param iqpForftinApp
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveIqpForftinAppInfo(IqpLoanApp iqpForftinApp) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (iqpForftinApp == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String cusId = iqpForftinApp.getCusId();

            log.info("客户" + cusId + "新增个人额度申请-获取当前登录用户信息");
//            User userInfo = SessionUtils.getUserInformation();
//            if(userInfo==null){
//                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
//                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
//                return result;
//            }else{
//                iqpForftinApp.setInputId(userInfo.getLoginCode());
//                iqpForftinApp.setInputBrId(userInfo.getOrg().getCode());
//                iqpForftinApp.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
//            }iqploanapp

            Map seqMap = new HashMap();

            //String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, seqMap);

            String iqp_serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);

            //iqpForftinApp.setSerno(serno);
            iqpForftinApp.setIqpSerno(iqp_serno);
            iqpForftinApp.setApproveStatus("000");
            int insertCount = iqpLoanAppMapper.insertSelective(iqpForftinApp);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno", iqp_serno);
            log.info("福费廷合同申请" + iqp_serno + "-保存成功！");
        } catch (YuspException e) {
            log.error("福费廷合同申请新增保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存福费廷合同申请异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 福费廷合同申请申请提交保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveIqpForftinAppInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "福费廷合同申请" + serno;

            log.info(logPrefix + "获取申请数据");
            IqpLoanApp iqpForftinApp = JSONObject.parseObject(JSON.toJSONString(params), IqpLoanApp.class);
            if (iqpForftinApp == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存福费廷合同申请-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpForftinApp.setUpdId(userInfo.getLoginCode());
                iqpForftinApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpForftinApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "保存福费廷合同申请数据");
            int updCount = iqpLoanAppMapper.updateByPrimaryKeySelective(iqpForftinApp);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存福费廷合同申请" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**

     * @方法名称: forftinLogicDelete
     * @方法描述: 根据主键做逻辑删除（福费廷合同申请）
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map forftinLogicDelete(IqpLoanApp iqpLoanApp) {
        iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            String iqpSerno = iqpLoanApp.getIqpSerno();
            log.info(String.format("根据主键%s对福费廷申请明细信息进行逻辑删除", iqpSerno));
            iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);

            log.info(String.format("根据主键%s对福费廷申请明细信息进行逻辑删除,获取用户登录信息", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                throw new YuspException(EcbEnum.E_GETUSER_EXCEPTION.key, EcbEnum.E_GETUSER_EXCEPTION.value);
            } else {
                iqpLoanApp.setUpdId(userInfo.getLoginCode());
                iqpLoanApp.setUpdBrId(userInfo.getOrg().getCode());
                iqpLoanApp.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            log.info(String.format("根据主键%s对福费廷申请信息进行逻辑删除,信息进行逻辑删除", iqpSerno));
            //iqpEntrustLoanAppMapper.updateByPrimaryKeySelective(iqpEntrustLoanApp);
            if(CmisCommonConstants.WF_STATUS_000.equals(iqpLoanApp.getApproveStatus())){
                iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);

            }else if(CmisCommonConstants.WF_STATUS_992.equals(iqpLoanApp.getApproveStatus())){
                iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
                iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);
                ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpSerno);
                if (iqpLoanApp.getLmtAccNo() != null && !"".equals(iqpLoanApp.getLmtAccNo())) {
                    //打回改为自行退出，恢复占额
                    String guarMode = iqpLoanApp.getGuarWay();
                    //恢复敞口
                    BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                    //恢复总额
                    BigDecimal recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                    CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                    cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getContNo());//合同编号
                    cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                    cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                    cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                    cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                    cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                    cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                    String code = resultDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.error("业务申请恢复额度异常！");
                        throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                    }
                }
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除对福费廷申请信息出现异常！", e);
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**

     * @方法名称: forftinToSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（福费廷合同申请）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional

    public List<IqpLoanApp> forftinToSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_05);
        return iqpLoanAppMapper.selectByModel(model);
    }

    /**

     * @方法名称: forftinDoneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（福费廷合同申请）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional

    public List<IqpLoanApp> forftinDoneSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_05);
        return iqpLoanAppMapper.selectByModel(model);
    }


    /**
     * @param
     * @return ResultDto
     * @author wzy
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 零售业务申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public IqpLoanApp saveIqpLoanAppSell(IqpLoanAppRetailDto iqpLoanAppRetailDto) {

        // 业务流水号
        String serno = "";
        int result = 0;

        //业务申请表
        IqpLoanApp iqpLoanApp = new IqpLoanApp();
        //赋值
        BeanUtils.copyProperties(iqpLoanAppRetailDto, iqpLoanApp);
        //辅助信息表
        IqpLoanAppAssist iqpLoanAppAssist = new IqpLoanAppAssist();
        //房屋信息表
        IqpHouse iqpHouse = new IqpHouse();
        //业务申请人信息表
        IqpApplAppt iqpApplAppt = new IqpApplAppt();
        //征信关联表
        CreditQryBizReal creditQryBizReal = new CreditQryBizReal();
        //征信详情表
        CreditReportQryLst creditReportQryLst = new CreditReportQryLst();
        String cusId = (String) iqpLoanApp.getCusId();
        String prdId = iqpLoanApp.getPrdId();
        iqpLoanApp.setBizType("01");
        if("02".equals(iqpLoanAppRetailDto.getAppChnl())){
            User user = SessionUtils.getUserInformation();
            UserIdentity org = user.getOrg();
            iqpLoanApp.setManagerBrId(org.getCode());
            iqpLoanApp.setManagerId(user.getLoginCode());
            iqpLoanApp.setInputId(user.getLoginCode());
            iqpLoanApp.setInputBrId(org.getCode());
        }
        // 默认使用授信额度
        iqpLoanApp.setIsUtilLmt("1");
        // 币种 默认人民币
        iqpLoanApp.setCurType("CNY");
        //默认期限类型为 M
        iqpLoanApp.setTermType("M");
        //贷款性质
        iqpLoanApp.setLoanCha("01");//自营贷款

        String openday = stringRedisTemplate.opsForValue().get("openDay");
        try {
            log.info("白领易贷通校验开始");
            //如果是白领易贷通产品，检查该客户下是否存在有效的合同，有则拒绝
            if("022028".equals(prdId)){
                //取系统日期
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                Map<String,String> hashMap = new HashMap();
                hashMap.put("cusId",cusId);
                hashMap.put("openDay",openDay);
                //如果是白领易贷通产品，检查该客户下是否存在有效的合同，有则拒绝
                result = ctrLoanContService.queryNormalContByCusId((HashMap<String, String>) hashMap);
                if( result >= 1){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "该客户下存在有效的合同,无法新增业务申请！");
                }
                //如果存在失效白领贷合同，并且合同到期日仍未到期，该笔业务申请为变更：复制合同信息至业务申请表中
                List<CtrLoanCont> ctrLoanContList = ctrLoanContService.queryUnuseContByCusId((HashMap<String, String>) hashMap);
                if (ctrLoanContList  != null && ctrLoanContList.size()>0) {
                    //复制原业务申请信息，生成新数据
                    serno = reconsidIqpLoanApp(ctrLoanContList.get(0).getIqpSerno());
                    iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(serno);//主表申请主表
                    iqpLoanApp.setOldIqpSerno("");//在新主表中放入旧业务流水号
                    iqpLoanApp.setIsReconsid("");//设置是复议标志
                    iqpLoanApp.setInputDate(openday);
                    iqpLoanApp.setChnlSour(iqpLoanAppRetailDto.getAppChnl());
                    updateSelective(iqpLoanApp);

                    //生成业务申请-辅助信息
                    IqpLoanAppAssist iqpLoanAppAssisttemp = iqpLoanAppAssistService.selectByPrimaryKey(serno);
                    if(iqpLoanAppAssisttemp == null ){
                        iqpLoanAppAssist.setIqpSerno(serno);
                        iqpLoanAppAssist.setAppChnl(iqpLoanAppRetailDto.getAppChnl());
                        result = iqpLoanAppAssistService.insertSelective(iqpLoanAppAssist);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成业务辅助信息数据异常！");
                        }
                    }

                    //生成客户申请表信息
                    IqpApplAppt iqpApplApptTemp  = iqpApplApptService.selectByPrimaryKey(serno);
                    if(iqpApplApptTemp == null){
                        iqpApplAppt.setIqpSerno(serno);
                        iqpApplAppt.setCusId(iqpLoanApp.getCusId());
                        iqpApplAppt.setCusId(iqpLoanApp.getCusName());
                        result = iqpApplApptService.insertSelective(iqpApplAppt);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成客户申请表信息异常！");
                        }
                    }


                }else{
                    serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
                    //无基本信息
                    iqpLoanApp.setOprType("01"); //设置操作类型为新增
                    iqpLoanApp.setApproveStatus("000"); //设置审批状态未待发起
                    iqpLoanApp.setBelgLine("02");//零售
                    iqpLoanApp.setGraper("0");
                    iqpLoanApp.setIqpSerno(serno);
                    iqpLoanApp.setAppCurType("CNY");
                    iqpLoanApp.setInputDate(openday);
                    iqpLoanApp.setChnlSour(iqpLoanAppRetailDto.getAppChnl());
                    result = this.insertSelective(iqpLoanApp);
                    //生成业务申请-辅助信息
                    iqpLoanAppAssist.setIqpSerno(serno);
                    iqpLoanAppAssist.setAppChnl(iqpLoanAppRetailDto.getAppChnl());
                    result = iqpLoanAppAssistService.insertSelective(iqpLoanAppAssist);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成业务辅助信息数据异常！");
                    }
                    //生成客户申请表信息
                    iqpApplAppt.setIqpSerno(serno);
                    iqpApplAppt.setCusId(iqpLoanApp.getCusId());
                    iqpApplAppt.setCusId(iqpLoanApp.getCusName());
                    result = iqpApplApptService.insertSelective(iqpApplAppt);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成客户申请表信息异常！");
                    }
                    String crqlSerno = null;
                    creditReportQryLst.setCusId(iqpLoanApp.getCusId());
                    creditReportQryLst.setCusName(iqpLoanApp.getCusName());
                    creditReportQryLst.setCertCode(iqpLoanApp.getCertCode());
                    creditReportQryLst.setCertType("A");
                    creditReportQryLst.setBorrowRel("001");
                    creditReportQryLst.setQryCls("0");
                    creditReportQryLst.setApproveStatus("000");
                    creditReportQryLst.setBorrowerCusId(iqpLoanApp.getCusId());
                    creditReportQryLst.setBorrowerCusName(iqpLoanApp.getCusName());
                    creditReportQryLst.setBorrowerCertCode(iqpLoanApp.getCertCode());
                    if (creditReportQryLstService.selectByPeriod("01",creditReportQryLst)) {
                        // 如果存在，直接关联
                        // 查出该信息
                        CreditReportQryLst creditReportQryLstNew = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                        crqlSerno = creditReportQryLstNew.getCrqlSerno();
                    } else {
                        //生成征信流水号
                        crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
                        //生成征信详情数据
                        creditReportQryLst.setCrqlSerno(crqlSerno);
                        creditReportQryLst.setQryResn(creditReportQryLstService.getQryResnByPerIodAndReal("001","01","0"));
                        result =creditReportQryLstService.insertSelective(creditReportQryLst);
                        if (result != 1) {
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信详情数据异常！");
                        }
                    }
                    //生成关联征信数据
                    creditQryBizReal.setBizSerno(serno);
                    creditQryBizReal.setCusId(iqpLoanApp.getCusId());
                    creditQryBizReal.setCusName(iqpLoanApp.getCusName());
                    creditQryBizReal.setCertCode(iqpLoanApp.getCertCode());
                    creditQryBizReal.setCrqlSerno(crqlSerno);
                    creditQryBizReal.setCertType("A");
                    creditQryBizReal.setBorrowRel("001");
                    creditQryBizReal.setScene("01");
                    result =creditQryBizRealService.insertSelective(creditQryBizReal);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信关联数据异常！");
                    }
                }
            }else{
                serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
                log.info("申请向导新增开始");
                //无基本信息
                iqpLoanApp.setOprType("01"); //设置操作类型为新增
                iqpLoanApp.setApproveStatus("000"); //设置审批状态未待发起
                iqpLoanApp.setBelgLine("02");//零售
                iqpLoanApp.setIqpSerno(serno);
                iqpLoanApp.setAppCurType("CNY");
                iqpLoanApp.setInputDate(openday);
                iqpLoanApp.setChnlSour(iqpLoanAppRetailDto.getAppChnl());
                result = this.insertSelective(iqpLoanApp);
                //生成业务申请-辅助信息
                iqpLoanAppAssist.setIqpSerno(serno);
                iqpLoanAppAssist.setAppChnl(iqpLoanAppRetailDto.getAppChnl());
                result = iqpLoanAppAssistService.insertSelective(iqpLoanAppAssist);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成业务辅助信息数据异常！");
                }
                //业务类型，如果是按揭类，新增基础房屋信息
                ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
                //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
                String catalogId = CfgPrdBasicinfoDto.getData().getCatalogId();
                if("C000700020001".equals(catalogId)){
                    iqpHouse.setIqpSerno(serno);
                    iqpHouse.setPkId(serno);
                    iqpHouse.setCusId(iqpLoanApp.getCusId());
                    iqpHouseService.insertSelective(iqpHouse);
                }
                //生成客户申请表信息
                iqpApplAppt.setIqpSerno(serno);
                iqpApplAppt.setCusId(iqpLoanApp.getCusId());
                iqpApplAppt.setCusId(iqpLoanApp.getCusName());
                result = iqpApplApptService.insertSelective(iqpApplAppt);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成客户申请表信息异常！");
                }

                String crqlSerno = null;
                creditReportQryLst.setCusId(iqpLoanApp.getCusId());
                creditReportQryLst.setCusName(iqpLoanApp.getCusName());
                creditReportQryLst.setCertCode(iqpLoanApp.getCertCode());
                creditReportQryLst.setCertType("A");
                creditReportQryLst.setBorrowRel("001");
                creditReportQryLst.setQryCls("0");
                creditReportQryLst.setApproveStatus("000");
                creditReportQryLst.setBorrowerCusId(iqpLoanApp.getCusId());
                creditReportQryLst.setBorrowerCusName(iqpLoanApp.getCusName());
                creditReportQryLst.setBorrowerCertCode(iqpLoanApp.getCertCode());
                if (creditReportQryLstService.selectByPeriod("01",creditReportQryLst)) {
                    // 如果存在，直接关联
                    // 查出该信息
                    CreditReportQryLst creditReportQryLstNew = creditReportQryLstMapper.selectByPeriod(creditReportQryLst);
                    crqlSerno = creditReportQryLstNew.getCrqlSerno();
                } else {
                    //生成征信流水号
                    crqlSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CRQL_SERNO, new HashMap<>());
                    //生成征信详情数据
                    creditReportQryLst.setCrqlSerno(crqlSerno);
                    creditReportQryLst.setQryResn(creditReportQryLstService.getQryResnByPerIodAndReal("001","01","0"));
                    result =creditReportQryLstService.insertSelective(creditReportQryLst);
                    if (result != 1) {
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信详情数据异常！");
                    }
                }

                //生成关联征信数据
                creditQryBizReal.setBizSerno(serno);
                creditQryBizReal.setCusId(iqpLoanApp.getCusId());
                creditQryBizReal.setCusName(iqpLoanApp.getCusName());
                creditQryBizReal.setCertCode(iqpLoanApp.getCertCode());
                creditQryBizReal.setCrqlSerno(crqlSerno);
                creditQryBizReal.setCertType("A");
                creditQryBizReal.setBorrowRel("001");
                creditQryBizReal.setScene("01");
                result =creditQryBizRealService.insertSelective(creditQryBizReal);
                if (result != 1) {
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "生成征信关联数据异常！");
                }
            }


            List<IqpStockLoanInfo>  list = iqpStockLoanInfoMapper.selectByIqpSerno(serno);
            if (list == null || list.size() == 0) {
                //如果不存在,先查询客户编号和客户姓名
                List<IqpStockLoanInfo> listInfo = null;
                QueryModel model = new QueryModel();
                model.addCondition("cusId",cusId);
                model.addCondition("oprType","01");
                model.addCondition("belgLine","02");
                List<LmtCrdReplyInfo> LmtCrdReplyInfoList = lmtCrdReplyInfoService.selectByModel(model);

                for(int i = 0; i < LmtCrdReplyInfoList.size(); i++){
                    LmtCrdReplyInfo lmtCrdReplyInfo = LmtCrdReplyInfoList.get(i);
                    IqpStockLoanInfo iqpStockLoanInfo = new IqpStockLoanInfo();
                    iqpStockLoanInfo.setIqpSerno(serno);
                    iqpStockLoanInfo.setPrdId(lmtCrdReplyInfo.getPrdId());
                    iqpStockLoanInfo.setPrdName(lmtCrdReplyInfo.getPrdName());
                    iqpStockLoanInfo.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());
                    iqpStockLoanInfo.setGuarMode(lmtCrdReplyInfo.getGuarMode());
                    //根据业务流水查询贷款余额
                    BigDecimal usedAmount = accLoanMapper.queryAccLoanLoanBalanceSum(lmtCrdReplyInfo.getSurveySerno());
                    if(usedAmount == null){
                        iqpStockLoanInfo.setBalance(new BigDecimal("0"));
                    }else{
                        iqpStockLoanInfo.setBalance(usedAmount);
                    }
                    iqpStockLoanInfo.setCreateTime(DateUtils.getCurrDate());
                    iqpStockLoanInfo.setUpdateTime(DateUtils.getCurrDate());
                    iqpStockLoanInfoMapper.insertSelective(iqpStockLoanInfo);
                }
            }


            //调查结论
            IqpSurveyConInfo iqpSurveyConInfoTemp = iqpSurveyConInfoMapper.selectByPrimaryKey(serno);
            if(iqpSurveyConInfoTemp == null){
                IqpSurveyConInfo iqpSurveyConInfo = new IqpSurveyConInfo();
                BeanUtils.copyProperties(iqpLoanApp, iqpSurveyConInfo);
                iqpSurveyConInfo.setDebitName(iqpLoanApp.getCusName());
                iqpSurveyConInfo.setDebitCode(iqpLoanApp.getCertCode());
                iqpSurveyConInfoMapper.insert(iqpSurveyConInfo);
            }


            IqpCusCreditConInfo iqpCusCreditConInfoTemp = iqpCusCreditConInfoService.selectByPrimaryKey(serno);
            if(iqpCusCreditConInfoTemp == null){
                IqpCusCreditConInfo iqpCusCreditConInfo = new IqpCusCreditConInfo();
                iqpCusCreditConInfo.setIqpSerno(iqpLoanApp.getIqpSerno());
                iqpCusCreditConInfoService.insert(iqpCusCreditConInfo);
            }

        } catch (Exception e) {
            log.info("申请向导新增报错{}", e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return iqpLoanApp;

    }


    /**
     * @param
     * @return ResultDto
     * @author wzy
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 零售业务申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto updateIqpLoanAppAndAssit(IqpLoanAppSLDto iqpLoanAppSLDto) {
        IqpLoanApp iqpLoanApp = iqpLoanAppSLDto.getIqpLoanApp();
        IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppSLDto.getIqpLoanAppAssist();
        IqpHouse iqpHouse = iqpLoanAppSLDto.getIqpHouse();
        try {
            String serno = iqpLoanApp.getIqpSerno();
            //修改基本信息
            iqpLoanApp.setAppCurType("CNY");
            iqpLoanApp.setManagerBrId(iqpLoanApp.getInputBrId());
            iqpLoanApp.setManagerId(iqpLoanApp.getInputId());
            iqpLoanApp.setCvtCnyAmt(iqpLoanApp.getAppAmt());
            iqpLoanApp.setContHighAvlAmt(iqpLoanApp.getAppAmt());
            int result = this.updateSelective(iqpLoanApp);
            //修改业务申请-辅助信息
            iqpLoanAppAssist.setIqpSerno(serno);
            int result2 = iqpLoanAppAssistService.updateSelective(iqpLoanAppAssist);
            iqpHouse.setPkId(serno);
            iqpHouse.setIqpSerno(serno);
            IqpHouse flag = iqpHouseService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());

            if (flag == null) {
                iqpHouseService.insertSelective(iqpHouse);

            } else {
                iqpHouseService.updateSelective(iqpHouse);
            }

            //调查结论
            IqpSurveyConInfo iqpSurveyConInfo = iqpSurveyConInfoMapper.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
            if(iqpSurveyConInfo == null){
                IqpSurveyConInfo iqpSurveyConInfoTemp = new IqpSurveyConInfo();
                BeanUtils.copyProperties(iqpLoanApp, iqpSurveyConInfoTemp);
                iqpSurveyConInfoTemp.setDebitName(iqpLoanApp.getCusName());
                iqpSurveyConInfoTemp.setDebitCode(iqpLoanApp.getCertCode());
                iqpSurveyConInfoTemp.setDebitAmt(iqpLoanApp.getAppAmt());//借款金额
                iqpSurveyConInfoTemp.setGuarMode(iqpLoanApp.getGuarWay());//担保方式
                iqpSurveyConInfoTemp.setLoanTerm(iqpLoanApp.getAppTerm()+"");//贷款期限
                iqpSurveyConInfoTemp.setLoanRate(iqpLoanApp.getExecRateYear());//贷款利率
                iqpSurveyConInfoTemp.setRepayMode(iqpLoanApp.getRepayMode());//还款方式
                iqpSurveyConInfoMapper.insert(iqpSurveyConInfoTemp);
            }else{
                iqpSurveyConInfo.setDebitAmt(iqpLoanApp.getAppAmt());//借款金额
                iqpSurveyConInfo.setGuarMode(iqpLoanApp.getGuarWay());//担保方式
                iqpSurveyConInfo.setLoanTerm(iqpLoanApp.getAppTerm()+"");//贷款期限
                iqpSurveyConInfo.setLoanRate(iqpLoanApp.getExecRateYear());//贷款利率
                iqpSurveyConInfo.setRepayMode(iqpLoanApp.getRepayMode());//还款方式
                iqpSurveyConInfoMapper.updateByPrimaryKeySelective(iqpSurveyConInfo);
            }

            //客户及关联人信用信息结论表
            IqpCusCreditConInfo iqpCusCreditConInfo = iqpCusCreditConInfoService.selectByPrimaryKey(serno);
            if(iqpCusCreditConInfo == null){
                IqpCusCreditConInfo iqpCusCreditConInfoTemp = new IqpCusCreditConInfo();
                iqpCusCreditConInfoTemp.setIqpSerno(iqpLoanApp.getIqpSerno());
                iqpCusCreditConInfoService.insert(iqpCusCreditConInfoTemp);
            }

        } catch (Exception e) {
            log.info("业务申请修改报错" + e.getMessage());
            throw BizException.error(null, "999999", "零售业务申请更新失败！");
        }
        return new ResultDto(iqpLoanApp);
    }


    /**
     * @param
     * @return ResultDto
     * @author shenli
     * @date 2021-6-16
     * @version 1.0.0
     * @desc 零售业务申请修改-专为移动接口提供
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto updateIqpLoanAppSellOam(IqpLoanAppLsUpdatDto iqpLoanAppLsUpdatDto) {
        IqpLoanApp iqpLoanApp = new IqpLoanApp();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanAppLsUpdatDto, iqpLoanApp);
        IqpLoanAppAssist iqpLoanAppAssist = new IqpLoanAppAssist();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanAppLsUpdatDto, iqpLoanAppAssist);
        IqpHouse iqpHouse = new IqpHouse();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanAppLsUpdatDto, iqpHouse);

        try {
            //修改基本信息
            iqpLoanApp.setAppCurType("CNY");
            iqpLoanApp.setManagerBrId(iqpLoanApp.getInputBrId());
            iqpLoanApp.setManagerId(iqpLoanApp.getInputId());
            int result = this.updateSelective(iqpLoanApp);
            iqpHouse.setPkId(iqpLoanApp.getIqpSerno());
            if(iqpHouse.getPundRate() != null ){
                iqpHouse.setPundRate(iqpHouse.getPundRate());
            }
            IqpHouse flag = iqpHouseService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());

            if (flag == null) {
                iqpHouseService.insertSelective(iqpHouse);

            } else {
                iqpHouseService.updateSelective(iqpHouse);
            }

            //调查结论
            IqpSurveyConInfo iqpSurveyConInfo = iqpSurveyConInfoMapper.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
            iqpSurveyConInfo.setDebitAmt(iqpLoanApp.getAppAmt());//借款金额
            iqpSurveyConInfo.setGuarMode(iqpLoanApp.getGuarWay());//担保方式
            iqpSurveyConInfo.setLoanTerm(iqpLoanApp.getAppTerm()+"");//贷款期限
            iqpSurveyConInfo.setLoanRate(iqpLoanApp.getExecRateYear());//贷款利率
            iqpSurveyConInfo.setRepayMode(iqpLoanApp.getRepayMode());//还款方式
            iqpSurveyConInfoMapper.updateByPrimaryKeySelective(iqpSurveyConInfo);

        } catch (Exception e) {
            log.info("业务申请修改报错" + e.getMessage());
            throw BizException.error(null, "999999", "零售业务申请更新失败！");
        }
        return new ResultDto(iqpLoanApp);
    }

    /**
     * @param
     * @return ResultDto
     * @author wzy
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 零售业务提交校验相关信息是否输入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public IqpLoanAppSLDto confirmIqpLoanApp(IqpLoanAppSLDto iqpLoanAppSLDto) {
        IqpLoanApp iqpLoanApp = iqpLoanAppSLDto.getIqpLoanApp();
        log.info("零售业务提交校验相关信息开始" + iqpLoanApp.getIqpSerno());

        try {
            iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);
            String serno = iqpLoanApp.getIqpSerno();
            String prdId = iqpLoanApp.getPrdId();
            //如果是打回重新提交，将审批信息同步至审批岗
            String approveStatus = iqpLoanApp.getApproveStatus();
            if("992".equals(approveStatus)){
                Map map = new HashMap();
                map.put("iqpSerno",serno);
                IqpLoanAppr iqpLoanAppr = iqpLoanApprMapper.queryBySernoAndNode(map);
                if(iqpLoanAppr !=null){
                    iqpLoanAppr.setApproveAdvice(iqpLoanApp.getAppAmt().toString());
                    iqpLoanAppr.setReplyTerm(Integer.parseInt(iqpLoanApp.getAppTerm().toString()));
                    iqpLoanAppr.setReplyRate(iqpLoanApp.getExecRateYear());
                    iqpLoanAppr.setRepayMode(iqpLoanApp.getRepayMode());
                    iqpLoanApp.setGuarWay(iqpLoanApp.getGuarWay());
                    int result = iqpLoanApprService.updateSelective(iqpLoanAppr);
                    if(result != 1){
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "修改批复信息异常！");
                    }
                }
            }

            IqpCusCreditConInfo iqpCusCreditConInfo = iqpCusCreditConInfoService.selectByPrimaryKey(serno);
            if(iqpCusCreditConInfo != null){
                String otherDesc = iqpCusCreditConInfo.getOtherDesc();
                if(otherDesc == null || "".equals(otherDesc)){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "零售调查结论不可为空");
                }
            }else{
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "零售调查结论不可为空");
            }

            List<IqpCusLsnpInfo> iqpCusLsnpInfoList = iqpCusLsnpInfoService.selectByIqpSerno(serno);
            if(iqpCusLsnpInfoList == null || iqpCusLsnpInfoList.size() == 0){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "内评信息未测算");
            }

            iqpLoanAppSLDto.setRuleRiskLvl(iqpCusLsnpInfoList.get(0).getRuleRiskLvl());
            //白领贷自动审批
            if("022028".equals(prdId)){
                try {
                    ResultDto<CfgRetailPrimeRateDto> CfgRetailPrimeRateDto = iCmisCfgClientService.selectbyPrdId(prdId);
                    iqpLoanAppSLDto.setOfferRate(CfgRetailPrimeRateDto.getData().getOfferRate());
                    IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoList.get(0);
                    iqpLoanAppSLDto.setInteRiskLvl(iqpCusLsnpInfo.getInteRiskLvl());
                    iqpLoanAppSLDto.setLimitFlag(iqpCusLsnpInfo.getLimitFlag());
                }catch (Exception e) {
                    log.info("白领贷自动审批异常：" + e.getMessage());
                }
            }

            iqpLoanAppSLDto.setIsRelationSuperPower("0");
            // 是否关联交易
            try {
                String isGLAppr = bizCommonService.checkCusIsGlf(iqpLoanApp.getCusId());
                // 如果是关联交易
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isGLAppr)){
                    Map<String,Object> gljyMap = bizCommonService.calGljyType(iqpLoanApp.getCusId(),iqpLoanApp.getAppAmt(),BigDecimal.ZERO,BigDecimal.ZERO,cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));
                    String isSupGLFLmtAmt = (String)gljyMap.get("isSupGLFLmtAmt");
                    iqpLoanAppSLDto.setIsRelationSuperPower(isSupGLFLmtAmt);//否超过关联方预计额度
                }
            }catch (Exception e) {
                log.info("是否关联交易异常：" + e.getMessage());
            }

            //按揭,校验房屋信息是否输入
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
            if("10".equals(prdType)){
                IqpHouse iqphouse =  iqpHouseService.selectByIqpSernos(serno);
                if(iqphouse == null || iqphouse.getHouseStatus() == null || "".equals(iqphouse.getHouseStatus())){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "购房信息未录入");
                }

                //共同借款人
                ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(iqpLoanApp.getCusId());
                String marStatus = cusIndivDto.getData().getMarStatus() == null?"":cusIndivDto.getData().getMarStatus();
                //按揭类贷款已婚的情况下，共同借款人必输，其他非必输
                if("20".equals(marStatus)){
                    List<LmtCobInfo>  lmtCobInfo = lmtCobInfoMapper.selectByIqpSerno(serno);
                    if(lmtCobInfo.size() <=0){
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "共同借款人信息未录入");
                    }
                }
            }
            //担保方式
            String guarType = iqpLoanApp.getGuarWay();
            //保证
            QueryModel  model = new QueryModel();
            model.addCondition("serno",serno);
            if ( "30".equals(guarType)) {
                List<GuarBizRelGuaranteeDto>  guarBizRel = guarGuaranteeMapper.selectByIqpSernoModel(model);
                if(guarBizRel.size() <=0){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "保证人信息未录入");
                }
                //抵质押
            }else if("10".equals(guarType) ||"20".equals(guarType) ||"21".equals(guarType) ||"40".equals(guarType)){
                List<GuarBizRelGuarBaseDto> guarBaseInfo = guarBaseInfoMapper.selectByIqpSernoModel(model);
                if(guarBaseInfo.size() <=0){
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "抵质押信息未录入");
                }
            }
            //还款能力分析
            IqpDisAssetIncomeDto iqpDisAssetIncomeDtos = iqpDisAssetIncomeService.selectMore(serno);
            if(iqpDisAssetIncomeDtos == null){
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "还款能力分析信息未录入！");
            }

            //是否pc端发起:1-是,0-否
            IqpLoanAppAssist iqpLoanAppAssist = iqpLoanAppAssistService.selectByPrimaryKey(serno);
            if("02".equals(iqpLoanAppAssist.getAppChnl())){
                iqpLoanAppSLDto.setIsPc("1");
            }else{
                iqpLoanAppSLDto.setIsPc("0");
            }

            log.info("审批模式开始---------------------");
            String apprMode = "";
            String cusId = iqpLoanApp.getCusId();
            if("10".equals(prdType)){//10零售按揭类
                //获取零售内评信息
                IqpCusLsnpInfo iqpCusLsnpInfo = iqpCusLsnpInfoList.get(0);
                String appScoreRiskLvl = iqpCusLsnpInfo.getAppScoreRiskLvl();//申请评分风险等级

                //审批中的申请金额
                BigDecimal appAmtAj = iqpLoanAppMapper.selectAppAmtAj(cusId)==null?new BigDecimal(0):iqpLoanAppMapper.selectAppAmtAj(cusId);
                log.info("审批中的零售按揭申请金额: "+ appAmtAj);
                //借据余额
                BigDecimal loanBalanceAj = accLoanMapper.selectAccLoanBalanceAj(cusId)==null?new BigDecimal(0):accLoanMapper.selectAccLoanBalanceAj(cusId);
                log.info("零售按揭借据余额: "+ loanBalanceAj);
                //台账金额
                BigDecimal loanAmtAj = accLoanMapper.selectLoanAmtAj(cusId)==null?new BigDecimal(0):accLoanMapper.selectLoanAmtAj(cusId);
                log.info("零售按揭台账金额: "+ loanAmtAj);
                //合同金额
                BigDecimal contAmtAj = ctrLoanContMapper.selectContAmtAj(cusId)==null?new BigDecimal(0):ctrLoanContMapper.selectContAmtAj(cusId);
                log.info("零售按揭合同金额: "+ contAmtAj);
                //已通过审批的业务金额 = 合同金额 - 台账金额
                BigDecimal AgreeAmtAj = contAmtAj.subtract(loanAmtAj);
                log.info("已通过审批的零售按揭业务金额 = 零售按揭合同金额 - 零售按揭台账金额: "+ AgreeAmtAj);
                //总金额 = 本次申请金额+借据余额+审批中的申请金额+合同金额   2021-9-29 14:13:34 与范东宇确认公式
                BigDecimal appAmt = iqpLoanApp.getAppAmt().add(appAmtAj).add(loanBalanceAj).add(AgreeAmtAj);
                log.info("总金额: "+ appAmtAj);

                QueryModel model1 = new QueryModel();
                model1.getCondition().put("prdId", prdId);
                model1.getCondition().put("cusCrdGrade", appScoreRiskLvl);
                List<IqpApproveMode> iqpApproveModeList =  iqpApproveModeService.selectByModel(model1);
                for(int i = 0; i<iqpApproveModeList.size();i++){

                    IqpApproveMode iqpApproveMode = iqpApproveModeList.get(i);
                    BigDecimal appAmtMin = iqpApproveMode.getAppAmtMin();
                    BigDecimal appAmtMax = iqpApproveMode.getAppAmtMax();
                    if(appAmt.compareTo(appAmtMin) >0 && appAmt.compareTo(appAmtMax) <=0){
                        iqpLoanAppSLDto.setApprovalModel(iqpApproveMode.getApprMode());
                        apprMode = iqpApproveMode.getApprMode();
                        break;
                    }
                }
            }else if("11".equals(prdType)){//11零售非按揭类

                if("022017".equals(prdId))//理财通 :单签模式
                {
                    iqpLoanAppSLDto.setApprovalModel("01");
                    apprMode = "01";

                }else{

                    int countCd =  iqpLoanAppMapper.selectCdCount(serno);
                    if(countCd > 0){

                        iqpLoanAppSLDto.setApprovalModel("01");//线下存单质押 :单签模式
                        apprMode = "01";

                    }else{

                        String guarTypeName = "";
                        BigDecimal appAmt = BigDecimal.ZERO;

                        //int countFdc = iqpLoanAppMapper.selectFdcCount(iqpSerno);
                        if("10".equals(guarType)){//抵押

                            guarTypeName = "房地产抵押";

                        }else if("00".equals(guarType)){//信用

                            guarTypeName = "信用";

                        }else if("30".equals(guarType)){//保证担保

                            guarTypeName = "保证担保";

                        }else{//其他资产担保（除存单质押、理财通）

                            guarTypeName = "其他资产担保（除存单质押、理财通）";

                        }

                        //审批中的申请金额
                        Map params = new HashMap();
                        params.put("cusId", cusId);
                        params.put("guarWay",guarType);
                        BigDecimal appAmtFaj = iqpLoanAppMapper.selectAppAmtFaj(params)==null?new BigDecimal(0):iqpLoanAppMapper.selectAppAmtFaj(params);
                        log.info("审批中的零售非按揭申请金额: "+ appAmtFaj);
                        //非按揭除白领贷台账余额
                        BigDecimal loanBalanceFaj = accLoanMapper.selectAccLoanBalanceFaj(params)==null?new BigDecimal(0):accLoanMapper.selectAccLoanBalanceFaj(params);
                        log.info("非按揭除白领贷台账余额: "+ loanBalanceFaj);
                        //非按揭除白领贷台账金额
                        BigDecimal loanAmtFaj = accLoanMapper.selectLoanAmtFaj(params)==null?new BigDecimal(0):accLoanMapper.selectLoanAmtFaj(params);
                        log.info("非按揭除白领贷台账金额: "+ loanAmtFaj);
                        //非按揭除白领贷合同金额
                        BigDecimal contAmtFaj = ctrLoanContMapper.selectContAmtFaj(params)==null?new BigDecimal(0):ctrLoanContMapper.selectContAmtFaj(params);
                        log.info("非按揭除白领贷合同金额: "+ contAmtFaj);
                        //非按揭除白领贷已通过审批的业务金额 = 合同金额- 台账金额
                        BigDecimal AgreeAmtFaj = contAmtFaj.subtract(loanAmtFaj);
                        log.info("非按揭除白领贷已通过审批的业务金额 = 合同金额- 台账金额: "+ AgreeAmtFaj);
                        //白领贷合同金额
                        BigDecimal contAmtFajBld = ctrLoanContMapper.selectContAmtFajBld(params)==null?new BigDecimal(0):ctrLoanContMapper.selectContAmtFajBld(params);
                        log.info("白领贷合同金额: "+ contAmtFajBld);
                        //本次申请金额+审批中的申请金额+借据余额（除白领贷）+已通过审批的业务金额（除白领贷）+白领贷合同金额 2021-9-29 14:13:34 与范东宇确认公式
                        appAmt = iqpLoanApp.getAppAmt().add(appAmtFaj).add(loanBalanceFaj).add(AgreeAmtFaj).add(contAmtFajBld);
                        log.info("总金额: "+ appAmt);

                        QueryModel model1 = new QueryModel();
                        model1.getCondition().put("prdType","11");
                        model1.getCondition().put("guarType", guarTypeName);
                        List<IqpApproveMode> iqpApproveModeList =  iqpApproveModeService.selectByModel(model1);
                        for(int i = 0; i<iqpApproveModeList.size();i++){

                            IqpApproveMode iqpApproveMode = iqpApproveModeList.get(i);
                            BigDecimal appAmtMin = iqpApproveMode.getAppAmtMin();
                            BigDecimal appAmtMax = iqpApproveMode.getAppAmtMax();
                            if(appAmt.compareTo(appAmtMin) >0 && appAmt.compareTo(appAmtMax) <=0){
                                iqpLoanAppSLDto.setApprovalModel(iqpApproveMode.getApprMode());
                                apprMode = iqpApproveMode.getApprMode();
                                break;
                            }
                        }

                    }
                }
            }

            if("".equals(apprMode)){
                apprMode = "01";
                iqpLoanAppSLDto.setApprovalModel("01");
            }

            iqpLoanAppAssist.setApprMode(apprMode);
            iqpLoanAppAssistService.update(iqpLoanAppAssist);

        } catch (Exception e) {
            log.info("校验必输信息错误" + e.getMessage());
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.info("零售业务提交校验相关信息结束");
        return iqpLoanAppSLDto;
    }

    /**
     * @param
     * @return ResultDto
     * @author wzy
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 根据【LPR定价区间】的选择，自动从核心获取当前LPR利率。
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map getLprRate(String newVal) {
        Map rtnData = new HashMap();
        String rate="";
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {

            QueryModel  queryModel = new QueryModel();
            queryModel.getCondition().put("rateTypeId",newVal);
            queryModel.getCondition().put("rateTypeName","10");
            ResultDto<CfgLprRateDto>  resultDto= iCmisCfgClientService.selectone(queryModel);
            if("A1".equals(newVal)){

                rate=resultDto.getData().getRate().toPlainString();
            }else if("A2".equals(newVal)){
                rate=resultDto.getData().getRate().toPlainString();
            }
        } catch (Exception e) {
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
            log.info("申请向导新增报错{}", e.getMessage());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("rate", rate);
        }
        return rtnData;

    }


    /**
     * @param
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @author shenli
     * @date 2021/4/28 0028 20:57
     * @version 1.0.0
     * @desc 小微合同申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map<String,Object> createIqpLoanAppXw(IqpLoanApp iqpLoanApp) {

        log.info("******************小微-合同申请创建 开始 ******************");

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("code", "0000");
        returnMap.put("msg", "成功");

        try {
            /***
             * 惠享贷合同线下签约检验条件
             * 1、首先判断申请的贷款为惠享贷
             * 2、若是签约产品为惠享贷，则判断是否存在已生效的优企贷、优农贷或惠享贷产品
             * ********************/
            String prdId = iqpLoanApp.getPrdId();
            if("SC060001".equals(prdId)){//惠享贷
                Map contNoParamMap = new HashMap();
                contNoParamMap.put("cusId", iqpLoanApp.getCusId());
                contNoParamMap.put("prdId", prdId);
                int count = ctrLoanContService.quertOtherPrdIdContSum(contNoParamMap);
                if(count>0){
                    returnMap.put("code", "9999");
                    returnMap.put("msg", "请确认是否存在已生效的优企贷、优农贷或惠享贷产品！");
                }
            }
            iqpLoanApp.setIqpSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap()));

            CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(iqpLoanApp.getCusId());
            iqpLoanApp.setPhone(cusIndivContactDto.getMobileNo());
            iqpLoanApp.setContNo(sequenceTemplateClient.getSequenceTemplate(cmisBizXwCommonService.getDKSeq(iqpLoanApp.getManagerBrId()), new HashMap<>()));
            // 2021年9月25日14:19:39 hubp 塞入渠道来源字段，02为PC端
            iqpLoanApp.setChnlSour("02");
            // 默认使用授信额度
            iqpLoanApp.setIsUtilLmt("1");
            // 币种 默认人民币
            iqpLoanApp.setCurType("CNY");
            // 额度分项编号
            iqpLoanApp.setLmtAccNo(iqpLoanApp.getReplyNo());
            // 录入时间
            iqpLoanApp.setCreateTime(DateUtils.getCurrDate());
            iqpLoanAppMapper.insert(iqpLoanApp);
            returnMap.put("IqpLoanApp", iqpLoanApp);

        } catch (Exception e) {
            e.printStackTrace();
            returnMap.put("code", "9999");
            returnMap.put("msg", "合同申请创建异常！");
        }

        log.info("******************小微-合同申请创建 结束 ******************");

        return returnMap;

    }

    /**
     * @param iqpLoanApp
     * @return void
     * @author 王玉坤
     * @date 2021/5/4 20:44
     * @version 1.0.0
     * @desc 零售按揭类业务处理逻辑
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBizEnd(IqpLoanApp iqpLoanApp) throws Exception {
        log.info("零售按揭类业务申请审批结束后业务处理逻辑开始【{}】", iqpLoanApp.getIqpSerno());
        // 批复信息实体类
        //IqpAppReply iqpAppReply = null;
        // 批复信息实体类
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        // 合同主表实体类
        CtrLoanCont ctrLoanCont = null;
        // 合同流水号
        String contNo = null;
        // 临时变量
        int nums;
        try {

            String openday = stringRedisTemplate.opsForValue().get("openDay");
            iqpLoanApp.setStartDate(openday);
            iqpLoanApp.setEndDate(DateUtils.addMonth(iqpLoanApp.getStartDate(), "yyyy-MM-dd", iqpLoanApp.getAppTerm().intValue()));
            if(iqpLoanApp.getIsOutstndTrdLmtAmt() == null || "".equals(iqpLoanApp.getIsOutstndTrdLmtAmt())){
                iqpLoanApp.setIsOutstndTrdLmtAmt("0");//不占用的赋值为否
            }


            if ("1".equals(iqpLoanApp.getPreferRateFlag())) {
                log.info("根据业务流水："+iqpLoanApp.getIqpSerno()+"查询优惠利率信息开始！");
                RetailPrimeRateApp  retailprimerateapp = retailPrimeRateAppService.selectByIqpSerno(iqpLoanApp.getIqpSerno());
                if(retailprimerateapp != null){
                    RetailPrimeRateAppr Retailprimerateappr = retailPrimeRateApprService.selectBySernoOrderByDate(retailprimerateapp.getSerno());
                    if(Retailprimerateappr !=null){
                        log.info("根据流水："+retailprimerateapp.getSerno() +"查询获取的批复利率为："+Retailprimerateappr.getReplyRate());
                        iqpLoanApp.setExecRateYear(Retailprimerateappr.getReplyRate());
                        log.info("赋值结束："+iqpLoanApp.getExecRateYear());
                    }
                }
            }

            // 1、生成批复信息
            //iqpAppReply = iqpAppReplyService.insertIqpAppReply(iqpLoanApp);
            // 1、生成批复信息
            lmtCrdReplyInfo = lmtCrdReplyInfoService.insertLmtCrdReplyRetail(iqpLoanApp);
            IqpLoanAppr iqpLoanApprTemp = new IqpLoanAppr();
            iqpLoanApprTemp.setIqpSerno(iqpLoanApp.getIqpSerno());
            IqpLoanAppr iqpLoanAppr = iqpLoanApprService.queryBySernoAndNode(iqpLoanApprTemp);
            if(iqpLoanAppr != null){
                lmtCrdReplyInfo.setReplyAmt(iqpLoanAppr.getReplyAmt());
                lmtCrdReplyInfo.setAppTerm(iqpLoanAppr.getReplyTerm());
                lmtCrdReplyInfo.setExecRateYear(iqpLoanAppr.getReplyRate());
                lmtCrdReplyInfo.setRepayMode(iqpLoanAppr.getRepayMode());
                lmtCrdReplyInfo.setGuarMode(iqpLoanAppr.getGuarMode());
                lmtCrdReplyInfo.setReplyStartDate(openday);
                lmtCrdReplyInfo.setReplyEndDate(DateUtils.addMonth(lmtCrdReplyInfo.getReplyStartDate(), "yyyy-MM-dd", lmtCrdReplyInfo.getAppTerm().intValue()));
                lmtCrdReplyInfoService.updateSelective(lmtCrdReplyInfo);
            }

            // 2、前往额度系统建立额度
            // 组装额度系统报文
            CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            cmisLmt0001ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));
            cmisLmt0001ReqDto.setAccNo(iqpLoanApp.getIqpSerno());
            cmisLmt0001ReqDto.setCurType(lmtCrdReplyInfo.getCurType());
            cmisLmt0001ReqDto.setCusId(lmtCrdReplyInfo.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtCrdReplyInfo.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("0");
            cmisLmt0001ReqDto.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);
            cmisLmt0001ReqDto.setTerm(lmtCrdReplyInfo.getAppTerm());
            cmisLmt0001ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());
            cmisLmt0001ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001ReqDto.setManagerId(iqpLoanApp.getManagerId());
            cmisLmt0001ReqDto.setManagerBrId(iqpLoanApp.getManagerBrId());
            cmisLmt0001ReqDto.setInputId(iqpLoanApp.getInputId());
            cmisLmt0001ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
            cmisLmt0001ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));

            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<CmisLmt0001LmtSubListReqDto>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo("");
            cmisLmt0001LmtSubListReqDto.setParentId(iqpLoanApp.getIqpSerno());
            // 额度品种编号
            String limitSubNo = "";
            ResultDto<List<LmtSubPrdMappConfDto>> listResultDto = cmisLmtClientService.selectLimitSubNoByPrdId(lmtCrdReplyInfo.getPrdId());
            if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode()) && CollectionUtils.nonEmpty(listResultDto.getData())) {
                limitSubNo = listResultDto.getData().get(0).getLimitSubNo();
            }else{
                limitSubNo = lmtCrdReplyInfo.getPrdId();
            }
            cmisLmt0001LmtSubListReqDto.setLimitSubNo(limitSubNo);
            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtCrdReplyInfo.getPrdName());
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02);
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt());
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType());
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate());
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate());
            cmisLmt0001LmtSubListReqDto.setBailPreRate(iqpLoanApp.getBailPerc());
            cmisLmt0001LmtSubListReqDto.setRateYear(lmtCrdReplyInfo.getExecRateYear());
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtCrdReplyInfo.getGuarMode());
            cmisLmt0001LmtSubListReqDto.setLmtGraper(0);

            if("022028".equals(lmtCrdReplyInfo.getPrdId())){
                cmisLmt0001LmtSubListReqDto.setIsRevolv("1");//白领贷可循环
            }else{
                cmisLmt0001LmtSubListReqDto.setIsRevolv("0");
            }
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0");
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0");
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);

            // 调用额度接口
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-建立额度请求报文：" + cmisLmt0001ReqDto.toString());
            ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-建立额度返回报文：" + cmisLmt0001RespDto.toString());


            if (cmisLmt0001RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0001RespDto.getData().getErrorCode())) {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度成功！", iqpLoanApp.getIqpSerno());
            } else {
                log.info("根据业务申请编号【{}】,前往额度系统建立额度失败！", iqpLoanApp.getIqpSerno());
                throw new Exception("额度系统-前往额度系统建立额度失败:" + cmisLmt0001RespDto.getData().getErrorMsg());
            }

            iqpLoanApp.setReplyNo(lmtCrdReplyInfo.getReplySerno());
            iqpLoanApp.setLmtAccNo(lmtCrdReplyInfo.getReplySerno());
            // 4、生成合同主表信息
            ctrLoanCont = new CtrLoanCont();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanApp, ctrLoanCont);
            // 生成合同流水号
            contNo = sequenceTemplateClient.getSequenceTemplate(cmisBizXwCommonService.getDKSeq(iqpLoanApp.getManagerBrId()), new HashMap<>());
            ctrLoanCont.setContNo(contNo);
            // 是否使用授信额度 默认是
            ctrLoanCont.setIsUtilLmt("1");
            ctrLoanCont.setApproveStatus("000");
            ctrLoanCont.setContStatus("100");//100-未生效
            ctrLoanCont.setCertType("A");//证件类型
            ctrLoanCont.setLprRateIntval(iqpLoanApp.getLprRateIntval());//LPR定价区间
            ctrLoanCont.setAddr(iqpLoanApp.getDeliveryAddr());
            ctrLoanCont.setUpdateTime(DateUtils.getCurrDate());
            ctrLoanCont.setCreateTime(DateUtils.getCurrDate());
            ctrLoanCont.setHighAvlAmt(lmtCrdReplyInfo.getReplyAmt());
            ctrLoanCont.setContAmt(lmtCrdReplyInfo.getReplyAmt());
            ctrLoanCont.setContTerm(lmtCrdReplyInfo.getAppTerm());
            ctrLoanCont.setExecRateYear(iqpLoanApp.getExecRateYear());
            ctrLoanCont.setRepayMode(lmtCrdReplyInfo.getRepayMode());
            ctrLoanCont.setGuarWay(lmtCrdReplyInfo.getGuarMode());
            ctrLoanCont.setContStartDate(lmtCrdReplyInfo.getReplyStartDate());
            ctrLoanCont.setContEndDate(lmtCrdReplyInfo.getReplyEndDate());
            ctrLoanCont.setLmtAccNo(ctrLoanCont.getReplyNo());
            ctrLoanCont.setLinkman(ctrLoanCont.getCusName());
            ctrLoanCont.setCvtCnyAmt(ctrLoanCont.getContAmt());
            ctrLoanCont.setCurType("CNY");
            //结息方式
            ctrLoanCont.setEiMode(iqpLoanApp.getEiMode());
            //签约方式 01--"线下，02-"线上"
            ctrLoanCont.setSignMode("01");
            //签约渠道 01--手机银行 02--PC端
            ctrLoanCont.setSignChannel("02");
            //签约状态 0-否，1-是
            ctrLoanCont.setSignState("1");
            //默认期限类型为 M
            ctrLoanCont.setTermType("M");
            //一般合同 逾期利率浮动比 逾期执行利率(年利率) 复息利率浮动比 复息执行利率(年利率) 违约利率浮动百分比 违约利率（年）为空
            if("1".equals(ctrLoanCont.getContType())){

                //逾期利率浮动比
                ctrLoanCont.setOverdueRatePefloat(new BigDecimal("0.5"));
                //逾期执行利率(年利率)
                ctrLoanCont.setOverdueExecRate(ctrLoanCont.getExecRateYear().multiply(new BigDecimal("1.5")));

                //复息利率浮动比
                ctrLoanCont.setCiRatePefloat(new BigDecimal("0.5"));
                //复息执行利率(年利率)
                ctrLoanCont.setCiExecRate(ctrLoanCont.getExecRateYear().multiply(new BigDecimal("1.5")));

                //违约利率浮动百分比
                ctrLoanCont.setDefaultRate(new BigDecimal("0.5"));
                //违约利率(年)
                ctrLoanCont.setDefaultRateY(ctrLoanCont.getExecRateYear().multiply(new BigDecimal("1.5")));

            }
            ctrLoanCont.setDeductDeduType("AUTO");//扣款扣息方式
            //借款利率调整日
            ctrLoanCont.setLoanRateAdjDay(iqpLoanApp.getLoanRateAdjDay());
            //是否占用第三方额度
            ctrLoanCont.setIsOutstndTrdLmtAmt(iqpLoanApp.getIsOutstndTrdLmtAmt());
            //贷款性质
            ctrLoanCont.setLoanCha(iqpLoanApp.getLoanCha());//自营贷款


            try{
                CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(iqpLoanApp.getCusId());
                ctrLoanCont.setAddr(cusIndivContactDto.getDeliveryStreet());//地址
                ctrLoanCont.setPhone(cusIndivContactDto.getMobileNo());//电话
                ctrLoanCont.setFax(cusIndivContactDto.getFaxCode());//传真
                ctrLoanCont.setEmail(cusIndivContactDto.getEmail());//邮箱
                ctrLoanCont.setQq(cusIndivContactDto.getQq());//QQ
                ctrLoanCont.setWechat(cusIndivContactDto.getWechatNo());//微信
            }catch (Exception e){
                log.info("获取客户联系信息:"+ e);
            }
            nums = ctrLoanContService.insert(ctrLoanCont);
            Asserts.isTrue(nums > 0, "插入合同主表信息出错！");

            //022040个人二手住房按揭贷款（资金托管）
            //022051 个人二手商用房按揭贷款（资金托管）
            //这两个产品写死张家港市房产交易服务中心
            if ("022040".equals(ctrLoanCont.getPrdId()) || "022051".equals(ctrLoanCont.getPrdId())) {
                //插入一个固定的托管账号
                ToppAcctSub toppAcctSub = new ToppAcctSub();
                toppAcctSub.setBizSerno(ctrLoanCont.getContNo());
                toppAcctSub.setToppAcctNo("801000006838588");
                toppAcctSub.setToppName("张家港市房产交易服务中心");
                toppAcctSub.setToppAmt(ctrLoanCont.getHighAvlAmt());
                toppAcctSub.setIsBankAcct("1");
                toppAcctSub.setBizSence("2");
                toppAcctSub.setIsOnline("1");
                toppAcctSub.setOprType("01");
                toppAcctSub.setPkId(StringUtils.uuid(true));
                toppAcctSubService.insert(toppAcctSub);
            }

            //3. 授信申请更新批复编号
            iqpLoanApp.setContNo(contNo);
            update(iqpLoanApp);


            //5.生成担保合同相关表
            try {
                String guarWay = iqpLoanApp.getGuarWay();
                if(!"00".equals(guarWay)){
                    log.info("担保合同生成开始。。。。");
                    GrtGuarCont grtGuarContData = new GrtGuarCont();
                    GrtGuarBizRstRel grtGuarBizRstRelData = new GrtGuarBizRstRel();

                    String uuid = UUID.randomUUID().toString();
                    grtGuarContData.setInputDate(DateUtils.getCurrDateStr());
                    grtGuarContData.setUpdDate(DateUtils.getCurrDateStr());
                    grtGuarContData.setGuarPkId(uuid);
                    grtGuarContData.setCusId(ctrLoanCont.getCusId());
                    grtGuarContData.setCusName(ctrLoanCont.getCusName());
                    grtGuarContData.setBizLine("02");//零售

                    /** 担保合同编号  **/
                    grtGuarContData.setGuarContNo(sequenceTemplateClient.getSequenceTemplate("GRT_GUAR_SERNO", new HashMap<>()));
                    /** 担保合同中文合同编号 **/
                    grtGuarContData.setGuarContCnNo("");
                    /** 是否追加担保  STD_ZB_YES_NO **/
                    grtGuarContData.setIsSuperaddGuar("0");
                    /** 担保合同类型 STD_ZB_GUAR_CONT_TYPE **/
                    String contType = iqpLoanApp.getContType();
                    if("1".equals(contType)){
                        grtGuarContData.setGuarContType("A");
                    }else if("2".equals(contType)){
                        grtGuarContData.setGuarContType("B");
                    }
                    /** 担保合同担保方式 STD_ZB_GUAR_WAY **/
                    grtGuarContData.setGuarWay(ctrLoanCont.getGuarWay());
                    /** 保证方式 **/
                    //assureWay;
                    /** 是否在线抵押 **/
                    grtGuarContData.setIsOnlinePld(ctrLoanCont.getIsOlPld());
                    /** 是否授信项下 STD_ZB_YES_NO **/
                    grtGuarContData.setIsUnderLmt("1");
                    /** 币种 **/
                    grtGuarContData.setCurType(ctrLoanCont.getCurType());
                    /** 担保金额 **/
                    grtGuarContData.setGuarAmt(ctrLoanCont.getContAmt());
                    /** 担保期限 **/
                    grtGuarContData.setGuarTerm(ctrLoanCont.getContTerm().intValue());
                    /** 担保起始日 **/
                    grtGuarContData.setGuarStartDate(ctrLoanCont.getContStartDate());
                    /** 担保终止日 **/
                    grtGuarContData.setGuarEndDate(ctrLoanCont.getContEndDate());
                    /** 担保合同状态 STD_ZB_GRT_ST **/
                    grtGuarContData.setGuarContState("100");
                    /** 主办人 **/
                    grtGuarContData.setManagerId(ctrLoanCont.getManagerId());
                    /** 主办机构 **/
                    grtGuarContData.setManagerBrId(ctrLoanCont.getManagerBrId());
                    /** 登记人 **/
                    grtGuarContData.setInputId(ctrLoanCont.getInputId());
                    /** 登记机构 **/
                    grtGuarContData.setInputBrId(ctrLoanCont.getInputBrId());
                    /** 登记日期 **/
                    grtGuarContData.setInputDate(ctrLoanCont.getInputDate());
                    /** 最后修改人 **/
                    grtGuarContData.setUpdId(ctrLoanCont.getUpdId());
                    /** 最后修改机构 **/
                    grtGuarContData.setUpdBrId(ctrLoanCont.getUpdBrId());
                    /** 最后修改日期 **/
                    grtGuarContData.setUpdDate(ctrLoanCont.getUpdDate());
                    /** 操作类型  STD_ZB_OPR_TYPE **/
                    grtGuarContData.setOprType("01");
                    /** 业务条线 **/
                    grtGuarContData.setBizLine("02");
                    /** 是否将存量债务纳入担保范围 默认否**/
                    grtGuarContData.setIsDebtGuar("0");
                    /** 批复编号 **/
                    grtGuarContData.setReplyNo(iqpLoanApp.getReplyNo());
                    grtGuarContMapper.insert(grtGuarContData);

                    grtGuarBizRstRelData.setGuarPkId(uuid);
                    grtGuarBizRstRelData.setGuarContNo(grtGuarContData.getGuarContNo());
                    grtGuarBizRstRelData.setGuarAmt(grtGuarContData.getGuarAmt());
                    grtGuarBizRstRelData.setOprType("01");
                    grtGuarBizRstRelData.setCorreRel("1");
                    grtGuarBizRstRelData.setIsAddGuar("0");

                    /** 主办人 **/
                    grtGuarBizRstRelData.setManagerId(iqpLoanApp.getManagerId());
                    /** 主办机构 **/
                    grtGuarBizRstRelData.setManagerBrId(iqpLoanApp.getManagerBrId());
                    /** 登记人 **/
                    grtGuarBizRstRelData.setInputId(iqpLoanApp.getInputId());
                    /** 登记机构 **/
                    grtGuarBizRstRelData.setInputBrId(iqpLoanApp.getInputBrId());
                    /** 最后修改人 **/
                    grtGuarBizRstRelData.setUpdId(iqpLoanApp.getUpdId());
                    /** 最后修改机构 **/
                    grtGuarBizRstRelData.setUpdBrId(iqpLoanApp.getUpdBrId());
                    grtGuarBizRstRelData.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                    grtGuarBizRstRelData.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
                    grtGuarBizRstRelData.setSerno(ctrLoanCont.getIqpSerno());//流水号
                    grtGuarBizRstRelData.setContNo(ctrLoanCont.getContNo());//合同号
                    grtGuarBizRstRelMapper.insert(grtGuarBizRstRelData);

                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno",ctrLoanCont.getIqpSerno());
                    if("30".equals(guarWay)) {
                        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.queryGuarGuaranteeIsUnderLmt(queryModel);
                        if(CollectionUtils.nonEmpty(guarGuaranteeList)){
                            for (int i= 0; guarGuaranteeList.size()>i; i++){
                                GuarGuarantee guarGuarantee = guarGuaranteeList.get(i);

                                Map map = new HashMap();
                                map.put("subSerno",ctrLoanCont.getIqpSerno());
                                map.put("guarNo",guarGuarantee.getGuarantyId());
                                GuarBizRel guarBizRel = guarBizRelService.queryGuarBizRelDataBySernoAndGuarNo(map);
                                if(guarBizRel.getIsAddGuar() != null && "101".equals(guarBizRel.getIsAddGuar())){//主担保
                                    grtGuarContData.setAssureWay(guarGuarantee.getGuarantyType());
                                    grtGuarContMapper.updateByPrimaryKey(grtGuarContData);
                                }

                                //保证人id
                                String guarantyId = guarGuarantee.getGuarantyId();

                                String guarContNos = grtGuarContRelService.selectGuarContNosByGuarantyId(guarantyId);

                                if (StringUtils.nonEmpty(guarContNos)){
                                    log.info("保证人id【"+guarantyId+"】已与担保合同编号为【"+guarContNos+"】关联，无需在新增担保合同和担保人(物)的关系");
                                    continue;
                                }

                                GrtGuarContRel grtGuarContRel = new  GrtGuarContRel();
                                grtGuarContRel.setPkId(UUID.randomUUID().toString());
                                /** 担保合同编号 **/
                                grtGuarContRel.setGuarContNo(grtGuarBizRstRelData.getGuarContNo());
                                /** 押品统一编号 **/
                                grtGuarContRel.setGuarNo(guarGuarantee.getGuarantyId());
                                /** 合同编号 **/
                                grtGuarContRel.setContNo(grtGuarBizRstRelData.getContNo());
                                grtGuarContRel.setStatus("0");
                                grtGuarContRel.setOprType("01");
                                /** 主办人 **/
                                grtGuarContRel.setManagerId(iqpLoanApp.getManagerId());
                                /** 主办机构 **/
                                grtGuarContRel.setManagerBrId(iqpLoanApp.getManagerBrId());
                                /** 登记人 **/
                                grtGuarContRel.setInputId(iqpLoanApp.getInputId());
                                /** 登记机构 **/
                                grtGuarContRel.setInputBrId(iqpLoanApp.getInputBrId());
                                /** 登记日期 **/
                                grtGuarContRel.setInputDate(iqpLoanApp.getInputDate());
                                /** 最后修改人 **/
                                grtGuarContRel.setUpdId(iqpLoanApp.getUpdId());
                                /** 最后修改机构 **/
                                grtGuarContRel.setUpdBrId(iqpLoanApp.getUpdBrId());
                                /** 最后修改日期 **/
                                grtGuarContRel.setUpdDate(iqpLoanApp.getUpdDate());
                                grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                if(grtGuarContRelService.insert(grtGuarContRel) == 0 ){
                                    log.info("新增担保合同和担保人(物)的关系失败");
                                }
                            }
                        }
                    }else{
                        List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseDtoList = (List<GuarBizRelGuarBaseDto>) guarBaseInfoService.queryGuarInfoSell(queryModel);
                        if(guarBizRelGuarBaseDtoList != null){
                            for (int i= 0; guarBizRelGuarBaseDtoList.size()>i; i++){
                                GuarBizRelGuarBaseDto guarBizRelGuarBaseDto = guarBizRelGuarBaseDtoList.get(i);
                                GrtGuarContRel grtGuarContRel = new  GrtGuarContRel();
                                grtGuarContRel.setPkId(UUID.randomUUID().toString());
                                /** 担保合同编号 **/
                                grtGuarContRel.setGuarContNo(grtGuarBizRstRelData.getGuarContNo());
                                /** 押品统一编号 **/
                                grtGuarContRel.setGuarNo(guarBizRelGuarBaseDto.getGuarNo());
                                /** 合同编号 **/
                                grtGuarContRel.setContNo(grtGuarBizRstRelData.getContNo());
                                grtGuarContRel.setStatus("0");
                                grtGuarContRel.setOprType("01");
                                /** 主办人 **/
                                grtGuarContRel.setManagerId(iqpLoanApp.getManagerId());
                                /** 主办机构 **/
                                grtGuarContRel.setManagerBrId(iqpLoanApp.getManagerBrId());
                                /** 登记人 **/
                                grtGuarContRel.setInputId(iqpLoanApp.getInputId());
                                /** 登记机构 **/
                                grtGuarContRel.setInputBrId(iqpLoanApp.getInputBrId());
                                /** 登记日期 **/
                                grtGuarContRel.setInputDate(iqpLoanApp.getInputDate());
                                /** 最后修改人 **/
                                grtGuarContRel.setUpdId(iqpLoanApp.getUpdId());
                                /** 最后修改机构 **/
                                grtGuarContRel.setUpdBrId(iqpLoanApp.getUpdBrId());
                                /** 最后修改日期 **/
                                grtGuarContRel.setUpdDate(iqpLoanApp.getUpdDate());
                                grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                                if(grtGuarContRelService.insert(grtGuarContRel) == 0 ){
                                    log.info("新增担保合同和担保人(物)的关系失败");
                                }
                            }
                        }
                    }
                }
            }catch (Exception e){
                log.info("担保生成异常"+ e);
            }

            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
            if("10".equals(prdType)){//零售按揭
                if ("1".equals(iqpLoanApp.getIsOutstndTrdLmtAmt())) {
                    //这里是对提交时第三方占用做覆盖
                    //1. 向额度系统发送接口：校验额度
                    CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                    cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0009ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0009ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//合同编号
                    cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                    cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                    cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                    cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                    cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                    cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
                    cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
                    cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
                    cmisLmt0009ReqDto.setBizAttr("1");//交易属性
                    //cmisLmt0009ReqDto.setOrigiDealBizNo(iqpLoanApp.getIqpSerno());//原交易业务编号
                    //cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                    //cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                    //cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
                    cmisLmt0009ReqDto.setInputId(iqpLoanApp.getInputId());
                    cmisLmt0009ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                    cmisLmt0009ReqDto.setInputDate(iqpLoanApp.getInputDate());
                    cmisLmt0009ReqDto.setDealBizAmt(lmtCrdReplyInfo.getReplyAmt());//交易业务金额
                    cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                    cmisLmt0009ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());//合同起始日
                    cmisLmt0009ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());//合同到期日
                    List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                    CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                    cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                    cmisLmt0009OccRelListReqDto.setLmtSubNo(iqpLoanApp.getTdpAgrNo());//额度分项编号
                    cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getCvtCnyAmt());//占用总额(折人民币)
                    cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getCvtCnyAmt());//占用敞口(折人民币)
                    cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                    cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);

                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验请求报文："+ cmisLmt0009ReqDto.toString());
                    ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                    log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度校验返回报文："+ cmisLmt0009RespDto.toString());

                    String code =  cmisLmt0009RespDto.getData().getErrorCode();

                    if("0000".equals(code)){
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验成功！", iqpLoanApp.getIqpSerno());
                        //2. 向额度系统发送接口：占用额度
                        CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
                        cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                        cmisLmt0011ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                        cmisLmt0011ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//业务流水号
                        cmisLmt0011ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                        cmisLmt0011ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                        cmisLmt0011ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                        cmisLmt0011ReqDto.setBizAttr("1");//交易属性
                        cmisLmt0011ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                        cmisLmt0011ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                        cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
                        cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
                        cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
                        //cmisLmt0011ReqDto.setOrigiDealBizNo("");//原交易业务编号
                        //cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                        //cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                        //cmisLmt0011ReqDto.setOrigiBizAttr("1");//原交易属性
                        cmisLmt0011ReqDto.setDealBizAmt(lmtCrdReplyInfo.getReplyAmt());//交易业务金额
                        cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                        cmisLmt0011ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());//合同起始日
                        cmisLmt0011ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());//合同到期日
                        cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
                        cmisLmt0011ReqDto.setInputId(iqpLoanApp.getInputId());//登记人
                        cmisLmt0011ReqDto.setInputBrId(iqpLoanApp.getInputBrId());//登记机构
                        cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期

                        List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
                        CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
                        cmisLmt0011OccRelListDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                        cmisLmt0011OccRelListDto.setLmtSubNo(iqpLoanApp.getTdpAgrNo());//额度分项编号
                        cmisLmt0011OccRelListDto.setBizTotalAmt(lmtCrdReplyInfo.getReplyAmt());//占用总额(折人民币)
                        cmisLmt0011OccRelListDto.setBizSpacAmt(lmtCrdReplyInfo.getReplyAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDto.setBizTotalAmtCny(lmtCrdReplyInfo.getReplyAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDto.setBizSpacAmtCny(lmtCrdReplyInfo.getReplyAmt());//占用敞口(折人民币)
                        cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
                        cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);

                        log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用请求报文："+ cmisLmt0011ReqDto.toString());
                        ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
                        log.info("根据业务申请编号【"+ iqpLoanApp.getIqpSerno()+"】前往额度系统-第三方额度占用返回报文："+ cmisLmt0011RespDto.toString());
                        code =  cmisLmt0011RespDto.getData().getErrorCode();

                        if("0000".equals(code)){
                            log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用成功！", iqpLoanApp.getIqpSerno());
                            //调额度接口：更新第三方起始到期日
                            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
                            cmisLmt0029ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());
                            cmisLmt0029ReqDto.setOriginAccNo(iqpLoanApp.getIqpSerno());
                            cmisLmt0029ReqDto.setNewAccNo(contNo);
                            cmisLmt0029ReqDto.setStartDate(iqpLoanApp.getStartDate());
                            cmisLmt0029ReqDto.setEndDate(iqpLoanApp.getEndDate());
                            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-更新第三方起始到期日请求报文：" + cmisLmt0029ReqDto.toString());
                            ResultDto<CmisLmt0029RespDto> cmisLmt0029RespDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
                            log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-更新第三方起始到期日返回报文：" + cmisLmt0029RespDto.toString());

                            if (cmisLmt0029RespDto != null && SuccessEnum.SUCCESS.key.equals(cmisLmt0029RespDto.getData().getErrorCode())) {
                                log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日成功！", iqpLoanApp.getIqpSerno());
                            } else {
                                log.info("根据业务申请编号【{}】,前往额度系统更新第三方起始到期日失败！", iqpLoanApp.getIqpSerno());
                            }

                        } else {
                            log.info("根据业务申请编号【{}】,前往额度系统-第三方额度占用异常！", iqpLoanApp.getIqpSerno());
                            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度占用异常:"+cmisLmt0011RespDto.getData().getErrorMsg());
                        }
                    } else if(!"0000".equals(code) && !"70125".equals(code)){
                        log.info("根据业务申请编号【{}】,前往额度系统-第三方额度校验失败！", iqpLoanApp.getIqpSerno());
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "额度系统-第三方额度校验失败:"+cmisLmt0009RespDto.getData().getErrorMsg());
                    }
                }
            }



            //调外部接口同步第一次授信时间
            CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto = new CusUpdateInitLoanDateDto();
            cusUpdateInitLoanDateDto.setCusId(iqpLoanApp.getCusId());
            cusUpdateInitLoanDateDto.setInitLoanDate(LocalDate.now().toString());
            try {
                log.info("调用客户同步首次建立信贷关系时间开始，请求信息"+cusUpdateInitLoanDateDto.toString());
                ResultDto<Integer> integerResultDto = iCusClientService.updateInitLoanDate(cusUpdateInitLoanDateDto);
                log.info("调用客户同步首次建立信贷关系时间结束，响应信息"+integerResultDto);
            }catch (Exception e){
                log.info("调用客户同步首次建立信贷关系时间失败"+ e);
            }


            if(!"00".equals(iqpLoanApp.getGuarWay())){

                try {
                    log.info("调用信贷授信协议信息同步开始");
                    guarBusinessRelService.sendLmtinf("02",iqpLoanApp.getIqpSerno());
                    log.info("调用信贷授信协议信息同步结束");
                }catch (Exception e){
                    log.info("调用信贷授信协议信息同步失败"+ e);
                }


                try {
                    log.info("调用根据业务流水号同步押品与业务关联信息开始");
                    guarBusinessRelService.sendBuscon(iqpLoanApp.getIqpSerno(),"01");
                    log.info("调用根据业务流水号同步押品与业务关联信息结束");
                }catch (Exception e){
                    log.info("调用根据业务流水号同步押品与业务关联信息失败"+ e);
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        log.info("零售按揭类业务申请审批结束后业务处理逻辑结束【{}】", iqpLoanApp.getIqpSerno());
    }

    /**
     * @方法名称: tcontToSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据(贸易融资合同申请)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpLoanApp> tcontToSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_04);
        return iqpLoanAppMapper.selectByModel(model);
    }

    /**
     * @方法名称: tcontDoneSignlist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据(贸易融资合同申请)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<IqpLoanApp> tcontDoneSignlist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        model.getCondition().put("bizType", CmisCommonConstants.STD_BUSI_TYPE_04);
        return iqpLoanAppMapper.selectByModel(model);
    }

    /**
     * @param iqpSerno
     * @return void
     * @author wzy
     * @date 2021/5/4 20:44
     * @version 1.0.0
     * @desc 零售按揭类业务处理逻辑删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public Integer deleteIqpbyIqpSerno(String iqpSerno) {
        try {
                log.info("删除业务" + iqpSerno + "申请信息开始！");
                IqpLoanApp iqpLoanApp =  iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);

                if (iqpSerno == null || "".equals(iqpSerno)) {
                    throw new YuspException(EcbEnum.E_IQP_DELETE_PARAM_FAILED.key, EcbEnum.E_IQP_DELETE_PARAM_FAILED.value);
                }

                //优先更新子表的标志位，子表更新没有问题之后再更新主表的标志位
                //当前所有业务都是直接进行逻辑删除操作，没有针对业务做额外的处理，业务类型暂不使用
                String prdId = iqpLoanApp.getPrdId();

                Map delMap = new HashMap();
                delMap.put("iqpSerno", iqpSerno);
                //获取业务标识位，通过标识位进行逻辑删除操作
                delMap.put("oprType", CmisCommonConstants.OPR_TYPE_DELETE);
                int delCount = 0;
                //如果是按揭类，需要逻辑删除房产信息

                ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
                //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
                String catalogId = CfgPrdBasicinfoDto.getData().getCatalogId();
                if("C000700020001".equals(catalogId)){
                    //房产信息表
                    log.info("删除业务" + iqpSerno + "信息-房产信息表");
                    delCount = iqpHouseService.updateByParams(delMap);
                    if (delCount < 0) {
                        throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
                    }
                }
                //担保信息
                log.info("删除业务" + iqpSerno + "信息-押品与业务关系表");
                delCount = guarBizRelMapper.updateByParams(delMap);
                if (delCount < 0) {
                    throw new YuspException(EcbEnum.E_IQP_DELETE_FAILED.key, EcbEnum.E_IQP_DELETE_FAILED.value);
                }

                //判断当前申请状态是否为退回，修改为自行退出
                if (iqpLoanApp != null){
                    log.info("删除业务" + iqpSerno + "信息-业务主表");
                    if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(iqpLoanApp.getApproveStatus())){
                        //流程删除 修改为自行退出
                        log.info("流程删除==》bizId：",iqpLoanApp.getIqpSerno());
                        // 删除流程实例
                        workflowCoreClient.deleteByBizId(iqpLoanApp.getIqpSerno());
                        //恢复第三方
                        cancelIqpRetail(iqpLoanApp.getIqpSerno());
                        iqpLoanApp.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
                        iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                        iqpLoanAppMapper.updateByPrimaryKey(iqpLoanApp);
                    }else{
                        iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                        iqpLoanAppMapper.updateByPrimaryKey(iqpLoanApp);
                    }
                    log.info("删除业务" + iqpSerno + "信息成功");
                }
        } catch (Exception e) {
            throw BizException.error(null, "9999", e.getMessage());
        }
        return 0;
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectIqpLoanAppListData
     * @函数描述:重写查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */

    public List<IqpLoanApp> selectIqpLoanAppListData(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.setSort("inputDate desc");
        List<IqpLoanApp> list = iqpLoanAppMapper.selectIqpLoanAppListData(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectIqpLoanAppListData
     * @函数描述:重写查询对象历史列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */

    public List<IqpLoanApp> selectIqpLoanAppHisListData(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        queryModel.setSort("inputDate desc");
        List<IqpLoanApp> list = iqpLoanAppMapper.selectIqpLoanAppHisListData(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param
     * @return cn.com.yusys.yusp.domain.CtrLoanCont
     * @author hubp
     * @date 2021/5/11 16:39
     * @version 1.0.0
     * @desc    通过申请流水号，查询合同信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CtrLoanCont selectContByIqpSerno(String iqpSerno) {
        return ctrLoanContService.selectContByIqpSerno(iqpSerno);
    }

    /**
     * @方法名称：selectForLmtAccNo
     * @方法描述：查授信台账号对应的用信申请
     * @创建人：zhangming12
     * @创建时间：2021/5/17 21:19
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<IqpLoanApp> selectForLmtAccNo(String lmtAccNo){
        return iqpLoanAppMapper.selectForLmtAccNo(lmtAccNo);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public IqpLoanApp selectBySerno(String serno) {
        return iqpLoanAppMapper.selectBySerno(serno);
    }

    /**
     * @param iqpSerno
     * @return java.lang.Integer
     * @author hubp
     * @date 2021/5/27 14:46
     * @version 1.0.0
     * @desc 小微合同申请-合同信息级联删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto deleteContIqpInfo(String iqpSerno) {
        log.info("合同信息级联删除开始..................");
        try {
            if (null == iqpSerno || "".equals(iqpSerno)) {
                throw BizException.error(null, "9999", "数据异常！删除失败");
            }
            //解除担保合同关联
            grtGuarBizRstRelService.deleteBySerno(iqpSerno);
            //删除共有人信息
            lmtCobInfoService.deleteByIds(iqpSerno);
            int result = -1;
            // 删除合同申请主表信息
            result = iqpLoanAppMapper.deleteByPrimaryKey(iqpSerno);
            if (result != 1) {
                throw BizException.error(null, "9999", "合同申请表数据异常！删除失败");
            }
        } catch (YuspException e) {
            log.info("合同信息级联删除失败..................");
            return new ResultDto(null).code(e.getCode()).message(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            log.info("合同信息级联删除结束..................");
        }
        return new ResultDto().code("0").message("删除成功");
    }

    /**
     * @param cusId
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanApp>
     * @author hubp
     * @date 2021/5/29 20:40
     * @version 1.0.0
     * @desc 根据证件号码查询申请信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<IqpLoanApp> selectByCusId(String cusId) {
        return iqpLoanAppMapper.selectByCusId(cusId);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/6/4 10:07
     * @version 1.0.0
     * @desc 小微合同申请-额度批复信息查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto selectData(QueryModel queryModel) {
        log.info("额度批复信息查询开始..................");
        ResultDto<CmisLmt0015RespDto> resultDto = null;
        try {
            CmisLmt0015ReqDto cmisLmt0015ReqDto = new CmisLmt0015ReqDto();
            //放入参数
            String serno = (String) queryModel.getCondition().get("serno"); //交易流水号
            String instuCde = "001";   // 金融机构编号
            String cusId = (String) queryModel.getCondition().get("cusId");//客户ID
            String cusName = (String) queryModel.getCondition().get("cusName");//客户名称
            String queryType = "01"; // 查询类型 01-单一客户授信额度，02-集团客户授信额度
            cmisLmt0015ReqDto.setSerno(serno);
            cmisLmt0015ReqDto.setInstuCde(instuCde);
            if (StringUtils.nonBlank(cusId)) {
                cmisLmt0015ReqDto.setCusId(cusId);
            }
            if (StringUtils.nonBlank(cusName)) {
                cmisLmt0015ReqDto.setCusName(cusName);
            }
            cmisLmt0015ReqDto.setQueryType(queryType);
            cmisLmt0015ReqDto.setStartNum(queryModel.getPage());
            cmisLmt0015ReqDto.setPageCount(queryModel.getSize());
            log.info("额度批复信息查询..................开始调用接口查询");
            resultDto = cmisLmtClientService.cmisLmt0015(cmisLmt0015ReqDto);
        } catch (Exception e) {
            log.info("额度批复信息查询失败.................." + e);
            return new ResultDto(null).code(9999).message(e.getMessage());
        } finally {
            log.info("额度批复信息查询结束..................");
        }
        return resultDto;
    }


    public Integer logicDelete(IqpLoanApp iqpLoanApp) {
        if (CmisCommonConstants.WF_STATUS_000.equals(iqpLoanApp.getApproveStatus())) {
            iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);

        } else if (CmisCommonConstants.WF_STATUS_992.equals(iqpLoanApp.getApproveStatus())) {
            iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);
            iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);
            ResultDto<ResultMessageDto> dto = workflowCoreClient.deleteByBizId(iqpLoanApp.getIqpSerno());
            //流程打回，恢复占额
            if (iqpLoanApp.getLmtAccNo() != null && !"".equals(iqpLoanApp.getLmtAccNo())) {
                String guarMode = iqpLoanApp.getGuarWay();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                } else {
                    recoverSpacAmtCny = iqpLoanApp.getCvtCnyAmt();
                    recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                String code = resultDto.getData().getErrorCode();
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
        return iqpLoanAppMapper.updateByPrimaryKey(iqpLoanApp);
    }

    /**
     * @param cfgTfRateQueryDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/29 21:09
     * @version 1.0.0
     * @desc 获取币种汇率
     * @修改历史: 修改时间: 2021年6月16日21:51:28    修改人员：hubp    修改原因：修改部分参数
     */
    public Map getCfgTfRate(CfgTfRateQueryDto cfgTfRateQueryDto) {

        Map rtnData = new HashMap();
        String rate="";
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            ResultDto<CfgTfRateDto>  resultDto= iCmisCfgClientService.queryCfgTfRate(cfgTfRateQueryDto);
            if(resultDto.getData().getRate()==null){
                rtnCode = EcbEnum.ECB020007.key;
                rtnMsg = EcbEnum.ECB020007.value;
                return rtnData;
            }
            rate=resultDto.getData().getRate().toString();

        } catch (Exception e) {
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
            log.info("申请汇率查询报错{}", e.getMessage());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("rate", rate);
        }
        return rtnData;
    }

    /**
     * @方法名称: riskItem0007
     * @方法描述: 白领易贷通申请校验
     * @参数与返回说明:
     * @算法描述:
     * 当前产品为“白领易贷通”时触发以下拦截规则：
     * （1）若当前客户已经存在审批中或退回的白领易贷通业务申请，则拦截
     * （2）若当前客户存在在途或生效的白领易贷通合同，则拦截
     * （3）若当前客户已经存在未结清的白领易贷通借据，则拦截
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:02:52
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0007(String serno) {
        log.info("白领易贷通申请校验开始*******************业务流水号：【{}】", serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(serno);
        if (Objects.isNull(iqpLoanApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
            return riskResultDto;
        }
        // 产品编号白领易贷通:022028
        if ("022028".equals(iqpLoanApp.getPrdId())) {
            int result = 0;
            //若当前客户已经存在审批中或退回的白领易贷通业务申请，则拦截
            QueryModel model1 = new QueryModel();
            model1.getCondition().put("cusId", iqpLoanApp.getCusId());
            model1.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            model1.getCondition().put("applyExistsStatus", "111,991,992");
            model1.getCondition().put("belgLine", "02");
            model1.getCondition().put("prdId", "022028");
            List<IqpLoanApp> iqpLoanAppList = iqpLoanAppMapper.selectByModel(model1);
            if (CollectionUtils.nonEmpty(iqpLoanAppList)) {
                for (IqpLoanApp iqpLoanAppt : iqpLoanAppList) {
                    if (serno.equals(iqpLoanAppt.getIqpSerno())) {
                        if("111".equals(iqpLoanAppt.getApproveStatus())){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00701);
                            return riskResultDto;
                        }
                    }else{
                        if("111".equals(iqpLoanAppt.getApproveStatus())
                                || "991".equals(iqpLoanAppt.getApproveStatus())
                                || "992".equals(iqpLoanAppt.getApproveStatus())){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00701);
                            return riskResultDto;
                        }
                    }
                }
            }

/*          合同状态	100 	未生效
            合同状态	200 	生效
            合同状态	500 	中止
            合同状态	600 	注销
            合同状态	700 	撤回
            合同状态	800	    作废*/
            //若当前客户存在在途或生效的白领易贷通合同，则拦截
            QueryModel model2 = new QueryModel();
            model2.getCondition().put("cusId", iqpLoanApp.getCusId());
            model2.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            model2.getCondition().put("contExistsStatus", "100,200");
            model2.getCondition().put("belgLine", "02");
            model2.getCondition().put("prdId", "022028");
            List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.selectByModel(model2);
            if (ctrLoanContList != null && ctrLoanContList.size() > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00702);
                return riskResultDto;
            }

/*
            0	已关闭
            1	正常
            2	逾期
            3	呆滞
            4	呆账
            5	已核销
            6	未出账
            7	作废
*/
            //若当前客户已经存在未结清的白领易贷通借据，则拦截
            QueryModel model3 = new QueryModel();
            model3.getCondition().put("cusId", iqpLoanApp.getCusId());
            model3.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            model3.getCondition().put("accStatus", "1");
            model3.getCondition().put("belgLine", "02");
            model3.getCondition().put("prdId", "022028");
            List<AccLoan> accLoanList = accLoanMapper.selectByModel(model3);
            if (accLoanList != null && accLoanList.size() > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00703);
                return riskResultDto;
            }
        }
        log.info("白领易贷通申请校验结束*******************业务流水号：【{}】", serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/6/23 10:54
     * @version 1.0.0
     * @desc 授信申请担保信息校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0001(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = (String) queryModel.getCondition().get("bizId");
        String bizType = (String) queryModel.getCondition().get("bizType");
        log.info("授信申请担保信息校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        if (StringUtils.isEmpty(serno) || StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 初始化担保类型
        String guarWay = StringUtils.EMPTY;
        // 初始所属条线
        String belgLine = StringUtils.EMPTY;
        // 判断是否零售
        if ("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)) {
            log.info("授信申请担保信息校验开始*******************业务流水号：【{}】，流程类型：【{}】，开始查询合同授信信息",serno,bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(serno);
            // 判定合同是否为空
            if(Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            } else {
                belgLine = iqpLoanApp.getBelgLine();
                guarWay = iqpLoanApp.getGuarWay();
                riskResultDto = this.riskItem0001(guarWay,serno,null,belgLine,iqpLoanApp.getAppAmt());
                if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                    if(Objects.nonNull(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            }
        } else if (bizType.contains("SX0") || bizType.contains("SGC") || bizType.contains("DHC")) {
            Map<String,String> map = new HashMap<>();
            map.put("serno",serno);
            map.put("oprType","01");
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByParams(map);
            if(Objects.isNull(lmtAppSubList) || lmtAppSubList.size() == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                return riskResultDto;
            }
            belgLine = "03";
            for(LmtAppSub lmtAppSub:lmtAppSubList){
                if(CmisCommonConstants.STD_ZB_YES_NO_0.equals(lmtAppSub.getIsPreLmt())){
                    guarWay = lmtAppSub.getGuarMode();
                    riskResultDto = this.riskItem0001(guarWay,lmtAppSub.getSubSerno(),lmtAppSub.getSubSerno(),belgLine,lmtAppSub.getLmtAmt());
                    if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                        if(Objects.nonNull(riskResultDto.getRiskResultType())) {
                            return riskResultDto;
                        }
                    }
                }
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_000116);
            return riskResultDto;
        }
        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param guarWay, serno, belgLine, appAmt
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/7/14 21:41
     * @version 1.0.0
     * @desc    riskItem0001风险拦截私有方法
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto riskItem0001(String guarWay,String serno,String subSerno,String belgLine,BigDecimal appAmt){
        log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，开始判定担保方式：【{}】",serno,guarWay);
        /*
        00	信用 ；10	抵押； 20	质押； 21	低风险质押； 30	保证； 40	全额保证金； 60	低风险
        */
        // 零售业务申请/小微授信申请/对公授信分项的担保方式不等于信用时，其下必须关联与其匹配的担保品，且担保品不能重复；
        RiskResultDto riskResultDto = new RiskResultDto();
        // 授信金额（如果为null，则设置为0）
        appAmt = Objects.isNull(appAmt) ? BigDecimal.ZERO :appAmt;


        //（1）对公授信申请时，保证方式=单人担保，保证金额必须大于等于对应的授信分项金额；保证方式=多人分保，保证人数量大于1，
        // 且保证人保证金额之和必须大于等于对应的授信分项金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的授信分项金额；
        //（2）零售业务申请时，保证方式=单人担保，保证金额必须大于等于对应的申请金额；保证方式=多人分保，保证人数量大于1，
        // 且保证人保证金额之和必须大于等于对应的申请金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的申请金额；
        //（3）小微授信申请时，保证方式=单人担保，保证金额必须大于等于对应的申请金额；保证方式=多人分保，保证人数量大于1，grt_guar_biz_rst_rel
        // 且保证人保证金额之和必须大于等于对应的申请金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的申请金额。
        // 押品，保证人和业务的关系
        // 获取
        // 对公授信
        // 初始化担保人集合
        List<GuarBizRelGuaranteeDto> assureMode0401List1 = new ArrayList<>();
        List<GuarBizRelGuaranteeDto> assureMode0402List1 = new ArrayList<>();
        List<GuarBizRelGuaranteeDto> assureMode0403List1 = new ArrayList<>();
        List<GuarBizRelGuaranteeDto> assureMode0404List1 = new ArrayList<>();
        List<GuarBizRelGuaranteeDto> assureMode0405List1 = new ArrayList<>();
        List<GuarBizRelGuaranteeDto> assureMode0406List1 = new ArrayList<>();

        if("03".equals(belgLine)) {
            QueryModel model1 = new QueryModel();
            model1.getCondition().put("serno", serno);
            model1.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
            log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，查询保证担保信息", serno);
            // 查询保证担保信息
            List<GuarBizRelGuaranteeDto> guarBizRelGuaranteeDtoList1 = guarGuaranteeMapper.selectByIqpSernoModel(model1);
            for (GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : guarBizRelGuaranteeDtoList1) {
                if (CmisCommonConstants.STD_GUAR_TYPE_101.equals(guarBizRelGuaranteeDto.getIsAddGuar())) {
                    if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0401, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0401List1.add(guarBizRelGuaranteeDto);
                    } else if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0402, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0402List1.add(guarBizRelGuaranteeDto);
                    } else if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0403, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0403List1.add(guarBizRelGuaranteeDto);
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00112);
                        return riskResultDto;
                    }
                } else if (CmisCommonConstants.STD_GUAR_TYPE_102.equals(guarBizRelGuaranteeDto.getIsAddGuar())) {
                    if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0401, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0404List1.add(guarBizRelGuaranteeDto);
                    } else if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0402, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0405List1.add(guarBizRelGuaranteeDto);
                    } else if (Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0403, guarBizRelGuaranteeDto.getGuarantyType())) {
                        assureMode0406List1.add(guarBizRelGuaranteeDto);
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00112);
                        return riskResultDto;
                    }
                }
            }
            // 保证=单人担保  主担保
            if (CollectionUtils.nonEmpty(assureMode0401List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=单人担保", serno);
                if (assureMode0401List1.size() > 1 || assureMode0402List1.size() > 0 || assureMode0403List1.size() > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00107);
                    return riskResultDto;
                }
            }
            if (CollectionUtils.nonEmpty(assureMode0402List1) && CollectionUtils.nonEmpty(assureMode0403List1)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00208);
                return riskResultDto;
            }
            // 保证=多人分保   主担保
            if (CollectionUtils.nonEmpty(assureMode0402List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=多人分保", serno);
                if (assureMode0402List1.size() <= 1) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00108);
                    return riskResultDto;
                }
            }
            // 保证方式=多人联保   主担保
            if (CollectionUtils.nonEmpty(assureMode0403List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证方式=多人联保", serno);
                if (assureMode0403List1.size() <= 1) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00110);
                    return riskResultDto;
                }
            }


            // 保证=单人担保  追加担保
            if (CollectionUtils.nonEmpty(assureMode0404List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=单人担保", serno);
                if (assureMode0404List1.size() > 1 || assureMode0405List1.size() > 0 || assureMode0406List1.size() > 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00207);
                    return riskResultDto;
                }
            }
            if (CollectionUtils.nonEmpty(assureMode0405List1) && CollectionUtils.nonEmpty(assureMode0406List1)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00209);
                return riskResultDto;
            }
            // 保证=多人分保   追加担保
            if (CollectionUtils.nonEmpty(assureMode0405List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=多人分保", serno);
                if (assureMode0405List1.size() <= 1) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00108);
                    return riskResultDto;
                }
            }
            // 保证方式=多人联保   追加担保
            if (CollectionUtils.nonEmpty(assureMode0406List1)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证方式=多人联保", serno);
                if (assureMode0406List1.size() <= 1) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00110);
                    return riskResultDto;
                }
            }
        }
        if (!CmisCommonConstants.GUAR_MODE_00.equals(guarWay) && !CmisCommonConstants.GUAR_MODE_60.equals(guarWay)) {
            log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，查询担保品信息（抵质押物信息以及保证担保信息）",serno);
            QueryModel model = new QueryModel();
            model.getCondition().put("serno", serno);
            model.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
            // 查询担保品信息（抵质押物信息以及保证担保信息）
            List<GuarBizRel> guarBizRelList = guarBizRelService.selectByModel(model);
            if (Objects.isNull(guarBizRelList) || guarBizRelList.size() == 0) {
                // 其下必须关联与其匹配的担保品
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00102);
                return riskResultDto;
            } else {
                // 担保品不能重复；
                Set<String> set = new HashSet<>();
                for (int i = 0; i < guarBizRelList.size(); i++) {
                    String guarNo = guarBizRelList.get(i).getGuarNo();
                    set.add(guarNo);
                }
                if (guarBizRelList.size() > set.size()) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00103);
                    return riskResultDto;
                }
            }
            //担保方式=抵押 或者 质押：
            if (CmisCommonConstants.GUAR_MODE_10.equals(guarWay) || CmisCommonConstants.GUAR_MODE_20.equals(guarWay)) {
                // 担保物评估价值金额之和
                BigDecimal totalAmt = BigDecimal.ZERO;
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，查询抵质押物信息",serno);
                // 查询抵质押物信息
                List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseList = guarBaseInfoMapper.selectByIqpSernoModel(model);
                // 判定抵质押是否为空
                if(Objects.isNull(guarBizRelGuarBaseList) || guarBizRelGuarBaseList.size() == 0) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0008);
                    return riskResultDto;
                }
                boolean isMainGuar = false;
                for (GuarBizRelGuarBaseDto guarBizRelGuarBaseDto : guarBizRelGuarBaseList) {
                    if(CmisCommonConstants.STD_GUAR_TYPE_101.equals(guarBizRelGuarBaseDto.getIsAddGuar())){
                        isMainGuar = true;
                        totalAmt = totalAmt.add(Objects.nonNull(guarBizRelGuarBaseDto.getEvalAmt()) ? new BigDecimal(guarBizRelGuarBaseDto.getEvalAmt()) : new BigDecimal("0"));
                    }
                }
                // （1）对公授信申请时，担保物评估价值金额之和必须大于等于对应的授信分项金额；
                // （2）零售业务申请时，担保物评估价值金额之和必须大于等于申请金额；
                // （3）小微授信申请时，担保物评估价值金额之和必须大于等于申请金额。
                if(CmisBizConstants.BELGLINE_XW_01.equals(belgLine) || CmisBizConstants.BELGLINE_LS_02.equals(belgLine)) {
                    if (totalAmt.compareTo(appAmt) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00104);
                        return riskResultDto;
                    }
                }
                // 对公授信
                if(CmisBizConstants.BELGLINE_DG_03.equals(belgLine)) {
                    if(!isMainGuar){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00125);
                        return riskResultDto;
                    }
                    if (totalAmt.compareTo(appAmt) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00105);
                        return riskResultDto;
                    }
                }
            } else if (CmisCommonConstants.GUAR_MODE_30.equals(guarWay)) {
                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，进入保证担保校验", serno);
                // 担保方式=保证：
                //（1）对公授信申请时，保证方式=单人担保，保证金额必须大于等于对应的授信分项金额；保证方式=多人分保，保证人数量大于1，
                // 且保证人保证金额之和必须大于等于对应的授信分项金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的授信分项金额；
                //（2）零售业务申请时，保证方式=单人担保，保证金额必须大于等于对应的申请金额；保证方式=多人分保，保证人数量大于1，
                // 且保证人保证金额之和必须大于等于对应的申请金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的申请金额；
                //（3）小微授信申请时，保证方式=单人担保，保证金额必须大于等于对应的申请金额；保证方式=多人分保，保证人数量大于1，grt_guar_biz_rst_rel
                // 且保证人保证金额之和必须大于等于对应的申请金额；保证方式=多人联保，保证人数量大于1，且每个保证人保证金额必须大于等于对应的申请金额。
                // 押品，保证人和业务的关系
                // 获取
                // 对公授信
                // 担保物评估价值金额之和
                BigDecimal totalAmt = BigDecimal.ZERO;
                // 担保方式初始化
                String assureMode;
                // 初始化担保人集合
                List<GuarBizRelGuaranteeDto> assureMode0401List = new ArrayList<>();
                List<GuarBizRelGuaranteeDto> assureMode0402List = new ArrayList<>();
                List<GuarBizRelGuaranteeDto> assureMode0403List = new ArrayList<>();
                List<GuarBizRelGuaranteeDto> assureMode0404List = new ArrayList<>();
                List<GuarBizRelGuaranteeDto> assureMode0405List = new ArrayList<>();
                List<GuarBizRelGuaranteeDto> assureMode0406List = new ArrayList<>();

                log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，查询保证担保信息", serno);
                // 查询保证担保信息
                List<GuarBizRelGuaranteeDto> guarBizRelGuaranteeDtoList = guarGuaranteeMapper.selectByIqpSernoModel(model);
                // 判定保证担保人信息是否为空
                if (CollectionUtils.isEmpty(guarBizRelGuaranteeDtoList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0009);
                    return riskResultDto;
                }
                boolean isMainGuar = false;
                for(GuarBizRelGuaranteeDto guarBizRelGuaranteeDto : guarBizRelGuaranteeDtoList) {
                    if(CmisCommonConstants.STD_GUAR_TYPE_101.equals(guarBizRelGuaranteeDto.getIsAddGuar())){
                        isMainGuar = true;
                        if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0401,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0401List.add(guarBizRelGuaranteeDto);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0402,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0402List.add(guarBizRelGuaranteeDto);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0403,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0403List.add(guarBizRelGuaranteeDto);
                        }else{
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00112);
                            return riskResultDto;
                        }
                    }else if (CmisCommonConstants.STD_GUAR_TYPE_102.equals(guarBizRelGuaranteeDto.getIsAddGuar())){
                        if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0401,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0404List.add(guarBizRelGuaranteeDto);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0402,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0405List.add(guarBizRelGuaranteeDto);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0403,guarBizRelGuaranteeDto.getGuarantyType())) {
                            assureMode0406List.add(guarBizRelGuaranteeDto);
                        }else{
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00112);
                            return riskResultDto;
                        }
                    }
                }
                if(!isMainGuar){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00125);
                    return riskResultDto;
                }

                // 保证=单人担保  主担保
                if(CollectionUtils.nonEmpty(assureMode0401List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=单人担保",serno);
                    if(assureMode0401List.size() > 1 || assureMode0402List.size() > 0 || assureMode0403List.size() > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00107);
                        return riskResultDto;
                    }else {
                        if(appAmt.compareTo(StringUtils.nonEmpty(assureMode0401List.get(0).getGuarAmt()) ? new BigDecimal(assureMode0401List.get(0).getGuarAmt()) :
                                new BigDecimal("0")) > 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00106);
                            return riskResultDto;
                        }
                    }
                }
                if(CollectionUtils.nonEmpty(assureMode0402List) && CollectionUtils.nonEmpty(assureMode0403List)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00208);
                    return riskResultDto;
                }
                // 保证=多人分保   主担保
                if(CollectionUtils.nonEmpty(assureMode0402List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=多人分保",serno);
                    if(assureMode0402List.size() <= 1) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00108);
                        return riskResultDto;
                    }else {
                        for(GuarBizRelGuaranteeDto guarBiz0402 : assureMode0402List) {
                            totalAmt = totalAmt.add(StringUtils.nonEmpty(guarBiz0402.getGuarAmt()) ? new BigDecimal(guarBiz0402.getGuarAmt()) :
                                    new BigDecimal("0"));
                        }
                        if(appAmt.compareTo(totalAmt) > 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00109);
                            return riskResultDto;
                        }
                    }
                }
                // 保证方式=多人联保   主担保
                if(CollectionUtils.nonEmpty(assureMode0403List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证方式=多人联保",serno);
                    if(assureMode0403List.size() <= 1) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00110);
                        return riskResultDto;
                    }else {
                        for(GuarBizRelGuaranteeDto guarBiz0403 : assureMode0403List) {
                            if(appAmt.compareTo(StringUtils.nonEmpty(guarBiz0403.getGuarAmt()) ? new BigDecimal(guarBiz0403.getGuarAmt()) :
                                    new BigDecimal("0")) > 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00111);
                                return riskResultDto;
                            }
                        }
                    }
                }


                // 保证=单人担保  追加担保
                if(CollectionUtils.nonEmpty(assureMode0404List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=单人担保",serno);
                    if(assureMode0404List.size() > 1 || assureMode0405List.size() > 0 || assureMode0406List.size() > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00207);
                        return riskResultDto;
                    }
                }
                if(CollectionUtils.nonEmpty(assureMode0405List) && CollectionUtils.nonEmpty(assureMode0406List)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00209);
                    return riskResultDto;
                }
                // 保证=多人分保   追加担保
                if(CollectionUtils.nonEmpty(assureMode0405List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证=多人分保",serno);
                    if(assureMode0405List.size() <= 1) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00108);
                        return riskResultDto;
                    }
                }
                // 保证方式=多人联保   追加担保
                if(CollectionUtils.nonEmpty(assureMode0406List)) {
                    log.info("授信申请担保信息校验执行中*******************业务流水号：【{}】，保证方式=多人联保",serno);
                    if(assureMode0406List.size() <= 1) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00110);
                        return riskResultDto;
                    }
                }
            }
        }
        // 担保方式=低风险时，低风险分项下（一般低风险额度），客户是否在关联方名单内，如果是，则必须有担保品或者保证金比例是100%，如果不在关联方名单内，则可以没有担保品。
        if(CmisCommonConstants.GUAR_MODE_60.equals(guarWay)) {
            List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(serno);
            if(CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                for(LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                    if(Objects.equals("12010103",lmtAppSubPrd.getLmtBizType())) {
                        CmisCus0020ReqDto reqDto = new CmisCus0020ReqDto();
                        reqDto.setCusId(lmtAppSubPrd.getCusId());
                        // 根据客户号查询关联方名单信息记录数
                        ResultDto<CmisCus0020RespDto> cmisCus0020RespDtoResultDto = cmisCusClientService.cmiscus0020(reqDto);
                        //请求成功
                        if (SuccessEnum.CMIS_SUCCSESS.key.equals(cmisCus0020RespDtoResultDto.getCode())) {
                            CmisCus0020RespDto data = cmisCus0020RespDtoResultDto.getData();
                            Integer count = data.getCount();
                            if ((Objects.nonNull(count) ? count : 0) > 0) {
                                QueryModel model2 = new QueryModel();
                                model2.getCondition().put("serno", serno);
                                model2.getCondition().put("oprType", CmisBizConstants.OPR_TYPE_01);
                                // 查询抵质押物信息
                                List<GuarBizRelGuarBaseDto> guarBaseList = guarBaseInfoMapper.selectByIqpSernoModel(model2);
                                if(new BigDecimal("1").compareTo(Objects.nonNull(lmtAppSubPrd.getBailPreRate()) ? lmtAppSubPrd.getBailPreRate() : BigDecimal.ZERO) != 0
                                        && CollectionUtils.isEmpty(guarBaseList)) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00115);
                                    return riskResultDto;
                                }
                            }
                        } else {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00114);
                            return riskResultDto;
                        }
                    }
                }
            }

        }

        return  riskResultDto;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/6/23 20:26
     * @version 1.0.0
     * @desc 若借款人为个人客户且担保方式=信用，触发拦截：查询人力资源系统，当前借款人及其家庭成员是否存在，存在则拦截
     * ---      解释说明：人力资源系统有家庭成员表；信贷系统的家庭成员信息不纳入规则校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0003(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString(); // 获取流程编号
        String guarMode = StringUtils.EMPTY;
        String cusId = StringUtils.EMPTY;
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno) || StringUtils.isEmpty(bizType)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 判断是哪个场景触发
        if ("XW001".equals(bizType)) {
            // 小微进来的，查询小微调查结论表
            LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(serno);
            LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(serno);
            guarMode = lmtSurveyConInfo.getGuarMode();
            cusId = basicInfo.getCusId();
        } else if ("LS001".equals(bizType) || "XW003".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)) {
            // 零售业务申请进来的，查业务申请表
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByIqpSerno(serno);
            guarMode = iqpLoanApp.getGuarWay();
            cusId = iqpLoanApp.getCusId();
        } else if ("LS004".equals(bizType) || "LS008".equals(bizType)) {
            // 零售合同申请进来的，查合同表
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            guarMode = ctrLoanCont.getGuarWay();
            cusId = ctrLoanCont.getCusId();
        } else if ("LS006".equals(bizType) || "LS005".equals(bizType) || Objects.equals("YX011", bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            // 零售（对公贷款出账申请）查放款表
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            guarMode = pvpLoanApp.getGuarMode();
            cusId = pvpLoanApp.getCusId();
        } else if (Objects.equals("YX001", bizType)) {
            // 最高额授信协议
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.queryIqpHighAmtAgrAppDataBySerno(serno);
            guarMode = iqpHighAmtAgrApp.getGuarMode();
            cusId = iqpHighAmtAgrApp.getCusId();
        } else if (Objects.equals("YX002", bizType)) {
            // 普通贷款合同申请
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByIqpSerno(serno);
            guarMode = iqpLoanApp.getGuarWay();
            cusId = iqpLoanApp.getCusId();
        } else if ("XW002".equals(bizType)) {
            // 小微放款进来的，查放款表
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            guarMode = pvpLoanApp.getGuarMode();
            cusId = pvpLoanApp.getCusId();
        } else if (bizType.contains("SX0")) {
            LmtApp lmtApp = lmtAppService.selectBySerno(serno);
            LmtGrpApp lmtGrpApp = null;
            if (Objects.isNull(lmtApp)) {
                lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(serno);
                if (Objects.nonNull(lmtGrpApp)) {
                    cusId = lmtGrpApp.getGrpCusId();
                }
            } else {
                cusId = lmtApp.getCusId();
                // 获取担保方式
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.queryLmtAppSubBySerno(serno);
                if (CollectionUtils.nonEmpty(lmtAppSubList)) {
                    for (LmtAppSub lmtAppSub : lmtAppSubList) {
                        String lmtAppGuarMode = lmtAppSub.getGuarMode();
                        if (CmisCommonConstants.GUAR_MODE_00.equals(lmtAppGuarMode)){
                            guarMode = lmtAppSub.getGuarMode();
                            log.info("授信申请担保方式存在信用担保方式：【{}】",guarMode);
                        }
                    }
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0023);
                    return riskResultDto;
                }

            }
            if (Objects.isNull(lmtApp) && Objects.isNull(lmtGrpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        try {

            // 调用客户接口，查询人力资源信息
            Xdkh0007DataReqDto xdkh0007ReqDto = new Xdkh0007DataReqDto();
            xdkh0007ReqDto.setCusId(cusId);
            ResultDto resultDto = dscmsCusClientService.xdkh0007(xdkh0007ReqDto);
            Xdkh0007DataRespDto xdkh0007RespDto = (Xdkh0007DataRespDto) resultDto.getData();
            log.info("人力资源系统信息【{}】，【{}】", xdkh0007RespDto.getIsBankStaff(),xdkh0007RespDto.getIsBankStaffFamily());
            if (CmisCommonConstants.STD_IS_FLAG_Y.equals(xdkh0007RespDto.getIsBankStaff()) ||
                    CmisCommonConstants.STD_IS_FLAG_Y.equals(xdkh0007RespDto.getIsBankStaffFamily())) {
                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                if (Objects.isNull(cusBaseClientDto)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0108);
                    return riskResultDto;
                }
                if ("1".equals(cusBaseClientDto.getCusCatalog()) && CmisCommonConstants.GUAR_MODE_00.equals(guarMode)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0301);
                    return riskResultDto;
                }
            }
        } catch (Exception e) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0304);
            return riskResultDto;
        }
        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0014
     * @方法描述:白领易贷通产品校验
     * @参数与返回说明:
     * @算法描述:
     * 授信申请提交时点触发：
     * （1）宽限期必须为0，否则拦截
     * （2）只能是循环授信（即合同类型为最高额合同），否则拦截
     * （3）若担保方式=质押，押品不能是理财产品，否则拦截
     * @创建人: shenli
     * @创建时间: 2021-6-22 22:26:07
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0014(String serno) {
        log.info("白领易贷通产品校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if(StringUtils.isBlank(serno)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(serno);
        if( ! Objects.isNull(iqpLoanApp) &&"022028".equals(iqpLoanApp.getPrdId())) {
            //宽限期必须为0，否则拦截
            // int graper = Integer.parseInt(iqpLoanApp.getGraper());//宽限期
            // if(graper != 0){
            //     riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            //     riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001401);
            //     return riskResultDto;
            // }

            //只能是循环授信（即合同类型为最高额合同），否则拦截
            String contType = iqpLoanApp.getContType();//合同类型
            if(!"2".equals(contType)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001402);
                return riskResultDto;

            }
            //若担保方式=质押，押品不能是理财产品，否则拦截
            String guarWay = iqpLoanApp.getGuarWay();//担保方式
            if("20".equals(guarWay) || "21".equals(guarWay) || "22".equals(guarWay)){
                QueryModel model = new QueryModel();
                model.getCondition().put("serno", iqpLoanApp.getIqpSerno());
                List<GuarBizRel>  guarBizRelList = guarBizRelMapper.selectByModel(model);
                if(CollectionUtils.nonEmpty(guarBizRelList)){
                    for(GuarBizRel guarBizRel : guarBizRelList){
                        String guarNo = guarBizRel.getGuarNo();
                        QueryModel queryModel = new QueryModel();
                        queryModel.getCondition().put("guarNo", guarNo);
                        List<GuarBaseInfo> guarBaseInfoList =  guarBaseInfoMapper.selectByModel(queryModel);
                        if(CollectionUtils.nonEmpty(guarBaseInfoList)) {
                            GuarBaseInfo guarBaseInfo = guarBaseInfoList.get(0);
                            String grtFlag = guarBaseInfo.getGuarTypeCd();
                            if(grtFlag.startsWith("ZY9901")){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_001403);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
        }
        log.info("白领易贷通产品校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: riskItem0030
     * @方法描述: 对公授信在途校验
     * @参数与返回说明:
     * @算法描述: 当合同申请对应的客户存在在途的授信申报/授信变更/预授信细化/授信批复
     * 变更/授信额度调剂时进行风险拦截
     * @创建人: cainingbo
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public IqpLoanApp selectByIqpSerno(String iqpSerno) {
        return iqpLoanAppMapper.selectByIqpSerno(iqpSerno);
    }

    /**
     * @函数名称:getAllIqpByInputId
     * @函数描述:根据客户经理工号查询合同申请数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> getAllIqpByInputId(QueryModel queryModel) {
        return iqpLoanAppMapper.getAllIqpByInputId(queryModel);
    }

    /**
     * @函数名称:getAllIqpByInputId
     * @函数描述:selectfdccount
     * @参数与返回说明: 根据流水号查询房地产抵押的方式条数
     * @算法描述:
     * @修改人:
     */
    public Integer selectFdcCount(String iqpSerno) {
        return iqpLoanAppMapper.selectFdcCount(iqpSerno);
    }


    /**
     * @创建人 shenli
     * @创建时间 2021-6-30 10:27:54
     * @注释 还款测算-第一个月应还款
     */
    public ResultDto selectcalculate(IqpLoanApp iqpLoanApp) {
        log.info("贷款还款计划试算开始..................");
        Ln3110RespDto ln3110RespDto = null;
        try {
            Ln3110ReqDto ln3110ReqDto = new Ln3110ReqDto();//req对象

            //daikjine	贷款金额
            ln3110ReqDto.setDaikjine(iqpLoanApp.getAppAmt());
            //sftsdkbz	是否特殊贷款标志  1是 0否
            ln3110ReqDto.setSftsdkbz("1");
            //tsdkjxqs	特殊贷款计息总期数

            //jixiguiz	计息规则
//            01--对月对日(30/365,366)
//            02--标准/365(30/365)
//            10--实际/标准(31/360)
//            11--实际/实际(31/365,366)
//            12--实际/365(31/365)
            ln3110ReqDto.setJixiguiz("01");
            //zhchlilv	正常利率
            ln3110ReqDto.setZhchlilv(iqpLoanApp.getExecRateYear().setScale(7,BigDecimal.ROUND_HALF_UP));
            //hkzhouqi	还款周期
            ln3110ReqDto.setHkzhouqi("1MA21");
            //hkqixian	还款期限(月)
            ln3110ReqDto.setHkqixian(iqpLoanApp.getAppTerm().toString());
            String startDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            String endDate = DateUtils.addMonth(startDate, "yyyy-MM-dd", iqpLoanApp.getAppTerm().intValue());
            //qixiriqi	起息日期 TODO 存疑
            ln3110ReqDto.setQixiriqi(startDate.replaceAll("-", ""));
            //daoqriqi	到期日期 TODO 存疑
            ln3110ReqDto.setDaoqriqi(endDate.replaceAll("-",""));
            String repayMode = iqpLoanApp.getRepayMode();
            if (repayMode.equals("A002")){
                repayMode="3";
            }else
            if (repayMode.equals("A003")){
                repayMode="4";
            }else
            if (repayMode.equals("A009")){
                repayMode="1";
            }else{
                // TODO 其他的不支持计算 直接返回
                return new ResultDto().code(-1).message("暂不支持该种还款方式的还款计划试算");
            }
            //我们的字典项 [{"key":"A001","value":"按期付息到期还本"},{"key":"A002","value":"等额本息"},
            // {"key":"A003","value":"等额本金"},{"key":"A009","value":"利随本清"},
            // {"key":"A012","value":"按226比例还款"},{"key":"A013","value":"按月还息按季还本"},
            // {"key":"A014","value":"按月还息按半年还本"},{"key":"A015","value":"按月还息,按年还本"},
            // {"key":"A016","value":"新226"},{"key":"A017","value":"前6月按月还息，后6个月等额本息"},
            // {"key":"A018","value":"前4个月按月还息，后8个月等额本息"},{"key":"A019","value":"第一年按月还息，接下来等额本息"},
            // {"key":"A020","value":"334比例还款"},{"key":"A021","value":"433比例还款"},{"key":"A022","value":"10年期等额本息"},
            // {"key":"A023","value":"按月付息，定期还本"},{"key":"A030","value":"定制还款"},{"key":"A031","value":"5年期等额本息"},
            // {"key":"A040","value":"按期付息,按计划还本"},{"key":"A041","value":"其他方式"}]
            //huankfsh	还款方式 TODO  1--利随本清  2--多次还息一次还本  3--等额本息  4--等额本金  5--等比累进  6--等额累进  7--定制还款
            ln3110ReqDto.setHuankfsh(repayMode);
            //qigscfsh	期供生成方式  TODO 1--还款规则   2--还款计划书
            ln3110ReqDto.setQigscfsh("1");
            //qglxleix	期供利息类型  1--贷款本金利息  2--本期本金利息  3--指定本金利息
            ln3110ReqDto.setQglxleix("3");
            //dechligz	等额处理规则   TODO 0--不适用  1--标准+差额  2--标准  3--标准/实际
            ln3110ReqDto.setDechligz("2");
            //meiqhkze	每期还款总额
            //meiqhbje	每期还本金额

            //baoliuje	保留金额
            //kuanxiqi	宽限期
            //leijinzh	累进值
            //leijqjsh	累进区间期数
            //qishibis	起始笔数
            //chxunbis	查询笔数  默认20
            ln3110ReqDto.setChxunbis(1000);
            //shifoudy	是否打印  1--是   0--否
            ln3110ReqDto.setShifoudy("0");
            //scihkrbz	首次还款日模式  0--不适用  1--第一个还款日   2--次月还款日
            ln3110ReqDto.setScihkrbz("2");
            //mqihkfsh	末期还款方式 TODO  1--在最后一期进行还款   2--到期日前一期进行还款   3--不足一期算做一期
            ln3110ReqDto.setMqihkfsh("3");
            //dzhhkjih	定制还款计划 TODO 1--是 0--否
            ln3110ReqDto.setDzhhkjih("0");
            //ljsxqish	累进首段期数
            //dzhhkzhl	定制还款种类 TODO 1--正常还款 2--提前还款
            //xzuetqhk	需足额提前还款
            //dzhkriqi	定制还款日期
            //huanbjee	还本金额
            //tqhkhxfs	提前还款还息方式
            //huankzhh	还款账号
            //hkzhhzxh	还款账号子序号
            //benqqish	本期期数
            //qishriqi	起始日期
            //daoqriqi	到期日期
            //huanbjee	还本金额
            //hxijinee	还息金额
            //zwhkriqi	最晚还款日
            ln3110RespDto = ln3110Service.ln3110(ln3110ReqDto);
//            PageHelper.startPage(lmtSurveyReportDto.getPage(), lmtSurveyReportDto.getSize());
//            PageHelper.clearPage();
        } catch (YuspException e) {
            log.info("贷款还款计划试算失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            log.info("贷款还款计划试算结束..................");
        }


        List<Lstdkhk> lstdkhkList = ln3110RespDto.getLstdkhk();
        double meiqhkze = 0;
        if(lstdkhkList !=null && lstdkhkList.size() >0){

            meiqhkze = lstdkhkList.get(0).getMeiqhkze().doubleValue();

        }

        return new ResultDto(meiqhkze);
    }

    /**
     * @函数名称:updateDelete
     * @函数描述: 普通贷款合同申请 逻辑删除更新
     * @参数与返回说明:
     * @算法描述:
     */
    public int updateDelete(IqpLoanApp iqpLoanApp) {
        String approveStatus = iqpLoanApp.getApproveStatus();
        String serno = iqpLoanApp.getIqpSerno();
        // 逻辑删除时，如果是打回状态，则修改为自行退出，其他状态为逻辑删除
        if(CmisCommonConstants.WF_STATUS_992.equals(approveStatus)){
            log.info("流程删除==》bizId->{}当前流程状态为992（打回）",serno);
            workflowCoreClient.deleteByBizId(serno);
            iqpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_996);

            // 打回改为自行退出后，恢复额度(如果是反向生成低风险的情况，不需要做恢复额度)
            if(iqpLoanApp.getLmtAccNo()!=null && !"".equals(iqpLoanApp.getLmtAccNo())){
                String guarMode = iqpLoanApp.getGuarWay();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if(CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)){
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                }else {
                    recoverSpacAmtCny = iqpLoanApp.getCvtCnyAmt();
                    recoverAmtCny = iqpLoanApp.getCvtCnyAmt();
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_06);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                ResultDto<CmisLmt0012RespDto> resultDto= cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                String code = resultDto.getData().getErrorCode();
                if(!"0000".equals(code)){
                    log.error("业务申请恢复额度异常！");
                    throw new YuspException(EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }else{
            //删除关联的担保合同及担保合同与押品关系表
            bizCommonService.logicDeleteGrtGuarCont(serno);

            iqpLoanApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        }
        return iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);
    }


    /**
     * @param
     * @return void
     * @author shenli
     * @date 2021-7-8 14:21:29
     * @version 1.0.0
     * @desc 根据业务品种判断合同模式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto changeContMode(String iqpSerno,String contMode){

        IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(iqpSerno);
        String prd_id  = iqpLoanApp.getPrdId();
        User user = SessionUtils.getUserInformation();
        String org = user.getOrg().getCode();
        ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(user.getOrg().getCode());
        String orgType = resultDto.getData().getOrgType();

//        0-总行部室
//        1-异地支行（有分行）
//        2-异地支行（无分行）
//        3-异地分行
//        4-中心支行
//        5-综合支行
//        6-对公支行
//        7-零售支行

//        01--异地 02--本地
        if("1".equals(orgType) || "2".equals(orgType) || "3".equals(orgType)){
            orgType = "01";
        }else{
            orgType = "02";
        }

        if("".equals(RetailPrdAttributionEnums.lookup("PRD_ID_"+prd_id+"_"+orgType+"_"+contMode))){
            return new ResultDto(null).message("业务品种不支持该模式");

        }
        return new ResultDto();
    }

    /**
     * @方法名称: checkLmtAmtIsEnough
     * @方法描述: 贷款合同申请判断是否足额
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String checkLmtAmtIsEnough(IqpLoanApp iqpLoanApp) {
        String result = CmisCommonConstants.STD_ZB_YES_NO_1;
        log.info("贷款合同申请判断是否足额,流水号【{}】", iqpLoanApp.getIqpSerno());
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpLoanApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//分项类型
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度", iqpLoanApp.getLmtAccNo());
        CmisLmt0026RespDto cmisLmt0026RespDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto).getData();
        BigDecimal avlAvailAmt = new BigDecimal("0.0");
        if(cmisLmt0026RespDto.getAvlAvailAmt() != null){
            avlAvailAmt = cmisLmt0026RespDto.getAvlAvailAmt();
        }
        log.info("根据额度台账编号【{}】前往额度系统查询可用额度返回报文：" ,iqpLoanApp.getLmtAccNo());
        if(cmisLmt0026RespDto != null && iqpLoanApp.getCvtCnyAmt().compareTo(avlAvailAmt) > 0 ){
            // 如果是申请额度大于授信总额可用
            result = CmisCommonConstants.STD_ZB_YES_NO_0;
        }
        return result;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 贷款合同申请流程参数更新
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        log.info("贷款合同申请更新流程参数,流水号{}", serno);
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());
        IqpLoanApp iqpLoanApp = this.selectBySerno(serno);
        Map<String, Object> params = new HashMap<>();

        if(!CmisCommonConstants.STD_BUSI_TYPE_05.equals(iqpLoanApp.getBizType())){
            if(CmisCommonConstants.GUAR_MODE_60.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanApp.getGuarWay())){
                // 是否低风险
                params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
                // 单户低风险总额
                params.put("lowRiskAmtTotal", iqpLoanApp.getCvtCnyAmt().add(iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId())));
            }else{
                // 是否低风险
                params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_0);
                // 单户低风险总额
                params.put("lowRiskAmtTotal", new BigDecimal("0"));
            }
        }else{
            // 福费廷合同默认为低风险是低风险
            params.put("isLowRisk", CmisCommonConstants.STD_ZB_YES_NO_1);
            // 单户低风险总额
            params.put("lowRiskAmtTotal", iqpLoanApp.getCvtCnyAmt().add(iqpHighAmtAgrAppService.queryCusLowRiskUseAmt(iqpLoanApp.getCusId(), iqpLoanApp.getManagerBrId())));
        }


        // 需要反向生成低风险分项时，额度确定不足额
        if(iqpLoanApp.getLmtAccNo()!=null){
            params.put("isAmtEnough", checkLmtAmtIsEnough(iqpLoanApp));
        }else {
            params.put("isAmtEnough", CmisCommonConstants.STD_ZB_YES_NO_0);
        }
        // 申请金额
        params.put("amt", iqpLoanApp.getCvtCnyAmt());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    /**
     * @函数名称:selectXwztCount
     * @函数描述:根据客户号查询是否有在途业务-小微客户批量移交
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public int selectXwztCount(String cusId) {
        return iqpLoanAppMapper.selectXwztCount(cusId);
    }

    /**
     * @方法名称: riskItem0038
     * @方法描述: 合同金额校验
     * @参数与返回说明:
     * @算法描述:贸易融资合同申请时，若合同金额大于分项金额，则拦截
     * @创建人: yfs
     * @创建时间: 2021-07-13 10:47:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Object riskItem0038(String serno) {
        log.info("合同金额校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 通过流水号查询贸易融资合同
        IqpLoanApp iqpLoanApp = selectByPrimaryKey(serno);
        if(Objects.isNull(iqpLoanApp) || StringUtils.isBlank(iqpLoanApp.getLmtAccNo())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03802);
            return riskResultDto;
        }
        // 组装额度报文
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(iqpLoanApp.getManagerBrId()));//金融机构代码
        cmisLmt0026ReqDto.setSubSerno(iqpLoanApp.getLmtAccNo());//分项编号
        cmisLmt0026ReqDto.setQueryType(CmisCommonConstants.STD_ZB_LMT_TYPE_03);//分项类型
        ResultDto<CmisLmt0026RespDto> cmisLmt0026ResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
        if(Objects.isNull(cmisLmt0026ResultDto) || Objects.isNull(cmisLmt0026ResultDto.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03802);
            return riskResultDto;
        }
        // 是否无缝对接，是：1，否：0。若是否无缝对接是是，则不拦截
        if(Objects.equals("0", iqpLoanApp.getIsSeajnt())) {
            BigDecimal contAmt = Objects.isNull(iqpLoanApp.getContAmt()) ? BigDecimal.ZERO : iqpLoanApp.getContAmt();
            BigDecimal avlAvailAmt = Objects.isNull(cmisLmt0026ResultDto.getData().getAvlAvailAmt()) ? BigDecimal.ZERO : cmisLmt0026ResultDto.getData().getAvlAvailAmt();
            if(contAmt.compareTo(avlAvailAmt) < 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_03801);
                return riskResultDto;
            }
        }
        log.info("合同金额校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param map
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author qw
     * @date 2021/8/14 18:09
     * @version 1.0.0
     * @desc 获取币种汇率
     * @修改历史:
     */
    public Map getExchangeRate(Map map) {

        Map rtnData = new HashMap();
        String rate="";
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
        try {
            if("CNY".equals(map.get("curType"))){
                rate = "1";
                return rtnData;
            }
            Xdgj09ReqDto xdgj09ReqDto = new Xdgj09ReqDto();
            xdgj09ReqDto.setCcy((String) map.get("curType"));
            xdgj09ReqDto.setBaseccy("CNY");
            ResultDto<Xdgj09RespDto> resultDto = dscms2GjjsClientService.xdgj09(xdgj09ReqDto);
            String code = resultDto.getCode();
            if ("0".equals(code)) {
                Xdgj09RespDto resultDtoData = resultDto.getData();
                if(resultDtoData.getRsp()==null){
                    rtnCode = EcbEnum.ECB019999.key;
                    rtnMsg = EcbEnum.ECB020007.value;
                    return rtnData;
                }
                BigDecimal bigDecimalRate = new BigDecimal(resultDtoData.getRsp());
                rate = bigDecimalRate.divide(new BigDecimal(100.00)).toString();
            }else{
                rtnCode = EcbEnum.ECB019999.key;
                rtnMsg = EcbEnum.ECB019999.value;
            }
        } catch (Exception e) {
            rtnCode = EcbEnum.IQP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_EXCEPTION_DEF.value + e.getMessage();
            log.info("申请汇率查询报错{}", e.getMessage());
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("rate", rate);
        }
        return rtnData;
    }

    /**
     * @param IqpSerno
     * @return int
     * @author shenli
     * @date 2021-8-13
     * @version 1.0.0
     * @desc 零售-业务申请历史-作废
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int cancelIqpRetail(String IqpSerno) {
        log.info("******************零售-业务申请-作废 开始 ******************", IqpSerno);
        try {
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByPrimaryKey(IqpSerno);
            //第三方额度占用
            String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识
            //第三方额度恢复
            if ("1".equalsIgnoreCase(thirdPartyFlag)) {
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(iqpLoanApp.getIqpSerno());//业务编号
                cmisLmt0012ReqDto.setRecoverType("06");//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(iqpLoanApp.getCvtCnyAmt());// 恢复敞口金额(人民币)
                cmisLmt0012ReqDto.setRecoverAmtCny(iqpLoanApp.getCvtCnyAmt());//恢复总额(人民币)
                cmisLmt0012ReqDto.setInputId(iqpLoanApp.getInputId());
                cmisLmt0012ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
                cmisLmt0012ReqDto.setIsRecoverCoop("1");

                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-第三方恢复占用请求报文：" + cmisLmt0012ReqDto.toString());
                ResultDto<CmisLmt0012RespDto> cmisLmt0012RespDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("根据业务申请编号【" + iqpLoanApp.getIqpSerno() + "】前往额度系统-第三方恢复占用返回报文：" + cmisLmt0012RespDto.toString());

                if (!"0000".equals(cmisLmt0012RespDto.getData().getErrorCode())) {
                    log.info("零售-第三方恢复占用异常：" + cmisLmt0012RespDto.getData().getErrorMsg());
                    throw BizException.error(null, "9999", "零售-第三方恢复占用异常");
                }
            }

            iqpLoanApp.setApproveStatus("996");//自行退出
            iqpLoanAppMapper.updateByPrimaryKeySelective(iqpLoanApp);
        } catch (Exception e) {
            throw BizException.error(null, "9999", e.getMessage());
        }
        log.info("******************零售-业务申请-作废 结束 ******************", IqpSerno);
        return 0;
    }

    /**
     * @param map
     * @return Boolean
     * @author qw
     * @date 2021-8-13
     * @version 1.0.0
     * @desc 判断所选分项是否能做最高额授信协议或其他合同
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Boolean> isAllowContApp(Map map) {
        Boolean isAllowContApp = true;
        String hasOtherBuss = "1";
        String accSubNo = (String)map.get("accSubNo");
        String isHighLmt = (String)map.get("isHighLmt");
        CmisLmt0048ReqDto cmisLmt0048ReqDto = new CmisLmt0048ReqDto();
        cmisLmt0048ReqDto.setAccSubNo(accSubNo);
        cmisLmt0048ReqDto.setIsHighLmt(isHighLmt);
        ResultDto<CmisLmt0048RespDto> resultDto = cmisLmtClientService.cmislmt0048(cmisLmt0048ReqDto);
        if(!"0".equals(resultDto.getCode())){
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        if(!"0000".equals(resultDto.getData().getErrorCode())){
            throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
        }
        hasOtherBuss = resultDto.getData().getHasOtherBuss();
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(hasOtherBuss)){
            isAllowContApp = false;
        }
        return new ResultDto<Boolean>(isAllowContApp);
    }

    /**
     * @param iqpSerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/9/3 16:29
     * @version 1.0.0
     * @desc   根据产品ID获取产品相关信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<String> getPrdInfo(String iqpSerno) {
        String prdType = StringUtils.EMPTY;
        try{
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectByIqpSerno(iqpSerno);
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            //C000700020001:->业务品种->个人消费类贷款->零售消费类贷款->按揭类
            //C000700020002:->业务品种->个人消费类贷款->零售消费类贷款->非按揭类
            prdType = CfgPrdBasicinfoDto.getData().getPrdType();;
        } catch (Exception e){
            log.error("******************查询产品详细信息失败【{}】 ******************", iqpSerno);
            return new ResultDto<String>().code(9999).message("查询产品详细信息失败");
        }
        return new ResultDto<String>(prdType);
    }

    /**
     * @方法名称: submitNoFlow
     * @方法描述: 村镇银行无流程提交后续处理 参照DGYX01BizService 普通贷款合同后续处理逻辑
     * @参数与返回说明:
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public Map submitNoFlow(String iqpSerno) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        try {
            log.info("村镇银行普通合同贷款申请提交审批无流程处理开始：" + iqpSerno);
            // 1.针对单笔单批业务，更新审批模式表中的全流程状态为【1-审批中】
            this.handleBusinessDataAfterStart(iqpSerno);
            // 2.复制至合同主表 2.复制 新引入/新增的担保与业务申请到结果表中 3.更新一般业务申请表的审批状态 由审批中111 -> 审批通过 997
            this.handleBusinessDataAfterEnd(iqpSerno);
            log.info("村镇银行普通合同贷款申请提交审批无流程微信通知开始：" + iqpSerno);
            //微信通知
            IqpLoanApp iqpLoanApp = iqpLoanAppMapper.selectBySerno(iqpSerno);
            String managerId = iqpLoanApp.getManagerId();
            String mgrTel = "";
            if (StringUtil.isNotEmpty(managerId)) {
                log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    adminSmUserDto = resultDto.getData();
                    mgrTel = adminSmUserDto.getUserMobilephone();
                }
                //执行发送借款人操作
                String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                String messageType = CmisCommonConstants.MSG_CF_M_0016;//短信编号
                Map paramMap = new HashMap();//短信填充参数
                paramMap.put("cusName", iqpLoanApp.getCusName());
                paramMap.put("prdName", "一般合同申请");
                paramMap.put("result", "通过");
                //执行发送客户经理操作
                messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                log.info("村镇银行普通合同贷款申请提交审批无流程处理结束：" + iqpSerno);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("村镇银行普通合同贷款申请提交数据出现异常！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 发送短信
     * @author wangzhuyuan
     * @date 2021-9-6 09:34:28
     **/
    public void sendMessage(IqpLoanApp record) {
        String iqpSerno = record.getIqpSerno();
        log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信开始------------------");
        IqpLoanApp iqpLoanApp = selectByPrimaryKey(iqpSerno);
        String managerId = iqpLoanApp.getManagerId();
        try {
            String mgrTel = "";
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            String messageType = "MSG_LS_M_0001";// 短信编号
            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", record.getCusName());
            paramMap.put("flowName", "零售业务授信");
            //执行发送借款人操作
            log.info("零售业务授信审批流程："+"流水号："+record.getIqpSerno()+"-发送短信------------------");
            ResultDto<Integer> resultMessageDto = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);

        } catch (Exception e) {
            log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信失败------------------"+e);
        }

        log.info("零售业务授信审批流程："+"流水号："+iqpSerno+"-发送短信结束------------------");
    }

    /**
     * @方法名称: sendOnlinePldRemind
     * @方法描述: 首页消息提醒
     * @参数与返回说明:
     * @创建者：qw
     * @算法描述: 无
     */
    public void sendOnlinePldRemind(ResultInstanceDto resultInstanceDto, String serno) throws Exception {
        log.info("业务申请" + serno + "流程结束操作，流程参数" + resultInstanceDto);
        //针对流程到办结节点，进行以下处理
        //首页消息提醒
        IqpLoanApp iqpLoanApp = this.selectByIqpSerno(serno);
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(iqpLoanApp.getIsOlPld())){
            String managerId = iqpLoanApp.getManagerId();
            String managerBrId = iqpLoanApp.getManagerBrId();
            if (StringUtil.isNotEmpty(managerId)) {
                try {
                    //执行发送借款人操作
                    String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                    String messageType = "MSG_DG_M_0001";//短信编号
                    // 翻译管护经理名称和管护经理机构名称
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(managerId);
                    if (!"0".equals(adminSmUserDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                    if (!"0".equals(adminSmOrgDtoResultDto.getCode())) {
                        log.error("业务申请: " + serno + " 查询用户数据异常");
                        throw BizException.error(null, EcbEnum.ECB010004.key, EcbEnum.ECB010004.value);
                    }
                    String managerBrName = adminSmOrgDtoResultDto.getData().getOrgName();
                    //短信填充参数
                    Map paramMap = new HashMap();
                    paramMap.put("cusName", iqpLoanApp.getCusName());
                    paramMap.put("managerId", managerName);
                    paramMap.put("managerBrId", managerBrName);
                    paramMap.put("instanceId", resultInstanceDto.getInstanceId());
                    paramMap.put("nodeSign", resultInstanceDto.getNodeSign());
                    //执行发送客户经理操作
                    messageCommonService.sendonlinepldremind(messageType, receivedUserType, paramMap);
                } catch (Exception e) {
                    throw new Exception("发送短信失败！");
                }
            }
        }
    }

    /**
     * @param map
     * @return Boolean
     * @author qw
     * @date 2021-8-13
     * @version 1.0.0
     * @desc 判断所选分项是否存在资产池
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto<Boolean> isExistAspl(Map map) {
        Boolean isAspl = false;
        String accSubNo = (String)map.get("accSubNo");
        CmisLmt0063ReqDto cmisLmt0063ReqDto = new CmisLmt0063ReqDto();
        cmisLmt0063ReqDto.setSubSerno(accSubNo);
        ResultDto<CmisLmt0063RespDto> resultDto = cmisLmtClientService.cmislmt0063(cmisLmt0063ReqDto);
        if(!"0".equals(resultDto.getCode())){
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        if(!"0000".equals(resultDto.getData().getErrorCode())){
            throw BizException.error(null,resultDto.getData().getErrorCode(),resultDto.getData().getErrorMsg());
        }
        List<CmisLmt0063SubListRespDto> subList = resultDto.getData().getCmisLmt0063SubListRespDtoList();
        for (CmisLmt0063SubListRespDto cmisLmt0063SubListRespDto : subList) {
            String lmtPrdId = cmisLmt0063SubListRespDto.getLimitSubNo();
            if("10030101".equals(lmtPrdId)){
                isAspl = true;
                return new ResultDto<Boolean>(isAspl);
            }
        }
        return new ResultDto<Boolean>(isAspl);
    }

    /**
     * 判断是否允许债项评级测算
     * @param map
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map isAllowLadEval(Map map) {
        String contNo = (String) map.get("contNo");
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            List<GrtGuarCont> grtGuarContList = grtGuarBizRstRelService.queryGrtGuarContByParams(contNo);
            if(CollectionUtils.isEmpty(grtGuarContList)){
                rtnCode = EcbEnum.ECB020062.key;
                rtnMsg = EcbEnum.ECB020062.value;
                return result;
            }
            for (GrtGuarCont grtGuarCont : grtGuarContList) {
                if(grtGuarCont.getGuarAmt()==null ||"".equals(grtGuarCont.getGuarAmt()) || BigDecimal.ZERO.compareTo(grtGuarCont.getGuarAmt())==0){
                    rtnCode = EcbEnum.ECB020063.key;
                    rtnMsg = EcbEnum.ECB020063.value;
                    return result;
                }
            }
        }catch(Exception e){
            log.error("判断是否允许测算异常！",e.getMessage());
            throw BizException.error(null,EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key, EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }

    /**
     * 判断客户名下是否存在在途的合同申请流程
     * @param map
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Boolean isOnWayAppFlow(Map map) {
        String cusId = (String) map.get("cusId");
        String lmtAccNo = (String) map.get("lmtAccNo");
        Boolean result = false;
        QueryModel queryModel = new QueryModel();
        queryModel.setSize(999999999);
        queryModel.addCondition("cusId",cusId);
        queryModel.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_111);
        List list= ctrLoanContService.selectOtherThingsAppLoan(queryModel);
        if(CollectionUtils.nonEmpty(list)){
            result = true;
        }
        return result;
    }
}

