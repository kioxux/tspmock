/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardFirstJudgInifo;
import cn.com.yusys.yusp.repository.mapper.CreditCardFirstJudgInifoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardFirstJudgInifoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:45:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardFirstJudgInifoService {

    @Autowired
    private CreditCardFirstJudgInifoMapper creditCardFirstJudgInifoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardFirstJudgInifo selectByPrimaryKey(String pkId) {
        return creditCardFirstJudgInifoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardFirstJudgInifo> selectAll(QueryModel model) {
        List<CreditCardFirstJudgInifo> records = (List<CreditCardFirstJudgInifo>) creditCardFirstJudgInifoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardFirstJudgInifo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardFirstJudgInifo> list = creditCardFirstJudgInifoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardFirstJudgInifo record) {
        String serno = record.getSerno();
        CreditCardFirstJudgInifo creditCardFirstJudgInifo = creditCardFirstJudgInifoMapper.selectBySerno(serno);
        if(creditCardFirstJudgInifo ==null ){
            return creditCardFirstJudgInifoMapper.insert(record);
        }else{
            return creditCardFirstJudgInifoMapper.updateByPrimaryKeySelective(record);
        }

    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardFirstJudgInifo record) {
        return creditCardFirstJudgInifoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardFirstJudgInifo record) {
        return creditCardFirstJudgInifoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardFirstJudgInifo record) {
        return creditCardFirstJudgInifoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardFirstJudgInifoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardFirstJudgInifoMapper.deleteByIds(ids);
    }

    /**
     * @param serno
     * @return  CreditCardFirstJudgInifo
     * @author wzy
     * @date 2021/5/29 10:36
     * @version 1.0.0
     * @desc   根据业务流水号查询初审意见
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CreditCardFirstJudgInifo selectBySerno(String serno) {
        return creditCardFirstJudgInifoMapper.selectBySerno(serno);
    }
}
