/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfo;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfoAcc;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoAccService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:23:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestBasicInfoAccService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestBasicInfoAccMapper lmtSigInvestBasicInfoAccMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoAcc selectByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoAccMapper.selectByPrimaryKey(pkId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestBasicInfoAcc> selectAll(QueryModel model) {
        List<LmtSigInvestBasicInfoAcc> records = (List<LmtSigInvestBasicInfoAcc>) lmtSigInvestBasicInfoAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestBasicInfoAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestBasicInfoAcc> list = lmtSigInvestBasicInfoAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestBasicInfoAcc record) {
        return lmtSigInvestBasicInfoAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestBasicInfoAcc record) {
        return lmtSigInvestBasicInfoAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestBasicInfoAcc record) {
        return lmtSigInvestBasicInfoAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestBasicInfoAcc record) {
        return lmtSigInvestBasicInfoAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestBasicInfoAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestBasicInfoAccMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoAcc initLmtSigInvestBasicInfoAccInfo(Object lmtSigInvestBean) {
        //主键
        String pkValue = sequenceTemplateClient.getSequenceTemplate(CmisLmtConstants.PK_VALUE, new HashedMap());
        //初始化对象
        LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = new LmtSigInvestBasicInfoAcc() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtSigInvestBean, lmtSigInvestBasicInfoAcc);
        //生成主键
        lmtSigInvestBasicInfoAcc.setPkId(pkValue);
        //最新更新日期
        lmtSigInvestBasicInfoAcc.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        //创建日期
        lmtSigInvestBasicInfoAcc.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtSigInvestBasicInfoAcc.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtSigInvestBasicInfoAcc ;
    }

    /**
     *
     * @param replySerno
     * @return
     */
    public LmtSigInvestBasicInfoAcc selectByReplySerno(String replySerno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);
        List<LmtSigInvestBasicInfoAcc> lmtSigInvestBasicInfoAccs = selectAll(queryModel);
        if (lmtSigInvestBasicInfoAccs!=null && lmtSigInvestBasicInfoAccs.size()>0){
            return lmtSigInvestBasicInfoAccs.get(0);
        }
        return null;
    }

    /**
     * 批复变更-生成台账表信息
     * @param lmtSigInvestBasicInfo
     * @param oldSerno
     * @param replySerno
     * @param currentOrgId
     * @param currentUserId
     * @param accSerno
     */
    public void insertBasicInfoAcc(LmtSigInvestBasicInfo lmtSigInvestBasicInfo, String oldSerno, String replySerno, String currentOrgId, String currentUserId, String accSerno) {
        LmtSigInvestBasicInfoAcc lmtSigInvestBasicInfoAcc = new LmtSigInvestBasicInfoAcc();
        BeanUtils.copyProperties(lmtSigInvestBasicInfo,lmtSigInvestBasicInfoAcc);

        lmtSigInvestBasicInfoAcc.setSerno(oldSerno);
        lmtSigInvestBasicInfoAcc.setAccNo(accSerno);
        lmtSigInvestBasicInfoAcc.setReplySerno(replySerno);
        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtSigInvestBasicInfoAcc,currentUserId,currentOrgId);
        insert(lmtSigInvestBasicInfoAcc);
    }

    /**
     * @方法名称: deleteByAccNo
     * @方法描述: 根据批复编号删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByAccNo(String accNo) {
        return lmtSigInvestBasicInfoAccMapper.deleteByAccNo(accNo);
    }

    /**
     * @方法名称: selectByAccNo
     * @方法描述: 根据台账编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtSigInvestBasicInfoAcc selectByAccNo(String accNo) {
        return lmtSigInvestBasicInfoAccMapper.selectByAccNo(accNo);
    }
}
