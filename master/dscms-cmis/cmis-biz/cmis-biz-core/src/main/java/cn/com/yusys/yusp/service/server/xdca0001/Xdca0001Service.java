package cn.com.yusys.yusp.service.server.xdca0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CreditCardAdjustmentAppInfo;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdca0001.req.Xdca0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0001.resp.Xdca0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CreditCardAdjustmentAppInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdca0001Service
 * @类描述: #服务类 信用卡调额申请
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-06-02 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdca0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0001Service.class);

    @Autowired
    private CreditCardAdjustmentAppInfoMapper creditCardAdjustmentAppInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Resource
    private CommonService commonService;

    /**
     * 信用卡调额申请
     *
     * @param xdca0001DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdca0001DataRespDto xdca0001(Xdca0001DataReqDto xdca0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value);
        Xdca0001DataRespDto xdca0001DataRespDto = new Xdca0001DataRespDto();

        try {
            //请求字段
            String adjustmentChnl = xdca0001DataReqDto.getAdjustmentChnl();
            String cardNo = xdca0001DataReqDto.getCardNo();
            String cardPrd = xdca0001DataReqDto.getCardPrd();
            String certType = xdca0001DataReqDto.getCertType();
            String certNo = xdca0001DataReqDto.getCertNo();
            String holdCardName = xdca0001DataReqDto.getHoldCardName();
            BigDecimal cdtAmt = xdca0001DataReqDto.getCdtAmt();
            BigDecimal newAmt = xdca0001DataReqDto.getNewAmt();
            String isProvidCdtProve = xdca0001DataReqDto.getIsProvidCdtProve();
            String memo = xdca0001DataReqDto.getMemo();

            //新增调额申请信息表
            CreditCardAdjustmentAppInfo creditCardAdjustmentAppInfo = new CreditCardAdjustmentAppInfo();

            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.LMT_INDIV_APP_SEQ, new HashMap<>());
            creditCardAdjustmentAppInfo.setSerno(serno);//业务流水
            creditCardAdjustmentAppInfo.setAdjustmentChnl(adjustmentChnl);//渠道
            creditCardAdjustmentAppInfo.setCardNo(cardNo);//卡号
            creditCardAdjustmentAppInfo.setCardPrd(cardPrd);//卡产品
            creditCardAdjustmentAppInfo.setCertType(certType);//证件类型
            creditCardAdjustmentAppInfo.setCertCode(certNo);//证件号
            creditCardAdjustmentAppInfo.setCusName(holdCardName);//持卡人
            creditCardAdjustmentAppInfo.setOrigCreditCardLmt(cdtAmt);//原额度
            creditCardAdjustmentAppInfo.setNewCreditCardLmt(newAmt);//新额度
            creditCardAdjustmentAppInfo.setIsIncrease(isProvidCdtProve);//是否提供增信证明
            creditCardAdjustmentAppInfo.setRemarks(memo);//备注

            //通过客户证件号查询客户信息
            String managerId = StringUtils.EMPTY;
            String managerBrid = StringUtils.EMPTY;
            logger.info("*********XDCA0001*信用卡调额申获取客户信息开始,查询参数为:{}", certNo);
            List<CusBaseDto> resultlist = commonService.getCusBaseListByCertCode(certNo);
            logger.info("*********XDCA0001*信用卡调额申获取客户信息结束,查询参数为:{}", JSON.toJSONString(resultlist));
            if (resultlist.size() > 0) {
                CusBaseDto cusBaseDto = resultlist.get(0);
                managerId = cusBaseDto.getManagerId();
                managerBrid = cusBaseDto.getMainBrId();
            }

            //其他信息
            String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            creditCardAdjustmentAppInfo.setAppDate(openDay);//申请日期
            creditCardAdjustmentAppInfo.setApproveStatus(CmisBizConstants.APPLY_STATE_TODO);//审批状态-待发起
            creditCardAdjustmentAppInfo.setOprType(CmisBizConstants.OPR_TYPE_01);//新增标示
            creditCardAdjustmentAppInfo.setInputId(managerId);//登记人
            creditCardAdjustmentAppInfo.setInputBrId(managerBrid);//登记机构
            creditCardAdjustmentAppInfo.setUpdId(managerId);
            creditCardAdjustmentAppInfo.setUpdBrId(managerBrid);
            creditCardAdjustmentAppInfo.setManagerId(managerId);
            creditCardAdjustmentAppInfo.setManagerBrId(managerBrid);
            creditCardAdjustmentAppInfo.setUpdDate(openDay);
            creditCardAdjustmentAppInfo.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            creditCardAdjustmentAppInfo.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            //新增
            logger.info("*********XDCA0001*信用卡调额申请信息新增开始,查询参数为:{}", JSON.toJSONString(creditCardAdjustmentAppInfo));
            int count = creditCardAdjustmentAppInfoMapper.insert(creditCardAdjustmentAppInfo);
            logger.info("*********XDCA0001*信用卡调额申请信息新增结束,返回参数为:{}", JSON.toJSONString(count));
            if (count > 0) {//成功
                xdca0001DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                xdca0001DataRespDto.setOpMsg(DscmsBizDbEnum.FALG_SUCCESS.value);
            } else {
                xdca0001DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdca0001DataRespDto.setOpMsg(DscmsBizDbEnum.FLAG_FAILD.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value);
        return xdca0001DataRespDto;
    }

}
