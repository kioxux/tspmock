/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptFncSituBsAnys;
import cn.com.yusys.yusp.service.RptFncSituBsAnysService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptFncSituBsAnysResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-06 16:33:56
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptfncsitubsanys")
public class RptFncSituBsAnysResource {
    @Autowired
    private RptFncSituBsAnysService rptFncSituBsAnysService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptFncSituBsAnys>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptFncSituBsAnys> list = rptFncSituBsAnysService.selectAll(queryModel);
        return new ResultDto<List<RptFncSituBsAnys>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptFncSituBsAnys>> index(QueryModel queryModel) {
        List<RptFncSituBsAnys> list = rptFncSituBsAnysService.selectByModel(queryModel);
        return new ResultDto<List<RptFncSituBsAnys>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptFncSituBsAnys> show(@PathVariable("pkId") String pkId) {
        RptFncSituBsAnys rptFncSituBsAnys = rptFncSituBsAnysService.selectByPrimaryKey(pkId);
        return new ResultDto<RptFncSituBsAnys>(rptFncSituBsAnys);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptFncSituBsAnys> create(@RequestBody RptFncSituBsAnys rptFncSituBsAnys) throws URISyntaxException {
        rptFncSituBsAnysService.insert(rptFncSituBsAnys);
        return new ResultDto<RptFncSituBsAnys>(rptFncSituBsAnys);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptFncSituBsAnys rptFncSituBsAnys) throws URISyntaxException {
        int result = rptFncSituBsAnysService.update(rptFncSituBsAnys);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptFncSituBsAnysService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * 条件查询
     *
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptFncSituBsAnys>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptFncSituBsAnys>>(rptFncSituBsAnysService.selectByModel(model));
    }

    /**
     * 初始化
     *
     * @param map
     * @return
     */
    @PostMapping("/initSmallStandard")
    protected ResultDto<List<RptFncSituBsAnys>> initSmallStandard(@RequestBody Map map) {
        return new ResultDto<List<RptFncSituBsAnys>>(rptFncSituBsAnysService.initSmallStandard(map));
    }

    /**
     * 保存
     *
     * @param rptFncSituBsAnys
     * @return
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptFncSituBsAnys rptFncSituBsAnys) {
        return new ResultDto<Integer>(rptFncSituBsAnysService.updateSelective(rptFncSituBsAnys));
    }

    @PostMapping("/updateFnc")
    protected ResultDto<Integer> updateFnc(@RequestBody String serno){
        return new ResultDto<Integer>(rptFncSituBsAnysService.updateFnc(serno));
    }
}
