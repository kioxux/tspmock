package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetNFinan
 * @类描述: iqp_dis_asset_n_finan数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-23 14:27:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDisAssetNFinanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 非金融资产表主键 **/
	private String nfinanPk;
	
	/** 业务申请流水号 **/
	private String iqpSerno;
	
	/** 贷款申请人主键 **/
	private String apptCode;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 非金融资产类型 **/
	private String nFinAssetType;
	
	/** 房屋属性 **/
	private String houseType;
	
	/** 房屋价值 **/
	private java.math.BigDecimal houseValue;
	
	/** 抵押余额 **/
	private java.math.BigDecimal mortgageBal;
	
	/** 其他非金融资产（折算后）金额 **/
	private java.math.BigDecimal otherFinanAsset;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 客户经理认定收入金额 **/
	private java.math.BigDecimal managerIncome;
	
	/** 分行审查认定收入金额 **/
	private java.math.BigDecimal branchExIncome;
	
	/** 总行审查认定收入金额 **/
	private java.math.BigDecimal headExIncome;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param nfinanPk
	 */
	public void setNfinanPk(String nfinanPk) {
		this.nfinanPk = nfinanPk == null ? null : nfinanPk.trim();
	}
	
    /**
     * @return NfinanPk
     */	
	public String getNfinanPk() {
		return this.nfinanPk;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param nFinAssetType
	 */
	public void setNFinAssetType(String nFinAssetType) {
		this.nFinAssetType = nFinAssetType == null ? null : nFinAssetType.trim();
	}
	
    /**
     * @return NFinAssetType
     */	
	public String getNFinAssetType() {
		return this.nFinAssetType;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param houseValue
	 */
	public void setHouseValue(java.math.BigDecimal houseValue) {
		this.houseValue = houseValue;
	}
	
    /**
     * @return HouseValue
     */	
	public java.math.BigDecimal getHouseValue() {
		return this.houseValue;
	}
	
	/**
	 * @param mortgageBal
	 */
	public void setMortgageBal(java.math.BigDecimal mortgageBal) {
		this.mortgageBal = mortgageBal;
	}
	
    /**
     * @return MortgageBal
     */	
	public java.math.BigDecimal getMortgageBal() {
		return this.mortgageBal;
	}
	
	/**
	 * @param otherFinanAsset
	 */
	public void setOtherFinanAsset(java.math.BigDecimal otherFinanAsset) {
		this.otherFinanAsset = otherFinanAsset;
	}
	
    /**
     * @return OtherFinanAsset
     */	
	public java.math.BigDecimal getOtherFinanAsset() {
		return this.otherFinanAsset;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param managerIncome
	 */
	public void setManagerIncome(java.math.BigDecimal managerIncome) {
		this.managerIncome = managerIncome;
	}
	
    /**
     * @return ManagerIncome
     */	
	public java.math.BigDecimal getManagerIncome() {
		return this.managerIncome;
	}
	
	/**
	 * @param branchExIncome
	 */
	public void setBranchExIncome(java.math.BigDecimal branchExIncome) {
		this.branchExIncome = branchExIncome;
	}
	
    /**
     * @return BranchExIncome
     */	
	public java.math.BigDecimal getBranchExIncome() {
		return this.branchExIncome;
	}
	
	/**
	 * @param headExIncome
	 */
	public void setHeadExIncome(java.math.BigDecimal headExIncome) {
		this.headExIncome = headExIncome;
	}
	
    /**
     * @return HeadExIncome
     */	
	public java.math.BigDecimal getHeadExIncome() {
		return this.headExIncome;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}