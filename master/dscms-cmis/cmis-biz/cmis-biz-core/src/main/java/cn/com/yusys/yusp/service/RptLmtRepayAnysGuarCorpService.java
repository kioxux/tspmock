/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.FinanIndicAnalyDto;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarCorp;
import cn.com.yusys.yusp.repository.mapper.RptLmtRepayAnysGuarCorpMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarCorpService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:04:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptLmtRepayAnysGuarCorpService {

    @Autowired
    private RptLmtRepayAnysGuarCorpMapper rptLmtRepayAnysGuarCorpMapper;

    @Autowired
    private GuarGuaranteeService guarGuaranteeService;

    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptLmtRepayAnysGuarCorp selectByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarCorpMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptLmtRepayAnysGuarCorp> selectAll(QueryModel model) {
        List<RptLmtRepayAnysGuarCorp> records = (List<RptLmtRepayAnysGuarCorp>) rptLmtRepayAnysGuarCorpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptLmtRepayAnysGuarCorp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptLmtRepayAnysGuarCorp> list = rptLmtRepayAnysGuarCorpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptLmtRepayAnysGuarCorp record) {
        return rptLmtRepayAnysGuarCorpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptLmtRepayAnysGuarCorp record) {
        return rptLmtRepayAnysGuarCorpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptLmtRepayAnysGuarCorp record) {
        return rptLmtRepayAnysGuarCorpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptLmtRepayAnysGuarCorp record) {
        return rptLmtRepayAnysGuarCorpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptLmtRepayAnysGuarCorpMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptLmtRepayAnysGuarCorpMapper.deleteByIds(ids);
    }

    /**
     * 保存信息
     *
     * @param rptLmtRepayAnysGuarCorp
     * @return
     */
    public int saveGuarCorp(RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp) {
        String pkId = rptLmtRepayAnysGuarCorp.getPkId();
        RptLmtRepayAnysGuarCorp temp = rptLmtRepayAnysGuarCorpMapper.selectByPrimaryKey(pkId);
        int count = 0;
        if (temp != null) {
            count = rptLmtRepayAnysGuarCorpMapper.updateByPrimaryKeySelective(rptLmtRepayAnysGuarCorp);
        } else {
            count = rptLmtRepayAnysGuarCorpMapper.insertSelective(rptLmtRepayAnysGuarCorp);
        }
        return count;
    }

    /**
     * 初始化
     *
     * @param rptLmtRepayAnysGuarCorp
     * @return
     */
    public String insertCorp(RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp) {
        List<Map<String, Object>> list = guarGuaranteeService.queryGuarGuaranteeBySerno(rptLmtRepayAnysGuarCorp.getSerno());
        if (CollectionUtils.nonEmpty(list)) {
            for (Map<String, Object> map : list) {
                String cusTyp = map.get("cusTyp").toString();
                String isGuarCom = map.get("isGuarCom").toString();
                String isAddGuar = map.get("isAddGuar").toString();
                if ("10003".equals(cusTyp)) {
                    if (Objects.nonNull(map.get("isGuarCom")) && CmisBizConstants.STD_ZB_YES_NO_Y.equals(map.get("isGuarCom").toString())) {
                        continue;
                    }

                }
            }
        }
        rptLmtRepayAnysGuarCorp.setPkId(StringUtils.getUUID());
        insert(rptLmtRepayAnysGuarCorp);
        return rptLmtRepayAnysGuarCorp.getPkId();
    }
    public RptLmtRepayAnysGuarCorp initGuarCorp(Map map){
        String pkId = map.get("pkId").toString();
        if(StringUtils.nonBlank(pkId)){
            return selectByPrimaryKey(pkId);
        }else {
            RptLmtRepayAnysGuarCorp rptLmtRepayAnysGuarCorp = new RptLmtRepayAnysGuarCorp();
            String cusId = map.get("cusId").toString();
            ResultDto<CusCorpDto> cusCorpDtoResultDto = iCusClientService.queryCusCropDtoByCusId(cusId);
            if (cusCorpDtoResultDto != null && cusCorpDtoResultDto.getData() != null) {
                CusCorpDto cusCorpDto = cusCorpDtoResultDto.getData();
                //担保人名称
                rptLmtRepayAnysGuarCorp.setGuarName(cusCorpDto.getCusName());
                //主营业务范围
                rptLmtRepayAnysGuarCorp.setMainBusi(cusCorpDto.getMainOptScp());
                //成立日期
                rptLmtRepayAnysGuarCorp.setBuildDate(cusCorpDto.getBuildDate());
                //实收资本
                rptLmtRepayAnysGuarCorp.setPaidCapAmt(cusCorpDto.getPaidCapAmt());
                //是否上市企业
                rptLmtRepayAnysGuarCorp.setIsStockCorp(cusCorpDto.getIsStockCorp());
                //企业地址
                rptLmtRepayAnysGuarCorp.setCompanyAddr(cusCorpDto.getOperAddrAct());
                //获取实际控制人
                Map<String, Object> map1 = new HashMap<>();
                map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
                map1.put("cusIdRel", cusId);
                ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType = cmisCusClientService.getCusCorpMgrByParams(map1);
                if (cusCorpMgrByMrgType != null && cusCorpMgrByMrgType.getData() != null) {
                    String realOperCusId = cusCorpMgrByMrgType.getData().getMrgName();
                    rptLmtRepayAnysGuarCorp.setRealOperCusId(realOperCusId);
                }
                //获取法人代表
                Map<String, Object> map2 = new HashMap<>();
                map2.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_200400);
                map2.put("cusIdRel", cusId);
                ResultDto<CusCorpMgrDto> cusCorpMgrByMrgType1 = cmisCusClientService.getCusCorpMgrByParams(map2);
                if (cusCorpMgrByMrgType1 != null && cusCorpMgrByMrgType1.getData() != null) {
                    String legal = cusCorpMgrByMrgType1.getData().getMrgName();
                    rptLmtRepayAnysGuarCorp.setLegal(legal);
                }
                ResultDto<List<FinanIndicAnalyDto>> rptFncTotalProfit = iCusClientService.getRptFncTotalProfit(map);
                if (rptFncTotalProfit != null && rptFncTotalProfit.getData() != null) {
                    List<FinanIndicAnalyDto> data = rptFncTotalProfit.getData();
                    for (FinanIndicAnalyDto finanIndicAnalyDto : data) {
                        if ("资产总计".equals(finanIndicAnalyDto.getItemName())) {
                            String inputYear = finanIndicAnalyDto.getInputYear();
                            Map param1 = new HashMap();
                            param1.put("cusId", cusId);
                            param1.put("statPrd", inputYear);
                            //获取当年财报类型
                            ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                            if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                                Map data1 = mapResultDto1.getData();
                                Object reportType = data1.get("reportType");
                                if (Objects.nonNull(reportType)) {
                                    String fncType = reportType.toString();
                                    if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setDepTsTotalRessetAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setSelfRepTotalRessetAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                    else {
                                        rptLmtRepayAnysGuarCorp.setSelfRepTotalRessetAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                } else {
                                    rptLmtRepayAnysGuarCorp.setSelfRepTotalRessetAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            }
                            continue;
                        }
                        if ("固定资产".equals(finanIndicAnalyDto.getItemName())) {
                            String inputYear = finanIndicAnalyDto.getInputYear();
                            Map param1 = new HashMap();
                            param1.put("cusId", cusId);
                            param1.put("statPrd", inputYear);
                            //获取当年财报类型
                            ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                            if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                                Map data1 = mapResultDto1.getData();
                                Object reportType = data1.get("reportType");
                                if (Objects.nonNull(reportType)) {
                                    String fncType = reportType.toString();
                                    if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setDepTsFixedAssets(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setSelfRepFixedAssets(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                    else {
                                        rptLmtRepayAnysGuarCorp.setSelfRepFixedAssets(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                } else {
                                    rptLmtRepayAnysGuarCorp.setSelfRepFixedAssets(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            }
                            continue;
                        }
                        if ("负债总额".equals(finanIndicAnalyDto.getItemName())) {
                            String inputYear = finanIndicAnalyDto.getInputYear();
                            Map param1 = new HashMap();
                            param1.put("cusId", cusId);
                            param1.put("statPrd", inputYear);
                            //获取当年财报类型
                            ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                            if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                                Map data1 = mapResultDto1.getData();
                                Object reportType = data1.get("reportType");
                                if (Objects.nonNull(reportType)) {
                                    String fncType = reportType.toString();
                                    if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setDepTsLiabilities(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setSelfRepLiabilities(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                    else {
                                        rptLmtRepayAnysGuarCorp.setSelfRepLiabilities(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                } else {
                                    rptLmtRepayAnysGuarCorp.setSelfRepLiabilities(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            }
                            continue;
                        }
                        if ("净资产".equals(finanIndicAnalyDto.getItemName())) {
                            String inputYear = finanIndicAnalyDto.getInputYear();
                            Map param1 = new HashMap();
                            param1.put("cusId", cusId);
                            param1.put("statPrd", inputYear);
                            //获取当年财报类型
                            ResultDto<Map> mapResultDto1 = iCusClientService.selectFncByCusIdRptYear(param1);
                            if (mapResultDto1 != null && mapResultDto1.getData() != null) {
                                Map data1 = mapResultDto1.getData();
                                Object reportType = data1.get("reportType");
                                rptLmtRepayAnysGuarCorp.setTotalLiabilitiesAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                if (Objects.nonNull(reportType)) {
                                    String fncType = reportType.toString();
                                    if (CmisCusConstants.STD_ZB_REPORT_TYPE_02.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setDepTsPureAsset(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    } else if (CmisCusConstants.STD_ZB_REPORT_TYPE_01.equals(fncType)) {
                                        rptLmtRepayAnysGuarCorp.setSelfRepPureAsset(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                    else {
                                        rptLmtRepayAnysGuarCorp.setSelfRepPureAsset(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                    }
                                } else {
                                    rptLmtRepayAnysGuarCorp.setSelfRepPureAsset(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                                }
                            }
                            continue;
                        }
                        if ("流动资产合计".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setCurfundAssetAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("货币资金".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setCurfundPaymentAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("应收账款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setDebtReceivableAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("其他应收款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setOtherDebtReceivableAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("预付账款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setPrepaymentsAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("存货".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setStockAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("固定资产净值".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setFixedAssetsAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("无形资产".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setIntangibleAssetsAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("流动负债合计".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setCurrentLiabilitiesAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("短期借款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setShortLoanAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("应付票据".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setBillPayableAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("应付账款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setAccountsPayableAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("预收帐款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setDepositReceivedAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("其他应付款".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setOtherPayableAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("长期负债合计".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setLongLiabilitiesAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                        if ("所有者权益".equals(finanIndicAnalyDto.getItemName())) {
                            rptLmtRepayAnysGuarCorp.setOwnerEquityAmt(Objects.isNull(finanIndicAnalyDto.getCurYmValue()) ? BigDecimal.ZERO : finanIndicAnalyDto.getCurYmValue());
                            continue;
                        }
                    }
                }
                rptLmtRepayAnysGuarCorp.setPkId(StringUtils.getUUID());
                String serno = map.get("serno").toString();
                rptLmtRepayAnysGuarCorp.setSerno(serno);
                insert(rptLmtRepayAnysGuarCorp);
                return rptLmtRepayAnysGuarCorp;
            }
        }
        return null;
    }
}
