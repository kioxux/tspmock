package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AsplAccpTask;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AsplAccpTaskService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @className AsplWhtls
 * @Description 发票补录业务后处理
 * @Date 2020/12/21 : 10:43
 */
@Service
public class ZCYW02BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(ZCYW02BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private AsplAccpTaskService asplAccpTaskService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        if(CmisFlowConstants.FLOW_ID_ZC002.equals(resultInstanceDto.getBizType())){
            this.handleZC002Biz(resultInstanceDto);
        }
    }

    public void handleZC002Biz(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String taskId = resultInstanceDto.getBizId();
        try {
            AsplAccpTask asplAccpTask = asplAccpTaskService.selectByTaskId(taskId);
            // 获取当前业务申请路程类型
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                // 正常提交下一步处理   审批中 111
                asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.更新贸易背景资料收集配置表的审批状态 由审批中111 -> 审批通过 997
                asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_997);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程退回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程打回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "流程拿回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "拿回初始节点操作，流程参数" + resultInstanceDto);
                //流程拿回到第一个节点，申请主表的业务
                asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("发票补录业务申请" + taskId + "否决操作，流程参数" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                asplAccpTaskService.updateApproveStatueAfterFlow(asplAccpTask,CmisCommonConstants.WF_STATUS_998);
            } else {
                log.info("发票补录业务申请" + taskId + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("发票补录业务后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
            // 贸易背景资料收集业务 审批流程
        return "ZCYW02".equals(flowCode);
    }
}
