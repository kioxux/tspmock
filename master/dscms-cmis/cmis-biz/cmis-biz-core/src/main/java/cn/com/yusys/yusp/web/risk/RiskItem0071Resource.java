package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0071Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0071Resource
 * @类描述: 还款能力分析校验
 * @功能描述: 还款能力分析校验
 * @创建人: dumd
 * @创建时间: 2021年8月03日20:16:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0071产品授信申报各个节点增加校验是否有同业管理类额度或者同业投资类额度满足占用")
@RestController
@RequestMapping("/api/riskcheck/riskItem0071")
public class RiskItem0071Resource {

    @Autowired
    private RiskItem0071Service riskItem0071Service;
    /**
     * @方法名称: riskItem0071
     * @方法描述: 产品授信申报各个节点增加校验是否有同业管理类额度或者同业投资类额度满足占用
     * @参数与返回说明:
     * @算法描述:
     * @创建人: lyj
     * @创建时间: 2021年8月2日20:16:04
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "同业管理类额度或者同业投资类额度满足占用")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0071(@RequestBody QueryModel queryModel) {
        if (queryModel.getCondition().size() == 0) {
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return ResultDto.success(riskResultDto);
        }
        return ResultDto.success(riskItem0071Service.riskItem0071(queryModel));
    }
}
