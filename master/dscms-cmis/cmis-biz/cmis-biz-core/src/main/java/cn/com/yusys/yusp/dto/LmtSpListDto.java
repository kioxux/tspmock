package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSpList
 * @类描述: lmt_sp_list数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 15:27:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSpListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 借据金额 **/
	private java.math.BigDecimal billAmt;
	
	/** 借据余额 **/
	private java.math.BigDecimal billBal;
	
	/** 欠息累计 **/
	private java.math.BigDecimal oweInt;
	
	/** 代偿本金 **/
	private java.math.BigDecimal subpayCap;
	
	/** 代偿利息 **/
	private java.math.BigDecimal subpayInt;
	
	/** 操作类型 **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return BillAmt
     */	
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBal
	 */
	public void setBillBal(java.math.BigDecimal billBal) {
		this.billBal = billBal;
	}
	
    /**
     * @return BillBal
     */	
	public java.math.BigDecimal getBillBal() {
		return this.billBal;
	}
	
	/**
	 * @param oweInt
	 */
	public void setOweInt(java.math.BigDecimal oweInt) {
		this.oweInt = oweInt;
	}
	
    /**
     * @return OweInt
     */	
	public java.math.BigDecimal getOweInt() {
		return this.oweInt;
	}
	
	/**
	 * @param subpayCap
	 */
	public void setSubpayCap(java.math.BigDecimal subpayCap) {
		this.subpayCap = subpayCap;
	}
	
    /**
     * @return SubpayCap
     */	
	public java.math.BigDecimal getSubpayCap() {
		return this.subpayCap;
	}
	
	/**
	 * @param subpayInt
	 */
	public void setSubpayInt(java.math.BigDecimal subpayInt) {
		this.subpayInt = subpayInt;
	}
	
    /**
     * @return SubpayInt
     */	
	public java.math.BigDecimal getSubpayInt() {
		return this.subpayInt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}