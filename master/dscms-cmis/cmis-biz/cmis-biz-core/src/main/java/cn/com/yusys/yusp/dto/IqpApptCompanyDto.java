package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptCompany
 * @类描述: iqp_appt_company数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:15:58
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApptCompanyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 贷款申请人主键 **/
	private String apptCode;
	
	/** 职业 **/
	private String pfn;
	
	/** 职务 **/
	private String dut;
	
	/** 职称 **/
	private String proTitle;
	
	/** 单位性质 **/
	private String type;
	
	/** 单位名称 **/
	private String workPlaceName;
	
	/** 统一社会信用代码 **/
	private String unifyCreditCode;
	
	/** 单位地址省/直辖市 **/
	private String addrProvience;
	
	/** 单位地址-市 **/
	private String addrCity;
	
	/** 单位地址-区 **/
	private String addrArear;
	
	/** 单位地址 **/
	private String addrDetail;
	
	/** 单位所属行业 **/
	private String kind;
	
	/** 单位电话 **/
	private String tel;
	
	/** 进入现单位时间 **/
	private String comeinDt;
	
	/** 所属部门 **/
	private String dep;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param pfn
	 */
	public void setPfn(String pfn) {
		this.pfn = pfn == null ? null : pfn.trim();
	}
	
    /**
     * @return Pfn
     */	
	public String getPfn() {
		return this.pfn;
	}
	
	/**
	 * @param dut
	 */
	public void setDut(String dut) {
		this.dut = dut == null ? null : dut.trim();
	}
	
    /**
     * @return Dut
     */	
	public String getDut() {
		return this.dut;
	}
	
	/**
	 * @param proTitle
	 */
	public void setProTitle(String proTitle) {
		this.proTitle = proTitle == null ? null : proTitle.trim();
	}
	
    /**
     * @return ProTitle
     */	
	public String getProTitle() {
		return this.proTitle;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}
	
    /**
     * @return Type
     */	
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param workPlaceName
	 */
	public void setWorkPlaceName(String workPlaceName) {
		this.workPlaceName = workPlaceName == null ? null : workPlaceName.trim();
	}
	
    /**
     * @return WorkPlaceName
     */	
	public String getWorkPlaceName() {
		return this.workPlaceName;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode == null ? null : unifyCreditCode.trim();
	}
	
    /**
     * @return UnifyCreditCode
     */	
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param addrProvience
	 */
	public void setAddrProvience(String addrProvience) {
		this.addrProvience = addrProvience == null ? null : addrProvience.trim();
	}
	
    /**
     * @return AddrProvience
     */	
	public String getAddrProvience() {
		return this.addrProvience;
	}
	
	/**
	 * @param addrCity
	 */
	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity == null ? null : addrCity.trim();
	}
	
    /**
     * @return AddrCity
     */	
	public String getAddrCity() {
		return this.addrCity;
	}
	
	/**
	 * @param addrArear
	 */
	public void setAddrArear(String addrArear) {
		this.addrArear = addrArear == null ? null : addrArear.trim();
	}
	
    /**
     * @return AddrArear
     */	
	public String getAddrArear() {
		return this.addrArear;
	}
	
	/**
	 * @param addrDetail
	 */
	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail == null ? null : addrDetail.trim();
	}
	
    /**
     * @return AddrDetail
     */	
	public String getAddrDetail() {
		return this.addrDetail;
	}
	
	/**
	 * @param kind
	 */
	public void setKind(String kind) {
		this.kind = kind == null ? null : kind.trim();
	}
	
    /**
     * @return Kind
     */	
	public String getKind() {
		return this.kind;
	}
	
	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel == null ? null : tel.trim();
	}
	
    /**
     * @return Tel
     */	
	public String getTel() {
		return this.tel;
	}
	
	/**
	 * @param comeinDt
	 */
	public void setComeinDt(String comeinDt) {
		this.comeinDt = comeinDt == null ? null : comeinDt.trim();
	}
	
    /**
     * @return ComeinDt
     */	
	public String getComeinDt() {
		return this.comeinDt;
	}
	
	/**
	 * @param dep
	 */
	public void setDep(String dep) {
		this.dep = dep == null ? null : dep.trim();
	}
	
    /**
     * @return Dep
     */	
	public String getDep() {
		return this.dep;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}