/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditReportQryRecord;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryRecordMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportQryRecordService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 09:30:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditReportQryRecordService {

    @Autowired
    private CreditReportQryRecordMapper creditReportQryRecordMapper;

    @Autowired
    private HttpServletRequest request;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditReportQryRecord selectByPrimaryKey(String crqrSerno) {
        return creditReportQryRecordMapper.selectByPrimaryKey(crqrSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditReportQryRecord> selectAll(QueryModel model) {
        List<CreditReportQryRecord> records = (List<CreditReportQryRecord>) creditReportQryRecordMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditReportQryRecord> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditReportQryRecord> list = creditReportQryRecordMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditReportQryRecord record) {
        return creditReportQryRecordMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditReportQryRecord record) {
        return creditReportQryRecordMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditReportQryRecord record) {
        return creditReportQryRecordMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditReportQryRecord record) {
        return creditReportQryRecordMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String crqrSerno) {
        return creditReportQryRecordMapper.deleteByPrimaryKey(crqrSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditReportQryRecordMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: createCreditReport
     * @方法描述: 新增
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int createCreditReport(CreditReportQryRecord record) {
        User userInfo = SessionUtils.getUserInformation();
        record.setManagerId(userInfo.getLoginCode());
        record.setManagerBrId(userInfo.getOrg().getCode());
        record.setDutyCde(userInfo.getOrg().getId());
        record.setQryTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setIp(getIpAddress(request));
        // 判断是否存在同一用户同一ip下有无征信报告查看记录，有则更新，无则新增
        List<CreditReportQryRecord> count = creditReportQryRecordMapper.getCreditRepot(record);
        if(count.size() == 0){
            creditReportQryRecordMapper.insert(record);
        }
        return 0;
    }

    public String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if(!StringUtils.isEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
            int index = ip.indexOf(",");
            if(index != -1){
                return ip.substring(index+1,ip.length());
            } else {
                return ip;
            }
        }
        ip = request.getHeader("X-Real-IP");
        if(!StringUtils.isEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
            return ip;
        }
        return request.getRemoteAddr();
    }
}
