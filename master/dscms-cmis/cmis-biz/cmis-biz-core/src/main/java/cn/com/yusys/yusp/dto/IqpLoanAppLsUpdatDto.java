package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.IqpLoanAppAssist;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanApp
 * @类描述: iqp_loan_app数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-10 14:42:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanAppLsUpdatDto implements Serializable{
    private static final long serialVersionUID = 1L;

    /** 业务申请流水号 **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "IQP_SERNO")
    private String iqpSerno;

    /** 全局流水号 **/
    @Column(name = "SERNO", unique = false, nullable = true, length = 40)
    private String serno;

    /** 原申请流水号 **/
    @Column(name = "OLD_IQP_SERNO", unique = false, nullable = true, length = 40)
    private String oldIqpSerno;

    /** 是否复议 **/
    @Column(name = "IS_RECONSID", unique = false, nullable = true, length = 5)
    private String isReconsid;

    /** 客户编号 **/
    @Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
    private String cusId;

    /** 客户名称 **/
    @Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
    private String cusName;

    /** 证件号码 **/
    @Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
    private String certCode;

    /** 手机号码 **/
    @Column(name = "PHONE", unique = false, nullable = true, length = 11)
    private String phone;

    /** 业务类型 **/
    @Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
    private String bizType;

    /** 特殊业务类型 **/
    @Column(name = "ESPEC_BIZ_TYPE", unique = false, nullable = true, length = 5)
    private String especBizType;

    /** 申请日期 **/
    @Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
    private String appDate;

    /** 产品编号 **/
    @Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
    private String prdId;

    /** 产品名称 **/
    @Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /** 贷款用途 **/
    @Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
    private String loanPurp;

    /** 贷款形式 **/
    @Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
    private String loanModal;

    /** 贷款性质 **/
    @Column(name = "LOAN_CHA", unique = false, nullable = true, length = 5)
    private String loanCha;

    /** 是否申请优惠利率 **/
    @Column(name = "PREFER_RATE_FLAG", unique = false, nullable = true, length = 1)
    private String preferRateFlag;

    /** 利率调整日 **/
    @Column(name = "RATE_ADJ_DATE", unique = false, nullable = true, length = 5)
    private String rateAdjDate;

    /** 其他消费贷款月还款额 **/
    @Column(name = "OTHER_COMSUME_REPAY_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal otherComsumeRepayAmt;

    /** 本笔公积金贷款月还款额 **/
    @Column(name = "PUND_LOAN_MON_REPAY_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal pundLoanMonRepayAmt;

    /** 本笔月还款额 **/
    @Column(name = "MONTH_REPAY_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal monthRepayAmt;

    /** 是否在线抵押 **/
    @Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 1)
    private String isOlPld;

    /** 合同模式 **/
    @Column(name = "CONT_MODE", unique = false, nullable = true, length = 5)
    private String contMode;

    /** 第三方标识 **/
    @Column(name = "THIRD_PARTY_FLAG", unique = false, nullable = true, length = 1)
    private String thirdPartyFlag;

    /** 项目编号 **/
    @Column(name = "PRO_NO", unique = false, nullable = true, length = 40)
    private String proNo;

    /** 项目流水号 **/
    @Column(name = "PRO_SERNO", unique = false, nullable = true, length = 40)
    private String proSerno;

    /** 调查人意见 **/
    @Column(name = "INVE_ADVICE", unique = false, nullable = true, length = 500)
    private String inveAdvice;

    /** 个人信用情况其他说明 **/
    @Column(name = "INDIV_CDT_EXPL", unique = false, nullable = true, length = 500)
    private String indivCdtExpl;

    /** 批复编号 **/
    @Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
    private String replyNo;

    /** 调查编号 **/
    @Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
    private String surveySerno;

    /** 合同类型 **/
    @Column(name = "CONT_TYPE", unique = false, nullable = true, length = 5)
    private String contType;

    /** 币种 **/
    @Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /** 合同金额 **/
    @Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contAmt;

    /** 合同期限 **/
    @Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
    private Integer contTerm;

    /** 起始日 **/
    @Column(name = "START_DATE", unique = false, nullable = true, length = 10)
    private String startDate;

    /** 到期日 **/
    @Column(name = "END_DATE", unique = false, nullable = true, length = 10)
    private String endDate;

    /** 签约方式 **/
    @Column(name = "SIGN_MODE", unique = false, nullable = true, length = 5)
    private String signMode;

    /** 贷款投向 **/
    @Column(name = "LOAN_TER", unique = false, nullable = true, length = 40)
    private String loanTer;

    /** 其他约定 **/
    @Column(name = "OTHER_AGREED", unique = false, nullable = true, length = 500)
    private String otherAgreed;

    /** 还款方式 **/
    @Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
    private String repayMode;

    /** 借款种类 **/
    @Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
    private String loanType;

    /** 浮动点数 **/
    @Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rateFloatPoint;

    /** 结息方式 **/
    @Column(name = "EI_MODE", unique = false, nullable = true, length = 5)
    private String eiMode;

    /** 贷款发放账号 **/
    @Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 40)
    private String loanPayoutAccno;

    /** 贷款发放账号名称 **/
    @Column(name = "LOAN_PAYOUT_ACC_NAME", unique = false, nullable = true, length = 40)
    private String loanPayoutAccName;

    /** 是否曾被拒绝 **/
    @Column(name = "IS_HAS_REFUSED", unique = false, nullable = true, length = 5)
    private String isHasRefused;

    /** 主担保方式 **/
    @Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
    private String guarWay;

    /** 是否共同申请人 **/
    @Column(name = "IS_COMMON_RQSTR", unique = false, nullable = true, length = 5)
    private String isCommonRqstr;

    /** 是否确认支付方式 **/
    @Column(name = "IS_CFIRM_PAY_WAY", unique = false, nullable = true, length = 5)
    private String isCfirmPayWay;

    /** 支付方式 **/
    @Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
    private String payMode;

    /** 申请币种 **/
    @Column(name = "APP_CUR_TYPE", unique = false, nullable = true, length = 5)
    private String appCurType;

    /** 申请金额 **/
    @Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal appAmt;

    /** 申请汇率 **/
    @Column(name = "APP_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal appRate;

    /** 保证金来源 **/
    @Column(name = "BAIL_SOUR", unique = false, nullable = true, length = 5)
    private String bailSour;

    /** 保证金比例 **/
    @Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bailPerc;

    /** 保证金币种 **/
    @Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
    private String bailCurType;

    /** 保证金金额 **/
    @Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bailAmt;

    /** 保证金汇率 **/
    @Column(name = "BAIL_EXCHANGE_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bailExchangeRate;

    /** 风险敞口金额 **/
    @Column(name = "RISK_OPEN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal riskOpenAmt;

    /** 期限类型 **/
    @Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
    private String termType;

    /** 申请期限 **/
    @Column(name = "APP_TERM", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal appTerm;

    /** 利率依据方式 **/
    @Column(name = "IR_ACCORD_TYPE", unique = false, nullable = true, length = 5)
    private String irAccordType;

    /** 利率种类 **/
    @Column(name = "IR_TYPE", unique = false, nullable = true, length = 6)
    private String irType;

    /** 基准利率（年） **/
    @Column(name = "RULING_IR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rulingIr;

    /** 对应基准利率(月) **/
    @Column(name = "RULING_IR_M", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rulingIrM;

    /** 计息方式 **/
    @Column(name = "LOAN_RAT_TYPE", unique = false, nullable = true, length = 5)
    private String loanRatType;

    /** 利率调整类型 **/
    @Column(name = "IR_ADJUST_TYPE", unique = false, nullable = true, length = 5)
    private String irAdjustType;

    /** 利率调整周期(月) **/
    @Column(name = "IR_ADJUST_TERM", unique = false, nullable = true, length = 10)
    private Integer irAdjustTerm;

    /** 调息方式 **/
    @Column(name = "PRA_TYPE", unique = false, nullable = true, length = 5)
    private String praType;

    /** 利率形式 **/
    @Column(name = "RATE_TYPE", unique = false, nullable = true, length = 5)
    private String rateType;

    /** 利率浮动方式 **/
    @Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
    private String irFloatType;

    /** 利率浮动百分比 **/
    @Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal irFloatRate;

    /** 固定加点值 **/
    @Column(name = "IR_FLOAT_POINT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal irFloatPoint;

    /** 执行年利率 **/
    @Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /** 执行利率(月) **/
    @Column(name = "REALITY_IR_M", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal realityIrM;

    /** 逾期利率浮动方式 **/
    @Column(name = "OVERDUE_FLOAT_TYPE", unique = false, nullable = true, length = 5)
    private String overdueFloatType;

    /** 逾期利率加点值 **/
    @Column(name = "OVERDUE_POINT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overduePoint;

    /** 逾期利率浮动百分比 **/
    @Column(name = "OVERDUE_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdueRate;

    /** 逾期利率（年） **/
    @Column(name = "OVERDUE_RATE_Y", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdueRateY;

    /** 违约利率浮动方式 **/
    @Column(name = "DEFAULT_FLOAT_TYPE", unique = false, nullable = true, length = 5)
    private String defaultFloatType;

    /** 违约利率浮动加点值 **/
    @Column(name = "DEFAULT_POINT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal defaultPoint;

    /** 违约利率浮动百分比 **/
    @Column(name = "DEFAULT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal defaultRate;

    /** 违约利率（年） **/
    @Column(name = "DEFAULT_RATE_Y", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal defaultRateY;

    /** 申请状态 **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /** 审批通过日期（精确到秒） **/
    @Column(name = "APPROVE_PASS_DATE", unique = false, nullable = true, length = 20)
    private String approvePassDate;

    /** 本合同项下最高可用信金额 **/
    @Column(name = "CONT_HIGH_AVL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contHighAvlAmt;

    /** 是否续签 **/
    @Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
    private String isRenew;

    /** 原合同编号 **/
    @Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
    private String origiContNo;

    /** 是否使用授信额度 **/
    @Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
    private String isUtilLmt;

    /** 授信额度编号 **/
    @Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
    private String lmtAccNo;

    /** 是否电子用印 **/
    @Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
    private String isESeal;

    /** 所属条线 **/
    @Column(name = "BELG_LINE", unique = false, nullable = true, length = 4)
    private String belgLine;

    /** 债项等级 **/
    @Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 5)
    private String debtLevel;

    /** 违约风险暴露EAD **/
    @Column(name = "EAD", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal ead;

    /** 违约损失率LGD **/
    @Column(name = "LGD", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lgd;

    /** 联系人 **/
    @Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
    private String linkman;

    /** 传真 **/
    @Column(name = "FAX", unique = false, nullable = true, length = 20)
    private String fax;

    /** 邮箱 **/
    @Column(name = "EMAIL", unique = false, nullable = true, length = 80)
    private String email;

    /** QQ **/
    @Column(name = "QQ", unique = false, nullable = true, length = 20)
    private String qq;

    /** 微信 **/
    @Column(name = "WECHAT", unique = false, nullable = true, length = 40)
    private String wechat;

    /** 送达地址 **/
    @Column(name = "DELIVERY_ADDR", unique = false, nullable = true, length = 500)
    private String deliveryAddr;

    /** 本行角色 **/
    @Column(name = "BANK_ROLE", unique = false, nullable = true, length = 40)
    private String bankRole;

    /** 银团总金额 **/
    @Column(name = "BKSYNDIC_TOTL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal bksyndicTotlAmt;

    /** 还款顺序 **/
    @Column(name = "REPAY_SEQ", unique = false, nullable = true, length = 40)
    private String repaySeq;

    /** 银团纸质合同编号 **/
    @Column(name = "BKSYNDIC_PAPER_CONT_NO", unique = false, nullable = true, length = 40)
    private String bksyndicPaperContNo;

    /** 借款利率调整日 **/
    @Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 5)
    private String loanRateAdjDay;

    /** LPR利率区间 **/
    @Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
    private String lprRateIntval;

    /** 当前LPR利率 **/
    @Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal curtLprRate;

    /** LPR浮动点(BP) **/
    @Column(name = "LPR_BP", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal lprBp;

    /** 提款方式  **/
    @Column(name = "DRAW_MODE", unique = false, nullable = true, length = 5)
    private String drawMode;

    /** 提款期限 **/
    @Column(name = "DRAW_TERM", unique = false, nullable = true, length = 40)
    private String drawTerm;

    /** 开户行名称 **/
    @Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 40)
    private String acctsvcrName;

    /** 还款具体说明 **/
    @Column(name = "REPAY_DETAIL", unique = false, nullable = true, length = 500)
    private String repayDetail;

    /** 目标企业 **/
    @Column(name = "TARGET_CORP", unique = false, nullable = true, length = 500)
    private String targetCorp;

    /** 并购协议 **/
    @Column(name = "MERGER_AGR", unique = false, nullable = true, length = 500)
    private String mergerAgr;

    /** 并购交易价款 **/
    @Column(name = "MERGER_TRAN_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal mergerTranAmt;

    /** 是否占用第三方额度 **/
    @Column(name = "IS_OUTSTND_TRD_LMT_AMT", unique = false, nullable = true, length = 5)
    private String isOutstndTrdLmtAmt;

    /** 第三方合同协议编号 **/
    @Column(name = "TDP_AGR_NO", unique = false, nullable = true, length = 40)
    private String tdpAgrNo;

    /** 合作方客户编号 **/
    @Column(name = "COOP_CUS_ID", unique = false, nullable = true, length = 20)
    private String coopCusId;

    /** 合作方客户名称 **/
    @Column(name = "COOP_CUS_NAME", unique = false, nullable = true, length = 80)
    private String coopCusName;

    /** 是否无缝对接 **/
    @Column(name = "IS_SEAJNT", unique = false, nullable = true, length = 5)
    private String isSeajnt;

    /** 折算人民币金额 **/
    @Column(name = "CVT_CNY_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal cvtCnyAmt;

    /** 用途分析 **/
    @Column(name = "PURP_ANALY", unique = false, nullable = true, length = 500)
    private String purpAnaly;

    /** 交叉核验详细分析 **/
    @Column(name = "CROSS_CHK_DETAIL_ANALY", unique = false, nullable = true, length = 500)
    private String crossChkDetailAnaly;

    /** 还款来源 **/
    @Column(name = "REPAY_SOUR", unique = false, nullable = true, length = 500)
    private String repaySour;

    /** 调查人结论 **/
    @Column(name = "INVE_CONCLU", unique = false, nullable = true, length = 500)
    private String inveConclu;

    /** 操作类型 **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /** 主管机构 **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /** 主管客户经理 **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /** 登记人 **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /** 登记机构 **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /** 登记日期 **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /** 最后修改人 **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /** 最后修改机构 **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /** 最后修改日期 **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /** 创建时间 **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /** 修改时间 **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /** 首付/定金 **/
    @Column(name = "FIRSTPAY_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal firstpayAmt;

    /** 合同总价 **/
    @Column(name = "CONT_TOTAL_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contTotalAmt;

    /** 宽限期 **/
    @Column(name = "GRAPER", unique = false, nullable = true, length = 20)
    private String graper;

    /** 是否公积金组合贷款 **/
    @Column(name = "FUND_UNION_FLAG", unique = false, nullable = true, length = 5)
    private String fundUnionFlag;

    /** 是否线上提款 **/
    @Column(name = "IS_ONLINE_DRAW", unique = false, nullable = true, length = 10)
    private String isOnlineDraw;

    /** 公积金贷款金额(元) **/
    @Column(name = "PUND_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal pundAmt;

    /** 公积金贷款利率(%) **/
    @Column(name = "PUND_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal pundRate;
    

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getOldIqpSerno() {
        return oldIqpSerno;
    }

    public void setOldIqpSerno(String oldIqpSerno) {
        this.oldIqpSerno = oldIqpSerno;
    }

    public String getIsReconsid() {
        return isReconsid;
    }

    public void setIsReconsid(String isReconsid) {
        this.isReconsid = isReconsid;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getEspecBizType() {
        return especBizType;
    }

    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanPurp() {
        return loanPurp;
    }

    public void setLoanPurp(String loanPurp) {
        this.loanPurp = loanPurp;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getLoanCha() {
        return loanCha;
    }

    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha;
    }

    public String getPreferRateFlag() {
        return preferRateFlag;
    }

    public void setPreferRateFlag(String preferRateFlag) {
        this.preferRateFlag = preferRateFlag;
    }

    public String getRateAdjDate() {
        return rateAdjDate;
    }

    public void setRateAdjDate(String rateAdjDate) {
        this.rateAdjDate = rateAdjDate;
    }

    public BigDecimal getOtherComsumeRepayAmt() {
        return otherComsumeRepayAmt;
    }

    public void setOtherComsumeRepayAmt(BigDecimal otherComsumeRepayAmt) {
        this.otherComsumeRepayAmt = otherComsumeRepayAmt;
    }

    public BigDecimal getPundLoanMonRepayAmt() {
        return pundLoanMonRepayAmt;
    }

    public void setPundLoanMonRepayAmt(BigDecimal pundLoanMonRepayAmt) {
        this.pundLoanMonRepayAmt = pundLoanMonRepayAmt;
    }

    public BigDecimal getMonthRepayAmt() {
        return monthRepayAmt;
    }

    public void setMonthRepayAmt(BigDecimal monthRepayAmt) {
        this.monthRepayAmt = monthRepayAmt;
    }

    public String getIsOlPld() {
        return isOlPld;
    }

    public void setIsOlPld(String isOlPld) {
        this.isOlPld = isOlPld;
    }

    public String getContMode() {
        return contMode;
    }

    public void setContMode(String contMode) {
        this.contMode = contMode;
    }

    public String getThirdPartyFlag() {
        return thirdPartyFlag;
    }

    public void setThirdPartyFlag(String thirdPartyFlag) {
        this.thirdPartyFlag = thirdPartyFlag;
    }

    public String getProNo() {
        return proNo;
    }

    public void setProNo(String proNo) {
        this.proNo = proNo;
    }

    public String getProSerno() {
        return proSerno;
    }

    public void setProSerno(String proSerno) {
        this.proSerno = proSerno;
    }

    public String getInveAdvice() {
        return inveAdvice;
    }

    public void setInveAdvice(String inveAdvice) {
        this.inveAdvice = inveAdvice;
    }

    public String getIndivCdtExpl() {
        return indivCdtExpl;
    }

    public void setIndivCdtExpl(String indivCdtExpl) {
        this.indivCdtExpl = indivCdtExpl;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public Integer getContTerm() {
        return contTerm;
    }

    public void setContTerm(Integer contTerm) {
        this.contTerm = contTerm;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSignMode() {
        return signMode;
    }

    public void setSignMode(String signMode) {
        this.signMode = signMode;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getOtherAgreed() {
        return otherAgreed;
    }

    public void setOtherAgreed(String otherAgreed) {
        this.otherAgreed = otherAgreed;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutAccName() {
        return loanPayoutAccName;
    }

    public void setLoanPayoutAccName(String loanPayoutAccName) {
        this.loanPayoutAccName = loanPayoutAccName;
    }

    public String getIsHasRefused() {
        return isHasRefused;
    }

    public void setIsHasRefused(String isHasRefused) {
        this.isHasRefused = isHasRefused;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getIsCommonRqstr() {
        return isCommonRqstr;
    }

    public void setIsCommonRqstr(String isCommonRqstr) {
        this.isCommonRqstr = isCommonRqstr;
    }

    public String getIsCfirmPayWay() {
        return isCfirmPayWay;
    }

    public void setIsCfirmPayWay(String isCfirmPayWay) {
        this.isCfirmPayWay = isCfirmPayWay;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getAppCurType() {
        return appCurType;
    }

    public void setAppCurType(String appCurType) {
        this.appCurType = appCurType;
    }

    public BigDecimal getAppAmt() {
        return appAmt;
    }

    public void setAppAmt(BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    public BigDecimal getAppRate() {
        return appRate;
    }

    public void setAppRate(BigDecimal appRate) {
        this.appRate = appRate;
    }

    public String getBailSour() {
        return bailSour;
    }

    public void setBailSour(String bailSour) {
        this.bailSour = bailSour;
    }

    public BigDecimal getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(BigDecimal bailPerc) {
        this.bailPerc = bailPerc;
    }

    public String getBailCurType() {
        return bailCurType;
    }

    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public BigDecimal getBailExchangeRate() {
        return bailExchangeRate;
    }

    public void setBailExchangeRate(BigDecimal bailExchangeRate) {
        this.bailExchangeRate = bailExchangeRate;
    }

    public BigDecimal getRiskOpenAmt() {
        return riskOpenAmt;
    }

    public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
        this.riskOpenAmt = riskOpenAmt;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public BigDecimal getAppTerm() {
        return appTerm;
    }

    public void setAppTerm(BigDecimal appTerm) {
        this.appTerm = appTerm;
    }

    public String getIrAccordType() {
        return irAccordType;
    }

    public void setIrAccordType(String irAccordType) {
        this.irAccordType = irAccordType;
    }

    public String getIrType() {
        return irType;
    }

    public void setIrType(String irType) {
        this.irType = irType;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public BigDecimal getRulingIrM() {
        return rulingIrM;
    }

    public void setRulingIrM(BigDecimal rulingIrM) {
        this.rulingIrM = rulingIrM;
    }

    public String getLoanRatType() {
        return loanRatType;
    }

    public void setLoanRatType(String loanRatType) {
        this.loanRatType = loanRatType;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public Integer getIrAdjustTerm() {
        return irAdjustTerm;
    }

    public void setIrAdjustTerm(Integer irAdjustTerm) {
        this.irAdjustTerm = irAdjustTerm;
    }

    public String getPraType() {
        return praType;
    }

    public void setPraType(String praType) {
        this.praType = praType;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public BigDecimal getIrFloatPoint() {
        return irFloatPoint;
    }

    public void setIrFloatPoint(BigDecimal irFloatPoint) {
        this.irFloatPoint = irFloatPoint;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getRealityIrM() {
        return realityIrM;
    }

    public void setRealityIrM(BigDecimal realityIrM) {
        this.realityIrM = realityIrM;
    }

    public String getOverdueFloatType() {
        return overdueFloatType;
    }

    public void setOverdueFloatType(String overdueFloatType) {
        this.overdueFloatType = overdueFloatType;
    }

    public BigDecimal getOverduePoint() {
        return overduePoint;
    }

    public void setOverduePoint(BigDecimal overduePoint) {
        this.overduePoint = overduePoint;
    }

    public BigDecimal getOverdueRate() {
        return overdueRate;
    }

    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    public BigDecimal getOverdueRateY() {
        return overdueRateY;
    }

    public void setOverdueRateY(BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    public String getDefaultFloatType() {
        return defaultFloatType;
    }

    public void setDefaultFloatType(String defaultFloatType) {
        this.defaultFloatType = defaultFloatType;
    }

    public BigDecimal getDefaultPoint() {
        return defaultPoint;
    }

    public void setDefaultPoint(BigDecimal defaultPoint) {
        this.defaultPoint = defaultPoint;
    }

    public BigDecimal getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    public BigDecimal getDefaultRateY() {
        return defaultRateY;
    }

    public void setDefaultRateY(BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApprovePassDate() {
        return approvePassDate;
    }

    public void setApprovePassDate(String approvePassDate) {
        this.approvePassDate = approvePassDate;
    }

    public BigDecimal getContHighAvlAmt() {
        return contHighAvlAmt;
    }

    public void setContHighAvlAmt(BigDecimal contHighAvlAmt) {
        this.contHighAvlAmt = contHighAvlAmt;
    }

    public String getIsRenew() {
        return isRenew;
    }

    public void setIsRenew(String isRenew) {
        this.isRenew = isRenew;
    }

    public String getOrigiContNo() {
        return origiContNo;
    }

    public void setOrigiContNo(String origiContNo) {
        this.origiContNo = origiContNo;
    }

    public String getIsUtilLmt() {
        return isUtilLmt;
    }

    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getIsESeal() {
        return isESeal;
    }

    public void setIsESeal(String isESeal) {
        this.isESeal = isESeal;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getDebtLevel() {
        return debtLevel;
    }

    public void setDebtLevel(String debtLevel) {
        this.debtLevel = debtLevel;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getBankRole() {
        return bankRole;
    }

    public void setBankRole(String bankRole) {
        this.bankRole = bankRole;
    }

    public BigDecimal getBksyndicTotlAmt() {
        return bksyndicTotlAmt;
    }

    public void setBksyndicTotlAmt(BigDecimal bksyndicTotlAmt) {
        this.bksyndicTotlAmt = bksyndicTotlAmt;
    }

    public String getRepaySeq() {
        return repaySeq;
    }

    public void setRepaySeq(String repaySeq) {
        this.repaySeq = repaySeq;
    }

    public String getBksyndicPaperContNo() {
        return bksyndicPaperContNo;
    }

    public void setBksyndicPaperContNo(String bksyndicPaperContNo) {
        this.bksyndicPaperContNo = bksyndicPaperContNo;
    }

    public String getLoanRateAdjDay() {
        return loanRateAdjDay;
    }

    public void setLoanRateAdjDay(String loanRateAdjDay) {
        this.loanRateAdjDay = loanRateAdjDay;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getLprBp() {
        return lprBp;
    }

    public void setLprBp(BigDecimal lprBp) {
        this.lprBp = lprBp;
    }

    public String getDrawMode() {
        return drawMode;
    }

    public void setDrawMode(String drawMode) {
        this.drawMode = drawMode;
    }

    public String getDrawTerm() {
        return drawTerm;
    }

    public void setDrawTerm(String drawTerm) {
        this.drawTerm = drawTerm;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getRepayDetail() {
        return repayDetail;
    }

    public void setRepayDetail(String repayDetail) {
        this.repayDetail = repayDetail;
    }

    public String getTargetCorp() {
        return targetCorp;
    }

    public void setTargetCorp(String targetCorp) {
        this.targetCorp = targetCorp;
    }

    public String getMergerAgr() {
        return mergerAgr;
    }

    public void setMergerAgr(String mergerAgr) {
        this.mergerAgr = mergerAgr;
    }

    public BigDecimal getMergerTranAmt() {
        return mergerTranAmt;
    }

    public void setMergerTranAmt(BigDecimal mergerTranAmt) {
        this.mergerTranAmt = mergerTranAmt;
    }

    public String getIsOutstndTrdLmtAmt() {
        return isOutstndTrdLmtAmt;
    }

    public void setIsOutstndTrdLmtAmt(String isOutstndTrdLmtAmt) {
        this.isOutstndTrdLmtAmt = isOutstndTrdLmtAmt;
    }

    public String getTdpAgrNo() {
        return tdpAgrNo;
    }

    public void setTdpAgrNo(String tdpAgrNo) {
        this.tdpAgrNo = tdpAgrNo;
    }

    public String getCoopCusId() {
        return coopCusId;
    }

    public void setCoopCusId(String coopCusId) {
        this.coopCusId = coopCusId;
    }

    public String getCoopCusName() {
        return coopCusName;
    }

    public void setCoopCusName(String coopCusName) {
        this.coopCusName = coopCusName;
    }

    public String getIsSeajnt() {
        return isSeajnt;
    }

    public void setIsSeajnt(String isSeajnt) {
        this.isSeajnt = isSeajnt;
    }

    public BigDecimal getCvtCnyAmt() {
        return cvtCnyAmt;
    }

    public void setCvtCnyAmt(BigDecimal cvtCnyAmt) {
        this.cvtCnyAmt = cvtCnyAmt;
    }

    public String getPurpAnaly() {
        return purpAnaly;
    }

    public void setPurpAnaly(String purpAnaly) {
        this.purpAnaly = purpAnaly;
    }

    public String getCrossChkDetailAnaly() {
        return crossChkDetailAnaly;
    }

    public void setCrossChkDetailAnaly(String crossChkDetailAnaly) {
        this.crossChkDetailAnaly = crossChkDetailAnaly;
    }

    public String getRepaySour() {
        return repaySour;
    }

    public void setRepaySour(String repaySour) {
        this.repaySour = repaySour;
    }

    public String getInveConclu() {
        return inveConclu;
    }

    public void setInveConclu(String inveConclu) {
        this.inveConclu = inveConclu;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public BigDecimal getFirstpayAmt() {
        return firstpayAmt;
    }

    public void setFirstpayAmt(BigDecimal firstpayAmt) {
        this.firstpayAmt = firstpayAmt;
    }

    public BigDecimal getContTotalAmt() {
        return contTotalAmt;
    }

    public void setContTotalAmt(BigDecimal contTotalAmt) {
        this.contTotalAmt = contTotalAmt;
    }

    public String getGraper() {
        return graper;
    }

    public void setGraper(String graper) {
        this.graper = graper;
    }

    public String getFundUnionFlag() {
        return fundUnionFlag;
    }

    public void setFundUnionFlag(String fundUnionFlag) {
        this.fundUnionFlag = fundUnionFlag;
    }

    public String getIsOnlineDraw() {
        return isOnlineDraw;
    }

    public void setIsOnlineDraw(String isOnlineDraw) {
        this.isOnlineDraw = isOnlineDraw;
    }

    public BigDecimal getPundAmt() {
        return pundAmt;
    }

    public void setPundAmt(BigDecimal pundAmt) {
        this.pundAmt = pundAmt;
    }

    public BigDecimal getPundRate() {
        return pundRate;
    }

    public void setPundRate(BigDecimal pundRate) {
        this.pundRate = pundRate;
    }

}
