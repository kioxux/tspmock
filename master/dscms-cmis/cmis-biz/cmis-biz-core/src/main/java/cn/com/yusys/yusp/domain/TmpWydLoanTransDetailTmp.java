/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydLoanTransDetail
 * @类描述: tmp_wyd_loan_trans_detail数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_loan_trans_detail_tmp")
public class TmpWydLoanTransDetailTmp {
	
	/** 系统调用流水号 **/
	@Id
	@Column(name = "SYS_TRANS_ID")
	private String sysTransId;
	
	/** 核心流水号 **/
	@Column(name = "CONSUMER_TRANS_ID", unique = false, nullable = true, length = 40)
	private String consumerTransId;
	
	/** 交易流水号 **/
	@Column(name = "TXN_SEQ", unique = false, nullable = true, length = 40)
	private String txnSeq;
	
	/** 交易日期 **/
	@Column(name = "TXN_DATE", unique = false, nullable = true, length = 10)
	private String txnDate;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 逻辑卡号 **/
	@Column(name = "LOGICAL_CARD_NO", unique = false, nullable = true, length = 32)
	private String logicalCardNo;
	
	/** 借据号 **/
	@Column(name = "LENDING_REF", unique = false, nullable = true, length = 64)
	private String lendingRef;
	
	/** 币种 **/
	@Column(name = "CURR_CD", unique = false, nullable = true, length = 10)
	private String currCd;
	
	/** 交易码 **/
	@Column(name = "TXN_CODE", unique = false, nullable = true, length = 10)
	private String txnCode;
	
	/** 交易描述 **/
	@Column(name = "TXN_DESC", unique = false, nullable = true, length = 32)
	private String txnDesc;
	
	/** 借贷标记 **/
	@Column(name = "DB_CR_IND", unique = false, nullable = true, length = 2)
	private String dbCrInd;
	
	/** 入账金额  放款金额或 实还本金 **/
	@Column(name = "POST_AMT", unique = false, nullable = true, length = 22)
	private String postAmt;
	
	/** 入账方式  -- 自主受托 code **/
	@Column(name = "POST_GL_IND", unique = false, nullable = true, length = 5)
	private String postGlInd;
	
	/** 支行 **/
	@Column(name = "OWNING_BRANCH", unique = false, nullable = true, length = 32)
	private String owningBranch;
	
	/** 科目 **/
	@Column(name = "SUBJECT", unique = false, nullable = true, length = 20)
	private String subject;
	
	/** 红蓝字标识 **/
	@Column(name = "RED_FLAG", unique = false, nullable = true, length = 2)
	private String redFlag;
	
	/** 排序 **/
	@Column(name = "QUEUE", unique = false, nullable = true, length = 32)
	private String queue;
	
	/** 账龄组 **/
	@Column(name = "AGE_GROUP", unique = false, nullable = true, length = 32)
	private String ageGroup;
	
	/** 余额成分组 **/
	@Column(name = "BNP_GROUP", unique = false, nullable = true, length = 32)
	private String bnpGroup;
	
	/** 银团代码 **/
	@Column(name = "BANK_GROUP_ID", unique = false, nullable = true, length = 32)
	private String bankGroupId;
	
	/** 银行代码 **/
	@Column(name = "BANK_NO", unique = false, nullable = true, length = 32)
	private String bankNo;
	
	/** 期数 **/
	@Column(name = "TERM", unique = false, nullable = true, length = 10)
	private String term;
	
	/** 批量 **/
	@Column(name = "BATCHDATE", unique = false, nullable = true, length = 10)
	private String batchdate;
	
	/** 交易时间 **/
	@Column(name = "TXN_TIME", unique = false, nullable = true, length = 20)
	private String txnTime;
	
	/** 帐号 借据表的结算帐号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;
	
	/** 账户类型 **/
	@Column(name = "ACCT_TYPE", unique = false, nullable = true, length = 10)
	private String acctType;
	
	/** 账户余额 **/
	@Column(name = "TXN_BALANCE", unique = false, nullable = true, length = 22)
	private String txnBalance;
	
	/** 现转标志 **/
	@Column(name = "CASH_FLAG", unique = false, nullable = true, length = 1)
	private String cashFlag;
	
	/** 入账日期 **/
	@Column(name = "POST_DATE", unique = false, nullable = true, length = 10)
	private String postDate;
	
	/** 入账时间 **/
	@Column(name = "POST_TIME", unique = false, nullable = true, length = 20)
	private String postTime;
	
	/** 对方账号 **/
	@Column(name = "YOUR_ACCT_NO", unique = false, nullable = true, length = 40)
	private String yourAcctNo;
	
	/** 对方户名 **/
	@Column(name = "YOUR_ACCT_NAME", unique = false, nullable = true, length = 100)
	private String yourAcctName;
	
	/** 对方行号 **/
	@Column(name = "YOUR_BANK_ID", unique = false, nullable = true, length = 40)
	private String yourBankId;
	
	/** 对方行名 **/
	@Column(name = "YOUR_BANK_NAME", unique = false, nullable = true, length = 100)
	private String yourBankName;
	
	/** 交易渠道编号 **/
	@Column(name = "QCHANNEL_ID", unique = false, nullable = true, length = 10)
	private String qchannelId;
	
	/** 交易柜员号 **/
	@Column(name = "TRASN_USER", unique = false, nullable = true, length = 50)
	private String trasnUser;
	
	/** 柜员流水号 **/
	@Column(name = "UESR_SER_NO", unique = false, nullable = true, length = 32)
	private String uesrSerNo;
	
	/** 授权柜员号 **/
	@Column(name = "AUTH_USER", unique = false, nullable = true, length = 50)
	private String authUser;
	
	/** 主凭证种类 **/
	@Column(name = "VOUCHER_TYPE", unique = false, nullable = true, length = 32)
	private String voucherType;
	
	/** 主凭证号 **/
	@Column(name = "VOUCHER_NO", unique = false, nullable = true, length = 32)
	private String voucherNo;
	
	/** 冲补抹标志 **/
	@Column(name = "TRANS_FLAG", unique = false, nullable = true, length = 1)
	private String transFlag;
	
	/** 代办人姓名 **/
	@Column(name = "AGENT_NAME", unique = false, nullable = true, length = 100)
	private String agentName;
	
	/** 代办人证件类别 **/
	@Column(name = "AGENT_ID_TYPE", unique = false, nullable = true, length = 10)
	private String agentIdType;
	
	/** 代办人证件号码 **/
	@Column(name = "AGENT_ID_NO", unique = false, nullable = true, length = 32)
	private String agentIdNo;
	
	/** 付款人账号 **/
	@Column(name = "PAYER_ACCT", unique = false, nullable = true, length = 40)
	private String payerAcct;
	
	/** 付款人名称 **/
	@Column(name = "PAYER_NAME", unique = false, nullable = true, length = 100)
	private String payerName;
	
	/** 付款人开户行行号 **/
	@Column(name = "PAYER_BRNO", unique = false, nullable = true, length = 40)
	private String payerBrno;
	
	/** 付款人开户行名称 **/
	@Column(name = "PAYER_BRNAME", unique = false, nullable = true, length = 100)
	private String payerBrname;
	
	/** 收款人账号 **/
	@Column(name = "PAYEE_ACCT", unique = false, nullable = true, length = 40)
	private String payeeAcct;
	
	/** 收款人名称 **/
	@Column(name = "PAYEE_NAME", unique = false, nullable = true, length = 100)
	private String payeeName;
	
	/** 收款人开户行行号 **/
	@Column(name = "PAYEE_BRNO", unique = false, nullable = true, length = 40)
	private String payeeBrno;
	
	/** 收款人开户行名称 **/
	@Column(name = "PAYEE_BRNAME", unique = false, nullable = true, length = 100)
	private String payeeBrname;
	
	
	/**
	 * @param consumerTransId
	 */
	public void setConsumerTransId(String consumerTransId) {
		this.consumerTransId = consumerTransId;
	}
	
    /**
     * @return consumerTransId
     */
	public String getConsumerTransId() {
		return this.consumerTransId;
	}
	
	/**
	 * @param sysTransId
	 */
	public void setSysTransId(String sysTransId) {
		this.sysTransId = sysTransId;
	}
	
    /**
     * @return sysTransId
     */
	public String getSysTransId() {
		return this.sysTransId;
	}
	
	/**
	 * @param txnSeq
	 */
	public void setTxnSeq(String txnSeq) {
		this.txnSeq = txnSeq;
	}
	
    /**
     * @return txnSeq
     */
	public String getTxnSeq() {
		return this.txnSeq;
	}
	
	/**
	 * @param txnDate
	 */
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	
    /**
     * @return txnDate
     */
	public String getTxnDate() {
		return this.txnDate;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param logicalCardNo
	 */
	public void setLogicalCardNo(String logicalCardNo) {
		this.logicalCardNo = logicalCardNo;
	}
	
    /**
     * @return logicalCardNo
     */
	public String getLogicalCardNo() {
		return this.logicalCardNo;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param currCd
	 */
	public void setCurrCd(String currCd) {
		this.currCd = currCd;
	}
	
    /**
     * @return currCd
     */
	public String getCurrCd() {
		return this.currCd;
	}
	
	/**
	 * @param txnCode
	 */
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	
    /**
     * @return txnCode
     */
	public String getTxnCode() {
		return this.txnCode;
	}
	
	/**
	 * @param txnDesc
	 */
	public void setTxnDesc(String txnDesc) {
		this.txnDesc = txnDesc;
	}
	
    /**
     * @return txnDesc
     */
	public String getTxnDesc() {
		return this.txnDesc;
	}
	
	/**
	 * @param dbCrInd
	 */
	public void setDbCrInd(String dbCrInd) {
		this.dbCrInd = dbCrInd;
	}
	
    /**
     * @return dbCrInd
     */
	public String getDbCrInd() {
		return this.dbCrInd;
	}
	
	/**
	 * @param postAmt
	 */
	public void setPostAmt(String postAmt) {
		this.postAmt = postAmt;
	}
	
    /**
     * @return postAmt
     */
	public String getPostAmt() {
		return this.postAmt;
	}
	
	/**
	 * @param postGlInd
	 */
	public void setPostGlInd(String postGlInd) {
		this.postGlInd = postGlInd;
	}
	
    /**
     * @return postGlInd
     */
	public String getPostGlInd() {
		return this.postGlInd;
	}
	
	/**
	 * @param owningBranch
	 */
	public void setOwningBranch(String owningBranch) {
		this.owningBranch = owningBranch;
	}
	
    /**
     * @return owningBranch
     */
	public String getOwningBranch() {
		return this.owningBranch;
	}
	
	/**
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
    /**
     * @return subject
     */
	public String getSubject() {
		return this.subject;
	}
	
	/**
	 * @param redFlag
	 */
	public void setRedFlag(String redFlag) {
		this.redFlag = redFlag;
	}
	
    /**
     * @return redFlag
     */
	public String getRedFlag() {
		return this.redFlag;
	}
	
	/**
	 * @param queue
	 */
	public void setQueue(String queue) {
		this.queue = queue;
	}
	
    /**
     * @return queue
     */
	public String getQueue() {
		return this.queue;
	}
	
	/**
	 * @param ageGroup
	 */
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}
	
    /**
     * @return ageGroup
     */
	public String getAgeGroup() {
		return this.ageGroup;
	}
	
	/**
	 * @param bnpGroup
	 */
	public void setBnpGroup(String bnpGroup) {
		this.bnpGroup = bnpGroup;
	}
	
    /**
     * @return bnpGroup
     */
	public String getBnpGroup() {
		return this.bnpGroup;
	}
	
	/**
	 * @param bankGroupId
	 */
	public void setBankGroupId(String bankGroupId) {
		this.bankGroupId = bankGroupId;
	}
	
    /**
     * @return bankGroupId
     */
	public String getBankGroupId() {
		return this.bankGroupId;
	}
	
	/**
	 * @param bankNo
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	
    /**
     * @return bankNo
     */
	public String getBankNo() {
		return this.bankNo;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(String term) {
		this.term = term;
	}
	
    /**
     * @return term
     */
	public String getTerm() {
		return this.term;
	}
	
	/**
	 * @param batchdate
	 */
	public void setBatchdate(String batchdate) {
		this.batchdate = batchdate;
	}
	
    /**
     * @return batchdate
     */
	public String getBatchdate() {
		return this.batchdate;
	}
	
	/**
	 * @param txnTime
	 */
	public void setTxnTime(String txnTime) {
		this.txnTime = txnTime;
	}
	
    /**
     * @return txnTime
     */
	public String getTxnTime() {
		return this.txnTime;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctType
	 */
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
    /**
     * @return acctType
     */
	public String getAcctType() {
		return this.acctType;
	}
	
	/**
	 * @param txnBalance
	 */
	public void setTxnBalance(String txnBalance) {
		this.txnBalance = txnBalance;
	}
	
    /**
     * @return txnBalance
     */
	public String getTxnBalance() {
		return this.txnBalance;
	}
	
	/**
	 * @param cashFlag
	 */
	public void setCashFlag(String cashFlag) {
		this.cashFlag = cashFlag;
	}
	
    /**
     * @return cashFlag
     */
	public String getCashFlag() {
		return this.cashFlag;
	}
	
	/**
	 * @param postDate
	 */
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	
    /**
     * @return postDate
     */
	public String getPostDate() {
		return this.postDate;
	}
	
	/**
	 * @param postTime
	 */
	public void setPostTime(String postTime) {
		this.postTime = postTime;
	}
	
    /**
     * @return postTime
     */
	public String getPostTime() {
		return this.postTime;
	}
	
	/**
	 * @param yourAcctNo
	 */
	public void setYourAcctNo(String yourAcctNo) {
		this.yourAcctNo = yourAcctNo;
	}
	
    /**
     * @return yourAcctNo
     */
	public String getYourAcctNo() {
		return this.yourAcctNo;
	}
	
	/**
	 * @param yourAcctName
	 */
	public void setYourAcctName(String yourAcctName) {
		this.yourAcctName = yourAcctName;
	}
	
    /**
     * @return yourAcctName
     */
	public String getYourAcctName() {
		return this.yourAcctName;
	}
	
	/**
	 * @param yourBankId
	 */
	public void setYourBankId(String yourBankId) {
		this.yourBankId = yourBankId;
	}
	
    /**
     * @return yourBankId
     */
	public String getYourBankId() {
		return this.yourBankId;
	}
	
	/**
	 * @param yourBankName
	 */
	public void setYourBankName(String yourBankName) {
		this.yourBankName = yourBankName;
	}
	
    /**
     * @return yourBankName
     */
	public String getYourBankName() {
		return this.yourBankName;
	}
	
	/**
	 * @param qchannelId
	 */
	public void setQchannelId(String qchannelId) {
		this.qchannelId = qchannelId;
	}
	
    /**
     * @return qchannelId
     */
	public String getQchannelId() {
		return this.qchannelId;
	}
	
	/**
	 * @param trasnUser
	 */
	public void setTrasnUser(String trasnUser) {
		this.trasnUser = trasnUser;
	}
	
    /**
     * @return trasnUser
     */
	public String getTrasnUser() {
		return this.trasnUser;
	}
	
	/**
	 * @param uesrSerNo
	 */
	public void setUesrSerNo(String uesrSerNo) {
		this.uesrSerNo = uesrSerNo;
	}
	
    /**
     * @return uesrSerNo
     */
	public String getUesrSerNo() {
		return this.uesrSerNo;
	}
	
	/**
	 * @param authUser
	 */
	public void setAuthUser(String authUser) {
		this.authUser = authUser;
	}
	
    /**
     * @return authUser
     */
	public String getAuthUser() {
		return this.authUser;
	}
	
	/**
	 * @param voucherType
	 */
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	
    /**
     * @return voucherType
     */
	public String getVoucherType() {
		return this.voucherType;
	}
	
	/**
	 * @param voucherNo
	 */
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	
    /**
     * @return voucherNo
     */
	public String getVoucherNo() {
		return this.voucherNo;
	}
	
	/**
	 * @param transFlag
	 */
	public void setTransFlag(String transFlag) {
		this.transFlag = transFlag;
	}
	
    /**
     * @return transFlag
     */
	public String getTransFlag() {
		return this.transFlag;
	}
	
	/**
	 * @param agentName
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
    /**
     * @return agentName
     */
	public String getAgentName() {
		return this.agentName;
	}
	
	/**
	 * @param agentIdType
	 */
	public void setAgentIdType(String agentIdType) {
		this.agentIdType = agentIdType;
	}
	
    /**
     * @return agentIdType
     */
	public String getAgentIdType() {
		return this.agentIdType;
	}
	
	/**
	 * @param agentIdNo
	 */
	public void setAgentIdNo(String agentIdNo) {
		this.agentIdNo = agentIdNo;
	}
	
    /**
     * @return agentIdNo
     */
	public String getAgentIdNo() {
		return this.agentIdNo;
	}
	
	/**
	 * @param payerAcct
	 */
	public void setPayerAcct(String payerAcct) {
		this.payerAcct = payerAcct;
	}
	
    /**
     * @return payerAcct
     */
	public String getPayerAcct() {
		return this.payerAcct;
	}
	
	/**
	 * @param payerName
	 */
	public void setPayerName(String payerName) {
		this.payerName = payerName;
	}
	
    /**
     * @return payerName
     */
	public String getPayerName() {
		return this.payerName;
	}
	
	/**
	 * @param payerBrno
	 */
	public void setPayerBrno(String payerBrno) {
		this.payerBrno = payerBrno;
	}
	
    /**
     * @return payerBrno
     */
	public String getPayerBrno() {
		return this.payerBrno;
	}
	
	/**
	 * @param payerBrname
	 */
	public void setPayerBrname(String payerBrname) {
		this.payerBrname = payerBrname;
	}
	
    /**
     * @return payerBrname
     */
	public String getPayerBrname() {
		return this.payerBrname;
	}
	
	/**
	 * @param payeeAcct
	 */
	public void setPayeeAcct(String payeeAcct) {
		this.payeeAcct = payeeAcct;
	}
	
    /**
     * @return payeeAcct
     */
	public String getPayeeAcct() {
		return this.payeeAcct;
	}
	
	/**
	 * @param payeeName
	 */
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}
	
    /**
     * @return payeeName
     */
	public String getPayeeName() {
		return this.payeeName;
	}
	
	/**
	 * @param payeeBrno
	 */
	public void setPayeeBrno(String payeeBrno) {
		this.payeeBrno = payeeBrno;
	}
	
    /**
     * @return payeeBrno
     */
	public String getPayeeBrno() {
		return this.payeeBrno;
	}
	
	/**
	 * @param payeeBrname
	 */
	public void setPayeeBrname(String payeeBrname) {
		this.payeeBrname = payeeBrname;
	}
	
    /**
     * @return payeeBrname
     */
	public String getPayeeBrname() {
		return this.payeeBrname;
	}


}