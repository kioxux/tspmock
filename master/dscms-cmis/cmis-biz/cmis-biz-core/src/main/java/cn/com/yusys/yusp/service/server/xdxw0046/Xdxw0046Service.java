package cn.com.yusys.yusp.service.server.xdxw0046;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.xdxw0046.req.Xdxw0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0046.resp.Xdxw0046DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.service.server.xdxw0062.Xdxw0062Service;
import cn.com.yusys.yusp.web.server.xdxw0046.BizXdxw0046Resource;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * 优享贷批复结果反馈
 *
 * @author sunzhen
 * @version 1.0
 */
@Service
public class Xdxw0046Service {

    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0046Resource.class);

    @Autowired
    private Xdxw0062Service xdxw0062Service;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    @Autowired
    private SenddxService senddxService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AreaAdminUserMapper areaAdminUserMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private LmtSurveyTaskDivisMapper lmtSurveyTaskDivisMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    MessageCommonService messageCommonService;

    /**
     * 交易描述：优享贷批复结果反馈
     *
     * @param xdxw0046DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0046DataRespDto insertSurveyResult(Xdxw0046DataReqDto xdxw0046DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataReqDto));
        //返回对象
        Xdxw0046DataRespDto xdxw0046DataRespDto = new Xdxw0046DataRespDto();
        int result = 0;//执行结果
        try {
            //获取请求DTO参数
            String cusName = xdxw0046DataReqDto.getCusName(); //客户名称
            String cusId = xdxw0046DataReqDto.getCusId(); //客户号
            String certType = xdxw0046DataReqDto.getCertType(); //证件类型
            String certNo = xdxw0046DataReqDto.getCertNo(); //证件号
            String areaName = xdxw0046DataReqDto.getAreaName(); //居住地址
            String managerId = xdxw0046DataReqDto.getManagerId(); //客户经理号
            BigDecimal applyAmount = xdxw0046DataReqDto.getApplyAmount(); //申请金额
            BigDecimal reality = xdxw0046DataReqDto.getReality(); //利率
            String loanStartDate = xdxw0046DataReqDto.getLoanStartDate(); //贷款开始时间
            String loanEndDate = xdxw0046DataReqDto.getLoanEndDate(); //贷款结束时间
            String applyTerm = xdxw0046DataReqDto.getApplyTerm(); //贷款期限
            String repayType = xdxw0046DataReqDto.getRepayType(); //还款方式
            String guarWays = xdxw0046DataReqDto.getGuarWays(); //担保方式
            String limitType = xdxw0046DataReqDto.getLimitType(); //额度类型
            BigDecimal curUseAmt = xdxw0046DataReqDto.getCurUseAmt(); //用信金额
            String isBeTrusted = xdxw0046DataReqDto.getIsBeTrusted(); //是否受托支付
            String entrustType = xdxw0046DataReqDto.getEntrustType(); //受托类型
            String surveySerno = xdxw0046DataReqDto.getSurveySerno(); //批复流水号   这个参数没用
            String geShuiDiZhi = xdxw0046DataReqDto.getGeShuiDiZhi(); //个税地址
            String contNo = xdxw0046DataReqDto.getContNo(); //合同号   这个参数没用
            String reqType = xdxw0046DataReqDto.getReqType(); //请求类型
            String sysDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT); //当前系统时间(String)
            Date date = DateUtils.parseDate(sysDate, DateFormatEnum.DEFAULT.getValue()); //当前系统时间(Date)

            //必填项校验
            if (StringUtils.isBlank(cusName)) {
                throw new Exception("客户名称为空！");
            }
            if (StringUtils.isBlank(cusId)) {
                throw new Exception("客户号为空！");
            }
            if (StringUtils.isBlank(certType)) {
                throw new Exception("证件类型为空！");
            }
            if (StringUtils.isBlank(certNo)) {
                throw new Exception("证件号为空！");
            }
            if (StringUtils.isBlank(areaName)) {
                throw new Exception("居住地址为空！");
            }
            if (StringUtils.isBlank(managerId)) {
                //throw new Exception("客户经理号为空！");
            }
            if (Objects.isNull(applyAmount)) {
                throw new Exception("申请金额为空！");
            }
            if (Objects.isNull(reality)) {
                throw new Exception("利率为空！");
            }
            if (StringUtils.isBlank(loanStartDate)) {
                throw new Exception("贷款开始时间为空！");
            }
            if (StringUtils.isBlank(loanEndDate)) {
                throw new Exception("贷款结束时间为空！");
            }
            if (StringUtils.isBlank(applyTerm)) {
                throw new Exception("贷款期限为空！");
            }
            if (StringUtils.isBlank(repayType)) {
                throw new Exception("还款方式为空！");
            }
            if (StringUtils.isBlank(guarWays)) {
                throw new Exception("担保方式为空！");
            }
            if (StringUtils.isBlank(limitType)) {
                throw new Exception("额度类型为空！");
            }
            if (Objects.isNull(curUseAmt)) {
//                throw new Exception("用信金额为空！");
            }
            if (StringUtils.isBlank(isBeTrusted)) {
                throw new Exception("是否受托支付为空！");
            }
            if (StringUtils.isBlank(entrustType)) {
//                throw new Exception("受托类型为空！");
            }
            if (StringUtils.isBlank(reqType)) {
                throw new Exception("请求类型为空！");
            }
            if (StringUtils.isBlank(surveySerno)) {
//                throw new Exception("批复流水号为空！");
            }
            if (StringUtils.isBlank(geShuiDiZhi)) {
//                throw new Exception("个税地址为空！");
            }
            if (StringUtils.isBlank(contNo)) {
//                throw new Exception("合同号为空！");
            }

            String openday = sysDate; //获取当前业务时间(yyyy-mm-dd)
            if ("1".equals(reqType)) {
                //如果reqType=1生成调查结果与批复
                /******第一步：分配责任人与责任机构*******/
                logger.info("*****XDXW0046:第一步：分配责任人与责任机构************");
                String serno = StringUtils.uuid(false); //根据方法自动生成
                String is_autodivis = ""; //是否自动分配
                String is_stop_offline = "0"; //是否线下调查
                String final_manager_id = ""; //最终的责任人
                String final_manager_name = ""; //最终的责任人名称
                String final_manager_org = ""; //最终的责任人机构
                String final_manager_phone = ""; //最终的责任人电话
                String cust_mgr = ""; //客户的管护人
                String cust_mgr_name = ""; //管护人的名称

                //调用客户模块服务查询客户信息
                CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCus(cusId)).orElse(new CusBaseClientDto());
                cust_mgr = cusBaseClientDto.getManagerId(); //客户的管护人

                //获取管护人的名称
                logger.info("**********XDXW0046*调用AdminSmUserService用户信息查询服务开始,查询参数为:{}", JSON.toJSONString(cust_mgr));
                AdminSmUserDto adminSmUserDto = Optional.ofNullable(commonService.getByLoginCode(cust_mgr)).orElse(new AdminSmUserDto());
                logger.info("**********XDXW0046*调用AdminSmUserService用户信息查询服务结束,返回结果为:{}", JSON.toJSONString(adminSmUserDto));
                cust_mgr_name = adminSmUserDto.getUserName();

                //判断是否自动分配
                logger.info("**********XDXW0046*调用Xdxw0062Service判断是否自动分配开始,查询参数为:{}", JSON.toJSONString(certNo));
                String isFengPei = xdxw0062Service.CheckGeShuiDiZhiFenPei("yxdauthor", certNo, managerId, geShuiDiZhi);
                logger.info("**********XDXW0046*调用Xdxw0062Service判断是否自动分配结束,返回结果为:{}", JSON.toJSONString(isFengPei));

                // 分配状态
                String divisStatus = DscmsBizXwEnum.DIVIS_STATUS_101.key;
                // 处理人
                String prcId = StringUtils.EMPTY;
                if ("1".equals(isFengPei) || "2".equals(isFengPei)) {
                    //自动分配最终责任人默认渠道传送的客户经理
                    final_manager_id = managerId;
                    is_autodivis = "1";
                    divisStatus = DscmsBizXwEnum.DIVIS_STATUS_100.key;
                    prcId = managerId;
                } else {
                    //手动分配
                    final_manager_id = cust_mgr;
                    is_autodivis = "0";
                }

                //获取最终责任人名称
                AdminSmUserDto adminSmUserDto2 = new AdminSmUserDto();
                logger.info("**********XDXW0046*调用AdminSmUserService获取最终责任人名称开始,查询参数为:{}", JSON.toJSONString(final_manager_id));
                ResultDto<AdminSmUserDto> resultDto2 = adminSmUserService.getByLoginCode(final_manager_id);
                logger.info("**********XDXW0046*调用AdminSmUserService获取最终责任人名称结束,返回结果为:{}", JSON.toJSONString(resultDto2));
                if (!Objects.isNull(resultDto2) && !Objects.isNull(resultDto2.getData())) {
                    adminSmUserDto2 = resultDto2.getData();
                    final_manager_name = adminSmUserDto2.getUserName();
                    final_manager_phone = adminSmUserDto2.getUserMobilephone();
                    final_manager_org = adminSmUserDto2.getOrgId();
                }

                // 更新客户信息
                if ("2".equals(isFengPei)) {
                    logger.info("新客户自动分配时更新客户信息开始");
                    CusBaseClientDto cusBase = new CusBaseClientDto();
                    cusBase.setCusId(cusId);
                    cusBase.setManagerId(final_manager_id);
                    cusBase.setManagerBrId(final_manager_org);
                    cusBase.setUpdId(final_manager_id);
                    cusBase.setUpdBrId(final_manager_org);
                    cusBase.setUpdDate(sysDate);
                    ResultDto<Integer> update = iCusClientService.updateCusbase(cusBase);
                    if (Objects.isNull(update.getData())) {
                        throw new Exception("客户信息更新失败！");
                    }
                    logger.info(cusBase.toString());
                    logger.info("新客户自动分配时更新客户信息结束");
                }

                /*******************查询优抵贷产品详细信息**************************/
                ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(DscmsBizXwEnum.YXD_TYPE_PW010004.key);
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = Optional.ofNullable(prdresultDto.getData()).orElse(new CfgPrdBasicinfoDto());


                /******第二步：生成新信贷授信批复表LMT_CRD_REPLY_INFO授信批复表*******/
                logger.info("*****XDXW0046:第二步：生成新信贷授信批复表LMT_CRD_REPLY_INFO授信批复表************");
                String crdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                String lmtSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                String taskSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                LmtCrdReplyInfo lmtCrdReplyInfo = new LmtCrdReplyInfo();
                lmtCrdReplyInfo.setReplySerno(crdSerno);
                lmtCrdReplyInfo.setSurveySerno(lmtSerno);
                lmtCrdReplyInfo.setCusId(cusId);
                lmtCrdReplyInfo.setCusName(cusName);
                lmtCrdReplyInfo.setCertCode(certNo);
                lmtCrdReplyInfo.setCusLvl("");
                lmtCrdReplyInfo.setGuarMode(guarWays);
                lmtCrdReplyInfo.setReplyAmt(applyAmount);
                lmtCrdReplyInfo.setTermType("M");
                lmtCrdReplyInfo.setPrdName(cfgPrdBasicinfoDto.getPrdName());
                lmtCrdReplyInfo.setPrdId(cfgPrdBasicinfoDto.getPrdId());
                lmtCrdReplyInfo.setCertType("A");

                String years = applyTerm.substring(0, 2);
                String months = applyTerm.substring(2, 4);
                int loanTerm = Integer.valueOf(years) * 12 + Integer.valueOf(months);
                lmtCrdReplyInfo.setAppTerm(loanTerm);
                lmtCrdReplyInfo.setReplyStartDate(loanStartDate);
                lmtCrdReplyInfo.setReplyEndDate(loanEndDate);
                lmtCrdReplyInfo.setExecRateYear(reality);
                lmtCrdReplyInfo.setRepayMode("01".equals(repayType) ? "A002":repayType);
                lmtCrdReplyInfo.setLimitType(limitType);
//                lmtCrdReplyInfo.setCurtLoanAmt(curUseAmt); TODO 风控无值
                if ("01".equals(isBeTrusted)) {
                    isBeTrusted = "1";
                } else if ("02".equals(isBeTrusted)) {
                    isBeTrusted = "2";
                }
                lmtCrdReplyInfo.setIsBeEntrustedPay(isBeTrusted);
                lmtCrdReplyInfo.setLoanCondFlg("1");
                /*   TODO 风控无值
                 if ("01".equals(entrustType)) {
                        entrustType = "1";
                    } else if ("02".equals(entrustType)) {
                        entrustType = "2";
                    }*/
                lmtCrdReplyInfo.setLmtGraper(0);// 宽限期 默认0
                lmtCrdReplyInfo.setCurType("CNY");// 币种
                lmtCrdReplyInfo.setBelgLine("01");// 条线 小微
                lmtCrdReplyInfo.setTruPayType(entrustType);
                lmtCrdReplyInfo.setApprType("");
                lmtCrdReplyInfo.setOprType(DscmsBizXwEnum.ADD_01.key);
                lmtCrdReplyInfo.setManagerBrId(final_manager_org);
                lmtCrdReplyInfo.setManagerId(final_manager_id);
                lmtCrdReplyInfo.setInputId(final_manager_id);
                lmtCrdReplyInfo.setInputBrId(final_manager_org);
                lmtCrdReplyInfo.setInputDate(openday);
                lmtCrdReplyInfo.setUpdId(final_manager_id);
                lmtCrdReplyInfo.setUpdBrId(final_manager_org);
                lmtCrdReplyInfo.setUpdDate(openday);
                lmtCrdReplyInfo.setCreateTime(date);
                lmtCrdReplyInfo.setUpdateTime(date);
                logger.info("**********XDXW0046*新增授信批复信息lmtCrdReplyInfo开始,新增参数为:{}", JSON.toJSONString(lmtCrdReplyInfo));
                int lmtCrdReplyInfoInsertCount = lmtCrdReplyInfoMapper.insert(lmtCrdReplyInfo);
                logger.info("**********XDXW0046*新增授信批复信息lmtCrdReplyInfo结束,返回结果为:{}", JSON.toJSONString(lmtCrdReplyInfoInsertCount));
                if (lmtCrdReplyInfoInsertCount < 1) {
                    throw new Exception("授信批复表插入失败！");
                }

                /****生成分配表数据 2021年7月22日18:30:30  WH 补充 **/
                logger.info("*****XDXW0046:第二步：插入调查任务分配表****");
                LmtSurveyTaskDivis lmtSurveyTaskDivis = new LmtSurveyTaskDivis();
                lmtSurveyTaskDivis.setPkId(taskSerno); //主键
                lmtSurveyTaskDivis.setSurveySerno(lmtSerno); //调查流水号
                lmtSurveyTaskDivis.setBizSerno(lmtSerno); //第三方业务流水号
                lmtSurveyTaskDivis.setPrdName(cfgPrdBasicinfoDto.getPrdName()); //产品名称
                lmtSurveyTaskDivis.setPrdType(cfgPrdBasicinfoDto.getPrdType()); //产品类型
                lmtSurveyTaskDivis.setPrdId(DscmsBizXwEnum.YXD_TYPE_PW010004.key); //产品编号
                lmtSurveyTaskDivis.setAppChnl(DscmsBizXwEnum.APP_CHNL_00.key); //申请渠道  微信小程序

                lmtSurveyTaskDivis.setCusId(cusId); //客户编号
                lmtSurveyTaskDivis.setCusName(cusName); //客户名称
                lmtSurveyTaskDivis.setCertType(certType); //证件类型
                lmtSurveyTaskDivis.setCertCode(certNo); //证件号码
                lmtSurveyTaskDivis.setAppAmt(applyAmount); //申请金额
                lmtSurveyTaskDivis.setPhone(StringUtils.EMPTY); //手机号码
                lmtSurveyTaskDivis.setWork(StringUtils.EMPTY); //工作
                lmtSurveyTaskDivis.setLoanPurp(""); //贷款用途
                lmtSurveyTaskDivis.setManagerName(final_manager_name); //客户经理名称
                lmtSurveyTaskDivis.setManagerId(final_manager_id); //客户经理编号
                lmtSurveyTaskDivis.setManagerArea(StringUtils.EMPTY); //客户经理片区
                // WH  2021年7月20日15:22:44  发现这个参数为空 先这样  优享贷是线上的
                lmtSurveyTaskDivis.setIsStopOffline(cfgPrdBasicinfoDto.getIdStopOffline() == null ? "0" : cfgPrdBasicinfoDto.getIdStopOffline()); //是否线下调查
                lmtSurveyTaskDivis.setIntoTime(DateUtils.getCurrentDate(DateFormatEnum.DATETIME)); //进件时间
                lmtSurveyTaskDivis.setDivisStatus(divisStatus); //调查分配状态,未分配
                lmtSurveyTaskDivis.setMarConfirmStatus(DscmsBizXwEnum.CONFIRM_STATUS_00.key); //客户经理确认状态,默认待确认
                lmtSurveyTaskDivis.setDivisTime(StringUtils.EMPTY); //分配时间
                lmtSurveyTaskDivis.setDivisId(StringUtils.EMPTY); //分配人
                lmtSurveyTaskDivis.setMarId(managerId); //营销人
                lmtSurveyTaskDivis.setPrcId(prcId); //处理人
                lmtSurveyTaskDivis.setBelgLine(DscmsBizXwEnum.BELG_LINE_01.key); //所属条线,默认小微条线
                lmtSurveyTaskDivis.setCreateTime(date); //创建时间
                lmtSurveyTaskDivis.setUpdateTime(date); //修改时间
                logger.info("**********XDXW0046**插入lmtSurveyTaskDivis开始,插入参数为:{}", JSON.toJSONString(lmtSurveyTaskDivis));
                int insert1 = lmtSurveyTaskDivisMapper.insert(lmtSurveyTaskDivis);
                logger.info("**********XDXW0046**插入lmtSurveyTaskDivis结束,返回结果为:{}", JSON.toJSONString(insert1));
                if (insert1 < 1) {
                    throw new Exception("客户授信调查表表插入失败！");
                }

                /******第三步：批复插完则向优享贷基本信息表中插入数据*******/
                logger.info("*****XDXW0046:第三步：批复插完则向优享贷基本信息表中插入数据************");
                LmtSurveyReportMainInfo lmtSurveyReportMainInfo = new LmtSurveyReportMainInfo();
                lmtSurveyReportMainInfo.setSurveySerno(lmtSerno);
//                lmtSurveyReportMainInfo.setBizSerno(serno);
                lmtSurveyReportMainInfo.setPrdName(cfgPrdBasicinfoDto.getPrdName());
                lmtSurveyReportMainInfo.setPrdType(cfgPrdBasicinfoDto.getPrdType());
                lmtSurveyReportMainInfo.setPrdId(DscmsBizXwEnum.YXD_TYPE_PW010004.key);
                lmtSurveyReportMainInfo.setSurveyType(DscmsBizXwEnum.SURVEY_TYPE_13.key); //优享贷STD_SURVEY_SUB_TYPE
//                lmtSurveyReportMainInfo.setSurveyType("");  // 2021年7月22日14:47:13  WH  多余字段
                lmtSurveyReportMainInfo.setCusId(cusId);
                lmtSurveyReportMainInfo.setCusName(cusName);
                lmtSurveyReportMainInfo.setCertType(DscmsBizXwEnum.CERT_TYPE_A.key);
                lmtSurveyReportMainInfo.setCertCode(certNo);
                lmtSurveyReportMainInfo.setAppAmt(applyAmount);
//                lmtSurveyReportMainInfo.setApproveStatus(BizFlowConstant.WF_STATUS_000);// 这里不需要放东西  不需要在待办界面显示
                lmtSurveyReportMainInfo.setApproveStatus(StringUtils.EMPTY);
                lmtSurveyReportMainInfo.setDataSource(DscmsBizXwEnum.DATA_SOURCE_01.key);
                lmtSurveyReportMainInfo.setIntoTime(sysDate);
                lmtSurveyReportMainInfo.setIsAutodivis(is_autodivis);
                lmtSurveyReportMainInfo.setIsStopOffline(is_stop_offline);
                lmtSurveyReportMainInfo.setOprType(DscmsBizXwEnum.ADD_01.key);
                lmtSurveyReportMainInfo.setMarId(managerId);
                lmtSurveyReportMainInfo.setManagerBrId(final_manager_org);
                lmtSurveyReportMainInfo.setManagerId(final_manager_id);
                lmtSurveyReportMainInfo.setInputId(final_manager_id);
                lmtSurveyReportMainInfo.setInputBrId(final_manager_org);
                lmtSurveyReportMainInfo.setInputDate(openday);
                lmtSurveyReportMainInfo.setUpdId(final_manager_id);
                lmtSurveyReportMainInfo.setUpdBrId(final_manager_org);
                lmtSurveyReportMainInfo.setUpdDate(openday);
                lmtSurveyReportMainInfo.setCreateTime(date);
                lmtSurveyReportMainInfo.setUpdateTime(date);
                logger.info("**********XDXW0046*新增授信调查主表信息lmtSurveyReportMainInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyReportMainInfo));
                int lmtSurveyReportMainInfoInsertCount = lmtSurveyReportMainInfoMapper.insert(lmtSurveyReportMainInfo);
                logger.info("**********XDXW0046*新增授信调查主表信息lmtSurveyReportMainInfo结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportMainInfoInsertCount));
                if (lmtSurveyReportMainInfoInsertCount < 1) {
                    throw new Exception("客户授信调查表表插入失败！");
                }

                //调查报告基本信息(LMT_SURVEY_REPORT_BASIC_INFO)
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = new LmtSurveyReportBasicInfo();
                lmtSurveyReportBasicInfo.setSurveySerno(lmtSerno);
                lmtSurveyReportBasicInfo.setCusName(cusName);
                lmtSurveyReportBasicInfo.setCusId(cusId);
                lmtSurveyReportBasicInfo.setCertCode(certNo);
                lmtSurveyReportBasicInfo.setPhone(final_manager_phone);
                lmtSurveyReportBasicInfo.setMarStatus("");
                lmtSurveyReportBasicInfo.setSpouseCusId("");
                lmtSurveyReportBasicInfo.setSpouseName("");
                lmtSurveyReportBasicInfo.setSpouseCertCode("");
                lmtSurveyReportBasicInfo.setSpousePhone("");
                lmtSurveyReportBasicInfo.setIsHaveChildren("");
                lmtSurveyReportBasicInfo.setEdu("");
                lmtSurveyReportBasicInfo.setWorkUnit("");
                lmtSurveyReportBasicInfo.setResiYears("");
                lmtSurveyReportBasicInfo.setLivingAddr("");
                lmtSurveyReportBasicInfo.setIsOnlinePld("");
                lmtSurveyReportBasicInfo.setGuarMode(guarWays);
                lmtSurveyReportBasicInfo.setAppAmt(applyAmount);
                lmtSurveyReportBasicInfo.setModelAdviceAmt(applyAmount);
                lmtSurveyReportBasicInfo.setModelAdviceRate(reality);
                lmtSurveyReportBasicInfo.setRefRate(reality);
                lmtSurveyReportBasicInfo.setAdviceAmt(applyAmount);
                lmtSurveyReportBasicInfo.setAdviceRate(reality);
                lmtSurveyReportBasicInfo.setAdviceTerm(applyTerm);
                lmtSurveyReportBasicInfo.setIsRenewLoan("0");
                lmtSurveyReportBasicInfo.setIsSceneInquest("0");
                lmtSurveyReportBasicInfo.setModelFstResult(CommonConstance.STD_YES_OR_NO_Y);
                lmtSurveyReportBasicInfo.setLoanPurp("");
                lmtSurveyReportBasicInfo.setIsAgri("");
                lmtSurveyReportBasicInfo.setIsNewEmployee("");
                lmtSurveyReportBasicInfo.setNewEmployeeName("");
                lmtSurveyReportBasicInfo.setNewEmployeePhone("");
                lmtSurveyReportBasicInfo.setOldBillNo("");
                lmtSurveyReportBasicInfo.setOldBillAmt(new BigDecimal("0"));
                lmtSurveyReportBasicInfo.setOldBillBalance(new BigDecimal("0"));
                lmtSurveyReportBasicInfo.setOldBillLoanRate(new BigDecimal("0"));
                lmtSurveyReportBasicInfo.setDebtFlag("");
                lmtSurveyReportBasicInfo.setGuarFlag("");
                lmtSurveyReportBasicInfo.setAccidentFlag("");
                lmtSurveyReportBasicInfo.setGuarFlag("");
                lmtSurveyReportBasicInfo.setAccidentFlag("");
                lmtSurveyReportBasicInfo.setManagementBelongFlag("");
                lmtSurveyReportBasicInfo.setSurveyMode("");
                lmtSurveyReportBasicInfo.setSingleSurveySerno("");
                lmtSurveyReportBasicInfo.setOprType(DscmsBizXwEnum.ADD_01.key);
                lmtSurveyReportBasicInfo.setMarId(managerId);
                lmtSurveyReportBasicInfo.setManagerBrId(final_manager_org);
                lmtSurveyReportBasicInfo.setInputId(final_manager_id);
                lmtSurveyReportBasicInfo.setInputBrId(final_manager_org);
                lmtSurveyReportBasicInfo.setInputDate(openday);
                lmtSurveyReportBasicInfo.setUpdId(final_manager_id);
                lmtSurveyReportBasicInfo.setUpdBrId(final_manager_org);
                lmtSurveyReportBasicInfo.setCreateTime(date);
                lmtSurveyReportBasicInfo.setUpdateTime(date);
                logger.info("**********XDXW0046*新增授信调查基本表信息lmtSurveyReportBasicInfo开始,新增参数为:{}", JSON.toJSONString(lmtSurveyReportBasicInfo));
                int lmtSurveyReportBasicInfoInsertCount = lmtSurveyReportBasicInfoMapper.insert(lmtSurveyReportBasicInfo);
                logger.info("**********XDXW0046*新增授信调查基本表信息lmtSurveyReportBasicInfo结束,返回结果为:{}", JSON.toJSONString(lmtSurveyReportBasicInfoInsertCount));
                if (lmtSurveyReportBasicInfoInsertCount < 1) {
                    throw new Exception("调查报告基本信息表插入失败！");
                }

                /******第四步：如果管护人和最终责任人不一致，发送短信*******/
                logger.info("*****XDXW0046:第四步：如果管护人和最终责任人不一致，发送短信************");
                if (cust_mgr != null && !"1".equals(isFengPei) //TODO  报错 添加字段
                        && !"2".equals(isFengPei)
                        && !cust_mgr.equals(managerId)
                        && !"".equals(final_manager_phone)
                        && !"".equals(final_manager_org)
                        && AreaSameOrNo(final_manager_org, geShuiDiZhi, managerId)
                ) {
                    if (!StringUtils.isBlank(final_manager_phone)) {
                        logger.info("********************XDXW0046发送的短信***********************");
                        String messageType = "MSG_XW_M_0014";// 短信编号
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", cusName);
                        paramMap.put("prdName", "优享贷");
                        try {
                            //执行发送借款人操作
                            ResultDto<Integer> resultDto1 = messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, final_manager_phone);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }

                /******第五步：返回信息*******/
                logger.info("*****XDXW0046:第五步：返回信息************");
                xdxw0046DataRespDto.setSerno(lmtSerno);
                xdxw0046DataRespDto.setManager_id("");
                xdxw0046DataRespDto.setManager_name("");
                xdxw0046DataRespDto.setOrg_id("");
                ResultDto<AdminSmOrgDto> orgInfo = adminSmOrgService.getByOrgCode(final_manager_org);
                if (orgInfo != null && StringUtil.isNotEmpty(orgInfo.getCode()) && CmisBizConstants.NUM_ZERO.equals(orgInfo.getCode())) {//返回成功
                    xdxw0046DataRespDto.setOrg_name(orgInfo.getData().getOrgName());
                }
                xdxw0046DataRespDto.setManager_phone(final_manager_phone);
            } else if ("2".equals(reqType)) {
                //如果reqType=2取消批复
                //1、根据调查流水查询合同数量
                logger.info("*****XDXW0046:如果reqType=2取消批复;1、根据调查流水查询合同数量;调查流水号:" + surveySerno);
                int count = ctrLoanContMapper.queryCtrLoanContCountBySurveySerno(surveySerno);
                //2、如果合同数量>0 返回：msg="已生成合同请撤销合同后撤销授信！"
                //3、如果合同数量=0,更新调查报告状态：
                if (count > 0) {
                    throw new Exception("已生成合同请撤销合同后撤销授信！");
                } else if (count == 0) {
                    int deleteCount = lmtCrdReplyInfoMapper.deleteBySurveySerno(surveySerno);
                    if (deleteCount < 1) {
                        throw new Exception("授信批复表数据删除失败！");
                    }
                    int deleteCount2 = lmtSurveyReportMainInfoMapper.deleteByPrimaryKey(surveySerno);
                    if (deleteCount2 < 1) {
                        throw new Exception("调查报告主表数据删除失败！");
                    }
                    int deleteCount3 = lmtSurveyReportBasicInfoMapper.deleteByPrimaryKey(surveySerno);
                    if (deleteCount3 < 1) {
                        throw new Exception("调查报告基本信表数据删除失败！");
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            throw new Exception(e.getMessage());

        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value);
        return xdxw0046DataRespDto;
    }

    public boolean AreaSameOrNo(String yxXfOrg, String geShuiDiZhi, String managerId) {
        boolean flag = false;
        String townOfCity = "";
        try {
            if ((yxXfOrg.startsWith("01") && !"016000".equals(yxXfOrg)) || "990000".equals(yxXfOrg)) {
                townOfCity = "张家港";
            } else {
                townOfCity = xdxw0062Service.getTownOfCityByXfOrgid(yxXfOrg);
            }
            if ((geShuiDiZhi.indexOf(townOfCity) >= 0) && !StringUtils.isBlank(townOfCity)) {
                flag = true;
            }
            int ct = areaAdminUserMapper.getCountAreaAdminUserByOnlinePrdManagerAndUserNo("01", managerId);
            if (ct == 0) {
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return flag;
    }
}
