package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailDepositInfo
 * @类描述: bail_deposit_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-09 23:53:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BailDepositInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 保证金类型 **/
	private String bailType;
	
	/** 保证金帐号 **/
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	private String bailAccNoSub;
	
	/** 币种 **/
	private String curType;
	
	/** 保证金金额 **/
	private java.math.BigDecimal bailAmt;
	
	/** 保证金汇率 **/
	private java.math.BigDecimal bailRate;
	
	/** 保证金利息存入帐号 **/
	private String bailInterestDepAcct;
	
	/** 登记银承承兑汇票保证金的流水号 **/
	private String inputAccpBailSerno;
	
	/** 票汇盈保证金释放账号 **/
	private String phyBailReleaseAccNo;
	
	/** 计息方式 **/
	private String interestMode;
	
	/** 申请人账号 **/
	private String rqstrAccNo;
	
	/** 申请人账户名 **/
	private String rqstrAccName;
	
	/** 申请人账户子序号 **/
	private String rqstrAccNoSub;
	
	/** 核心账户子序号 **/
	private String coreAccNoSub;
	
	/** 合同部门 **/
	private String contDep;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 状态 **/
	private String status;
	
	/** 是否释放 **/
	private String isRelease;
	
	/** 部提次数 **/
	private String ministryTimes;
	
	/** 是否为定期 **/
	private String isRegular;
	
	/** 缴存登记日期 **/
	private String depositInputDate;
	
	/** 缴存登记人 **/
	private String depositInputId;
	
	/** 缴存登记机构 **/
	private String depositInputBrId;
	
	/** 审批状态 **/
	private String approveStatus;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param bailType
	 */
	public void setBailType(String bailType) {
		this.bailType = bailType == null ? null : bailType.trim();
	}
	
    /**
     * @return BailType
     */	
	public String getBailType() {
		return this.bailType;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo == null ? null : bailAccNo.trim();
	}
	
    /**
     * @return BailAccNo
     */	
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSub
	 */
	public void setBailAccNoSub(String bailAccNoSub) {
		this.bailAccNoSub = bailAccNoSub == null ? null : bailAccNoSub.trim();
	}
	
    /**
     * @return BailAccNoSub
     */	
	public String getBailAccNoSub() {
		return this.bailAccNoSub;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return BailAmt
     */	
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return BailRate
     */	
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param bailInterestDepAcct
	 */
	public void setBailInterestDepAcct(String bailInterestDepAcct) {
		this.bailInterestDepAcct = bailInterestDepAcct == null ? null : bailInterestDepAcct.trim();
	}
	
    /**
     * @return BailInterestDepAcct
     */	
	public String getBailInterestDepAcct() {
		return this.bailInterestDepAcct;
	}
	
	/**
	 * @param inputAccpBailSerno
	 */
	public void setInputAccpBailSerno(String inputAccpBailSerno) {
		this.inputAccpBailSerno = inputAccpBailSerno == null ? null : inputAccpBailSerno.trim();
	}
	
    /**
     * @return InputAccpBailSerno
     */	
	public String getInputAccpBailSerno() {
		return this.inputAccpBailSerno;
	}
	
	/**
	 * @param phyBailReleaseAccNo
	 */
	public void setPhyBailReleaseAccNo(String phyBailReleaseAccNo) {
		this.phyBailReleaseAccNo = phyBailReleaseAccNo == null ? null : phyBailReleaseAccNo.trim();
	}
	
    /**
     * @return PhyBailReleaseAccNo
     */	
	public String getPhyBailReleaseAccNo() {
		return this.phyBailReleaseAccNo;
	}
	
	/**
	 * @param interestMode
	 */
	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode == null ? null : interestMode.trim();
	}
	
    /**
     * @return InterestMode
     */	
	public String getInterestMode() {
		return this.interestMode;
	}
	
	/**
	 * @param rqstrAccNo
	 */
	public void setRqstrAccNo(String rqstrAccNo) {
		this.rqstrAccNo = rqstrAccNo == null ? null : rqstrAccNo.trim();
	}
	
    /**
     * @return RqstrAccNo
     */	
	public String getRqstrAccNo() {
		return this.rqstrAccNo;
	}
	
	/**
	 * @param rqstrAccName
	 */
	public void setRqstrAccName(String rqstrAccName) {
		this.rqstrAccName = rqstrAccName == null ? null : rqstrAccName.trim();
	}
	
    /**
     * @return RqstrAccName
     */	
	public String getRqstrAccName() {
		return this.rqstrAccName;
	}
	
	/**
	 * @param rqstrAccNoSub
	 */
	public void setRqstrAccNoSub(String rqstrAccNoSub) {
		this.rqstrAccNoSub = rqstrAccNoSub == null ? null : rqstrAccNoSub.trim();
	}
	
    /**
     * @return RqstrAccNoSub
     */	
	public String getRqstrAccNoSub() {
		return this.rqstrAccNoSub;
	}
	
	/**
	 * @param coreAccNoSub
	 */
	public void setCoreAccNoSub(String coreAccNoSub) {
		this.coreAccNoSub = coreAccNoSub == null ? null : coreAccNoSub.trim();
	}
	
    /**
     * @return CoreAccNoSub
     */	
	public String getCoreAccNoSub() {
		return this.coreAccNoSub;
	}
	
	/**
	 * @param contDep
	 */
	public void setContDep(String contDep) {
		this.contDep = contDep == null ? null : contDep.trim();
	}
	
    /**
     * @return ContDep
     */	
	public String getContDep() {
		return this.contDep;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param isRelease
	 */
	public void setIsRelease(String isRelease) {
		this.isRelease = isRelease == null ? null : isRelease.trim();
	}
	
    /**
     * @return IsRelease
     */	
	public String getIsRelease() {
		return this.isRelease;
	}
	
	/**
	 * @param ministryTimes
	 */
	public void setMinistryTimes(String ministryTimes) {
		this.ministryTimes = ministryTimes == null ? null : ministryTimes.trim();
	}
	
    /**
     * @return MinistryTimes
     */	
	public String getMinistryTimes() {
		return this.ministryTimes;
	}
	
	/**
	 * @param isRegular
	 */
	public void setIsRegular(String isRegular) {
		this.isRegular = isRegular == null ? null : isRegular.trim();
	}
	
    /**
     * @return IsRegular
     */	
	public String getIsRegular() {
		return this.isRegular;
	}
	
	/**
	 * @param depositInputDate
	 */
	public void setDepositInputDate(String depositInputDate) {
		this.depositInputDate = depositInputDate == null ? null : depositInputDate.trim();
	}
	
    /**
     * @return DepositInputDate
     */	
	public String getDepositInputDate() {
		return this.depositInputDate;
	}
	
	/**
	 * @param depositInputId
	 */
	public void setDepositInputId(String depositInputId) {
		this.depositInputId = depositInputId == null ? null : depositInputId.trim();
	}
	
    /**
     * @return DepositInputId
     */	
	public String getDepositInputId() {
		return this.depositInputId;
	}
	
	/**
	 * @param depositInputBrId
	 */
	public void setDepositInputBrId(String depositInputBrId) {
		this.depositInputBrId = depositInputBrId == null ? null : depositInputBrId.trim();
	}
	
    /**
     * @return DepositInputBrId
     */	
	public String getDepositInputBrId() {
		return this.depositInputBrId;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}


}