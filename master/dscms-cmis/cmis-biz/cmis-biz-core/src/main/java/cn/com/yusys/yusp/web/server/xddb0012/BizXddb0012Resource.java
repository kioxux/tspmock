package cn.com.yusys.yusp.web.server.xddb0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0012.req.Xddb0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.MortgagorsList;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.Xddb0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0012.Xddb0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:抵押登记获取押品信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDDB0012:抵押登记获取押品信息")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0012Resource.class);

    @Autowired
    private Xddb0012Service xddb0012Service;
    /**
     * 交易码：xddb0012
     * 交易描述：抵押登记获取押品信息
     *
     * @param xddb0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记获取押品信息")
    @PostMapping("/xddb0012")
    protected @ResponseBody
    ResultDto<Xddb0012DataRespDto> xddb0012(@Validated @RequestBody Xddb0012DataReqDto xddb0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
        Xddb0012DataRespDto xddb0012DataRespDto = new Xddb0012DataRespDto();// 响应Dto:抵押登记获取押品信息
        ResultDto<Xddb0012DataRespDto> xddb0012DataResultDto = new ResultDto<>();
        try {
			logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
			xddb0012DataRespDto = xddb0012Service.getXddb0012(xddb0012DataReqDto);
			logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
			// 封装xddb0012DataResultDto中正确的返回码和返回信息
            xddb0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            xddb0012DataResultDto.setCode(e.getErrorCode());
            xddb0012DataResultDto.setMessage(e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            // 封装xddb0012DataResultDto中异常返回码和返回信息
            xddb0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0012DataRespDto到xddb0012DataResultDto中
        xddb0012DataResultDto.setData(xddb0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataRespDto));
        return xddb0012DataResultDto;
    }
}
