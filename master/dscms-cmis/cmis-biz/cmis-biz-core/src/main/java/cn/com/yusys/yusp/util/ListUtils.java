package cn.com.yusys.yusp.util;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mashun
 * @Description Object转换List类
 * @create 2021/1/15 10:11:01
 */
@Component
public class ListUtils {

    public ListUtils() {
    }

    /***
     * @数组装换，用于Mapper中传入参数的Map value为数组的情况，无法有效的使用if foreach
     * @param className T 类型
     * @param obj 数组对象
     * @param <T>
     * @return
     * @throws ClassNotFoundException
     */
    public static <T> List<T> castList(String className, Object obj) throws ClassNotFoundException {
        ClassLoader classLoader = ListUtils.class.getClassLoader();
        Class<T> clazz = (Class<T>) classLoader.loadClass(className);
        List<T> result = new ArrayList<T>();
        if(obj instanceof List<?>){
            for (Object o : (List<?>) obj){
                result.add(clazz.cast(o));
            }
            return result;
        }
        return null;
    }

}
