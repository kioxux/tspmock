package cn.com.yusys.yusp.web.server.xdht0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0020.req.Xdht0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0020.Xdht0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 接口处理类:合同信息查看
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0020:合同信息查看")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0020Resource.class);

    @Resource
    private Xdht0020Service xdht0020Service;

    /**
     * 交易码：xdht0020
     * 交易描述：合同信息查看
     *
     * @param xdht0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息查看")
    @PostMapping("/xdht0020")
    protected @ResponseBody
    ResultDto<Xdht0020DataRespDto> xdht0020(@Validated @RequestBody Xdht0020DataReqDto xdht0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataReqDto));
        ResultDto<Xdht0020DataRespDto> xdht0020DataResultDto = new ResultDto<>();
        // 从xdht0020DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataReqDto));
            xdht0020DataResultDto = xdht0020Service.queryContStatusByCertCode(xdht0020DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataResultDto));
        }  catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdht0020DataResultDto.setCode(e.getErrorCode());
            xdht0020DataResultDto.setMessage(e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, e.getMessage());
            // 封装xdht0020DataResultDto中异常返回码和返回信息
            xdht0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataResultDto));
        return xdht0020DataResultDto;
    }
}
