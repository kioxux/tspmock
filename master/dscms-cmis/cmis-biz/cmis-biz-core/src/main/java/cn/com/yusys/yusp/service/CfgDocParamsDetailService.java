/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CfgDocParamsDetail;
import cn.com.yusys.yusp.repository.mapper.CfgDocParamsDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CfgDocParamsDetailService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:49:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CfgDocParamsDetailService {

    @Autowired
    private CfgDocParamsDetailMapper cfgDocParamsDetailMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CfgDocParamsDetail selectByPrimaryKey(String cdpdSerno) {
        return cfgDocParamsDetailMapper.selectByPrimaryKey(cdpdSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CfgDocParamsDetail> selectAll(QueryModel model) {
        List<CfgDocParamsDetail> records = (List<CfgDocParamsDetail>) cfgDocParamsDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModelSecond
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:cainingbo_yx
     * @算法描述: 无
     */

    public List<CfgDocParamsDetail> selectByModelSecond(QueryModel model) {
       // PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgDocParamsDetail> list = cfgDocParamsDetailMapper.selectByModel(model);
        Set<String> docTypeDataSet = new HashSet<>();
        if(list != null && list.size() >0){
            for(CfgDocParamsDetail cfgDocParamsDetails : list){
                String docTypeData = cfgDocParamsDetails.getDocTypeData();
                String cdplSerno = cfgDocParamsDetails.getCdplSerno();
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("docTypeData",docTypeData);
                queryModel.addCondition("cdplSerno",cdplSerno);
                if(!docTypeDataSet.contains(docTypeData)){
                    int count = cfgDocParamsDetailMapper.selectCountsByDocTypeData(queryModel);
                    if(count >= 1 ){
                        cfgDocParamsDetails.setDocTypeDataCount(count);
                    }
                    docTypeDataSet.add(docTypeData);
                }else{
                    cfgDocParamsDetails.setDocTypeDataCount(0);
                }
            }
        }
      //  PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:cainingbo_yx
     * @算法描述: 无
     */

    public List<CfgDocParamsDetail> selectByModel(QueryModel model) {
       // PageHelper.startPage(model.getPage(), model.getSize());
        List<CfgDocParamsDetail> list = cfgDocParamsDetailMapper.selectByModel(model);
       // PageHelper.clearPage();
        return list;
    }
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CfgDocParamsDetail record) {
        return cfgDocParamsDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CfgDocParamsDetail record) {
        return cfgDocParamsDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CfgDocParamsDetail record) {
        return cfgDocParamsDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CfgDocParamsDetail record) {
        return cfgDocParamsDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cdpdSerno) {
        return cfgDocParamsDetailMapper.deleteByPrimaryKey(cdpdSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cfgDocParamsDetailMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: selectByCdplSerno
     * @方法描述: 根据参数配置流水号查询清单信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CfgDocParamsDetail selectByCdplSerno(String cdplSerno) {
        return cfgDocParamsDetailMapper.selectByCdplSerno(cdplSerno);
    }
    /**
     * @函数名称:deleteByCdplSerno
     * @函数描述:根据配置流水号删除档案配置信息
     * @参数与返回说明:
     * @算法描述:
     */
    public int deleteByCdplSerno(String cdplSerno) {
        return cfgDocParamsDetailMapper.deleteByCdplSerno(cdplSerno);
    }
}
