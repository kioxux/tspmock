package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.req.Fb1168ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.resp.Fb1168RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.Dscms2CircpClientService;
import cn.com.yusys.yusp.service.RiskXdGuarantyService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 房抵e点贷押品查询查封流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX09BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX09BizService.class);//定义log

    @Autowired
    private RiskXdGuarantyService riskXdGuarantyService;
    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 房贷押品查询查封
        if ("YX017".equals(bizType)){
            riskXdGuarantyBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 房贷押品查询查封
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void riskXdGuarantyBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            RiskXdGuaranty riskXdGuaranty = riskXdGuarantyService.selectByPrimaryKey(serno);
            log.info("房抵e点贷抵押查询查封:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("房抵e点贷抵押查询查封:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("房抵e点贷抵押查询查封:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("房抵e点贷抵押查询查封:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("房抵e点贷抵押查询查封:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("房抵e点贷抵押查询查封:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("房抵e点贷抵押查询查封:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_997);
                // 调用fb1168 交易描述：抵押查封结果推送
                String flag = this.sendfb1168(riskXdGuaranty);
                if(!"success".equals(flag)){
                    throw BizException.error(null, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value + "风控返回："+flag);
                }
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("房抵e点贷抵押查询查封:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(riskXdGuaranty, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("房抵e点贷抵押查询查封:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 抵押查询查封审批通过业务处理
     * sfResultInfo 业务申请信息
     * */
    public String sendfb1168(RiskXdGuaranty record){
        /*
         *************************************************************************
         * 调用风控fb1168房抵e点贷抵押查封结果推送
         * *************************************************************************/
        // 获取营业日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        log.info("押品查询查封推送风控开始,流水号【{}】", record.getSerno());
        Fb1168ReqDto fb1168ReqDto = new Fb1168ReqDto();
        fb1168ReqDto.setCHANNEL_TYPE("13"); //渠道来源 默认‘13’ 参照老信贷代码逻辑
        fb1168ReqDto.setCO_PLATFORM("2002"); //合作平台 默认‘2002’ 参照老信贷代码逻辑
        fb1168ReqDto.setPRD_CODE ("2002000001"); //产品代码 默认‘2002000001’ 参照老信贷代码逻辑
        fb1168ReqDto.setDy_no(record.getDyNo()); //抵押品编号
        fb1168ReqDto.setBdc_no(record.getBdcNo()); //不动产证号
        fb1168ReqDto.setJk_cus_name(record.getJkCusName()); //借款人名称
        fb1168ReqDto.setYp_manager_id(record.getYpManagerId()); //押品所有人名称
        fb1168ReqDto.setYp_book_serno(record.getYpBookSerno()); //押品权证编号
        fb1168ReqDto.setCx_cf_result(record.getCxCfResult()); //查询查封结果
        fb1168ReqDto.setPre_app_no(record.getPreAppNo()); // 预授信流水号
        fb1168ReqDto.setCx_cf_time(openDay.replaceAll("-", "")); // 查询查封时间
        log.info("押品查询查封推送风控请求：" + fb1168ReqDto);
        ResultDto<Fb1168RespDto> fb1166RespResultDto = dscms2CircpClientService.fb1168(fb1168ReqDto);
        log.info("押品查询查封推送风控返回：" + fb1166RespResultDto);
        String fb1166Code = Optional.ofNullable(fb1166RespResultDto.getCode()).orElse(StringUtils.EMPTY);
        String fb1166Meesage = Optional.ofNullable(fb1166RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
        log.info("押品查询查封推送风控返回信息：" + fb1166Meesage);
        if (Objects.equals(fb1166Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取相关的值并解析
            return "success";
        } else {
            return fb1166Meesage;
        }
    }

    /***
     * 流程审批状态更新
     * 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (RiskXdGuaranty riskXdGuaranty, String approveStatus){
        riskXdGuaranty.setApproveStatus(approveStatus);
        riskXdGuarantyService.updateSelective(riskXdGuaranty);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX09).equals(flowCode);
    }
}
