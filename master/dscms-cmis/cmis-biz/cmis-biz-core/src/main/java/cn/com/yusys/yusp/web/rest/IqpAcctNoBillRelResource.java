/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAcctNoBillRel;
import cn.com.yusys.yusp.service.IqpAcctNoBillRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctNoBillRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-25 14:55:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpacctnobillrel")
public class IqpAcctNoBillRelResource {
    @Autowired
    private IqpAcctNoBillRelService iqpAcctNoBillRelService;

	/**
     * 全表查询.
     * 
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpAcctNoBillRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpAcctNoBillRel> list = iqpAcctNoBillRelService.selectAll(queryModel);
        return new ResultDto<List<IqpAcctNoBillRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpAcctNoBillRel>> index(QueryModel queryModel) {
        List<IqpAcctNoBillRel> list = iqpAcctNoBillRelService.selectByModel(queryModel);
        return new ResultDto<List<IqpAcctNoBillRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpAcctNoBillRel> show(@PathVariable("pkId") String pkId) {
        IqpAcctNoBillRel iqpAcctNoBillRel = iqpAcctNoBillRelService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpAcctNoBillRel>(iqpAcctNoBillRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAcctNoBillRel> create(@RequestBody IqpAcctNoBillRel iqpAcctNoBillRel) throws URISyntaxException {
        iqpAcctNoBillRelService.insert(iqpAcctNoBillRel);
        return new ResultDto<IqpAcctNoBillRel>(iqpAcctNoBillRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAcctNoBillRel iqpAcctNoBillRel) throws URISyntaxException {
        int result = iqpAcctNoBillRelService.update(iqpAcctNoBillRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAcctNoBillRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAcctNoBillRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
