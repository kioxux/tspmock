package cn.com.yusys.yusp.web.server.xdtz0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0010.req.Xdtz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.resp.Xdtz0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0005.Xdtz0005Service;
import cn.com.yusys.yusp.service.server.xdtz0010.Xdtz0010Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据身份证号获取借据信息
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0010:根据身份证号获取借据信息")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0010Resource.class);

    @Autowired
    private Xdtz0010Service xdtz0010Service;
    /**
     * 交易码：xdtz0010
     * 交易描述：根据身份证号获取借据信息
     * @param xdtz0010DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("根据身份证号获取借据信息")
    @PostMapping("/xdtz0010")
    protected @ResponseBody
    ResultDto<Xdtz0010DataRespDto> xdtz0010(@Validated @RequestBody Xdtz0010DataReqDto xdtz0010DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataReqDto));
        Xdtz0010DataRespDto  xdtz0010DataRespDto  = new Xdtz0010DataRespDto();// 响应Dto:根据身份证号获取借据信息
        ResultDto<Xdtz0010DataRespDto>xdtz0010DataResultDto = new ResultDto<>();
        try {
            // 从xdtz0010DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdtz0010DataRespDto = xdtz0010Service.xdtz0010(xdtz0010DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdtz0010DataRespDto对象开始
            // TODO 封装xdtz0010DataRespDto对象结束
            // 封装xdtz0010DataResultDto中正确的返回码和返回信息
            xdtz0010DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0010DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value,e.getMessage());
            // 封装xdtz0010DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdtz0010DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0010DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdtz0010DataRespDto到xdtz0010DataResultDto中
        xdtz0010DataResultDto.setData(xdtz0010DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataResultDto));
        return xdtz0010DataResultDto;
    }
}