/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelFinaInfo
 * @类描述: lmt_sig_invest_rel_fina_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:22:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_rel_fina_info")
public class LmtSigInvestRelFinaInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户大类 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String cusCatalog;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 当前财务信息年月份 **/
	@Column(name = "CUR_FNC_DATE", unique = false, nullable = true, length = 10)
	private String curFncDate;
	
	/** 区域经济/行业情况分析 **/
	@Column(name = "AREA_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String areaSituAnaly;
	
	/** 区域经济/行业情况分析图片路径 **/
	@Column(name = "AREA_SITU_ANALY_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String areaSituAnalyPicturePath;
	
	/** 企业的资产状况分析 **/
	@Column(name = "CORP_ASSET_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String corpAssetSituAnaly;
	
	/** 企业的负债状况分析 **/
	@Column(name = "CORP_DEBT_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String corpDebtSituAnaly;
	
	/** 收入情况分析 **/
	@Column(name = "INCOME_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String incomeSituAnaly;
	
	/** 盈利情况分析 **/
	@Column(name = "GAIN_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String gainSituAnaly;
	
	/** 业务运营情况分析 **/
	@Column(name = "BIZ_OPER_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String bizOperSituAnaly;
	
	/** 现金流情况分析 **/
	@Column(name = "CASH_SITU_ANALY", unique = false, nullable = true, length = 65535)
	private String cashSituAnaly;
	
	/** 其他情况分析 **/
	@Column(name = "OTHER_CASE_ANALY", unique = false, nullable = true, length = 65535)
	private String otherCaseAnaly;
	
	/** 其他情况图片路径 **/
	@Column(name = "OTHER_CASE_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String otherCasePicturePath;
	
	/** 企业商业模式情况 **/
	@Column(name = "CORP_COMM_MODE_SITU", unique = false, nullable = true, length = 65535)
	private String corpCommModeSitu;
	
	/** 同业授信准入 **/
	@Column(name = "INTBANK_LMT_ADMIT", unique = false, nullable = true, length = 65535)
	private String intbankLmtAdmit;
	
	/** 经营情况 **/
	@Column(name = "OPER_SITU", unique = false, nullable = true, length = 65535)
	private String operSitu;
	
	/** 经营情况图片路径 **/
	@Column(name = "OPER_SITU_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String operSituPicturePath;
	
	/** 财务情况 **/
	@Column(name = "FINA_SITU", unique = false, nullable = true, length = 65535)
	private String finaSitu;
	
	/** 财务情况图片路径 **/
	@Column(name = "FINA_SITU_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String finaSituPicturePath;
	
	/** 重点关注 **/
	@Column(name = "PRIOR_CONCED", unique = false, nullable = true, length = 65535)
	private String priorConced;
	
	/** 重点关注图片路径 **/
	@Column(name = "PRIOR_CONCED_PICTURE_PATH", unique = false, nullable = true, length = 1000)
	private String priorConcedPicturePath;
	
	/** 主要风险分析 **/
	@Column(name = "MAIN_RISK_ANALY", unique = false, nullable = true, length = 65535)
	private String mainRiskAnaly;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 65535)
	private String indgtResult;
	
	/** 或有负债情况说明 **/
	@Column(name = "DEBT_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String debtSituDesc;
	
	/** 其他对外担保情况说明 **/
	@Column(name = "OTHER_OUTGUAR_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String otherOutguarSituDesc;
	
	/** 融资波动原因 **/
	@Column(name = "FINA_WAVE_REASON", unique = false, nullable = true, length = 65535)
	private String finaWaveReason;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param curFncDate
	 */
	public void setCurFncDate(String curFncDate) {
		this.curFncDate = curFncDate;
	}
	
    /**
     * @return curFncDate
     */
	public String getCurFncDate() {
		return this.curFncDate;
	}
	
	/**
	 * @param areaSituAnaly
	 */
	public void setAreaSituAnaly(String areaSituAnaly) {
		this.areaSituAnaly = areaSituAnaly;
	}
	
    /**
     * @return areaSituAnaly
     */
	public String getAreaSituAnaly() {
		return this.areaSituAnaly;
	}
	
	/**
	 * @param areaSituAnalyPicturePath
	 */
	public void setAreaSituAnalyPicturePath(String areaSituAnalyPicturePath) {
		this.areaSituAnalyPicturePath = areaSituAnalyPicturePath;
	}
	
    /**
     * @return areaSituAnalyPicturePath
     */
	public String getAreaSituAnalyPicturePath() {
		return this.areaSituAnalyPicturePath;
	}
	
	/**
	 * @param corpAssetSituAnaly
	 */
	public void setCorpAssetSituAnaly(String corpAssetSituAnaly) {
		this.corpAssetSituAnaly = corpAssetSituAnaly;
	}
	
    /**
     * @return corpAssetSituAnaly
     */
	public String getCorpAssetSituAnaly() {
		return this.corpAssetSituAnaly;
	}
	
	/**
	 * @param corpDebtSituAnaly
	 */
	public void setCorpDebtSituAnaly(String corpDebtSituAnaly) {
		this.corpDebtSituAnaly = corpDebtSituAnaly;
	}
	
    /**
     * @return corpDebtSituAnaly
     */
	public String getCorpDebtSituAnaly() {
		return this.corpDebtSituAnaly;
	}
	
	/**
	 * @param incomeSituAnaly
	 */
	public void setIncomeSituAnaly(String incomeSituAnaly) {
		this.incomeSituAnaly = incomeSituAnaly;
	}
	
    /**
     * @return incomeSituAnaly
     */
	public String getIncomeSituAnaly() {
		return this.incomeSituAnaly;
	}
	
	/**
	 * @param gainSituAnaly
	 */
	public void setGainSituAnaly(String gainSituAnaly) {
		this.gainSituAnaly = gainSituAnaly;
	}
	
    /**
     * @return gainSituAnaly
     */
	public String getGainSituAnaly() {
		return this.gainSituAnaly;
	}
	
	/**
	 * @param bizOperSituAnaly
	 */
	public void setBizOperSituAnaly(String bizOperSituAnaly) {
		this.bizOperSituAnaly = bizOperSituAnaly;
	}
	
    /**
     * @return bizOperSituAnaly
     */
	public String getBizOperSituAnaly() {
		return this.bizOperSituAnaly;
	}
	
	/**
	 * @param cashSituAnaly
	 */
	public void setCashSituAnaly(String cashSituAnaly) {
		this.cashSituAnaly = cashSituAnaly;
	}
	
    /**
     * @return cashSituAnaly
     */
	public String getCashSituAnaly() {
		return this.cashSituAnaly;
	}
	
	/**
	 * @param otherCaseAnaly
	 */
	public void setOtherCaseAnaly(String otherCaseAnaly) {
		this.otherCaseAnaly = otherCaseAnaly;
	}
	
    /**
     * @return otherCaseAnaly
     */
	public String getOtherCaseAnaly() {
		return this.otherCaseAnaly;
	}
	
	/**
	 * @param otherCasePicturePath
	 */
	public void setOtherCasePicturePath(String otherCasePicturePath) {
		this.otherCasePicturePath = otherCasePicturePath;
	}
	
    /**
     * @return otherCasePicturePath
     */
	public String getOtherCasePicturePath() {
		return this.otherCasePicturePath;
	}
	
	/**
	 * @param corpCommModeSitu
	 */
	public void setCorpCommModeSitu(String corpCommModeSitu) {
		this.corpCommModeSitu = corpCommModeSitu;
	}
	
    /**
     * @return corpCommModeSitu
     */
	public String getCorpCommModeSitu() {
		return this.corpCommModeSitu;
	}
	
	/**
	 * @param intbankLmtAdmit
	 */
	public void setIntbankLmtAdmit(String intbankLmtAdmit) {
		this.intbankLmtAdmit = intbankLmtAdmit;
	}
	
    /**
     * @return intbankLmtAdmit
     */
	public String getIntbankLmtAdmit() {
		return this.intbankLmtAdmit;
	}
	
	/**
	 * @param operSitu
	 */
	public void setOperSitu(String operSitu) {
		this.operSitu = operSitu;
	}
	
    /**
     * @return operSitu
     */
	public String getOperSitu() {
		return this.operSitu;
	}
	
	/**
	 * @param operSituPicturePath
	 */
	public void setOperSituPicturePath(String operSituPicturePath) {
		this.operSituPicturePath = operSituPicturePath;
	}
	
    /**
     * @return operSituPicturePath
     */
	public String getOperSituPicturePath() {
		return this.operSituPicturePath;
	}
	
	/**
	 * @param finaSitu
	 */
	public void setFinaSitu(String finaSitu) {
		this.finaSitu = finaSitu;
	}
	
    /**
     * @return finaSitu
     */
	public String getFinaSitu() {
		return this.finaSitu;
	}
	
	/**
	 * @param finaSituPicturePath
	 */
	public void setFinaSituPicturePath(String finaSituPicturePath) {
		this.finaSituPicturePath = finaSituPicturePath;
	}
	
    /**
     * @return finaSituPicturePath
     */
	public String getFinaSituPicturePath() {
		return this.finaSituPicturePath;
	}
	
	/**
	 * @param priorConced
	 */
	public void setPriorConced(String priorConced) {
		this.priorConced = priorConced;
	}
	
    /**
     * @return priorConced
     */
	public String getPriorConced() {
		return this.priorConced;
	}
	
	/**
	 * @param priorConcedPicturePath
	 */
	public void setPriorConcedPicturePath(String priorConcedPicturePath) {
		this.priorConcedPicturePath = priorConcedPicturePath;
	}
	
    /**
     * @return priorConcedPicturePath
     */
	public String getPriorConcedPicturePath() {
		return this.priorConcedPicturePath;
	}
	
	/**
	 * @param mainRiskAnaly
	 */
	public void setMainRiskAnaly(String mainRiskAnaly) {
		this.mainRiskAnaly = mainRiskAnaly;
	}
	
    /**
     * @return mainRiskAnaly
     */
	public String getMainRiskAnaly() {
		return this.mainRiskAnaly;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param debtSituDesc
	 */
	public void setDebtSituDesc(String debtSituDesc) {
		this.debtSituDesc = debtSituDesc;
	}
	
    /**
     * @return debtSituDesc
     */
	public String getDebtSituDesc() {
		return this.debtSituDesc;
	}
	
	/**
	 * @param otherOutguarSituDesc
	 */
	public void setOtherOutguarSituDesc(String otherOutguarSituDesc) {
		this.otherOutguarSituDesc = otherOutguarSituDesc;
	}
	
    /**
     * @return otherOutguarSituDesc
     */
	public String getOtherOutguarSituDesc() {
		return this.otherOutguarSituDesc;
	}
	
	/**
	 * @param finaWaveReason
	 */
	public void setFinaWaveReason(String finaWaveReason) {
		this.finaWaveReason = finaWaveReason;
	}
	
    /**
     * @return finaWaveReason
     */
	public String getFinaWaveReason() {
		return this.finaWaveReason;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}