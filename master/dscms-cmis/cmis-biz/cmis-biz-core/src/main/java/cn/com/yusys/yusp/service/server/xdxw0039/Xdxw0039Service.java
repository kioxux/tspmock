package cn.com.yusys.yusp.service.server.xdxw0039;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.req.CmisCus0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.resp.CmisCus0015RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0039.req.CusSurveyList;
import cn.com.yusys.yusp.dto.server.xdxw0039.req.Xdxw0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0039.resp.Xdxw0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportComInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * 接口处理类:提交决议
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxw0039Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0039Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;//台账信息表
    @Resource
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;//授信调查批复表
    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;//授信调查主表
    @Resource
    private CommonService commonService;//客户信息查询
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;//合同信息
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;//出账申请信息表
    @Resource
    private AdminSmUserService adminSmUserService;//用户信息
    @Resource
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;//调查报告企业信息
    @Resource
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private SenddxService senddxService;
    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    @Autowired
    private LmtSurveyReportBasicInfoService lmtSurveyReportBasicInfoService;

    @Autowired
    private LmtRenewLoanAppInfoService lmtRenewLoanAppInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CusLstMclWhbxdService cusLstMclWhbxdService;

    /**
     * 提交决议
     *
     * @param xdxw0039DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0039DataRespDto xdxw0039(Xdxw0039DataReqDto xdxw0039DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value);
        //返回信息
        Xdxw0039DataRespDto xdxw0039DataRespDto = new Xdxw0039DataRespDto();
        try {
            //请求字段
            String optionFlag = xdxw0039DataReqDto.getOption_flag();// 01：提交，02：作废，03：东方税银审批结果入账
            String mainSerno = xdxw0039DataReqDto.getSurvey_serno();
            String approveStatus = xdxw0039DataReqDto.getApprove_status();
            List<CusSurveyList> cusSurveyLists = xdxw0039DataReqDto.getCusSurveyList();
            CusSurveyList cusSurveyList = new CusSurveyList();

            // 根据调查报告编号查找出经营（一）、经营（二）以及创意贷中调用的    借据号
            if (DscmsBizXwEnum.OPTIONFLAG_03.key.equals(optionFlag)) {// 03：东方税银审批结果入账
                boolean exists = false;
                logger.info("********XDXW0039***03：东方税银审批结果入账开始*****START**************");
                if (CollectionUtils.nonEmpty(cusSurveyLists)) {
                    /**
                     * 则新增当前的最新的审批结果
                     */
                    // 开始新增
                    for (int i = 0; i < cusSurveyLists.size(); i++) {
                        cusSurveyList = cusSurveyLists.get(i);

                        String surveySerno = cusSurveyList.getSurvey_serno();// 小贷客户调查主表主键
                        logger.info("**********XDXW0039**03：东方税银审批结果入账开始,查询参数为:{}", JSON.toJSONString(surveySerno));
                        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectByPrimaryKey(surveySerno);
                        logger.info("**********XDXW0039**03：东方税银审批结果入账结束,返回结果为:{}", JSON.toJSONString(lmtCrdReplyInfo));

                        if (lmtCrdReplyInfo != null) {//存在批复记录
                            exists = true;
                        } else {//不存在批复记录,直接新增
                            logger.info("*******XDXW0039*03：东方税银审批结果入账;不存在批复记录,直接新增开始,查询参数为:{}", JSON.toJSONString(cusSurveyList));
                            int count = insertIntoLmtCrdReplyInfo(cusSurveyList);
                            logger.info("**********XDXW0039*03：东方税银审批结果入账;不存在批复记录,直接新增结束,返回结果为:{}", JSON.toJSONString(count));
                            if (count > 0) {//成功
                                xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                                xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                            }
                        }
                    }
                } else {
                    //未传有效数据
                    logger.info("**********XDXW0039传入参数cusSurveyLists为空;***03：东方税银审批结果入账结束*****END**************");
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3901.value);
                }
                if (exists) {
                    //该客户税贷审批已经生成决议，请勿重复提交！
                    logger.info("***********XDXW0039该客户税贷审批已经生成决议，请勿重复提交！***************");
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3902.value);
                } else {
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3903.value);
                }
            } else if (DscmsBizXwEnum.OPTIONFLAG_02.key.equals(optionFlag)) {
                logger.info("***********XDXW0039****02作废操作开始***************");
                QueryModel model = new QueryModel();
                model.addCondition("surveySerno", mainSerno);
                List<CtrLoanCont> list = ctrLoanContMapper.selectByModel(model);
                // 合同未引用调查报告
                if (list.size() == 0) {
                    /***** 删除决议信息 *****/
                    logger.info("*******XDXW0039****02作废操作开始;合同未引用调查报告,删除决议信息开始,查询参数为:{}", JSON.toJSONString(mainSerno));
                    int count = lmtCrdReplyInfoMapper.deleteByPrimaryKey(mainSerno);
                    logger.info("**********XDXW0039****02作废操作开始;合同未引用调查报告,删除决议信息结束,返回结果为:{}", JSON.toJSONString(count));
                    //删除决议信息成功
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                } else {//合同信息已经引用
                    //查询是否已经出账引用
                    logger.info("*******XDXW0039**02作废操作开始;合同信息已经引用,查询是否已经出账引用开始,查询参数为:{}", JSON.toJSONString(mainSerno));
                    int result = pvpLoanAppMapper.queryPvpLoanContBysureySerno(mainSerno);
                    logger.info("******XDXW0039**02作废操作开始;合同信息已经引用,查询是否已经出账引用结束,返回结果为:{}", JSON.toJSONString(result));
                    if (result == 0) {// 未放款
                        /***** 删除决议信息 *****/
                        int count = lmtCrdReplyInfoMapper.deleteByPrimaryKey(mainSerno);
                        //删除决议信息成功
                        xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                        xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                    } else {// 已放款
                        //该决议下已经有放款，无法删除
                        xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3902.value);
                    }
                }
            } else if (DscmsBizXwEnum.OPTIONFLAG_01.key.equals(optionFlag)) {//01：提交
                boolean exists = false;
                logger.info("***********XDXW0039****01提交操作开始***************");
                // 审批通过之后才进行决议表插入
                if (CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) {
                    if (CollectionUtils.nonEmpty(cusSurveyLists)) {
                        for (int i = 0; i < cusSurveyLists.size(); i++) {
                            cusSurveyList = cusSurveyLists.get(i);

                            String surveySerno = cusSurveyList.getSurvey_serno();// 小贷客户调查主表主键

                            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectByPrimaryKey(surveySerno);

                            // 新增审批结果
                            logger.info("*******XDXW0039****01提交操作开始;新增审批结果开始,查询参数为:{}", JSON.toJSONString(lmtCrdReplyInfo));
                            if (lmtCrdReplyInfo != null) {//存在批复记录
                                exists = true;
                            } else {//不存在批复记录,直接新增
                                int count = insertIntoLmtCrdReplyInfo(cusSurveyList);
                                if (count > 0) {//成功
                                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                                }
                            }

                        }
                    } else {
                        //未传有效数据
                        xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3901.value);
                        return xdxw0039DataRespDto;
                    }
                }
                // 更新调查表状态（LMT_SURVEY_REPORT_MAIN_INFO）
                logger.info("******XDXW0039****更新调查表状态*LMT_SURVEY_REPORT_MAIN_INFO开始,查询参数为:{}", JSON.toJSONString(mainSerno));
                LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(mainSerno);
                if (CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) {
                    String is_wxbxd = cusSurveyList.getIs_wxbxd();//无还本续贷标记
                    // 查询授信客户调查表 LMT_SURVEY_REPORT_MAIN_INFO
                    // xd_xd_enterprise、xd_wd_basic、xd_xf_basic_info；此三张表军全量迁移新微贷
                    // 2021年10月12日23:48:26 hubp BUG14603
                    // 如果为无还本续贷，则更新无还本续贷申请状态
                    if("SC010010".equals(lmtSurveyReportMainInfo.getPrdId())){
                        QueryModel model = new QueryModel();
                        model.addCondition("serno", lmtSurveyReportMainInfo.getListSerno());
                        // 查询无还本续贷名单信息
                        List<CusLstMclWhbxd> cusLstMclWhbxds= cusLstMclWhbxdService.selectByModel(model);
                        logger.info("******997****更新无还本续贷申请信息查找,查询参数为:【{}】", JSON.toJSONString(cusLstMclWhbxds));
                        if(cusLstMclWhbxds.size() > 0){
                            CusLstMclWhbxd whbxd =  cusLstMclWhbxds.get(0);
                            whbxd.setApplyStatus("3");
                            cusLstMclWhbxdService.updateSelective(whbxd);
                        }
                    }
                } else if (CmisCommonConstants.WF_STATUS_998.equals(approveStatus)) {
                    // 更新无本还贷标记  老信贷 sqdxd_info whbxd_apply 表已废弃
                    // 如果为无还本续贷，则更新无还本续贷申请状态
                    if("SC010010".equals(lmtSurveyReportMainInfo.getPrdId())){
                        QueryModel model = new QueryModel();
                        model.addCondition("serno", lmtSurveyReportMainInfo.getListSerno());
                        // 查询无还本续贷名单信息
                        List<CusLstMclWhbxd> cusLstMclWhbxds= cusLstMclWhbxdService.selectByModel(model);
                        logger.info("******998****更新无还本续贷申请信息查找,查询参数为:【{}】", JSON.toJSONString(cusLstMclWhbxds));
                        if(cusLstMclWhbxds.size() > 0){
                            CusLstMclWhbxd whbxd =  cusLstMclWhbxds.get(0);
                            whbxd.setApplyStatus("2");
                            cusLstMclWhbxdService.updateSelective(whbxd);
                        }
                    }
                } else if (CmisCommonConstants.WF_STATUS_992.equals(approveStatus)) {
                    // 更新无本还贷标记  老信贷 sqdxd_info whbxd_apply 表已废弃
                    if("SC010010".equals(lmtSurveyReportMainInfo.getPrdId())){
                        QueryModel model = new QueryModel();
                        model.addCondition("serno", lmtSurveyReportMainInfo.getListSerno());
                        // 查询无还本续贷名单信息
                        List<CusLstMclWhbxd> cusLstMclWhbxds= cusLstMclWhbxdService.selectByModel(model);
                        logger.info("******992****更新无还本续贷申请信息查找,查询参数为:【{}】", JSON.toJSONString(cusLstMclWhbxds));
                        if(cusLstMclWhbxds.size() > 0){
                            CusLstMclWhbxd whbxd =  cusLstMclWhbxds.get(0);
                            whbxd.setApplyStatus("1");
                            cusLstMclWhbxdService.updateSelective(whbxd);
                        }
                    }
                }

                String managerId = StringUtils.EMPTY;
                if (lmtSurveyReportMainInfo != null) {
                    managerId = lmtSurveyReportMainInfo.getManagerId();
                    //更新调查表的状态和最后修改日期
                    logger.info("***********XDXW0039****更新调查表的状态和最后修改日期**************");
                    lmtSurveyReportMainInfo.setUpdateTime(DateUtils.getCurrDate());
                    lmtSurveyReportMainInfo.setApproveStatus(approveStatus);
                    lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(lmtSurveyReportMainInfo);
                }
                //返回处理结果
                if (exists) {
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.RETURN_MESSAGE_3905.value);
                } else {
                    xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                    xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                }
                //发送短信给队客户经理，提醒尽快处理
                String telPhone = StringUtils.EMPTY;
                if (StringUtil.isNotEmpty(managerId)) {//客户经理工号不能为空
                    logger.info("**********XDXW0039*调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    logger.info("**********XDXW0039*调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        telPhone = adminSmUserDto.getUserMobilephone();
                    }
                }
                // 根据工号获取用户表中的联系电话
                if (StringUtil.isNotEmpty(telPhone)) {//电话号码不为空，发送短信
                    String sendCusMessage = "";
                    if (CmisCommonConstants.WF_STATUS_997.equals(approveStatus)) {
                        sendCusMessage = "你业务编号为" + mainSerno + "的调查表审批【通过】请尽快处理";
                    } else if (CmisCommonConstants.WF_STATUS_998.equals(approveStatus)) {
                        sendCusMessage = "你业务编号为" + mainSerno + "的调查表审批【否决】请尽快处理";
                    } else if (CmisCommonConstants.WF_STATUS_992.equals(approveStatus)) {
                        sendCusMessage = "你业务编号为" + mainSerno + "的调查表审批【退回】请尽快处理";
                    }
                    //发送短信
                    sendMsg(sendCusMessage, telPhone);
                }
            }

            //审批通过
            if (CmisCommonConstants.WF_STATUS_997.equals(approveStatus) && DscmsBizXwEnum.SUCCESS.key.equals(xdxw0039DataRespDto.getOpFlag())) {
                logger.info("***********XDXW0039***审批通过*************");
                LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoMapper.selectByPrimaryKey(mainSerno);//查询企业调查报告信息
                LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(mainSerno);//调查报告主表

                String managerBrId = StringUtils.EMPTY;
                String trade = StringUtils.EMPTY;
                String cusId = StringUtils.EMPTY;
                if (lmtSurveyReportComInfo != null && lmtSurveyReportMainInfo != null) {//查询结果不为空
                    managerBrId = lmtSurveyReportComInfo.getManagerBrId();//管理机构
                    trade = lmtSurveyReportComInfo.getTrade();//行业
                    cusId = lmtSurveyReportMainInfo.getCusId();
                }
                if (DscmsBizXwEnum.ORG_CODE_016000.key.equals(managerBrId) || DscmsBizXwEnum.ORG_CODE_016001.key.equals(managerBrId)) {//小贷机构跟新客户行业信息
                    //更新客户单位信息表  cus_indiv_unit
                    CmisCus0015ReqDto cmisCus0015ReqDto = new CmisCus0015ReqDto();
                    cmisCus0015ReqDto.setCusId(cusId);
                    cmisCus0015ReqDto.setIndivComTrade(trade);
                    logger.info("***********XDXW0039***小贷机构跟新客户行业信息开始,查询参数为:{}", JSON.toJSONString(cmisCus0015ReqDto));
                    ResultDto<CmisCus0015RespDto> resultDto = cmisCusClientService.cmiscus0015(cmisCus0015ReqDto);
                    logger.info("***********XDXW0039***小贷机构跟新客户行业信息结束,返回结果为:{}", JSON.toJSONString(resultDto));
                    String code = resultDto.getCode();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        xdxw0039DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                        xdxw0039DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value);
        return xdxw0039DataRespDto;
    }

    /**
     * @方法名称: insertIntoLmtCrdReplyInfo
     * @方法描述: 新增插入小贷调查报告审批结果表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int insertIntoLmtCrdReplyInfo(CusSurveyList cusSurveyList) throws ParseException {
        logger.info("***********XDXW0039***新增插入小贷调查报告审批结果表开始**START***********");
        int count = CoopPlanAppConstant.NUMBER_ZERO;
        try {
            // 营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            //请求列表字段
            String surveySerno = cusSurveyList.getSurvey_serno();// 小贷客户调查主表主键
            String cusId = cusSurveyList.getCus_id(); // 客户编号
            String prd_id = cusSurveyList.getPrd_id();//产品编号
            String prd_name = cusSurveyList.getPrd_name();//产品名称
            BigDecimal apply_amount = cusSurveyList.getApply_amount();// 合同金额
            BigDecimal reality_ir_y = cusSurveyList.getReality_ir_y();  // 利率
            String loan_start_date = cusSurveyList.getLoan_start_date(); // 合同起始日
            String loan_end_date = cusSurveyList.getLoan_end_date();// 合同到期日
            Integer apply_term = cusSurveyList.getApply_term();// 期限
            String repay_type = cusSurveyList.getRepay_type();// 还款方式
            String guar_ways = cusSurveyList.getGuar_ways();  // 担保方式
            String limit_type = cusSurveyList.getLimit_type();// 额度类型 01一次性 02循环
            BigDecimal cur_use_amt = cusSurveyList.getCur_use_amt(); // 本次用信金额
            String is_be_entrusted = cusSurveyList.getIs_be_entrusted();// 是否受托支付 01 02
            if (DscmsBizXwEnum.YESNO_01.key.equals(is_be_entrusted)) {
                is_be_entrusted = "1";// 1--是
            } else {
                is_be_entrusted = "0";// 2--否
            }
            String is_credit_condition = cusSurveyList.getIs_credit_condition(); //是否有用信条件
            if (DscmsBizXwEnum.YESNO_01.key.equals(is_credit_condition)) {
                is_credit_condition = "1";// 1--是;
            } else {
                is_credit_condition = "0";// 2--否;
            }
            String entrust_type = cusSurveyList.getEntrust_type(); // 受托类型
            String approval_type = cusSurveyList.getApproval_type();// 审批类型
            String isCommBorr = cusSurveyList.getIs_comm_borr();// 是否有共借人

            //根据传过来的客户号查询客户信息
            String cusName = StringUtils.EMPTY;
            String certCode = StringUtils.EMPTY;
            String cusLvl = StringUtils.EMPTY;
            String certType = StringUtils.EMPTY;
            CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusId);
            if (cusBaseDto != null) {//存量客户
                cusName = cusBaseDto.getCusName();
                certCode = cusBaseDto.getCertCode();
                cusLvl = cusBaseDto.getCusCrdGrade();
                certType = cusBaseDto.getCertType();
            }

            logger.info("***********XDXW0039***新增插入小贷调查报告审批结果表开始，插入授信调查批复表************");
            //获取流水号  TODO by WH
            String crdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
            //插入授信调查批复表
            LmtCrdReplyInfo lmtCrdReplyInfo = new LmtCrdReplyInfo();
            lmtCrdReplyInfo.setReplySerno(crdSerno);//主键
            lmtCrdReplyInfo.setSurveySerno(surveySerno);
            lmtCrdReplyInfo.setCusId(cusId);
            lmtCrdReplyInfo.setCusName(cusName);//客户姓名
            lmtCrdReplyInfo.setCertCode(certCode);//证件号
            lmtCrdReplyInfo.setCusLvl(cusLvl);//客户等级
            lmtCrdReplyInfo.setGuarMode(guar_ways);//担保方式
            lmtCrdReplyInfo.setReplyAmt(apply_amount);//批复金额
            lmtCrdReplyInfo.setAppTerm(apply_term);
            lmtCrdReplyInfo.setReplyStartDate(loan_start_date);
            lmtCrdReplyInfo.setReplyEndDate(loan_end_date);
            lmtCrdReplyInfo.setExecRateYear(reality_ir_y);
            lmtCrdReplyInfo.setCertType(certType);
            lmtCrdReplyInfo.setRepayMode(repay_type);
            lmtCrdReplyInfo.setLimitType(limit_type);
            lmtCrdReplyInfo.setCurtLoanAmt(cur_use_amt);
            lmtCrdReplyInfo.setIsBeEntrustedPay(is_be_entrusted);// 是否受托支付 01 02
            lmtCrdReplyInfo.setLoanCondFlg(is_credit_condition);//是否有用信条件
            lmtCrdReplyInfo.setTruPayType(entrust_type);//受托类型
            lmtCrdReplyInfo.setApprType(approval_type);
            //TODO WH  2021年7月21日16:09:42 补全数据
            lmtCrdReplyInfo.setPrdId(prd_id);
            lmtCrdReplyInfo.setPrdName(prd_name);
            lmtCrdReplyInfo.setBelgLine("01");//小微条线
            lmtCrdReplyInfo.setManagerId("");
            lmtCrdReplyInfo.setManagerBrId("");
            lmtCrdReplyInfo.setOprType("01");//新增
            lmtCrdReplyInfo.setInputId("");
            lmtCrdReplyInfo.setInputBrId("");
            lmtCrdReplyInfo.setUpdId("");
            lmtCrdReplyInfo.setUpdBrId("");
            lmtCrdReplyInfo.setInputDate(openDay);
            lmtCrdReplyInfo.setCurType("CNY");//币种
            lmtCrdReplyInfo.setReplyStatus("01");//批复状态
            lmtCrdReplyInfo.setLmtGraper(0);//宽限期
            lmtCrdReplyInfo.setTermType("M");//期限类型 月
            // 2021年8月3日18:45:13  因为 批复查询条件的原因 需要赋值机构id和责任人id
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(surveySerno);
            if (lmtSurveyReportMainInfo != null && !StringUtils.isBlank(lmtSurveyReportMainInfo.getManagerId()) && !StringUtils.isBlank(lmtSurveyReportMainInfo.getManagerBrId())) {
                logger.info("审批通过后 修改批复信息 更新责任人责任机构数据" + lmtSurveyReportMainInfo.getManagerId());
                lmtCrdReplyInfo.setManagerId(lmtSurveyReportMainInfo.getManagerId());
                lmtCrdReplyInfo.setManagerBrId(lmtSurveyReportMainInfo.getManagerBrId());
            }

            // 判断批复是否为无还本续贷优农贷默认否 SC010008--优企贷
            if (Objects.equals(prd_id, "SC010008")) {
                logger.info("判断优企贷是否为无还本续贷开始,调查流水号【{}】", surveySerno);
                LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
                if (Objects.isNull(lmtSurveyReportBasicInfo)) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "未查询到调查报告基本信息！");
                }
                logger.info("查询优企贷基本信息结束,调查流水号【{}】", surveySerno);

                // 根据周转方式无还本续贷判断 H--还本续贷 W--无还本续贷 S--其他
                String turnWay = lmtSurveyReportBasicInfo.getAppLoanWay();
                if ("W".equals(turnWay)) {
                    // 1--是
                    lmtCrdReplyInfo.setIsWxbxd("1");
                    // 存储续贷原合同号信息
                    logger.info("查询优企贷续贷白名单信息开始,调查流水号【{}】、名单流水号【{}】", surveySerno, lmtSurveyReportMainInfo.getListSerno());
                    LmtRenewLoanAppInfo lmtRenewLoanAppInfo = lmtRenewLoanAppInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getListSerno());
                    if (Objects.isNull(lmtRenewLoanAppInfo)) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "未查询到优企贷续贷白名单信息！");
                    }
                    logger.info("查询优企贷续贷白名单信息结束,调查流水号【{}】、名单流水号【{}】", surveySerno, lmtSurveyReportMainInfo.getListSerno());
                    lmtCrdReplyInfo.setXdOrigiContNo(lmtRenewLoanAppInfo.getOldContNo());
                } else {
                    // 0--否
                    lmtCrdReplyInfo.setIsWxbxd("0");
                }
            } else if (Objects.equals(prd_id, "SC010010")) {
                // BUG 15522
                // 2021年10月26日22:15:19 hubp 为房抵循环贷 则IsWxbxd为是
                logger.info("prdId:【{}】为房抵循环贷，则设置IsWxbxd为是", prd_id);
                lmtCrdReplyInfo.setIsWxbxd("1");
            } else {
                // 0--否
                lmtCrdReplyInfo.setIsWxbxd("0");
            }
            count = lmtCrdReplyInfoMapper.insertSelective(lmtCrdReplyInfo);
            logger.info("***********XDXW0039***申请额度**开始***********");
            cmisBizXwCommonService.sendCmisLmt0001(lmtCrdReplyInfo);
            logger.info("***********XDXW0039***申请额度**结束***********");
        } catch (BizException e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EpbEnum.EPB099999.key, e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info("***********XDXW0039***新增插入小贷调查报告审批结果表结束**END***********");
        return count;
    }

    /**
     * 发送短信
     *
     * @param sendMessage
     * @param telnum
     * @throws Exception
     */
    private void sendMsg(String sendMessage, String telnum) throws Exception {
        logger.info("发送的短信内容：" + sendMessage);

        SenddxReqDto senddxReqDto = new SenddxReqDto();
        senddxReqDto.setInfopt("dx");
        SenddxReqList senddxReqList = new SenddxReqList();
        senddxReqList.setMobile(telnum);
        senddxReqList.setSmstxt(sendMessage);
        ArrayList<SenddxReqList> list = new ArrayList<>();
        list.add(senddxReqList);
        senddxReqDto.setSenddxReqList(list);
        try {
            senddxService.senddx(senddxReqDto);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new Exception("发送短信失败！");
        }
    }

}
