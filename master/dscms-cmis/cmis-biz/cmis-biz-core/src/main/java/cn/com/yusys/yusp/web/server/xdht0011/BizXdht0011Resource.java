package cn.com.yusys.yusp.web.server.xdht0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0011.req.Xdht0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.Xdht0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdht0011.Xdht0011Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0011:查询符合条件的省心快贷合同")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0011Resource.class);

    @Autowired
    private Xdht0011Service xdht0011Service;
    /**
     * 交易码：xdht0011
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param xdht0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询符合条件的省心快贷合同")
    @PostMapping("/xdht0011")
    protected @ResponseBody
    ResultDto<Xdht0011DataRespDto> xdht0011(@Validated @RequestBody Xdht0011DataReqDto xdht0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataReqDto));
        Xdht0011DataRespDto xdht0011DataRespDto = new Xdht0011DataRespDto();// 响应Dto:查询符合条件的省心快贷合同
        ResultDto<Xdht0011DataRespDto> xdht0011DataResultDto = new ResultDto<>();
        try {
            // 从xdht0011DataReqDto获取业务值进行业务逻辑处理
            // 调用Xdht0011Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataReqDto));

            xdht0011DataRespDto = xdht0011Service.queryCtrLoanContOrFk(xdht0011DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataReqDto));
            // 封装xdxw0014DataResultDto中正确的返回码和返回信息
            xdht0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, e.getMessage());
            // 封装xdht0011DataResultDto中异常返回码和返回信息
            xdht0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0011DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdht0011DataRespDto到xdht0011DataResultDto中
        xdht0011DataResultDto.setData(xdht0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataRespDto));
        return xdht0011DataResultDto;
    }
}
