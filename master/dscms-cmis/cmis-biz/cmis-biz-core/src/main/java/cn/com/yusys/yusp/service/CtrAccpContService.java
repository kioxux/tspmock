package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.req.CmisLmt0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0024.resp.CmisLmt0024RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.OccRelList;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAccpContMapper;
import cn.com.yusys.yusp.repository.mapper.IqpAccpAppMapper;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAccpContService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zxz
 * @创建时间: 2021-04-14 15:01:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrAccpContService {

    private static final Logger log = LoggerFactory.getLogger(CtrLoanContService.class);
    @Autowired
    private CtrAccpContMapper ctrAccpContMapper;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private Dscms2PjxtClientService dscms2PjxtClientService;

    @Autowired
    private GuarBusinessRelService guarBusinessRelService;

    @Autowired
    private IqpAccpAppMapper iqpAccpAppMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AccAccpDrftSubService accAccpDrftSubService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrAccpCont selectByPrimaryKey(String pkId) {
        return ctrAccpContMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrAccpCont> selectAll(QueryModel model) {
        List<CtrAccpCont> records = (List<CtrAccpCont>) ctrAccpContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrAccpCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrAccpCont> list = ctrAccpContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CtrAccpCont record) {
        return ctrAccpContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insertSelective(CtrAccpCont record) {
        return ctrAccpContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CtrAccpCont record) {
        return ctrAccpContMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateSelective(CtrAccpCont record) {
        return ctrAccpContMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 通过合同号更新LMTACCNO
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int updateLmtAccNoByContNo(CtrAccpCont record) {
        return ctrAccpContMapper.updateLmtAccNoByContNo(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return ctrAccpContMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return ctrAccpContMapper.deleteByIds(ids);
    }

    /**
     * 获取基本信息
     * chenlong9
     *
     * @param queryMap
     * @return
     */
    public CtrAccpCont queryCtrAccpContByDataParams(HashMap<String, String> queryMap) {
        return ctrAccpContMapper.queryCtrAccpContByDataParams(queryMap);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询未生效数据(列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrAccpCont> toSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatus", CmisCommonConstants.CONT_STATUS_100);
        return ctrAccpContMapper.selectByModel(model);
    }

    /**
     * @方法名称: doneSignlist
     * @方法描述: 查询除了合同状态为未生效数据(历史列表)
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrAccpCont> doneSignlist(QueryModel model) {
        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        model.getCondition().put("contStatusOther", CmisCommonConstants.CONT_STATUS_OTHER);
        return ctrAccpContMapper.selectByModel(model);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrAccpCont> selectByLmtAccNo(String lmtAccNo) {
        return ctrAccpContMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: onSign
     * @方法描述: 银承合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int onSign(CtrAccpCont ctrAccpCont) {
        HashMap<String, String> queryMap = new HashMap<>();
        // 占承兑行白名单额度
        this.acceptanceLmt(ctrAccpCont.getContNo());

        queryMap.put("contNo", ctrAccpCont.getContNo());
        CtrAccpCont ctrAccpContObj = ctrAccpContMapper.queryCtrAccpContByDataParams(queryMap);
        ctrAccpContObj.setContStatus(CmisCommonConstants.CONT_STATUS_200);
        ctrAccpContObj.setPaperContSignDate(ctrAccpCont.getPaperContSignDate());
        if(!CmisCommonConstants.GUAR_MODE_40.equals(ctrAccpContObj.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(ctrAccpContObj.getGuarMode())
         && !CmisCommonConstants.GUAR_MODE_00.equals(ctrAccpContObj.getGuarMode())){
            List<GrtGuarBizRstRel> list = grtGuarBizRstRelService.getByContNo(ctrAccpContObj.getContNo());
            if (CollectionUtils.isEmpty(list)) {
                log.error("主合同【{}】未关联担保合同！",ctrAccpContObj.getContNo());
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            //担保合同签订生效
            for (GrtGuarBizRstRel grtGuarBizRstRel : list) {
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                grtGuarCont.setGuarContState(CmisCommonConstants.GUAR_CONT_STATE_101);
                grtGuarCont.setSignDate(ctrAccpCont.getPaperContSignDate());
                if(StringUtils.isEmpty(grtGuarCont.getBizLine())){
                    log.error("担保合同【"+grtGuarCont.getGuarContNo()+"】的业务条线为空");
                }
                grtGuarContService.updateSelective(grtGuarCont);
            }
        }

        // 判断是否为合同续签,若为续签合同则恢复原合同的占额
        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(ctrAccpContObj.getIsRenew())){
            CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
            cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrAccpContObj.getManagerBrId()));//金融机构代码
            cmisLmt0012ReqDto.setBizNo(ctrAccpContObj.getOrigiContNo());//合同编号
            cmisLmt0012ReqDto.setRecoverType(CmisLmtConstants.STD_RECOVER_TYPE_08);//恢复类型
            cmisLmt0012ReqDto.setRecoverAmtCny(BigDecimal.ZERO);//恢复金额
            cmisLmt0012ReqDto.setRecoverSpacAmtCny(BigDecimal.ZERO);//恢复敞口金额
            cmisLmt0012ReqDto.setInputId(ctrAccpContObj.getInputId());//登记人
            cmisLmt0012ReqDto.setInputBrId(ctrAccpContObj.getInputBrId());//登记机构
            cmisLmt0012ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));//登记日期
            log.info("银承合同【{}】前往额度系统恢复原合同【{}】额度开始,请求报文为：【{}】",ctrAccpContObj.getContNo(), ctrAccpContObj.getOrigiContNo(), JSON.toJSONString(cmisLmt0012ReqDto));
            ResultDto<CmisLmt0012RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
            log.info("银承合同【{}】前往额度系统恢复原合同【{}】额度结束,响应报文为：【{}】",ctrAccpContObj.getContNo(), ctrAccpContObj.getOrigiContNo(),JSON.toJSONString(resultDtoDto));
            if (!"0".equals(resultDtoDto.getCode())) {
                log.error("银承合同【{}】签订异常",ctrAccpContObj.getContNo());
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDtoDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.error("恢复原合同额度异常！" + resultDtoDto.getData().getErrorMsg());
                throw BizException.error(null, code, resultDtoDto.getData().getErrorMsg());
            }
        }

        guarBusinessRelService.sendBusinf("03", CmisCommonConstants.STD_BUSI_TYPE_07, ctrAccpCont.getContNo());
        int result = this.updateSelective(ctrAccpContObj);
        if (result > 0) {
            // 银承合同签订生成档案归档任务
            createDocArchive(ctrAccpContObj);
        }
        return result;
    }

    // 承兑行白名单的占额
    public void acceptanceLmt(String contNo){
        CtrAccpCont ctrAccpCont = ctrAccpContMapper.selectByContNo(contNo);
        if(!CmisCommonConstants.GUAR_MODE_40.equals(ctrAccpCont.getGuarMode()) && !CmisCommonConstants.GUAR_MODE_60.equals(ctrAccpCont.getGuarMode())
                && !CmisCommonConstants.GUAR_MODE_00.equals(ctrAccpCont.getGuarMode())){
            List<GrtGuarCont> grtGuarContList = grtGuarBizRstRelService.queryGrtGuarContByParams(contNo);
            if(grtGuarContList.isEmpty()){
                log.error("主合同【{}】未关联担保合同！",contNo);
                throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            for (GrtGuarCont grtGuarCont : grtGuarContList) {
                if (CmisCommonConstants.GUAR_MODE_20.equals(grtGuarCont.getGuarWay())) {
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("guarContNo", grtGuarCont.getGuarContNo());
                    List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.queryGuarBaseInfoDataByGuarContNo(queryModel);
                    for (GuarBaseInfo guarBaseInfo : guarBaseInfoList) {
                        if (CmisCommonConstants.STD_GRT_FLAG_02.equals(guarBaseInfo.getGuarType()) && ("ZY0301003".equals(guarBaseInfo.getGuarTypeCd()) || "ZY0301005".equals(guarBaseInfo.getGuarTypeCd()))) {
                            CmisLmt0024ReqDto cmisLmt0024ReqDto = new CmisLmt0024ReqDto();
                            cmisLmt0024ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);//系统编号
                            cmisLmt0024ReqDto.setInstuCde(CmisCommonUtils.getInstucde(guarBaseInfo.getManagerBrId()));//金融机构代码
                            cmisLmt0024ReqDto.setIsOccLmt(CmisCommonConstants.STD_ZB_YES_NO_1);//是否占用
                            cmisLmt0024ReqDto.setInputId(guarBaseInfo.getInputId());//登记人
                            cmisLmt0024ReqDto.setInputBrId(guarBaseInfo.getInputBrId());//登记机构
                            cmisLmt0024ReqDto.setInputDate(guarBaseInfo.getInputDate());//登记日期

                            List<CmisLmt0024OccRelListReqDto> occRelList = new ArrayList<>();
                            CmisLmt0024OccRelListReqDto occRelListReqDto = new CmisLmt0024OccRelListReqDto();
                            occRelListReqDto.setDealBizNo(guarBaseInfo.getGuarNo());//交易业务编号
                            occRelListReqDto.setCusId(guarBaseInfo.getCusId());//客户编号
                            occRelListReqDto.setPrdNo("YPZY");//产品编号
                            occRelListReqDto.setPrdName("银票质押");//产品名称
                            occRelListReqDto.setOrigiDealBizNo("");//原交易业务编号
                            occRelListReqDto.setOrigiRecoverType("");//原交易业务恢复类型
                            occRelListReqDto.setStartDate(grtGuarCont.getGuarStartDate());//起始日
                            occRelListReqDto.setEndDate(grtGuarCont.getGuarEndDate());//到期日
                            occRelListReqDto.setOrgCusId(guarBaseInfo.getAorgNo());//承兑行客户号
                            occRelListReqDto.setBizTotalAmt(guarBaseInfo.getConfirmAmt());//占用金额
                            occRelList.add(occRelListReqDto);
                            cmisLmt0024ReqDto.setOccRelList(occRelList);
                            log.info("银承合同【{}】，前往额度系统进行承兑行白名单额度占用开始,请求报文为:【{}】", contNo, cmisLmt0024ReqDto.toString());
                            ResultDto<CmisLmt0024RespDto> resultDtoDto = cmisLmtClientService.cmislmt0024(cmisLmt0024ReqDto);
                            log.info("银承合同【{}】，前往额度系统进行承兑行白名单额度占用开始,响应报文为:【{}】", contNo, resultDtoDto.toString());
                            if (!"0".equals(resultDtoDto.getCode())) {
                                log.error("额度0024接口调用失败");
                                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                            }
                            if (!"0000".equals(resultDtoDto.getData().getErrorCode())) {
                                log.error("承兑行白名单额度占用失败！错误信息为：【{}】", resultDtoDto.getData().getErrorMsg());
                                throw BizException.error(null, resultDtoDto.getData().getErrorCode(), resultDtoDto.getData().getErrorMsg());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @方法名称：selectByIqpSerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:03
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrAccpCont selectByIqpSerno(String serno) {
        return ctrAccpContMapper.selectByIqpSerno(serno);
    }

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/20 15:03
     * @修改记录：修改时间 修改人员 修改时间
     */
    public CtrAccpCont selectByContNo(String contNo) {
        return ctrAccpContMapper.selectByContNo(contNo);
    }

    /**
     * @方法名称: 银承贷款合同注销
     * @方法描述: 根据合同号更新签订状态和时间, 包含合同项下的担保合同签订
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map onLogOut(Map params) {
        {
            Map result = new HashMap();
            String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
            String rtnMsg = EcbEnum.IQP_SUCCESS_DEF.value;
            String contNo = "";
            String contStatus = "";
            int pvpLoanAppCount = 0;
            int accLoanCount = 0;
            try {
                // 获取当前合同以及担保合同信息0
                // 更新当前登录信息
                User userInfo = SessionUtils.getUserInformation();
                String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                contNo = (String) params.get("contNo");//主键合同编号
                contStatus = (String) params.get("contStatus");//合同状态
                HashMap<String, String> queryMap = new HashMap<>();
                queryMap.put("contNo", contNo);
                CtrAccpCont ctrAccpCont = ctrAccpContMapper.queryCtrAccpContByDataParams(queryMap);
                // 先判断合同是否存在未结清的业务
                String responseResult = ctrLoanContService.isContUncleared(ctrAccpCont.getContNo());
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(responseResult)){
                    rtnCode = EcbEnum.ECB020055.key;
                    rtnMsg = EcbEnum.ECB020055.value;
                    return result;
                }
                // 获取合同注销后的合同状态
                String finalContStatus = ctrLoanContService.getContStatusAfterLogout(ctrAccpCont.getEndDate(),ctrAccpCont.getContStatus());
                if ("100".equals(ctrAccpCont.getContStatus())) {
                    // 更新 合同信息
                    ctrAccpCont.setUpdId(userInfo.getLoginCode());
                    ctrAccpCont.setUpdBrId(userInfo.getOrg().getCode());
                    ctrAccpCont.setUpdDate(nowDate);
                    ctrAccpCont.setContStatus(finalContStatus);
                    updateSelective(ctrAccpCont);
                } else if ("200".equals(ctrAccpCont.getContStatus())) {
                    //出账申请，审批结束。 pvploanapp
                    Map pvpQueryMap = new HashMap();
                    pvpQueryMap.put("contNo", contNo);
                    ctrAccpCont.setUpdId(userInfo.getLoginCode());
                    ctrAccpCont.setUpdBrId(userInfo.getOrg().getCode());
                    ctrAccpCont.setUpdDate(nowDate);
                    ctrAccpCont.setContStatus(finalContStatus);
                    updateSelective(ctrAccpCont);
                }
                //合同注销，释放额度
                String guarMode = ctrAccpCont.getGuarMode();
                //恢复敞口
                BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
                //恢复总额
                BigDecimal recoverAmtCny = BigDecimal.ZERO;
                if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                        || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
                    recoverSpacAmtCny = BigDecimal.ZERO;
                    recoverAmtCny = ctrAccpCont.getContHighAvlAmt();
                } else {
                    recoverSpacAmtCny = ctrAccpCont.getContHighAvlAmt();
                    recoverAmtCny = ctrAccpCont.getContHighAvlAmt().add(ctrAccpCont.getBailAmt());
                }
                // 根据合是否存在业务，得到恢复类型
                String recoverType = "";
                List<AccAccp> accAccpList = accAccpService.selectByContNo(ctrAccpCont.getContNo());
                if (CollectionUtils.nonEmpty(accAccpList)) {
                    //结清注销
                    for (AccAccp accAccp : accAccpList) {
                        if (CmisCommonConstants.STD_ACC_ACCP_STATUS_2.equals(accAccp.getAccStatus()) ||
                                CmisCommonConstants.STD_ACC_ACCP_STATUS_7.equals(accAccp.getAccStatus())) {
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                        } else {
                            recoverType = CmisLmtConstants.STD_RECOVER_TYPE_01;
                            break;
                        }
                    }
                } else {
                    //未用注销
                    recoverType = CmisLmtConstants.STD_RECOVER_TYPE_02;
                }
                CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
                cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrAccpCont.getManagerBrId()));//金融机构代码
                cmisLmt0012ReqDto.setBizNo(ctrAccpCont.getContNo());//合同编号
                cmisLmt0012ReqDto.setRecoverType(recoverType);//恢复类型
                cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
                cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
                cmisLmt0012ReqDto.setInputId(ctrAccpCont.getInputId());
                cmisLmt0012ReqDto.setInputBrId(ctrAccpCont.getInputBrId());
                cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                log.info("根据合同编号【{}】，前往额度系统进行额度恢复开始,请求报文为:【{}】", contNo, cmisLmt0012ReqDto.toString());
                ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
                log.info("根据合同编号【{}】，前往额度系统进行额度恢复结束,响应报文为:【{}】", contNo, resultDto.toString());
                String code = resultDto.getData().getErrorCode();
                if(!"0".equals(resultDto.getCode())){
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
                if (!"0000".equals(code)) {
                    log.error("业务申请恢复额度异常！");
                    throw BizException.error(null,code, resultDto.getData().getErrorMsg());
                }

                //不存在电子用印的情况下，需要作废档案归档任务
                if (!"1".equals(ctrAccpCont.getIsESeal())) {
                    log.info("根据业务申请编号【" + ctrAccpCont.getSerno() + "】需要作废档案归档任务开始");
                    try {
                        docArchiveInfoService.invalidByBizSerno(ctrAccpCont.getSerno(),"");
                    } catch (Exception e) {
                        log.info("根据业务申请编号【" + ctrAccpCont.getSerno() + "】需要作废档案归档任务异常：" + e.getMessage());
                    }
                    log.info("根据业务申请编号【" + ctrAccpCont.getSerno() + "】需要作废档案归档任务结束");
                }

            } catch (Exception e) {
                log.error("银承贷款合同申请注销异常", e.getMessage(), e);
                rtnCode = EcbEnum.CTR_EXCEPTION_DEF.key;
                rtnMsg = EcbEnum.CTR_EXCEPTION_DEF.value;
            } finally {
                //成功则无需处理
                result.put("rtnCode", rtnCode);
                result.put("rtnMsg", rtnMsg);
            }
            return result;
        }
    }

    /**
     * 根据银承协议编号存在生效的合同
     *
     * @return
     */
    public int queryNumContByContNo(@Param("contNo") String contNo) {
        return ctrAccpContMapper.queryNumContByContNo(contNo);
    }

    /**
     * 查询银承合同信息
     *
     * @param xdht0002DataReqDto
     * @return
     */
    public Xdht0002DataRespDto queryContInfoByContNoAndCusId(Xdht0002DataReqDto xdht0002DataReqDto) {
        return ctrAccpContMapper.queryContInfoByContNoAndCusId(xdht0002DataReqDto);
    }

    /**
     * 根据合同编号、合同状态查询 授信台账编号
     *
     * @param queryLmtAccNoMap
     * @return
     */
    public String queryLmtAccNo(Map queryLmtAccNoMap) {
        return ctrAccpContMapper.queryLmtAccNo(queryLmtAccNoMap);
    }

    /**
     * @方法名称: selectByQuerymodel
     * @方法描述: 根据入参查询合同数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrAccpCont> selectByQuerymodel(QueryModel model) {
        return ctrAccpContMapper.selectByModel(model);
    }

    /**
     * @param contNo
     * @return BigDecimal
     * @author qw
     * @date 2021/8/20 18:09
     * @version 1.0.0
     * @desc 根据合同号获取合同可出账金额
     * @修改历史:
     */
    public BigDecimal getCanOutAccountAmt(String contNo) {
        //可出账金额
        BigDecimal canOutAccountAmt = BigDecimal.ZERO;
        CtrAccpCont ctrAccpCont = ctrAccpContMapper.selectByContNo(contNo);
        if (Objects.isNull(ctrAccpCont)) {
            throw BizException.error(null, EcbEnum.ECB020022.key, EcbEnum.ECB020022.value);
        }
        //合同最高可用信金额折算人民币金额
        BigDecimal contHighAmt = ctrAccpCont.getContHighAvlAmt();

        //业务敞口余额（未扣减保证金）
        BigDecimal totalSpacBalance = BigDecimal.ZERO;
        //业务敞口初始总金额（未扣减保证金）
        BigDecimal totalOrigiSpacAmt = BigDecimal.ZERO;

        //业务敞口余额（扣减保证金）
        BigDecimal totalSpacBalanceSubtractBailAmt = BigDecimal.ZERO;
        //业务敞口初始总金额（扣减保证金）
        BigDecimal totalOrigiSpacSubtractBailAmt = BigDecimal.ZERO;

        //票面金额
        BigDecimal draftAmt = BigDecimal.ZERO;
        //保证金金额
        BigDecimal bailAmt = BigDecimal.ZERO;

        List<AccAccp> accAccpList = accAccpService.selectByContNo(ctrAccpCont.getContNo());
        if (CollectionUtils.nonEmpty(accAccpList)) {
            for (AccAccp accAccp : accAccpList) {
                //除去了台账状态为未用退回和作废的银票
                List<AccAccpDrftSub> accAccpDrftSubList = accAccpDrftSubService.selectByBillNo(accAccp.getCoreBillNo());
                if (CollectionUtils.nonEmpty(accAccpDrftSubList)) {
                    for (AccAccpDrftSub accAccpDrftSub : accAccpDrftSubList) {
                        //银票台账票面总金额
                        draftAmt = draftAmt.add(accAccpDrftSub.getDraftAmt());
                        //保证金总金额
                        bailAmt = bailAmt.add(accAccpDrftSub.getBailAmt());
                    }
                }
                totalOrigiSpacSubtractBailAmt = totalOrigiSpacSubtractBailAmt.add(Optional.ofNullable(accAccp.getOrigiOpenAmt()).orElse(BigDecimal.ZERO));//业务敞口初始总金额（扣减保证金）
            }
            totalSpacBalanceSubtractBailAmt = draftAmt.subtract(bailAmt);//业务敞口余额（扣减保证金）

            totalSpacBalance = draftAmt;//业务敞口余额（未扣减保证金）
            totalOrigiSpacAmt = draftAmt;//业务敞口初始总金额（未扣减保证金）
            //低风险
            if (CmisCommonConstants.GUAR_MODE_21.equals(ctrAccpCont.getGuarMode()) ||
                    CmisCommonConstants.GUAR_MODE_40.equals(ctrAccpCont.getGuarMode()) ||
                    CmisCommonConstants.GUAR_MODE_60.equals(ctrAccpCont.getGuarMode())) {
                //一般合同
                if (CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrAccpCont.getContType())) {
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalance);
                } else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacAmt);
                }
            } else {
                //一般合同
                if (CmisCommonConstants.STD_CONT_TYPE_1.equals(ctrAccpCont.getContType())) {
                    canOutAccountAmt = contHighAmt.subtract(totalOrigiSpacSubtractBailAmt);
                } else {
                    //最高额合同
                    canOutAccountAmt = contHighAmt.subtract(totalSpacBalanceSubtractBailAmt);
                }
            }
        } else {
            canOutAccountAmt = contHighAmt;
        }
        return canOutAccountAmt;
    }

    /**
     * 银承合同签订生成档案归档任务
     * @author jijian_yx
     * @date 2021/9/10 20:51
     **/
    private void createDocArchive(CtrAccpCont ctrAccpCont) {
        //档案归档任务规则
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(ctrAccpCont.getCusId());
        DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
        docArchiveClientDto.setDocClass("03");// 档案分类 03重要信息档案
        docArchiveClientDto.setDocType("14");// 档案类型 14承兑业务
        docArchiveClientDto.setBizSerno(ctrAccpCont.getSerno());// 关联流水号
        docArchiveClientDto.setCusId(ctrAccpCont.getCusId());// 客户号
        docArchiveClientDto.setCusName(ctrAccpCont.getCusName());// 客户名称
        docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());// 证件号
        docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());// 证件类型
        docArchiveClientDto.setManagerId(ctrAccpCont.getManagerId());// 责任人
        docArchiveClientDto.setManagerBrId(ctrAccpCont.getManagerBrId());// 责任机构
        docArchiveClientDto.setInputId(ctrAccpCont.getInputId());// 登记人
        docArchiveClientDto.setInputBrId(ctrAccpCont.getInputBrId());// 登记机构
        docArchiveClientDto.setContNo(ctrAccpCont.getContNo());
        docArchiveClientDto.setLoanAmt(ctrAccpCont.getContAmt());
        docArchiveClientDto.setStartDate(ctrAccpCont.getStartDate());
        docArchiveClientDto.setEndDate(ctrAccpCont.getEndDate());
        docArchiveClientDto.setPrdId(ctrAccpCont.getPrdId());
        docArchiveClientDto.setPrdName(ctrAccpCont.getPrdName());
        int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
        if (num < 1) {
            log.info("银承合同签订生成档案任务失败,业务流水号[{}]", ctrAccpCont.getSerno());
        }
    }

    /**
     * @方法名称: getSumContAmt
     * @方法描述: 根据额度编号查询合同金额总和
     * @参数与返回说明:
     * @算法描述: 无
     */

    public BigDecimal getSumContAmt(String lmtAccNo) {
        return ctrAccpContMapper.getSumContAmt(lmtAccNo);
    }
}
