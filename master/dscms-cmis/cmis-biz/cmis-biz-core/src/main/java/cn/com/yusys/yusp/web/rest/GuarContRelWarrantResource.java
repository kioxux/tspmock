/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import cn.com.yusys.yusp.service.GuarContRelWarrantService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContRelWarrantResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-13 15:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarcontrelwarrant")
public class GuarContRelWarrantResource {
    @Autowired
    private GuarContRelWarrantService guarContRelWarrantService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<GuarContRelWarrant>> query() {
        QueryModel queryModel = new QueryModel();
        List<GuarContRelWarrant> list = guarContRelWarrantService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * 根据合同号查询权证入库核心担保编号列表
     * @函数名称:selectCoreGuarantyNoByContNo
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectcoreguarantynobycontno/{contNo}")
    protected ResultDto<String> selectCoreGuarantyNoByContNo(@PathVariable("contNo") String contNo){
        String result = guarContRelWarrantService.selectCoreGuarantyNoByContNo(contNo);
        return new ResultDto<>(result);
    }
}