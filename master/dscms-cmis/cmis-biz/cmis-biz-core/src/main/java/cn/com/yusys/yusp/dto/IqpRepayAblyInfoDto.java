package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayAblyInfo
 * @类描述: iqp_repay_ably_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:16:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpRepayAblyInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 商贷月还款金额 **/
	private java.math.BigDecimal mRepayAmt;
	
	/** 公积金月还款 **/
	private java.math.BigDecimal mHpfRepayAmt;
	
	/** I/I(%) **/
	private java.math.BigDecimal mAmt;
	
	/** D/I(%) **/
	private java.math.BigDecimal dAmt;
	
	/** D/I(%)（折算后） **/
	private java.math.BigDecimal dAmtCvt;
	
	/** 总还款额 **/
	private java.math.BigDecimal mTotalAmt;
	
	/** 月收入总额（元） **/
	private java.math.BigDecimal mIncomeTot;
	
	/** 资产价值汇总（折算后） **/
	private java.math.BigDecimal totalAssetCvt;
	
	/** 月物业管理费（元） **/
	private java.math.BigDecimal mPropertyFee;
	
	/** 已有负债月还款额汇总 **/
	private java.math.BigDecimal debtMRepayTot;
	
	/** 已有负债金额汇总 **/
	private java.math.BigDecimal debtAmtTot;
	
	/** 已有负债月还款额汇总（折算后） **/
	private java.math.BigDecimal debtMRepayTotCvt;
	
	/** 已有负债金额汇总（折算后） **/
	private java.math.BigDecimal debtAmtTotCvt;
	
	/** 测算阶段 **/
	private String calculateStage;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param mRepayAmt
	 */
	public void setMRepayAmt(java.math.BigDecimal mRepayAmt) {
		this.mRepayAmt = mRepayAmt;
	}
	
    /**
     * @return MRepayAmt
     */	
	public java.math.BigDecimal getMRepayAmt() {
		return this.mRepayAmt;
	}
	
	/**
	 * @param mHpfRepayAmt
	 */
	public void setMHpfRepayAmt(java.math.BigDecimal mHpfRepayAmt) {
		this.mHpfRepayAmt = mHpfRepayAmt;
	}
	
    /**
     * @return MHpfRepayAmt
     */	
	public java.math.BigDecimal getMHpfRepayAmt() {
		return this.mHpfRepayAmt;
	}
	
	/**
	 * @param mAmt
	 */
	public void setMAmt(java.math.BigDecimal mAmt) {
		this.mAmt = mAmt;
	}
	
    /**
     * @return MAmt
     */	
	public java.math.BigDecimal getMAmt() {
		return this.mAmt;
	}
	
	/**
	 * @param dAmt
	 */
	public void setDAmt(java.math.BigDecimal dAmt) {
		this.dAmt = dAmt;
	}
	
    /**
     * @return DAmt
     */	
	public java.math.BigDecimal getDAmt() {
		return this.dAmt;
	}
	
	/**
	 * @param dAmtCvt
	 */
	public void setDAmtCvt(java.math.BigDecimal dAmtCvt) {
		this.dAmtCvt = dAmtCvt;
	}
	
    /**
     * @return DAmtCvt
     */	
	public java.math.BigDecimal getDAmtCvt() {
		return this.dAmtCvt;
	}
	
	/**
	 * @param mTotalAmt
	 */
	public void setMTotalAmt(java.math.BigDecimal mTotalAmt) {
		this.mTotalAmt = mTotalAmt;
	}
	
    /**
     * @return MTotalAmt
     */	
	public java.math.BigDecimal getMTotalAmt() {
		return this.mTotalAmt;
	}
	
	/**
	 * @param mIncomeTot
	 */
	public void setMIncomeTot(java.math.BigDecimal mIncomeTot) {
		this.mIncomeTot = mIncomeTot;
	}
	
    /**
     * @return MIncomeTot
     */	
	public java.math.BigDecimal getMIncomeTot() {
		return this.mIncomeTot;
	}
	
	/**
	 * @param totalAssetCvt
	 */
	public void setTotalAssetCvt(java.math.BigDecimal totalAssetCvt) {
		this.totalAssetCvt = totalAssetCvt;
	}
	
    /**
     * @return TotalAssetCvt
     */	
	public java.math.BigDecimal getTotalAssetCvt() {
		return this.totalAssetCvt;
	}
	
	/**
	 * @param mPropertyFee
	 */
	public void setMPropertyFee(java.math.BigDecimal mPropertyFee) {
		this.mPropertyFee = mPropertyFee;
	}
	
    /**
     * @return MPropertyFee
     */	
	public java.math.BigDecimal getMPropertyFee() {
		return this.mPropertyFee;
	}
	
	/**
	 * @param debtMRepayTot
	 */
	public void setDebtMRepayTot(java.math.BigDecimal debtMRepayTot) {
		this.debtMRepayTot = debtMRepayTot;
	}
	
    /**
     * @return DebtMRepayTot
     */	
	public java.math.BigDecimal getDebtMRepayTot() {
		return this.debtMRepayTot;
	}
	
	/**
	 * @param debtAmtTot
	 */
	public void setDebtAmtTot(java.math.BigDecimal debtAmtTot) {
		this.debtAmtTot = debtAmtTot;
	}
	
    /**
     * @return DebtAmtTot
     */	
	public java.math.BigDecimal getDebtAmtTot() {
		return this.debtAmtTot;
	}
	
	/**
	 * @param debtMRepayTotCvt
	 */
	public void setDebtMRepayTotCvt(java.math.BigDecimal debtMRepayTotCvt) {
		this.debtMRepayTotCvt = debtMRepayTotCvt;
	}
	
    /**
     * @return DebtMRepayTotCvt
     */	
	public java.math.BigDecimal getDebtMRepayTotCvt() {
		return this.debtMRepayTotCvt;
	}
	
	/**
	 * @param debtAmtTotCvt
	 */
	public void setDebtAmtTotCvt(java.math.BigDecimal debtAmtTotCvt) {
		this.debtAmtTotCvt = debtAmtTotCvt;
	}
	
    /**
     * @return DebtAmtTotCvt
     */	
	public java.math.BigDecimal getDebtAmtTotCvt() {
		return this.debtAmtTotCvt;
	}
	
	/**
	 * @param calculateStage
	 */
	public void setCalculateStage(String calculateStage) {
		this.calculateStage = calculateStage == null ? null : calculateStage.trim();
	}
	
    /**
     * @return CalculateStage
     */	
	public String getCalculateStage() {
		return this.calculateStage;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}