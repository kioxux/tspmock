package cn.com.yusys.yusp.service.server.xdht0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.CtrYddSignResult;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.server.xdht0016.req.Xdht0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0016.resp.Xdht0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrYddSignResultMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.CtrLoanContService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称:
 * @类名称: 合同签订维护（优抵贷不见面抵押签约）
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021/6/11 9:13
 * @修改备注:(交易码：dzhtqy DzhtqyAction)
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0016Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0016Service.class);

    @Autowired
    private CtrYddSignResultMapper ctrYddSignResultMapper;
    @Autowired
    private CtrLoanContService ctrLoanContService;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0016DataRespDto xdht0016(Xdht0016DataReqDto xdht0016DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataReqDto));
        Xdht0016DataRespDto xdht0016DataRespDto = new Xdht0016DataRespDto();
        CtrYddSignResult CtrYddSignResult = new CtrYddSignResult();
        QueryModel queryModel = new QueryModel();
        String msg = "";
        try {
            //4、查询当前日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");

            if (!StringUtils.hasLength(xdht0016DataReqDto.getCertNo())) {
                msg += "身份证号码【cert_no】不能为空！";
            }
            if (!StringUtils.hasLength(xdht0016DataReqDto.getLoanContNo())) {
                msg += "借款合同号【cont_no】不能为空！";
            }
            if ("".equals(msg)) {
                // 查询ctr_ydd_sign_result对象
                queryModel.addCondition("certCode", xdht0016DataReqDto.getCertNo());//证件号
                queryModel.addCondition("contNo", xdht0016DataReqDto.getLoanContNo());//
                List<CtrYddSignResult> ctrrResult = ctrYddSignResultMapper.selectByModel(queryModel);
                //如果ctr_ydd_sign_result对象不为空，执行更新操作
                if (ctrrResult != null && ctrrResult.size() > 0) {
                    // 签订对应合同，更新对应签订状态
                    // 借款合同
                    if (StringUtils.hasLength(xdht0016DataReqDto.getLoanContSignStatus())) {
                        CtrYddSignResult.setCertCode(xdht0016DataReqDto.getCertNo());
                        CtrYddSignResult.setContNo(xdht0016DataReqDto.getLoanContNo());
                        CtrYddSignResult.setGuarContNo(xdht0016DataReqDto.getGrtContNo());
                        CtrYddSignResult.setLoanContSignStatus(xdht0016DataReqDto.getLoanContSignStatus());
                        CtrYddSignResult.setIsMainBorrower(xdht0016DataReqDto.getIsMborrow());
                        CtrYddSignResult.setLoanContImageNo(xdht0016DataReqDto.getLoanContImageSerno());
                        logger.info("**********XDHT0016**借款合同修改,修改参数为:{}", JSON.toJSONString(CtrYddSignResult));
                        ctrYddSignResultMapper.updateByPrimaryKeySelective(CtrYddSignResult);
                        //借款合同签订
                        try {
                            //根据合同号查询合同信息
                            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(xdht0016DataReqDto.getLoanContNo());
                            String contStatus = ctrLoanCont.getContStatus();
                            if ("100".equals(contStatus)) {//未签约状态
                                ctrLoanCont.setContStatus("200");
                                ctrLoanCont.setSignDate(openDay);
                                int count = ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
                                // 合同校验接口开设
                                logger.info("************XDHT0016*发送额度系统cmislmt0009合同校验接口开始,校验信息【{}】", JSON.toJSONString(ctrLoanCont));
                                ResultDto cmisLmt0009RespDto = cmisBizXwCommonService.contLmtForXw(ctrLoanCont);
                                logger.info("************XDHT0016*发送额度系统cmislmt0009合同校验接口结束,返回信息【{}】", JSON.toJSONString(cmisLmt0009RespDto));
                                if (count > 0) {
                                    logger.info("************借款合同签订成功*******************");
                                }
                            }
                        } catch (BizException e) {
                            logger.info("************借款合同签订失败*******************失败原因:");
                            e.printStackTrace();
                            throw BizException.error(null, EcbEnum.ECB010029.key, "借款合同签订失败");
                        }
                    }
                    // 担保合同
                    if (StringUtils.hasLength(xdht0016DataReqDto.getGrtContSignStatus())) {
                        CtrYddSignResult = new CtrYddSignResult();
                        CtrYddSignResult.setCertCode(xdht0016DataReqDto.getCertNo());
                        CtrYddSignResult.setContNo(xdht0016DataReqDto.getLoanContNo());
                        CtrYddSignResult.setGuarContNo(xdht0016DataReqDto.getGrtContNo());
                        CtrYddSignResult.setGuarContSignStatus(xdht0016DataReqDto.getGrtContSignStatus());
                        CtrYddSignResult.setGuarContImageNo(xdht0016DataReqDto.getGrtContImageSerno());
                        CtrYddSignResult.setLoanContImageNo(xdht0016DataReqDto.getLoanContImageSerno());
                        logger.info("**********XDHT0016**担保合同修改,修改参数为:{}", JSON.toJSONString(CtrYddSignResult));
                        ctrYddSignResultMapper.updateByPrimaryKeySelective(CtrYddSignResult);
                    }
                    // 主债权合同及抵押合同表
                    if (StringUtils.hasLength(xdht0016DataReqDto.getMainClaimsPldContSignStatus()) && StringUtils.hasLength(xdht0016DataReqDto.getRealEstateRegiAppbookSignStatus())) {
                        CtrYddSignResult = new CtrYddSignResult();
                        CtrYddSignResult.setCertCode(xdht0016DataReqDto.getCertNo());
                        CtrYddSignResult.setContNo(xdht0016DataReqDto.getLoanContNo());
                        CtrYddSignResult.setGuarContNo(xdht0016DataReqDto.getGrtContNo());
                        CtrYddSignResult.setClaimContSignStatus(xdht0016DataReqDto.getMainClaimsPldContSignStatus());
                        CtrYddSignResult.setClaimContImageNo(xdht0016DataReqDto.getMainClaimsContImageSerno());
                        CtrYddSignResult.setClaimContNo(xdht0016DataReqDto.getMainClaimsPldContNo());
                        CtrYddSignResult.setRegistAppSignStatus(xdht0016DataReqDto.getRealEstateRegiAppbookSignStatus());
                        CtrYddSignResult.setRegistAppImageNo(xdht0016DataReqDto.getRegiAppbookImageSerno());
                        CtrYddSignResult.setRegistAppNo(xdht0016DataReqDto.getRealEstateRegiAppbookNo());
                        CtrYddSignResult.setIsMainBorrower(xdht0016DataReqDto.getIsMborrow());
                        CtrYddSignResult.setGuarContSignStatus(xdht0016DataReqDto.getGrtContSignStatus());
                        CtrYddSignResult.setGuarContImageNo(xdht0016DataReqDto.getGrtContImageSerno());
                        CtrYddSignResult.setLoanContImageNo(xdht0016DataReqDto.getLoanContImageSerno());
                        logger.info("**********XDHT0016**主债权合同及抵押合同表修改,修改参数为:{}", JSON.toJSONString(CtrYddSignResult));
                        ctrYddSignResultMapper.updateByPrimaryKeySelective(CtrYddSignResult);
                        // 主债权合同及抵押合同表签约成功后 签约担保合同
                        if (StringUtil.isNotEmpty(xdht0016DataReqDto.getGrtContNo())) {//担保合同不为空
                            GrtGuarCont grtGuarCont = new GrtGuarCont();
                            grtGuarCont.setGuarContState("101");//生效
                            grtGuarCont.setSignDate(openDay);
                            grtGuarCont.setGuarContNo(xdht0016DataReqDto.getGrtContNo());
                            grtGuarContMapper.updateSelectiveByGuarContNo(grtGuarCont);
                        }
                    }
                } else {
                    String loanContSignStatus = xdht0016DataReqDto.getLoanContSignStatus();
                    String guarContNo = xdht0016DataReqDto.getGrtContNo();
                    //如果ctr_ydd_sign_result对象为空，执行新增操作
                    CtrYddSignResult = new CtrYddSignResult();
                    CtrYddSignResult.setCertCode(xdht0016DataReqDto.getCertNo());
                    CtrYddSignResult.setContNo(xdht0016DataReqDto.getLoanContNo());
                    CtrYddSignResult.setGuarContNo(guarContNo);
                    CtrYddSignResult.setLoanContSignStatus(loanContSignStatus);
                    CtrYddSignResult.setIsMainBorrower(xdht0016DataReqDto.getIsMborrow());
                    CtrYddSignResult.setLoanContImageNo(xdht0016DataReqDto.getLoanContImageSerno());
                    CtrYddSignResult.setClaimContSignStatus(xdht0016DataReqDto.getMainClaimsPldContSignStatus());
                    CtrYddSignResult.setClaimContImageNo(xdht0016DataReqDto.getMainClaimsContImageSerno());
                    CtrYddSignResult.setClaimContNo(xdht0016DataReqDto.getMainClaimsPldContNo());
                    CtrYddSignResult.setRegistAppSignStatus(xdht0016DataReqDto.getRealEstateRegiAppbookSignStatus());
                    CtrYddSignResult.setRegistAppImageNo(xdht0016DataReqDto.getRegiAppbookImageSerno());
                    CtrYddSignResult.setRegistAppNo(xdht0016DataReqDto.getRealEstateRegiAppbookNo());
                    CtrYddSignResult.setGuarContSignStatus(xdht0016DataReqDto.getGrtContSignStatus());
                    CtrYddSignResult.setGuarContImageNo(xdht0016DataReqDto.getGrtContImageSerno());
                    CtrYddSignResult.setImageDir(xdht0016DataReqDto.getImageCatalog());
                    logger.info("**********XDHT0016**执行新增操作,新增参数为:{}", JSON.toJSONString(CtrYddSignResult));
                    int count = ctrYddSignResultMapper.insertSelective(CtrYddSignResult);
                    if ("1".equals(loanContSignStatus) && count > 0) {//借款合同已签约，且插入成功
                        //借款合同签订、担保合同状态签订
                        try {
                            //根据合同号查询合同信息
                            CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(xdht0016DataReqDto.getLoanContNo());
                            ctrLoanCont.setContStatus("200");
                            ctrLoanCont.setSignDate(openDay);
                            int num = ctrLoanContMapper.updateByPrimaryKey(ctrLoanCont);
                            // 合同校验接口开设
                            logger.info("************XDHT0016*发送额度系统cmislmt0009合同校验接口开始,校验信息【{}】", JSON.toJSONString(ctrLoanCont));
                            ResultDto cmisLmt0009RespDto = cmisBizXwCommonService.contLmtForXw(ctrLoanCont);
                            logger.info("************XDHT0016*发送额度系统cmislmt0009合同校验接口结束,返回信息【{}】", JSON.toJSONString(cmisLmt0009RespDto));
                            if (num > 0) {
                                logger.info("************借款合同签订成功*******************");
                            }
                        } catch (BizException e) {
                            logger.info("************借款合同签订失败*******************失败原因:");
                            e.printStackTrace();
                            throw BizException.error(null, EcbEnum.ECB010029.key, "借款合同签订失败");
                        }
                    }
                }
                xdht0016DataRespDto.setOpFlag("S");
                xdht0016DataRespDto.setOpMsg("交易成功");
            } else {
                xdht0016DataRespDto.setOpFlag("F");
                xdht0016DataRespDto.setOpMsg("交易失败----》" + msg);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, e.getMessage());
            xdht0016DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdht0016DataRespDto.setOpMsg(e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdht0016DataRespDto.setOpFlag("F");
            xdht0016DataRespDto.setOpMsg("交易失败");
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataReqDto));
        return xdht0016DataRespDto;
    }
}
