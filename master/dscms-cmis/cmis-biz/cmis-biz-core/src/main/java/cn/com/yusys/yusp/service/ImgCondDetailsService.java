/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CreditReportQryLstAndRealDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.GrtGuarContDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.ImgCondDetailsMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ImgCondDetailsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-13 14:57:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class ImgCondDetailsService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(ImgCondDetailsService.class);

    @Autowired
    private ImgCondDetailsMapper imgCondDetailsMapper;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public ImgCondDetails selectByPrimaryKey(String pkId) {
        return imgCondDetailsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(ImgCondDetails record) {
        return imgCondDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(ImgCondDetails record) {
        return imgCondDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(ImgCondDetails record) {
        return imgCondDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(ImgCondDetails record) {
        return imgCondDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return imgCondDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return imgCondDetailsMapper.deleteByIds(ids);
    }

    /**
     *@函数名称:imgCondDetailsMapper
     *@函数描述:根据合同查询授信落实条件
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    public List<Map<String, Object>> queryImgCondDetailsContNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = imgCondDetailsMapper.queryImgCondDetailsContNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     *@函数名称:generateImgCondDetailsData
     *@函数描述:根据合同查询授信落实条件
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    public int generateImgCondDetailsData(Map map) {
        ImgCondDetails imgCondDetails = new ImgCondDetails();
        int insertCont = 0;
        List<LmtReplyLoanCond> lmtReplyLoanCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByReplySerno((String) map.get("replySerno"));
        if(CollectionUtils.nonEmpty(lmtReplyLoanCondList)){
            for (LmtReplyLoanCond lmtReplyLoanCond : lmtReplyLoanCondList) {
                imgCondDetails.setPkId(StringUtils.uuid(true));
                imgCondDetails.setCondDesc(lmtReplyLoanCond.getCondDesc());
                imgCondDetails.setContNo((String)map.get("contNo"));
                imgCondDetails.setIsPact(CmisCommonConstants.STD_ZB_YES_NO_0);
                imgCondDetails.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                User userInfo = SessionUtils.getUserInformation();
                if(Objects.nonNull(userInfo)){
                    imgCondDetails.setInputId(userInfo.getLoginCode());
                    imgCondDetails.setInputBrId(userInfo.getOrg().getCode());
                    imgCondDetails.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    imgCondDetails.setUpdId(userInfo.getLoginCode());
                    imgCondDetails.setUpdBrId(userInfo.getOrg().getCode());
                    imgCondDetails.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                }
                insertCont = imgCondDetailsMapper.insertSelective(imgCondDetails);
                if(insertCont<=0){
                    throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
                }
            }
        }
        return insertCont;
    }

    @Transactional(rollbackFor = Exception.class)
    public Map saveImgCondDetails(ImgCondDetails imgCondDetails) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (imgCondDetails == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            String pkId = imgCondDetails.getPkId();
            ImgCondDetails imgCondDetailsData = imgCondDetailsMapper.selectByPrimaryKey(pkId);
            if(Objects.nonNull(imgCondDetailsData)){
                int updateCount = this.updateSelective(imgCondDetails);
                if(updateCount<=0){
                    log.error("用信落实情况"+imgCondDetails.getCondDesc()+"-保存失败！");
                    throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                }
            }else {
                int insertCount = imgCondDetailsMapper.insertSelective(imgCondDetails);
                if(insertCount<=0){
                    log.error("用信落实情况"+imgCondDetails.getCondDesc()+"-保存失败！");
                    throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                }
            }

            result.put("contNo",imgCondDetails.getContNo());
            log.info("用信落实情况"+imgCondDetails.getCondDesc()+"-保存成功！");
        }catch(BizException e){
            log.error("用信落实情况"+imgCondDetails.getCondDesc()+"-保存异常！");
            rtnCode = EcbEnum.EDS990001.key;
            rtnMsg = EcbEnum.EDS990001.value;
        }catch(Exception e){
            log.error("用信落实情况保存异常！",e.getMessage());
            throw BizException.error(null,EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }finally {
            result.put("rtnCode",rtnCode);
            result.put("rtnMsg",rtnMsg);
        }
        return result;
    }
}
