/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptSpdAnysJyxwydRentIncomeDetail;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysJyxwydRentIncomeDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncomeDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-12 22:03:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysJyxwydRentIncomeDetailService {

    @Autowired
    private RptSpdAnysJyxwydRentIncomeDetailMapper rptSpdAnysJyxwydRentIncomeDetailMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptSpdAnysJyxwydRentIncomeDetail selectByPrimaryKey(String pkId, String serno) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.selectByPrimaryKey(pkId, serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptSpdAnysJyxwydRentIncomeDetail> selectAll(QueryModel model) {
        List<RptSpdAnysJyxwydRentIncomeDetail> records = (List<RptSpdAnysJyxwydRentIncomeDetail>) rptSpdAnysJyxwydRentIncomeDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptSpdAnysJyxwydRentIncomeDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysJyxwydRentIncomeDetail> list = rptSpdAnysJyxwydRentIncomeDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptSpdAnysJyxwydRentIncomeDetail record) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysJyxwydRentIncomeDetail record) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptSpdAnysJyxwydRentIncomeDetail record) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysJyxwydRentIncomeDetail record) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String serno) {
        return rptSpdAnysJyxwydRentIncomeDetailMapper.deleteByPrimaryKey(pkId, serno);
    }

    public List<RptSpdAnysJyxwydRentIncomeDetail> selectByRentSerno(String rentSerno){
        return  rptSpdAnysJyxwydRentIncomeDetailMapper.selectByRentSerno(rentSerno);
    }

    public RptSpdAnysJyxwydRentIncomeDetail selectByParams(Map map){
        return rptSpdAnysJyxwydRentIncomeDetailMapper.selectByParams(map);
    }
}
