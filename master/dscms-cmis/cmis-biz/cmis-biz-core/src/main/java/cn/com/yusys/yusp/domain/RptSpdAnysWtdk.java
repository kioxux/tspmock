/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWtdk
 * @类描述: rpt_spd_anys_wtdk数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-19 15:53:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_wtdk")
public class RptSpdAnysWtdk extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 企业全称 **/
	@Column(name = "FULL_NAME_OF_ENTERPRISE", unique = false, nullable = true, length = 80)
	private String fullNameOfEnterprise;
	
	/** 成立日期 **/
	@Column(name = "DATE_OF_INCORPORATION", unique = false, nullable = true, length = 10)
	private String dateOfIncorporation;
	
	/** 注册资本 **/
	@Column(name = "REGISTERED_CAPITAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal registeredCapital;
	
	/** 实际控制人 **/
	@Column(name = "ACTUAL_CONTROLLER", unique = false, nullable = true, length = 80)
	private String actualController;
	
	/** 注册地址 **/
	@Column(name = "COMPANY_REGISTERED_ADDRESS", unique = false, nullable = true, length = 500)
	private String companyRegisteredAddress;
	
	/** 经营范围 **/
	@Column(name = "NATURE_OF_BUSINESS", unique = false, nullable = true, length = 500)
	private String natureOfBusiness;
	
	/** 企业性质 **/
	@Column(name = "NATURE_OF_ENTERPRISE", unique = false, nullable = true, length = 80)
	private String natureOfEnterprise;
	
	/** 行业分类 **/
	@Column(name = "INDUSTRY_CLASSIFICATION", unique = false, nullable = true, length = 80)
	private String industryClassification;
	
	/** 实收资本 **/
	@Column(name = "PAID_IN_CAPITAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidInCapital;
	
	/** 法人代表 **/
	@Column(name = "LEGAL_REPRESENTATIVE", unique = false, nullable = true, length = 80)
	private String legalRepresentative;
	
	/** 实际经营地址 **/
	@Column(name = "ACTUAL_BUSINESS_ADDRESS", unique = false, nullable = true, length = 500)
	private String actualBusinessAddress;
	
	/** 股东情况其他说明 **/
	@Column(name = "OTHER_INF_ABOUT_SHR", unique = false, nullable = true, length = 65535)
	private String otherInfAboutShr;
	
	/** 其他需说明事项 **/
	@Column(name = "OTHER_MATTERS_TO_BE_EXPLAINED", unique = false, nullable = true, length = 65535)
	private String otherMattersToBeExplained;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param fullNameOfEnterprise
	 */
	public void setFullNameOfEnterprise(String fullNameOfEnterprise) {
		this.fullNameOfEnterprise = fullNameOfEnterprise;
	}
	
    /**
     * @return fullNameOfEnterprise
     */
	public String getFullNameOfEnterprise() {
		return this.fullNameOfEnterprise;
	}
	
	/**
	 * @param dateOfIncorporation
	 */
	public void setDateOfIncorporation(String dateOfIncorporation) {
		this.dateOfIncorporation = dateOfIncorporation;
	}
	
    /**
     * @return dateOfIncorporation
     */
	public String getDateOfIncorporation() {
		return this.dateOfIncorporation;
	}
	
	/**
	 * @param registeredCapital
	 */
	public void setRegisteredCapital(java.math.BigDecimal registeredCapital) {
		this.registeredCapital = registeredCapital;
	}
	
    /**
     * @return registeredCapital
     */
	public java.math.BigDecimal getRegisteredCapital() {
		return this.registeredCapital;
	}
	
	/**
	 * @param actualController
	 */
	public void setActualController(String actualController) {
		this.actualController = actualController;
	}
	
    /**
     * @return actualController
     */
	public String getActualController() {
		return this.actualController;
	}
	
	/**
	 * @param companyRegisteredAddress
	 */
	public void setCompanyRegisteredAddress(String companyRegisteredAddress) {
		this.companyRegisteredAddress = companyRegisteredAddress;
	}
	
    /**
     * @return companyRegisteredAddress
     */
	public String getCompanyRegisteredAddress() {
		return this.companyRegisteredAddress;
	}
	
	/**
	 * @param natureOfBusiness
	 */
	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	
    /**
     * @return natureOfBusiness
     */
	public String getNatureOfBusiness() {
		return this.natureOfBusiness;
	}
	
	/**
	 * @param natureOfEnterprise
	 */
	public void setNatureOfEnterprise(String natureOfEnterprise) {
		this.natureOfEnterprise = natureOfEnterprise;
	}
	
    /**
     * @return natureOfEnterprise
     */
	public String getNatureOfEnterprise() {
		return this.natureOfEnterprise;
	}
	
	/**
	 * @param industryClassification
	 */
	public void setIndustryClassification(String industryClassification) {
		this.industryClassification = industryClassification;
	}
	
    /**
     * @return industryClassification
     */
	public String getIndustryClassification() {
		return this.industryClassification;
	}
	
	/**
	 * @param paidInCapital
	 */
	public void setPaidInCapital(java.math.BigDecimal paidInCapital) {
		this.paidInCapital = paidInCapital;
	}
	
    /**
     * @return paidInCapital
     */
	public java.math.BigDecimal getPaidInCapital() {
		return this.paidInCapital;
	}
	
	/**
	 * @param legalRepresentative
	 */
	public void setLegalRepresentative(String legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}
	
    /**
     * @return legalRepresentative
     */
	public String getLegalRepresentative() {
		return this.legalRepresentative;
	}
	
	/**
	 * @param actualBusinessAddress
	 */
	public void setActualBusinessAddress(String actualBusinessAddress) {
		this.actualBusinessAddress = actualBusinessAddress;
	}
	
    /**
     * @return actualBusinessAddress
     */
	public String getActualBusinessAddress() {
		return this.actualBusinessAddress;
	}

	public String getOtherInfAboutShr() {
		return otherInfAboutShr;
	}

	public void setOtherInfAboutShr(String otherInfAboutShr) {
		this.otherInfAboutShr = otherInfAboutShr;
	}

	/**
	 * @param otherMattersToBeExplained
	 */
	public void setOtherMattersToBeExplained(String otherMattersToBeExplained) {
		this.otherMattersToBeExplained = otherMattersToBeExplained;
	}
	
    /**
     * @return otherMattersToBeExplained
     */
	public String getOtherMattersToBeExplained() {
		return this.otherMattersToBeExplained;
	}


}