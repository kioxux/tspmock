package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanSuitOrgInfo
 * @类描述: coop_plan_suit_org_info数据实体类
 * @功能描述: 
 * @创建人: pc
 * @创建时间: 2021-04-15 22:00:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanSuitOrgInfoQueryVo extends PageQuery{
	
	/** 合作方案申请流水号 **/
	private String coopPlanSerno;
	/** 主键流水号 **/
	private String serno;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 适用机构编号 **/
	private String suitOrgNo;
	
	/** 适用机构名称 **/
	private String suitOrgName;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param coopPlanSerno
	 */
	public void setCoopPlanSerno(String coopPlanSerno) {
		this.coopPlanSerno = coopPlanSerno == null ? null : coopPlanSerno.trim();
	}
	
    /**
     * @return CoopPlanSerno
     */	
	public String getCoopPlanSerno() {
		return this.coopPlanSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param suitOrgNo
	 */
	public void setSuitOrgNo(String suitOrgNo) {
		this.suitOrgNo = suitOrgNo == null ? null : suitOrgNo.trim();
	}
	
    /**
     * @return SuitOrgNo
     */	
	public String getSuitOrgNo() {
		return this.suitOrgNo;
	}
	
	/**
	 * @param suitOrgName
	 */
	public void setSuitOrgName(String suitOrgName) {
		this.suitOrgName = suitOrgName == null ? null : suitOrgName.trim();
	}
	
    /**
     * @return SuitOrgName
     */	
	public String getSuitOrgName() {
		return this.suitOrgName;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}