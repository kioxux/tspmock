/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.AsplBailAcctDto;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpAppAspl;
import cn.com.yusys.yusp.service.IqpAppAsplService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAppAsplResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 13:52:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "资产池协议申请")
@RequestMapping("/api/iqpappaspl")
public class IqpAppAsplResource {
    @Autowired
    private IqpAppAsplService iqpAppAsplService;

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpAppAspl> create(@RequestBody IqpAppAspl iqpAppAspl) throws URISyntaxException {
        iqpAppAsplService.insert(iqpAppAspl);
        return new ResultDto<IqpAppAspl>(iqpAppAspl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpAppAspl iqpAppAspl) throws URISyntaxException {
        int result = iqpAppAsplService.update(iqpAppAspl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpAppAsplService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpAppAsplService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 资产池协议新增保存操作
     * @param iqpAppAspl
     * @return
     */
    @ApiOperation("资产池协议申请新增保存")
    @PostMapping("/saveiqpappasplinfo")
    public ResultDto<Map> saveIqpAppAsplInfo(@RequestBody IqpAppAspl iqpAppAspl){
        Map result = iqpAppAsplService.saveIqpAppAsplInfo(iqpAppAspl);
        return new ResultDto<>(result);
    }

    /**
     * 资产池协议申请通用的保存方法
     * @param params
     * @return
     */
    @ApiOperation("资产池协议申请通用的保存方法")
    @PostMapping("/commonsaveiqpappasplinfo")
    public ResultDto<Map> commonSaveIqpAppAsplInfo(@RequestBody Map params){
        Map rtnData = iqpAppAsplService.commonSaveIqpAppAsplInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * 资产池协议申请逻辑删除方法
     * @param iqpAppAspl
     * @return
     */
    @ApiOperation("资产池协议申请逻辑删除")
    @PostMapping("/logicdelete")
    public ResultDto<Integer> logicDelete(@RequestBody IqpAppAspl iqpAppAspl){
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Integer result = iqpAppAsplService.logicDelete(iqpAppAspl);
        if (result > 0) {
            return new ResultDto<Integer>(result).code(0).message("删除成功！");
        } else {
            return new ResultDto<Integer>(result).code(9999).message("删除失败！");
        }
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:资产池协议申请待发起列表
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("资产池协议申请待发起列表")
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpAppAspl>> toSignlist(@RequestBody QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpAppAspl> list = iqpAppAsplService.toSignlist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<IqpAppAspl>>(list);
    }

    /**
     * @方法名称: queryLmtReplyAccDataByParams
     * @方法描述: 根据入参查询资产池协议申请数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据入参查询数据")
    @PostMapping("/queryiqpappaspldatabyparams")
    protected ResultDto<IqpAppAspl> queryIqpAppAsplDataByParams(@RequestBody Map map) {
        HashMap<String,String> queryData = new HashMap<String,String>();
        queryData.put("serno",(String)map.get("serno"));
        queryData.put("oprType", CommonConstance.OPR_TYPE_ADD);
        IqpAppAspl iqpAppAspl = iqpAppAsplService.queryIqpAppAsplDataByParams(queryData);
        return new ResultDto<IqpAppAspl>(iqpAppAspl);
    }

    /**
     * @方法名称: generateCtrAsplDetails
     * @方法描述: 根据资产池协议申请数据生成合同
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据资产池协议申请数据生成合同")
    @PostMapping("/generatectraspldetails")
    public ResultDto<Map> generateCtrAsplDetails(@RequestBody Map map) {
        Map rtnData = iqpAppAsplService.generateCtrAsplDetails(map);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称: checkOutAsplAccNo
     * @方法描述: 校验资产池协议申请是否关联保证金账号 或结算账号
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("校验资产池协议申请是否关联保证金账号")
    @PostMapping("/checkOutAsplAccNo")
    public ResultDto<Map> checkOutAsplAccNo(@RequestBody Map map) {
        Map rtnData = iqpAppAsplService.checkOutAsplAccNo(map);
        return new ResultDto<>(rtnData);
    }

    /**
     * @方法名称: generateCtrAsplDetails
     * @方法描述: 根据资产池协议变更修改状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    @ApiOperation("根据资产池协议申请变更修改状态")
    @PostMapping("/changesplapp")
    public ResultDto<Map> changeasplApp(@RequestBody Map map) {
        Map rtnData = iqpAppAsplService.changeasplApp(map);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称: queryIqpAppAsplDataBySerno
     * @函数描述: 根据申请流水号获取申请信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号获取申请信息")
    @PostMapping("/queryiqpappaspldatabyserno")
    protected ResultDto<List<IqpAppAspl>> queryIqpAppAsplDataBySerno(@RequestBody String serno) {
        List<IqpAppAspl> list = iqpAppAsplService.queryIqpAppAsplDataBySerno(serno);
        return new ResultDto<List<IqpAppAspl>>(list);
    }

    /**
     * @函数名称: queryAsplBailAcctDtoDataByParams
     * @函数描述: 根据入参获取账户信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据入参获取账户信息")
    @PostMapping("/queryasplbailacctdtodatabyserno")
    protected ResultDto<List<AsplBailAcctDto>> queryAsplBailAcctDtoDataByParams(@RequestBody Map map) {
        List<AsplBailAcctDto> list = iqpAppAsplService.queryAsplBailAcctDtoDataByParams(map);
        return new ResultDto<List<AsplBailAcctDto>>(list);
    }

    /**
     * @函数名称: queryGrtGuarContByParams
     * @函数描述: 根据申请流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据申请流水号获取担保合同信息")
    @PostMapping("/querygrtguarcontbyparams")
    protected ResultDto<List<GrtGuarCont>> queryGrtGuarContByParams(@RequestBody Map map) {
        List<GrtGuarCont> list = iqpAppAsplService.queryGrtGuarContByParams((String)map.get("serno"));
        return new ResultDto<List<GrtGuarCont>>(list);
    }

    /**
     * @函数名称: getGuarBaseInfoByParams
     * @函数描述: 根据申请流水号获取质押物信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据担保合同编号获取质押物信息")
    @PostMapping("/getguarbaseinfobyparams")
    protected ResultDto<List<GuarBaseInfo>> getGuarBaseInfoByParams(@RequestBody Map map) {
        List<GuarBaseInfo> list = iqpAppAsplService.getGuarBaseInfoByParams(map);
        return new ResultDto<List<GuarBaseInfo>>(list);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateselective")
    protected ResultDto<Integer> updateSelective(@RequestBody IqpAppAspl iqpAppAspl) throws URISyntaxException {
        int result = iqpAppAsplService.updateSelective(iqpAppAspl);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:chCtrAsplDetail
     * @函数描述:资产池协议变更
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @ApiOperation("资产池协议变更")
    @PostMapping("/chIqpappaspl")
    protected ResultDto<IqpAppAspl> chCtrAsplDetail(@RequestBody QueryModel queryModel) {
        ResultDto<IqpAppAspl> result = new ResultDto<IqpAppAspl>().code(9999).message("资产池协议变更新增失败");
        // 判断是否资产池变更 1变更
        String chgFlag = (String)queryModel.getCondition().get("chgFlag");
        String serno = (String)queryModel.getCondition().get("serno");
        if(CommonConstance.STD_ZB_YES_NO_1.equals(chgFlag) && StringUtils.nonEmpty(serno)){
            return iqpAppAsplService.updateChgFlag(serno);
        }else{
            return result;
        }
    }
}
