package cn.com.yusys.yusp.web.server.xdxw0041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0041.req.Xdxw0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0041.resp.Xdxw0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0041.Xdxw0041Service;
import cn.com.yusys.yusp.service.server.xdxw0047.Xdxw0047Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:小企业无还本续贷审批结果维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0041:小企业无还本续贷审批结果维护")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0041Resource.class);

    @Resource
    private Xdxw0041Service xdxw0041Service;

    /**
     * 交易码：xdxw0041
     * 交易描述：小企业无还本续贷审批结果维护
     *
     * @param xdxw0041DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小企业无还本续贷审批结果维护")
    @PostMapping("/xdxw0041")
    protected @ResponseBody
    ResultDto<Xdxw0041DataRespDto> xdxw0041(@Validated @RequestBody Xdxw0041DataReqDto xdxw0041DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataReqDto));
        Xdxw0041DataRespDto xdxw0041DataRespDto = new Xdxw0041DataRespDto();// 响应Dto:小企业无还本续贷审批结果维护
        ResultDto<Xdxw0041DataRespDto> xdxw0041DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0041DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataReqDto));
            xdxw0041DataRespDto = xdxw0041Service.xdxw0041(xdxw0041DataReqDto);
            logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataRespDto));
            // 封装xdxw0041DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0041DataRespDto.getOpFlag();
            String opMessage = xdxw0041DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0041DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0041DataResultDto.setMessage(opMessage);
            } else {
                xdxw0041DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0041DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0041DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0041DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0041DataResultDto.setCode(e.getErrorCode());
            xdxw0041DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, e.getMessage());
            xdxw0041DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0041DataResultDto.setMessage(e.getMessage());
            // 封装xdxw0041DataResultDto中异常返回码和返回信息
            xdxw0041DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0041DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0041DataRespDto到xdxw0041DataResultDto中
        xdxw0041DataResultDto.setData(xdxw0041DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataResultDto));
        return xdxw0041DataResultDto;
    }
}
