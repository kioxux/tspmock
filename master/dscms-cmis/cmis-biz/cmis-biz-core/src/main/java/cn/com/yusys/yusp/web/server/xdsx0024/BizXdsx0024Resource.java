package cn.com.yusys.yusp.web.server.xdsx0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0024.req.Xdsx0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.resp.Xdsx0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdsx0023.Xdsx0023Service;
import cn.com.yusys.yusp.service.server.xdsx0024.Xdsx0024Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:省心快贷plus授信，风控自动审批结果推送信贷
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0024:省心快贷plus授信，风控自动审批结果推送信贷")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0024Resource.class);
    @Autowired
    private Xdsx0024Service xdsx0024Service;
    /**
     * 交易码：xdsx0024
     * 交易描述：省心快贷plus授信，风控自动审批结果推送信贷
     *
     * @param xdsx0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("省心快贷plus授信，风控自动审批结果推送信贷")
    @PostMapping("/xdsx0024")
    protected @ResponseBody
    ResultDto<Xdsx0024DataRespDto> xdsx0024(@Validated @RequestBody Xdsx0024DataReqDto xdsx0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024DataReqDto));
        Xdsx0024DataRespDto xdsx0024DataRespDto = new Xdsx0024DataRespDto();// 响应Dto:省心快贷plus授信，风控自动审批结果推送信贷
        ResultDto<Xdsx0024DataRespDto> xdsx0024DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0024DataReqDto获取业务值进行业务逻辑处理
            // 调用xdsx0024Service层开始
            xdsx0024DataRespDto = xdsx0024Service.xdsx0024(xdsx0024DataReqDto);
            // 调用xdsx0024Service层结束
            // 封装xdsx0024DataResultDto中正确的返回码和返回信息
            xdsx0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, e.getMessage());
            // 封装xdxw0019DataResultDto中异常返回码和返回信息
            xdsx0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0024DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, e.getMessage());
            // 封装xdsx0024DataResultDto中异常返回码和返回信息
            xdsx0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0024DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdsx0024DataRespDto到xdsx0024DataResultDto中
        xdsx0024DataResultDto.setData(xdsx0024DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024DataRespDto));
        return xdsx0024DataResultDto;
    }
}
