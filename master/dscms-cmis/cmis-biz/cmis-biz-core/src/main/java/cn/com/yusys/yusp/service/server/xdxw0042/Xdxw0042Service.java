package cn.com.yusys.yusp.service.server.xdxw0042;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;

import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtModelApprResultInfo;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.server.xdxw0042.req.Xdxw0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0042.resp.Xdxw0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtModelApprResultInfoMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类: 惠享贷模型结果推送给信贷
 */
@Service
public class Xdxw0042Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0042Service.class);

    @Autowired
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper;//模型审批结果表
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;//客户授信调查主表
    @Autowired
    private AdminSmUserService adminSmUserService;//用户信息模块
    @Autowired
    MessageCommonService messageCommonService;
    @Autowired
    private ICusClientService iCusClientService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0042DataRespDto xdxw0042(Xdxw0042DataReqDto xdxw0042DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value);
        Xdxw0042DataRespDto xdxw0042DataRespDto = new Xdxw0042DataRespDto();
        try {
            String surveySerno = xdxw0042DataReqDto.getSurvey_serno();//业务流水
            String modelApprTime = xdxw0042DataReqDto.getModel_appr_time();//模型审批时间
            String modelGrade = xdxw0042DataReqDto.getModel_grade();//模型评级
            String modelScore = xdxw0042DataReqDto.getModel_score();//模型得分
            BigDecimal modelAmt = xdxw0042DataReqDto.getModel_amt();//模型金额
            BigDecimal modelRate = xdxw0042DataReqDto.getModel_rate();//模型利率
            String judiCaseJudge = xdxw0042DataReqDto.getJudi_case_judge();//司法案件判断
            String resultStatus = xdxw0042DataReqDto.getResult_status();//模型结果状态
            String applyNo = xdxw0042DataReqDto.getApply_no();//业务唯一编号
            String opinion = xdxw0042DataReqDto.getOpinion();//模型意见
            BigDecimal initModelAmt = xdxw0042DataReqDto.getInit_model_amt();//初始额度

            if ("".equals(surveySerno)) {
                throw new Exception("业务流水编号为空！");
            }
            LmtModelApprResultInfo model = lmtModelApprResultInfoMapper.selectByPrimaryKey(surveySerno);
            if (model == null) {
                // 生成LMT_MODEL_APPR_RESULT_INFO模型结果表
                LmtModelApprResultInfo lmtModelApprResultInfo = new LmtModelApprResultInfo();
                lmtModelApprResultInfo.setSurveySerno(surveySerno);//调查流水号
                lmtModelApprResultInfo.setModelApprTime(modelApprTime);//模型审批时间
                lmtModelApprResultInfo.setModelGrade(modelGrade);//模型评级
                lmtModelApprResultInfo.setModelScore(modelScore);//模型得分
                lmtModelApprResultInfo.setModelAmt(modelAmt);//模型金额
                lmtModelApprResultInfo.setModelRate(modelRate);//模型利率
                lmtModelApprResultInfo.setJojc(judiCaseJudge);//司法案件判断
                lmtModelApprResultInfo.setModelRstStatus(resultStatus);//模型结果状态
                lmtModelApprResultInfo.setBizUniqueNo(applyNo);//业务唯一编号
                lmtModelApprResultInfo.setModelAdvice(opinion);//模型意见
                lmtModelApprResultInfo.setModelFstLmt(initModelAmt);//模型初始额度

                String sysDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                Date date = DateUtils.parseDate(sysDate, DateFormatEnum.DATETIME.getValue());
                lmtModelApprResultInfo.setCreateTime(date);//创建时间
                lmtModelApprResultInfo.setUpdateTime(date);//修改时间
                //LMT_MODEL_APPR_RESULT_INFO模型结果表插入数据库
                logger.info("**********XDXW0042**插入lmtModelApprResultInfo开始,插入参数为:{}", JSON.toJSONString(lmtModelApprResultInfo));
                lmtModelApprResultInfoMapper.insert(lmtModelApprResultInfo);
            } else {
                model.setSurveySerno(surveySerno);//调查流水号
                model.setModelApprTime(modelApprTime);//模型审批时间
                model.setModelGrade(modelGrade);//模型评级
                model.setModelScore(modelScore);//模型得分
                model.setModelAmt(modelAmt);//模型金额
                model.setModelRate(modelRate);//模型利率
                model.setJojc(judiCaseJudge);//司法案件判断
                model.setModelRstStatus(resultStatus);//模型结果状态
                model.setBizUniqueNo(applyNo);//业务唯一编号
                model.setModelAdvice(opinion);//模型意见
                model.setModelFstLmt(initModelAmt);//模型初始额度

                String sysDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                Date date = DateUtils.parseDate(sysDate, DateFormatEnum.DATETIME.getValue());
                model.setCreateTime(date);//创建时间
                model.setUpdateTime(date);//修改时间
                //LMT_MODEL_APPR_RESULT_INFO模型结果表插入数据库
                logger.info("**********XDXW0042**更新lmtModelApprResultInfo开始,更新参数为:{}", JSON.toJSONString(model));
                lmtModelApprResultInfoMapper.updateByPrimaryKey(model);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0042DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);// 操作成功标志位
            xdxw0042DataRespDto.setOpMsg(DscmsBizXwEnum.FAIL.value);// 描述信息
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value);
        xdxw0042DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);// 操作成功标志位
        xdxw0042DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);// 描述信息
        return xdxw0042DataRespDto;
    }
}
