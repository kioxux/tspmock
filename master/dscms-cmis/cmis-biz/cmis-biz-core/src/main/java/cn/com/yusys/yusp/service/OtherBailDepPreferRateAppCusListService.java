/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.OtherGrtDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.OtherBailDepPreferRateAppCusListMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherBailDepPreferRateAppCusListService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-11 09:52:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherBailDepPreferRateAppCusListService {
    private static final Logger log = LoggerFactory.getLogger(OtherBailDepPreferRateAppCusListService.class);

    @Autowired
    private OtherBailDepPreferRateAppCusListMapper otherBailDepPreferRateAppCusListMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherBailDepPreferRateAppCusList selectByPrimaryKey(String serno, String cusId) {
        return otherBailDepPreferRateAppCusListMapper.selectByPrimaryKey(serno, cusId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherBailDepPreferRateAppCusList> selectAll(QueryModel model) {
        List<OtherBailDepPreferRateAppCusList> records = (List<OtherBailDepPreferRateAppCusList>) otherBailDepPreferRateAppCusListMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherBailDepPreferRateAppCusList> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherBailDepPreferRateAppCusList> list = otherBailDepPreferRateAppCusListMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherBailDepPreferRateAppCusList record) {
        return otherBailDepPreferRateAppCusListMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherBailDepPreferRateAppCusList record) {
        return otherBailDepPreferRateAppCusListMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherBailDepPreferRateAppCusList record) {
        return otherBailDepPreferRateAppCusListMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherBailDepPreferRateAppCusList record) {
        return otherBailDepPreferRateAppCusListMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno, String cusId) {
        return otherBailDepPreferRateAppCusListMapper.deleteByPrimaryKey(serno, cusId);
    }

    /**
     * @方法名称: addotherbaildeppreferrateappcuslist
     * @方法描述: 保证金存款特惠利率登记
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto addotherbaildeppreferrateappcuslist(OtherBailDepPreferRateAppCusList otherbaildeppreferrateappcuslist) {
        {
            OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList = new OtherBailDepPreferRateAppCusList();
            try {
                //克隆对象
                BeanUtils.copyProperties(otherbaildeppreferrateappcuslist, otherBailDepPreferRateAppCusList);
                /** 对公活期存款日均 **/
                BigDecimal corpAverdayDep  = otherBailDepPreferRateAppCusList.getCorpAverdayDep().multiply(new BigDecimal(10000));
                otherBailDepPreferRateAppCusList.setCorpAverdayDep(corpAverdayDep);
                /** 关联个人存款日均 **/
                BigDecimal relIndivAverdayDep  = otherBailDepPreferRateAppCusList.getRelIndivAverdayDep().multiply(new BigDecimal(10000));
                otherBailDepPreferRateAppCusList.setRelIndivAverdayDep(relIndivAverdayDep);
                /** 对公贷款日均 **/
                BigDecimal corpAverdayLoan  = otherBailDepPreferRateAppCusList.getCorpAverdayLoan().multiply(new BigDecimal(10000));
                otherBailDepPreferRateAppCusList.setCorpAverdayLoan(corpAverdayLoan);
                //补充 克隆缺少数据
                //放入必填参数 操作类型 新增
                otherBailDepPreferRateAppCusList.setOprType(CmisBizConstants.OPR_TYPE_01);
                //待生效
                otherBailDepPreferRateAppCusList.setCusListStatus(CmisBizConstants.STD_CUS_LIST_STATUS_00);
                //时间 登记人 等级机构相关
                //转换日期 ASSURE_CERT_CODE
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                User user = SessionUtils.getUserInformation();
                otherBailDepPreferRateAppCusList.setInputId(user.getLoginCode());
                otherBailDepPreferRateAppCusList.setInputBrId(user.getOrg().getCode());
                otherBailDepPreferRateAppCusList.setInputDate(dateFormat.format(new Date()) );
                otherBailDepPreferRateAppCusList.setCreateTime(DateUtils.getCurrDate());
                otherBailDepPreferRateAppCusList.setUpdateTime(DateUtils.getCurrDate());
                otherBailDepPreferRateAppCusList.setUpdDate(dateFormat.format(new Date()) );
                otherBailDepPreferRateAppCusList.setUpdId(user.getLoginCode());
                otherBailDepPreferRateAppCusList.setUpdBrId(user.getOrg().getCode());
                int i = otherBailDepPreferRateAppCusListMapper.insertSelective(otherBailDepPreferRateAppCusList);
                if (i != 1) {
                    throw BizException.error(null, "999999", "新增：授信抵质押物价值认定与抵质押物关联表异常");
                }
            }catch (Exception e) {
                log.error("\n" +
                        "保证金存款特惠利率登记新增异常：",e);
                throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
            }

            return new ResultDto(otherBailDepPreferRateAppCusList);

        }
    }

    /**
     * @方法名称: updateotherbaildeppreferrateappcuslist
     * @方法描述: 保证金存款特惠利率登记
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto updateotherbaildeppreferrateappcuslist(OtherBailDepPreferRateAppCusList otherbaildeppreferrateappcuslist) {
        OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList = new OtherBailDepPreferRateAppCusList();
        try {
            //克隆对象
            BeanUtils.copyProperties(otherbaildeppreferrateappcuslist, otherBailDepPreferRateAppCusList);
            //补充 克隆缺少数据
            OtherBailDepPreferRateAppCusList oldOtherBailDepPreferRateAppCusList = otherBailDepPreferRateAppCusListMapper.selectByPrimaryKey(otherBailDepPreferRateAppCusList.getSerno(),otherBailDepPreferRateAppCusList.getCusId());
            //放入必填参数 操作类型 新增
            otherBailDepPreferRateAppCusList.setOprType(oldOtherBailDepPreferRateAppCusList.getOprType());
            otherBailDepPreferRateAppCusList.setCusListStatus(oldOtherBailDepPreferRateAppCusList.getCusListStatus());
            /** 对公活期存款日均 **/
            BigDecimal corpAverdayDep  = otherBailDepPreferRateAppCusList.getCorpAverdayDep().multiply(new BigDecimal(10000));
            otherBailDepPreferRateAppCusList.setCorpAverdayDep(corpAverdayDep);
            /** 关联个人存款日均 **/
            BigDecimal relIndivAverdayDep  = otherBailDepPreferRateAppCusList.getRelIndivAverdayDep().multiply(new BigDecimal(10000));
            otherBailDepPreferRateAppCusList.setRelIndivAverdayDep(relIndivAverdayDep);
            /** 对公贷款日均 **/
            BigDecimal corpAverdayLoan  = otherBailDepPreferRateAppCusList.getCorpAverdayLoan().multiply(new BigDecimal(10000));
            otherBailDepPreferRateAppCusList.setCorpAverdayLoan(corpAverdayLoan);
            //时间 登记人 等级机构相关
            //转换日期 ASSURE_CERT_CODE
            SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
            User user = SessionUtils.getUserInformation();
            otherBailDepPreferRateAppCusList.setUpdateTime(DateUtils.getCurrDate());
            otherBailDepPreferRateAppCusList.setUpdDate(dateFormat.format(new Date()) );
            otherBailDepPreferRateAppCusList.setUpdId(user.getLoginCode());
            otherBailDepPreferRateAppCusList.setUpdBrId(user.getOrg().getCode());
            int i = otherBailDepPreferRateAppCusListMapper.updateByPrimaryKeySelective(otherBailDepPreferRateAppCusList);
            if (i != 1) {
                throw BizException.error(null, "999999", "修改：抵押物申请异常");
            }
        }catch (Exception e) {
            log.error("\n" +
                    "保证金存款特惠利率登记修改异常：",e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }

        return new ResultDto(otherBailDepPreferRateAppCusList);

    }

    /**
     * @方法名称: deleteInfo
     * @方法描述: 根据主键将操作类型置为删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteInfo(String cusInfo) {
        OtherBailDepPreferRateAppCusList otherBailDepPreferRateAppCusList = otherBailDepPreferRateAppCusListMapper.selectByPrimaryKey(cusInfo.split(",")[0],cusInfo.split(",")[1]);
        //操作类型  删除
        otherBailDepPreferRateAppCusList.setOprType(CmisBizConstants.OPR_TYPE_02);
        //转换日期 ASSURE_CERT_CODE
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        User user = SessionUtils.getUserInformation();
        otherBailDepPreferRateAppCusList.setUpdateTime(DateUtils.getCurrDate());
        otherBailDepPreferRateAppCusList.setUpdDate(dateFormat.format(new Date()) );
        otherBailDepPreferRateAppCusList.setUpdId(user.getLoginCode());
        otherBailDepPreferRateAppCusList.setUpdBrId(user.getOrg().getCode());

        int i = otherBailDepPreferRateAppCusListMapper.updateByPrimaryKeySelective(otherBailDepPreferRateAppCusList);
        if (i != 1) {
            throw BizException.error(null, "999999", "删除：\n" +
                    "保证金存款特惠利率登记异常");
        }
        return i;
    }

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public  List<OtherBailDepPreferRateAppCusList> selectByCusId(Map params) {
        String  cusId = (String) params.get("cusId");
        if(null!=cusId || !"".equals(cusId)) {
            return otherBailDepPreferRateAppCusListMapper.selectByCusId(params);
        }else {
            throw new RuntimeException("Oops: cusId can't be null!" );
        }
    }
}
