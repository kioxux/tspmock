/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ZxdPreLmtApply
 * @类描述: zxd_pre_lmt_apply数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 20:10:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "zxd_pre_lmt_apply")
public class ZxdPreLmtApply extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 对接记录ID **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "APPLY_RECORD_ID")
	private String applyRecordId;
	
	/** 企业代码 **/
	@Column(name = "CORP_ID", unique = false, nullable = true, length = 40)
	private String corpId;
	
	/** 企业名称 **/
	@Column(name = "CORP_NAME", unique = false, nullable = true, length = 60)
	private String corpName;
	
	/** 借款人预期借款额度 **/
	@Column(name = "BORROWER_LOSS_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal borrowerLossLmt;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 纳税评级 **/
	@Column(name = "TAX_GRADE", unique = false, nullable = true, length = 8)
	private String taxGrade;
	
	/** 征信报告信用评分 **/
	@Column(name = "REPORT_CREDIT_GRADE", unique = false, nullable = true, length = 8)
	private String reportCreditGrade;
	
	/** 信用贷款用信余额 **/
	@Column(name = "CREDIT_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal creditBalance;
	
	/** 是否黑灰名单客户 **/
	@Column(name = "IF_BLACK", unique = false, nullable = true, length = 2)
	private String ifBlack;
	
	/** 苏州地方征信是否显示纳税信息 **/
	@Column(name = "IF_SHOW_TAX", unique = false, nullable = true, length = 2)
	private String ifShowTax;
	
	/** 上年度纳税金额 **/
	@Column(name = "LT_YEAR_TAX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ltYearTaxAmt;
	
	/** 是否租用 **/
	@Column(name = "IF_RENT", unique = false, nullable = true, length = 2)
	private String ifRent;
	
	/** 是否自有 **/
	@Column(name = "IF_SELF", unique = false, nullable = true, length = 2)
	private String ifSelf;
	
	/** 是否具备自有产权证 **/
	@Column(name = "IF_HAVE_CERT", unique = false, nullable = true, length = 2)
	private String ifHaveCert;
	
	/** 企业苏州地方征信信用情况（系统评分） **/
	@Column(name = "CORP_CREDIT_INFO", unique = false, nullable = true, length = 10)
	private Integer corpCreditInfo;
	
	/** 资产负债率 **/
	@Column(name = "ASSET_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetRate;
	
	/** 企业存量贷款与销售收入比率 **/
	@Column(name = "CORP_STOCK_RATE_SYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal corpStockRateSys;
	
	/** 专利信息情况（系统评分） **/
	@Column(name = "PATENT_INFO_SYS", unique = false, nullable = true, length = 10)
	private Integer patentInfoSys;
	
	/** 利润增长情况（系统评分） **/
	@Column(name = "PROFIT_INCREASE_SYS", unique = false, nullable = true, length = 10)
	private Integer profitIncreaseSys;
	
	/** 实际控制人从事本行业年限（系统评分） **/
	@Column(name = "INDUSTRY_YEAR_SYS", unique = false, nullable = true, length = 10)
	private Integer industryYearSys;
	
	/** 企业所属行业 **/
	@Column(name = "CORP_TRADE", unique = false, nullable = true, length = 8)
	private String corpTrade;
	
	/** 公积金、社保缴纳情况（系统评分） **/
	@Column(name = "SOCIAL_PAYMENT_SYS", unique = false, nullable = true, length = 10)
	private Integer socialPaymentSys;
	
	/** 水电气费缴纳情况（系统评分） **/
	@Column(name = "WATER_ELEC_SYS", unique = false, nullable = true, length = 10)
	private Integer waterElecSys;
	
	/** 法人或实际控制人落户情况（系统评分） **/
	@Column(name = "SETTLE_SYS", unique = false, nullable = true, length = 10)
	private Integer settleSys;
	
	/** 总分值（系统评分） **/
	@Column(name = "SCORE_TOTAL_SYS", unique = false, nullable = true, length = 10)
	private Integer scoreTotalSys;
	
	/** 是否准入（自动测算，无需手填） **/
	@Column(name = "IF_ADMIT_SYS", unique = false, nullable = true, length = 2)
	private String ifAdmitSys;
	
	/** 最大预授信额度 **/
	@Column(name = "MAX_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal maxLmtAmt;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 发起机构 **/
	@Column(name = "LAUNCH_ORG", unique = false, nullable = true, length = 5)
	private String launchOrg;
	
	
	/**
	 * @param applyRecordId
	 */
	public void setApplyRecordId(String applyRecordId) {
		this.applyRecordId = applyRecordId;
	}
	
    /**
     * @return applyRecordId
     */
	public String getApplyRecordId() {
		return this.applyRecordId;
	}
	
	/**
	 * @param corpId
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	
    /**
     * @return corpId
     */
	public String getCorpId() {
		return this.corpId;
	}
	
	/**
	 * @param corpName
	 */
	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}
	
    /**
     * @return corpName
     */
	public String getCorpName() {
		return this.corpName;
	}
	
	/**
	 * @param borrowerLossLmt
	 */
	public void setBorrowerLossLmt(java.math.BigDecimal borrowerLossLmt) {
		this.borrowerLossLmt = borrowerLossLmt;
	}
	
    /**
     * @return borrowerLossLmt
     */
	public java.math.BigDecimal getBorrowerLossLmt() {
		return this.borrowerLossLmt;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param taxGrade
	 */
	public void setTaxGrade(String taxGrade) {
		this.taxGrade = taxGrade;
	}
	
    /**
     * @return taxGrade
     */
	public String getTaxGrade() {
		return this.taxGrade;
	}
	
	/**
	 * @param reportCreditGrade
	 */
	public void setReportCreditGrade(String reportCreditGrade) {
		this.reportCreditGrade = reportCreditGrade;
	}
	
    /**
     * @return reportCreditGrade
     */
	public String getReportCreditGrade() {
		return this.reportCreditGrade;
	}
	
	/**
	 * @param creditBalance
	 */
	public void setCreditBalance(java.math.BigDecimal creditBalance) {
		this.creditBalance = creditBalance;
	}
	
    /**
     * @return creditBalance
     */
	public java.math.BigDecimal getCreditBalance() {
		return this.creditBalance;
	}
	
	/**
	 * @param ifBlack
	 */
	public void setIfBlack(String ifBlack) {
		this.ifBlack = ifBlack;
	}
	
    /**
     * @return ifBlack
     */
	public String getIfBlack() {
		return this.ifBlack;
	}
	
	/**
	 * @param ifShowTax
	 */
	public void setIfShowTax(String ifShowTax) {
		this.ifShowTax = ifShowTax;
	}
	
    /**
     * @return ifShowTax
     */
	public String getIfShowTax() {
		return this.ifShowTax;
	}
	
	/**
	 * @param ltYearTaxAmt
	 */
	public void setLtYearTaxAmt(java.math.BigDecimal ltYearTaxAmt) {
		this.ltYearTaxAmt = ltYearTaxAmt;
	}
	
    /**
     * @return ltYearTaxAmt
     */
	public java.math.BigDecimal getLtYearTaxAmt() {
		return this.ltYearTaxAmt;
	}
	
	/**
	 * @param ifRent
	 */
	public void setIfRent(String ifRent) {
		this.ifRent = ifRent;
	}
	
    /**
     * @return ifRent
     */
	public String getIfRent() {
		return this.ifRent;
	}
	
	/**
	 * @param ifSelf
	 */
	public void setIfSelf(String ifSelf) {
		this.ifSelf = ifSelf;
	}
	
    /**
     * @return ifSelf
     */
	public String getIfSelf() {
		return this.ifSelf;
	}
	
	/**
	 * @param ifHaveCert
	 */
	public void setIfHaveCert(String ifHaveCert) {
		this.ifHaveCert = ifHaveCert;
	}
	
    /**
     * @return ifHaveCert
     */
	public String getIfHaveCert() {
		return this.ifHaveCert;
	}
	
	/**
	 * @param corpCreditInfo
	 */
	public void setCorpCreditInfo(Integer corpCreditInfo) {
		this.corpCreditInfo = corpCreditInfo;
	}
	
    /**
     * @return corpCreditInfo
     */
	public Integer getCorpCreditInfo() {
		return this.corpCreditInfo;
	}
	
	/**
	 * @param assetRate
	 */
	public void setAssetRate(java.math.BigDecimal assetRate) {
		this.assetRate = assetRate;
	}
	
    /**
     * @return assetRate
     */
	public java.math.BigDecimal getAssetRate() {
		return this.assetRate;
	}
	
	/**
	 * @param corpStockRateSys
	 */
	public void setCorpStockRateSys(java.math.BigDecimal corpStockRateSys) {
		this.corpStockRateSys = corpStockRateSys;
	}
	
    /**
     * @return corpStockRateSys
     */
	public java.math.BigDecimal getCorpStockRateSys() {
		return this.corpStockRateSys;
	}
	
	/**
	 * @param patentInfoSys
	 */
	public void setPatentInfoSys(Integer patentInfoSys) {
		this.patentInfoSys = patentInfoSys;
	}
	
    /**
     * @return patentInfoSys
     */
	public Integer getPatentInfoSys() {
		return this.patentInfoSys;
	}
	
	/**
	 * @param profitIncreaseSys
	 */
	public void setProfitIncreaseSys(Integer profitIncreaseSys) {
		this.profitIncreaseSys = profitIncreaseSys;
	}
	
    /**
     * @return profitIncreaseSys
     */
	public Integer getProfitIncreaseSys() {
		return this.profitIncreaseSys;
	}
	
	/**
	 * @param industryYearSys
	 */
	public void setIndustryYearSys(Integer industryYearSys) {
		this.industryYearSys = industryYearSys;
	}
	
    /**
     * @return industryYearSys
     */
	public Integer getIndustryYearSys() {
		return this.industryYearSys;
	}
	
	/**
	 * @param corpTrade
	 */
	public void setCorpTrade(String corpTrade) {
		this.corpTrade = corpTrade;
	}
	
    /**
     * @return corpTrade
     */
	public String getCorpTrade() {
		return this.corpTrade;
	}
	
	/**
	 * @param socialPaymentSys
	 */
	public void setSocialPaymentSys(Integer socialPaymentSys) {
		this.socialPaymentSys = socialPaymentSys;
	}
	
    /**
     * @return socialPaymentSys
     */
	public Integer getSocialPaymentSys() {
		return this.socialPaymentSys;
	}
	
	/**
	 * @param waterElecSys
	 */
	public void setWaterElecSys(Integer waterElecSys) {
		this.waterElecSys = waterElecSys;
	}
	
    /**
     * @return waterElecSys
     */
	public Integer getWaterElecSys() {
		return this.waterElecSys;
	}
	
	/**
	 * @param settleSys
	 */
	public void setSettleSys(Integer settleSys) {
		this.settleSys = settleSys;
	}
	
    /**
     * @return settleSys
     */
	public Integer getSettleSys() {
		return this.settleSys;
	}
	
	/**
	 * @param scoreTotalSys
	 */
	public void setScoreTotalSys(Integer scoreTotalSys) {
		this.scoreTotalSys = scoreTotalSys;
	}
	
    /**
     * @return scoreTotalSys
     */
	public Integer getScoreTotalSys() {
		return this.scoreTotalSys;
	}
	
	/**
	 * @param ifAdmitSys
	 */
	public void setIfAdmitSys(String ifAdmitSys) {
		this.ifAdmitSys = ifAdmitSys;
	}
	
    /**
     * @return ifAdmitSys
     */
	public String getIfAdmitSys() {
		return this.ifAdmitSys;
	}
	
	/**
	 * @param maxLmtAmt
	 */
	public void setMaxLmtAmt(java.math.BigDecimal maxLmtAmt) {
		this.maxLmtAmt = maxLmtAmt;
	}
	
    /**
     * @return maxLmtAmt
     */
	public java.math.BigDecimal getMaxLmtAmt() {
		return this.maxLmtAmt;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param launchOrg
	 */
	public void setLaunchOrg(String launchOrg) {
		this.launchOrg = launchOrg;
	}
	
    /**
     * @return launchOrg
     */
	public String getLaunchOrg() {
		return this.launchOrg;
	}


}