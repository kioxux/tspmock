package cn.com.yusys.yusp.service.server.xdtz0017;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList;
import cn.com.yusys.yusp.dto.server.xdtz0017.req.Xdtz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.resp.Xdtz0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccAccpDrftSubMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0040Service
 * @类描述: #服务类
 * @功能描述:票据系统进行更换票号后通知信贷系统，信贷根据旧票号查询acc_accp信息，存在则修改为新票号及收款人账户信息。
 * @创建人: xll
 * @创建时间: 2021-05-03 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0017Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0017Service.class);

    @Autowired
    private AccAccpDrftSubMapper AccAccpDrftSubMapper;

    /**
     * 交易码：Xdtz0017
     * 交易描述：通知信贷系统更新贴现台账状态
     *
     * @param xdtz0017DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0017DataRespDto xdtz0017(Xdtz0017DataReqDto xdtz0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataReqDto));
        //定义返回信息
        Xdtz0017DataRespDto xdht0017DataRespDto = new Xdtz0017DataRespDto();
        //得到请求字段
        String oldDrftNo = xdtz0017DataReqDto.getOldDrftNo();//旧票号
        String newDrftNo = xdtz0017DataReqDto.getNewDrftNo();//新票号
        String pyeeName = xdtz0017DataReqDto.getPyeeName();//收款人名称
        String pyeeAcctbNo = xdtz0017DataReqDto.getPyeeAcctbNo();//收款人开户行行号
        String pyeeAcctNo = xdtz0017DataReqDto.getPyeeAcctNo();//收款人账号

        //业务处理开始
        try {
            //设置查询参数
            Map queryMap = new HashMap();
            queryMap.put("oldDrftNo", oldDrftNo);//旧票号
            queryMap.put("newDrftNo", newDrftNo);//新票号
            queryMap.put("pyeeName", pyeeName);//收款人名称
            queryMap.put("pyeeAcctbNo", pyeeAcctbNo);//收款人开户行行号
            queryMap.put("pyeeAcctNo", pyeeAcctNo);//收款人账号

            //根据票号查询是否存在该笔
            int count = AccAccpDrftSubMapper.selectAccAccpSubCountNumByDrftNo(queryMap);
            if (count > 0) {//存在记录,执行更新操作
                logger.info("*********XDTZ0017*根据票号查询该笔票据台账状态开始,查询参数为:{}", JSON.toJSONString(queryMap));
                int result = 0;
                //先查询
                QueryModel model = new QueryModel();
                model.addCondition("billNo",oldDrftNo);
                List<AccAccpDrftSub> accAccpDrftSubList = AccAccpDrftSubMapper.querymodelByCondition(model);
                for (int i = 0; i < accAccpDrftSubList.size(); i++) {
                    AccAccpDrftSub accAccpDrftSub = accAccpDrftSubList.get(i);
                    //再删除
                    AccAccpDrftSubMapper.deleteByIds(accAccpDrftSub.getPkId());
                    //再新增
                    accAccpDrftSub.setPyeeAccno(pyeeAcctNo);
                    accAccpDrftSub.setPyeeAcctsvcrNo(pyeeAcctbNo);
                    accAccpDrftSub.setPyeeAcctsvcrName(pyeeName);
                    accAccpDrftSub.setBillNo(newDrftNo);
                    result = AccAccpDrftSubMapper.insert(accAccpDrftSub);
                }
                //int result = AccAccpDrftSubMapper.updateAccAccpSubCountNumByDrftNo(queryMap);
                logger.info("*********XDTZ0017*根据票号查询该笔票据台账状态结束,返回参数为:{}", JSON.toJSONString(result));
                if (result > 0) {
                    xdht0017DataRespDto.setFlag(DscmsBizTzEnum.SUCCSEE.key);
                } else {
                    xdht0017DataRespDto.setFlag(DscmsBizTzEnum.FAIL.key);
                }
            } else {
                xdht0017DataRespDto.setFlag(DscmsBizTzEnum.FAIL.key);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdht0017DataRespDto));
        return xdht0017DataRespDto;
    }
}
