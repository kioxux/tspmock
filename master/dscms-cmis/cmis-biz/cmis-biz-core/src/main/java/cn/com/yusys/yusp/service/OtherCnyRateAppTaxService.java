/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.OtherCnyRateAppTax;
import cn.com.yusys.yusp.repository.mapper.OtherCnyRateAppTaxMapper;

/**
 * @项目名称: cmis-biz-coreModule
 * @类名称: OtherCnyRateAppTaxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-06-05 11:19:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class OtherCnyRateAppTaxService {

    @Autowired
    private OtherCnyRateAppTaxMapper otherCnyRateAppTaxMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public OtherCnyRateAppTax selectByPrimaryKey(String pkId) {
        return otherCnyRateAppTaxMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<OtherCnyRateAppTax> selectAll(QueryModel model) {
        List<OtherCnyRateAppTax> records = (List<OtherCnyRateAppTax>) otherCnyRateAppTaxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<OtherCnyRateAppTax> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<OtherCnyRateAppTax> list = otherCnyRateAppTaxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(OtherCnyRateAppTax record) {
        return otherCnyRateAppTaxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(OtherCnyRateAppTax record) {
        return otherCnyRateAppTaxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(OtherCnyRateAppTax record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppTaxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(OtherCnyRateAppTax record) {
        User userInfo = SessionUtils.getUserInformation(); // 获取当前用户信息
        if(Objects.nonNull(userInfo)){
            record.setUpdId(userInfo.getLoginCode()); // 当前用户号
            record.setUpdBrId(userInfo.getOrg().getCode()); // 当前用户机构
        }
        record.setUpdDate(DateUtils.getCurrDateStr()); // 当前日期
        record.setUpdateTime(DateUtils.getCurrTimestamp());// 当前时间
        return otherCnyRateAppTaxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return otherCnyRateAppTaxMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return otherCnyRateAppTaxMapper.deleteByIds(ids);
    }
    
    /**
     * 获取税务数据
     * @param queryModel
     * @return
     */
    public List<OtherCnyRateAppTax> query(QueryModel queryModel) {
    	List<OtherCnyRateAppTax> taxs = otherCnyRateAppTaxMapper.selectByModel(queryModel);
    	if (CollectionUtils.isEmpty(taxs)) {
    		taxs = new ArrayList<>();
    		String reportType = (String) queryModel.getCondition().get("reportType");
    		//TODO
    		//1.构造上年的数据
    		OtherCnyRateAppTax lastTax = new OtherCnyRateAppTax();
    		lastTax.setBelgYear(CmisCommonConstants.BELG_YEAR_LAST);
    		lastTax.setReportType(reportType);
    		taxs.add(lastTax);
    		//2.构造本期的数据
    		OtherCnyRateAppTax currTax = new OtherCnyRateAppTax();
    		currTax.setBelgYear(CmisCommonConstants.BELG_YEAR_CURR);
    		currTax.setReportType(reportType);
    		taxs.add(currTax);
    	}
    	return taxs;
    }
}
