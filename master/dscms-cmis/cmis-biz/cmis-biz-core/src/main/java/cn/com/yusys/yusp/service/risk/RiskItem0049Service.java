package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.IDCardUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0049Service
 * @类描述: 零售产品规则校验
 * @功能描述: 零售产品规则校验
 * @创建人: hubp
 * @创建时间: 2021年9月6日22:13:53
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0049Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0049Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtCobInfoService lmtCobInfoService;

    @Autowired
    private IqpHouseService iqpHouseService;

    @Autowired
    private IqpCusLsnpInfoService iqpCusLsnpInfoService;

    @Autowired
    private IqpDisAssetConInfoService iqpDisAssetConInfoService;

    @Autowired
    private GuarBizRelService  guarBizRelService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private IqpDisAssetIncomeService iqpDisAssetIncomeService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * @方法名称: riskItem0049
     * @方法描述: 零售产品规则校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: hubp
     * @创建时间: 2021年9月6日22:13:53
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0049(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString(); // 流程编码
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        if("DHE04".equals(bizType) || "LS005".equals(bizType) || "LS006".equals(bizType) || "SGE04".equals(bizType)){
            log.info("*************零售产品规则校验--放款申请进入***********流水号：【{}】", iqpSerno);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(iqpSerno);
            if (Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); //通过放款流水号未获取到出账申请信息
                return riskResultDto;
            }
            if (StringUtils.isBlank(pvpLoanApp.getPrdId())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("产品ID不能为空！");
                return riskResultDto;
            }
            if("022028".equals(pvpLoanApp.getPrdId())){
                // 白领易贷通
                return whiteCollarLoanFk(pvpLoanApp);
            }
        }else {
            log.info("*************零售产品规则校验--业务申请进入***********流水号：【{}】", iqpSerno);
            String prdId = StringUtils.EMPTY;
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
            List<IqpDisAssetIncome> iqpDisAssetIncomeList = iqpDisAssetIncomeService.selectByIqpSerno(iqpSerno);
            if (Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003); //通过授信申请流水号未获取到对应的授信申请信息
                return riskResultDto;
            } else if (iqpDisAssetIncomeList.size() == 0) {
                // 2021年10月13日17:12:04 hubp BUG14683
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("通过授信申请流水号未获取到对应的还款能力分析");
                return riskResultDto;
            }  else{
                prdId = iqpLoanApp.getPrdId();
                // 022024 接力贷
                if("022001".equals(prdId) || "022002".equals(prdId) || "022024".equals(prdId)){
                    // 住房按揭产品（一手、二手、接力贷）
                    return isMortgage(iqpLoanApp);
                } else if ("022020".equals(prdId) || "022021".equals(prdId)  || "022051".equals(prdId)) {
                    // 商用房按揭贷款（一手、二手）
                    return isNoMortgage(iqpLoanApp);
                } else if ("022028".equals(prdId)) {
                    // 白领易贷通
                    return whiteCollarLoan(iqpLoanApp);
                } else if ("022005".equals(prdId)) {
                    // 个人综合消费贷款
                    return consumeLoan(iqpLoanApp);
                } else if ("022006".equals(prdId)) {
                    // 装修贷
                    return fitmentLoan(iqpLoanApp);
                } else if ("022017".equals(prdId)) {
                    // 理财通
                    return managerFinances(iqpLoanApp);
                } else if ("022031".equals(prdId)){
                    // 拍卖贷--> 取房屋信息取房屋类型字段来判断走个人按揭还是商住房
                    IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);
                    if(Objects.isNull(iqpHouse) || StringUtils.isBlank(iqpHouse.getHouseType())){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("房屋信息不能为空！");
                        return riskResultDto;
                    }
                    if("05".equals(iqpHouse.getHouseType())){
                        // 如果为商业用房，走商住房规则
                        return isNoMortgage(iqpLoanApp);
                    } else {
                        // 其他全部走个人按揭
                        return isMortgage(iqpLoanApp);
                    }
                }
            }
        }
        log.info("*************零售产品规则结束***********流水号：【{}】", iqpSerno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }


    /**
     * @param certCode
     * @return java.lang.Integer
     * @author hubp
     * @date 2021/7/29 17:12
     * @version 1.0.0
     * @desc 根据证件号码计算年龄 --精确到月
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Integer handleAgeMonth(String certCode) {
        // 获取当前时间
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        //获取年龄
        int age = IDCardUtils.getAgeByIdCard(certCode);
        // 获取身份证月份 9
        int cardMonth = IDCardUtils.getMonthByIdCard(certCode);
        // 获取当前月份 7
        int nowMonth = Integer.parseInt(openday.substring(5,7));
        // 获取身份证日期  20
        int cardDate = IDCardUtils.getDateByIdCard(certCode);
        // 获取当前日期 20
        int nowDate = Integer.parseInt(openday.substring(8,10));
        // 临时存在月份
        Integer tempMonth = 0;
        // 当周岁需要减一时，计算剩下的月份
        if ((cardMonth -nowMonth) < 0) {
            tempMonth = (nowMonth - cardMonth) + (age * 12);
        } else if ((cardMonth - nowMonth) == 0 && (cardDate-nowDate) < 0) {
            tempMonth = age * 12;
        } else if ((cardMonth - nowMonth) == 0 && (cardDate-nowDate) >= 0) {
            tempMonth = age * 12 - 1;
        } else {
            tempMonth = (age * 12) - (cardMonth - nowMonth);
        }
        log.info("*************根据证件号码计算年龄 --精确到月***********周岁：【{}】", tempMonth);
        return tempMonth;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 住房按揭产品（一手、二手、接力贷、拍卖贷）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto isMortgage(IqpLoanApp iqpLoanApp) {
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal certCodeMonth = new BigDecimal(this.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpLoanApp.getIqpSerno()); // 房产信息
        if (Objects.isNull(iqpHouse)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("根据业务申请流水号查找房屋信息为空！"); //根据业务申请流水号查找房屋信息为空！
            return riskResultDto;
        }
        log.info("客户年龄（月）【{}】：，申请期限：【{}】", certCodeMonth.toPlainString(),iqpLoanApp.getAppTerm().toPlainString());
        //  1、借款人年龄≧18周岁，借款人年龄+期限≦70周岁；
        if (certCodeMonth.compareTo(BigDecimal.valueOf(18 * 12.00)) < 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄必须大于18周岁！"); //借款人年龄必须大于18周岁！
            return riskResultDto;
        }
        if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(70 * 12.00)) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于70周岁！"); //借款人年龄必须小于或等于70周岁！
            return riskResultDto;
        }
        // 2、贷款额度≤房价70%；
        if (iqpLoanApp.getAppAmt().compareTo(iqpHouse.getHouseTotal().multiply(BigDecimal.valueOf(0.7))) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款额度不能大于房价70%！"); //贷款额度不能大于房价70%
            return riskResultDto;
        }
        // 3、住房贷款还款额≦家庭月收入50%、所有负债还款额≦家庭月收入55%；
        IqpDisAssetConInfo conInfo = iqpDisAssetConInfoService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
        if(Objects.isNull(conInfo)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05201); //业务流水号为空
            return riskResultDto;
        }else{
            if( Objects.isNull(conInfo.getIncomePerc1()) || Objects.isNull(conInfo.getIncomePerc2())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05202); //业务流水号为空
                return riskResultDto;
            }else{
                if(conInfo.getIncomePerc1().compareTo(new BigDecimal("0.5")) > 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05203); //业务流水号为空
                    return riskResultDto;
                }
                if(conInfo.getIncomePerc2().compareTo(new BigDecimal("0.55")) > 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05204); //业务流水号为空
                    return riskResultDto;
                }
            }
        }
        // 4、贷款最长期限30年；
        if (iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(30 * 12.00)) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款最长期限30年！"); //贷款最长期限30年！
            return riskResultDto;
        }
        // 5、贷款采取受托支付；
        if (!"1".equals(iqpLoanApp.getPayMode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款必须采取受托支付！"); //贷款必须采取受托支付！
            return riskResultDto;
        }
        // 6、接力贷共同借款人父亲≦60周岁、母亲≦55周岁、子女≧18周岁；
        if ("022024".equals(iqpLoanApp.getPrdId())) {
            List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(iqpLoanApp.getIqpSerno());
            if (lmtCobInfoList.size() > 0) {
                for (LmtCobInfo lmtCobInfo : lmtCobInfoList) {
                    String debitCorre = lmtCobInfo.getCommonDebitCorre(); // 共借人关系
                    String certCode = lmtCobInfo.getCommonDebitCertCode(); // 共借人证件号码
                    if (StringUtils.isBlank(debitCorre)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("共同借款人与主借款人关系不能为空！"); //共同借款人与主借款人关系不能为空
                        return riskResultDto;
                    } else if ("101000".equals(debitCorre)) {
                        // 关系为父母
                        log.info("客户性别【{}】：，客户年龄月：【{}】", IDCardUtils.getGenderByIdCard(certCode),this.handleAgeMonth(certCode));
                        if (("M".equals(IDCardUtils.getGenderByIdCard(certCode)) && (this.handleAgeMonth(certCode) - 720) > 0)
                                || ("F".equals(IDCardUtils.getGenderByIdCard(certCode)) && (this.handleAgeMonth(certCode) - 660) > 0)
                        ) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc("父母年龄不能超过法定退休年龄！"); //父母不超法定退休年龄
                            return riskResultDto;
                        }
                    } else if ("103000".equals(debitCorre)) {
                        // 关系为子女
                        if ((this.handleAgeMonth(certCode) - (18 * 12)) < 0) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc("子女年龄需满18周岁！"); //子女年龄需满18周岁
                            return riskResultDto;
                        }
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("接力贷必须有共同借款人！"); //接力贷必须有共同借款人
                return riskResultDto;
            }
        }
        // 7、签署一般借款合同。
        if (!"1".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为一般借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 商用房按揭贷款（一手、二手、拍卖贷）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto isNoMortgage(IqpLoanApp iqpLoanApp) {
        /**

         */
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal certCodeMonth = new BigDecimal(this.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpLoanApp.getIqpSerno()); // 房产信息
        if (Objects.isNull(iqpHouse)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("根据业务申请流水号查找房屋信息为空！"); //根据业务申请流水号查找房屋信息为空！
            return riskResultDto;
        }
        log.info("客户年龄（月）【{}】：，申请期限：【{}】", certCodeMonth.toPlainString(),iqpLoanApp.getAppTerm().toPlainString());
        //  1、借款人年龄≧18周岁，借款人年龄+期限，男性≦60周岁、女性≦55周岁；；
        if (certCodeMonth.compareTo(BigDecimal.valueOf(18 * 12.00)) < 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄必须大于18周岁！"); //借款人年龄必须大于18周岁！
            return riskResultDto;
        }

        //  2、贷款额度≦房价50%；；
        if (iqpLoanApp.getAppAmt().compareTo(iqpHouse.getHouseTotal().multiply(BigDecimal.valueOf(0.5))) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款额度不能大于房价50%！"); //贷款额度不能大于房价50%
            return riskResultDto;
        }
        // 3、贷款最长期限10年；
        if (iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(10 * 12.00)) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款最长期限10年！"); //贷款最长期限10年！
            return riskResultDto;
        }
        IqpDisAssetConInfo conInfo = iqpDisAssetConInfoService.selectByPrimaryKey(iqpLoanApp.getIqpSerno());
        if(Objects.isNull(conInfo)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05201); //业务流水号为空
            return riskResultDto;
        }else{
            if( Objects.isNull(conInfo.getIncomePerc1()) || Objects.isNull(conInfo.getIncomePerc2())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05202); //业务流水号为空
                return riskResultDto;
            }else{
                // 4、商用房贷款还款额≦家庭月收入50%、所有负债还款额≦家庭月收入55%；
                if(conInfo.getIncomePerc1().compareTo(new BigDecimal("0.5")) > 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05203); //业务流水号为空
                    return riskResultDto;
                }
                if(conInfo.getIncomePerc2().compareTo(new BigDecimal("0.55")) > 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05204); //业务流水号为空
                    return riskResultDto;
                }
            }
        }
        // 5、贷款采取受托支付；
        if (!"1".equals(iqpLoanApp.getPayMode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款必须采取受托支付！"); //贷款必须采取受托支付！
            return riskResultDto;
        }
        // 6、签署一般借款合同。
        if (!"1".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为一般借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 白领易贷通
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto whiteCollarLoan(IqpLoanApp iqpLoanApp){
        /**
         1、借款人年龄≧18周岁，借款人年龄+期限≦60周岁；
         2、信用方式贷款额度≦50万元，信用方式授信期限1年；
         3、评级B级以下的，不得采用信用方式；
         4、单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
         5、签署最高额借款合同
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal certCodeMonth = new BigDecimal(this.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        //  1、借款人年龄≧18周岁，借款人年龄+期限≦60周岁；
        log.info("客户年龄（月）【{}】：，申请期限：【{}】", certCodeMonth.toPlainString(),iqpLoanApp.getAppTerm().toPlainString());
        if (certCodeMonth.compareTo(BigDecimal.valueOf(18 * 12.00)) < 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄必须大于18周岁！"); //借款人年龄必须大于18周岁！
            return riskResultDto;
        }
        if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(60 * 12.00)) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于60周岁！"); //借款人年龄加贷款期限必须小于或等于60周岁
            return riskResultDto;
        }
        // 2、信用方式贷款额度≦50万元，信用方式授信期限1年；
        // 3、评级B级以下的，不得采用信用方式；appScoreRiskLvl
        if("00".equals(iqpLoanApp.getGuarWay())) {
            List<IqpCusLsnpInfo> iqpCusLsnpInfoList = iqpCusLsnpInfoService.selectByIqpSerno(iqpLoanApp.getIqpSerno());
            IqpCusLsnpInfo iqpCusLsnpInfo = null;
            if(iqpCusLsnpInfoList.size() == 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("零售内评信息为空！"); //零售内评信息为空！
                return riskResultDto;
            } else {
                iqpCusLsnpInfo = iqpCusLsnpInfoList.get(0);
            }
            if(!"A".equals(iqpCusLsnpInfo.getAppScoreRiskLvl()) && !"B".equals(iqpCusLsnpInfo.getAppScoreRiskLvl())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("评级B级以下的，不得采用信用方式！"); //评级B级以下的，不得采用信用方式！
                return riskResultDto;
            }
            if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(500000.0)) > 0 || iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(12.0)) > 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("担保方式为信用，贷款额度不得超过50万元，授信期限不得超过1年！"); //评级B级以下的，不得采用信用方式！
                return riskResultDto;
            }
        }
        // 单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
        // 2021年11月22日16:40:53 hubp 问题编号：20211122-00113 先注掉
//        if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(300000)) > 0 && !"1".equals(iqpLoanApp.getPayMode())){
//            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
//            riskResultDto.setRiskResultDesc("单笔支用大于30万元，必须采取受托支付！"); //评级B级以下的，不得采用信用方式！
//            return riskResultDto;
//        }
        if (!"2".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为最高额借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/11/22 16:55
     * @version 1.0.0
     * @desc  白领易贷通放款时校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto whiteCollarLoanFk(PvpLoanApp pvpLoanApp) {
        RiskResultDto riskResultDto = new RiskResultDto();
        log.info("*************零售产品规则校验--放款申请进入***********放款参数：【{}】", JSON.toJSONString(pvpLoanApp));
        if (pvpLoanApp.getPvpAmt().compareTo(BigDecimal.valueOf(300000)) > 0 && !"1".equals(pvpLoanApp.getPayMode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("单笔支用大于30万元，必须采取受托支付！");
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 个人综合消费贷款
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto consumeLoan(IqpLoanApp iqpLoanApp){
        /**
         1、借款人年龄≧18周岁，借款人年龄+期限≦法定退休年龄；
         2、贷款额度≦300万；
         3、贷款最长期限10年；
         4、评级B级以下的，不得采用信用方式；
         5、单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
         6、签署一般借款合同。
         */

        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal certCodeMonth = new BigDecimal(this.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        //  1、借款人年龄≧18周岁，借款人年龄+期限，男性≦60周岁、女性≦55周岁；
        if (certCodeMonth.compareTo(BigDecimal.valueOf(18 * 12.00)) < 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄必须大于18周岁！"); //借款人年龄必须大于18周岁！
            return riskResultDto;
        }


        // 2、贷款额度≦300万；
        // 3、贷款最长期限10年；
        if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(3000000.0)) > 0 || iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(12.0 * 10)) > 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款额度不得超过300万元，贷款期限不得超过10年！"); //
            return riskResultDto;
        }
        if("00".equals(iqpLoanApp.getGuarWay())) {
            List<IqpCusLsnpInfo> iqpCusLsnpInfoList = iqpCusLsnpInfoService.selectByIqpSerno(iqpLoanApp.getIqpSerno());
            IqpCusLsnpInfo iqpCusLsnpInfo = null;
            if(iqpCusLsnpInfoList.size() == 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("零售内评信息为空！"); //零售内评信息为空！
                return riskResultDto;
            } else {
                iqpCusLsnpInfo = iqpCusLsnpInfoList.get(0);
            }
            if(!"A".equals(iqpCusLsnpInfo.getAppScoreRiskLvl()) && !"B".equals(iqpCusLsnpInfo.getAppScoreRiskLvl())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("评级B级以下的，不得采用信用方式！"); //评级B级以下的，不得采用信用方式！
                return riskResultDto;
            }
        }
        // 单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
        if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(300000)) > 0 && !"1".equals(iqpLoanApp.getPayMode())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("单笔支用大于30万元，必须采取受托支付！"); //评级B级以下的，不得采用信用方式！
            return riskResultDto;
        }
        if (!"1".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为一般借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        // 先判断借款人性别  --->性别(M - 男 ， F - 女 ， N - 未知)
        // 2021年10月9日22:56:51 hubp BUG14346
        if(CmisCommonConstants.GUAR_MODE_20.equals(iqpLoanApp.getGuarWay())){
            //如果担保方式为质押，则判断是否为存单质押，为存单质押，可突破年龄限制
            if(guarBaseInfoService.getBaseInfo(iqpLoanApp.getIqpSerno()) == 0){
                if ("M".equals(IDCardUtils.getGenderByIdCard(iqpLoanApp.getCertCode()))) {
                    if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(60 * 12.00)) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于法定退休年龄！"); //借款人年龄必须小于或等于60周岁！
                        return riskResultDto;
                    }
                } else {
                    if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(55 * 12.00)) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于法定退休年龄！"); //借款人年龄必须小于或等于55周岁！
                        return riskResultDto;
                    }
                }
            }else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc("担保方式为个人存单质押时,借款人不受法定退休年龄限制"); //校验通过
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 装修贷
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto fitmentLoan(IqpLoanApp iqpLoanApp){
        /**
         1、借款人年龄≧18周岁，借款人年龄+期限≦法定退休年龄；
         2、贷款额度≦装修合同总价70%；
         3、贷款最长期限10年；
         4、贷款采取受托支付；
         5、签署一般借款合同；
         6、不得采用信用方式。
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal certCodeMonth = new BigDecimal(this.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        //  1、借款人年龄≧18周岁，借款人年龄+期限，男性≦60周岁、女性≦55周岁；；
        if (certCodeMonth.compareTo(BigDecimal.valueOf(18 * 12.00)) < 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款人年龄必须大于18周岁！"); //借款人年龄必须大于18周岁！
            return riskResultDto;
        }
        // 先判断借款人性别  --->性别(M - 男 ， F - 女 ， N - 未知)
        if ("M".equals(IDCardUtils.getGenderByIdCard(iqpLoanApp.getCertCode()))) {
            if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(60 * 12.00)) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于法定退休年龄！"); //借款人年龄必须小于或等于60周岁！
                return riskResultDto;
            }
        } else {
            if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(55 * 12.00)) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于法定退休年龄！"); //借款人年龄必须小于或等于55周岁！
                return riskResultDto;
            }
        }
        if( Objects.isNull(iqpLoanApp.getContTotalAmt())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("未获取到装修贷合同总价！"); //
            return riskResultDto;
        }
        // 2、贷款额度≦装修合同总价70%；
        if( iqpLoanApp.getAppAmt().compareTo(iqpLoanApp.getContTotalAmt().multiply(BigDecimal.valueOf(0.7))) > 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款额度不能大于装修合同总价70%！"); //
            return riskResultDto;
        }
        // 3、贷款最长期限10年；
        if( iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(12.0 * 10)) > 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款期限不得超过10年！"); //
            return riskResultDto;
        }
        // 贷款采取受托支付；
        if (!"1".equals(iqpLoanApp.getPayMode())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款必须采取受托支付！"); //贷款必须采取受托支付！
            return riskResultDto;
        }
        // 签署一般借款合同；
        if (!"1".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为一般借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        // 不得采用信用方式。
        if ("00".equals(iqpLoanApp.getGuarWay())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("不得采用信用方式！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/6 22:30
     * @version 1.0.0
     * @desc 理财通
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto managerFinances(IqpLoanApp iqpLoanApp){
        /**
         1、1万元≦贷款额度≦理财产品的90%；
         2、7天≦贷款期限≦1年；
         3、单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
         4、签署一般借款合同。
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal totalAmt = BigDecimal.ZERO;
        // 1万元≦贷款额度≦理财产品的90%；
        List<GuarBizRel> guarBizRelList = guarBizRelService.queryGuarBizRelDataBySerno(iqpLoanApp.getIqpSerno());
        for(GuarBizRel guarBizRel : guarBizRelList){
            GuarBaseInfo guarBaseInfo = guarBaseInfoService.queryBaseInfoByGuarId(guarBizRel.getGuarNo());
            totalAmt = totalAmt.add(guarBaseInfo.getEvalAmt());
        }
        if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(10000)) < 0 || iqpLoanApp.getAppAmt().compareTo(totalAmt.multiply(BigDecimal.valueOf(0.9))) > 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款额度必须大于1万元且不能大于质押物总金额的90%！"); //
            return riskResultDto;
        }
        // 3、贷款最长期限1年；
        if( iqpLoanApp.getAppTerm().compareTo(BigDecimal.valueOf(12.0 * 1)) > 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("贷款期限不得超过1年！"); //
            return riskResultDto;
        }
        // 单笔支用大于30万元，采取受托支付，≦30万元，自主支付、受托支付均可采用；
        if(iqpLoanApp.getAppAmt().compareTo(BigDecimal.valueOf(300000)) > 0 && !"1".equals(iqpLoanApp.getPayMode())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("单笔支用大于30万元，必须采取受托支付！"); //评级B级以下的，不得采用信用方式！
            return riskResultDto;
        }
        // 签署一般借款合同；
        if (!"1".equals(iqpLoanApp.getContType())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc("借款合同类型必须为一般借款合同！"); //借款合同类型必须为一般借款合同！
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
