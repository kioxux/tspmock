package cn.com.yusys.yusp.service.server.xdtz0059;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccEntrustLoan;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.req.Xdtz0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.resp.Xdtz0059DataRespDto;
import cn.com.yusys.yusp.enums.common.MessageNoEnums;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AccEntrustLoanService;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.service.client.lmt.cmislmt0014.CmisLmt0014Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * 业务逻辑类:还款通知广播交易
 *
 * @author wangyk
 * @version 1.0
 */
@Service
public class Xdtz0059Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0059Service.class);
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private AccEntrustLoanService accEntrustLoanService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisLmt0014Service cmisLmt0014Service;
    @Autowired
    private MessageCommonService messageCommonService;

    /**
     * 交易码：xdtz0059
     * 交易描述：更新信贷台账信息
     *
     * @param xdtz0059DataReqDto
     * @return
     */
    @Transactional
    public Xdtz0059DataRespDto xdtz0059(Xdtz0059DataReqDto xdtz0059DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataReqDto));
        Xdtz0059DataRespDto xdtz0059DataRespDto = new Xdtz0059DataRespDto();
        try {
            // 从redis中获取营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 从xdtz0059DataReqDto获取业务值进行业务逻辑处理
            String loanno = xdtz0059DataReqDto.getLoanno();//借据号
            String status = xdtz0059DataReqDto.getStatus();//状态
            BigDecimal psprcp = xdtz0059DataReqDto.getPsprcp();//发放金额 正常本金
            BigDecimal osprcp = xdtz0059DataReqDto.getOsprcp();//剩余本金 逾期本金
            if (StringUtils.isBlank(loanno)) {
                xdtz0059DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdtz0059DataRespDto.setOpMsg("借据号loanno不能为空");
                return xdtz0059DataRespDto;
            }
            if (StringUtils.isBlank(status)) {
                xdtz0059DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdtz0059DataRespDto.setOpMsg("状态status不能为空");
                return xdtz0059DataRespDto;
            }
            if (psprcp == null) {
                xdtz0059DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdtz0059DataRespDto.setOpMsg("发放金额psprcp不能为空");
                return xdtz0059DataRespDto;
            }
            if (osprcp == null) {
                xdtz0059DataRespDto.setOpFlag(DscmsBizDbEnum.RETURN_FAIL.key);
                xdtz0059DataRespDto.setOpMsg("剩余本金osprcp不能为空");
                return xdtz0059DataRespDto;
            }

            // 前往额度系统释放额度
            // 1、计算还款金额
            logger.info("根据借据编号【{}】查询台账信息开始", loanno);
            AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(loanno);
            AccEntrustLoan accEntrustLoan = accEntrustLoanService.queryByBillNo(loanno);
            // 普通贷款处理
            if(Objects.nonNull(accLoan)){
                logger.info("根据借据号【{}】计算还款金额开始，原贷款余额【{}】，正常本金余额【{}】，逾期本金余额【{}】", loanno, accLoan.getLoanBalance()
                        , psprcp, osprcp);
                BigDecimal repayAmt = accLoan.getLoanBalance().subtract(psprcp.add(osprcp));
                logger.info("根据借据号【{}】计算还款金额结束，原贷款余额【{}】，正常本金余额【{}】，逾期本金余额【{}】，最终计算还款金额【{}】", loanno, accLoan.getLoanBalance()
                        , psprcp, osprcp, repayAmt);

                logger.info("根据借据号【{}】计算最终计算还款金额(人民币)开始,原台账币种【{}】、汇率【{}】", loanno, accLoan.getContCurType(), accLoan.getExchangeRate());
                BigDecimal repayRMBAmt = null;
                // 若汇率为空，默认人民币
                BigDecimal exchangeRate = BigDecimal.ONE;
                if (Objects.isNull(accLoan.getExchangeRate())) {
                    logger.info("根据借据号【{}】查询汇率为空，默认为1", loanno);
                    repayRMBAmt = repayAmt;
                } else {
                    if ("CNY".equals(accLoan.getContCurType())) {
                        logger.info("根据借据号【{}】查询币种为人民币，汇率默认为1", loanno);
                        repayRMBAmt = repayAmt;
                    } else {
                        logger.info("根据借据号【{}】查询币种为【{}】，汇率为【{}】", loanno, accLoan.getContCurType(), accLoan.getExchangeRate());
                        repayRMBAmt = repayAmt.multiply(accLoan.getExchangeRate());
                        exchangeRate = accLoan.getExchangeRate();
                    }
                }

                logger.info("根据借据号【{}】计算最终计算还款金额(人民币)【{}】结束", loanno, repayRMBAmt.stripTrailingZeros().toPlainString());
                // 2、发送额度系统
                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
                List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accLoan.getManagerBrId()));//金融机构代码
                // 唯一编号
                cmisLmt0014ReqDto.setSerno(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                cmisLmt0014ReqDto.setInputId(accLoan.getInputId());
                cmisLmt0014ReqDto.setInputBrId(accLoan.getInputBrId());
                cmisLmt0014ReqDto.setInputDate(accLoan.getInputDate());

                dealBizList.setDealBizNo(loanno);
                if (!"1".equals(status)) {//03 还款
                    dealBizList.setRecoverType("03");
                } else {
                    dealBizList.setRecoverType("01");//01  结清恢复
                }
                // 21	低风险质押 40	全额保证金 60	低风险 敞口 送 0
                // 获取担保方式
                String guarMode  = accLoan.getGuarMode();
                if("21".equals(guarMode) || "40".equals(guarMode) || "60".equals(guarMode)){
                    dealBizList.setRecoverSpacAmtCny(new BigDecimal("0"));
                } else {
                    dealBizList.setRecoverSpacAmtCny(repayRMBAmt);
                }
                dealBizList.setRecoverAmtCny(repayRMBAmt);
                dealBizList.setDealBizBailPreRate(new BigDecimal("0"));
                dealBizList.setSecurityAmt(new BigDecimal("0"));
                cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
                cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

                logger.info("根据借据编号【" + loanno + "】前往额度系统-台账额度恢复请求报文：" + cmisLmt0014ReqDto.toString());
                CmisLmt0014RespDto cmisLmt0014RespDto = cmisLmt0014Service.cmisLmt0014(cmisLmt0014ReqDto);
                logger.info("根据业务申请编号【" + loanno + "】前往额度系统-台账额度恢复返回报文：" + cmisLmt0014RespDto.toString());

                // 3、更新贷款余额
                Map<String, String> loanMap = new HashMap<>();
                if ("1".equals(status)) {// 结清
                    logger.info("借据编号【{}】，台账已结清，更新台账状态开始！", loanno);
                    loanMap.put("billNo", loanno);// 借据号
                    loanMap.put("accStatus", "0");// 贷款状态
                    loanMap.put("loanBalance", "0");// 贷款余额
                    loanMap.put("settlDate", openDay);// 结清日期
                    loanMap.put("exchangeRmbBal", "0");//折合人民币余额
                    // 更新贷款台账状态为结清
                    logger.info("更新台账信息,参数【{}】", loanMap);
                    accLoanService.updateAccLoanByBillNo(loanMap);

                    // 发送结清短信信息开始
                    String receivedUserType = "1";// 接收人员类型 1--客户经理 2--借款人
                    Map paramMap = new HashMap();// 短信填充参数
                    paramMap.put("cusName", accLoan.getCusName());
                    paramMap.put("prdName", accLoan.getPrdName());
                    paramMap.put("billNo", accLoan.getBillNo());
                    //执行发送客户经理操作
                    messageCommonService.sendMessage(MessageNoEnums.MSG_HK_M_0001.key, paramMap, receivedUserType, accLoan.getManagerId(), "");
                } else {
                    loanMap.put("billNo", loanno);//借据号
                    loanMap.put("loanBalance", psprcp.add(osprcp).toString());// 贷款余额
                    loanMap.put("exchangeRmbBal", (psprcp.add(osprcp)).multiply(exchangeRate).stripTrailingZeros().toPlainString());// 折合人民币余额
                    // 更新贷款台账余额
                    logger.info("更新台账信息,参数【{}】", loanMap);
                    accLoanService.updateAccLoanByBillNo(loanMap);
                }

            } else if(Objects.nonNull(accEntrustLoan)){ // 委托贷款
                logger.info("根据借据号【{}】计算还款金额开始，原贷款余额【{}】，正常本金余额【{}】，逾期本金余额【{}】", loanno, accEntrustLoan.getLoanBalance()
                        , psprcp, osprcp);
                BigDecimal repayAmt = accEntrustLoan.getLoanBalance().subtract(psprcp.add(osprcp));
                logger.info("根据借据号【{}】计算还款金额结束，原贷款余额【{}】，正常本金余额【{}】，逾期本金余额【{}】，最终计算还款金额【{}】", loanno, accEntrustLoan.getLoanBalance()
                        , psprcp, osprcp, repayAmt);

                logger.info("根据借据号【{}】计算最终计算还款金额(人民币)开始,原台账币种【{}】、汇率【{}】", loanno, accEntrustLoan.getContCurType(), accEntrustLoan.getExchangeRate());
                BigDecimal repayRMBAmt = null;
                // 若汇率为空，默认人民币
                BigDecimal exchangeRate = BigDecimal.ONE;
                if (Objects.isNull(accEntrustLoan.getExchangeRate())) {
                    logger.info("根据借据号【{}】查询汇率为空，默认为1", loanno);
                    repayRMBAmt = repayAmt;
                } else {
                    if ("CNY".equals(accEntrustLoan.getContCurType())) {
                        logger.info("根据借据号【{}】查询币种为人民币，汇率默认为1", loanno);
                        repayRMBAmt = repayAmt;
                    } else {
                        logger.info("根据借据号【{}】查询币种为【{}】，汇率为【{}】", loanno, accEntrustLoan.getContCurType(), accEntrustLoan.getExchangeRate());
                        repayRMBAmt = repayAmt.multiply(accEntrustLoan.getExchangeRate());
                        exchangeRate = accEntrustLoan.getExchangeRate();
                    }
                }

                logger.info("根据借据号【{}】计算最终计算还款金额(人民币)【{}】结束", loanno, repayRMBAmt.stripTrailingZeros().toPlainString());
                // 2、发送额度系统
                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                CmisLmt0014ReqdealBizListDto dealBizList = new CmisLmt0014ReqdealBizListDto();
                List<CmisLmt0014ReqdealBizListDto> cmisLmt0014ReqdealBizListDtoList = new ArrayList<>();
                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0014ReqDto.setInstuCde(CmisCommonUtils.getInstucde(accEntrustLoan.getManagerBrId()));//金融机构代码
                // 唯一编号
                cmisLmt0014ReqDto.setSerno(UUID.randomUUID().toString().replace("-", "").toUpperCase());
                cmisLmt0014ReqDto.setInputId(accEntrustLoan.getInputId());
                cmisLmt0014ReqDto.setInputBrId(accEntrustLoan.getInputBrId());
                cmisLmt0014ReqDto.setInputDate(accEntrustLoan.getInputDate());

                dealBizList.setDealBizNo(loanno);
                if (!"1".equals(status)) {//03 还款
                    dealBizList.setRecoverType("03");
                } else {
                    dealBizList.setRecoverType("01");//01  结清恢复
                }
                // 21	低风险质押 40	全额保证金 60	低风险 敞口 送 0
                // 获取担保方式
                String guarMode  = accEntrustLoan.getGuarMode();
                if("21".equals(guarMode) || "40".equals(guarMode) || "60".equals(guarMode)){
                    dealBizList.setRecoverSpacAmtCny(new BigDecimal("0"));
                } else {
                    dealBizList.setRecoverSpacAmtCny(repayRMBAmt);
                }
                dealBizList.setRecoverAmtCny(repayRMBAmt);
                dealBizList.setDealBizBailPreRate(new BigDecimal("0"));
                dealBizList.setSecurityAmt(new BigDecimal("0"));
                cmisLmt0014ReqdealBizListDtoList.add(dealBizList);
                cmisLmt0014ReqDto.setDealBizList(cmisLmt0014ReqdealBizListDtoList);

                logger.info("根据借据编号【" + loanno + "】前往额度系统-台账额度恢复请求报文：" + cmisLmt0014ReqDto.toString());
                CmisLmt0014RespDto cmisLmt0014RespDto = cmisLmt0014Service.cmisLmt0014(cmisLmt0014ReqDto);
                logger.info("根据业务申请编号【" + loanno + "】前往额度系统-台账额度恢复返回报文：" + cmisLmt0014RespDto.toString());

                // 3、更新贷款余额
                Map<String, String> loanMap = new HashMap<>();
                if ("1".equals(status)) {// 结清
                    loanMap.put("billNo", loanno);// 借据号
                    loanMap.put("accStatus", "0");// 贷款状态
                    loanMap.put("loanBalance", "0");// 贷款余额
                    loanMap.put("settlDate", openDay);// 结清日期
                    loanMap.put("exchangeRmbBal", "0");//折合人民币余额
                    // 更新委托贷款余额和状态
                    accEntrustLoanService.updateAccEntrustLoanByBillNo(loanMap);

                    // 发送结清短信信息开始
                    String receivedUserType = "1";// 接收人员类型 1--客户经理 2--借款人
                    Map paramMap = new HashMap();// 短信填充参数
                    paramMap.put("cusName", accLoan.getCusName());
                    paramMap.put("prdName", accLoan.getPrdName());
                    paramMap.put("billNo", accLoan.getBillNo());
                    //执行发送客户经理操作
                    messageCommonService.sendMessage(MessageNoEnums.MSG_HK_M_0001.key, paramMap, receivedUserType, accLoan.getManagerId(), "");
                } else {
                    loanMap.put("billNo", loanno);//借据号
                    loanMap.put("loanBalance", psprcp.add(osprcp).toString());// 贷款余额
                    loanMap.put("exchangeRmbBal", (psprcp.add(osprcp)).multiply(exchangeRate).stripTrailingZeros().toPlainString());// 折合人民币余额
                    // 更新委托贷款余额
                    accEntrustLoanService.updateAccEntrustLoanByBillNo(loanMap);
                }
            }
            xdtz0059DataRespDto.setOpFlag(DscmsBizCzEnum.OP_FLAG_S.key);// 操作成功标志位
            xdtz0059DataRespDto.setOpMsg(DscmsBizCzEnum.OP_FLAG_S.value);// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataRespDto));
        return xdtz0059DataRespDto;
    }
}
