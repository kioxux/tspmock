package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpExtLoan
 * @类描述: pvp_ext_loan数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-13 14:33:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpExtLoanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 展期出账申请流水号 **/
	private String pvpSerno;
	
	/** 原展期出账流水号 **/
	private String oldPvpSerno;
	
	/** 展期协议编号 **/
	private String extCtrNo;
	
	/** 原借据编号 **/
	private String oldBillNo;
	
	/** 原合同编号 **/
	private String oldContNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 原币种 STD_ZB_CUR_TYP **/
	private String fountCurType;
	
	/** 原贷款金额 **/
	private java.math.BigDecimal fountLoanAmt;
	
	/** 原贷款余额 **/
	private java.math.BigDecimal fountLoanBalance;
	
	/** 原起贷日期 **/
	private String fountStartDate;
	
	/** 原止贷日期 **/
	private String fountEndDate;
	
	/** 原执行利率（年） **/
	private java.math.BigDecimal oldRealityIrY;
	
	/** 原执行利率(月) **/
	private java.math.BigDecimal oldRealityIrM;
	
	/** 原逾期利率（年） **/
	private java.math.BigDecimal oldOverdueRateY;
	
	/** 原违约利率（年） **/
	private java.math.BigDecimal oldDefaultRateY;
	
	/** 展期金额 **/
	private java.math.BigDecimal extAmt;
	
	/** 展期到期日期 **/
	private String extEndDate;
	
	/** 利率依据方式 STD_ZB_IR_WAY **/
	private String irAccordType;
	
	/** 利率种类 STD_ZB_IR_TYP **/
	private String irType;
	
	/** 基准利率（年） **/
	private java.math.BigDecimal rulingIr;
	
	/** 对应基准利率(月) **/
	private java.math.BigDecimal rulingIrM;
	
	/** 计息方式 STD_ZB_LOAN_RAT_TYPE **/
	private String loanRatType;
	
	/** 利率调整方式 STD_ZB_RADJ_TYP **/
	private String irAdjustType;
	
	/** 利率调整周期(月) **/
	private java.math.BigDecimal irAdjustTerm;
	
	/** 调息方式 STD_ZB_PRA_MODE **/
	private String praType;
	
	/** 利率形式 STD_ZB_RATE_TYPE **/
	private String rateType;
	
	/** 正常利率浮动方式 STD_ZB_RFLOAT_TYP **/
	private String irFloatType;
	
	/** 利率浮动百分比 **/
	private java.math.BigDecimal irFloatRate;
	
	/** 固定加点值 **/
	private java.math.BigDecimal irFloatPoint;
	
	/** 执行利率（年） **/
	private java.math.BigDecimal realityIrY;
	
	/** 执行利率(月) **/
	private java.math.BigDecimal realityIrM;
	
	/** 逾期利率浮动方式 STD_ZB_RFLOAT_TYP **/
	private String overdueFloatType;
	
	/** 逾期利率浮动加点值 **/
	private java.math.BigDecimal overduePoint;
	
	/** 逾期利率浮动百分比 **/
	private java.math.BigDecimal overdueRate;
	
	/** 逾期利率（年） **/
	private java.math.BigDecimal overdueRateY;
	
	/** 违约利率浮动方式 STD_ZB_RFLOAT_TYP **/
	private String defaultFloatType;
	
	/** 违约利率浮动加点值 **/
	private java.math.BigDecimal defaultPoint;
	
	/** 违约利率浮动百分比 **/
	private java.math.BigDecimal defaultRate;
	
	/** 违约利率（年） **/
	private java.math.BigDecimal defaultRateY;
	
	/** 备注 **/
	private String remark;
	
	/** 主办机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 最后修改日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 申请状态 STD_ZB_APP_ST **/
	private String approveStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
	}
	
    /**
     * @return PvpSerno
     */	
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param oldPvpSerno
	 */
	public void setOldPvpSerno(String oldPvpSerno) {
		this.oldPvpSerno = oldPvpSerno == null ? null : oldPvpSerno.trim();
	}
	
    /**
     * @return OldPvpSerno
     */	
	public String getOldPvpSerno() {
		return this.oldPvpSerno;
	}
	
	/**
	 * @param extCtrNo
	 */
	public void setExtCtrNo(String extCtrNo) {
		this.extCtrNo = extCtrNo == null ? null : extCtrNo.trim();
	}
	
    /**
     * @return ExtCtrNo
     */	
	public String getExtCtrNo() {
		return this.extCtrNo;
	}
	
	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo == null ? null : oldBillNo.trim();
	}
	
    /**
     * @return OldBillNo
     */	
	public String getOldBillNo() {
		return this.oldBillNo;
	}
	
	/**
	 * @param oldContNo
	 */
	public void setOldContNo(String oldContNo) {
		this.oldContNo = oldContNo == null ? null : oldContNo.trim();
	}
	
    /**
     * @return OldContNo
     */	
	public String getOldContNo() {
		return this.oldContNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param fountCurType
	 */
	public void setFountCurType(String fountCurType) {
		this.fountCurType = fountCurType == null ? null : fountCurType.trim();
	}
	
    /**
     * @return FountCurType
     */	
	public String getFountCurType() {
		return this.fountCurType;
	}
	
	/**
	 * @param fountLoanAmt
	 */
	public void setFountLoanAmt(java.math.BigDecimal fountLoanAmt) {
		this.fountLoanAmt = fountLoanAmt;
	}
	
    /**
     * @return FountLoanAmt
     */	
	public java.math.BigDecimal getFountLoanAmt() {
		return this.fountLoanAmt;
	}
	
	/**
	 * @param fountLoanBalance
	 */
	public void setFountLoanBalance(java.math.BigDecimal fountLoanBalance) {
		this.fountLoanBalance = fountLoanBalance;
	}
	
    /**
     * @return FountLoanBalance
     */	
	public java.math.BigDecimal getFountLoanBalance() {
		return this.fountLoanBalance;
	}
	
	/**
	 * @param fountStartDate
	 */
	public void setFountStartDate(String fountStartDate) {
		this.fountStartDate = fountStartDate == null ? null : fountStartDate.trim();
	}
	
    /**
     * @return FountStartDate
     */	
	public String getFountStartDate() {
		return this.fountStartDate;
	}
	
	/**
	 * @param fountEndDate
	 */
	public void setFountEndDate(String fountEndDate) {
		this.fountEndDate = fountEndDate == null ? null : fountEndDate.trim();
	}
	
    /**
     * @return FountEndDate
     */	
	public String getFountEndDate() {
		return this.fountEndDate;
	}
	
	/**
	 * @param oldRealityIrY
	 */
	public void setOldRealityIrY(java.math.BigDecimal oldRealityIrY) {
		this.oldRealityIrY = oldRealityIrY;
	}
	
    /**
     * @return OldRealityIrY
     */	
	public java.math.BigDecimal getOldRealityIrY() {
		return this.oldRealityIrY;
	}
	
	/**
	 * @param oldRealityIrM
	 */
	public void setOldRealityIrM(java.math.BigDecimal oldRealityIrM) {
		this.oldRealityIrM = oldRealityIrM;
	}
	
    /**
     * @return OldRealityIrM
     */	
	public java.math.BigDecimal getOldRealityIrM() {
		return this.oldRealityIrM;
	}
	
	/**
	 * @param oldOverdueRateY
	 */
	public void setOldOverdueRateY(java.math.BigDecimal oldOverdueRateY) {
		this.oldOverdueRateY = oldOverdueRateY;
	}
	
    /**
     * @return OldOverdueRateY
     */	
	public java.math.BigDecimal getOldOverdueRateY() {
		return this.oldOverdueRateY;
	}
	
	/**
	 * @param oldDefaultRateY
	 */
	public void setOldDefaultRateY(java.math.BigDecimal oldDefaultRateY) {
		this.oldDefaultRateY = oldDefaultRateY;
	}
	
    /**
     * @return OldDefaultRateY
     */	
	public java.math.BigDecimal getOldDefaultRateY() {
		return this.oldDefaultRateY;
	}
	
	/**
	 * @param extAmt
	 */
	public void setExtAmt(java.math.BigDecimal extAmt) {
		this.extAmt = extAmt;
	}
	
    /**
     * @return ExtAmt
     */	
	public java.math.BigDecimal getExtAmt() {
		return this.extAmt;
	}
	
	/**
	 * @param extEndDate
	 */
	public void setExtEndDate(String extEndDate) {
		this.extEndDate = extEndDate == null ? null : extEndDate.trim();
	}
	
    /**
     * @return ExtEndDate
     */	
	public String getExtEndDate() {
		return this.extEndDate;
	}
	
	/**
	 * @param irAccordType
	 */
	public void setIrAccordType(String irAccordType) {
		this.irAccordType = irAccordType == null ? null : irAccordType.trim();
	}
	
    /**
     * @return IrAccordType
     */	
	public String getIrAccordType() {
		return this.irAccordType;
	}
	
	/**
	 * @param irType
	 */
	public void setIrType(String irType) {
		this.irType = irType == null ? null : irType.trim();
	}
	
    /**
     * @return IrType
     */	
	public String getIrType() {
		return this.irType;
	}
	
	/**
	 * @param rulingIr
	 */
	public void setRulingIr(java.math.BigDecimal rulingIr) {
		this.rulingIr = rulingIr;
	}
	
    /**
     * @return RulingIr
     */	
	public java.math.BigDecimal getRulingIr() {
		return this.rulingIr;
	}
	
	/**
	 * @param rulingIrM
	 */
	public void setRulingIrM(java.math.BigDecimal rulingIrM) {
		this.rulingIrM = rulingIrM;
	}
	
    /**
     * @return RulingIrM
     */	
	public java.math.BigDecimal getRulingIrM() {
		return this.rulingIrM;
	}
	
	/**
	 * @param loanRatType
	 */
	public void setLoanRatType(String loanRatType) {
		this.loanRatType = loanRatType == null ? null : loanRatType.trim();
	}
	
    /**
     * @return LoanRatType
     */	
	public String getLoanRatType() {
		return this.loanRatType;
	}
	
	/**
	 * @param irAdjustType
	 */
	public void setIrAdjustType(String irAdjustType) {
		this.irAdjustType = irAdjustType == null ? null : irAdjustType.trim();
	}
	
    /**
     * @return IrAdjustType
     */	
	public String getIrAdjustType() {
		return this.irAdjustType;
	}
	
	/**
	 * @param irAdjustTerm
	 */
	public void setIrAdjustTerm(java.math.BigDecimal irAdjustTerm) {
		this.irAdjustTerm = irAdjustTerm;
	}
	
    /**
     * @return IrAdjustTerm
     */	
	public java.math.BigDecimal getIrAdjustTerm() {
		return this.irAdjustTerm;
	}
	
	/**
	 * @param praType
	 */
	public void setPraType(String praType) {
		this.praType = praType == null ? null : praType.trim();
	}
	
    /**
     * @return PraType
     */	
	public String getPraType() {
		return this.praType;
	}
	
	/**
	 * @param rateType
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType == null ? null : rateType.trim();
	}
	
    /**
     * @return RateType
     */	
	public String getRateType() {
		return this.rateType;
	}
	
	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType == null ? null : irFloatType.trim();
	}
	
    /**
     * @return IrFloatType
     */	
	public String getIrFloatType() {
		return this.irFloatType;
	}
	
	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}
	
    /**
     * @return IrFloatRate
     */	
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}
	
	/**
	 * @param irFloatPoint
	 */
	public void setIrFloatPoint(java.math.BigDecimal irFloatPoint) {
		this.irFloatPoint = irFloatPoint;
	}
	
    /**
     * @return IrFloatPoint
     */	
	public java.math.BigDecimal getIrFloatPoint() {
		return this.irFloatPoint;
	}
	
	/**
	 * @param realityIrY
	 */
	public void setRealityIrY(java.math.BigDecimal realityIrY) {
		this.realityIrY = realityIrY;
	}
	
    /**
     * @return RealityIrY
     */	
	public java.math.BigDecimal getRealityIrY() {
		return this.realityIrY;
	}
	
	/**
	 * @param realityIrM
	 */
	public void setRealityIrM(java.math.BigDecimal realityIrM) {
		this.realityIrM = realityIrM;
	}
	
    /**
     * @return RealityIrM
     */	
	public java.math.BigDecimal getRealityIrM() {
		return this.realityIrM;
	}
	
	/**
	 * @param overdueFloatType
	 */
	public void setOverdueFloatType(String overdueFloatType) {
		this.overdueFloatType = overdueFloatType == null ? null : overdueFloatType.trim();
	}
	
    /**
     * @return OverdueFloatType
     */	
	public String getOverdueFloatType() {
		return this.overdueFloatType;
	}
	
	/**
	 * @param overduePoint
	 */
	public void setOverduePoint(java.math.BigDecimal overduePoint) {
		this.overduePoint = overduePoint;
	}
	
    /**
     * @return OverduePoint
     */	
	public java.math.BigDecimal getOverduePoint() {
		return this.overduePoint;
	}
	
	/**
	 * @param overdueRate
	 */
	public void setOverdueRate(java.math.BigDecimal overdueRate) {
		this.overdueRate = overdueRate;
	}
	
    /**
     * @return OverdueRate
     */	
	public java.math.BigDecimal getOverdueRate() {
		return this.overdueRate;
	}
	
	/**
	 * @param overdueRateY
	 */
	public void setOverdueRateY(java.math.BigDecimal overdueRateY) {
		this.overdueRateY = overdueRateY;
	}
	
    /**
     * @return OverdueRateY
     */	
	public java.math.BigDecimal getOverdueRateY() {
		return this.overdueRateY;
	}
	
	/**
	 * @param defaultFloatType
	 */
	public void setDefaultFloatType(String defaultFloatType) {
		this.defaultFloatType = defaultFloatType == null ? null : defaultFloatType.trim();
	}
	
    /**
     * @return DefaultFloatType
     */	
	public String getDefaultFloatType() {
		return this.defaultFloatType;
	}
	
	/**
	 * @param defaultPoint
	 */
	public void setDefaultPoint(java.math.BigDecimal defaultPoint) {
		this.defaultPoint = defaultPoint;
	}
	
    /**
     * @return DefaultPoint
     */	
	public java.math.BigDecimal getDefaultPoint() {
		return this.defaultPoint;
	}
	
	/**
	 * @param defaultRate
	 */
	public void setDefaultRate(java.math.BigDecimal defaultRate) {
		this.defaultRate = defaultRate;
	}
	
    /**
     * @return DefaultRate
     */	
	public java.math.BigDecimal getDefaultRate() {
		return this.defaultRate;
	}
	
	/**
	 * @param defaultRateY
	 */
	public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
		this.defaultRateY = defaultRateY;
	}
	
    /**
     * @return DefaultRateY
     */	
	public java.math.BigDecimal getDefaultRateY() {
		return this.defaultRateY;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}