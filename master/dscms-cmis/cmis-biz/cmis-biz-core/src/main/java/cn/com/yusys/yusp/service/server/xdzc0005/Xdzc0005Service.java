package cn.com.yusys.yusp.service.server.xdzc0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.req.CredisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.CredisRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.req.DepoisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.DepoisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.req.List;
import cn.com.yusys.yusp.dto.server.xdzc0005.req.Xdzc0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.resp.Xdzc0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 接口处理类:资产池入池接口
 *
 * @Author xs
 * @Date 2021/06/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0005Service {

    @Autowired
    private AsplIoPoolService asplIoPoolService;

    @Autowired
    private AsplAorgListService asplAorgListService;

    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private Dscms2YphsxtClientService dscms2YphsxtClientService;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0005Service.class);

    /**
     * 接收资产列表，其中资产包括入池、入池且质押、已入池质押三种不同类型的入池处理，
     * 1、遍历每条数据，
     * ---校验 入池银票的承兑行名单 如果不存在？？？？
     * ---校验 是否是入池质押 如果是调用押品系统的交易进行押品创建和押品入库(调用xddb0003接口)
     * 2、资产出入池表 资产出入池明细表 中新增该记录
     * 3、根据 协议编号 资产编号 去客户资产清单表 校验该资产是否存在资产清单里
     * *    ---如果不存在，在该协议编号下新增 资产信息（入池不需要走审批997)
     * *    ---存在，更新资产状态（是否入池、是否质押）
     *
     * @param xdzc0005DataReqDto
     * @return
     */
    @Transactional
    public Xdzc0005DataRespDto xdzc0005Service(Xdzc0005DataReqDto xdzc0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value);
        Xdzc0005DataRespDto xdzc0005DataRespDto = new Xdzc0005DataRespDto();
        String contNo = xdzc0005DataReqDto.getContNo();//协议编号
        java.util.List<List> lists = xdzc0005DataReqDto.getList();// 入池资产清单
        //默认成功
        List listObj = lists.get(0);
        xdzc0005DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
        xdzc0005DataRespDto.setOpMsg("入池成功");
        try {
//            /**
//             * 校验 入池银票的承兑行名单
//             */
//            java.util.List<String> acptBankIdList = new ArrayList<String>();
//            lists.stream()
//                    .distinct()//去重复
//                    .filter(e -> CommonConstance.STD_ASPL_ASSET_TYPE_01.equals(e.getAssetType()))//判断是不是银票
//                    .forEach(f -> acptBankIdList.add(f.getAcptBankId()));
//            // 调用资产池承兑行名单校验（校验内容 ：1、是否生效 2、是否存在 3、操作类型）
//            java.util.List<String> avaAcptBankIdList = asplAorgListService.isAsplAorgList(acptBankIdList);
//            //校验有效银票承兑行清单和参数中银票承兑行行号
//            acptBankIdList.removeAll(avaAcptBankIdList);
//            //如果存在 无效的承兑行行号，那该笔资产不符合入池，终止整个请求
//            if (acptBankIdList.size() > 0) {
//                //默认成功
//                xdzc0005DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
//                xdzc0005DataRespDto.setOpMsg("无效的承兑行号" + acptBankIdList.toString());
//                return xdzc0005DataRespDto;
//            }
            //批量查询
            java.util.List<String> assetNoList = lists.stream()
                    .map(e -> {
                        return e.getAssetNo();
                    })
                    .collect(Collectors.toList());
            //根据资产编号去批量查询已经存在的池内的资产清单(需要更新的资产编号)
            java.util.List<String> updateAssetNoList = asplAssetsListService.isAssetNoList(assetNoList);

            //如果出现重复的资产编号 终止（ 已和风控，网银确认，重复入池不报错）
            if(CollectionUtils.nonEmpty(updateAssetNoList)){
                xdzc0005DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdzc0005DataRespDto.setOpMsg("资产编号："+updateAssetNoList.toString()+"已存在该池下");
                return xdzc0005DataRespDto;
            }

            // 资产出入池
            AsplIoPool asplIoPool = new AsplIoPool();
            // 资产出入池明细表
            java.util.List<AsplIoPoolDetails> asplIoPoolDetailsList = new ArrayList<AsplIoPoolDetails>();
            // 客户资产清单
            java.util.List<AsplAssetsList> asplAssetsListList = new ArrayList<AsplAssetsList>();
            // 资产池协议信息
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);


            logger.info("入池质押资产清单校验，是否已被质押");
            // 获取所有的入池质押资产清单
            java.util.List<List> IsPledgeAsplAssetsListList = lists.stream()
                    .filter(e -> {
                        return CommonConstance.STD_ZB_YES_NO_1.equals(e.getIsPledge());
                    }).collect(Collectors.toList());
            // 获取所有资产的 统一押品编号集合(只有入池质押的的资产才会有统一押品编号)
            java.util.List<String> guarNoList = IsPledgeAsplAssetsListList.stream()
                    .map(e->{
                        return e.getGuarNo();
                    }).collect(Collectors.toList());
            // 生成核心担保编号
            String grpNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YP_SERNO, new HashMap<>());
            java.util.List<String> guarCopyNoList = guarNoList;
            // 判断押品是否已经被质押过（使用统一押品编号去查询表中是否存在）
            if (CollectionUtils.nonEmpty(guarCopyNoList)){
                java.util.List<String> alreadyGrtGuarContRelList= grtGuarContRelService.selectByGuarCopyNoList(guarCopyNoList);
                // 如果存在已经被质押过的资产编号 return
                if(CollectionUtils.nonEmpty(alreadyGrtGuarContRelList)){
                    xdzc0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdzc0005DataRespDto.setOpMsg("资产已被质押,押品编号为:"+alreadyGrtGuarContRelList.toString());
                    return xdzc0005DataRespDto;
                }
            }else {
                throw new BizException(null,"",null,"入池质押资产清单为空！");
            }
            logger.info("池内资产入池质押操作开始");
            // * 01银行承兑汇票（电子）(风控不传)
            // * 02本行存单（电子）
            // 营业时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 申请流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
            asplIoPool.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
            asplIoPool.setSerno(serno);//业务流水号
            asplIoPool.setContNo(contNo);//合同编号
            asplIoPool.setInoutType(CmisCommonConstants.INOUT_TYPE_1);// 出入类型
            asplIoPool.setCusId(ctrAsplDetails.getCusId());// 客户编号
            asplIoPool.setCusName(ctrAsplDetails.getCusName());// 客户名称
            asplIoPool.setLmtAmt(ctrAsplDetails.getLmtAmt());//授信额度
            asplIoPool.setOutstndAmt(BigDecimal.ZERO);//todo 已用额度
            asplIoPool.setStartDate(openDay);//起始日期
            asplIoPool.setEndDate(ctrAsplDetails.getEndDate());//到期日期
            asplIoPool.setResn(StringUtils.EMPTY);//原因
            asplIoPool.setApprStatus(CmisCommonConstants.WF_STATUS_997);//审批状态(入池不需要走审批，默认通过)
            asplIoPool.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            asplIoPool.setInputId(ctrAsplDetails.getInputId());//登记人
            asplIoPool.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
            asplIoPool.setInputDate(openDay);//登记日期
            asplIoPool.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
            asplIoPool.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
            asplIoPool.setUpdDate(openDay);//最近修改日期
            asplIoPool.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
            asplIoPool.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
            asplIoPool.setCreateTime(DateUtils.getCurrTimestamp());//创建时间
            asplIoPool.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间

            lists.stream().forEach(e -> {
                AsplIoPoolDetails asplIoPoolDetails = new AsplIoPoolDetails();
                asplIoPoolDetails.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
                asplIoPoolDetails.setSerno(serno);//业务流水号
                asplIoPoolDetails.setContNo(contNo);//合同编号
                asplIoPoolDetails.setAssetNo(e.getAssetNo());//资产编号
                asplIoPoolDetails.setAssetType(e.getAssetType());//资产类型
                asplIoPoolDetails.setAssetValue(e.getAssetValue());//资产价值
                asplIoPoolDetails.setAssetEndDate(e.getAssetEndDate());//资产到期日
                asplIoPoolDetails.setAssetStatus(e.getAssetStatus());//资产状态
                asplIoPoolDetails.setIsPool(CmisCommonConstants.INOUT_TYPE_1);//是否入池
                asplIoPoolDetails.setIsPledge(CommonConstance.STD_ZB_YES_NO_1);// 默认质押
                asplIoPoolDetails.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                asplIoPoolDetails.setInputId(ctrAsplDetails.getInputId());//登记人
                asplIoPoolDetails.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                asplIoPoolDetails.setInputDate(openDay);//登记日期
                asplIoPoolDetails.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                asplIoPoolDetails.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
                asplIoPoolDetails.setUpdDate(openDay);//最近修改日期
                asplIoPoolDetailsList.add(asplIoPoolDetails);

                AsplAssetsList assetsList = new AsplAssetsList();
                assetsList.setPkId(UUID.randomUUID().toString().replace("-",""));
                assetsList.setContNo(contNo);//合同编号
                assetsList.setAssetNo(e.getAssetNo());//资产编号
                assetsList.setAssetType(e.getAssetType());//资产类型
                assetsList.setAssetValue(e.getAssetValue());//资产价值
                assetsList.setAssetEndDate(e.getAssetEndDate());//资产到期日
                assetsList.setAssetStatus(e.getAssetStatus());//资产状态
                assetsList.setAssteSour(e.getAssetSour());//资产来源
                assetsList.setIsPool(CmisCommonConstants.INOUT_TYPE_1);//是否入池（默认入池）
                assetsList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                assetsList.setIsPledge(e.getIsPledge());//是否入池质押
                assetsList.setInpTime(openDay);//入池时间
                assetsList.setAorgNo(e.getAcptBankId());//行号
                assetsList.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                assetsList.setInputId(ctrAsplDetails.getInputId());//登记人
                assetsList.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                assetsList.setInputDate(openDay);//登记日期
                assetsList.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                assetsList.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
                assetsList.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
                assetsList.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
                assetsList.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
                assetsList.setCreateTime(ctrAsplDetails.getCreateTime());//创建时间
                assetsList.setUpdateTime(ctrAsplDetails.getUpdateTime());//修改时间
                assetsList.setGuarNo(e.getGuarNo());// 统一押品编号
                asplAssetsListList.add(assetsList);
            });
            java.util.List<AsplAssetsList> asplAssetsListListCopy = asplAssetsListList;

            // 资产出入池表 资产出入池明细表 中新增该记录
            logger.info("资产出入池表记录新增开始");
            asplIoPoolService.insertSelective(asplIoPool);
            asplIoPoolDetailsService.insertAsplIoPoolDetailsList(asplIoPoolDetailsList);

            logger.info("资产池内清单更新开始");
            asplAssetsListListCopy.stream().filter(e -> {
                return updateAssetNoList.contains(e.getAssetNo());
            }).forEach(f -> {
                asplAssetsListService.updateAsplAssets(f);
            });
            // 需要新增的资产
            assetNoList.removeAll(updateAssetNoList);
            asplAssetsListListCopy = asplAssetsListListCopy.stream().filter(e -> {
                return assetNoList.contains(e.getAssetNo());
            }).collect(Collectors.toList());
            // 资产清单（新增）
            if(CollectionUtils.nonEmpty(asplAssetsListListCopy)){
                asplAssetsListService.insertAsplAssetsList(asplAssetsListListCopy);
            }
            // 获取已经存在 押品信息的 统一押品编号 集合(需要更新的)
            if (CollectionUtils.nonEmpty(guarNoList)){
                java.util.List<String> alreadyGuarNoList = guarBaseInfoService.selectByGuarNoKList(guarNoList);
                guarNoList.removeAll(alreadyGuarNoList);
                // 1、需要更新的押品（根据押品编号查询押品表中是否存在该押品）
                logger.info("押品更新开始");
            }
            // 2、需要新增的押品
            logger.info("押品新增开始");
            java.util.List<GuarBaseInfo> addGuarBaseInfos = IsPledgeAsplAssetsListList.stream()
                    .filter(f->guarNoList.contains(f.getGuarNo()))
                    .map(e->{
                        GuarBaseInfo guarBaseInfo = new GuarBaseInfo();
                        guarBaseInfo.setGuarNo(e.getGuarNo());// 同一押品编号
                        guarBaseInfo.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>()));// 申请流水号
                        guarBaseInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_997);// 审批状态
                        guarBaseInfo.setGrtFlag("02");// 押品类型(质押)
                        guarBaseInfo.setPldimnMemo(e.getGuarName());// 抵质押物名称
                        guarBaseInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                        guarBaseInfo.setInputId(ctrAsplDetails.getInputId());//登记人
                        guarBaseInfo.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                        guarBaseInfo.setInputDate(openDay);//登记日期
                        guarBaseInfo.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                        guarBaseInfo.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
                        guarBaseInfo.setUpdDate(ctrAsplDetails.getUpdDate());//最近修改日期
                        guarBaseInfo.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
                        guarBaseInfo.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
                        guarBaseInfo.setCurType("CNY");// 一期默认 人民币
                        return guarBaseInfo;
                    }).collect(Collectors.toList());
            if (CollectionUtils.nonEmpty(addGuarBaseInfos)){
                if(!guarBaseInfoService.insertGuarBaseInfoList(addGuarBaseInfos)){
                    logger.info("押品新增失败");
                    xdzc0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdzc0005DataRespDto.setOpMsg("押品新增失败");
                    return xdzc0005DataRespDto;
                }else{
                    logger.info("押品新增成功");
                }
            }

            logger.info("资产池内清单更新开始");
            // 获取该协议下流水号
            QueryModel model = new QueryModel();
            // 根据流水号查询
            model.addCondition("serno",ctrAsplDetails.getSerno());
            java.util.List<GrtGuarBizRstRel> GrtGuarBizRstRelList = grtGuarBizRstRelService.selectByModel(model);
            GrtGuarBizRstRel grtGuarBizRstRel = null;
            // 资产池关联的担保合同只有一个
            if(CollectionUtils.nonEmpty(GrtGuarBizRstRelList)){
                grtGuarBizRstRel = GrtGuarBizRstRelList.get(0);
            }else{
                logger.info("未查询到对应的业务与担保合同的关系，业务流水："+ctrAsplDetails.getSerno());
                xdzc0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                xdzc0005DataRespDto.setOpMsg("未查询到对应的业务与担保合同的关系，业务流水："+ctrAsplDetails.getSerno());
                return xdzc0005DataRespDto;//终止foreach的循环
            }
            // 担保合同编号
            String guarContNo = grtGuarBizRstRel.getGuarContNo();
            //
            // 担保合同挂押品
            guarCopyNoList.stream().forEach(e->{
                GrtGuarContRel grtGuarContRel = new GrtGuarContRel();
                grtGuarContRel.setPkId(UUID.randomUUID().toString().replace("-", ""));
                grtGuarContRel.setGuarContNo(guarContNo);// 担保合同编号
                grtGuarContRel.setGuarNo(e);// 押品统一编号
                grtGuarContRel.setContNo(contNo);// 合同编号
                grtGuarContRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
                grtGuarContRel.setInputId(ctrAsplDetails.getInputId());//登记人
                grtGuarContRel.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                grtGuarContRel.setInputDate(openDay);//登记日期
                grtGuarContRel.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                grtGuarContRel.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
                grtGuarContRel.setUpdDate(openDay);//最近修改日期
                grtGuarContRel.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
                grtGuarContRel.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
                grtGuarContRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                grtGuarContRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));

                if(grtGuarContRelService.insertSelective(grtGuarContRel)<1){//小于500条没关系
                    xdzc0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdzc0005DataRespDto.setOpMsg("入池质押引入押品关系失败");
                    return;//终止foreach的循环
                }
            });
            // 入库 权证新增(cetinf) 押品入库(certis)
            // 1、权证新增(cetinf)
            logger.info("权证新增(cetinf)");
            CetinfReqDto cetinfReqDto = new CetinfReqDto();
            java.util.List<CetinfListInfo> cetinfList = guarCopyNoList.stream().map(e->{
                CetinfListInfo cetinfListInfo = new CetinfListInfo();
                cetinfListInfo.setYptybh(e);
                return cetinfListInfo;
            }).collect(Collectors.toList());
            cetinfReqDto.setList(cetinfList); // 押品统一编号
            cetinfReqDto.setDbhtbh(guarContNo);// 担保合同编号
            cetinfReqDto.setSernoy(grpNo);// 核心担保编号(自己生成的)
            cetinfReqDto.setDyswbs("01");// 抵押顺位标识
            cetinfReqDto.setQzlxyp("13");// 权证类型
            cetinfReqDto.setQlpzhm(grpNo);// 权利凭证号
            cetinfReqDto.setQzfzjg("苏州不动产登记中心");// 权证发证机关名称
            cetinfReqDto.setQzffrq(openDay);// 权证发证日期
            cetinfReqDto.setQzdqrq(lists.get(0).getAssetEndDate());// 权证到期日期
            cetinfReqDto.setQljeyp(String.valueOf(lists.get(0).getAssetValue()));// 权利金额
            cetinfReqDto.setQzztyp("10006");// 权证状态
            cetinfReqDto.setQzbzxx("资产池入池质押");// 权证备注信息
            cetinfReqDto.setDjrmyp(ctrAsplDetails.getInputId());// 登记人
            cetinfReqDto.setDjjgyp(ctrAsplDetails.getInputId());// 登记机构
            cetinfReqDto.setDjrqyp(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));// 登记日期
            cetinfReqDto.setOperat("05"); // 操作
            // 发送cetinf接口
            ResultDto<CetinfRespDto> cetinfResultDto = dscms2YpxtClientService.cetinf(cetinfReqDto);
            if(!"0".equals(cetinfResultDto.getCode())){
                //失败直接返回
                xdzc0005DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdzc0005DataRespDto.setOpMsg(cetinfResultDto.getMessage());
                return xdzc0005DataRespDto;
            }

            // 2、押品入库(certis)
            logger.info("押品入库(certis)");
            CertisReqDto certisReqDto = new CertisReqDto();
            java.util.List<CertisListInfo> certisListInfoList = IsPledgeAsplAssetsListList.stream().map(e->{
                CertisListInfo certisListInfo = new CertisListInfo();
                certisListInfo.setSernoy(grpNo);//核心担保编号
                certisListInfo.setQlpzhm(e.getGuarNo());//权利凭证号 (统一押品编号)
                // todo 权证类型 40 存单(02)  06 国内信用证(04)  64 国外信用证(03 目前不考虑)
                if("02".equals(e.getAssetType())){
                    // 本行存单(电子)
                    certisListInfo.setQzlxyp("40");
                }else if("04".equals(e.getAssetType())){
                    // 国内信用证
                    certisListInfo.setQzlxyp("06");
                }
                certisListInfo.setQzztyp("10006");//权证状态
                certisListInfo.setQzrkrq(openDay);//权证入库日期                                   状态为10006-已入库时必输
                certisListInfo.setQzckrq(StringUtil.EMPTY_STRING);//权证正常出库日期（ 核心时间）      状态为10008-正常出库时必输
                certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称             状态为10019-已借阅时，必输
                certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期                  状态为10019-已借阅时，必输
                certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期                   状态为10019-已借阅时，必输
                certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期               状态为10020-外借归还是，必输
                certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因                   状态为10019-已借阅时，必输
                certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
                return certisListInfo;
            }).collect(Collectors.toList());
            certisReqDto.setList(certisListInfoList);
            ResultDto<CertisRespDto> certisResultDto =  dscms2YpxtClientService.certis(certisReqDto);
            if(!"0".equals(certisResultDto.getCode())){
                xdzc0005DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdzc0005DataRespDto.setOpMsg(certisResultDto.getMessage());
                return xdzc0005DataRespDto;
            }

        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value);
        return xdzc0005DataRespDto;
    }
}