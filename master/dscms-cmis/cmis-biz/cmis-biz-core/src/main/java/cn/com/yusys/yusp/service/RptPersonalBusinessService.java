/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpCusLsnpInfo;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptPersonalBusiness;
import cn.com.yusys.yusp.repository.mapper.RptPersonalBusinessMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptPersonalBusinessService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:13:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptPersonalBusinessService {

    @Autowired
    private RptPersonalBusinessMapper rptPersonalBusinessMapper;
	@Autowired
    private CmisCusClientService cmisCusClientService;
	@Autowired
    private IqpCusLsnpInfoService iqpCusLsnpInfoService;
	@Autowired
    private ICusClientService iCusClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RptPersonalBusiness selectByPrimaryKey(String serno) {
        return rptPersonalBusinessMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptPersonalBusiness> selectAll(QueryModel model) {
        List<RptPersonalBusiness> records = (List<RptPersonalBusiness>) rptPersonalBusinessMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RptPersonalBusiness> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptPersonalBusiness> list = rptPersonalBusinessMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RptPersonalBusiness record) {
        return rptPersonalBusinessMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RptPersonalBusiness record) {
        return rptPersonalBusinessMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RptPersonalBusiness record) {
        return rptPersonalBusinessMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RptPersonalBusiness record) {
        return rptPersonalBusinessMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptPersonalBusinessMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptPersonalBusinessMapper.deleteByIds(ids);
    }

    /**
     * 初始化数据
     * @param map
     * @return
     */
    public RptPersonalBusiness initPersonal(Map map){
        String serno = map.get("serno").toString();
        String cusId = map.get("cusId").toString();
        RptPersonalBusiness rptPersonalBusiness = selectByPrimaryKey(serno);
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusId);
        if(Objects.nonNull(rptPersonalBusiness)){
            return rptPersonalBusiness;
        }else{
            RptPersonalBusiness result = new RptPersonalBusiness();
            ResultDto<CusIndivDto> cusIndivDtoResultDto = cmisCusClientService.queryCusindivByCusid(cusId);
            CusIndivContactDto cusIndivContactDto = iCusClientService.queryCusIndivByCusId(cusId);
            String phone = "";
            String indivRsdAddr = "";
            if(Objects.nonNull(cusIndivContactDto)){
                phone = cusIndivContactDto.getMobileA();
                indivRsdAddr = cusIndivContactDto.getIndivRsdAddr();
            }
            if(cusIndivDtoResultDto!=null&&cusIndivDtoResultDto.getData()!=null){
                CusIndivDto data = cusIndivDtoResultDto.getData();
                result.setSerno(serno);
                result.setBorrCusName(data.getCusName());
                result.setPhone(phone);
                //信用评级
                result.setCreditRate("");
                //出生年月
                result.setDtOfBirth(data.getIndivDtOfBirth());
                //婚姻状况
                result.setMarStatus(data.getMarStatus());
                //户籍所在地
                result.setPermanentAddr(data.getIndivHouhRegAdd());
                //实际居住地
                result.setRealLivingAddr(indivRsdAddr);
                //配偶姓名、工作单位 暂无

                //子女姓名、工作单位 暂无
                int count = rptPersonalBusinessMapper.insert(result);
                if(count>0){
                    return result;
                }
            }
        }
        return null;
    }
}
