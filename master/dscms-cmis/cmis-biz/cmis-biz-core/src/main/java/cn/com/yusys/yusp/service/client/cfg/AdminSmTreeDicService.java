package cn.com.yusys.yusp.service.client.cfg;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author wzy
 * @version 1.0.0se
 * @date 2021-09-01
 * @desc 产品相关类
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class AdminSmTreeDicService {
    private Logger logger = LoggerFactory.getLogger(AdminSmTreeDicService.class);

    @Autowired
    DscmsCfgClientService dscmsCfgClientService;

    /**
     * @param record
     * @return cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto
     * @author wzy
     * @date 2021/9/1 20:26
     * @version 1.0.0
     * @desc 根据树形字典项码值查询中文翻译
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public AdminSmTreeDicDto queryCfgTreeByCode(AdminSmTreeDicDto record) {
        logger.info("根据产品代码【{}】查询树形字典信息开始");
        ResultDto<AdminSmTreeDicDto> adminSmTreeDicDto = null;
        AdminSmTreeDicDto returnRecord=null;
        try {
            adminSmTreeDicDto = dscmsCfgClientService.queryCfgTreeByCode(record);

            // 判断产品条件
            if (Objects.isNull(adminSmTreeDicDto) && Objects.isNull(adminSmTreeDicDto.getData())) {
                logger.info("根据产品代码【{}】, 未查询到树形字典信息");
                throw BizException.error(null, null, "未查询到产品信息！");
            }else{
                returnRecord = adminSmTreeDicDto.getData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("根据产品代码【{}】查询产品信息开始");
        return returnRecord;
    }

}
