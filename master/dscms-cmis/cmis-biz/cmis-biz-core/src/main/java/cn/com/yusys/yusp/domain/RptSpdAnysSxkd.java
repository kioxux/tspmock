/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysSxkd
 * @类描述: rpt_spd_anys_sxkd数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-23 16:15:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_sxkd")
public class RptSpdAnysSxkd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 主办客户经理编号 **/
	@Column(name = "MAIN_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String mainManagerId;
	
	/** 协办客户经理编号 **/
	@Column(name = "ASSIST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String assistManagerId;
	
	/** 企业征信信用情况简要情况描述 **/
	@Column(name = "PFK_KSXD_1_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd1Desc;
	
	/** 企业征信信用情况主办客户经理评分 **/
	@Column(name = "PFK_KSXD_1_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd1Grade1;
	
	/** 企业征信信用情况协办客户经理评分 **/
	@Column(name = "PFK_KSXD_1_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd1Grade2;
	
	/** 实际控制人征信信用情况简要情况描述 **/
	@Column(name = "PFK_KSXD_2_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd2Desc;
	
	/** 实际控制人征信信用情况主办客户经理评分 **/
	@Column(name = "PFK_KSXD_2_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd2Grade1;
	
	/** 实际控制人征信信用情况协办客户经理评分 **/
	@Column(name = "PFK_KSXD_2_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd2Grade2;
	
	/** 企业及实际控制人负债与资产比例简要情况描述 **/
	@Column(name = "PFK_KSXD_3_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd3Desc;
	
	/** 企业及实际控制人负债与资产比例主办客户经理评分 **/
	@Column(name = "PFK_KSXD_3_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd3Grade1;
	
	/** 企业及实际控制人负债与资产比例协办客户经理评分 **/
	@Column(name = "PFK_KSXD_3_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd3Grade2;
	
	/** 实际控制人从事本行业年限简要情况描述 **/
	@Column(name = "PFK_KSXD_4_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd4Desc;
	
	/** 实际控制人从事本行业年限主办客户经理评分 **/
	@Column(name = "PFK_KSXD_4_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd4Grade1;
	
	/** 实际控制人从事本行业年限协办客户经理评分 **/
	@Column(name = "PFK_KSXD_4_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd4Grade2;
	
	/** 企业所属行业简要情况描述 **/
	@Column(name = "PFK_KSXD_5_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd5Desc;
	
	/** 企业所属行业主办客户经理评分 **/
	@Column(name = "PFK_KSXD_5_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd5Grade1;
	
	/** 企业所属行业协办客户经理评分 **/
	@Column(name = "PFK_KSXD_5_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd5Grade2;
	
	/** 企业经营状况简要情况描述 **/
	@Column(name = "PFK_KSXD_6_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd6Desc;
	
	/** 企业经营状况主办客户经理评分 **/
	@Column(name = "PFK_KSXD_6_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd6Grade1;
	
	/** 企业经营状况协办客户经理评分 **/
	@Column(name = "PFK_KSXD_6_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd6Grade2;
	
	/** 企业上下游合作情况简要情况描述 **/
	@Column(name = "PFK_KSXD_7_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd7Desc;
	
	/** 企业上下游合作情况主办客户经理评分 **/
	@Column(name = "PFK_KSXD_7_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd7Grade1;
	
	/** 企业上下游合作情况协办客户经理评分 **/
	@Column(name = "PFK_KSXD_7_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd7Grade2;
	
	/** 申请金额与抵押物价值的比率简要情况描述 **/
	@Column(name = "PFK_KSXD_8_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd8Desc;
	
	/** 申请金额与抵押物价值的比率主办客户经理评分 **/
	@Column(name = "PFK_KSXD_8_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd8Grade1;
	
	/** 申请金额与抵押物价值的比率协办客户经理评分 **/
	@Column(name = "PFK_KSXD_8_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd8Grade2;
	
	/** 抵押物分类简要情况描述 **/
	@Column(name = "PFK_KSXD_9_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd9Desc;
	
	/** 抵押物分类主办客户经理评分 **/
	@Column(name = "PFK_KSXD_9_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd9Grade1;
	
	/** 抵押物分类协办客户经理评分 **/
	@Column(name = "PFK_KSXD_9_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd9Grade2;
	
	/** 抵押物处置能力简要情况描述 **/
	@Column(name = "PFK_KSXD_10_DESC", unique = false, nullable = true, length = 500)
	private String pfkKsxd10Desc;
	
	/** 抵押物处置能力主办客户经理评分 **/
	@Column(name = "PFK_KSXD_10_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd10Grade1;
	
	/** 抵押物处置能力协办客户经理评分 **/
	@Column(name = "PFK_KSXD_10_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkKsxd10Grade2;
	
	/** 企业有无违规排污，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_SXKD_1", unique = false, nullable = true, length = 65535)
	private String focusSxkd1;
	
	/** 企业员工人数是否稳定，员工待遇是否合理 **/
	@Column(name = "FOCUS_SXKD_2", unique = false, nullable = true, length = 65535)
	private String focusSxkd2;
	
	/** 企业有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_SXKD_3", unique = false, nullable = true, length = 65535)
	private String focusSxkd3;
	
	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_SXKD_4", unique = false, nullable = true, length = 65535)
	private String focusSxkd4;
	
	/** 合规经营其它不利情况请简述 **/
	@Column(name = "FOCUS_SXKD_5", unique = false, nullable = true, length = 65535)
	private String focusSxkd5;
	
	/** 实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_SXKD_6", unique = false, nullable = true, length = 65535)
	private String focusSxkd6;
	
	/** 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_SXKD_7", unique = false, nullable = true, length = 65535)
	private String focusSxkd7;
	
	/** 实际控制人是否参与民间融资、投资高风险行业等行为 **/
	@Column(name = "FOCUS_SXKD_8", unique = false, nullable = true, length = 65535)
	private String focusSxkd8;
	
	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_SXKD_9", unique = false, nullable = true, length = 65535)
	private String focusSxkd9;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainManagerId
	 */
	public void setMainManagerId(String mainManagerId) {
		this.mainManagerId = mainManagerId;
	}
	
    /**
     * @return mainManagerId
     */
	public String getMainManagerId() {
		return this.mainManagerId;
	}
	
	/**
	 * @param assistManagerId
	 */
	public void setAssistManagerId(String assistManagerId) {
		this.assistManagerId = assistManagerId;
	}
	
    /**
     * @return assistManagerId
     */
	public String getAssistManagerId() {
		return this.assistManagerId;
	}
	
	/**
	 * @param pfkKsxd1Desc
	 */
	public void setPfkKsxd1Desc(String pfkKsxd1Desc) {
		this.pfkKsxd1Desc = pfkKsxd1Desc;
	}
	
    /**
     * @return pfkKsxd1Desc
     */
	public String getPfkKsxd1Desc() {
		return this.pfkKsxd1Desc;
	}
	
	/**
	 * @param pfkKsxd1Grade1
	 */
	public void setPfkKsxd1Grade1(Integer pfkKsxd1Grade1) {
		this.pfkKsxd1Grade1 = pfkKsxd1Grade1;
	}
	
    /**
     * @return pfkKsxd1Grade1
     */
	public Integer getPfkKsxd1Grade1() {
		return this.pfkKsxd1Grade1;
	}
	
	/**
	 * @param pfkKsxd1Grade2
	 */
	public void setPfkKsxd1Grade2(Integer pfkKsxd1Grade2) {
		this.pfkKsxd1Grade2 = pfkKsxd1Grade2;
	}
	
    /**
     * @return pfkKsxd1Grade2
     */
	public Integer getPfkKsxd1Grade2() {
		return this.pfkKsxd1Grade2;
	}
	
	/**
	 * @param pfkKsxd2Desc
	 */
	public void setPfkKsxd2Desc(String pfkKsxd2Desc) {
		this.pfkKsxd2Desc = pfkKsxd2Desc;
	}
	
    /**
     * @return pfkKsxd2Desc
     */
	public String getPfkKsxd2Desc() {
		return this.pfkKsxd2Desc;
	}
	
	/**
	 * @param pfkKsxd2Grade1
	 */
	public void setPfkKsxd2Grade1(Integer pfkKsxd2Grade1) {
		this.pfkKsxd2Grade1 = pfkKsxd2Grade1;
	}
	
    /**
     * @return pfkKsxd2Grade1
     */
	public Integer getPfkKsxd2Grade1() {
		return this.pfkKsxd2Grade1;
	}
	
	/**
	 * @param pfkKsxd2Grade2
	 */
	public void setPfkKsxd2Grade2(Integer pfkKsxd2Grade2) {
		this.pfkKsxd2Grade2 = pfkKsxd2Grade2;
	}
	
    /**
     * @return pfkKsxd2Grade2
     */
	public Integer getPfkKsxd2Grade2() {
		return this.pfkKsxd2Grade2;
	}
	
	/**
	 * @param pfkKsxd3Desc
	 */
	public void setPfkKsxd3Desc(String pfkKsxd3Desc) {
		this.pfkKsxd3Desc = pfkKsxd3Desc;
	}
	
    /**
     * @return pfkKsxd3Desc
     */
	public String getPfkKsxd3Desc() {
		return this.pfkKsxd3Desc;
	}
	
	/**
	 * @param pfkKsxd3Grade1
	 */
	public void setPfkKsxd3Grade1(Integer pfkKsxd3Grade1) {
		this.pfkKsxd3Grade1 = pfkKsxd3Grade1;
	}
	
    /**
     * @return pfkKsxd3Grade1
     */
	public Integer getPfkKsxd3Grade1() {
		return this.pfkKsxd3Grade1;
	}
	
	/**
	 * @param pfkKsxd3Grade2
	 */
	public void setPfkKsxd3Grade2(Integer pfkKsxd3Grade2) {
		this.pfkKsxd3Grade2 = pfkKsxd3Grade2;
	}
	
    /**
     * @return pfkKsxd3Grade2
     */
	public Integer getPfkKsxd3Grade2() {
		return this.pfkKsxd3Grade2;
	}
	
	/**
	 * @param pfkKsxd4Desc
	 */
	public void setPfkKsxd4Desc(String pfkKsxd4Desc) {
		this.pfkKsxd4Desc = pfkKsxd4Desc;
	}
	
    /**
     * @return pfkKsxd4Desc
     */
	public String getPfkKsxd4Desc() {
		return this.pfkKsxd4Desc;
	}
	
	/**
	 * @param pfkKsxd4Grade1
	 */
	public void setPfkKsxd4Grade1(Integer pfkKsxd4Grade1) {
		this.pfkKsxd4Grade1 = pfkKsxd4Grade1;
	}
	
    /**
     * @return pfkKsxd4Grade1
     */
	public Integer getPfkKsxd4Grade1() {
		return this.pfkKsxd4Grade1;
	}
	
	/**
	 * @param pfkKsxd4Grade2
	 */
	public void setPfkKsxd4Grade2(Integer pfkKsxd4Grade2) {
		this.pfkKsxd4Grade2 = pfkKsxd4Grade2;
	}
	
    /**
     * @return pfkKsxd4Grade2
     */
	public Integer getPfkKsxd4Grade2() {
		return this.pfkKsxd4Grade2;
	}
	
	/**
	 * @param pfkKsxd5Desc
	 */
	public void setPfkKsxd5Desc(String pfkKsxd5Desc) {
		this.pfkKsxd5Desc = pfkKsxd5Desc;
	}
	
    /**
     * @return pfkKsxd5Desc
     */
	public String getPfkKsxd5Desc() {
		return this.pfkKsxd5Desc;
	}
	
	/**
	 * @param pfkKsxd5Grade1
	 */
	public void setPfkKsxd5Grade1(Integer pfkKsxd5Grade1) {
		this.pfkKsxd5Grade1 = pfkKsxd5Grade1;
	}
	
    /**
     * @return pfkKsxd5Grade1
     */
	public Integer getPfkKsxd5Grade1() {
		return this.pfkKsxd5Grade1;
	}
	
	/**
	 * @param pfkKsxd5Grade2
	 */
	public void setPfkKsxd5Grade2(Integer pfkKsxd5Grade2) {
		this.pfkKsxd5Grade2 = pfkKsxd5Grade2;
	}
	
    /**
     * @return pfkKsxd5Grade2
     */
	public Integer getPfkKsxd5Grade2() {
		return this.pfkKsxd5Grade2;
	}
	
	/**
	 * @param pfkKsxd6Desc
	 */
	public void setPfkKsxd6Desc(String pfkKsxd6Desc) {
		this.pfkKsxd6Desc = pfkKsxd6Desc;
	}
	
    /**
     * @return pfkKsxd6Desc
     */
	public String getPfkKsxd6Desc() {
		return this.pfkKsxd6Desc;
	}
	
	/**
	 * @param pfkKsxd6Grade1
	 */
	public void setPfkKsxd6Grade1(Integer pfkKsxd6Grade1) {
		this.pfkKsxd6Grade1 = pfkKsxd6Grade1;
	}
	
    /**
     * @return pfkKsxd6Grade1
     */
	public Integer getPfkKsxd6Grade1() {
		return this.pfkKsxd6Grade1;
	}
	
	/**
	 * @param pfkKsxd6Grade2
	 */
	public void setPfkKsxd6Grade2(Integer pfkKsxd6Grade2) {
		this.pfkKsxd6Grade2 = pfkKsxd6Grade2;
	}
	
    /**
     * @return pfkKsxd6Grade2
     */
	public Integer getPfkKsxd6Grade2() {
		return this.pfkKsxd6Grade2;
	}
	
	/**
	 * @param pfkKsxd7Desc
	 */
	public void setPfkKsxd7Desc(String pfkKsxd7Desc) {
		this.pfkKsxd7Desc = pfkKsxd7Desc;
	}
	
    /**
     * @return pfkKsxd7Desc
     */
	public String getPfkKsxd7Desc() {
		return this.pfkKsxd7Desc;
	}
	
	/**
	 * @param pfkKsxd7Grade1
	 */
	public void setPfkKsxd7Grade1(Integer pfkKsxd7Grade1) {
		this.pfkKsxd7Grade1 = pfkKsxd7Grade1;
	}
	
    /**
     * @return pfkKsxd7Grade1
     */
	public Integer getPfkKsxd7Grade1() {
		return this.pfkKsxd7Grade1;
	}
	
	/**
	 * @param pfkKsxd7Grade2
	 */
	public void setPfkKsxd7Grade2(Integer pfkKsxd7Grade2) {
		this.pfkKsxd7Grade2 = pfkKsxd7Grade2;
	}
	
    /**
     * @return pfkKsxd7Grade2
     */
	public Integer getPfkKsxd7Grade2() {
		return this.pfkKsxd7Grade2;
	}
	
	/**
	 * @param pfkKsxd8Desc
	 */
	public void setPfkKsxd8Desc(String pfkKsxd8Desc) {
		this.pfkKsxd8Desc = pfkKsxd8Desc;
	}
	
    /**
     * @return pfkKsxd8Desc
     */
	public String getPfkKsxd8Desc() {
		return this.pfkKsxd8Desc;
	}
	
	/**
	 * @param pfkKsxd8Grade1
	 */
	public void setPfkKsxd8Grade1(Integer pfkKsxd8Grade1) {
		this.pfkKsxd8Grade1 = pfkKsxd8Grade1;
	}
	
    /**
     * @return pfkKsxd8Grade1
     */
	public Integer getPfkKsxd8Grade1() {
		return this.pfkKsxd8Grade1;
	}
	
	/**
	 * @param pfkKsxd8Grade2
	 */
	public void setPfkKsxd8Grade2(Integer pfkKsxd8Grade2) {
		this.pfkKsxd8Grade2 = pfkKsxd8Grade2;
	}
	
    /**
     * @return pfkKsxd8Grade2
     */
	public Integer getPfkKsxd8Grade2() {
		return this.pfkKsxd8Grade2;
	}
	
	/**
	 * @param pfkKsxd9Desc
	 */
	public void setPfkKsxd9Desc(String pfkKsxd9Desc) {
		this.pfkKsxd9Desc = pfkKsxd9Desc;
	}
	
    /**
     * @return pfkKsxd9Desc
     */
	public String getPfkKsxd9Desc() {
		return this.pfkKsxd9Desc;
	}
	
	/**
	 * @param pfkKsxd9Grade1
	 */
	public void setPfkKsxd9Grade1(Integer pfkKsxd9Grade1) {
		this.pfkKsxd9Grade1 = pfkKsxd9Grade1;
	}
	
    /**
     * @return pfkKsxd9Grade1
     */
	public Integer getPfkKsxd9Grade1() {
		return this.pfkKsxd9Grade1;
	}
	
	/**
	 * @param pfkKsxd9Grade2
	 */
	public void setPfkKsxd9Grade2(Integer pfkKsxd9Grade2) {
		this.pfkKsxd9Grade2 = pfkKsxd9Grade2;
	}
	
    /**
     * @return pfkKsxd9Grade2
     */
	public Integer getPfkKsxd9Grade2() {
		return this.pfkKsxd9Grade2;
	}
	
	/**
	 * @param pfkKsxd10Desc
	 */
	public void setPfkKsxd10Desc(String pfkKsxd10Desc) {
		this.pfkKsxd10Desc = pfkKsxd10Desc;
	}
	
    /**
     * @return pfkKsxd10Desc
     */
	public String getPfkKsxd10Desc() {
		return this.pfkKsxd10Desc;
	}
	
	/**
	 * @param pfkKsxd10Grade1
	 */
	public void setPfkKsxd10Grade1(Integer pfkKsxd10Grade1) {
		this.pfkKsxd10Grade1 = pfkKsxd10Grade1;
	}
	
    /**
     * @return pfkKsxd10Grade1
     */
	public Integer getPfkKsxd10Grade1() {
		return this.pfkKsxd10Grade1;
	}
	
	/**
	 * @param pfkKsxd10Grade2
	 */
	public void setPfkKsxd10Grade2(Integer pfkKsxd10Grade2) {
		this.pfkKsxd10Grade2 = pfkKsxd10Grade2;
	}
	
    /**
     * @return pfkKsxd10Grade2
     */
	public Integer getPfkKsxd10Grade2() {
		return this.pfkKsxd10Grade2;
	}
	
	/**
	 * @param focusSxkd1
	 */
	public void setFocusSxkd1(String focusSxkd1) {
		this.focusSxkd1 = focusSxkd1;
	}
	
    /**
     * @return focusSxkd1
     */
	public String getFocusSxkd1() {
		return this.focusSxkd1;
	}
	
	/**
	 * @param focusSxkd2
	 */
	public void setFocusSxkd2(String focusSxkd2) {
		this.focusSxkd2 = focusSxkd2;
	}
	
    /**
     * @return focusSxkd2
     */
	public String getFocusSxkd2() {
		return this.focusSxkd2;
	}
	
	/**
	 * @param focusSxkd3
	 */
	public void setFocusSxkd3(String focusSxkd3) {
		this.focusSxkd3 = focusSxkd3;
	}
	
    /**
     * @return focusSxkd3
     */
	public String getFocusSxkd3() {
		return this.focusSxkd3;
	}
	
	/**
	 * @param focusSxkd4
	 */
	public void setFocusSxkd4(String focusSxkd4) {
		this.focusSxkd4 = focusSxkd4;
	}
	
    /**
     * @return focusSxkd4
     */
	public String getFocusSxkd4() {
		return this.focusSxkd4;
	}
	
	/**
	 * @param focusSxkd5
	 */
	public void setFocusSxkd5(String focusSxkd5) {
		this.focusSxkd5 = focusSxkd5;
	}
	
    /**
     * @return focusSxkd5
     */
	public String getFocusSxkd5() {
		return this.focusSxkd5;
	}
	
	/**
	 * @param focusSxkd6
	 */
	public void setFocusSxkd6(String focusSxkd6) {
		this.focusSxkd6 = focusSxkd6;
	}
	
    /**
     * @return focusSxkd6
     */
	public String getFocusSxkd6() {
		return this.focusSxkd6;
	}
	
	/**
	 * @param focusSxkd7
	 */
	public void setFocusSxkd7(String focusSxkd7) {
		this.focusSxkd7 = focusSxkd7;
	}
	
    /**
     * @return focusSxkd7
     */
	public String getFocusSxkd7() {
		return this.focusSxkd7;
	}
	
	/**
	 * @param focusSxkd8
	 */
	public void setFocusSxkd8(String focusSxkd8) {
		this.focusSxkd8 = focusSxkd8;
	}
	
    /**
     * @return focusSxkd8
     */
	public String getFocusSxkd8() {
		return this.focusSxkd8;
	}
	
	/**
	 * @param focusSxkd9
	 */
	public void setFocusSxkd9(String focusSxkd9) {
		this.focusSxkd9 = focusSxkd9;
	}
	
    /**
     * @return focusSxkd9
     */
	public String getFocusSxkd9() {
		return this.focusSxkd9;
	}


}