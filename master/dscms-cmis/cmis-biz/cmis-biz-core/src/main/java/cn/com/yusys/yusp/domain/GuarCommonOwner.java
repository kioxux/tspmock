/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarCommonOwner
 * @类描述: guar_common_owner数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-04 14:29:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_common_owner")
public class GuarCommonOwner extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 押品编号 **/
	@Id
	@Column(name = "GUAR_NO")
	private String guarNo;
	
	/** 共有人客户编号 **/
	@Column(name = "COMMON_OWNER_ID", unique = false, nullable = true, length = 40)
	private String commonOwnerId;
	
	/** 共有人客户名称 **/
	@Column(name = "COMMON_OWNER_NAME", unique = false, nullable = true, length = 200)
	private String commonOwnerName;
	
	/** 共有人客户类型 **/
	@Column(name = "COMMON_OWNER_TYPE", unique = false, nullable = true, length = 20)
	private String commonOwnerType;
	
	/** 共有人证件类型 **/
	@Column(name = "COMMON_CERT_TYPE", unique = false, nullable = true, length = 20)
	private String commonCertType;
	
	/** 共有人证件号码 **/
	@Column(name = "COMMON_CERT_CODE", unique = false, nullable = true, length = 40)
	private String commonCertCode;
	
	/** 共有人所占份额 **/
	@Column(name = "COMMON_HOLD_PORTIO", unique = false, nullable = true, length = 6)
	private java.math.BigDecimal commonHoldPortio;
	
	/** 房产证（共有权证）号码 **/
	@Column(name = "COMMON_HOUSE_LAND_NO", unique = false, nullable = true, length = 100)
	private String commonHouseLandNo;
	
	/** 土地证（共有权证）号码 **/
	@Column(name = "COMMON_LAND_NO", unique = false, nullable = true, length = 100)
	private String commonLandNo;
	
	/** 共有人类型 **/
	@Column(name = "SHARE_OWNER_TYPE", unique = false, nullable = true, length = 10)
	private String shareOwnerType;
	
	/** 共有人地址 **/
	@Column(name = "COMMON_ADDR", unique = false, nullable = true, length = 500)
	private String commonAddr;
	
	/** 共有人电话 **/
	@Column(name = "COMMON_TEL", unique = false, nullable = true, length = 20)
	private String commonTel;
	
	/** 不动产（房产证号） **/
	@Column(name = "HOUSE_NO", unique = false, nullable = true, length = 200)
	private String houseNo;
	
	/** 共有人QQ号码 **/
	@Column(name = "COMMON_QQ", unique = false, nullable = true, length = 30)
	private String commonQq;
	
	/** 共有人微信号码 **/
	@Column(name = "COMMON_WXIN", unique = false, nullable = true, length = 200)
	private String commonWxin;
	
	/** 共有人电子邮箱 **/
	@Column(name = "COMMON_MAIL", unique = false, nullable = true, length = 200)
	private String commonMail;
	
	/** 联系人 **/
	@Column(name = "LEGAL_NAME", unique = false, nullable = true, length = 200)
	private String legalName;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param commonOwnerId
	 */
	public void setCommonOwnerId(String commonOwnerId) {
		this.commonOwnerId = commonOwnerId;
	}
	
    /**
     * @return commonOwnerId
     */
	public String getCommonOwnerId() {
		return this.commonOwnerId;
	}
	
	/**
	 * @param commonOwnerName
	 */
	public void setCommonOwnerName(String commonOwnerName) {
		this.commonOwnerName = commonOwnerName;
	}
	
    /**
     * @return commonOwnerName
     */
	public String getCommonOwnerName() {
		return this.commonOwnerName;
	}
	
	/**
	 * @param commonOwnerType
	 */
	public void setCommonOwnerType(String commonOwnerType) {
		this.commonOwnerType = commonOwnerType;
	}
	
    /**
     * @return commonOwnerType
     */
	public String getCommonOwnerType() {
		return this.commonOwnerType;
	}
	
	/**
	 * @param commonCertType
	 */
	public void setCommonCertType(String commonCertType) {
		this.commonCertType = commonCertType;
	}
	
    /**
     * @return commonCertType
     */
	public String getCommonCertType() {
		return this.commonCertType;
	}
	
	/**
	 * @param commonCertCode
	 */
	public void setCommonCertCode(String commonCertCode) {
		this.commonCertCode = commonCertCode;
	}
	
    /**
     * @return commonCertCode
     */
	public String getCommonCertCode() {
		return this.commonCertCode;
	}
	
	/**
	 * @param commonHoldPortio
	 */
	public void setCommonHoldPortio(java.math.BigDecimal commonHoldPortio) {
		this.commonHoldPortio = commonHoldPortio;
	}
	
    /**
     * @return commonHoldPortio
     */
	public java.math.BigDecimal getCommonHoldPortio() {
		return this.commonHoldPortio;
	}
	
	/**
	 * @param commonHouseLandNo
	 */
	public void setCommonHouseLandNo(String commonHouseLandNo) {
		this.commonHouseLandNo = commonHouseLandNo;
	}
	
    /**
     * @return commonHouseLandNo
     */
	public String getCommonHouseLandNo() {
		return this.commonHouseLandNo;
	}
	
	/**
	 * @param commonLandNo
	 */
	public void setCommonLandNo(String commonLandNo) {
		this.commonLandNo = commonLandNo;
	}
	
    /**
     * @return commonLandNo
     */
	public String getCommonLandNo() {
		return this.commonLandNo;
	}
	
	/**
	 * @param shareOwnerType
	 */
	public void setShareOwnerType(String shareOwnerType) {
		this.shareOwnerType = shareOwnerType;
	}
	
    /**
     * @return shareOwnerType
     */
	public String getShareOwnerType() {
		return this.shareOwnerType;
	}
	
	/**
	 * @param commonAddr
	 */
	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr;
	}
	
    /**
     * @return commonAddr
     */
	public String getCommonAddr() {
		return this.commonAddr;
	}
	
	/**
	 * @param commonTel
	 */
	public void setCommonTel(String commonTel) {
		this.commonTel = commonTel;
	}
	
    /**
     * @return commonTel
     */
	public String getCommonTel() {
		return this.commonTel;
	}
	
	/**
	 * @param houseNo
	 */
	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}
	
    /**
     * @return houseNo
     */
	public String getHouseNo() {
		return this.houseNo;
	}
	
	/**
	 * @param commonQq
	 */
	public void setCommonQq(String commonQq) {
		this.commonQq = commonQq;
	}
	
    /**
     * @return commonQq
     */
	public String getCommonQq() {
		return this.commonQq;
	}
	
	/**
	 * @param commonWxin
	 */
	public void setCommonWxin(String commonWxin) {
		this.commonWxin = commonWxin;
	}
	
    /**
     * @return commonWxin
     */
	public String getCommonWxin() {
		return this.commonWxin;
	}
	
	/**
	 * @param commonMail
	 */
	public void setCommonMail(String commonMail) {
		this.commonMail = commonMail;
	}
	
    /**
     * @return commonMail
     */
	public String getCommonMail() {
		return this.commonMail;
	}
	
	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}
	
    /**
     * @return legalName
     */
	public String getLegalName() {
		return this.legalName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}