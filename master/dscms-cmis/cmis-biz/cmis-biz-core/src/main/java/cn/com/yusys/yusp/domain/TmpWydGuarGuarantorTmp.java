/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydGuarGuarantor
 * @类描述: tmp_wyd_guar_guarantor数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_guar_guarantor_tmp")
public class TmpWydGuarGuarantorTmp {
	
	/** 担保合同编号 **/
	@Id
	@Column(name = "GUAR_CONTRACT_NO")
	private String guarContractNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 保证人客户号 **/
	@Column(name = "GUARANTOR_CUST_ID", unique = false, nullable = true, length = 32)
	private String guarantorCustId;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 保证或者抵质押物风险缓释类型 **/
	@Column(name = "RISK_TYPE", unique = false, nullable = true, length = 10)
	private String riskType;
	
	/** 保证人客户类型 **/
	@Column(name = "GUARANTOR_TYPE", unique = false, nullable = true, length = 10)
	private String guarantorType;
	
	/** 保证人名称 **/
	@Column(name = "GUARANTOR_NAME", unique = false, nullable = true, length = 80)
	private String guarantorName;
	
	/** 保证人证件类型 **/
	@Column(name = "GUARANTOR_ID_TYPE", unique = false, nullable = true, length = 32)
	private String guarantorIdType;
	
	/** 保证人证件号码 **/
	@Column(name = "GUARANTOR_ID_NO", unique = false, nullable = true, length = 40)
	private String guarantorIdNo;
	
	/** 保证人保证能力上限 **/
	@Column(name = "GUARANTY_VALUE_LIMIT", unique = false, nullable = true, length = 22)
	private String guarantyValueLimit;
	
	/** 保证人净资产 **/
	@Column(name = "GUARANTOR_ASSET", unique = false, nullable = true, length = 22)
	private String guarantorAsset;
	
	/** 保证金额 **/
	@Column(name = "GUARANTY_VALUE", unique = false, nullable = true, length = 22)
	private String guarantyValue;
	
	/** 单位id **/
	@Column(name = "MERCHANT_ID", unique = false, nullable = true, length = 32)
	private String merchantId;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param guarContractNo
	 */
	public void setGuarContractNo(String guarContractNo) {
		this.guarContractNo = guarContractNo;
	}
	
    /**
     * @return guarContractNo
     */
	public String getGuarContractNo() {
		return this.guarContractNo;
	}
	
	/**
	 * @param guarantorCustId
	 */
	public void setGuarantorCustId(String guarantorCustId) {
		this.guarantorCustId = guarantorCustId;
	}
	
    /**
     * @return guarantorCustId
     */
	public String getGuarantorCustId() {
		return this.guarantorCustId;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param riskType
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
    /**
     * @return riskType
     */
	public String getRiskType() {
		return this.riskType;
	}
	
	/**
	 * @param guarantorType
	 */
	public void setGuarantorType(String guarantorType) {
		this.guarantorType = guarantorType;
	}
	
    /**
     * @return guarantorType
     */
	public String getGuarantorType() {
		return this.guarantorType;
	}
	
	/**
	 * @param guarantorName
	 */
	public void setGuarantorName(String guarantorName) {
		this.guarantorName = guarantorName;
	}
	
    /**
     * @return guarantorName
     */
	public String getGuarantorName() {
		return this.guarantorName;
	}
	
	/**
	 * @param guarantorIdType
	 */
	public void setGuarantorIdType(String guarantorIdType) {
		this.guarantorIdType = guarantorIdType;
	}
	
    /**
     * @return guarantorIdType
     */
	public String getGuarantorIdType() {
		return this.guarantorIdType;
	}
	
	/**
	 * @param guarantorIdNo
	 */
	public void setGuarantorIdNo(String guarantorIdNo) {
		this.guarantorIdNo = guarantorIdNo;
	}
	
    /**
     * @return guarantorIdNo
     */
	public String getGuarantorIdNo() {
		return this.guarantorIdNo;
	}
	
	/**
	 * @param guarantyValueLimit
	 */
	public void setGuarantyValueLimit(String guarantyValueLimit) {
		this.guarantyValueLimit = guarantyValueLimit;
	}
	
    /**
     * @return guarantyValueLimit
     */
	public String getGuarantyValueLimit() {
		return this.guarantyValueLimit;
	}
	
	/**
	 * @param guarantorAsset
	 */
	public void setGuarantorAsset(String guarantorAsset) {
		this.guarantorAsset = guarantorAsset;
	}
	
    /**
     * @return guarantorAsset
     */
	public String getGuarantorAsset() {
		return this.guarantorAsset;
	}
	
	/**
	 * @param guarantyValue
	 */
	public void setGuarantyValue(String guarantyValue) {
		this.guarantyValue = guarantyValue;
	}
	
    /**
     * @return guarantyValue
     */
	public String getGuarantyValue() {
		return this.guarantyValue;
	}
	
	/**
	 * @param merchantId
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
    /**
     * @return merchantId
     */
	public String getMerchantId() {
		return this.merchantId;
	}


}