package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038PrdListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.req.CmisLmt0041ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0041.resp.CmisLmt0041RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.resp.CmisLmt0056RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:13:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtReplyAccService.class);

    @Resource
    private LmtReplyAccMapper lmtReplyAccMapper;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private LmtReplySubService lmtReplySubService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtReplyAccNeedSubService lmtReplyAccNeedSubService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtReplyAccLoanCondService lmtReplyAccLoanCondService;

    @Autowired
    private LmtReplyLoanCondService lmtReplyLoanCondService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAcc selectByPrimaryKey(String pkId) {
        return lmtReplyAccMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtReplyAcc> selectAll(QueryModel model) {
        List<LmtReplyAcc> records = (List<LmtReplyAcc>) lmtReplyAccMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplyAcc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAcc> list = lmtReplyAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtReplyAcc record) {
        return lmtReplyAccMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtReplyAcc record) {
        return lmtReplyAccMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtReplyAcc record) {
        return lmtReplyAccMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtReplyAcc record) {
        return lmtReplyAccMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtReplyAccMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: getLastLmtAcc
     * @方法描述: 根据客户号查询客户最新的一笔授信台账，
     * @参数与返回说明:
     * @算法描述: 通过客户号查询授信台账生效日最近的一笔授信台账
     * @创建人: mashun1
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAcc getLastLmtReplyAcc(String cusId) {
        LmtReplyAcc lmtReplyAcc = new LmtReplyAcc();
        try {
            Map paramMap = new HashMap<String, Object>();
            paramMap.put("cusId", cusId);
            paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramMap.put("accStatus", CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            paramMap.put("sort", "input_date desc");
            List<LmtReplyAcc> lmtReplyAccList = this.selectByParams(paramMap);
            if (lmtReplyAccList != null && lmtReplyAccList.size() > 0) {
                lmtReplyAcc = lmtReplyAccList.get(0);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return lmtReplyAcc;
    }


    /**
     * @方法名称: synLmtReplyAccToLmtSys
     * @方法描述: 同步额度台账至额度系统
     * @参数与返回说明:
     * @算法描述: 1.
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void synLmtReplyAccToLmtSys(String cusId) throws Exception {
        CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
        // 查询客户生效的授信批复台账
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("accStatus", CmisLmtConstants.STD_ZB_LMT_STATE_01);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("cusId", cusId);
        List<LmtReplyAcc> lmtReplyAccList = this.selectByParams(queryMap);
        log.info("授信批复台账查询结果为：【{}】,数组大小为【{}】", lmtReplyAccList.toString(), lmtReplyAccList.size());
        String lmtType = "";
        Boolean isExitOrigi = false;
        if (lmtReplyAccList != null && lmtReplyAccList.size() == 1) {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccList.get(0);
            lmtType = lmtReplyAcc.getLmtType();
            // 组装额度系统报文
            cmisLmt0001ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
            cmisLmt0001ReqDto.setInstuCde(CmisCommonUtils.getInstucde(lmtReplyAcc.getManagerBrId()));
            cmisLmt0001ReqDto.setAccNo(lmtReplyAcc.getAccNo());
            cmisLmt0001ReqDto.setCurType(lmtReplyAcc.getCurType());
            cmisLmt0001ReqDto.setCusId(lmtReplyAcc.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtReplyAcc.getCusName());
            log.info("客户接口查询,请求报文[{}]", JSON.toJSONString(cmisLmt0001ReqDto));
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(lmtReplyAcc.getCusId());
            log.info("客户接口查询,响应报文[{}]", JSON.toJSONString(cusBaseClientDto));
            cmisLmt0001ReqDto.setCusType(cusBaseClientDto.getCusCatalog());
            if (CmisCommonConstants.LMT_TYPE_01.equals(lmtType)
                    || CmisCommonConstants.LMT_TYPE_04.equals(lmtType)
                    || CmisCommonConstants.LMT_TYPE_07.equals(lmtType)
                    || CmisCommonConstants.LMT_TYPE_06.equals(lmtType)
                    || CmisCommonConstants.LMT_TYPE_05.equals(lmtType)
                    || CmisCommonConstants.LMT_TYPE_02.equals(lmtType)) {
                cmisLmt0001ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_0);
            } else if (
                    CmisCommonConstants.LMT_TYPE_03.equals(lmtType)) {
                cmisLmt0001ReqDto.setIsCreateAcc(CmisCommonConstants.STD_ZB_YES_NO_1);
                Map<String, String> map = new HashMap<>();
                LmtReply lmtReplyNow = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyAcc.getReplySerno());
                log.info("当前批复信息[{}]", JSON.toJSONString(lmtReplyNow));
                map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                map.put("replySerno", lmtReplyNow.getOrigiLmtReplySerno());
                LmtReplyAcc lmtReplyAccOrigi = lmtReplyAccMapper.selectAccNoDataByParams(map);
                log.info("原批复台账信息[{}]", JSON.toJSONString(lmtReplyAccOrigi));
                cmisLmt0001ReqDto.setOrigiAccNo(lmtReplyAccOrigi.getAccNo());
            }
            cmisLmt0001ReqDto.setLmtAmt(lmtReplyAcc.getOpenTotalLmtAmt().add(lmtReplyAcc.getLowRiskTotalLmtAmt()));
            cmisLmt0001ReqDto.setTerm(lmtReplyAcc.getLmtTerm());
            cmisLmt0001ReqDto.setAccStatus(lmtReplyAcc.getAccStatus());
            cmisLmt0001ReqDto.setManagerId(lmtReplyAcc.getManagerId());
            cmisLmt0001ReqDto.setManagerBrId(lmtReplyAcc.getManagerBrId());
            cmisLmt0001ReqDto.setInputId(lmtReplyAcc.getInputId());
            cmisLmt0001ReqDto.setInputBrId(lmtReplyAcc.getInputBrId());
            cmisLmt0001ReqDto.setInputDate(lmtReplyAcc.getInputDate());
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_APPR_ST_01);
            cmisLmt0001ReqDto.setStartDate(lmtReplyAccSubPrdService.getEarlyDateByAccNo(lmtReplyAcc.getAccNo()));
            cmisLmt0001ReqDto.setEndDate(lmtReplyAccSubPrdService.getLastDateByAccNo(lmtReplyAcc.getAccNo()));
            HashMap<String, String> querySubMap = new HashMap<>();
            querySubMap.put("accNo", lmtReplyAcc.getAccNo());
            querySubMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.queryLmtReplyAccSubByParams(querySubMap);
            log.info("当前批复台账分项信息[{}]", JSON.toJSONString(lmtReplyAccSubList));
            if (lmtReplyAccSubList != null) {
                List<CmisLmt0001LmtSubListReqDto> cmisLmt0001LmtSubListReqDtoList = new ArrayList<CmisLmt0001LmtSubListReqDto>();
                for (LmtReplyAccSub lmtReplyAccSub : lmtReplyAccSubList) {
                    // 获取对应批复中的原台账分项编号
                    CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
                    cmisLmt0001LmtSubListReqDto.setParentId(lmtReplyAcc.getAccNo());
                    cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtReplyAccSub.getAccSubName());
                    cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtReplyAccSub.getAccSubNo());
                    cmisLmt0001LmtSubListReqDto.setAvlamt(lmtReplyAccSub.getLmtAmt());
                    cmisLmt0001LmtSubListReqDto.setOprType(lmtReplyAccSub.getOprType());
                    cmisLmt0001LmtSubListReqDto.setCurType(lmtReplyAccSub.getCurType());
                    cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtReplyAccSub.getGuarMode());
                    cmisLmt0001LmtSubListReqDto.setIsRevolv(lmtReplyAccSub.getIsRevolvLimit());
                    cmisLmt0001LmtSubListReqDto.setIsPreCrd(lmtReplyAccSub.getIsPreLmt());
                    cmisLmt0001LmtSubListReqDto.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSub.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                    cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                    cmisLmt0001LmtSubListReqDto.setLmtSubType("01");
                    Map<String,String> mapQuery = new HashMap<>();
                    mapQuery.put("accSubNo",lmtReplyAccSub.getAccSubNo());
                    mapQuery.put("isRevolvLimit",lmtReplyAccSub.getIsRevolvLimit());
                    String lastDate = lmtReplyAccSubPrdService.getLastEndDate(mapQuery);
                    String earlyDate = lmtReplyAccSubPrdService.getEarlyStartDate(mapQuery);
                    cmisLmt0001LmtSubListReqDto.setStartDate(earlyDate);
                    cmisLmt0001LmtSubListReqDto.setEndDate(lastDate);
                    if (CmisCommonConstants.LMT_TYPE_03.equals(lmtType)) {
                        HashMap<String, String> map = new HashMap<>();
                        LmtReplySub lmtReplySubNow = lmtReplySubService.queryLmtReplySubByReplySubSerno(lmtReplyAccSub.getReplySubSerno());
                        log.info("当前批复分项信息[{}]", JSON.toJSONString(lmtReplySubNow));
                        if (lmtReplySubNow != null && !"".equals(lmtReplySubNow.getOrigiLmtAccSubNo())) {
                            isExitOrigi = true;
                            map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                            map.put("accSubNo", lmtReplySubNow.getOrigiLmtAccSubNo());
                            List<LmtReplyAccSub> LmtReplyAccSubList = lmtReplyAccSubService.queryLmtReplyAccSubByParams(map);
                            log.info("原批复分项信息[{}]", JSON.toJSONString(lmtReplySubNow));
                            if (LmtReplyAccSubList != null && LmtReplyAccSubList.size() == 1) {
                                cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo(LmtReplyAccSubList.get(0).getAccSubNo());
                            }
                        } else {
                            isExitOrigi = false;
                        }
                    }
                    cmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
                    HashMap<String, String> querySubPrdMap = new HashMap<>();
                    querySubPrdMap.put("accSubNo", lmtReplyAccSub.getAccSubNo());
                    querySubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(querySubPrdMap);
                    if (lmtReplyAccSubPrdList != null) {
                        for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                            CmisLmt0001LmtSubListReqDto lmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
                            lmt0001LmtSubListReqDto.setAccSubNo(lmtReplyAccSubPrd.getAccSubPrdNo());
                            lmt0001LmtSubListReqDto.setParentId(cmisLmt0001LmtSubListReqDto.getAccSubNo());
                            lmt0001LmtSubListReqDto.setLimitSubName(lmtReplyAccSubPrd.getLmtBizTypeName());
                            lmt0001LmtSubListReqDto.setIsRevolv(lmtReplyAccSubPrd.getIsRevolvLimit());
                            lmt0001LmtSubListReqDto.setLimitSubNo(lmtReplyAccSubPrd.getLmtBizType());
                            lmt0001LmtSubListReqDto.setAvlamt(lmtReplyAccSubPrd.getLmtAmt());
                            lmt0001LmtSubListReqDto.setCurType(lmtReplyAccSubPrd.getCurType());
                            lmt0001LmtSubListReqDto.setTerm(lmtReplyAccSubPrd.getLmtTerm());
                            lmt0001LmtSubListReqDto.setStartDate(lmtReplyAccSubPrd.getStartDate());
                            lmt0001LmtSubListReqDto.setEndDate(lmtReplyAccSubPrd.getEndDate());
                            lmt0001LmtSubListReqDto.setBailPreRate(lmtReplyAccSubPrd.getBailPreRate());
                            lmt0001LmtSubListReqDto.setRateYear(lmtReplyAccSubPrd.getRateYear());
                            lmt0001LmtSubListReqDto.setSuitGuarWay(lmtReplyAccSubPrd.getGuarMode());
                            lmt0001LmtSubListReqDto.setLmtGraper(lmtReplyAccSubPrd.getLmtGraperTerm());
                            lmt0001LmtSubListReqDto.setIsRevolv(lmtReplyAccSubPrd.getIsRevolvLimit());
                            lmt0001LmtSubListReqDto.setIsPreCrd(lmtReplyAccSubPrd.getIsPreLmt());
                            lmt0001LmtSubListReqDto.setOprType(lmtReplyAccSubPrd.getOprType());
                            lmt0001LmtSubListReqDto.setLmtBizTypeProp(lmtReplyAccSubPrd.getLmtBizTypeProp());
                            lmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                            lmt0001LmtSubListReqDto.setLmtSubType("02");
                            if (isExitOrigi) {
                                LmtReplySubPrd lmtReplySubPrdNow = lmtReplySubPrdService.queryLmtReplySubPrdByReplySubPrdSerno(lmtReplyAccSubPrd.getReplySubPrdSerno());
                                log.info("当前批复分项品种信息[{}]", JSON.toJSONString(lmtReplySubPrdNow));
                                if (lmtReplySubPrdNow != null && !"".equals(lmtReplySubPrdNow.getOrigiLmtAccSubPrdNo())) {
                                    LmtReplyAccSubPrd LmtReplyAccSubPrd = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByAccSubPrdNo(lmtReplySubPrdNow.getOrigiLmtAccSubPrdNo());
                                    log.info("当前批复分项品种对应的原分项品种信息[{}]", JSON.toJSONString(LmtReplyAccSubPrd));
                                    if (LmtReplyAccSubPrd != null) {
                                        lmt0001LmtSubListReqDto.setOrigiAccSubNo(LmtReplyAccSubPrd.getAccSubPrdNo());
                                    }
                                }
                            }
                            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
                            lmt0001LmtSubListReqDto.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyAccSubPrd.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                            cmisLmt0001LmtSubListReqDtoList.add(lmt0001LmtSubListReqDto);
                        }
                    } else {
                        throw new Exception("授信批复台账分项适用品种查询异常");
                    }
                }

                // 获取当前批复对应的申请时删除且原分项编号不为空的的分项
                log.info("获取当前批复对应的申请时删除且原分项编号不为空的的分项----start----");
                HashMap map = new HashMap();
                map.put("serno",lmtReplyAcc.getSerno());
                map.put("isHaveOriSub",CmisCommonConstants.YES_NO_1);
                List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByParams(map);
                log.info("当前批复对应的申请时删除且原分项编号不为空的的分项列表:"+JSON.toJSONString(lmtAppSubList));
                if(CollectionUtils.nonEmpty(lmtAppSubList)){
                    for(LmtAppSub lmtAppSub : lmtAppSubList){
                        if(CmisCommonConstants.OPR_TYPE_DELETE.equals(lmtAppSub.getOprType())){
                            log.info("分项层处理开始:"+JSON.toJSONString(lmtAppSub));
                            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
                            cmisLmt0001LmtSubListReqDto.setParentId(lmtReplyAcc.getAccNo());
                            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtAppSub.getSubName());
                            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtAppSub.getOrigiLmtAccSubNo());
                            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo(lmtAppSub.getOrigiLmtAccSubNo());
                            cmisLmt0001LmtSubListReqDto.setAvlamt(lmtAppSub.getLmtAmt());
                            cmisLmt0001LmtSubListReqDto.setOprType(lmtAppSub.getOprType());
                            cmisLmt0001LmtSubListReqDto.setCurType(lmtAppSub.getCurType());
                            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtAppSub.getGuarMode());
                            cmisLmt0001LmtSubListReqDto.setIsRevolv(lmtAppSub.getIsRevolvLimit());
                            cmisLmt0001LmtSubListReqDto.setIsPreCrd(lmtAppSub.getIsPreLmt());
                            cmisLmt0001LmtSubListReqDto.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                            cmisLmt0001LmtSubListReqDto.setLmtSubType("01");
                            cmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
                            log.info("分项层处理结束:"+JSON.toJSONString(cmisLmt0001LmtSubListReqDto));
                        }
                        HashMap<String, String> querySubPrdMap = new HashMap<>();
                        querySubPrdMap.put("subSerno", lmtAppSub.getSubSerno());
                        querySubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_DELETE);
                        querySubPrdMap.put("isHaveOriSubPrd", CmisCommonConstants.YES_NO_1);
                        List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectByParams(querySubPrdMap);
                        log.info("当前删除的分项编号[{}]项下的分项品种列表[{}]",lmtAppSub.getSubSerno(),JSON.toJSONString(lmtAppSubPrdList));
                        if(CollectionUtils.nonEmpty(lmtAppSubPrdList)){
                            for(LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList){
                                log.info("分项明细层处理开始:"+JSON.toJSONString(lmtAppSubPrd));
                                CmisLmt0001LmtSubListReqDto lmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
                                lmt0001LmtSubListReqDto.setAccSubNo(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                                lmt0001LmtSubListReqDto.setParentId(lmtAppSub.getOrigiLmtAccSubNo());
                                lmt0001LmtSubListReqDto.setLimitSubName(lmtAppSubPrd.getLmtBizTypeName());
                                lmt0001LmtSubListReqDto.setIsRevolv(lmtAppSubPrd.getIsRevolvLimit());
                                lmt0001LmtSubListReqDto.setLimitSubNo(lmtAppSubPrd.getLmtBizType());
                                lmt0001LmtSubListReqDto.setOrigiAccSubNo(lmtAppSubPrd.getOrigiLmtAccSubPrdNo());
                                lmt0001LmtSubListReqDto.setAvlamt(lmtAppSubPrd.getLmtAmt());
                                lmt0001LmtSubListReqDto.setCurType(lmtAppSubPrd.getCurType());
                                lmt0001LmtSubListReqDto.setTerm(lmtAppSubPrd.getLmtTerm());
                                lmt0001LmtSubListReqDto.setStartDate(lmtAppSubPrd.getStartDate());
                                lmt0001LmtSubListReqDto.setEndDate(lmtAppSubPrd.getEndDate());
                                lmt0001LmtSubListReqDto.setBailPreRate(lmtAppSubPrd.getBailPreRate());
                                lmt0001LmtSubListReqDto.setRateYear(lmtAppSubPrd.getRateYear());
                                lmt0001LmtSubListReqDto.setSuitGuarWay(lmtAppSubPrd.getGuarMode());
                                lmt0001LmtSubListReqDto.setLmtGraper(lmtAppSubPrd.getLmtGraperTerm());
                                lmt0001LmtSubListReqDto.setIsRevolv(lmtAppSubPrd.getIsRevolvLimit());
                                lmt0001LmtSubListReqDto.setIsPreCrd(lmtAppSubPrd.getIsPreLmt());
                                lmt0001LmtSubListReqDto.setOprType(lmtAppSubPrd.getOprType());
                                lmt0001LmtSubListReqDto.setLmtBizTypeProp(lmtAppSubPrd.getLmtBizTypeProp());
                                lmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                                lmt0001LmtSubListReqDto.setLmtSubType("02");
                                lmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_99);
                                lmt0001LmtSubListReqDto.setIsLriskLmt(CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSubPrd.getGuarMode()) ? CmisCommonConstants.STD_ZB_YES_NO_1 : CmisCommonConstants.STD_ZB_YES_NO_0);
                                cmisLmt0001LmtSubListReqDtoList.add(lmt0001LmtSubListReqDto);
                                log.info("分项明细层处理结束:"+JSON.toJSONString(lmt0001LmtSubListReqDto));
                            }
                        }
                    }
                }
                log.info("获取当前批复对应的申请时删除且原分项编号不为空的的分项----end----");
                log.info("当前批复台账分项信息[{}]", JSON.toJSONString(lmtReplyAccSubList));
                cmisLmt0001ReqDto.setLmtSubList(cmisLmt0001LmtSubListReqDtoList);
            } else {
                throw new Exception("授信批复台账分项查询异常");
            }
            // 调用额度接口
            log.info("发送额度系统请求报文[{}]", JSON.toJSONString(cmisLmt0001ReqDto));
            ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
            log.info("发送额度系统请求报文[{}]", JSON.toJSONString(cmisLmt0001RespDto));
            if (cmisLmt0001RespDto != null && cmisLmt0001RespDto.getData() != null && "0000".equals(cmisLmt0001RespDto.getData().getErrorCode())) {
                log.info("额度系统同步成功授信台账成功");
            } else {
                throw new Exception("同步额度系统异常");
            }
        } else {
            throw new Exception("授信批复台账查询异常");
        }
    }

    /**
     * @方法名称: generateLmtReplyAccBySerno
     * @方法描述: 根据申请流水号查询授信批复生成授信批复台账
     * @参数与返回说明:
     * @算法描述: 1.查询批复
     * 2.根据授信类型的不同，批复台账的处理逻辑不同
     * 2.1 ►授信新增时，生成新的业务流水号，新的批复号，新的批复台账号
     * 2.2 ►授信续作时，生成新业务流水号，新批复号，新批复生效时旧批复自动失效，TODO 原批复台账项下业务自动挂到新批复台账项下，要找李成金确认（是否需要判断台账的状态）
     * 2.3 ►授信复审时，生成新的业务流水号，新的批复号，更新原批复台账
     * 2.4 ►预授信细化时，生成新的业务流水号，新的批复号，更新原批复台账
     * 2.5 ►授信变更时，复制原授信方案。生成新业务流水号，更新原批复台账
     * 2.6 ►授信再议时，不改变业务流水号，生成新的批复号，TODO 根据再议前的授信类型去判断批复台账处理逻辑
     * 2.7 ►授信复议时，系统将原批复对应的申报（授信基本信息、授信分项明细）内容进行复制，生成新的业务流水号，新的批复号，TODO 根据复议前的授信类型去判断批复台账处理逻辑
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateLmtReplyAccBySerno(String serno) throws Exception {
        // 1.根据流水号查询授信批复
        LmtReply lmtReply = lmtReplyService.queryLmtReplyBySerno(serno);
        // 获取真正的授信类型
        String realLmtType = lmtReply.getLmtType();
        // 再议或者复议的情况下 需要找到原始授信申请数据
        if (CmisCommonConstants.LMT_TYPE_05.equals(lmtReply.getLmtType()) || CmisCommonConstants.LMT_TYPE_06.equals(lmtReply.getLmtType())) {
            realLmtType = findRealLmtType(lmtReply);
        }

        if ((CmisCommonConstants.LMT_TYPE_01.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_03.equals(realLmtType))
                || CmisCommonConstants.LMT_TYPE_06.equals(realLmtType)
                || "998_05".equals(realLmtType)) {
            // 授信新增 获取授信再议时 生成新的批复
            this.generateNewLmtReplyAcc(lmtReply);
        } else if (CmisCommonConstants.LMT_TYPE_02.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_04.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_07.equals(realLmtType)
                || CmisCommonConstants.LMT_TYPE_05.equals(realLmtType)
                || "997_05".equals(realLmtType)) {
            // 授信变更、授信复审、授信 更新原批复台账
            this.updateLmtReplyAcc(lmtReply, CmisCommonConstants.YES_NO_0);
        } else {
            throw new Exception("授信申请类型不符合，暂不支持" + lmtReply.getLmtType() + "类型");
        }
    }

    /**
     * @方法名称: generateLmtReplyAccBySerno
     * @方法描述: 根据批复流水号更新授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccByReplySerno(String replySerno) throws Exception {
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
        this.updateLmtReplyAccForLmtReplyChg(lmtReply);
    }

    /**
     * @方法名称: findRealLmtType
     * @方法描述: 根据授信批复查询原始批复的授信类型
     * @参数与返回说明:
     * @算法描述: 递归查询最近一笔授信类型不为复议或者再议的数据
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String findRealLmtType(LmtReply lmtReply) throws Exception {

        HashMap<String, String> queryMap = new HashMap<>();
        if (CmisCommonConstants.LMT_TYPE_05.equals(lmtReply.getLmtType())) {
            if (!StringUtils.isBlank(lmtReply.getOrigiLmtReplySerno())) {
                queryMap.put("replySerno", lmtReply.getOrigiLmtReplySerno());
                queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                List<LmtReply> lastLmtReplyList = lmtReplyService.queryLmtReplyDataByParams(queryMap);
                if (lastLmtReplyList != null && lastLmtReplyList.size() == 1) {
                    // 如果复议的上一笔为否决 那么生成新的批复 以及批复台账
                    // 如果复议上一笔为通过的数据 那么新增的批复 更新现有批复台账
                    LmtApp lmtApp = lmtAppService.selectBySerno(lastLmtReplyList.get(0).getSerno());
                    if (lmtApp == null) {
                        throw new Exception("根据原授信批复查询授信申请信息失败!原授信批复流水号:" + lmtReply.getOrigiLmtReplySerno());
                    }
                    if (lmtApp.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_997)) {// 通过
                        return "997_05";
                    } else if (lmtApp.getApproveStatus().equals(CmisCommonConstants.WF_STATUS_998)) {// 否决
                        return "998_05";
                    } else {
                        return findRealLmtType(lastLmtReplyList.get(0));
                    }
                } else {
                    throw new Exception("原授信批复查询异常!原授信批复流水号:" + lmtReply.getOrigiLmtReplySerno());
                }
            } else {
                return lmtReply.getLmtType();
            }
        } else if (CmisCommonConstants.LMT_TYPE_06.equals(lmtReply.getLmtType())) {
            return lmtReply.getLmtType();
        } else {
            return lmtReply.getLmtType();
        }
    }


    /**
     * @方法名称: updateLmtReplyAcc
     * @方法描述: 根据授信批复更新原来的批复台账
     * @参数与返回说明:
     * @算法描述: 1.通过查询授信批复，将查出的数据更新至批复台账中
     * 2.通过查询授信批复分项，将查出的数据更新至批复分项台账中
     * 3.通过查询授信批复分项适用品种，将查出的数据更新至批复分项台账适用品种中
     * 4.通过查询授信批复管理条件，将查出的数据更新至批复台账管理条件中
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAcc(LmtReply lmtReply, String isReplyChg) throws Exception {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        if (CmisLmtConstants.YES_NO_Y.equals(isReplyChg)) {// 批复变更意味需要根据传入的批复数据对台账进行更新
            queryMap.put("replySerno", lmtReply.getReplySerno());
        } else {// 非批复变更的场景意味要通过原批复编号查询对应的台账数据
            queryMap.put("replySerno", lmtReply.getOrigiLmtReplySerno());
        }
        // 1.通过批复编号或者原批复编号查询批复台账，将批复数据更新至批复台账中
        List<LmtReplyAcc> lmtReplyAccList = this.selectByParams(queryMap);
        if (lmtReplyAccList != null && lmtReplyAccList.size() == 1) {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccList.get(0);
            String oldPkId = lmtReplyAcc.getPkId();
            BeanUtils.beanCopy(lmtReply, lmtReplyAcc);
            lmtReplyAcc.setPkId(oldPkId);
            lmtReplyAcc.setAccNo(lmtReplyAcc.getAccNo());
            lmtReplyAcc.setReplySerno(lmtReply.getReplySerno());
            lmtReplyAcc.setSerno(lmtReply.getSerno());
            lmtReplyAcc.setAccStatus(CmisCommonConstants.STD_XD_REPLY_STATUS_01);
            this.update(lmtReplyAcc);
            log.info("更新授信台账:" + lmtReplyAcc.getAccNo());

            // 通过查询授信批复分项，将查出的数据更新至批复分项台账中
            lmtReplyAccSubService.updateLmtReplyAccSubByLmtReplySub(lmtReply, lmtReplyAcc.getAccNo());

            // 通过查询授信批复管理条件，将查出的数据更新至批复台账管理条件中
            lmtReplyAccLoanCondService.updateLmtReplyAccLoanCondByLmtReplyNeed(lmtReply, lmtReplyAcc.getAccNo());

        } else {
            throw new Exception("授信批复查询异常!");
        }

    }

    /**
     * @方法名称: updateLmtReplyAccForLmtReplyChg
     * @方法描述: 根据授信批复更新原来的批复台账(用于批复变更的情况)
     * @参数与返回说明:
     * @算法描述: 1.通过查询授信批复，将查出的数据更新至批复台账中
     * 2.通过查询授信批复分项，将查出的数据更新至批复分项台账中
     * 3.通过查询授信批复分项适用品种，将查出的数据更新至批复分项台账适用品种中
     * 4.通过查询授信批复管理条件，将查出的数据更新至批复台账管理条件中
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccForLmtReplyChg(LmtReply lmtReply) throws Exception {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("replySerno", lmtReply.getReplySerno());

        List<LmtReplyLoanCond> replyCondList = lmtReplyLoanCondService.queryLmtReplyLoanCondDataByParams(queryMap);
        // 1.通过查询批复台账，将批复数据更新至批复台账中
        List<LmtReplyAcc> lmtReplyAccList = this.selectByParams(queryMap);
        if (lmtReplyAccList != null && lmtReplyAccList.size() == 1) {
            LmtReplyAcc lmtReplyAcc = lmtReplyAccList.get(0);
            HashMap<String, String> map = new HashMap<>();
            queryMap.put("accNo", lmtReplyAcc.getAccNo());
            List<LmtReplyAccLoanCond> lmtReplyAccLoanCondList = lmtReplyAccLoanCondService.queryLmtReplyAccLoanCondByParams(map);
            for(LmtReplyAccLoanCond lmtReplyAccLoanCond :lmtReplyAccLoanCondList){
                lmtReplyAccLoanCondService.deleteByPrimaryKey(lmtReplyAccLoanCond.getPkId());
            }
            for(LmtReplyLoanCond lmtReplyLoanCond : replyCondList){
                LmtReplyAccLoanCond lmtReplyAccLoanCond = new LmtReplyAccLoanCond();
                BeanUtils.beanCopy(lmtReplyLoanCond, lmtReplyAccLoanCond);
                lmtReplyAccLoanCond.setAccNo(lmtReplyAcc.getAccNo());
                lmtReplyAccLoanCond.setPkId(UUID.randomUUID().toString());
                lmtReplyAccLoanCondService.insert(lmtReplyAccLoanCond);
            }
            lmtReplyAcc.setPspManaNeed(lmtReply.getPspManaNeed());
            this.update(lmtReplyAcc);
            /*String oldPkId = lmtReplyAcc.getPkId();
            BeanUtils.beanCopy(lmtReply, lmtReplyAcc);
            lmtReplyAcc.setPkId(oldPkId);


            log.info("更新授信台账:" + lmtReplyAcc.getAccNo());*/

            // 通过查询授信批复分项，将查出的数据更新至批复分项台账中
            //lmtReplyAccSubService.updateLmtReplyAccSubByLmtReplySub(lmtReply, lmtReplyAcc.getAccNo());

            // 通过查询授信批复管理条件，将查出的数据更新至批复台账管理条件中
            //lmtReplyAccNeedSubService.updateLmtReplyAccNeedSubByLmtReplyNeed(lmtReply, lmtReplyAcc.getAccNo());

        } else {
            throw new Exception("授信批复查询异常!");
        }

    }

    /**
     * @方法名称: generateNewLmtReplyAcc
     * @方法描述: 生成全新的授信批复台账
     * @参数与返回说明:
     * @算法描述: 1.通过查询授信批复，将查出的数据插入至批复台账中
     * 2.通过查询授信批复分项，将查出的数据插入至批复分项台账中
     * 3.通过查询授信批复分项适用品种，将查出的数据插入至批复分项台账适用品种中
     * 4.通过查询授信批复管理条件，将查出的数据插入至批复台账管理条件中
     * @创建人: mashun
     * @创建时间: 2021-04-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateNewLmtReplyAcc(LmtReply lmtReply) throws Exception {
        // 1.通过查询授信批复，将查出的数据插入至批复台账中
        LmtReplyAcc lmtReplyAcc = new LmtReplyAcc();
        if (CmisCommonConstants.LMT_TYPE_03.equals(lmtReply.getLmtType())) {
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("accStatus", CmisLmtConstants.STD_ZB_LMT_STATE_01);
            queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            queryMap.put("cusId", lmtReply.getCusId());
            List<LmtReplyAcc> lmtReplyAccList = this.selectByParams(queryMap);
            if (lmtReplyAccList != null && lmtReplyAccList.size() == 1) {
                LmtReplyAcc lmtReplyAccOld = lmtReplyAccList.get(0);
                lmtReplyAccOld.setAccStatus(CmisLmtConstants.STD_ZB_LMT_STATE_02);
                this.update(lmtReplyAccOld);
            }
        }
        BeanUtils.beanCopy(lmtReply, lmtReplyAcc);
        String accNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        log.info("生成新的授信台账流水号:" + accNo);
        lmtReplyAcc.setPkId(UUID.randomUUID().toString());
        lmtReplyAcc.setAccNo(accNo);
        lmtReplyAcc.setInureDate(stringRedisTemplate.opsForValue().get("openDay"));
        lmtReplyAcc.setAccStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
        this.insert(lmtReplyAcc);
        log.info("生成新的授信台账:" + accNo);
        // 通过查询授信批复分项，将查出的数据插入至批复分项台账中
        lmtReplyAccSubService.generateLmtReplyAccSubHandle(lmtReply.getReplySerno(), accNo);
        // 通过查询授信批复管理条件，将查出的数据插入至批复台账管理条件中
        lmtReplyAccLoanCondService.generateNewLmtReplyAccLoanCondHandle(lmtReply, accNo);
        return accNo;
    }

    /**
     * @方法名称: selectByParams
     * @方法描述: 通过参数查询批复台账数据，
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-02 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyAcc> selectByParams(Map queryMap) {
        return lmtReplyAccMapper.selectByParams(queryMap);
    }

    /**
     * @方法名称: getLmtReplyAccByAccNo
     * @方法描述: 根据台帐号查询授信台账，
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-04-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAcc getLmtReplyAccByAccNo(String accNo) {
        LmtReplyAcc lmtReplyAcc = new LmtReplyAcc();
        try {
            lmtReplyAcc = lmtReplyAccMapper.selectByAccNo(accNo);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return lmtReplyAcc;
    }

    /**
     * @方法名称：selectForManager
     * @方法描述：当前登录人下的批复台账信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-30 下午 1:56
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyAcc> selectForManager(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.getCondition().put("isGrp",CmisCommonConstants.STD_ZB_STATUS_0);
        model.setSort("inure_date desc");
        List<LmtReplyAcc> list = lmtReplyAccMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getReplyAccForRefine
     * @方法描述: 过滤得到可进行预授信细化的授信批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: zhuzr
     * @创建时间: 2021-05-05 10:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtReplyAcc> getReplyAccForRefine(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        Map<String, Object> condition = model.getCondition();
        if (condition.isEmpty()) {
            // TODO 初始查询条件待补充
        }
        condition.put("isPreLmt", "1");
        condition.put("accStatus", "01");
        List<LmtReplyAcc> list = lmtReplyAccMapper.getReplyAccForChg(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称: generateLmtReplyAccForGrpBySerno
     * @方法描述: 根据单一客户授信申请流水号生成集团的单一客户的批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtReplyAccForGrpBySerno(String serno) throws Exception {
        LmtReply lmtReply = lmtReplyService.queryLmtReplyBySerno(serno);
        return generateNewLmtReplyAcc(lmtReply);
    }

    /**
     * @方法名称: generateLmtReplyAccForGrpByReplySerno
     * @方法描述: 根据单一客户授信批复申请流水号生成集团的单一客户的批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public String generateLmtReplyAccForGrpByReplySerno(String replySerno) throws Exception {
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
        return generateNewLmtReplyAcc(lmtReply);
    }


    /**
     * @方法名称: updateLmtReplyAccForGrpBySerno
     * @方法描述: 根据单一客户授信申请流水号更新集团的单一客户的批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccForGrpBySerno(String serno, String isReplyChg) throws Exception {
        LmtReply lmtReply = lmtReplyService.queryLmtReplyBySerno(serno);
        updateLmtReplyAcc(lmtReply, isReplyChg);
    }

    /**
     * @方法名称: selectLmtReplyAccByGrpAccNo
     * @方法描述: 根据单一客户授信申请流水号更新集团的单一客户的批复台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-07 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public List<LmtReplyAcc> selectLmtReplyAccByGrpAccNo(String grpAccNo) {
        return lmtReplyAccMapper.selectLmtReplyAccByGrpAccNo(grpAccNo);
    }


    /**
     * @方法名称：selectBySerno
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public LmtReplyAcc selectBySerno(String serno) {
        return lmtReplyAccMapper.selectBySerno(serno);
    }

    /**
     * @方法名称: selectAccNoDataByParams
     * @方法描述: 根据批复编号获取批复台账详情信息，
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAcc selectAccNoDataByParams(Map queryMap) {
        return lmtReplyAccMapper.selectAccNoDataByParams(queryMap);
    }

    /**
     * @方法名称: getLmtReplyAccBySingleSerno
     * @方法描述: 根据申请流水号查询授信批复台账
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplyAcc> getLmtReplyAccBySingleSerno(String singleSerno) {
        return lmtReplyAccMapper.getLmtReplyAccBySingleSerno(singleSerno);
    }

    /**
     * @方法名称：getAllReplyAccInfo
     * @方法描述：获取批复所有信息包括分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-21 下午 9:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Map getAllReplyAccInfo(String accNo) {
        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
        String rtnCode = EcbEnum.ECB010000.key;
        String rtnMsg = EcbEnum.ECB010000.value;
        LmtReplyAcc lmtReplyAcc = null;
        List<LmtReplyAccLoanCond> lmtReplyAccLoanCondList = null;
        List<LmtReplyAccLoanCond> lmtReplyAfterLoanCondList = null;
        List<LmtReplyAccSubDto> lmtReplyAccSubDtoList = null;
        ResultDto<AdminSmUserDto> adminSmUserDto = null;
        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = null;
        Map map = new HashMap();
        Map newMap = new HashMap();
        try {
            if (StringUtils.isBlank(accNo)) {
                rtnCode = EcbEnum.ECB010001.key;
                rtnMsg = EcbEnum.ECB010001.value;
                return rtnData;
            }
            lmtReplyAcc = getLmtReplyAccByAccNo(accNo);
            adminSmUserDto = adminSmUserService.getByLoginCode(lmtReplyAcc.getManagerId());
            adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(lmtReplyAcc.getManagerBrId());
            HashMap<String, String> queryMap = new HashMap<>();
            queryMap.put("accNo", accNo);
            //queryMap.put("condType", CmisCommonConstants.STD_COND_TYPE_01);
            lmtReplyAccLoanCondList = lmtReplyAccLoanCondService.queryLmtReplyAccLoanCondByParams(queryMap);
            //queryMap.put("condType", CmisCommonConstants.STD_COND_TYPE_02);
            //lmtReplyAfterLoanCondList = lmtReplyAccLoanCondService.queryLmtReplyAccLoanCondByParams(queryMap);
            HashMap<String, String> querySubMap = new HashMap<>();
            querySubMap.put("accNo", accNo);
            querySubMap.put("oprType",CmisCommonConstants.LMT_OPER_OPT_TYPE_01);
            List<LmtReplyAccSub> lmtReplyAccSubs = lmtReplyAccSubService.queryLmtReplyAccSubByParams(querySubMap);
            if (lmtReplyAccSubs != null) {
                lmtReplyAccSubDtoList = (List<LmtReplyAccSubDto>) BeanUtils.beansCopy(lmtReplyAccSubs, LmtReplyAccSubDto.class);
                lmtReplyAccSubDtoList.forEach(lmtReplyAccSubDto -> {
                    String accSubNo = lmtReplyAccSubDto.getAccSubNo();
                    String subName = lmtReplyAccSubDto.getAccSubName();
                    if (StringUtils.nonBlank(subName)) {
                        lmtReplyAccSubDto.setLmtDrawType(subName);
                    }
                    if (!StringUtils.isBlank(accSubNo)) {
                        lmtReplyAccSubDto.setLmtDrawNo(accSubNo);
                        List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(accSubNo);
                        List<LmtReplyAccSubPrdDto> lmtReplyAccSubPrdDtoList = (List<LmtReplyAccSubPrdDto>) BeanUtils.beansCopy(lmtReplyAccSubPrdList, LmtReplyAccSubPrdDto.class);
                        List<LmtReplyAccSubPrdDto> lmtReplyAccSubPrdDtoListCopy = new ArrayList<>();
                        for (LmtReplyAccSubPrdDto lmtReplyAccSubPrdDto : lmtReplyAccSubPrdDtoList) {
                            String bizTypeName = lmtReplyAccSubPrdDto.getLmtBizTypeName();
                            String accSubPrdNo = lmtReplyAccSubPrdDto.getAccSubPrdNo();
                            String subPrdserno = lmtReplyAccSubPrdDto.getSubPrdSerno();
                            String replySubPrdserno = lmtReplyAccSubPrdDto.getReplySubPrdSerno();
                            if (StringUtils.nonBlank(bizTypeName)) {
                                lmtReplyAccSubPrdDto.setLmtDrawType(bizTypeName);
                            }
                            if (StringUtils.nonBlank(accSubPrdNo)) {
                                lmtReplyAccSubPrdDto.setLmtDrawNo(accSubPrdNo);
                            }
                            if (StringUtils.nonBlank(subPrdserno)) {
                                lmtReplyAccSubPrdDto.setSubSerno(subPrdserno);
                            }
                            if (StringUtils.nonBlank(replySubPrdserno)) {
                                lmtReplyAccSubPrdDto.setReplySubSerno(replySubPrdserno);
                            }
                            lmtReplyAccSubPrdDtoListCopy.add(lmtReplyAccSubPrdDto);
                        }
                        lmtReplyAccSubDto.setChildren(lmtReplyAccSubPrdDtoListCopy);
                        // 分项层不展示授信期限
                        lmtReplyAccSubDto.setLmtTerm(null);
                    }
                });
            }

            // 计算到期日
            log.info("计算到期日----------start--------------");
            String inureDate = lmtReplyAcc.getInureDate();
            map = BeanUtils.beanToMap(lmtReplyAcc);
            if(StringUtils.nonBlank(inureDate)){
                int lmtTerm = lmtReplyAcc.getLmtTerm();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date startDate = simpleDateFormat.parse(inureDate);
                Date endDate = DateUtils.addDay(DateUtils.addMonth(startDate,lmtTerm),-1);
                newMap.putAll(map);
                newMap.put("endDate",simpleDateFormat.format(endDate));

            }
            log.info("计算到期日----------end--------------");
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("获取批复台账所有信息包括分项明细！", e);
            rtnCode = EpbEnum.EPB099999.key;
            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
        } finally {
            rtnData.put("lmtReplyAcc", newMap);
            rtnData.put("lmtReplyAccSubDtoList", lmtReplyAccSubDtoList);
            rtnData.put("lmtReplyAccLoanCondList", lmtReplyAccLoanCondList);
            //rtnData.put("lmtReplyAfterLoanCondList", lmtReplyAfterLoanCondList);
            rtnData.put("managerIdName", adminSmUserDto.getData().getUserName());
            rtnData.put("managerBrIdName", adminSmOrgDtoResultDto.getData().getOrgName());
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    public static void main(){

    }

    /**
     * @方法名称: 根据合同金额生成批复低风险分项数据、批复台账低风险分项数据
     * @方法描述: ，
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-07-12 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public boolean  generaLmtReplyAccForLowRisk(BigDecimal contAmt, String cusId, String busiType, BigDecimal bailRate, BigDecimal rateYear,String endDate,String isDisc) throws Exception {
        boolean result = false;
        LmtReplyAcc lmtReplyAcc = this.getLastLmtReplyAcc(cusId);
        if (lmtReplyAcc == null || StringUtils.isBlank(lmtReplyAcc.getAccNo())) {
            throw BizException.error(null, EcbEnum.ECB010017.key, EcbEnum.ECB010017.value);
        }

        String lmtStartDate = stringRedisTemplate.opsForValue().get("openDay");
        String lmtEndDate = "";
        Integer gaper = 0;
        // 计算获取起始日，到期日和宽限期
        LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelBySingleSerno(lmtReplyAcc.getAccNo());
        log.info("根据单一授信成员流水号【{}】查询集团成员关系表，返回参数为【】",lmtReplyAcc.getAccNo(),JSON.toJSONString(lmtGrpMemRel));
        if(Objects.isNull(lmtGrpMemRel)){
            List<LmtReplyAccSub> lmtReplyAccSubList = lmtReplyAccSubService.selectByAccNo(lmtReplyAcc.getAccNo());
            if(CollectionUtils.nonEmpty(lmtReplyAccSubList)){
                for (LmtReplyAccSub replyAccSub : lmtReplyAccSubList) {
                    List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(replyAccSub.getAccSubNo());
                    if(CollectionUtils.nonEmpty(lmtReplyAccSubPrdList)){
                        for (LmtReplyAccSubPrd replyAccSubPrd : lmtReplyAccSubPrdList) {
                            if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(replyAccSubPrd.getIsRevolvLimit())){
                                lmtEndDate = replyAccSubPrd.getEndDate();
                                gaper = lmtReplyAcc.getLmtGraperTerm();
                            }
                        }
                    }
                }
                log.info("单一客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                if("".equals(lmtEndDate)){
                    // 一般贴现协议
                    if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isDisc)){
                        LocalDate parse = LocalDate.parse(lmtStartDate);
                        lmtEndDate = parse.plusYears(1).minusDays(1).toString();
                        gaper = 12;
                        log.info("(一般贴现协议)单一客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                    }else {
                        Integer term = CmisCommonUtils.getBetweenMonth(lmtStartDate,endDate);
                        if(term.compareTo(12)>0){
                            LocalDate parse = LocalDate.parse(lmtStartDate);
                            lmtEndDate = parse.plusYears(1).minusDays(1).toString();
                            gaper = 12;
                            log.info("单一客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                        }else {
                            lmtEndDate = endDate;
                            gaper = 12;
                            log.info("单一客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                        }
                    }
                }
            }
        }else{
            List<String> endDateList = new ArrayList<>();
            List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(lmtGrpMemRel.getGrpSerno());
            if(CollectionUtils.nonEmpty(lmtGrpMemRelList)){
                for (LmtGrpMemRel grpMemRel : lmtGrpMemRelList) {
                    LmtReplyAcc lmtReplyAcc1 = this.getLmtReplyAccByAccNo(grpMemRel.getSingleSerno());
                    if(Objects.nonNull(lmtReplyAcc1)){
                        List<LmtReplyAccSubPrd> origiSubPrdList =  lmtReplyAccSubPrdService.getLmtReplyAccSubPrdByReplyNo(lmtReplyAcc1.getReplySerno());
                        for (LmtReplyAccSubPrd replyAccSubPrd : origiSubPrdList) {
                            if(CmisCommonConstants.YES_NO_1.equals(replyAccSubPrd.getIsRevolvLimit())){
                                if(lmtStartDate.compareTo(replyAccSubPrd.getEndDate())<0){
                                    endDateList.add(replyAccSubPrd.getEndDate());
                                }
                            }
                        }
                    }
                }
                if(CollectionUtils.nonEmpty(endDateList)){
                    List<String> collect = endDateList.parallelStream().filter(Objects::nonNull).sorted((e1, e2) -> e1.compareTo(e2)).collect(Collectors.toList());
                    lmtEndDate = collect.get(0);
                    log.info("集团成员客户最早的循环额度到期日为【{}】",lmtEndDate);
                    gaper = lmtReplyAcc.getLmtGraperTerm();
                    log.info("集团成员客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                }else {
                    if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isDisc)){
                        LocalDate parse = LocalDate.parse(lmtStartDate);
                        lmtEndDate = parse.plusYears(1).minusDays(1).toString();
                        gaper = 12;
                        log.info("集团成员客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                    }else {
                        Integer term = CmisCommonUtils.getBetweenMonth(lmtStartDate,endDate);
                        if(term.compareTo(12)>0){
                            LocalDate parse = LocalDate.parse(lmtStartDate);
                            lmtEndDate = parse.plusYears(1).minusDays(1).toString();
                            gaper = 12;
                            log.info("集团成员客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                        }else {
                            lmtEndDate = endDate;
                            gaper = 12;
                            log.info("集团成员客户的授信起始日【{}】,到期日【{}】,宽限期【{}】,分别为：",lmtStartDate,lmtEndDate,gaper);
                        }
                    }
                }
            }
        }

        log.info("根据合同金额{}生成低风险授信分项", contAmt);
        String accSubNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        String accSubPrdNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        String replySubNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());
        String replySubPrdNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_REPAY_SERNO, new HashMap<>());

        LmtReplyAccSub lmtReplyAccSub = new LmtReplyAccSub();
        lmtReplyAccSub.setPkId(UUID.randomUUID().toString());
        lmtReplyAccSub.setAccSubNo(accSubNo);
        if (CmisCommonConstants.STD_BUSI_TYPE_03.equals(busiType)) { // 贴现协议
            lmtReplyAccSub.setAccSubName("贴现");
        } else if (CmisCommonConstants.STD_BUSI_TYPE_05.equals(busiType)) { // 福费廷合同
            lmtReplyAccSub.setAccSubName("福费廷");
        } else {
            lmtReplyAccSub.setAccSubName("一般低风险额度");
        }
        if (null == lmtReplyAcc) {
            throw new RuntimeException("未查询到授信批复台账数据！");
        }
        lmtReplyAccSub.setAccNo(lmtReplyAcc.getAccNo());
        lmtReplyAccSub.setReplySerno(lmtReplyAcc.getReplySerno());
        lmtReplyAccSub.setReplySubSerno(replySubNo);
        lmtReplyAccSub.setSerno(lmtReplyAcc.getSerno());
        lmtReplyAccSub.setCusId(lmtReplyAcc.getCusId());
        lmtReplyAccSub.setCusType(lmtReplyAcc.getCusType());
        lmtReplyAccSub.setCusName(lmtReplyAcc.getCusName());
        lmtReplyAccSub.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        lmtReplyAccSub.setGuarMode(CmisCommonConstants.GUAR_MODE_60);
        lmtReplyAccSub.setCurType("CNY");
        lmtReplyAccSub.setLmtAmt(contAmt);
        lmtReplyAccSub.setIsPreLmt(CmisCommonConstants.STD_ZB_YES_NO_0);
        lmtReplyAccSub.setIsRevolvLimit(CmisCommonConstants.STD_ZB_YES_NO_1);
        lmtReplyAccSub.setLmtTerm(lmtReplyAcc.getLmtTerm());
        lmtReplyAccSub.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        lmtReplyAccSub.setInputId(lmtReplyAcc.getInputId());
        lmtReplyAccSub.setInputBrId(lmtReplyAcc.getInputBrId());
        lmtReplyAccSub.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        lmtReplyAccSub.setUpdId(lmtReplyAcc.getInputId());
        lmtReplyAccSub.setUpdBrId(lmtReplyAcc.getInputBrId());
        lmtReplyAccSub.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
        lmtReplyAccSub.setCreateTime(DateUtils.getCurrDate());
        lmtReplyAccSub.setUpdateTime(DateUtils.getCurrDate());
        // 插入批复台账分项数据
        lmtReplyAccSubService.insert(lmtReplyAccSub);
        log.info("插入授信批复台账分项{}", lmtReplyAccSub.toString());
        LmtReplySub lmtReplySub = BeanUtils.beanCopy(lmtReplyAccSub, LmtReplySub.class);
        lmtReplySub.setReplySerno(lmtReplyAcc.getReplySerno());
        //  插入批复分项数据
        lmtReplySubService.insert(lmtReplySub);
        log.info("插入授信批复分项{}", lmtReplySub.toString());

        LmtReplyAccSubPrd lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
        lmtReplyAccSubPrd.setPkId(UUID.randomUUID().toString());
        lmtReplyAccSubPrd.setAccSubPrdNo(accSubPrdNo);
        lmtReplyAccSubPrd.setReplySubPrdSerno(replySubPrdNo);
        lmtReplyAccSubPrd.setAccSubNo(accSubNo);
        lmtReplyAccSubPrd.setReplySubSerno(replySubNo);
        lmtReplyAccSubPrd.setCusId(lmtReplyAcc.getCusId());
        lmtReplyAccSubPrd.setCusType(lmtReplyAcc.getCusType());
        lmtReplyAccSubPrd.setCusName(lmtReplyAcc.getCusName());
        if (CmisCommonConstants.STD_BUSI_TYPE_03.equals(busiType)) { // 贴现协议
            lmtReplyAccSubPrd.setLmtBizType("12010101");
            lmtReplyAccSubPrd.setLmtBizTypeName("贴现");
        } else if (CmisCommonConstants.STD_BUSI_TYPE_05.equals(busiType)) { // 福费廷合同
            lmtReplyAccSubPrd.setLmtBizType("12010102");
            lmtReplyAccSubPrd.setLmtBizTypeName("福费廷");
        } else {
            lmtReplyAccSubPrd.setLmtBizType("12010103");
            lmtReplyAccSubPrd.setLmtBizTypeName("一般低风险额度");
        }
        lmtReplyAccSubPrd.setGuarMode(CmisCommonConstants.GUAR_MODE_60);
        lmtReplyAccSubPrd.setCurType("CNY");
        lmtReplyAccSubPrd.setLmtAmt(contAmt);
        lmtReplyAccSubPrd.setStartDate(lmtStartDate);
        lmtReplyAccSubPrd.setEndDate(lmtEndDate);
        Integer term = CmisCommonUtils.getBetweenMonth(lmtStartDate,lmtEndDate);
        lmtReplyAccSubPrd.setLmtTerm(term);
        lmtReplyAccSubPrd.setLmtGraperTerm(gaper);
        lmtReplyAccSubPrd.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);
        lmtReplyAccSubPrd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
        lmtReplyAccSubPrd.setIsPreLmt(CmisCommonConstants.STD_ZB_YES_NO_0);
        lmtReplyAccSubPrd.setIsRevolvLimit(CmisCommonConstants.STD_ZB_YES_NO_1);
        lmtReplyAccSubPrd.setInputId(lmtReplyAcc.getInputId());
        lmtReplyAccSubPrd.setInputBrId(lmtReplyAcc.getInputBrId());
        lmtReplyAccSubPrd.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        lmtReplyAccSubPrd.setUpdId(lmtReplyAcc.getInputId());
        lmtReplyAccSubPrd.setUpdBrId(lmtReplyAcc.getInputBrId());
        lmtReplyAccSubPrd.setUpdDate(stringRedisTemplate.opsForValue().get("openDay"));
        lmtReplyAccSubPrd.setCreateTime(DateUtils.getCurrDate());
        lmtReplyAccSubPrd.setUpdateTime(DateUtils.getCurrDate());
        lmtReplyAccSubPrdService.insert(lmtReplyAccSubPrd);
        log.info("插入授信批复台账分项品种{}", lmtReplyAccSubPrd.toString());

        LmtReplySubPrd lmtReplySubPrd = BeanUtils.beanCopy(lmtReplyAccSubPrd, LmtReplySubPrd.class);
        //  插入批复分项数据
        lmtReplySubPrdService.insert(lmtReplySubPrd);
        log.info("插入授信批复分项品种{}", lmtReplySubPrd.toString());

        // 更新批复台账金额
        lmtReplyAcc.setLowRiskTotalLmtAmt(contAmt);
        this.update(lmtReplyAcc);
        log.info("更新授信批复台账低风险金额{}", contAmt);

        // 查询台账对应批复
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(lmtReplyAcc.getReplySerno());
        lmtReply.setLowRiskTotalLmtAmt(contAmt);
        lmtReplyService.updateSelective(lmtReply);
        log.info("更新授信批复低风险金额{}", contAmt);


        log.info("同步额度系统反向生成低风险额度开始");
        CmisLmt0041ReqDto cmisLmt0041ReqDto = new CmisLmt0041ReqDto();
        cmisLmt0041ReqDto.setSerno(UUID.randomUUID().toString());
        cmisLmt0041ReqDto.setInputId(CmisCommonUtils.getInstucde(lmtReplyAcc.getManagerBrId()));
        cmisLmt0041ReqDto.setCusId(cusId);
        cmisLmt0041ReqDto.setCusName(lmtReplyAcc.getCusName());
        cmisLmt0041ReqDto.setApprSerno(lmtReplyAcc.getAccNo());
        cmisLmt0041ReqDto.setParent(accSubNo);
        cmisLmt0041ReqDto.setApprSubSerno(accSubPrdNo);
        cmisLmt0041ReqDto.setTerm(lmtReplyAcc.getLmtTerm());
        cmisLmt0041ReqDto.setStartDate(lmtStartDate);
        cmisLmt0041ReqDto.setEndDate(lmtEndDate);
        cmisLmt0041ReqDto.setLmtGraper(gaper);
        cmisLmt0041ReqDto.setLimitSubNo(lmtReplyAccSubPrd.getLmtBizType());
        cmisLmt0041ReqDto.setLimitSubName(lmtReplyAccSubPrd.getLmtBizTypeName());
        cmisLmt0041ReqDto.setSuitGuarWay(lmtReplyAccSub.getGuarMode());
        cmisLmt0041ReqDto.setLmtAmt(lmtReplyAccSubPrd.getLmtAmt());
        cmisLmt0041ReqDto.setBailPreRate(bailRate);
        cmisLmt0041ReqDto.setRateYear(rateYear);
        cmisLmt0041ReqDto.setInputId(lmtReplyAcc.getInputId());
        cmisLmt0041ReqDto.setInputBrId(lmtReplyAcc.getInputBrId());
        cmisLmt0041ReqDto.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        log.info("同步额度系统反向生成低风险额度开始,传参为【{}】", JSON.toJSONString(cmisLmt0041ReqDto));
        ResultDto<CmisLmt0041RespDto> cmisLmt0041RespDto = cmisLmtClientService.cmislmt0041(cmisLmt0041ReqDto);
        log.info("同步额度系统反向生成低风险额度结束,回参为【{}】", JSON.toJSONString(cmisLmt0041RespDto));
        String rtCode = cmisLmt0041RespDto.getCode();
        if (rtCode != null && SuccessEnum.CMIS_SUCCSESS.key.equals(rtCode)) {
            result = true;
        }
        return result;
    }

    /**
     * @方法名称: isExistMultipleAndLowRiskLmt
     * @方法描述: 根据客户编号判断是否存在综合授信额度以及低风险分项
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map isExistMultipleAndLowRiskLmt(Map map) {
        Map resultMap = new HashMap();
        String cusId = (String) map.get("cusId");
        String prdId = (String) map.get("prdId");
        if(cusId==null||"".equals(cusId)){
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        //调判断接口，判断客户是否存在综合授信额度
        CmisLmt0019ReqDto cmisLmt0019ReqDto = new CmisLmt0019ReqDto();
        User userInfo = SessionUtils.getUserInformation();

        cmisLmt0019ReqDto.setInstuCde(CmisCommonUtils.getInstucde(userInfo.getOrg().getCode()));
        cmisLmt0019ReqDto.setCusId(cusId);
        log.info("判断客户："+cusId+" 是否存在综合授信额度开始，请求报文："+cmisLmt0019ReqDto.toString());
        ResultDto<CmisLmt0019RespDto> resultDto = cmisLmtClientService.cmisLmt0019(cmisLmt0019ReqDto);
        log.info("判断客户："+cusId+" 是否存在综合授信额度结束，响应报文："+resultDto.toString());
        if (!"0".equals(resultDto.getCode())) {
            log.error("判断客户" + cusId + "是否存在综合授信额度接口异常！");
            throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
        }
        String code = resultDto.getData().getErrorCode();
        if (!"0000".equals(code)) {
            log.error("判断客户" + cusId + "是否存在综合授信额度接口异常！");
            throw BizException.error(null, resultDto.getData().getErrorCode(), resultDto.getData().getErrorMsg());
        }
        String result = resultDto.getData().getResult();
        if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(result)) {
            throw BizException.error(null, EcbEnum.ECB020008.key, EcbEnum.ECB020008.value);
        } else {
            // 判断客户名下是否存在低风险分项，有的话自动挂接对应的低风险产品
            CmisLmt0056ReqDto cmisLmt0056ReqDto = new CmisLmt0056ReqDto();
            cmisLmt0056ReqDto.setInstuCde(CmisCommonUtils.getInstucde(userInfo.getOrg().getCode()));
            cmisLmt0056ReqDto.setCusId(cusId);
            cmisLmt0056ReqDto.setPrdId(prdId);
            log.info("判断客户："+cusId+" 是否存在对应产品的低风险分项开始，请求报文："+cmisLmt0056ReqDto.toString());
            ResultDto<CmisLmt0056RespDto> cmisLmt0056RespDtoResultDto = cmisLmtClientService.cmislmt0056(cmisLmt0056ReqDto);
            log.info("判断客户："+cusId+" 是否存在对应产品的低风险分项结束，响应报文："+cmisLmt0056RespDtoResultDto.toString());
            if (!"0".equals(cmisLmt0056RespDtoResultDto.getCode())) {
                log.error("判断客户" + cusId + "是否存在对应产品的低风险分项异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String responseCode = cmisLmt0056RespDtoResultDto.getData().getErrorCode();
            if (!"0000".equals(responseCode)) {
                log.error("判断客户" + cusId + "是否存在对应产品的低风险分项异常！");
                throw BizException.error(null, resultDto.getData().getErrorCode(), resultDto.getData().getErrorMsg());
            }
            resultMap.put("accSubPrdNo",cmisLmt0056RespDtoResultDto.getData().getApprSubSerno());
            resultMap.put("accSubNo",cmisLmt0056RespDtoResultDto.getData().getSubSerno());
            resultMap.put("suitGuarWay",cmisLmt0056RespDtoResultDto.getData().getSuitGuarWay());
            resultMap.put("lmtBizTypeProp",cmisLmt0056RespDtoResultDto.getData().getLmtBizTypeProp());
        }
        return resultMap;
    }

    /**
     * @方法名称: isExistAccSubOnHighAmtAgrCont
     * @方法描述: 根据客户编号以及产品查找适用的品种分项
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAccSubPrd isExistAccSubOnHighAmtAgrCont(String lmtAccNo,String prdId) {
        HashMap<String, String> map = new HashMap();
        map.put("accSubNo",lmtAccNo);
        map.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.queryLmtReplyAccSubPrdByParams(map);
        for (LmtReplyAccSubPrd lmtReplyAccSubPrdData : lmtReplyAccSubPrdList) {
            //根据额度品种编号返回对应的产品
            CmisLmt0038ReqDto cmisLmt0038ReqDto = new CmisLmt0038ReqDto();
            cmisLmt0038ReqDto.setLimitSubNo(lmtReplyAccSubPrdData.getLmtBizType());
            ResultDto<CmisLmt0038RespDto> resultData = cmisLmtClientService.cmislmt0038(cmisLmt0038ReqDto);
            CmisLmt0038RespDto CmisLmt0038RespDto =  resultData.getData();
            if (Objects.isNull(CmisLmt0038RespDto) || !"0000".equals(CmisLmt0038RespDto.getErrorCode())) {
                log.error("根据额度品种编号" + lmtReplyAccSubPrdData.getLmtBizType() + "查询适用产品异常！");
                throw BizException.error(null, resultData.getData().getErrorMsg());
            }
            List<CmisLmt0038PrdListRespDto> cmisLmt0038PrdListRespDtoList = CmisLmt0038RespDto.getCmisLmt0038PrdListRespDtoList();
            if(CollectionUtils.nonEmpty(cmisLmt0038PrdListRespDtoList)) {
                for (CmisLmt0038PrdListRespDto cmisLmt0038PrdListRespDto : cmisLmt0038PrdListRespDtoList) {
                    if(Objects.equals(prdId,cmisLmt0038PrdListRespDto.getPrdId())){
                        return lmtReplyAccSubPrdData;
                    }
                }
            }
        }
        return null;
    }
}
