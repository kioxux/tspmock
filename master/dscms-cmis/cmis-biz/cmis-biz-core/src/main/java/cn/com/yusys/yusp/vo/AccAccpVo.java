package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "银承台账列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccAccpVo {

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
   银承核心编号
    */
    @ExcelField(title = "银承核心编号", viewLength = 20)
    private String coreBillNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /*
    担保方式
     */
    @ExcelField(title = "担保方式", viewLength = 20 ,dictCode = "STD_ZB_GUAR_WAY")
    private String guarMode;

    /*
    开票张数
     */
    @ExcelField(title = "开票张数", viewLength = 20)
    private String isseCnt;

    /*
    票面总金额
     */
    @ExcelField(title = "票面总金额", viewLength = 20,format = "#0.00")
    private java.math.BigDecimal drftTotalAmt;

    /*
    出票日期
     */
    @ExcelField(title = "出票日期", viewLength = 20)
    private String isseDate;

    /*
    保证金总金额
     */
    @ExcelField(title = "保证金总金额", viewLength = 20,format = "#0.00")
    private java.math.BigDecimal bailAmt;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
     */
    @ExcelField(title = "台账状态", viewLength = 20 ,dictCode = "STD_ACC_ACCP_STATUS")
    private String accStatus;

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCoreBillNo() {
        return coreBillNo;
    }

    public void setCoreBillNo(String coreBillNo) {
        this.coreBillNo = coreBillNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getIsseCnt() {
        return isseCnt;
    }

    public void setIsseCnt(String isseCnt) {
        this.isseCnt = isseCnt;
    }

    public BigDecimal getDrftTotalAmt() {
        return drftTotalAmt;
    }

    public void setDrftTotalAmt(BigDecimal drftTotalAmt) {
        this.drftTotalAmt = drftTotalAmt;
    }

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
