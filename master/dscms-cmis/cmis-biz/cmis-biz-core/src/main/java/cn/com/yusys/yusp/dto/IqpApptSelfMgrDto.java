package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApptSelfMgr
 * @类描述: iqp_appt_self_mgr数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-10 10:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApptSelfMgrDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 贷款申请人主键 **/
	private String apptCode;
	
	/** 企业名称 **/
	private String companyName;
	
	/** 企业地址 **/
	private String companyAddr;
	
	/** 企业性质 **/
	private String companyType;
	
	/** 企业规模 **/
	private String companyScale;
	
	/** 统一社会信用代码 **/
	private String unifyCreditCode;
	
	/** 成立年份 **/
	private String foundYear;
	
	/** 企业-省 **/
	private String companyProvince;
	
	/** 企业-市 **/
	private String companyCity;
	
	/** 企业-区 **/
	private String companyArea;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}
	
    /**
     * @return ApptCode
     */	
	public String getApptCode() {
		return this.apptCode;
	}
	
	/**
	 * @param companyName
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName == null ? null : companyName.trim();
	}
	
    /**
     * @return CompanyName
     */	
	public String getCompanyName() {
		return this.companyName;
	}
	
	/**
	 * @param companyAddr
	 */
	public void setCompanyAddr(String companyAddr) {
		this.companyAddr = companyAddr == null ? null : companyAddr.trim();
	}
	
    /**
     * @return CompanyAddr
     */	
	public String getCompanyAddr() {
		return this.companyAddr;
	}
	
	/**
	 * @param companyType
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType == null ? null : companyType.trim();
	}
	
    /**
     * @return CompanyType
     */	
	public String getCompanyType() {
		return this.companyType;
	}
	
	/**
	 * @param companyScale
	 */
	public void setCompanyScale(String companyScale) {
		this.companyScale = companyScale == null ? null : companyScale.trim();
	}
	
    /**
     * @return CompanyScale
     */	
	public String getCompanyScale() {
		return this.companyScale;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode == null ? null : unifyCreditCode.trim();
	}
	
    /**
     * @return UnifyCreditCode
     */	
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param foundYear
	 */
	public void setFoundYear(String foundYear) {
		this.foundYear = foundYear == null ? null : foundYear.trim();
	}
	
    /**
     * @return FoundYear
     */	
	public String getFoundYear() {
		return this.foundYear;
	}
	
	/**
	 * @param companyProvince
	 */
	public void setCompanyProvince(String companyProvince) {
		this.companyProvince = companyProvince == null ? null : companyProvince.trim();
	}
	
    /**
     * @return CompanyProvince
     */	
	public String getCompanyProvince() {
		return this.companyProvince;
	}
	
	/**
	 * @param companyCity
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity == null ? null : companyCity.trim();
	}
	
    /**
     * @return CompanyCity
     */	
	public String getCompanyCity() {
		return this.companyCity;
	}
	
	/**
	 * @param companyArea
	 */
	public void setCompanyArea(String companyArea) {
		this.companyArea = companyArea == null ? null : companyArea.trim();
	}
	
    /**
     * @return CompanyArea
     */	
	public String getCompanyArea() {
		return this.companyArea;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}