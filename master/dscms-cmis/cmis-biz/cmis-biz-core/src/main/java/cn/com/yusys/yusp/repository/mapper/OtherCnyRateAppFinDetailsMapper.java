/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.LmtApp;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateAppFinDetailsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-05 14:38:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface OtherCnyRateAppFinDetailsMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    OtherCnyRateAppFinDetails selectByPrimaryKey(@Param("subSerno") String subSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<OtherCnyRateAppFinDetails> selectByModel(QueryModel model);

    /**
     * @函数名称:getLmtReplyAccSubPrdByCusId
     * @函数描述:根据查到的集团成员客户号查询授信批复台账
     * @参数与返回说明:
     * @算法描述:
     */
    List<OtherCnyRateAppFinDetails> getFinDetailsByCusId(String cusId);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(OtherCnyRateAppFinDetails record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(OtherCnyRateAppFinDetails record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(OtherCnyRateAppFinDetails record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(OtherCnyRateAppFinDetails record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("subSerno") String subSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @创建人 lixm
     * @创建时间 2021-06-05
     * @注释 根据客户编号获取有效申请信息
     */
    List<OtherCnyRateAppFinDetails> selectEffectiveList(QueryModel queryModel);

    /**
     * 根据合同表授信额度编号查询人民币定价利率申请表信息
     *
     * @param
     * @创建者：zhangliang15
     * @return
     */
    OtherCnyRateAppFinDetails selectByLmtAccNo(@Param("contNo") String contNo);
   
}