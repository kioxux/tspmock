package cn.com.yusys.yusp.web.server.xddb0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0001.req.Xddb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0001.resp.Xddb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0001.Xddb0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:查询押品是否按揭
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0001:查询押品是否按揭")
@RestController
@RequestMapping("/api/bizls4bsp")
public class BizXddb0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0001Resource.class);

    @Autowired
    private Xddb0001Service xddb0001Service;

    /**
     * 交易码：xddb0001
     * 交易描述：查询押品是否按揭
     *
     * @param xddb0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询押品是否按揭")
    @PostMapping("/xddb0001")
    protected @ResponseBody
    ResultDto<Xddb0001DataRespDto> xddb0001(@Validated @RequestBody Xddb0001DataReqDto xddb0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001DataReqDto));
        Xddb0001DataRespDto xddb0001DataRespDto = new Xddb0001DataRespDto();// 响应Dto:查询押品是否按揭
        ResultDto<Xddb0001DataRespDto> xddb0001DataResultDto = new ResultDto<>();
        // 从xddb0001DataReqDto获取业务值进行业务逻辑处理
        try {
            //先根据核心押品编号查询是否存在指定的押品记录
            String coreGrtNo = xddb0001DataReqDto.getGuarNo();//押品编号
            if (StringUtil.isNotEmpty(coreGrtNo)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001DataReqDto));
                xddb0001DataRespDto = xddb0001Service.getIsWarrantSatte(xddb0001DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001DataRespDto));
                // 封装xddb0001DataResultDto中正确的返回码和返回信息
                xddb0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {//请求字段为空
                xddb0001DataResultDto.setCode(EcbEnum.ECB010001.key);
                xddb0001DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, e.getMessage());
            // 封装xddb0001DataResultDto中异常返回码和返回信息
            xddb0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0001DataRespDto到xddb0001DataResultDto中
        xddb0001DataResultDto.setData(xddb0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001DataResultDto));
        return xddb0001DataResultDto;
    }
}
