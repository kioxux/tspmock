package cn.com.yusys.yusp.web.server.xdxw0049;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0049.req.Xdxw0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0049.resp.Xdxw0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0049.Xdxw0049Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:智能风控删除通知
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0049:智能风控删除通知")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0049Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0049Resource.class);

    @Autowired
    private Xdxw0049Service xdxw0049Service;

    /**
     * 交易码：xdxw0049
     * 交易描述：智能风控删除通知
     *
     * @param xdxw0049DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("智能风控删除通知")
    @PostMapping("/xdxw0049")
    protected @ResponseBody
    ResultDto<Xdxw0049DataRespDto> xdxw0049(@Validated @RequestBody Xdxw0049DataReqDto xdxw0049DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataReqDto));
        Xdxw0049DataRespDto xdxw0049DataRespDto = new Xdxw0049DataRespDto();// 响应Dto:智能风控删除通知
        ResultDto<Xdxw0049DataRespDto> xdxw0049DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0049DataReqDto获取业务值进行业务逻辑处理
            String survey_serno = xdxw0049DataReqDto.getSurvey_serno();//业务流水号
            if (StringUtils.isEmpty(survey_serno)) {
                xdxw0049DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdxw0049DataResultDto.setMessage(EcbEnum.ECB010001.value);
                return xdxw0049DataResultDto;
            }
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataReqDto));
            xdxw0049DataRespDto = xdxw0049Service.xdxw0049(xdxw0049DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataRespDto));
            // 封装xdxw0049DataResultDto中正确的返回码和返回信息
            String opFlag = xdxw0049DataRespDto.getOpFlag();
            String opMessage = xdxw0049DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdxw0049DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0049DataResultDto.setMessage(opMessage);
            } else {
                xdxw0049DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdxw0049DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, e.getMessage());
            // 封装xdxw0047DataResultDto中异常返回码和返回信息
            xdxw0049DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0049DataResultDto.setMessage(e.getMessage());
            xdxw0049DataResultDto.setCode(e.getErrorCode());
            xdxw0049DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, e.getMessage());
            // 封装xdxw0049DataResultDto中异常返回码和返回信息
            xdxw0049DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0049DataResultDto.setMessage(e.getMessage());
            xdxw0049DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0049DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0049DataRespDto到xdxw0049DataResultDto中
        xdxw0049DataResultDto.setData(xdxw0049DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataRespDto));
        return xdxw0049DataResultDto;
    }
}
