package cn.com.yusys.yusp.service.server.xdxt0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AreaManager;
import cn.com.yusys.yusp.domain.AreaOrg;
import cn.com.yusys.yusp.domain.AreaUser;
import cn.com.yusys.yusp.domain.FbManager;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.UserAndDutyReqDto;
import cn.com.yusys.yusp.dto.UserAndDutyRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.req.Xdxt0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.resp.Xdxt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AreaManagerService;
import cn.com.yusys.yusp.service.AreaOrgService;
import cn.com.yusys.yusp.service.AreaUserService;
import cn.com.yusys.yusp.service.FbManagerService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: yusp-oca模块
 * @类名称: Xdxt0006Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-05-25 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxt0006Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0006Service.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private AreaOrgService areaOrgService;

    @Autowired
    private AreaUserService areaUserService;

    @Autowired
    private AreaManagerService areaManagerService;

    @Autowired
    private FbManagerService fbManagerService;

    /**
     * 分页查询小微客户经理
     *
     * @return
     */
    @Transactional
    public Xdxt0006DataRespDto getXdxt0006(Xdxt0006DataReqDto xdxt0006DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataReqDto));
        Xdxt0006DataRespDto xdxt0006DataRespDto = new Xdxt0006DataRespDto();
        try {
            if (StringUtils.isEmpty(xdxt0006DataReqDto.getDutyName()) && StringUtils.isEmpty(xdxt0006DataReqDto.getDutyNo()) && StringUtils.isEmpty(xdxt0006DataReqDto.getManagerId())) {
                throw BizException.error(null, EcbEnum.ECB010046.key, EcbEnum.ECB010046.value);
            }
            // 岗位编号
            String dutyNo = xdxt0006DataReqDto.getDutyNo();
            // 客户经理名称
            String managerId = xdxt0006DataReqDto.getManagerId();
            // 岗位名称
            String dutyName = xdxt0006DataReqDto.getDutyName();

            // 1、根据客户经理查询岗位信息
            // 调用oca接口查询机构
            AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(managerId);
            // 校验非空
            if (Objects.isNull(adminSmUserDto)) {
                throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户信息！");
            }
            String orgId = adminSmUserDto.getOrgId();
            // XWB02 分中心负责人
            if ("XWB02".equals(dutyNo)) {
                // 1、根据客户经理获取机构信息,根据机构信息查询区域信息
                logger.info("根据机构号【{}】查询区域信息开始", orgId);
                List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
                if (CollectionUtils.isEmpty(areaOrgDtos)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户机构所属区域信息！");
                }
                logger.info("根据机构号【{}】查询区域信息结束,区域编号【{}】", orgId, areaOrgDtos.get(0).getAreaNo());

                // 2、一个机构只属于一个区域，取第一条即可，根据区域编号查询区域负责人信息
                logger.info("根据区域编号【{}】查询区域负责人信息开始", areaOrgDtos.get(0).getAreaNo());
                List<AreaUser> areaUsers = areaUserService.selectByAreaNo(areaOrgDtos.get(0).getAreaNo());
                if (CollectionUtils.isEmpty(areaUsers)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到区域负责人信息！");
                }
                logger.info("区域编号【{}】查询区域负责人信息结束,负责人工号【{}】", orgId, areaUsers.get(0).getUserNo());
                managerId = areaUsers.get(0).getUserNo();
                dutyNo = "";
                dutyName = "";

            } else if ("XWFBBZ01".equals(dutyNo) || "XWFGZC01".equals(dutyNo)) {// XWFBBZ01分部部长，区域所属分部部长 XWFGZC01分管中层，区域所属分管中层

                // 1、根据客户经理获取机构信息,根据机构信息查询区域信息
                logger.info("根据机构号【{}】查询区域信息开始", orgId);
                List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
                if (CollectionUtils.isEmpty(areaOrgDtos)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户机构所属区域信息！");
                }
                logger.info("根据机构号【{}】查询区域信息结束,区域编号【{}】", orgId, areaOrgDtos.get(0).getAreaNo());

                // 2、根据区域编号查询区域信息，获取区域分部长编号
                logger.info("根据区域编号【{}】查询区域信息开始", areaOrgDtos.get(0).getAreaNo());
                AreaManager areaManager = areaManagerService.selectByPrimaryKey(areaOrgDtos.get(0).getAreaNo());
                if (Objects.isNull(areaManager)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到区域信息！");
                }
                logger.info("根据区域编号【{}】查询区域信息结束,区域分部长编号", areaOrgDtos.get(0).getAreaNo(), areaManager.getSubBranch());

                // 3、根据分部编号查询分部部长
                FbManager fbManager = fbManagerService.selectByPrimaryKey(areaManager.getSubBranch());
                if (Objects.isNull(fbManager) || StringUtils.isEmpty(fbManager.getMinisterId())) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到分部信息！");
                }
                managerId = "XWFBBZ01".equals(dutyNo) ? fbManager.getMinisterId() : fbManager.getZcManagerId();

                // 调用oca接口查询机构
                AdminSmUserDto adminSmUserDtoTemp = commonService.getByLoginCode(managerId);
                // 校验非空
                if (Objects.isNull(adminSmUserDtoTemp)) {
                    throw BizException.error(null, EpbEnum.EPB099999.value, "未查询到用户信息！");
                }

                List<cn.com.yusys.yusp.dto.server.xdxt0006.resp.List> lists = new ArrayList<>();
                cn.com.yusys.yusp.dto.server.xdxt0006.resp.List list = new cn.com.yusys.yusp.dto.server.xdxt0006.resp.List();
                list.setActorNo(managerId);
                list.setActorName(adminSmUserDtoTemp.getUserName());
                list.setDutyNo(dutyNo);
                list.setDutyName(dutyName);
                lists.add(list);
                xdxt0006DataRespDto.setList(lists);
                return xdxt0006DataRespDto;
            } else if ("ZHT21".equals(dutyNo) || "XWB06".equals(dutyNo)) {// 角色码： 分管行长：ZHT21 总经理：XWB06
                // 直接根据角色查询用户信息，分管行长与总经理只有一人
                logger.info("分管行长与总经理仅有一人，根据角色查询即可！");
                managerId = "";
            } else {// 其他角色
                managerId = "";
            }

            UserAndDutyReqDto userAndDutyReqDto = new UserAndDutyReqDto();
            userAndDutyReqDto.setDutyName(dutyName);
            userAndDutyReqDto.setDutyNo(dutyNo);
            userAndDutyReqDto.setManagerId(managerId);
            List<UserAndDutyRespDto> userAndDutyRespDtos = commonService.getUserAndDuty(userAndDutyReqDto);
            java.util.List<cn.com.yusys.yusp.dto.server.xdxt0006.resp.List> lists = userAndDutyRespDtos.stream().map(userAndDutyRespDto -> {
                cn.com.yusys.yusp.dto.server.xdxt0006.resp.List list = new cn.com.yusys.yusp.dto.server.xdxt0006.resp.List();
                BeanUtils.copyProperties(userAndDutyRespDto, list);
                return list;
            }).collect(Collectors.toList());
            xdxt0006DataRespDto.setList(lists);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataRespDto));
        return xdxt0006DataRespDto;
    }
}
