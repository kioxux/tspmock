/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrLmtRel;
import cn.com.yusys.yusp.service.CtrLmtRelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLmtRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-28 19:37:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrlmtrel")
public class CtrLmtRelResource {
    private static final Logger log = LoggerFactory.getLogger(CtrLmtRelResource.class);
    @Autowired
    private CtrLmtRelService ctrLmtRelService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrLmtRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrLmtRel> list = ctrLmtRelService.selectAll(queryModel);
        return new ResultDto<List<CtrLmtRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrLmtRel>> index(QueryModel queryModel) {
        List<CtrLmtRel> list = ctrLmtRelService.selectByModel(queryModel);
        return new ResultDto<List<CtrLmtRel>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrLmtRel> show(@PathVariable("pkId") String pkId) {
        CtrLmtRel ctrLmtRel = ctrLmtRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrLmtRel>(ctrLmtRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrLmtRel> create(@RequestBody CtrLmtRel ctrLmtRel) throws URISyntaxException {
        ctrLmtRelService.insert(ctrLmtRel);
        return new ResultDto<CtrLmtRel>(ctrLmtRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrLmtRel ctrLmtRel) throws URISyntaxException {
        int result = ctrLmtRelService.update(ctrLmtRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrLmtRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrLmtRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据个人授信协议编号 查询在途的业务申请（特殊业务、额度项下业务）申请流水号
     * @param map
     * @return
     */
    @PostMapping("/getContnoByLmtCtrNo")
    protected List<String> getContnoByLmtCtrNo(@RequestBody Map map){
        log.info("根据个人授信协议编号查询合同信息..............");
        return ctrLmtRelService.getContnoByLmtCtrNo(map);
    }
}
