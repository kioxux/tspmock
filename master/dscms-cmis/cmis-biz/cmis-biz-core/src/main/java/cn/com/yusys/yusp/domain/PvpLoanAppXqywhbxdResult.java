/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppXqywhbxdResult
 * @类描述: pvp_loan_app_xqywhbxd_result数据实体类
 * @功能描述: 
 * @创建人: xuchao
 * @创建时间: 2021-06-18 13:41:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_loan_app_xqywhbxd_result")
public class PvpLoanAppXqywhbxdResult extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 审批结果 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 10)
	private String approveStatus;
	
	/** 行内贷款无欠本欠息情况 **/
	@Column(name = "RULE01", unique = false, nullable = true, length = 10)
	private String rule01;
	
	/** 本行或他行经营性贷款无欠本欠息情况 **/
	@Column(name = "RULE02", unique = false, nullable = true, length = 10)
	private String rule02;
	
	/** 原周转贷款分类为正常类 **/
	@Column(name = "RULE03", unique = false, nullable = true, length = 10)
	private String rule03;
	
	/** 企业工商信息状态正常，且无工商处罚记录 **/
	@Column(name = "RULE04", unique = false, nullable = true, length = 10)
	private String rule04;
	
	/** 个人和企业无失信被执行情况 **/
	@Column(name = "RULE05", unique = false, nullable = true, length = 10)
	private String rule05;
	
	/** 授信评审部提供的信贷客户内部分类 **/
	@Column(name = "RULE06", unique = false, nullable = true, length = 10)
	private String rule06;
	
	/** 内部评级为BBB级以上 **/
	@Column(name = "RULE07", unique = false, nullable = true, length = 10)
	private String rule07;
	
	/** 查询时间 **/
	@Column(name = "QUERY_TIME", unique = false, nullable = true, length = 20)
	private String queryTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param rule01
	 */
	public void setRule01(String rule01) {
		this.rule01 = rule01;
	}
	
    /**
     * @return rule01
     */
	public String getRule01() {
		return this.rule01;
	}
	
	/**
	 * @param rule02
	 */
	public void setRule02(String rule02) {
		this.rule02 = rule02;
	}
	
    /**
     * @return rule02
     */
	public String getRule02() {
		return this.rule02;
	}
	
	/**
	 * @param rule03
	 */
	public void setRule03(String rule03) {
		this.rule03 = rule03;
	}
	
    /**
     * @return rule03
     */
	public String getRule03() {
		return this.rule03;
	}
	
	/**
	 * @param rule04
	 */
	public void setRule04(String rule04) {
		this.rule04 = rule04;
	}
	
    /**
     * @return rule04
     */
	public String getRule04() {
		return this.rule04;
	}
	
	/**
	 * @param rule05
	 */
	public void setRule05(String rule05) {
		this.rule05 = rule05;
	}
	
    /**
     * @return rule05
     */
	public String getRule05() {
		return this.rule05;
	}
	
	/**
	 * @param rule06
	 */
	public void setRule06(String rule06) {
		this.rule06 = rule06;
	}
	
    /**
     * @return rule06
     */
	public String getRule06() {
		return this.rule06;
	}
	
	/**
	 * @param rule07
	 */
	public void setRule07(String rule07) {
		this.rule07 = rule07;
	}
	
    /**
     * @return rule07
     */
	public String getRule07() {
		return this.rule07;
	}
	
	/**
	 * @param queryTime
	 */
	public void setQueryTime(String queryTime) {
		this.queryTime = queryTime;
	}
	
    /**
     * @return queryTime
     */
	public String getQueryTime() {
		return this.queryTime;
	}


}