package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtEgcInfo
 * @类描述: lmt_egc_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-10 23:15:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtEgcInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 联系人电话 **/
	private String linkPhone;
	
	/** 工作单位 **/
	private String workUnit;
	
	/** 共借人关系 **/
	private String commonDebitRela;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param linkPhone
	 */
	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone == null ? null : linkPhone.trim();
	}
	
    /**
     * @return LinkPhone
     */	
	public String getLinkPhone() {
		return this.linkPhone;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit == null ? null : workUnit.trim();
	}
	
    /**
     * @return WorkUnit
     */	
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param commonDebitRela
	 */
	public void setCommonDebitRela(String commonDebitRela) {
		this.commonDebitRela = commonDebitRela == null ? null : commonDebitRela.trim();
	}
	
    /**
     * @return CommonDebitRela
     */	
	public String getCommonDebitRela() {
		return this.commonDebitRela;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}