/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtReplyChgCond;
import cn.com.yusys.yusp.service.LmtReplyChgCondService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgCondResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-26 14:34:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplychgcond")
public class LmtReplyChgCondResource {
    @Autowired
    private LmtReplyChgCondService lmtReplyChgCondService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyChgCond>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyChgCond> list = lmtReplyChgCondService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyChgCond>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyChgCond>> index(QueryModel queryModel) {
        List<LmtReplyChgCond> list = lmtReplyChgCondService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyChgCond>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyChgCond> show(@PathVariable("pkId") String pkId) {
        LmtReplyChgCond lmtReplyChgCond = lmtReplyChgCondService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyChgCond>(lmtReplyChgCond);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<Integer> create(@RequestBody LmtReplyChgCond lmtReplyChgCond) throws URISyntaxException {
        int insertRow = lmtReplyChgCondService.insert(lmtReplyChgCond);
        return new ResultDto<>(insertRow);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyChgCond lmtReplyChgCond) throws URISyntaxException {
        int result = lmtReplyChgCondService.update(lmtReplyChgCond);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyChgCondService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyChgCondService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    @PostMapping("/queryByCondition")
    protected ResultDto<List<LmtReplyChgCond>> queryByCondition(@RequestBody QueryModel queryModel) {
        String serno = (String) queryModel.getCondition().get("serno");
        String condType = (String) queryModel.getCondition().get("condType");
        HashMap<String,String> queryMap = new HashMap<>();
        queryMap.put("serno",serno);
        queryMap.put("condType",condType);
        List<LmtReplyChgCond> list = lmtReplyChgCondService.queryLmtReplyChgCondByParams(queryMap);
        return new ResultDto<>(list);
    }
}
