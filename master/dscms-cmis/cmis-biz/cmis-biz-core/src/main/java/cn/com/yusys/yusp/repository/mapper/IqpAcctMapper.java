/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpAcct;
import cn.com.yusys.yusp.domain.PvpTruPayInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpAcctMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2020-12-12 11:00:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpAcctMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    IqpAcct selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpAcct> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(IqpAcct record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(IqpAcct record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(IqpAcct record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(IqpAcct record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);
    /**
     * 通过业务申请主键进行逻辑删除，即修改opr_type
     * @param delMap
     * @return
     */
    int updateByParams(Map delMap);

    List<IqpAcct> selectByIqpSerno(@Param("iqpSernoOld") String iqpSernoOld);

    /**
     * 通过业务申请流水号查询生效的账户信息数据
     * @param iqpSerno
     * @return
     */
    List<IqpAcct> selectAffectInfoByIqpSerno(@Param("iqpSerno") String iqpSerno);

    /**
     * 通过入参集合查询数据信息
     * @param param
     * @return
     */
    List<IqpAcct> selectAffectInfoByParam(Map param);

    /**
     * @方法名称: selectByKey
     * @方法描述: 根据字段查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectCountByKey(@Param("acctNo") String acctNo);

    /**
     * @方法名称: updateAcctStatusByContNoSelective
     * @方法描述: 审批通过后 更新账户状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateAcctStatusByContNoSelective(Map paramMap);

    /**
     * @方法名称: insertByContNoAfterApp
     * @方法描述: 审批通过后 新增账户信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertByContNoAfterApp(Map paramMap);

    /**
     * 根据借据标号查询借据信息
     * @param acctNo
     * @return
     */
    IqpAcct getIqpAcct(String acctNo);
}
