package cn.com.yusys.yusp.web.server.xdtz0042;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0042.req.Xdtz0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.resp.Xdtz0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0042.Xdtz0042Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:申请人在本行当前逾期贷款数量
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0042:申请人在本行当前逾期贷款数量")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0042Resource.class);

    @Autowired
    private Xdtz0042Service xdtz0042Service;

    /**
     * 交易码：xdtz0042
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param xdtz0042DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("申请人在本行当前逾期贷款数量")
    @PostMapping("/xdtz0042")
    protected @ResponseBody
    ResultDto<Xdtz0042DataRespDto> xdtz0042(@Validated @RequestBody Xdtz0042DataReqDto xdtz0042DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataReqDto));
        Xdtz0042DataRespDto xdtz0042DataRespDto = null;// 响应Dto:申请人在本行当前逾期贷款数量
        ResultDto<Xdtz0042DataRespDto> xdtz0042DataResultDto = new ResultDto<>();
        String certNo = xdtz0042DataReqDto.getCertNo();
        // 从xdtz0042DataReqDto获取业务值进行业务逻辑处理
        try {
            // 调用xdtz0042Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, certNo);
            xdtz0042DataRespDto = Optional.ofNullable(xdtz0042Service.getAccByCertNo(certNo)).orElse(new Xdtz0042DataRespDto(0));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataRespDto));
            // 封装xdtz0042DataResultDto中正确的返回码和返回信息
            xdtz0042DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0042DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, e.getMessage());
            // 封装xdtz0042DataResultDto中异常返回码和返回信息
            xdtz0042DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0042DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0042DataRespDto到xdtz0042DataResultDto中
        xdtz0042DataResultDto.setData(xdtz0042DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataResultDto));
        return xdtz0042DataResultDto;
    }
}
