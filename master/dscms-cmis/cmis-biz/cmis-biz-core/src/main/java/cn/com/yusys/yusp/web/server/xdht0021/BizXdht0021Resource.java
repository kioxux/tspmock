package cn.com.yusys.yusp.web.server.xdht0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0021.req.Xdht0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0021.resp.Xdht0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0021.Xdht0021Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:合同签订
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0021:合同签订")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0021Resource.class);

    @Autowired
    private Xdht0021Service xdht0021Service;

    /**
     * 交易码：xdht0021
     * 交易描述：合同签订
     *
     * @param xdht0021DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订")
    @PostMapping("/xdht0021")
    protected @ResponseBody
    ResultDto<Xdht0021DataRespDto> xdht0021(@Validated @RequestBody Xdht0021DataReqDto xdht0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataReqDto));
        Xdht0021DataRespDto xdht0021DataRespDto = new Xdht0021DataRespDto();// 响应Dto:合同签订
        ResultDto<Xdht0021DataRespDto> xdht0021DataResultDto = new ResultDto<>();
        try {
            String contNo = xdht0021DataReqDto.getContNo();
            if (StringUtils.isBlank(contNo)) {
                xdht0021DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0021DataResultDto.setMessage("合同编号【contNo】不能为空！");
                return xdht0021DataResultDto;
            }

            // 从xdht0021DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataReqDto));
            xdht0021DataRespDto = xdht0021Service.signCont(xdht0021DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataRespDto));
            // 封装xdht0021DataResultDto中正确的返回码和返回信息
            String opFlag = xdht0021DataRespDto.getOpFlag();
            String opMessage = xdht0021DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//出账失败
                xdht0021DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0021DataResultDto.setMessage(opMessage);
            } else {
                xdht0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdht0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, e.getMessage());
            // 封装xdht0021DataResultDto中异常返回码和返回信息
            xdht0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0021DataRespDto到xdht0021DataResultDto中
        xdht0021DataResultDto.setData(xdht0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataRespDto));
        return xdht0021DataResultDto;
    }
}
