/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpWriteoffDetl;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpWriteoffDetlMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: IqpWriteoffDetlService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:31:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpWriteoffDetlService {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(IqpWriteoffAppService.class);

    @Autowired
    private IqpWriteoffDetlMapper iqpWriteoffDetlMapper;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    // 呆账核销信息服务接口
    @Autowired
    private IqpWriteoffAppService iqpWriteoffAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpWriteoffDetl selectByPrimaryKey(String pkId) {
        return iqpWriteoffDetlMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<IqpWriteoffDetl> selectAll(QueryModel model) {
        List<IqpWriteoffDetl> records = (List<IqpWriteoffDetl>) iqpWriteoffDetlMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpWriteoffDetl> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpWriteoffDetl> list = iqpWriteoffDetlMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpWriteoffDetl record) {
        return iqpWriteoffDetlMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpWriteoffDetl record) {
        return iqpWriteoffDetlMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpWriteoffDetl record) {
        return iqpWriteoffDetlMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpWriteoffDetl record) {
        return iqpWriteoffDetlMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpWriteoffDetlMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpWriteoffDetlMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveIqpWriteOffDetailLeadInfo
     * @方法描述: 根据向导页面表单信息保存至核销申请明细信息中
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public Map saveIqpWriteOffDetailLeadInfo(IqpWriteoffDetl iqpWriteoffDetl) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        try {
            log.info("根据向导页面表单信息保存至核销申请明细信息中");
            String iqpSerno = iqpWriteoffDetl.getIqpSerno();
            String billNo = iqpWriteoffDetl.getBillNo();
            String pkId = iqpWriteoffDetl.getPkId();

            // 检查是否存在同一业务流水号中存在相同借据号的新增数据
            HashMap<String, String> paramsMap = new HashMap<String, String>();
            paramsMap.put("iqpSerno", iqpSerno);
            paramsMap.put("billNo", billNo);
            paramsMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramsMap.put("pkId", pkId);
            List<IqpWriteoffDetl> iqpArrayList = iqpWriteoffDetlMapper.selectIqpWriteOffDetailsListByParams(paramsMap);
            if (iqpArrayList != null && iqpArrayList.size() > 0) {
                rtnCode = EcbEnum.E_IQP_CHG_INSERT_EXISTS.key;
                rtnMsg = EcbEnum.E_IQP_CHG_INSERT_EXISTS.value;
                return rtnData;
            }
            if (StringUtils.nonBlank(pkId)) {
                log.info(String.format("根据向导页面表单信息保存, 主键%s存在，更新表单信息", pkId));
                int count = update(iqpWriteoffDetl);
                if (count != 1) {
                    rtnCode = EcbEnum.E_IQP_CHG_UPDATE_ERROR.key;
                    rtnMsg = EcbEnum.E_IQP_CHG_UPDATE_ERROR.value;
                }
                log.info(String.format("更新核销申请信息中的汇总金额,流水号%s", iqpSerno));
                iqpWriteoffAppService.updateIqpWriteOffAppTotalInfo(iqpSerno);
                return rtnData;
            }

            iqpWriteoffDetl.setPkId(UUID.randomUUID().toString());
            // 数据操作标志为新增
            iqpWriteoffDetl.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

            log.info(String.format("保存核销明细信息%s-获取当前登录用户数据", iqpSerno));
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_EXCEPTION.value;
                return rtnData;
            } else {
                iqpWriteoffDetl.setInputId(userInfo.getLoginCode());
                iqpWriteoffDetl.setInputBrId(userInfo.getOrg().getCode());
                iqpWriteoffDetl.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                iqpWriteoffDetl.setUpdId(userInfo.getLoginCode());
                iqpWriteoffDetl.setUpdBrId(userInfo.getOrg().getCode());
                iqpWriteoffDetl.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }


            log.info(String.format("保存核销明细信息,流水号%s", iqpSerno));
            int count = iqpWriteoffDetlMapper.insert(iqpWriteoffDetl);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }

            log.info(String.format("更新核销申请信息中的汇总金额,流水号%s", iqpSerno));
            iqpWriteoffAppService.updateIqpWriteOffAppTotalInfo(iqpSerno);
            log.info(String.format("保存核销明细信息成功,流水号%s", iqpSerno));

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存核销明细信息出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("iqpWriteoffDetl", iqpWriteoffDetl);
        }
        return rtnData;
    }

    /**
     * @方法名称: deleteIqpWriteOffDetailLogic
     * @方法描述: 根据主键对核销申请明细信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 对核销申请明细信息进行逻辑删除，更新核销申请汇总金额
     */
    @Transactional
    public Map deleteIqpWriteOffDetailLogic(String pkId) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        String iqpSerno = null;
        try {
            log.info("根据主键对核销申请明细信息进行逻辑删除");
            log.info(String.format("根据主键%s查询核销明细信息", pkId));
            IqpWriteoffDetl iqpWriteoffDetl = iqpWriteoffDetlMapper.selectByPrimaryKey(pkId);
            iqpWriteoffDetl.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            log.info(String.format("根据主键%s对查询核销明细信息进行逻辑删除", pkId));
            int updateCount = iqpWriteoffDetlMapper.updateByPrimaryKey(iqpWriteoffDetl);
            if (updateCount != 1) {
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",删除失败！");
            }
            iqpSerno = iqpWriteoffDetl.getIqpSerno();
            log.info(String.format("更新核销申请信息中的汇总金额,流水号%s", iqpSerno));
            iqpWriteoffAppService.updateIqpWriteOffAppTotalInfo(iqpSerno);
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("逻辑删除核销明细信息出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;

    }

    /**
     * @方法名称: updateIqpWriteOffDetail
     * @方法描述: 更新核销明细详情信息，并更新核销申请信息的汇总金额
     * @参数与返回说明:
     * @算法描述: 可以获取更新表单数据，与原数据进行比对，如果金额类型数据发生变化，则更新核销申请汇总金额，需要domain重写比较方法
     */
    @Transactional
    public Map updateIqpWriteOffDetail(IqpWriteoffDetl iqpWriteOffDetail) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        String iqpSerno = null;
        try {

            // 更新表单数据
            log.info(String.format("更新核销明细信息,主键%s", iqpWriteOffDetail.getPkId()));
            iqpWriteoffDetlMapper.updateByPrimaryKey(iqpWriteOffDetail);

            iqpSerno = iqpWriteOffDetail.getPkId();
            log.info(String.format("获取核销明细信息的主键,流水号%s", iqpSerno));

            log.info(String.format("更新核销申请信息中的汇总金额,流水号%s", iqpSerno));
            iqpWriteoffAppService.updateIqpWriteOffAppTotalInfo(iqpSerno);

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("更新核销明细详情信息出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;

    }

    /**
     * @方法名称: selectIqpWriteOffDetailsListByParams
     * @方法描述: 根据向导页面表单信息保存至核销申请明细信息中
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<IqpWriteoffDetl> selectIqpWriteOffDetailsListByParams(HashMap<String, String> queryMap) {
        return iqpWriteoffDetlMapper.selectIqpWriteOffDetailsListByParams(queryMap);
    }

    /**
     * @方法名称: deleteIqpWriteOffDetailsByIqpSerno
     * @方法描述: 根据呆账核销申请流水号逻辑删除核算明细列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public void deleteIqpWriteOffDetailsByIqpSerno(String iqpSerno, User userInfo) throws Exception{
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("iqpSerno",iqpSerno);
        List<IqpWriteoffDetl> iqpWriteoffDetlList = iqpWriteoffDetlMapper.selectIqpWriteOffDetailsListByParams(queryMap);
        for (IqpWriteoffDetl iqpWriteoffDetl: iqpWriteoffDetlList) {
            iqpWriteoffDetl.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            iqpWriteoffDetl.setUpdId(userInfo.getLoginCode());
            iqpWriteoffDetl.setUpdBrId(userInfo.getOrg().getCode());
            iqpWriteoffDetl.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            iqpWriteoffDetlMapper.updateByPrimaryKeySelective(iqpWriteoffDetl);
        }
    }

}
