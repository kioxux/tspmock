package cn.com.yusys.yusp.web.server.xdht0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0002.Xdht0002Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:银承协议查询接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0002:银承协议查询接口")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0002Resource.class);

    @Autowired
    private Xdht0002Service xdht0002Sevice;
    /**
     * 交易码：xdht0002
     * 交易描述：银承协议查询接口
     *
     * @param xdht0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("银承协议查询接口")
    @PostMapping("/xdht0002")
    protected @ResponseBody
    ResultDto<Xdht0002DataRespDto> xdht0002(@Validated @RequestBody Xdht0002DataReqDto xdht0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataReqDto));
        Xdht0002DataRespDto xdht0002DataRespDto = new Xdht0002DataRespDto();// 响应Dto:银承协议查询接口
        ResultDto<Xdht0002DataRespDto> xdht0002DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataReqDto));
            xdht0002DataRespDto = xdht0002Sevice.xdht0002(xdht0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataRespDto));
            // 封装xdht0002DataResultDto中正确的返回码和返回信息
            xdht0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, e.getMessage());
            // 封装xdht0002DataResultDto中异常返回码和返回信息
            xdht0002DataResultDto.setCode(e.getErrorCode());
            xdht0002DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, e.getMessage());
            // 封装xdht0002DataResultDto中异常返回码和返回信息
            xdht0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0002DataRespDto到xdht0002DataResultDto中
        xdht0002DataResultDto.setData(xdht0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataRespDto));
        return xdht0002DataResultDto;
    }
}
