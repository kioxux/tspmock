package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAdjustDetail
 * @类描述: lmt_adjust_detail数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-09 23:10:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtAdjustDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信申请流水号 **/
	private String lmtSerno;
	
	/** 集团调剂申请概述 **/
	private String grpAdjustAppMemo;
	
	/** 本次授信额度调剂内容 **/
	private String lmtAdjustContent;
	
	/** 授信细化理由 **/
	private String lmtAdjustResn;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param grpAdjustAppMemo
	 */
	public void setGrpAdjustAppMemo(String grpAdjustAppMemo) {
		this.grpAdjustAppMemo = grpAdjustAppMemo == null ? null : grpAdjustAppMemo.trim();
	}
	
    /**
     * @return GrpAdjustAppMemo
     */	
	public String getGrpAdjustAppMemo() {
		return this.grpAdjustAppMemo;
	}
	
	/**
	 * @param lmtAdjustContent
	 */
	public void setLmtAdjustContent(String lmtAdjustContent) {
		this.lmtAdjustContent = lmtAdjustContent == null ? null : lmtAdjustContent.trim();
	}
	
    /**
     * @return LmtAdjustContent
     */	
	public String getLmtAdjustContent() {
		return this.lmtAdjustContent;
	}
	
	/**
	 * @param lmtAdjustResn
	 */
	public void setLmtAdjustResn(String lmtAdjustResn) {
		this.lmtAdjustResn = lmtAdjustResn == null ? null : lmtAdjustResn.trim();
	}
	
    /**
     * @return LmtAdjustResn
     */	
	public String getLmtAdjustResn() {
		return this.lmtAdjustResn;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}