package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccpEntrustAcc
 * @类描述: aspl_accp_entrust数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-04 14:09:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AsplAccpEntrustAccDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 合同编号 **/
	private String contNo;

	/** 合同状态 **/
	private String contStatus;

	/** 借据编号 **/
	private String billNo;

	/** 借据金额 **/
	private BigDecimal billAmt;

	/** 借据金额 **/
	private BigDecimal billBalance;

	/** 借据起始日 **/
	private String billStartDate;

	/** 借据到期日 **/
	private String billEndDate;

	/** 借据状态 **/
	private String billStatus;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public BigDecimal getBillAmt() {
		return billAmt;
	}

	public void setBillAmt(BigDecimal billAmt) {
		this.billAmt = billAmt;
	}

	public BigDecimal getBillBalance() {
		return billBalance;
	}

	public void setBillBalance(BigDecimal billBalance) {
		this.billBalance = billBalance;
	}

	public String getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}

	public String getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

}