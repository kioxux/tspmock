/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtReplyChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtReplyChgSub;
import cn.com.yusys.yusp.repository.mapper.LmtReplyChgSubMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgSubService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 10:22:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyChgSubService {

    @Autowired
    private LmtReplyChgSubMapper lmtReplyChgSubMapper;

    @Autowired
    private LmtReplyChgService lmtReplyChgService;

    @Autowired
    private LmtReplyChgSubPrdService lmtReplyChgSubPrdService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public LmtReplyChgSub selectByPrimaryKey(String pkId) {
        return lmtReplyChgSubMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<LmtReplyChgSub> selectAll(QueryModel model) {
        List<LmtReplyChgSub> records = (List<LmtReplyChgSub>) lmtReplyChgSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyChgSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyChgSub> list = lmtReplyChgSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyChgSub record) {
        return lmtReplyChgSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyChgSub record) {
        return lmtReplyChgSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(LmtReplyChgSub record) {
        return lmtReplyChgSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyChgSub record) {
        return lmtReplyChgSubMapper.updateSelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyChgSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyChgSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：insertSelectiveList
     * @方法描述：批量插入变更分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-26 下午 4:59
     * @修改记录：修改时间 修改人员  修改原因
     */
    public int insertList(List<LmtReplyChgSub> lmtReplyChgSubList) {
        return lmtReplyChgSubMapper.insertList(lmtReplyChgSubList);
    }

    /**
     * @方法名称：queryLmtReplyChgSubByParams
     * @方法描述：通过查询条件查询数据
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-05-03 14:44:44
     * @修改记录：修改时间 修改人员  修改原因
     */
    public List<LmtReplyChgSub> queryLmtReplyChgSubByParams(HashMap<String, String> queryMap) {
        return lmtReplyChgSubMapper.queryLmtReplyChgSubByParams(queryMap);
    }

    /**
     * @方法名称：updateLmtReplyChgSub
     * @方法描述：更新授信批复变更分项
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-05-03 14:44:44
     * @修改记录：修改时间 修改人员  修改原因
     */
    public Integer updateLmtReplyChgSub(LmtReplyChgSub record) {
        Integer result = 0;
        result = lmtReplyChgSubMapper.updateSelective(record);
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("serno", record.getSerno());
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyChgSub> lmtReplyChgSubList = this.queryLmtReplyChgSubByParams(queryMap);
        LmtReplyChg lmtReplyChg = lmtReplyChgService.queryLmtReplyChgBySerno(record.getSerno());
        BigDecimal lowRiskTotalAmt = new BigDecimal("0.0");
        BigDecimal openTotalAmt = new BigDecimal("0.0");
        for (LmtReplyChgSub lmtReplyChgSub : lmtReplyChgSubList ) {
            if(CmisCommonConstants.GUAR_MODE_60.equals(lmtReplyChgSub.getGuarMode())){
                lowRiskTotalAmt = lowRiskTotalAmt.add(lmtReplyChgSub.getLmtAmt());
            }else {
                openTotalAmt = openTotalAmt.add(lmtReplyChgSub.getLmtAmt());
            }
        }
        lmtReplyChg.setLowRiskTotalLmtAmt(lowRiskTotalAmt);
        lmtReplyChg.setOpenTotalLmtAmt(openTotalAmt);
        int result2 = lmtReplyChgService.update(lmtReplyChg);
        if(result2 != 1 || result != 1 ){
            BizException.error(null, EcbEnum.ECB010066.key, EcbEnum.ECB010066.value);
        }
        return result;
    }

    public int updateLmtReplyChgSubOprDeleteByChg(String serno) {
        Integer result = 0;
        HashMap hashMap = new HashMap();
        hashMap.put("serno",serno);
        hashMap.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplyChgSub> listSub = this.queryLmtReplyChgSubByParams(hashMap);
        for(LmtReplyChgSub lmtReplyChgSub : listSub){
            LmtReplyChgSub newLmtReplyChgSub = new LmtReplyChgSub();
            newLmtReplyChgSub.setPkId(lmtReplyChgSub.getPkId());
            newLmtReplyChgSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
            updateSelective(newLmtReplyChgSub);
            result++;
            lmtReplyChgSubPrdService.updateLmtReplyChgSubPrdOprDeleteByChgSub(lmtReplyChgSub.getReplySubSerno());
        }
        return result;
    }
}
