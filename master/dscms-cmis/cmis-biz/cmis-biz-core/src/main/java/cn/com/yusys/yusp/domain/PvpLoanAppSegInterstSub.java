/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppSegInterstSub
 * @类描述: pvp_loan_app_seg_interst_sub数据实体类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-04-21 10:25:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_loan_app_seg_interst_sub")
public class PvpLoanAppSegInterstSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 贷款出账流水号 **/
	@Column(name = "LOAN_PVP_SEQ", unique = false, nullable = true, length = 40)
	private String loanPvpSeq;
	
	/** 分段起始日 **/
	@Column(name = "SEG_STARTDATE", unique = false, nullable = true, length = 40)
	private String segStartdate;
	
	/** 分段到期日 **/
	@Column(name = "SEG_ENDDATE", unique = false, nullable = true, length = 40)
	private String segEnddate;
	
	/** 利率调整方式 **/
	@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 40)
	private String rateAdjMode;
	
	/** LPR授信利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;
	
	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;
	
	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;
	
	/** 执行利率（年） **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;
	
	/** 利率调整选项 **/
	@Column(name = "RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
	private String rateAdjType;
	
	/** 下一次利率调整间隔 **/
	@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private String nextRateAdjInterval;
	
	/** 下一次调整间隔单位 **/
	@Column(name = "NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 5)
	private String nextRateAdjUnit;
	
	/** 第一次调整日 **/
	@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 20)
	private String firstAdjDate;
	
	/** 结息间隔周期 **/
	@Column(name = "EI_INTERVAL_CYCLE", unique = false, nullable = true, length = 10)
	private String eiIntervalCycle;
	
	/** 结息间隔周期单位 **/
	@Column(name = "EI_INTERVAL_UNIT", unique = false, nullable = true, length = 5)
	private String eiIntervalUnit;
	
	/** 扣款日 **/
	@Column(name = "DEDUCT_DAY", unique = false, nullable = true, length = 20)
	private String deductDay;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param loanPvpSeq
	 */
	public void setLoanPvpSeq(String loanPvpSeq) {
		this.loanPvpSeq = loanPvpSeq;
	}
	
    /**
     * @return loanPvpSeq
     */
	public String getLoanPvpSeq() {
		return this.loanPvpSeq;
	}
	
	/**
	 * @param segStartdate
	 */
	public void setSegStartdate(String segStartdate) {
		this.segStartdate = segStartdate;
	}
	
    /**
     * @return segStartdate
     */
	public String getSegStartdate() {
		return this.segStartdate;
	}
	
	/**
	 * @param segEnddate
	 */
	public void setSegEnddate(String segEnddate) {
		this.segEnddate = segEnddate;
	}
	
    /**
     * @return segEnddate
     */
	public String getSegEnddate() {
		return this.segEnddate;
	}
	
	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}
	
    /**
     * @return rateAdjMode
     */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}
	
	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}
	
    /**
     * @return lprRateIntval
     */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}
	
	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}
	
    /**
     * @return curtLprRate
     */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}
	
	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}
	
    /**
     * @return rateFloatPoint
     */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return execRateYear
     */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}
	
    /**
     * @return rateAdjType
     */
	public String getRateAdjType() {
		return this.rateAdjType;
	}
	
	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}
	
    /**
     * @return nextRateAdjInterval
     */
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}
	
	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit;
	}
	
    /**
     * @return nextRateAdjUnit
     */
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}
	
	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}
	
    /**
     * @return firstAdjDate
     */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}
	
	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle;
	}
	
    /**
     * @return eiIntervalCycle
     */
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}
	
	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit;
	}
	
    /**
     * @return eiIntervalUnit
     */
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}
	
	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay;
	}
	
    /**
     * @return deductDay
     */
	public String getDeductDay() {
		return this.deductDay;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}