package cn.com.yusys.yusp.dto;

public class LmtGrpMemDto {
    private static final long serialVersionUID = 1L;
    // 管护客户经理
    private String managerId;
    // 所属机构
    private String managerBrId;
    // 成员客户类型
    private String cusType;
    // 成员客户编号
    private String cusId;
    // 单一客户测算限额
    private String singleEvalLimit;
    // 成员客户名称
    private String cusName;
    // 债项登记
    private String debtLevel;
    // 违约风险暴露ead
    private String ead;
    // 违约损失率lgd
    private String lgd;
    private String grpSerno;

    public String getGrpSerno() {
        return grpSerno;
    }

    public void setGrpSerno(String grpSerno) {
        this.grpSerno = grpSerno;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getSingleEvalLimit() {
        return singleEvalLimit;
    }

    public void setSingleEvalLimit(String singleEvalLimit) {
        this.singleEvalLimit = singleEvalLimit;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDebtLevel() {
        return debtLevel;
    }

    public void setDebtLevel(String debtLevel) {
        this.debtLevel = debtLevel;
    }

    public String getEad() {
        return ead;
    }

    public void setEad(String ead) {
        this.ead = ead;
    }

    public String getLgd() {
        return lgd;
    }

    public void setLgd(String lgd) {
        this.lgd = lgd;
    }
}
