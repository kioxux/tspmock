package cn.com.yusys.yusp.service.client.bsp.core.ln3104;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/21 10:33
 * @desc 客户账交易明细查询
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Ln3104Service {
    private static final Logger logger = LoggerFactory.getLogger(Ln3104Service.class);
    // 1）注入：BSP封装调用核心系统的接口(Ln开头)
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    /**
     * @param ln3104ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto
     * @author hubp
     * @date 2021/5/21 10:41
     * @version 1.0.0
     * @desc 客户账交易明细查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public ResultDto<Ln3104RespDto> ln3104(Ln3104ReqDto ln3104ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(ln3104ReqDto));
        ResultDto<Ln3104RespDto> ln3104ResultDto = dscms2CoreLnClientService.ln3104(ln3104ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(ln3104ResultDto));
//        String ln3104Code = Optional.ofNullable(ln3104ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
//        String ln3104Meesage = Optional.ofNullable(ln3104ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
//        Ln3104RespDto ln3104RespDto = null;
//        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3104ResultDto.getCode())) {
//            //  获取相关的值并解析
//            ln3104RespDto = ln3104ResultDto.getData();
//        } else {
//            //  抛出错误异常
//            //throw new YuspException(ln3104Code, ln3104Meesage);
//        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value);
        return ln3104ResultDto;
    }
}
