/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.CusIndivUnitDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.repository.mapper.RptSpdAnysSxkdMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysSxkdService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-06-23 16:15:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptSpdAnysSxkdService {

    @Autowired
    private RptSpdAnysSxkdMapper rptSpdAnysSxkdMapper;
    @Autowired
    private RptCptlSituCorpService rptCptlSituCorpService;
    @Autowired
    private RptCptlSituLegalRepreLoanService rptCptlSituLegalRepreLoanService;
    @Autowired
    private RptBasicInfoPersFamilyAssetsService rptBasicInfoPersFamilyAssetsService;
    @Autowired
    private RptOperationService rptOperationService;
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private LmtAppService lmtAppService;
    @Autowired
    private RptBasicInfoRealEstateService rptBasicInfoRealEstateService;
    @Autowired
    private RptLmtRepayAnysGuarPldDetailService rptLmtRepayAnysGuarPldDetailService;
    @Autowired
    private LmtAppSubService lmtAppSubService;
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;
    @Autowired
    private RptOperProfitSituationService profitSituationService;
    @Autowired
    private RptOperEnergySalesService rptOperEnergySalesService;
    @Autowired
    private RptFncSituBsAnysService rptFncSituBsAnysService;

    @Autowired
    private RptBasicInfoAssoService rptBasicInfoAssoService;
    @Autowired
    private RptFncSituBsService rptFncSituBsService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private RptOperProfitSituationService rptOperProfitSituationService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptSpdAnysSxkd selectByPrimaryKey(String serno) {
        return rptSpdAnysSxkdMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<RptSpdAnysSxkd> selectAll(QueryModel model) {
        List<RptSpdAnysSxkd> records = (List<RptSpdAnysSxkd>) rptSpdAnysSxkdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptSpdAnysSxkd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptSpdAnysSxkd> list = rptSpdAnysSxkdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptSpdAnysSxkd record) {
        return rptSpdAnysSxkdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptSpdAnysSxkd record) {
        return rptSpdAnysSxkdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptSpdAnysSxkd record) {
        return rptSpdAnysSxkdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptSpdAnysSxkd record) {
        return rptSpdAnysSxkdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return rptSpdAnysSxkdMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptSpdAnysSxkdMapper.deleteByIds(ids);
    }

    /**
     * 评分卡自动打分
     *
     * @param params
     * @return
     */
    public int autoValue(Map params) {
        String serno = params.get("serno").toString();
        int count = 0;
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        String rptType = params.get("rptType").toString();
        //客户经理类别: 01 主办客户经理，02协办客户经理
        String managerType = params.get("managerType").toString();
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        List<RptCptlSituCorp> rptCptlSituCorpList = rptCptlSituCorpService.selectByModel(model);
        //第1项自动打分
        int value1 = 0;
        BigDecimal curAmt = BigDecimal.ZERO;
        int totalTimes = 0;
        int totalDebtTimes = 0;
        int totalOverDueTimes = 0;
        if (CollectionUtils.nonEmpty(rptCptlSituCorpList)) {
            for (RptCptlSituCorp rptCptlSituCorp : rptCptlSituCorpList) {
                int debtTimes = rptCptlSituCorp.getDebtTimes();
                int overdueTimes = rptCptlSituCorp.getOverdueTimes();
                totalDebtTimes += debtTimes;
                totalOverDueTimes += overdueTimes;
                totalTimes += debtTimes + overdueTimes;
                curAmt = curAmt.add(rptCptlSituCorp.getCurMonthAmt());
            }
            if (totalTimes == 0) {
                value1 = 9;
            } else if (totalOverDueTimes == 0 && totalDebtTimes <= 3) {
                value1 = 7;
            } else if (totalOverDueTimes == 0 && totalDebtTimes > 3 && totalDebtTimes <= 6) {
                value1 = 5;
            } else if (totalOverDueTimes <= 2 && totalDebtTimes <= 6) {
                value1 = 3;
            } else {
                value1 = 0;
            }
        } else {
            value1 = 9;
        }
        //第2项自动打分
        int value2 = 0;
        List<RptCptlSituLegalRepreLoan> rptCptlSituLegalRepreLoanList = rptCptlSituLegalRepreLoanService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptCptlSituLegalRepreLoanList)) {
            List<RptCptlSituLegalRepreLoan> controlList = new ArrayList<>();
            for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : rptCptlSituLegalRepreLoanList) {
                if ("02".equals(rptCptlSituLegalRepreLoan.getEnterprisesRelation())) {
                    controlList.add(rptCptlSituLegalRepreLoan);
                }
            }
            int debtTimes = 0;
            int creditCardTimes = 0;
            if (CollectionUtils.nonEmpty(controlList)) {
                for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : controlList) {
                    //融资品种: 1贷款 2贷记卡 3准贷记卡
                    if (rptCptlSituLegalRepreLoan.getCptlType().equals("1")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("2")) {
                        creditCardTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("3")) {
                        creditCardTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("4")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("5")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    } else if (rptCptlSituLegalRepreLoan.getCptlType().equals("6")) {
                        debtTimes += rptCptlSituLegalRepreLoan.getOverdueTimes();
                    }
                }
                if (debtTimes == 0 && creditCardTimes == 0) {
                    value2 = 9;
                } else if (debtTimes <= 2 && (creditCardTimes + debtTimes) <= 5) {
                    value2 = 7;
                } else if (debtTimes <= 2 && (creditCardTimes + debtTimes) <= 7) {
                    value2 = 5;
                } else if (debtTimes <= 3 && (creditCardTimes + debtTimes) <= 9) {
                    value2 = 3;
                } else {
                    value2 = 0;
                }
            } else {
                value2 = 9;
            }
        } else {
            value2 = 9;
        }
        //第3项自动打分
        int value3 = 0;
        if (CollectionUtils.nonEmpty(rptCptlSituLegalRepreLoanList)) {
            for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : rptCptlSituLegalRepreLoanList) {
                //实控人及其配偶
                if ("02".equals(rptCptlSituLegalRepreLoan.getEnterprisesRelation()) || "04".equals(rptCptlSituLegalRepreLoan.getEnterprisesRelation())) {
                    BigDecimal cptlBalance = rptCptlSituLegalRepreLoan.getCptlBalance();
                    curAmt = curAmt.add(cptlBalance);
                }
            }
        }
        QueryModel model1 = new QueryModel();
        model1.addCondition("serno", serno);
        model1.addCondition("assetRange", "2");
        List<RptBasicInfoPersFamilyAssets> rptBasicInfoPersFamilyAssets = rptBasicInfoPersFamilyAssetsService.selectByModel(model1);
        BigDecimal totalMarketValue = BigDecimal.ZERO;
        List<RptBasicInfoRealEstate> rptBasicInfoRealEstates = rptBasicInfoRealEstateService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptBasicInfoRealEstates)) {
            for (RptBasicInfoRealEstate rptBasicInfoRealEstate : rptBasicInfoRealEstates) {
                if (Objects.nonNull(rptBasicInfoRealEstate.getEquityCertPaperValue())) {
                    totalMarketValue = totalMarketValue.add(rptBasicInfoRealEstate.getEquityCertPaperValue());
                }
            }
        }
        if (CollectionUtils.nonEmpty(rptBasicInfoPersFamilyAssets)) {
            for (RptBasicInfoPersFamilyAssets rptBasicInfoPersFamilyAsset : rptBasicInfoPersFamilyAssets) {
                if ("02".equals(rptBasicInfoPersFamilyAsset.getEnterprisesRelation()) || "04".equals(rptBasicInfoPersFamilyAsset.getEnterprisesRelation()) || "05".equals(rptBasicInfoPersFamilyAsset.getEnterprisesRelation())) {
                    BigDecimal marketValue = rptBasicInfoPersFamilyAsset.getMarketValue();
                    totalMarketValue = totalMarketValue.add(marketValue);
                }
            }
        }
        BigDecimal amount3 = BigDecimal.ZERO;
        if (totalMarketValue.compareTo(BigDecimal.ZERO) > 0) {
            amount3 = curAmt.divide(totalMarketValue, 6, RoundingMode.HALF_UP);
            if (amount3.compareTo(BigDecimal.valueOf(0.3)) <= 0) {
                value3 = 12;
            } else if (amount3.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                value3 = 10;
            } else if (amount3.compareTo(BigDecimal.valueOf(0.7)) <= 0) {
                value3 = 7;
            } else if (amount3.compareTo(BigDecimal.valueOf(1.0)) <= 0) {
                value3 = 3;
            } else if (amount3.compareTo(BigDecimal.valueOf(1.0)) > 0) {
                value3 = 0;
            }
        }
        if (curAmt.compareTo(BigDecimal.ZERO) == 0) {
            value3 = 12;
        }
        //第4项自动打分
        int value4 = 0;
        String cusId = lmtApp.getCusId();
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        if (Objects.nonNull(cusBaseClientDto)) {
            if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                String fjobDate = "";
                Map<String, Object> map1 = new HashMap<>();
                map1.put("mrgType", CmisCusConstants.STD_CROP_MRG_TYPE_201200);
                map1.put("cusIdRel", lmtApp.getCusId());
                ResultDto<CusCorpMgrDto> cusCorpMgrByParams = cmisCusClientService.getCusCorpMgrByParams(map1);
                //获取实际控制人从业日期
                if (cusCorpMgrByParams != null && cusCorpMgrByParams.getData() != null) {
                    fjobDate = cusCorpMgrByParams.getData().getFjobDate();
                }
                if (StringUtils.nonBlank(fjobDate)) {
                    int month = DateUtils.getMonthsByTwoDatesDef(fjobDate, DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
                    double year = month / 12;
                    if (year >= 5) {
                        value4 = 6;
                    } else if (year >= 1 && year < 5) {
                        value4 = 3;
                    } else if (year < 1) {
                        value4 = 0;
                    }
                }
            }else if(CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)){
                ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
                if(cusIndivUnitDtoResultDto!=null&&cusIndivUnitDtoResultDto.getData()!=null){
                    CusIndivUnitDto data = cusIndivUnitDtoResultDto.getData();
                    String fjobDate = data.getWorkDate();
                    String currentDate = DateUtils.getCurrentDate(DateFormatEnum.YEAR);
                    if(StringUtils.nonBlank(fjobDate)){
                        int year = Integer.parseInt(currentDate)-Integer.parseInt(fjobDate);
                        if (year >= 5) {
                            value4 = 6;
                        } else if (year >= 1 && year < 5) {
                            value4 = 3;
                        } else if (year < 1) {
                            value4 = 0;
                        }
                    }
                }
            }
        }
        //第5项自动打分
        int value5 = 0;
        String tradeClass = "";
        if (Objects.nonNull(cusBaseClientDto)) {
            if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusCatalog)) {
                RptBasicInfoAsso rptBasicInfoAsso = rptBasicInfoAssoService.selectByPrimaryKey(serno);
                tradeClass = rptBasicInfoAsso.getTradeClass();
            } else if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusCatalog)) {
                ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
                if (cusIndivUnitDtoResultDto != null && cusIndivUnitDtoResultDto.getData() != null) {
                    CusIndivUnitDto data = cusIndivUnitDtoResultDto.getData();
                    String indivComTrade = data.getIndivComTrade();
                    tradeClass = indivComTrade;
                }
            }
            if (StringUtils.nonBlank(tradeClass)) {
                if (tradeClass.startsWith("A")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("B")) {
                    value5 = 1;
                } else if (tradeClass.startsWith("C")) {
                    if ("C".equals(tradeClass)) {
                        value5 = 3;
                    } else if (tradeClass.startsWith("C13") || tradeClass.startsWith("C14") || tradeClass.startsWith("C15") || tradeClass.startsWith("C16")) {
                        value5 = 6;
                    } else if (tradeClass.startsWith("C17") || tradeClass.startsWith("C18") || tradeClass.startsWith("C19") || tradeClass.startsWith("C20") || tradeClass.startsWith("C21")) {
                        value5 = 3;
                    } else if (tradeClass.startsWith("C22") || tradeClass.startsWith("C23")) {
                        value5 = 1;
                    } else if (tradeClass.startsWith("C24")) {
                        value5 = 6;
                    } else if (tradeClass.startsWith("C25")) {
                        value5 = 1;
                    } else if (tradeClass.startsWith("C26")) {
                        value5 = 3;
                    } else if (tradeClass.startsWith("C27")) {
                        value5 = 6;
                    } else if (tradeClass.startsWith("C28") || tradeClass.startsWith("C29") || tradeClass.startsWith("C30")) {
                        value5 = 3;
                    } else if (tradeClass.startsWith("C31") | tradeClass.startsWith("C32")) {
                        value5 = 1;
                    } else if (tradeClass.startsWith("C33") || tradeClass.startsWith("C34") || tradeClass.startsWith("C35") || tradeClass.startsWith("C36")) {
                        value5 = 3;
                    } else if (tradeClass.startsWith("C37")) {
                        if ("C373".equals(tradeClass)) {
                            value5 = 1;
                        } else {
                            value5 = 3;
                        }
                    } else {
                        value5 = 3;
                    }
                } else if (tradeClass.startsWith("D")) {
                    value5 = 3;
                } else if (tradeClass.startsWith("E")) {
                    value5 = 1;
                } else if (tradeClass.startsWith("F")) {
                    value5 = 3;
                } else if (tradeClass.startsWith("G")) {
                    value5 = 3;
                } else if (tradeClass.startsWith("H")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("I")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("J")) {
                    value5 = 3;
                } else if (tradeClass.startsWith("K")) {
                    value5 = 1;
                } else if (tradeClass.startsWith("L")) {
                    value5 = 3;
                } else if (tradeClass.startsWith("M")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("N")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("O")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("P")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("Q")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("R")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("S")) {
                    value5 = 6;
                } else if (tradeClass.startsWith("T")) {
                    value5 = 6;
                }
            }
        }


        //第6项自动打分  财报利润表
        int value6 = this.value6(serno, rptType);

        //第8项自动打分(第7项手填)
        int value8 = 0;
        QueryModel model2 = new QueryModel();
        model2.addCondition("serno", serno);
        List<RptLmtRepayAnysGuarPldDetail> rptLmtRepayAnysGuarPldDetails = rptLmtRepayAnysGuarPldDetailService.selectByModel(model);
        if (CollectionUtils.nonEmpty(rptLmtRepayAnysGuarPldDetails)) {
            //抵押物评估价值
            BigDecimal totalValue = BigDecimal.ZERO;
            BigDecimal lmtAmt = BigDecimal.ZERO;
            //抵押物商用房评估价值
            BigDecimal totalSyfValue = BigDecimal.ZERO;
            BigDecimal lmtSyfAmt = BigDecimal.ZERO;
            for (RptLmtRepayAnysGuarPldDetail rptLmtRepayAnysGuarPldDetail : rptLmtRepayAnysGuarPldDetails) {
                String subSerno = rptLmtRepayAnysGuarPldDetail.getSubSerno();
                List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(subSerno);
                if (CollectionUtils.nonEmpty(lmtAppSubPrds)) {
                    boolean sxkdFlag = false;
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                        String lmtBizTypeProp = lmtAppSubPrd.getLmtBizTypeProp();
                        if (StringUtils.nonBlank(lmtBizTypeProp)) {
                            if ("P011".equals(lmtBizTypeProp)) {
                                sxkdFlag = true;
                            }
                        }
                    }
                    if (sxkdFlag) {
                        lmtAmt = lmtAmt.add(NumberUtils.nullDefaultZero(rptLmtRepayAnysGuarPldDetail.getCorreFinAmt()));
                        BigDecimal maxMortagageAmt = rptLmtRepayAnysGuarPldDetail.getMaxMortagageAmt();
                        totalValue = totalValue.add(NumberUtils.nullDefaultZero(maxMortagageAmt));
                        //商用房评估价值
                        if (rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102") || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102001")
                                || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102002") || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102003")
                                || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102004") || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102005")
                                || rptLmtRepayAnysGuarPldDetail.getGuarTypeCd().equals("DY0102999")) {
                            totalSyfValue = totalSyfValue.add(NumberUtils.nullDefaultZero(rptLmtRepayAnysGuarPldDetail.getMaxMortagageAmt()));
                            lmtSyfAmt = lmtSyfAmt.add(NumberUtils.nullDefaultZero(rptLmtRepayAnysGuarPldDetail.getCorreFinAmt()));

                        }
                    }
                }
            }
            if (totalSyfValue.compareTo(BigDecimal.ZERO) > 0) {
                if (lmtAmt.compareTo(BigDecimal.ZERO) > 0 && totalValue.compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal divide1 = lmtAmt.divide(totalValue, 6, RoundingMode.HALF_UP);
                    BigDecimal divide2 = lmtSyfAmt.divide(totalSyfValue, 6, RoundingMode.HALF_UP);
                    if (divide1.compareTo(BigDecimal.valueOf(0.5)) <= 0 && divide2.compareTo(BigDecimal.valueOf(0.3)) <= 0) {
                        value8 = 12;
                    } else if (divide1.compareTo(BigDecimal.valueOf(0.6)) <= 0 && divide2.compareTo(BigDecimal.valueOf(0.4)) <= 0) {
                        value8 = 10;
                    } else if (divide1.compareTo(BigDecimal.valueOf(0.7)) <= 0 && divide2.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                        value8 = 8;
                    } else if (divide1.compareTo(BigDecimal.valueOf(1)) < 0 && divide2.compareTo(BigDecimal.valueOf(0.7)) <= 0) {
                        value8 = 3;
                    } else {
                        value8 = 0;
                    }
                }
            } else {
                if (lmtAmt.compareTo(BigDecimal.ZERO) > 0 && totalValue.compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal divide1 = lmtAmt.divide(totalValue, 6, RoundingMode.HALF_UP);
                    if (divide1.compareTo(BigDecimal.valueOf(0.5)) <= 0) {
                        value8 = 12;
                    } else if (divide1.compareTo(BigDecimal.valueOf(0.6)) <= 0) {
                        value8 = 10;
                    } else if (divide1.compareTo(BigDecimal.valueOf(0.7)) <= 0) {
                        value8 = 8;
                    } else if (divide1.compareTo(BigDecimal.valueOf(1)) < 0) {
                        value8 = 3;
                    } else {
                        value8 = 0;
                    }
                }
            }
        }
        RptSpdAnysSxkd rptSpdAnysSxkd = new RptSpdAnysSxkd();
        rptSpdAnysSxkd.setSerno(serno);
        User userInfo = SessionUtils.getUserInformation();
        if (managerType.equals("01")) {
            rptSpdAnysSxkd.setPfkKsxd1Grade1(value1);
            rptSpdAnysSxkd.setPfkKsxd2Grade1(value2);
            rptSpdAnysSxkd.setPfkKsxd3Grade1(value3);
            rptSpdAnysSxkd.setPfkKsxd4Grade1(value4);
            rptSpdAnysSxkd.setPfkKsxd5Grade1(value5);
            rptSpdAnysSxkd.setPfkKsxd6Grade1(value6);
            rptSpdAnysSxkd.setPfkKsxd8Grade1(value8);
            rptSpdAnysSxkd.setMainManagerId(userInfo.getLoginCode());
            count = rptSpdAnysSxkdMapper.updateByPrimaryKeySelective(rptSpdAnysSxkd);
        } else if (managerType.equals("02")) {
            RptSpdAnysSxkd temp = selectByPrimaryKey(serno);
            if (Objects.nonNull(temp)) {
                rptSpdAnysSxkd.setPfkKsxd7Grade2(temp.getPfkKsxd7Grade1());
                rptSpdAnysSxkd.setPfkKsxd9Grade2(temp.getPfkKsxd9Grade1());
                rptSpdAnysSxkd.setPfkKsxd10Grade2(temp.getPfkKsxd10Grade1());
            }
            rptSpdAnysSxkd.setPfkKsxd1Grade2(value1);
            rptSpdAnysSxkd.setPfkKsxd2Grade2(value2);
            rptSpdAnysSxkd.setPfkKsxd3Grade2(value3);
            rptSpdAnysSxkd.setPfkKsxd4Grade2(value4);
            rptSpdAnysSxkd.setPfkKsxd5Grade2(value5);
            rptSpdAnysSxkd.setPfkKsxd6Grade2(value6);
            rptSpdAnysSxkd.setPfkKsxd8Grade2(value8);
            rptSpdAnysSxkd.setAssistManagerId(userInfo.getLoginCode());
            count = rptSpdAnysSxkdMapper.updateByPrimaryKeySelective(rptSpdAnysSxkd);
        }
        return count;
    }

    /**
     * 打分卡
     *
     * @param serno
     * @return
     */
    public int value6(String serno, String rptType) {
        BigDecimal lrzeCurr = BigDecimal.ZERO;
        BigDecimal lrzeLastYear = BigDecimal.ZERO;
        BigDecimal lrzeLastSecond = BigDecimal.ZERO;
        BigDecimal xssrCurr = BigDecimal.ZERO;
        BigDecimal xssrLastYear = BigDecimal.ZERO;
        BigDecimal xssrLastSecond = BigDecimal.ZERO;
        List<RptFncSituBsAnys> rptFncSituBsAnysList = new ArrayList<>();
        if (CmisBizConstants.STD_RPT_TYPE_C11.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C14.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_4);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_I04.equals(rptType) || CmisBizConstants.STD_RPT_TYPE_I05.equals(rptType)) {
            Map map = new HashMap();
            map.put("serno", serno);
            map.put("fncFlag", CmisBizConstants.STD_FNC_FLAG_5);
            rptFncSituBsAnysList = rptFncSituBsAnysService.selectByParamMap(map);
        } else if (CmisBizConstants.STD_RPT_TYPE_C01.equals(rptType)) {
            RptOperEnergySales rptOperEnergySales = rptOperEnergySalesService.selectByPrimaryKey(serno);
            RptOperProfitSituation rptOperProfitSituation = rptOperProfitSituationService.selectByPrimaryKey(serno);
            if (Objects.nonNull(rptOperEnergySales)) {
                if (Objects.nonNull(rptOperEnergySales.getNearSecondAppSaleIncome())) {
                    xssrLastSecond = rptOperEnergySales.getNearSecondAppSaleIncome();
                }
                if (Objects.nonNull(rptOperEnergySales.getNearFirstAppSaleIncome())) {
                    xssrLastYear = rptOperEnergySales.getNearFirstAppSaleIncome();
                }
                if (Objects.nonNull(rptOperEnergySales.getCurrAppSaleIncome())) {
                    xssrCurr = rptOperEnergySales.getCurrAppSaleIncome();
                }
            }
            if (Objects.nonNull(rptOperProfitSituation)) {
                lrzeCurr = rptOperProfitSituation.getCurrTotalProfit();
                lrzeLastYear = rptOperProfitSituation.getNearFirstTotalProfit();
                lrzeLastSecond = rptOperProfitSituation.getNearSecondTotalProfit();
            }
        }
        RptFncSituBs rptFncSituBs = rptFncSituBsService.selectByPrimaryKey(serno);
        if (Objects.isNull(rptFncSituBs)) {
            return 0;
        }
        String inputYear = rptFncSituBs.getInputYear();
        if (CollectionUtils.nonEmpty(rptFncSituBsAnysList)) {
            for (RptFncSituBsAnys rptFncSituBsAnys : rptFncSituBsAnysList) {
                if ("利润总额".equals(rptFncSituBsAnys.getSubjectName())) {
                    lrzeCurr = rptFncSituBsAnys.getCurYearLastMonth();
                    lrzeLastYear = rptFncSituBsAnys.getLastYear();
                    lrzeLastSecond = rptFncSituBsAnys.getLastTwoYear();
                } else if ("销售收入".equals(rptFncSituBsAnys.getSubjectName())) {
                    xssrCurr = rptFncSituBsAnys.getCurYearLastMonth();
                    xssrLastYear = rptFncSituBsAnys.getLastYear();
                    xssrLastSecond = rptFncSituBsAnys.getLastTwoYear();
                }
            }
        }
        int month = Integer.parseInt(inputYear.substring(4, 6));
        BigDecimal xssrCurrYear = BigDecimal.ZERO;
        BigDecimal lrzeCurrYear = BigDecimal.ZERO;
        if (month < 12) {
            xssrCurrYear = (xssrCurr.divide(BigDecimal.valueOf(month), 6, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(12));
            lrzeCurrYear = (lrzeCurr.divide(BigDecimal.valueOf(month), 6, BigDecimal.ROUND_HALF_UP)).multiply(BigDecimal.valueOf(12));
        } else if (month == 12) {
            xssrCurrYear = xssrCurr;
            lrzeCurrYear = lrzeCurr;
        }
        BigDecimal lastYearXssrDiff = xssrLastYear.subtract(xssrLastSecond);
        BigDecimal curXssrDiff = xssrCurrYear.subtract(xssrLastYear);
        if (lrzeCurrYear.compareTo(BigDecimal.ZERO) > 0 && lrzeLastYear.compareTo(BigDecimal.ZERO) > 0 && lrzeLastSecond.compareTo(BigDecimal.ZERO) > 0) {
            //利润总额上一年增长率
            if (lrzeLastYear.compareTo(lrzeLastSecond.add(lrzeLastSecond.multiply(BigDecimal.valueOf(0.1)))) >= 0 && lrzeCurrYear.compareTo(lrzeLastYear.add(lrzeLastYear.multiply(BigDecimal.valueOf(0.1)))) >= 0) {
                if (xssrCurrYear.compareTo(BigDecimal.ZERO) > 0 && xssrLastYear.compareTo(BigDecimal.ZERO) > 0 && xssrLastSecond.compareTo(BigDecimal.ZERO) > 0) {
                    if (xssrLastYear.compareTo(xssrLastSecond.add(xssrLastSecond.multiply(BigDecimal.valueOf(0.1)))) >= 0 && xssrCurrYear.compareTo(xssrLastYear.add(xssrLastYear.multiply(BigDecimal.valueOf(0.1)))) >= 0) {
                        return 12;
                    }
                }
            }

            if (lastYearXssrDiff.compareTo(BigDecimal.ZERO) > 0 && curXssrDiff.compareTo(BigDecimal.ZERO) > 0) {
                return 10;
            }
        }
        if (lrzeCurrYear.compareTo(BigDecimal.ZERO) > 0) {
            if (curXssrDiff.compareTo(BigDecimal.ZERO) > 0) {
                return 8;
            } else {
                return 6;
            }
        }
        if (lrzeCurrYear.compareTo(lrzeLastYear.subtract(lrzeLastYear.multiply(BigDecimal.valueOf(0.3)))) >= 0 &&
                xssrCurrYear.compareTo(xssrLastYear.subtract(xssrLastYear.multiply(BigDecimal.valueOf(0.3)))) >= 0) {
            return 2;
        }

        return 0;

    }

}


