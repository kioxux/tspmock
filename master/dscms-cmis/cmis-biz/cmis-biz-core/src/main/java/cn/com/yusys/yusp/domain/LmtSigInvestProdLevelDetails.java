/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestProdLevelDetails
 * @类描述: lmt_sig_invest_prod_level_details数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 17:25:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_prod_level_details")
public class LmtSigInvestProdLevelDetails extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 投资级别 **/
	@Column(name = "INVEST_LEVEL", unique = false, nullable = true, length = 40)
	private String investLevel;
	
	/** 债项评级 **/
	@Column(name = "DEBT_GRADE", unique = false, nullable = true, length = 5)
	private String debtGrade;
	
	/** 本级别募集规模（元） **/
	@Column(name = "CURT_LEVEL_RAISE_SCALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLevelRaiseScale;
	
	/** 本级别本行拟投资规模（元） **/
	@Column(name = "CURT_LEVEL_INVEST_SCALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLevelInvestScale;
	
	/** 本级别预计年化收益率 **/
	@Column(name = "CURT_LEVEL_FORE_ARR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLevelForeArr;
	
	/** 还本付息方式 **/
	@Column(name = "GDP_MODE", unique = false, nullable = true, length = 40)
	private String gdpMode;
	
	/** 预期到期日 **/
	@Column(name = "PRE_END_DATE", unique = false, nullable = true, length = 20)
	private String preEndDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param investLevel
	 */
	public void setInvestLevel(String investLevel) {
		this.investLevel = investLevel;
	}
	
    /**
     * @return investLevel
     */
	public String getInvestLevel() {
		return this.investLevel;
	}
	
	/**
	 * @param debtGrade
	 */
	public void setDebtGrade(String debtGrade) {
		this.debtGrade = debtGrade;
	}
	
    /**
     * @return debtGrade
     */
	public String getDebtGrade() {
		return this.debtGrade;
	}
	
	/**
	 * @param curtLevelRaiseScale
	 */
	public void setCurtLevelRaiseScale(java.math.BigDecimal curtLevelRaiseScale) {
		this.curtLevelRaiseScale = curtLevelRaiseScale;
	}
	
    /**
     * @return curtLevelRaiseScale
     */
	public java.math.BigDecimal getCurtLevelRaiseScale() {
		return this.curtLevelRaiseScale;
	}
	
	/**
	 * @param curtLevelInvestScale
	 */
	public void setCurtLevelInvestScale(java.math.BigDecimal curtLevelInvestScale) {
		this.curtLevelInvestScale = curtLevelInvestScale;
	}
	
    /**
     * @return curtLevelInvestScale
     */
	public java.math.BigDecimal getCurtLevelInvestScale() {
		return this.curtLevelInvestScale;
	}
	
	/**
	 * @param curtLevelForeArr
	 */
	public void setCurtLevelForeArr(java.math.BigDecimal curtLevelForeArr) {
		this.curtLevelForeArr = curtLevelForeArr;
	}
	
    /**
     * @return curtLevelForeArr
     */
	public java.math.BigDecimal getCurtLevelForeArr() {
		return this.curtLevelForeArr;
	}
	
	/**
	 * @param gdpMode
	 */
	public void setGdpMode(String gdpMode) {
		this.gdpMode = gdpMode;
	}
	
    /**
     * @return gdpMode
     */
	public String getGdpMode() {
		return this.gdpMode;
	}
	
	/**
	 * @param preEndDate
	 */
	public void setPreEndDate(String preEndDate) {
		this.preEndDate = preEndDate;
	}
	
    /**
     * @return preEndDate
     */
	public String getPreEndDate() {
		return this.preEndDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}