package cn.com.yusys.yusp.service.server.xdcz0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0051.req.CmisLmt0051ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.CmisLmt0051RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0051.resp.LmtDiscOrgDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.req.Xdcz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.resp.Xdcz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: xdcz0005Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xs
 * @创建时间: 2021-05-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdcz0005Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0005Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private DscmsCfgClientService dscmsCfgClientService;

    //lmt_disc_org
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * 场景：票据系统贴现业务申请前，查询机构当月贴现总额与当月发生额比较，判断是否足额。
     * 业务逻辑：
     * 1.根据接口中的参数【机构号orrgid】+当前系统时间查询机构贴现限额表lmt_disc_org获取该机构当月总贴现总额  TODO  lmt_disc_org 对应新信贷表待坤经理确认
     * 1).贴现限额表lmt_disc_org无记录，报错并提示"该支行本月没有设置贴现额度限额！"
     * 2).lmt_disc_org存在记录，比较接口参数【单一机构当月贴现可用的发生额mttamt】与查得的【机构当月总贴现总额】大小，如mttamt>lmtAmount则拦截并提示"单一机构当月贴现发生额【"+mttamt+"】大于本支行【"+mmyy+"】起的贴现限额【"+lmtAmount+"】"
     * 备注：老系统SingleOrgTxAmtAction.java
     *
     * @param xdcz0005DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0005DataRespDto xdcz0005(Xdcz0005DataReqDto xdcz0005DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value);
        Xdcz0005DataRespDto xdcz0005DataRespDto = new Xdcz0005DataRespDto();
        String chnl = xdcz0005DataReqDto.getChnl();//渠道
        String chnlSerNo = xdcz0005DataReqDto.getChnlSerNo();//渠道流水
        String clkId = xdcz0005DataReqDto.getClkId();//柜员号
        String deptNo = xdcz0005DataReqDto.getDeptNo();//部门号
        String orgNo = xdcz0005DataReqDto.getOrgNo();//机构号
        BigDecimal singleOrgDiscAvlBal = Optional.ofNullable(xdcz0005DataReqDto.getSingleOrgDiscAvlBal()).orElse(BigDecimal.ZERO);//单一机构当月贴现可用的发生额
        if (StringUtils.isBlank(orgNo)) {
            xdcz0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
            xdcz0005DataRespDto.setOpMsg("机构号不能为空");
            return xdcz0005DataRespDto;
        }
        if (Objects.isNull(singleOrgDiscAvlBal)) {
            singleOrgDiscAvlBal = BigDecimal.ZERO;
        }
        try {
            String openDay = Optional.ofNullable(stringRedisTemplate.opsForValue().get("openDay")).orElse(LocalDate.now().toString());
            String mmyy = openDay.substring(0, 7);

            CmisLmt0051ReqDto cmisLmt0051ReqDto = new CmisLmt0051ReqDto();
            cmisLmt0051ReqDto.setMmyy(mmyy);
            cmisLmt0051ReqDto.setOpenDay(openDay);
            cmisLmt0051ReqDto.setOrganno(orgNo);
            logger.info("===cmisLmt0051RespDtoResultDto===>【cmisLmt0051ReqDto】=====【{}】", JSONObject.toJSON(cmisLmt0051ReqDto));
            ResultDto<CmisLmt0051RespDto> cmisLmt0051RespDtoResultDto = cmisLmtClientService.cmislmt0051(cmisLmt0051ReqDto);
            logger.info("===cmisLmt0051RespDtoResultDto===>【CmisLmt0051RespDto】=====【{}】", JSONObject.toJSON(cmisLmt0051RespDtoResultDto));

            String code = cmisLmt0051RespDtoResultDto.getCode();
            if (!SuccessEnum.CMIS_SUCCSESS.key.equals(code)){
                throw BizException.error(null,code,cmisLmt0051RespDtoResultDto.getMessage());
            }
            CmisLmt0051RespDto data = cmisLmt0051RespDtoResultDto.getData();
            if (!SuccessEnum.SUCCESS.key.equals(data.getErrorCode())){
                throw BizException.error(null,data.getErrorCode(),data.getErrorMsg());
            }
            List<LmtDiscOrgDto> lmtDiscOrgDtoList = data.getLmtDiscOrgDtoList();
            if (CollectionUtils.isEmpty(lmtDiscOrgDtoList)) {
                xdcz0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                xdcz0005DataRespDto.setOpMsg("该支行本月没有设置贴现额度限额！");
                return xdcz0005DataRespDto;
            } else {
                BigDecimal reduce = Optional.ofNullable(lmtDiscOrgDtoList.stream().map(LmtDiscOrgDto::getApproveAmount).
                        reduce(BigDecimal.ZERO, BigDecimal::add)).orElse(BigDecimal.ZERO);
                if (singleOrgDiscAvlBal.compareTo(reduce) > 0) {
                    xdcz0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                    xdcz0005DataRespDto.setOpMsg("单一机构当月贴现发生额【" + singleOrgDiscAvlBal + "】大于本支行【" + mmyy + "】起的贴现限额【" + reduce + "】");
                    return xdcz0005DataRespDto;
                }
            }
            xdcz0005DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 操作成功标志位
            xdcz0005DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, e.getMessage());
            xdcz0005DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 操作成功标志位
            xdcz0005DataRespDto.setOpMsg("操作失败");// 描述信息
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key,
                DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataRespDto));
        return xdcz0005DataRespDto;
    }
}
