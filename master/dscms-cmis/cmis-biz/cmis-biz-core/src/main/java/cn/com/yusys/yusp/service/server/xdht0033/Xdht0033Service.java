package cn.com.yusys.yusp.service.server.xdht0033;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0033.req.Xdht0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0033.resp.Xdht0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizHtEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * 业务逻辑类:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xdht0033Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0033Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 交易码：xdht0033
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0033DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0033DataRespDto xdht0033(Xdht0033DataReqDto xdht0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataReqDto));
        Xdht0033DataRespDto xdht0033DataRespDto = new Xdht0033DataRespDto();
        try {
            String queryType = xdht0033DataReqDto.getQueryType();//查询类型
            String cusId = xdht0033DataReqDto.getCusId();//客户号
            QueryModel queryModel = new QueryModel();
            BigDecimal applyAmt = BigDecimal.ZERO;// 申请金额
            if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_SURVEYCONTLOANAMT.key, queryType)) {
                queryModel.addCondition("cus_id", cusId);
                logger.info("根据核心客户号查询小贷是否具有客户调查合同并返回合同金额开始,查询参数为:{}", JSON.toJSONString(queryModel));
                applyAmt = ctrLoanContMapper.querySurveyContLoanAmt(queryModel);
                logger.info("根据核心客户号查询小贷是否具有客户调查合同并返回合同金额结束,返回结果为:{}", JSON.toJSONString(applyAmt));
            } else if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_OPERATINGCONTAMT.key, queryType)) {
                queryModel.addCondition("cus_id", cusId);
                logger.info("根据核心客户号查询经营性贷款关联生效合同的批复额度开始,查询参数为:{}", JSON.toJSONString(queryModel));
                applyAmt = ctrLoanContMapper.queryOperatingContAmt(queryModel);
                logger.info("根据核心客户号查询经营性贷款关联生效合同的批复额度结束,返回结果为:{}", JSON.toJSONString(applyAmt));
            } else if (Objects.equals(DscmsBizHtEnum.QUERY_TYPE_OPERATINGCONTPFAMT.key, queryType)) {
                queryModel.addCondition("cus_id", cusId);
                logger.info("根据核心客户号查询经营性贷款关联生效合同的批复额度开始,查询参数为:{}", JSON.toJSONString(queryModel));
                applyAmt = ctrLoanContMapper.queryOperatingContPfAmt(queryModel);
                logger.info("根据核心客户号查询经营性贷款关联生效合同的批复额度结束,返回结果为:{}", JSON.toJSONString(applyAmt));
            }
            xdht0033DataRespDto.setApplyAmt(applyAmt.toEngineeringString());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataRespDto));
        return xdht0033DataRespDto;
    }
}
