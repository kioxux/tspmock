/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasicInfoRealEstate
 * @类描述: rpt_basic_info_real_estate数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 13:48:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic_info_real_estate")
public class RptBasicInfoRealEstate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Id
	@Column(name = "SERNO")
	private String serno;
	
	/** 所属性质 **/
	@Column(name = "LAND_CHA", unique = false, nullable = true, length = 5)
	private String landCha;
	
	/** 所属企业名称 **/
	@Column(name = "LAND_CUS_NAME", unique = false, nullable = true, length = 40)
	private String landCusName;
	
	/** 资产项目类型 **/
	@Column(name = "ASSET_PRO_TYPE", unique = false, nullable = true, length = 5)
	private String assetProType;
	
	/** 资产有证项目面积 **/
	@Column(name = "ASSET_CERT_PRO_SQU", unique = false, nullable = true, length = 40)
	private String assetCertProSqu;
	
	/** 资产无证项目面积 **/
	@Column(name = "ASSET_PRO_SQU", unique = false, nullable = true, length = 40)
	private String assetProSqu;
	
	/** 账面价值有证原值 **/
	@Column(name = "ORIGI_CERT_PAPER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiCertPaperValue;
	
	/** 账面价值有证净值 **/
	@Column(name = "EQUITY_CERT_PAPER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal equityCertPaperValue;
	
	/** 账面价值无证原值 **/
	@Column(name = "ORIGI_PAPER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiPaperValue;
	
	/** 账面价值无证净值 **/
	@Column(name = "EQUITY_PAPER_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal equityPaperValue;
	
	/** 目前抵押情况抵押行 **/
	@Column(name = "CURR_GUAR_CASE_BANK", unique = false, nullable = true, length = 40)
	private String currGuarCaseBank;
	
	/** 目前抵押情况承货主体 **/
	@Column(name = "CURR_GUAR_CASE_MAIN_PART", unique = false, nullable = true, length = 40)
	private String currGuarCaseMainPart;
	
	/** 目前抵押情况抵押贷款金额 **/
	@Column(name = "CURR_GUAR_CASE_LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal currGuarCaseLoanAmt;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 65535)
	private String remark;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param landCha
	 */
	public void setLandCha(String landCha) {
		this.landCha = landCha;
	}
	
    /**
     * @return landCha
     */
	public String getLandCha() {
		return this.landCha;
	}
	
	/**
	 * @param landCusName
	 */
	public void setLandCusName(String landCusName) {
		this.landCusName = landCusName;
	}
	
    /**
     * @return landCusName
     */
	public String getLandCusName() {
		return this.landCusName;
	}
	
	/**
	 * @param assetProType
	 */
	public void setAssetProType(String assetProType) {
		this.assetProType = assetProType;
	}
	
    /**
     * @return assetProType
     */
	public String getAssetProType() {
		return this.assetProType;
	}
	
	/**
	 * @param assetCertProSqu
	 */
	public void setAssetCertProSqu(String assetCertProSqu) {
		this.assetCertProSqu = assetCertProSqu;
	}
	
    /**
     * @return assetCertProSqu
     */
	public String getAssetCertProSqu() {
		return this.assetCertProSqu;
	}
	
	/**
	 * @param assetProSqu
	 */
	public void setAssetProSqu(String assetProSqu) {
		this.assetProSqu = assetProSqu;
	}
	
    /**
     * @return assetProSqu
     */
	public String getAssetProSqu() {
		return this.assetProSqu;
	}
	
	/**
	 * @param origiCertPaperValue
	 */
	public void setOrigiCertPaperValue(java.math.BigDecimal origiCertPaperValue) {
		this.origiCertPaperValue = origiCertPaperValue;
	}
	
    /**
     * @return origiCertPaperValue
     */
	public java.math.BigDecimal getOrigiCertPaperValue() {
		return this.origiCertPaperValue;
	}
	
	/**
	 * @param equityCertPaperValue
	 */
	public void setEquityCertPaperValue(java.math.BigDecimal equityCertPaperValue) {
		this.equityCertPaperValue = equityCertPaperValue;
	}
	
    /**
     * @return equityCertPaperValue
     */
	public java.math.BigDecimal getEquityCertPaperValue() {
		return this.equityCertPaperValue;
	}
	
	/**
	 * @param origiPaperValue
	 */
	public void setOrigiPaperValue(java.math.BigDecimal origiPaperValue) {
		this.origiPaperValue = origiPaperValue;
	}
	
    /**
     * @return origiPaperValue
     */
	public java.math.BigDecimal getOrigiPaperValue() {
		return this.origiPaperValue;
	}
	
	/**
	 * @param equityPaperValue
	 */
	public void setEquityPaperValue(java.math.BigDecimal equityPaperValue) {
		this.equityPaperValue = equityPaperValue;
	}
	
    /**
     * @return equityPaperValue
     */
	public java.math.BigDecimal getEquityPaperValue() {
		return this.equityPaperValue;
	}
	
	/**
	 * @param currGuarCaseBank
	 */
	public void setCurrGuarCaseBank(String currGuarCaseBank) {
		this.currGuarCaseBank = currGuarCaseBank;
	}
	
    /**
     * @return currGuarCaseBank
     */
	public String getCurrGuarCaseBank() {
		return this.currGuarCaseBank;
	}
	
	/**
	 * @param currGuarCaseMainPart
	 */
	public void setCurrGuarCaseMainPart(String currGuarCaseMainPart) {
		this.currGuarCaseMainPart = currGuarCaseMainPart;
	}
	
    /**
     * @return currGuarCaseMainPart
     */
	public String getCurrGuarCaseMainPart() {
		return this.currGuarCaseMainPart;
	}
	
	/**
	 * @param currGuarCaseLoanAmt
	 */
	public void setCurrGuarCaseLoanAmt(java.math.BigDecimal currGuarCaseLoanAmt) {
		this.currGuarCaseLoanAmt = currGuarCaseLoanAmt;
	}
	
    /**
     * @return currGuarCaseLoanAmt
     */
	public java.math.BigDecimal getCurrGuarCaseLoanAmt() {
		return this.currGuarCaseLoanAmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}