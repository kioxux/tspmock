package cn.com.yusys.yusp.web.server.xdca0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0003.req.Xdca0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0003.resp.Xdca0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdca0003.Xdca0003Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:大额分期合同列表查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0003:大额分期合同列表查询")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0003Resource.class);

    @Autowired
    private Xdca0003Service xdca0003Service;

    /**
     * 交易码：xdca0003
     * 交易描述：大额分期合同列表查询
     *
     * @param xdca0003DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同列表查询")
    @PostMapping("/xdca0003")
    protected @ResponseBody
    ResultDto<Xdca0003DataRespDto> xdca0003(@Validated @RequestBody Xdca0003DataReqDto xdca0003DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003DataReqDto));
        Xdca0003DataRespDto xdca0003DataRespDto = new Xdca0003DataRespDto();// 响应Dto:大额分期合同列表查询
        ResultDto<Xdca0003DataRespDto> xdca0003DataResultDto = new ResultDto<>();
        try {
            String cusName = xdca0003DataReqDto.getCusName();//客户姓名
            String certType = xdca0003DataReqDto.getCertType();//证件类型
            String certCode = xdca0003DataReqDto.getCertCode();//证件号码
            String startNo = xdca0003DataReqDto.getStartNo();//起始记录号
            String queryNum = xdca0003DataReqDto.getQueryNum();//查询记录数

            Boolean flag = true;
            if (StringUtil.isEmpty(cusName)) {
                flag = false;
            } else if (StringUtil.isEmpty(certCode)) {
                flag = false;
            } else if (StringUtil.isEmpty(certType)) {
                flag = false;
            } else if (StringUtil.isEmpty(startNo)) {
                flag = false;
            } else if (StringUtil.isEmpty(queryNum)) {
                flag = false;
            }
            if (flag) {
                // 从xdca0003DataReqDto获取业务值进行业务逻辑处理
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0003DataReqDto));
                xdca0003DataRespDto = xdca0003Service.xdca0003(xdca0003DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0003DataRespDto));
                // 封装xdca0003DataResultDto中正确的返回码和返回信息
                xdca0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdca0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                // 请求字段为空
                xdca0003DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdca0003DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            // 封装xdca0003DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdca0003DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdca0003DataRespDto到xdca0003DataResultDto中
        xdca0003DataResultDto.setData(xdca0003DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003DataRespDto));
        return xdca0003DataResultDto;
    }
}
