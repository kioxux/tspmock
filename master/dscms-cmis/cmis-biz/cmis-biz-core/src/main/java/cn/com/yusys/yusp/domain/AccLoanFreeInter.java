/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanFreeInter
 * @类描述: acc_loan_free_inter数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-08 22:06:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_loan_free_inter")
public class AccLoanFreeInter extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 借据编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "BILL_NO")
	private String billNo;
	
	/** 是否免息 **/
	@Column(name = "IS_FREE_INTER", unique = false, nullable = true, length = 5)
	private String isFreeInter;
	
	/** 免息天数 **/
	@Column(name = "FREE_INTER_DAY", unique = false, nullable = true, length = 10)
	private Integer freeInterDay;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param isFreeInter
	 */
	public void setIsFreeInter(String isFreeInter) {
		this.isFreeInter = isFreeInter;
	}
	
    /**
     * @return isFreeInter
     */
	public String getIsFreeInter() {
		return this.isFreeInter;
	}
	
	/**
	 * @param freeInterDay
	 */
	public void setFreeInterDay(Integer freeInterDay) {
		this.freeInterDay = freeInterDay;
	}
	
    /**
     * @return freeInterDay
     */
	public Integer getFreeInterDay() {
		return this.freeInterDay;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}