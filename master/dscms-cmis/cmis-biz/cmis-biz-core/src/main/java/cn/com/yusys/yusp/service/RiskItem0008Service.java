package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Transactional
public class RiskItem0008Service {

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;

    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0032Service.class);

    public RiskResultDto riskItem008(QueryModel queryModel) {
        String cusId = (String) queryModel.getCondition().get("bizUserId");
        String serno = (String) queryModel.getCondition().get("bizId");
        String bizType = (String) queryModel.getCondition().get("bizType");
        RiskResultDto riskResultDto = new RiskResultDto();
        if (CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType) ) {
            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(serno);
            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                long count = lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                        && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                        && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                if (count == 0) {
                    // 授信新增仅申报的授信分项中 只有低风险，不进行校验
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                }
            }
            return this.checkCusGrade(cusId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_SX008_TO_SX014.indexOf(bizType + ",") != -1) {
            List<LmtGrpMemRel> lmtGrpMemRels = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
            if (CollectionUtils.nonEmpty(lmtGrpMemRels)) {
                long count = 0;
                for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRels) {
                    LmtGrpApp lmtGrpApp = lmtGrpAppService.queryInfoByGrpSerno(lmtGrpMemRel.getGrpSerno());
                    if (CmisCommonConstants.LMT_TYPE_02.equals(lmtGrpApp.getLmtType())) { // 变更
                        if (CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRel.getIsCurtChg())) {
                            String singleSerno = lmtGrpMemRel.getSingleSerno();
                            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(singleSerno);
                            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                                count += lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                            }
                        }
                    } else if (CmisCommonConstants.LMT_TYPE_07.equals(lmtGrpApp.getLmtType())) { // 预授信细化
                        if (CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRel.getIsCurtRefine())) {
                            String singleSerno = lmtGrpMemRel.getSingleSerno();
                            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(singleSerno);
                            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                                count += lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                            }
                        }
                    } else if (CmisCommonConstants.LMT_TYPE_08.equals(lmtGrpApp.getLmtType())) { // 额度调剂
                        if (CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRel.getIsCurtAdjust())) {
                            String singleSerno = lmtGrpMemRel.getSingleSerno();
                            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(singleSerno);
                            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                                count += lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                            }
                        }
                    } else {  // 除去变更 预授信细化 调剂 外 统一以 是否参与本次申报 字段判断
                        if (CmisCommonConstants.YES_NO_1.equals(lmtGrpMemRel.getIsPrtcptCurtDeclare())) {
                            String singleSerno = lmtGrpMemRel.getSingleSerno();
                            List<LmtAppSub> lmtAppSubs = lmtAppSubService.selectLmtAppSubDataBySerno(singleSerno);
                            if (CollectionUtils.nonEmpty(lmtAppSubs)) {
                                count += lmtAppSubs.parallelStream().filter(e -> !CmisCommonConstants.GUAR_MODE_60.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_21.equals(e.getGuarMode())
                                        && !CmisCommonConstants.GUAR_MODE_40.equals(e.getGuarMode())).count();
                            }
                        }
                    }
                }
                if (count == 0) {
                    // 授信新增仅申报的授信分项中 只有低风险，不进行校验
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                }
            }
            return this.checkCusGrade(cusId);
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX001.equals(bizType)){
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
            if(Objects.isNull(iqpHighAmtAgrApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if(CmisCommonConstants.GUAR_MODE_40.equals(iqpHighAmtAgrApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpHighAmtAgrApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpHighAmtAgrApp.getGuarMode())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_YX003.equals(bizType)){
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(iqpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if(CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX006.equals(bizType)){
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if(CmisCommonConstants.GUAR_MODE_40.equals(iqpAccpApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpAccpApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpAccpApp.getGuarMode())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX007.equals(bizType)){
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectByCvrgSernoKey(serno);
            if(Objects.isNull(iqpCvrgApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if(CmisCommonConstants.GUAR_MODE_40.equals(iqpCvrgApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpCvrgApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpCvrgApp.getGuarMode())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX008.equals(bizType)){
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectByIqpEntrustLoanSernoKey(serno);
            if(Objects.isNull(iqpEntrustLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if(CmisCommonConstants.GUAR_MODE_40.equals(iqpEntrustLoanApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpEntrustLoanApp.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpEntrustLoanApp.getGuarMode())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        } else if (CmisFlowConstants.FLOW_TYPE_TYPE_XW001.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW004.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW005.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW006.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW007.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_XW008.equals(bizType) ) {
            // 小微授信调查
            LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(serno);
            if (Objects.isNull(lmtSurveyConInfo)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if (CmisCommonConstants.GUAR_MODE_40.equals(lmtSurveyConInfo.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_60.equals(lmtSurveyConInfo.getGuarMode())
                    || CmisCommonConstants.GUAR_MODE_21.equals(lmtSurveyConInfo.getGuarMode())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS001.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS002.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS003.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_DHE01.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_DHE02.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_DHE03.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_SGE01.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_SGE02.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_SGE03.equals(bizType)) {
            // 零售业务申请和小微合同申请
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
            if (Objects.isNull(iqpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if (CmisCommonConstants.GUAR_MODE_40.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_60.equals(iqpLoanApp.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_21.equals(iqpLoanApp.getGuarWay())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }else if (CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType) ||
                CmisFlowConstants.FLOW_TYPE_TYPE_LS008.equals(bizType)) {
            // 零售合同申请
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            if (Objects.isNull(ctrLoanCont)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0101); //业务流水号为空
                return riskResultDto;
            }
            if (CmisCommonConstants.GUAR_MODE_40.equals(ctrLoanCont.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_60.equals(ctrLoanCont.getGuarWay())
                    || CmisCommonConstants.GUAR_MODE_21.equals(ctrLoanCont.getGuarWay())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015); // 校验通过
                return riskResultDto;
            }
            return this.checkCusGrade(cusId);
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); // 校验通过
        return riskResultDto;
    }

    public RiskResultDto checkCusGrade(String cusId) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(cusId)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        if(Objects.isNull(cusBaseClientDto)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0100);
            return riskResultDto;
        }
        // todo 个人客户暂不校验（2021/08/23）
        if (!CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseClientDto.getCusCatalog())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); // 校验通过
            return riskResultDto;
        }

        // 如果对公客户的行业分类为 J开头（金融业) 则客户评级直接通过
        CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
        if (cusCorpDto != null && !StringUtils.isEmpty(cusCorpDto.getTradeClass()) && cusCorpDto.getTradeClass().startsWith("J")) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); // 校验通过
            return riskResultDto;
        }

        // 查询法人客户的信用评级信息
        ResultDto<Map<String, String>> result = iCusClientService.selectGradeInfoByCusId(cusId);
        if(Objects.isNull(result) || Objects.isNull(result.getData())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00802); //业务流水号为空
            return riskResultDto;
        }

        Map<String, String> map = result.getData();
        if(StringUtils.isBlank(map.get("dueDt"))) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00803); //业务流水号为空
            return riskResultDto;
        }
        String dateStr = map.get("dueDt").replace("/","-");
        Date date = DateUtils.parseDate(dateStr,"yyyy-MM-dd");
        if(Objects.isNull(date)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00804);
            return riskResultDto;
        }
        if(DateUtils.getCurrDate().compareTo(date) > 0) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00805);
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}
