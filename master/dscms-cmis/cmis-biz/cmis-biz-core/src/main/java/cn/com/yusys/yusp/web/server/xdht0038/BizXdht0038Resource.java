package cn.com.yusys.yusp.web.server.xdht0038;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0038.resp.List;
import cn.com.yusys.yusp.dto.server.xdht0038.resp.Xdht0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0038.Xdht0038Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;

/**
 * 接口处理类:乐悠金根据核心客户号查询房贷首付款比例进行额度计算
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0038:乐悠金根据核心客户号查询房贷首付款比例进行额度计算")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0038Resource.class);

    @Autowired
    private Xdht0038Service xdht0038Service;

    /**
     * 交易码：xdht0038
     * 交易描述：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
     *
     * @param xdht0038DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("乐悠金根据核心客户号查询房贷首付款比例进行额度计算")
    @PostMapping("/xdht0038")
    protected @ResponseBody
    ResultDto<Xdht0038DataRespDto> xdht0038(@Validated @RequestBody Xdht0038DataReqDto xdht0038DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataReqDto));
        Xdht0038DataRespDto xdht0038DataRespDto = new Xdht0038DataRespDto();// 响应Dto:乐悠金根据核心客户号查询房贷首付款比例进行额度计算
        ResultDto<Xdht0038DataRespDto> xdht0038DataResultDto = new ResultDto<>();
        String cusId = xdht0038DataReqDto.getCusId();//客户号
        try {
            // 从xdht0038DataReqDto获取业务值进行业务逻辑处理
            // 调用xdht0038Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataReqDto));
            java.util.List<List> result = Optional.ofNullable(xdht0038Service.getFirstpayByCusId(xdht0038DataReqDto)).orElseGet(() -> {
                List temp = new List();
                temp.setBillNo(StringUtils.EMPTY);// 借据编号
                temp.setFirstpayPerc(new BigDecimal(0L));// 首付款比例
                return Arrays.asList(temp);
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(result));
            xdht0038DataRespDto.setList(result);
            // 封装xdht0038DataResultDto中正确的返回码和返回信息
            xdht0038DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0038DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, e.getMessage());
            // 封装xdht0038DataResultDto中异常返回码和返回信息
            xdht0038DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0038DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0038DataRespDto到xdht0038DataResultDto中
        xdht0038DataResultDto.setData(xdht0038DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataResultDto));
        return xdht0038DataResultDto;
    }
}
