///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.service;
//
//import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.commons.util.StringUtils;
//import cn.com.yusys.yusp.constant.BizFlowConstant;
//import cn.com.yusys.yusp.constant.LmtSurveyEnums;
//import cn.com.yusys.yusp.constants.CmisCommonConstants;
//import cn.com.yusys.yusp.domain.*;
//import cn.com.yusys.yusp.enums.returncode.EcbEnum;
//import cn.com.yusys.yusp.repository.mapper.SurveyReportMainInfoMapper;
//import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
//import com.github.pagehelper.PageHelper;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @version 1.0.0
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyReportMainInfoService
// * @类描述: #服务类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 10:02:12
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @Copyright (c) 宇信科技-版权所有
// */
//@Service
//@Transactional
//public class SurveyReportMainInfoService {
//    Logger logger = LoggerFactory.getLogger(SurveyReportMainInfoService.class);
//
//    @Autowired(required = false)
//    private SurveyReportMainInfoMapper surveyReportMainInfoMapper;
//
//    @Autowired
//    private SurveyReportMainInfoService surveyReportMainInfoService;
//
//    @Autowired
//    private SurveyReportBasicInfoService SurveyReportBasicInfoService;
//
//    @Autowired
//    private  SurveyConInfoService SurveyConInfoService;
//
//    // 流水号生成服务
//    @Autowired
//    private SequenceTemplateService sequenceTemplateClient;
//
//    @Autowired
//    private ModelApprResultInfoService modelApprResultInfoService;
//
//    @Autowired
//    private PerferRateApplyInfoService perferRateApplyInfoService;
//
//    @Autowired
//    private SurveyReportBasicInfoService surveyReportBasicInfoService;
//
//    /**
//     * @方法名称: selectByPrimaryKey
//     * @方法描述: 根据主键查询
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public SurveyReportMainInfo selectByPrimaryKey(String surveyNo) {
//        return surveyReportMainInfoMapper.selectByPrimaryKey(surveyNo);
//    }
//
//    /**
//     * @方法名称: selectAll
//     * @方法描述: 查询所有数据
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    @Transactional(readOnly = true)
//    public List<SurveyReportMainInfo> selectAll(QueryModel model) {
//        List<SurveyReportMainInfo> records = (List<SurveyReportMainInfo>) surveyReportMainInfoMapper.selectByModel(model);
//        return records;
//    }
//
//    /**
//     * @方法名称: selectByModel
//     * @方法描述: 条件查询 - 查询进行分页
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public List<SurveyReportMainInfo> selectByModel(QueryModel model) {
//        PageHelper.startPage(model.getPage(), model.getSize());
//        List<SurveyReportMainInfo> list = surveyReportMainInfoMapper.selectByModel(model);
//        PageHelper.clearPage();
//        return list;
//    }
//
//    /**
//     * @方法名称: insert
//     * @方法描述: 插入
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int insert(SurveyReportMainInfo record) {
//        return surveyReportMainInfoMapper.insert(record);
//    }
//
//    /**
//     * @方法名称: insertSelective
//     * @方法描述: 插入 - 只插入非空字段
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int insertSelective(SurveyReportMainInfo record) {
//        return surveyReportMainInfoMapper.insertSelective(record);
//    }
//
//    /**
//     * @方法名称: update
//     * @方法描述: 根据主键更新
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int update(SurveyReportMainInfo record) {
//        return surveyReportMainInfoMapper.updateByPrimaryKey(record);
//    }
//
//    /**
//     * @方法名称: updateSelective
//     * @方法描述: 根据主键更新 - 只更新非空字段
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int updateSelective(SurveyReportMainInfo record) {
//        return surveyReportMainInfoMapper.updateByPrimaryKeySelective(record);
//    }
//
//    /**
//     * @方法名称: deleteByPrimaryKey
//     * @方法描述: 根据主键删除
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int deleteByPrimaryKey(String surveyNo) {
//        return surveyReportMainInfoMapper.deleteByPrimaryKey(surveyNo);
//    }
//
//    /**
//     * @方法名称: deleteByIds
//     * @方法描述: 根据多个主键删除
//     * @参数与返回说明:
//     * @算法描述: 无
//     */
//
//    public int deleteByIds(String ids) {
//        return surveyReportMainInfoMapper.deleteByIds(ids);
//    }
//
//
//    /**
//     * @author wzy
//     * @date ${DATE} ${TIME}
//     * @version 1.0.0
//     * @desc
//     * @修改历史:
//     * @方法描述: 根据调查主表主键，自动生成一个新的流水号，赋值原来的信息，更改审批状态
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public Map handleAgain(String serno) {
//        Map rtnData = new HashMap(CmisCommonConstants.DEF_MAP_SIZE);
//        String rtnCode = EcbEnum.ECB010000.key;
//        String rtnMsg = EcbEnum.ECB010000.value;
//        try{
//            //查询客户授信调查主表数据
//            SurveyReportMainInfo SurveyReportMainInfo = surveyReportMainInfoService.selectByPrimaryKey(serno);
//            //查询调查报告基本信息
//            SurveyReportBasicInfo SurveyReportBasicInfo  = SurveyReportBasicInfoService.selectByPrimaryKey(serno);
//            //查询调查结论信息
//            SurveyConInfo SurveyConInfo = SurveyConInfoService.selectByPrimaryKey(serno);
//            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
//
//            // 生成流水号异常 如空的情况
//            if(StringUtils.isEmpty(serno)){
//                rtnCode = EcbEnum.ECB010003.key;
//                rtnMsg = EcbEnum.ECB010003.value;
//                return rtnData;
//            }
//            //复制客户授信调查主表数据
//            SurveyReportMainInfo.setSurveyNo(serno);
//            SurveyReportMainInfo.setApprStatus("000");
//            int result = surveyReportMainInfoService.insertSelective(SurveyReportMainInfo);
//            //复制调查报告基本信息
//            SurveyReportBasicInfo.setSurveyNo(serno);
//            int result2 = SurveyReportBasicInfoService.insertSelective(SurveyReportBasicInfo);
//            //复制调查结论信息
//            SurveyConInfo.setSurveyNo(serno);
//            int result3 = SurveyConInfoService.insertSelective(SurveyConInfo);
//            if (result != 1 || result2 !=1 ||result3 !=1 ) {
//                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
//                throw new YuspException(EpbEnum.EPB099999.key,  EpbEnum.EPB099999.value+ ",生成授信调查信息失败！");
//            }
//        }catch (YuspException e) {
//            rtnCode = e.getCode();
//            rtnMsg = e.getMsg();
//            logger.info("授信调查重新办理返回错误"+e);
//        }catch (Exception e) {
//            rtnCode = EpbEnum.EPB099999.key;
//            rtnMsg = EpbEnum.EPB099999.value + "," + e.getMessage();
//            logger.info("授信调查重新办理返回错误"+e);
//        } finally {
//            rtnData.put("rtnCode", rtnCode);
//            rtnData.put("rtnMsg", rtnMsg);
//        }
//        return rtnData;
//    }
//
//    /**
//     * @return 提交审批进入审批流
//     * @创建人 WH
//     * @创建时间 20:06 2021-04-14
//     **/
//    public ResultDto<Integer> updatastatus(String id) {
//        //先查询是否完成模型审批
//        ModelApprResultInfo modelApprResultInfo = modelApprResultInfoService.selectByPrimaryKey(id);
//        if (modelApprResultInfo == null) {
//            return new ResultDto<Integer>(2).message("未完成模型审批,请审批后再试");
//        }
//        if (!"003".equals(modelApprResultInfo.getModelRstStatus())) {
//            return new ResultDto<Integer>(2).message("模型审批未通过,请等待审批通过后再试");
//        }
//        //查询是否存在审批中的优惠利率申请
//        PerferRateApplyInfo perferRateApplyInfo = perferRateApplyInfoService.selectByPrimaryKey(id);
//        if (perferRateApplyInfo != null && !CmisCommonConstants.WF_STATUS_997.equals(perferRateApplyInfo.getApproveoveStatus())) {
//            return new ResultDto<Integer>(2).message("该授信单下存在未审批通过的利率申请,请等待通过后再试");
//        }
//        //通过
//
//        return new ResultDto<Integer>(1).message("风控通过");
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 2021-04-16 16:48
//     * @注释 生成流水号 根据字段插入 记得修改 此时没有默认产品类型 无默认调查报告类型 均用假数据 因为无法根据产品信息带出相应数据
//     */
//    public ResultDto<SurveyReportMainInfo> installonedata(SurveyReportMainInfo surveyReportMainInfo) {
//        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
//        surveyReportMainInfo.setSurveyNo(serno);
//        //审批状态为待发起
//        surveyReportMainInfo.setApprStatus("000");
//        //数据来源为人工新增
//        surveyReportMainInfo.setDataSour("00");
//        //此时是假数据
//        if (surveyReportMainInfo.getSurveyType().equals("")){
//            surveyReportMainInfo.setSurveyType("03");
//        }
//         surveyReportMainInfoService.insertSelective(surveyReportMainInfo);
//        return new ResultDto<>(surveyReportMainInfo).message("新增成功");
//    }
//
//
//    /***
//     * @param surveyNo
//     * @return ResultDto<Boolean>
//     * @author 王玉坤
//     * @date 2021/4/17 15:03
//     * @version 1.0.0
//     * @desc
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public ResultDto<Boolean> submit(String surveyNo) {
//        // 调查报告主表信息
//        SurveyReportMainInfo surveyReportMainInfo = null;
//        // 调查报告基本信息
//        SurveyReportBasicInfo surveyReportBasicInfo = null;
//        // 模型审批结果
//        ModelApprResultInfo modelApprResultInfo = null;
//        // 查询优惠利率审批
//        PerferRateApplyInfo perferRateApplyInfo = null;
//        try {
//            // 1.根据流水号查询调查报告基本信息，若无，则提示客户未完善信息
//            surveyReportMainInfo = selectByPrimaryKey(surveyNo);
//            surveyReportBasicInfo = surveyReportBasicInfoService.selectByPrimaryKey(surveyNo);
//            if (surveyReportBasicInfo == null) {
//                logger.info("根据调查流水号{}未获取到调查报告基本信息，不允许提交！", surveyNo);
//                throw new YuspException(EcbEnum.ECB010006.key, "请完善调查报告信息！");
//            }
//
//            // 2.根据不同调查报告类型，做不同逻辑判断
//            // 2.1 惠享贷
//            if (LmtSurveyEnums.SURVEY_TYPE_04.getValue().equals(surveyReportMainInfo.getSurveyType())) {
//                // 2.1.1先查询是否完成模型审批
//                modelApprResultInfo = modelApprResultInfoService.selectByPrimaryKey(surveyNo);
//                if (modelApprResultInfo == null) {
//                    throw new YuspException(EcbEnum.ECB010006.key, "请先进行模型审批！");
//                } else {
//                    // 校验模型审批是否通过
//                    if (!"003".equals(modelApprResultInfo.getModelRstStatus())) {
//                        throw new YuspException(EcbEnum.ECB010006.key, "模型审批未通过！");
//                    }
//                }
//
//                // 2.1.2查询优惠利率申请是否在审批中
//                perferRateApplyInfo = perferRateApplyInfoService.selectByPrimaryKey(surveyNo);
//                if (perferRateApplyInfo != null && !CmisCommonConstants.WF_STATUS_997.equals(perferRateApplyInfo.getApproveoveStatus())) {
//                    throw new YuspException(EcbEnum.ECB010006.key, "存在审批中的优惠利率申请！");
//                }
//            }
//
//            // 返回结果
//            return new ResultDto<Boolean>(Boolean.TRUE);
//        } catch (YuspException e) {
//            logger.error(e.getCode(), e.getMessage());
//            e.printStackTrace();
//            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message(e.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("系统异常");
//        }
//
//    }
//}
