/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: MajorGradeInfo
 * @类描述: major_grade_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-30 10:43:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "major_grade_info")
public class MajorGradeInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 评级申请流水号 **/
	@Column(name = "GRADE_SERNO", unique = false, nullable = true, length = 40)
	private String gradeSerno;
	
	/** 授信申请流水号 **/
	@Column(name = "LMT_SERNO", unique = false, nullable = true, length = 40)
	private String lmtSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 评级年度 **/
	@Column(name = "GRADE_YEAR", unique = false, nullable = true, length = 10)
	private String gradeYear;
	
	/** 评级发生类型 **/
	@Column(name = "GRADE_HAPPEN_TYPE", unique = false, nullable = true, length = 5)
	private String gradeHappenType;
	
	/** 评级模型 **/
	@Column(name = "GRADE_MODE", unique = false, nullable = true, length = 10)
	private String gradeMode;
	
	/** 专业贷款系统初始等级 **/
	@Column(name = "INIT_GRADE", unique = false, nullable = true, length = 5)
	private String initGrade;
	
	/** 专业贷款调整后等级 **/
	@Column(name = "AFTER_GRADE", unique = false, nullable = true, length = 5)
	private String afterGrade;
	
	/** 专业贷款建议等级 **/
	@Column(name = "ADVICE_GRADE", unique = false, nullable = true, length = 5)
	private String adviceGrade;
	
	/** 专业贷款最终认定等级 **/
	@Column(name = "FINAL_GRADE", unique = false, nullable = true, length = 5)
	private String finalGrade;
	
	/** 专业贷款预期损失率 **/
	@Column(name = "EXPECTED_LOSS_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal expectedLossRate;
	
	/** 风险大类 **/
	@Column(name = "BIG_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String bigriskclass;
	
	/** 风险种类 **/
	@Column(name = "MID_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String midriskclass;
	
	/** 风险小类 **/
	@Column(name = "FEW_RISK_CLASS", unique = false, nullable = true, length = 5)
	private String fewriskclass;
	
	/** 风险划分日期 **/
	@Column(name = "RISK_DIVIDE_DATE", unique = false, nullable = true, length = 10)
	private String riskdividedate;
	
	/** 评级生效日 **/
	@Column(name = "INURE_DATE", unique = false, nullable = true, length = 10)
	private String inureDate;
	
	/** 评级到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 19)
	private java.util.Date updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 19)
	private java.util.Date updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 20)
	private String createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 20)
	private String updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param gradeSerno
	 */
	public void setGradeSerno(String gradeSerno) {
		this.gradeSerno = gradeSerno;
	}
	
    /**
     * @return gradeSerno
     */
	public String getGradeSerno() {
		return this.gradeSerno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno;
	}
	
    /**
     * @return lmtSerno
     */
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param gradeYear
	 */
	public void setGradeYear(String gradeYear) {
		this.gradeYear = gradeYear;
	}
	
    /**
     * @return gradeYear
     */
	public String getGradeYear() {
		return this.gradeYear;
	}
	
	/**
	 * @param gradeHappenType
	 */
	public void setGradeHappenType(String gradeHappenType) {
		this.gradeHappenType = gradeHappenType;
	}
	
    /**
     * @return gradeHappenType
     */
	public String getGradeHappenType() {
		return this.gradeHappenType;
	}
	
	/**
	 * @param gradeMode
	 */
	public void setGradeMode(String gradeMode) {
		this.gradeMode = gradeMode;
	}
	
    /**
     * @return gradeMode
     */
	public String getGradeMode() {
		return this.gradeMode;
	}
	
	/**
	 * @param initGrade
	 */
	public void setInitGrade(String initGrade) {
		this.initGrade = initGrade;
	}
	
    /**
     * @return initGrade
     */
	public String getInitGrade() {
		return this.initGrade;
	}
	
	/**
	 * @param afterGrade
	 */
	public void setAfterGrade(String afterGrade) {
		this.afterGrade = afterGrade;
	}
	
    /**
     * @return afterGrade
     */
	public String getAfterGrade() {
		return this.afterGrade;
	}
	
	/**
	 * @param adviceGrade
	 */
	public void setAdviceGrade(String adviceGrade) {
		this.adviceGrade = adviceGrade;
	}
	
    /**
     * @return adviceGrade
     */
	public String getAdviceGrade() {
		return this.adviceGrade;
	}
	
	/**
	 * @param finalGrade
	 */
	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}
	
    /**
     * @return finalGrade
     */
	public String getFinalGrade() {
		return this.finalGrade;
	}
	
	/**
	 * @param expectedLossRate
	 */
	public void setExpectedLossRate(java.math.BigDecimal expectedLossRate) {
		this.expectedLossRate = expectedLossRate;
	}
	
    /**
     * @return expectedLossRate
     */
	public java.math.BigDecimal getExpectedLossRate() {
		return this.expectedLossRate;
	}
	
	/**
	 * @param bigriskclass
	 */
	public void setBigriskclass(String bigriskclass) {
		this.bigriskclass = bigriskclass;
	}
	
    /**
     * @return bigriskclass
     */
	public String getBigriskclass() {
		return this.bigriskclass;
	}
	
	/**
	 * @param midriskclass
	 */
	public void setMidriskclass(String midriskclass) {
		this.midriskclass = midriskclass;
	}
	
    /**
     * @return midriskclass
     */
	public String getMidriskclass() {
		return this.midriskclass;
	}
	
	/**
	 * @param fewriskclass
	 */
	public void setFewriskclass(String fewriskclass) {
		this.fewriskclass = fewriskclass;
	}
	
    /**
     * @return fewriskclass
     */
	public String getFewriskclass() {
		return this.fewriskclass;
	}
	
	/**
	 * @param riskdividedate
	 */
	public void setRiskdividedate(String riskdividedate) {
		this.riskdividedate = riskdividedate;
	}
	
    /**
     * @return riskdividedate
     */
	public String getRiskdividedate() {
		return this.riskdividedate;
	}
	
	/**
	 * @param inureDate
	 */
	public void setInureDate(String inureDate) {
		this.inureDate = inureDate;
	}
	
    /**
     * @return inureDate
     */
	public String getInureDate() {
		return this.inureDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(java.util.Date updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public java.util.Date getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(java.util.Date updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public java.util.Date getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public String getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public String getUpdateTime() {
		return this.updateTime;
	}


}