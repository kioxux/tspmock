/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysMcdOperDetail;
import cn.com.yusys.yusp.service.RptSpdAnysMcdOperDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysMcdOperDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-29 15:59:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysmcdoperdetail")
public class RptSpdAnysMcdOperDetailResource {
    @Autowired
    private RptSpdAnysMcdOperDetailService rptSpdAnysMcdOperDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysMcdOperDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysMcdOperDetail> list = rptSpdAnysMcdOperDetailService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysMcdOperDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysMcdOperDetail>> index(QueryModel queryModel) {
        List<RptSpdAnysMcdOperDetail> list = rptSpdAnysMcdOperDetailService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysMcdOperDetail>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysMcdOperDetail> create(@RequestBody RptSpdAnysMcdOperDetail rptSpdAnysMcdOperDetail) throws URISyntaxException {
        rptSpdAnysMcdOperDetailService.insert(rptSpdAnysMcdOperDetail);
        return new ResultDto<RptSpdAnysMcdOperDetail>(rptSpdAnysMcdOperDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysMcdOperDetail rptSpdAnysMcdOperDetail) throws URISyntaxException {
        int result = rptSpdAnysMcdOperDetailService.update(rptSpdAnysMcdOperDetail);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = rptSpdAnysMcdOperDetailService.deleteByPrimaryKey(pkId, serno);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptSpdAnysMcdOperDetail>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptSpdAnysMcdOperDetail>>(rptSpdAnysMcdOperDetailService.selectByModel(model));
    }

    @PostMapping("/deleteMcdOperDetail")
    protected ResultDto<Integer> deleteMcdOperDetail(@RequestBody RptSpdAnysMcdOperDetail rptSpdAnysMcdOperDetail){
        return  new ResultDto<Integer>(rptSpdAnysMcdOperDetailService.deleteByPrimaryKey(rptSpdAnysMcdOperDetail.getPkId(),rptSpdAnysMcdOperDetail.getSerno()));
    }
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptSpdAnysMcdOperDetail rptSpdAnysMcdOperDetail){
        return new ResultDto<Integer>(rptSpdAnysMcdOperDetailService.save(rptSpdAnysMcdOperDetail));
    }
}
