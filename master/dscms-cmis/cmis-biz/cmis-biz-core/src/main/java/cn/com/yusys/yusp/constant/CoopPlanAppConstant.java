package cn.com.yusys.yusp.constant;

/**
 * 合作方
 */
public class CoopPlanAppConstant {
    //合作方协议准入增加
    public static String OPR_TYPE_ADD = "1";

    //合作方协议准入变更
    public static String OPR_TYPE_MODIFY = "2";

    //合作方案状态
    public static String COOP_PLAN_STATUS_SX = "1";

    //合作方案状态
    public static int LIST_ZERO_INDEX = 0;

    //请求失败
    public static int RESULT_FIAL = 1;

    //请求成功
    public static int RESULT_SUCCESS = 0;


    //审批状态，待发起
    public static String APPR_STATUS_START = "000";

    //数字0
    public static int NUMBER_ZERO = 0;

    //数字1
    public static int NUMBER_ONE = 1;

    //流水号字段名称
    public static String COOP_PLAN_SERNO = "serno";

    //合作方案编号
    public static String COOP_PLAN_NO = "coopPlanNo";

    //横线占位符
    public static String REPLACE_LINE = "-";

    //流水号字段名称
    public static String REPLACE_SPACE = "";

    //日期减一
    public static int COOP_PLAN_END_DATE_INDEX = -1;

    //日期格式化
    public static String COOP_PLAN_DATE_FORMAT = "yyyy-MM-dd";

    //合作方编号
    public static String PARTNER_NO = "partnerNo";



}
