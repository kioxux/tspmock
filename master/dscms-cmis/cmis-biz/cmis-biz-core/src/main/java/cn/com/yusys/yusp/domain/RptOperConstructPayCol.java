/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperConstructPayCol
 * @类描述: rpt_oper_construct_pay_col数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-16 14:12:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_construct_pay_col")
public class RptOperConstructPayCol extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 工程名称 **/
	@Column(name = "PROJECT_NAME", unique = false, nullable = true, length = 100)
	private String projectName;
	
	/** 工程金额 **/
	@Column(name = "PROJECT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal projectAmt;
	
	/** 工程进度 **/
	@Column(name = "PROJECT_SPEED", unique = false, nullable = true, length = 5)
	private String projectSpeed;
	
	/** 已回款金额 **/
	@Column(name = "PAY_REVICED_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payRevicedAmt;
	
	/** 应收账款 **/
	@Column(name = "ACCOUANT_REVICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal accouantRevice;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
    /**
     * @return projectName
     */
	public String getProjectName() {
		return this.projectName;
	}
	
	/**
	 * @param projectAmt
	 */
	public void setProjectAmt(java.math.BigDecimal projectAmt) {
		this.projectAmt = projectAmt;
	}
	
    /**
     * @return projectAmt
     */
	public java.math.BigDecimal getProjectAmt() {
		return this.projectAmt;
	}
	
	/**
	 * @param projectSpeed
	 */
	public void setProjectSpeed(String projectSpeed) {
		this.projectSpeed = projectSpeed;
	}
	
    /**
     * @return projectSpeed
     */
	public String getProjectSpeed() {
		return this.projectSpeed;
	}
	
	/**
	 * @param payRevicedAmt
	 */
	public void setPayRevicedAmt(java.math.BigDecimal payRevicedAmt) {
		this.payRevicedAmt = payRevicedAmt;
	}
	
    /**
     * @return payRevicedAmt
     */
	public java.math.BigDecimal getPayRevicedAmt() {
		return this.payRevicedAmt;
	}
	
	/**
	 * @param accouantRevice
	 */
	public void setAccouantRevice(java.math.BigDecimal accouantRevice) {
		this.accouantRevice = accouantRevice;
	}
	
    /**
     * @return accouantRevice
     */
	public java.math.BigDecimal getAccouantRevice() {
		return this.accouantRevice;
	}


}