/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FbManager;
import cn.com.yusys.yusp.service.FbManagerService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FbManagerResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-08 23:05:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/fbmanager")
public class FbManagerResource {
    @Autowired
    private FbManagerService fbManagerService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FbManager>> query() {
        QueryModel queryModel = new QueryModel();
        List<FbManager> list = fbManagerService.selectAll(queryModel);
        return new ResultDto<List<FbManager>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FbManager>> index(QueryModel queryModel) {
        List<FbManager> list = fbManagerService.selectByModel(queryModel);
        return new ResultDto<List<FbManager>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{fbCode}")
    protected ResultDto<FbManager> show(@PathVariable("fbCode") String fbCode) {
        FbManager fbManager = fbManagerService.selectByPrimaryKey(fbCode);
        return new ResultDto<FbManager>(fbManager);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FbManager> create(@RequestBody FbManager fbManager) throws URISyntaxException {
        fbManagerService.insert(fbManager);
        return new ResultDto<FbManager>(fbManager);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FbManager fbManager) throws URISyntaxException {
        int result = fbManagerService.update(fbManager);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{fbCode}")
    protected ResultDto<Integer> delete(@PathVariable("fbCode") String fbCode) {
        int result = fbManagerService.deleteByPrimaryKey(fbCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = fbManagerService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
