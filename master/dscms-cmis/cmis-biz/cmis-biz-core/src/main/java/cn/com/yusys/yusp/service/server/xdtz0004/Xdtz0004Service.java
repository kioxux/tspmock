package cn.com.yusys.yusp.service.server.xdtz0004;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.req.Xdtz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.Xdtz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.SmLookupItemService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:在查询经营性贷款借据信息
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0004Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0004Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private SmLookupItemService smLookupItemService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 交易码：Xdtz0004
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param xdtz0004DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0004DataRespDto xdtz0004(Xdtz0004DataReqDto xdtz0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataReqDto));
        //定义返回信息
        Xdtz0004DataRespDto xdtz0004DataRespDto = new Xdtz0004DataRespDto();
        try {
            java.util.List<BillList> retLists = null;
            java.util.List<String> cusIds = new ArrayList<>();
            String certCodes = xdtz0004DataReqDto.getCertNo();//请求字段

            if (certCodes.contains(",")) {
                String[] certCodeAry = certCodes.split(",");//分隔证件号
                for (int i = 0; i < certCodeAry.length; i++) {
                    String certCode = certCodeAry[i];
                    //通过客户证件号查询客户信息
                    logger.info("*********XDTZ0004**通过客户证件号查询客户信息开始,查询参数为:{}", JSON.toJSONString(certCode));
                    java.util.List<String> cusNos = commonService.getCusBaseByCertCode(certCode);
                    logger.info("*********XDTZ0004**通过客户证件号查询客户信息结束,返回结果为:{}", JSON.toJSONString(cusNos));
                    cusIds.addAll(cusNos);
                }
            } else {
                //通过客户证件号查询客户信息
                logger.info("*********XDTZ0004**通过客户证件号(单笔)查询客户信息开始,查询参数为:{}", JSON.toJSONString(certCodes));
                java.util.List<String> cusNos = commonService.getCusBaseByCertCode(certCodes);
                logger.info("*********XDTZ0004**通过客户证件号(单笔)查询客户信息结束,返回结果为:{}", JSON.toJSONString(cusNos));
                cusIds.addAll(cusNos);
            }

            /*****************************查询出所有经营性贷款的产品编号***********************************/
            String prdType = "01,02,03,04,05,06,07,08";//对公贷款+小微经营性贷款
            String prdId = "";
            java.util.List<String> prdIds = new ArrayList<>();
            logger.info("***********调用iCmisCfgClientService查询产品类别*START**************产品类型" + prdType);
            ResultDto<List<CfgPrdBasicinfoDto>> resultDto = iCmisCfgClientService.queryCfgPrdBasicInfoByPrdType(prdType);
            String prdCode = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                List<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoList = JSON.parseObject(JSON.toJSONString(resultDto.getData()), new TypeReference<List<CfgPrdBasicinfoDto>>() {
                });
                for (int i = 0; i < cfgPrdBasicinfoDtoList.size(); i++) {
                    CfgPrdBasicinfoDto cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoList.get(i);
                    prdId = cfgPrdBasicinfoDto.getPrdId();
                    if (i < cfgPrdBasicinfoDtoList.size() - 1) {
                        prdIds.add(prdId);
                    }
                }
            }
            logger.info("***********调用iCmisCfgClientService查询产品类别*END**************产品号" + prdIds);
            /*****************************查询出所有经营性贷款的产品编号结束***********************************/
            //查询台账信息
            Map queryMap = new HashMap();
            queryMap.put("cusIds", cusIds);
            queryMap.put("prdIds", prdIds);
            retLists = accLoanMapper.queryBusinessLoantails(queryMap);

            //查询对应的字典
            logger.info("获取字典项缓存开始");
            retLists = Optional.ofNullable(retLists).orElseThrow(() -> new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value));
            retLists = retLists.parallelStream().map(ret -> {
                BillList temp = new BillList();
                BeanUtils.copyProperties(ret, temp);
                //担保方式码值转换
                String guarType = temp.getGuarType();//参数值
                String guarTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_GUAR_WAY", guarType);
                temp.setGuarType(guarTypeName);
                //放款方式码值转换
                String repayType = temp.getDisbMode();
                String repayTypeName = DictTranslatorUtils.findValueByDictKey("STD_ZB_LOAN_MODE", repayType);
                temp.setDisbMode(repayTypeName);
                //获取客户信息
                String cusid = temp.getCertNo();
                CusBaseDto cusBaseDto = commonService.getCusBaseByCusId(cusid);
                String certCode = cusBaseDto.getCertCode();
                temp.setCertNo(certCode);
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            logger.info("获取字典项缓存结束");

            xdtz0004DataRespDto.setBillList(retLists);//返回参数
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataRespDto));
        return xdtz0004DataRespDto;
    }

}
