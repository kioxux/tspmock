package cn.com.yusys.yusp.service.server.xdzc0025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GuarWarrantManageApp;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.req.Xdzc0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.resp.Xdzc0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CtrAsplDetailsService;
import cn.com.yusys.yusp.service.Dscms2CoreCoClientService;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import cn.com.yusys.yusp.service.GuarWarrantManageAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * 接口处理类:信贷押品出库
 *
 * @Author xs
 * @Date 2021/6/21 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0025Service {

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0025.Xdzc0025Service.class);

    /**
     * 交易码：xdzc0025
     * 交易描述: 资产池业务开关检查
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0025DataRespDto xdzc0025Service(Xdzc0025DataReqDto xdzc0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value);
        Xdzc0025DataRespDto xdzc0025DataRespDto = new Xdzc0025DataRespDto();
        String openType = xdzc0025DataReqDto.getOpenType();// 检验开关类型
        String cusId = xdzc0025DataReqDto.getCusId();// 客户编号
        xdzc0025DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
        xdzc0025DataRespDto.setOpMsg("校验通过");
        try {
            // 检验开关 1超短贷款 0出票
            if(Objects.equals("1",openType)){
                if(!ctrAsplDetailsService.getAsplOpenLoan()){
                    xdzc0025DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                    xdzc0025DataRespDto.setOpMsg("资产池超短业务开关：关闭，请联系银行客户经理！");// 描述信息
                    return xdzc0025DataRespDto;
                }
            }else if(Objects.equals("0",openType)){
                if(!ctrAsplDetailsService.getAsplOpenAccp()){
                    xdzc0025DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                    xdzc0025DataRespDto.setOpMsg("资产池银票业务开关：关闭，请联系银行客户经理！");// 描述信息
                    return xdzc0025DataRespDto;
                }
            } else{
                throw BizException.error(null,"9999", "【openType】参数异常");
            }

            // 校验拦截名单
            if(Objects.nonNull(cusId)){
                if(!ctrAsplDetailsService.checkAsplBlacks(openType,cusId)){
                    xdzc0025DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                    xdzc0025DataRespDto.setOpMsg("客户【"+cusId+"】业务被拦截，请联系银行客户经理！");// 描述信息
                    return xdzc0025DataRespDto;
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value);
        return xdzc0025DataRespDto;
    }
}
