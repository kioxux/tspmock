/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherGrtValueBranchIdentySub
 * @类描述: other_grt_value_branch_identy_sub数据实体类
 * @功能描述: 
 * @创建人: xiaomei
 * @创建时间: 2021-06-11 16:39:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "other_grt_value_branch_identy_sub")
public class OtherGrtValueBranchIdentySub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = false, length = 40)
	private String guarNo;
	
	/** 房产确认价值 **/
	@Column(name = "REALPRO_CFIRM_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realproCfirmValue;
	
	/** 土地确认价值 **/
	@Column(name = "LAND_CFIRM_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landCfirmValue;
	
	/** 房地产确认总价 **/
	@Column(name = "REALPRO_CFIRM_TOTAL_VALUE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realproCfirmTotalValue;
	
	/** 该抵押物项下申请的融资金额 **/
	@Column(name = "PLD_APP_FIN_PRICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pldAppFinPrice;
	
	/** 房地产抵押情况 **/
	@Column(name = "REALPRO_PLD_CASE", unique = false, nullable = true, length = 5)
	private String realproPldCase;
	
	/** 抵押到期日 **/
	@Column(name = "PLD_END_DATE", unique = false, nullable = true, length = 20)
	private String pldEndDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param realproCfirmValue
	 */
	public void setRealproCfirmValue(java.math.BigDecimal realproCfirmValue) {
		this.realproCfirmValue = realproCfirmValue;
	}
	
    /**
     * @return realproCfirmValue
     */
	public java.math.BigDecimal getRealproCfirmValue() {
		return this.realproCfirmValue;
	}
	
	/**
	 * @param landCfirmValue
	 */
	public void setLandCfirmValue(java.math.BigDecimal landCfirmValue) {
		this.landCfirmValue = landCfirmValue;
	}
	
    /**
     * @return landCfirmValue
     */
	public java.math.BigDecimal getLandCfirmValue() {
		return this.landCfirmValue;
	}
	
	/**
	 * @param realproCfirmTotalValue
	 */
	public void setRealproCfirmTotalValue(java.math.BigDecimal realproCfirmTotalValue) {
		this.realproCfirmTotalValue = realproCfirmTotalValue;
	}
	
    /**
     * @return realproCfirmTotalValue
     */
	public java.math.BigDecimal getRealproCfirmTotalValue() {
		return this.realproCfirmTotalValue;
	}
	
	/**
	 * @param pldAppFinPrice
	 */
	public void setPldAppFinPrice(java.math.BigDecimal pldAppFinPrice) {
		this.pldAppFinPrice = pldAppFinPrice;
	}
	
    /**
     * @return pldAppFinPrice
     */
	public java.math.BigDecimal getPldAppFinPrice() {
		return this.pldAppFinPrice;
	}
	
	/**
	 * @param realproPldCase
	 */
	public void setRealproPldCase(String realproPldCase) {
		this.realproPldCase = realproPldCase;
	}
	
    /**
     * @return realproPldCase
     */
	public String getRealproPldCase() {
		return this.realproPldCase;
	}
	
	/**
	 * @param pldEndDate
	 */
	public void setPldEndDate(String pldEndDate) {
		this.pldEndDate = pldEndDate;
	}
	
    /**
     * @return pldEndDate
     */
	public String getPldEndDate() {
		return this.pldEndDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}