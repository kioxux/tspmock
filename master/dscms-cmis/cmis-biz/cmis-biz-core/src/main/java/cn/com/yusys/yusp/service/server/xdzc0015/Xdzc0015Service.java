package cn.com.yusys.yusp.service.server.xdzc0015;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.BailAccInfo;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.req.Xdzc0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.resp.Xdzc0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * 接口处理类:保证金接口
 *
 * @Author xs
 * @Date 2021/15/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0015Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0015Service.class);

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    @Autowired
    private GrtGuarContService grtGuarContService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;


    @Autowired
    private AsplAssetsListService asplAssetsListService;

    /**
     * 交易码：xdzc0015
     * 交易描述：参考老信贷代码 PjcAction 关键字 wy2pjc007
     * 1:查询保证金账号  2:查询可提取保证金  3:提取保证金  4:查询客户是否支持超短贷功能
     *
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0015DataRespDto xdzc0015Service(Xdzc0015DataReqDto xdzc0015DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value);
        Xdzc0015DataRespDto xdzc0015DataRespDto = new Xdzc0015DataRespDto();

        String cusId = xdzc0015DataReqDto.getCusId();//客户编号
        String cusAccount = xdzc0015DataReqDto.getCusAccount();//客户账号
        BigDecimal tqAmount = xdzc0015DataReqDto.getTqAmount();//提取金额
        String grteac = xdzc0015DataReqDto.getGrteac();//保证金账号
        //查询类型  1:查询保证金账号2:查询可提取保证金3:提取保证金
        String martype = xdzc0015DataReqDto.getMartype();

        // 保证金可提取金额计算
        BigDecimal assetPoolBailAmtDeciaml = BigDecimal.ZERO;
        try {
            //一个客户只有一笔有效的资产池协议
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            if (ctrAsplDetails == null) {
                return xdzc0015DataRespDto;
            }
            String serno = ctrAsplDetails.getSerno();
            List<BailAccInfo> bailAccInfoList = bailAccInfoService.selectBySerno(serno);
            BailAccInfo bailAccInfo = new BailAccInfo();
            if (CollectionUtils.nonEmpty(bailAccInfoList)) {
                bailAccInfo = bailAccInfoList.get(0);
            }
            // 获取保证金留存比例
            BigDecimal bailRate = bailAccInfo.getBailRate();

            Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
            dp2099ReqDto.setKehuzhao(bailAccInfo.getBailAccNo());// 保证金账号
            dp2099ReqDto.setZhhaoxuh(bailAccInfo.getBailAccNoSub());// 子账户序号
            ResultDto<Dp2099RespDto> Dp2099ResultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);
            Dp2099RespDto dp2099RespDto = new Dp2099RespDto();
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,Dp2099ResultDto.getCode())) {
                dp2099RespDto = Dp2099ResultDto.getData();
                //生效状态下的资产池只有一个保证金账户
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List> dp2099RespDtoLists = dp2099RespDto.getList();
                if (CollectionUtils.nonEmpty(dp2099RespDtoLists)) {
                    assetPoolBailAmtDeciaml = dp2099RespDtoLists
                            .stream()
                            .map(e -> e.getZhanghye())
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    logger.info("保证金账户余额总额："+ Objects.toString(assetPoolBailAmtDeciaml));
                }
            }

            String guarContNo = grtGuarBizRstRelService.selectGuarContNoBycontNo(ctrAsplDetails.getContNo());
            xdzc0015DataRespDto.setBailAccAmt(assetPoolBailAmtDeciaml);// 保证金账户金额
            xdzc0015DataRespDto.setBailAccNo(bailAccInfo.getBailAccNo());// 保证金账户
            xdzc0015DataRespDto.setBailOrgNO(bailAccInfo.getAcctsvcrNo());// 保证金开户行号
            xdzc0015DataRespDto.setBailOrgName(bailAccInfo.getAcctsvcrName());// 保证金开户行行名
            xdzc0015DataRespDto.setGuarContNo(guarContNo);// 担保合同编号
            // 担保合同中文编号BUYONGL
            //xdzc0015DataRespDto.setGuarContCnNo(grtGuarCont.getGuarContCnNo());// 担保合同中文编号
            xdzc0015DataRespDto.setContNO(ctrAsplDetails.getContNo());// 资产池协议号
            xdzc0015DataRespDto.setContName(ctrAsplDetails.getContCnNo());// 资产池协议名称
            xdzc0015DataRespDto.setEndDate(ctrAsplDetails.getEndDate());// 资产池协议名称

            String isBegin = ctrAsplDetails.getIsBegin();
            // 保证金账户余额 * 保证金可提取比例
            BigDecimal assetPoolBailAmt = assetPoolBailAmtDeciaml.multiply(BigDecimal.ONE.subtract(bailRate));
            // 资产池可用融资额度 = 已质押入池资产 * 质押率 + 保证金账户余额 - 资产池下融资余额
            // 资产池融资额度
            BigDecimal assetPoolFinAmt = asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails,assetPoolBailAmtDeciaml);
            // 资产池可用额度
            BigDecimal assetPoolAvaAmt = asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails,assetPoolFinAmt,assetPoolBailAmt);
            // 取最小值
            xdzc0015DataRespDto.setBailAvaAmt(assetPoolBailAmt.min(assetPoolAvaAmt));// 可提取的保证金账户金额

            if ("1".equals(martype)) {
                // 查询保证金账号
                xdzc0015DataRespDto.setRetcod("0000");
                xdzc0015DataRespDto.setOpMessage("保证金查询正常");
            } else if ("2".equals(martype)) {
                // 查询可提取保证金
                // 资产池已使用金额
                // 超短贷款已使用额度
                // TODO 需要完善老代码太过复杂
                xdzc0015DataRespDto.setRetcod("0000");
                xdzc0015DataRespDto.setOpMessage("保证金提取校验成功");

            } else if ("3".equals(martype)) {
                // 提取保证金 ()
                // TODO 需要完善老代码太过复杂
                xdzc0015DataRespDto.setRetcod("0000");
                xdzc0015DataRespDto.setOpMessage("");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value);
        return xdzc0015DataRespDto;
    }
}