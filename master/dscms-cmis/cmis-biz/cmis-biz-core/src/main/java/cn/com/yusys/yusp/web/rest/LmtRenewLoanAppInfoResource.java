/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.dto.LmtRenewLoanAppCustInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtRenewLoanAppInfo;
import cn.com.yusys.yusp.service.LmtRenewLoanAppInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRenewLoanAppInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-02 09:10:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtrenewloanappinfo")
public class LmtRenewLoanAppInfoResource {
    @Autowired
    private LmtRenewLoanAppInfoService lmtRenewLoanAppInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRenewLoanAppInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRenewLoanAppInfo> list = lmtRenewLoanAppInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtRenewLoanAppInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRenewLoanAppInfo>> index(QueryModel queryModel) {
        List<LmtRenewLoanAppInfo> list = lmtRenewLoanAppInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtRenewLoanAppInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtRenewLoanAppInfo> show(@PathVariable("serno") String serno) {
        LmtRenewLoanAppInfo lmtRenewLoanAppInfo = lmtRenewLoanAppInfoService.selectByPrimaryKey(serno);
        return new ResultDto<LmtRenewLoanAppInfo>(lmtRenewLoanAppInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtRenewLoanAppInfo> create(@RequestBody LmtRenewLoanAppInfo lmtRenewLoanAppInfo) throws URISyntaxException {
        lmtRenewLoanAppInfoService.insert(lmtRenewLoanAppInfo);
        return new ResultDto<LmtRenewLoanAppInfo>(lmtRenewLoanAppInfo);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtRenewLoanAppInfo> save(@RequestBody LmtRenewLoanAppInfo lmtRenewLoanAppInfo) throws URISyntaxException {
        lmtRenewLoanAppInfoService.save(lmtRenewLoanAppInfo);
        return new ResultDto<LmtRenewLoanAppInfo>(lmtRenewLoanAppInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRenewLoanAppInfo lmtRenewLoanAppInfo) throws URISyntaxException {
        int result = lmtRenewLoanAppInfoService.update(lmtRenewLoanAppInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtRenewLoanAppInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtRenewLoanAppInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:getRenewLoanCusInfo
     * @函数描述:分页查询相关客户信息，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/getRenewLoanCusInfo")
    protected ResultDto<List<LmtRenewLoanAppCustInfo>> getRenewLoanCusInfo(QueryModel queryModel) {
        List<LmtRenewLoanAppCustInfo> list = lmtRenewLoanAppInfoService.getRenewLoanCusInfo(queryModel);
        return new ResultDto<List<LmtRenewLoanAppCustInfo>>(list);
    }

}
