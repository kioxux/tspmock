package cn.com.yusys.yusp.service.server.xddb0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarWarrantInfo;
import cn.com.yusys.yusp.dto.server.xddb0014.req.Xddb0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0014.resp.Xddb0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import cn.com.yusys.yusp.service.GuarContRelWarrantService;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 接口处理类:抵押登记注销风控
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xddb0014Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0014Service.class);
    @Resource
    private GuarBaseInfoService guarBaseInfoService;
    @Resource
    private GuarWarrantInfoService guarWarrantInfoService;
    @Resource
    private GuarContRelWarrantService guarContRelWarrantService;

    /**
     * 抵押登记注销风控
     *
     * @param xddb0014DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0014DataRespDto selcetGuarBaseInfoByCusName(Xddb0014DataReqDto xddb0014DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value);
        Xddb0014DataRespDto xddb0014DataRespDto = new Xddb0014DataRespDto();
        try {
            String retyxpe = xddb0014DataReqDto.getRetype();//登记类型
            String coreGrtNo = xddb0014DataReqDto.getGuarid();//押品编号

            if (DscmsBizDbEnum.RETYPE_01.key.equals(retyxpe)) {//注销类型
                GuarWarrantInfo guarWarrantInfo = guarWarrantInfoService.selectByCoreGuarantyNo(coreGrtNo);
                String certiState = guarWarrantInfo.getCertiState();

                //押品所属权证未出库，不能注销
                if (!CmisBizConstants.STD_ZB_CERTI_STATE_10.equals(certiState)) {
                    xddb0014DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                    xddb0014DataRespDto.setOpMsg("押品所属权证【"+coreGrtNo+"】不是出库已记账状态，不能注销");
                } else {//允许注销
                    xddb0014DataRespDto.setOpFlag(DscmsBizDbEnum.FALG_SUCCESS.key);
                    xddb0014DataRespDto.setOpMsg(StringUtils.EMPTY);
                }
            } else {//其他类型不能注销
                xddb0014DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);// 操作成功标志位
                xddb0014DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value);
        return xddb0014DataRespDto;
    }
}
