package cn.com.yusys.yusp.service.client.bsp.gaps.cljctz;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req.CljctzReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.resp.CljctzRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2GapsClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Objects;
import java.util.Optional;

/**
 * @author shenli
 * @version 1.0.0
 * @date 2021-10-12 17:07:29
 * @desc    连云港-资金缴存通知
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class CljctzService {
    private static final Logger logger = LoggerFactory.getLogger(CljctzService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2GapsClientService dscms2GapsClientService;

    /**
     * @param cljctzReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto
     * @author shenli
     * @date 2021-10-12 17:11:50
     * @version 1.0.0
     * @desc    连云港-资金缴存通知
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CljctzRespDto cljctz (CljctzReqDto cljctzReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value, JSON.toJSONString(cljctzReqDto));
        ResultDto<CljctzRespDto> cljctzRespDtoResultDto = dscms2GapsClientService.cljctz(cljctzReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value, JSON.toJSONString(cljctzRespDtoResultDto));
        String idchekCode = Optional.ofNullable(cljctzRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String idchekMeesage = Optional.ofNullable(cljctzRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CljctzRespDto cljctzRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cljctzRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            cljctzRespDto = cljctzRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(idchekCode, idchekMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value);
        return cljctzRespDto;
    }
}
