package cn.com.yusys.yusp.dto;

public class BasicAndSingDto {
    private String singNo;
    private String singDate;

    public String getSingNo() {
        return singNo;
    }

    public void setSingNo(String singNo) {
        this.singNo = singNo;
    }

    public String getSingDate() {
        return singDate;
    }

    public void setSingDate(String singDate) {
        this.singDate = singDate;
    }
}
