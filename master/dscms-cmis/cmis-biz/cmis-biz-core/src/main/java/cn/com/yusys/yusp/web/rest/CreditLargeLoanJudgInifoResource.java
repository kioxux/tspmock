/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditLargeLoanJudgInifo;
import cn.com.yusys.yusp.service.CreditLargeLoanJudgInifoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditLargeLoanJudgInifoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-31 17:14:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditlargeloanjudginifo")
public class CreditLargeLoanJudgInifoResource {
    @Autowired
    private CreditLargeLoanJudgInifoService creditLargeLoanJudgInifoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditLargeLoanJudgInifo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditLargeLoanJudgInifo> list = creditLargeLoanJudgInifoService.selectAll(queryModel);
        return new ResultDto<List<CreditLargeLoanJudgInifo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditLargeLoanJudgInifo>> index(QueryModel queryModel) {
        List<CreditLargeLoanJudgInifo> list = creditLargeLoanJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditLargeLoanJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditLargeLoanJudgInifo> show(@PathVariable("pkId") String pkId) {
        CreditLargeLoanJudgInifo creditLargeLoanJudgInifo = creditLargeLoanJudgInifoService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditLargeLoanJudgInifo>(creditLargeLoanJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增大额分期放款审批信息")
    @PostMapping("/")
    protected ResultDto<CreditLargeLoanJudgInifo> create(@RequestBody CreditLargeLoanJudgInifo creditLargeLoanJudgInifo) throws URISyntaxException {
        creditLargeLoanJudgInifoService.insert(creditLargeLoanJudgInifo);
        return new ResultDto<CreditLargeLoanJudgInifo>(creditLargeLoanJudgInifo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的新增大额分期放款审批信息")
    @PostMapping("/save")
    protected ResultDto<CreditLargeLoanJudgInifo> save(@RequestBody CreditLargeLoanJudgInifo creditLargeLoanJudgInifo) throws URISyntaxException {
        creditLargeLoanJudgInifoService.save(creditLargeLoanJudgInifo);
        return new ResultDto<CreditLargeLoanJudgInifo>(creditLargeLoanJudgInifo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("更新大额分期放款审批信息")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditLargeLoanJudgInifo creditLargeLoanJudgInifo) throws URISyntaxException {
        int result = creditLargeLoanJudgInifoService.update(creditLargeLoanJudgInifo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditLargeLoanJudgInifoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditLargeLoanJudgInifoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期放款审批信息")
    @PostMapping("/queryModel")
    protected ResultDto<List<CreditLargeLoanJudgInifo>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditLargeLoanJudgInifo> list = creditLargeLoanJudgInifoService.selectByModel(queryModel);
        return new ResultDto<List<CreditLargeLoanJudgInifo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键查询大额分期放款审批信息")
    @PostMapping("/{pkId}")
    protected ResultDto<CreditLargeLoanJudgInifo> showPost(@RequestBody CreditLargeLoanJudgInifo creditLargeLoanJudgInifos) {
        CreditLargeLoanJudgInifo creditLargeLoanJudgInifo = creditLargeLoanJudgInifoService.selectByPrimaryKey(creditLargeLoanJudgInifos.getPkId());
        return new ResultDto<CreditLargeLoanJudgInifo>(creditLargeLoanJudgInifo);
    }
}
