/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.domain.SigInvestDetailsInfo;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.service.SigInvestDetailsInfoService;
import cn.com.yusys.yusp.vo.SigInvestDetailsImportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SigInvestDetailsInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-22 10:10:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/siginvestdetailsinfo")
public class SigInvestDetailsInfoResource {
    private static final Logger log = LoggerFactory.getLogger(SigInvestDetailsInfoResource.class);
    @Autowired
    private SigInvestDetailsInfoService sigInvestDetailsInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<SigInvestDetailsInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<SigInvestDetailsInfo> list = sigInvestDetailsInfoService.selectAll(queryModel);
        return new ResultDto<List<SigInvestDetailsInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<SigInvestDetailsInfo>> index(QueryModel queryModel) {
        List<SigInvestDetailsInfo> list = sigInvestDetailsInfoService.selectByModel(queryModel);
        return new ResultDto<List<SigInvestDetailsInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<SigInvestDetailsInfo> show(@PathVariable("pkId") String pkId) {
        SigInvestDetailsInfo sigInvestDetailsInfo = sigInvestDetailsInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<SigInvestDetailsInfo>(sigInvestDetailsInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<SigInvestDetailsInfo> create(@RequestBody SigInvestDetailsInfo sigInvestDetailsInfo) throws URISyntaxException {
        sigInvestDetailsInfoService.insert(sigInvestDetailsInfo);
        return new ResultDto<SigInvestDetailsInfo>(sigInvestDetailsInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody SigInvestDetailsInfo sigInvestDetailsInfo) throws URISyntaxException {
        int result = sigInvestDetailsInfoService.update(sigInvestDetailsInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = sigInvestDetailsInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = sigInvestDetailsInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *异步下载底层资产明细模板
     */
    @PostMapping("/exportsiginvestdetailstemplate")
    protected ResultDto<ProgressDto> asyncExportSigInvestDetailsTemplate() {
        ProgressDto progressDto = sigInvestDetailsInfoService.asyncExportSigInvestDetailsTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * 异步导出底层资产明细
     */
    @PostMapping("/asyncexportsiginvestdetailsinfo")
    public ResultDto<ProgressDto> asyncExportSigInvestDetailsInfo(@RequestBody SigInvestDetailsInfo sigInvestDetailsInfo) {
        ProgressDto progressDto = sigInvestDetailsInfoService.asyncExportSigInvestDetailsInfo(sigInvestDetailsInfo);
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importsiginvestdetailsinfo")
    public ResultDto<ProgressDto> asyncImportSigInvestDetailsInfo(@RequestParam("fileId") String fileId) {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = null;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        } catch (IOException e) {
            log.error(EclEnum.ECN060021.value,e);
            throw BizException.error(null, EclEnum.ECN060021.key,EclEnum.ECN060021.value);
        }

        // 将文件内容导入数据库，StudentScore为导入数据的类
        try {
            ExcelUtils.syncImport(SigInvestDetailsImportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return sigInvestDetailsInfoService.insertSigInvestDetails(dataList);
            }), true);
        } catch (Exception e) {
            log.error(EclEnum.ECL070094.value,e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
//        log.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success().message("导入成功！");
    }

    /**
     * 条件查询
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected  ResultDto<List<SigInvestDetailsInfo>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<SigInvestDetailsInfo>>(sigInvestDetailsInfoService.selectByModel(model));
    }

    /**
     * 条件查询
     * @param model
     * @return
     */
    @PostMapping("/selectByModelForInfoDetail")
    protected  ResultDto<List<SigInvestDetailsInfo>> selectByModelForInfoDetail(@RequestBody QueryModel model){
        return new ResultDto<List<SigInvestDetailsInfo>>(sigInvestDetailsInfoService.selectByModel(model));
    }

    /**
     * 新增修改底层资产明细
     */
    @PostMapping("/updateSigInvestDetail")
    protected ResultDto<Integer> updateSigInvestDetail(@RequestBody SigInvestDetailsInfo sigInvestDetailsInfo){
        return  new ResultDto<Integer>(sigInvestDetailsInfoService.updateSigInvestDetail(sigInvestDetailsInfo));
    }

    /**
     * 根据流水号查询
     * @param map
     * @return
     */
    @PostMapping("/selectByPkId")
    protected  ResultDto<SigInvestDetailsInfo> selectByPkId(@RequestBody Map<String,Object> map){
        String pkId = map.get("pkId").toString();
        return new ResultDto<SigInvestDetailsInfo>(sigInvestDetailsInfoService.selectByPrimaryKey(pkId));
    }

    /**
     * 逻辑删除
     */
    @PostMapping("deleteSigInvestDetail")
    protected ResultDto<Integer> deleteSigInvestDetail(@RequestBody Map<String,Object> map){
        String pkId = map.get("pkId").toString();
        SigInvestDetailsInfo sigInvestDetailsInfo = new SigInvestDetailsInfo();
        sigInvestDetailsInfo.setPkId(pkId);
        sigInvestDetailsInfo.setOprType(CommonConstance.OPR_TYPE_DELETE);
        Date date = new Date();
        sigInvestDetailsInfo.setUpdateTime(date);
        return new ResultDto<Integer>(sigInvestDetailsInfoService.updateSelective(sigInvestDetailsInfo));
    }
}
