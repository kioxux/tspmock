package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.LmtOtherAppRel;
import cn.com.yusys.yusp.domain.OtherGrtValueIdentyApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.LmtOtherAppRelService;
import cn.com.yusys.yusp.service.OtherGrtValueIdentyAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RiskItem0096Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0096Service.class);

   @Autowired
    private  OtherGrtValueIdentyAppService otherGrtValueIdentyAppService;

   @Autowired
    private LmtOtherAppRelService lmtOtherAppRelService;

    public RiskResultDto riskItem0096 (QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        String serno = queryModel.getCondition().get("bizId").toString();//业务流水号
        // 获取抵质押物价值认定信息 lmt_other_app_rel
        QueryModel model = new QueryModel();
        model.addCondition("lmtSerno",serno);
        model.addCondition("oprType","01");
        // 其他事项类型 10  授信抵质押物价值认定申请
        String lmtOtherAppType = "";
        // 其他事项申请流水号
        String lmtOtherAppSerno ="";
        List<LmtOtherAppRel> lmtOtherAppRelList = lmtOtherAppRelService.selectByLmtSerno(model);
        for (int i = 0; i < lmtOtherAppRelList.size(); i++) {
            LmtOtherAppRel lmtOtherAppRel = lmtOtherAppRelList.get(i);
            // 其他事项类型
            lmtOtherAppType = lmtOtherAppRel.getLmtOtherAppType();
           if("10".equals(lmtOtherAppType)){
               lmtOtherAppSerno = lmtOtherAppRel.getLmtOtherAppSerno();
           }
        }
        if(!"".equals(lmtOtherAppSerno)){
            queryModel.addCondition("serno",lmtOtherAppSerno);
            queryModel.addCondition("oprType","01");
            List<OtherGrtValueIdentyApp> otherGrtValueIdentyApps = otherGrtValueIdentyAppService.selectByModel(queryModel);
            for (int i = 0; i < otherGrtValueIdentyApps.size(); i++) {
                OtherGrtValueIdentyApp otherGrtValueIdentyApp = otherGrtValueIdentyApps.get(i);
                if(!"997".equals(otherGrtValueIdentyApp.getApproveStatus())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc("授信抵押价值认定申请未通过,请确认["+otherGrtValueIdentyApp.getSerno()+"]");
                    return riskResultDto;
                }
            }
        }
        return riskResultDto;
    }
}
