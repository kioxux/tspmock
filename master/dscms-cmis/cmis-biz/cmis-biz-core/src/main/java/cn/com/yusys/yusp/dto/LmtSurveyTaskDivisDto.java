package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyTaskDivis
 * @类描述: lmt_survey_task_divis数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-10 22:25:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyTaskDivisDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 第三方业务流水号 **/
	private String bizSerno;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品类型 **/
	private String prdType;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 申请渠道 **/
	private String appChnl;
	
	/** 贷款类别 **/
	private String loanType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;
	
	/** 手机号码 **/
	private String phone;
	
	/** 工作 **/
	private String work;
	
	/** 贷款用途 **/
	private String loanPurp;
	
	/** 客户经理名称 **/
	private String managerName;
	
	/** 客户经理编号 **/
	private String managerId;
	
	/** 客户经理片区 **/
	private String managerArea;
	
	/** 是否线下调查 **/
	private String isStopOffline;
	
	/** 进件时间 **/
	private String intoTime;
	
	/** 分配状态 **/
	private String divisStatus;
	
	/** 分配时间 **/
	private String divisTime;
	
	/** 分配人 **/
	private String divisId;
	
	/** 营销人 **/
	private String marId;
	
	/** 处理人 **/
	private String prcId;
	
	/** 所属条线 **/
	private String belgLine;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType == null ? null : prdType.trim();
	}
	
    /**
     * @return PrdType
     */	
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl == null ? null : appChnl.trim();
	}
	
    /**
     * @return AppChnl
     */	
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType == null ? null : loanType.trim();
	}
	
    /**
     * @return LoanType
     */	
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param work
	 */
	public void setWork(String work) {
		this.work = work == null ? null : work.trim();
	}
	
    /**
     * @return Work
     */	
	public String getWork() {
		return this.work;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp == null ? null : loanPurp.trim();
	}
	
    /**
     * @return LoanPurp
     */	
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerArea
	 */
	public void setManagerArea(String managerArea) {
		this.managerArea = managerArea == null ? null : managerArea.trim();
	}
	
    /**
     * @return ManagerArea
     */	
	public String getManagerArea() {
		return this.managerArea;
	}
	
	/**
	 * @param isStopOffline
	 */
	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline == null ? null : isStopOffline.trim();
	}
	
    /**
     * @return IsStopOffline
     */	
	public String getIsStopOffline() {
		return this.isStopOffline;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime == null ? null : intoTime.trim();
	}
	
    /**
     * @return IntoTime
     */	
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param divisStatus
	 */
	public void setDivisStatus(String divisStatus) {
		this.divisStatus = divisStatus == null ? null : divisStatus.trim();
	}
	
    /**
     * @return DivisStatus
     */	
	public String getDivisStatus() {
		return this.divisStatus;
	}
	
	/**
	 * @param divisTime
	 */
	public void setDivisTime(String divisTime) {
		this.divisTime = divisTime == null ? null : divisTime.trim();
	}
	
    /**
     * @return DivisTime
     */	
	public String getDivisTime() {
		return this.divisTime;
	}
	
	/**
	 * @param divisId
	 */
	public void setDivisId(String divisId) {
		this.divisId = divisId == null ? null : divisId.trim();
	}
	
    /**
     * @return DivisId
     */	
	public String getDivisId() {
		return this.divisId;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId == null ? null : marId.trim();
	}
	
    /**
     * @return MarId
     */	
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param prcId
	 */
	public void setPrcId(String prcId) {
		this.prcId = prcId == null ? null : prcId.trim();
	}
	
    /**
     * @return PrcId
     */	
	public String getPrcId() {
		return this.prcId;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}
	
    /**
     * @return BelgLine
     */	
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}