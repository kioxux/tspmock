/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PerferRateApplyInfo;
import cn.com.yusys.yusp.service.PerferRateApplyInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PerferRateApplyInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-13 10:17:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/perferrateapplyinfo")
public class PerferRateApplyInfoResource {
    @Autowired
    private PerferRateApplyInfoService perferRateApplyInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PerferRateApplyInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<PerferRateApplyInfo> list = perferRateApplyInfoService.selectAll(queryModel);
        return new ResultDto<List<PerferRateApplyInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PerferRateApplyInfo>> index(QueryModel queryModel) {
        List<PerferRateApplyInfo> list = perferRateApplyInfoService.selectByModel(queryModel);
        return new ResultDto<List<PerferRateApplyInfo>>(list);
    }

    /**
     * @param surveyNo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.PerferRateApplyInfo>
     * @author 王玉坤
     * @date 2021/4/16 17:10
     * @version 1.0.0
     * @desc 查询单个对象，公共API接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/{surveyNo}")
    protected ResultDto<PerferRateApplyInfo> show(@PathVariable("surveyNo") String surveyNo) {
        PerferRateApplyInfo perferRateApplyInfo = perferRateApplyInfoService.selectByPrimaryKey(surveyNo);
        return new ResultDto<PerferRateApplyInfo>(perferRateApplyInfo);
    }

    /**
     * @param perferRateApplyInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.PerferRateApplyInfo>
     * @author 王玉坤
     * @date 2021/4/16 17:17
     * @version 1.0.0
     * @desc 根据调查流水号插入或更新优惠利率信息
     * @修改历史:
     */
    @PostMapping("/savebysurveyNo")
    protected ResultDto<Map> saveBySurveyNo(@RequestBody PerferRateApplyInfo perferRateApplyInfo) {
        return perferRateApplyInfoService.saveBySurveyNo(perferRateApplyInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PerferRateApplyInfo> create(@RequestBody PerferRateApplyInfo perferRateApplyInfo) throws URISyntaxException {
        perferRateApplyInfoService.insert(perferRateApplyInfo);
        return new ResultDto<PerferRateApplyInfo>(perferRateApplyInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PerferRateApplyInfo perferRateApplyInfo) throws URISyntaxException {
        int result = perferRateApplyInfoService.update(perferRateApplyInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{surveyNo}")
    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
        int result = perferRateApplyInfoService.deleteByPrimaryKey(surveyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = perferRateApplyInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @创建人 WH
     * @创建时间 14:27 2021-04-13
     * @return 废弃优惠利率审批接口  updateSelective
     **/
    @GetMapping("/updateSelective/{surveyNo}")
    protected ResultDto<Integer> updateSelective(@PathVariable String surveyNo) {
        PerferRateApplyInfo perferRateApplyInfo = new PerferRateApplyInfo();
        perferRateApplyInfo.setSurveyNo(surveyNo);
        perferRateApplyInfo.setApproveoveStatus("200");
        int result = perferRateApplyInfoService.updateSelective(perferRateApplyInfo);
        return new ResultDto<Integer>(result);
    }

}
