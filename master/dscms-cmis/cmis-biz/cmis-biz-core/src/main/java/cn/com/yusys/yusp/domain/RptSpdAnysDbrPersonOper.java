/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysDbrPersonOper
 * @类描述: rpt_spd_anys_dbr_person_oper数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-24 11:17:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_dbr_person_oper")
public class RptSpdAnysDbrPersonOper extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 担保公司名称 **/
	@Column(name = "GUAR_CORP_NAME", unique = false, nullable = true, length = 80)
	private String guarCorpName;
	
	/** 担保公司内部评级 **/
	@Column(name = "GUAR_CORP_INNER_GRADE", unique = false, nullable = true, length = 40)
	private String guarCorpInnerGrade;
	
	/** 担保公司外部评级 **/
	@Column(name = "GUAR_CORP_OUTER_GRADE", unique = false, nullable = true, length = 40)
	private String guarCorpOuterGrade;
	
	/** 经营场所类型备注 **/
	@Column(name = "BUSI_PREMISE_TYPE_REMARK", unique = false, nullable = true, length = 65535)
	private String busiPremiseTypeRemark;
	
	/** 经营场所类型 **/
	@Column(name = "BUSI_PREMISE_TYPE", unique = false, nullable = true, length = 40)
	private String busiPremiseType;
	
	/** 经营场所变更次数 **/
	@Column(name = "BUSI_PREMISE_CHG_TIMES", unique = false, nullable = true, length = 40)
	private String busiPremiseChgTimes;
	
	/** 经营场所变更次数备注 **/
	@Column(name = "BUSI_PREMISE_CHG_TIMES_REMARK", unique = false, nullable = true, length = 65535)
	private String busiPremiseChgTimesRemark;
	
	/** 企业上年度所有供应商的总供应量 **/
	@Column(name = "LAST_YEAR_SUP_SUM", unique = false, nullable = true, length = 40)
	private String lastYearSupSum;
	
	/** 企业上年度所有供应商的总供应量备注 **/
	@Column(name = "LAST_YEAR_SUP_SUM_REMARK", unique = false, nullable = true, length = 65535)
	private String lastYearSupSumRemark;
	
	/** 供应商1名称 **/
	@Column(name = "TOPSUP1_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup1CusName;
	
	/** 供应商1合作年限 **/
	@Column(name = "TOPSUP1_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup1CoopTerm;
	
	/** 供应商1上年供应量 **/
	@Column(name = "TOPSUP1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup1Income;
	
	/** 供应商2名称 **/
	@Column(name = "TOPSUP2_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup2CusName;
	
	/** 供应商2合作年限 **/
	@Column(name = "TOPSUP2_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup2CoopTerm;
	
	/** 供应商2上年供应量 **/
	@Column(name = "TOPSUP2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup2Income;
	
	/** 供应商3名称 **/
	@Column(name = "TOPSUP3_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsup3CusName;
	
	/** 供应商3合作年限 **/
	@Column(name = "TOPSUP3_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsup3CoopTerm;
	
	/** 供应商3上年供应量 **/
	@Column(name = "TOPSUP3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsup3Income;
	
	/** 供应商备注 **/
	@Column(name = "SUPPLIER_DESC", unique = false, nullable = true, length = 65535)
	private String supplierDesc;
	
	/** 客户1名称 **/
	@Column(name = "TOPSELL1_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell1CusName;
	
	/** 客户1合作年限 **/
	@Column(name = "TOPSELL1_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell1CoopTerm;
	
	/** 客户1销售收入 **/
	@Column(name = "TOPSELL1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell1Income;
	
	/** 客户2名称 **/
	@Column(name = "TOPSELL2_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell2CusName;
	
	/** 客户2合作年限 **/
	@Column(name = "TOPSELL2_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell2CoopTerm;
	
	/** 客户2销售收入 **/
	@Column(name = "TOPSELL2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell2Income;
	
	/** 客户3名称 **/
	@Column(name = "TOPSELL3_CUS_NAME", unique = false, nullable = true, length = 40)
	private String topsell3CusName;
	
	/** 客户3合作年限 **/
	@Column(name = "TOPSELL3_COOP_TERM", unique = false, nullable = true, length = 40)
	private String topsell3CoopTerm;
	
	/** 客户3销售收入 **/
	@Column(name = "TOPSELL3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal topsell3Income;
	
	/** 销售客户备注 **/
	@Column(name = "SELLCUS_DESC", unique = false, nullable = true, length = 65535)
	private String sellcusDesc;
	
	/** 去年发放工资当期月份 **/
	@Column(name = "LAST_YEAR_PAY_WAGES_MONTH", unique = false, nullable = true, length = 40)
	private String lastYearPayWagesMonth;
	
	/** 去年发放工资当前总额 **/
	@Column(name = "LAST_YEAR_PAY_WAGES_CUR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPayWagesCur;
	
	/** 去年发放工资去年总额 **/
	@Column(name = "LAST_YEAR_PAY_WAGES_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearPayWagesTotal;
	
	/** 去年发放工资备注 **/
	@Column(name = "LAST_YEAR_PAY_WAGES_REMARK", unique = false, nullable = true, length = 65535)
	private String lastYearPayWagesRemark;
	
	/** 贷款本金逾期次数 **/
	@Column(name = "OVERDUE_LOAN_PRINCIPAL_TIMES", unique = false, nullable = true, length = 40)
	private String overdueLoanPrincipalTimes;
	
	/** 贷款本金逾期最长天数 **/
	@Column(name = "LOAN_PRINCIPAL_LONGEST_OVERDUE_DAYS", unique = false, nullable = true, length = 40)
	private String loanPrincipalLongestOverdueDays;
	
	/** 贷款欠息记录次数 **/
	@Column(name = "RECORD_TIMES_OF_LOAN_INTEREST", unique = false, nullable = true, length = 40)
	private String recordTimesOfLoanInterest;
	
	/** 信用卡累积逾期次数 **/
	@Column(name = "CREDIT_CARD_ACCUMULATED_OVERDUE_TIMES", unique = false, nullable = true, length = 40)
	private String creditCardAccumulatedOverdueTimes;
	
	/** 最高累积逾期次数 **/
	@Column(name = "MAXIMUM_CUMULATIVE_OVERDUE_TIMES", unique = false, nullable = true, length = 40)
	private String maximumCumulativeOverdueTimes;
	
	/** 企业及实际控制人征信信用情况说明 **/
	@Column(name = "CIOEAAC", unique = false, nullable = true, length = 65535)
	private String cioeaac;
	
	/** 对外保证金额 **/
	@Column(name = "EXT_GUARANTEE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extGuaranteeAmt;
	
	/** 对外保证金额说明 **/
	@Column(name = "EXT_GUARANTEE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extGuaranteeAmtDesc;
	
	/** 对外抵押金额 **/
	@Column(name = "EXT_MORTGAGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extMortgageAmt;
	
	/** 对外抵押金额说明 **/
	@Column(name = "EXT_MORTGAGE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extMortgageAmtDesc;
	
	/** 对外质押金额 **/
	@Column(name = "EXT_PLEDGE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal extPledgeAmt;
	
	/** 对外质押金额说明 **/
	@Column(name = "EXT_PLEDGE_AMT_DESC", unique = false, nullable = true, length = 65535)
	private String extPledgeAmtDesc;
	
	/** 企业是否按时交纳各项税款，有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_DDRGRJYD_1", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd1;
	
	/** 企业有无违规排污，是否切实做好环保治理工作，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_DDRGRJYD_2", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd2;
	
	/** 企业实际控制人有无吸毒、赌博等不良嗜好，有无经常往返澳门等地，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_DDRGRJYD_3", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd3;
	
	/** 企业实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_DDRGRJYD_4", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd4;
	
	/** 企业员工人数是否稳定，有无出现明显裁员情况，员工待遇是否合理 **/
	@Column(name = "FOCUS_DDRGRJYD_5", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd5;
	
	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_DDRGRJYD_6", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd6;
	
	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_DDRGRJYD_7", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd7;
	
	/** 其它不利情况请简述 **/
	@Column(name = "FOCUS_DDRGRJYD_8", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd8;
	
	/** 反担保实物资产状态查询结果、变现能力评价 **/
	@Column(name = "FOCUS_DDRGRJYD_9", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd9;
	
	/** 反担保人评价（如有） **/
	@Column(name = "FOCUS_DDRGRJYD_10", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd10;
	
	/** 担保融个人经营贷其他 **/
	@Column(name = "FOCUS_DDRGRJYD_11", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd11;
	
	/** 担保融个人经营贷调查结论 **/
	@Column(name = "FOCUS_DDRGRJYD_12", unique = false, nullable = true, length = 65535)
	private String focusDdrgrjyd12;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarCorpName
	 */
	public void setGuarCorpName(String guarCorpName) {
		this.guarCorpName = guarCorpName;
	}
	
    /**
     * @return guarCorpName
     */
	public String getGuarCorpName() {
		return this.guarCorpName;
	}
	
	/**
	 * @param guarCorpInnerGrade
	 */
	public void setGuarCorpInnerGrade(String guarCorpInnerGrade) {
		this.guarCorpInnerGrade = guarCorpInnerGrade;
	}
	
    /**
     * @return guarCorpInnerGrade
     */
	public String getGuarCorpInnerGrade() {
		return this.guarCorpInnerGrade;
	}
	
	/**
	 * @param guarCorpOuterGrade
	 */
	public void setGuarCorpOuterGrade(String guarCorpOuterGrade) {
		this.guarCorpOuterGrade = guarCorpOuterGrade;
	}
	
    /**
     * @return guarCorpOuterGrade
     */
	public String getGuarCorpOuterGrade() {
		return this.guarCorpOuterGrade;
	}
	
	/**
	 * @param busiPremiseTypeRemark
	 */
	public void setBusiPremiseTypeRemark(String busiPremiseTypeRemark) {
		this.busiPremiseTypeRemark = busiPremiseTypeRemark;
	}
	
    /**
     * @return busiPremiseTypeRemark
     */
	public String getBusiPremiseTypeRemark() {
		return this.busiPremiseTypeRemark;
	}
	
	/**
	 * @param busiPremiseType
	 */
	public void setBusiPremiseType(String busiPremiseType) {
		this.busiPremiseType = busiPremiseType;
	}
	
    /**
     * @return busiPremiseType
     */
	public String getBusiPremiseType() {
		return this.busiPremiseType;
	}
	
	/**
	 * @param busiPremiseChgTimes
	 */
	public void setBusiPremiseChgTimes(String busiPremiseChgTimes) {
		this.busiPremiseChgTimes = busiPremiseChgTimes;
	}
	
    /**
     * @return busiPremiseChgTimes
     */
	public String getBusiPremiseChgTimes() {
		return this.busiPremiseChgTimes;
	}
	
	/**
	 * @param busiPremiseChgTimesRemark
	 */
	public void setBusiPremiseChgTimesRemark(String busiPremiseChgTimesRemark) {
		this.busiPremiseChgTimesRemark = busiPremiseChgTimesRemark;
	}
	
    /**
     * @return busiPremiseChgTimesRemark
     */
	public String getBusiPremiseChgTimesRemark() {
		return this.busiPremiseChgTimesRemark;
	}
	
	/**
	 * @param lastYearSupSum
	 */
	public void setLastYearSupSum(String lastYearSupSum) {
		this.lastYearSupSum = lastYearSupSum;
	}
	
    /**
     * @return lastYearSupSum
     */
	public String getLastYearSupSum() {
		return this.lastYearSupSum;
	}
	
	/**
	 * @param lastYearSupSumRemark
	 */
	public void setLastYearSupSumRemark(String lastYearSupSumRemark) {
		this.lastYearSupSumRemark = lastYearSupSumRemark;
	}
	
    /**
     * @return lastYearSupSumRemark
     */
	public String getLastYearSupSumRemark() {
		return this.lastYearSupSumRemark;
	}
	
	/**
	 * @param topsup1CusName
	 */
	public void setTopsup1CusName(String topsup1CusName) {
		this.topsup1CusName = topsup1CusName;
	}
	
    /**
     * @return topsup1CusName
     */
	public String getTopsup1CusName() {
		return this.topsup1CusName;
	}
	
	/**
	 * @param topsup1CoopTerm
	 */
	public void setTopsup1CoopTerm(String topsup1CoopTerm) {
		this.topsup1CoopTerm = topsup1CoopTerm;
	}
	
    /**
     * @return topsup1CoopTerm
     */
	public String getTopsup1CoopTerm() {
		return this.topsup1CoopTerm;
	}
	
	/**
	 * @param topsup1Income
	 */
	public void setTopsup1Income(java.math.BigDecimal topsup1Income) {
		this.topsup1Income = topsup1Income;
	}
	
    /**
     * @return topsup1Income
     */
	public java.math.BigDecimal getTopsup1Income() {
		return this.topsup1Income;
	}
	
	/**
	 * @param topsup2CusName
	 */
	public void setTopsup2CusName(String topsup2CusName) {
		this.topsup2CusName = topsup2CusName;
	}
	
    /**
     * @return topsup2CusName
     */
	public String getTopsup2CusName() {
		return this.topsup2CusName;
	}
	
	/**
	 * @param topsup2CoopTerm
	 */
	public void setTopsup2CoopTerm(String topsup2CoopTerm) {
		this.topsup2CoopTerm = topsup2CoopTerm;
	}
	
    /**
     * @return topsup2CoopTerm
     */
	public String getTopsup2CoopTerm() {
		return this.topsup2CoopTerm;
	}
	
	/**
	 * @param topsup2Income
	 */
	public void setTopsup2Income(java.math.BigDecimal topsup2Income) {
		this.topsup2Income = topsup2Income;
	}
	
    /**
     * @return topsup2Income
     */
	public java.math.BigDecimal getTopsup2Income() {
		return this.topsup2Income;
	}
	
	/**
	 * @param topsup3CusName
	 */
	public void setTopsup3CusName(String topsup3CusName) {
		this.topsup3CusName = topsup3CusName;
	}
	
    /**
     * @return topsup3CusName
     */
	public String getTopsup3CusName() {
		return this.topsup3CusName;
	}
	
	/**
	 * @param topsup3CoopTerm
	 */
	public void setTopsup3CoopTerm(String topsup3CoopTerm) {
		this.topsup3CoopTerm = topsup3CoopTerm;
	}
	
    /**
     * @return topsup3CoopTerm
     */
	public String getTopsup3CoopTerm() {
		return this.topsup3CoopTerm;
	}
	
	/**
	 * @param topsup3Income
	 */
	public void setTopsup3Income(java.math.BigDecimal topsup3Income) {
		this.topsup3Income = topsup3Income;
	}
	
    /**
     * @return topsup3Income
     */
	public java.math.BigDecimal getTopsup3Income() {
		return this.topsup3Income;
	}
	
	/**
	 * @param supplierDesc
	 */
	public void setSupplierDesc(String supplierDesc) {
		this.supplierDesc = supplierDesc;
	}
	
    /**
     * @return supplierDesc
     */
	public String getSupplierDesc() {
		return this.supplierDesc;
	}
	
	/**
	 * @param topsell1CusName
	 */
	public void setTopsell1CusName(String topsell1CusName) {
		this.topsell1CusName = topsell1CusName;
	}
	
    /**
     * @return topsell1CusName
     */
	public String getTopsell1CusName() {
		return this.topsell1CusName;
	}
	
	/**
	 * @param topsell1CoopTerm
	 */
	public void setTopsell1CoopTerm(String topsell1CoopTerm) {
		this.topsell1CoopTerm = topsell1CoopTerm;
	}
	
    /**
     * @return topsell1CoopTerm
     */
	public String getTopsell1CoopTerm() {
		return this.topsell1CoopTerm;
	}
	
	/**
	 * @param topsell1Income
	 */
	public void setTopsell1Income(java.math.BigDecimal topsell1Income) {
		this.topsell1Income = topsell1Income;
	}
	
    /**
     * @return topsell1Income
     */
	public java.math.BigDecimal getTopsell1Income() {
		return this.topsell1Income;
	}
	
	/**
	 * @param topsell2CusName
	 */
	public void setTopsell2CusName(String topsell2CusName) {
		this.topsell2CusName = topsell2CusName;
	}
	
    /**
     * @return topsell2CusName
     */
	public String getTopsell2CusName() {
		return this.topsell2CusName;
	}
	
	/**
	 * @param topsell2CoopTerm
	 */
	public void setTopsell2CoopTerm(String topsell2CoopTerm) {
		this.topsell2CoopTerm = topsell2CoopTerm;
	}
	
    /**
     * @return topsell2CoopTerm
     */
	public String getTopsell2CoopTerm() {
		return this.topsell2CoopTerm;
	}
	
	/**
	 * @param topsell2Income
	 */
	public void setTopsell2Income(java.math.BigDecimal topsell2Income) {
		this.topsell2Income = topsell2Income;
	}
	
    /**
     * @return topsell2Income
     */
	public java.math.BigDecimal getTopsell2Income() {
		return this.topsell2Income;
	}
	
	/**
	 * @param topsell3CusName
	 */
	public void setTopsell3CusName(String topsell3CusName) {
		this.topsell3CusName = topsell3CusName;
	}
	
    /**
     * @return topsell3CusName
     */
	public String getTopsell3CusName() {
		return this.topsell3CusName;
	}
	
	/**
	 * @param topsell3CoopTerm
	 */
	public void setTopsell3CoopTerm(String topsell3CoopTerm) {
		this.topsell3CoopTerm = topsell3CoopTerm;
	}
	
    /**
     * @return topsell3CoopTerm
     */
	public String getTopsell3CoopTerm() {
		return this.topsell3CoopTerm;
	}
	
	/**
	 * @param topsell3Income
	 */
	public void setTopsell3Income(java.math.BigDecimal topsell3Income) {
		this.topsell3Income = topsell3Income;
	}
	
    /**
     * @return topsell3Income
     */
	public java.math.BigDecimal getTopsell3Income() {
		return this.topsell3Income;
	}
	
	/**
	 * @param sellcusDesc
	 */
	public void setSellcusDesc(String sellcusDesc) {
		this.sellcusDesc = sellcusDesc;
	}
	
    /**
     * @return sellcusDesc
     */
	public String getSellcusDesc() {
		return this.sellcusDesc;
	}
	
	/**
	 * @param lastYearPayWagesMonth
	 */
	public void setLastYearPayWagesMonth(String lastYearPayWagesMonth) {
		this.lastYearPayWagesMonth = lastYearPayWagesMonth;
	}
	
    /**
     * @return lastYearPayWagesMonth
     */
	public String getLastYearPayWagesMonth() {
		return this.lastYearPayWagesMonth;
	}
	
	/**
	 * @param lastYearPayWagesCur
	 */
	public void setLastYearPayWagesCur(java.math.BigDecimal lastYearPayWagesCur) {
		this.lastYearPayWagesCur = lastYearPayWagesCur;
	}
	
    /**
     * @return lastYearPayWagesCur
     */
	public java.math.BigDecimal getLastYearPayWagesCur() {
		return this.lastYearPayWagesCur;
	}
	
	/**
	 * @param lastYearPayWagesTotal
	 */
	public void setLastYearPayWagesTotal(java.math.BigDecimal lastYearPayWagesTotal) {
		this.lastYearPayWagesTotal = lastYearPayWagesTotal;
	}
	
    /**
     * @return lastYearPayWagesTotal
     */
	public java.math.BigDecimal getLastYearPayWagesTotal() {
		return this.lastYearPayWagesTotal;
	}
	
	/**
	 * @param lastYearPayWagesRemark
	 */
	public void setLastYearPayWagesRemark(String lastYearPayWagesRemark) {
		this.lastYearPayWagesRemark = lastYearPayWagesRemark;
	}
	
    /**
     * @return lastYearPayWagesRemark
     */
	public String getLastYearPayWagesRemark() {
		return this.lastYearPayWagesRemark;
	}
	
	/**
	 * @param overdueLoanPrincipalTimes
	 */
	public void setOverdueLoanPrincipalTimes(String overdueLoanPrincipalTimes) {
		this.overdueLoanPrincipalTimes = overdueLoanPrincipalTimes;
	}
	
    /**
     * @return overdueLoanPrincipalTimes
     */
	public String getOverdueLoanPrincipalTimes() {
		return this.overdueLoanPrincipalTimes;
	}
	
	/**
	 * @param loanPrincipalLongestOverdueDays
	 */
	public void setLoanPrincipalLongestOverdueDays(String loanPrincipalLongestOverdueDays) {
		this.loanPrincipalLongestOverdueDays = loanPrincipalLongestOverdueDays;
	}
	
    /**
     * @return loanPrincipalLongestOverdueDays
     */
	public String getLoanPrincipalLongestOverdueDays() {
		return this.loanPrincipalLongestOverdueDays;
	}
	
	/**
	 * @param recordTimesOfLoanInterest
	 */
	public void setRecordTimesOfLoanInterest(String recordTimesOfLoanInterest) {
		this.recordTimesOfLoanInterest = recordTimesOfLoanInterest;
	}
	
    /**
     * @return recordTimesOfLoanInterest
     */
	public String getRecordTimesOfLoanInterest() {
		return this.recordTimesOfLoanInterest;
	}
	
	/**
	 * @param creditCardAccumulatedOverdueTimes
	 */
	public void setCreditCardAccumulatedOverdueTimes(String creditCardAccumulatedOverdueTimes) {
		this.creditCardAccumulatedOverdueTimes = creditCardAccumulatedOverdueTimes;
	}
	
    /**
     * @return creditCardAccumulatedOverdueTimes
     */
	public String getCreditCardAccumulatedOverdueTimes() {
		return this.creditCardAccumulatedOverdueTimes;
	}
	
	/**
	 * @param maximumCumulativeOverdueTimes
	 */
	public void setMaximumCumulativeOverdueTimes(String maximumCumulativeOverdueTimes) {
		this.maximumCumulativeOverdueTimes = maximumCumulativeOverdueTimes;
	}
	
    /**
     * @return maximumCumulativeOverdueTimes
     */
	public String getMaximumCumulativeOverdueTimes() {
		return this.maximumCumulativeOverdueTimes;
	}
	
	/**
	 * @param cioeaac
	 */
	public void setCioeaac(String cioeaac) {
		this.cioeaac = cioeaac;
	}
	
    /**
     * @return cioeaac
     */
	public String getCioeaac() {
		return this.cioeaac;
	}
	
	/**
	 * @param extGuaranteeAmt
	 */
	public void setExtGuaranteeAmt(java.math.BigDecimal extGuaranteeAmt) {
		this.extGuaranteeAmt = extGuaranteeAmt;
	}
	
    /**
     * @return extGuaranteeAmt
     */
	public java.math.BigDecimal getExtGuaranteeAmt() {
		return this.extGuaranteeAmt;
	}
	
	/**
	 * @param extGuaranteeAmtDesc
	 */
	public void setExtGuaranteeAmtDesc(String extGuaranteeAmtDesc) {
		this.extGuaranteeAmtDesc = extGuaranteeAmtDesc;
	}
	
    /**
     * @return extGuaranteeAmtDesc
     */
	public String getExtGuaranteeAmtDesc() {
		return this.extGuaranteeAmtDesc;
	}
	
	/**
	 * @param extMortgageAmt
	 */
	public void setExtMortgageAmt(java.math.BigDecimal extMortgageAmt) {
		this.extMortgageAmt = extMortgageAmt;
	}
	
    /**
     * @return extMortgageAmt
     */
	public java.math.BigDecimal getExtMortgageAmt() {
		return this.extMortgageAmt;
	}
	
	/**
	 * @param extMortgageAmtDesc
	 */
	public void setExtMortgageAmtDesc(String extMortgageAmtDesc) {
		this.extMortgageAmtDesc = extMortgageAmtDesc;
	}
	
    /**
     * @return extMortgageAmtDesc
     */
	public String getExtMortgageAmtDesc() {
		return this.extMortgageAmtDesc;
	}
	
	/**
	 * @param extPledgeAmt
	 */
	public void setExtPledgeAmt(java.math.BigDecimal extPledgeAmt) {
		this.extPledgeAmt = extPledgeAmt;
	}
	
    /**
     * @return extPledgeAmt
     */
	public java.math.BigDecimal getExtPledgeAmt() {
		return this.extPledgeAmt;
	}
	
	/**
	 * @param extPledgeAmtDesc
	 */
	public void setExtPledgeAmtDesc(String extPledgeAmtDesc) {
		this.extPledgeAmtDesc = extPledgeAmtDesc;
	}
	
    /**
     * @return extPledgeAmtDesc
     */
	public String getExtPledgeAmtDesc() {
		return this.extPledgeAmtDesc;
	}
	
	/**
	 * @param focusDdrgrjyd1
	 */
	public void setFocusDdrgrjyd1(String focusDdrgrjyd1) {
		this.focusDdrgrjyd1 = focusDdrgrjyd1;
	}
	
    /**
     * @return focusDdrgrjyd1
     */
	public String getFocusDdrgrjyd1() {
		return this.focusDdrgrjyd1;
	}
	
	/**
	 * @param focusDdrgrjyd2
	 */
	public void setFocusDdrgrjyd2(String focusDdrgrjyd2) {
		this.focusDdrgrjyd2 = focusDdrgrjyd2;
	}
	
    /**
     * @return focusDdrgrjyd2
     */
	public String getFocusDdrgrjyd2() {
		return this.focusDdrgrjyd2;
	}
	
	/**
	 * @param focusDdrgrjyd3
	 */
	public void setFocusDdrgrjyd3(String focusDdrgrjyd3) {
		this.focusDdrgrjyd3 = focusDdrgrjyd3;
	}
	
    /**
     * @return focusDdrgrjyd3
     */
	public String getFocusDdrgrjyd3() {
		return this.focusDdrgrjyd3;
	}
	
	/**
	 * @param focusDdrgrjyd4
	 */
	public void setFocusDdrgrjyd4(String focusDdrgrjyd4) {
		this.focusDdrgrjyd4 = focusDdrgrjyd4;
	}
	
    /**
     * @return focusDdrgrjyd4
     */
	public String getFocusDdrgrjyd4() {
		return this.focusDdrgrjyd4;
	}
	
	/**
	 * @param focusDdrgrjyd5
	 */
	public void setFocusDdrgrjyd5(String focusDdrgrjyd5) {
		this.focusDdrgrjyd5 = focusDdrgrjyd5;
	}
	
    /**
     * @return focusDdrgrjyd5
     */
	public String getFocusDdrgrjyd5() {
		return this.focusDdrgrjyd5;
	}
	
	/**
	 * @param focusDdrgrjyd6
	 */
	public void setFocusDdrgrjyd6(String focusDdrgrjyd6) {
		this.focusDdrgrjyd6 = focusDdrgrjyd6;
	}
	
    /**
     * @return focusDdrgrjyd6
     */
	public String getFocusDdrgrjyd6() {
		return this.focusDdrgrjyd6;
	}
	
	/**
	 * @param focusDdrgrjyd7
	 */
	public void setFocusDdrgrjyd7(String focusDdrgrjyd7) {
		this.focusDdrgrjyd7 = focusDdrgrjyd7;
	}
	
    /**
     * @return focusDdrgrjyd7
     */
	public String getFocusDdrgrjyd7() {
		return this.focusDdrgrjyd7;
	}
	
	/**
	 * @param focusDdrgrjyd8
	 */
	public void setFocusDdrgrjyd8(String focusDdrgrjyd8) {
		this.focusDdrgrjyd8 = focusDdrgrjyd8;
	}
	
    /**
     * @return focusDdrgrjyd8
     */
	public String getFocusDdrgrjyd8() {
		return this.focusDdrgrjyd8;
	}
	
	/**
	 * @param focusDdrgrjyd9
	 */
	public void setFocusDdrgrjyd9(String focusDdrgrjyd9) {
		this.focusDdrgrjyd9 = focusDdrgrjyd9;
	}
	
    /**
     * @return focusDdrgrjyd9
     */
	public String getFocusDdrgrjyd9() {
		return this.focusDdrgrjyd9;
	}
	
	/**
	 * @param focusDdrgrjyd10
	 */
	public void setFocusDdrgrjyd10(String focusDdrgrjyd10) {
		this.focusDdrgrjyd10 = focusDdrgrjyd10;
	}
	
    /**
     * @return focusDdrgrjyd10
     */
	public String getFocusDdrgrjyd10() {
		return this.focusDdrgrjyd10;
	}
	
	/**
	 * @param focusDdrgrjyd11
	 */
	public void setFocusDdrgrjyd11(String focusDdrgrjyd11) {
		this.focusDdrgrjyd11 = focusDdrgrjyd11;
	}
	
    /**
     * @return focusDdrgrjyd11
     */
	public String getFocusDdrgrjyd11() {
		return this.focusDdrgrjyd11;
	}
	
	/**
	 * @param focusDdrgrjyd12
	 */
	public void setFocusDdrgrjyd12(String focusDdrgrjyd12) {
		this.focusDdrgrjyd12 = focusDdrgrjyd12;
	}
	
    /**
     * @return focusDdrgrjyd12
     */
	public String getFocusDdrgrjyd12() {
		return this.focusDdrgrjyd12;
	}


}