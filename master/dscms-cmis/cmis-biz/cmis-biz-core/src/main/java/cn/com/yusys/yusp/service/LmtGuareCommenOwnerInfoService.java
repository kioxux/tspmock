package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtGuareCommenOwnerInfo;
import cn.com.yusys.yusp.repository.mapper.LmtGuareCommenOwnerInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGuareCommenOwnerInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-25 17:22:05
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGuareCommenOwnerInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtGuareCommenOwnerInfoService.class);
    @Autowired
    private LmtGuareCommenOwnerInfoMapper lmtGuareCommenOwnerInfoMapper;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtGuareCommenOwnerInfo selectByPrimaryKey(String pkId) {
        return lmtGuareCommenOwnerInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtGuareCommenOwnerInfo> selectAll(QueryModel model) {
        List<LmtGuareCommenOwnerInfo> records = (List<LmtGuareCommenOwnerInfo>) lmtGuareCommenOwnerInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtGuareCommenOwnerInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGuareCommenOwnerInfo> list = lmtGuareCommenOwnerInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtGuareCommenOwnerInfo record) {
        return lmtGuareCommenOwnerInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtGuareCommenOwnerInfo record) {
        return lmtGuareCommenOwnerInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtGuareCommenOwnerInfo record) {
        return lmtGuareCommenOwnerInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtGuareCommenOwnerInfo record) {
        return lmtGuareCommenOwnerInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return lmtGuareCommenOwnerInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtGuareCommenOwnerInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: saveCommenOwnerInfo
     * @方法描述: 新增抵质押物的共有人信息
     * @参数与返回说明:
     * @算法描述: 无
     * @author wzy
     * @修改记录: 修改时间: 2021年6月7日20:42:51    修改人员: hubp    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto saveCommenOwnerInfo(LmtGuareCommenOwnerInfo lmtGuareCommenOwnerInfo) {
        logger.info("新增抵质押物共有人信息开始", lmtGuareCommenOwnerInfo.toString());
        int result;
        try {
            String pldimnNo = lmtGuareCommenOwnerInfo.getPldimnNo();
            String cusId = lmtGuareCommenOwnerInfo.getCommenOwnerCusId();
            if (StringUtils.isBlank(pldimnNo) || StringUtils.isBlank(cusId)) {
                return new ResultDto(null).code(9999).message("请核查数据!有关键数据为空");
            }
            LmtGuareCommenOwnerInfo owner = lmtGuareCommenOwnerInfoMapper.selectByPldimnNoAndCusId(pldimnNo, cusId);
            if (null != owner) { // 如果通过抵押物编号，和客户ID能查到数据
                logger.info("共有人信息不能在同一个抵押物下重复添加", lmtGuareCommenOwnerInfo.toString());
                return new ResultDto(null).code(9999).message("共有人信息不能在同一个抵押物下重复添加");
            }
            String pkId = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SERNO, new HashMap<>());
            lmtGuareCommenOwnerInfo.setPkId(pkId);
            result = this.insertSelective(lmtGuareCommenOwnerInfo);
        } catch (Exception e) {
            logger.error("新增抵质押物共有人信息失败", e.getMessage());
            return new ResultDto(null).code(9999).message("共有人新增失败！请核查数据");
        } finally {
            logger.info("新增抵质押物共有人信息结束");
        }
        return new ResultDto(result).code(0).message("保存成功");
    }

}
