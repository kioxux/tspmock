/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IntbankOrgAdmitAppr;
import cn.com.yusys.yusp.service.IntbankOrgAdmitApprService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: yuanz
 * @创建时间: 2021-05-10 21:29:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/intbankorgadmitappr")
public class IntbankOrgAdmitApprResource {
    @Autowired
    private IntbankOrgAdmitApprService intbankOrgAdmitApprService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IntbankOrgAdmitAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<IntbankOrgAdmitAppr> list = intbankOrgAdmitApprService.selectAll(queryModel);
        return new ResultDto<List<IntbankOrgAdmitAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IntbankOrgAdmitAppr>> index(QueryModel queryModel) {
        List<IntbankOrgAdmitAppr> list = intbankOrgAdmitApprService.selectByModel(queryModel);
        return new ResultDto<List<IntbankOrgAdmitAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IntbankOrgAdmitAppr> show(@PathVariable("pkId") String pkId) {
        IntbankOrgAdmitAppr intbankOrgAdmitAppr = intbankOrgAdmitApprService.selectByPrimaryKey(pkId);
        return new ResultDto<IntbankOrgAdmitAppr>(intbankOrgAdmitAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IntbankOrgAdmitAppr> create(@RequestBody IntbankOrgAdmitAppr intbankOrgAdmitAppr) throws URISyntaxException {
        intbankOrgAdmitApprService.insert(intbankOrgAdmitAppr);
        return new ResultDto<IntbankOrgAdmitAppr>(intbankOrgAdmitAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IntbankOrgAdmitAppr intbankOrgAdmitAppr) throws URISyntaxException {
        int result = intbankOrgAdmitApprService.update(intbankOrgAdmitAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = intbankOrgAdmitApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = intbankOrgAdmitApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    @PostMapping("/selectByModel")
    protected  ResultDto<List<IntbankOrgAdmitAppr>> selectByModel(@RequestBody QueryModel model){
        return  new ResultDto<List<IntbankOrgAdmitAppr>>(intbankOrgAdmitApprService.selectByModel(model));
    }
    @PostMapping("/updateAppr")
    protected ResultDto<Integer> updateAppr(@RequestBody IntbankOrgAdmitAppr intbankOrgAdmitAppr){
        return new ResultDto<Integer>(intbankOrgAdmitApprService.updateSelective(intbankOrgAdmitAppr));
    }

    /**
     * @函数名称:updateRestByPkId
     * @函数描述:根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkId")
    protected int updateRestByPkId(@RequestBody QueryModel model){
        return intbankOrgAdmitApprService.updateRestByPkId(model);
    }

    /**
     * @函数名称:updateRestByPkIdZH
     * @函数描述:根据Serno更新信贷管理部风险派驻岗审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkIdZH")
    protected int updateRestByPkIdZH(@RequestBody QueryModel model){
        return intbankOrgAdmitApprService.updateRestByPkIdZH(model);
    }

    /**
     * 获取最新审批详情
     * @return
     */
    @PostMapping("/selectLastApprBySerno")
    protected ResultDto<List> selectLastApprBySerno(@RequestBody Map condition){
        Map<String,Object> result = intbankOrgAdmitApprService.selectLastApprBySerno(condition);
        List<Map<String,Object>> maps = new ArrayList<>();
        maps.add(result);
        return new ResultDto<List>(maps);
    }

    /**
     * 保存
     * @return
     */
    @PostMapping("/saveTerm")
    protected ResultDto<Integer> saveTerm(@RequestBody Map condition){
        int result = intbankOrgAdmitApprService.saveTerm(condition);
        return new ResultDto<>(result);
    }

}
