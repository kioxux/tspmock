/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.SfResultInfo;
import cn.com.yusys.yusp.service.SfResultInfoService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SfResultInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:30:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "业务申请信息")
@RequestMapping("/api/sfresultinfo")
public class SfResultInfoResource {
    @Autowired
    private SfResultInfoService sfResultInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<SfResultInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<SfResultInfo> list = sfResultInfoService.selectAll(queryModel);
        return new ResultDto<List<SfResultInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<SfResultInfo>> index(QueryModel queryModel) {
        List<SfResultInfo> list = sfResultInfoService.selectByModel(queryModel);
        return new ResultDto<List<SfResultInfo>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过流水号查询详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过流水号查询详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        SfResultInfo sfResultInfo = sfResultInfoService.selectBySerno((String) params.get("serno"));
        if (sfResultInfo != null) {
            resultDto.setCode(200);
            resultDto.setData(sfResultInfo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:sfResultInfolist
     * @函数描述: 获取未审批尽调结果
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷尽调结果申请列表")
    @PostMapping("/sfResultInfolist")
    protected ResultDto<List<SfResultInfo>> sfResultInfolist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SfResultInfo> list = sfResultInfoService.sfResultInfolist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SfResultInfo>>(list);
    }

    /**
     * @函数名称:sfResultInfoHislist
     * @函数描述: 获取已审批尽调结果
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷尽调结果历史列表")
    @PostMapping("/sfResultInfoHislist")
    protected ResultDto<List<SfResultInfo>> sfResultInfoHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<SfResultInfo> list = sfResultInfoService.sfResultInfoHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<SfResultInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<SfResultInfo> show(@PathVariable("serno") String serno) {
        SfResultInfo sfResultInfo = sfResultInfoService.selectByPrimaryKey(serno);
        return new ResultDto<SfResultInfo>(sfResultInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<SfResultInfo> create(@RequestBody SfResultInfo sfResultInfo) throws URISyntaxException {
        sfResultInfoService.insert(sfResultInfo);
        return new ResultDto<SfResultInfo>(sfResultInfo);
    }

    /**
     * @函数名称:insertSfResultInfo
     * @函数描述:房抵e点贷尽调结果录入
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("")
    @PostMapping("/insertSfResultInfo")
    protected ResultDto<Map> insertSfResultInfo(@RequestBody SfResultInfo sfResultInfo) throws URISyntaxException {
        Map rtnData = sfResultInfoService.insertSfResultInfo(sfResultInfo);
        return new ResultDto<>(rtnData);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody SfResultInfo sfResultInfo) throws URISyntaxException {
        int result = sfResultInfoService.update(sfResultInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = sfResultInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:物理删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷尽调结果逻辑删除")
    @PostMapping("/sfResultInfodelete")
    public ResultDto<Map> sfResultInfodelete(@RequestBody SfResultInfo sfResultInfo) {
        ResultDto<Integer> resultDto = new ResultDto<Integer>();
        Map rtnData = sfResultInfoService.sfResultInfodelete(sfResultInfo);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = sfResultInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSfResultInfo
     * @函数描述:房抵e点贷尽调结果录入校验并保存/提交
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷尽调结果录入校验并保存")
    @PostMapping("/submitSfResultInfo")
    protected ResultDto<Map> updateSfResultInfo(@RequestBody SfResultInfo sfResultInfo) throws URISyntaxException {
        Map rtnData = sfResultInfoService.submitSfResultInfo(sfResultInfo);
        return new ResultDto<>(rtnData);
    }
}
