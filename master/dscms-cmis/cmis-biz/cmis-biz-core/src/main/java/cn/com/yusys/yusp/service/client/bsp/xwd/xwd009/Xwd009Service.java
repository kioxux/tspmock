package cn.com.yusys.yusp.service.client.bsp.xwd.xwd009;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2XwdClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
public class Xwd009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xwd009Service.class);

    // 1）注入：BSP封装调用新微贷系统的接口
    @Autowired
    private Dscms2XwdClientService dscms2XwdClientService;

    @Transactional
    public Xwd009RespDto xwd009(Xwd009ReqDto xwd009ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value, JSON.toJSONString(xwd009ReqDto));
        ResultDto<Xwd009RespDto> xwd009ResultDto = dscms2XwdClientService.xwd009(xwd009ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value, JSON.toJSONString(xwd009ResultDto));
        String xwd009Code = Optional.ofNullable(xwd009ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xwd009Meesage = Optional.ofNullable(xwd009ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Xwd009RespDto xwd009RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xwd009ResultDto.getCode())) {
            //  获取相关的值并解析
            xwd009RespDto = xwd009ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(xwd009Code, xwd009Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value);
        return xwd009RespDto;
    }

}
