package cn.com.yusys.yusp.dto;

import java.io.Serializable;
/**
 * @项目名称: cmis-cfg-core模块
 * @类名称: CfgFlexQryParamIndexDto
 * @类描述: CfgFlexQryParamIndexDto 灵活查询查询及对应表信息和字段控件
 * @功能描述:
 * @创建人: 马顺
 * @创建时间: 2020-02-04 20:43:10
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CfgFlexQryParamIndexDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String pkId;
    private String paramKey;
    private String paramName;
    private String paramValue;
    private String paramType;
    private String showOrder;
    private String indexCode;
    private String colNameEn;
    private String itemtype;
    private String itemdefine;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getShowOrder() {
        return showOrder;
    }

    public void setShowOrder(String showOrder) {
        this.showOrder = showOrder;
    }

    public String getIndexCode() {
        return indexCode;
    }

    public void setIndexCode(String indexCode) {
        this.indexCode = indexCode;
    }

    public String getColNameEn() {
        return colNameEn;
    }

    public void setColNameEn(String colNameEn) {
        this.colNameEn = colNameEn;
    }

    public String getItemtype() {
        return itemtype;
    }

    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }

    public String getItemdefine() {
        return itemdefine;
    }

    public void setItemdefine(String itemdefine) {
        this.itemdefine = itemdefine;
    }
}
