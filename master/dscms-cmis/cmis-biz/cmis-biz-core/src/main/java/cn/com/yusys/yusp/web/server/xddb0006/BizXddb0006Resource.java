package cn.com.yusys.yusp.web.server.xddb0006;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0006.req.Xddb0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0006.resp.*;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0006.Xddb0006Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:信贷押品状态查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "xddb0006:信贷押品状态查询")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0006Resource.class);
    @Autowired
    private Xddb0006Service xddb0006Service;//业务逻辑处理类:信贷押品状态查询

    /**
     * 交易码：xddb0006
     * 交易描述：信贷押品状态查询
     *
     * @param xddb0006DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0006:信贷押品状态查询")
    @PostMapping("/xddb0006")
    protected @ResponseBody
    ResultDto<Xddb0006DataRespDto> xddb0006(@Validated @RequestBody Xddb0006DataReqDto xddb0006DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006DataReqDto));
        Xddb0006DataRespDto xddb0006DataRespDto = new Xddb0006DataRespDto();// 响应Dto:信贷押品状态查询
        ResultDto<Xddb0006DataRespDto> xddb0006DataResultDto = new ResultDto<>();
        // 从xddb0006DataReqDto获取业务值进行业务逻辑处理
        try {
            String distco = xddb0006DataReqDto.getDistco();//区县代码
            String certnu = xddb0006DataReqDto.getCertnu();//不动产权证书号
            String mocenu = xddb0006DataReqDto.getMocenu();//不动产登记证明号
            xddb0006DataRespDto = xddb0006Service.xddb0006(xddb0006DataReqDto);
            RegisterFacilityList registerFacilityList = new RegisterFacilityList();
            RegisterHouseList registerHouseList = new RegisterHouseList();
            RegisterLandList registerLandList = new RegisterLandList();
            RegisterMortgageList registerMortgageList = new RegisterMortgageList();
            RegisterOwnerList registerOwnerList = new RegisterOwnerList();
            RegisterSeizureList registerSeizureList = new RegisterSeizureList();
            xddb0006DataRespDto.setReocnu(StringUtils.EMPTY);// 登记簿概要信息-不动产权证号
            xddb0006DataRespDto.setReoreu(StringUtils.EMPTY);// 登记簿概要信息-不动产单元号
            xddb0006DataRespDto.setReoona(StringUtils.EMPTY);// 登记簿概要信息-权利人
            xddb0006DataRespDto.setReoodn(StringUtils.EMPTY);// 登记簿概要信息-证件号
            xddb0006DataRespDto.setReobda(StringUtils.EMPTY);// 登记簿概要信息-登簿日期
            xddb0006DataRespDto.setReooss(StringUtils.EMPTY);// 登记簿概要信息-权属状态
            xddb0006DataRespDto.setReorem(StringUtils.EMPTY);// 登记簿概要信息-附记

            registerFacilityList.setAffaar(StringUtils.EMPTY);// 登记簿附属设施-建筑面积
            registerFacilityList.setAfinar(StringUtils.EMPTY);// 登记簿附属设施-套内建筑面积
            registerFacilityList.setAfshar(StringUtils.EMPTY);// 登记簿附属设施-分摊建筑面积
            registerFacilityList.setAffuse(StringUtils.EMPTY);// 登记簿附属设施-用途
            registerFacilityList.setAffloc(StringUtils.EMPTY);// 登记簿附属设施-坐落
            registerFacilityList.setAflrsd(StringUtils.EMPTY);// 登记簿附属设施-土地使用权开始时间
            registerFacilityList.setAflred(StringUtils.EMPTY);// 登记簿附属设施-土地使用权结束时间

            registerHouseList.setRearea(StringUtils.EMPTY);// 登记簿概要信息-建筑面积
            registerHouseList.setReinar(StringUtils.EMPTY);// 登记簿概要信息-套内建筑面积
            registerHouseList.setReosha(StringUtils.EMPTY);// 登记簿概要信息-分摊建筑面积
            registerHouseList.setReouse(StringUtils.EMPTY);// 登记簿概要信息-房屋用途
            registerHouseList.setReonat(StringUtils.EMPTY);// 登记簿概要信息-房屋性质
            registerHouseList.setReotla(StringUtils.EMPTY);// 登记簿概要信息-总层数
            registerHouseList.setReosla(StringUtils.EMPTY);// 登记簿概要信息-所在层
            registerHouseList.setLocate(StringUtils.EMPTY);// 登记簿概要信息-坐落

            registerLandList.setLaarea(StringUtils.EMPTY);// 登记簿土地信息-土地面积
            registerLandList.setLanuse(StringUtils.EMPTY);// 登记簿土地信息-土地用途
            registerLandList.setLandna(StringUtils.EMPTY);// 登记簿土地信息-土地性质
            registerLandList.setLanrsd(StringUtils.EMPTY);// 登记簿土地信息-土地使用权开始
            registerLandList.setLanred(StringUtils.EMPTY);// 登记簿土地信息-土地使用权结束
            registerLandList.setLanloc(StringUtils.EMPTY);// 登记簿土地信息-坐落

            registerMortgageList.setMmcenu(StringUtils.EMPTY);// 登记簿抵押信息-不动产登记证明号
            registerMortgageList.setMormor(StringUtils.EMPTY);// 登记簿抵押信息-抵押权人
            registerMortgageList.setMormog(StringUtils.EMPTY);// 登记簿抵押信息-抵押人
            registerMortgageList.setMogusc(StringUtils.EMPTY);// 登记簿抵押信息-担保范围
            registerMortgageList.setMogord(StringUtils.EMPTY);// 登记簿抵押信息-顺位
            registerMortgageList.setMomome(StringUtils.EMPTY);// 登记簿抵押信息-抵押方式
            registerMortgageList.setModeam(new BigDecimal(0L));// 登记簿抵押信息-债权数额
            registerMortgageList.setMordst(StringUtils.EMPTY);// 登记簿抵押信息-债务履行开始时间
            registerMortgageList.setMordet(StringUtils.EMPTY);// 登记簿抵押信息-债务履行结束时间
            registerMortgageList.setMorbod(StringUtils.EMPTY);// 登记簿抵押信息-登簿日期


            registerOwnerList.setOwnnam(StringUtils.EMPTY);// 登记簿权利人信息-权利人姓名
            registerOwnerList.setOwdonu(StringUtils.EMPTY);// 登记簿权利人信息-权利人证件号码
            registerOwnerList.setOwdoty(StringUtils.EMPTY);// 登记簿权利人信息-权利人证件类型
            registerOwnerList.setOwcenu(StringUtils.EMPTY);// 登记簿权利人信息-不动产权证号
            registerOwnerList.setOoshmo(StringUtils.EMPTY);// 登记簿权利人信息-共有方式
            registerOwnerList.setOoshin(StringUtils.EMPTY);// 登记簿权利人信息-共有情况
            registerOwnerList.setOoshpr(StringUtils.EMPTY);// 登记簿权利人信息-权利比例
            registerOwnerList.setOhcemo(StringUtils.EMPTY);// 登记簿权利人信息-持证方式

            registerSeizureList.setSeioff(StringUtils.EMPTY);// 登记簿查封信息-查封机关
            registerSeizureList.setSeimod(StringUtils.EMPTY);// 登记簿查封信息-查封类型
            registerSeizureList.setSeinum(StringUtils.EMPTY);// 登记簿查封信息-查封文号
            registerSeizureList.setSeistd(StringUtils.EMPTY);// 登记簿查封信息-开始日期
            registerSeizureList.setSeiend(StringUtils.EMPTY);// 登记簿查封信息-结束日期
            registerSeizureList.setSeicad(StringUtils.EMPTY);// 登记簿查封信息-注销日期
            registerSeizureList.setSeirem(StringUtils.EMPTY);// 登记簿查封信息-附记

            xddb0006DataRespDto.setRegisterFacilityLists(Arrays.asList(registerFacilityList));
            xddb0006DataRespDto.setRegisterHouseLists(Arrays.asList(registerHouseList));
            xddb0006DataRespDto.setRegisterLandLists(Arrays.asList(registerLandList));
            xddb0006DataRespDto.setRegisterMortgageLists(Arrays.asList(registerMortgageList));
            xddb0006DataRespDto.setRegisterOwnerLists(Arrays.asList(registerOwnerList));
            xddb0006DataRespDto.setRegisterSeizureList(Arrays.asList(registerSeizureList));
            // 封装xddb0006DataResultDto中正确的返回码和返回信息
            String opFlag = xddb0006DataRespDto.getOpFlag();
            String opMessage = xddb0006DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("9999".equals(opFlag)) {
                xddb0006DataResultDto.setCode(opFlag);
                xddb0006DataResultDto.setMessage(opMessage);
            } else if ("0001".equals(opFlag) || "0002".equals(opFlag) || "5555".equals(opFlag)) {
                xddb0006DataResultDto.setCode(opFlag);
                xddb0006DataResultDto.setMessage(opMessage);
            } else {
                xddb0006DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb0006DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, e.getMessage());
            // 封装xddb0006DataResultDto中异常返回码和返回信息
            xddb0006DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0006DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0006DataRespDto到xddb0006DataResultDto中
        xddb0006DataResultDto.setData(xddb0006DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006DataResultDto));
        return xddb0006DataResultDto;
    }
}
