package cn.com.yusys.yusp.web.server.xdxw0078;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0078.req.Xdxw0078DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0078.resp.Xdxw0078DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0078.Xdxw0078Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:推送云评估信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0078:推送云评估信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0078Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0078Resource.class);

    @Autowired
    private Xdxw0078Service xdxw0078Service;

    /**
     * 交易码：xdxw0078
     * 交易描述：推送云评估信息
     *
     * @param xdxw0078DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送云评估信息")
    @PostMapping("/xdxw0078")
    protected @ResponseBody
    ResultDto<Xdxw0078DataRespDto> xdxw0078(@Validated @RequestBody Xdxw0078DataReqDto xdxw0078DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataReqDto));
        Xdxw0078DataRespDto xdxw0078DataRespDto = new Xdxw0078DataRespDto();// 响应Dto:勘验任务查询
        ResultDto<Xdxw0078DataRespDto> xdxw0078DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0078DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataReqDto));
            xdxw0078DataRespDto = xdxw0078Service.xdxw0078(xdxw0078DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataRespDto));
            // 封装xdxw0078DataResultDto中正确的返回码和返回信息
            xdxw0078DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0078DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, e.getMessage());
            // 封装xdxw0078DataResultDto中异常返回码和返回信息
            xdxw0078DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0078DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0078DataRespDto到xdxw0078DataResultDto中
        xdxw0078DataResultDto.setData(xdxw0078DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataRespDto));
        return xdxw0078DataResultDto;
    }
}
