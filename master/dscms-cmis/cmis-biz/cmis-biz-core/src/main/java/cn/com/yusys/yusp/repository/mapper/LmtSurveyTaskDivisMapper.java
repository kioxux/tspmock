/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.DoubleViewDto;
import cn.com.yusys.yusp.dto.HxdxdInfoDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtSurveyTaskDivis;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyTaskDivisMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-15 09:46:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtSurveyTaskDivisMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtSurveyTaskDivis selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtSurveyTaskDivis> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtSurveyTaskDivis record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtSurveyTaskDivis record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtSurveyTaskDivis record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtSurveyTaskDivis record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectDoubleViewByModel
     * @方法描述: 根据条件唤醒贷信息查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<HxdxdInfoDto> selectHxdMessageByModel(Map queryMap);

    /**
     * @方法名称: updateBySernoSelective
     * @方法描述: 根据调查流水更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySernoSelective(LmtSurveyTaskDivis record);

    /**
     * @创建人 WH
     * @创建时间 2021-04-28 19:11
     * @注释 自建的条件分页查询 改变了状态参数  原本是=  现在是in 及传多个参数就可以查询不同条件的数据
     */
    List<LmtSurveyTaskDivis> findlistbymodel(QueryModel model);

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.domain.LmtSurveyTaskDivis
     * @author hubp
     * @date 2021/5/20 11:08
     * @version 1.0.0
     * @desc    根据调查流水号查找分配表信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    LmtSurveyTaskDivis selectBySurveySerno(@Param("surveySerno")String surveySerno);
   
}