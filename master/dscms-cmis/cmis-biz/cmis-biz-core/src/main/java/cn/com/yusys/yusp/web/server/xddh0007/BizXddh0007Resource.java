package cn.com.yusys.yusp.web.server.xddh0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.List;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDH0007:还款记录列表查询")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0007Resource.class);

    /**
     * 交易码：xddh0007
     * 交易描述：还款记录列表查询
     *
     * @param xddh0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款记录列表查询")
    @PostMapping("/xddh0007")
    protected @ResponseBody
    ResultDto<Xddh0007DataRespDto> xddh0007(@Validated @RequestBody Xddh0007DataReqDto xddh0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataReqDto));
        Xddh0007DataRespDto xddh0007DataRespDto = new Xddh0007DataRespDto();// 响应Dto:还款记录列表查询
        ResultDto<Xddh0007DataRespDto> xddh0007DataResultDto = new ResultDto<>();
		String billNo = xddh0007DataReqDto.getBillNo();//借据号
		Integer startPageNum = xddh0007DataReqDto.getStartPageNum();//起始页数
		Integer pageSize = xddh0007DataReqDto.getPageSize();//分页大小
        try {
            // 从xddh0007DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0007DataRespDto对象开始
			xddh0007DataRespDto.setTotalQnt(new Integer(1));// 总数
			List list = new List();
			list.setCusName(StringUtils.EMPTY);// 客户姓名
			list.setCusCertNo(StringUtils.EMPTY);// 客户身份证号
			list.setBillNo(StringUtils.EMPTY);// 借据号
			list.setLoanStatus(StringUtils.EMPTY);// 贷款状态
			list.setLoanAmt(new BigDecimal(0L));// 借据金额
			list.setLoanBalance(new BigDecimal(0L));// 贷款余额
			list.setHasbcCap(new BigDecimal(0L));// 已还本金
			list.setHasbcInt(new BigDecimal(0L));// 已还利息
			list.setRepayAmt(new BigDecimal(0L));// 还款金额
			list.setCurType(StringUtils.EMPTY);// 币种
			list.setLoanDate(StringUtils.EMPTY);// 贷款日期
			list.setLoanEndDate(StringUtils.EMPTY);// 贷款到期日
			list.setLoanRate(new BigDecimal(0L));// 贷款利率
			list.setRepayTime(StringUtils.EMPTY);// 还款时间
			xddh0007DataRespDto.setList(Arrays.asList(list));// start
            // TODO 封装xddh0007DataRespDto对象结束
            // 封装xddh0007DataResultDto中正确的返回码和返回信息
            xddh0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, e.getMessage());
            // 封装xddh0007DataResultDto中异常返回码和返回信息
            xddh0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddh0007DataRespDto到xddh0007DataResultDto中
        xddh0007DataResultDto.setData(xddh0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataResultDto));
        return xddh0007DataResultDto;
    }
}
