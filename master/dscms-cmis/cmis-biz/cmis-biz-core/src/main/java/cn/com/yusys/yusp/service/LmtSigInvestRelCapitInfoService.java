/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.LmtSigInvestRelCapitInfo;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestRelCapitInfoMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestRelCapitInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-28 13:55:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestRelCapitInfoService extends BizInvestCommonService{

    @Autowired
    private LmtSigInvestRelCapitInfoMapper lmtSigInvestRelCapitInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestRelCapitInfo selectByPrimaryKey(String pkId) {
        return lmtSigInvestRelCapitInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestRelCapitInfo> selectAll(QueryModel model) {
        List<LmtSigInvestRelCapitInfo> records = (List<LmtSigInvestRelCapitInfo>) lmtSigInvestRelCapitInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestRelCapitInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestRelCapitInfo> list = lmtSigInvestRelCapitInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int insert(LmtSigInvestRelCapitInfo record) {
        record.setPkId(generatePkId());
        record.setOprType(CmisBizConstants.OPR_TYPE_01);
        record.setCreateTime(getCurrrentDate());
        record.setUpdateTime(getCurrrentDate());
        record.setInputDate(getCurrrentDateStr());
        record.setInputId(getCurrentUser().getLoginCode());
        record.setInputBrId(getCurrentUser().getOrg().getCode());
        record.setUpdBrId(getCurrentUser().getOrg().getCode());
        record.setUpdId(getCurrentUser().getLoginCode());
        record.setUpdDate(getCurrrentDateStr());
        return lmtSigInvestRelCapitInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestRelCapitInfo record) {
        return lmtSigInvestRelCapitInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestRelCapitInfo record) {
        return lmtSigInvestRelCapitInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtSigInvestRelCapitInfo record) {
        return lmtSigInvestRelCapitInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestRelCapitInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestRelCapitInfoMapper.deleteByIds(ids);
    }

    /**
     * 逻辑删除
     * @param pkId
     * @return
     */
    public int deleteLogicByPkId(String pkId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId",pkId);
        BizInvestCommonService.checkParamsIsNull("pkId",pkId);
        List<LmtSigInvestRelCapitInfo> lmtSigInvestRelCapitInfos = selectAll(queryModel);
        if (lmtSigInvestRelCapitInfos!=null && lmtSigInvestRelCapitInfos.size()>0){
            LmtSigInvestRelCapitInfo lmtSigInvestRelCapitInfo = lmtSigInvestRelCapitInfos.get(0);
            lmtSigInvestRelCapitInfo.setOprType(CmisBizConstants.OPR_TYPE_02);
            return update(lmtSigInvestRelCapitInfo);
        }
        return 0;
    }
}
