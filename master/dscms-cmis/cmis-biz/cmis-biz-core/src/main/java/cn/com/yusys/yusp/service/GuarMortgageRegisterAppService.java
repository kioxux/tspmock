package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.GuarMortgageRegisterApp;
import cn.com.yusys.yusp.domain.GuarRegisterDetail;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarMortgageRegisterAppMapper;
import cn.com.yusys.yusp.vo.GuarMortgageRegisterVo;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageRegisterAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-16 11:33:28
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarMortgageRegisterAppService {

    private static final Logger log = LoggerFactory.getLogger(GuarMortgageRegisterAppService.class);

    @Autowired
    private GuarMortgageRegisterAppMapper guarMortgageRegisterAppMapper;

    @Autowired
    private GuarRegisterDetailService guarRegDetailService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GuarMortgageRegisterApp selectByPrimaryKey(String serno) {
        return guarMortgageRegisterAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<GuarMortgageRegisterApp> selectAll(QueryModel model) {
        List<GuarMortgageRegisterApp> records = (List<GuarMortgageRegisterApp>) guarMortgageRegisterAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GuarMortgageRegisterApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarMortgageRegisterApp> list = guarMortgageRegisterAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(GuarMortgageRegisterApp record) {
        return guarMortgageRegisterAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(GuarMortgageRegisterApp record) {
        return guarMortgageRegisterAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(GuarMortgageRegisterApp record) {
        return guarMortgageRegisterAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(GuarMortgageRegisterApp record) {
        return guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String serno) {
        return guarMortgageRegisterAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return guarMortgageRegisterAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto updateSelectiveByPrimaryKey(GuarMortgageRegisterApp record) {
        ResultDto<Integer> result = new ResultDto<>();
        try {
            int num = guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(record);
            if (0 > num) {
                result.setCode(CmisCommonConstants.INTERFACE_SUCCESS_CODE);
                result.setMessage(String.valueOf(EcbEnum.COMMON_SUCCESS_DEF));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            result.setCode(CmisCommonConstants.INTERFACE_FAIL_CODE);
            result.setMessage(String.valueOf(EcbEnum.COMMON_EXCEPTION_DEF));
        }
        return result;
    }

    /**
     * @方法名称: updateSelectiveToMap
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map updateSelectiveToMap(GuarMortgageRegisterApp record) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.value;
        try {
            log.info("修改数据信息开始！获取请求入参数据");
            int counts = guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(record);
            if (0 > counts) {
                throw new YuspException(EcbEnum.E_GUAR_REG_APP_UPDATE_FAILED.key, EcbEnum.E_GUAR_REG_APP_UPDATE_FAILED.value);
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            rtnCode = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: isExistByGuarContNo
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ResultDto<Boolean> isExistByGuarContNo(String guarContNo) {
        ResultDto<Boolean> resultDto = new ResultDto<>();
        String rtnCode = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.GUAR_REG_APP_SUCCESS_DEF.value;
        try {
            log.info("修改数据信息开始！获取请求入参数据");
            int counts = guarMortgageRegisterAppMapper.isExistByGuarContNo(guarContNo);
            if (counts > 0) {
                resultDto.data(true);
            } else {
                resultDto.data(false);
            }
        } catch (Exception e) {
            rtnCode = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.GUAR_REG_APP_EXCEPTION_DEF.value + e.getMessage();
        } finally {
            resultDto.setCode(rtnCode);
            resultDto.setMessage(rtnMsg);
        }
        return resultDto;
    }

    //流程最终处理类
    public ResultDto dealEnd(GuarMortgageRegisterApp record) {
        ResultDto<Integer> result = new ResultDto<>();
        try {
            int num = guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(record);
            if (0 > num) {
                result.setCode(CmisCommonConstants.INTERFACE_SUCCESS_CODE);
                result.setMessage(String.valueOf(EcbEnum.COMMON_SUCCESS_DEF));
            }
            List<GuarRegisterDetail> guarRegisterDetailList = guarRegDetailService.selectByRegSerno(record.getSerno());
            int flag = -1;
            //检查抵押登记申请下所有的押品明细是否是已完成状态
            guarRegisterDetailList.stream().forEach(a -> {

            });
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            result.setCode(CmisCommonConstants.INTERFACE_FAIL_CODE);
            result.setMessage(String.valueOf(EcbEnum.COMMON_EXCEPTION_DEF));
        }
        return result;
    }

    /**
     * 通过主键逻辑删除
     *
     * @param pkId
     * @return
     */
    public int deleteOnLogic(String pkId) {
        GuarMortgageRegisterApp guarMortgageRegisterApp = new GuarMortgageRegisterApp();
        guarMortgageRegisterApp.setSerno(pkId);
        guarMortgageRegisterApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(guarMortgageRegisterApp);
    }

    /**
     * @方法名称: saveByGuarBasicBuildingVo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public ResultDto saveByGuarMortgageRegisterVo(GuarMortgageRegisterVo record) {
        //TODO..修改封装逻辑
        ResultDto resultDto = new ResultDto("0", "保存成功");
        GuarMortgageRegisterApp guarMortgageRegisterApp = record.getGuarMortgageRegisterApp();
        List<GuarRegisterDetail> guarRegisterDetail = record.getGuarRegisterDetail();
        int numGuarBaseInfo = checkGuarInfoIsExist(guarMortgageRegisterApp.getSerno());
        int resultGuarMortgageRegisterApp = -1;
        int resultGuarRegDetail = -1;
        if (0 < numGuarBaseInfo) {
            resultGuarMortgageRegisterApp = guarMortgageRegisterAppMapper.updateByPrimaryKeySelective(guarMortgageRegisterApp);
        } else {
            resultGuarMortgageRegisterApp = guarMortgageRegisterAppMapper.insert(guarMortgageRegisterApp);
        }
        resultGuarRegDetail = guarRegDetailService.saveGuarRegisterDetail(guarRegisterDetail);
        return resultDto;
    }

    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明: 存在 为 1 ,不存在为 0
     * @算法描述:
     */
    public int checkGuarInfoIsExist(String serno) {
        GuarMortgageRegisterApp guarMortgageRegisterApp = guarMortgageRegisterAppMapper.selectByPrimaryKey(serno);
        return (null == guarMortgageRegisterApp || null == guarMortgageRegisterApp.getSerno() || "".equals(guarMortgageRegisterApp.getSerno())) ? 0 : 1;
    }
}
