package cn.com.yusys.yusp.service.server.xdtz0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0011.req.Xdtz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.Xdtz0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 接口处理类:借据信息明细
 *
 * @author xs
 * @version 1.0
 */
@Service
public class Xdtz0011Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdtz0011.Xdtz0011Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CommonService commonService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0011DataRespDto xdtz0011(Xdtz0011DataReqDto xdtz0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataReqDto));
        Xdtz0011DataRespDto xdtz0011DataRespDto = new Xdtz0011DataRespDto();
        int record = 0;//查询结果
        try {
            String cusId = xdtz0011DataReqDto.getCusId();//客户号
            String billNo = xdtz0011DataReqDto.getBillNo();//借据编号
            String certNo = xdtz0011DataReqDto.getCertNo();//身份证号
            String qryType = xdtz0011DataReqDto.getQryType();//查询类型
            //查询参数
            logger.info("借据明细,查询参数为:{}", xdtz0011DataReqDto);
            // 根据查询类型 1 列表查询  2 明细查询
            if("1".equals(qryType)){
                // 列表查询
                if(StringUtils.isEmpty(cusId)){
                    if(StringUtils.isNotEmpty(certNo)){
                        //根据身份证号获取客户编号
                        cusId = commonService.getCusBaseByCertCode(certNo).get(0);
                    }else{
                        // 客户号和证件号码不能同时为空
                        return xdtz0011DataRespDto;
                    }
                }
                java.util.List<List> list = accLoanMapper.getBillInfoListByCusId(cusId);
                xdtz0011DataRespDto.setList(list);
                if(list.size()>0){
                    xdtz0011DataRespDto.setQnt(new BigDecimal(list.size()));
                }else{
                    xdtz0011DataRespDto.setQnt(BigDecimal.ZERO);
                }
            }else if("2".equals(qryType)){
                //明细查询
                if(StringUtils.isNotEmpty(billNo)){
                    xdtz0011DataRespDto = accLoanMapper.getBillInfoByBillNo(billNo);
                }else{
                    //借据号不可以为空
                    throw BizException.error(null, EcbEnum.ECB010012.key, EcbEnum.ECB010012.value);
                }
            }else{
                //查询类型 为空
                throw BizException.error(null, EcbEnum.ECB010013.key, EcbEnum.ECB010013.value);
            }
            logger.info("借据明细,反回参数为:{}", JSON.toJSONString(record));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, e.getMessage());
            throw new Exception(EcbEnum.ECB019999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataRespDto));
        return xdtz0011DataRespDto;
    }
}
