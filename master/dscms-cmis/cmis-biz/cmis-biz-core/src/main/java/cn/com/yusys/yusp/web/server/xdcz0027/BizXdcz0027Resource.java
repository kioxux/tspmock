package cn.com.yusys.yusp.web.server.xdcz0027;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0027.req.Xdcz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0027.resp.Xdcz0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.service.server.xdcz0027.Xdcz0027Service;

import java.util.Map;

/**
 * 接口处理类:审批失败支用申请( 登记支用失败)
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0027:审批失败支用申请( 登记支用失败)")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0027Resource.class);

    @Autowired
    private Xdcz0027Service xdcz0027Service;

    /**
     * 交易码：xdcz0027
     * 交易描述：审批失败支用申请( 登记支用失败)
     *
     * @param xdcz0027DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("审批失败支用申请( 登记支用失败)")
    @PostMapping("/xdcz0027")
    protected @ResponseBody
    ResultDto<Xdcz0027DataRespDto>  xdcz0027(@Validated @RequestBody Xdcz0027DataReqDto xdcz0027DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027DataReqDto));
        Xdcz0027DataRespDto  xdcz0027DataRespDto  = new Xdcz0027DataRespDto();// 响应Dto:审批失败支用申请( 登记支用失败)
        ResultDto<Xdcz0027DataRespDto>xdcz0027DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0027DataReqDto获取业务值进行业务逻辑处理
            Map<String, String> result = xdcz0027Service.execute(xdcz0027DataReqDto);
            String code = result.get("code");
            String message = result.get("message");
            xdcz0027DataRespDto.setOpFlag(code);// 操作成功标志位
            xdcz0027DataRespDto.setOpMsg(message);// 描述信息
            // 封装xdcz0027DataResultDto中正确的返回码和返回信息
            xdcz0027DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0027DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value,e.getMessage());
            // 封装xdcz0027DataResultDto中异常返回码和返回信息
            xdcz0027DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0027DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0027DataRespDto到xdcz0027DataResultDto中
        xdcz0027DataResultDto.setData(xdcz0027DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027DataRespDto));
        return xdcz0027DataResultDto;
    }
}
