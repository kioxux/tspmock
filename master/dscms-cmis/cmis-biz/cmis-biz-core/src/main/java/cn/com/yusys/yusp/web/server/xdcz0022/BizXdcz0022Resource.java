package cn.com.yusys.yusp.web.server.xdcz0022;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0022.req.Xdcz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.resp.Xdcz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0022.Xdcz0022Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:风控发送相关信息至信贷进行支用校验
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0022:风控发送相关信息至信贷进行支用校验")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0022Resource.class);

    @Autowired
    private Xdcz0022Service xdcz0022Service;

    /**
     * 交易码：xdcz0022
     * 交易描述：风控发送相关信息至信贷进行支用校验
     *
     * @param xdcz0022DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发送相关信息至信贷进行支用校验")
    @PostMapping("/xdcz0022")
    protected @ResponseBody
    ResultDto<Xdcz0022DataRespDto> xdcz0022(@Validated @RequestBody Xdcz0022DataReqDto xdcz0022DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataReqDto));
        Xdcz0022DataRespDto xdcz0022DataRespDto = new Xdcz0022DataRespDto();// 响应Dto:小贷用途承诺书文本生成pdf
        ResultDto<Xdcz0022DataRespDto> xdcz0022DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataReqDto));
            xdcz0022DataRespDto = xdcz0022Service.sendMessageToCheck(xdcz0022DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataRespDto));
            // 封装xdcz0022DataResultDto中正确的返回码和返回信息
            xdcz0022DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0022DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, e.getMessage());
            // 封装xdcz0022DataResultDto中异常返回码和返回信息
            xdcz0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0022DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, e.getMessage());
            // 封装xdcz0022DataResultDto中异常返回码和返回信息
            xdcz0022DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0022DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0022DataRespDto到xdcz0022DataResultDto中
        xdcz0022DataResultDto.setData(xdcz0022DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataRespDto));
        return xdcz0022DataResultDto;
    }
}
