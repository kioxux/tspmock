/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OnlineCvrgCusInfo
 * @类描述: online_cvrg_cus_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-04 20:50:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "online_cvrg_cus_info")
public class OnlineCvrgCusInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 企业统一社会代码 **/
	@Column(name = "credit_code", unique = false, nullable = true, length = 60)
	private String creditCode;
	
	/** 企业客户名称 **/
	@Column(name = "cus_name", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 联系人 **/
	@Column(name = "contacts", unique = false, nullable = true, length = 80)
	private String contacts;
	
	/** 联系电话 **/
	@Column(name = "phone", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 登记时间 **/
	@Column(name = "input_time", unique = false, nullable = true, length = 30)
	private String inputTime;
	
	/** 管户客户经理号 **/
	@Column(name = "manager_id", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 管户机构号 **/
	@Column(name = "manager_br_id", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 处理结果(01已完成，02已关闭) **/
	@Column(name = "result", unique = false, nullable = true, length = 2)
	private String result;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param creditCode
	 */
	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}
	
    /**
     * @return creditCode
     */
	public String getCreditCode() {
		return this.creditCode;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contacts
	 */
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	
    /**
     * @return contacts
     */
	public String getContacts() {
		return this.contacts;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param result
	 */
	public void setResult(String result) {
		this.result = result;
	}
	
    /**
     * @return result
     */
	public String getResult() {
		return this.result;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}