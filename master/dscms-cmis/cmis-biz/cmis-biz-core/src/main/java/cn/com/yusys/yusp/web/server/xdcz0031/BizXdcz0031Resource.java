package cn.com.yusys.yusp.web.server.xdcz0031;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0031.req.Xdcz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.resp.Xdcz0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdcz0031.Xdcz0031Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0031:查询出账次数")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0031Resource {
	private static final Logger logger = LoggerFactory.getLogger(BizXdcz0031Resource.class);

	@Autowired
	private Xdcz0031Service xdcz0031Service;

	/**
	 * 交易码：xdca0031
	 * 交易描述：
	 *
	 * @param xdcz0031DataReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("出账次数查询")
	@PostMapping("/xdcz0031")
	protected @ResponseBody
	ResultDto<Xdcz0031DataRespDto> xdcz0031(@Validated @RequestBody Xdcz0031DataReqDto xdcz0031DataReqDto) throws Exception {
		logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataReqDto));
		Xdcz0031DataRespDto xdcz0031DataRespDto = new Xdcz0031DataRespDto();// 响应Dto:
		ResultDto<Xdcz0031DataRespDto> xdca0003DataResultDto = new ResultDto<>();
		try {
			String billNo = xdcz0031DataReqDto.getBillNo();//借据

			Boolean flag = true;
			if (StringUtil.isEmpty(billNo)) {
				flag = false;
			}
			if (flag) {
				// 从xdca0003DataReqDto获取业务值进行业务逻辑处理
				logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataReqDto));
				xdcz0031DataRespDto = xdcz0031Service.xdcz0031(xdcz0031DataReqDto);
				logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataRespDto));
				// 封装xdca0003DataResultDto中正确的返回码和返回信息
				xdca0003DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xdca0003DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
			} else {
				// 请求字段为空
				xdca0003DataResultDto.setCode(EcbEnum.ECB010001.key);
				xdca0003DataResultDto.setMessage(EcbEnum.ECB010001.value);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, e.getMessage());
			// 封装xdca0003DataResultDto中异常返回码和返回信息
			//  EcsEnum.ECS049999 待调整 开始
			xdca0003DataResultDto.setCode(EpbEnum.EPB099999.key);
			xdca0003DataResultDto.setMessage(EpbEnum.EPB099999.value);
			//  EcsEnum.ECS049999 待调整  结束
		}
		// 封装xdca0003DataRespDto到xdca0003DataResultDto中
		xdca0003DataResultDto.setData(xdcz0031DataRespDto);
		logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataRespDto));
		return xdca0003DataResultDto;
	}
}
