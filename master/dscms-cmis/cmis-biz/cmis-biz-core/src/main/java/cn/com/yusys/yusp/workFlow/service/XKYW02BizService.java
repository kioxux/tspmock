package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constant.CardPrdCode;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CreditCardAppInfo;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CreditCardAppInfoService;
import cn.com.yusys.yusp.service.CreditReportQryLstService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @className CreditReportQryLst
 * @Description 征信查询业务后处理
 * @Date 2020/12/21 : 10:43
 */
@Service
public class XKYW02BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(XKYW02BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CreditCardAppInfoService creditCardAppInfoService;

    private CreditCardAppInfo creditCardAppInfo;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {

        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        String nodeId = instanceInfo.getNodeId();
        Map param = instanceInfo.getParam();
        String firstflag ="";
        if(param.containsKey("firstflag")){
            firstflag = String.valueOf(param.get("firstflag"));
        }
        log.info("流程状理开始："+serno);
        CreditCardAppInfo creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(serno);
        updatFlowParam(instanceInfo);
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("流程状态处理开始："+serno);
                creditCardAppInfo.setBusinessStage("A05");//信息终审
                creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                creditCardAppInfoService.updateSelective(creditCardAppInfo);
                log.info("-------业务处理 正常下一步,零售内评系统字段决策结果开始：-- ----"+instanceInfo);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                //审批通过
                log.info("结束操作:" + instanceInfo);
                creditCardAppInfoService .handleBusinessDatFirst(serno,nodeId);
                creditCardAppInfoService .handleBusinessDataAfterEnd(serno,firstflag,nodeId);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    creditCardAppInfoService.updateSelective(creditCardAppInfo);
                }
            }else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    creditCardAppInfoService.updateSelective(creditCardAppInfo);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
                creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                creditCardAppInfoService.updateSelective(creditCardAppInfo);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                creditCardAppInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                creditCardAppInfoService.updateSelective(creditCardAppInfo);
                log.info("否决操作结束:" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_XKYW02.equals(flowCode);
    }

    /**
     * 更新流程变量-审批状态
     *
     * @author shenli
     * @date 2021-9-6 09:34:28
     **/
    private void updatFlowParam(ResultInstanceDto resultInstanceDto) {
        CreditCardAppInfo  creditCardAppInfo = creditCardAppInfoService.selectByPrimaryKey(resultInstanceDto.getBizId());
        if(creditCardAppInfo != null ){
            WFBizParamDto param = new WFBizParamDto();
            param.setBizId(resultInstanceDto.getBizId());
            param.setInstanceId(resultInstanceDto.getInstanceId());
            String applyCardPrd = creditCardAppInfo.getApplyCardPrd();
            String cardCusType = creditCardAppInfo.getCardCusType();
            String outsystemcode = "XXD_PK_B0601,XXD_PK_B0602,XXD_PK_B0603,XXD_PK_B0604,XXD_PK_B0605";
            String topoutsystemcode = "XXD_PK";
            // 客户类型与影像目录映射
            if (!CardPrdCode.CARD_PRD_CODE_LYJK.equals(applyCardPrd) && !CardPrdCode.CARD_PRD_CODE_LYJDZK.equals(applyCardPrd)) {
                if ("B06".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0601,XXD_PK_B0602,XXD_PK_B0603,XXD_PK_B0604,XXD_PK_B0605";
                } else if ("B01".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0101,XXD_PK_B0102,XXD_PK_B0103,XXD_PK_B0104,XXD_PK_B0105";
                } else if ("B02".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0201,XXD_PK_B0202,XXD_PK_B0203,XXD_PK_B0204,XXD_PK_B0205";
                } else if ("B07".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0701,XXD_PK_B0702,XXD_PK_B0703,XXD_PK_B0704,XXD_PK_B0705";
                } else if ("B03".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0301,XXD_PK_B0302,XXD_PK_B0303,XXD_PK_B0304,XXD_PK_B0305";
                } else if ("C09".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_C0901,XXD_PK_C0902,XXD_PK_C0903,XXD_PK_C0904,XXD_PK_C0905,XXD_PK_C0906";
                } else if ("C06".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_C0601,XXD_PK_C0602,XXD_PK_C0603,XXD_PK_C0604,XXD_PK_C0605,XXD_PK_C0606";
                } else if ("H07".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_H0701,XXD_PK_H0702,XXD_PK_H0703,XXD_PK_H0704,XXD_PK_H0705,XXD_PK_H0706";
                } else if ("E01".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_E0101,XXD_PK_E0102,XXD_PK_E0103,XXD_PK_E0104,XXD_PK_E0105,XXD_PK_E0106";
                } else if ("H04".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_H0401,XXD_PK_H0402,XXD_PK_H0403,XXD_PK_H0404,XXD_PK_H0405,XXD_PK_H0406";
                } else if ("B14".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B1401,XXD_PK_B1402,XXD_PK_B1403,XXD_PK_B1404,XXD_PK_B1405,XXD_PK_B1406";
                } else if ("X01".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_X0101,XXD_PK_X0102,XXD_PK_X0103,XXD_PK_X0104,XXD_PK_X0105,XXD_PK_X0106";
                } else if ("C08".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_C0801,XXD_PK_C0802,XXD_PK_C0803,XXD_PK_C084,XXD_PK_C0805,XXD_PK_C0806,XXD_PK_C0807,XXD_PK_C0808";
                } else if ("B15".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B1501,XXD_PK_B1502,XXD_PK_B1503,XXD_PK_B1504,XXD_PK_B1505,XXD_PK_B1506";
                } else if ("C04".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_C0401,XXD_PK_C0402,XXD_PK_C0403,XXD_PK_C0404,XXD_PK_C0405,XXD_PK_C0406,XXD_PK_C0407";
                } else if ("B04".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_B0401,XXD_PK_B0402,XXD_PK_B0403,XXD_PK_B0404,XXD_PK_B0405,XXD_PK_B0406";
                } else if ("H05".equals(cardCusType)) {
                    outsystemcode = "XXD_PK_H0501,XXD_PK_H0502,XXD_PK_H0503,XXD_PK_H0504,XXD_PK_H0505,XXD_PK_H0506";
                }
            } else {
                // 乐悠金电子卡影像目录
                topoutsystemcode = "XXD_LYJ";
                if ("B06".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B0601,XXD_LYJ_B0602,XXD_LYJ_B0603,XXD_LYJ_B0604,XXD_LYJ_B0605";
                } else if ("B01".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B0101,XXD_LYJ_B0102,XXD_LYJ_B0103,XXD_LYJ_B0104,XXD_LYJ_B0105";
                } else if ( "B02".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B0201,XXD_LYJ_B0202,XXD_LYJ_B0203,XXD_LYJ_B0204,XXD_LYJ_B0205";
                } else if ("B03".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B0301,XXD_LYJ_B0302,XXD_LYJ_B0303,XXD_LYJ_B0304,XXD_LYJ_B0305";
                } else if ("C09".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_C0901,XXD_LYJ_C0902,XXD_LYJ_C0903,XXD_LYJ_C0904,XXD_LYJ_C0905,,XXD_LYJ_C0906";
                } else if ("E01".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_E0101,XXD_LYJ_E0102,XXD_LYJ_E0103,XXD_LYJ_E0104,XXD_LYJ_E0105,XXD_LYJ_E0106,XXD_LYJ_E0107,XXD_LYJ_E0108,XXD_LYJ_E0109,XXD_LYJ_E0110";
                } else if ("H04".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_H0401,XXD_LYJ_H0402,XXD_LYJ_H0403,XXD_LYJ_H0404,XXD_LYJ_H0405,XXD_LYJ_H0406,XXD_LYJ_H0407,XXD_LYJ_H0408,XXD_LYJ_H0409,XXD_LYJ_H0410";
                } else if ("B14".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B1401,XXD_LYJ_B1402,XXD_LYJ_B1403,XXD_LYJ_B1404,XXD_LYJ_B1405,XXD_LYJ_B1406";
                } else if ("C08".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_C0801,XXD_LYJ_C0802,XXD_LYJ_C0803,XXD_LYJ_C0804,XXD_LYJ_C0805,XXD_LYJ_C0806,XXD_LYJ_C0807,XXD_LYJ_C0808";
                } else if ("C03".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_C0301,XXD_LYJ_C0302,XXD_LYJ_C0303,XXD_LYJ_C0304,XXD_LYJ_C0305,XXD_LYJ_C0306,XXD_LYJ_C0307";
                } else if ("C02".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_C0401,XXD_LYJ_C0402,XXD_LYJ_C0403,XXD_LYJ_C0404,XXD_LYJ_C0405,XXD_LYJ_C0406";
                } else if ("B08".equals(cardCusType)) {
                    outsystemcode = "XXD_LYJ_B0801,XXD_LYJ_B0802,XXD_LYJ_B0803,XXD_LYJ_B0804,XXD_LYJ_B0805,XXD_LYJ_B0806";
                }
            }
            Map<String, Object> params = new HashMap<>();
            params = resultInstanceDto.getParam();
            params.put("applyCardType",creditCardAppInfo.getApplyType());
            params.put("topoutsystemcode",topoutsystemcode);
            params.put("outsystemcode",outsystemcode);
            params.put("certCode",creditCardAppInfo.getCertCode());
            log.info("流程变量:" + params.toString());
            param.setParam(params);
            workflowCoreClient.updateFlowParam(param);
        }

    }
}
