/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpEntrustLoanApp;
import cn.com.yusys.yusp.domain.IqpHighAmtAgrApp;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpChgTrupayAcctApp;
import cn.com.yusys.yusp.service.IqpChgTrupayAcctAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpChgTrupayAcctAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 屈文
 * @创建时间: 2021-04-14 20:06:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpchgtrupayacctapp")
public class IqpChgTrupayAcctAppResource {
    @Autowired
    private IqpChgTrupayAcctAppService iqpChgTrupayAcctAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpChgTrupayAcctApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpChgTrupayAcctApp> list = iqpChgTrupayAcctAppService.selectAll(queryModel);
        return new ResultDto<List<IqpChgTrupayAcctApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpChgTrupayAcctApp>> index(QueryModel queryModel) {
        List<IqpChgTrupayAcctApp> list = iqpChgTrupayAcctAppService.selectByModel(queryModel);
        return new ResultDto<List<IqpChgTrupayAcctApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpChgTrupayAcctApp> show(@PathVariable("pkId") String pkId) {
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = iqpChgTrupayAcctAppService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpChgTrupayAcctApp>(iqpChgTrupayAcctApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpChgTrupayAcctApp> create(@RequestBody IqpChgTrupayAcctApp iqpChgTrupayAcctApp) throws URISyntaxException {
        iqpChgTrupayAcctAppService.insert(iqpChgTrupayAcctApp);
        return new ResultDto<IqpChgTrupayAcctApp>(iqpChgTrupayAcctApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpChgTrupayAcctApp iqpChgTrupayAcctApp) throws URISyntaxException {
        int result = iqpChgTrupayAcctAppService.update(iqpChgTrupayAcctApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpChgTrupayAcctAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpChgTrupayAcctAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 修改受托账号申请保存操作
     * @param map
     * @return
     */
    @PostMapping("/savechgtrupayacctappinfo")
    public ResultDto<Map> saveIqpChgTrupayAcctAppInfo(@RequestBody Map map){
        Map result = iqpChgTrupayAcctAppService.saveIqpChgTrupayAcctAppInfo(map);
        return new ResultDto<>(result);
    }

    /**
     * 修改受托账号申请通用的保存方法
     * @param params
     * @return
     */
    @PostMapping("/commonsavechgtrupayacctappinfo")
    public ResultDto<Map> commonSaveIqpChgTrupayAcctAppInfo(@RequestBody Map params){
        Map rtnData = iqpChgTrupayAcctAppService.commonSaveIqpChgTrupayAcctAppInfo(params);
        return new ResultDto<>(rtnData);
    }

    /**
     * @函数名称:toSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/tosignlist")
    protected ResultDto<List<IqpChgTrupayAcctApp>> toSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpChgTrupayAcctApp> list = iqpChgTrupayAcctAppService.toSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpChgTrupayAcctApp>>(list);
    }

    /**
     * @函数名称:doneSignlist
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/donesignlist")
    protected ResultDto<List<IqpChgTrupayAcctApp>> doneSignlist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno asc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<IqpChgTrupayAcctApp> list = iqpChgTrupayAcctAppService.doneSignlist(queryModel);
        PageHelper.clearPage();
        long total = 0;
        return new ResultDto<List<IqpChgTrupayAcctApp>>(list);
    }

    /**
     * @函数名称:sendcore
     * @函数描述:修改受托信息发送核心
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendcore")
    protected  ResultDto<Map> sendCore(@RequestBody IqpChgTrupayAcctApp iqpChgTrupayAcctApp) {
        Map rtnData =  iqpChgTrupayAcctAppService.sendCore(iqpChgTrupayAcctApp);
        return new ResultDto<>(rtnData);
    }

    @ApiOperation("修改受托账号删除操作")
    @PostMapping("/deleteiqpchgtrupayacctapp")
    protected ResultDto<Map> deleteIqpChgtruPayAcctapp(@RequestBody Map params) {
        Map map = iqpChgTrupayAcctAppService.deleteIqpChgtruPayAcctapp((String)params.get("serno"));
        return new ResultDto<Map>(map);
    }


    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody  Map map) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        IqpChgTrupayAcctApp temp = new IqpChgTrupayAcctApp();
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = iqpChgTrupayAcctAppService.selectByPrimaryKey((String)map.get("pkId"));
        if (iqpChgTrupayAcctApp != null) {
            resultDto.setCode(200);
            resultDto.setData(iqpChgTrupayAcctApp);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }
}
