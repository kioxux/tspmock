package cn.com.yusys.yusp.web.server.xdsx0018;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0018.req.Xdsx0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.resp.Xdsx0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0017.Xdsx0017Service;
import cn.com.yusys.yusp.service.server.xdsx0018.Xdsx0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:房抵e点贷无还本续贷审核
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDSX0018:房抵e点贷无还本续贷审核")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0018Resource.class);
    @Autowired
    private Xdsx0018Service xdsx0018Service;
    /**
     * 交易码：xdsx0018
     * 交易描述：房抵e点贷无还本续贷审核
     *
     * @param xdsx0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("房抵e点贷无还本续贷审核")
    @PostMapping("/xdsx0018")
    protected @ResponseBody
    ResultDto<Xdsx0018DataRespDto> xdsx0018(@Validated @RequestBody Xdsx0018DataReqDto xdsx0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018DataReqDto));
        Xdsx0018DataRespDto xdsx0018DataRespDto = new Xdsx0018DataRespDto();// 响应Dto:房抵e点贷无还本续贷审核
        ResultDto<Xdsx0018DataRespDto> xdsx0018DataResultDto = new ResultDto<>();
        try {
            // 从xdsx0018DataReqDto获取业务值进行业务逻辑处理
            // 调用xdsx0018Service层开始
            xdsx0018DataRespDto = xdsx0018Service.Xdsx0018(xdsx0018DataReqDto);
            // 调用xdsx0018Service层结束
            // 封装xdsx0018DataResultDto中正确的返回码和返回信息
            xdsx0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, e.getMessage());
            // 封装xdsx0018DataResultDto中异常返回码和返回信息
            // TODO EcsEnum.ECS049999 待调整 开始
            xdsx0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdsx0018DataRespDto到xdsx0018DataResultDto中
        xdsx0018DataResultDto.setData(xdsx0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018DataRespDto));
        return xdsx0018DataResultDto;
    }
}
