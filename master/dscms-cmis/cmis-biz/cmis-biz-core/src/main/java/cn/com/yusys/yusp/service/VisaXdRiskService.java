/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.VisaXdRisk;
import cn.com.yusys.yusp.dto.CusIndivAttrDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.req.Fb1166ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.resp.Fb1166RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.VisaXdRiskMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: VisaXdRiskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-19 21:50:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class VisaXdRiskService {

    @Autowired
    private VisaXdRiskMapper visaXdRiskMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private ICusClientService icusClientService;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(VisaXdRiskService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public VisaXdRisk selectByPrimaryKey(String serno) {
        return visaXdRiskMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<VisaXdRisk> selectAll(QueryModel model) {
        List<VisaXdRisk> records = (List<VisaXdRisk>) visaXdRiskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<VisaXdRisk> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<VisaXdRisk> list = visaXdRiskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<VisaXdRisk> querySfVisaList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<VisaXdRisk> list = visaXdRiskMapper.querySfVisaList(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(VisaXdRisk record) {
        return visaXdRiskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(VisaXdRisk record) {
        return visaXdRiskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(VisaXdRisk record) {
        return visaXdRiskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateVisaXdRise
     * @方法描述: 面签信息修改将信息同步发送风控
     * @参数与返回说明: 交易码fb1166
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public ResultDto<String> updateVisaXdRise(VisaXdRisk record) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (record != null) {
            //通过客户编号去客户表查询相关数据
            ResultDto<CusIndivAttrDto> cusIndivAttrDtoResultDto = new ResultDto<>();
            // 根据客户号获取
            log.info("根据客户编号查询个人客户属性信息表：" + record.getSerno());
            cusIndivAttrDtoResultDto=icusClientService.queryCusIndivAttrByCusId(record.getCusId());
            log.info("根据客户号获取个人客户基本信息返回：" + cusIndivAttrDtoResultDto);
            if (Objects.isNull(cusIndivAttrDtoResultDto) || !Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cusIndivAttrDtoResultDto.getCode())
                    || Objects.isNull(cusIndivAttrDtoResultDto.getData()) || Objects.isNull(cusIndivAttrDtoResultDto.getData().getIsSmconCus())) {
                resultDto.setCode("9999");
                resultDto.setMessage("请先维护完整个人客户信息");
                return resultDto;
            }
            log.info("面签信息修改推送风控开始,流水号【{}】", record.getSerno());
            Fb1166ReqDto fb1166ReqDto = new Fb1166ReqDto();
            fb1166ReqDto.setCHANNEL_TYPE("13"); //渠道来源 默认‘13  ’ 参照老信贷代码逻辑
            fb1166ReqDto.setCO_PLATFORM("2002"); //合作平台 默认‘2002’ 参照老信贷代码逻辑
            fb1166ReqDto.setPRD_CODE ("2002000001"); //产品代码 默认‘2002000001’ 参照老信贷代码逻辑
            fb1166ReqDto.setCus_id(record.getCusId()); //客户id
            fb1166ReqDto.setCus_name(record.getCusName()); //客户名称
            fb1166ReqDto.setCredit_type(commonService.toFkCertType(record.getCertType())); //证件类型
            fb1166ReqDto.setCredit_id(record.getCertCode()); //证件号
            fb1166ReqDto.setPhone_no(record.getMobileNo()); //电话号
            fb1166ReqDto.setManager_id(record.getManagerId()); //管户经理编号
            fb1166ReqDto.setManager_name(record.getManagerName()); //管户经理名称
            fb1166ReqDto.setQd_time(record.getSignatureTime()); //面签时间
            fb1166ReqDto.setQd_area(record.getSignatureAddr()); //面前地址
            log.info("面签邀约结果推送风控请求：" + fb1166ReqDto);
            ResultDto<Fb1166RespDto> fb1166RespResultDto = dscms2CircpClientService.fb1166(fb1166ReqDto);
            log.info("面签邀约结果推送风控返回：" + fb1166RespResultDto);
            String fb1166Code = Optional.ofNullable(fb1166RespResultDto.getCode()).orElse(StringUtils.EMPTY);
            String fb1166Meesage = Optional.ofNullable(fb1166RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
            log.info("面签邀约结果推送风控返回信息：" + fb1166Meesage);
            if (Objects.equals(fb1166Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                // 获取修改日期
                record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                visaXdRiskMapper.updateByPrimaryKey(record);
            } else {
                resultDto.setCode(fb1166Code);
                resultDto.setMessage(fb1166Meesage);
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        return resultDto;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(VisaXdRisk record) {
        return visaXdRiskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return visaXdRiskMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return visaXdRiskMapper.deleteByIds(ids);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public VisaXdRisk selectBySerno(String serno) {
        return visaXdRiskMapper.selectByPrimaryKey(serno);
    }

}
