package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/7/5 21:03
 * @desc  白领易贷通产品校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0014Service {

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/7/5 21:04
     * @version 1.0.0
     * @desc    白领易贷通产品校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto riskItem0014 ( QueryModel queryModel) {
        RiskResultDto riskResultDto = null;
        String lcbh = queryModel.getCondition().get("bizType").toString();
        if("LS001".equals(lcbh) || "LS002".equals(lcbh) || "LS003".equals(lcbh)
                || "SGE01".equals(lcbh) || "SGE02".equals(lcbh) || "SGE03".equals(lcbh)
                || "DHE01".equals(lcbh) || "DHE02".equals(lcbh) || "DHE03".equals(lcbh)){//授信
            riskResultDto = iqpLoanAppService.riskItem0014(queryModel.getCondition().get("bizId").toString());
        }else if ("LS005".equals(lcbh) || "LS006".equals(lcbh)
                || "SGE04".equals(lcbh) || "DHE04".equals(lcbh)){//放款
            riskResultDto = pvpLoanAppService.riskItem0014(queryModel.getCondition().get("bizId").toString());
        }
        return ResultDto.success(riskResultDto);
    }
}
