package cn.com.yusys.yusp.web.server.xdxw0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdxw0021.Xdxw0021Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0021.req.Xdxw0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0021.resp.Xdxw0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0021:根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0021Resource.class);

    @Autowired
    private Xdxw0021Service xdxw0021Service;
    /**
     * 交易码：xdxw0021
     * 交易描述：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
     *
     * @param xdxw0021DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）")
    @PostMapping("/xdxw0021")
    protected @ResponseBody
    ResultDto<Xdxw0021DataRespDto> xdxw0021(@Validated @RequestBody Xdxw0021DataReqDto xdxw0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataReqDto));
        Xdxw0021DataRespDto xdxw0021DataRespDto = new Xdxw0021DataRespDto();// 响应Dto:根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
        ResultDto<Xdxw0021DataRespDto> xdxw0021DataResultDto = new ResultDto<>();
        String serno = xdxw0021DataReqDto.getSerno();//业务流水号
        try {
            if(StringUtils.isEmpty(serno)){
                xdxw0021DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
                return xdxw0021DataResultDto;
            }
            // 从xdxw0021DataReqDto获取业务值进行业务逻辑处理
            xdxw0021DataRespDto = xdxw0021Service.xdxw0021(xdxw0021DataReqDto);

            // 封装xdxw0021DataResultDto中正确的返回码和返回信息
            xdxw0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, e.getMessage());
            // 封装xdxw0021DataResultDto中异常返回码和返回信息
            xdxw0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0021DataRespDto到xdxw0021DataResultDto中
        xdxw0021DataResultDto.setData(xdxw0021DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataResultDto));
        return xdxw0021DataResultDto;
    }
}
