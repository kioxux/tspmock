package cn.com.yusys.yusp.service.server.xdzc0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.domain.AsplIoPoolDetails;
import cn.com.yusys.yusp.dto.server.xdzc0019.req.Xdzc0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.resp.Xdzc0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import cn.com.yusys.yusp.service.AsplIoPoolDetailsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:历史资产查询接口
 * 入池、出池相关操作记录
 * @Author xs
 * @Date 2021/6/11 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0019Service {

    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0019.Xdzc0019Service.class);

    /**
     * 交易码：xdzc0019
     * 交易描述：
     * 历史资产查询接口
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0019DataRespDto xdzc0019Service(Xdzc0019DataReqDto xdzc0019DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value);
        Xdzc0019DataRespDto xdzc0019DataRespDto = new Xdzc0019DataRespDto();

        String assetType = xdzc0019DataReqDto.getAssetType();// 资产类型
        String cusId = xdzc0019DataReqDto.getCusId();// 客户编号
        String contNo = xdzc0019DataReqDto.getContNo();// 资产池协议编号
        String page = xdzc0019DataReqDto.getPageNum();//查询起始页
        String size = xdzc0019DataReqDto.getPageSize();//请求条数
        String queryType = xdzc0019DataReqDto.getQueryType();// 当前历史 0当前(池内资产清单) 1历史（出入池记录）
        String assetStartDate = xdzc0019DataReqDto.getAssetStartDate();// 资产到期日起（yyyy-MM-dd）
        String assetEndDate = xdzc0019DataReqDto.getAssetEndDate();// 资产到期日止
        String assetCeiling = xdzc0019DataReqDto.getAssetCeiling();// 资产价值上限
        String assetFloor = xdzc0019DataReqDto.getAssetFloor();// 资产价值下限
        String opStartDate = xdzc0019DataReqDto.getOpStartDate();// 操作日开始
        String opEndDate = xdzc0019DataReqDto.getOpEndDate();// 操作日结束
        List<cn.com.yusys.yusp.dto.server.xdzc0019.resp.List> lists = new ArrayList<cn.com.yusys.yusp.dto.server.xdzc0019.resp.List>();

        try {
            if(StringUtils.isEmpty(page)){
                page = "1";
            }
            if(StringUtils.isEmpty(size)){
                size = "10";
            }
            // 当前历史 0当前(池内资产清单)
            if("0".equals(queryType)) {
                //客户编号 是否查询全量数据
                if (StringUtils.nonEmpty(cusId)) {
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("cusId", cusId);//客户编号
                    queryModel.addCondition("assetType", assetType);//资产类型
                    queryModel.addCondition("assetStartDate", assetStartDate);//资产到期日起
                    queryModel.addCondition("assetEndDate", assetEndDate);//资产到期日止
                    queryModel.addCondition("assetCeiling", assetCeiling);//资产价值上限
                    queryModel.addCondition("assetFloor", assetFloor);//资产价值下限
                    queryModel.addCondition("isPool", "1");//入池
                    queryModel.addCondition("isPledge", "1");//质押

                    java.util.List<AsplAssetsList> asplAssetsLists = null;
                    PageHelper.startPage(Integer.parseInt(page), Integer.parseInt(size));
                    asplAssetsLists = asplAssetsListService.selectByModelDate(queryModel);
                    PageInfo<AsplAssetsList> pageinfo = new PageInfo<>(asplAssetsLists);
                    long count = pageinfo.getTotal();
                    PageHelper.clearPage();

                    xdzc0019DataRespDto.setTotalSize(Optional.ofNullable(String.valueOf(count)).orElse("0"));// 总记录数
                    lists = asplAssetsLists.parallelStream().map(e -> {
                        cn.com.yusys.yusp.dto.server.xdzc0019.resp.List object = new cn.com.yusys.yusp.dto.server.xdzc0019.resp.List();
                        object.setAssetType(e.getAssetType());// 资产类别
                        object.setAssetNo(e.getAssetNo());// 资产编号
                        object.setAssetAmt(e.getAssetValue());// 资产金额
                        object.setAssetEndDate(e.getAssetEndDate());// 资产到期日
                        object.setAssetStatus(e.getAssetStatus());// 资产状态
                        object.setTradeType(e.getIsPool());// 交易类型
                        return object;
                    }).collect(Collectors.toList());
                }
            }
            // 1历史（出入池记录）
            if("1".equals(queryType)){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("assetType",assetType);// 资产类型
                queryModel.addCondition("contNo",contNo);// 资产池协议编号
                queryModel.addCondition("cusId",cusId);// 客户编号
                queryModel.addCondition("opStartDate",opStartDate);// 操作日开始
                queryModel.addCondition("opEndDate",opEndDate);// 操作日结束
                PageHelper.startPage(Integer.parseInt(page), Integer.parseInt(size));
                List<AsplIoPoolDetails> asplIoPoolDetailsList = asplIoPoolDetailsService.selectBySelfModel(queryModel);
                PageInfo<AsplIoPoolDetails> pageinfo = new PageInfo<>(asplIoPoolDetailsList);
                long count = pageinfo.getTotal();
                PageHelper.clearPage();

                xdzc0019DataRespDto.setTotalSize(Optional.ofNullable(String.valueOf(count)).orElse("0"));// 总记录数
                lists = asplIoPoolDetailsList.parallelStream().map(e->{
                    cn.com.yusys.yusp.dto.server.xdzc0019.resp.List  object = new cn.com.yusys.yusp.dto.server.xdzc0019.resp.List();
                    object.setAssetType(e.getAssetType());// 资产类别
                    object.setAssetNo(e.getAssetNo());// 资产编号
                    object.setAssetAmt(e.getAssetValue());// 资产金额
                    object.setAssetEndDate(e.getAssetEndDate());// 资产到期日
                    object.setAssetStatus(e.getIsPledge());// 资产状态
                    object.setTradeType(e.getIsPool());// 交易类型
                    return object;
                }).collect(Collectors.toList());
            }
            xdzc0019DataRespDto.setList(lists);// 列表信息

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value);
        return xdzc0019DataRespDto;
    }
}

