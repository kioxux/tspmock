/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GrtStockDebtRel;
import cn.com.yusys.yusp.repository.mapper.GrtStockDebtRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtStockDebtRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-15 10:54:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GrtStockDebtRelService {

    @Autowired
    private GrtStockDebtRelMapper grtStockDebtRelMapper;

    @Autowired
    private CtrLoanContService ctrLoanContService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GrtStockDebtRel selectByPrimaryKey(String pkId, String guarContNo) {
        return grtStockDebtRelMapper.selectByPrimaryKey(pkId, guarContNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GrtStockDebtRel> selectAll(QueryModel model) {
        List<GrtStockDebtRel> records = grtStockDebtRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GrtStockDebtRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtStockDebtRel> list = grtStockDebtRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GrtStockDebtRel record) {
        return grtStockDebtRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GrtStockDebtRel record) {
        return grtStockDebtRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GrtStockDebtRel record) {
        return grtStockDebtRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GrtStockDebtRel record) {
        return grtStockDebtRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return grtStockDebtRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByModel
     * @方法描述: 根据model删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByModel(QueryModel model) {
        return grtStockDebtRelMapper.deleteByModel(model);
    }

    /**
     * @方法名称：tempSave
     * @方法描述：担保合同暂存/保存
     * @创建人：zfq
     * @创建时间：2021/6/1 10:23
     * @修改记录：修改时间 修改人员 修改时间
     */
    @Transactional
    public int tempSave(List<GrtStockDebtRel> grtStockDebtRels) {
        int result = 0;

        if (CollectionUtils.isEmpty(grtStockDebtRels)) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }

        for (GrtStockDebtRel grtStockDebtRel : grtStockDebtRels) {
            //担保合同编号
            String guarContNo = grtStockDebtRel.getGuarContNo();

            //合同编号
            String contNo = grtStockDebtRel.getContNo();

            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("guarContNo",guarContNo);
            queryModel.addCondition("contNo",contNo);
            queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

            List<GrtStockDebtRel> grtStockDebtRelList = grtStockDebtRelMapper.selectByModel(queryModel);

            if (CollectionUtils.isEmpty(grtStockDebtRelList)){
                //查询不到担保合同编号和合同编号对应的记录，则新增
                grtStockDebtRel.setOprType(CmisBizConstants.OPR_TYPE_01);
                result = grtStockDebtRelMapper.insert(grtStockDebtRel);
            }else {
                throw new BizException(null, "", null, "不能引入重复的合同编号！");
            }
        }

        return result;
    }

    /**
     * 根据担保合同编号查询关联的合同编号
     * @param guarContNo
     * @return
     */
    public String selectContNosByGuarContNo(String guarContNo){
        return grtStockDebtRelMapper.selectContNosByGuarContNo(guarContNo);
    }

    /**
     * 根据担保合同编号查询关联的合同信息
     * @param guarContNo
     * @return
     */
    public List<CtrLoanCont> selectByGuarContNo(String guarContNo){
        List<CtrLoanCont> ctrLoanConts = new ArrayList<>();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("guarContNo",guarContNo);
        queryModel.addCondition("oprType", CmisBizConstants.OPR_TYPE_01);

        //根据担保合同编号查询关联的合同编号
        String contNos = grtStockDebtRelMapper.selectContNosByGuarContNo(guarContNo);

        if (StringUtils.isNotEmpty(contNos)){
            ctrLoanConts = ctrLoanContService.selectByContNos(contNos);
        }

        return ctrLoanConts;
    }

    public static void main(String[] args) {
        BigDecimal evalAmt = new BigDecimal(114360000.00);
        BigDecimal correFinAmt = new BigDecimal(10000000.00);
        System.out.println(correFinAmt.divide(evalAmt,4,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP));
    }

}
