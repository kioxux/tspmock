package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisCommonConstants;

import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AsplIoPoolService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @className AsplIoPool
 * @Description 资产出池业务后处理
 * @Date 2020/12/21 : 10:43
 */
@Service
public class ZCYW01BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(ZCYW01BizService.class);
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private AsplIoPoolService asplIoPoolService;


    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String iqpSerno = resultInstanceDto.getBizId();
        try {
            // 获取当前业务申请路程类型
            String flowCode = resultInstanceDto.getBizType();
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程发起操作，流程参数" + resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "业务处理 正常下一步,不做任何操作" + resultInstanceDto);
                // 正常提交下一步处理   审批中 111
                asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程跳转操作，流程参数" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程结束操作，流程参数" + resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                //1.更新资产出池配置表的审批状态 由审批中111 -> 审批通过 997
                asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_997);
                asplIoPoolService.handleBusinessDataAfterEnd(iqpSerno);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程退回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为退回992
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程打回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_992);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "流程拿回操作，流程参数" + resultInstanceDto);
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为拿回
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "拿回初始节点操作，流程参数" + resultInstanceDto);
                //流程拿回到第一个节点，申请主表的业务
                asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("资产出池业务申请" + iqpSerno + "否决操作，流程参数" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                asplIoPoolService.updateStatusBySerno(iqpSerno,CmisCommonConstants.WF_STATUS_998);
            } else {
                log.info("资产出池业务申请" + iqpSerno + "未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
            // 资产出池业务 审批流程
        return "ZCYW01".equals(flowCode);
    }
}
