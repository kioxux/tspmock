package cn.com.yusys.yusp.service.server.xddh0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0012.req.Xddh0012DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisPspClientService;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdcz0012Service
 * @类描述: #服务类
 * @功能描述:优农贷贷后预警通知
 * @创建人: YX-Qianxin
 * @创建时间: 2021-06-09 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xddh0012Service {

    private static final Logger logger = LoggerFactory.getLogger(Xddh0012Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private AccLoanMapper accloanMapper;
    @Autowired
    private CmisPspClientService cmisPspClientService;

    /**
     * 生成小微平台贷后预警定期任务</br>
     *
     * @param xddh0012DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0011DataRespDto xddh0012(Xddh0012DataReqDto xddh0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value);
        Xddh0011DataRespDto xddh0011DataRespDto = new Xddh0011DataRespDto();
        try {
            String yw_date = xddh0012DataReqDto.getYw_date();//任务日期
            BigDecimal dqrw_num = xddh0012DataReqDto.getDqrw_num();//任务数量
            String doc_name = xddh0012DataReqDto.getDoc_name();//文件名称
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            String saveLocalftpPath = "/weblogic/webroot/download/dhyj_path";//小贷微银优企贷定期检查任务文件保存路径
            File file = new File(saveLocalftpPath + doc_name);
            int i = 0;
            ResultDto<Boolean> m = null;
            ResultDto<Boolean> n = null;
            String managerId = "";
            if (file.exists()) {
                /**** 执行异步操作 *******/
                logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, "优企贷" + yw_date + "贷后定期任务开始处理，任务文件" + doc_name);
                InputStreamReader read = new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8"));
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                String part = "1";

                //拼接序列号
                HashMap<String, String> param = new HashMap<>();
                //全局序列号业务类型 10-PC端
                param.put("biz", SeqConstant.YPSEQ_BIZ_10);
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    i++;
                    if ((lineTxt != null) && lineTxt.length() > 0) {
                        if (lineTxt.startsWith("---")) {//第二部分
                            part = "2";
                            continue;
                        }
                        String[] line = lineTxt.split("\\#");
                        if ("1".equals(part)) {

                        } else if ("2".equals(part)) {

                        }
                    }
                    logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, "优企贷" + yw_date + "贷后定期任务处理结束，任务文件" + doc_name);
                }
            } else {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            }
            if (null == m || null == n) {
                throw new RuntimeException("m或n未查询到数据！");
            }
            if (!m.getData() || !n.getData()) {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
                // 短信通知
                sendMsg(managerId);
            } else {
                // 给xddh0011DataRespDto赋值
                xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
                xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);
            }


        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, e.getMessage());
            xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, e.getMessage());
            xddh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xddh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value);
        return xddh0011DataRespDto;
    }


    /**
     * 发送短信
     *
     * @param managerId
     * @throws Exception
     */
    private void sendMsg(String managerId) throws Exception {
        String message = "【张家港农商银行】今日优企贷贷后定期任务文件处理失败，请及时处理";
        logger.info("发送的短信内容：" + message);

        try {
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
            ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
            AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());

            SenddxReqDto senddxReqDto = new SenddxReqDto();
            senddxReqDto.setInfopt("dx");
            SenddxReqList senddxReqList = new SenddxReqList();
            senddxReqList.setMobile(adminSmUserDto.getUserMobilephone());
            senddxReqList.setSmstxt(message);
            ArrayList<SenddxReqList> list = new ArrayList<>();
            list.add(senddxReqList);
            senddxReqDto.setSenddxReqList(list);

            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
            ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

            String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            SenddxRespDto senddxRespDto = null;
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
                //  获取相关的值并解析
                senddxRespDto = senddxResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(senddxCode, senddxMeesage);
            }
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
