/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.nio.file.Watchable;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AreaOrg;
import cn.com.yusys.yusp.service.AreaOrgService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaOrgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zlf
 * @创建时间: 2021-04-12 15:43:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "微贷区域机构关联")
@RequestMapping("/api/areaorg")
public class AreaOrgResource {
    @Autowired
    private AreaOrgService areaOrgService;
    private static final Logger log = LoggerFactory.getLogger(AreaOrgService.class);

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AreaOrg>> query() {
        QueryModel queryModel = new QueryModel();
        List<AreaOrg> list = areaOrgService.selectAll(queryModel);
        return new ResultDto<List<AreaOrg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AreaOrg>> index(QueryModel queryModel) {
        List<AreaOrg> list = areaOrgService.selectByModel(queryModel);
        return new ResultDto<List<AreaOrg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AreaOrg> show(@PathVariable("pkId") String pkId) {
        AreaOrg areaOrg = areaOrgService.selectByPrimaryKey(pkId);
        return new ResultDto<AreaOrg>(areaOrg);
    }



    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<AreaOrg> create(@RequestBody AreaOrg areaOrg) {
        log.info("add数据【{"+this.getClass().getName().toString()+"}】", JSONObject.toJSON(areaOrg.getAreaNo()));
        areaOrgService.insert(areaOrg);
        return new ResultDto<AreaOrg>(areaOrg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AreaOrg areaOrg) throws URISyntaxException {
        int result = areaOrgService.update(areaOrg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = areaOrgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @author zlf
     * @date 2021/4/26 14:06
     * @version 1.0.0
     * @desc 根据区域编号删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据区域编号删除")
    @PostMapping("/deletebyareano/{areaNo}")
    protected ResultDto<Map> deleteByAreaNo(@PathVariable("areaNo") String areaNo) {
        Map result = areaOrgService.deleteByAreaNo(areaNo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = areaOrgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /** 
     * @author zlf
     * @date 2021/4/26 13:51
     * @version 1.0.0
     * @desc 根据区域编号查询关联列表
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("根据区域编号查询关联列表")
    @PostMapping("/selectareano/{areaNo}")
    protected ResultDto<List<AreaOrg>> showByareano(@PathVariable("areaNo") String areaNo) {
        List<AreaOrg> list = areaOrgService.selectByAreaNo(areaNo);
        return new ResultDto<List<AreaOrg>>(list);
    }
    /**
    * @author zlf
    * @date 2021/6/16 14:12
    * @version 1.0.0
    * @desc     分页查询
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/queryall")
    protected ResultDto<List<AreaOrg>> queryAll(@RequestBody QueryModel queryModel) {
        List<AreaOrg> list = areaOrgService.selectByModel(queryModel);
        return new ResultDto<List<AreaOrg>>(list);
    }
    /**
    * @author zlf
    * @date 2021/6/16 14:12
    * @version 1.0.0
    * @desc     删除
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/deleteone")
    protected ResultDto<Integer> deleteOne(@RequestBody AreaOrg areaOrg) {
        int result = areaOrgService.deleteByPrimaryKey(areaOrg.getPkId());
        return new ResultDto<Integer>(result);
    }


}
