/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtSurveyReportDto;
import cn.com.yusys.yusp.dto.LmtPlListInfoReqDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtPlListInfo;
import cn.com.yusys.yusp.service.LmtPlListInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPlListInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-05-10 15:10:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "损益明细信息")
@RequestMapping("/api/lmtpllistinfo")
public class LmtPlListInfoResource {
    @Autowired
    private LmtPlListInfoService lmtPlListInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtPlListInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtPlListInfo> list = lmtPlListInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtPlListInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtPlListInfo>> index(QueryModel queryModel) {
        List<LmtPlListInfo> list = lmtPlListInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtPlListInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtPlListInfo> show(@PathVariable("pkId") String pkId) {
        LmtPlListInfo lmtPlListInfo = lmtPlListInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtPlListInfo>(lmtPlListInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtPlListInfo> create(@RequestBody LmtPlListInfo lmtPlListInfo) throws URISyntaxException {
        lmtPlListInfoService.insert(lmtPlListInfo);
        return new ResultDto<LmtPlListInfo>(lmtPlListInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtPlListInfo lmtPlListInfo) throws URISyntaxException {
        int result = lmtPlListInfoService.update(lmtPlListInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtPlListInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtPlListInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @author hubp
     * @date 2021/5/18 16:30
     * @version 1.0.0
     * @desc 通过调查流水号查询损益明细信息
     * @修改历史: 修改时间： 2021年6月15日16:28:01   修改人员： hubp   修改原因：  传参修改
     */
    @ApiOperation("通过调查流水号查询损益明细信息")
    @PostMapping("/selectbysurveyserno")
    protected ResultDto<LmtPlListInfoReqDto> selectBySurveySerno(@RequestBody LmtSurveyReportDto lmtSurveyReportDto) {
        return lmtPlListInfoService.selectBySurveySerno(lmtSurveyReportDto);
    }

    /**
     * @author hubp
     * @date 2021/5/18 19:45
     * @version 1.0.0
     * @desc 保存更新损益表相关信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("保存更新损益表相关信息")
    @PostMapping("/savedata")
    protected ResultDto<Integer> saveData(@RequestBody LmtPlListInfoReqDto lmtPlListInfoReqDto) {
        int result = lmtPlListInfoService.saveData(lmtPlListInfoReqDto);
        return new ResultDto<Integer>(result);
    }
}
