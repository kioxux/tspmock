/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.GuarGuaranteeDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarGuaranteeMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarGuaranteeService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-28 20:27:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarGuaranteeService {

    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;

    @Autowired
    private GuarBizRelService guarBizRelService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public GuarGuarantee selectByPrimaryKey(String guarantyId) {
        return guarGuaranteeMapper.selectByPrimaryKey(guarantyId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<GuarGuarantee> selectAll(QueryModel model) {
        List<GuarGuarantee> records = (List<GuarGuarantee>) guarGuaranteeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarGuarantee> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarGuarantee> list = guarGuaranteeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 根据担保合同编号查询关联的保证人信息
     * @param model
     * @return
     */
    public List<GuarGuarantee> queryListByGuarContNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarGuarantee> list = guarGuaranteeMapper.queryListByGuarContNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GuarGuarantee record) {
        return guarGuaranteeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GuarGuarantee record) {
        return guarGuaranteeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GuarGuarantee record) {
        return guarGuaranteeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GuarGuarantee record) {
        return guarGuaranteeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String guarantyId) {
        return guarGuaranteeMapper.deleteByPrimaryKey(guarantyId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarGuaranteeMapper.deleteByIds(ids);
    }

    public List<GuarBizRelGuaranteeDto> queryGuarInfoSell(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<GuarBizRelGuaranteeDto> GuarGuarantee = guarGuaranteeMapper.selectByIqpSernoModel(queryModel);
        PageHelper.clearPage();
        return GuarGuarantee;
    }

    public List<GuarBizRelGuaranteeDto> queryGuarInfoSellNoPage(QueryModel queryModel) {
        List<GuarBizRelGuaranteeDto> GuarGuarantee = guarGuaranteeMapper.selectByIqpSernoModel(queryModel);
        return GuarGuarantee;
    }

    /**
     * @param serno
     * @return List
     * @author 王祝远
     * @date 2021/4/23 16:42
     * @version 1.0.0
     * @desc 根据业务流水号删除担保人信息
     * @修改历史: V1.0
     */
    public int deleteGuaranteefoSell(String serno) {
        int result;
        result = this.deleteByPrimaryKey(serno);
        result = guarBizRelService.deleteByPrimaryKey(serno);
        return result;
    }

    /**
     * @函数名称:selectGuarGuaranteeByGuarContNo
     * @函数描述:根据担保合同编号获取保证人信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<GuarGuarantee> selectGuarGuaranteeByGuarContNo(QueryModel queryModel) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.selectGuarGuaranteeByGuarContNo(queryModel);
        return guarGuaranteeList;
    }

    /**
     * 根据授信分项编号查询担保合同关联的保证人信息
     * @param queryModel
     * @return
     */
    public String selectGuarantyIdsByLmtAccNo(QueryModel queryModel){
        return guarGuaranteeMapper.selectGuarantyIdsByLmtAccNo(queryModel);
    }

    /**
     * @方法名称: queryGuarGuaranteeSignDateByMap
     * @方法描述: 根据客户号和担保合同编号去查询签约日期
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String queryGuarGuaranteeSignDateByMap(Map queryMap) {
        return guarGuaranteeMapper.queryGuarGuaranteeSignDateByMap(queryMap);
    }

    /**
     * @return
     * @方法名称: selectlListByDbContNo
     * @方法描述: 根据担保合同号查询cusid_list、 cusname_list
     * @参数与返回说明:
     * @算法描述: 无
     */
    public HashMap<String, Object> selectlListByDbContNo(String DbContNo) {
        return guarGuaranteeMapper.selectlListByDbContNo(DbContNo);
    }

    /**
     * @方法名称: selectCountByContNo
     * @方法描述: 根据合同号查询担保合同未签约的保证人数量
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectCountByContNo(String contNo) {
        return guarGuaranteeMapper.selectCountByContNo(contNo);
    }

    /**
     * @方法名称: updateContStatusByContNo
     * @方法描述: 根据合同号更新借款合同或担保合同签约状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateContStatusByContNo(Map queryMap) {
        return guarGuaranteeMapper.updateContStatusByContNo(queryMap);
    }

    /**
     * @函数名称:queryGuarGuaranteeIsUnderLmt
     * @函数描述:根据业务流水号查询授信项下的押品保证人信息
     * @算法描述:
     * @参数与返回说明:
     */
    public List<GuarGuarantee> queryGuarGuaranteeIsUnderLmt(QueryModel queryModel) {
        //授信额度编号
        String lmtAccNo = (String) queryModel.getCondition().get("lmtAccNo");
        //授信分项流水号
        String subSerno = lmtReplyAccSubPrdService.getLmtReplyAccSubDataByAccSubPrdNo(lmtAccNo);

        if (StringUtils.isEmpty(subSerno)){
            Map map = new HashMap();
            map.put("accSubNo",lmtAccNo);
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
            subSerno = lmtReplyAccSub.getSubSerno();
        }
        QueryModel model = new QueryModel();
        model.addCondition("serno",subSerno);

        List<GuarGuarantee> list = guarGuaranteeMapper.queryGuarGuaranteeIsUnderLmt(model);
        return list;
    }

    /**
     * 根据业务流水号查询担保人信息
     *
     * @param serno
     * @return
     * @author lyj
     */
    public List<Map<String, Object>> queryGuarGuaranteeBySerno(String serno) {
        if (null == serno) {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        QueryModel model = new QueryModel();
        model.addCondition("serno", serno);
        model.addCondition("oprType", CommonConstance.OPR_TYPE_ADD);
        List<Map<String, Object>> result = new ArrayList<>();
        List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByModel(model);
        if (null != lmtAppSubList && lmtAppSubList.size() > 0) {
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                BigDecimal amt = lmtAppSub.getLmtAmt();
                if(Objects.isNull(amt)||amt.compareTo(BigDecimal.ZERO) ==0){
                    continue;
                }
                String isPreLmt = lmtAppSub.getIsPreLmt();
                if(StringUtils.nonBlank(isPreLmt)&&isPreLmt.equals(CmisBizConstants.STD_ZB_YES_NO_Y)){
                    continue;
                }
                String subSerno = lmtAppSub.getSubSerno();
                String guarMode = lmtAppSub.getGuarMode();
                List<Map<String, Object>> list = guarGuaranteeMapper.queryGuarGuaranteeBySubSerno(subSerno);
                if (null != list && list.size() > 0) {
                    for (Map<String, Object> temp : list) {
                        Map<String, Object> map = new HashMap<>();
                        map = temp;
                        map.put("subSerno", subSerno);
                        map.put("guarMode", guarMode);
                        result.add(map);
                    }
                }else{
                    Map<String, Object> map = new HashMap<>();
                    map.put("subSerno", subSerno);
                    map.put("guarMode", guarMode);
                    result.add(map);
                }
            }
        }
        return result;
    }

    /**
     * 根据申请流水号查询保证人信息
     *
     * @param serno
     * @return
     */
    public ArrayList<GuarGuarantee> queryGuaranteeInfo(String serno) {
        return guarGuaranteeMapper.queryGuaranteeInfo(serno);
    }

    /**
     * 根据流水号查询授信分项下保证人
     *
     * @param serno
     * @return
     */
    public List<GuarGuarantee> getGuarGuaranteeBySerno(String serno) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.getGuarGuaranteeBySerno(serno);
        if (null != guarGuaranteeList && guarGuaranteeList.size() > 0) {
            return guarGuaranteeList;
        } else {
            return null;
        }
    }

    /**
     * 根据流水号查询授信分项下主担保的保证人
     *
     * @param serno
     * @return
     */
    public List<GuarGuarantee> getMainGuarGuaranteeBySerno(String serno,String isAddGuar) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.getMainGuarGuaranteeBySerno(serno,isAddGuar);
        if (null != guarGuaranteeList && guarGuaranteeList.size() > 0) {
            return guarGuaranteeList;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * 根据流水号查询授信分项下保证人
     *
     * @param serno
     * @return
     */
    public List<GuarGuarantee> queryGuarGuaranteeDataBySerno(String serno) {
        List<GuarGuarantee> guarGuaranteeList = guarGuaranteeMapper.queryGuarGuaranteeDataBySerno(serno);
        if (null != guarGuaranteeList && guarGuaranteeList.size() > 0) {
            return guarGuaranteeList;
        } else {
            return null;
        }
    }

    /**
     * @方法名称: queryGuarGuaranteeByGuarContNo
     * @方法描述: 根据担保合同编号关联查询保证人信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarGuaranteeDto> queryGuarGuaranteeByGuarContNo(String guarContNo) {
        return guarGuaranteeMapper.queryGuarGuaranteeByGuarContNo(guarContNo);
    }

    /**
     * @param map
     * @return List
     * @author css
     * @desc 审查报告查询保证人列表
     * @修改历史: V1.0
     */

    public List<Map> querGuaranteeRelByLmtAppSerno(Map map) {
        return guarGuaranteeMapper.querGuaranteeRelByLmtAppSerno(map);
    }
}
