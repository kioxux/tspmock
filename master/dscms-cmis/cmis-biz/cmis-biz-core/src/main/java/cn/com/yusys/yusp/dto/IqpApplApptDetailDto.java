package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @className IqpApplApptDetailDto
 * @Author xianglei
 * @Description 申请人详细信息DTO
 * @Date 2020/12/10 : 9:41
 */

public class IqpApplApptDetailDto implements Serializable {
    private String iqpSerno;
    private String 	cusName	;
    private String 	perCustName	;
    private String 	certType	;
    private String 	certCode	;
    private String 	enName	;
    private String 	issuDep	;
    private String 	issuStartDt	;
    private String 	issuEndDt	;
    private String 	sex	;
    private String 	cusId	;
    private String 	cnty	;
    private String 	area	;
    private String 	permanentAreaName	;
    private String 	permanentAddr	;
    private String 	marrSts	;
    private String 	edu	;
    private String 	degree	;
    private String 	isSelfEmploy	;
    private String 	isHousehold	;
    private String 	pfn	;
    private String 	dut	;
    private String 	proTitle	;
    private String 	type	;
    private String 	workPlaceName	;
    private String 	addrArearName	;
    private String 	addrDetail	;
    private String 	kindName	;
    private String 	tel	;
    private String 	dep	;
    private String 	comeinDt	;
    private String 	companyName	;
    private String 	unifyCreditCode	;
    private String 	companyAreaName	;
    private String 	companyAddr	;
    private String 	companyType	;
    private String 	companyScale	;
    private String 	foundYear	;
    private String 	livingAreaName	;
    private String 	livingAddr	;
    private String 	livingSts	;
    private String 	postSendAddrKind	;
    private String 	postSendAreaName	;
    private String 	postSendAddr	;
    private String 	postCode	;
    private String 	tel1	;
    private String 	backTel	;
    private String 	mail	;
    private String 	livingArea	;
    private String 	companyArea	;
    private String 	addrArear	;
    private String 	permanentArea	;
    private String 	postSendArea	;
    private String 	kind	;
    private String 	apptCode	;
    private String  relType;

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPerCustName() {
        return perCustName;
    }

    public void setPerCustName(String perCustName) {
        this.perCustName = perCustName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getIssuDep() {
        return issuDep;
    }

    public void setIssuDep(String issuDep) {
        this.issuDep = issuDep;
    }

    public String getIssuStartDt() {
        return issuStartDt;
    }

    public void setIssuStartDt(String issuStartDt) {
        this.issuStartDt = issuStartDt;
    }

    public String getIssuEndDt() {
        return issuEndDt;
    }

    public void setIssuEndDt(String issuEndDt) {
        this.issuEndDt = issuEndDt;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCnty() {
        return cnty;
    }

    public void setCnty(String cnty) {
        this.cnty = cnty;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPermanentAreaName() {
        return permanentAreaName;
    }

    public void setPermanentAreaName(String permanentAreaName) {
        this.permanentAreaName = permanentAreaName;
    }

    public String getPermanentAddr() {
        return permanentAddr;
    }

    public void setPermanentAddr(String permanentAddr) {
        this.permanentAddr = permanentAddr;
    }

    public String getMarrSts() {
        return marrSts;
    }

    public void setMarrSts(String marrSts) {
        this.marrSts = marrSts;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getIsSelfEmploy() {
        return isSelfEmploy;
    }

    public void setIsSelfEmploy(String isSelfEmploy) {
        this.isSelfEmploy = isSelfEmploy;
    }

    public String getIsHousehold() {
        return isHousehold;
    }

    public void setIsHousehold(String isHousehold) {
        this.isHousehold = isHousehold;
    }

    public String getPfn() {
        return pfn;
    }

    public void setPfn(String pfn) {
        this.pfn = pfn;
    }

    public String getDut() {
        return dut;
    }

    public void setDut(String dut) {
        this.dut = dut;
    }

    public String getProTitle() {
        return proTitle;
    }

    public void setProTitle(String proTitle) {
        this.proTitle = proTitle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWorkPlaceName() {
        return workPlaceName;
    }

    public void setWorkPlaceName(String workPlaceName) {
        this.workPlaceName = workPlaceName;
    }

    public String getAddrArearName() {
        return addrArearName;
    }

    public void setAddrArearName(String addrArearName) {
        this.addrArearName = addrArearName;
    }

    public String getAddrDetail() {
        return addrDetail;
    }

    public void setAddrDetail(String addrDetail) {
        this.addrDetail = addrDetail;
    }

    public String getKindName() {
        return kindName;
    }

    public void setKindName(String kindName) {
        this.kindName = kindName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getComeinDt() {
        return comeinDt;
    }

    public void setComeinDt(String comeinDt) {
        this.comeinDt = comeinDt;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUnifyCreditCode() {
        return unifyCreditCode;
    }

    public void setUnifyCreditCode(String unifyCreditCode) {
        this.unifyCreditCode = unifyCreditCode;
    }

    public String getCompanyAreaName() {
        return companyAreaName;
    }

    public void setCompanyAreaName(String companyAreaName) {
        this.companyAreaName = companyAreaName;
    }

    public String getCompanyAddr() {
        return companyAddr;
    }

    public void setCompanyAddr(String companyAddr) {
        this.companyAddr = companyAddr;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyScale() {
        return companyScale;
    }

    public void setCompanyScale(String companyScale) {
        this.companyScale = companyScale;
    }

    public String getFoundYear() {
        return foundYear;
    }

    public void setFoundYear(String foundYear) {
        this.foundYear = foundYear;
    }

    public String getLivingAreaName() {
        return livingAreaName;
    }

    public void setLivingAreaName(String livingAreaName) {
        this.livingAreaName = livingAreaName;
    }

    public String getLivingAddr() {
        return livingAddr;
    }

    public void setLivingAddr(String livingAddr) {
        this.livingAddr = livingAddr;
    }

    public String getLivingSts() {
        return livingSts;
    }

    public void setLivingSts(String livingSts) {
        this.livingSts = livingSts;
    }

    public String getPostSendAddrKind() {
        return postSendAddrKind;
    }

    public void setPostSendAddrKind(String postSendAddrKind) {
        this.postSendAddrKind = postSendAddrKind;
    }

    public String getPostSendAreaName() {
        return postSendAreaName;
    }

    public void setPostSendAreaName(String postSendAreaName) {
        this.postSendAreaName = postSendAreaName;
    }

    public String getPostSendAddr() {
        return postSendAddr;
    }

    public void setPostSendAddr(String postSendAddr) {
        this.postSendAddr = postSendAddr;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getBackTel() {
        return backTel;
    }

    public void setBackTel(String backTel) {
        this.backTel = backTel;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLivingArea() {
        return livingArea;
    }

    public void setLivingArea(String livingArea) {
        this.livingArea = livingArea;
    }

    public String getCompanyArea() {
        return companyArea;
    }

    public void setCompanyArea(String companyArea) {
        this.companyArea = companyArea;
    }

    public String getAddrArear() {
        return addrArear;
    }

    public void setAddrArear(String addrArear) {
        this.addrArear = addrArear;
    }

    public String getPermanentArea() {
        return permanentArea;
    }

    public void setPermanentArea(String permanentArea) {
        this.permanentArea = permanentArea;
    }

    public String getPostSendArea() {
        return postSendArea;
    }

    public void setPostSendArea(String postSendArea) {
        this.postSendArea = postSendArea;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getApptCode() {
        return apptCode;
    }

    public void setApptCode(String apptCode) {
        this.apptCode = apptCode;
    }
}
