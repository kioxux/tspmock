/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaManager
 * @类描述: area_manager数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-05 10:12:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "area_manager")
public class AreaManager extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 区域编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "AREA_NO")
	private String areaNo;
	
	/** 区域名称 **/
	@Column(name = "AREA_NAME", unique = false, nullable = false, length = 80)
	private String areaName;
	
	/** 是否启用 **/
	@Column(name = "IS_BEGIN", unique = false, nullable = false, length = 1)
	private String isBegin;
	
	/** 签约方式 **/
	@Column(name = "SIGN_MODE", unique = false, nullable = true, length = 10)
	private String signMode;
	
	/** 备注 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 500)
	private String memo;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 所属分部 **/
	@Column(name = "SUB_BRANCH", unique = false, nullable = true, length = 20)
	private String subBranch;
	
	
	/**
	 * @param areaNo
	 */
	public void setAreaNo(String areaNo) {
		this.areaNo = areaNo;
	}
	
    /**
     * @return areaNo
     */
	public String getAreaNo() {
		return this.areaNo;
	}
	
	/**
	 * @param areaName
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
    /**
     * @return areaName
     */
	public String getAreaName() {
		return this.areaName;
	}
	
	/**
	 * @param isBegin
	 */
	public void setIsBegin(String isBegin) {
		this.isBegin = isBegin;
	}
	
    /**
     * @return isBegin
     */
	public String getIsBegin() {
		return this.isBegin;
	}
	
	/**
	 * @param signMode
	 */
	public void setSignMode(String signMode) {
		this.signMode = signMode;
	}
	
    /**
     * @return signMode
     */
	public String getSignMode() {
		return this.signMode;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param subBranch
	 */
	public void setSubBranch(String subBranch) {
		this.subBranch = subBranch;
	}
	
    /**
     * @return subBranch
     */
	public String getSubBranch() {
		return this.subBranch;
	}


}