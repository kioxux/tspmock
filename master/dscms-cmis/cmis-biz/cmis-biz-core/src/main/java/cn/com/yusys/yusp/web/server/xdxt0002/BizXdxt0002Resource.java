package cn.com.yusys.yusp.web.server.xdxt0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdxt0002.Xdxt0002Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.dto.server.xdxt0002.req.Xdxt0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.Xdxt0002DataRespDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据直营团队类型查询客户经理工号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0002:根据直营团队类型查询客户经理工号")
@RestController
@RequestMapping("/api/bizxt4bsp")
public class BizXdxt0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxt0002Resource.class);

    @Autowired
    private Xdxt0002Service xdxt0002Service;
    /**
     * 交易码：xdxt0002
     * 交易描述：根据直营团队类型查询客户经理工号
     *
     * @param xdxt0002DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据直营团队类型查询客户经理工号")
    @PostMapping("/xdxt0002")
    protected @ResponseBody
    ResultDto<Xdxt0002DataRespDto> xdxt0002(@Validated @RequestBody Xdxt0002DataReqDto xdxt0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataReqDto));
        Xdxt0002DataRespDto xdxt0002DataRespDto = new Xdxt0002DataRespDto();// 响应Dto:根据直营团队类型查询客户经理工号
        ResultDto<Xdxt0002DataRespDto> xdxt0002DataResultDto = new ResultDto<>();
        try {
            // 从xdxt0002DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataReqDto));
            xdxt0002DataRespDto = xdxt0002Service.getXdxt0002(xdxt0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataRespDto));
            // 封装xdxt0002DataResultDto中正确的返回码和返回信息
            xdxt0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxt0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, e.getMessage());
            // 封装xdxt0002DataResultDto中异常返回码和返回信息
            xdxt0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxt0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxt0002DataRespDto到xdxt0002DataResultDto中
        xdxt0002DataResultDto.setData(xdxt0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataResultDto));
        return xdxt0002DataResultDto;
    }
}
