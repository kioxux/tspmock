/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.GuarMortgageManageRelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarMortgageManageRel;
import cn.com.yusys.yusp.repository.mapper.GuarMortgageManageRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarMortgageManageRelService {

    @Autowired
    private GuarMortgageManageRelMapper guarMortgageManageRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarMortgageManageRel selectByPrimaryKey(String pkId) {
        return guarMortgageManageRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarMortgageManageRel> selectAll(QueryModel model) {
        List<GuarMortgageManageRel> records = (List<GuarMortgageManageRel>) guarMortgageManageRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarMortgageManageRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarMortgageManageRel> list = guarMortgageManageRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarMortgageManageRel record) {
        return guarMortgageManageRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarMortgageManageRel record) {
        return guarMortgageManageRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarMortgageManageRel record) {
        return guarMortgageManageRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarMortgageManageRel record) {
        return guarMortgageManageRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return guarMortgageManageRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarMortgageManageRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称:queryListBySerno
     * @方法描述:根据流水号查询列表
     * @param serno
     * @return
     */
    public List<GuarMortgageManageRelDto> queryListBySerno(String serno){
        List<GuarMortgageManageRelDto> guarMortgageManageRelDtos = guarMortgageManageRelMapper.queryListBySerno(serno);
        return guarMortgageManageRelDtos;
    }

    /**
     * @方法名称:deleteBySerno
     * @方法描述:根据流水号删除
     * @param serno
     * @return
     */
    public int deleteBySerno(String serno) {
        return guarMortgageManageRelMapper.deleteBySerno(serno);
    }

    /**
     * 根据抵押注销流水号查询权证出库核心担保编号
     * @param serno
     * @return
     */
    public String selectCoreGuarantyNoBySerno(String serno) {
        return guarMortgageManageRelMapper.selectCoreGuarantyNoBySerno(serno);
    }
}
