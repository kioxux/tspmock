package cn.com.yusys.yusp.web.server.xdtz0019;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xdtz0019.Xdtz0019Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdtz0019.req.Xdtz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.resp.Xdtz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0019:台账入账")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0019Resource.class);

    @Autowired
    private Xdtz0019Service xdtz0019Service;

    /**
     * 交易码：xdtz0019
     * 交易描述：台账入账
     *
     * @param xdtz0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账")
    @PostMapping("/xdtz0019")
    protected @ResponseBody
    ResultDto<Xdtz0019DataRespDto> xdtz0019(@Validated @RequestBody Xdtz0019DataReqDto xdtz0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019DataReqDto));
        Xdtz0019DataRespDto xdtz0019DataRespDto = new Xdtz0019DataRespDto();// 响应Dto:台账入账
        ResultDto<Xdtz0019DataRespDto> xdtz0019DataResultDto = new ResultDto<>();
        String bizType = xdtz0019DataReqDto.getBizType();//业务类型
        String oprtype = xdtz0019DataReqDto.getOprtype();//操作类型
        String billNo = xdtz0019DataReqDto.getBillNo();//借据号
        String contNo = xdtz0019DataReqDto.getContNo();//合同编号
        String bizVariet = xdtz0019DataReqDto.getBizVariet();//业务品种
        String cusId = xdtz0019DataReqDto.getCusId();//客户号
        String cusName = xdtz0019DataReqDto.getCusName();//客户名称
        String creditIdate = xdtz0019DataReqDto.getCreditIdate();//信用证有效期
        String creditNo = xdtz0019DataReqDto.getCreditNo();//信用证号码
        String issuingCurType = xdtz0019DataReqDto.getIssuingCurType();//开证币种
        String issuingContAmt = xdtz0019DataReqDto.getIssuingContAmt();//开证合同金额
        String billAmt = xdtz0019DataReqDto.getBillAmt();//借据金额
        String overdueDays = xdtz0019DataReqDto.getOverdueDays();//逾期天数
        String floodactPerc = xdtz0019DataReqDto.getFloodactPerc();//溢装比例
        String exchgRate = xdtz0019DataReqDto.getExchgRate();//汇率
        String assureMeans = xdtz0019DataReqDto.getAssureMeans();//担保方式
        String accStatus = xdtz0019DataReqDto.getAccStatus();//台账状态
        String inputId = xdtz0019DataReqDto.getInputId();//登记人
        String finaBrId = xdtz0019DataReqDto.getFinaBrId();//账务机构
        String managerId = xdtz0019DataReqDto.getManagerId();//责任人
        String managerBrId = xdtz0019DataReqDto.getManagerBrId();//责任机构
        try {
            // 从xdtz0019DataReqDto获取业务值进行业务逻辑处理
            // 调用XXXXXService层开始
            xdtz0019DataRespDto = xdtz0019Service.xdtz0019(xdtz0019DataReqDto);
            // 调用XXXXXService层结束
            // 封装xdtz0019DataResultDto中正确的返回码和返回信息
            xdtz0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, e.getMessage());
            // 封装xdtz0019DataResultDto中异常返回码和返回信息
            xdtz0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0019DataRespDto到xdtz0019DataResultDto中
        xdtz0019DataResultDto.setData(xdtz0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019DataResultDto));
        return xdtz0019DataResultDto;
    }
}
