/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAdjustDetail;
import cn.com.yusys.yusp.service.LmtAdjustDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAdjustDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-09 23:10:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtadjustdetail")
public class LmtAdjustDetailResource {
    @Autowired
    private LmtAdjustDetailService lmtAdjustDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAdjustDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAdjustDetail> list = lmtAdjustDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtAdjustDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAdjustDetail>> index(QueryModel queryModel) {
        List<LmtAdjustDetail> list = lmtAdjustDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtAdjustDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAdjustDetail> show(@PathVariable("pkId") String pkId) {
        LmtAdjustDetail lmtAdjustDetail = lmtAdjustDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAdjustDetail>(lmtAdjustDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAdjustDetail> create(@RequestBody LmtAdjustDetail lmtAdjustDetail) throws URISyntaxException {
        lmtAdjustDetailService.insert(lmtAdjustDetail);
        return new ResultDto<LmtAdjustDetail>(lmtAdjustDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAdjustDetail lmtAdjustDetail) throws URISyntaxException {
        int result = lmtAdjustDetailService.update(lmtAdjustDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAdjustDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAdjustDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByLmtSerno
     * @函数描述:查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbylmtserno")
    protected ResultDto<LmtAdjustDetail> selectByLmtSerno(@RequestBody Map map) {
        LmtAdjustDetail result = lmtAdjustDetailService.selectByLmtSerno((String) map.get("lmtSerno"));
        return new ResultDto<LmtAdjustDetail>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:有则更新,无则插入
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody LmtAdjustDetail lmtAdjustDetail) {
        LmtAdjustDetail lmtAdjustDetail1 = lmtAdjustDetailService.selectByLmtSerno(lmtAdjustDetail.getLmtSerno());
        int result = 0;
        if(lmtAdjustDetail1 != null){
            result = lmtAdjustDetailService.update(lmtAdjustDetail);
        }else{
            lmtAdjustDetail.setPkId(UUID.randomUUID().toString());
            User userInfo = SessionUtils.getUserInformation();
            lmtAdjustDetail.setOprType(CmisCommonConstants.OP_TYPE_01);
            lmtAdjustDetail.setInputId(userInfo.getLoginCode());
            lmtAdjustDetail.setInputBrId(userInfo.getOrg().getCode());
            lmtAdjustDetail.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            lmtAdjustDetail.setUpdId(userInfo.getLoginCode());
            lmtAdjustDetail.setUpdBrId(userInfo.getOrg().getCode());
            lmtAdjustDetail.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            lmtAdjustDetail.setCreateTime(new Date());
            lmtAdjustDetail.setUpdateTime(new Date());
            result = lmtAdjustDetailService.insertSelective(lmtAdjustDetail);
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetailByLmtSerno")
    protected ResultDto<List<Map<String,String>>> queryDetailByLmtSerno(@RequestBody Map<String,String> map) {
        List<Map<String,String>> lmtChgDetails = lmtAdjustDetailService.queryDetailByLmtSerno(map.get("lmtSerno"));
        return  ResultDto.success(lmtChgDetails);
    }
}
