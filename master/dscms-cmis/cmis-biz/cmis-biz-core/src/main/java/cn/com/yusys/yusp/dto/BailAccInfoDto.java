package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BailAccInfo
 * @类描述: bail_acc_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-30 21:23:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BailAccInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 流水号 **/
	private String serno;
	
	/** 账户类型 **/
	private String accountType;
	
	/** 保证金账号 **/
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	private String bailAccNoSub;
	
	/** 保证金账号名称 **/
	private String bailAccName;
	
	/** 保证金开户行号 **/
	private String acctsvcrNo;
	
	/** 保证金开户行名称 **/
	private String acctsvcrName;
	
	/** 保证金币种 **/
	private String bailCurType;
	
	/** 保证金金额 **/
	private java.math.BigDecimal bailAmt;
	
	/** 保证金比例 **/
	private java.math.BigDecimal bailRate;
	
	/** 保证金计息方式 **/
	private String bailInterestMode;
	
	/** 母户序号 **/
	private String firstAccount;
	
	/** 结算账号 **/
	private String settlAccno;
	
	/** 结算户名 **/
	private String settlAccname;
	
	/** 结算账号子序号 **/
	private String settlAccnoSub;
	
	/** 待清算账号 **/
	private String clearAccno;
	
	/** 待清算账户名 **/
	private String clearAccname;

	/** 待清算账户子序号 **/
	private String clearAccnoSub;
	
	/** 客户号 **/
	private String cusId;
	
	/** 利率 **/
	private String rate;
	
	/** 支付方式 **/
	private String zhfutojn;

	/** 账户类型 **/
	private String accountAmount;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	public String getAccountAmount() {
		return accountAmount;
	}

	public void setAccountAmount(String accountAmount) {
		this.accountAmount = accountAmount;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param accountType
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType == null ? null : accountType.trim();
	}
	
    /**
     * @return AccountType
     */	
	public String getAccountType() {
		return this.accountType;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo == null ? null : bailAccNo.trim();
	}
	
    /**
     * @return BailAccNo
     */	
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSub
	 */
	public void setBailAccNoSub(String bailAccNoSub) {
		this.bailAccNoSub = bailAccNoSub == null ? null : bailAccNoSub.trim();
	}
	
    /**
     * @return BailAccNoSub
     */	
	public String getBailAccNoSub() {
		return this.bailAccNoSub;
	}
	
	/**
	 * @param bailAccName
	 */
	public void setBailAccName(String bailAccName) {
		this.bailAccName = bailAccName == null ? null : bailAccName.trim();
	}
	
    /**
     * @return BailAccName
     */	
	public String getBailAccName() {
		return this.bailAccName;
	}
	
	/**
	 * @param acctsvcrNo
	 */
	public void setAcctsvcrNo(String acctsvcrNo) {
		this.acctsvcrNo = acctsvcrNo == null ? null : acctsvcrNo.trim();
	}
	
    /**
     * @return AcctsvcrNo
     */	
	public String getAcctsvcrNo() {
		return this.acctsvcrNo;
	}
	
	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName == null ? null : acctsvcrName.trim();
	}
	
    /**
     * @return AcctsvcrName
     */	
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType == null ? null : bailCurType.trim();
	}
	
    /**
     * @return BailCurType
     */	
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return BailAmt
     */	
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param bailRate
	 */
	public void setBailRate(java.math.BigDecimal bailRate) {
		this.bailRate = bailRate;
	}
	
    /**
     * @return BailRate
     */	
	public java.math.BigDecimal getBailRate() {
		return this.bailRate;
	}
	
	/**
	 * @param bailInterestMode
	 */
	public void setBailInterestMode(String bailInterestMode) {
		this.bailInterestMode = bailInterestMode == null ? null : bailInterestMode.trim();
	}
	
    /**
     * @return BailInterestMode
     */	
	public String getBailInterestMode() {
		return this.bailInterestMode;
	}
	
	/**
	 * @param firstAccount
	 */
	public void setFirstAccount(String firstAccount) {
		this.firstAccount = firstAccount == null ? null : firstAccount.trim();
	}
	
    /**
     * @return FirstAccount
     */	
	public String getFirstAccount() {
		return this.firstAccount;
	}
	
	/**
	 * @param settlAccno
	 */
	public void setSettlAccno(String settlAccno) {
		this.settlAccno = settlAccno == null ? null : settlAccno.trim();
	}
	
    /**
     * @return SettlAccno
     */	
	public String getSettlAccno() {
		return this.settlAccno;
	}
	
	/**
	 * @param settlAccname
	 */
	public void setSettlAccname(String settlAccname) {
		this.settlAccname = settlAccname == null ? null : settlAccname.trim();
	}
	
    /**
     * @return SettlAccname
     */	
	public String getSettlAccname() {
		return this.settlAccname;
	}
	
	/**
	 * @param settlAccnoSub
	 */
	public void setSettlAccnoSub(String settlAccnoSub) {
		this.settlAccnoSub = settlAccnoSub == null ? null : settlAccnoSub.trim();
	}
	
    /**
     * @return SettlAccnoSub
     */	
	public String getSettlAccnoSub() {
		return this.settlAccnoSub;
	}
	
	/**
	 * @param clearAccno
	 */
	public void setClearAccno(String clearAccno) {
		this.clearAccno = clearAccno == null ? null : clearAccno.trim();
	}
	
    /**
     * @return ClearAccno
     */	
	public String getClearAccno() {
		return this.clearAccno;
	}
	
	/**
	 * @param clearAccname
	 */
	public void setClearAccname(String clearAccname) {
		this.clearAccname = clearAccname == null ? null : clearAccname.trim();
	}
	
    /**
     * @return ClearAccname
     */	
	public String getClearAccname() {
		return this.clearAccname;
	}

	/**
	 * @param clearAccnoSub
	 */
	public void setClearAccnoSub(String clearAccnoSub) {
		this.clearAccnoSub = clearAccnoSub == null ? null : clearAccnoSub.trim();
	}

	/**
	 * @return ClearAccnoSub
	 */
	public String getClearAccnoSub() {
		return this.clearAccnoSub;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param rate
	 */
	public void setRate(String rate) {
		this.rate = rate == null ? null : rate.trim();
	}
	
    /**
     * @return Rate
     */	
	public String getRate() {
		return this.rate;
	}
	
	/**
	 * @param zhfutojn
	 */
	public void setZhfutojn(String zhfutojn) {
		this.zhfutojn = zhfutojn == null ? null : zhfutojn.trim();
	}
	
    /**
     * @return Zhfutojn
     */	
	public String getZhfutojn() {
		return this.zhfutojn;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}