package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpBillAcctChgRel
 * @类描述: iqp_bill_acct_chg_rel数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-01-25 09:09:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpBillAcctChgRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 账户属性 STD_ZB_BR_ID_ATTR **/
	private String acctAttr;
	
	/** 账号归属 **/
	private String acctBelong;
	
	/** 账号分类 **/
	private String acctClass;
	
	/** 账号 **/
	private String acctNo;
	
	/** 账号名称 **/
	private String acctName;
	
	/** 开户行行号 **/
	private String opanOrgNo;
	
	/** 开户行行名 **/
	private String opanOrgName;
	
	/** 机构编号 **/
	private String orgNo;
	
	/** 机构名称 **/
	private String orgName;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 支付金额 **/
	private java.math.BigDecimal payAmt;
	
	/** 支付用途 **/
	private String payUse;
	
	/** 账号状态 STD_ZB_PVP_ACCT_ST **/
	private String acctStatus;
	
	/** 操作类型 **/
	private String oprType;

	/** 账号变更标识  STD_ACCT_CHG_FLAG **/
	private String acctChgFlag;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param acctAttr
	 */
	public void setAcctAttr(String acctAttr) {
		this.acctAttr = acctAttr == null ? null : acctAttr.trim();
	}
	
    /**
     * @return AcctAttr
     */	
	public String getAcctAttr() {
		return this.acctAttr;
	}
	
	/**
	 * @param acctBelong
	 */
	public void setAcctBelong(String acctBelong) {
		this.acctBelong = acctBelong == null ? null : acctBelong.trim();
	}
	
    /**
     * @return AcctBelong
     */	
	public String getAcctBelong() {
		return this.acctBelong;
	}
	
	/**
	 * @param acctClass
	 */
	public void setAcctClass(String acctClass) {
		this.acctClass = acctClass == null ? null : acctClass.trim();
	}
	
    /**
     * @return AcctClass
     */	
	public String getAcctClass() {
		return this.acctClass;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo == null ? null : acctNo.trim();
	}
	
    /**
     * @return AcctNo
     */	
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}
	
    /**
     * @return AcctName
     */	
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param opanOrgNo
	 */
	public void setOpanOrgNo(String opanOrgNo) {
		this.opanOrgNo = opanOrgNo == null ? null : opanOrgNo.trim();
	}
	
    /**
     * @return OpanOrgNo
     */	
	public String getOpanOrgNo() {
		return this.opanOrgNo;
	}
	
	/**
	 * @param opanOrgName
	 */
	public void setOpanOrgName(String opanOrgName) {
		this.opanOrgName = opanOrgName == null ? null : opanOrgName.trim();
	}
	
    /**
     * @return OpanOrgName
     */	
	public String getOpanOrgName() {
		return this.opanOrgName;
	}
	
	/**
	 * @param orgNo
	 */
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo == null ? null : orgNo.trim();
	}
	
    /**
     * @return OrgNo
     */	
	public String getOrgNo() {
		return this.orgNo;
	}
	
	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName == null ? null : orgName.trim();
	}
	
    /**
     * @return OrgName
     */	
	public String getOrgName() {
		return this.orgName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param payAmt
	 */
	public void setPayAmt(java.math.BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	
    /**
     * @return PayAmt
     */	
	public java.math.BigDecimal getPayAmt() {
		return this.payAmt;
	}
	
	/**
	 * @param payUse
	 */
	public void setPayUse(String payUse) {
		this.payUse = payUse == null ? null : payUse.trim();
	}
	
    /**
     * @return PayUse
     */	
	public String getPayUse() {
		return this.payUse;
	}
	
	/**
	 * @param acctStatus
	 */
	public void setAcctStatus(String acctStatus) {
		this.acctStatus = acctStatus == null ? null : acctStatus.trim();
	}
	
    /**
     * @return AcctStatus
     */	
	public String getAcctStatus() {
		return this.acctStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param acctChgFlag
	 */
	public void setAcctChgFlag(String acctChgFlag) {
		this.acctChgFlag = acctChgFlag == null ? null : acctChgFlag.trim();
	}

	/**
	 * @return AcctChgFlag
	 */
	public String getAcctChgFlag() {
		return this.acctChgFlag;
	}

}