package cn.com.yusys.yusp.web.risk;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.risk.RiskItem0014Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0014Resource
 * @类描述: 白领易贷通产品校验
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-06-21 21:30:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "riskItem0014白领易贷通产品校验")
@RestController
@RequestMapping("/api/riskcheck/riskItem0014")
public class RiskItem0014Resource {

    @Autowired
    private RiskItem0014Service riskItem0014Service;
    /**
     * @方法名称: riskItem0014
     * @方法描述: 白领易贷通产品校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: shenli
     * @创建时间: 2021-06-21 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "白领易贷通产品校验")
    @PostMapping("/")
    protected ResultDto<RiskResultDto> riskItem0014(@RequestBody QueryModel queryModel) {
        //当流程中打回到客户经理节点时获取不到数据 因已走过风险拦截所以放行
        if(queryModel.getCondition().size()==0){
            RiskResultDto riskResultDto = new RiskResultDto();
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return  ResultDto.success(riskResultDto);
        }
        return riskItem0014Service.riskItem0014(queryModel);
    }
}
